{
    "host" : "cartographie.francetierslieux.fr",
    "sso" : [
        "tierslieuxorg"
    ],
    "welcomeTpl" : "costum.views.custom.franceTierslieux.home",
    "sourceKey" : true,
    "slug" : "franceTierslieux",
    "favicon" : "/images/franceTierslieux/logo-02.png",
    "metaImg" : "/images/franceTierslieux/banner.png",
    "mailsConfig" : {
        "footerTpl" : "costum.views.custom.franceTierslieux.emails.footer",
        "headerTpl" : "costum.views.custom.franceTierslieux.emails.header",
        "logo" : "/upload/communecter/organizations/5ec3b895690864db108b46b8/album/Banniere-site-recensement.png",
        "basic" : {
            "tpl" : "costum.views.custom.franceTierslieux.emails.remerciement"
        },
        "expeditor" : "France Tiers-Lieux"
    },
    "admin" : {
        "email" : "recensementtierslieux2023@communecter.org"
    },
    "js" : {
        "urls" : [ 
            "franceTierslieux_index.js"
        ]
    },
    "class" : {
        "function" : [ 
            "getSortedZone", 
            "elementAfterSave", 
            "elementAfterUpdate",
            "prepDataForUpdate"
        ]
    },
    "typeObj" : {
        "organizations" : {
            "add" : true,
            "color" : "dark",
            "name" : "Tiers-lieu",
            "icon" : "home",
            "dynFormCostum" : {
                "beforeBuild" : {
                    "properties" : {
                        "name" : {
                            "label" : "Nom du tiers-lieu",
                            "placeholder" : "Nom du tiers-lieu",
                            "rules" : {
                                "required" : true
                            },
                            "order" : 2
                        },
                        "similarLink" : {
                            "order" : 3
                        },
                        "openingDate" : {
                            "inputType" : "date",
                            "label" : "Quelle est la date d’ouverture de votre tiers-lieu ?",
                            "rules" : {
                                "required" : true
                            },
                            "order" : 5
                        },
                        "shortDescription" : {
                            "rules" : {
                                "required" : true
                            },
                            "order" : 6
                        },
                        "holderOrganization" : {
                            "inputType" : "text",
                            "label" : "Si différente, nom de la structure porteuse du tiers-lieu",
                            "order" : 7
                        },
                        "manageModel" : {
                            "inputType" : "select",
                            "label" : "Mode de gestion",
                            "placeholder" : "Structure juridique gérant votre tiers-lieu",
                            "list" : "manageModel",
                            "groupOptions" : false,
                            "groupSelected" : false,
                            "optionsValueAsKey" : true,
                            "noOrder" : true,
                            "select2" : {
                                "multiple" : false
                            },
                            "rules" : {
                                "required" : true
                            },
                            "order" : 8
                        },
                        "extraManageModel" : {
                            "inputType" : "text",
                            "label" : "Si autre mode de gestion, précisez",
                            "order" : 9
                        },
                        "typePlace" : {
                            "inputType" : "select",
                            "label" : "Dans quelle(s) famille(s) de tiers-lieux vous reconnaissez-vous ?",
                            "placeholder" : "Plusieurs champs possibles",
                            "list" : "typePlace",
                            "groupOptions" : false,
                            "groupSelected" : false,
                            "optionsValueAsKey" : true,
                            "select2" : {
                                "multiple" : true
                            },
                            "order" : 10
                        },
                        "extraTypePlace" : {
                            "inputType" : "text",
                            "label" : "Si autre famille de tiers-lieux, précisez",
                            "order" : 11
                        },
                        "compagnon" : {
                            "inputType" : "select",
                            "label" : "Vous souhaitez être inscrit comme “tiers-lieu compagnon” et ainsi accueillir des porteurs de projet qui souhaitent en savoir plus sur les tiers-lieux, bénéficier de vos conseils et de votre expérience ?",
                            "placeholder" : "Si oui, sélectionnez 'Compagnon FTL' ci-dessous",
                            "info" : "<a href='https://francetierslieux.fr/wp-content/uploads/2023/02/Charte_TLCompagnon.pdf' target='_blank'>Qu’est-ce qu’un tiers-lieu compagnon ?</a>",
                            "list" : "compagnon",
                            "groupOptions" : false,
                            "groupSelected" : false,
                            "optionsValueAsKey" : true,
                            "select2" : {
                                "multiple" : false
                            },
                            "order" : 12
                        },
                        "buildingSurfaceArea" : {
                            "inputType" : "text",
                            "label" : "Quelle est la surface de votre espace bâti (en m²) ?",
                            "placeholder" : "En m²",
                            "required" : {
                                "required" : true,
                                "number" : true
                            },
                            "order" : 13
                        },
                        "siteSurfaceArea" : {
                            "inputType" : "text",
                            "label" : "Quelle est la surface de votre terrain extérieur (en m²) ?",
                            "placeholder" : "En m²",
                            "required" : {
                                "required" : true,
                                "number" : true
                            },
                            "order" : 14
                        },
                        "formLocality" : {
                            "rules" : {
                                "required" : true
                            },
                            "order" : 15
                        },
                        "location" : {
                            "order" : 16
                        },
                        "image" : {
                            "label" : "Logo",
                            "itemLimit" : 1,
                            "order" : 17
                        },
                        "photo" : {
                            "label" : "Photo(s) du tiers-lieu",
                            "inputType" : "uploader",
                            "docType" : "image",
                            "showUploadBtn" : false,
                            "template" : "qq-template-gallery",
                            "filetypes" : [ 
                                "jpeg", 
                                "jpg", 
                                "gif", 
                                "png"
                            ],
                            "endPoint" : "/forcedUnloggued/true",
                            "contentKey" : "slider",
                            "domElement" : "photoElement",
                            "itemLimit" : 3,
                            "order" : 18
                        },
                        "socialNetwork" : {
                            "inputType" : "lists",
                            "label" : "Réseaux sociaux",
                            "entries" : {
                                "platform" : {
                                    "type" : "select",
                                    "label" : "Plateforme",
                                    "options" : [ 
                                        "votre page Facebook", 
                                        "votre compte Twitter", 
                                        "votre compte Instagram", 
                                        "votre compte Linkedin", 
                                        "votre compte Mastodon", 
                                        "votre page Movilab"
                                    ],
                                    "class" : "col-lg-5"
                                },
                                "url" : {
                                    "type" : "text",
                                    "placeholder" : "http://www.exemple.org",
                                    "rules" : {
                                        "url" : true
                                    },
                                    "label" : "Lien",
                                    "class" : "col-lg-5"
                                }
                            },
                            "order" : 19
                        },
                        "url" : {
                            "label" : "Site internet (commençant par \" http:// \")",
                            "order" : 20
                        },
                        "openingHours" : {
                            "inputType" : "openingHours",
                            "label" : "Horaires d'ouverture",
                            "class" : "repeatWeek",
                            "checked" : "",
                            "order" : 21
                        },
                        "email" : {
                            "label" : "Email contact",
                            "rules" : {
                                "required" : true,
                                "email" : true
                            },
                            "order" : 22
                        },
                        "telephone" : {
                            "inputType" : "text",
                            "label" : "Contact téléphone",
                            "required" : {
                                "required" : true,
                                "number" : true
                            },
                            "order" : 23
                        },
                        "video" : {
                            "label" : "Lien vers vidéo de présentation",
                            "placeholder" : "Lien vers vidéo de présentation",
                            "inputType" : "array",
                            "initOptions" : {
                                "type" : "video",
                                "labelAdd" : "Ajouter un lien vidéo"
                            },
                            "order" : 30
                        },
                        "description" : {
                            "label" : "Description longue",
                            "inputType" : "textarea",
                            "markdown" : true,
                            "order" : 31
                        },
                        "mainTag" : {
                            "inputType" : "hidden"
                        }
                    }
                },
                "afterSave" : "costum.franceTierslieux.organizations.afterSave",
                "formData" : "costum.franceTierslieux.organizations.formData",
                "afterBuild" : "costum.franceTierslieux.organizations.afterBuild",
                "onload" : {
                    "actions" : {
                        "setTitle" : "<span class='text-title'>VOUS VOULEZ AJOUTER VOTRE TIERS-LIEU SUR LA CARTE</span><span class='interogation'>?</span>",
                        "html" : {
                            "infocustom" : ""
                        },
                        "presetValue" : {
                            "mainTag" : "TiersLieux",
                            "role" : "admin"
                        },
                        "hide" : {
                            "tagstags" : 1,
                            "publiccheckboxSimple" : 1,
                            "categoryselect" : 1,
                            "roleselect" : 1,
                            "typeselect" : 1,
                            "extraManageModeltext" : 1,
                            "extraTypePlacetext" : 1
                        }
                    }
                },
                "config" : {
                    "unlogged" : {
                        "gtu" : "<span class='col-xs-12'>En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='https://francetierslieux.fr/cgu/' target='_blank' style='color: #FF286B;'>CGU</a>.</span>"
                    }
                }
            }
        }
    },
    "lists" : {
        "level" : {
            "level1" : "National",
            "level3" : "Régional",
            "level4" : "Départemental",
            "level5" : "Communauté de communes"
        },
        "typePlace" : [ 
            "Ateliers artisanaux partagés", 
            "Bureaux partagés / Coworking", 
            "Cuisine partagée / Foodlab", 
            "Fablab / Makerspace / Hackerspace (Espaces du Faire)", 
            "LivingLab / Laboratoire d'innovation sociale", 
            "Tiers-lieu nourricier", 
            "Tiers-lieu culturel / Lieux intermédiaires et indépendants", 
            "Autre"
        ],
        "services" : [ 
            "Accompagnement des publics", 
            "Action sociale", 
            "Aiguillage / Orientation", 
            "Bar / café", 
            "Boutique / Épicerie", 
            "Coopérative d'Activités et d'Emploi / Groupement d'employeur", 
            "Cantine / restaurant", 
            "Centre de ressources", 
            "Chantier participatif", 
            "Complexe évènementiel", 
            "Conciergerie ", 
            "Domiciliation", 
            "Espace de stockage", 
            "Espace détente", 
            "Espace enfants", 
            "Formation / Transfert de savoir-faire / Éducation", 
            "Habitat", 
            "Incubateur", 
            "Lieu d'éducation populaire et nouvelles formes d'apprentissage", 
            "Maison de services au public", 
            "Marché", 
            "Média et son", 
            "MediaLab", 
            "Médiation numérique", 
            "Offre artistique ou culturelle", 
            "Pépinière d'entreprises", 
            "Point d'appui à la vie associative", 
            "Point Information Jeunesse", 
            "Point d'information touristique", 
            "Pratiques de soin (art thérapie, massage, médecine douce, méditation...)", 
            "Résidences d'artistes", 
            "Ressourcerie / recyclerie", 
            "Service enfance-jeunesse", 
            "Services liés à la mobilité"
        ],
        "manageModel" : [ 
            "Association", 
            "Collectif citoyen", 
            "Universités / Écoles d’ingénieurs ou de commerce / EPST", 
            "Établissements scolaires (Lycée, Collège, Ecole)", 
            "Collectivités (Département, Intercommunalité, Région, etc)", 
            "SARL-SA-SAS", 
            "SCIC", 
            "SCOP", 
            "Autre"
        ],
        "state" : [ 
            "En projet", 
            "Ouvert", 
            "Fermé"
        ],
        "spaceSize" : [ 
            "Moins de 60m²", 
            "Entre 60 et 200m²", 
            "Plus de 200m²"
        ],
        "network" : [ 
            "A+ C'est Mieux", 
            "Actes‐IF", 
            "Artfactories/Autresparts", 
            "Bienvenue dans la Canopée", 
            "Cap Tiers‐Lieux", 
            "Cédille Pro", 
            "Collectif des Tiers‐Lieux Ile‐de‐France", 
            "Collectif Hybrides", 
            "Compagnie des Tiers‐Lieux", 
            "Coopérative des Tiers‐Lieux", 
            "Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)", 
            "Cowork'in Tarn", 
            "Coworking Grand Lyon", 
            "CRLII Occitanie", 
            "GIP Recia", 
            "Hub France Connectée", 
            "L'ALIM (l'Assemblée des lieux intermédiaires marseillais)", 
            "La Trame 07", 
            "Label Tiers‐Lieux Occitanie", 
            "Label C3", 
            "Label Tiers‐Lieux Normandie", 
            "Le DOG", 
            "Le LIEN", 
            "Lieux Intermédiaires en région Centre", 
            "Réseau des tiers‐lieux Bourgogne Franche Comté", 
            "Réseau Français des Fablabs", 
            "Réseau Médoc", 
            "Réseau TELA", 
            "Tiers‐Lieux Edu"
        ],
        "regionalNetwork" : [ 
            "Réseau Relief (Auvergne Rhône-Alpes)", 
            "Réseau Tiers-lieux Bourgogne-France-Comté (BFC)", 
            "Réseau Bretagne Tiers-Lieux (Bretagne)", 
            "Réseau Ambition Tiers Lieux (Centre-Val-de-Loire)", 
            "Réseau Da Locu (Corse)", 
            "Réseau Tiers-Lieu Grand Est", 
            "Réseau La Compagnie des Tiers-Lieux (Hauts-de-France)", 
            "Réseau Ile de France Tiers Lieux (Consortium réunissant A+ c’est mieux, Actifs, le Collectif des Tiers-lieux, Makers IDF)", 
            "Réseau TILINO (Normandie)", 
            "Réseau La Coopérative Tiers-Lieux (Nouvelle Aquitaine)", 
            "Réseau La Rosée (Occitanie)", 
            "Réseau CAP Tiers-Lieux (Pays-de-la-Loire) et la CRESS Pays de la Loire", 
            "Réseau Sud Tiers-Lieux (Provence Alpes Côte d’Azur)", 
            "Réseau La Réunion des Tiers-Lieux (La Réunion)", 
            "Réseau régional des Tiers-lieux de Martinique", 
            "Réseau régional des Tiers-lieux de Guadeloupe"
        ],
        "themeNetwork" : [ 
            "Artfactories/Autresparts", 
            "Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)", 
            "Réseau Français des Fablabs, Espaces et Communautés du Faire (RFFLabs)", 
            "Tiers-Lieux Edu", 
            "Réseau National des Ressourceries et Recycleries", 
            "Réseau Cocagne", 
            "Réseau des CREFAD", 
            "Réseau des Cafés Culturels et Cantines Associatifs (RECCCA)", 
            "Les tiers-lieuses", 
            "Autres"
        ],
        "localNetwork" : [ 
            "Actes-If - Lieux intermédiaires et indépendants en Ile de France", 
            "A+ c’est mieux !", 
            "Cédille Pro", 
            "Collectif Hybrides", 
            "Collectif des Tiers-Lieux Ile-de-France", 
            "Coworking Grand Lyon", 
            "Cowork’in Tarn", 
            "La Trame 07", 
            "AliiCe - Association des Lieux Intermédiaires indépendants en région Centre", 
            "Réseau Médoc", 
            "Réseau TELA", 
            "RedLab (Réseau des Labs d'Occitanie)", 
            "Makers IDF", 
            "Le LIEN", 
            "CRLii Occitanie", 
            "L’ALIM (l’Assemblée des lieux intermédiaires marseillais) / Collectif indéterminé", 
            "Autre"
        ],
        "hubFranceConnecte" : [ 
            "Hub du Sud", 
            "Hubik", 
            "Les Assembleurs", 
            "MedNum Bourgogne-Franche-Comté", 
            "Hub PiNG", 
            "Hub Ultra Numérique", 
            "Hub AURA", 
            "Hubert", 
            "Hub Ile-de-France", 
            "Hub Occitanie", 
            "Hub Antilles-Guyane", 
            "Autres"
        ],
        "greeting" : [ 
            "Conseil et orientation avec contact téléphonique", 
            "Point d'échange physique proposé pour monter en compétences respectivement", 
            "Voyage apprenant pour découverte de lieux", 
            "Accueil d'élus pour acculturation", 
            "Visioconférence de présentations", 
            "Étude d'opportunité", 
            "Aide à la réalisation de dossiers pour AMI ou subventions", 
            "Conseil projet", 
            "Incubation de projets", 
            "Mentoring / coaching de porteurs de projet", 
            "Parcours de formation pour des porteurs de projet"
        ],
        "certification" : [ 
            "Fabrique de Territoire", 
            "Fabrique Numérique de Territoire"
        ],
        "compagnon" : [ 
            "Compagnon France Tiers-Lieux"
        ],
        "territory" : [ 
            "En agglomérations", 
            "En métropole", 
            "En milieu rural", 
            "En ville moyenne (entre 20000 et 100000 habitants)"
        ]
    },
    "htmlConstruct" : {
        "loadingModal" : {
            "logo" : "/images/franceTierslieux/logo-02.png"
        },
        "appRendering" : "horizontal",
        "redirect" : {
            "logged" : "search",
            "unlogged" : "search"
        },
        "adminPanel" : {
            "js" : true,
            "add" : true,
            "menu" : {
                "statistic" : true,
                "directory" : false,
                "converter" : true,
                "import" : true,
                "community" : {
                    "show" : true
                },
                "reference" : {
                    "show" : true,
                    "initType" : [ 
                        "organizations"
                    ]
                },
                "tiersLieux" : {
                    "label" : "Tiers-lieux",
                    "class" : "text-dark",
                    "view" : "tiersLieux",
                    "icon" : "map-marker"
                },
                "existingReference" : {
                    "label" : "Référencement des lieux communectés",
                    "class" : "text-dark",
                    "view" : "existingReference",
                    "icon" : "list"
                }
            }
        },
        "preview" : {
            "element" : "costum.views.custom.franceTierslieux.preview"
        },
        "header" : {
            "menuTop" : {
                "left" : {
                    "buttonList" : {
                        "logo" : {
                            "height" : 40,
                            "href" : "#"
                        },
                        "searchBar" : {
                            "class" : " pull-left margin-top-5 margin-left-15 hidden-xs"
                        }
                    },
                    "xsMenu" : {
                        "buttonList" : {
                            "app" : {
                                "label" : true,
                                "icon" : false,
                                "spanTooltip" : false,
                                "labelClass" : "padding-left-10",
                                "buttonList" : {
                                }
                            }
                        }
                    }
                },
                "center" : {
                    "addClass" : "margin-top-15 pull-left",
                    "buttonList" : {
                        "app" : {
                            "label" : true,
                            "buttonList" : {
                            },
                            "icon" : false,
                            "spanTooltip" : false,
                            "labelClass" : "text-white bg-main2 padding-10 margin-left-30 font-helvetica border-radius-3 font-weight-600"
                        }
                    }
                },
                "right" : {
                    "addClass" : "margin-top-5",
                    "buttonList" : {
                        "userProfil" : {
                            "dashboard" : true,
                            "class" : "pull-left col-xs-11 col-xs-offset-1 no-padding",
                            "view" : "costum.views.custom.franceTierslieux.accountModal",
                            "label" : "Tableau de bord",
                            "tooltips" : "Tableau de bord"
                        },
                        "dropdown" : {
                            "connected" : true,
                            "buttonList" : {
                                "admin" : true,
                                "settings" : true,
                                "logout" : true
                            }
                        },
                        "login" : true,
                        "map" : true
                    }
                }
            }
        },
        "menuBottom" : {
            "buttonList" : {
                "add" : {
                    "label" : "Ajouter un tiers-lieu"
                }
            }
        },
        "menuRight" : {
            "buttonList" : {
                "map" : {
                    "icon" : "list",
                    "label" : "Afficher l’annuaire",
                    "id" : "show-button-map",
                    "addClass" : "changelabel"
                }
            }
        },
        "element" : {
            "initView" : "detail",
            "js" : "pageProfil.js",
            "css" : "pageProfil.css",
            "banner" : {
                "headerInfos" : "costum.views.custom.franceTierslieux.element.headerInfos",
                "img" : "/images/franceTierslieux/bannerFTLgradient.png",
                "preferences" : false,
                "editButton" : {
                    "dynform" : true
                },
                "badges" : true,
                "costumize" : {
                    "url" : "costum.views.custom.franceTierslieux.admin.costumize",
                    "slug" : "reseauTierslieux",
                    "restricted" : {
                        "types" : [ 
                            "organizations"
                        ],
                        "category" : [ 
                            "network"
                        ]
                    }
                }
            },
            "containerClass" : {
                "centralSection" : "col-xs-12",
                "centralSectionSub" : "col-xs-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1"
            },
            "menuLeft" : false,
            "menuTop" : {
                "left" : {
                    "buttonList" : {
                        "imgUploader" : true
                    }
                },
                "right" : {
                    "buttonList" : {
                        "imgProfil" : true,
                        "nameProfil" : true,
                        "xsMenu" : {
                            "label" : "Menu",
                            "buttonList" : {
                                "detail" : true,
                                "community" : true
                            }
                        },
                        "detail" : true,
                        "community" : true,
                        "gallery" : {
                            "label" : "Photos",
                            "icon" : "camera",
                            "view" : "photos"
                        },
                        "invite" : true,
                        "events" : {
                            "label" : "Agenda",
                            "icon" : "calendar",
                            "dataAttr" : {
                                "dir" : "events",
                                "icon" : "calendar"
                            },
                            "restricted" : {
                                "types" : [ 
                                    "organizations"
                                ],
                                "category" : [ 
                                    "network"
                                ]
                            }
                        },
                        "settings" : {
                            "class" : "params-ftl",
                            "labelClass" : "hidden-xs hidden-sm"
                        },
                        "settingsCommunity" : {
                            "class" : "params-ftl",
                            "labelClass" : "hidden-xs hidden-sm"
                        }
                    }
                }
            }
        }
    },
    "css" : {
        "urls" : [ 
            "franceTierslieux_index.css", 
            "franceTierslieux_filters.css"
        ],
        "loader" : {
            "background" : "white",
            "ring1" : {
                "color" : "#4623c9",
                "height" : 360,
                "width" : 360,
                "left" : -15,
                "borderWidth" : 4,
                "top" : -35
            },
            "ring2" : {
                "color" : "#4623c9",
                "height" : 350,
                "width" : 350,
                "left" : -10,
                "borderWidth" : 5,
                "top" : -30
            }
        },
        "progress" : {
            "value" : {
                "background" : "#4623c9"
            },
            "bar" : {
                "background" : "#fff"
            }
        },
        "menuTop" : {
            "background" : "white",
            "button" : {
                "paddingTop" : 5,
                "fontSize" : 20,
                "color" : "#fff"
            },
            "badge" : {
                "background" : "#337ab7",
                "border" : "1px solid white"
            },
            "scopeBtn" : {
                "display" : "none"
            },
            "connectBtn" : {
                "background" : "#000",
                "color" : "white",
                "fontSize" : 18,
                "borderRadius" : 10,
                "padding" : "8px 15px"
            }
        },
        "menuApp" : {
            "display" : "none",
            "top" : "58",
            "background" : "#ef5b2b",
            "button" : {
                "fontSize" : 20,
                "color" : "#fff",
                "padding-left" : "5px",
                "hover" : {
                    "borderBottom" : "2px solid #fff"
                }
            }
        },
        "button" : {
            "footer" : {
                "add" : {
                    "bottom" : 50,
                    "background" : "transparent",
                    "color" : "white"
                }
            }
        },
        "color" : {
            "green" : "#93C020",
            "blue" : "#3595a8",
            "dark" : "#161315"
        }
    },
    "app" : {
        "#welcome" : {
            "subdomainName" : "Les tiers-lieux",
            "hash" : "#app.search",
            "icon" : "search",
            "filterObj" : "costum.views.custom.franceTierslieux.filters",
            "useFilter" : true,
            "useHeader" : false,
            "showMap" : true,
            "filters" : {
                "types" : [ 
                    "organizations"
                ]
            },
            "searchObject" : {
                "indexStep" : "0"
            },
            "menuRight" : false
        },
        "#search" : {
            "subdomainName" : "Les projets",
            "icon" : "search",
            "filterObj" : "costum.views.custom.franceTierslieux.filters",
            "useFilter" : true,
            "useHeader" : false,
            "showMap" : true,
            "filters" : {
                "types" : [ 
                    "organizations"
                ]
            },
            "searchObject" : {
                "indexStep" : "0"
            },
            "menuRight" : false
        },
        "#dashboard" : {
            "hash" : "#francetierslieux.dashboard",
            "icon" : "binoculars",
            "module" : "costum",
            "header" : "costum.views.custom.franceTierslieux.filters",
            "urlExtra" : "/url/costum.views.custom.franceTierslieux.dashboard",
            "useHeader" : true,
            "useFilter" : false,
            "useFooter" : true,
            "dropdownResult" : true,
            "subdomainName" : "L'observatoire des Tiers-lieux",
            "placeholderMainSearch" : "what are you looking for ?",
            "useMapBtn" : false
        },
        "#dataAccess" : {
            "hash" : "#app.view",
            "icon" : "graph",
            "urlExtra" : "/page/dataAccess",
            "useHeader" : true,
            "useFooter" : true,
            "useFilter" : false,
            "inMenu" : true,
            "subdomainName" : "Données"
        },
        "#observatoire" : {
            "staticPage" : true,
            "subdomainName" : "Observatoire des Tiers-lieux",
            "placeholderMainSearch" : "Observatoire",
            "hash" : "#app.view",
            "icon" : "",
            "urlExtra" : "/page/observatoire",
            "useHeader" : true,
            "useFilter" : false,
            "inMenu" : false
        },
        "#metier" : {
            "hash" : "#app.view",
            "icon" : "hands",
            "useFooter" : true,
            "urlExtra" : "/url/costum.views.custom.franceTierslieux.metier",
            "useHeader" : true,
            "useFilter" : false,
            "inMenu" : true,
            "subdomainName" : "Metier"
        },
        "#competence" : {
            "hash" : "#app.view",
            "icon" : "hands",
            "useFooter" : true,
            "urlExtra" : "/url/costum.views.custom.franceTierslieux.competence",
            "useHeader" : true,
            "useFilter" : false,
            "inMenu" : true,
            "subdomainName" : "Competence"
        },
        "#relocalisation" : {
            "hash" : "#app.view",
            "icon" : "hands",
            "useFooter" : true,
            "urlExtra" : "/url/costum.views.custom.franceTierslieux.relocalisation",
            "useHeader" : true,
            "useFilter" : false,
            "inMenu" : true,
            "subdomainName" : "Relocalisation"
        },
        "#anatole" : {
            "hash" : "#app.view",
            "icon" : "hands",
            "useFooter" : true,
            "urlExtra" : "/url/costum.views.custom.franceTierslieux.anatole",
            "useHeader" : true,
            "useFilter" : false,
            "inMenu" : true,
            "subdomainName" : "Anatole"
        }
    },
    "map" : {
        "menuRight" : false,
        "defaultPos" : [ 
            46.7342232, 
            2.74686
        ],
        "activePreview" : true
    },
    "filters" : {
        "searchTypeGS" : [ 
            "organizations"
        ],
        "sourceKey" : true
    }
}