costum.init = function(where){
    if(typeof costum.filters != "undefined"){
        if(typeof costum.filters.scopes != "undefined"){
            if(typeof costum.filters.scopes.cities != "undefined"){
                setOpenBreadCrum({'cities': costum.filters.scopes.cities }, true, null, "costum");
            }
            if(typeof costum.filters.scopes.zones != "undefined"){
                setOpenBreadCrum({'zones': costum.filters.scopes.zones}, true, null, "costum");
            }
        
        }
        if(typeof costum.filters.sourceKey != "undefined"){
            searchObject.sourceKey=costum.request.sourceKey;
        }
    }
    // if(typeof costum.js != "undefined" && typeof costum.js.urls != "undefined"){
    //     $.each(costum.js.urls, function(e,v){
    //         lazyLoad(assetPath+"/js/"+costum.slug+"/"+v, null, function(){
                if( typeof costum[costum.slug] != "undefined" && 
                    typeof costum[costum.slug].init != "undefined" && 
                    typeof costum[costum.slug].init == "function")
                    costum[costum.slug].init(); 
    //         });
    //     });
    // }
    
    if(typeof costum != "undefined"){
        costum.initCssCustom();
        costum.addClassSection ();
    }
    if(typeof costum.typeObj != "undefined")
        costum.initTypeObj(costum.typeObj);
    /*if(typeof costum != "undefined" && typeof costum.searchObject != "undefined" && typeof costum.searchObject.indexStep != "undefined") 
        searchObject.indexStep=costum.searchObject.indexStep;
    if(typeof costum != "undefined" && typeof costum.searchObject != "undefined" && typeof costum.searchObject.sortBy != "undefined")
        searchObject.sortBy=costum.searchObject.sortBy;*/
    // mylog.log("directoryViewMode 1", directoryViewMode);
    // if(typeof costum.htmlConstruct != "undefined" 
    //     && typeof costum.htmlConstruct.directory != "undefined"
    //     && typeof costum.htmlConstruct.directory.viewMode != "undefined"){
    //     mylog.log("directoryViewMode 2", directoryViewMode);
    //     directoryViewMode=costum.htmlConstruct.directory.viewMode;
    // }
    if(typeof costum.htmlConstruct != "undefined"
        && typeof costum.htmlConstruct.directoryViewMode != "undefined"){
        mylog.log("directoryViewMode 2", directoryViewMode);
        directoryViewMode=costum.htmlConstruct.directoryViewMode;
        directory.viewMode = directoryViewMode;
    }
    costum.htmlConstruct.header.menuTop=jQuery.extend(true,{},themeParams.header.menuTop);
    mylog.log("directoryViewMode 3", directoryViewMode); 

    var link = window.location.href;
    var edit = "?edit=true";
    if( (userId == "" ||  typeof userId == "undefined") && (link.includes(edit) == true || link.includes("/edit/true") == true)){
		toastr.error("<p>"+tradCms.adminArea+"</p>");
		Login.openLogin();
	} else if ( costum.editMode == false && costum.isCostumAdmin && (link.includes(edit) == false || link.includes("/edit/true") == false)) {
        var btnEdit =
            '<li class="menu-button btn-menu btn-menu-tooltips menu-btn-top">' +
            '<a href="javascript:;" class="modeEditBtn cosDyn-buttonList">' +
            '<i class="fa fa-pencil"></i> '+trad.edit +
            '</a>' +
            '</li>';
        $('#menuTopRight .dropdown-menu-top ul.dropdown-menu').append(btnEdit);
        $(".modeEditBtn").off().on('click',  function() {
            var urlCostum = link+edit;
            if(link.includes('#')) {
                urlCostum = link.split('#').shift()+edit+location.hash;
            }
            window.open(urlCostum);
        });
    } else if (userId != "" && costum.isCostumAdmin === false) {
        toastr.info(tradCms.featureforadmin);
    }

    // initialize cssFile
    $.each(["#menuTopLeft","#menuTopCenter","#menuTopRight"],function(e,dom){
        if(!notEmpty($(dom).html()))
            $(dom).hide();
    });
    var strCss = costum.getCss();
    str="<style id='cssFile' type='text/css'>";
    if(notEmpty(strCss))
        str+= strCss;
        str+="</style>"; 

    $("head").append(str);

};
costum.initTypeObj=function(typeObjCostum){
    mylog.log("costum.initTypeObj", typeObjCostum);
    $.each(typeObj, function(e, obj){
        var entryObj=e;
        if(typeof obj.sameAs != "undefined")
                entryObj=obj.sameAs;
        if(typeof typeObjCostum[e] != "undefined"){
            if(typeof typeObjCostum[e] == "object"){
                $.each(typeObjCostum[e], function(entry, value){
                    typeObj[entryObj][entry]=value;
                });
            }
        }else if(typeof obj.add != "undefined" && obj.add){
            typeObj[entryObj].add=false;
        }
    });
    $.each(typeObjCostum, function(e, v){
        if(typeof typeObj[e]=="undefined")
            typeObj[e]=v;
    });
    mylog.log("initTypeObj typeObj", typeObj);
    typeObj.buildCreateButton(".toolbar-bottom-adds", true, null, costum);
    coInterface.bindButtonOpenForm();
};
costum.styleProperties={
    "background": "background",
    "backgroundColor" : "background-color",
    "backgroundSize" : "background-size",
    "border": "border",
    "borderBottom": "border-bottom",
    "borderLeft": "border-left",
    "borderRight": "border-right",
    "borderTop": "border-top",
    "borderWidth": "border-width",
    "borderColor": "border-color",
    "borderRadius": "border-radius",
    "box": "box-shadow",
    "boxShadow": "box-shadow",
    "fontSize": "font-size",
    "fontWeight": "font-weight",
    "color": "color",
    "padding": "padding",
    "paddingTop": "padding-top",
    "paddingBottom": "padding-bottom",
    "paddingLeft": "padding-left",
    "paddingRight": "padding-right",
    "margin": "margin",
    "marginTop": "margin-top",
    "marginBottom": "margin-bottom",
    "marginLeft": "margin-left",
    "marginRight": "margin-right",
    "height": "height",
    "width": "width",
    "top": "top",
    "bottom": "bottom",
    "right": "right",
    "left": "left",
    "lineHeight": "line-height",
    "display": "display",
    "textTransform": "text-transform",
    "textDecoration": "text-decoration",
    "textAlign": "text-align",
    "position": "position",
    "verticalAlign":"vertical-align",
    "float": "float",
    "clipPath":"clip-path"
};
costum.cssStyleWithPx=[
    "fontSize",
    "width", 
    "height", 
    "borderWidth", 
    "padding", 
    "paddingTop", 
    "paddingBottom", 
    "paddingLeft", 
    "paddingRight",
    "margin", 
    "marginTop", 
    "marginBottom", 
    "marginLeft", 
    "marginRight", 
    "top",
    "left",
    "right", 
    "left", 
    "lineHeight"
];
costum.getSpecPx=function(k, v){
    if($.inArray(k, costum.cssStyleWithPx) >=0){
        if(typeof v == "number"){
            // alert("")
            return "px";
        }
        else if(v == "auto")
            return "";
        else if (v.toString().indexOf("%") >=0){
            //alert("ici%");
            return "";
        }
        else if(v.toString().indexOf("px")>=0){
            //alert("icipx");
            return "";
        }
        else
            return "px";
    }else
        return "";
};
costum.getStyleCustom=function(cssObj,cssPath){
    var cssAdd="";
    $.each(costum.styleProperties, function(e, v){
        if(jsonHelper.notNull(cssPath+"."+e))
            cssAdd+=v+":"+costum.value(cssObj,cssPath+"."+e)+costum.getSpecPx(e, costum.value(cssObj,cssPath+"."+e))+";";
    
    });
   
    return cssAdd;
};
costum.getUpdateClass=function(selectClass,column,newClass){
    var classDiv = $(selectClass).attr("class");
    mylog.log("classDiv",classDiv);
    if(newClass == "col-"+column+"-")
        newClass = "";

    for (let index = 1; index <= 12; index++) {
        if (classDiv.indexOf("col-"+column+"-"+index)!==-1){
            classDiv=classDiv.replace("col-"+column+"-"+index,"");
            mylog.log("classDiv2",classDiv);
        }
    }
    //$(selectClass).removeAttr("class");
    $(selectClass).attr("class",(classDiv+" "+newClass).replace(/\s+/g, ' '));
};
costum.getClassCustom=function(classSection,cssObj){
    var md = jsonHelper.getValueByPath(cssObj,"columnWidth.colMd");
    var sm = jsonHelper.getValueByPath(cssObj,"columnWidth.colSm");
    var xs = jsonHelper.getValueByPath(cssObj,"columnWidth.colXs");
    if(typeof md != "" && typeof md != "undefined"){
        costum.getUpdateClass(classSection,"md","col-md-"+md)
    }
    if(typeof sm != "" && typeof sm != "undefined"){
        costum.getUpdateClass(classSection,"sm","col-sm-"+sm)
    }
    if(typeof xs != "" && typeof xs != "undefined") {
        costum.getUpdateClass(classSection,"xs","col-xs-"+xs)
    }
};

costum.getCss = function(){

                var result = new Array();
                extraParamsAjax={
                    async: false
                };

                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cssFile/getbycostumid",
                    {id: costum.contextId},
                    function(data){ 
                        $.each(data, function(key,value) {
                            if (notEmpty(value.css)){
                                result.push({"css" : value.css})
                            }
                         });
                      },
                    null,
                    "json",
                    extraParamsAjax
                    );
                
				mylog.log("cssFile costum");
                if (result.length > 0){
                    return result[0]["css"];
                }
                else{
                    return null;
                }

};


costum.initCssCustom=function(cssObj,path){
    var cssObj = (notNull(cssObj)) ? cssObj : costum.css;
    var style = (notNull(path)) ? path : "costum.css";
    // if(jsonHelper.notNull(style+".urls")  && costum.css.urls.length >= 1){
    //     $.each(costum.css.urls, function(e,v){
    //         lazyLoad(null, assetPath+"/css/"+costum.slug+"/"+v, null);
    //     });
    // }
    var str="";
    var idCSS="amazing-costumizer-css";
    var removeDomCss="";
    if($("#amazing-costumizer-css").length > 0){
        idCSS="amazing-costumizer-css-new";
        removeDomCss="#amazing-costumizer-css";
    }
    else if($("#amazing-costumizer-css-new").length > 0)
        removeDomCss="#amazing-costumizer-css-new";

    if($("#iframe-xs").contents().find("#amazing-costumizer-css").length > 0){
        $("#iframe-xs").contents().find("#amazing-costumizer-css").remove();
    }
    else if($("#iframe-xs").contents().find("#amazing-costumizer-css-new").length > 0)
        $("#iframe-xs").contents().find("#amazing-costumizer-css-new").remove();

    if(jsonHelper.notNull(style+".font") ){
        let urlString = "";
        if(exists(cssObj.font.useUploader) && cssObj.font.useUploader == true){
            urlString = uploadedFont.replaceAll('/', "");
            str+="@font-face {font-family: 'customFont"+urlString.replaceAll(" ","")+"';src: url('"+uploadedFont+"')}";
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont"+urlString.replaceAll(" ","")+"'}";
        } else if(exists(cssObj.font.useUploader) && cssObj.font.useUploader == false){
            urlString = cssObj.font.url.replaceAll('/', "");
            str+="@font-face {font-family: 'customFont"+urlString.replaceAll(" ","")+"';src: url('"+assetPath+"/"+cssObj.font.url+"')}";
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont"+urlString.replaceAll(" ","")+"'}";            
        } else {
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;}";
        }
    } 

    if(jsonHelper.notNull(style+".fonts") ){

        $.each(cssObj.fonts, function(kF,vF){
            str+="@font-face { font-family: '"+vF.name+"';src: url('"+assetPath+"/"+vF.url+"')}";
        });
    }

    if(jsonHelper.notNull(style+".styles") ){
        $.each(cssObj.styles, function(e,r){
           if(e.indexOf("#"))
            str += e+"{";
           $.each(r, function(k,c){
            str += k+":"+c;
           });
           str += "}"; 
        });

    }
    // Old way do some shit nain;
    /*if(jsonHelper.notNull(style+".menuTop") ) {
        costum.getClassCustom("#mainNav", cssObj, style + ".menuTop");
        if (jsonHelper.notNull(style + ".menuTop.left")) {
            costum.getClassCustom("#mainNav #menuTopLeft", cssObj, style + ".menuTop.left");
        }
        if (jsonHelper.notNull(style + ".menuTop.center")) {
            costum.getClassCustom("#mainNav #menuTopCenter", cssObj, style + ".menuTop.center");
        }
        if (jsonHelper.notNull(style + ".menuTop.right")) {
            costum.getClassCustom("#mainNav #menuTopRight", cssObj, style + ".menuTop.right");
        }
    }
    if(jsonHelper.notNull(style+".subMenu") ){
        costum.getClassCustom("#subMenu",cssObj, style+".subMenu");
    }
    if(jsonHelper.notNull(style+".menuLeft") ) {
        costum.getClassCustom("#menuLeft", cssObj, style + ".menuLeft");
    }*/

    /*if(jsonHelper.notNull(style+".menuTop") ){
        str+="#mainNav{"+costum.getStyleCustom(cssObj, style+".menuTop")+"}";
        costum.getClassCustom("#mainNav",cssObj, style+".menuTop");
        if(jsonHelper.notNull(style+".menuTop.left") ){
            str+="#mainNav #menuTopLeft{"+costum.getStyleCustom(cssObj, style+".menuTop.left")+"}";
            costum.getClassCustom("#mainNav #menuTopLeft",cssObj, style+".menuTop.left");

        }
        if(jsonHelper.notNull(style+".menuTop.center") ){
            str+="#mainNav #menuTopCenter{"+costum.getStyleCustom(cssObj, style+".menuTop.center")+"}";
            costum.getClassCustom("#mainNav #menuTopCenter",cssObj, style+".menuTop.center");
        }
        if(jsonHelper.notNull(style+".menuTop.right") ){
            str+="#mainNav #menuTopRight{"+costum.getStyleCustom(cssObj, style+".menuTop.right")+"}";
            costum.getClassCustom("#mainNav #menuTopRight",cssObj, style+".menuTop.right");
        }
        if(jsonHelper.notNull(style+".menuTop.header") )
            str+="#headerBand{"+costum.getStyleCustom(cssObj, style+".menuTop.header")+"}";
        if(jsonHelper.notNull(style+".menuTop.logo") )
            str+="#mainNav .logo-menutop{"+costum.getStyleCustom(cssObj, style+".menuTop.logo")+"}";
        if(jsonHelper.notNull(style+".menuTop.button") ){
            str+="#mainNav .menu-btn-top{"+costum.getStyleCustom(cssObj, style+".menuTop.button")+"}";
        }
        if(jsonHelper.notNull(style+".menuTop.badge") )
            str+=".btn-menu-notif .notifications-count, .btn-menu-chat .chatNotifs, .btn-dashboard-dda .coopNotifs{"+costum.getStyleCustom(cssObj, style+".menuTop.badge")+"}";
        if(jsonHelper.notNull(style+".menuTop.scopeBtn") ){
            str+=".menu-btn-scope-filter{"+costum.getStyleCustom(cssObj, style+".menuTop.scopeBtn")+"}";
            if(jsonHelper.notNull(style+".menuTop.scopeBtn.hover") )
                str+=".menu-btn-scope-filter:hover, .menu-btn-scope-filter.active{"+costum.getStyleCustom(cssObj, style+".menuTop.scopeBtn.hover")+"}";
        }
        if(jsonHelper.notNull(style+".menuTop.app") ){
            str+="#mainNav .menu-app-top{"+costum.getStyleCustom(cssObj, style+".menuTop.app")+"}";
            if(jsonHelper.notNull(style+".menuTop.app.button") )
                str+="#mainNav .lbh-menu-app,#mainNav .lbh-urlExtern,#mainNav .lbh-anchor,#mainNav .lbh-dropdown,#mainNav .lbh-default{"+costum.getStyleCustom(cssObj, style+".menuTop.app.button")+"}";
            if(jsonHelper.notNull(style+".menuTop.app.button.design") )
                str+= "@media (min-width: 767px){"+
                        "#mainNav .lbh-menu-app,#mainNav .lbh-urlExtern,#mainNav .lbh-anchor,#mainNav .lbh-dropdown,#mainNav .lbh-default{"+costum.getStyleCustom(cssObj, style+".menuTop.app.button.design")+"}"+
                      "}";
            
            if(jsonHelper.notNull(style+".menuTop.app.button.design.hover") )
                str+= "@media (min-width: 767px){"+
                            "#mainNav .lbh-menu-app:hover,#mainNav .lbh-urlExtern:hover,#mainNav .lbh-menu-app.active,#mainNav .lbh-anchor:hover,#mainNav .lbh-anchor.active,#mainNav .lbh-dropdown:hover"+
                            "{"+costum.getStyleCustom(cssObj, style+".menuTop.app.button.design.hover")+"}"+
                      "}";
            
        }
        if(jsonHelper.notNull(style+".menuTop.xsMenu") ){
            str+="#mainNav .menu-xs-container{"+costum.getStyleCustom(cssObj, style+".menuTop.xsMenu")+"}";
            if(jsonHelper.notNull(style+".menuTop.xsMenu.button") )
                str+="#mainNav .menu-xs-container a{"+costum.getStyleCustom(cssObj, style+".menuTop.xsMenu.button")+"}";

            if(jsonHelper.notNull(style+".menuTop.xsMenu.background") )
                str+="#mainNav .menu-xs-container {"+costum.getStyleCustom(cssObj, style+".menuTop.xsMenu.background")+"}";

            if(jsonHelper.notNull(style+".menuTop.xsMenu.button.design") )
                str+= "#mainNav .menu-xs-container a{"+costum.getStyleCustom(cssObj, style+".menuTop.xsMenu.button.design")+"}";

            
            if(jsonHelper.notNull(style+".menuTop.xsMenu.button.design.hover") ){
                  str+=  "#mainNav .menu-xs-container a:hover{"+costum.getStyleCustom(cssObj, style+".menuTop.xsMenu.button.design.hover")+"}"+
                    "#mainNav .menu-xs-container a:focus{"+costum.getStyleCustom(cssObj, style+".menuTop.xsMenu.button.design.hover")+"}";
            }
                
        }
        
        if(jsonHelper.notNull(style+".menuTop.filterBtn")){
            str+=".btn-show-filters{"+costum.getStyleCustom(cssObj, style+".menuTop.filterBtn")+"}";
            if(jsonHelper.notNull(style+".menuTop.filterBtn.hover"))
                str+=".btn-show-filters:hover, .btn-show-filters.active{"+costum.getStyleCustom(cssObj, style+".menuTop.filterBtn.hover")+"}";
        }
        if(jsonHelper.notNull(style+".menuTop.connectBtn"))
            str+="#mainNav .btn-menu-connect{"+costum.getStyleCustom(cssObj, style+".menuTop.connectBtn")+"}";
        if(jsonHelper.notNull(style+".menuTop.userProfil") )
                str+= "#mainNav a.menu-name-profil{"+costum.getStyleCustom(cssObj, style+".menuTop.userProfil")+"}";
    }
    if(jsonHelper.notNull(style+".menuLeft") ){
        str+="#menuLeft{"+costum.getStyleCustom(cssObj, style+".menuLeft")+"}";
        costum.getClassCustom("#menuLeft",cssObj, style+".menuLeft");
        if(jsonHelper.notNull(style+".menuLeft.button") )
            str+="#menuLeft .menu-button.btn-menu{"+costum.getStyleCustom(cssObj, style+".menuLeft.button")+"}";
        if(jsonHelper.notNull(style+".menuLeft.button.hover") )
            str+="#menuLeft .menu-button.btn-menu:hover, #menuLeft .menu-button.btn-menu:active, #menuLeft .menu-button.btn-menu:focus, #menuLeft .menu-button.btn-menu.active{"+costum.getStyleCustom(cssObj, style+".menuLeft.button.hover")+"}";
        if(jsonHelper.notNull(style+".menuLeft.app.button.design") )
                str+= "@media (min-width: 767px){"+
                        "#menuLeft .menu-button.btn-menu{"+costum.getStyleCustom(cssObj, style+".menuLeft.app.button.design")+"}"+
                      "}";
            
        if(jsonHelper.notNull(style+".menuLeft.app.button.design.hover") )
                str+= "@media (min-width: 767px){"+
                            "#menuLeft .menu-button.btn-menu:hover{"+costum.getStyleCustom(cssObj, style+".menuLeft.app.button.design.hover")+"}"+
                            "#menuLeft .menu-button.btn-menu.active{"+costum.getStyleCustom(cssObj, style+".menuLeft.app.button.design.hover")+"}"+
                      "}";
        if(jsonHelper.notNull(style+".menuLeft.userProfil") )
                str+= "#menuLeft a.menu-name-profil{"+costum.getStyleCustom(cssObj, style+".menuLeft.userProfil")+"}";
    }
    if(jsonHelper.notNull(style+".subMenu") ){
        str+="#subMenu{"+costum.getStyleCustom(cssObj, style+".subMenu")+"}";
        costum.getClassCustom("#subMenu",cssObj, style+".subMenu");
    }
    if(jsonHelper.notNull(style+".button") ){
        if(jsonHelper.notNull(style+".button.footer") ){
            if(jsonHelper.notNull(style+".button.footer.add") )
                str+="#show-bottom-add{"+costum.getStyleCustom(cssObj, style+".button.footer.add")+"}";
            if(jsonHelper.notNull(style+".button.footer.toolbarAdds") )
                str+=".toolbar-bottom-adds{"+costum.getStyleCustom(cssObj, style+".button.footer.toolbarAdds")+"}";
            if(jsonHelper.notNull(style+".button.footer.donate") )
                str+="#donation-btn{"+costum.getStyleCustom(cssObj, style+".button.footer.donate")+"}";
        }
    }*/

    if(jsonHelper.notNull(style+".progress") ){
        if(jsonHelper.notNull(style+".progress.bar") )
            str+=".progressTop::-webkit-progress-bar { "+costum.getStyleCustom(cssObj, style+".progress.bar")+" }";
        if(jsonHelper.notNull(style+".progress.value") )
            str+=".progressTop::-webkit-progress-value{"+costum.getStyleCustom(cssObj, style+".progress.value")+"}.progressTop::-moz-progress-bar{"+costum.getStyleCustom(cssObj, style+".progress.value")+"}";
    }
    //if(jsonHelper.notNull(style+".loader") ){
      //  if(jsonHelper.notNull(style+".loader.background") )
        //    str+="#loadingModal, .blockUI.blockMsg.blockPage{background-color:"+costum.getStyleCustom(cssObj, style+".loader.background")+" !important}";
       // if(jsonHelper.notNull(style+".loader.ring1") )
       //     str+=".lds-dual-ring div{"+costum.getStyleCustom(cssObj, style+".loader.ring1")+"}";
       // if(jsonHelper.notNull(style+".loader.ring2") )
       //     str+=".lds-dual-ring div:nth-child(2){"+costum.getStyleCustom(cssObj, style+".loader.ring2")+"}";
    //}

    if(typeof cssObj.color != "undefined" ){
        colorCSS=["purple", "orange", "blue", "red", "black", "green", "green-k", "yellow", "yellow-k", "vine", "brown","main1","main2"];
        $.each(colorCSS, function(e,v){
            if(typeof cssObj.color[v] != "undefined" ){
                str+=".text-"+v+" { color:"+cssObj.color[v]+" !important;}"+
                     " .bg-"+v+" { background-color:"+cssObj.color[v]+" !important;}";
            }
        });
    }
    str+="@media (max-width: 767px){"+
            "#mainNav{margin-top:0px !important;}";
            str+="#mainNav .menu-btn-top{font-size:22px !important;}";
            if( jsonHelper.notNull(style+".menuTop") &&
                jsonHelper.notNull(style+".menuTop.responsive") &&
                jsonHelper.notNull(style+".menuTop.responsive.connectBtn")){
                str+= "#mainNav .btn-menu-connect{"+costum.getStyleCustom(cssObj, style+".menuTop.responsive.connectBtn")+"}";
            }
            
        str+="#mainNav .logo-menutop{height:40px;}"+
        "}";
    str+=costum.generalCssStyle(cssObj);
   // if($("#amazing-costumizer-css").length <= 0)
     //   str+="</style>";
    //if($("#amazing-costumizer-css").length <= 0)
    $("head").append("<style type='text/css' id='"+idCSS+"'>"+str+"</style>");
    $("#iframe-xs").contents().find("head").append("<style type='text/css' id='"+idCSS+"'>"+str+"</style>");
    if(notEmpty(removeDomCss))
        setTimeout(function(){$(removeDomCss).remove()},200);
    //else
      //  $("#amazing-costumizer-css").html(str);
    $("#menuLeft .menu-name-profil").removeClass("pull-right");
    $("#menuLeft .image-menu ").removeClass("pull-left");
    if(jsonHelper.notNull("costum.htmlConstruct.header.menuTop.left") && costum.htmlConstruct.header.menuTop.left == false)
        $("#mainNav").removeClass("padding-top-5 padding-bottom-5");
    if(jsonHelper.notNull("costum.htmlConstruct.header.menuTop.left.addClass") && costum.htmlConstruct.header.menuTop.left.addClass.indexOf("visible-xs") != -1){
        $("#mainNav").removeClass("padding-top-5 padding-bottom-5");
    }
};
costum.rejectCssProperties=["urls", "font"];
costum.getCssInCascade=function(obj, objClass, path){
    mylog.log("ouuuuuuuuuuuuuu1",obj, objClass, path);
    $.each(obj,function(e,v){
        if($.inArray(e, costum.rejectCssProperties) < 0 ){
            //var classCss=classTreeCss;

             //   classCss=(notEmpty(classCss)) ? classCss+" .cosDyn-"+e : ".cosDyn-"+e;
            if(typeof costum.styleProperties[e]== "undefined" || typeof v == "object"){
                //var keypath=path;
                keypath=(notEmpty(path)) ? path+"."+e : e;
                objClass[keypath]=[];
            }
            if(typeof costum.styleProperties[e]!= "undefined" && typeof v != "object"){
            
                objClass[path].push(costum.styleProperties[e]+":"+v+costum.getSpecPx(e, v)+"!important;");
            }
            else if(typeof v == "object"){
                //if($.inArray(e, ["hover", "active", "focus"]) >=0) classCss=classCss.replace(" ."+e, ":"+e);
                objClass=costum.getCssInCascade(v, objClass, keypath);
            }
            //".costumizer-css-menuTop > .costumizer-css-menuTopLeft > .costumizer-css-app
        }
    });
    return objClass;
}

costum.addClassSection = function (ObjClass,srcPath){
    var clsObj = (notNull(ObjClass)) ? ObjClass : costum.css;
    var classPath = (notNull(srcPath)) ? srcPath : "css";
    mylog.log ("srcPath",srcPath);
    $.each(clsObj,function(e,v) {
        if ($.inArray(e, ["hideOnDesktop", "hideOnTablet", "hideOnMobil", "columnWidth"]) >= 0) {
            var newPath = classPath.replace('css.', '');
            var tabClass=newPath.split(".");
            var arrayTab = tabClass.map(function (params) {
                return ".cosDyn-"+params
            });
            var classList = arrayTab.join(" ");
            if (e == "columnWidth") {
                costum.getClassCustom(classList, clsObj);
            } else {
                if (v == true)
                    $(classList).addClass(cssHelpers.json[e].classValue);
                else
                    $(classList).removeClass(cssHelpers.json[e].classValue);
            }
        } else if (typeof v == "object") {
            //alert(classPath+"."+e);
            callObjClass = costum.addClassSection(v, classPath+"."+e);
        }
    });
}
costum.generalCssStyle=function(obj){
    var str="";
    var objClass=costum.getCssInCascade(obj, {});
    mylog.log("ou2",objClass)
    
    $.each(objClass, function(classes,style){
        tabClass=classes.split(".");
        classStr="";
        $.each(tabClass,function(i,classTarget){
            if($.inArray(classTarget, ["hover", "active", "focus"]) >=0) 
                classStr+=":"+classTarget;
            else
                classStr+=" .cosDyn-"+classTarget;
        });

        if(notEmpty(style))
            str+=classStr+"{"+style.join('')+"}";
    });
    //alert(str);
    //str+=styleForCurrentClass+"";
    mylog.log("ou3",str);
    return str;
};
costum.value = function(obj,path){
    //mylog.log("costum.value ",path);
    if( jsonHelper.notNull(path) ){
        //mylog.log("costum.value ",path);
        if( path.indexOf("this.") > -1 ){
            pieces = path.split('.');
            return jsonHelper.eval(obj,pieces[1]);
        } else {
            path=path.split("css.")[1];
           // path=path.replace("costum.","");
            return jsonHelper.getValueByPath(obj,path);
        }
    }
    else 
        return null;
};
//costum.init();

/*
template(col) : {
    id : 908478427428722,
    name : artiste,
    type : costum,
    params : {
        etct
    },
    costumUsingTpls : {
        "987876868767"
        "987876868767"
        "987876868767"
        "987876868767"
        "987876868767",
        etc autant d'id que de personne qui utilisent un template
    }
} 

ou stocker dans le duplicate de tpl 
== costum : {
    id: 66476476457625462,
    name : 'je suis utilisateur du costum template',
    params: {
        blablablabla
    },
    htmlConstruct: {
        etc 
    }
    tplsOrigin : {id tpl artiste}
}
*/