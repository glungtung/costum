(function($){
    var TOGGLABLE_PROPERTIES = ["font-weight", "font-style", "text-decoration"],
        TAG_STYLES = {
            b:{ property:"font-weight", value:"bold" },
            u:{ property:"text-decoration", value:"underline" },
            i:{ property:"font-style", value:"italic" }
        }

    var context = null;

    $.fn.coEditor = function(property, value){
        {context = this;
        
                var selection = getSelection();
                // console.log("selection",selection)
        
                if(!(selection && selection.rangeCount > 0)){
                    bootbox.alert("Selectionner un text.")
                    return;
                }
        
                var range = selection.getRangeAt(0);
        
                if(!this[0].contains(range.commonAncestorContainer)){
                    // if($("#text-display")[0].contains(range.commonAncestorContainer)){
                    //     context = $("#text-display");
                    // }else{
                        bootbox.alert("La selection doit être dans l'élément.")
                        return;
                    // }
                }
        
                if(range.startOffset == range.endOffset){
                    selectElement($(context)[0])
                    selection = getSelection()
                }
               
        $container = $(getSelectionContainer(selection.getRangeAt(0).commonAncestorContainer));
        
                if($container[0].innerText === selection.toString()){
                    if (!$container.hasClass("sp-text")) {
                        updateSelection($container, { property:property, value:value })
                    }else{
                        replaceSelection($container, { property:property, value:value }, selection)
                    }
                }else{
                        replaceSelection($container, { property:property, value:value }, selection)
                }
        
        }
                // console.log("$container",container)
                // console.log("container",container)
                // console.log("style",style)
    }

    /**
     * 
     * @param {object} anchorNode 
     */
    function getSelectionContainer(anchorNode){
        return anchorNode.nodeType !== Node.TEXT_NODE && anchorNode.nodeType !== Node.COMMENT_NODE ? anchorNode:anchorNode.parentElement;
    } 

    /**
     * 
     * @param {object} $element 
     * @param {object} style
     * @param {string} style.property
     * @param {string} style.value 
     */
    function updateSelection($element, style){
        var element_tagName = $element.prop("tagName").toLowerCase(),
            element_style_value = $element[0].style[style.property];

        if(element_style_value == style.value && TOGGLABLE_PROPERTIES.includes(style.property)){
            $element.css(style.property, "")
        }else if(
            TAG_STYLES[element_tagName] &&
            TAG_STYLES[element_tagName].property == style.property &&
            TAG_STYLES[element_tagName].value == style.value
        ){
            element_tagName = "span"
            $element = replaceElementTagName($element, element_tagName)
        }else if(style.property == "text-align"){
            if(element_style_value == style.value){
                $element.css("text-align", "")
                
                /* if($(context).has($element).length > 0){
                    element_tagName = "span"
                    $element = replaceElementTagName($element, element_tagName)
                } */
            }else{
                if(element_tagName !== "div"){
                    element_tagName = "div"
                    $element = replaceElementTagName($element, element_tagName)
                }
                $element.css("text-align", style.value)
            }
        }else{
            $element.css(style.property, style.value)
        }

        if(!$element.attr("style") && ["span"].includes(element_tagName) && $(context).has($element).length > 0)
            $element.replaceWith($element.html())

        if($element.attr("style").trim() == "")
            $element.removeAttr("style")

        cleanChildren($element, style)

        selectElement($element[0])
    }

     /**
     * 
     * @param {object} $element 
     * @param {object} style
     * @param {string} style.property
     * @param {string} style.value 
     * @param {object} selection
     */
    function replaceSelection($element, style, selection){
        if(!getParentHasStyle($element, style)){
            var range = selection.getRangeAt(0),
                container = range.commonAncestorContainer,
                fragment = range.extractContents()

 
                selectedContainer = getSelectionParentElement()
                $selectedContainer = $(selectedContainer)
                // $selectedText_tag = $($container).prop("tagName")
                // $(this).children().first().prop('tagName');

            var $newElement = $(`<${ style.property == "text-align" ? "div":"span" }/>`);
            if ($selectedContainer.children().first().prop('tagName') == "DIV") {
                $newElement = $(`<${ style.property == "text-align" ? "div":"div" }/>`);
            }
            $newElement.css(style.property, style.value)
            $newElement.append(fragment)

            range.insertNode($newElement[0])
            $(container).children().each(function(){
                if($(this).is(":empty"))
                    $(this).remove()
            })

            cleanChildren($newElement, style)

            selectElement($newElement[0])
        }
    }

    /**
     * 
     * @param {object} $element 
     * @param {string} newTagName 
     * @returns 
     */
    function replaceElementTagName($element, newTagName){
        var $newElement = $(`<${newTagName}/>`)
        $.each($element[0].attributes, function(){
            if(this.specified){
                if($element[0].nodeName.toLowerCase() == "font"){
                    switch(this.name){
                        case "color":
                            $newElement.css("color", this.value)
                        break;
                        case "face":
                            $newElement.css("font-family", this.value)
                        break;
                        case "size":
                            var size = parseInt(this.value),
                                sizeLinks = ["0.63em","0.82em","1em","1.13em","1.5em","2em","3em"];

                            if(sizeLinks[size-1]){
                                $newElement.css("font-size", sizeLinks[size-1])
                            }
                        break;
                    }
                }else{
                    $newElement.attr(this.name, this.value)
                }
            }
        })
        $newElement.html($element.html())
        $element.replaceWith($newElement)
        return $newElement;
    }

    /**
     * 
     * @param {object} $element 
     * @param {object} style
     * @param {string} style.property
     * @param {string} style.value 
     */
    function getParentHasStyle($element, style){
        if(!$(context).has($element).length)
            return false;

        if($element[0].style && $element[0].style[style.property] == style.value)
            return $element;

        if(!$element.parent().length)
            return false;


        return getParentHasStyle($element.parent(), style)
    }

    /**
     * 
     * @param {object} $element 
     * @param {object} style
     * @param {string} style.property
     * @param {string} style.value
     */
    function cleanChildren($element, style){
        if(!$element.children().length)
            return;

        $element.children().each(function(){
            var $child = $(this),
                child_style_value = $child[0].style && $child[0].style[style.property],
                child_tag_name = $child.prop("tagName").toLowerCase()
            
            if (child_tag_name !== "br" && $child.is(':empty')) {
                $child.remove()
            }                

            if ($child.css(style.property)) {
                $child.css(style.property, "")
            }                

            if(child_style_value == style.value)
                $child.css(style.property, "")
            else if(
                TAG_STYLES[child_tag_name] &&
                TAG_STYLES[child_tag_name].property == style.property &&
                TAG_STYLES[child_tag_name].value == style.value
            ){
                $child = replaceElementTagName($child, "span")
            }

            if(!$child.attr("style") && ["span", "div"].includes($child.prop("tagName").toLowerCase()))
                $child = $child.replaceWith($child.html())

            cleanChildren($child, style)
        })
    }

    function selectElement(element){
        var doc = element.ownerDocument,
            win = doc.defaultView;

        var range = doc.createRange();
        range.setStart(element, 0)
        range.collapse(true)
        range.setEnd(element, (element.nodeType == Node.TEXT_NODE)?element.length:element.childNodes.length);

        var sel = win.getSelection()
        sel.removeAllRanges()
        sel.addRange(range)
    }

    function getSelectionParentElement() {
        var parentEl = null, sel;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                parentEl = sel.getRangeAt(0).commonAncestorContainer;
                if (parentEl.nodeType != 1) {
                    parentEl = parentEl.parentNode;
                }
            }
        } else if ( (sel = document.selection) && sel.type != "Control") {
            parentEl = sel.createRange().parentElement();
        }
        return parentEl;
    }

}(jQuery))