var swiperObj = {
    initSwiper: function (containerClass, paramsData) {
        var options = {
            autoHeight: (paramsData["autoHeight"]) ? paramsData["autoHeight"] : false,
            autoplay: (paramsData["autoplay"]) ? paramsData["autoplay"] : false,
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 10,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 15,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
            },
            direction: (paramsData["slideDirection"]) ? `${paramsData["slideDirection"]}` : "horizontal",
            effect: (paramsData["effect"]) ? `${paramsData["effect"]}` : "slide",
            grabCursor: (paramsData["grabCursor"]) ? paramsData["grabCursor"] : false,
            initialSlide: (paramsData["initialSlide"]) ? Number(paramsData["initialSlide"]) : 0,
            loop: (paramsData["loop"]) ? paramsData["loop"] : false,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
            },
            noSwiping: (paramsData["noSwiping"]) ? paramsData["noSwiping"] : false,
            speed: (paramsData["speed"]) ? Number(paramsData["speed"]) : 400
        }
        if (typeof paramsData["breakpoints"] != "undefined") {
            if (typeof paramsData["breakpoints"]["slidesPerView"] != "undefined") {
                let slidesPerView = paramsData["breakpoints"]["slidesPerView"];
                if (typeof slidesPerView == "object") {
                    $.each(slidesPerView, function(key, value) {
                        if (value != "") {
                            switch (key) {
                                case "xs":
                                    options["breakpoints"]["640"]["slidesPerView"] = Number(value);
                                    break;
                                case "sm":
                                    options["breakpoints"]["768"]["slidesPerView"] = Number(value);
                                    break;
                                case "md":
                                    options["breakpoints"]["1024"]["slidesPerView"] = Number(value);
                                    break;
                                default:
                                    break;
                            }
                        }
                    })
                }
            }
            if (typeof paramsData["breakpoints"]["spaceBetween"] != "undefined") {
                let spaceBetween = paramsData["breakpoints"]["spaceBetween"];
                if (typeof spaceBetween == "object") {
                    $.each(spaceBetween, function(key, value) {
                        if (value != "") {
                            switch (key) {
                                case "xs":
                                    options["breakpoints"]["640"]["spaceBetween"] = value;
                                    break;
                                case "sm":
                                    options["breakpoints"]["768"]["spaceBetween"] = value;
                                    break;
                                case "md":
                                    options["breakpoints"]["1024"]["spaceBetween"] = value;
                                    break;
                                default:
                                    break;
                            }
                        }
                    })
                }
            }            
        }
        if (paramsData["noSwiping"] === true) {
            options["noSwipingClass"] = "swiper-container";
        }
        if (typeof paramsData["pagination"] !== "undefined") {
            options.pagination = {
                el: ".swiper-pagination",
                clickable: (paramsData["pagination"]["clickable"]) ? paramsData["pagination"]["clickable"] : false,
                type: (paramsData["pagination"]["type"]) ? `${paramsData["pagination"]["type"]}` : "bullets"
            }
        }
        if (typeof paramsData["scrollbar"] !== "undefined") {
            options.scrollbar = {
                el: ".swiper-scrollbar",
                hide: (paramsData["scrollbar"]["hide"]) ? paramsData["scrollbar"]["hide"] : false,
            }
        }
        if (typeof paramsData["autoplay"] !== "undefined" && paramsData["autoplay"] !== false) {
            options.autoplay = {
                delay: 2000,
                disableOnInteraction: true
            }
        }
        var swiper = new Swiper(`${containerClass} .swiper-container`, options);
    },
    htmlSwiper: function(containerContent) {
        var html =
            `<div class="swiper-container">
                  <div class="swiper-wrapper">
            
                  </div>
                  <div class="swiper-pagination"></div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>
            `;
        $(`${containerContent}`).append(html);
    },
    inputsOnEdit: function(paramsData = []) {
        var inputSwiper = [
            {
                type: "inputGroup",
                options: {
                    label: tradCms.spaceBtSlide,
                    name: "spaceBetween",
                    linkValue:true,
                    inputs:[
                        {
                            type: "number",
                            name: "md",
                            icon : "desktop",
                            label: "desktop"
                        },
                        {
                            type: "number",
                            name: "sm",
                            icon : "tablet",
                            label: "tablet"
                        },
                        {
                            type: "number",
                            name: "xs",
                            icon : "mobile",
                            label: "mobile"
                        }
                    ],
                    defaultValue: (notEmpty(paramsData) && typeof paramsData["breakpoints"] != "undefined" && typeof paramsData["breakpoints"]["spaceBetween"] != "undefined") ? paramsData["breakpoints"]["spaceBetween"] : ""
                },
                payload: {
                    path: "swiper.breakpoints.spaceBetween"
                }
            },
            {
                type: "select",
                options: {
                    name: "slideDirection",
                    label: tradCms.direction,
                    options: ["horizontal", "vertical"],
                }
            },
            {
                type: "number",
                options: {
                    name: "speed",
                    label: tradCms.animationSpeed,
                    filterValue: cssHelpers.form.rules.checkLengthProperties
                }
            },
            {
                type : "inputSimple",
                options : {
                    type : "checkbox",
                    name : "loop",
                    label : tradCms.infiniteAnimation,
                    class : "switch"
                }
            },
            {
                type : "inputSimple",
                options : {
                    type : "checkbox",
                    name : "autoplay",
                    label : tradCms.autoplay,
                    class : "switch"
                }
            },
            {
                type : "inputSimple",
                options : {
                    type : "checkbox",
                    name : "autoHeight",
                    label : tradCms.autoHeight,
                    class : "switch"
                }
            },
            {
                type : "inputSimple",
                options : {
                    type : "checkbox",
                    name : "noSwiping",
                    label : tradCms.noSwiping,
                    class : "switch"
                }
            },
            {
                type: "select",
                    options: {
                    name: "effect",
                    label: tradCms.effect,
                    options: ["slide", "fade", "cube", "coverflow", "flip", "creative"],
                }
            },
            {
                type: "section",
                options: {
                    name: "pagination",
                    label: "Pagination",
                    showInDefault: true,
                    inputs: [ 
                        {
                        type : "inputSimple",
                        options : {
                            type : "checkbox",
                            name : "clickable",
                            label : tradCms.clickable,
                            class : "switch"
                        }
                        },
                        {
                        type: "select",
                        options: {
                            name: "type",
                            label: tradCms.type,
                            options: ["bullets", "fraction", "progressbar"],
                        }
                        }
                    ]
                }
            },
            {
                type: "section",
                options: {
                    name: "scrollbar",
                    label: "Scrollbar",
                    showInDefault: true,
                    inputs: [
                        {
                            type : "inputSimple",
                            options : {
                                type : "checkbox",
                                name : "hide",
                                label : tradCms.hidden,
                                class : "switch"
                            }
                        },
                    ]
                }
            },
            {
                type : "inputSimple",
                options : {
                    type : "checkbox",
                    name : "grabCursor",
                    label : tradCms.mouseInteraction,
                    class : "switch"
                }
            }
        ]  
        cmsConstructor.commonEditor.swiper = inputSwiper;
    },
    incrementeNumberOfOptions: function(size) {
        var tab = [];
        for (let index = 0; index <= size; index++) {
          tab.push(`${index}`);
        }
        return tab
    } 
}