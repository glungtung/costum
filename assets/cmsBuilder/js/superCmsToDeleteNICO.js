
var superCms =  {
  sp_params : {},
  pathConfig : {},
  parents: "",
  element: "",
  kunik :"",
  spId : "",
  eltBlur   : "",
  eltSpread : "",
  eltAxeX   : "",
  eltAxeY   : "",
  eltColor  : "",
  inset     : "",
  hoverEltBlur   : "",
  hoverEltSpread : "",
  hoverEltAxeX   : "",
  hoverEltAxeY   : "",
  hoverEltColor  : "",
  hoverInset     : "",
  cssSelected : "",
  recentlyEdited : "",
  elSelected : "",
  focusedDom : "",

  menu : { 

      viewContainer : function(thisParams,thisParent) {
       htmlView = `
     <div class="text-center tool-scms-title cursor-move">
       <i class="fa fa-arrow-up text-blue gotoparentBtn" aria-hidden="true"  data-id-parent="${thisParent}" ></i>
       <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres du Container</i>
       <button class="bg-transparent deleteLine delete-super-cms tooltips" data-id="${superCms.spId}" data-parent="${thisParent}" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément">
       <i class="fa fa-trash text-red" aria-hidden="true"></i>
       </button>
       <i class="fa fa-window-close closeBtn view-super-cms" data-target="toolsBar-edit-block" aria-hidden="true"></i>
     </div>

     <div class="text-center tool-scms-title cursor-move">
      <i class="fa fa-pencil-square-o contentEditable" aria-hidden="true" title="nom du bloc editable" ></i>
      <input type=text class="blockName" id="block-name" data-id="${superCms.spId}" value="${thisParams.name}" ></input>
     </div>

     <ul class="nav nav-tabs">
       <li class="active"><a data-toggle="tab" href="#home" class="black-default-cms">Général</a></li>
       <li><a data-toggle="tab" href="#addElt" class="panel-insertion">Insertion</a></li>
       <li><a data-toggle="tab" href="#style-setting">Style</a></li>
       <li><a data-toggle="tab" href="#advanced-settings">Advanced</a></li>
     </ul>


        <div class="tab-content tab-content-sp-cms">
        <div id="home" class="tab-pane fade in active">
        <div class="col-md-12 sp-cms-options">
          
           <div class="col-md-12">
              <li class="dropdown sp-cms-std ">Taille du bloc par rapport à l'écran<input style=" background-color: #404040;width:84px" class="dropdown-toggle sizes" value="${thisParams.css.size.width}" data-toggle="dropdown" style="width: 70px;height: 25px;border: solid 1px #e0e0e0; margin-left: 5px;">
              <ul class="dropdown-menu" style="background-color:#2b2b2b;">
                <li class="content-size text-center" data-size="10"><a style="color: #ababab;">10</a></li>
                <li class="content-size text-center" data-size="20"><a style="color: #ababab;">20</a></li>
                <li class="content-size text-center" data-size="30"><a style="color: #ababab;">30</a></li>
                <li class="content-size text-center" data-size="40"><a style="color: #ababab;">40</a></li>
                <li class="content-size text-center" data-size="50"><a style="color: #ababab;">50</a></li>
                <li class="content-size text-center" data-size="60"><a style="color: #ababab;">60</a></li>
                <li class="content-size text-center" data-size="70"><a style="color: #ababab;">70</a></li>
                <li class="content-size text-center" data-size="80"><a style="color: #ababab;">80</a></li>
                <li class="content-size text-center" data-size="90"><a style="color: #ababab;">90</a></li>
                <li class="content-size text-center" data-size="100"><a style="color: #ababab;">100</a></li>
              </ul>
              </li>
           </div>

          <div class="col-md-12">  
          
            <div class="col-md-12" style="display: grid;margin-top:5px">  
              <div class="bg-img-preview" style="margin: auto;">
                <img class="imageBg-container" style="width: 200px;" ></image>
              </div>
            <label style='padding-top: 10px;' >Couleur du fond</label>
            </div>
            <div class="input-group">
              <input id="fond-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.background.color ? thisParams.css.background.color : "transparent"}" placeholder="#ffffff">
              <div class="input-group-btn closeMenu" style="width: 0px">                    
                <button class="edit-img${superCms.kunik}Params tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Modifier la photo">
                  <i class="fa fa-camera" aria-hidden="true"></i><br>
                </button> 
              </div>
            </div>
            
            <div class="bg-control">
              <div class="col-md-12 no-padding">
                <label class="title" data-toggle="collapse" data-target="#ajustementFond" style="padding-top: 10px;" >Ajustement du fond (Photo)</label>
              </div>
              <div class="col-md-12 collapse" id="ajustementFond">
              <div class="col-md-12">
                <label>Ajustement</label>
                <select class="form-control" id="objfit" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`
                  $.each(bgSize, function(kBgSize, vBgSize) {                          
                    if(thisParams.css.background.size == vBgSize){
                      htmlView += `<option value="${kBgSize}" selected="selected">${vBgSize}</option>`;
                    }else{
                      htmlView += `<option value="${kBgSize}">${vBgSize}</option>`;
                    }
                  });
              htmlView += `</select>                    
              </div>
            <div class="col-md-12"> 
              <label>Taille</label>
              <select class="form-control" id="bgSize" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(bgSize, function(kBgSize, vBgSize) {                          
                  if(thisParams.css.background.size == vBgSize){
                    htmlView += `<option value="${kBgSize}" selected="selected">${vBgSize}</option>`;
                  }else{
                    htmlView += `<option value="${kBgSize}">${vBgSize}</option>`;
                  }
                });
              htmlView += `</select>
            </div>  
            <div class="col-md-12"> 
              <label>Position</label>
              <select class="form-control" id="bgPosition" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(bgPosition, function(kPosition, vPosition) {                          
                  if(thisParams.css.background.position == kPosition){
                    htmlView += `<option value="${kPosition}" selected="selected">${vPosition}</option>`;
                  }else{
                    htmlView += `<option value="${kPosition}">${vPosition}</option>`;
                  }
                });
              htmlView += `</select>
            </div>   
            <div class="col-md-12"> 
              <label>Répéter</label>
              <select class="form-control" id="bgRepeat" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(bgRepeat, function(kRepeat, vRepeat) {                          
                  if(thisParams.css.background.repeat == kRepeat){
                    htmlView += `<option value="${kRepeat}" selected="selected">${vRepeat}</option>`;
                  }else{
                    htmlView += `<option value="${kRepeat}">${vRepeat}</option>`;
                  }
                });
              htmlView += `</select>
            </div>
            </div>
            </div>
            <div class="flex-control">
            <div class="col-md-12 no-padding">
              <label class="title" data-toggle="collapse" data-target="#flex"  style="padding-top: 10px;">Flex</label>
              <div class="col-md-12 collapse" id="flex">
              <div class="col-md-12">
                <label>Direction</label>
                <select class="form-control" id="flexDirection" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(flexDirection, function(kFDirection, vFDirection) {                          
                  if(thisParams.css["flex-direction"] == vFDirection){
                    htmlView += `<option value="${kFDirection}" selected="selected">${vFDirection}</option>`;
                  }else{
                    htmlView += `<option value="${kFDirection}">${vFDirection}</option>`;
                  }
                });
                htmlView += `</select>                    
              </div>
              <div class="col-md-12">
                <label>Wrap</label>
                <select class="form-control" id="flexWrap" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(flexWrap, function(kFwrap, vFwrap) {                          
                  if(thisParams.css["flex-wrap"] == vFwrap){
                    htmlView += `<option value="${kFwrap}" selected="selected">${vFwrap}</option>`;
                  }else{
                    htmlView += `<option value="${kFwrap}">${vFwrap}</option>`;
                  }
                });
                htmlView += `</select>                    
              </div>
            </div>
            </div>
            <div class="flex-control">
            <div class="col-md-12 no-padding">
              <label class="title" data-toggle="collapse" data-target="#content" style="padding-top: 10px;">Content</label>
              <div class="col-md-12 collapse" id="content">
              <div class="col-md-12">
                <label>Align</label>
                <select class="form-control" id="contentAlign" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(contentAlign, function(kCAlign, vCAlign) {                          
                  if(thisParams.css["content-align"] == vCAlign){
                    htmlView += `<option value="${kCAlign}" selected="selected">${vCAlign}</option>`;
                  }else{
                    htmlView += `<option value="${kCAlign}">${vCAlign}</option>`;
                  }
                });
                htmlView += `</select>                    
              </div>
              <div class="col-md-12">
                <label>Justify</label>
                <select class="form-control" id="contentJustify" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">`;
                $.each(contentJustify, function(kJustify, vJustify) {                          
                  if(thisParams.css["content-justify"] == vJustify){
                    htmlView += `<option value="${kJustify}" selected="selected">${vJustify}</option>`;
                  }else{
                    htmlView += `<option value="${kJustify}">${vJustify}</option>`;
                  }
                });
                htmlView += `</select>                    
              </div>
            </div>
            </div>
            </div>
          </div>
          
          <!--button class="btn btn-danger btn-save" style="margin-left: 15px;margin-top: 15px" ><i class="fa fa-save"></i> Enregistrer</button-->   
        </div>  
        </div>
               <div>`
               htmlView += `</div>
       <div class="sp-message text-center"></div>
     </div> 
            `;
          return htmlView;
      },

      viewLineSeparator : function(thisParams,thisParent) {
        htmlView = `
             <div class="text-center tool-scms-title cursor-move">
               <i class="fa fa-arrow-up text-blue gotoparentBtn" aria-hidden="true"  data-id-parent="${thisParent}" ></i>
               <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres du Line Separator</i>
               <button class="bg-transparent deleteLine delete-super-cms tooltips" data-id="${superCms.spId}" data-parent="${thisParent}" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément">
                 <i class="fa fa-trash text-red" aria-hidden="true"></i>
               </button>
               <i class="fa fa-window-close closeBtn view-super-cms" data-target="toolsBar-edit-block" aria-hidden="true"></i>
             </div>

             <div class="text-center tool-scms-title cursor-move">
              <i class="fa fa-pencil-square-o contentEditable" aria-hidden="true" title="nom du bloc editable" ></i>
              <input type=text class="blockName" id="block-name" data-id="${superCms.spId}" value="${thisParams.name}" ></input>
             </div>
 
             <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#home" class="black-default-cms">Général</a></li>
               <li><a data-toggle="tab" href="#style-setting">Style</a></li>
               <li><a data-toggle="tab" href="#icon-settings" class="black-default-cms">Icon</a></li>
               <li><a data-toggle="tab" href="#advanced-settings">Advanced</a></li>
             </ul>
 
    
                <div class="tab-content tab-content-sp-cms">
                  <div id="home" class="tab-pane fade in active">
                    <div class="col-md-12 sp-cms-options">
                      <div class="col-md-12">  
                        <label>Couleur du fond</label>
                        <div class="input-group">
                          <input id="fond-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.background.color ? thisParams.css.background.color : "transparent"}" placeholder="#ffffff">
                        </div>
                      </div> 
                      
                      <div class="col-md-12"> 
                        <label>Taille </label>
                        <div class="size-slider text-center">
                            <span class="range-width__value">Longueur  ${thisParams.css.size.width} %</span>
                            <input  class="block-width" type="range" value="${thisParams.css.size.width.replace('%', '')}" min="0" max="100" step="0.2">
                        </div>
                        <div class="size-slider text-center">
                            <span class="range-heigth__value">Largeur  ${thisParams.css.size.height}</span>
                            <input  class="block-height" type="range" value="${(thisParams.css.size.height == "auto" ? 0 : thisParams.css.size.height.replace('px', ''))}" min="0" max="100" step="0.2">
                        </div>
                      </div>

                      <!--button class="btn btn-danger btn-save" style="margin-left: 15px;margin-top: 15px" ><i class="fa fa-save"></i> Enregistrer</button-->
                    </div> 
                  </div> 
                </div>
                       <div>`
                       htmlView += `</div>
               <div class="sp-message text-center"></div>
             </div> 
             `;
           return htmlView;
       },

       viewMenuIcon : function(thisParams) {
        htmlView = `

            <div id="icon-settings" class="tab-pane fade">
              <div class="col-md-12 sp-cms-options">  
                <div>
                  <label class="title" style="padding-top: 20px">Configuration Icone</label>
                  <div class="input-group-btn closeMenu">
                    <button class="lineSeparator-change-icon" tooltips btn btn-primary" type="button" data-toggle="tooltip"
                      data-placement="left" title="" data-original-title="Plus de réglage">
                      <i class="fa fa-${thisParams.css.icon.style}" id="change-icone" aria-hidden="true"></i><br>
                    </button>
                    Changer l'icone
                  </div>

                  <label>Couleur de l'icone</label>
                  <input id="icon-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"
                    class="sp-color-picker spectrum sp-colorize form-control" name=""
                    value="${thisParams.css.icon.color ? thisParams.css.icon.color : " transparent"}" placeholder="#ffffff">
                  <label>Couleur de fond de l'icone</label>
                    <input id="icon-background-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"
                      class="sp-color-picker spectrum sp-colorize form-control" name=""
                      value="${thisParams.css.icon.background.color ? thisParams.css.icon.background.color : " transparent"}"
                      placeholder="#ffffff">
                  <label padding-top-20>Taille</label>
                  <div class="text-center">
                    <span class="range-blur__value">Taille ${thisParams.css.icon.size} px</span>
                    <input class="line-separator-icon-size" type="range" value="${thisParams.css.icon.size}" min="0" max="100" step="0.2">
                  </div>
              
                  <label padding-top-20>Margin</label>
                  <div class="text-center">
                  <span class="range-top__value">Vertical ${thisParams.css.icon.margin.top} %</span>
                  <input id="iconMargin" class="block-margin-top" type="range" value="${thisParams.css.icon.margin.top}" min="-50" max="50" step="0.2">
                  <span class="range-left__value">Horizontal ${thisParams.css.icon.margin.left} %</span>
                  <input id="iconMargin" class="block-margin-left" type="range" value="${thisParams.css.icon.margin.left}" min="-50" max="50" step="0.2">
                </div>      
              </div>
            </div>


        `;
       return htmlView;
      },

      viewMenuBorders : function(thisParams) {
        htmlView = `

        <div id="border-settings" class="tab-pane fade">
        <div class="col-md-12 sp-cms-options">
          <div class="col-md-12">      
            <div class="panel-group">
              <label>Rayon de la bordure</label>                      
              <div class="input-group">
                <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.border.radius["top-left"], thisParams.css.border.radius["top-right"], thisParams.css.border.radius["bottom-right"],thisParams.css.border.radius["bottom-left"])}" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"  data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>"> 
                <div class="input-group-btn">                    
                  <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                    <i class="fa fa-plus" aria-hidden="true"></i><br>
                  </button> 
                </div>
              </div>
              <div id="border" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field"> 
                    <label>Haut à gauche</label>                
                    <input class="form-control top-left border-control" type="number" min="0" value="${thisParams.css.border.radius["top-left"]}" placeholder="0" data-target="border-control" data-path="border.radius" data-given="border-top-left-radius" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                  </div>                  
                  <div class="element-field">      
                  <label>Haut à droite</label>                   
                  <input class="form-control top-right border-control" type="number" min="0" value="${thisParams.css.border.radius["top-right"]}" placeholder="0" data-target="border-control" data-path="border.radius" data-given="border-top-right-radius" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                </div>
                <div class="element-field"> 
                  <label>Bas à gauche</label>                           
                  <input class="form-control bottom-left border-control" type="number" min="0" value="${thisParams.css.border.radius["bottom-left"]}" data-target="border-control" data-path="border.radius" data-given="border-bottom-left-radius" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                </div>                  
                <div class="element-field">      
                  <label>Bas à droite</label>                   
                  <input class="form-control bottom-right border-control" type="number" min="0" value="${thisParams.css.border.radius["bottom-right"]}" data-target="border-control" data-path="border.radius" data-given="border-bottom-right-radius" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                </div>
              </div>
            </div>
            <label>Couleur de la bordure</label>
            <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.border.color}" placeholder="#ffffff" style="" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
            <label>Type  de la bordure</label>
            <select class="form-control" id="borderType" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
              <option value="dashed">dashed</option>
              <option value="dotted">dotted</option>
              <option value="double">double</option>
              <option value="groove">groove</option>
              <option value="inset">inset</option>
              <option value="ridge">ridge</option>
              <option value="solid">solid</option>
              <option value="outset">outset</option>  
            </select>
          </div> 

        </div>
        <div class="col-md-12">      
          <div class="panel-group">
          <label>Epaisseur de la bordure</label>
          <input class="form-control elem-control" id="border-width" type="number" min="0" value="${thisParams.css.border.width}" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">  
          <label>Ajouter une bordure</label> <br> 
          <div class="col-md-12">
            <input id="check-border-top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" type="checkbox" ${thisParams.css.border.top}>
            <label for="check2">En haut</label><br>
            <input id="check-border-bottom" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" type="checkbox" ${thisParams.css.border.bottom}>
            <label for="check2">En bas</label>
          </div>
          <div class="col-md-12">
            <input id="check-border-left" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" type="checkbox" ${thisParams.css.border.left}>
            <label for="check2">A gauche</label><br>
            <input id="check-border-right" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" type="checkbox" ${thisParams.css.border.right}>
            <label for="check2">En droite</label>
          </div>
          </div>
        </div>
             
      </div>


        `;
       return htmlView;
      },

      viewButton : function(thisParams,thisParent) {
        htmlView = `
             <div class="text-center tool-scms-title cursor-move">
               <i class="fa fa-arrow-up text-blue gotoparentBtn" aria-hidden="true"  data-id-parent="${thisParent}" ></i>
               <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres du Button</i>
               <button class="bg-transparent deleteLine delete-super-cms tooltips" data-id="${superCms.spId}" data-parent="${thisParent}" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément">
                 <i class="fa fa-trash text-red" aria-hidden="true"></i>
               </button>
               <i class="fa fa-window-close closeBtn view-super-cms" data-target="toolsBar-edit-block" aria-hidden="true"></i>
             </div>

             <div class="text-center tool-scms-title cursor-move">
              <i class="fa fa-pencil-square-o contentEditable" aria-hidden="true" title="nom du bloc editable" ></i>
              <input type=text class="blockName" id="block-name" data-id="${superCms.spId}" value="${thisParams.name}" ></input>
             </div>
 
             <ul class="nav nav-tabs">
               <li class="active"><a data-toggle="tab" href="#home" class="black-default-cms">Général</a></li>
               <li><a data-toggle="tab" href="#style-setting">Style</a></li>
               <li><a data-toggle="tab" href="#advanced-settings">Advanced</a></li>
             </ul>
 
    
                <div class="tab-content tab-content-sp-cms">
                  <div id="home" class="tab-pane fade in active">
                    <div class="col-md-12 sp-cms-options">
                        <div class="col-md-12">  
                          <label>Couleur du fond</label>
                          <div class="input-group">
                            <input id="fond-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.background.color ? thisParams.css.background.color : "transparent"}" placeholder="#ffffff">
                          </div>
                        </div> 
                        <div class="col-md-12">  
                          <label>Couleur du texte</label>
                          <div class="input-group">
                            <input id="font-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.color ? thisParams.css.color : "transparent"}" placeholder="#ffffff">
                          </div>

                          <div class="size-slider text-center">
                            <span class="range-font-size__value">Taille du texte ${thisParams.css["font-size"]}</span>
                            <input  class="block-font-size" type="range" value="${thisParams.css["font-size"].replace('px', '')}" min="0" max="100" step="1">
                          </div>
                        </div> 
                      
                      <div class="col-md-12"> 
                        <label>Taille </label>
                        <div class="size-slider text-center">
                            <span class="range-width__value">Longueur  ${thisParams.css.size.width}</span>
                            <input  class="block-width" type="range" value="${thisParams.css.size.width.replace('%', '')}" min="0" max="100" step="0.2">
                        </div>
                        <div class="size-slider text-center">
                            <span class="range-heigth__value">Hauteur  ${thisParams.css.size.height}</span>
                            <input  class="block-height" type="range" value="${(thisParams.css.size.height == "auto" ? 0 : thisParams.css.size.height.replace('px', ''))}" min="0" max="100" step="1">
                        </div>
                      </div>
                    </div> 
                    <div class="col-md-12">   
                    <label>Texte</label> 
                    <input class="form-control super-btn-text" name="" value="${thisParams.params.text}" placeholder="Button">
                    <label>Lien</label>   
                    
                      <div class="panel-group">
                        <div class="row">          
                          <div class="col-md-6">
                          <input class="form-control super-btn-link" name="" value="${thisParams.params.url}" placeholder="#">
                          </div>        
                          <div class="col-md-6">  
                            <div class="form-check mb-2 mr-sm-2">
                              <input class="form-check-input check-target-lbh" type="checkbox" checked="true">
                              <label class="form-check-label" for="inlineFormCheck">
                              Chargé sur la page actuelle
                              </label>
                            </div>
                            <div class="form-check mb-2 mr-sm-2">
                              <input class="form-check-input check-target-blank" type="checkbox">
                              <label class="form-check-label" for="inlineFormCheck">
                              Ouvrir dans une nouvelle page
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      

                      <label>Lien Externe</label>   
                      <div class="panel-group">
                        <div class="row">          
                          <div class="col-md-6">
                          <input class="form-control super-btn-external" name="" value="${thisParams.params.external}" placeholder="https://">
                          </div>  
                          <div class="col-md-6">  
                            <div class="form-check mb-2 mr-sm-2">
                              <input class="form-check-input check-target-external" type="checkbox">
                              <label class="form-check-label" for="inlineFormCheck">
                              Lien extern
                              </label>
                            </div>
                          </div>      
                        </div>
                       </div>
                      <!--button class="btn btn-danger save-button btn-save" style="margin-left: 15px;" ><i class="fa fa-save"></i> Enregistrer</button-->
                      </div> 
                    </div> 
                  </div> 
                </div>
                       <div>`
                       htmlView += `</div>
               <div class="sp-message text-center"></div>
             </div> 
             `;
           return htmlView;
       },

      viewImage : function(thisParams,thisParent) {
        htmlView = `
        <div class="text-center tool-scms-title cursor-move">
          <i class="fa fa-arrow-up text-blue gotoparentBtn" aria-hidden="true"  data-id-parent="${thisParent}" ></i>
          <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres d'image</i>
          <button class="bg-transparent deleteLine delete-super-cms tooltips" data-path="tpls.blockCms.superCms.elements.image" data-id="${superCms.spId}" data-parent="${thisParent}" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément d'image">
            <i class="fa fa-trash text-red" aria-hidden="true"></i>
          </button>
          <i class="fa fa-window-close closeBtn view-super-cms" data-target="toolsBar-edit-block" aria-hidden="true"></i>
        </div>

        <div class="text-center tool-scms-title cursor-move">
          <i class="fa fa-pencil-square-o contentEditable" aria-hidden="true" title="nom du bloc editable" ></i>
          <input type=text class="blockName" id="block-name" data-id="${superCms.spId}" value="${thisParams.name}" ></input>
        </div>


        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Général</a></li>
          <li><a data-toggle="tab" href="#style-setting">Style</a></li>
          <li><a data-toggle="tab" href="#advanced-settings">Advanced</a></li>
        </ul>
        

        <div class="container-fluid">
        <div class="tab-content padding-top-20">
          <div id="home" class="tab-pane fade in active">
            <div class="row">
              <div class="col-md-12">      
                <div class="panel-group">
                    <div class="col-md-12" style="display: grid;margin-top:5px">  
                     <div class="bg-img-preview" style="margin: auto;">
                     <img class="image-bloc-image" style="width: 200px;" ></image>
                     </div>
                    <label>Ajouter une image</label>
                    <div class="input-group">
                      <input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.background.color}" placeholder="#ffffff" style="">
                      <div class="input-group-btn closeMenu" style="width: 0px">                    
                        <button class="edit-img${superCms.kunik}Params tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Modifier la photo">
                          <i class="fa fa-camera" aria-hidden="true"></i><br>
                        </button> 
                      </div>
                    </div>    
                  </div>
                  <div class="col-md-12"> 
                  <label>Taille </label>
                  <div class="size-slider text-center">
                      <span class="range-width__value">Longueur  ${thisParams.css.size.width} %</span>
                      <input  class="block-width" type="range" value="${thisParams.css.size.width.replace('%', '')}" min="0" max="100" step="0.2">
                  </div>
                  <div class="size-slider text-center">
                      <span class="range-heigth__value">Largeur  ${thisParams.css.size.height}</span>
                      <input  class="block-height" type="range" value="${(thisParams.css.size.height == "auto" ? 0 : thisParams.css.size.height.replace('px', ''))}" min="0" max="600" step="1">
                  </div>
                </div>
                  <label>Ajustement de la photo</label>
                    <select class="form-control" id="objfit" data-kunik="${superCms.kunik}" data-id="${superCms.spId}>
                      <option value="contain">Contain</option>
                      <option value="cover">Cover</option>
                      <option value="fill">Fill</option>
                      <option value="inherit">Inherit</option>
                      <option value="initial">Initial</option>
                      <option value="none">None</option>
                      <option value="revert">Revert</option>
                      <option value="scale-down">Scale-down</option>
                      <option value="unset">Unset</option>
                    </select>
                </div>
              </div>      
            </div>  
          </div>
        </div> 
      </div> 
        `;
        return htmlView;
      },

      viewAdvanced : function(thisParams) {
        htmlView = `
          <div id="advanced-settings" class="tab-pane fade">    
            <div class="col-md-12">      
              <div class="panel-group">
                <div class="form-group">
                  <label for="">Class bootstrap <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                  <textarea class="form-control other-class" rows="5" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">${thisParams.class.other}</textarea>
                </div>
                <div class="form-group">
                  <label for="">Styles css <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                  <textarea class="form-control other-css" rows="5" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">${thisParams.css.other}</textarea>
                  <p class="css_err"></p>
                </div>
              </div>
              <div class="text-center showPrt${superCms.kunik} hidden">
                  <div class="panel-group">
                    <div class="form-group">
                      <label for="">Insértion de html <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                      <textarea class="form-control other-html" rows="5">${thisParams.html}</textarea>
                      <p class="css_err"></p>
                    </div>
                  </div>
                </div>
                <div class="sp-cms-std" id="html" style="width: 100%;padding: 10px;">
                  <label style="width:100%">Insérer page <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                      <input class="form-control" type="text" id="addPage" style="width: 90%;height: auto;border-radius: inherit;">                    
                  <button class="btn btn-sm saveHtml" style="font-family: sans-serif;width: 9.9%;border-radius: inherit;background-color: #ffffff6b">OK</button>
                </div>
            </div>

            <button class="btn btn-danger save-advanced" style="margin-left: 20px;"><i class="fa fa-save"></i> Enregistrer</button>
          </div>
        `;
        return htmlView;
      },

      viewStyle : function(thisParams) {
        cmsBuilder.block.menuSelector.init();
        htmlView = `

        <div id="style-setting" class="tab-pane fade">
          <div class="style">
            <button data-toggle="collapse" data-target="#style-general" class="btn btn-primary" style="width: 100%;"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Style Générale</button> 
            <div class="col-md-12 collapse in" id="style-general">  
                    <div class="col-md-12" style="margin-top:10px">
                    <label data-toggle="collapse" data-target="#bordure" >Configuration du Bordure</label> 
                      <div class="collapse" id="bordure">
                      <div class="col-md-12">   
                      <label>Epaisseur de la bordure</label>     
                        <div class="input-group">             
                          <input class="form-control element-control" id="border-width" type="number" min="0" value="${(Math.max(thisParams.css.border.top, thisParams.css.border.left, thisParams.css.border.right,thisParams.css.border.bottom) == 0) ? thisParams.css.border.width : Math.max(thisParams.css.border.top, thisParams.css.border.left, thisParams.css.border.right,thisParams.css.border.bottom)}" placeholder="0" data-path="border.width" data-given="border-width" data-what="border-width"  data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                          <div class="input-group-btn">                    
                            <button  data-toggle="collapse" data-target="#borderWidth" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                              <i class="fa fa-plus" aria-hidden="true"></i><br>
                            </button> 
                          </div>
                        </div>  
                    <div id="borderWidth" class="collapse element-value">
                      <div class="col-elem-value">
                        <div class="element-field"> 
                          <label>Haut</label>                
                          <input class="form-control border-top border-width" type="number" min="0" value="${thisParams.css.border.top}" placeholder="0" data-target="border-width" data-path="border" data-given="top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                        </div>                  
                        <div class="element-field">      
                        <label>Bas</label>                   
                        <input class="form-control border-bottom border-width" type="number" min="0" value="${thisParams.css.border.bottom}" placeholder="0" data-target="border-width" data-path="border" data-given="bottom" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      </div>
                      <div class="element-field"> 
                        <label>Gauche</label>                           
                        <input class="form-control border-left border-width" type="number" min="0" value="${thisParams.css.border.left}" data-target="border-width" data-path="border" data-given="left" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      </div>                  
                      <div class="element-field">      
                        <label>Droite</label>                   
                        <input class="form-control border-right border-width" type="number" min="0" value="${thisParams.css.border.right}" data-target="border-width" data-path="border" data-given="right" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <label>Rayon de la bordure</label>                      
                  <div class="input-group">
                    <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.border.radius["top-left"], thisParams.css.border.radius["top-right"], thisParams.css.border.radius["bottom-left"],thisParams.css.border.radius["bottom-right"])}" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"  data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>"> 
                    <div class="input-group-btn">                    
                      <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                        <i class="fa fa-plus" aria-hidden="true"></i><br>
                      </button>
                    </div>
                  </div>
                  <div id="border" class="collapse elem-value">
                    <div class="col-elem-value">
                      <div class="element-field">
                        <label>Haut à gauche</label>
                        <input class="form-control top-left border-control" type="number" min="0" value="${thisParams.css.border.radius["top-left"]}"  data-target="border-control" data-path="border.radius" data-given="border-top-left-radius" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                      <div class="element-field">
                        <label>Haut à droite</label>
                        <input class="form-control top-right border-control" type="number" min="0" value="${thisParams.css.border.radius["top-right"]}"  data-target="border-control" data-path="border.radius" data-given="border-top-right-radius" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                      <div class="element-field">
                        <label>Bas à gauche</label>
                        <input class="form-control bottom-left border-control" type="number" min="0" value="${thisParams.css.border.radius["bottom-left"]}" data-target="border-control" data-path="border.radius" data-given="border-bottom-left-radius"  data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                      <div class="element-field">
                        <label>Bas à droite</label>
                        <input class="form-control bottom-right border-control" type="number" min="0" value="${thisParams.css.border.radius["bottom-right"]}" data-target="border-control" data-path="border.radius" data-given="border-bottom-right-radius"  data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                    </div>
                  </div>
                <label>Couleur de la bordure</label>
                <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.border.color}" placeholder="#ffffff" style="" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                <label>Type de la bordure</label>
                <select class="form-control" id="borderType" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                  <option value="dashed">dashed</option>
                  <option value="dotted">dotted</option>
                  <option value="double">double</option>
                  <option value="groove">groove</option>
                  <option value="inset">inset</option>
                  <option value="ridge">ridge</option>
                  <option value="solid">solid</option>
                  <option value="outset">outset</option>
                </select>
              </div>
              </div>
            </div>
            <div class="col-md-12" style="margin-top:10px">
              <label data-toggle="collapse" data-target="#rembourrage" >Configuration du rembourrage</label>
              <div class="collapse col-md-12" id="rembourrage">
                <div class="panel-group">
                  <label>Rembourrage (Padding)</label>
                  <div class="input-group">
                    <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.padding.top, thisParams.css.padding.right, thisParams.css.padding.left,thisParams.css.padding.bottom)}" placeholder="0" data-path="padding" data-given="padding" data-what="padding-control" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    <div class="input-group-btn">
                      <button data-toggle="collapse" data-target="#padding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <br>
                      </button>
                    </div>
                  </div>
                </div>
                <div id="padding" class="collapse elem-value">
                  <div class="col-elem-value">
                    <div class="element-field" style="width:100px">
                      <label>En haut</label>
                      <input class="form-control top-left padding-control" type="number" min="0" value="${thisParams.css.padding.top}" placeholder="0" data-target="padding-control" data-path="padding" data-given="padding-top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À droite</label>
                      <input class="form-control top-right padding-control" type="number" min="0" value="${thisParams.css.padding.right}" placeholder="0" data-target="padding-control" data-path="padding" data-given="padding-right" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À gauche</label>
                      <input class="form-control bottom-left padding-control" type="number" min="0" value="${thisParams.css.padding.left}" data-path="padding" data-target="padding-control" data-given="padding-left" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>En bas</label>
                      <input class="form-control bottom-right padding-control" type="number" min="0" value="${thisParams.css.padding.bottom}" data-path="padding" data-target="padding-control" data-given="padding-bottom" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                  </div>
                </div>
                <label>Marge</label>
                <div class="input-group">
                  <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.margin.top, thisParams.css.margin.right, thisParams.css.margin.left,thisParams.css.margin.right)}" placeholder="0" data-path="margin" data-given="margin" data-what="margin-control" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                  <div class="input-group-btn">
                    <button data-toggle="collapse" data-target="#margin" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                      <br>
                    </button>
                  </div>
                </div>
                <div id="margin" class="collapse elem-value">
                  <div class="col-elem-value">
                    <div class="element-field" style="width:100px">
                      <label>En haut</label>
                      <input class="form-control top-left margin-control" type="number" value="${thisParams.css.margin.top}" placeholder="0" data-target="margin-control" data-path="margin" data-given="margin-top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À droite</label>
                      <input class="form-control top-right margin-control" type="number" value="${thisParams.css.margin.right}" placeholder="0" data-target="margin-control" data-path="margin" data-given="margin-right" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À gauche</label>
                      <input class="form-control bottom-left margin-control" type="number" value="${thisParams.css.margin.left}" data-path="margin" data-target="margin-control" data-given="margin-left" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>En bas</label>
                      <input class="form-control bottom-right margin-control" type="number" value="${thisParams.css.margin.bottom}" data-path="margin" data-target="margin-control" data-given="margin-bottom" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;">
              <label data-toggle="collapse" data-target="#ombre" >Configuration des Ombres</label>
                <div class="collapse col-md-12" id="ombre">
                <div class="panel-group">
                  <label>Couleur</label>
                  <div class="pull-right">
                  <input id="check-inset" type="checkbox"` 
                  if (thisParams.css['box-shadow'].inset != "") {
                      htmlView += `checked="checked"`
                  }
                  htmlView += `<label for="check1">Inset</label>
                    <p></p>
                  </div>
                  <input id="shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css['box-shadow'].color}" placeholder="#ffffff" style="">
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-blur__value">Blur ${thisParams.css['box-shadow'].blur}px</span>
                    <input class="range-slider-blur super-cms-range-slider" type="range" value="${thisParams.css['box-shadow'].blur}" min="0" max="20" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-spread__value">Spread ${thisParams.css['box-shadow'].spread}px</span>
                    <input class="range-slider-spread super-cms-range-slider" type="range" value="${thisParams.css['box-shadow'].spread}" min="0" max="20" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-x__value">Axe X: ${thisParams.css['box-shadow'].x}px</span>
                    <input class="range-slider-x super-cms-range-slider" type="range" value="${thisParams.css['box-shadow'].x}" min="-10" max="10" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-y__value">Axe Y: ${thisParams.css['box-shadow'].y}px</span>
                    <input class="range-slider-y super-cms-range-slider" type="range" value="${thisParams.css['box-shadow'].y}" min="-10" max="10" step="0.2">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hover">
            <button data-toggle="collapse" data-target="#style-hover" class="btn btn-primary" style="width: 100%;"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Style Hover</button> 
            <div class="col-md-12 collapse padding-bottom-30" id="style-hover"> 
                <div class="col-md-12">  
                  <label>Couleur du fond</label>
                  <div class="input-group">
                    <input id="hover-fond-color" data-kunik="${superCms.kunik}" data-id="${superCms.spId}" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.hover.background.color ? thisParams.css.hover.background.color : "transparent"}" placeholder="#ffffff">
                  </div>
                </div> 
                <div class="col-md-12">      
                  <label>Configuration du Bordure</label>    
                  <div class="col-md-12">
                  <label>Epaisseur de la bordure</label>                  
                    <div class="input-group">
                      <input class="form-control element-control" id="hover-border-width" type="number" min="0" value="${(Math.max(thisParams.css.hover.border.top, thisParams.css.hover.border.left, thisParams.css.hover.border.right,thisParams.css.hover.border.bottom) == 0) ? thisParams.css.hover.border.width : Math.max(thisParams.css.hover.border.top, thisParams.css.hover.border.left, thisParams.css.hover.border.right,thisParams.css.hover.border.bottom)}" placeholder="0" data-path="hover.border.width" data-given="border-width" data-what="hover-border-width"  data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      <div class="input-group-btn">                    
                        <button  data-toggle="collapse" data-target="#hoverBorderWidth" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                          <i class="fa fa-plus" aria-hidden="true"></i><br>
                        </button> 
                      </div>
                    </div>
                    <div id="hoverBorderWidth" class="collapse element-value">
                      <div class="col-elem-value">
                        <div class="element-field"> 
                          <label>Haut</label>                
                          <input class="form-control hover-border-top hover-border-width" type="number" min="0" value="${thisParams.css.hover.border.top}" placeholder="0" data-target="hover-border-width" data-path="hover.border" data-given="top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                        </div>                  
                        <div class="element-field">      
                        <label>Bas</label>                   
                        <input class="form-control hover-border-bottom hover-hover-border-width" type="number" min="0" value="${thisParams.css.hover.border.bottom}" placeholder="0" data-target="hover-border-width" data-path="hover.border" data-given="bottom" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      </div>
                      <div class="element-field"> 
                        <label>Gauche</label>                           
                        <input class="form-control hover-border-left hover-hover-border-width" type="number" min="0" value="${thisParams.css.hover.border.left}" data-target="hover-border-width" data-path="hover.border" data-given="left" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      </div>                  
                      <div class="element-field">      
                        <label>Droite</label>                   
                        <input class="form-control hover-border-right hover-hover-border-width" type="number" min="0" value="${thisParams.css.hover.border.right}" data-target="hover-border-width" data-path="hover.border" data-given="right" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"> 
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <label>Rayon de la bordure</label>                      
                  <div class="input-group">
                    <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.hover.border.radius["top-left"], thisParams.css.hover.border.radius["top-right"], thisParams.css.hover.border.radius["bottom-left"],thisParams.css.hover.border.radius["bottom-right"])}" placeholder="0" data-path="hover.border.radius" data-given="border-radius" data-what="hover-border-control"  data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>"> 
                    <div class="input-group-btn">                    
                      <button  data-toggle="collapse" data-target="#hoverBorder" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                        <i class="fa fa-plus" aria-hidden="true"></i><br>
                      </button>
                    </div>
                  </div>
                  <div id="hoverBorder" class="collapse elem-value">
                    <div class="col-elem-value">
                      <div class="element-field">
                        <label>Haut à gauche</label>
                        <input class="form-control top-left form-control top-left hover-border-control" type="number" min="0" value="${thisParams.css.hover.border.radius["top-left"]}"  data-target="hover-border-control" data-path="hover.border.radius" data-given="border-top-left-radius" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                      <div class="element-field">
                        <label>Haut à droite</label>
                        <input class="form-control top-right hover-border-control" type="number" min="0" value="${thisParams.css.hover.border.radius["top-right"]}"  data-target="hover-border-control" data-path="hover.border.radius" data-given="border-top-right-radius" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                      <div class="element-field">
                        <label>Bas à gauche</label>
                        <input class="form-control bottom-left hover-border-control" type="number" min="0" value="${thisParams.css.hover.border.radius["bottom-left"]}" data-target="hover-border-control" data-path="hover.border.radius" data-given="border-bottom-left-radius"  data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                      <div class="element-field">
                        <label>Bas à droite</label>
                        <input class="form-control bottom-right hover-border-control" type="number" min="0" value="${thisParams.css.hover.border.radius["bottom-right"]}" data-target="hover-border-control" data-path="hover.border.radius" data-given="border-bottom-right-radius"  data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                      </div>
                    </div>
                  </div>
                <label>Couleur de la bordure</label>
                <input id="hover-border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.hover.border.color}" placeholder="#ffffff" style="" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                <label>Type de la bordure</label>
                <select class="form-control" id="hoverBorderType" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                  <option value="dashed">dashed</option>
                  <option value="dotted">dotted</option>
                  <option value="double">double</option>
                  <option value="groove">groove</option>
                  <option value="inset">inset</option>
                  <option value="ridge">ridge</option>
                  <option value="solid">solid</option>
                  <option value="outset">outset</option>
                </select>
              </div>
            </div>
            <div class="col-md-12" style="margin-top:10px">
              <label>Configuration du rembourrage</label>
              <div class="col-md-12">
                <div class="panel-group">
                  <label>Rembourrage (Padding)</label>
                  <div class="input-group">
                    <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.hover.padding.top, thisParams.css.hover.padding.right, thisParams.css.hover.padding.left,thisParams.css.hover.padding.bottom)}" placeholder="0" data-path="hover.padding" data-given="padding" data-what="hover-padding-control" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    <div class="input-group-btn">
                      <button data-toggle="collapse" data-target="#hoverPadding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <br>
                      </button>
                    </div>
                  </div>
                </div>
                <div id="hoverPadding" class="collapse elem-value">
                  <div class="col-elem-value">
                    <div class="element-field" style="width:100px">
                      <label>En haut</label>
                      <input class="form-control top-left hover-padding-control" type="number" min="0" value="${thisParams.css.hover.padding.top}" placeholder="0" data-target="hover-padding-control" data-path="hover.padding" data-given="padding-top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À droite</label>
                      <input class="form-control top-right hover-padding-control" type="number" min="0" value="${thisParams.css.hover.padding.right}" placeholder="0" data-target="hover-padding-control" data-path="hover.padding" data-given="padding-right" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À gauche</label>
                      <input class="form-control bottom-left hover-padding-control" type="number" min="0" value="${thisParams.css.hover.padding.left}" data-path="hover.padding" data-target="hover-padding-control" data-given="padding-left" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>En bas</label>
                      <input class="form-control bottom-right hover-padding-control" type="number" min="0" value="${thisParams.css.hover.padding.bottom}" data-path="hover.padding" data-target="hover-padding-control" data-given="padding-bottom" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                  </div>
                </div>
                <label>Marge</label>
                <div class="input-group">
                  <input class="form-control elem-control" type="number" min="0" value="${Math.max(thisParams.css.hover.margin.top, thisParams.css.hover.margin.right, thisParams.css.hover.margin.left,thisParams.css.hover.margin.right)}" placeholder="0" data-path="hover.margin" data-given="margin" data-what="hover-margin-control" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                  <div class="input-group-btn">
                    <button data-toggle="collapse" data-target="#hoverMargin" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                      <br>
                    </button>
                  </div>
                </div>
                <div id="hoverMargin" class="collapse elem-value">
                  <div class="col-elem-value">
                    <div class="element-field" style="width:100px">
                      <label>En haut</label>
                      <input class="form-control top-left hover-margin-control" type="number" value="${thisParams.css.hover.margin.top}" placeholder="0" data-target="hover-margin-control" data-path="hover.margin" data-given="margin-top" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À droite</label>
                      <input class="form-control top-right hover-margin-control" type="number" value="${thisParams.css.hover.margin.right}" placeholder="0" data-target="hover-margin-control" data-path="hover.margin" data-given="margin-right" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>À gauche</label>
                      <input class="form-control bottom-left hover-margin-control" type="number" value="${thisParams.css.hover.margin.left}" data-path="hover.margin" data-target="hover-margin-control" data-given="margin-left" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                    <div class="element-field" style="width:100px">
                      <label>En bas</label>
                      <input class="form-control bottom-right hover-margin-control" type="number" value="${thisParams.css.hover.margin.bottom}" data-path="hover.margin" data-target="hover-margin-control" data-given="margin-bottom" placeholder="0" data-kunik="${superCms.kunik}" data-id="${superCms.spId}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;">
              <label>Configuration des Ombres</label>
              <div class="col-md-12">
                <div class="panel-group">
                  <label>Couleur</label>
                  <div class="pull-right">
                  <input id="hover-check-inset" type="checkbox"` 
                  if (thisParams.css.hover['box-shadow'].inset != "") {
                      htmlView += `checked="checked"`
                  }
                  htmlView += `<label for="check1">Inset</label>
                    <p></p>
                  </div>
                  <input id="hover-shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="${thisParams.css.hover['box-shadow'].color}" placeholder="#ffffff" style="">
                  <div class="range-slider text-center padding-top-20">
                    <span class="hover-range-blur__value">Blur ${thisParams.css.hover['box-shadow'].blur}px</span>
                    <input class="hover-range-slider-blur super-cms-range-slider" type="range" value="${thisParams.css.hover['box-shadow'].blur}" min="0" max="20" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="hover-range-spread__value">Spread ${thisParams.css.hover['box-shadow'].spread}px</span>
                    <input class="hover-range-slider-spread super-cms-range-slider" type="range" value="${thisParams.css.hover['box-shadow'].spread}" min="0" max="20" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="hover-range-x__value">Axe X: ${thisParams.css.hover['box-shadow'].x}px</span>
                    <input class="hover-range-slider-x super-cms-range-slider" type="range" value="${thisParams.css.hover['box-shadow'].x}" min="-10" max="10" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="hover-range-y__value">Axe Y: ${thisParams.css.hover['box-shadow'].y}px</span>
                    <input class="hover-range-slider-y super-cms-range-slider" type="range" value="${thisParams.css.hover['box-shadow'].y}" min="-10" max="10" step="0.2">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      `;
        return htmlView;
      },

      viewAddElement : function(thisParams) {
        var list = cmsBuilder.block.lists.menuSelector;
        htmlView = `
        <div id="addElt" class="tab-pane fade ">    
                <div class="col-md-12 sp-cms-options"> 
                <div class="cards-list">

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-type-cms" data-toggle="collapse" data-target="#blocList">
                        <div class="card_icon">
                          <i class="fa fa-object-group"></i>
                          </div>
                        <div class="card_title title-black">
                          <p>Bloc cms</p>
                        </div>
                      </div>
                      <div id="blocList" class="collapse">
                       ${cmsBuilder.block.menuSelector.construct()}
                      </div>
                    </div>
                    

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="Super bloc texte" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.supertext">
                        <div class="card_icon">
                          <i class="fa fa-align-center"></i>
                           </div>
                          <div class="card_title  title-black">
                            <p>Texte</p>
                          </div>
                        </div>
                      </div>

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="Super bloc image" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.image">
                        <div class="card_icon">
                          <i class="fa fa-image"></i>
                          </div>
                        <div class="card_title title-black">
                          <p>Image</p>
                        </div>
                      </div>
                    </div>

                    <div style="width: 100%;" class="super-element hidden">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="Super bloc form" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.forms">
                        <div class="card_icon">                          
                          <i class="fa fa-window-restore"></i>
                          </div>
                        <div class="card_title title-black">
                          <p>Forms</p>
                        </div>
                      </div>
                    </div>

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="Super bloc bouton" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.button">
                        <div class="card_icon">
                          <i class="fa fa-keyboard-o"></i>
                        </div>
                        <div class="card_title title-black">
                          <p>Bouton</p>
                        </div>
                      </div>
                    </div>

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="Super colonne" data-subtype="supercms" data-path="tpls.blockCms.superCms.container">
                        <div class="card_icon">
                          <i class="fa fa-columns"></i>
                        </div>
                        <div class="card_title title-black">
                          <p>Colonne</p>
                        </div>
                      </div>
                    </div>

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="graph bloc" data-subtype="graph" data-path="tpls.blockCms.graph.graph">
                        <div class="card_icon">
                          <i class="fa fa-pie-chart"></i>
                        </div>
                        <div class="card_title title-black">
                          <p>Graph</p>
                        </div>
                      </div>
                    </div>

                    <div style="width: 100%;" class="super-element">
                      <div class="card-sp-cms add-super-element" data-costum='<?= json_encode($costum) ?>' data-name="Separator" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.lineSeparator">
                        <div class="card_icon">
                          <i class="fa fa-braille"></i>
                        </div>
                        <div class="card_title title-black">
                          <p>Ligne separatrice</p>
                        </div>
                      </div>
                    </div>


                  </div>    
                </div>
              </div>

     `;
        return htmlView;
      },

      viewTextEditor : function(thisParams) {
        let blockParent = thisParams.parentId;
        let blockName = thisParams.name;
        let domTitle = ""
        if (thisParams.name != "") {
          domTitle = `<div class="text-center tool-scms-title cursor-move">
                        <i class="fa fa-pencil-square-o contentEditable" aria-hidden="true" title="nom du bloc editable" ></i>
                        <input type=text class="blockName" id="block-name" data-id="${superCms.spId}" value="${thisParams.name}" ></input>
                       </div>`
        }
        htmlView = `
        
                  <div class="text-center tool-scms-title cursor-move">
                    <i class="fa fa-arrow-up text-blue gotoparentBtn" aria-hidden="true"  data-id-parent="${blockParent}" ></i>
                    <span style="cursor:default;font-size: large;display: inline-block;width: 90%;padding-right:10%;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;" aria-hidden="true" class="block-title">Paramètres de texte</span>
                    <button class="bg-transparent deleteLine delete-super-cms tooltips" data-id="${superCms.spId}" data-parent="${blockParent}" data-collection="cms" type="text" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer tout le texte">
                      <i class="fa fa-trash text-red" aria-hidden="true" ></i>
                    </button>
                    <i class="fa fa-window-close closeBtn pull-right" aria-hidden="true" data-target="toolsBar-edit-block"  ></i>
                  </div>

                  ${domTitle}

                  <div class="container-fluid">

                  <div id="container" >
                    <fieldset>

                      <div class="button-wrapper">
                        <select id="input-font" class="input"></select>
                      </div>

                      <div class="button-wrapper">
                        <!-- font size start -->
                        <select id="fontSize">
                        </select>
                        <!-- font size end -->
                      </div>

                      <div class="colorize-wrapper">
                        <input class="color-apply  sp-colorize colorize-wrapper" type="color" id="myColor" value="#0b0b0b"> 
                      </div>

                      <div class="colorize-wrapper">
                        <input class="color-apply  sp-colorize colorize-wrapper" type="color" id="myBGColor" value="#0b0b0b"> 
                      </div>

                      <div class="button-wrapper">
                        <button class="fontStyle font-setting" data-target="italic" title="Italicize Highlighted Text"><i class="fa fa-italic"></i></button>
                        <button class="fontStyle font-setting" data-target="bold" title="Bold Highlighted Text"><i class="fa fa-bold"></i></button>
                        <button class="fontStyle font-setting" data-target="underline"><i class="fa fa-underline"></i></button> 
                        <button class="fontStyle font-setting" data-target="strikethrough" ><i class="fa fa-strikethrough"></i></button>
                      </div>

                      <div class="button-wrapper">
                        <button class="fontStyle font-setting" data-target="justifyLeft"><i class="fa fa-align-left"></i></button>
                        <button class="fontStyle font-setting" data-target="justifyFull"><i class="fa fa-align-justify"></i></button>
                        <button class="fontStyle font-setting" data-target="justifyCenter"><i class="fa fa-align-center"></i></button>
                        <button class="fontStyle font-setting" data-target="justifyRight"><i class="fa fa-align-right"></i></button>
                      </div>

                      <div class="button-wrapper">
                        <button onmousedown="createLink('[label](http://www)')" class="fontStyle add-link" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ajouter une lien">
                        <i class="fa fa-link" aria-hidden="true"></i><br>
                        </button> 
                      </div>


                      
                    </fieldset>
                    
                    
                  </div>

                  <!--  <div class="form-group">
                    <label for="">Texte</label>
                    <textarea class="form-control edit-text" rows="5" data-kunik="${superCms.kunik}" data-id="${superCms.spId}"></textarea>
                  </div> -->
                <div id="text-display" style="width:100%;padding:5px" data-target="display"></div>
              </div>

     `;
        return htmlView;

      },
      
    },

    init : function() {    
      superCms.bindEvents() 
      cmsBuilder.block.menuSelector.init();   
      //----------Close menu--------------
      $(document).on("click",".closeBtn, .closeMenu",function(e){
        var close = $(this);
        // if(JSON.stringify(superCms.pathConfig) == JSON.stringify(superCms.sp_params[superCms.spId].config)){
          //superCms.viewMode();
          // $("#supercms-overlay").empty();
          // $("#supercms-overlay").css({"display": "none"});
          // $(superCms.parents).append(superCms.element);
          $("#"+$(this).data("target")).hide();
          $("#"+$(this).data("target")).html("");
          if($(this).data("target")=="toolsBar-edit-block")
            coInterface.initHtmlPosition();
        // }else{
        //   bootbox.confirm("Vous avez des modifications non enregistrées! Vous êtes sur de fermer le menu ?", function(response) {
        //     if (response) {
        //       //superCms.viewMode();
        //       $("#customStyle"+superCms.kunik).remove();
        //       $("#customIconStyle"+superCms.kunik).remove();
        //       $("#"+close.data("target")).hide();
        //       $("#"+close.data("target")).html("");
        //       if(close.data("target")=="toolsBar-edit-block")
        //         coInterface.initHtmlPosition();
        //     }
        //   });
        // }
        e.stopImmediatePropagation();
      });

      $(document).on("click",".gotoparentBtn",function(e){
        var idParent = $(this).data("id-parent");
        $("."+idParent).click();
        e.stopImmediatePropagation();
      });

      /**********Color settings *************/
      if($(".colorpickerInput").length){
        loadColorPicker(initColor);
      }
    },

    /*-----View and edit mode------- */
    viewMode: function(){
      $(".super-cms").resizable();
      $(".super-cms").resizable("destroy");
      $(".super-cms").removeClass("editing");
      //$(".super-cms").removeClass("selected-mode");
    },
    editMode: function(){
      // $(".super-cms").addClass("editing");
      // mode = "w";
      if(superCms.kunik){
        $("."+superCms.kunik).removeClass("in_edition");
        $(".img-"+superCms.kunik).removeClass("in_edition");
        $(".btn-"+superCms.kunik).removeClass("in_edition");
      }
      $(".super-cms").resizable({          
        handles: 'e, w',
        start:function(event, ui){  
          alert("okokoko")          
          $(".whole-"+this.dataset.superCms.kunik).css("min-height", "auto");
        },
        stop: function(event, ui){
          var myWidth = $(".whole-"+this.dataset.superCms.kunik).width();
          var myHeight = $(".whole-"+this.dataset.superCms.kunik).height();
          var parentWidth = $(".whole-"+this.dataset.superCms.kunik).parent().width();
          if (this.dataset.type === "blockCopy") {
            parentWidth = $( window ).width();
          }
          widthPrnt = (myWidth / parentWidth) * 100;
          $(".whole-"+this.dataset.superCms.kunik).css("height", "inherit");
          // $(".whole-<?//= $superCms.kunik ?>").css("min-height", (myHeight+4)+"px");
          $(".whole-"+this.dataset.superCms.kunik).css("width", (widthPrnt+0.374)+"%");
          revert: true,
          superCms.pathConfig.css['size'] = superCms.pathConfig.css['size'] || Object();
          superCms.pathConfig.css['size']['css'] = {};
          superCms.pathConfig.css['size']['width'] = widthPrnt+'%';
          //superCms.viewMode();
        }
      });
      $(".btn-functionLink").resizable("destroy");
    },

    manage : {
      addClassToSelection : function (className){
        var selection = window.getSelection();
        var cssSelected = superCms.cssSelected; 
        cssSelected = (selection.getRangeAt(0).startContainer.parentNode).style.cssText;
        clasSelected = selection.baseNode.parentNode.className;
        clasSelected = clasSelected.replace("mytext focus-visible","");
        clasSelected = clasSelected.replace("stop-propagation","");
        array_class_selected = clasSelected.split(/[ ,]+/);
        array_css_selected = cssSelected.split(";");

        if(selection.rangeCount > 0){
          var range = selection.getRangeAt(0),
          fragments = range.extractContents(),
          nodeList = fragments.childNodes;

          nodeList.forEach(node => {
            if(node.nodeType == Node.TEXT_NODE){
              var span = document.createElement("span")
              span.classList.add(className)              
              wrap(node, span)
            }else
            node.classList.add(className)
          })

          range.insertNode(fragments)
        }
      },
      eltSelection : function () {
        


        function String2Json(element, json) {
          var treeObject = {};

          if (typeof element === "string") {
            if (window.DOMParser) {
              parser = new DOMParser();
              docNode = parser.parseFromString(element,"text/xml");
            } else { 
              docNode = new ActiveXObject("Microsoft.XMLDOM");
              docNode.async = false;
              docNode.loadXML(element); 
            } 
            element = docNode.firstChild;
          }

          function treeHTML(element, object) {
            object["type"] = element.nodeName;
            var nodeList = element.childNodes;
            if (nodeList != null) {
              if (nodeList.length) {
                object["content"] = [];
                for (var i = 0; i < nodeList.length; i++) {
                  if (nodeList[i].nodeType == 3) {
                    object["content"].push(nodeList[i].nodeValue);
                  } else {
                    object["content"].push({});
                    treeHTML(nodeList[i], object["content"][object["content"].length -1]);
                  }
                }
              }
            }
            if (element.attributes != null) {
              if (element.attributes.length) {
                object["attributes"] = {};
                for (var i = 0; i < element.attributes.length; i++) {
                  object["attributes"][element.attributes[i].nodeName] = element.attributes[i].nodeValue;
                }
              }
            }
          }
          treeHTML(element, treeObject);

          return (json) ? treeObject : treeObject;
        }



        $(function () {
          var count = 1;
          /**************Open text settings panel**************/

          $(document).on("mouseup", ".sp-text",function(e) { 
            var selectedText = window.getSelection().getRangeAt(0).startContainer.parentNode
            if (selectedText.color)
              $("#myColor").val(selectedText.color);

            if (selectedText.style.color) {
              let colorRGB = selectedText.style.color;
              let hexColor = "#"+colorRGB.match(/[0-9|.]+/g).map((x,i) => i === 3 ? parseInt(255 * parseFloat(x)).toString(16) : parseInt(x).toString(16)).join('')
              if (hexColor.length < 6) {
                for (var i = 0; hexColor.length < 6; i++) {
                  hexColor = hexColor+"0"
                }
              }
              $("#myColor").val(hexColor);
            }

            if (selectedText.face)     
              $("#input-font").val(selectedText.face)

            if (selectedText.size)
              $("#fontSize").val(selectedText.size)

            if (selectedText.style["font-size"])
              $("#fontSize").val(parseInt(selectedText.style["font-size"]));

            /************Font size options************/
            var fontOpt ="";
            for (var i = 5; i < 501; i++) {
              fontOpt += "<option value='"+i+"'>"+i+"</option>"
            }
            $("#fontSize").append(fontOpt);
            /*********End font size options***********/

            e.stopImmediatePropagation();
          });

  
          $(document).on("mousedown", ".sp-text",function(e){
            e.stopImmediatePropagation();
            if (costum.editMode) {  
              superCms.elSelected = $(this);
              superCms.spId = superCms["elSelected"].data("id");
              superCms.focusedDom = "is-sp-text"  
              document.designMode = "on"  
              var thisName = "" ;
              var thisParent = "";
              actual_text = superCms["elSelected"].html() ;           
              waitForTyping(superCms.elSelected)
              if (notNull(superCms.sp_params[superCms.spId])) {
               thisParent = superCms.sp_params[superCms.spId].parent
               thisName = superCms.sp_params[superCms.spId].config.name
             }
              var thisParams = {name : thisName, id: superCms.spId, parentId : thisParent, $this : superCms["elSelected"]};
              // Paste as text without styles
              superCms["elSelected"].bind('paste', function (e) {
                e.stopImmediatePropagation()
                e.preventDefault()
                var text = e.originalEvent.clipboardData;
                text = text.getData("text/plain")
                document.execCommand('insertText', false, text)
              })

              superCms["elSelected"].attr('contenteditable','true');
              mylog.log(superCms["elSelected"].attr('id'))
              if (superCms["elSelected"].data("target") != "display") {
                $(".sp-container").hide();  
                superCms["elSelected"].children(":first").focus();
                $("#toolsBar-edit-block").html("");
                $("#toolsBar-edit-block").html(superCms.menu.viewTextEditor(thisParams));
                $("#text-display").attr('contenteditable','true');
                $("#text-display").css("background-color" , InheritedBackgroundColor($(this)).bgTextColor)
                $("#text-display").css("color" , InheritedBackgroundColor($(this)).textColor)
                $("#text-display").html(superCms["elSelected"].html())
              }


              $("#toolsBar-edit-block").show();
              $(".sp-settings-btn").hide();


              if (thisParent != ""){
                $(".gotoparentBtn").css({"visibility": "visible"});  
              }

              $('#input-font').append(fontOptions);
              cmsBuilder.init();

              function _isContains(json, value) {
                let contains = false;
                Object.keys(json).some(key => {
                    contains = typeof json[key] === 'object' ? 
                    _isContains(json[key], value) : json[key] === value;
                    return contains;
                });
                return contains;
              }

              $(document).on("blur", ".sp-text",function(e){
                superCms["elSelected"].removeAttr('contenteditable')
                text = superCms["elSelected"].html();
                tplCtx = {};
                tplCtx.id = superCms["elSelected"].data("id");
                tplCtx.collection = "cms";
                tplCtx.path = superCms["elSelected"].data("field"); 
                tplCtx.value = text;

                json  = String2Json(text , true);
                
                if (actual_text != text){
                  if (_isContains(json, "script")  == true ) {
                    toastr.error("Script detecté");
                  } else {
                    dataHelper.path2Value( tplCtx, function(params) {       
                      toastr.success("Modification enrégistré");});
                  }
                }
                e.stopImmediatePropagation()
              });

              
              
              $(".closeBtn").click(function(){
                text = superCms["elSelected"].html();
                superCms["elSelected"].removeAttr('contenteditable')
                tplCtx = {};
                tplCtx.id = superCms["elSelected"].data("id");
                tplCtx.collection = "cms";
                tplCtx.path = superCms["elSelected"].data("field"); 
                tplCtx.value = text;

                json  = String2Json(text , true);

                if (actual_text != text){
                  if (_isContains(json, "script")  == true ) {
                    toastr.error("Script detecté");
                  } else {
                    dataHelper.path2Value( tplCtx, function(params) {       
                      toastr.success("Modification enrégistré");});
                  }
                }

                $("#toolsBar").hide();
                $("#toolsBar").html("");
              });


              $("#text-display").on("click",function() {
                superCms.focusedDom = "is-mirror" 
                waitForTyping($("#text-display"))
                e.stopImmediatePropagation()
              })

              $(".font-setting").on("click",function() {
                document.execCommand($(this).data("target"),false,null);
               if (superCms.focusedDom == "is-sp-text") {
                  $("#text-display").html(superCms["elSelected"].html())
                }else{
                  superCms["elSelected"].html($("#text-display").html())
                }   
                e.stopImmediatePropagation()
              })


              $(document).on("mouseup",".add-link", function(e) {
                mylog.log("es", superCms.focusedDom)
                if (superCms.focusedDom == "is-mirror") {
                  superCms["elSelected"].html($("#text-display").html())
                }else{
                  $("#text-display").html(superCms["elSelected"].html())
                } 
                e.stopImmediatePropagation()
              })

              $( "#myColor" ).change(function() {
                var color = $(this).val();    
                setTimeout(function(){    
                  document.execCommand('foreColor', false, color);
                  if (superCms.focusedDom == "is-sp-text") {
                    $("#text-display").html(superCms["elSelected"].html())
                  }else{
                    superCms["elSelected"].html($("#text-display").html())
                  }                  
                },90);
              });

              $( "#input-font" ).change(function() {
                var myFont = document.getElementById("input-font").value;
                document.execCommand('fontName', false, myFont);
                if (superCms.focusedDom == "is-sp-text") {
                  $("#text-display").html(superCms["elSelected"].html())
                }else{
                  superCms["elSelected"].html($("#text-display").html())
                }  
              });

              $( "#fontSize" ).change(function() {
                let fontSize = document.getElementById("fontSize").value;;
                  changeFont(fontSize)
                  if (superCms.focusedDom == "is-sp-text") {
                    $("#text-display").html(superCms["elSelected"].html())
                  }else{
                    superCms["elSelected"].html($("#text-display").html())
                  }  
                });

              function changeFont(fontValue) {
                document.execCommand( "styleWithCSS", false, "true" );
                document.execCommand("fontSize", false, "4");
                var fontElements = window.getSelection().anchorNode.parentNode
                fontElements.removeAttribute("size");
                fontElements.style.fontSize = fontValue+"px";
              }

              function checkDiv(){
                var editorText = document.getElementById("#"+superCms.spId+"textShow").innerHTML;
                if(editorText === ''){
                  document.getElementById("#"+superCms.spId+"textShow").style.border = '5px solid red';
                }
              }

              function removeBorder(){
                document.getElementById("#"+superCms.spId+"textShow").style.border = '1px solid transparent';
              }

              function waitForTyping(input) {                
                var timer = null;
                input.keyup(function(){
                 clearTimeout(timer); 
                 timer = setTimeout(updateText, 1000)
               });
 
                function updateText() {
                  if (superCms.focusedDom == "is-sp-text") {
                    $("#text-display").html(superCms["elSelected"].html())
                  }else{
                    superCms["elSelected"].html($("#text-display").html())
                  } 
                };
              }

            }
              /**************End open text settings panel**************/
              
          });
      });

        $(document).on("click",".spCustomBtn", function(e){
          e.stopImmediatePropagation();
          if (costum.editMode && !$(".block-title").text().match("texte")) {
            superCms.kunik = $(this).data("kunik");
            superCms.spId = $(this).data("id");
            if(superCms.sp_params[superCms.spId]){
              $("."+$(this).data("kunik")).addClass("customStyle"+superCms.kunik);
              $(".icon-"+$(this).data("kunik")).addClass("customIconStyle"+superCms.kunik);
              var thisParams = superCms.sp_params[superCms.spId].config;
              var thisParent = superCms.sp_params[superCms.spId].parent;
              superCms.pathConfig = JSON.parse(JSON.stringify(superCms.sp_params[superCms.spId].config));
                var highlightedText = "";
                if (window.getSelection) {
                  highlightedText = window.getSelection().toString();
                }
                if (highlightedText == "") {
                  if(superCms.recentlyEdited != ""){
                    $("."+superCms.recentlyEdited).addClass("edit-"+superCms.recentlyEdited);    
                  }
                  superCms.recentlyEdited = superCms.kunik;
                  $(".super-cms").removeClass("in_edition");
                  $("."+superCms.kunik).removeClass("editing");
                  $("."+superCms.kunik).addClass("in_edition");
                  $(".spDrag").hide(); 

                  if ($("#menu-right-costumizer").is(':visible')){
                    $("#menu-right-costumizer").hide();  
                  };

                  if (superCms.kunik.startsWith('container') == true) {
                    $("#toolsBar-edit-block").html("");
                    $("#toolsBar-edit-block").show();  
                    $("#toolsBar-edit-block").html(superCms.menu.viewContainer(thisParams,thisParent));
                    $(".tab-content").append(superCms.menu.viewAdvanced(thisParams));
                    $(".tab-content").append(superCms.menu.viewStyle(thisParams));
                    $(".tab-content").append(superCms.menu.viewAddElement(thisParams));
                    var imageSrc = $('.'+superCms.kunik).css('background-image').replace(/^url\(['"](.+)['"]\)/, '$1');
                    
                    if (imageSrc != "none") {
                      $(".imageBg-container").attr('src', imageSrc);  
                    }else{
                      $(".bg-control").css({"display": "none"});  
                    }

                    if ($("."+superCms.kunik+" div").hasClass("empty-test")) {
                      $(".panel-insertion").click();
                    }
                  }else if (superCms.kunik.startsWith('lineSeparator') == true) {;
                    $("#toolsBar-edit-block").show(); 
                    $("#toolsBar-edit-block").html(superCms.menu.viewLineSeparator(thisParams,thisParent));
                    $(".tab-content-sp-cms").append(superCms.menu.viewMenuIcon(thisParams));
                    $(".tab-content").append(superCms.menu.viewAdvanced(thisParams));
                    $(".tab-content").append(superCms.menu.viewStyle(thisParams));
                  }else if (superCms.kunik.startsWith('image') == true){
                    $("."+superCms.kunik).removeClass("editing");
                    $("."+superCms.kunik).removeClass("in_edition");
                    $(".img-"+superCms.kunik).addClass("in_edition");
                    $("#toolsBar-edit-block").show(); 
                    $("#toolsBar-edit-block").html(superCms.menu.viewImage(thisParams,thisParent));
                    $(".tab-content").append(superCms.menu.viewAdvanced(thisParams));
                    $(".tab-content").append(superCms.menu.viewStyle(thisParams));
                    
                    var imageSrc = $('.'+superCms.kunik).attr('src').replace(/^url\(['"](.+)['"]\)/, '$1');

                    $(".image-bloc-image").attr('src', imageSrc);  

                    // $(".tab-content-sp-cms").append(superCms.menu.viewMenuBorders(thisParams));
                  }

                  if (typeof isSuperAdmin == undefined || isSuperAdmin != true || superCms.kunik.startsWith('container') == false) {
                    $("a[href='#advanced-settings']").remove();
                  }

                  if (thisParent != ""){
                    $(".gotoparentBtn").css({"visibility": "visible"});  
                  }

                  
                  coInterface.initHtmlPosition();
                  // $("#toolsBar-edit-block").show();  
                  // $("#toolsBar-edit-block").html(superCms.menu.view(thisParams));
                  superCms.eltBlur   = $('.range-slider-blur').val();
                  superCms.eltSpread = $('.range-slider-spread').val();
                  superCms.eltAxeX   = $('.range-slider-x').val();
                  superCms.eltAxeY   = $('.range-slider-y').val();
                  superCms.eltColor  = $("#shw-color").val();
                  superCms.inset     = "";
                  superCms.hoverEltBlur   = $('.hover-range-slider-blur').val();
                  superCms.hoverEltSpread = $('.hover-range-slider-spread').val();
                  superCms.hoverEltAxeX   = $('.hover-range-slider-x').val();
                  superCms.hoverEltAxeY   = $('.hover-range-slider-y').val();
                  superCms.hoverEltColor  = $("#hover-shw-color").val();
                  superCms.hoverInset     = "";
                  $(".sp-color-picker").spectrum({
                    type: "text",
                    showInput: "true",
                    showInitial: "true"
                  });
                   
                  // if($(".sp-color-picker").length){
                  //   $(".sp-color-picker").spectrum({
                  //     type: "text",
                  //     showInput: "true",
                  //     showInitial: "true"
                  //   });
                  //   $('.sp-color-picker').addClass('form-control');
                  //   $('.sp-original-input-container').css("width","100%");
                  // }

                  /* Upload d'image */
                  sectionDyf[superCms.kunik+"Params"] = {
                    "jsonSchema" : {    
                      "title" : "Importation d'image",
                      "description" : "Personnaliser votre bloc",
                      "icon" : "fa-cog",

                      "properties" : {    
                        "image" :{
                          "inputType" : "uploader",
                          "label" : "image",
                          "docType": "image",
                          "contentKey" : "slider",
                          "itemLimit" : 1,
                          "endPoint": "/subKey/block",
                          "domElement" : "image",
                          "filetypes": ["jpeg", "jpg", "gif", "png"],
                          "label": "Image :",
                          "showUploadBtn": false,
                          initList : thisParams.initImage
                        },
                      },
                      beforeBuild : function(){
                        uploadObj.set("cms",superCms.spId);
                      },
                      save : function (data) {  
                        tplCtx.value = {};
                        $.each( sectionDyf[superCms.kunik+"Params"]["jsonSchema"]["properties"] , function(k,val) { 
                          tplCtx.value[k] = $("#"+k).val();
                          if (k == "parent")
                            tplCtx.value[k] = formData.parent;

                          if(k == "items")
                            tplCtx.value[k] = data.items;
                        });

                        if(typeof tplCtx.value == "undefined")
                          toastr.error('value cannot be empty!');
                        else {
                          dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                              toastr.success("Élément bien ajouter");
                              $("#ajax-modal").modal('hide');
                              cmsBuilder.block.loadIntoPage(
                                $("."+superCms.kunik).parents(".custom-block-cms").data("id"),
                                cmsBuilder.config.page,
                                $("."+superCms.kunik).parents(".custom-block-cms").data("path"),
                                $("."+superCms.kunik).parents(".custom-block-cms").data("kunik"),
                                function(){
                                  $(".spCustomBtn."+superCms.spId).trigger("click");
                                });
                              //urlCtrl.loadByHash(location.hash);
                              
                            });
                          } );
                        }
                      }
                    }
                  };
                  $(".edit-img"+superCms.kunik+"Params").off().on("click",function() {  
                    tplCtx.subKey = "imgParent";
                    tplCtx.id = superCms.spId;
                    tplCtx.collection = "cms";
                    tplCtx.path = "allToRoot";
                    dyFObj.openForm( sectionDyf[superCms.kunik+"Params"],null, null);
                  });
                  $(".lineSeparator-change-icon").off().on("click",function() {
                    lineSeparatorIconId = $('.elem-control').data("id");
                    costumizer.icon.openIcon ("fa fa-"+superCms.sp_params[superCms.spId].config.css.icon.style)
                  });
                }
              // }
              // changeCssStyle(superCms.sp_params[superCms.spId].config);
              // $("."+superCms.kunik).addClass('customStyle'+superCms.kunik);
              superCms.setting.shadow()  
              superCms.setting.otherCss()  
              superCms.setting.objectFit()
              superCms.setting.otherClass() 
              superCms.setting.background()
              superCms.setting.borderType()
              superCms.setting.aos()
              superCms.setting.borderWidth()
              superCms.setting.borderColor()
              superCms.setting.elementSize()  
              superCms.setting.htmlInsertion()
              superCms.setting.borderMarginAndPadding() 
            }else{
              if($(".edit"+superCms.kunik+"Params").length > 0){
                $(".edit"+superCms.kunik+"Params").click();
              }
            }
          }
        });
        /* $(document).on("mouseenter",'.super-cms',function(e){
          if (costum.editMode == true) {
            var parent = $(this).parent(".super-cms");
            $(this).addClass("sp-hover");
            parent.removeClass("sp-hover");            
          }
          e.stopImmediatePropagation();
        }); */

        $(document).on("mouseleave",'.super-cms',function(e){
          if (costum.editMode == true && $("#toolsBar-edit-block").is(":hidden")) {
            var parent = $(this).parent(".super-cms");
            parent.addClass("sp-hover");
            $(this).removeClass("sp-hover");
          }
          e.stopImmediatePropagation();
        });
      },

      btnFunction : function () {
        
        $(document).on('click', '.btn-functionLink',function(e){
          e.stopImmediatePropagation();
          var link = $(this).attr("data-link");
          var external = $(this).attr("data-external");
          var type = $(this).attr("data-type");
          superCms.spId = $(this).data("id");
          var thisParent = superCms.sp_params[superCms.spId].parent;
          superCms.kunik = $(this).data("kunik");
          
          if(costum.editMode){

            if(superCms.sp_params[superCms.spId]){
              var thisParams = superCms.sp_params[superCms.spId].config;
              var thisParent = superCms.sp_params[superCms.spId].parent;
              superCms.pathConfig = JSON.parse(JSON.stringify(superCms.sp_params[superCms.spId].config));
              // superCms.parents = $("."+superCms.kunik).parent();
              // superCms.element = $("."+superCms.kunik).detach();
              // $("#supercms-overlay").append(superCms.element);
              // $("#supercms-overlay").css({"display": "block"});

              $(".super-cms").removeClass("in_edition");
              // $(".super-cms").addClass("editing");
             // $("."+superCms.kunik).removeClass("selected-mode");
              $(".btn-"+superCms.kunik).removeClass("editing");
              $(".btn-"+superCms.kunik).addClass("in_edition");
              mylog.log("Etokatra zao");

                   
              $("#toolsBar-edit-block").html("");
              $("#toolsBar-edit-block").show(); 
              $("#toolsBar-edit-block").html(superCms.menu.viewButton(thisParams,thisParent));
              $(".tab-content").append(superCms.menu.viewAdvanced(thisParams));
              $(".tab-content").append(superCms.menu.viewStyle(thisParams));

              if (thisParent != ""){
                $(".gotoparentBtn").css({"visibility": "visible"});  
              }

              if (thisParams.params.type == "lbh") {
                $( ".check-target-lbh" ).prop( "checked", true );
              }else{    
                $( ".check-target-lbh" ).prop( "checked", false );
              }
    
              if (thisParams.params.type == "blank") {
                $( ".check-target-blank" ).prop( "checked", true );
              }else{    
                $( ".check-target-blank" ).prop( "checked", false );
              }
              
              if (thisParams.params.type == "external") {
                $( ".check-target-external" ).prop( "checked", true );
              }else{    
                $( ".check-target-external" ).prop( "checked", false );
              }


              coInterface.initHtmlPosition();
              // changeCssStyle(superCms.sp_params[superCms.spId].config);
              // $("."+superCms.kunik).addClass('customStyle'+superCms.kunik);
              // $(".btn-"+superCms.kunik).addClass("customButtonStyle"+superCms.kunik);
              superCms.setting.shadow()  
              superCms.setting.otherCss()  
              superCms.setting.objectFit()
              superCms.setting.otherClass() 
              superCms.setting.background()
              superCms.setting.borderType()
              superCms.setting.aos()
              superCms.setting.borderWidth()
              superCms.setting.borderColor()
              superCms.setting.elementSize()  
              superCms.setting.htmlInsertion()
              superCms.setting.borderMarginAndPadding() 
               $(".sp-color-picker").spectrum({
                      type: "text",
                      showInput: "true",
                      showInitial: "true"
                    });
            }
          }else{

                if (!notEmpty(link) && !notEmpty(external)){
          
                  bootbox.alert({
                    message: `<br>
                    <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                    <p class="padding-left-20">L'URL de ce bouton est vide!</p>
                    </div>`
                    ,
                    size: 'medium'
                  });
              } else {

                if (type == "lbh"){
                  urlCtrl.loadByHash(link);
                } else if (type == "external") {
                  window.open(external, "_blank");
                } else {
                  window.open(link);
                }

              }
          }
        });
        // $(document).on('click', '.btn-save', function(e){
        //   var config = JSON.parse(JSON.stringify(superCms.pathConfig));
        //   removeAllBlankObjects(config);
        //   tplCtx = {};
        //   tplCtx.id = superCms.spId;
        //   tplCtx.collection = "cms";
        //   tplCtx.path = "allToRoot"; 
        //   tplCtx.value = config;
        //   dataHelper.path2Value( tplCtx, function(params) {
        //     superCms.sp_params[superCms.spId].config = JSON.parse(JSON.stringify(superCms.pathConfig));
        //     addStyleSave(superCms.sp_params[superCms.spId].config);
        //     // $("."+superCms.kunik).removeClass('customStyle'+superCms.kunik);
        //     // $(".icon-"+superCms.kunik).removeClass('customIconStyle'+superCms.kunik);
        //     $("#customIconStyle"+superCms.kunik).remove();
        //     $("#customStyle"+superCms.kunik).remove();
        //     toastr.success("Modification enrégistré");
        //   });
        //   e.stopImmediatePropagation();
        // });
      },

      texteEditFunction : function () {

        function String2Json(element, json) {
          var treeObject = {};

          if (typeof element === "string") {
            if (window.DOMParser) {
              parser = new DOMParser();
              docNode = parser.parseFromString(element,"text/xml");
            } else { 
              docNode = new ActiveXObject("Microsoft.XMLDOM");
              docNode.async = false;
              docNode.loadXML(element); 
            } 
            element = docNode.firstChild;
          }

          function treeHTML(element, object) {
            object["type"] = element.nodeName;
            var nodeList = element.childNodes;
            if (nodeList != null) {
              if (nodeList.length) {
                object["content"] = [];
                for (var i = 0; i < nodeList.length; i++) {
                  if (nodeList[i].nodeType == 3) {
                    object["content"].push(nodeList[i].nodeValue);
                  } else {
                    object["content"].push({});
                    treeHTML(nodeList[i], object["content"][object["content"].length -1]);
                  }
                }
              }
            }
            if (element.attributes != null) {
              if (element.attributes.length) {
                object["attributes"] = {};
                for (var i = 0; i < element.attributes.length; i++) {
                  object["attributes"][element.attributes[i].nodeName] = element.attributes[i].nodeValue;
                }
              }
            }
          }
          treeHTML(element, treeObject);

          return (json) ? treeObject : treeObject;
        }

        function isKeyExists(obj,key){
          if( obj[key] == undefined ){
            return false;
          }else{
            return true;
          }
        }

        $(document).on('keyup', '#block-name',function(e){
          var text = $(this).val();
          superCms.spId = $(this).data("id");
          tplCtx.id = superCms.spId ;
          tplCtx.collection = "cms" ;
          tplCtx.path = "name"; 
          tplCtx.value = text ;
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.name = text ;
          } );
          e.stopImmediatePropagation()  
        });


        var allText = $("#"+superCms.spId+"textShow").html();
        var p = document.createElement('p');
        p.innerHTML = allText;
        let links = p.querySelectorAll('a');
        links.forEach(x => {
          allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
        });
        $("#"+superCms.spId+"textShow").html(allText);

        $("."+superCms.spId+"font-size").change(function(e) {        
          var item=$(this);
          var val = (item.val())*1;
          $("#"+superCms.spId+"textShow").css("fontSize", val);
          e.stopPropagation();
        });


        $('.'+superCms.spId+'-text').click(function(event){
          event.stopPropagation();
        });

        $( "#"+superCms.spId+"textShow" ).blur(function(e) {
          text = $("#"+superCms.spId+"textShow").html();
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "text"; 
          tplCtx.value = text;

          json  = String2Json(text , true);

          function _isContains(json, value) {
            let contains = false;
            Object.keys(json).some(key => {
              contains = typeof json[key] === 'object' ? 
              _isContains(json[key], value) : json[key] === value;
              return contains;
            });
            return contains;
          }

          if (_isContains(json, "script")  == true ) {
            toastr.error("Script detecté");
          } else {
            dataHelper.path2Value( tplCtx, function(params) {       
              toastr.success("Modification enrégistré");});
          }

          e.stopPropagation();
        });


      },


      eltInsertion : function() {


        $(document).on("click",".add-type-cms",function(event){
          cmsBuilder.block.initView()
          localStorage.setItem("parentCmsIdForChild", superCms.spId);
          event.stopImmediatePropagation()
        });

        $(document).on("click", ".add-super-element",function(e){
          e.stopImmediatePropagation();
          var order = 0;
          var dataCostum = $(this).data("costum")     
          $(".empty-sp-element"+superCms.kunik).remove();   
          $(".whole-"+superCms.kunik).append('<div class="'+superCms.kunik+'-load-sp sp-is-loading" style="width: 100%;min-height: 30px;height: 100%;position: absolute;"></div>')
          if (typeof $(".whole-"+superCms.kunik+" .super-cms:first-child").css("order") !== "undefined") {
            order = $(".whole-"+superCms.kunik+" .super-cms:first-child").css("order")+1;
          }
          $("#toolsBar-edit-block").hide();
          coInterface.initHtmlPosition();
          $("#toolsBar-edit-block").html(``);
          var tplCtx = {};
          tplCtx.collection= "cms";
          tplCtx.value = {};
          tplCtx.value.path = $(this).data("path");
          tplCtx.value.name = $(this).data("name");
          tplCtx.value.page = page;
          tplCtx.value.order = order;
          tplCtx.value.subtype = $(this).data("subtype");;
          tplCtx.value.blockParent = superCms.spId;
          tplCtx.value.type = "blockChild";
          tplCtx.value.parent={};
          tplCtx.value.parent[costum.contextId] = {
            type : costum.contextType, 
            name : costum.contextName
          };
          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              var newChildId = params.saved.id;
              cmsChildren[superCms.spId].push(newChildId);
              upParent = {};
              upParent.id = superCms.spId;
              upParent.collection = "cms";
              upParent.path = "cmsList"; 
              upParent.value = cmsChildren[superCms.spId];
              dataHelper.path2Value( upParent, function(params) {
                // urlCtrl.loadByHash(location.hash);
                // var required = {
                //   "idblock" : superCms.spId ,
                //   "contextId" : contextId,
                //   "contextType" : contextType,
                //   "contextSlug" : contextSlug,
                //   "page" : page,
                //   "path" : path,

                // };
                // ajaxPost(
                //   null, 
                //   baseUrl+"/costum/blockcms/loadbloccms",
                //   required,
                //   function(data){
                  
                    // $(superCms.parents).append(superCms.element);
                    var id = $("."+superCms.kunik).parents(".custom-block-cms").data("id");
                    var path = $("."+superCms.kunik).parents(".custom-block-cms").data("path");
                    var kunik = $("."+superCms.kunik).parents(".custom-block-cms").data("kunik");
                    mylog.log("required",id,path,kunik)
                    // $("#supercms-overlay").empty();
                    // $("#supercms-overlay").css({"display": "none"});
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);

                //   } 
                // );
              });
            });      
          }
        });
        $(".stop-propagation").click(function(e) {
          e.stopPropagation();
        });
      },
      menuDisplay : function () {
        // $(document).on("click", ".edit-text", function(){   
        //   var html = $("#"+superCms.spId+"textShow").html();    
        //   superCms.spId = $(this).data("id");
        //   var text = $(".edit-text" ).val();
        //   mylog.log(html)
        //   $("#"+superCms.spId+"textShow  span").text(text);
        //   tplCtx = {};
        //   tplCtx.id = superCms.spId;
        //   tplCtx.collection = "cms";
        //   tplCtx.path = "html"; 
        //   tplCtx.value = valValid;

        //     dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );  
        // });
      },

      $thisChildren : [],
      createLayer : function(layer) {
        var spLayer = "";
        $.each(layer, function() {
          if(jQuery.inArray($(this).data('id'), superCms.manage.$thisChildren) == -1) {
           superCms.manage.$thisChildren.push($(this).data('id'))
           if ($(this).data('id') != undefined) {
            spLayer += `<li style="cursor:pointer" class="sp-layer spCustomBtn" data-kunik="${$(this).data('kunik')}" data-id='${$(this).data('id')}'>${$(this).data('name')}`

            spLayer += `<ul>`
            spLayer +=  superCms.manage.createLayer($(this).find(".super-cms"))

            spLayer += `</ul>`  

            spLayer += `</li>`  
          }
        }

      })
        return spLayer;
      },

      spRoot : function(eltToShow) {  
        if (costum.editMode) {
          $("#cms-layer-Bar").fadeIn();   
          setTimeout(function(){
            $("#cms-layer-Bar").html(`
             <div class="text-center tool-scms-title cursor-move">
             <i style="cursor:default;font-size: large;" aria-hidden="true">Couches</i>
             <i class="fa fa-window-close closeBtn view-super-cms" data-target="cms-layer-Bar" aria-hidden="true"></i>
             </div><div><ul class="sp-cms-tree">${superCms.manage.createLayer(eltToShow)}</ul></div>`);  
            superCms.manage.$thisChildren = []         

            $(".sp-layer").hover(
              function (e) {
                var $this = $(this);
                var $thisLayer = $("."+$this.data("kunik"));
                if (costum.editMode == false) {
                  var parent = $thisLayer.parent(".super-cms");
                  $thisLayer.removeClass("editing");            
                  superCms.kunik = $thisLayer.data("kunik");
                  superCms.spId = $thisLayer.data("id");
                  //$thisLayer.addClass("selected-mode");
                  // parent.addClass("editing");
                  //parent.removeClass("selected-mode");
                }
                e.stopImmediatePropagation();
              },
              function(e) {
                var $this = $(this);
                var $thisLayer = $("."+$this.data("kunik"));
                if (costum.editMode == false) {
                  $thisLayer.children(".sp-settings-btn").hide();
                  // $thisLayer.addClass("editing");
                  //$thisLayer.removeClass("selected-mode");
                }
                e.stopImmediatePropagation();
              }
              )       
          },10)
        }
      }
    },
 

    setting : {
      objectFit : function () {
        $(document).on("change", "#objfit",function(e) {
          var myfit = $( "#objfit" ).val();
          $("."+$(this).data("kunik") ).css("object-fit",myfit);
          
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.object-fit"; 
          tplCtx.value = myfit;
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['object-fit'] = myfit;
          } );
          e.stopImmediatePropagation()
        });
      },
      aos : function(){
        $(document).on("change", "#aos",function(e) {

          var myaos = $( "#aos" ).val();
          $("."+$(this).data("kunik") ).attr("data-aos",myaos);
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.aos.type"; 
          tplCtx.value = myaos;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['aos'] = superCms.pathConfig.css['aos'] || Object();
            superCms.sp_params[superCms.spId].config.css['aos']['type'] = myaos;
          } );
          e.stopImmediatePropagation()
        }); 
        $(document).on("change", "#aosDuration",function(e) {

          var aosDuration = $( "#aosDuration" ).val();
          $("."+$(this).data("kunik") ).attr("data-aos-duration",aosDuration);
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.aos.duration"; 
          tplCtx.value = aosDuration;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['aos'] = superCms.pathConfig.css['aos'] || Object();
            superCms.sp_params[superCms.spId].config.css['aos']['duration'] = aosDuration;
          } );
          e.stopImmediatePropagation()
        });
      },
      background : function() {
        $(document).on("change", "#fond-color", function(e) {
          if(!superCms.kunik.startsWith("button")){
            $("."+superCms.kunik ).css("background-color",$("#fond-color").val());
          }
          $(".icon-"+superCms.kunik ).css("background-color",$("#fond-color").val());
          $(".btn-"+superCms.kunik ).css("background-color",$("#fond-color").val());
          $(".this-content-"+superCms.kunik ).css("background-color",$("#fond-color").val());
          $(".img-"+superCms.kunik ).css("background-color",$("#fond-color").val());
          // superCms.pathConfig.css['background'] = superCms.pathConfig.css['background'] || Object();
          // superCms.pathConfig.css['background']['color'] = $("#fond-color").val();
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.background.color"; 
          tplCtx.value = $("#fond-color").val();
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css.background.color = $("#fond-color").val();
            toastr.success("Modification enrégistré");
          } );
          e.stopImmediatePropagation()
        });
        $(document).on("change", "#hover-fond-color", function(e) {
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.hover.background.color"; 
          tplCtx.value = $("#fond-color").val();
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css.hover.background.color = $("#hover-fond-color").val();
            toastr.success("Modification enrégistré");
          } );
          e.stopImmediatePropagation()
        });
        $( "#check-bgTarget" ).change(function() {
          tplCtx = {};
          if ($( "#check-bgTarget" ).is( ":checked" ) == true) { 
            tplCtx.value = "this"+page;
            superCms.bgTarget = "this"+page;
            if(typeof superCms.sp_params[superCms.spId].config.initImage == "object" && typeof superCms.sp_params[superCms.spId].config.initImage[0] != "undefined" && typeof superCms.sp_params[superCms.spId].config.initImage[0].imagePath != "undefined"){
              $("#all-block-container").css("background-image","url("+superCms.sp_params[superCms.spId].config.initImage[0].imagePath+")");
            }else{
              $("#all-block-container").css("background-color",superCms.sp_params[superCms.spId].config.css.background.color);
            }            
            $("."+superCms.kunik ).css("background-image","url()");
          }else{    
            tplCtx.value = " ";
            superCms.bgTarget = " ";
            $("#all-block-container").css("background-image","url()");
            $("#all-block-container").css("background-color","transparent");
            if(typeof superCms.sp_params[superCms.spId].config.initImage == "object" && typeof superCms.sp_params[superCms.spId].config.initImage[0] != "undefined" && typeof superCms.sp_params[superCms.spId].config.initImage[0].imagePath != "undefined"){
               $("."+superCms.kunik ).css("background-image","url("+superCms.sp_params[superCms.spId].config.initImage[0].imagePath+")");
            }
            
          }
         
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.background.target"; 
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['background'] = superCms.sp_params[superCms.spId].config.css['background'] || Object();
            superCms.sp_params[superCms.spId].config.css['background']['target'] = superCms.bgTarget;
          } );
          
        });
        $(document).on("change", "#bgSize",function(e) {
          var mybgSize = $( "#bgSize" ).val();
          $("."+superCms.kunik ).css("background-size",mybgSize);
        
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.background.size"; 
          tplCtx.value = mybgSize;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['background'] = superCms.sp_params[superCms.spId].config.css['background'] || Object();
            superCms.sp_params[superCms.spId].config.css['background']['size'] = mybgSize;
  
          } );
          e.stopImmediatePropagation()
        });
        $(document).on("change", "#bgPosition",function(e) {
          var mybgPosition = $( "#bgPosition" ).val();
          $("."+superCms.kunik ).css("background-position",mybgPosition);
        
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.background.position"; 
          tplCtx.value = mybgPosition;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['background'] = superCms.sp_params[superCms.spId].config.css['background'] || Object();
            superCms.sp_params[superCms.spId].config.css['background']['position'] = mybgPosition;
  
          } );
          e.stopImmediatePropagation()
        }); 

        $(document).on("change", "#bgRepeat",function(e) {
          var mybgRepeat = $( "#bgRepeat" ).val();
          $("."+superCms.kunik ).css("background-repeat",mybgRepeat);
        
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.background.repeat"; 
          tplCtx.value = mybgRepeat;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['background'] = superCms.sp_params[superCms.spId].config.css['background'] || Object();
            superCms.sp_params[superCms.spId].config.css['background']['repeat'] = mybgRepeat;
  
          } );
          e.stopImmediatePropagation()
        }); 

        $(document).on("change", "#flexDirection",function(e) {
          var flexDirection = $( "#flexDirection" ).val();
          $("."+superCms.kunik ).css("flex-direction",flexDirection);
        
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.flex-direction"; 
          tplCtx.value = flexDirection;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['flex-direction'] = flexDirection;
          } );
          e.stopImmediatePropagation()
        }); 

        $(document).on("change", "#flexWrap",function(e) {
          var flexWrap = $( "#flexWrap" ).val();
          $("."+superCms.kunik ).css("flex-wrap",flexWrap);
        
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.flex-wrap"; 
          tplCtx.value = flexWrap;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['flex-wrap'] = flexWrap;
          } );
          e.stopImmediatePropagation()

        }); 
        
        $(document).on("change", "#contentAlign",function(e) {
          var contentAlign = $( "#contentAlign" ).val();
          $("."+superCms.kunik ).css("align-content",contentAlign);
        
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.content-align"; 
          tplCtx.value = contentAlign;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['content-align'] = contentAlign;
          } );
          e.stopImmediatePropagation()
        }); 

        $(document).on("change", "#contentJustify",function(e) {
          var contentJustify = $( "#contentJustify" ).val();
          $("."+superCms.kunik ).css("justify-content",contentJustify);
        
          tplCtx = {};
          tplCtx.id =superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.content-justify"; 
          tplCtx.value = contentJustify;

          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['content-justify'] = contentJustify;
          } );
          e.stopImmediatePropagation()
        }); 


        

      },
      borderType : function(){
        $(document).on("change", "#borderType",function(e) {
          var borderType = $( "#borderType" ).val();
          if(superCms.kunik.startsWith("button")){
            $(".btn-"+superCms.kunik).css("border-style",borderType);
          }else{
            $("."+superCms.kunik).css("border-style",borderType);
          }
         

          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.border.type"; 
          tplCtx.value = borderType;
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css['border']['type'] = borderType;
          } );
          e.stopImmediatePropagation()
        });
        $(document).on("change", "#hoverBorderType",function(e) {
          var borderType = $( "#hoverBorderType" ).val();
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.hover.border.type"; 
          tplCtx.value = borderType;
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css.hover['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css.hover['border']['type'] = borderType;
          } );
          e.stopImmediatePropagation()
        });
        $( "#check-border-top" ).change(function() {
          tplCtx = {};

          superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
          if ($( "#check-border-top" ).is( ":checked" ) == true) { 
            tplCtx.value = "checked";
            superCms.borderTop = "checked";
            // $("."+$(this).data("kunik") ).css("border-top",superCms.borderWidth +"px "+superCms.borderType+" "+superCms.borderColor);
            if(superCms.kunik.startsWith("button")){
              $(".btn-"+superCms.kunik).css("border-top",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }else{
              $("."+superCms.kunik).css("border-top",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }
          }else{    
            tplCtx.value = " ";
            superCms.borderTop = " ";
          }
          

          // changeCssStyle(superCms.pathConfig);
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.border.top"; 
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css['border']['top'] = superCms.borderTop;
          } );
          
        });
        $( "#check-border-bottom" ).change(function() {
          if ($( "#check-border-bottom" ).is( ":checked" ) == true) {  
            tplCtx.value = "checked";    
            superCms.borderBottom = "checked";
            // $("."+$(this).data("kunik") ).css("border-bottom",superCms.borderWidth +"px "+superCms.borderType+" "+superCms.borderColor);
            if(superCms.kunik.startsWith("button")){
              $(".btn-"+superCms.kunik).css("border-bottom",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }else{
              $("."+superCms.kunik).css("border-bottom",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }
          }else{
            tplCtx.value = " ";  
            superCms.borderBottom = " ";
          }
       
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.border.bottom"; 
          tplCtx.value = superCms.borderBottom;
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css['border']['bottom'] = superCms.borderBottom;
          } );
          
        });
        $( "#check-border-left" ).change(function() {
          if ($( "#check-border-left" ).is( ":checked" ) == true) {  
            tplCtx.value = "checked";      
            superCms.borderLeft= "checked";
            // $("."+$(this).data("kunik") ).css("border-left",superCms.borderWidth +"px "+superCms.borderType+" "+superCms.borderColor);
            if(superCms.kunik.startsWith("button")){
              $(".btn-"+superCms.kunik).css("border-left",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }else{
              $("."+superCms.kunik).css("border-left",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }
          }else{
            tplCtx.value = "";  
            superCms.borderLeft = " ";
          }
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.border.left"; 
          tplCtx.value = superCms.borderLeft;
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css['border']['left'] = superCms.borderLeft;
          // changeCssStyle(superCms.pathConfig);
          } );
          
        });
        $( "#check-border-right" ).change(function() {
          if ($( "#check-border-right" ).is( ":checked" ) == true) {   
            tplCtx.value = "checked";     
            superCms.borderRight= "checked";
            // $("."+$(this).data("kunik") ).css("border-right",superCms.borderWidth +"px "+superCms.borderType+" "+superCms.borderColor);
            if(superCms.kunik.startsWith("button")){
              $(".btn-"+superCms.kunik).css("border-right",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }else{
              $("."+superCms.kunik).css("border-right",superCms.sp_params[superCms.spId].config.css['border'].width +"px "+superCms.borderType+" "+superCms.sp_params[superCms.spId].config.css['border'].color);
            }
          }else{
            tplCtx.value = "";  
            superCms.borderRight = " ";
          }
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.border.right"; 
          tplCtx.value = superCms.borderRight;
  
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css['border']['right'] = superCms.borderRight;
          } );
       
          // changeCssStyle(superCms.pathConfig);
        });
      },
      borderWidth : function(){

        $(document).on('change', ".border-width", function(e){
          var borderWidth = $(this).val();
          if(superCms.kunik.startsWith("button")){
            $(".btn-"+$(this).data("kunik") ).css("border-"+$(this).data('given')+"-width",borderWidth+"px");
          }else{
            $("."+$(this).data("kunik") ).css("border-"+$(this).data('given')+"-width",borderWidth+"px");
          }
          // superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
          // superCms.sp_params[superCms.spId].config.css['border'][$(this).data('given')] = borderWidth;
          e.stopImmediatePropagation();
        });
        $(document).on("change", "#border-width",function(e) {
          var borderWidth = $( "#border-width" ).val();
          if(superCms.kunik.startsWith("button")){
            $(".btn-"+$(this).data("kunik") ).css("border-width",borderWidth+"px");
          }else{
            $("."+$(this).data("kunik") ).css("border-width",borderWidth+"px");
          }
          $(".element-value ."+$(this).data('what')).val(borderWidth);
          $(".border-width").change();
          // tplCtx = {};
          // tplCtx.id = superCms.spId;
          // tplCtx.collection = "cms";
          // tplCtx.path = "css.border.width"; 
          // tplCtx.value = borderWidth;
  
          // dataHelper.path2Value( tplCtx, function(params) {} );
          // superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
          // superCms.sp_params[superCms.spId].config.css['border']['width'] = borderWidth;
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()
        });
        $(document).on('change', ".hoverborder-width", function(e){
          var borderWidth = $(this).val();
          superCms.sp_params[superCms.spId].config.css.hover['border'] = superCms.sp_params[superCms.spId].config.css.hover['border'] || Object();
          superCms.sp_params[superCms.spId].config.css.hover['border'][$(this).data('given')] = borderWidth;
          e.stopImmediatePropagation();
        });
        $(document).on("change", "#hover-border-width",function(e) {
          var borderWidth = $( "#hover-border-width" ).val();
          $(".border-width").change();
          // tplCtx = {};
          // tplCtx.id = superCms.spId;
          // tplCtx.collection = "cms";
          // tplCtx.path = "css.border.width"; 
          // tplCtx.value = borderWidth;
  
          // dataHelper.path2Value( tplCtx, function(params) {} );
          superCms.sp_params[superCms.spId].config.css.hover['border'] = superCms.sp_params[superCms.spId].config.css.hover['border'] || Object();
          superCms.sp_params[superCms.spId].config.css.hover['border']['width'] = borderWidth;
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()
        }); 

        $(document).on("blur", "#border-width, .border-width", function(e){
          saveWidth();
          e.stopImmediatePropagation();
        }); 
        $(document).on("blur", "#hover-border-width, .hover-border-width", function(e){
          saveHoverWidth();
          e.stopImmediatePropagation();
        });
        
      },
      borderColor : function() {
        $(document).on("change","#border-color",function(e) {
          if(superCms.kunik.startsWith("button")){
            $(".btn-"+superCms.kunik ).css("border-color",$("#border-color").val());
          }else{
            $("."+superCms.kunik ).css("border-color",$("#border-color").val());
          }
          
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.border.color"; 
          tplCtx.value = $("#border-color").val();

          dataHelper.path2Value( tplCtx, function(params) {
             toastr.success("Modification enrégistré");
             superCms.sp_params[superCms.spId].config.css.border.color = $("#border-color").val();
             superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
             superCms.sp_params[superCms.spId].config.css['border']['color'] = $("#border-color").val();
          } );
          e.stopImmediatePropagation()
        });
        $(document).on("change","#hover-border-color",function(e) {
          // changeCssStyle(superCms.pathConfig);
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.hover.border.color"; 
          tplCtx.value = $("#hover-border-color").val();

          dataHelper.path2Value( tplCtx, function(params) {
             toastr.success("Modification enrégistré");
             superCms.sp_params[superCms.spId].config.css.hover['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
             superCms.sp_params[superCms.spId].config.css.hover['border']['color'] = $("#hover-border-color").val();
          } );
          e.stopImmediatePropagation()
        });
      },
      borderMarginAndPadding : function() {
        var thisetting = {};
        $(document).on('keyup change',".border-control, .padding-control, .margin-control", function (e){
          $('.elem-value .'+$(this).data("target")).each(function(k,v){
            pars   = $(this).val() ? parseInt($(this).val()) : '0';
            thisIs = $(this).data("given");
            var dataWhat = $(this).data("what");
            $(this).val(pars);
            thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
            if(dataWhat.indexOf('hover') < 0){
              if(superCms.kunik.startsWith("button")){
                $(".btn-"+superCms.kunik).css(thisIs,pars+"px");
              }else{
                $("."+superCms.kunik).css(thisIs,pars+"px");
              }
            }
            // superCms.sp_params[superCms.spId].config[thisIs.replace(/-/g,"")] = pars
            if (thisIs.match("border")) {
              
              superCms.sp_params[superCms.spId].config.css.border.radius[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
            }else if(thisIs.match("padding")){
              superCms.sp_params[superCms.spId].config.css.padding[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
            }else if(thisIs.match("margin")){
              superCms.sp_params[superCms.spId].config.css.margin[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
            }
            // changeCssStyle(superCms.pathConfig);
            // Mbola hodinihina
          })
          e.stopImmediatePropagation()
        })

        $(document).on('keyup change',".elem-control", function (e){
          globalConf = $(this).val() ? parseInt($(this).val()) : '0';
          $("."+$(this).data("what")).val(globalConf);
          var dataWhat = $(this).data("what");
          $('.elem-value .'+$(this).data("what")).each(function(k,v){
            pars   = $(this).val() ? parseInt($(this).val()) : '0';
            thisIs = $(this).data("given");
            // if($(this).data("given").indexOf("border") > -1){
            //   if(superCms.kunik.startsWith("button")){
            //     $(".btn"+superCms.kunik).css($(this).data("given"),globalConf+"px");
            //   }else{
            //     $("."+superCms.kunik).css($(this).data("given"),globalConf+"px");
            //   }
            //   superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            //   superCms.sp_params[superCms.spId].config.css['border']['radius'] = JSON.parse(JSON.stringify(thisetting));
            // }else{
            //   superCms.sp_params[superCms.spId].config.css[$(this).data('path')] = JSON.parse(JSON.stringify(thisetting));
            //   $("."+superCms.kunik).css($(this).data("given"),globalConf+"px");
            // }
            // $("."+superCms.kunik).css(thisIs,pars+"px");
            $(this).val(pars);
            mylog.log("elem", thisIs)
            mylog.log("elem k", v)
            thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
            if (thisIs.match("border")) {
              // superCms.sp_params[superCms.spId].config.borderRadius = globalConf;
              // superCms.sp_params[superCms.spId].config.bordertopleftradius = globalConf;
              // superCms.sp_params[superCms.spId].config.bordertoprightradius = globalConf;
              // superCms.sp_params[superCms.spId].config.borderbottomrightradius = globalConf;
              // superCms.sp_params[superCms.spId].config.borderbottomleftradius = globalConf;
              superCms.sp_params[superCms.spId].config.css.border.radius["top-left"] = globalConf;
              superCms.sp_params[superCms.spId].config.css.border.radius["top-right"] = globalConf;
              superCms.sp_params[superCms.spId].config.css.border.radius["bottom-left"] = globalConf;
              superCms.sp_params[superCms.spId].config.css.border.radius["bottom-right"] = globalConf;

            }else if(thisIs.match("padding")){
              // superCms.sp_params[superCms.spId].config.padding = globalConf;
              // superCms.sp_params[superCms.spId].config.paddingtop = globalConf;
              // superCms.sp_params[superCms.spId].config.paddingright = globalConf;
              // superCms.sp_params[superCms.spId].config.paddingleft = globalConf;
              // superCms.sp_params[superCms.spId].config.paddingbottom = globalConf;
              superCms.sp_params[superCms.spId].config.css.padding.top = globalConf;
              superCms.sp_params[superCms.spId].config.css.padding.left = globalConf;
              superCms.sp_params[superCms.spId].config.css.padding.right = globalConf;
              superCms.sp_params[superCms.spId].config.css.padding.bottom = globalConf;
            }else if(thisIs.match("margin")){
              // superCms.sp_params[superCms.spId].config.margin = globalConf;
              // superCms.sp_params[superCms.spId].config.marginright = globalConf;
              // superCms.sp_params[superCms.spId].config.margintop = globalConf;
              // superCms.sp_params[superCms.spId].config.marginleft = globalConf;
              // superCms.sp_params[superCms.spId].config.marginbottom = globalConf;
              superCms.sp_params[superCms.spId].config.css.margin.top = globalConf;
              superCms.sp_params[superCms.spId].config.css.margin.left = globalConf;
              superCms.sp_params[superCms.spId].config.css.margin.right = globalConf;
              superCms.sp_params[superCms.spId].config.css.margin.bottom = globalConf;
            }
          })
          if(dataWhat.indexOf('hover') < 0){
            if(superCms.kunik.startsWith("button")){
              $(".btn-"+superCms.kunik).css($(this).data("given"),globalConf+"px");
            }else{
              $("."+superCms.kunik).css($(this).data("given"),globalConf+"px");
            }
          }
          if($(this).data("given").indexOf("border") > -1){
            superCms.sp_params[superCms.spId].config.css['border'] = superCms.sp_params[superCms.spId].config.css['border'] || Object();
            superCms.sp_params[superCms.spId].config.css['border']['radius'] = JSON.parse(JSON.stringify(thisetting));
          }else{
            superCms.sp_params[superCms.spId].config.css[$(this).data('path')] = JSON.parse(JSON.stringify(thisetting));
          }
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()
        });

        $(document).on("blur",".elem-control, .elem-value .form-control",function(e) {
          
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css."+$(this).data("path"); 
          tplCtx.value = thisetting;

          if (notEmpty(thisetting)){
            dataHelper.path2Value( tplCtx, function(params) {
              thisetting = {};
              toastr.success("Modification enrégistré");
            } );
          }
          e.stopImmediatePropagation()
        });
      },

      otherClass : function() {
        $(document).on('keyup', ".other-class", function (e){
          var valValid = superCms.setting.validateScript(".other-class", "<p class='text-red bg-white'>Oops! 🤯<br>Invalid class!</p>");
          if (valValid !== false) {
           if($(this).data('kunik').startsWith('button')){
            $(".btn-"+$(this).data("kunik")).removeClassExcept("btn-functionLink bs btn-"+$(this).data("kunik")+" other-css-"+$(this).data("kunik")+" newCss-"+$(this).data("kunik"));
            $(".btn-"+$(this).data("kunik")).addClass(valValid);
           }else{
            $("."+$(this).data("kunik")).removeClassExcept("sp-cms-container "+$(this).data("kunik")+" super-cms other-css-"+$(this).data("kunik")+" newCss-"+$(this).data("kunik")+" sp-cms-10 sp-cms-20 sp-cms-30 sp-cms-40 sp-cms-50 sp-cms-120 sp-cms-120 sp-cms-80 sp-cms-90 sp-cms-100 super-container sp-cms-std");
            $("."+$(this).data("kunik")).addClass(valValid);
           }
           superCms.sp_params[superCms.spId].config.class.other = valValid;
         }    
         e.stopImmediatePropagation()
       });

        $(document).on('blur',".other-class", function (e){
          var valValid = superCms.setting.validateScript(".other-class", "<p class='text-red bg-white'>Oops! 🤯<br>Invalid class!</p>");
          if (valValid !==false) {
            tplCtx = {};
            tplCtx.id = $(this).data("id");
            tplCtx.collection = "cms";
            tplCtx.path = "class.other"; 
            tplCtx.value = valValid;
            dataHelper.path2Value( tplCtx, function(params) { 
              toastr.success("Modification enrégistré");
              superCms.sp_params[superCms.spId].config.class.other = valValid;
            } );
                      e.stopImmediatePropagation()
          }  
        });
      },
      otherCss : function() {
        /****************Other css option***************
         * Create css class then insert it into target element
         * **********************************************/
         $(document).on('keyup',".other-css", function (e){ 
           $("."+$(this).data("kunik")).removeClass("newCss-"+$(this).data("kunik")); 
           $("."+$(this).data("kunik")).removeClass("other-css-"+$(this).data("kunik"));
           $("#myStyleId"+$(this).data("kunik")).remove();
           superCms.sp_params[superCms.spId].config.css.other = $(".other-css" ).val()
           var element  = document.createElement("style");
           element.id = "myStyleId"+$(this).data("kunik") ;
           element.innerHTML = ".newCss-"+$(this).data("kunik")+" {"+superCms.sp_params[superCms.spId].config.css.other+"}" ;
           var header = document.getElementsByTagName("HEAD")[0] ;
           header.appendChild(element) ;
           $("."+$(this).data("kunik")).addClass("newCss-"+$(this).data("kunik")); 
           e.stopImmediatePropagation()
         });

         $(document).on('blur',".other-css", function (e){
          tplCtx = {};
          tplCtx.id = $(this).data("id");
          tplCtx.collection = "cms";
          tplCtx.path = "css.other"; 
          tplCtx.value = superCms.sp_params[superCms.spId].config.css.other;
          dataHelper.path2Value( tplCtx, function(params) { 
            toastr.success("Modification enrégistré");
            superCms.sp_params[superCms.spId].config.css['other'] =  $(this).val();
        } );
         
          e.stopImmediatePropagation()
        });
         /*************End other css option****************/
       },
       shadow : function() {
         if (lineSeparator == "true") {
          $( "#check-lineSeparator" ).prop('checked', true);
        }

        $( "#check-lineSeparator" ).change(function() {
          if ($( "#check-lineSeparator" ).is( ":checked" ) == true) {      
            lineSeparator = "true";
            $('.whole-'+superCms.kunik).append(`<hr class="hr-${superCms.kunik}" style="width:40%; margin:20px auto; border: 1px dashed #6CC3AC;">`)
          }else{
            lineSeparator = "";
            $(".hr-"+superCms.kunik).remove();

          }
        
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.separator"; 
          tplCtx.value = lineSeparator;
          dataHelper.path2Value( tplCtx, function(params) { 
            toastr.success("Modification enrégistré");
            superCms.sp_params[superCms.spId].config.css['separator'] = lineSeparator;
        } );
        });

        $( "#check-inset" ).change(function() {
          if ($( "#check-inset" ).is( ":checked" ) == true) {      
            superCms.inset = "inset";
          }else{
            superCms.inset = "";
          }
          $("."+superCms.kunik ).css('box-shadow', superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
          saveShadow();
          // changeCssStyle(superCms.pathConfig);
        });
        $( "#hover-check-inset" ).change(function() {
          if ($( "#hover-check-inset" ).is( ":checked" ) == true) {      
            superCms.hoverInset = "inset";
          }else{
            superCms.hoverInset = "";
          }
          saveHoverShadow();
          // changeCssStyle(superCms.pathConfig);
        });


        $(document).on("icon-changed", function(e){
          $('.icon-lineSeparator'+superCms.spId).removeClass(function (index, css) {
            return (css.match (/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
          });
          $('.icon-lineSeparator'+superCms.spId).addClass("fa fa-"+costumizer.icon.selected)
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.style"; 
          tplCtx.value = costumizer.icon.selected;
          dataHelper.path2Value( tplCtx, function(params) { 
            toastr.success("Modification enrégistré");
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['style'] = costumizer.icon.selected;
        } );

         
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()      
        });


        $('.line-separator-icon-size').mousemove(function(){
          fontSize = $('.line-separator-icon-size').val();
          $(".range-blur__value").html("Taille "+fontSize+"px");
          $(".icon-"+superCms.kunik ).css("font-size",fontSize+"px");
        });

        $('.block-width').mousemove(function(){
          width = $('.block-width').val();
          $(".range-width__value").html("Longueur "+width+"%");
          if(superCms.kunik.startsWith("image")){
            $(".img-"+superCms.kunik ).css("width",width+"%");
          }else if(superCms.kunik.startsWith("button")){
            $(".btn-"+superCms.kunik ).css("width",width+"%");
          }else{
            $("."+superCms.kunik ).css("width",width+"%");
          }
        });

        $('.block-height').mousemove(function(){
          height = $('.block-height').val();
          var cssHeight = 0;
          if(height == 0){
            cssHeight = "auto";
          }else{
            cssHeight = height+"px";
          }
          $(".range-heigth__value").html("Hauteur "+height+"px");
          if(superCms.kunik.startsWith("button")){
            $(".btn-"+superCms.kunik ).css("height",cssHeight);
          }else if(superCms.kunik.startsWith("lineSeparator")){
            $("."+superCms.kunik ).css("height",height+"px");
          }else if(superCms.kunik.startsWith("image")){
            $(".img-"+superCms.kunik ).css("height",cssHeight);
          }else{
            $("."+superCms.kunik ).css("height",cssHeight);
          }
        });

        $('.block-margin-top').mousemove(function(){
          iconMarginTop = $(this).val();
          $(".range-top__value").html("Vertical "+iconMarginTop+"%");
          $(".icon-"+superCms.kunik ).css("margin-top",iconMarginTop+"%");
        });

        $('.block-margin-left').mousemove(function(){
          iconMarginLeft = $(this).val();
          $(".range-left__value").html("Horizontal "+iconMarginLeft+"%");
          $(".icon-"+superCms.kunik ).css("margin-left",iconMarginLeft+"%");
        });

        $('.block-margin-right').mousemove(function(){
          iconMarginRight = $(this).val();
          $(".  ").html("Droite "+iconMarginRight+"%");
          $(".icon-"+superCms.kunik ).css("margin-right",iconMarginRight+"%");
        });

        $('.block-margin-bottom').mousemove(function(){
          iconMarginBottom = $(this).val();
          $(".range-top__value").html("Bas "+iconMarginBottom+"%");
          $(".icon-"+superCms.kunik ).css("margin-bottom",iconMarginBottom+"%");
        });

        $(document).on('mouseup','.block-width',function(e){
          tplCtx = {};
          tplCtx.id =  tplCtx.id = superCms.spId;;
          tplCtx.collection = "cms";
          tplCtx.path = "css.size.width"; 
          tplCtx.value = width+"%";
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['size'] = superCms.sp_params[superCms.spId].config.css['size'] || Object();
            superCms.sp_params[superCms.spId].config.css['size']['width'] = width+'%';
            toastr.success("Modification enrégistré");
        } );

         
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()   
        });

        $(document).on('mouseup','.block-height',function(e){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.size.height"; 
          if(superCms.kunik.startsWith("lineSeparator")){
            tplCtx.value = height+"px";
          }else{
            if(height == 0){
              tplCtx.value = "auto";
            }else{
              tplCtx.value = height+"px";
            }
          }
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['size'] = superCms.sp_params[superCms.spId].config.css['size'] || Object();
            superCms.sp_params[superCms.spId].config.css['size']['height'] = height;
            toastr.success("Modification enrégistré");
        } );
        
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()   
        });


        

        $(document).on('mouseup','.block-margin-top',function(e){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.margin.top"; 
          tplCtx.value = iconMarginTop;
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin'] = superCms.sp_params[superCms.spId].config.css['icon']['margin'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin']['top'] = iconMarginTop;
            toastr.success("Modification enrégistré");
        } );
         
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()   
        });

        $(document).on('mouseup','.block-margin-left',function(e){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.margin.left"; 
          tplCtx.value = iconMarginLeft;
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin'] = superCms.sp_params[superCms.spId].config.css['icon']['margin'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin']['left'] = iconMarginLeft;
            toastr.success("Modification enrégistré");
        } );
       
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()   
        });

        $(document).on('mouseup','.block-margin-right',function(e){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.margin.right"; 
          tplCtx.value = iconMarginRight;
          dataHelper.path2Value( tplCtx, function(params) {
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin'] = superCms.sp_params[superCms.spId].config.css['icon']['margin'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin']['right'] = iconMarginRight;
            toastr.success("Modification enrégistré");
        } );

       
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()   
        });

        $(document).on('mouseup','.block-margin-bottom',function(e){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.margin.bottom"; 
          tplCtx.value = iconMarginBottom;
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin'] = superCms.sp_params[superCms.spId].config.css['icon']['margin'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['margin']['bottom'] = iconMarginBottom;
          // changeCssStyle(superCms.pathConfig);
            toastr.success("Modification enrégistré");
          } );
          
          e.stopImmediatePropagation()   
        });


        
        $(document).on('mouseup', '.line-separator-icon-size',function(e){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.size"; 
          tplCtx.value = fontSize;

          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['size'] = fontSize;
            toastr.success("Modification enrégistré");
        } );
       
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()   
        });

        $(document).on("change", "#icon-color", function(e) {
          $(".icon-"+superCms.kunik ).css("color",$("#icon-color").val());
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.color"; 
          tplCtx.value = $("#icon-color").val();
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['color'] = $("#icon-color").val();
            toastr.success("Modification enrégistré");
        } );
      
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()
        });

        $(document).on("change", "#icon-background-color", function(e) {
          $(".icon-"+superCms.kunik ).css("background-color",$("#icon-background-color").val());
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.icon.background.color"; 
          tplCtx.value = $("#icon-background-color").val();
          dataHelper.path2Value( tplCtx, function(params) { 
            superCms.sp_params[superCms.spId].config.css['icon'] = superCms.sp_params[superCms.spId].config.css['icon'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['background'] = superCms.sp_params[superCms.spId].config.css['icon']['background'] || Object();
            superCms.sp_params[superCms.spId].config.css['icon']['background']['color'] = $("#icon-background-color").val();
            toastr.success("Modification enrégistré");
        } );
         
          // changeCssStyle(superCms.pathConfig);
          e.stopImmediatePropagation()
        });

        // /********************Button function**********************/
       
                /***Link & text***/
        $( ".super-btn-link" ).blur(function() {
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "params"; 
            tplCtx.value = {};
            tplCtx.value.url = $( ".super-btn-link" ).val();
            tplCtx.value.type = $( ".super-btn-link" ).val();

            dataHelper.path2Value( tplCtx, function(params) { 
              superCms.sp_params[superCms.spId].config.params['url'] = $( ".super-btn-link" ).val();
              superCms.sp_params[superCms.spId].config.params['type'] = $( ".super-btn-link" ).val();
              toastr.success("Modification enrégistré");
            } );
           
          
          });

          $( ".super-btn-external" ).blur(function() {
           
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "params"; 
            tplCtx.value = {};
            tplCtx.value.external = $( ".super-btn-external" ).val();
            tplCtx.value.type = "external";

            dataHelper.path2Value( tplCtx, function(params) {
                superCms.sp_params[superCms.spId].config.params['external'] = $( ".super-btn-external" ).val();
                superCms.sp_params[superCms.spId].config.params['type'] = "external";
                toastr.success("Modification enrégistré");
              } );
          });

          $(".super-btn-text" ).on('keyup', function (){
            $(".btn-button"+superCms.spId).text($(".super-btn-text" ).val());
          });

          $("#font-color").on('change', function(){
          
            $(".btn-"+superCms.kunik).css('color', $(this).val());
            $("."+superCms.kunik).css('color', $(this).val());
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "css.color"; 
            tplCtx.value = $(this).val();

            dataHelper.path2Value( tplCtx, function(params) { 
              superCms.sp_params[superCms.spId].config.css.color = $(this).val();
              toastr.success("Modification enrégistré");
            } );
            // changeCssStyle(superCms.pathConfig);
          });

          $(".block-font-size").on('mousemove', function(){
            var fontSize = $(this).val();
            $(".range-font-size__value").html("Taille du texte "+fontSize+"px");
          });

          $(document).on('mouseup','.block-font-size',function(e){
          
            // changeCssStyle(superCms.pathConfig);superCms.sp_params[superCms.spId].config.css.color = $(this).val();
            $(".btn-"+superCms.kunik).css('font-size', $(this).val()+'px');
            // $("."+superCms.kunik).css('font-size', $(this).val()+'px');
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "css.font-size"; 
            tplCtx.value = $(this).val()+'px';

            dataHelper.path2Value( tplCtx, function(params) {
               superCms.sp_params[superCms.spId].config.css['font-size']= $(this).val()+'px';
               toastr.success("Modification enrégistré"); 
              } );
            e.stopImmediatePropagation()   
          });
          // $(".super-btn-text" ).on('mouseleave', function (){
          //   tplCtx = {};
          //   tplCtx.id = superCms.spId;
          //   tplCtx.collection = "cms";
          //   tplCtx.path = "params.text"; 
          //   tplCtx.value = $(".super-btn-text" ).val();
          //   dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
          //   superCms.sp_params[superCms.spId].config.params['text'] = $(".super-btn-text").val();
          // });

          //check lbh button , load by hash change action
          $( ".check-target-lbh" ).change(function() {
            if ($( ".check-target-lbh" ).is( ":checked" ) == true) {      
              type = "lbh";
              $( ".check-target-blank" ).prop( "checked", false );
              $( ".check-target-external" ).prop( "checked", false );
            }

            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "params.type"; 
            tplCtx.value = type;

            dataHelper.path2Value( tplCtx, function(params) { 
              superCms.sp_params[superCms.spId].config.params['type'] = type;
              toastr.success("Modification enrégistré");
            } );
          });

          // ------------------------------------------------------------------------------------------------//

          //check blank button , blank change action

          $( ".check-target-blank" ).change(function() {
            if ($( ".check-target-blank" ).is( ":checked" ) == true) {      
              type = "blank";
              $( ".check-target-lbh" ).prop( "checked", false );
              $( ".check-target-external" ).prop( "checked", false );
            }
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "params.type"; 
            tplCtx.value = type;
  
            dataHelper.path2Value( tplCtx, function(params) { 
              superCms.sp_params[superCms.spId].config.params['type'] = type;
              toastr.success("Modification enrégistré");
            } );
            
          });
          // ------------------------------------------------------------------------------------------------//

          //check external button , external change action//

        

          $( ".check-target-external" ).change(function() {
            if ($( ".check-target-external" ).is( ":checked" ) == true) {      
              type = "external";
              $( ".check-target-lbh" ).prop( "checked", false );
              $( ".check-target-blank" ).prop( "checked", false );
            }
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "params.type"; 
            tplCtx.value = type;
  
            dataHelper.path2Value( tplCtx, function(params) {
              superCms.sp_params[superCms.spId].config.params['type'] = type;
              toastr.success("Modification enrégistré");
              } );
           
          });

          // ------------------------------------------------------------------------------------------------//

         
          $( ".super-btn-text" ).blur(function() {
            text = $( ".super-btn-text" ).val();
            // superCms.pathConfig.params['text'] = $( ".super-btn-text" ).val();
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "text"; 
            tplCtx.value = $( ".super-btn-text" ).val();

            dataHelper.path2Value( tplCtx, function(params) { 
              superCms.sp_params[superCms.spId].config.params['text'] = $( ".super-btn-text" ).val();
              toastr.success("Modification enrégistré");
            } );
           
          });
        /***End Link & text***/


        $(document).on("change", "#shw-color" ,function(e) {
          superCms.eltColor = $("#shw-color").val();
          $("."+superCms.kunik ).css('box-shadow', superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
          saveShadow();
          e.stopImmediatePropagation();
          // changeCssStyle(superCms.pathConfig);
        });
        $(document).on("change", "#hover-shw-color" ,function(e) {
          superCms.hoverEltColor = $("#hover-shw-color").val();
          saveHoverShadow();
          e.stopImmediatePropagation();
          // changeCssStyle(superCms.pathConfig);
        });

        $(document).on('mousedown','.range-slider', function(){
          $('.range-slider-blur').mousemove(function(){
            superCms.eltBlur = $('.range-slider-blur').val();
            $(".range-blur__value").html("Blur "+superCms.eltBlur+"px");
  
            // changeCssStyle(superCms.pathConfig);
            // if(superCms.kunik.startsWith('button')){
            //   $(".btn-"+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }else{
             $("."+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }
          });
           $('.range-slider-spread').mousemove(function(){
            superCms.eltSpread = $('.range-slider-spread').val();
            $(".range-spread__value").html("Spread "+superCms.eltSpread+"px");
  
            // changeCssStyle(superCms.pathConfig);
            // if(superCms.kunik.startsWith('button')){
            //   $(".btn-"+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }else{
             $("."+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }
          });
           $('.range-slider-x').mousemove(function(){
            superCms.eltAxeX = $('.range-slider-x').val();
            $(".range-x__value").html("Axe X "+superCms.eltAxeX+"px");
            // changeCssStyle(superCms.pathConfig);
            // if(superCms.kunik.startsWith('button')){
            //   $(".btn-"+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }else{
             $("."+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }
          });  
           $('.range-slider-y').mousemove(function(){
            superCms.eltAxeY = $('.range-slider-y').val();
            $(".range-y__value").html("Axe Y "+superCms.eltAxeY+"px");
            // changeCssStyle(superCms.pathConfig);
            // if(superCms.kunik.startsWith('button')){
            //   $(".btn-"+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }else{
             $("."+superCms.kunik ).css('box-shadow',' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px');
            // }
            // mylog.log(' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px')
          });
          $('.hover-range-slider-blur').mousemove(function(){
            superCms.hoverEltBlur = $('.hover-range-slider-blur').val();
            $(".hover-range-blur__value").html("Blur "+superCms.hoverEltBlur+"px");
  
            // changeCssStyle(superCms.pathConfig);
          });
           $('.hover-range-slider-spread').mousemove(function(){
            superCms.hoverEltSpread = $('.hover-range-slider-spread').val();
            $(".hover-range-spread__value").html("Spread "+superCms.hoverEltSpread+"px");
          });
           $('.hover-range-slider-x').mousemove(function(){
            superCms.hoverEltAxeX = $('.hover-range-slider-x').val();
            $(".hover-range-x__value").html("Axe X "+superCms.hoverEltAxeX+"px");
            // changeCssStyle(superCms.pathConfig);
          });  
           $('.hover-range-slider-y').mousemove(function(){
            superCms.hoverEltAxeY = $('.hover-range-slider-y').val();
            $(".hover-range-y__value").html("Axe Y "+superCms.hoverEltAxeY+"px");
            // changeCssStyle(superCms.pathConfig);
            // mylog.log(' '+superCms.inset+' '+superCms.eltColor+' '+superCms.eltAxeX+'px '+superCms.eltAxeY+'px '+superCms.eltBlur+'px '+superCms.eltSpread+'px')
          });
       });

        $(document).on('mouseup', '.range-slider',function(e){
          // saveShadow();
          if($(this).children("input").attr('class').indexOf("hover") > -1){
            saveHoverShadow();
          }else{
            saveShadow();
          }
          e.stopImmediatePropagation();
          // changeCssStyle(superCms.pathConfig);
        });

        function saveShadow(){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.box-shadow"; 
          tplCtx.value = {};
          tplCtx.value.inset =  superCms.inset;
          tplCtx.value.color =  superCms.eltColor;
          tplCtx.value.x     =  superCms.eltAxeX;
          tplCtx.value.y     =  superCms.eltAxeY;
          tplCtx.value.blur  =  superCms.eltBlur;
          tplCtx.value.spread =  superCms.eltSpread;

          actual_shadow = superCms.sp_params[superCms.spId].config.css['box-shadow'] ;
          new_shadow = {
            inset :  superCms.inset,
            color :  superCms.eltColor,
            x : superCms.eltAxeX,
            y : superCms.eltAxeY,
            spread :  superCms.eltSpread,
            blur :  superCms.eltBlur,
          }
          if ( actual_shadow != new_shadow ){

            dataHelper.path2Value( tplCtx, function(params) {           
              toastr.success("Modification enrégistré");
              superCms.sp_params[superCms.spId].config.css['box-shadow']['inset'] = superCms.inset;
              superCms.sp_params[superCms.spId].config.css['box-shadow']['color'] = superCms.eltColor;
              superCms.sp_params[superCms.spId].config.css['box-shadow']['x'] = superCms.eltAxeX;
              superCms.sp_params[superCms.spId].config.css['box-shadow']['y'] = superCms.eltAxeY;
              superCms.sp_params[superCms.spId].config.css['box-shadow']['blur'] = superCms.eltBlur;
              superCms.sp_params[superCms.spId].config.css['box-shadow']['spread'] = superCms.eltSpread;
            } );

          }
            
            // superCms.sp_params[superCms.spId].config.css['box-shadow'] = superCms.pathConfig.css['box-shadow'] || Object();
          
        }
        function saveHoverShadow(){
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "css.hover.box-shadow"; 
          tplCtx.value = {};
          tplCtx.value.inset =  superCms.hoverInset;
          tplCtx.value.color =  superCms.hoverEltColor;
          tplCtx.value.x     =  superCms.hoverEltAxeX;
          tplCtx.value.y     =  superCms.hoverEltAxeY;
          tplCtx.value.blur  =  superCms.hoverEltBlur;
          tplCtx.value.spread =  superCms.hoverEltSpread;

          dataHelper.path2Value( tplCtx, function(params) {           
            toastr.success("Modification enrégistré");
            superCms.sp_params[superCms.spId].config.css.hover['box-shadow']['inset'] = superCms.hoverInset;
            superCms.sp_params[superCms.spId].config.css.hover['box-shadow']['color'] = superCms.hoverEltColor;
            superCms.sp_params[superCms.spId].config.css.hover['box-shadow']['x'] = superCms.hoverEltAxeX;
            superCms.sp_params[superCms.spId].config.css.hover['box-shadow']['y'] = superCms.hoverEltAxeY;
            superCms.sp_params[superCms.spId].config.css.hover['box-shadow']['blur'] = superCms.hoverEltBlur;
            superCms.sp_params[superCms.spId].config.css.hover['box-shadow']['spread'] = superCms.hoverEltSpread;
          } );
            
            // superCms.sp_params[superCms.spId].config.css['box-shadow'] = superCms.pathConfig.css['box-shadow'] || Object();
          
        }

      },
      elementSize : function () {
        /**************Content size**************/    
        $(document).on("click", ".content-size", function(e){
          size = $(this).data("size");
          var $el = $('.whole-'+superCms.kunik);
          $(".whole-"+superCms.kunik).removeClass("super-"+superCms.spId);
          $(".whole-"+superCms.kunik).removeClass("sp-cms-container");
          var classList = $el.attr('class').split(' ');

          $.each(classList, function(id, item) {
            if (item.indexOf('sp-cms-') == 0) $el.removeClass(item);
          });
          if (size == "container") {
            if ($("#check-container").is(':checked')) {
             $("#check-container").prop('checked', false);
             size = "100%";
             $el.addClass('sp-cms-std');
             widthClass = 'sp-cms-std';
             $(".whole-"+superCms.kunik).css("width", size);
             $(".sizes").val(size); 
            // superCms.pathConfig.class['width'] = widthClass;
            // superCms.pathConfig.css['size'] = superCms.pathConfig.css['size'] || Object();
            // superCms.pathConfig.css['size']['width'] = size;
            // changeCssStyle(superCms.pathConfig);
            //  tplCtx = {};
            //  tplCtx.id = superCms.spId;
            //  tplCtx.collection = "cms";
            //  tplCtx.path = "class.width"; 
            //  tplCtx.value = widthClass;
            //  dataHelper.path2Value( tplCtx, function(params) {
            //   tplCss = {};
            //   tplCss.id = superCms.spId;
            //   tplCss.collection = "cms";
            //   tplCss.path = "css.size.width"; 
            //   tplCss.value = size;
            //   dataHelper.path2Value( tplCss, function(params) {
            //     superCms.sp_params[superCms.spId].config.class['width'] = widthClass;
            //     superCms.sp_params[superCms.spId].config.css['size'] = superCms.sp_params[superCms.spId].config.css['size'] || Object();
            //     superCms.sp_params[superCms.spId].config.css['size']['width'] = size;
            //     toastr.success("Modification enrégistré");} );
            // } );
           }else{
             $("#check-container").prop('checked', true);
             size = "100%";
             $(".sizes").val(size); 
             widthClass = 'sp-cms-container';
             $el.addClass('sp-cms-container');

            //  superCms.pathConfig.class['width'] = widthClass;
            //  superCms.pathConfig.css['size'] = superCms.pathConfig.css['size'] || Object();
            //  superCms.pathConfig.css['size']['width'] = size;
            //  tplCtx = {};
            //  tplCtx.id = superCms.spId;
            //  tplCtx.collection = "cms";
            //  tplCtx.path = "class.width"; 
            //  tplCtx.value = widthClass;
            //  dataHelper.path2Value( tplCtx, function(params) {
            //   tplCss = {};
            //   tplCss.id = superCms.spId;
            //   tplCss.collection = "cms";
            //   tplCss.path = "css.size.width"; 
            //   tplCss.value = size;
            //   dataHelper.path2Value( tplCss, function(params) { 
            //     superCms.sp_params[superCms.spId].config.class['width'] = widthClass;
            //     superCms.sp_params[superCms.spId].config.css['size'] = superCms.sp_params[superCms.spId].config.css['size'] || Object();
            //     superCms.sp_params[superCms.spId].config.css['size']['width'] = size;
            //     toastr.success("Modification enrégistré");} );
            // } );
           }
         }else{             
           $el.addClass('sp-cms-std');
           widthClass = 'sp-cms-std';
           size = size+"%";
           $(".whole-"+superCms.kunik).css("width", size);
           $(".sizes").val(size); 

          //  superCms.pathConfig.class.width = widthClass;
          //  superCms.pathConfig.css.size = superCms.pathConfig.css.size || Object();
          //  superCms.pathConfig.css.size.width = size;
          //  changeCssStyle(superCms.pathConfig);
          //  tplCtx = {};
          //  tplCtx.id = superCms.spId;
          //  tplCtx.collection = "cms";
          //  tplCtx.path = "class.width"; 
          //  tplCtx.value = widthClass;
          //  dataHelper.path2Value( tplCtx, function(params) {
          //   tplCss = {};
          //   tplCss.id = superCms.spId;
          //   tplCss.collection = "cms";
          //   tplCss.path = "css.size.width"; 
          //   tplCss.value = size;
          //   dataHelper.path2Value( tplCss, function(params) { toastr.success("Modification enrégistré");} );

          //   superCms.sp_params[superCms.spId].config.class['width'] = widthClass;
          //   superCms.sp_params[superCms.spId].config.css['size'] = superCms.sp_params[superCms.spId].config.css['size'] || Object();
          //   superCms.sp_params[superCms.spId].config.css['size']['width'] = size;
          // } );
         }
       });

        $(document).on("keyup", ".sizes", function(){        
          $(".whole-"+superCms.kunik).css("width", $(".sizes").val());
          size = $(".sizes").val();
          // changeCssStyle(superCms.pathConfig);
        });
        
         

        $(document).on("blur",".sizes",function(e){ 
          setTimeout(function() {         
            var regex = /[+-]?\d+(\.\d+)?/g;
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "css.size.width"; 
            tplCtx.value = {};

            superCms.sp_params[superCms.spId].config.css['size'] = superCms.sp_params[superCms.spId].config.css['size'] || Object();
              if( size.indexOf('%') >= 0){       
                superCms.sp_params[superCms.spId].config.css['size']['width'] = size.match(regex).map(function(v) { return parseFloat(v); })+"%";
                tplCtx.value = size.match(regex).map(function(v) { return parseFloat(v); })+"%";
              }else if(size === "container"){
                superCms.sp_params[superCms.spId].config.css['size']['width'] = '100%';
                tplCtx.value = "100%";
              }else{
                superCms.sp_params[superCms.spId].config.css['size']['width'] = size.match(regex).map(function(v) { return parseFloat(v); })+"px";
                tplCtx.value = size.match(regex).map(function(v) { return parseFloat(v); })+"px";
              }  
            
            // superCms.sp_params[superCms.spId].config.css['size']["width"] = tplCtx.value
            dataHelper.path2Value( tplCtx, function(params) {   
              toastr.success("Modification enrégistré");
            } );
          }, 600);
          e.stopImmediatePropagation()
        }); 
        
      },
      htmlInsertion : function () {   
        
        $(document).on('keyup',".other-html", function (e){
          var valValid = superCms.setting.validateScript(".other-html", "<p class='text-red bg-white'>Oops! Invalid html 🤯</p>");
          if (valValid !== false) {
            if (valValid !== "") {
              $(".empty-sp-element"+superCms.kunik).hide();
            }else{
              $(".empty-sp-element"+superCms.kunik).show();
            }
            $(".html"+superCms.kunik).show();
            $(".html"+superCms.kunik).html(valValid);
            superCms.sp_params[superCms.spId].config.html = valValid;
          }  
          e.stopImmediatePropagation()
        });

        $(document).on('blur',".other-html", function (e){
          var valValid = superCms.setting.validateScript(".other-html", "<p class='text-red bg-white'>Oops! Invalid html 🤯</p>");
          if (valValid !== false) {
            tplCtx = {};
            tplCtx.id = superCms.spId;
            tplCtx.collection = "cms";
            tplCtx.path = "html"; 
            tplCtx.value = valValid;

            dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );  
                
          }  
          e.stopImmediatePropagation()
        });

        $(document).on('click',".saveHtml", function (){
          superCms.sp_params[superCms.spId].config.pageHtml = $("#addPage").val();
          tplCtx = {};
          tplCtx.id = superCms.spId;
          tplCtx.collection = "cms";
          tplCtx.path = "pageHtml"; 
          tplCtx.value = $("#addPage").val();

          dataHelper.path2Value( tplCtx, function(params) { 

            urlCtrl.loadByHash(location.hash);

            ;} );
        });
      },      
         validateScript : function(element,sp_message){
         var devElem = $(element).val();
         var strDev = "";
         if (devElem.indexOf('script') > -1 || devElem.indexOf(')') > -1 || devElem.indexOf('}') > -1 || devElem.indexOf(']') > -1){
          $(".sp-message").html(sp_message);
          return false;
        }else{
          strDev = devElem;
          $(".sp-message").html("");
          return strDev;
        }
      }
    },

  bindEvents : function() {
    cmsBuilder.manage.delete();
    cmsBuilder.events.sortBlock();
    superCms.manage.eltSelection();
    superCms.manage.eltInsertion();
    superCms.manage.menuDisplay();
    superCms.manage.btnFunction();
    superCms.manage.texteEditFunction();
  },
  events: {
    editBloc : function(){
        
    }
  }
}
function removeAllBlankObjects(configObj) {
  Object.keys(configObj).forEach(k => {
     if ((configObj[k] && typeof configObj[k] === 'object' && removeAllBlankObjects(configObj[k]) === null) || configObj[k] == "") {
        delete configObj[k];
     }
  });
  if (!Object.keys(configObj).length) {
     return null;
  }
}

jQuery.fn.removeClassExcept = function (val) {
  return this.each(function (index, el) {
    var keep = val.split(" "),
    reAdd = [],
    $el = $(el);         
    for (var i = 0; i < keep.length; i++){
      if ($el.hasClass(keep[i])) reAdd.push(keep[i]);

    }          
    $el
    .removeClass()
    .addClass(reAdd.join(' '));
  });
};
superCms.init()

function transformRadius(keyP, keyS, value){
  var textRadius = "";
  $.each(value, function(k, v){
    if(v != ""){
      textRadius += keyP+"-"+k+"-"+keyS+": "+v+"px !important;\n";
    }
  });
  return textRadius;
}
function transformShadow(key,value){
  return key+": "+value.inset+' '+' '+value.color+' '+value.x+'px'+' '+value.y+'px'+' '+value.blur+'px'+' '+value.spread+'px !important; \n';
}
function transformCssText(key, array){
  var text = "";
  if(typeof array === 'object'){
    if(key.indexOf('shadow') > -1){
      text += transformShadow(key, array);
    }else{
      $.each(array, function(k, v){
        if(k == "radius"){
          text += transformRadius(key,k, v);
        }else if(key == "size"){
          text += transformCssText(k, v);
        }else{
          text += transformCssText(key+'-'+k, v);
        }
      });
    }
  }else{
    if(array != ""){
      if(key.indexOf("other") > -1){
    
      }else if(key.indexOf("padding") > -1 || key.indexOf("margin") > -1){
        text += key+': '+array+"px !important;\n";
      }else if(key.indexOf("border") > -1){
        if(key.indexOf("type") > -1){
          text += 'border-style: '+array+" !important;\n";
        }else if(key.indexOf("width") > -1){
          text += key+': '+array+"px !important;\n";
        }
      }else{
        text += key+': '+array+" !important;\n";
      }
    }
  }
  return text;
}
function transformCssButton(key, array){
  var text = "";
  if(typeof array === 'object'){
    if(key.indexOf('shadow') > -1){
      text += transformShadow(key, array);
    }else{
      $.each(array, function(k, v){
        if(k == "radius"){
          text += transformRadius(key,k, v);
        }else if(key == "size"){
          // text += transformCssButton(k, v);
        }else{
          text += transformCssButton(key+'-'+k, v);
        }
      });
    }
  }else{
    if(array != ""){
      if(key.indexOf("other") > -1 || key.indexOf("padding") > -1 || key.indexOf("margin") > -1){
        // text += key+': '+array+"px !important;\n";
      }else if(key.indexOf("border") > -1){
        if(key.indexOf("type") > -1){
          text += 'border-style: '+array+" !important;\n";
        }else if(key.indexOf("width") > -1){
          text += key+': '+array+"px !important;\n";
        }
      }else{
        text += key+': '+array+" !important;\n";
      }
    }
  }
  return text;
}

function transformCssIcon(key, array) {
  var text = "";
  if(typeof array === 'object'){
    $.each(array, function(k, v){
      text += transformCssText(key+'-'+k, v);
    });
  }else{
    if(array != ""){
      if(key.indexOf("other") > -1){
    
      }else if(key.indexOf("style") > -1){
        $('.icon-'+superCms.spId).removeClass(function (index, css) {
          return (css.match (/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
        });
        $(".icon-"+superCms.kunik).addClass('fa fa-'+array);
      }else if(key.indexOf("size") > -1){
        text += "font-size: "+array+"px !important; \n";
      }else if(key.indexOf("position") > -1 || key.indexOf("margin") > -1){
        text += key+': '+array+"px !important;\n";
      }else{
        text += key+': '+array+" !important;\n";
      }
    }
  }
  return text;
}

function changeCssIcon(style, type) {
  $("#"+type+"IconStyle"+superCms.kunik).remove();
  var element  = document.createElement("style");
  element.id = type+"IconStyle"+superCms.kunik;
  var cssText = "."+type+"IconStyle"+superCms.kunik+"{ \n";
  $.each(style, function(k, v){
    cssText += transformCssIcon(k, v);
  });
  cssText += "}";
  element.innerHTML = cssText;
  var header = document.getElementsByTagName("HEAD")[0] ;
  header.appendChild(element);
  if(type == "saved"){
    $('.icon-'+superCms.kunik).addClass("savedIconStyle"+superCms.kunik);
  }
}

function changeCssStyle(style){
  $("#customStyle"+superCms.kunik).remove();
  var element  = document.createElement("style");
  element.id = "customStyle"+superCms.kunik;
  var cssText = ".customStyle"+superCms.kunik+"{ \n";
  var cssButton = ".customButtonStyle"+superCms.kunik+"{ \n";
  $.each(style.css, function(k, v){
    if(k == "icon"){
      changeCssIcon(v, "custom");
    }else{
      cssText += transformCssText(k, v);
      cssButton += transformCssButton(k, v);
    }
  });
  cssText += "}";
  cssButton += "}";
  element.innerHTML = (superCms.kunik.indexOf("button") > -1 ? cssText+"\n"+cssButton : cssText);
  var header = document.getElementsByTagName("HEAD")[0] ;
  header.appendChild(element);
}

function addStyleSave(style){
  $("#savedStyle"+superCms.kunik).remove();
  var element  = document.createElement("style");
  element.id = "savedStyle"+superCms.kunik;
  var cssText = ".savedStyle"+superCms.kunik+"{ \n";
  var cssButton = ".savedButtonStyle"+superCms.kunik+"{ \n";
  $.each(style.css, function(k, v){
    if(k == "icon"){
      changeCssIcon(v, "saved");
    }else{
      cssText += transformCssText(k, v);
      cssButton += transformCssButton(k, v);
    }
  });
  cssText += "}";
  cssButton += "}";
  element.innerHTML = (superCms.kunik.indexOf("button") > -1 ? cssText+"\n"+cssButton : cssText);
  var header = document.getElementsByTagName("HEAD")[0] ;
  header.appendChild(element);
  $('.'+superCms.kunik).addClass("savedStyle"+superCms.kunik);
  if(superCms.kunik.indexOf("button") > -1){
    $('.btn-'+superCms.kunik).addClass("savedButtonStyle"+superCms.kunik);
  }
}   

function saveWidth() {
  tplCtx = {};
  tplCtx.id = superCms.spId;
  tplCtx.collection = "cms";
  tplCtx.path = "css.border"; 
  tplCtx.value = {
      "type" : superCms.sp_params[superCms.spId].config.css.border.type,
      "top" : $(".border-width[data-given=top]").val(),
      "left" : $(".border-width[data-given=left]").val(),
      "bottom" : $(".border-width[data-given=bottom]").val(),
      "right" : $(".border-width[data-given=right]").val(),
      "width" : $("#border-width").val(),
      "color" : $("#border-color").val(),
      "radius" : superCms.sp_params[superCms.spId].config.css.border.radius
  };

  actual_borderWidth = superCms.sp_params[superCms.spId].config.css.border ;

  if ( JSON.stringify(actual_borderWidth) !== JSON.stringify(tplCtx.value) ) {
    superCms.sp_params[superCms.spId].config.css.border =  tplCtx.value ;
    dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification bordure enrégistré");} );
  }
}
function saveHoverWidth() {
  tplCtx = {};
  tplCtx.id = superCms.spId;
  tplCtx.collection = "cms";
  tplCtx.path = "css.hover.border"; 
  tplCtx.value = {
      "width" : $("#hover-border-width").val(),
      "color" : $("#hover-border-color").val(),
      "type" : superCms.sp_params[superCms.spId].config.css.hover.border.type,
      "bottom" : $(".hover-border-width[data-given=bottom]").val(),
      "top" : $(".hover-border-width[data-given=top]").val(),
      "left" : $(".hover-border-width[data-given=left]").val(),
      "right" : $(".hover-border-width[data-given=right]").val(),
      "radius" : superCms.sp_params[superCms.spId].config.css.hover.border.radius
  };
  dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
}

function getComputedBg(element){
  let bgc = element.css('background-color')

  if(element.hasClass("block-container-html")!= true  /* && bgc == "rgb(255, 255, 255)"*/ && bgc == "rgba(0, 0, 0, 0)") {  
    getComputedBg(element.parent());
  }

  if (/*bgc != "rgb(255, 255, 255)" &&*/ bgc != "rgba(0, 0, 0, 0)") {
    superCms["bgTextColor"] = bgc
  }
   
}
function getComputedColor(element){
  let color = element.css('color')

  if(element.hasClass("block-container-html") != true && color == "rgba(0, 0, 0, 0)") {  
    getComputedColor(element.parent());
  }
    superCms["textColor"] = color
}


function InheritedBackgroundColor(elt){
  delete superCms.bgTextColor;
  delete superCms.textColor;
  getComputedBg(elt)
  getComputedColor(elt)
  if (typeof superCms.bgTextColor == "undefined" && superCms.bgTextColor == "rgb(255, 255, 255)"){
    superCms.bgTextColor = "#ffffff"
  }
  if (typeof superCms.textColor == "undefined" && superCms.bgTextColor == "rgb(0, 0, 0)"){
    superCms.textColor = "#000"
  }
  return { bgTextColor : superCms.bgTextColor, textColor : superCms.textColor}
}
