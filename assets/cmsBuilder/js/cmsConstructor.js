var cmsConstructor = {
    /* PROPERTIES */

    sp_params: {},
    values: {},
    pathConfig: {},
    kunik: "",
    spId: "",
    configView:{},
    dragstart:false,
    
    /* UTILITIES */
    helpers: {
        hexDigits : new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"),

        getComputedBg : function(sptextField = null){

            let bgc = "";
            if(sptextField){
                bgc = sptextField.css('background-color')

                if(sptextField.hasClass("block-container-html")!= true && bgc == "rgba(0, 0, 0, 0)") {  
                    cmsConstructor.helpers.getComputedBg(sptextField.parent());
                }

                if ( bgc != "rgba(0, 0, 0, 0)") {
                    cmsConstructor["bgTextColor"] = bgc
                }   
            }        

        },
        getComputedColor : function(sptextField = null){
            if (sptextField) {
                let color =  sptextField.css('color')
                if(sptextField.hasClass("block-container-html") != true && color == "rgba(0, 0, 0, 0)") {  
                    cmsConstructor.helpers.getComputedColor(sptextField.parent());
                }
                cmsConstructor["textColor"] = color
            }
        },
        InheritedBackgroundColor : function(elt){
            delete cmsConstructor.bgTextColor;
            delete cmsConstructor.textColor;
            cmsConstructor.helpers.getComputedBg(elt)
            cmsConstructor.helpers.getComputedColor(elt)
            if (typeof cmsConstructor.bgTextColor == "undefined"|| cmsConstructor.bgTextColor == "rgb(255, 255, 255)"){
                cmsConstructor.bgTextColor = "rgb(255, 255, 255)";
            }
            if (typeof cmsConstructor.textColor == "undefined" || cmsConstructor.bgTextColor == "rgb(0, 0, 0)"){
                cmsConstructor.textColor = "rgba(0, 0, 0, 0)"
            }
            return { bgTextColor : cmsConstructor.bgTextColor, textColor : cmsConstructor.textColor}
        },

        rgb2hex : function(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            return "#" + cmsConstructor.helpers.hex(rgb[1]) + cmsConstructor.helpers.hex(rgb[2]) + cmsConstructor.helpers.hex(rgb[3]);
        },

        hex : function(x) {
            return isNaN(x) ? "00" : cmsConstructor.helpers.hexDigits[(x - x % 16) / 16] + cmsConstructor.helpers.hexDigits[x % 16];
        },

        validateScript: function (element, sp_message) {
            var strDev = "";
            if (element.indexOf('script') > -1 || element.indexOf(')') > -1 || element.indexOf('}') > -1 || element.indexOf(']') > -1) {
                $(".sp-message").html(sp_message);
                return false;
            } else {
                strDev = element;
                $(".sp-message").html("");
                return strDev;
            }
        },
        validateOtherCss: function (cssValue,message) { 
            // let validate = cssValue.replace(/[^\S+]/g,"");
			// 	validate = validate.replace(/\/\*[\s\S]*?\*\//g,"");
			// 	validate = validate.replace(/http:\/\//gm,"");
			// 	validate = validate.replace(/((#|[.]|[*]){0,1}[\w\-:>~+,()])+\s*\{((?:[A-Za-z\- \s]+[:]\s*([#'"0-9\w .,\/()\-!%]+\;)))+\}(?:\s*)/mg,"");
			// 	validate = validate.replace(/@importurl\([\"']([^)]*)[\"']\);|@import(\"|\')([^)]*)(\"|\');/g,"");
			// 	validate = validate.replace(/(@.*?:?[^}{@]+(\{(?:[^}{]+|\{(?:[^}{]+|\{[^}{]*\})*\})*\}))/g,"")
			
            const validation = csstreeValidator.validate(cssValue);

			if(validation.length > 0){ 
                if (message == true){
                    toastr.error(validation[0]["message"]+": line "+validation[0]["line"]+",column "+validation[0]["column"]);
                }
				return false;
			}
            else {
                return true;
            }
        },
        getBlockTitle:function($block){
            var type = $block.data("blocktype"),
                name = $block.data("name"),
                path = $block.data("path");

            var title = name;

            if(type == "section" && path == "tpls.blockCms.superCms.container")
                title = "Section"
            else if(type == "column" && title.indexOf("blockCms.superCms") > 0 )
                title = "Colonne"
            
            return title;
        },
        scrollTo: function($container, $target, duration=500){
            $container.animate({
                scrollTop: $target.offset().top -  $container.offset().top +  $container.scrollTop()
            }, duration)
        },
        string2Json: function (element, json) {
            var treeObject = {};

            if (typeof element === "string") {
                if (window.DOMParser) {
                    parser = new DOMParser();
                    docNode = parser.parseFromString(element, "text/xml");
                } else {
                    docNode = new ActiveXObject("Microsoft.XMLDOM");
                    docNode.async = false;
                    docNode.loadXML(element);
                }
                element = docNode.firstChild;
            }

            function treeHTML(element, object) {
                object["type"] = element.nodeName;
                var nodeList = element.childNodes;
                if (nodeList != null) {
                    if (nodeList.length) {
                        object["content"] = [];
                        for (var i = 0; i < nodeList.length; i++) {
                            if (nodeList[i].nodeType == 3) {
                                object["content"].push(nodeList[i].nodeValue);
                            } else {
                                object["content"].push({});
                                treeHTML(nodeList[i], object["content"][object["content"].length - 1]);
                            }
                        }
                    }
                }
                if (element.attributes != null) {
                    if (element.attributes.length) {
                        object["attributes"] = {};
                        for (var i = 0; i < element.attributes.length; i++) {
                            object["attributes"][element.attributes[i].nodeName] = element.attributes[i].nodeValue;
                        }
                    }
                }
            }
            treeHTML(element, treeObject);

            return (json) ? treeObject : treeObject;
        },
        isContains: function (json, value) {
            let contains = false;
            Object.keys(json).some(key => {
                contains = typeof json[key] === 'object' ? this.isContains(json[key], value) : json[key] === value;
                return contains;
            });
            return contains;
        },
        selectNode: function(node){
            var selection = window.getSelection()
            selection.removeAllRanges()
            var range = document.createRange()
            range.selectNode(node)
            selection.addRange(range)
        },
        // setStyleOfSelection:function(property, value, toggle=false){
        //     if(window.getSelection){
        //         var selection = window.getSelection()
        //         if(selection.rangeCount){
        //             var range = selection.getRangeAt(0),
        //                 parentNode = range.startContainer.nodeName == "#text" ? range.startContainer.parentNode:range.startContainer,
        //                 textContent = range.toString(),
        //                 newNode = null;

        //             if(parentNode.getAttribute("contenteditable") == null && parentNode.textContent == textContent){
        //                 newNode = parentNode;
        //                 if(toggle && newNode.style[property])
        //                     newNode.style[property] = null
        //                 else
        //                     newNode.style[property] = value

        //                 //remove span if no style
        //                 if(!parentNode.getAttribute("style")){
        //                     var textNode = document.createTextNode(textContent)
        //                     newNode.replaceWith(textNode)
        //                     newNode = textNode;
        //                 }

        //             }else if(value){
        //                 newNode = document.createElement("span")
        //                 newNode.classList.add("d-inline-block")
        //                 newNode.style[property] = value;
        //                 newNode.textContent = textContent;

        //                 range.deleteContents()
        //                 range.insertNode(newNode);
        //             }

        //             this.selectNode(newNode.nodeName=="#text"?newNode:newNode.childNodes[0])
        //         }
        //     }
        // },
        getStyleOfSelection:function(){
            if(!window.getSelection || !window.getSelection().rangeCount)
                return {}

            var range = window.getSelection().getRangeAt(0),
                parentNode = range.startContainer.parentNode,
                textContent = range.toString();

            window.parentNode = parentNode
            if(parentNode.getAttribute("contenteditable") == null && parentNode.textContent == textContent){
                var res = {};
                if (parentNode.getAttribute("style")) {
                    var styles = parentNode.getAttribute("style").trim().split(";");
                    styles.forEach(function(style){
                        style = style.split(":")
                        if(style.length > 1)
                            res[style[0].trim()] = style[1].trim()
                    })
                }
                return res;
            }

            return {}
        },
        colorRGBtoHex: function(colorRGB, defaultValue="#000000"){
            if(/^#.*/.test(colorRGB))
                return colorRGB
            else if(/rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)/.test(colorRGB)){
                var rgb = colorRGB.substring(4, colorRGB.length-1).replace(/ /g, '').split(',');
                
                return "#" + (1 << 24 | rgb[0] << 16 | rgb[1] << 8 | rgb[2]).toString(16).slice(1);
            }else{
                return defaultValue
            }
        },
        onchangeAdvanced: function(name,kunik,value){
            if(name == "otherClass") {
                    var valValid = cmsConstructor.helpers.validateScript(value, "<p class='text-red bg-white'>Oops! 🤯<br>Invalid class!</p>");
                    if (valValid !== false) {
                        if(kunik.startsWith('button')){ 
                            $(".btn-"+kunik).removeClassExcept("cmsbuilder-block super-cms "+kunik+"-css btn-functionLink bs btn-"+kunik+" other-css-"+kunik+" newCss-"+kunik);
                            $(".btn-"+kunik).addClass(valValid);
                        }else{
                            $("." +kunik).removeClassExcept("super-img-"+cmsConstructor.spId+" "+kunik+"-css this-content-"+kunik+" unselectable sp-cms-container cmsbuilder-block-container text-left has-children " + kunik + " whole-" + kunik + " super-cms spCustomBtn other-css-" + kunik + " newCss-" + kunik + " sp-cms-10 sp-cms-20 sp-cms-30 sp-cms-40 sp-cms-50 sp-cms-120 sp-cms-120 sp-cms-80 sp-cms-90 sp-cms-100 super-container sp-cms-std");
                            $("." +kunik).addClass(valValid);
                        }
                    }
                }

                if(name == "otherCss") {

                    $("#myStyleId" + kunik).remove();
                    cmsConstructor.sp_params[cmsConstructor.spId].css.other = value;
                    var element = document.createElement("style");
                    element.id = "myStyleId" + kunik;
                    element.innerHTML = ".newCss-" + kunik + " {" + cmsConstructor.sp_params[cmsConstructor.spId].css.other + "}";
                    if(cmsConstructor.helpers.validateOtherCss(element.innerHTML,false)){
                        var header = document.getElementsByTagName("HEAD")[0];
                        header.appendChild(element);
                        if(kunik.startsWith('button')){
                            $(".btn-" +kunik).removeClass("newCss-" + kunik);
                            $(".btn-" +kunik).removeClass("other-css-" + kunik);
                            $(".btn-" +kunik).addClass("newCss-" + kunik);
                        }else{
                            $("." +kunik).removeClass("newCss-" + kunik);
                            $("." +kunik).removeClass("other-css-" + kunik);
                            $("." +kunik).addClass("newCss-" + kunik);
                        }
                    }
                   
                }
                // if(name=="balise"){
                //     baliseToReplace=(typeof cmsConstructor.sp_params[cmsConstructor.spId].advanced != "undefined"  && typeof cmsConstructor.sp_params[cmsConstructor.spId].advanced.balise != "undefined" && notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].advanced.balise)) ? cmsConstructor.sp_params[cmsConstructor.spId].advanced.balise : "div";
                //     newH=document.querySelector("."+cmsConstructor.kunik).outerHTML.replaceAll(baliseToReplace, value);
                //     $("."+cmsConstructor.kunik).replaceWith(newH);
                // }
        },
        getAutoScroller:function(container, params={}){
            var $container = $(container);
        
            var config = Object.assign({}, {
                distance:100, timer:80, step:50
            }, params)
            var offset = $container.offset(),
                offsetHeight = offset.top + $container.height()

            var handlers = { 
                top:null, 
                bottom:null,
                clear:function(){
                    clearInterval(this.top),
                    clearInterval(this.bottom)
                }
            }

            return {
                start:function(event){
                    var isMoving = false
    
                    if((event.pageY - offset.top) <= config.distance){
                        isMoving = true
                        handlers.clear()
                        handlers.top = setInterval(function(){
                            $container.scrollTop($container.scrollTop() - config.step)
                        }, config.timer)
                    }
                    //bottom
                    if(event.pageY >= (offsetHeight - config.distance)){
                        isMoving = true
                        handlers.clear()
                        handlers.bottom = setInterval(function(){
                            $container.scrollTop($container.scrollTop() + config.step)
                        }, config.timer)
                    }
                    if(!isMoving)
                        handlers.clear()
                },
                stop:function(){
                    handlers.clear();
                }
            }
        },
        isInViewport:function(el,marge) {
            const rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom-marge <= (window.innerHeight || document.documentElement.clientHeight ) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth )
            );
        },
        getBlockType:function(kunik,id){
            var blockType = kunik.replace(id,"");

            if(blockType == 'container'){
                blockType = 'section';
            }
            return blockType;
        },
        refreshBlock: function(id, dom) {
            ajaxPost(
                null,
                baseUrl+"/co2/cms/refreshblock",
                {
                    idBlock: id 
                },
                function(data) {
                    $(`${dom}`).html(data["view"]);
                },
                null,
                "json",
                {
                    async: false
                }
            )
        }
    },

    // Moved to the right place
    // actions: {
    //     deleteBlock: function(callback=null){
    //         var id = cmsConstructor.spId;
    //         ajaxPost(
    //             null,
    //             `${baseUrl}/co2/cms/delete/id/${id}`,
    //             null,
    //             function(data) {
    //                 if (data && data.result) {
    //                     var idparent = $("[data-id="+id+"]").parents('.custom-block-cms').data('id');
    //                     var kunik = $("[data-id="+id+"]").parents(".custom-block-cms").data("kunik");
    //                     if(idparent == id){
    //                         toastr.success("Element effacé");
    //                         $('.block-container-'+kunik).remove();
    //                     }else{
    //                         var parents = $("[data-id="+id+"]").parent();
    //                         var kunikparents = parents.data("kunik");
    //                         var imageSrc = $('.'+kunikparents).css('background-image');
    //                         $("[data-id="+id+"]").remove();
    //                         mylog.log("Hide HTML COntent Hide", $("."+kunik).is(':hidden') , imageSrc == "none" , parents.children(".cmsbuilder-block").length)
    //                         if( imageSrc == "none"  && parents.children(".cmsbuilder-block").length == 0 && $(".html"+kunik).is(':hidden') == true ){
    //                             $(".empty-sp-element"+kunikparents).removeClass("d-none");
    //                         }else{
    //                             if(imageSrc != "none" && parents.children(".cmsbuilder-block").length == 0 && $(".html"+kunik).is(':hidden') == true ){
    //                                 if( typeof cmsConstructor.sp_params[parents.data('id')] != "undefined" && typeof cmsConstructor.sp_params[parents.data('id')].css.height != "undefined" && cmsConstructor.sp_params[parents.data('id')].css.height == ""){
    //                                     parents.css("height","250px");
    //                                 }
    //                             }
    //                         }   
                            
    //                         // var path = $("[data-id="+id+"]").parents(".custom-block-cms").data("path");
    //                         // cmsBuilder.block.loadIntoPage(idparent, page, path, kunik);
    //                     } 
    //                     $(".cmsbuilder-right-content").removeClass("active")
    //                     $("#right-panel").html("")
    //                     $(".block-actions-wrapper").removeClass("selected")
    //                     coInterface.initHtmlPosition();
    //                 } else {
    //                     toastr.error("something went wrong!! please try again.");
    //                 }

    //                 if(callback && typeof callback === "function")
    //                     callback(data)
    //             },
    //             null,
    //             "json"
    //         );
    //     }
    // },

    // To Delete 

    // dynform: {
    //     initUploadImage:function(){
    //         return {
    //             "jsonSchema": {
    //                 "title": "Importation d'image",
    //                 "description": "Personnaliser votre bloc",
    //                 "icon": "fa-cog",
        
    //                 "properties": {
    //                     "image": {
    //                         "inputType": "uploader",
    //                         "label": "image",
    //                         "docType": "image",
    //                         "contentKey": "slider",
    //                         "itemLimit": 1,
    //                         "endPoint": "/subKey/block",
    //                         "domElement": "image",
    //                         "filetypes": ["jpeg", "jpg", "gif", "png"],
    //                         "label": "Image :",
    //                         "showUploadBtn": false,
    //                         initList: cmsConstructor.sp_params[cmsConstructor.spId].config.initImage
    //                     },
    //                 },
    //                 beforeBuild: function () {
    //                     uploadObj.set("cms", cmsConstructor.spId);
    //                 },
    //                 save: function (data) {
    //                     tplCtx.value = {};
    //                     $.each(this.properties, function (k, val) {
    //                         tplCtx.value[k] = $("#" + k).val();
    //                         if (k == "parent")
    //                             tplCtx.value[k] = formData.parent;
        
    //                         if (k == "items")
    //                             tplCtx.value[k] = data.items;
    //                     });
        
    //                     if (typeof tplCtx.value == "undefined")
    //                         toastr.error('value cannot be empty!');
    //                     else {
    //                         dataHelper.path2Value(tplCtx, function (params) {
    //                             dyFObj.commonAfterSave(params, function () {
    //                                 toastr.success("Élément bien ajouter");
    //                                 $("#ajax-modal").modal('hide');

                                    
    //                                     cmsBuilder.block.loadIntoPage(
    //                                         $("."+cmsConstructor.kunik).parents(".custom-block-cms").data("id"),
    //                                         cmsBuilder.config.page,
    //                                         $("."+cmsConstructor.kunik).parents(".custom-block-cms").data("path"),
    //                                         $("."+cmsConstructor.kunik).parents(".custom-block-cms").data("kunik"),

    //                                         function (){
    //                                             $("."+cmsConstructor.kunik).click();
    //                                         }
    //                                     )
                                    
    //                             });
    //                         });
    //                     }
    //                 }
    //             }
    //         }
    //     } 
    // },

    /* ENTRY SPACE */
    init: function () {
        if(costum && costum.editMode){
            var topCss = ($("#headerBand").is(":visible")) ? $("#headerBand").outerHeight() : 0;
            $('.cmsbuilder-container').append(`<style>.top-${topCss}{top: ${topCss}px !important;}</style>`)
            $(".main-container #mainNav").addClass(`top-${topCss}`)
            $(".sp-text").attr("placeholder",trad.writeatexthere)
        }

        if(window.costumizer && costum && costum.editMode){
          //  this.initViews()
            this.initEvents()
            this.builder.init()
            this.layer.init()
        }
    },
    initViews: function () {
        
    },
    initEvents: function () {
        cmsConstructor.block.events.init()

        $(".cmsbuilder-center-content").off("scroll").on('scroll', function() {
            $('.sp-is-loading').each(function() {
              if ($(this).isInViewport()) {
                cmsBuilder.block.loadIntoPage($(this).data("id"),$(this).data('page'),$(this).data("path"),$(this).data("kunik"))
              }
            });
        });

        $(".cmsbuilder-center-content").off("mouseleave").on("mouseleave", function(){
            $(".block-actions-wrapper:not(.selected)").remove()
        })

        $(".main-container").off().on("click", ".editFooter" , function (e) {
            let id = $(this).data("id") 
            let ctxPage = page;           
            if ($(this).hasClass("editing-footer")) {
                cmsBuilder.config.page = ctxPage;
                // page = ctxPage;
                $(this).removeClass("editing-footer")
                $(".focus-on-footer").addClass("hidden")
                $(this).text(trad.edit+" footer")
                $(".better-footer").remove()
                let required = {
                    displayFooter : true
                }
                ajaxPost(
                    null, 
                    baseUrl+"/costum/blockcms/loadfootercms",
                    required,
                    function(data){
                        $(".footer-cms").html(data.html) 
                        var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
                        if($(".footer-cms").length > 0){
                            $(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
                        }
                    // cmsConstructor.initEvents()
                    }
                    );
            }else{
                $(this).addClass("editing-footer")
                $(this).text(trad.done)
                  var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
            '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
            '</div>';
            $(".sp-footer").hide()            
            $(".focus-on-footer").css("height",$("#all-block-container").height()+"px")
            $(".focus-on-footer").removeClass("hidden")
            $("#all-block-container").append(strNewBlc)
             var required = {
                "orderBlock" : "10000",
                "idblock" : id,
                "contextId" : costum.contextId,
                "contextType" : costum.contextType,
                "contextSlug" : costum.contextSlug,
                "page" : "allPages",
                "clienturi" : location.href,
                "path" : "",
            };
            ajaxPost(
                null, 
                baseUrl+"/costum/blockcms/loadbloccms",
                required,
                function(data){
                    let footerSection = `
                    <div 
                    id="${id}-10000"
                    class="block-footer better-footer cmsbuilder-block cmsbuilder-block-droppable sortable-container${id} block-container-container${id} custom-block-cms col-xs-12 no-padding col-lg-12 col-md-12 col-sm-12 col-xs-12 block-container-container${id}" data-blocktype="section" data-path="tpls.blockCms.superCms.container" data-id="${id}" data-kunik="container${id}" data-name="Section">
                        <div class="block-container-html">
                        ${data.html}
                        </div>
                    </div>                    `
                    $(".sample-cms").replaceWith(footerSection) 
                    $(".sp-text").attr("placeholder",trad.writeatexthere)

                    var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
                    if($(".block-footer").length > 0){
                        $(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
                        cmsConstructor.init()
                        cmsBuilder.config.page = "allPages";
                        // page = "allPages";
                    }
                    },
                {async:true}
            );
            }
        })
    },
   // helpers: {

    blocks : {
        section : {
            name: "section",
            label: tradCms.oneSection,
            image: "section.png",
            path:"tpls.blockCms.superCms.container",
            configTabs : {
                general : {
                    inputsConfig : [
                        "backgroundType",
                        "height",
                        "width",
                        "justifyContent",
                        "alignContent",
                        {
                            type : "select",
                            options : {
                                name : "targetAnchor",
                                label : tradCms.targetAnchor,
                                options : Object.keys(costum.app)
                            }
                           
                        },
                    ], 
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ],
                },
                hover: {
                    inputsConfig : [
                        "addCommonConfig"
                    ],
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                } 
            }
        },
        column : {
            label: tradCms.column,
            image: "column.png",
            name:"Colonne",
           // subtype:"supercms",
            path:"tpls.blockCms.superCms.container",
            configTabs : {
                general : {
                },
                style: {
                    common:true,
                    columnWidth : true
                },
                hover: {
                    common:true,
                    columnWidth : true
                },
                advanced : {
                    common:true,
                    targetAnchor:true
                }
            }
        },
        title:{
            label: tradCms.title,
            image : "title.png",
            name : "Titre",
            path : "tpls.blockCms.superCms.elements.title",
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type : "inputSimple",
                            options: {
                                name : "title",
                                label : tradCms.title,
                            }
                        },
                        {
                            type : "select",
                            options: {
                                name : "balise",
                                label : tradCms.selectTitleBalise,
                                options : ["h1", "h2", "h3", "h4", "h5","h6"]
                            }
                        },
                        "color", 
                        "fontSize", 
                        "fontFamily", 
                        "fontStyle",
                        "textTransform",
                        "fontWeight",
                        "textAlign",
                        "textShadow",
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ],
                },
                hover: {
                    inputsConfig : [
                    "addCommonConfig"
                    ]
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            onChange : function(path,valueToSet,name,payload,value){
                cmsConstructor.helpers.onchangeAdvanced(name,cmsConstructor.kunik,value);
                if(name=="balise"){
                    baliseToReplace=(typeof cmsConstructor.sp_params[cmsConstructor.spId].balise != "undefined" && notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].balise)) ? cmsConstructor.sp_params[cmsConstructor.spId].balise : "h1";
                    newH=document.querySelector("."+cmsConstructor.kunik).outerHTML.replaceAll(baliseToReplace, value);
                    $("."+cmsConstructor.kunik).replaceWith(newH);
                }
                if(name=="title")
                    $("."+cmsConstructor.kunik).text(value)
            },
            afterSave : function(){

            }
        },
        text : {
            label: tradCms.text,
            image: "text.png",
            name:"Texte",
            path:"tpls.blockCms.superCms.elements.supertext",
            configTabs : {
                general : {
                    inputsConfig : []
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            }
        },
        image :{
            label: tradCms.image,
            image: "image.png",
            name:"Image",
            path:"tpls.blockCms.superCms.elements.image",
            configTabs : {
                general : {
                    inputsConfig : [
                        {
							type : "inputFileImage",
							options : {
								name : "image",
								label : tradCms.uploadeImage,
                                collection : "cms"
							}
						},
                        {
                            type: "inputNumberRange",
                            options: {
                                name: "height",
                                label : tradCms.height,
                                property: "height",
                                defaultZero: "auto",
                                units: ["px", "%", "vh"],
                                views: ["desktop", "tablet", "mobile"],
                                filterValue: cssHelpers.form.rules.checkLengthProperties, 
                            }
                        },
                        "width"
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                hover: {
                    inputsConfig : [
                        "width",
                        {
                            type: "inputNumberRange",
                            options: {
                                name: "height",
                                label : tradCms.height,
                                property: "height",
                                defaultZero: "auto",
                                units: ["px", "%", "vh"],
                                views: ["desktop", "tablet", "mobile"],
                                filterValue: cssHelpers.form.rules.checkLengthProperties, 
                            }
                        },
                        "margin",
                        "padding",
                        "boxShadow",
                        "border",
                        "borderRadius"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            onChange : function(path,valueToSet,name,payload,value){
                if ( name == "image"){
                    $(".this-content-"+cmsConstructor.kunik).attr("src",value);
                }
                
            },
        },
        button :{
            label: tradCms.button,
            image: "button.png",
            name:"Button",
            path:"tpls.blockCms.superCms.elements.button",
            configTabs : {
                    general : {
                        inputsConfig : [
                        {
                            type : "inputSimple",
                            options : {
                                name : "text",
                                label : tradCms.text,
                            }
                        },
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeUrl",
                                label : tradCms.link,
                                tabs: [
                                    {
                                        value:"internalLink",  
                                        label: tradCms.internalLink,
                                        inputs : [
                                            {
                                                type : "select",
                                                name : "link",
                                                options : Object.keys(costum.app)
                                            }
                                        ]
                                    },
                                    {
                                        
                                        value:"externalLink"  , 
                                        label: tradCms.externalLink,   
                                        inputs :[
                                            {
                                                type : "inputSimple",
                                                name : "link"
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                type : "checkbox",
                                name : "targetBlank",
                                label : tradCms.openInNewTab,
                                class : "switch"
                            }
                        },
                        "width",
                        "height",
                        "color",
                        "background"
                        ]
                    },
                    style: {
                        inputsConfig : [
                            "fontSize",
                            "addCommonConfig"
                        ]
                    },
                    hover: {
                        inputsConfig : [
                            "background",
                            "color",
                            "fontSize",
                            "addCommonConfig"
                        ],
                    },
                    advanced :{
                        inputsConfig : [
                            "addCommonConfig"
                        ]
                    }
            },
            onChange: function(path,valueToSet,name,payload,value){
                if(name == "text") {
                    $("."+cmsConstructor.kunik).text(value);
                }
                /*if (name == "targetBlank") {
                    var valueTarget = (value == true) ? $("."+cmsConstructor.kunik).attr("target","_blank") : $("."+cmsConstructor.kunik).removeAttr("target");
                }*/

            }

        },
        buttonOpenModal :{
            label: "Bouton avec modal",
            image: "buttonModal.png",
            name:"Bouton avec modal",
            path:"tpls.blockCms.superCms.elements.buttonOpenModal",
            configTabs : {
                general : {
                    inputsConfig : [
                    {
                        type : "inputSimple",
                        options : {
                            name : "text",
                            label : tradCms.text,
                        }
                    },
                    {
                        type : "select",
                        options : {
                            name : "btnOpenModal",
                            label : tradCms.openModalOf,
                            options : []
                        }
                    },
                    {
                        type : "select",
                        options : {
                            name : "elementsType",
                            class : "addEltType hidden",
                            label : tradCms.elToCreate,
                            options : []
                        }
                    },
                    {
                        type : "select",
                        options : {
                            name : "addExistingPage",
                            label : tradCms.pageSlugToOpen,
                            class : "addExistingPage hidden",
                            options : []
                        }
                    },
                    "width",
                    "height",
                    "color",
                    "background"
                    ]
                },
                style: {
                    inputsConfig : [
                        "fontSize",
                        "addCommonConfig"
                        ]
                },
                hover: {
                    inputsConfig : [
                        "background",
                        "color",
                        "fontSize",
                        "addCommonConfig"
                        ],
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                        ]
                }
            },
            beforeLoad : function (){
                var eltOptions = {}
                var actionBtn = {
                    "addExistingPage" : tradCms.staticPageOrApp,
                    "contact" : trad.contact,
                    "register" : tradCms.registration,
                    "login" : trad.connection,
                    "createElement" : trad.creationElement
                }

                var modalPageOptions = {}
                var i_page = 0;
                for (const [key, value] of Object.entries(costum.app)) {
                    var btnModalPageType = value.hash
                    if (typeof btnModalPageType != "undefined") {                        
                        cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[3].options.options[i_page] = {
                            "value" : btnModalPageType.replace("#app.","")+value.urlExtra,
                            "label" : key
                        }

                    }
                    i_page++
                }

                cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[1].options.options = $.map(actionBtn, function(key, value) {
                  return {
                    label: key.toUpperCase(),
                    value: value
                }})
                for (const [key, value] of Object.entries(typeObj)) {
                    if(typeof value != "function" && typeof value.add != "undefined"){ 
                        if (value.add == true) {          
                            eltOptions[key] = (value.name).toUpperCase();                  
                        }               
                    }
                    cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[2].options.options = $.map(eltOptions, function(key, value) {
                      return {
                        label: key,
                        value: value
                    }})
                }
                if (cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "createElement") 
                    cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[2].options.class = 'addEltType capitalize'
                else              
                    cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[2].options.class = 'addEltType capitalize hidden'

                if (cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "addExistingPage") 
                    cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[3].options.class = 'addExistingPage'
                else              
                    cmsConstructor.blocks.buttonOpenModal.configTabs.general.inputsConfig[3].options.class = 'addExistingPage hidden'
            },
            onChange: function(path,valueToSet,name,payload,value){
                if(name == "text") {
                    $("."+cmsConstructor.kunik).text(value);
                }
                if (name == "btnOpenModal") {
                    if (value == "createElement") 
                        $(".addEltType").removeClass("hidden")
                    else
                        $(".addEltType").addClass("hidden")  

                    if (value == "addExistingPage") 
                        $(".addExistingPage").removeClass("hidden")
                    else
                        $(".addExistingPage").addClass("hidden")
                }

            }

        },
        video : {
            label: tradCms.video,
            image: "video.png",
            name:"Video",
            path:"tpls.blockCms.superCms.elements.video",
            configTabs : {
                general : {
                    inputsConfig :[
                        {
                            type : "inputSimple",
                            options : {
                                type : "text",
                                name : "link",
                                label : tradCms.link
                            },
                        },
                        "width",
                        "height",
                        {
                            type : "inputSimple",
                            options : {
                                type : "checkbox",
                                name : "autoplay",
                                label : tradCms.autoplay,
                                class : "switch"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                type : "checkbox",
                                name : "repeat",
                                label : tradCms.repeat,
                                class : "switch"
                            },
                        }
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            onChange: function(path,valueToSet,name,payload,value){
                if (name == "link"){
                    var videoId = ""
                    if (value.includes("=") == true) {
                        videoId = value.split("=").pop();
                    } else {
                        videoId = value.split("/").pop();
                    }

                    if (value.includes("vimeo") == true) {
                        $src = "https://player.vimeo.com/video/"+videoId;
                    } else if (value.includes("youtu") == true) {
                        $src = "https://www.youtube.com/embed/"+videoId;
                    } else if (value.includes("dailymotion") == true) {
                        $src = "https://www.dailymotion.com/embed/video/"+videoId;
                    } else if (value.includes("indymotion") == true) {
                        $src = "https://indymotion.fr/videos/embed/w/"+videoId;
                    }
                    $("."+cmsConstructor.kunik).attr("src",$src)
                }
            }
        },
        icon : {
            label: tradCms.icon,
            image: "icon.png",
            name:"icon",
            path:"tpls.blockCms.superCms.elements.icon",
            configTabs : {
                    general : {
                        inputsConfig : [
                            "color", 
                            "background",
                            "fontSize", 
                            {
                             type: "inputIcon",
                             options: {
                                name: "iconStyle",
                                label: tradCms.icon,
                             }
                            }
                        ]
                    },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                hover: {
                    inputsConfig : [
                        "background",
                        "color",
                        "addCommonConfig"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            onChange: function(path,valueToSet,name,payload,value){
                    if (name == "iconStyle"){
                        $('.' + cmsConstructor.kunik).removeClass(function (index, css) {
                        return (css.match(/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
                        });
                        
                        $('.' + cmsConstructor.kunik).addClass("fa fa-" + value)
                    }
                    
            }
        },
        lineSeparator : {
            label: tradCms.separator,
            image: "separator.png",
            name:"Separator",
            path:"tpls.blockCms.superCms.elements.lineSeparator",
            configTabs : {
                general : {
                        inputsConfig : [
                            {
                                type : "inputSimple",
                                options : {
                                    type : "checkbox",
                                    name : "positionTop",
                                    label : tradCms.lineSeparatorPositionTop,
                                    class : "switch"
                                }
                            },
                            "backgroundColor",
                            "height",
                            "width",
                            {
                                type : "section",
                                options : {
                                    name : "icon",
                                    label: tradCms.icon,
                                    inputs : [
                                        {
                                            type : "inputSimple",
                                            options : {
                                                type : "checkbox",
                                                name : "display",
                                                label : tradCms.icon+" "+tradCms.display,
                                                class : "switch"
                                            }
                                        },
                                        {
                                            type: "inputIcon",
                                            options: {
                                               name: "iconStyle",
                                               label: tradCms.icon,
                                            },
                                        },
                                        {
                                            type: "fontSize",
                                            options: {
                                               name: "fontSize",
                                               label: tradCms.size,
                                            },
                                        },
                                        {
                                            type: "color",
                                            options: {
                                               name: "color",
                                               label: tradCms.color,
                                            },
                                        },
                                        {
                                            type: "background",
                                            options: {
                                               name: "background",
                                               label: tradCms.background,
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "radius",
                                                label: tradCms.radius,
                                                minRange: 0,
                                                maxRange: 100,
                                                units: ["px"],
                                                filterValue: cssHelpers.form.rules.checkLengthProperties,                            
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "marginTop",
                                                label: tradCms.vertical,
                                                minRange: -100,
                                                maxRange: 100,
                                                units: ["px"],
                                                filterValue: cssHelpers.form.rules.checkLengthProperties
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "marginLeft",
                                                label: tradCms.horizontal,
                                                minRange: -100,
                                                maxRange: 100,
                                                units: ["px","%"],
                                                filterValue: cssHelpers.form.rules.checkLengthProperties,                                    
                                            }
                                        },
                                        // "padding"
    
                                    ],
                                }
                            },

                        ]
                    },
                    style: {
                        inputsConfig :["addCommonConfig"]
                    },
                    advanced :{
                        inputsConfig :["addCommonConfig"]
                    }
            },
            onChange: function(path,valueToSet,name,payload,value){

                if (name == "positionTop") {
                    $("#wrapperforLineSeparator"+cmsConstructor.kunik).toggleClass("bottom-lineSeparator"+cmsConstructor.kunik);  
                    $("#wrapperforLineSeparator"+cmsConstructor.kunik).toggleClass("top-lineSeparator"+cmsConstructor.kunik); 
                }

                if (name == "iconStyle"){
                    $('.' + cmsConstructor.kunik +"-icon").removeClass(function (index, css) {
                        return (css.match(/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
                    });
                    
                    $('.' + cmsConstructor.kunik +"-icon").addClass("fa fa-" + value)
                }
            }
        },
        blockCms : {
            name: "blockcms",
            label: tradCms.blockcms,
            image: "cms.png"
        },
    },
    commonEditor:{
        general : [
        ],
        style : [
            "margin", 
            "padding",
            "boxShadow",
            "border",
            "borderRadius"
        ],
        hover : [
            "width",
            "height",
            "margin",
            "padding",
            "boxShadow",
            "border",
            "borderRadius"
        ],
        advanced : [
            {
                type : "textarea",
                options : {
                    type : "text",
                    name : "otherClass",
                    label : tradCms.otherClass,
                }
            },
            {
                type : "textarea",
                options : {
                    type : "text",
                    name : "otherCss",
                    label : tradCms.otherCss,
                }
            },
            "hideOnDesktop", 
            "hideOnTablet",
            "hideOnMobil",
            {
                type : "select",
                options: {
                    name : "balise",
                    label : tradCms.selectTitleBalise,
                    options : ["div", "section"]
                }
            },
        ]
    }, 
    /* SPACES TO ADD*/
    builder: {
        init:function(){
            costumizer.actions.tabs.init(
                "#left-panel",
                [
                    {
                        key: "blocks",
                        icon: "object-group",
                        label: tradCms.blocks,
                        content: this.views.init()
                    },
                    {
                        key: "layers",
                        icon: "list-ul",
                        label: tradCms.layers,
                        content: cmsConstructor.layer.views.getContainer()
                    }
                ]
                
            );

            this.events.init()
        },
        views: {
            init: function () {
                var $html = $(`<div class="cmsbuilder-block-section"></div>`)

                var $blocksContainerSection = $(`
					<div class="cmsbuilder-block-section-item">
						<div class="cmsbuilder-block-section-label">${tradCms.container}</div>
						<div class="cmsbuilder-block-list"></div>
					</div>
				`);
                var containerArray=["section", "column"]
                containerArray.forEach(function (k) {
                    block=cmsConstructor.blocks[k];
                    $blocksContainerSection.find(".cmsbuilder-block-list").append(`
						<div class="cmsbuilder-block-list-item" data-name="${block.name}" data-path="${block.path}" data-subtype="${block.subtype}" draggable="true">
							<img src="${assetPath}/cmsBuilder/img/blocks/${block.image}" alt="" draggable="false">
							<span>${block.label}</span>
						</div>
					`)
                })
                $html.append($blocksContainerSection)

                var $blocksContentsSection = $(`
					<div class="cmsbuilder-block-section-item">
						<div class="cmsbuilder-block-section-label">${tradCms.components}</div>
						<div class="cmsbuilder-block-list"></div>
					</div>
				`)
                var contentsArray=["title", "text", "image", "video", "button", "buttonOpenModal", "icon", "lineSeparator", "blockCms"]
                contentsArray.forEach(function (k) {
                     block=cmsConstructor.blocks[k];
                    $blocksContentsSection.find(".cmsbuilder-block-list").append(`
						<div class="cmsbuilder-block-list-item" data-name="${block.name}" data-path="${block.path}" data-subtype="${block.subtype}" draggable="true">
							<img src="${assetPath}/cmsBuilder/img/blocks/${block.image}" alt="" draggable="false">
							<span>${block.label}</span>
						</div>
					`)
                })
                $html.append($blocksContentsSection)

                return $html;
            },
            dropzone : {
                addContainer:function(){
                    $(".block-cms-dropzone").remove()
                    //add column dropzone
                    $(".cmsbuilder-block:not([data-blockType='element'])").each(function(){
                        var dropzoneContainer = $(`
                            <div class="block-cms-dropzone block-cms-dropzone-container" data-kunik="${$(this).data("kunik")}">
                                <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                            </div>
                        `)

                        $(this).prepend(dropzoneContainer)
                    })
                },
                addPage:function(){
                     $(".block-cms-dropzone").remove()


                    if($(".pageContent #all-block-container").children().length < 1){
                        $(".pageContent").append(`
                            <div class="block-cms-dropzone block-cms-dropzone-page" data-position="in" style="height:${$(".pageContent").height()}px !important;">
                                <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                            </div>
                        `)
                    }else{
                        //add section dropzone
                        $(".pageContent #all-block-container > .cmsbuilder-block[data-blockType='section']").each(function(index){
                            var getDropzone = function(id, kunik, position){
                                return (`
                                    <div class="block-cms-dropzone block-cms-dropzone-page" data-position="${position}" data-id="${id}" data-kunik="${kunik}">
                                        <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                                    </div>
                                `)
                            }

                            if (cmsConstructor.helpers.isInViewport(this,1000)){
                                $(this).before(getDropzone($(this).data("id"), $(this).data("kunik"), "before"))
                                if((index+1)=== $(".cmsbuilder-block[data-blockType='section']").length)
                                    $(this).after(getDropzone($(this).data("id"), $(this).data("kunik"), "after"))
                            }
                        })
                    }
                }
               
            }
        },
        actions : {
            /**
                * 
             * @param {object} parent
             * @param {string} parent.id
             * @param {string} parent.kunik 
             * @param {object} block 
             * @param {string} block.path
             * @param {string} block.name
             * @param {string} block.subtype 
             */
            addBlock: function(parent, block){
                var order = 0,
                    id = parent.id,
                    kunik = parent.kunik,
                    path = block.path,
                    name = block.name,
                    subtype = block.subtype;
                $(".empty-sp-element"+kunik).addClass("d-none"); 
                // $(".whole-"+kunik).append('<div class="'+kunik+'-load-sp sp-is-loading" style="width: 100%;min-height: 30px;height: 100%;position: absolute;"></div>')
                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                        '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                    '</div>';
                $(".cmsbuilder-block-container[data-id="+id+"]").append(strNewBlc);
                // if (typeof $(".whole-"+kunik+" .super-cms:first-child").css("order") !== "undefined") {
                //     order = $(".whole-"+kunik+" .super-cms:first-child").css("order")+1;
                // }
                coInterface.initHtmlPosition();
                var tplCtx = {
                    collection:"cms",
                    value:{
                        path:path,
                        page:page,
                        order:order,
                        blockParent:id,
                        //type:"blockChild",
                        parent:{}
                    }
                }
                tplCtx.value.parent[costum.contextId] = {
                    type: costum.contextType,
                    name: costum.contextName
                };

                dataHelper.path2Value(tplCtx, function(params){
                    var newChildId = params.saved.id;
                    ajaxPost(
                        null,
                        baseUrl+'/co2/cms/refreshblock',
                        {
                            idBlock: newChildId
                        },
                        function(response){
                            $(".sample-cms").replaceWith(response.view)
                            cmsBuilder.block.initEvent();
                            cmsBuilder.block.updateOrder();
                            toastr.success(tradCms.elementwelladded);
                        }
                    )
                   // cmsChildren[id].push(newChildId);
                    // var parentId = $("." + kunik).parents(".custom-block-cms").data("id");
                    // var parentPath = $("." + kunik).parents(".custom-block-cms").data("path");
                    // var parentKunik = $("." + kunik).parents(".custom-block-cms").data("kunik");
                    // cmsBuilder.block.loadIntoPage(parentId, page, parentPath, parentKunik);
                })
            },
            insertBlock: function(position="in", kunik=null){
                localStorage.removeItem("parentCmsIdForChild");
                localStorage.setItem("addCMS", true);
                $(".sample-cms").remove();
                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                        '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                    '</div>';
                switch(position){
                    case "in":
                        $("#all-block-container").append(strNewBlc)
                    break;
                    case "before":
                        $(strNewBlc).insertBefore(`.block-container-${kunik}`)
                    break;
                    case "after":
                        $(strNewBlc).insertAfter(`.block-container-${kunik}`)
                    break;
                };
                ajaxPost(
                    null,
                    baseUrl+"/co2/cms/insertblocksection",
                    {   
                        "page":cmsBuilder.config.page
                    }, 
                    function (response) {
                        $(".sample-cms").replaceWith(response.html);
                        cmsBuilder.block.initEvent(response.params._id.$id, response.params._id.$id);
                        cmsBuilder.block.updateOrder();
                        coInterface.initHtmlPosition();
                        toastr.success(tradCms.elementwelladded);
                    }
                );
            },
            sortBlock:function($el, action){
                var sendData = false;
                var kunik = $el.attr("data-kunik");
                var $cloneBlock = $el.clone(true);
                if($el.hasClass("cmsbuilder-block")){
                    if((action == "move_up" && $el.prevAll().filter(".cmsbuilder-block").length != 0) || (action == "move_down" && $el.nextAll().filter(".cmsbuilder-block").length != 0)){
                        var parents = $el.parent();
                        if(action == "move_up")
                            $el.prevAll().filter(".cmsbuilder-block").first().before($cloneBlock)
                        else
                            $el.nextAll().filter(".cmsbuilder-block").first().after($cloneBlock)

                        cmsConstructor.block.events.actions.bindOnclickActionItem()

                        $el.remove()

                        coInterface.scrollTo($el.attr("id"))

                        var params = {
                            id : costum.contextId,
                            collection : costum.contextType,
                            idAndPosition : []
                        };
                        parents.children(".cmsbuilder-block").each(function(){
                            params.idAndPosition.push($(this).data("id"));
                        });
                        sendData = true;
                    }
                }else{
                    if((action == "move_up" && $el.prev(".custom-block-cms").length != 0) || (action == "move_down" && $el.next(".custom-block-cms").length != 0)){
                        if(action == "move_up")
                            $el.prev(".custom-block-cms").before($cloneBlock)
                        else
                            $el.next(".custom-block-cms").after($cloneBlock)

                        cmsConstructor.block.events.actions.bindOnclickActionItem()

                        $el.remove()

                        coInterface.scrollTo($el.attr("id"))

                        var params = {
                            id : costum.contextId,
                            collection : costum.contextType,
                            idAndPosition : []
                        };

                        $(".custom-block-cms").each(function(){
                            params.idAndPosition.push($(this).data("id"));
                        });
                        sendData = true;
                    }
                }

                if(sendData){
                    ajaxPost(
                        null,
                        baseUrl+"/costum/blockcms/dragblock",
                        params,
                        function(data){
                            cmsConstructor.layer.init()
                            if ($(".cmsbuilder-left-content").hasClass("active")){
                                cmsConstructor.layer.actions.selectPath(kunik)
                            }
                        }
                    );
                }
            },
            deleteBlock: function(callback=null){
                var id = cmsConstructor.spId;
                ajaxPost(
                    null,
                    `${baseUrl}/co2/cms/delete/id/${id}`,
                    null,
                    function(data) {
                        if (data && data.result) {
                            var idparent = $("[data-id="+id+"]").parents('.custom-block-cms').data('id');
                            var kunik = $("[data-id="+id+"]").parents(".custom-block-cms").data("kunik");
                            if(idparent == id){
                                toastr.success("Element effacé");
                                $('.block-container-'+kunik).remove();
                            }else{
                                var parents = $("[data-id="+id+"]").parent();
                                var kunikparents = parents.data("kunik");
                                var imageSrc = $('.'+kunikparents).css('background-image');
                                var backgroundColor = $('.'+kunikparents).css('background-color');
                                var height = (typeof cmsConstructor.sp_params[idparent] != "undefined" && typeof cmsConstructor.sp_params[idparent].css != "undefined" && typeof cmsConstructor.sp_params[idparent].css.height != "undefined") ? cmsConstructor.sp_params[idparent].css.height : "";
                                $("[data-id="+id+"]").remove();
                                if( (imageSrc == "none" || backgroundColor == "none") && parents.children(".cmsbuilder-block").length == 0 ){
                                    $(".empty-sp-element"+kunikparents).removeClass("d-none");
                                }else{
                                    if(imageSrc != "none" && parents.children(".cmsbuilder-block").length == 0 ){
                                        if( height == ""){
                                            parents.css("height","250px");
                                        }
                                    }
                                }   
                                
                                // var path = $("[data-id="+id+"]").parents(".custom-block-cms").data("path");
                                // cmsBuilder.block.loadIntoPage(idparent, page, path, kunik);
                            } 
                            $(".cmsbuilder-right-content").removeClass("active")
                            $("#right-panel").html("")
                            $(".block-actions-wrapper").removeClass("selected")
                            cmsConstructor.layer.init()
                            coInterface.initHtmlPosition();
                        } else {
                            toastr.error("something went wrong!! please try again.");
                        }
    
                        if(callback && typeof callback === "function")
                            callback(data)
                    },
                    null,
                    "json"
                );
            }
        },
        events:{
            init:function(){
                this.dragAndDropBlock();
                this.modalBlockCms()
            },
            dropzone :{ 
                launch : function(){
                    $(".cmsbuilder-block-droppable").droppable({
                        greedy:true,
                        over:function(e){
                            $(".block-cms-dropzone").removeClass("dragover")
                            $(this).find("> .block-cms-dropzone").addClass("dragover")
                        },
                        out:function(){
                            $(this).find("> .block-cms-dropzone").removeClass("dragover")
                        },
                        drop:function(e, ui){
                            e.stopPropagation()
                            
                            var blockName = $(ui.draggable).data("name"),
                                parentType = $(e.target).data("blocktype");

                            if(blockName == "blockcms"){
                                $("#modal-blockcms").addClass("open")
                                $("#blocklist-search-text").parent().show()
                                 $(".cmsbuilder-right-content").removeClass("active")
                                 cmsConstructor["blockParent"] = $(e.target).data("id")
                                localStorage.setItem("parentCmsIdForChild", $(e.target).data("id"));
                            }else if(parentType == "section" && blockName == "section"){
                                bootbox.alert({
                                    message: `<br>
                                    <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                    <p class="padding-left-20">${tradCms.alertSecinSec}</p>
                                    </div>`
                                    ,
                                    size: 'medium'
                                });
                            }else if(parentType == "column" && blockName == "section"){
                                bootbox.alert({
                                    message: `<br>
                                    <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                    <p class="padding-left-20">${tradCms.alertSecinColumn}</p>
                                    </div>`
                                    ,
                                    size: 'medium'
                                });
                            }else{
                                var parent = {
                                    id:$(e.target).data("id"),
                                    kunik:$(e.target).data("kunik")
                                }
                                var block = {
                                    name: blockName,
                                    subtype: $(ui.draggable).data("subtype"),
                                    path: $(ui.draggable).data("path")
                                }
                                
                                cmsConstructor.builder.actions.addBlock(parent, block)
                            }
                        }
                    })
                    $(".block-cms-dropzone-page").droppable({
                        greedy:true,
                        hoverClass:"dragover",
                        drop:function(){
                            cmsConstructor.builder.actions.insertBlock($(this).data("position"), $(this).data("kunik"))
                        }
                    })
                }
            },
            dragAndDropBlock: function(){
                var autoScroller = cmsConstructor.helpers.getAutoScroller(".cmsbuilder-center-content")
                $(".cmsbuilder-block-list-item").draggable({
                    containment:"document",
                    scroll:true,
                    zIndex:999999999,
                    helper:"clone",
                    start:function(){
                        cmsConstructor.dragstart = true;

                        $(".block-actions-wrapper").remove()

                        var name = $(this).data("name")

                        if(name === "section"){
                            cmsConstructor.builder.views.dropzone.addPage()
                        }else{
                            //add space and border to column block on drag start
                            $(".cmsbuilder-block-droppable").each(function() {
                                var $element = $(this);
                                if (cmsConstructor.helpers.isInViewport($element[0],0)) {
                                  $element.addClass("dragstart");
                                }
                              });
                              
                            cmsConstructor.builder.views.dropzone.addContainer()
                        }
                    
                        cmsConstructor.builder.events.dropzone.launch()
                    },
                    stop:function(){
                        cmsConstructor.dragstart = false
                        //remove space and border to column block on drag end
                        $(".cmsbuilder-block-droppable").removeClass("dragstart")
                        $(".block-cms-dropzone").remove()
                  
                        //cmsConstructor.builder.events.dropzone.remove()
                        autoScroller.stop()
                    },
                    drag: function(e){
                        autoScroller.start(e)
                    }
                })
            },
            modalBlockCms: function(){
                var filterListBlocks = function(){
                    var type = $("#blocklist-dropdown-menu li.selected").data("type"),
                        text = $("#blocklist-search-text").val().toLowerCase()

                    $(".modal-blockcms-list-item").filter(function(){
                        $(this).toggle(
                            (($(this).data("type") == type) || (type=="all")) &&
                            ((text.replaceAll(" ", "") === "") || ($(this).data("name").toLowerCase().indexOf(text) > -1))
                        )
                    })
                }

                $("#blocklist-dropdown-menu li").off("click").on("click", function(){
                    var type = $(this).data("type");
                    $("#blocklist-dropdown-menu li").removeClass("selected")
                    $(this).addClass("selected")

                    $("#blocklist-dropdown-toggle .btn-label").text((type == "all") ? "Tout les blocks":type)

                    filterListBlocks()
                })

                var typingTimer = null
                $("#blocklist-search-text").off("keyup").on("keyup", function(){
                    clearTimeout(typingTimer)
                    setTimeout(function(){  
                        filterListBlocks()
                    }, 100)
                })

                $("#btn-hide-modal-blockcms").off("click").on("click", function(){
                    $("#modal-blockcms").removeClass("open")
                })

                $(".modal-blockcms-list-item").off("click").on("click", function(e){
                    if(cmsConstructor["blockParent"] == "tpl-footer"){
                        $(".sample-cms").remove();
                        var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                        '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                        '</div>';
                        $(`.whole-container${cmsConstructor["blockParent"]}`).append(strNewBlc)
                        $(`.empty-sp-elementcontainer${cmsConstructor["blockParent"]}`).remove()
                        cmsBuilder.block.duplicate($(this).data('id'),"", "dplFooter");
                    }else{                      
                        $(".sample-cms").remove();
                        var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                        '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                        '</div>';
                        $(`.whole-container${cmsConstructor["blockParent"]}`).append(strNewBlc)
                        $(`.empty-sp-elementcontainer${cmsConstructor["blockParent"]}`).remove()
                        cmsBuilder.block.duplicate($(this).data('id'), cmsConstructor["blockParent"]);
                    }
                    $("#modal-blockcms").removeClass("open")
                    e.stopImmediatePropagation()
                    e.stopPropagation()
                })

                $(".modal-blockcms-list-item .deleteCms").off().on("click", function(e) {
                    e.stopImmediatePropagation()
                    e.stopPropagation()
                    var id = $(this).data("id");
                    $("#modal-blockcms").removeClass("open");
                    bootbox.confirm(`<div role="alert">
                        <p><b class="">Supprimer le bloc</b> <b class="text-red">`+$(this).data("name")+`</b> <b class="">pour toute l'éternité?</b><br><br>
                        Il ne peut pas être récupéré une fois supprimé.<br>
                        </p> 
                        </div> `,
                        function(result) {
                            if (!result) {
                                $("#modal-blockcms").addClass("open")
                                return;
                            } else {
                                var type = "cms";
                                var urlToSend = baseUrl + "/co2/cms/delete/id/" + id;
                                ajaxPost(
                                    null,
                                    urlToSend,
                                    null,
                                    function(data) {
                                        if (data && data.result) {
                                            toastr.success("Elément effacé");
                                            window.location.reload()
                                        } else {
                                            toastr.error("something went wrong!! please try again.");
                                        }
                                    },
                                    null,
                                    "json"
                                    );
                            }
                        }
                        );
                });
                
                $(".modal-blockcms-list-item .editCMS").on("click", function(e) {
                    e.stopImmediatePropagation()
                    e.stopPropagation()
                    var tplId = $(this).data("id");
                    var tplCtx = {};

                    var dynFormCostum = {
                        "beforeBuild": {
                            "properties": {
                                image: dyFInputs.image(),
                                path: {
                                    label: "Chemin",
                                    inputType: "text",
                                    order: 2
                                }
                            }
                        },
                        "onload": {
                            "Title": "Modifier le cms",
                            "actions": {
                                "html": {
                                    "nametext>label": "Nom du bloc",
                                    "infocustom": ""
                                },

                                "hide": {
                                    "documentationuploader": 1,
                                    "structagstags": 1
                                }
                            }
                        },
                        afterSave: function() {
                            dyFObj.commonAfterSave(params, function() {
                                window.location.reload();
                            });

                        }
                    };
                    dyFObj.editElement("cms", tplId, null, dynFormCostum);

                });
            }
        }
    },
    layer: {  
        actions:{
            selectPath:function(kunik){
                $(".page-layer-item-btn").removeClass("selected")
                $(`.page-layer-item-btn[data-kunik="${kunik}"]`).addClass("selected")
                $(".page-layer-item").removeClass("active")
                $(`.page-layer-item-btn[data-kunik="${kunik}"]`).parents(".page-layer-item").addClass("active")
            }
        },
        init:function(){
            this.views.init()
            this.events.init()
        },
        views: {
            init:function(){
                $("#page-layer-container").html(cmsConstructor.layer.views.getLayer($("#all-block-container")))
            },
            getLayer : function($el){
                var $layer = $(`<ul class="page-layer-item-children"></ul>`)
                if($el.children(".cmsbuilder-block").length > 0){
                    $el.children(".cmsbuilder-block").each(function(){
                        var kunik = $(this).data("kunik"),
                            blocktype = $(this).data("blocktype"),
                            name = $(this).data("name"),
                            nameTrad = name == "image" ? tradCms.image : name == "button" ? tradCms.button : name == "text" ? tradCms.text : name == "separator" ? tradCms.separator : name ,
                            path = $(this).data("path")
                            isBlockContinainer = ((blocktype=="section" && path=="tpls.blockCms.superCms.container") || blocktype=="column");

                        var $li = $(`<li class="page-layer-item"></li>`)

                        $li.append(`
                            <div class="page-layer-item-btn" data-blocktype="${blocktype}" data-kunik="${kunik}" data-label="${name}">
                                <div>
                                    <span><i class="fa fa-${ blocktype=='section'?'object-group':'columns' }" aria-hidden="true"></i></span>
                                    ${nameTrad}
                                </div> 
                                ${ isBlockContinainer ? `<span><i class="fa fa-caret-down" aria-hidden="true"></i></span>`:""}
                            </div>
                        `)

                        var $children = cmsConstructor.layer.views.getLayer($(this));
                        if($children.find("li").length > 0)
                            $li.append($children)

                        $layer.append($li)
                    })
                }else if($el.children(".block-container-html").length > 0)
                    $layer = cmsConstructor.layer.views.getLayer($($el.children(".block-container-html")[0]))
                else if($el.children(".cmsbuilder-block-container").length > 0)
                    $layer = cmsConstructor.layer.views.getLayer($($el.children(".cmsbuilder-block-container")[0]))
    
                return $layer;
            },
            getArbre : function($el){
                var $layer = $(`<ol></ol>`);
                if($el.children(".cmsbuilder-block").length > 0){
                    $el.children(".cmsbuilder-block").each(function(){
                        var kunik = $(this).data("kunik"),
                            blocktype = $(this).data("blocktype"),
                            name = $(this).data("name"),
                            nameTrad = name == "image" ? tradCms.image : name == "button" ? tradCms.button : name == "text" ? tradCms.text : name == "separator" ? tradCms.separator : name == "une section" ? tradCms.oneSection : name,
                            path = $(this).data("path")
                            isBlockContinainer = ((blocktype=="section" && path=="tpls.blockCms.superCms.container") || blocktype=="column");
                        var $li = $(`<li>${nameTrad}</li>`)

                        var $children = cmsConstructor.layer.views.getArbre($(this));
                        if($children.find("li").length > 0)
                            $li.append($children)
                        $layer.append($li)
                    })
                }else if($el.children(".block-container-html").length > 0)
                    $layer = cmsConstructor.layer.views.getArbre($($el.children(".block-container-html")[0]))
                else if($el.children(".cmsbuilder-block-container").length > 0)
                    $layer = cmsConstructor.layer.views.getArbre($($el.children(".cmsbuilder-block-container")[0]))
    
                return $layer;
            },
            getContainer: function(){
                return `<div id="page-layer-container" style="width:100%; height:100%; color:white;"></div>`
            }
        },
        events:{
            init:function(){
                $(".page-layer-item-btn").off("click").on("click", function(e){
                    e.stopPropagation();

                    $(this).closest(".page-layer-item").toggleClass("active")
                    $(".page-layer-item-btn").removeClass("selected")
                    $(".block-actions-wrapper").removeClass("selected")
                    $(this).addClass("selected")

                    var kunik = $(this).data("kunik"),
                        $target = $(`.cmsbuilder-block[data-kunik="${kunik}"]`).first();
                    cmsConstructor.block.actions.addActions($target, true)
                    cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $target);
                })
            }
        }
    },
    editor: {
        init:function(){
            this.views.init()
            this.events.init()
        },
        actions:{
            generateCoInputs : function(kunik, spId, values , blockType , tabKey){
                var inputsForm=[];
                if (values == null){
                    values = {}
                    values['icon'] = {}
                    values['css'] = {}
                }else if (typeof values.css == 'undefined') {
                    values.css = {}
                }
                if(typeof cmsConstructor.blocks[blockType].beforeLoad == "function"){
                    cmsConstructor.blocks[blockType].beforeLoad();    
                }

                if(cmsConstructor.blocks[blockType].configTabs[tabKey]["inputsConfig"].indexOf("addCommonConfig") >= 0){
                    sliceIndex=cmsConstructor.blocks[blockType].configTabs[tabKey]["inputsConfig"].indexOf("addCommonConfig");
                    inputsForm=cmsConstructor.blocks[blockType].configTabs[tabKey]["inputsConfig"].slice(0, sliceIndex).concat(cmsConstructor.commonEditor[tabKey], cmsConstructor.blocks[blockType].configTabs[tabKey]["inputsConfig"].slice(sliceIndex));
                }else{
                    inputsForm=cmsConstructor.blocks[blockType].configTabs[tabKey]["inputsConfig"]; 
                }
                var onchange = function(path,valueToSet,name,payload,value){ 
                    var newKey = jsonHelper.getValueByPath(payload, tabKey+"."+name+".path");
                    var keyPath = typeof cmsConstructor.blocks[blockType].configTabs[tabKey]["keyPath"] != "undefined" ? cmsConstructor.blocks[blockType].configTabs[tabKey]["keyPath"]+"." : "";
                    if (typeof newKey != "undefined"){
                        path = newKey;
                    } else if ( path == ""){
                        path = keyPath+name;
                    } else {
                        if( typeof cssHelpers.json[name] != "undefined"){
                            if (tabKey == "hover"){
                                path = "css.hover"+path;
                            } else {
                                if (typeof payload.sectionPath != "undefined")
                                    path = "css."+path;
                                else 
                                    path = "css"+path;
                            }
                        } else {
                            if (typeof payload.sectionPath != "undefined"){
                                path += "."+name;
                                path = keyPath+path;
                            }
                            else 
                                path = name;
                        }
                    }
                    

                    if (name === "display"){
                        value == true ? value = "block" : value = "none";
                    }


                    if( tabKey == "advanced"){
                        cmsConstructor.helpers.onchangeAdvanced(name,cmsConstructor.kunik,value);
                    }

                    if( typeof cssHelpers.json[name] != "undefined" ){
                        cmsConstructor.editor.actions.onChangeCss(path,valueToSet,name,payload,value,tabKey);
                    }
                    else if(typeof cmsConstructor.blocks[blockType].onChange == "function"){
                        cmsConstructor.blocks[blockType].onChange(path,valueToSet,name,payload,value);    
                    }


                    if( path != "advanced.otherCss"){
                        cmsConstructor.editor.actions.updateBlock(blockType,path,valueToSet,name,payload,value,tabKey);
                    } else if ( path === "advanced.otherCss" && cmsConstructor.helpers.validateOtherCss(".newCss-" + kunik + " {" + value + "}",true) == true){
                        cmsConstructor.editor.actions.updateBlock(blockType,path,valueToSet,name,payload,value,tabKey);
                    }

                    
                };
                
                var payload = this.setPayload(inputsForm,tabKey)
                var defaultValue = this.setDefaultValueCoInput(inputsForm, values, tabKey , payload)
                cssHelpers.form.launch("#"+tabKey,inputsForm,payload,defaultValue,{},onchange);
            },
            onChangeCss: function(path,valueToSet,name,payload,value,tabKey){
                var classTarget = cmsConstructor.kunik ;
                var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId);
                if (path.includes("icon")){
                    classTarget = cmsConstructor.kunik+'-icon' ;
                }

                if ( tabKey != "hover"){
                    if (typeof value == "object") {
                        $.each(value, function(keyChange, valueChange) {
                            cssHelpers.render.addStyleUI(classTarget+"."+keyChange,valueChange)
                        })
                    } else {
                        cssHelpers.render.addStyleUI(classTarget+"."+name,value)
                    }
                } 
                if ( blockType == "section" ) {
                    if ( name == "backgroundType"){
                        if ( value == "backgroundColor"){
                            $('.'+classTarget).css('background-image' , 'none');
                            if ( typeof cmsConstructor.sp_params[cmsConstructor.spId].css.backgroundColor != "undefined" ){
                                $('.'+classTarget).css('background-color' , cmsConstructor.sp_params[cmsConstructor.spId].css.backgroundColor);
                            } 
                        }else {
                            if ( typeof cmsConstructor.sp_params[cmsConstructor.spId].css.backgroundImage != "undefined"){
                                cssHelpers.json[value].input.options.forEach(element => {
                                    if ( typeof cmsConstructor.sp_params[cmsConstructor.spId].css[element] != "undefined"){
                                        $('.'+classTarget).css(cssHelpers.json[element].property , cmsConstructor.sp_params[cmsConstructor.spId].css[element]);
                                    }
                                });
                            }
                        }
                        
                    } 

                    if ( name == "backgroundColor" || name == "backgroundImage" ){
                    
                        height = cmsConstructor.sp_params[cmsConstructor.spId].css.height
                        if ($('.empty-sp-element'+cmsConstructor.kunik).hasClass("d-none") == false){
                                $('.empty-sp-element'+cmsConstructor.kunik).addClass("d-none");
                                if (typeof cmsConstructor.sp_params[cmsConstructor.spId].blockParent == "undefined" && (typeof height != "undefined" || $('.'+classTarget).css('height') != "max-content")){
                                    $('.'+classTarget).css('height' , '250px');
                            }
                        }
                    
                    } 
                }

                
                  

                
            },
            updateBlock:function(blockType,path,valueToSet,name,payload,value,tabKey){
                tplCtx = {};
                tplCtx.id = cmsConstructor.spId;
                tplCtx.collection = "cms";
                tplCtx.value = {};
                tplCtx.path =  path;

                var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId)


                var keypath = tabKey == 'hover' ? "css.hover." : "css.";
    
                if (typeof value != "object"){
                    tplCtx.value = value;
                    jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], tplCtx.path, value);
                    if (name == "iconStyle"){
                        tplCtx.value = "fa fa-"+value;
                        jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], tplCtx.path, "fa fa-"+value);
                    } 
                } 
                else if (typeof value == "object"){
                    if( typeof cssHelpers.json[name] != "undefined" ){

                        tplCtx.path = "css" ;

                        if ( blockType == "lineSeparator" && tabKey == "general" ){
                            tplCtx.path = "css.icon";
                            keypath = "css.icon.";
                        }
                        
                        $.each(value, function(key,val){
                            jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], keypath+key, val);
                        });
                        tplCtx.value = cmsConstructor.sp_params[cmsConstructor.spId].css;
                    } else {
                        tplCtx.value = value;
                        jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], tplCtx.path, value);
                    }
                }

                dataHelper.path2Value(tplCtx, function (params) { 
                    toastr.success(tradCms.editionSucces); 
                        if(typeof cmsConstructor.blocks[blockType].afterSave == "function")
                            cmsConstructor.blocks[blockType].afterSave(path,valueToSet,name,payload,value);
                });
                
            },
            setDefaultValueCoInput : function(inputs, blockValues, tabKey ){
                valueToSend={};
                blockValues['icon'] = { 'display' : true };

                if ( typeof blockValues.iconStyle != "undefined"){
                    blockValues["icon"] = { 
                        'iconStyle' : blockValues.iconStyle                          
                    };
                    // jsonHelper.setValueByPath(valueToSend, 'css.icon', blockValues.icon);
                } 
                
                if ( typeof blockValues.css.icon != "undefined"){
                    $.each( blockValues.css.icon, function(key,val){
                            if (key == 'display'){
                                if ( val == 'block'){
                                    blockValues["icon"][key] = true;
                                } else {
                                    blockValues["icon"][key] = false;
                                }
                            } else {
                                blockValues["icon"][key] = val;
                            }
                    });
                }  


                if ( typeof blockValues.css != "undefined" ){
                    if (tabKey == "hover"){
                        valueToSend = blockValues.css['hover'] ? blockValues.css['hover']:{};
                    } else if( tabKey =="style" ){
                        valueToSend = blockValues['css'] ? blockValues['css']:{};
                    } else if ( tabKey != "general"){
                        inputs.forEach(function callback(value, index) {
                            if( typeof value == "object" && typeof value["payload"] != "undefined"  && typeof value["payload"]["path"] != "undefined" ) {
                                mylog.log(value);
                                blockValues[tabKey][value["options"]["name"]] = jsonHelper.getValueByPath(blockValues, value["payload"]["path"]);
                            } 
                        });
                        valueToSend=blockValues[tabKey];
                    } else {
                        
                        valueToSend = blockValues;
                        inputs.forEach(function callback(value, index) {
                            var valueKey = "";

                            if ( typeof value == "string" ) {
                                valueKey =  value;
                            } else if ( typeof value == "object" && typeof value["options"]["name"] != "undefined"  ) {
                                valueKey = value["options"]["name"];
                            }

                            

                            if( typeof value == "object" && typeof value["payload"] != "undefined"  && typeof value["payload"]["path"] != "undefined" ) {
                                valueToSend[valueKey] = jsonHelper.getValueByPath(blockValues, value["payload"]["path"]);
                            } else if (typeof cssHelpers.json[valueKey] != "undefined"  ) {
                                valueToSend[valueKey]= typeof blockValues.css[valueKey] != "undefined" ? blockValues.css[valueKey] : "";
                            } else if ( valueKey != ""){
                                valueToSend[valueKey] = typeof blockValues[valueKey] != "undefined" ? blockValues[valueKey] : "";
                            }
                        })   
                    }
                 } else {
                    inputs.forEach(function callback(value, index) {
                        if ( typeof value == "object" && typeof value.options != "undefined" && value.options["type"] != "undefined" &&value.options["type"] == "checkbox" ) {
                            if ( typeof blockValues[value.options["name"]] != "undefined" && blockValues[value.options["name"]] == true){
                                $("#"+blockValues[value.options["name"]]).prop("checked", true);
                            }
                        }
                    })  
                }
                return valueToSend;
            },
            setPayload : function(inputs,tabKey){
                payload={};
                
                var valueKey = "";
                inputs.forEach(function callback(value, index) {
                    if ( typeof value == "string" ) {
                        valueKey =  value;
                    } else if ( typeof value == "object" && typeof value["options"]["name"] != "undefined"  ) {
                        valueKey = value["options"]["name"];
                    }


                    if( typeof value == "object" && typeof value["payload"] != "undefined" ) {
                        jsonHelper.setValueByPath(payload, tabKey+"."+valueKey, value["payload"]);
                    } else {
                        jsonHelper.setValueByPath(payload, tabKey+"."+valueKey, "");
                    }
                });
                return payload;
            },
            // setCheckBox : function(inputs, blockValues){
            //     inputs.forEach(function callback(value, index) {
            //         if ( typeof value == "object" && value.options["type"] == "checkbox" ) {
            //             mylog.log("check",value.options["type"] , value.options["name"] )
            //             if ( typeof blockValues['css'][value.options["name"]] != "undefined" && blockValues['css'][value.options["name"]] == true  ){
            //                 $("#"+value.options["name"]).prop("checked", true);
            //             } else  if ( typeof blockValues[value.options["name"]] != "undefined" && blockValues[value.options["name"]] == true ){
            //                 $("#"+value.options["name"]).prop("checked", true);
            //             } 
            //         }
            //     });

            //     if ( typeof blockValues['icon']['display'] != 'undefined' && blockValues['icon']['display'] == 'block') {
            //         $("#display").prop("checked", true);
            //     }
            // },
            setDefaultValueTextEditor:function(styles){
                Object.keys(styles).forEach(function(property){
                    $(`.cms-text-editor-actions button[data-property="${property}"][data-value="${styles[property]}"]`).addClass("active")
                })

                setTimeout(function(){
                   if (window.getSelection()) {
                    var selectedText = window.getSelection().getRangeAt(0).startContainer.parentNode

                    if (selectedText.color){
                        $("#myColor").spectrum("set", selectedText.color);
                    }else if(selectedText.style.color){
                        let colorRGB = selectedText.style.color;
                        let hexColor = cmsConstructor.helpers.colorRGBtoHex(colorRGB)   
                        $("#myColor").spectrum("set", hexColor);
                    }else{
                        $("#myColor").spectrum("set", cmsConstructor.helpers.colorRGBtoHex(cmsConstructor.textColor));
                    }
                    if (selectedText.style["background-color"]) {
                        $("#myBgColor").spectrum("set", cmsConstructor.helpers.colorRGBtoHex(selectedText.style["background-color"]));
                    }else{
                        $("#myBgColor").spectrum("set", cmsConstructor.helpers.colorRGBtoHex(cmsConstructor.helpers.InheritedBackgroundColor(cmsConstructor.selectedDom).bgTextColor));
                    }

                    if (selectedText.face)    {
                        $("#input-font").val(selectedText.face)
                    }else if(selectedText.style["font-family"]){
                        $("#input-font").val(selectedText.style["font-family"])
                    }else{
                        $("#input-font").val("Arial")
                    }

                    if (selectedText.size){
                        $("#fontSize").val(selectedText.size)
                    }else{
                        $("#fontSize").val(18)
                    }

                    if (selectedText.style["font-size"]){
                        $("#fontSize").val(parseInt(selectedText.style["font-size"]));
                    }else{                    
                        $("#fontSize").val(18);
                    }
                }
                })
            },
            // setStyleHover: function(css){
            //     $("#hover"+cmsConstructor.kunik).remove();
            //     var element  = document.createElement("style");
            //     element.id = "hover"+cmsConstructor.kunik;
            //     if(cmsConstructor.kunik.startsWith('button')){
            //         var cssText = `.btn-${cmsConstructor.kunik}:hover{`;
            //     }else{
            //         var cssText = `.${cmsConstructor.kunik}:hover{`;
            //     }
            //     if(cmsConstructor.kunik.startsWith("button")){
            //         cssText += `
            //         font-size: ${css['hover']['font-size']} !important;
            //         color: ${css['hover']['color']} !important;
            //         height:${css.size.height == "auto" ? "" : css.size.height} !important;`;
            //     }

            //     cssText += `width: ${css.size.width} !important;
                    
            //         background-color: ${css.hover.background.color} !important;
            //         border-style: ${css["hover"]["border"]["type"]} !important;
            //         border-color: ${css["hover"]["border"]["color"]} !important;
            //         border-width: ${css["hover"]["border"]["width"]}px !important;
            //         border-top-width: ${css["hover"]["border"]["top"]}px !important;
            //         border-right-width: ${css["hover"]["border"]["right"]}px !important;
            //         border-bottom-width: ${css["hover"]["border"]["bottom"]}px !important;
            //         border-left-width: ${css["hover"]["border"]["left"]} !importantpx;
            //         border-radius: ${css["hover"]["border"]["radius"]["top-left"]}px ${css["hover"]["border"]["radius"]["top-right"]}px ${css["hover"]["border"]["radius"]["bottom-right"]}px ${css["hover"]["border"]["radius"]["bottom-left"]}px !important; 
            //         padding: ${css["hover"]["padding"]["top"]} ${css["hover"]["padding"]["right"]} ${css["hover"]["padding"]["bottom"]} ${css["hover"]["padding"]["left"]} !important;
            //         margin: ${css["hover"]["margin"]["top"]} ${css["hover"]["margin"]["right"]} ${css["hover"]["margin"]["bottom"]} ${css["hover"]["margin"]["left"]} !important;
            //         -webkit-box-shadow: ${css["hover"]["box-shadow"]["inset"]} ${css["hover"]["box-shadow"]["color"]} ${css["hover"]["box-shadow"]["x"]}px ${css["hover"]["box-shadow"]["y"]}px ${css["hover"]["box-shadow"]["blur"]}px ${css["hover"]["box-shadow"]["spread"]}px !important;
            //         -moz-box-shadow: ${css["hover"]["box-shadow"]["inset"]} ${css["hover"]["box-shadow"]["color"]} ${css["hover"]["box-shadow"]["x"]}px ${css["hover"]["box-shadow"]["y"]}px ${css["hover"]["box-shadow"]["blur"]}px ${css["hover"]["box-shadow"]["spread"]}px !important;
            //         box-shadow: ${css["hover"]["box-shadow"]["inset"]} ${css["hover"]["box-shadow"]["color"]} ${css["hover"]["box-shadow"]["x"]}px ${css["hover"]["box-shadow"]["y"]}px ${css["hover"]["box-shadow"]["blur"]}px ${css["hover"]["box-shadow"]["spread"]}px !important;
            //     }`;
            //     element.innerHTML = cssText;
            //     if(cmsConstructor.kunik.startsWith('button')){
            //         $(element).insertBefore(".btn-"+cmsConstructor.kunik);
            //     }else{
            //         $(element).insertBefore("."+cmsConstructor.kunik);
            //     }
            // }
        },
        views: {
            init: function () {
                var kunik = cmsConstructor.kunik,
                    spId = cmsConstructor.spId,
                    values = cmsConstructor.sp_params[spId] ? cmsConstructor.sp_params[spId]:null;

                var blockType = cmsConstructor.helpers.getBlockType(kunik,spId);
                       
                cmsConstructor.panelConfig = [];
                
                if( typeof cmsConstructor.blocks[blockType] != "undefined" ){ 
                    defaultPanelConfig = {
                        general :  {
                            key: "general",
                            icon: "sliders",
                            label: tradCms.general,
                            content: this.tabConstructViews(kunik, spId, values,blockType,"general")
                        },
                        style : {
                            key: "style",
                            icon: "paint-brush",
                            label: tradCms.style,
                            content: this.tabConstructViews(kunik, spId, values,blockType,"style")
                        },
                        hover : {
                            key: "hover",
                            icon: "mouse-pointer",
                            label: tradCms.styleHover,
                            content: this.tabConstructViews(kunik, spId, values,blockType,"hover")                 
                        },
                        advanced : {
                            key: "advanced",
                            icon: "cogs",
                            keyPath: "advanced",
                            label: tradCms.advanced,
                            content: this.tabConstructViews(kunik, spId, values,blockType,"advanced")
                        }
                    }

                    tabsOptions = {
                        onInitialized : function(){
                            costumizer.actions.cancel("#menu-right-costumizer");
                        }
                    };

                    

                    $.each(defaultPanelConfig, function(e, v){
                            if ( typeof cmsConstructor.blocks[blockType].configTabs[e] != "undefined"){
                                $.each(defaultPanelConfig[e], function(el, val){
                                    if ( typeof cmsConstructor.blocks[blockType].configTabs[e][el] == "undefined"){
                                        cmsConstructor.blocks[blockType].configTabs[e][el] = val;
                                    }
                                });
                            }
                    });
                        
                    config = cmsConstructor.blocks[blockType].configTabs
                        
                    $.each(config, function(e, v){
                        if ( e != "advanced" || ( (isSuperAdmin || isSuperAdminCms) && e == "advanced")){
                                cmsConstructor.panelConfig.push(config[e]);
                        }
                    });

                    if (cmsConstructor.panelConfig){
                        $(".cmsbuilder-right-content").addClass("active")
                        costumizer.actions.tabs.init("#right-panel", cmsConstructor.panelConfig , tabsOptions);
                    }

                    $.each(cmsConstructor.panelConfig, function(e, v){
                        cmsConstructor.editor.actions.generateCoInputs(kunik, spId, values,blockType,v.key);
                    });

                    if (blockType == "text"){
                        $(".cmsbuilder-tab-section #general").html(cmsConstructor.editor.views.inputsText(cmsConstructor.kunik,cmsConstructor.spId)) 
                    } 
                }
                
            },
            tabConstructViews: function (kunik, spId, values , blockType , tabKey) {
                
                var $container = $(`<div class="col-md-12 sp-cms-options dark" id="${tabKey}" ></div>`);
                return $container;

            },
            inputsText: function (kunik, spId, values = null) {
                var $inputsText = $(`
                    <div class="cms-text-editor">
                        <div class="cms-text-editor-header">
                            <div class="cms-text-editor-group">
                                <div class="cms-text-editor-form cms-text-editor-form-font">
                                    <select class="cms-text-editor-form-control" id="input-font">
                                    </select>
                                </div>
                                <div class="cms-text-editor-form">
                                    <select class="cms-text-editor-form-control" id="fontSize">
                                    </select>
                                </div>
                                <div class="cms-text-editor-form">
                                    <div class="cms-text-editor-form-color">
                                        <input type="color" id="myColor">
                                    </div>
                                </div>
                                <div class="cms-text-editor-form hidden">
                                    <div class="cms-text-editor-form-color">
                                        <span><i class="fa fa-font" aria-hidden="true"></i></span>
                                        <input type="color" id="myBgColor">
                                    </div>
                                </div>
                            </div>
                            <div class="cms-text-editor-group">
                                <ul class="cms-text-editor-actions" data-action="style">
                                    <li><button data-property='italic'><i class="fa fa-italic" aria-hidden="true"></i></button></li>
                                    <li><button data-property='bold'><i class="fa fa-bold" aria-hidden="true"></i></button></li>
                                    <li><button data-property='underline'><i class="fa fa-underline" aria-hidden="true"></i></button></li>
                                    <li><button data-property='strikethrough'><i class="fa fa-strikethrough" aria-hidden="true"></i></button></li>
                                </ul>
                                <ul class="cms-text-editor-actions" data-action="align">
                                    <li><button data-property='justifyLeft'><i class="fa fa-align-left" aria-hidden="true"></i></button></li>
                                    <li><button data-property='justifyCenter'><i class="fa fa-align-center" aria-hidden="true"></i></button></li>
                                    <li><button data-property='justifyRight'><i class="fa fa-align-right" aria-hidden="true"></i></button></li>
                                    <li><button data-property='justifyFull'><i class="fa fa-align-justify" aria-hidden="true"></i></button></li>
                                </ul>
                                <ul class="cms-text-editor-actions">
                                    <li><button onclick="cmsConstructor.block.actions.textLinkInsertion('[label](http://www)')"><i class="fa fa-link" aria-hidden="true"></i></button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                `)

                for (var i = 5; i < 501; i++) {
                    $inputsText.find("#fontSize").append("<option value='"+i+"'>"+i+"px</option>")
                }

                $inputsText.find("#input-font").append(fontOptions) 

                return $inputsText;
            }
        },
        events: {
            init: function () {
                $.each(cmsConstructor.editor.events, function (key, eventHandler) {
                        if (key !== "init" && typeof eventHandler === "function")
                            eventHandler()
                })
            },            
            textEditor:function(){
                $("#myColor").spectrum({
                    type: "color",
                    color: $("#myColor").val(),
                    showInput: true,
                    showInitial: true,
                    showAlpha: false,
                    change:function(color){
                        var mycolor = "";
                        restoreSelection(editable, divSelection); 
                        mycolor = color.toRgbString();
                        mycolor = cmsConstructor.helpers.rgb2hex(mycolor);
                        setTimeout(function(){
                            document.execCommand('foreColor', false,mycolor);
                        },90);
                        cmsConstructor.block.actions.saveTextEdition()   
                    }
                })

                $("#myBgColor").spectrum({
                    type: "color",
                    color: $("#myBgColor").val(),
                    showInput: true,
                    showInitial: true,
                    change:function(color){
                        $(cmsConstructor.selectedDom).coEditor("background-color", color.toHexString())
                        cmsConstructor.block.actions.saveTextEdition()   
                    }
                })

                $("#input-font").off("change").on("change", function () {
                    $(cmsConstructor.selectedDom).coEditor("font-family", $(this).val())
                    cmsConstructor.block.actions.saveTextEdition()   
                });

                $("#fontSize").off("change").on("change", function(){
                    $(cmsConstructor.selectedDom).coEditor("font-size", $(this).val()+"px")
                    cmsConstructor.block.actions.saveTextEdition()   
                })

                $(".cms-text-editor-actions li button").off("click").on("click", function(e){
                    document.execCommand($(this).data("property"),false,null);
                    cmsConstructor.block.actions.saveTextEdition()   
                    // $(cmsConstructor.selectedDom).coEditor($(this).data("property"), $(this).data("value"))
                })
            }
        }
    },
    block: {
        actions : {
            addActions:function($target, $pin = false){
                var dragnotStart = !cmsConstructor.dragstart,
                    blockActionIsNotExist = $target.find("> .block-actions-wrapper").length < 1,
                    isSelected = $target.find("> .block-actions-wrapper.selected").length > 0;

                if(isSelected)
                    $(".block-actions-wrapper:not(.selected)").remove()

                if(dragnotStart && blockActionIsNotExist){
                    $(".block-actions-wrapper:not(.selected)").remove()
                    
                    var type = $target.data("blocktype")
                    cmsConstructor.block.views.addActions(
                        type, 
                        $target, 
                        cmsConstructor.helpers.getBlockTitle($target), 
                        $pin ? 'selected':""
                    )

                    cmsConstructor.block.events.actions.bindOnclickActionItem()
                }
            },
            saveTextEdition:function(){
                if(cmsConstructor.selectedDom){
                    // cmsConstructor.selectedDom.removeAttr('contenteditable');
                    text = cmsConstructor.selectedDom.html();
                    tplCtx = {};
                    tplCtx.id = cmsConstructor.selectedDom.data("id");
                    tplCtx.collection = "cms";
                    tplCtx.path = cmsConstructor.selectedDom.data("field");
                    tplCtx.value = text;

                    var json = cmsConstructor.helpers.string2Json(text, true);

                    if (cmsConstructor.helpers.isContains(json, "script") == true) {
                        toastr.error("Script detecté");
                        //cmsConstructor.elSelected = null;
                    } else {
                        dataHelper.path2Value(tplCtx, function (params) {
                            toastr.success(tradCms.editionSucces);
                            //cmsConstructor.elSelected = null;
                        });
                    }
                  //   if (cmsConstructor.elSelected.text() == "") {
                  //       cmsConstructor.elSelected.html(trad.writeatexthere);
                  // }
                }
            },
            textLinkInsertion : function(defaultSyntax) {
                let sel, range;
                if (window.getSelection) {
                    sel = window.getSelection();
                    selectedText = sel.toString();
                    if (selectedText !== "") {
                        label = "["+selectedText+"](http://www.) ";
                    }else{
                        label = defaultSyntax;
                    }
                    if (sel.getRangeAt && sel.rangeCount) {
                        range = sel.getRangeAt(0);
                        range.deleteContents();
                        range.insertNode( document.createTextNode(label) );
                    }
                } else if (document.selection && document.selection.createRange) {
                    document.selection.createRange().label = label;
                }
            }
        },
        init:function(){
            this.events.init()
        },
        views:{
            addActions: function(type, $el, label, className=""){
                //var actions = []

                //if(type == "section" || type == "column" || type == "customSection"){
                    actions = [
                        [
                            {
                                name:"move_up",
                                icon:"arrow-up",
                                label: tradCms.moveToUp
                            },
                            {
                                name:"move_down",
                                icon:"arrow-down",
                                label: tradCms.moveToBottom
                            }
                        ],
                        [
                            {
                                name:"edit",
                                icon:"pencil",
                                label: tradCms.edit
                            },
                            {
                                name:"select_parent",
                                icon:"level-up",
                                label: tradCms.selectParent
                            },
                            {
                                name:"duplicate",
                                icon:"clone",
                                label: tradCms.duplicate
                            },
                            {
                                name:"copy",
                                icon:"copy",
                                label: trad.copy
                            },
                            {
                                name:"delete",
                                icon:"trash-o",
                                label: tradCms.delete,
                                className:"bg-red"
                            },
                            {
                                name:"deselect",
                                icon:"ban",
                                label: tradCms.deselect,
                                className:"bg-orange"
                            }
                        ]
                    ]

                    if (typeof cmsConstructor["copy"] != "undefined") {
                        if (label == "Section" || label == "Colonne") {
                            var pasteParams = {name:"paste", icon:"paste", label: trad.paste}
                            actions[1].splice(4,0,pasteParams);
                        }
                    }

               /*}else{
                     actions = [
                        [
                            {
                                name:"move_up",
                                icon:"arrow-up",
                                label: tradCms.moveToUp
                            },
                            {
                                name:"move_down",
                                icon:"arrow-down",
                                label: tradCms.moveToBottom
                            }
                        ],
                        [
                            {
                                name:"edit",
                                icon:"pencil",
                                label: tradCms.edit
                            },
                            {
                                name:"select_parent",
                                icon:"level-up",
                                label: tradCms.selectParent
                            },
                            {
                                name:"delete",
                                icon:"trash-o",
                                label: tradCms.delete,
                                className:"bg-red"
                            },
                            {
                                name:"deselect",
                                icon:"ban",
                                label: tradCms.deselect,
                                className:"bg-orange"
                            }
                        ]
                    ]
                }*/

                var labelTrad = label == "image" ? tradCms.image : label == "icon" ? tradCms.icon : label == "button" ? tradCms.button : label == "text" ? tradCms.text : label == "separator" ? tradCms.separator : label == "une section" ? tradCms.oneSection : label ;
                var $actions = $(`
                    <div class="block-actions-wrapper ${className}">
                        <div class="cmsbuilder-block-action">
                            <div class="cmsbuilder-block-action-label">${labelTrad}</div>
                            <div class="cmsbuilder-block-action-list"></div>
                        </div>
                    </div>
                `)

                actions.forEach(function(actionsGroup){
                    var $ul = $(`<ul></ul>`)
                    actionsGroup.forEach(function(action){
                        if (label !== "text") {
                        $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}">
                                <span><i class="fa fa-${action.icon}" aria-hidden="true"></i></span>
                                <span>${action.label}</span>
                            </li>
                        `)
                        }else if (action.name !== "edit") {
                            $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}">
                                <span><i class="fa fa-${action.icon}" aria-hidden="true"></i></span>
                                <span>${action.label}</span>
                            </li>
                        `)
                        }
                    })
                    $actions.find(".cmsbuilder-block-action-list").append($ul)
                })

                $(".block-actions-wrapper").not(".selected").remove()
                $el.css({ overflow:"visible" })
                $el.prepend($actions)
            }
        },
        events:{
            init:function(){
                if(window.costumizer && costum && costum.editMode){
                    this.actions.bindOnMouseover()
                    this.bindEventsSuperText()
                }
            },
            actions:{
                bindOnMouseover:function(){
                    $(".cmsbuilder-block").off("mouseover").on("mouseover", function(e){
                        e.stopPropagation();
                        if (window.getSelection().toString() == "") 
                            cmsConstructor.block.actions.addActions($(this))
                    })

                    $(".cmsbuilder-block").off("click").on("click", function(e){
                        if(!(($(e.target).parents().hasClass("carousel-control") || $(e.target).parents().hasClass("carousel-indicators")) || ($(e.target).data("toggle") == "collapse"))){
                            e.stopPropagation()
                            if (window.getSelection().toString() == "") {
                                var kunik = $(this).data("kunik"),
                                    id = $(this).data("id"),
                                    ownerType = $(this).data("blocktype"),
                                    path = $(this).data("path"),
                                    name = $(this).data("name")

                                cmsConstructor.kunik = kunik
                                cmsConstructor.spId = id
                                cmsConstructor["ownerType"] = ownerType;


                                if(!(ownerType == "custom" ||  (ownerType == "section" && path != "tpls.blockCms.superCms.container"))){
                                    $(".block-actions-wrapper").removeClass("selected")
                                    $(this).find("> .block-actions-wrapper").addClass("selected")
                                    cmsConstructor.editor.init()
                                    cmsConstructor.layer.actions.selectPath(kunik)
                                    // var editableElements = $('[contenteditable]');
                                    // editableElements.each(function() {
                                    //     $(this).removeAttr('contenteditable');
                                    // });
                                }
                            }
                        }
                    })
                },
                bindOnclickActionItem:function(){
                    var self = this;
                    $(".block-actions-wrapper li").off("click").on("click", function(e){
                        e.stopPropagation();
                        var $owner = $($(this).parents(".cmsbuilder-block")[0]),
                            kunik = $owner.data("kunik"),
                            id = $owner.data("id"),
                            ownerType = $owner.data("blocktype"),
                            path = $owner.data("path"),
                            name = $owner.data("name")

                        cmsConstructor.kunik = kunik
                        cmsConstructor.spId = id
                        cmsConstructor["ownerType"] = ownerType;
                        var blockType =  cmsConstructor.helpers.getBlockType(kunik,id);

                        switch($(this).data("action")){
                            case "edit":
                                if( (ownerType == "custom" && typeof cmsConstructor.blocks[blockType] == "undefined" ) ||  (ownerType == "section" && path != "tpls.blockCms.superCms.container")){
                                    if(costumizer){
                                        costumizer.actions.closeRightSubPanel()
                                        costumizer.actions.toogleSidePanel("right", false)
                                    }
                                    $(`.edit${kunik}Params`).click()
                                }else{
                                    $(".block-actions-wrapper").removeClass("selected")
                                    $(this).closest(".block-actions-wrapper").addClass("selected")
                                    cmsConstructor.editor.init()
                                    cmsConstructor.layer.actions.selectPath(kunik)
                                }
                            break;
                            case "duplicate":                             
                                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                                '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                                '</div>';                  

                                //var parentId = $owner.parents(".cmsbuilder-block").data("id");
                                
                                //if(ownerType == "column") {
                                    $owner.after(strNewBlc);
                                   // $(strNewBlc).insertAfter();//`.whole-${cmsConstructor.kunik}`)
                                    cmsBuilder.block.duplicate(cmsConstructor.spId);
                               // }else{
                                    //$(strNewBlc).insertAfter(`.block-container-${cmsConstructor.kunik}`)
                                    //cmsBuilder.block.duplicate(cmsConstructor.spId);
                                //}
                            break;
                            case "delete":
                                var children = cmsConstructor.layer.views.getArbre($owner);
                                var nameTrad = name == "image" ? tradCms.image : name == "button" ? tradCms.button : name == "text" ? tradCms.text : name == "separator" ? tradCms.separator : name == "une section" ? tradCms.oneSection : name;
                                bootbox.confirm(
                                    "<font size='5' class='text-red'>"+trad.delete+"  "+nameTrad+":</font><br>"+trad.areyousuretodelete+"<br>"+((children.find("li").length != 0 ? tradCms.deleteWithElement : ""))+"<br>"+children.html(),
                                    function(result){
                                        if(result)
                                            cmsConstructor.builder.actions.deleteBlock()
                                    }
                                )
                            break;
                            case "select_parent":
                                var $target = null;
                                if($owner.parents(".cmsbuilder-block[data-blockType='column']").length > 0)
                                    $target = $($owner.parents(".cmsbuilder-block[data-blockType='column']")[0])
                                else if($owner.parents(".cmsbuilder-block[data-blockType='section']").length > 0)
                                    $target = $($owner.parents(".cmsbuilder-block[data-blockType='section']")[0])

                                //si le parent existe et les actions ne sont pas encore ajoutés
                                if($target && $target.find("> .block-actions-wrapper").length < 1){
                                    var targetType = $target.data("blocktype"),
                                        targetId = $target.data("id"),
                                        targetKunik = $target.data("kunik"),
                                        targetName = $target.data("name"),
                                        actionLabel = targetName;

                                    if(targetType == "section")
                                        actionLabel = "Section"
                                    else if(targetType == "column")
                                        actionLabel = "Colonne"
                                        
                                    $(".block-actions-wrapper").remove()
                                    cmsConstructor.block.views.addActions(targetType, $target, actionLabel, "selected")
            
                                    self.bindOnclickActionItem()

                                    cmsConstructor.kunik = targetKunik
                                    cmsConstructor.spId = targetId
                                    cmsConstructor.editor.init()
                                }
                            break;
                            case "deselect":
                                $(".cmsbuilder-right-content").removeClass("active")
                                $("#right-panel").html("")
                                $(".block-actions-wrapper").removeClass("selected")
                            break;
                            case "move_up":
                                cmsConstructor.builder.actions.sortBlock($owner, "move_up")
                            break;
                            case "move_down":
                                cmsConstructor.builder.actions.sortBlock($owner, "move_down")
                            break;
                            case "copy":
                                cmsConstructor["copy"] = cmsConstructor.spId
                                toastr.success("CMS copié!");
                            break;
                            case "paste":            
                                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                                '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                                '</div>';   
                                // empty-sp-element      
                                if ($(".empty-sp-element"+cmsConstructor.kunik)[0])
                                    $(".empty-sp-element"+cmsConstructor.kunik).hide()
                                
                                $owner.find(".block-container-html").append(strNewBlc);         
                                cmsBuilder.block.duplicate(cmsConstructor["copy"], cmsConstructor.spId);           
                                // delete cmsConstructor["copy"]
                            break;

                        }
                    })
                }
            },
            
            bindEventsSuperText:function(){
                var prevText = "";
                // var spTextSelected = null;
                //do not propagate click on sp-text
                $(".cmsbuilder-block:not(.sp-text)").off("click").on("click", function(e){ 
                    if(!(($(e.target).parents().hasClass("carousel-control") || $(e.target).parents().hasClass("carousel-indicators")) || ($(e.target).data("toggle") == "collapse"))){
                        e.stopPropagation() 
                        if (window.getSelection().toString().length == 0) {   
                            var $cmsbuilderBlock = $(this).closest(".cmsbuilder-block")
                            var kunik = $(this).data("kunik") ? $(this).data("kunik"):"",
                            id = $(this).data("id"),
                            ownerType = $cmsbuilderBlock.data("blocktype"),
                            path = $cmsbuilderBlock.data("path");
                            $(".block-actions-wrapper").removeClass("selected")  
                            $cmsbuilderBlock.find("> .block-actions-wrapper").addClass("selected")
                            cmsConstructor.kunik = kunik
                            cmsConstructor.spId = id
                            cmsConstructor.ownerType = ownerType;
                            cmsConstructor.editor.init()

                            var editableElements = $('[contenteditable]');
                            editableElements.each(function() {
                             $(this).removeAttr('contenteditable');
                         });
                            return false;
                        } 
                    }
                })
                $(".sp-text").off("mousedown").on("mousedown", function(e){
                    cmsConstructor.selectedDom = $(this)
                    cmsConstructor.spId = $(this).data("id")
                    
                    // Catch tout les blocks qui a de texteditable (avoir de data-field)
                    if(typeof $(this).data('field') !== 'undefined'){
                        var $cmsbuilderBlock = $(this).closest(".cmsbuilder-block")
                        $(".block-actions-wrapper").removeClass("selected")  
                        $cmsbuilderBlock.find("> .block-actions-wrapper").addClass("selected")
                        cmsConstructor.kunik = "text"+cmsConstructor.spId;
                        cmsConstructor.ownerType = "text";                      
                        cmsConstructor.selectedDom.attr('contenteditable','true');
                        cmsConstructor.layer.actions.selectPath(cmsConstructor.kunik)
                    }
                    
                    $(".cmsbuilder-center-content").on("mouseup", function(eMouseup){
                        eMouseup.stopImmediatePropagation()
                        if(typeof cmsConstructor.selectedDom.data('field') !== 'undefined'){
                            cmsConstructor.editor.init()
                            let $this = cmsConstructor.selectedDom

                            prevText = $this.html()

                            var thisName = "", 
                            thisParent = "";
                            if(notNull(cmsConstructor.sp_params[cmsConstructor.spId])){
                                thisParent = cmsConstructor.sp_params[cmsConstructor.spId].parent
                                thisName = cmsConstructor.sp_params[cmsConstructor.spId].name
                            }
                            var thisParams = {
                                name : thisName, 
                                id: cmsConstructor.spId, 
                                parentId : thisParent, 
                                $this : cmsConstructor.selectedDom
                            };

                            cmsConstructor.selectedDom.bind("paste", function(e){
                                e.stopImmediatePropagation()
                                e.preventDefault()
                                let text = e.originalEvent.clipboardData;
                                text = text.getData("text/plain")
                                document.execCommand('insertText', false, text)
                            })

                            setTimeout(function () {             
                                editable = $this[0];
                                if (window.getSelection().type != "None") {   
                                    divSelection = saveSelection(editable);    
                                    cmsConstructor.editor.actions.setDefaultValueTextEditor(cmsConstructor.helpers.getStyleOfSelection())
                                    cmsConstructor.editor.events.init()
                                }
                            },10)
                        }
                        $(".cmsbuilder-center-content").off("mouseup");
                    })
                    e.stopImmediatePropagation()               
                })
                
                
                // $(".cmsbuilder-center-content, .super-cms , .sp-text").off("mouseup").on("mouseup", function(e){
                //     e.stopImmediatePropagation();
                //     if (costum.editMode) {   
                //         editable = $(this)[0];
                //         if (window.getSelection().type != "None") {   
                //             // divSelection = saveSelection(editable);                         
                //             // cmsConstructor.editor.actions.setDefaultValueTextEditor(cmsConstructor.helpers.getStyleOfSelection())
                //             // cmsConstructor.editor.events.init()
                //         }
                //     }
                // })

                $(".sp-text").off("blur").on("blur", function (e) {
                    if(prevText != cmsConstructor.selectedDom.html()){
                        cmsConstructor.block.actions.saveTextEdition()
                    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                });

                $(".sp-text").off("keydown").on("keydown", function(e){
                    if (costum.editMode) {                
                        editable = $(this)[0];
                        if (e.ctrlKey) {    
                            if (e.keyCode == 65) {
                                setTimeout(function () {     
                                    if (window.getSelection().type != "None") {   
                                        divSelection = saveSelection(editable);                         
                                        cmsConstructor.editor.actions.setDefaultValueTextEditor(cmsConstructor.helpers.getStyleOfSelection())
                                        cmsConstructor.editor.events.init()
                                    }
                                },100)
                            }
                        }             

                        if(e.keyCode==13){
                            e.preventDefault();
                            if (window.getSelection) {
                                var selection = window.getSelection(),
                                range = selection.getRangeAt(0),
                                br = document.createElement("br"),
                                textNode = document.createTextNode("\u00a0"); //Passing " " directly will not end up being shown correctly

                                range.deleteContents();
                                range.insertNode(br);
                                range.collapse(false);
                                range.insertNode(textNode);
                                range.selectNodeContents(textNode);

                                selection.removeAllRanges();
                                selection.addRange(range);

                                return false;
                            }
                        }    
                    }  
                })
                
            }
        }
    }
}


 /**************Required for execCommand*************/
var saveSelection, restoreSelection, divSelection;
var editable = null;

var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

// function rgb2hex(rgb) {
//  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
//  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
// }

// function hex(x) {
//   return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
//  }

if (window.getSelection && document.createRange) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var range = win.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;

        return {
            start: start,
            end: start + range.toString().length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var charIndex = 0, range = doc.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = win.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
} else if (document.selection) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var selectedTextRange = doc.selection.createRange();
        var preSelectionTextRange = doc.body.createTextRange();
        preSelectionTextRange.moveToElementText(containerEl);
        preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
        var start = preSelectionTextRange.text.length;

        return {
            start: start,
            end: start + selectedTextRange.text.length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var textRange = doc.body.createTextRange();
        textRange.moveToElementText(containerEl);
        textRange.collapse(true);
        textRange.moveEnd("character", savedSel.end);
        textRange.moveStart("character", savedSel.start);
        textRange.select();
    };
}

cmsConstructor.init();