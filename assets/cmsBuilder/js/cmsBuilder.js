var cmsBuilder = {
    init : function(){
        cmsBuilder.tpl.initEvent();
        cmsBuilder.bindEvents();
        cmsBuilder.manage.delete();
        cmsBuilder.block.initEvent();
        $(".openListTpls").off().on("click", function(){
          cmsBuilder.openModal();
          localStorage.removeItem("parentCmsIdForChild");
        });
    },
    config : {
    },
    helpers: {
        getDifference: function(array1, array2) {
            return array1.filter(x => array2.includes(x));
        },
        getCssChildren: function(element, index,sptext,imgIndex, css) {
            var indexParent = index.index -1;
            $(element).children().each((number, child) => {
                var sp = $(child).find('.sp-text');
                var img = $(child).find('img');
                if((sp.length > 0) || img.length > 0){
                    css['parent'+index.index] = css['parent'+index.index] || Object();
                    css['parent'+index.index]['css'] = cmsBuilder.helpers.getCssElement(child, true);
                    css['parent'+index.index]['ordre'] = number;
                    css['parent'+index.index]['parent'] = "parent"+indexParent;
                    index.index++;
                    cmsBuilder.helpers.getCssChildren(child, index, sptext,imgIndex, css);
                }else if($(child).hasClass("sp-text")){
                    css['sp-text'+sptext.index] = css['sp-text'+sptext.index] || Object();
                    // css['sp-text'+sptext.index]['css'] = getCssText(child);
                    css['sp-text'+sptext.index]['ordre'] = number;
                    css['sp-text'+sptext.index]['parent'] = "parent"+indexParent;
                    css['sp-text'+sptext.index]['text'] = cmsBuilder.helpers.transformText(cmsBuilder.helpers.getCssText(child), $(child).html());
                    sptext.index++;
                }else if(child.nodeName.toLowerCase() == "img"){
                    css['img'+imgIndex.index] = css['img'+imgIndex.index] || Object();
                    css['img'+imgIndex.index]['css'] = cmsBuilder.helpers.getCssElement(child);
                    css['img'+imgIndex.index]['ordre'] = number;
                    css['img'+imgIndex.index]['parent'] = "parent"+indexParent;
                    css['img'+imgIndex.index]['src'] = $(child).attr('src');
                    imgIndex.index++;
                }else if(($(child).children().length == 0)){
                    css['parent'+index.index] = css['parent'+index.index] || Object();
                    css['parent'+index.index]['css'] = cmsBuilder.helpers.getCssElement(child);
                    css['parent'+index.index]['ordre'] = number;
                    css['parent'+index.index]['parent'] = "parent"+indexParent;
                    index.index++;
                }
            });
        },
        transformText: function(css, text){
            return "<div style='"+css+"'><span>"+text+"</span></div>";
        },
        checkKey: function(array, key){
            var found = false;
            for (var i = 0; i < array.length && !found; i++) {
                if (key.indexOf(array[i]) > -1) {
                    found = true;
                }
            }
            return found;
        },
        getCssText: function(element){
            var css = element.computedStyleMap();
            var cssString = "";

            var cssKey = ["font-size", "font-weight", "font-family", "font-style","float", "text-align", "color","position", "display", "top", "left", "right", "bottom", "transform"];
            var cssKeyPartial = ["border", "background", "margin", "padding"];
            css.forEach((value, index) => {
                if(cssKey.includes(index)){
                cssString += index+": "+css.get(index).toString()+" !important;";
                }else if(cmsBuilder.helpers.checkKey(cssKeyPartial, index)){
                cssString += index+": "+css.get(index).toString()+" !important;";
                }
            });
            return cssString;
        },
        getCssElement: function(node, child){
            child = child || false;
            var css = node.computedStyleMap();
            //getComputedStyle(element, null);
            var cssObj = Object();
            var cssKey = ["font-size", "font-weight", "font-family", "font-style", "text-align", "color", "box-shadow", "width", "position","height","min-height", "display", "float","top", "left", "right", "bottom", "transform", "flex-basis", "flex-wrap"];
            var cssKeyPartial = ["border", "background", "margin", "padding"];
            css.forEach((value, index) => {
                if(cssKey.includes(index)){
                if(index == "width"){
                    //cssObj[index] = css.get(index).toString().indexOf("%") > 0 ? css.get(index).toString() : ((css.get(index).toString() == "auto") ? "100%" : (100*parseInt(css.get(index).value)/parseInt($(document).innerWidth())).toFixed(2)+"%");
                    if($(node).hasClass('ariane-suggest-elt') || (child && $(node).find('img').length > 0)){
                    cssObj[index] = css.get(index).toString();
                    }else{
                    cssObj[index] = (css.get(index).toString() == "auto") ? "100%" : css.get(index).toString();
                    }
                }else{
                    cssObj[index] = css.get(index).toString();
                }
                }else if(cmsBuilder.helpers.checkKey(cssKeyPartial, index)){
                cssObj[index] = css.get(index).toString();
                }
            });
            var cssGrouped = Object.assign({}, Object.keys(cssObj).reduce(function (accumulator, currentValue) {
                var data = ['background-', 'border-', 'shadow', 'font-', "margin-", "padding-"];
                var dataFull = ['color', 'text-align',"height", "width"];
                var dataOther = ["position","top", "left", "right","display", "bottom", "transform", "float","flex-basis", "flex-wrap", "min-height"];
                data.forEach((element) => {
                var index = element.replace('-', '');
                accumulator[index] = accumulator[index] || Object();
                if (currentValue.indexOf(element) >= 0) {
                    if(element == 'border-'){
                    var border = ["top", 'left', 'bottom', "right"];
                    if (currentValue.indexOf("radius") >= 0) {
                        accumulator[index]["radius"] = accumulator[index]["radius"] || Object();
                        border.forEach((indexBorder) => {
                            accumulator["other"] = accumulator["other"] || "";
                        if(currentValue.indexOf(indexBorder) >= 0){
                            accumulator["other"] += currentValue+": "+cssObj[currentValue]+";";
                            //accumulator[index]['radius'][currentValue.replace("-radius", '').replace(element, '')] = cssObj[currentValue];
                        }
                        });
                    }else{
                        border.forEach((indexBorder) => {
                        accumulator["other"] = accumulator["other"] || "";
                        if(currentValue.indexOf(indexBorder) >= 0){
                            accumulator["other"] += currentValue+": "+cssObj[currentValue]+";";
                        }
                        });
                    }
                    }else if(index == "margin" || index == "padding"){
                    var coin = ["top", 'right', 'bottom', "left"];
                    
                    coin.forEach((indexCoin) => {
                        accumulator["other"] = accumulator["other"] || "";
                        if(currentValue.indexOf(indexCoin) >= 0){
                        accumulator["other"] += currentValue+": "+cssObj[currentValue]+";";
                        }
                    });
                    }else{
                    accumulator[index][currentValue.replace(element, '')] = cssObj[currentValue];
                    }
                    
                }
                });
                dataOther.forEach((value) => {
                if(currentValue == value){
                    accumulator["other"] = accumulator["other"] || "";
                    accumulator["other"] += currentValue+": "+cssObj[currentValue]+";";
                }
                });
                dataFull.forEach((value) => {
                if(currentValue == value){
                    if(value == "width" || value == "height"){
                    if(cssObj[currentValue] == "auto"){
                        accumulator["other"] = accumulator["other"] || "";
                        accumulator["other"] += currentValue+": "+cssObj[currentValue]+";";
                    }else{
                        accumulator["size"] = accumulator["size"] || Object();
                        accumulator["size"][value] = cssObj[currentValue];
                    }
                    }else{
                    accumulator[value] = accumulator[value] || Object();
                    accumulator[value] = cssObj[currentValue];
                    }
                }
                })
                return accumulator;
            }, Object()));
            if(node.nodeName.toLowerCase() == "img"){
                cssGrouped['other'] += "object-fit: "+css.get('object-fit').toString()+";";
            }
            if($(node).attr('class')){
                cssGrouped["class"] = cssGrouped['class'] || Object();
                cssGrouped['class']['other'] = $(node).attr('class');
            }
            return cssGrouped;
        }
    },
    events : {
        cssHandle : function(){
            $(".cssHandle").off().on('click', function(){
                var css = Object(); 
                var kunik = $(this).data('kunik');
                var id = $(this).data('id');
                var sp = $(".block-parent[data-kunik=" + kunik + "]").find('.sp-text');
                var img = $(".block-parent[data-kunik=" + kunik + "]").find('img');
                var indexBlock = $(".sortable-"+kunik).attr("id").replace(id+'-', '');
                var parents = Array();
                sp.each((index, element) => {
                    if($(element).html() != ""){
                    if(parents.length == 0){
                        parents = $(element).parentsUntil(".block-container-html").toArray();
                    }else{
                        parents = cmsBuilder.helpers.getDifference(parents, $(element).parentsUntil(".block-container-html").toArray());
                    }
                    }
                });
                img.each((index, element) => {
                    if(parents.length == 0){
                        parents = $(element).parentsUntil(".block-container-html").toArray();
                    }else{
                        parents = cmsBuilder.helpers.getDifference(parents, $(element).parentsUntil(".block-container-html").toArray());
                    }
                });
                parents = parents.reverse();
                var parent = {index: 1, parentIndex: 0};
                var sptext = {index: 1};
                var imgIndex = {index: 1};
                parents.forEach(element => {
                    css['parent'+parent.index] = css['parent'+parent.index] || Object();
                    css['parent'+parent.index]['css'] = cmsBuilder.helpers.getCssElement(element);
                    if(parent.index != 1){
                        css['parent'+parent.index]['parent'] = "parent"+parent.parentIndex;
                    }
                    if($(element).children().length > 1){
                        parent.parentIndex = parent.index;
                        parent.index++;
                        cmsBuilder.helpers.getCssChildren(element, parent,sptext,imgIndex, css);
                    }
                });

                if(Object.keys(css).length > 0){
                    ajaxPost(
                    null,
                    baseUrl+'/co2/CmsRefactorMigration/blockcms',
                    {
                        css: css,
                        page: page,
                        costumId: costum.contextId,
                        costumType: costum.contextType
                    },
                    function(data) {
                        ajaxPost(
                        null,
                        baseUrl+'/co2/cms/migrate/slug/'+costum.slug+'/page/'+page+'/id/'+id+'/indice/'+indexBlock,
                        {cmsList: [data]},
                        function(response) {
                            $(".sortable-"+kunik).after(response);
                            var id = $(response).find(".custom-block-cms").data('id');
                            var loadBlock = $("."+id+"-load");
                            if (loadBlock.isInViewport()) {
                                cmsBuilder.block.loadIntoPage(loadBlock.data("id"),loadBlock.data('page'),loadBlock.data("path"),loadBlock.data("kunik"))
                            }
                            $(".sortable-"+kunik).remove();
                            // sortBlock(itemClassArray.join(","));     
                            toastr.success(tradCms.Completed);
                        }
                        );
                    }
                    );
                }
            })
        },
        sortBlock : function(){
            if(costum.editMode && $('#all-block-container').length){
                
                $(".custom-block-cms").mouseover(function(){
                   $(this).addClass("blur").find('.command-block-pos').show();
                }).mouseout(function(){
                      $(this).removeClass("blur").find('.command-block-pos').hide();
                  
                });
                $(".custom-block-cms").first().find(".handleDrag.moveUp").hide();
                  $(".custom-block-cms").last().find(".handleDrag.moveDown").hide();
                  
                $(".handleDrag").off().on("click",function(){
                  domToClone="#"+$(this).parents().eq(1).attr("id");
                  cloneBlock=$(domToClone).clone(true);
                  if($(this).data("move")=="up")
                    $(this).parents().eq(1).prev().before(cloneBlock);
                  else
                    $(this).parents().eq(1).next().after(cloneBlock);
                  $(this).parents().eq(1).remove();
                  $(".handleDrag").show();
                  $(".custom-block-cms").first().find(".handleDrag.moveUp").hide();
                  $(".custom-block-cms").last().find(".handleDrag.moveDown").hide();
                  coInterface.scrollTo(domToClone);
                  var params = {
                      id : costum.contextId,
                      collection : costum.contextType,
                      idAndPosition : []
                  };
                    $(".custom-block-cms").each(function(){
                        params.idAndPosition.push($(this).data("id"));
                    });
                    ajaxPost(
                        null,
                        baseUrl+"/costum/blockcms/dragblock",
                        params,
                        function(data){
                            /*if(data.result)
                                toastr.success("Bien déplacé");
                            else
                                toastr.success(trad.somethingwrong);*/
                        }
                     );
                  }); 
            }
        }
    }, 
    tpl:{
        backup : function (){
            $('#backupTplsContainer').fadeIn();
            ajaxPost(
                null,
                baseUrl+"/co2/cms/gettemplatebycategory",
                {
                    "contextId" : cmsBuilder.config.context.id,
                    "page" : cmsBuilder.config.page
                },
                function(data){
                    var backupStr = ""
                    if (data != "") {
                        $.each(data, function(keyBackup,valueBackup) {
                            backupStr +=`
                           <div class="tplItems" data-status="backup">
                              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">
                                <div class="item-slide">
                                  <div class="entityCenter">
                                    <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                                    </a>
                                  </div>
                                  <div class="img-back-card">
                                    <div class="div-img">
                                      <img src="`+valueBackup.profilImageUrl+`">
                                    </div>
                                    <div class="text-wrap searchEntity">
                                      <div class="entityRight profil no-padding">
                                        <a href="#" class="entityName letter-orange">
                                          <font >`+valueBackup.name+`</font>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="slide-hover co-scroll">
                                    <div class="text-wrap">
                                      <div class="entityPrice">
                                        <font class="letter-orange" >`+valueBackup.name+`</font>
                                      </div>
                                      <div class="entityType col-xs-12 no-padding">
                                        <p class="p-short">
                                          <font >`+valueBackup.description+`</font>
                                        </p>
                                      </div>
                                      <hr>
                                      <ul class="tag-list">
                                        <span class="badge btn-tag tag padding-5">
                                          <font >
                                          </font>
                                        </span>
                                      </ul>
                                      <hr>
                                      <div class="desc">
                                        <div class="socialEntityBtnActions btn-link-content">
                                          <a href="`+valueBackup.profilImageUrl+`" class="btn btn-info btn-link thumb-info" data-title="Aperçu de Creercostum2" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye"></i>`+trad.Preview+`</font>
                                          </a>
                                          <a href="javascript:;" class="btn btn-info btn-link" onclick="cmsBuilder.tpl.switchTemplate('`+keyBackup+`')">
                                            <font > Restore</font>
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>`;
                      });
                  }else
                      backupStr = `<div class='tplItems' data-status='backup'>`+tradCms.noBackupRegistered+`</div>`;
                  $('#backupTplsContainer').html(backupStr);
                },
                 null,
                "json",
                {async : false}
            );
        },

        //Save template user-------------------------------------------
        saveTplUser : function (idTpl, stat) {
            if (idTpl !== "") {
                var tplCtx = {};
                tplCtx.id = idTpl;
                tplCtx.path = "tplsUser."+cmsBuilder.config.context.id+"."+cmsBuilder.config.page;
                tplCtx.collection = "cms"; 
                tplCtx.value = stat;
                dataHelper.path2Value( tplCtx, function(params) {});
            }else{
                toastr.error("Erreur lors de la mise à jour de Template")
            }
        },
        //Chose template-----------------------------------------------
        chooseConfirm : function(ide,tplSelected){
            if (canEditTpl) {
                var adjF = tradCms.section;
                if (cmsBuilder.config.block.newCmsId.length > 1) {
                    adjF = trad.sections;
                }
                bootbox.dialog({
                    message: `<p class="">`+tradCms.addTplExplaination+` `+adjF+`, <br>`+tradCms.saveOrFussTpl+`.</p>
                        <div class="alert alert-danger" role="alert">
                          <p><b><i class="fa fa-exclamation-triangle"></i> NB</b>: `+tradCms.warningOverwritingSection+`!</p>
                        </div>`,
                    title: tradCms.templateSelection,
                    onEscape: function() {},
                    show: true,
                    backdrop: true,
                    closeButton: true,
                    animate: true,
                    className: "my-modal",
                    buttons: {
                        success: {   
                            label: trad.save,
                            className: "btn-success",
                            callback: function() {
                              cmsBuilder.tpl.saveThis();
                            }
                        },
                        merge: {
                            label : trad.merge,
                            className: "btn-primary",
                            callback: function() {
                                cmsBuilder.tpl.chooseThis(ide, tplSelected);
                            }
                        },
                        overwrite: {
                            label: trad.overwrite,
                            className: "btn-danger",
                            callback: function() {
                                bootbox.confirm('<div role="alert">'+
                                                '<p>'+tradCms.explainDeleteSectionTpl+'</p>'+ 
                                            '</div>',
                                    function(result){
                                        if (!result) {
                                          return;
                                        }else{            
                                          cmsBuilder.block.delete(cmsBuilder.config.block.newCmsId);            
                                          cmsBuilder.tpl.chooseThis(ide, tplSelected);
                                        }
                                        urlCtrl.loadByHash(location.hash);
                                    }
                                ); 
                            }
                        },
                        cancel: function() {}
                    }
                });
            }else{    
                cmsBuilder.tpl.chooseThis(ide, tplSelected);
            } 
        },
        chooseThis : function(ide, tplSelected){
            if(ide.length > 0){
                var tplUsedData = Object.keys("");
                  //Check if this tpl already used before 
                if($.inArray(tplSelected,tplUsedData) != -1)    
                    cmsBuilder.tpl.switchTemplate(tplSelected);
                else{
                    cmsBuilder.block.duplicate(ide,tplSelected);
                    //Check if page never uses a template
                    if(cmsBuilder.config.tpl.activeId == "")
                      cmsBuilder.tpl.saveTplUser(tplSelected,"using");
                    else
                      cmsBuilder.tpl.switchTemplate(tplSelected);
                    

                    urlCtrl.loadByHash(location.hash);

                }

            }else{ 
              $(`<div class="col-md-12 sample-cms text-center custom-block-cms" data-id="undefined">
                <div id="cms-selected" class="selected cms-area-selected blink-info"><p><strong>Cette page ne contient aucun élément!</strong></p>Veuillez selectionner un bloc sur le menu à gauche</div>
                </div>`).insertBefore("#all-block-container");
                cmsBuilder.block.menuSelector.show();
            }
        },
        //Switch template used------------------------------------------
        switchTemplate : function (tplSelected){ 
            cmsBuilder.tpl.saveTplUser(tplSelected,"using");
            cmsBuilder.tpl.saveTplUser(cmsBuilder.config.tpl.activeId,"backup");
            urlCtrl.loadByHash(location.hash);
        },
    
        // Remove cms' stat---------------------------------------------
        removeCmsStat : function (idTpl,elementId){  
            var params = {
                "tplId" : idTpl,
                "ide" : elementId,
                "action" : "removestat" 
            };
            ajaxPost(
                null,
                baseUrl+"/co2/cms/getcmsaction",
                params,
                function(data){
                    toastr.success(tradCms.completed+"!!");
                },
                null,
                "json",
                {async : false}
            );
        },    
        saveThis : function(){
            if (cmsBuilder.config.idCmsMerged == "") {
                bootbox.alert(tradCms.emptyCmsTpl);
            }else{
                var tplCtx = {};
                var activeForm = {
                    "jsonSchema" : {
                        "title" : tradCms.registerTpl,
                        "type" : "object",
                        "properties" : {
                            name : {
                                label: tradCms.nameTpl,
                                inputType : "text",
                                rules:{
                                    "required":true
                                }
                            },
                            type : { 
                                "inputType" : "hidden",
                                value : "template" 
                            },
                            subtype : { 
                                "inputType" : "select",
                                label : tradCms.saveOptions,
                                rules:{
                                    "required":true
                                },
                                options : {
                                    site : tradCms.allSite,
                                    page : tradCms.thisPageOnly
                                },
                                placeholder : tradCms.selectAnOption
                            },
                            category : {
                                label : trad.category,
                                inputType : "select",
                                rules:{
                                    "required":true
                                },
                                options : cmsBuilder.config.tpl.listCategories,
                                placeholder : tradCms.selectAnOption 
                            },
                            image : dyFInputs.image(),
                            description : {
                                label: tradDynForm.description,
                                inputType : "text",
                                value : ""
                            }
                        },
                        beforeBuild : function(){
                            dyFObj.setMongoId('cms',function(data){
                                // uploadObj.gotoUrl = location.hash;
                                tplCtx.id=dyFObj.currentElement.id;
                                tplCtx.collection=dyFObj.currentElement.type;
                            });
                        },
                        save : function () {
                            tplCtx.value = {};
                            params = {};
                            $.each( activeForm.jsonSchema.properties , function(k,val) { 
                                tplCtx.value[k] = $("#"+k).val();
                            });
                            tplCtx.value.parent={};
                            tplCtx.value.parent[costum.contextId] = {
                                type : costum.contextType, 
                            };
                            if (tplCtx.value.subtype == "site"){    
                                params["allPage"] = true;
                                tplCtx.collection = "templates"
                                tplCtx.value.collection = "templates"
                                tplCtx.value.siteParams = {}
                                tplCtx.value.siteParams.css = costumizer.obj.css;
                                tplCtx.value.siteParams.app = costumizer.obj.app
                                tplCtx.value.siteParams.htmlConstruct = costumizer.obj.htmlConstruct;
                                ajaxPost(
                                    null,
                                    baseUrl+"/"+moduleId+"/cms/getallcms",
                                    params,
                                    function(data){ 
                                        mylog.log("allcsm",data.allCms);    
                                        tplCtx.value.cmsList = data.allCms;                
                                    },
                                    null,
                                    null,
                                    {
                                        async: false
                                    }
                                );

                            }else{
                                tplCtx.value.cmsList = cmsBuilder.config.block.idCmsMerged;
                                delete tplCtx.value.subtype;
                            }

                            tplCtx.value.tplsUser={};
                            tplCtx.value.tplsUser[costum.contextId] = {};
                            tplCtx.value.tplsUser[costum.contextId][cmsBuilder.config.page] ="using"
                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value( tplCtx, function(params) {
                                    dyFObj.commonAfterSave(params,function(){
                                        toastr.success(tradCms.elementwelladded);
                                        $("#ajax-modal").modal('hide');           
                                    }); 
                                })
                                // if (tplCtx.value.subtype == "page"){
                                //     dataHelper.unsetPath( {
                                //         id : tplCtx.id,
                                //         collection: "cms",
                                //         path : "subtype"
                                //     }, function(params) { })
                                // }
                                if(cmsBuilder.config.tpl.activeId != ""){
                                // cmsBuilder.tpl.saveTplUser(cmsBuilder.config.tpl.activeId,"backup");
                                }
                                //if "save as new", duplicate and leave currently cms then save it into the new template
                                if (Object.keys(cmsBuilder.config.block.cmsInUseId).length > 0) {                
                                    // cmsBuilder.block.duplicate(cmsBuilder.config.block.cmsInUseId,tplCtx.id);
                                }
                                // cmsBuilder.tpl.removeCmsStat(tplCtx.id,cmsBuilder.config.block.newCmsId);
                                // urlCtrl.loadByHash(location.hash);
                            }       
                        },
                    }
                 };    

                dyFObj.openForm( activeForm );
            }
        },
        
        //Preview template-------------------------------------------------------------
        preview :function () { 
          // localStorage.setItem("previewMode","v");
          // cmsBuilder.config.previewMode = true;
            $(hideOnPreview).fadeOut();
            // $(".hiddenEdit").show();
            var styleHideTitleOnPreview = 
            `<style id="hideBlocksCmsTitleOnHover">
              .block-parent:hover .title:not(:empty):before,.block-parent:hover .title-1:not(:empty):before,
              .block-parent:hover .subtitle:not(:empty):before,.block-parent:hover .title-2:not(:empty):before,
              .block-parent .description:not(:empty):before,.block-parent .title-3:not(:empty):before,
              .block-parent:hover .other:not(:empty):before,.block-parent:hover .title-4:not(:empty):before,
              .block-parent .title-5:not(:empty):before,
              .block-parent:hover .title-6:not(:empty):before{
                  display: none !important;
              }
            </style>`;
            $('head').append(styleHideTitleOnPreview);
            // cmsBuilder.config.previewMode = true;
            $(document).trigger('cms-view-mode');
        },
        bindPreview : function(){
            window.onhashchange = function() {
                if($(hideOnPreview).is(':visible') !=true){
                    $(hideOnPreview).fadeIn(); 
                    // cmsBuilder.config.previewMode = false;
                }
            }
          //  $(document).keyup(function(e) {
            //    if (e.key === "Escape") { 
                    // localStorage.setItem("previewMode","w");
              //      $(hideOnPreview).fadeIn();
                    // $(".hiddenEdit").hide();
                //    $("#hideBlocksCmsTitleOnHover").remove();
                    // cmsBuilder.config.previewMode = false;
                //}
            //});
        },
        edit : function () {      
            $(document).trigger('cms-edit-mode');
            cmsBuilder.tpl.textModeEdit();
        },
        textModeEdit : function(){
            $(".sp-text").css("visibility","visible")
            $('.sp-text').each(function(i, objsptext) {
                var allTexts = $(objsptext).html();
                // if (allTexts.match("Ecrire un texte")) {
                // }
                var p = document.createElement('div');
                p.innerHTML = allTexts;
                var links = p.querySelectorAll('.super-href');
                links.forEach(spx => {
                    allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"]("+spx.href+")");
                });
                $(objsptext).html(allTexts);
                var spcmstextId = document.querySelector('spcms');
                if (spcmstextId !== null) {
                    var texttextId = spcmstextId.textContent;
                    spcmstextId.parentNode.replaceChild(document.createTextNode(texttextId), spcmstextId);
                }
            });
        },
        initEvent: function(){
            $(".tryOtherTpl").off().on("click",function(){
                $("#listeTemplate").show();
                smallMenu.open($("#listeTemplate"));
            });
        },
        openModal : function(){},
        // Remove cmsList inside tpl if cms is empty--------------------
        removeEmptyBlockList : function (idTpl){  
            var params = {
                "tplId" : idTpl,
                "action" : "tplEmpty" 
            };
            ajaxPost(
                null,
                baseUrl+"/co2/cms/getcmsaction",
                params,
                function(data){
                    toastr.success(tradCms.completed+"!!");
                },
                null,
                "json",
                {async : false}
            );
        },
        // LIEN dans un texte convertisseur()
        convAndCheckLink : function(elems){              
          var current_full_Url = window.location.href;
            $(elems).each(function(i, objelems) {
                var derText = $(objelems).html();
                // if ($(objelems).text().match(trad.write)) {
                //     $(objelems).css("visibility","hidden")
                // }

                $(objelems).fadeIn("show");
                if (derText !== 'undefined') {
                    derText = derText.replace(/(?:\r\n|\r|\n)/g, '<br>');
                    let elements = derText.match(/\[.*?\)/g);
                    if( elements !== null && elements.length > 0){
                        for(el of elements){
                            if (el.match(/\[(.*?)\]/) !== null) {
                                let eltxt = el.match(/\[(.*?)\]/);
                                let elurl = el.match(/\((.*?)\)/);
                                if (elurl !== null && eltxt !== null) {            
                                    let txt = eltxt[1];
                                    let spUrl = elurl[1];
                                    if (spUrl.includes("#")) {                
                                        let textBeforHtag = spUrl.substring(0, spUrl.indexOf("#"));
                                        let textBefor_full_UrlHtag = current_full_Url.substring(0, current_full_Url.indexOf("#"));
                                        let textAfterHtag = spUrl.split('#')[1]

                                        let extLoadableUrls= (typeof costum!="undefined" && costum.app!="undefined") ? Object.keys(urlCtrl.loadableUrls).concat(Object.keys(costum.app)) : Object.keys(urlCtrl.loadableUrls);
                                        if(textAfterHtag.includes("?preview=")){
                                            if (textAfterHtag.includes("preview=poi")) {
                                                textAfterHtag = "page.type.poi.id."+textAfterHtag.split('poi.')[1]
                                            }else if (textAfterHtag.includes("preview=projects")) {
                                                textAfterHtag = "page.type.projects.id."+textAfterHtag.split('projects.')[1]
                                            }else if (textAfterHtag.includes("preview=organizations")) {
                                                textAfterHtag = "page.type.organizations.id."+textAfterHtag.split('organizations.')[1]
                                            }else if (textAfterHtag.includes("preview=citoyens")) {
                                                textAfterHtag = "page.type.citoyens.id."+textAfterHtag.split('citoyens.')[1]
                                            }else if (textAfterHtag.includes("preview=events")) {
                                                textAfterHtag = "page.type.events.id."+textAfterHtag.split('events.')[1]
                                            }else if (textAfterHtag.includes("preview=news")) {
                                                textAfterHtag = "page.type.news.id."+textAfterHtag.split('news.')[1]
                                            }else if (textAfterHtag.includes("preview=cities")) {
                                                textAfterHtag = "page.type.cities.id."+textAfterHtag.split('cities.')[1]
                                            }
                                            textAfterHtag = textAfterHtag.replace("?preview=","")
                                            derText = derText.replace(el,'<a class="lbh-preview-element super-href" href="#'+textAfterHtag+'">'+txt+'</a>');
                                        }else if (textBeforHtag == textBefor_full_UrlHtag || textBeforHtag == "") {   
                                            let hashToLoad = spUrl.slice(spUrl.lastIndexOf('#') + 1);
                                            derText = derText.replace(el,`<a class="lbh super-href" href="#`+hashToLoad+`">`+txt+`</a>`)          
                                        }else{
                                            derText = derText.replace(el,'<a target="_blank" class="super-href" href="'+spUrl+'">'+txt+'</a>');
                                        }
                                    }else{              
                                        derText = derText.replace(el,'<a target="_blank" class="super-href" href="'+spUrl+'">'+txt+'</a>')
                                    }
                                }else{
                                    derText = derText.replace(el,'<spcms style="color:red;background-color:whitesmoke;" data-toggle="tooltip" data-placement="top" title="<?php echo Yii::t("cms", "Syntax error. Please try again")?>!">'+el+'</spcms>')

                                }
                            }
                        }
                    }
                    $(objelems).html(derText);
                    $(objelems).css("cursor", "default")
                }

            }); 
           /* $(".btn-functionLink").off('click').on('click', function(){
                var internal = $(this).attr("data-internal");
                var external = $(this).attr("data-external");
                var targetBlank = $(this).attr("data-targetBlank");
                if(!costum.editMode){
                    if (internal === "" && external === ""){
                        if (isInterfaceAdmin) {
                            bootbox.alert({
                                message: `<br>
                                <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                <p class="padding-left-20">L'URL de ce bouton est vide!</p>
                                </div>
                                <p class="padding-left-20">Ce message apparaît pour l'administrateur uniquement</p>`
                                ,
                                size: 'medium'
                            });
                        }
                    } else {
                        if( external != ""){
                            if ( targetBlank == true ){
                                window.open(external);
                            } else {
                                window.open(external,"_self");
                            }
                        } else if ( internal != ""){
                            if ( targetBlank == true ){
                                internal = internal.replace("#","?h=");
                                window.open( window.location.href+internal, "_blank");
                            } else {
                                urlCtrl.loadByHash(internal)
                            }
                        }
                    }
                }
            });*/
        coInterface.bindLBHLinks()
        }
    },
    block : {
        orphans : [],
        reloadBlock : function(blockID){    
            showLoader(".sp-elt-"+blockID)       
          var required = {
                "idblock" : blockID ,
                "contextId" : costum.contextId,
                "contextType" : costum.contextType,
                "contextSlug" : costum.contextSlug,
                "page" : page,
                "clienturi" : location.href,
                "path" : path,
            };
            ajaxPost(
                null, 
                baseUrl+"/costum/blockcms/loadbloccms",
                required,
                function(data){
                     $(".sp-elt-"+blockID).html(data.html)
                     $(".sp-text").attr("placeholder",trad.writeatexthere)
                },
                {async:true}
            );
        },
        renderBlock : function(kunik, id, data, callback){
            if($(".block-container-"+kunik).children(".block-container-html").length == 0){
                $(".block-container-"+kunik).append('<div class="block-container-html">'+data.html+'</div>');
            }else{
                $(".block-container-"+kunik).children('.block-container-html').html(data.html);
            }
            if(($(".block-container-"+kunik).find("button").length > 0) || $(".block-container-"+kunik).find("a").length > 0){
                $(".cssHandle[data-kunik="+kunik+"]").remove();
            }
          
            cmsBuilder.block.initEvent(kunik,id); 
            cmsConstructor.block.events.init()       
            if(typeof callback=="function"){
                callback();
            }
        },
        initEvent : function(kunik, id){
            if(notNull(kunik)) $(".block-container-"+kunik).removeClass("is-loading-block");
            $(document).trigger("sp-loaded")
            if (costum.editMode != true) {
                cmsBuilder.tpl.convAndCheckLink(".sp-text");
                $('div[contenteditable]').removeAttr('contenteditable')
            }
            $(".sp-text").show()
             if(notNull(id)) $("."+id+"-load").remove();
            
            cmsBuilder.manage.delete();

            if (costum.editMode != true) {
                $(hideOnPreview).remove();
            }else if(costum.editMode){
                cmsConstructor.block.init()
                cmsConstructor.layer.init()
                cmsBuilder.events.cssHandle();
            }
        },
        loadIntoPage : function(id,page,path,kunik, callback){
           mylog.log("scroll "+kunik+" id "+id,costum)
           var pathBlocksExcludedMigrations = ["video", "timeline", "tags", "tab", "step", "social", "slide", "search", "ressources", "projets", "news", "multimedia", "misc", "meteolamer", "menu", "map", "header", "graph", "gallery", "footer", "filters", "faq", "events", "elements", "docs", "crowdfunding", "contact", "community", "coform", "article", "app", "annuaire", "affiche"];
           if (id != undefined && !$("."+id+"-load").hasClass('currently-loading')) {
              $("."+id+"-load").addClass("currently-loading")
              mylog.log($(".sortable-"+kunik+".custom-block-cms").attr('id'))
              var orderBlock = $(".sortable-"+kunik+".custom-block-cms").attr('id').split("-")[1];
              var required = {
                "orderBlock" : orderBlock,
                "idblock" : id ,
                "contextId" : costum.contextId,
                "contextType" : costum.contextType,
                "contextSlug" : costum.contextSlug,
                "page" : page,
                "clienturi" : location.href,
                "path" : path,
            };
            ajaxPost(
                null, 
                baseUrl+"/costum/blockcms/loadbloccms",
                required,
                function(data){
                    cmsBuilder.block.renderBlock(kunik, id,  data, callback);    
                    if (costum.editMode) {
                            $(".sp-text").attr("placeholder",trad.writeatexthere)
                    }    
                },
                {async:true}
            );
        }
    },
        initView : function(){
            cmsBuilder.block.menuSelector.init();
        },
        addNew: function($this){
            var strNewBlc='';
            var newOrder = parseInt($this.index())
            if(notNull($this.data("kunik"))){
                if(notNull($this.data("desire")) && $this.data("desire") == "below"){
                    strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined" data-order="'+newOrder+'">'+
                    '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
                    '</div>'
                    $(strNewBlc).insertAfter(".block-container-"+$this.data("kunik"));
                }else{    
                    newOrder = newOrder - 1
                    if((newOrder) < 0){
                        newOrder = 0
                    }
                    strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined" data-order="'+newOrder+'">'+
                  '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
              '</div>'
                    $(strNewBlc).insertBefore(".block-container-"+$this.data("kunik"));
                }
            }else{
                strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined" data-order="'+newOrder+'">'+
                  '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
              '</div>'
                $("#all-block-container").append(strNewBlc);
            }
            $([document.documentElement, document.body]).animate({
                scrollTop: $(".other-cms").offset().top - 150
            }, 500);
            // event.stopImmediatePropagation()
        },    
        //Delete cms----------------------------------------------------
        delete : function (ide){
            var params = {
                "ide" : ide,
                "action" : "delete" 
            }
            ajaxPost(
                null,
                baseUrl+"/co2/cms/getcmsaction",
                params,
                function(data){
                    toastr.success(tradCms.Blocksuccessfullydeleted);
                },
                null,
                "json",
                {async : false}
            );
        },

        duplicate: function (ide, blockParent=null/*, tplSelected, action = null*/){ 
            let parentKunik = $(".cmsbuilder-block [data-id="+ide+"]").parents(".custom-block-cms").data("kunik");
            let strLoading = `<div class="col-xs-12 text-center loader-contain load${parentKunik}">
            <div class="horizontal_bar"><div class="horizontal_bar_loader" style="background-color:${costum.css.loader.ring1.color ? costum.css.loader.ring1.color :"red"};height:5px;">
            <div class="horizontal_bar_loading" style="background-color:${costum.css.loader.ring2.color ? costum.css.loader.ring2.color :"green"};height:4px;"></div>
            </div></div></div>`
            var intervalBlocAdded = setInterval(function () {
                clearInterval(intervalBlocAdded);
                if ($(".load"+parentKunik).length > 0) {
                    let strTimeOut = `<p class="padding-top-20">Probleme de chargement!</p><i onclick="window.location.reload()" class="fa fa-repeat fa-2x cursor-pointer padding-top-10"></i>`
                    $(".load"+parentKunik).html(strTimeOut)
                }
            }, 30000)
            // let ctxPage = cmsBuilder.config.page;
            // // $(".cmsbuilder-block [data-id="+ide+"]").parents().append(strLoading);
            // if (action == "dplSection") {
            //     $(".cmsbuilder-block [data-id="+ide+"]").parents().append(strLoading);
            // }else if (action == "dplChild"){
            //     $(".sample-cms").replaceWith(response)
            // }
            // if (otherParams == "allPages") {
            //     ctxPage = "allPages";
            // }
            //if (tplKey != "")
              //  tplSelected = tplKey;
            var data = {   
                "idCmsToDuplic" : [ide],
                //"tplParent"     : tplSelected,
                "page"          : cmsBuilder.config.page,
                "costumId"      : costum.contextId,
                "costumSlug"    : costum.contextSlug,
                "costumType"    : costum.contextType,
               // "action"        : action
            }
            if(notNull(blockParent))
                data.blockParent=blockParent;
            // if (localStorage.getItem("addCMS") != null) {
            //     data["type"] = "blockCopy"
            //     localStorage.removeItem("addCMS");
            // }

            // if (localStorage.getItem("parentCmsIdForChild") != null) {
            //     data.tplParent = localStorage.getItem("parentCmsIdForChild")
            //     data["type"] = "blockChild"
            //     localStorage.removeItem("parentCmsIdForChild");
            // }

            ajaxPost(
                null,
                baseUrl+"/co2/cms/duplicateblock",
                data, 
                function (response) {
                    //if (action == "dplSection") {
                    //    $(".sample-cms").replaceWith(response.view)
                    //}else if (action == "dplChild"){
                    $(".sample-cms").replaceWith(response.view)
                    //}
                    cmsBuilder.block.initEvent();
                    cmsBuilder.block.updateOrder();
                    toastr.success(tradCms.elementwelladded);
                }
            );
      },

        use: function (id, tplSelected, otherParams = null){ 
            let parentKunik = $("div[data-id="+localStorage.getItem("parentCmsIdForChild")+"]").parents(".custom-block-cms").data("kunik");
            let strLoading = `<div class="col-xs-12 text-center loader-contain load${parentKunik}">
            <div class="horizontal_bar"><div class="horizontal_bar_loader" style="background-color:${costum.css.loader.ring1.color ? costum.css.loader.ring1.color :"red"};height:5px;">
            <div class="horizontal_bar_loading" style="background-color:${costum.css.loader.ring2.color ? costum.css.loader.ring2.color :"green"};height:4px;"></div>
            </div></div></div>`
            var intervalBlocAdded = setInterval(function () {
                clearInterval(intervalBlocAdded);
                if ($(".load"+parentKunik).length > 0) {
                    let strTimeOut = `<p class="padding-top-20">Probleme de chargement!</p><i onclick="window.location.reload()" class="fa fa-repeat fa-2x cursor-pointer padding-top-10"></i>`
                    $(".load"+parentKunik).html(strTimeOut)
                }
            }, 30000)
            let ctxPage = cmsBuilder.config.page;
            let type = "blockChild";
            if (otherParams == "tpl-footer") {
                ctxPage = "allPages";
                type = "blockCopy";
            }else{
                $("div[data-id="+cmsBuilder.config.blockParent+"]").html(strLoading);
            }
            if (tplKey != "")
                tplSelected = tplKey;
            var data = {   
                "id" : id,
                "tplParent"  : tplSelected,
                "page"       : ctxPage,
                "costumId"   : costum.contextId,
                "otherParams": otherParams,
                "costumSlug" : costum.contextSlug,
                "costumType" : costum.contextType,
                "type"       : type
            }


            ajaxPost(
                null,
                baseUrl+"/co2/cms/usecms",
                data, 
                function (response) {
                    if(cmsBuilder.config["blockParent"] == "tpl-footer"){
                        $(".footer-cms").html(response.html) 
                        var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
                        if($(".footer-cms").length > 0){
                            $(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
                        }
                        // cmsBuilder.block.use([$(this).data('id')],"", "tpl-footer" )
                    }else{                      
                       $(".load"+parentKunik).remove()
                       let id = $(".custom-block-cms[data-kunik="+parentKunik+"]").data("id");
                       let path = $(".custom-block-cms[data-kunik="+parentKunik+"]").data("path");
                       let kunik = $(".custom-block-cms[data-kunik="+parentKunik+"]").data("kunik");
                       cmsBuilder.block.loadIntoPage(id, page, path, kunik);

                       mylog.log("response",id, page, path, kunik );
                       
                       cmsBuilder.block.initEvent();
                       cmsBuilder.block.updateOrder();
                       toastr.success(tradCms.elementwelladded);
                    }
                    
                    cmsBuilder.block.initEvent();
                    cmsBuilder.block.updateOrder();
                    toastr.success(tradCms.elementwelladded);
                }
            );
      },
      updateOrder : function() {
        var data = {}
        var order = {}
        $.each( $(".custom-block-cms") , function(k,val) { 
            order[$(val).index()] = $(val).data("id")
        });
        data["order"] = order;
        ajaxPost(
            null,
            baseUrl+"/co2/cms/updateblockorder",
            data, 
            function (response) {}
            );
        },

        repairBlock : function(id) {
            $(".block-container-container"+id).find(".fa-search").replaceWith(`<i class="check${id} fa fa-spin fa-circle-o-notch" aria-hidden="true"></i>`)
            ajaxPost(
                null,
                baseUrl+"/co2/cms/repairecms",
                data = {
                    "idCms" : id,
                    "page" : page,
                    "check" : true
                }, 
                function (repairBlock) {
                    let res = repairBlock.result
                    cmsBuilder.block.orphans = res;

                    mylog.log("sq",repairBlock.result);
                    if (repairBlock.result != false) {
                        if (res.length > 0) {
                            bootbox.confirm({
                                message: `<div role="alert">
                                <p><b class="">There is </b> <b class="text-red">`+res.length+`</b> <b>CMS not found.</b><br><br>
                                Are you want to repaire?<br>
                                </p> 
                                </div> `,
                                buttons: {
                                    confirm: {
                                        label: trad.yes,
                                        className: 'btn btn-primary'
                                    },
                                    cancel: {
                                        label: trad.no,
                                        className: 'btn'
                                    }
                                },
                                callback: function (result) {
                                    if (result) {
                                      ajaxPost(
                                        null,
                                        baseUrl+"/co2/cms/repairecms",
                                        data = {
                                            "idCms" : id,
                                            "page" : page,
                                            "check" : false
                                        }, 
                                        function (response) {
                                            toastr.success(res.length+" CMS "+ trad.finish)
                                            urlCtrl.loadByHash(location.hash);
                                        }
                                        );  
                                  }else{                                    
                                    $(".block-container-container"+id).find(".check"+id).replaceWith(`<i class="fa fa-search" aria-hidden="true"></i>`)
                                }

                            }
                        });
                        }
                 }else{
                    toastr.info(repairBlock.msg)
                    $(".block-container-container"+id).find(".check"+id).replaceWith(`<i class="fa fa-search" aria-hidden="true"></i>`)
                 }    
             }
             );     
       },

      menuSelector:{
        init : function(){
          ajaxPost(
            null,
            baseUrl+"/co2/cms/getlistsbuilder",
            null , 
            function(d){
              //cmsBuilder.tpl.list=d.tpls;
              cmsBuilder.block.lists.menuSelector=d.blocks;       
              cmsBuilder.block.menuSelector.construct();
              cmsBuilder.block.menuSelector.initEvent();
            }
          );
        },
        construct: function(){
            var strMenu="";
            strMenu += ` 
            <div class="row panel-display-cms hiddenPreview">
      <div id="sp-main-cms" style="background-color: #2b2b2b;color:#ababab"> 
          <div style="height: 70px;display: flex;justify-content: space-around;padding-left: 24px;background-color: #404040;">
            <div class="searchBar-filters" style="width: 75%;margin-top: 10px;">
              <input id="searchInput" onkeyup="cmsBuilder.block.menuSelector.search()" type="text" class="form-control text-center main-search-bar search-bar" data-field="text" placeholder="Que recherchez-vous ?">
              <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text">
                <i class="fa fa-arrow-circle-right"></i>
              </span>
            </div>
          </div>
          <div class="floop" style="height:600px;text-shadow: 0px 0px 1px #000f10;">
            <nav class="tpl-engine-right">
              <ul id="list-cms-disponible">`
            $.each( cmsBuilder.block.lists.menuSelector , function(k,val) { 
                var path = val.path;    
                //callByName[path.replace(/\./g, '')] = val.name;
                strMenu+='<li class="cms-list padding-top-5 padding-bottom-5 black-default-cms">';
                            if(notNull(val.profilImageUrl)){
                              strMenu+='<a href="'+val.profilImageUrl+'" class="btn-link thumb-info display" data-title="Aperçu" data-lightbox="all" title="${val.description}">'+
                                  '<img class="fit-picture" style="height: 50px;width: 79px;border: solid 2px #bdbdbd;"src='+val.profilImageUrl+'>'+
                                '</a>';
                            }
                            else
                              strMenu+='<div class="sp-div-img"><i class="fa fa-image fa-3x" style="padding-left: 17px;padding-right: 19px;"></i></div>';
                            strMenu+='<a class="cms-disponible padding-left-10 text-white" data-path="'+val.path+'" data-id="'+k+'" href="javascript:;">'+
                                        '<div style="width: 215px; overflow: hidden;text-overflow: ellipsis;">'+val.name+'</div>'+
                                    '</a>'+
                                    // '<button class="btn editDescBlock" data-id="'+k+'" style="white-space: nowrap;background-color: #ffffff2e;color:white;border-radius:0px">'+
                                    //     '<i class="fa fa-edit"></i>'+
                                    // '</button>'+
                                    // '<button class="btn deleteCms" data-id="${k}" data-name="${val.name}" style="white-space: nowrap;background-color: #ffffff2e;color:white;border-radius:0px"><i class="fa fa-trash"></i></button>'+
                        '</li>';
            });
            strMenu += `</ul></nav></div></div></div>`;
            return strMenu;
            // $(".main-container .pageContent").append(strMenu);
            // $(".panel-display-cms").draggable();
        },
        initEvent:function(){
            $(".add-cms-here").off().on("click",function(event){
                localStorage.removeItem("parentCmsIdForChild");
                localStorage.setItem("addCMS", true)
                // cmsBuilder.block.menuSelector.show();
                $(".sample-cms").remove();
                cmsBuilder.block.duplicate(null, tplKey, "otherParams");
                // $(".add-cms-here").parent().parent().removeClass("tpl-menu-show");
                // $(".cms-area-selected").removeClass("cms-area-selected");
                // $(".add-cms-here").siblings(".selected").text("");
                // $(".add-cms-here").siblings(".cms-sample-options").hide(); 
                // cmsBuilder.block.initView()
                cmsBuilder.block.addNew($(this));
            });
            $(".close-list-cms").off().on("click",function(){
                $(".panel-display-cms").hide();
                if($(".sample-cms").text().length > 0){
                  $(".sample-cms").remove();
                }
            });
            $(".cms-duplicate-btn").off().on("click",function(event) {  
                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms sp-is-loading" data-id="undefined" data-order="">'+
                            '<div class="selected cms-area-selected blink-info">Chargement...</div>'+
                            '</div>'
                $(strNewBlc).insertAfter("#"+$(this).data("id"));
                var thisId = $(this).data('id')
                thisId = thisId.split("-")[0]    
                cmsBuilder.block.duplicate(thisId,"", "otherParams");
                event.stopImmediatePropagation();
            });

            // $(".save-as-cms").on("click", function(event) {  
            //     dataCms = {
            //         "jsonSchema" : {
            //             "title" : tradCms.setupsection,
            //             "description" : tradCms.customizesection,
            //             "icon" : "fa-cog",
            //             "properties" : {                    
            //                 "name" : {
            //                     "inputType" : "text",
            //                     label : "Nom du bloc"
            //                 },
            //                 "description" : {
            //                     "inputType" : "text",
            //                     label : "Descriptions"
            //                 } 
            //             },
            //             beforeBuild : function(){
            //             },
            //             save : function () {  
            //                 tplCtx.value = {};

            //                 $.each( dataCms.jsonSchema.properties , function(k,val) { 
            //                     tplCtx.value[k] = $("#"+k).val();
            //                 });
            //                 tplCtx.value.type = "blockCms";

            //                 if(typeof tplCtx.value == "undefined")
            //                     toastr.error('value cannot be empty!');
            //                 else {
            //                     dataHelper.path2Value( tplCtx, function(params) {
            //                         dyFObj.commonAfterSave(params,function(){
            //                             toastr.success(tradCms.elementwelladded);
            //                             $("#ajax-modal").modal('hide');
            //                             urlCtrl.loadByHash(location.hash);
            //                         });
            //                     } );
            //                 }
            //             }
            //         }
            //     };
            //     tplCtx.id = $(this).data("id");
            //     tplCtx.collection = "cms";
            //     tplCtx.path = "allToRoot";
            //     dyFObj.openForm( dataCms,null, dataCms);
            //     event.stopImmediatePropagation();
            // });
            // $(".cms-disponible").on("click", function(event) {  
            //     cmsBuilder.block.duplicate([$(this).data('id')],localStorage.getItem("parentCmsIdForChild"), "otherParams");
            //     $(".panel-display-cms").hide();
            //     // $(".sample-cms").remove()
            //     // urlCtrl.loadByHash(location.hash); 
            //     event.stopImmediatePropagation();
            // });
        },
        show : function() {
            $(".panel-display-cms").show();

            $("#list-cms-disponible li").on("mouseenter",function(){
            })
            $("#list-cms-disponible li").on("mouseleave",function(){
            })
        },
        search : function(){
            var input, filter, liste, item, i, txtValue;
            input = $('#searchInput');
            filter = input.val().toUpperCase();
            liste = $("#list-cms-disponible");
            item = $('.cms-list');
            ti = item.length;
            for (i = 0; i < item.length; i++) {
                paragraph = item[i].getElementsByTagName("a")[0];
                txtValue = paragraph.textContent || paragraph.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) 
                  item[i].style.display = "";
                else 
                  item[i].style.display = "none";
            }
        }
    }
    },
    bindEvents: function(){        
        if (costum.editMode != true) {
            $(hideOnPreview).remove();
        }
      $('body').click(function() {
          $(".tpl-menu-show").removeClass('tpl-menu-show');
      });
      $('block-parent').click(function() {
          $('.sp-text').blur()
      });
       

        var intervalEditDesBlocks = setInterval(function() {
            if ($(".editDescBlock").length > 0) {
                clearInterval(intervalEditDesBlocks);
                $(".editDescBlock").on("click", function() {
                    var tplId = $(this).data("id");
                    var tplCat = $(this).data("category");
                    var tplName = $(this).data("name");
                    var tplCollection = $(this).data("collection");
                    var tplDescription = $(this).data("description");
                    var dynFormCostum = {
                        "beforeBuild": {
                            "properties" : {
                                name : {
                                    label: tradCms.nameTpl,
                                    inputType : "text",
                                    value : tplName,
                                    rules:{
                                        "required":true
                                    }
                                },
                                type : { 
                                    "inputType" : "hidden",
                                    value : "template" 
                                },
                                description : {
                                    label : "Description",
                                    value : tplDescription
                                },
                                subtype : { 
                                    "inputType" : "select",
                                    label : "Options",
                                    rules:{
                                        "required":true
                                    },
                                    options : {
                                        site : "Site",
                                        page : "Page"
                                    },
                                    value: "site",
                                    placeholder : "Select an option" 
                                },
                                category : {
                                    label : trad.category,
                                    inputType : "select",
                                    rules:{
                                        "required":true
                                    },
                                    options : cmsBuilder.config.tpl.listCategories,
                                    value : tplCat,
                                    placeholder : "Select an option" 
                                },
                                image : dyFInputs.image(),
                            },
                        },
                        "onload": {
                            "actions": {
                                "setTitle": "Modifier le template",
                                "hide": {
                                    "documentationuploader": 1,
                                    "structagstags": 1,
                                    "parentfinder": 1,
                                    "descriptiontextarea": 0
                                }
                            }
                        },
                        afterSave: function() {
                            if (formData.subtype == "page") {
                                delSubtype = {
                                    id : tplId,
                                    collection: formData.collection,
                                    path : "subtype"
                                  }
                                  dataHelper.unsetPath( delSubtype, function(params) { }
                                );
                                costumizer.template.actions.changeCollection(formData.id, formData.collection)
                            }
                            if (formData.subtype == "site") {   
                                costumizer.template.actions.changeCollection(formData.id, formData.collection)  
                            }
                            dyFObj.commonAfterSave(params, function() {
                                urlCtrl.loadByHash(location.hash);
                                dyFObj.closeForm();
                                costumizer.template.actions.reload();
                            });
                        }
                    };
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/element/get/type/"+tplCollection+"/id/"+tplId+"",
                        {},
                        function (data) {    
                                if (data.map.subtype == undefined) {
                                    dynFormCostum.beforeBuild.properties.subtype.value = "page"
                                }   
                                dyFObj.editElement(tplCollection, tplId, null, dynFormCostum);
                            }      
                    );          
                });
            }
        }, 100);
        var intervalUseThis = setInterval(function () {
            if ($(".use-this").length > 0) {
                clearInterval(intervalUseThis);
                $(".use-this").on("click", function (e) {
                    e.stopImmediatePropagation
                    if (canUse) {
                        canUse = false;
                        //tplCtx.key = $(this).data("key");
                        var addedBlock = $(".custom-block-cms");
                        var idNewPosition = [];
                        // var saved = false;
                        var $this = $(this);
                        $this.html('<i class="fa  fa-spinner fa-spin"></i>').prop("disabled",true);
                        if (localStorage.getItem("parentCmsIdForChild") != null) {
                            tplCtx = {};
                            tplCtx.collection = $this.data("collection");
                            tplCtx.path = "allToRoot";
                            tplCtx.value = {
                                name: $this.data("name"),
                                type: "blockCopy",
                                path: $this.data("path"),
                                page: pageCms
                            };
        
                            tplCtx.value.parent = {};
        
                            tplCtx.value.parent[costum.contextId] = {
                                type: costum.contextType,
                                name: costum.contextSlug
                            };
                            tplCtx.value.haveTpl = false;
                            tplCtx.value.tplParent = localStorage.getItem("parentCmsIdForChild");
                            tplCtx.value.type = "blockChild";
                            delete tplCtx.value.haveTpl;
                            if(typeof tplCtx.value == "undefined"){
                                toastr.error("value cannot be empty!");
                            }else{
                                dataHelper.path2Value( tplCtx, function(params) {
                                    if(params.result){
                                            cmsChildren[localStorage.getItem("parentCmsIdForChild")].push(params.saved.id);
                                            upParent = {
                                                id : localStorage.getItem("parentCmsIdForChild"),
                                                collection : "cms",
                                                path : "cmsList",
                                                value : cmsChildren[localStorage.getItem("parentCmsIdForChild")]
                                            };
                                            dataHelper.path2Value( upParent, function(params) {
                                              localStorage.removeItem("parentCmsIdForChild")
                                              toastr.success("Mise à jour enregistré");
                                              $("#ajax-modal").modal("hide");
                                              urlCtrl.loadByHash(location.hash);
                                            });
                                        }else{
                                            toastr.error(params.msg);
                                    }
                                });
                            }
                        }else{
                            $.each(addedBlock , function(k,thisVal) { 
                                if ($(thisVal).data('id') == "undefined") {
                                     tplCtx = {};
                                     tplCtx.collection = $this.data("collection");
                                     tplCtx.path = "allToRoot";
                                     tplCtx.value = {
                                        name: $this.data("name"),
                                        type: "blockCopy",
                                        path: $this.data("path"),
                                        page: pageCms,
                                        position : k
                                    };
        
                                    tplCtx.value.parent = {};
        
                                    tplCtx.value.parent[costum.contextId] = {
                                        type: costum.contextType,
                                        name: costum.contextSlug
                                    };
                                    tplCtx.value.haveTpl = false;
                                    if (localStorage.getItem("parentCmsIdForChild") != null) {
                                      tplCtx.value.tplParent = localStorage.getItem("parentCmsIdForChild");
                                      tplCtx.value.type = "blockChild";
                                      delete tplCtx.value.haveTpl;
                                      localStorage.removeItem("parentCmsIdForChild")
                                    }
                                    if(typeof tplCtx.value == "undefined"){
                                        toastr.error("value cannot be empty!");
                                    }else{
                                        dataHelper.path2Value( tplCtx, function(params) {
                                            if(params.result){
                                                toastr.success("Mise à jour enregistré");
                                                $("#ajax-modal").modal("hide");
                                                urlCtrl.loadByHash(location.hash);
                                            }else{
                                                toastr.error(params.msg);
                                            }
                                        });
                                    }
                                }else{                
                                    idNewPosition.push($(thisVal).data('id')+"-"+k);
                                }
                            });
                            if (addedBlock.length > 1) {
                                var params = {
                                    id : costum.contextId,
                                    collection : costum.contextType,
                                    useCase : "new_blockCMS",
                                    idAndPosition : idNewPosition
                                }
                                ajaxPost(
                                    null,
                                    baseUrl+"/costum/blockcms/saveposition",
                                    params,
                                    function(data){
                                        if(data.result)
                                            toastr.success("Position sauvegardé");
                                        else
                                            toastr.success(trad.somethingwrong);
                                    }
                                );
                            }
                        }
                    }
                });
            }
        }, 100);

    },
    manage: {
        insertCMS : function() {
            var tplCtx = {};
            var dynFormCostum = {
                beforeBuild: {
                    "properties": {
                        image: dyFInputs.image(),
                        path: {
                            label: "Chemin",
                            inputType: "text",
                            value: "tpls.blockCms.[type de block].[nom du block]",
                            order: 2
                        },
                        type: {
                            "inputType": "hidden",
                            value: "blockCms"
                        }
                    }
                },
                onload: {
                    "setTitle": "Créer un nouveau cms",
                    "actions": {
                        "html": {
                            "nametext>label": "Nom du bloc cms",
                            "infocustom": ""
                        },
                        "hide": {
                            "documentationuploader": 1,
                            "structagstags": 1
                        }
                    }
                },
                afterSave: function(params) {
                    dyFObj.commonAfterSave(params, function() {
                        toastr.success("Élément bien ajouter");
                        window.location.reload();
                    });
                }
            };
            dyFObj.openForm("cms", null, null, null, dynFormCostum);
        },

        delete: function() {
            var intervalDeleteLine = setInterval(function() {
                if ($(".deleteLine").length > 0) {
                    clearInterval(intervalDeleteLine);
                    $(".deleteLine").on("click", function(e) {
                        e.stopImmediatePropagation()
                        // $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                        var id = $(this).attr('data-id');
                        let kunikToDelete = $(this).attr("data-kunik");
                        var type = $(this).attr("data-collection");
                        var urlToSend = baseUrl+"/co2/cms/delete/id/"+id;
                        bootbox.confirm("<font size='5' class='text-red'>"+trad.delete+" "+$(this).attr("data-name")+":</font><br>"+trad.areyousuretodelete,
                            function(result) {
                                if (!result) {
                                    return;
                                } else {

                                    ajaxPost(
                                        null,
                                        urlToSend,
                                        null,
                                        function(data) {
                                            if (data && data.result) {
                                                var idparent = $("div[data-id="+id+"]").parents('.custom-block-cms').data('id');
                                                if(idparent == id){
                                                    toastr.success("Element effacé");
                                                    $('.block-container-'+kunikToDelete).remove();
                                                }else{
                                                    // $(superCms.parents).append(superCms.element);
                                                    var path = $("div[data-id="+id+"]").parents(".custom-block-cms").data("path");
                                                    var kunik = $("div[data-id="+id+"]").parents(".custom-block-cms").data("kunik");
                                                    cmsBuilder.block.loadIntoPage(idparent, page, path, kunik);
                                                }
                                                // $("#supercms-overlay").empty();
                                                // $("#supercms-overlay").css({"display": "none"});
                                                $("#toolsBar-edit-block").hide(); 
                                                coInterface.initHtmlPosition();
                                                // $(".sp-elt-" + id).remove();
                                                // ($(".custom-block-cms").find("[data-id='" + id + "']")).remove(); 
                                            } else {
                                                toastr.error(data.msg);
                                            }
                                        },
                                        null,
                                        "json"
                                    );
                                }
                            }
                        );       
                    });
                }
            }, 100);
        }
    }  

};