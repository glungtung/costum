costumizerV2.on(costumizerV2.actions.ADD_BLOCK, function(payload){
    var blockToAdd = payload.block,
        /**
         * Le target contient les informations de parent qui va recevoir le block
         * 
         * target.type = "block" (si c'est un bock dans la page) ou "page" (si on va l'ajouter dans la page)
         * //les autres paramètres et juste si on a target.type = "block"
         * target.position = "in" ou "before" ou "after" 
         * target.id = c'est l'id du block cible
         */
        target = payload.target;

    /* add the business logic here */

    alert(`Add the business logic of ADD_BLOCK in: "modules/costum/assets/cmsBuilder/js/handlers/pageEventHandlers.js:15"`)
})

costumizerV2.on(costumizerV2.actions.CHANGE_SCREEN_MODE, function(payload){
    var mode = payload.mode; //sm || md || lg

    /* add the business logic here */

    alert(`Add the business logic of CHANGE_SCREEN_MODE in: "modules/costum/assets/cmsBuilder/js/handlers/pageEventHandlers.js:23"`)
})

costumizerV2.on(costumizerV2.actions.OPEN_PREVIEW, function(payload){
    /* add the business logic here */

    alert(`Add the business logic of OPEN_PREVIEW in: "modules/costum/assets/cmsBuilder/js/handlers/pageEventHandlers.js:29"`)
})
