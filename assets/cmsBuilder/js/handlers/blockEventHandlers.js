costumizerV2.on(costumizerV2.actions.MOVE_BLOCK_TO_TOP, function(payload){
    var id = payload.id,
        name = payload.name,
        path = payload.path;
        

    /* add the business logic here */

    alert(`Add the business logic of MOVE_BLOCK_TO_TOP in: "modules/costum/assets/cmsBuilder/js/handlers/blockEventHandlers.js:8"`)
})

costumizerV2.on(costumizerV2.actions.MOVE_BLOCK_TO_BOTTOM, function(payload){
    var id = payload.id,
        name = payload.name,
        path = payload.path;

    /* add the business logic here */

    alert(`Add the business logic of MOVE_BLOCK_TO_BOTTOM in: "modules/costum/assets/cmsBuilder/js/handlers/blockEventHandlers.js:18"`)
})

costumizerV2.on(costumizerV2.actions.DUPLICATE_BLOCK, function(payload){
    var id = payload.id,
        name = payload.name,
        path = payload.path;

    /* add the business logic here */

    alert(`Add the business logic of DUPLICATE_BLOCK in: "modules/costum/assets/cmsBuilder/js/handlers/blockEventHandlers.js:28"`)
})

costumizerV2.on(costumizerV2.actions.DELETE_BLOCK, function(payload){
    var id = payload.id,
        name = payload.name,
        path = payload.path;

    /* add the business logic here */

    alert(`Add the business logic of DELETE_BLOCK in: "modules/costum/assets/cmsBuilder/js/handlers/blockEventHandlers.js:38"`)
})
