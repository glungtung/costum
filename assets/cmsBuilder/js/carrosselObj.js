
var carouselObj = {
    dataElement :{},
    costumDefaultImage:{},
    initCarousel: function(paramsData, opt) {
        var options = {
            onlyExternal: true,
            slidesPerView: 1,
            spaceBetween: 10,
            keyboard: {
                enabled: true,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: exists(opt.xs) ? opt.xs : 1,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: exists(opt.md) ? opt.md : 2,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: exists(opt.lg) ? opt.lg : 4,
                    spaceBetween: 20,
                },
            }
        };
        if (exists(paramsData["cardAutoPlay"]) && paramsData["cardAutoPlay"] == true) {
            options.autoplay = {
                delay: 6000,
                disableOnInteraction: true,
            }
        }
        if(exists(paramsData["elDesign"]) && (paramsData["elDesign"] != 6))
            var swiper = new Swiper(opt.container, options);
    },
    bindEvents: function(paramsData, kunik) {
        $('.delete-element-' + kunik).on('click', function() {
            var idEvent = $(this).data('id');
            bootbox.confirm(trad.areyousuretodelete, function(result) {
                if (result) {
                    var url = baseUrl + "/" + moduleId + "/element/delete/id/" + idEvent + "/type/" + paramsData["elType"];
                    var param = new Object;
                    ajaxPost(null, url, param, function(data) {
                        if (data.result) {
                            toastr.success(data.msg);
                            urlCtrl.loadByHash(location.hash);
                        } else {
                            toastr.error(data.msg);
                        }
                    })
                }
            })
        });
        $('.edit-element-' + kunik).on('click', function() {
            if(paramsData["elType"] != "poi"){
                dyFObj.editElement(paramsData["elType"],$(this).data("id"));
            }else{
                dyFObj.editElement(paramsData["elType"],$(this).data("id"),paramsData["elPoiType"]);
            }
        });
        $('.container1-' + kunik + ' .smartgrid-slide-element').removeClass('col-lg-4 col-md-4 col-sm-6');
        $('.container1-' + kunik + ' .swiper-slide .lbh').removeClass('lbh').addClass('lbh-preview-element');
        $('.container1-' + kunik + ' .swiper-slide .see-more').removeClass('lbh-preview-element').addClass('lbh');
        if(paramsData["elType"] == "poi")
            $(".stdr-elem-content-"+kunik+" button.add").text(exists(trad["add"+paramsData["elPoiType"]]) ? trad["add"+paramsData["elPoiType"]] : "Ajouter "+ paramsData["elPoiType"])
        else
            $(".stdr-elem-content-"+kunik+" button.add").text(exists(trad["add"+paramsData["elType"]]) ? trad["add"+paramsData["elType"]] : "Ajouter")

        document.querySelectorAll('img.lzy_img').forEach((v) => {
            imageObserver.observe(v);
        });
        coInterface.bindLBHLinks();
    },
    toSameHeight: function(selector = null) {
        var maxHeight = Math.max.apply(null, $(selector).map(function() {
            return $(this).height();
        }).get());
        $(selector).css("height", maxHeight);
    },
    commonProperties : function(paramsData){
        var properties = {
            "title" : {
                "inputType" : "text",
                "label" : "Titre",
            },
            "title2" : {
                "inputType" : "text",
                "label" : "Sous titre",
            },
            "titleBg" : {
                "inputType" : "colorpicker",
                "label" : "Fond du titre",
            },
            "elType" : {
                "inputType" : "selectMultiple",
                "isSelect2" : true,
                  "label" : "Type de l'élément",
                  "options" : elTypeOptions,
            },
            "elPoiType" : {
                  "inputType" : "select",
                  "label" : "Type de "+trad.poi,
                  "options" : poiOptions,
            },
            "elDesign" : {
              "inputType" : "select",
                "label" : "Mode d'affichage",
                "options" : {
                  "1" : "Carousel multi-carte configurable",
                  "3" : "Non carousel multi-carte configurable",
                  "2" : "Carousel à une carte horizontal",
                  "6" : "Non Carousel à une carte horizontal",
                  "4" : "Carousel multi-carte de communecter",
                  "5" : "Non Carousel multi-carte de communecter",
                  "7" : "Carte rotatif",
                },
                "noOrder":true    
            },
            "imgShow" : {
                "label": "Afficher l'image",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },
            "imgWidth" : {
                "inputType" : "text",
                "label" : "Largeur de l'image (% ou px)",
                "placeholder" : "359px"
            },
            "imgHeight" : {
                "inputType" : "text",
                "label" : "Hauteur de l'image",
                "placeholder" : "359px"
            },
            "imgRound" : {
                "inputType" : "text",
                "label" : "Image ronde (% ou px)",
                "placeholder" : "50%"
            },
            "imgBg" :{
                "inputType" : "colorpicker",
                "label" : "Fond de l'image",
            },
            "imgBorder" : {
                "inputType" : "text",
                "label" : "Epaisseur de la bordure (px)",
                "placeholder" : "5px"
            },
            "imgBorderColor" :{
                "inputType" : "colorpicker",
                "label" : "Couleur de la bordure",
            },
            "imgDefault" : {
                "label": "Image par défaut d'un item",
                "inputType" : "uploader",
                "docType": "image",
                "contentKey" : "slider",
                "itemLimit" : 1,
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "showUploadBtn": false,
                "domElement" : "image",
                "endPoint" :"/subKey/image",
            },
            "imgFit" : {
                "inputType" : "select",
                "label" : "Taille del'image", 
                "options" : {
                    "auto" : "auto", 
                    "length" : "length", 
                    "cover" : "cover", 
                    "contain" : "contain", 
                    "initial" : "initial", 
                    "inherit" : "inherit"
                }
            },
            "cardBoxShadow" : {
              "label": "Ombre sur la carte",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },
            "cardBg" : {
                "inputType" : "colorpicker",
                "label" : "Fond de la carte",
            },
            "cardNbResult" : {
                "inputType" : "text",
                "label" : "Nombre de resultat",
                rules : {
                  number:true,
                  max:100,
                  min:0,
                },
            },
            "cardBtnShowMore" : { 
              "label": "Bouton voir plus",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },
            "cardOrderBy" : { 
                "label": "Trier par",
                "inputType" : "select",
                "options" : {
                    "name" : "Nom",
                    "created" : "Date de création",
                    "updated" : "Date de modification",
                    "startDate" : "Date de début"
                }                
            },
            "cardOrderType" : { 
                "label": "Ordre",
                "inputType" : "select",
                "options" : {
                    "1" : "Croissant",
                    "-1" : "Décroissant",
                }                
            },
            "infoBg" : {
                "inputType" : "colorpicker",
                "label" : "Fond des informations",
            },
            "infoHeight" : {
              "label": "Hauteur des informations textuelle",
                "inputType" : "text",
                "placeholder" :"default : auto"
            },
            "showName" : { 
                "label": "Nom",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showType" : { 
                "label": "Type",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },                
            "showShortDescription" : { 
                "label": " Description courte",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDescription" : { 
                "label": "Description longue",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDate" : { 
                "label": "Date",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDateOnly" : {
                "label": "Date uniquement (pas d'heure)",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showYearOnly" : {
                "label": "Montrer l'année",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDayOnly" : {
                "label": "Montrer le jour",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDayNumberOnly" : {
                "label": "Montrer le jour (sans le nom de jour de la semaine )",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showMonthOnly" : {
                "label": "Montrer le mois",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showAdress" : { 
                "label": "Adresse",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showTags" : { 
                "label": "Mots clef",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showRoles" : { 
                "label": "Roles",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showLinks" : { 
                "label": "Liens",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "formTitle" : {
                "label": "Titre",
                "inputType" : "text",          
            },
            "buttonShowAll" : {
                "label": "Bouton afficher tout les élément",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "buttonShowAllType" : {
                "label": "Afficher le type",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "buttonShowAllText" : {
                "label": "Text du bouton afficher tout les élément",
                "inputType" : "text",
            },
            "buttonShowAllWdColor" : {
                "label": "Couleur du bouton",
                "inputType" : "colorpicker",
            },
            "buttonShowAllWdBorderColor" : {
                "label": "Couleur de la bordure",
                "inputType" : "colorpicker",
            },
            "buttonShowAllWdFontColor" : {
                "label": "Couleur du texte",
                "inputType" : "colorpicker",
            },
            "buttonShowAllWdFontweight" : {
                "label": "Epaisseur du texte du bouton",
                "inputType" : "text",
                rules : {
                    number:true,
                },
            },
            "buttonShowAllWdFontSize" : {
                "label": "Taille du texte du bouton",
                "inputType" : "text",
            },
            "buttonShowAllWdFontFamily" : {
                "label": "Famille de polices",
                "inputType" : "select",
                "options" : {},
            },
            "buttonShowAllWdPaddingVertical" : {
                "label": "Rembourage en haut et en bas",
                "inputType" : "text",
            },
            "buttonShowAllWdPaddingHorizontal" : {
                "label": "Rembourage à droite et à gauche",
                "inputType" : "text",
            },
            "buttonShowAllMore" : {
                "label": "Personnaliser le boutton",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "buttonShowAllContentView" : {
                "inputType" : "select",
                  "label" : "Mode d'affichage du contenu",
                  "options" : {
                    "1" : "Disposition en grille",
                    "2" : "Disposition en liste",
                  },
                  "noOrder":true    
            },
            "buttonShowAllWdBorderRadius" : {
                "label": "Rayon de la bordure",
                "inputType" : "text",
            },
            "buttonShowAllContentView" : {
                "inputType" : "select",
                  "label" : "Mode d'affichage du contenu",
                  "options" : {
                    "1" : "disposition en grille",
                    "2" : "disposition en liste",
                  },
                  "noOrder":true    
            },
            "btnEditDelete" : {
                "label": "Bouton édition et suppression ",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "btnAdd" : {
                "label": "Bouton ajouter",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "btnChoose" : {
                "label": "Choisir les éléments à afficher ?",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "btnCreateNewEvent" : {
                "label": "Afficher le bouton d'ajout de l'element",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "btnShowAllParticipant" : {
                "label": "Montrer le boutton qui affiche les participants",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            }
        };

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] != 3){
            properties["cardAutoPlay"] = { 
              "label": "Slide automatique",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            }
        }

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] == 3){ 
            properties["cardSpaceBetween"] = {
                "inputType" : "text",
                "label" : "Espace entre les cartes",
                rules : {
                  number:true,
                  max:100,
                  min:0,
                },
            }          
        }

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] == 1 || paramsData["elDesign"] == 3){
            properties["infoDistanceFromImage"] = {
                "inputType" : "text",
                "label" : "Distance des infos et image('ex: -150px')",
                "placeholder" : "-144px"
            };
            properties["infoMarginLeft"] = {
                "inputType" : "text",
                "label" : "Marge à gauche",
                "placeholder" : "0px"
            };
            properties["infoMarginRight"] = {
                "inputType" : "text",
                "label" : "Marge à droite",
                "placeholder" : "0px"
            };
            properties["infoBorderRadius"] = {
                "inputType" : "text",
                "label" : "Rayon de bordure",
                "placeholder" : "0px"
            };
            properties["infoOnHover"] = { 
                "label": "Afficher les informations au survol",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            };
        }

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] == 2 || exists(paramsData["elDesign"]) &&  paramsData["elDesign"] == 6) {
            properties["infoAlign"] = {
                "inputType" : "select",
                "label" : "Alignement",
                "options" : {
                  "flex-start" : "Gauche",
                  "center" : "Centre",
                  "flex-end" : "Droite"
                }           
            };
            properties["infoOrder"] = {
                "inputType" : "checkboxSimple",
                "label" : "Image à gauche",
                "params" : checkboxSimpleParams,    
            };
            properties["infoCenter"] = {
                "inputType" : "checkboxSimple",
                "label" : "Centrer verticalement",
                "params" : checkboxSimpleParams,
            };
        }

        if(exists(paramsData["elDesign"]) && paramsData["elType"] == "events"){
            properties["infoDateFormat"] = {
                  "inputType" : "select",
                  "label" : "Format de la date",
                  "options" : {
                    "1" : "Format 1",
                    "2" : "Format 2"
                }       
            }
        }
        
        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] != 2 && paramsData["elDesign"] != 6){    
            properties["cardNumberPhone"] = {
                "inputType" : "text",
                "label" : "Nombre de la carte sur téléphone",
                rules : {
                  number:true,
                  max:2,
                  min:0,
                },
            };
            properties["cardNumberTablet"] = {
                "inputType" : "text",
                "label" : "Nombre de la carte sur tablet",
                rules : {
                  number:true,
                  max:3,
                  min:0,
                },
            };
            properties["cardNumberDesktop"] = {
                "inputType" : "text",
                "label" : "Nombre de la carte sur desktop",
                rules : {
                  number:true,
                  max:10,
                  min:0,
                },
            };
        }
        return properties;
    },
    //******************FIRST DESIGN*************************/
    multiCart: {
        init: function(paramsData, kunik,autocomplete=true,externalData=null) {
            var $this = this;
            $this.cssCarousel(paramsData, kunik);
            if ($(".container1-" + kunik + " .swiper-wrapper").length <= 0) {
                $this.fetchData(paramsData, kunik,autocomplete,externalData);
            }
            var carOption = {
                xs: exists(paramsData["cardNumberPhone"]) ? paramsData["cardNumberPhone"] : 1,
                md: exists(paramsData["cardNumberTablet"]) ? paramsData["cardNumberTablet"] : 2,
                lg: exists(paramsData["cardNumberDesktop"]) ? paramsData["cardNumberDesktop"] : 3,
            }

            if (exists(paramsData["elDesign"]) && paramsData["elDesign"] != 3)
                carOption["container"] = ".container1-" + kunik + " .swiper-container";

            setTimeout(function() {
                carouselObj.initCarousel(paramsData, carOption);
                carouselObj.toSameHeight(".container1-" + kunik + " .el-info-container>div");
                carouselObj.bindEvents(paramsData, kunik);
            }, 2500)
            mylog.log("carOptionko", carOption)
        },
        fetchData: function(paramsData, kunik,autocomplete=true,externalData=null) {
            var html = "";
            var dataEl = {} ;
            var dataCount=0;
            var contextParent = "parent."+costum.contextId;
            var cardOrderBy = paramsData["cardOrderBy"];
            var cardOrderType =paramsData["cardOrderType"];
            params = {
                sortBy :{ [cardOrderBy] : cardOrderType},
                fields : ["profilImageUrl","category","urls"],
                filters :{
                    '$or':{},
                    '$and':[]
                },
                forced : {
                    filters : {}
                }
            };
            if(typeof paramsData.elType == "string")
                params.searchType=[paramsData["elType"]];
            else
                params.searchType=paramsData["elType"];
            mylog.log("params.searchType",params.searchType);
            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true  || paramsData["elDesign"] == 3|| paramsData["elDesign"] == 5|| paramsData["elDesign"] == 6)
                params.indexStep = 0
            if(paramsData["elType"] == "events" ||  $.inArray( "events", paramsData.elType) != -1){
                params.filters['$or'][contextParent] = {'$exists':true};
                params.filters['$or']["source.keys"] = {'$exists':true};
                params.filters['$or']["organizer."+costum.contextId] = {'$exists':true};

            }else if (paramsData["elType"] == "costum") {
                params.searchType = ["organizations","projects"];
                params.filters['$or']['$and'] = [
                    {"costum.slug" :{'$not':{'$regex':"cocity"}}},
                    {"costum.slug" :{'$exists':true}}
                ],
                params.notSourceKey = true;
                params.indexStep = 0;
            } else{
                params.filters['$or'][contextParent] = {'$exists':true};
                params.filters['$or']["source.keys"] = costum.contextSlug;

            }
                
            /*if (exists(paramsData["cardNbResult"]) && paramsData["cardNbResult"] != "")
                params["indexStep"] = paramsData["cardNbResult"];*/
            if (exists(paramsData["elPoiType"]) && exists(paramsData["elType"]) && paramsData["elType"]=="poi" && paramsData["elPoiType"] != ""){
                params.filters.type = paramsData["elPoiType"];
                params.types = ["poi"];
            }

            if(autocomplete == true)
                ajaxPost(
                    null,
                    baseUrl + "/" + moduleId + "/search/globalautocomplete",
                    params,
                    function(data) {
                        dataCount = Object.keys(data.results).length;
                        var i = 1;
                        
                        
                        if(typeof paramsData.elType == "object" && exists(paramsData["elPoiType"]) && paramsData["elPoiType"] != "" && $.inArray( "poi", paramsData.elType) != -1){
                            $.each(data.results, function(k, v) {
                                if((v.type == paramsData["elPoiType"] && v.collection =="poi") || v.collection !="poi"){                                    
                                    dataEl[k] = v;
                                }
                            });
                            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {
                                $.each(paramsData["element"], function(ke, ve){
                                    if(exists(data.results[ve])){
                                        var v = data.results[ve];
                                        if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 4 || paramsData["elDesign"] == 5)) {
                                            v = directory.prepParamsHtml(v);
                                            html += `<div class="swiper-slide">`;
                                            if (paramsData["elType"] == "events")
                                                html += directory.eventPanelHtml(v);
                                            else if (paramsData["elType"] == "classifieds")
                                                html += directory.classifiedPanelHtml(v);
                                            else
                                                html += directory.elementPanelHtml(v);
                    
                                            if (isInterfaceAdmin && paramsData["btnEditDelete"]==true) {
                                                html += `
                                                    <button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                <i class="fa fa-trash"></i>
                                                    </button>
                                                `;
                                            }
                                            html += `</div>`;
                                        } else{                                  
                                            html += carouselObj.multiCart.view(v, paramsData, kunik);
                                        }
                                    }
                                });
                            }else{
                                $.each(data.results, function(k, v) {
                                    if((v.type == paramsData["elPoiType"] && v.collection =="poi") || v.collection !="poi"){
                                        if( i <= paramsData["cardNbResult"]){
                                            if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 4 || paramsData["elDesign"] == 5)) {
                                                v = directory.prepParamsHtml(v);
                                                html += `<div class="swiper-slide">`;
                                                if (paramsData["elType"] == "events")
                                                    html += directory.eventPanelHtml(v);
                                                else if (paramsData["elType"] == "classifieds")
                                                    html += directory.classifiedPanelHtml(v);
                                                else
                                                    html += directory.elementPanelHtml(v);
            
                                                if (isInterfaceAdmin && paramsData["btnEditDelete"]==true) {
                                                    html += `
                                                        <button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                    <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                    <i class="fa fa-trash"></i>
                                                        </button>
                                                    `;
                                                }
                                                html += `</div>`;
                                            } else{
                                                html += carouselObj.multiCart.view(v, paramsData, kunik);                                  
                                            }
                                        }
                                        i++;  
                                    }
                                })
                            }                               
                        }else{
                            dataEl[paramsData["elType"]] = data.results;
                            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {                                   
                                $.each(paramsData["element"], function(ke, ve){
                                    if(exists(data.results[ve])){
                                        var v = data.results[ve];
                                        if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 4 || paramsData["elDesign"] == 5)) {

                                        v = directory.prepParamsHtml(v);
                                            html += `<div class="swiper-slide">`;
                                            if (paramsData["elType"] == "events")
                                                html += directory.eventPanelHtml(v);
                                            else if (paramsData["elType"] == "classifieds")
                                                html += directory.classifiedPanelHtml(v);
                                            else
                                                html += directory.elementPanelHtml(v);
        
                                            if (isInterfaceAdmin && paramsData["btnEditDelete"]==true) {
                                                html += `
                                                    <button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                <i class="fa fa-trash"></i>
                                                    </button>
                                                `;
                                            }
                                            html += `</div>`;
                                        } else{
                                            html += carouselObj.multiCart.view(v, paramsData, kunik);                                  
                                        }
                                    }
                                    
                                });
                            }else{
                                $.each(data.results, function(k, v) {
                                    if( i <= paramsData["cardNbResult"]){
                                        if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 4 || paramsData["elDesign"] == 5)) {
                                            v = directory.prepParamsHtml(v);
                                            html += `<div class="swiper-slide">`;
                                            if (paramsData["elType"] == "events") {
                                                v["btnShowParticipant"] = paramsData["btnShowAllParticipant"];
                                                html += directory.eventPanelHtml(v);
                                            }
                                            else if (paramsData["elType"] == "classifieds")
                                                html += directory.classifiedPanelHtml(v);
                                            else
                                                html += directory.elementPanelHtml(v);
        
                                            if (isInterfaceAdmin && paramsData["btnEditDelete"]==true) {
                                                html += `
                                                    <button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                                <i class="fa fa-trash"></i>
                                                    </button>
                                                `;
                                            }
                                            html += `</div>`;
                                        } else{
                                            html += carouselObj.multiCart.view(v, paramsData, kunik);                                  
                                        }
                                    } 
                                    i++;
                                });
                            }
                        }

                        if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"]==true){
                        if (paramsData["elType"] == "events")
                            html +=
                            `<div class="swiper-slide">
                                <a href="#agenda" class="btn btn-sm btn-primary lbh see-more">
                                    <i class="fa fa-plus"></i> VOIR PLUS
                                </a>
                            </div>`;
                        else if (paramsData["elType"] == "classifieds")
                            html +=
                            `<div class="swiper-slide">
                                <a href="#annonces" class="btn btn-sm btn-primary lbh see-more">
                                    <i class="fa fa-plus"></i> VOIR PLUS
                                </a>
                            </div>`; 
                        else                           
                            html +=
                                `<div class="swiper-slide">
                                <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                                    <i class="fa fa-plus"></i> VOIR PLUS
                                </a>
                            </div>`;
                        }
                        carouselObj.multiCart.htmlCarousel(kunik);
                        $('.container1-' + kunik + ' .swiper-wrapper').html(html);
                    },null,null,{"async":false}
                );

            else{
                var i =1;
                dataCount = Object.keys(externalData).length;
                $.each(externalData, function(k, v) {
                  mylog.log("externalData",externalData);
                  if(exists(paramsData["cardNbResult"]) && i <= paramsData["cardNbResult"])
                    html += carouselObj.multiCart.view(v, paramsData, kunik);
                    i++;
                });
                 if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"]==true)
                html +=
                    `<div class="swiper-slide">
                      <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                        <i class="fa fa-plus"></i> VOIR PLUS
                      </a>
                  </div>`;
                carouselObj.multiCart.htmlCarousel(kunik);
                $('.container1-' + kunik + ' .swiper-wrapper').html(html);            
            }
            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {   
                carouselObj.dataElement = {};
                 $.each(paramsData["element"], function(ke, ve){
                    if(typeof dataEl[ve] != "undefined"){
                        carouselObj.dataElement[ve]=dataEl[ve]
                        delete dataEl[ve];  
                    }
                })
                $.each(dataEl, function(kel, vel){
                    carouselObj.dataElement[kel] = vel;
                })           
            }
            return carouselObj.dataElement;

        },
        view: function(v, paramsData, kunik) {
            mylog.log(v)
            var html = "";
            var df = getDateFormatedCms(v);
            if (typeof v.collection == "undefined" ) 
              v.collection = v.type;
            
            html +=
                `<div class="swiper-slide">
                    <div class="row">
                        <div class="col-xs-12 el-img-container">`;
                        if ((typeof v.category != "undefined" && v.category  == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined" ) || (v.collection == "poi" && typeof v.type != "undefined" && v.type == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined"))
                                html += '<a href="'+v.urls[0]+'" target="_blank">';
                            else 
                                html += `<a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element ${exists(v.category)?v.category:""}">`;
                                if (typeof v.profilImageUrl != "undefined" && v.profilImageUrl!=""){
                                    html += `<img class="img-agenda" src="${v.profilImageUrl}" alt="" style="">`;
                                }else if(typeof carouselObj.costumDefaultImage[kunik]!="undefined"){
                                    html += `<img class="img-agenda defaultImage" src="${carouselObj.costumDefaultImage[kunik]}" alt="" style="">`;
                                }else{
                                    html += `<img class="img-agenda" src="${defaultImage}" alt="" style="">`;
                                }
                    
                            html += `</a>
                        </div>
                        <div class="col-xs-12 el-info-container blur">
                            <div>`;
                                if (exists(paramsData["infoDateFormat"]) && paramsData["infoDateFormat"] == "1") {
                                    if (exists(df.startDay) && exists(paramsData["showDate"]) && paramsData["showDate"]==true)
                                        html += `<h6 class="title-2" style="font-size: 32px;">${df.startDay}</h6>
                                        <h6 class="title-3" style="font-size: 22px;">${df.startMonth+' '+df.startYear+(exists(df.endTime) ? ' <i class="fa fa-clock-o"></i> '+df.endTime : '')}</h6>`;
                                }
                                if (exists(paramsData["showDate"]) && paramsData["showDate"]==true &&  v.collection == "poi"){
                                    var returnDate = directory.showDatetimePost(v.collection, v._id.$id, v.created,null,paramsData["showDateOnly"]);
                                    if (!(paramsData["showDayOnly"] && paramsData["showMonthOnly"] && paramsData["showYearOnly"])) {
                                        let dayType = (exists(paramsData["showDayNumberOnly"]) && paramsData["showDayNumberOnly"]===true) ? 'dayNumber' : 'day'; 
                                        if (paramsData["showDayOnly"])
                                            returnDate = directory.decomposeDate(v.created, dayType);
                                        if (paramsData["showMonthOnly"])
                                            returnDate = directory.decomposeDate(v.created, "month");
                                        if (paramsData["showYearOnly"])
                                            returnDate = directory.decomposeDate(v.created, "year");
                                        if (paramsData["showYearOnly"] && paramsData["showMonthOnly"])
                                            returnDate = directory.decomposeDate(v.created, "month-year");
                                        if (paramsData["showDayOnly"] && paramsData["showMonthOnly"])
                                            returnDate = directory.decomposeDate(v.created, dayType+"-month");
                                    }
                                    if (paramsData["showDate"] && paramsData["showDateOnly"] && paramsData["showDayOnly"] && paramsData["showDayNumberOnly"] && paramsData["showMonthOnly"] && paramsData["showYearOnly"])
                                        returnDate = directory.decomposeDate(v.created, "dayNumber-month-year");
                                    html += `<h6 class="title-5 datePoi">${returnDate}</h6>`;
                                }
                                html += `<h4 class="text-center el-name title-6">`;
                                        if ((typeof v.category != "undefined" && v.category  == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined" ) || (v.collection == "poi" && typeof v.type != "undefined" && v.type == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined")) {
                                            html += '<a class="event-place Oswald" href="'+v.urls[0]+'" target="_blank">';}
                                        else {
                                            html += `<a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element ${exists(v.category)?v.category:""} event-place Oswald ">`}
                                        html += ` ${v.name}
                                            </a> 
                                        </h4>`;
                                if(exists(paramsData["showRoles"]) && paramsData["showRoles"]==true && exists(v.roles) && typeof v.roles == "object" && v.roles.length !=0)
                                    html += `<h5 class="text-center title-5">
                                            ${v.roles.join(",")}
                                        </h5>`;

                                if (exists(paramsData["showType"]) && paramsData["showType"]==true &&  v.collection == "poi")
                                    html += `<h6 class="title-5">${v.type}</h6>`;

                                if (v.type == "classifieds") {
                                    if (exists(v.price) && exists(v.devise))
                                        html += `<h6 class="title-5">${v.price+' '+v.devise }</h6>`;
                                    if ($.inArray(v.collection, ['classifieds', 'ressources']) >= 0 && typeof v.category != 'undefined') {
                                        html += `<h6 class="title-4">`;
                                        html += tradCategory[v.section];
                                        if (typeof v.category != 'undefined' && v.type != 'poi') html += ' > ' + tradCategory[v.category];
                                        subtype = (typeof v.subtype != 'undefined' && typeof tradCategory[v.subtype] != 'undefined') ? ' > ' + tradCategory[v.subtype] : ' > ' + v.subtype;
                                        html += subtype;
                                        html += `</h6>`;
                                    }
                                }

                                if (exists(paramsData["showAdress"]) && paramsData["showAdress"]==true && exists(v.address))
                                    html += `<h5 class="text-center title-4">
                                                ${exists(v.address.level4Name) ? "<i class='fa fa-map-marker'></i> " + v.address.level4Name : ""}
                                                    ${exists(v.address.streetAddress) ? v.address.streetAddress :"" }
                                                
                                            </h5>`;

                                if (exists(paramsData["infoDateFormat"]) && paramsData["infoDateFormat"] == "2" && exists(paramsData["showDate"]) && paramsData["showDate"]==true) {
                                    if (exists(df.startDay))
                                        html += `<h6 class="title-5">${df.startLbl+' '+df.startDayNum+' '+df.startDay+' '+df.startMonth+' '+df.startYear+' <i class="fa fa-clock-o"></i> '+df.startTime}</h6>`;
                                    if (exists(df.endDay))
                                        html += `<h6 class="title-5">${df.endLbl+' '+df.endDayNum+' '+df.endDay+' '+df.endMonth+' '+df.endYear+' <i class="fa fa-clock-o"></i> '+df.endTime}</h6>`;
                                }
                                if (exists(paramsData["showShortDescription"]) && paramsData["showShortDescription"]==true && exists(v.shortDescription)){
                                // var points = (v.shortDescription.length>100)?"...":"";
                                    html += `<p class="title-3">${v.shortDescription != null ? v.shortDescription : ""}</p>`;
                                }

                                if (exists(paramsData["showDescription"]) && paramsData["showDescription"]==true && exists(v.description))
                                    html += `<p class="title-3 markdown">${v.description != null ? v.description : ""}</p>`;
                                if (exists(paramsData["showTags"]) && paramsData["showTags"]==true && notNull(v.tags)){
                                    var tagsCount = v.tags.length;
                                    html += `<div>`;
                                    $.each(v.tags, function(k, vT) {
                                        if(k<4){
                                            html += `<span class="margin-right-5">#${vT != null ?vT : ""}</span>`;
                                        }
                                        if(k==4){
                                            html += `<span class="margin-right-5">+${tagsCount - k}</span>`;
                                        }
                                    });    
                                    html += `</div>`;                            
                                }
                                // if(exists(paramsData["showLinks"]) && paramsData["showLinks"] == true){
                                //     if(exists(v.urls) && exists(v.urls[0]))
                                //         html += `<div class="col-md-12 div-link-${kunik}">
                                //                     <div class="col-md-6 text-left no-padding">
                                //                         <a  href="#page.type.${v.collection}.id.${v._id.$id}" title="découvrir l'outil" class="lbh-preview-element event-place Oswald ">Découvrir</a>
                                //                     </div>
                                //                     <div class="col-md-6 text-right no-padding">
                                //                         <a title="ouvrir l'outils" target="_blank" href="${v.urls[0]}" class="display-links-${kunik}"><i class="fa fa-angle-right"></i></a>
                                //                     </div>
                                //                 </div>`;
                                // }
                    
                                if (isInterfaceAdmin && paramsData["btnEditDelete"]==true) {
                                    html += `<button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                            <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                            <i class="fa fa-trash"></i>
                                            </button>`;
                                }

                            html += `</div>
                        </div>
                    </div>
                </div>`;
            return html;
        },
        htmlCarousel: function(kunik) {
            var html =
            `<div class="container1-${kunik} col-md-12">
                <div id="filtersContainer${kunik}"></div>
                <div class="swiper-container">
                  <div class="swiper-wrapper">
                    <div style="position: relative;width:100%;height: 100%">
                      <p class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></p>
                    </div>
                  </div>
                  <div class="swiper-pagination"></div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>
            </div>`;
            $('.stdr-elem-content-' + kunik).append(html);
        },
        cssCarousel: function(paramsData, kunik) {
            var containerKunik = ".container1-" + kunik;
            var css = `<style>
            .display-links-${kunik} {
                background: black;
                height: 20px; 
                color: white;
                padding: 6px;
            }
            .div-link-${kunik}{
                margin-bottom : 10px;
            }
            .div-link-${kunik} .text-left a{
                padding: 5px;
                font-size: 15px;
                border-radius: 1em;
                border: 1px solid #2c3e50;
            }
            .display-links-${kunik} i{
                background: black;
                height: 20px; 
                color: white;
                padding: 6px;
            }
            .swiper-slide .el-info-container p.title-3 {
                word-wrap: break-word;
                display: -webkit-box;
                -webkit-line-clamp: 2;
                -webkit-box-orient: vertical;
                overflow: hidden;
            }
            ${containerKunik} .el-name a {
                text-decoration :none ;
            }
            .datePoi{
                margin-bottom : 25px;
            }
            .datePoi .dateUpdated{
                font-size : 18px !important;
            }
          ${containerKunik} .swiper-container {
                width: 100%;
                height: 100%;
                padding-bottom: 50px;
                padding-top: 5px;
           }
          ${containerKunik} .swiper-slide {
              text-align: center;
              font-size: 18px;
              background: transparent;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              -webkit-justify-content: center;
              justify-content: center;
              -webkit-box-align: center;
              -ms-flex-align: center;
              -webkit-align-items: center;
              align-items: center;
              }`;
            if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 5)) {
                css += `${containerKunik} .swiper-slide {
                  display: block;
                  }`
            }else {
                css += `${containerKunik} .swiper-slide {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: -webkit-flex;
                  display: flex;
                  }`
            }

            if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 3 || paramsData["elDesign"] == 5)) {
                css += `.container1-${kunik} .swiper-pagination,
                  .container1-${kunik} .swiper-button-next,
                  .container1-${kunik} .swiper-button-prev{
                      display:none
                  }

                  .container1-${kunik} .swiper-wrapper{
                    flex-wrap : wrap;
                    justify-content: space-evenly;                    
                  }
                  ${containerKunik} .swiper-slide{
                    margin-right: 0px !important;
                    margin-bottom: ${paramsData["cardSpaceBetween"]}px !important;
                  }
                  @media (max-width:400px){
                    ${containerKunik} .swiper-slide{
                        flex-basis : calc(95%/ ${exists(paramsData["cardNumberPhone"]) ? paramsData["cardNumberPhone"] : 1}) !important;
                    }
                  }
                  @media (max-width:768px){
                    ${containerKunik} .swiper-slide{
                        flex-basis : calc(95%/ ${exists(paramsData["cardNumberTablet"]) ? paramsData["cardNumberTablet"] : 2}) !important;
                    }
                  }
                  @media (min-width:769px){
                    ${containerKunik} .swiper-slide{
                        flex-basis : calc(95%/ ${exists(paramsData["cardNumberDesktop"]) ? paramsData["cardNumberDesktop"] : 3}) !important;
                    }
                  }
                  `;
            }

            css += `
              ${containerKunik} .description{
                color: white;
                padding:0 5px 0 5px;
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-line-clamp: 2; /* number of lines to show */
                -webkit-box-orient: vertical;
                font-size: 13px;
                margin-top: 20px;
              }
              ${containerKunik} h3{
                  padding:0 10px 0 10px !important;
              }
              ${containerKunik} .add-event-caroussel{
                visibility: hidden
              }
              ${containerKunik}:hover .add-event-caroussel{
                visibility: visible;
              }
              ${containerKunik} .img-agenda,${containerKunik} .swiper-slide img{
                  width: ${exists(paramsData["imgWidth"]) ? paramsData["imgWidth"]+" !important" : "100% !important" };
                  height: ${exists(paramsData["imgHeight"]) ? paramsData["imgHeight"]+" !important" : "auto !important"};
                  border-radius: ${exists(paramsData["imgRound"]) ? paramsData["imgRound"] : "0%"  };
                  display: ${(exists(paramsData["imgShow"]) && paramsData["imgShow"] == true ) ? "initial"  : "none" };
                  border : ${exists(paramsData["imgBorder"]) ? paramsData["imgBorder"] : "0px"} solid ${exists(paramsData["imgBorderColor"]) ? paramsData["imgBorderColor"] : "#ffffff"};
                  object-fit: ${exists(paramsData["imgFit"]) ? paramsData["imgFit"] : "cover" };
                  object-position: center;
                  ${(exists(paramsData["elDesign"]) && (paramsData["elDesign"] != 4 && paramsData["elDesign"] != 5)) ? "z-index:15;" : ""}
              }}
              ${containerKunik} .el-info-container > div{
                position:relative;
                margin: 0 auto;
                margin: ${exists(paramsData["infoDistanceFromImage"]) ? paramsData["infoDistanceFromImage"] : "-14px" } 0 0 0 !important;
                background: ${exists(paramsData["infoBg"]) ? paramsData["infoBg"] : "" } !important;
                margin-left: ${exists(paramsData["infoMarginLeft"]) ? paramsData["infoMarginLeft"] :"" } !important;
                margin-right: ${exists(paramsData["infoMarginRight"]) ? paramsData["infoMarginRight"] :"" } !important;
                border-radius: ${exists(paramsData["infoBorderRadius"]) ? paramsData["infoBorderRadius"] :"0%" }!important; 
                height : ${exists(paramsData["infoHeight"]) ? paramsData["infoHeight"] :"auto" }!important;
                padding: 5px;
                ${(exists(paramsData["elDesign"]) && (paramsData["elDesign"] != 4 && paramsData["elDesign"] != 5)) ? "z-index:15;" : ""}
              }
              ${containerKunik} .swiper-slide > .row:before,${containerKunik} .swiper-slide > .row:after{
                width:100%;height:100%;background:rgba(11,33,47,.9);position:absolute;top:0;left:0;opacity:0;transition:all .5s ease 0s
              }
              ${containerKunik} .swiper-slide > .row{
                margin-left: 0;
                margin-right: 0;
                box-shadow: ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true )  ? "0 2px 5px 0 #3f4e58, 0 2px 10px 0 #3f4e58" : "" };
                background: ${exists(paramsData["cardBg"]) ? paramsData["cardBg"] : "" }; 
              }
              ${containerKunik} .swiper-slide >.row>.col-xs-12{
                padding-left: 0;
                padding-right: 0;
                cardBoxShadow : ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true ) ? "padding-top: 0px;" : "padding-top: 10px;"}
                background: ${exists(paramsData["imgBg"]) ? paramsData["imgBg"] : "" }; 
                
              }`;

              if(exists(paramsData["elDesign"]) && ((paramsData["elDesign"] != 4) || (paramsData["elDesign"] != 5))){
                css+=` ${containerKunik} .img-agenda,${containerKunik} .swiper-slide img{
                    position: relative;
                }`

              }
          if(exists(paramsData["infoOnHover"]) && paramsData["infoOnHover"] == true)
            css+=`
                ${containerKunik} .el-info-container > div > * { 
                  display: none; 
                }
                ${containerKunik} .swiper-slide .el-name{
                    display:block !important;
                    position: relative;
                    z-index: 99;
                }
                ${containerKunik} .el-info-container{
                    position: absolute;
                    height: 100%;
                    visibility:visible;
                    background: ${exists(paramsData["infoBg"]) ? paramsData["infoBg"] : "" } ;
                    z-index: 99;
                    top:100%;
                    transition: all .5s ease;
                    -moz-transition: all .5s ease;
                    -webkit-transition: all .5s ease; 
                }
                ${containerKunik} .swiper-slide:hover .el-info-container{
                    top:0;
                    height: 100%;
                    visibility:visible;
                    transition: all .5s ease;
                    -moz-transition: all .5s ease;
                    -webkit-transition: all .5s ease;                  
                }
                ${containerKunik} .swiper-slide:hover .el-info-container> div > * { 
                  display: block !important; 
                }
                ${containerKunik} .el-info-container > div{
                    height : 100% !important;
                    margin:0px !important;
                }
                ${containerKunik} .swiper-slide > .row{
                    position : relative
                }`;


          css+= `${containerKunik} .edit-element-${kunik},${containerKunik} .delete-element-${kunik}{
                display: none
              }
              ${containerKunik} .swiper-slide:hover .edit-element-${kunik}{
                position : absolute;
                left:10px;
                bottom:5px;
                display: initial;
                z-index:99;
              }
              ${containerKunik} .swiper-slide:hover .delete-element-${kunik}{
                position : absolute;
                left:57px;
                bottom:5px;
                display: initial;
                z-index:99;
              }
              @media (max-width: 420px){
                ${containerKunik} .img-agenda{
                    width: 100%;
                    height: auto;
                    border-radius: 0%;
                    object-fit: contain;
                    object-position: center;
                }
                ${containerKunik} .el-info-container > div{
                  border-radius: 0px;
                }
              }
              ${containerKunik} .see-more{
                line-height: 90px;
                border-radius: 50%;
               margin-top : 82px;
              }
            </style>`;
            $('head').append(css);
        }
    },


    //******************SECOND DESIGN*************************/ 
    
  
    cardBlock: {
        init: function(paramsData, kunik) {
            var $this = this;
            $this.cssCarousel(paramsData, kunik);
            if ($(".container2-" + kunik + " .swiper-wrapper").length <= 0) {
                $this.fetchData(paramsData, kunik);
            }
            var carOption = {
                xs: 1,
                md: 1,
                lg: 1,
            }

            if (exists(paramsData["elDesign"]) && paramsData["elDesign"] != 3)
                carOption["container"] = ".container2-" + kunik + " .swiper-container";

            setTimeout(function() {
                carouselObj.bindEvents(paramsData, kunik);
                carouselObj.initCarousel(paramsData, carOption);
                carouselObj.toSameHeight(".container2-" + kunik + " .el-info-container>div");
            }, 2500)
            mylog.log("carOptionko", carOption)
        },

        fetchData: function(paramsData, kunik) {
            var html = "";
            var dataCount = 0;
            var dataEl = {};
            var contextParent = "parent."+costum.contextId;
            var cardOrderBy = paramsData["cardOrderBy"];
            var cardOrderType =paramsData["cardOrderType"];
            params = {
                searchType: paramsData["elType"],
                fields : ["profilImageUrl","category","urls"],
                sortBy :{ [cardOrderBy] : cardOrderType},
                filters :{'$or':{
                    "source.keys":costum.contextSlug
                }}
            };

            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true  || paramsData["elDesign"] == 3|| paramsData["elDesign"] == 5|| paramsData["elDesign"] == 6)
                params.indexStep = 0;
            params.filters['$or'][contextParent] = {'$exists':true};
            /*if (exists(paramsData["cardNbResult"]) && paramsData["cardNbResult"] != "")
                params["indexStep"] = paramsData["cardNbResult"];*/

            if (exists(paramsData["elPoiType"]) && exists(paramsData["elType"]) && paramsData["elType"]=="poi" && paramsData["elPoiType"] != ""){
                params.filters.type = paramsData["elPoiType"];
                params.types = ["poi"];
            }
            // if (exists(paramsData["cardNbResult"]) && paramsData["cardNbResult"] != "")
            //     params["indexStep"] = paramsData["cardNbResult"]
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data) {
                    dataEl[[paramsData["elType"]]] = data.results;
                    dataCount = Object.keys(data.results).length;
                    var i = 1;
                    $.each(data.results, function(k, v) {

                        if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {
                            $.each(paramsData["element"], function(ke, ve){
                                if(ve == k) {
                                    html += carouselObj.cardBlock.view(v, paramsData, kunik);
                                }
                            });
                        }else if(exists(paramsData["cardNbResult"]) && i <= paramsData["cardNbResult"])
                            html += carouselObj.cardBlock.view(v, paramsData, kunik);
                        i++;
                    });
                    if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"] ==true)
                      html +=
                          `<div class="swiper-slide">
                              <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                                <i class="fa fa-plus"></i> VOIR PLUS
                              </a>
                          </div>`;
                    carouselObj.cardBlock.htmlCarousel(kunik);
                    $('.container2-' + kunik + ' .swiper-wrapper').html(html);
                    document.querySelectorAll('img.lzy_img').forEach((v) => {
                        imageObserver.observe(v);
                    });
                },null,null,{"async":false}
            );
            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {                                   
                carouselObj.dataElement = {};
                $.each(paramsData["element"], function(ke, ve){
                    if(typeof dataEl[ve] != "undefined"){
                        carouselObj.dataElement[ve]=dataEl[ve]
                        delete dataEl[ve];  
                    }
                })
                $.each(dataEl, function(kel, vel){
                    carouselObj.dataElement[kel] = vel;
                })           
            }
            return carouselObj.dataElement;
        },
        view: function(v, paramsData, kunik) {
            var $this = this;
            var df = getDateFormatedCms(v);
            let html = "";
            html += `
                <div class="swiper-slide">
                      <div class="swiper-slide-container">
                          <div class="col-info">
                            <h4 class="text-center">`
                            if (typeof v.category != "undefined" && v.category  == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined") 
                                html += '<a href="'+v.urls[0]+'" target="_blank">';
                            else 
                                html += `<a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element  ${exists(v.category)?v.category:""} event-place text-center  title-6">
                                        ${v.name}
                                    </a>
                            </h4>`;

            if (v.type == "classifieds") {
                if (exists(v.price) && exists(v.devise))
                    html += `<h6 class="title-5">${v.price+' '+v.devise }</h6>`;
                if ($.inArray(v.collection, ['classifieds', 'ressources']) >= 0 && typeof v.category != 'undefined') {
                    html += `<h6 class="title-3">`;
                    html += tradCategory[v.section];
                    if (typeof v.category != 'undefined' && v.type != 'poi') html += ' > ' + tradCategory[v.category];
                    subtype = (typeof v.subtype != 'undefined' && typeof tradCategory[v.subtype] != 'undefined') ? ' > ' + tradCategory[v.subtype] : ' > ' + v.subtype;
                    html += subtype;
                    html += `</h6>`;
                }
            }

            if (exists(df.startDay) && exists(paramsData["showDate"]) && paramsData["showDate"]==true)
                html += `<h6 class="title-5">${df.startLbl+' '+df.startDayNum+' '+df.startDay+' '+df.startMonth+' '+df.startYear+' <i class="fa fa-clock-o"></i> '+df.startTime}</h6>`;
            if (exists(df.endDay) && exists(paramsData["showDate"]) && paramsData["showDate"]==true)
                html += `<h6 class="title-5">${df.endLbl+' '+df.endDayNum+' '+df.endDay+' '+df.endMonth+' '+df.endYear+' <i class="fa fa-clock-o"></i> '+df.endTime}</h6>`;
            
            if (exists(paramsData["showShortDescription"]) && paramsData["showShortDescription"]==true && exists(v.shortDescription))
                html += `<p class="title-3">${v.shortDescription != null ? v.shortDescription : ""}</p>`;

            if (exists(paramsData["showDescription"]) && paramsData["showDescription"]==true && exists(v.description))
                html += `<p class="title-3 markdown">${v.description != null ? v.description : ""}</p>`;

            if (exists(paramsData["showAdress"]) && paramsData["showAdress"]==true && exists(v.address))
                html += `<h5 class="text-center">`
                if (typeof v.category != "undefined" && v.category  == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined") 
                        html += '<a href="'+v.urls[0]+'" target="_blank">';
                else 
                  html += `<a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element ${exists(v.category)?v.category:""} event-place text-center Oswald other">`
                        if (exists(v.address)) {                                
                   html += ` ${exists(v.address.level4Name) ? "<i class='fa fa-map-marker'></i> " + v.address.level4Name : ""}`
                   html += ` ${exists(v.address.streetAddress) ? v.address.streetAddress :"" }`
                        }
                             
                   html += `</a></h5>`;

            if (isInterfaceAdmin && paramsData["btnEditDelete"]==true) {
                html += `<button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                        <i class="fa fa-edit"></i>
                                      </button>
                                  <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                        <i class="fa fa-trash"></i>
                                      </button>`;
            }

            html += `</div>
                        <div class="col-img">`
                        if (typeof v.category != "undefined" && v.category  == "link" && typeof v.urls!= "undefined" && typeof v.urls[0]!= "undefined") 
                                html += '<a href="'+v.urls[0]+'" target="_blank">';
                        else 
                            html+= `<a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element ${exists(v.category)?v.category:""}">`;
            if (typeof v.profilImageUrl != "undefined" && v.profilImageUrl!=""){
                html += `<img class="img-agenda" src="${v.profilImageUrl}" alt="" style="">`;
            
            }else if(typeof carouselObj.costumDefaultImage[kunik]!="undefined"){
                html += `<img class="img-agenda defaultImage" src="${carouselObj.costumDefaultImage[kunik]}" alt="" style="">`;
            }else{
                html += `<img class="img-agenda" src="${defaultImage}" alt="" style="">`;
            }
            html += `</a>
                        </div>
                      </div>
                  </div>`;
            return html;
        },
        htmlCarousel: function(kunik) {
            var html =
            `<div class="container2-${kunik} col-md-12">
              <div id="filtersContainer${kunik}"></div>
              <div class="swiper-container">
                  <div class="swiper-wrapper">

                  </div>
                  <div class="swiper-pagination"></div>
              </div>
            </div>`;
            $('.stdr-elem-content-' + kunik).append(html);
        },
        cssCarousel: function(paramsData, kunik) {
            var css = `<style>
            .container2-${kunik} .swiper-container {
              width: 100%;
              height: 100%;
            }
            .container2-${kunik} .swiper-slide {
              text-align: center;
              font-size: 18px;
              background: transparent;
              display: -webkit-box;
              display: -ms-flexbox;
              display: -webkit-flex;
              display: flex;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              -webkit-justify-content: center;
              justify-content: center;
              -webkit-box-align: center;
              -ms-flex-align: center;
              -webkit-align-items: center;
              align-items: center;
            }`;

            if(exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 6)){
                css +=
                `.container2-${kunik} .swiper-wrapper {
                    flex-direction:column;
                    flex-wrap:wrap;"
                }
                .container2-${kunik} .swiper-pagination,
                .container2-${kunik} .swiper-button-next,
                .container2-${kunik} .swiper-button-prev{
                    display:none
                }
                .container2-${kunik} .swiper-slide{
                  margin-bottom:7px
                }`;              
            }

            css +=`.container2-${kunik} .swiper-slide-container{
              display: flex;
              flex-direction: row;
              width: 100%;
              box-shadow: ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true )  ? "0 2px 5px 0 #3f4e58, 0 2px 10px 0 #3f4e58" : "" };
              height : ${exists(paramsData["infoHeight"]) ? paramsData["infoHeight"] :"auto" };
            }
            .container2-${kunik} .col-info{
              flex-basis: ${(exists(paramsData["imgShow"]) && paramsData["imgShow"]== false) ? "100%" : "60%" };
              padding: 5px;
              display: flex;
              flex-direction: column;
              align-items: ${exists(paramsData["infoAlign"]) ? paramsData["infoAlign"] : "center" };
              justify-content: ${(exists(paramsData["infoCenter"]) && paramsData["infoCenter"] == true) ? "center" : "" };
              order: ${(exists(paramsData["infoOrder"]) && paramsData["infoOrder"] == true) ? 1 : 0 }; 
              background: ${exists(paramsData["infoBg"]) ? paramsData["infoBg"] : "#f0ad16" };
            }
            .container2-${kunik} .col-img{
              flex-basis: 40%;
              background : background: ${exists(paramsData["imgBg"]) ? paramsData["imgBg"] : "" }; 
              display: ${(exists(paramsData["imgShow"]) && paramsData["imgShow"] == true ) ? "initial"  : "none" };
            }
            .container2-${kunik} .col-img img {
              object-fit: cover;
              width: ${exists(paramsData["imgWidth"]) ? paramsData["imgWidth"]+" !important" : "100% !important" };
              height: ${exists(paramsData["imgHeight"]) ? paramsData["imgHeight"]+" !important" : "auto !important"};
              border-radius: ${exists(paramsData["imgRound"]) ? paramsData["imgRound"] : "0%"  };
            }
          .container2-${kunik} .edit-element-${kunik},.container2-${kunik} .delete-element-${kunik}{
            display: none
          }
          .container2-${kunik} .swiper-slide:hover .edit-element-${kunik},.container2-${kunik} .swiper-slide:hover .delete-element-${kunik}{
            display: initial;
          }
          @media (max-width: 957px){
            .container2-${kunik} .swiper-slide-container{
              display: flex;
              flex-direction: column;
            }
            .container2-${kunik} .col-info{
              order: 2;
            }
            .container2-${kunik} .col-img{
              order: 1;
            }
          }
          .container2-${kunik} .see-more{
            line-height: 90px;
            border-radius: 50%;
           margin-top : 82px;
          }
        </style>`;
            $('head').append(css);
        },
    },

    //******************Rotating Design*************************/
    rotatingDesign: {
        init: function(paramsData, kunik) {
            var $this = this;
            $this.cssCarousel(paramsData, kunik);
            $this.fetchData(paramsData, kunik);

            var carOption = {
                xs: 1,
                md: 2,
                lg: 4,
            }
            if (exists(paramsData["elDesign"]) && paramsData["elDesign"] == 7)
                carOption["container"] = ".container3-" + kunik + " .swiper-container";

            setTimeout(function() {
                carouselObj.bindEvents(paramsData, kunik);
                carouselObj.initCarousel(paramsData, carOption);
                $this.bindEvents(kunik);
            }, 2500);
        },

        fetchData: function(paramsData, kunik) {
            var html = "";
            var dataEl = {};
            var dataCount = 0;
            var contextParent = "parent."+costum.contextId;
            var cardOrderBy = paramsData["cardOrderBy"];
            var cardOrderType =paramsData["cardOrderType"];
            params = {
                sortBy :{ [cardOrderBy] : cardOrderType},
                fields : ["profilImageUrl","category","urls"],
                filters :{
                    '$or':{},
                    '$and':[]
                },
                forced : {
                    filters : {}
                }
            };
            if(typeof paramsData.elType == "string")
                params.searchType=[paramsData["elType"]];
            else
                params.searchType=paramsData["elType"];
            mylog.log("params.searchType",params.searchType);
            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true)
                params.indexStep = 0
            if(paramsData["elType"] == "events" ||  $.inArray( "events", paramsData.elType) != -1){
                params.filters['$or'][contextParent] = {'$exists':true};
                params.filters['$or']["organizer."+costum.contextId] = {'$exists':true};

            }else if (paramsData["elType"] == "costum") {
                params.searchType = ["organizations","projects"];
                params.filters['$or']['$and'] = [
                    {"costum.slug" :{'$not':{'$regex':"cocity"}}},
                    {"costum.slug" :{'$exists':true}}
                ],
                    params.notSourceKey = true;
                params.indexStep = 0;
            } else{
                params.filters['$or'][contextParent] = {'$exists':true};
                params.filters['$or']["source.keys"] = costum.contextSlug;

            }

            /*if (exists(paramsData["cardNbResult"]) && paramsData["cardNbResult"] != "")
                params["indexStep"] = paramsData["cardNbResult"];*/
            if (exists(paramsData["elPoiType"]) && exists(paramsData["elType"]) && paramsData["elType"]=="poi" && paramsData["elPoiType"] != ""){
                params.filters.type = paramsData["elPoiType"];
                params.types = ["poi"];
            }

            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data) {
                    dataEl[[paramsData["elType"]]] = data.results;
                    dataCount = Object.keys(data.results).length;
                    var i = 1;
                    $.each(data.results, function(k, v) {

                        if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {
                            $.each(paramsData["element"], function(ke, ve){
                                if(ve == k) {
                                    html += carouselObj.rotatingDesign.view(v, paramsData, kunik);
                                }
                            });
                        }else if(exists(paramsData["cardNbResult"]) && i <= paramsData["cardNbResult"])
                            html += carouselObj.rotatingDesign.view(v, paramsData, kunik);
                        i++;
                    });
                    if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"] ==true)
                        html +=
                            `<div class="swiper-slide">
                              <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                                <i class="fa fa-plus"></i> VOIR PLUS
                              </a>
                          </div>`;
                    carouselObj.rotatingDesign.htmlCarousel(kunik);
                    $(".container3-"+kunik+" .swiper-wrapper").html(html);
                    document.querySelectorAll('img.lzy_img').forEach((v) => {
                        imageObserver.observe(v);
                    });
                },null,null,{"async":false}
            );
            if(exists (paramsData["btnChoose"]) && paramsData["btnChoose"] == true && exists (paramsData["element"])) {                                   
                carouselObj.dataElement = {};
                $.each(paramsData["element"], function(ke, ve){
                    if(typeof dataEl[ve] != "undefined"){
                        carouselObj.dataElement[ve]=dataEl[ve]
                        delete dataEl[ve];  
                    }
                })
                $.each(dataEl, function(kel, vel){
                    carouselObj.dataElement[kel] = vel;
                })           
            }
            return carouselObj.dataElement;


        },

        view: function(v, paramsData, kunik) {
            var html = `<div class="swiper-slide">
                        <div class="swiper-slide-container">
                             <div class="card-container manual-flip">
                                <div class="card-flip">
                                    <div class="front-flip">
                                        <div class="cover-flip">
                                            <img src="${assetPath}/images/costumDesCostums/banner.png"/>
                                        </div>
                                        <div class="user">`;
            if (typeof v.profilImageUrl != "undefined" && v.profilImageUrl!=""){
                html += `<img class="img-agenda" src="${v.profilImageUrl}" alt="" style="">`;
            }else if(typeof carouselObj.costumDefaultImage[kunik]!="undefined"){
                html += `<img class="img-agenda defaultImage img-circle" src="${carouselObj.costumDefaultImage[kunik]}" alt="" style="">`;
            }else{
                html += `<img class="img-agenda img-circle" src="${defaultImage}" alt="" style="">`;
            }

            html += `                </div>
                                        <div class="content-flip">
                                            <div class="main-flip">
                                                <h3 class="name">${v.name}</h3>`;
            if (exists(paramsData["showAdress"]) && paramsData["showAdress"]==true && exists(v.address))
                html += `                            <p class="profession">
                                                        ${exists(v.address.level4Name) ? "<i class='fa fa-map-marker'></i> " + v.address.level4Name : ""}
                                                        ${exists(v.address.streetAddress) ? v.address.streetAddress :"<i class='fa fa-map-marker'> tsa misy" }
                                                    </p>`;
            if (isInterfaceAdmin && paramsData["elType"] != "costum" && paramsData["btnEditDelete"]==true) {
                html += `                            <button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                            <i class="fa fa-edit"></i>
                                                      </button>
                                                      <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                            <i class="fa fa-trash"></i>
                                                      </button>`;
            }
            html += `                   </div>
                                            <div class="footer">
                                                <button class="btn btn-simple btn-rotating">
                                                    <i class="fa fa-mail-forward"></i> Information
                                                </button>
                                            </div>
                                        </div>
                                    </div> <!-- end front panel -->
                                    <div class="back-flip">
                                        <div class="header">
                                            <h5 class="motto">${v.name}</h5>
                                        </div>
                                        <div class="content-flip">
                                            <div class="main-flip">`;
            if (v.type == "classifieds") {
                if (exists(v.price) && exists(v.devise))
                    html += `<h4 class="text-center">${v.price + ' ' + v.devise}</h4>`;
                if ($.inArray(v.collection, ['classifieds', 'ressources']) >= 0 && typeof v.category != 'undefined') {
                    html += `<h6 class="title-3">`;
                    html += tradCategory[v.section];
                    if (typeof v.category != 'undefined' && v.type != 'poi') html += ' > ' + tradCategory[v.category];
                    subtype = (typeof v.subtype != 'undefined' && typeof tradCategory[v.subtype] != 'undefined') ? ' > ' + tradCategory[v.subtype] : ' > ' + v.subtype;
                    html += subtype;
                    html += `</h6>`;
                }
            }

            if (exists(paramsData["showShortDescription"]) && paramsData["showShortDescription"]==true && exists(v.shortDescription))
                html += `                            <p class="text-center">${v.shortDescription != null ? v.shortDescription : ""}</p>`;

            html += `                   </div>
                                        </div>
                                        <div class="footer text-center">
                                            <button class="btn btn-simple btn-rotating" rel="tooltip" title="Flip Card" >
                                                <i class="fa fa-reply"></i> Retour
                                            </button>`;
            if (paramsData["elType"] != "costum") {
                html += `                       <a  href="#page.type.${v.collection}.id.${v._id.$id}" class="btn btn-simple lbh-preview-element ${exists(v.category)?v.category:""} event-place" > 
                                                    En savoir plus <i class="fa  fa-info-circle"></i> 
                                                </a>`;
            }else {
                html += `                       <a href='${baseUrl}/costum/co/index/slug/${v.slug}' class="btn btn-simple" target='_blank'>
                                                 Visiter le site <i class="fa  fa-mail-forward"></i>
                                                </a>`;
            }
            html += `                </div>
                                    </div> 
                                </div> 
                            </div> 
                         </div> 
                         </div> 
                        `;
            return html;
        },

        htmlCarousel: function(kunik) {
            var html =
                `<div class="container3-${kunik} col-md-12">
              <div id="filtersContainer${kunik}"></div>
              <div class="swiper-container">
                  <div class="swiper-wrapper">

                  </div>
                  <div class="swiper-pagination"></div>
              </div>
            </div>`;
            $('.stdr-elem-content-' + kunik).append(html);
        },

        cssCarousel: function(paramsData, kunik) {
            var css = `<style>

            .container3-${kunik} .swiper-container {
              width: 100%;
              height: 100%;
            }
            .container3-${kunik} .swiper-slide {
              text-align: center;
              font-size: 18px;
              background: transparent;
              display: -webkit-box;
              display: -ms-flexbox;
              display: -webkit-flex;
              display: flex;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              -webkit-justify-content: center;
              justify-content: center;
              -webkit-box-align: center;
              -ms-flex-align: center;
              -webkit-align-items: center;
              align-items: center;
            }
            .container3-${kunik} .swiper-slide-container{
              display: flex;
              flex-direction: row;
              width: 100%;
              box-shadow: ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true )  ? "0 2px 5px 0 #3f4e58, 0 2px 10px 0 #3f4e58" : "" };
              height : ${exists(paramsData["infoHeight"]) ? paramsData["infoHeight"] :"auto" };
            }
            .container3-${kunik} .manual-flip .btn:hover,
            .container3-${kunik} .manual-flip .btn:focus,
            .container3-${kunik} .manual-flip .btn:active{
                    outline: 0 !important;
                }
            .container3-${kunik} .manual-flip {
                    -webkit-perspective: 800px;
                    -moz-perspective: 800px;
                    -o-perspective: 800px;
                    perspective: 800px;
                    margin-bottom: 30px;
                }
            .container3-${kunik} .card-container.hover.manual-flip .card-flip{
                    -webkit-transform: rotateY( 180deg );
                    -moz-transform: rotateY( 180deg );
                    -o-transform: rotateY( 180deg );
                    transform: rotateY( 180deg );
                }
            .container3-${kunik} .manual-flip.static:hover .card-flip,
            .container3-${kunik} .manual-flip.static.hover .card-flip {
                    -webkit-transform: none;
                    -moz-transform: none;
                    -o-transform: none;
                    transform: none;
                }
            .container3-${kunik} .manual-flip .card-flip {
                    -webkit-transition: -webkit-transform .5s;
                    -moz-transition: -moz-transform .5s;
                    -o-transition: -o-transform .5s;
                    transition: transform .5s;
                    -webkit-transform-style: preserve-3d;
                    -moz-transform-style: preserve-3d;
                    -o-transform-style: preserve-3d;
                    transform-style: preserve-3d;
                    position: relative;
                }
            .container3-${kunik} .manual-flip .front-flip, .manual-flip .back-flip {
                    -webkit-backface-visibility: hidden;
                    -moz-backface-visibility: hidden;
                    -o-backface-visibility: hidden;
                    backface-visibility: hidden;
                    position: absolute;
                    top: 0;
                    left: 0;
                    background-color: #FFF;
                    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.14);
                }
            .container3-${kunik} .manual-flip .front-flip {
                    z-index: 2;
                }
            .container3-${kunik} .manual-flip .back-flip {
                    -webkit-transform: rotateY( 180deg );
                    -moz-transform: rotateY( 180deg );
                    -o-transform: rotateY( 180deg );
                    transform: rotateY( 180deg );
                    z-index: 3;
                }
            .container3-${kunik} .manual-flip .back-flip .btn-simple{
                    /*position: absolute;*/
                    left: 0;
                    bottom: 4px;
                }
            .container3-${kunik} .manual-flip .card-flip{
                    background: none repeat scroll 0 0 #FFFFFF;
                    border-radius: 4px;
                    color: #444444;
                }
            .container3-${kunik} .manual-flip, .container3-${kunik} .front-flip, .container3-${kunik} .back-flip {
                    width: 100%;
                    height: 420px;
                    border-radius: 4px;
                    -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
                    -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
                    box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
                }
            .container3-${kunik} .manual-flip .card-flip .cover-flip{
                    height: 105px;
                    overflow: hidden;
                    border-radius: 4px 4px 0 0;
                }
            .container3-${kunik} .manual-flip .card .cover-flip img{
                    width: 100%;
                }
            .container3-${kunik} .manual-flip .card-flip .user{
                    border-radius: 50%;
                    display: block;
                    height: 124px;
                    margin: -62px auto 0;
                    overflow: hidden;
                    width: 124px;
                    border: 4px solid #fff;
                }
            .container3-${kunik} .manual-flip .card-flip .user img{
                    background: none repeat scroll 0 0 #FFFFFF;
                    width: 120px;
                    height: 120px;
                    object-fit: cover;
                    object-position: center;
                }
            .container3-${kunik} .manual-flip .card-flip .content-flip{
                    background-color: rgba(0, 0, 0, 0);
                    box-shadow: none;
                    padding: 10px 20px 20px;
                }
            .container3-${kunik} .manual-flip .card-flip .content-flip .main-flip {
                    min-height: 160px;
                }
            .container3-${kunik} .manual-flip .card-flip .back-flip .content-flip .main-flip {
                    height: 215px;
                }
            .container3-${kunik} .manual-flip .card-flip .name {
                    font-family: 'Arima Madurai', cursive;
                    font-size: 22px;
                    line-height: 28px;
                    margin: 10px 0 0;
                    text-align: center;
                    text-transform: none;
                    /*height: 70px;*/
                    word-wrap: break-word;
                    display: -webkit-box;
                    -webkit-line-clamp: 2;
                    -webkit-box-orient: vertical;
                    overflow: hidden;
                }
            .container3-${kunik} .manual-flip .card-flip h5{
                    margin: 5px 0;
                    font-weight: 400;
                    line-height: 20px;
                }
            .container3-${kunik} .manual-flip .card-flip .profession{
                    color: #999999;
                    text-align: center;
                    margin-bottom: 20px;
                    margin-top: 15px;
                }
            .container3-${kunik} .manual-flip .card-flip .footer {
                    border-top: 1px solid #EEEEEE;
                    color: #999999;
                    margin: 30px 0 0;
                    padding: 10px 0 0;
                    text-align: center;
                }
            .container3-${kunik} .manual-flip .card .footer .btn-simple{
                    margin-top: -6px;
                }
            .container3-${kunik} .manual-flip .card-flip .header {
                    padding: 15px 20px;
                    height: 90px;
                    position: relative;
                }
            .container3-${kunik} .manual-flip .card-flip .motto{
                    font-family: 'Arima Madurai', cursive;
                    border-bottom: 1px solid #EEEEEE;
                    color: #999999;
                    font-size: 14px;
                    font-weight: 400;
                    padding-bottom: 10px;
                    text-align: center;
                }
            .container3-${kunik} .manual-flip .btn-simple{
                    opacity: .8;
                    color: #666666;
                    background-color: transparent;
                }
            .container3-${kunik} .manual-flip .btn-simple:hover,
            .container3-${kunik} .manual-flip .btn-simple:focus{
                    background-color: transparent;
                    box-shadow: none;
                    opacity: 1;
                }
            .container3-${kunik} .manual-flip .btn-simple i{
                    font-size: 16px;
                }
   
                /*       Fix bug for IE      */
    
            @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
                .manual-flip .front, .manual-flip .back-flip{
                        -ms-backface-visibility: visible;
                        backface-visibility: visible;
                    }
    
                .manual-flip .back-flip {
                        visibility: hidden;
                        -ms-transition: all 0.2s cubic-bezier(.92,.01,.83,.67);
                    }
                .manual-flip .front-flip{
                        z-index: 4;
                    }
                .card-container.manual-flip.hover .back-flip{
                        z-index: 5;
                        visibility: visible;
                    }
                </style>`;
            $('head').append(css);
        },

        bindEvents : function (kunik) {
            $(".btn-rotating").on("click",function(){
                var $card = $(this).closest('.card-container');
                mylog.log($card);
                if($card.hasClass('hover')){
                    $card.removeClass('hover');
                } else {
                    $card.addClass('hover');
                }
            })

        }
    },

    initDesign3: function() {

    }
}