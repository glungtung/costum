/* Lobjet costumizer.obj est une extension de l'objet costum 
	** Il permet d'apporter les modifs js à volonté jusque l'enregistrement finalement des modifications qui vient modifier la db et le costum js
	** L'objet retient et centralise les méthodes d'édition du Costum :
			[ ] Pages (costum.app)
			[ ] htmlConstruct 
				[ ] menu 
				[ ] page Element
				[ ] Adminstration dash 
				[ ] ...
	** Le but de ces méthodes est de limiter au maximum les reload instempestif sur les modifications apportées au costum
	** Il est générique et généré dans le mainSearch.php rendu donc global à tout l'environnement d'un costum ou pourquoi pas de co2    
	!!! TRAVAIL À RÉALISER et à organiser !!!!!
	[ ] CSS
	[ ] JS
	[ ] Métier 
	==> TOUTES LES TÂCHES SONT DÉCRITES AU FIL DES CLICS ET DES METHODES DANS L'OBJECT
*/ 

var costumizer = {
	obj : {},
	preview : false,
	liveChange : false,
	mode : "md",
	connectedMode : true,
	space : "",
	multiSpaces : [],
	init : function(){
		costumizer.obj=jQuery.extend(true,{},costum);
		//costumizer.initView();
		costumizer.initEvents();
		cssHelpers.form.extendCoInput();
	},
	initEvents : function(){
		$.each(costumizer.events, function(e, v){
			v();
		});
		/*costumizer.events.startView();
		costumizer.events.close();
		costumizer.events.previewLink();
		costumizer.events.viewMode();
		costumizer.events.save();
		costumizer.events.editMenu();*/
		/*$(".design-costumizer").on("click", function(){
			titleRightCostumizer="Design";
			$("#menu-right-costumizer").find(".costumizer-title > span").text(titleRightCostumizer);
			$("#menu-right-costumizer").fadeIn(1000);
			costumizer.design.views.init();
			costumizer.design.events.bind();
		});*/

	
		//open dropdown on click
		$(".cmsbuilder-toolbar-item").click(function(e) {
			e.stopPropagation()
			var hasClass = $(this).find(".cmsbuilder-toolbar-dropdown").hasClass("dropdown-active")
			$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")
			if(!hasClass)
				$(this).find(".cmsbuilder-toolbar-dropdown").addClass("dropdown-active")
		})
		//close dropdown on click outside
		$(document).click(function(){
			$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")
		})
	},
	initView : function(config){	
		var domTarget=config.domTarget;
		$(domTarget).fadeIn(500);
		$(domTarget).find(".costumizer-title > span").text(config.title);
			
		if(typeof costumizer[costumizer.space].views != "undefined"
			&& typeof costumizer[costumizer.space].views.init === "function"){
			$(domTarget+" > .contains-view-admin").html(costumizer[costumizer.space].views.init());
		}
		else if(typeof costumizer[costumizer.space].views != "undefined"
			&& typeof costumizer[costumizer.space].views.initAjax === "function"){
				costumizer[costumizer.space].views.initAjax(domTarget+" .contains-view-admin");
		}
		else{
			ajaxPost(domTarget+" .contains-view-admin", baseUrl+"/"+moduleId+"/cms/admindashboard", 
				{ 
					type: costum.contextType,
					id: costum.contextId,
					view:costumizer.space
				},
				function(){
					if(typeof costumizer[costumizer.space].events != "undefined"
						&& typeof costumizer[costumizer.space].events.bind === "function")
						costumizer[costumizer.space].events.bind();
				}
			);
		}
		//Bind events for space costumizer opened 
		if(typeof costumizer[costumizer.space].events != "undefined"
			&& typeof costumizer[costumizer.space].events.bind === "function")
			costumizer[costumizer.space].events.bind();
		//Function initCallback for workspace costumizer opened
		if(typeof costumizer[costumizer.space].actions != "undefined"
			&& typeof costumizer[costumizer.space].actions.init === "function")
			costumizer[costumizer.space].actions.init();
	},
	events : {
		startView : function(){
			$(".lbh-costumizer").off().on("click", function(e){
				e.stopPropagation()
				$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")
				
				if ($(this).data("link") == "css"){
					if ($("#amazing-costumizer-css").length > 0 ){
						$("#amazing-costumizer-css").attr("type", "text");
					} else if ($("#amazing-costumizer-css-new").length > 0){
						$("#amazing-costumizer-css-new").attr("type", "text");
					}
				}else{
					if ($("#amazing-costumizer-css").length > 0 ){
						$("#amazing-costumizer-css").attr("type", "text/css");
					} else if ($("#amazing-costumizer-css-new").length > 0){
						$("#amazing-costumizer-css-new").attr("type", "text/css");
					}	
				}
				
				var params = {
					space:$(this).data("link"),
					title:$(this).data("title"),
					showSaveButton:typeof $(this).data("save-button") == "undefined"
				}
				if(typeof $(this).data("dom") != "undefined")
					params.domTarget = $(this).data("dom")

				costumizer.actions.openCostumizer(params)
			});
		},
		close : function(){
			$(".close-dash-costumizer").off().on("click", function(){
				costumizer.actions.cancel($(this).data("dom"));
				//$($(this).data("dom")).fadeOut(1000);
				$("#content-btn-delete-page").remove();
			});
		},
		save : function(){
			$(".save-costumizer").off().on("click", function(){
				$(this).find("i").removeClass("fa-save").addClass("fa-spin fa-spinner");
				$(this).addClass("disabled");
				if ($("#costum-design #collapsefont").hasClass('collapse in')) {
					if (costumizer.obj.css.font) {
						if (costumizer.obj.css.font.url == "" && $('#inputs-font input[name="uploadFont"]').val() != "") {
							costumizer.design.events.getDataFont();
							if (!costumizer.design.fontUploadExist) {
								costumizer.design.events.addFont();
								uploadedFont = "/upload/communecter/"+costumizer.obj.contextType+"/"+costumizer.obj.contextId+"/file/"+$('#inputs-font input[name="uploadFont"]').val();
								costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");	
							}
						}
					}
				}
				costumizer.actions.update();
				$("#menu-right-costumizer, #modal-content-costumizer-administration").fadeOut(1000);
			});
		},
		previewLink : function(){
			$(".open-preview-costum").off().on("click", function(){
				window.open($(this).data("url")+location.hash);
			});
		},
		viewMode : function(){
			$(".costumizer-edit-mode-connected").off().on("click", function(){
				//.slideToggle();
				$(".costumizer-edit-mode-connected").find("i.fa-eye").parent().remove();
				$(this).append('<span><i class="fa fa-eye" aria-hidden="true"></i></span>');
				costumizer.connectedMode=($(this).data("value")===true) ? true : false;
				var addClassIcon=($(this).data("value")===true) ? "fa-sign-in" : "fa-sign-out";
				$(".activeModeConnected").find("i.statusConnected").slideToggle( "fast", function() {
				    $(this).removeClass("fa-sign-in fa-sign-out").addClass(addClassIcon).slideToggle();
				  });
				//$(".activeModeConnected").find("i.statusConnected").removeClass("fa-sign-in fa-sign-out").addClass(addClassIcon);
			//	$(".activeModeConnected").find("i.statusConnected").slideToggle();
				costumizer.actions.switchMode();
			});
			$(".btn-costumizer-viewMode").off().on("click", function(){
				$(".btn-costumizer-viewMode").removeClass("active");
				$(this).addClass("active");

				$(".cmsbuilder-center-content").scrollTop(0);
				costumizer.mode=$(this).data("mode");
				costumizer.responsive[$(this).data("mode")]();
			});
		},
		editMenu : function(){
			$(".configuration-menus-costumizer").off().hover(function(e){
				$(this).addClass("blur");
				if($(this).find(".editMenuStandalone").length > 0)
					$(this).find(".editMenuStandalone").show();
				else{
					$(this).append("<button class='editMenuStandalone badge circle'><i class='fa fa-pencil'></i></button>");
					$(".configuration-menus-costumizer .editMenuStandalone").off().on("click", function(e){
						$(this).addClass("focus");
						e.preventDefault();
						var viewMenuConstruct=costumizer.htmlConstruct.views.menuConstruct("htmlConstruct."+$(this).parent().data("path"));
						//alert($(this).parent().data("path").replace('htmlConstruct.',"").replace(".",".subList."));
						/* var titleMenu=jsonHelper.getValueByPath(costumizer.htmlConstruct.map,$(this).parent().data("path").replaceAll(".",".subList.")+".title");
						costumizer.space="htmlConstruct";
						//$("#menu-right-costumizer").find(".costumizer-title > span").text("Menu edition");
						//$("#menu-right-costumizer").fadeIn(1000);
						var subMenuTools=`<div class="menuLiveConstruct subMenuTools col-xs-12 no-padding" data-path="${$(this).parent().data("path")}">
								<a href='javascript:;' class='btn btn-live-menu-edit active' data-object='htmlConstruct' data-path="${$(this).parent().data("path")}">${tradCms.content}</a>
								<a href='javascript:;' class='btn btn-live-menu-edit' data-object='design' data-path="${$(this).parent().data("path")}">${tradCms.style}</a>
							</div>`;
						costumizer.actions.openRightPanel(viewMenuConstruct,
							{ 	width:300,
								footer: true,
								subMenuTools: subMenuTools,
								title : titleMenu
							}
						); */

						costumizer.space="htmlConstruct";
						var path = $(this).parent().data("path");

						$(".cmsbuilder-right-content").addClass("active")
						costumizer.actions.tabs.init(
							"#right-panel",
							[
								{
									key: "htmlConstruct",
									icon: "list-ul",
									label: tradCms.content,
									containerClassname:"contains-view-admin"
								},
								{
									key: "design",
									icon: "paint-brush",
									label: tradCms.style,
									containerClassname:"contains-view-admin"
								}
							],
							{
								footer:`
									<button class="btn btn-sm btn-danger save-costumizer disabled"><i class="fa fa-floppy-o" aria-hidden="true"></i>${trad.save}</button>
								`,
								getContent: function(tabKey){
									if( $.inArray(costumizer.space, costumizer.multiSpaces) < 0)
										costumizer.multiSpaces.push(costumizer.space);

									if($.inArray(tabKey,costumizer.multiSpaces) < 0)
										costumizer.multiSpaces.push(tabKey);

									costumizer.space = tabKey;

									if(tabKey == "htmlConstruct")
										return costumizer.htmlConstruct.views.menuConstruct("htmlConstruct."+path);
									else{
										costumizer.design.activeObjCss = costumizer.design.events.generateObj("htmlConstruct."+path);
										return costumizer.design.views.css()
									}
								},
								onTabChanged:function(tabKey){
									costumizer.space=tabKey;
									if(typeof costumizer[costumizer.space].events != "undefined"
										&& typeof costumizer[costumizer.space].events.bind === "function")
										costumizer[costumizer.space].events.bind();
									if(typeof costumizer[costumizer.space].actions != "undefined"
										&& typeof costumizer[costumizer.space].actions.init === "function")
										costumizer[costumizer.space].actions.init();
								},
								onInitialized:function(){
									costumizer.events.save()
								}
							}
						)

						costumizer.liveChange=true;
							
					});
				}
			}, function(){
				$(this).removeClass("blur");
				$(this).find(".editMenuStandalone").hide();
			});
		},
		bindLivePanel : function(){
			$(".btn-live-menu-edit").off().on("click", function(){
				$("#menu-right-costumizer-hover, #menu-sub-right-costumizer").fadeOut(500);
				if($.inArray(costumizer.space, costumizer.multiSpaces) < 0)
					costumizer.multiSpaces.push(costumizer.space);
				if($.inArray($(this).data("object"),costumizer.multiSpaces) < 0)
					costumizer.multiSpaces.push($(this).data("object"));
				costumizer.space=$(this).data("object");
				$('.btn-live-menu-edit').removeClass("active");
				$(this).addClass("active");
				//alert("path:"+cssMenuConstruct);
				if(costumizer.space=="htmlConstruct")
					var viewMenuConstruct=costumizer.htmlConstruct.views.menuConstruct("htmlConstruct."+$(this).parent().data("path"));
				else {
					costumizer.design.activeObjCss=costumizer.design.events.generateObj("htmlConstruct."+$(this).parent().data("path"));
					var viewMenuConstruct=costumizer.design.views.css();

				}
				$("#menu-right-costumizer > .contains-view-admin").html(viewMenuConstruct);
				costumizer.space=$(this).data("object");
				if(typeof costumizer[costumizer.space].events != "undefined"
					&& typeof costumizer[costumizer.space].events.bind === "function")
					costumizer[costumizer.space].events.bind();
				if(typeof costumizer[costumizer.space].actions != "undefined"
					&& typeof costumizer[costumizer.space].actions.init === "function")
					costumizer[costumizer.space].actions.init();
				
					
			});
		},
		uiEvents:function(){
			$(".btn-toggle-cmsbuilder-sidepanel").click(function() {
				costumizer.actions.toogleSidePanel($(this).data("target"))
            })
		}
	},
	actions : {
		pathToValue : [],
		useTemplate : function (tpl_params) {
			var params = {
				"id"		 : tpl_params.idTplSelected,
				"page"		 : page,
				"action"	 : tpl_params.action,
				"collection" : tpl_params.collection,
				"parentId"   : costum.contextId,
				"parentSlug" : costum.contextSlug,
				"parentType" : costum.contextType
			}
			ajaxPost(
				null,
				baseUrl + "/co2/cms/usetemplate",
				params,
				function (data) {					
					window.location.reload()
				},
				{ async: false }
				);
		},
		switchMode : function(){
			var refreshCostumMenu={};
			$.each(["htmlConstruct.header.menuTop","htmlConstruct.menuLeft", "htmlConstruct.subMenu","htmlConstruct.menuRight","htmlConstruct.menuBottom"], function(e,v){
					if(jsonHelper.notNull("costumizer.obj."+v))
						refreshCostumMenu[v]=true;
			});
			costumizer.htmlConstruct.actions.refreshmenu(refreshCostumMenu, costumizer.obj.htmlConstruct);
		},
		change : function(path){
			// Cette fonction permet d'itérer qu'elle attribut au final nous devront mettre à jour en base de donnée sur le costum
			if($(".save-costumizer").hasClass("disabled")){
				$(".save-costumizer").removeClass("disabled");
			}
			let keyByPath=path.split(".");
			let lenghtPath=keyByPath.length;
			let finalPath="";
			let count=0;
			$.each(keyByPath, function(e,v){
				if(count==0){
					if($.inArray(v, costumizer.actions.pathToValue) >= 0)
						return false;
					else
						finalPath=v;
				}
				else
					finalPath+="."+v;
				if($.inArray(finalPath, costumizer.actions.pathToValue) >= 0){
					return false;
				}else if(count==3)
					return false;
				count++;
			});
			if(notEmpty(finalPath) && $.inArray(finalPath, costumizer.actions.pathToValue) < 0){
				costumizer.actions.pathToValue.push(finalPath);
			}
		},
		commonSpaceFunction: function(actionName, data){
			if(notEmpty(costumizer.multiSpaces)){
				$.each(costumizer.multiSpaces, function(e, space){
					if(typeof costumizer[space].actions != "undefined"
						&& typeof costumizer[space].actions[actionName] === "function")
						data=costumizer[space].actions[actionName](data);
				});
			}else if(notEmpty(costumizer.space) && typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions[actionName] === "function")
				data=costumizer[costumizer.space].actions[actionName](data);
			
			return data;
		},
		prepData : function(){
			var objToUp={};
			$.each(costumizer.actions.pathToValue, function(e, v){
				let valueToUp=jsonHelper.getValueByPath(costumizer.obj,v);
				objToUp[v]=valueToUp;
				jsonHelper.setValueByPath(costum, v, valueToUp);
			});
			return objToUp=costumizer.actions.commonSpaceFunction("prepData", objToUp);
			/*if(notEmpty(costumizer.multiSpaces)){
				$.each(costumizer.multiSpaces, function(e, space){
					if(typeof costumizer[space].actions != "undefined"
						&& typeof costumizer[space].actions.prepData === "function")
						objToUp=costumizer[space].actions.prepData(objToUp);
				});
			}else if(typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions.prepData === "function")
				objToUp=costumizer[costumizer.space].actions.prepData(objToUp);
			
			return objToUp;*/
		},
		update : function(){
			var pathValueToUpdate=costumizer.actions.prepData();
			if(!$(".save-costumizer").hasClass("disabled")){
				$(".save-costumizer").addClass("disabled");
			}
			ajaxPost(
				null,
				baseUrl+"/"+moduleId+"/cms/updatecostum",
				{
					params : pathValueToUpdate
				},
				function(data){
					costumizer.actions.commonSpaceFunction("afterUpdate");
					toastr.success(tradCms.updateConfig);
					$(".save-costumizer").find("i").removeClass("fa-spin fa-spinner").addClass("fa-save");
					costumizer.actions.pathToValue=[];
					if(notEmpty(costumizer.multiSpaces)){
						$.each(costumizer.multiSpaces, function(e, space){
							if(typeof costumizer[space].actions != "undefined"
								&& typeof costumizer[space].actions.callback === "function")
								costumizer[space].actions.callback(data);
						});
					} else if(typeof costumizer[costumizer.space].actions != "undefined"
						&& typeof costumizer[costumizer.space].actions.callback === "function")
						costumizer[costumizer.space].actions.callback(data);
				},
				null, 
				"json"//,
				//{contentType: 'application/json'}
							
			);
		},
		cancel : function(dom){
			if(((costumizer.liveChange && dom=="#menu-right-costumizer") || dom=="#modal-content-costumizer-administration") 
				&& !$(".save-costumizer").hasClass("disabled") &&
				notEmpty(costumizer.actions.pathToValue)){
				bootbox.confirm({
				    message: "<span class='text-center bold'>"+tradCms.warningUnsavedChanges+"</span>",
				    buttons: {
				        confirm: {
				            label: "<i class='fa fa-save'></i> "+trad.save,
				            className: 'btn-danger'
				        },
				        cancel: {
				            label: trad.cancel,
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				    	if(result){
				    		$(".save-costumizer").trigger("click");
				    		costumizer.actions.closeModal(dom, true);
				    	}else{
				    		costumizer.actions.commonSpaceFunction("cancel");
				    		/*if(notEmpty(costumizer.multiSpaces)){
				    			$.each(costumizer.multiSpaces, function(e, space){
									if(typeof costumizer[space].actions != "undefined"
										&& typeof costumizer[space].actions.cancel === "function"){
										costumizer[space].actions.cancel();
									}
								});
							}else if(typeof costumizer[costumizer.space].actions.cancel=="function"){
				    			costumizer[costumizer.space].actions.cancel();
				    		}*///else{
								costumizer.actions.pathToValue=[];
	    						costumizer.obj=jQuery.extend(true,{},costum);
				    		//}
				    		costumizer.actions.closeModal(dom, true); 
				    	}
				        //console.log('This was logged in the callback: ' + result);
				    }
				});
			}else{
				costumizer.actions.closeModal(dom); 
				costumizer.liveChange = true
			}
		},
		closeModal: function(dom, mainDom){
			//$("#menu-right-costumizer, #menu-right-costumizer .modal-tools-costumizer").css({"width":"300px"});		
			if(notEmpty(dom))
				$(dom).fadeOut(600);
			if(notEmpty(mainDom) || !notEmpty(dom)){
				$("#menu-right-costumizer, #modal-content-costumizer-administration").fadeOut(1000);
				$(".lbh-costumizer").parent().removeClass("active");
			}
			costumizer.actions.commonSpaceFunction("close");
		},
		tabs:{
			/**
			 * Initialize tabs views
			 * @param {object[]} tabs
			 * @param {string} tabs[].key - Clé pour identifier chaque tab
			 * @param {string} tabs[].icon - Icon du menu de navigation
			 * @param {string} tabs[].label - Label du menu de navigation
			 * @param {string|object|null} tabs[].content - Contenu du tab 
			 * @param {string} tabs[].containerClassname
			 * 
			 * @param {object} options
			 * @param {function} options.onInitialized
			 * @param {function} options.onTabChanged
			 * @param {function} options.getContent
			 * @param {string|object} options.footer - Pied de la vue
			 */
			init:function(container, tabs, options={}){
				var $tabsContainer = $(`
					<div class="cmsbuilder-tabs">
						<div class="cmsbuilder-tabs-header"><ul></ul></div>
						<div class="cmsbuilder-tabs-body"></div>
						<div class="cmsbuilder-tabs-footer"></div>
					</div>
				`)
	
				tabs.forEach(function(tab){
					/* add header */
					$tabsContainer.find(".cmsbuilder-tabs-header ul").append(`
						<li data-key="${tab.key}">
							<span><i class="fa fa-${tab.icon}" aria-hidden="true"></i></span>
							<span>${tab.label}</span>
						</li>
					`)
	
					/* add body */
					var $content = $(`<div class="cmsbuilder-tab-section ${tab.containerClassname?tab.containerClassname:""}" data-key="${tab.key}"></div>`)
					$content.append(tab.content ? tab.content:"");
					$tabsContainer.find(".cmsbuilder-tabs-body").append($content);
				})

				/* add footer */
				if(options.footer)
					$tabsContainer.find(".cmsbuilder-tabs-footer").append(options.footer);
				else
					$tabsContainer.find(".cmsbuilder-tabs-footer").remove();
	
				$tabsContainer.find(".cmsbuilder-tabs-header ul li").click(function(){
					$tabsContainer.find(".cmsbuilder-tabs-header ul li").removeClass("active");
					$(this).addClass("active");

					$tabsContainer.find(".cmsbuilder-tab-section").removeClass("active");

					var tabKey = $(this).data("key"),
						$container = $tabsContainer.find(`.cmsbuilder-tab-section[data-key="${tabKey}"]`)
					if(typeof options.getContent == "function")
						$container.html(options.getContent(tabKey))
					if(typeof options.onTabChanged == "function")
						options.onTabChanged(tabKey)

					$container.addClass("active");
				});

				$(container).html("").append($tabsContainer);
	
				$tabsContainer.find(".cmsbuilder-tabs-header ul li").first().click();

				if(typeof options.onInitialized === "function")
					options.onInitialized();
				coInterface.initHtmlPosition();
			}
		},
		openRightPanel: function(view, config){
			/* if($("#toolsBar-edit-block").is(':visible')){
				$("#toolsBar-edit-block").hide();  
			}; */
			var domTarget="#menu-right-costumizer";
			if(typeof config.hover != "undefined" && config.hover){
				domTarget="#menu-right-costumizer-hover";
				$(domTarget).css({top : $("#menu-right-costumizer > .modal-tools-costumizer").outerHeight()+40})
			}else{
				$(domTarget+ " > #menu-right-costumizer-hover").hide();
			}

			if(typeof config.subRight != "undefined" && config.subRight){
				domTarget="#menu-sub-right-costumizer";
				$(domTarget+ ", "+domTarget+" > .modal-tools-costumizer, "+domTarget+" > .modal-tools-costumizer-footer").css({right : $("#menu-right-costumizer").outerWidth()});
			}
			var titleConfig=(typeof config.title != "undefined") ? config.title : "";
			var widthContainer=(typeof config.width != "undefined") ? config.width : 300;	
			$(domTarget+" , "+domTarget+" > .modal-tools-costumizer, "+domTarget+" > .modal-tools-costumizer-footer").css({"width":widthContainer+"px"});
			$(domTarget+" > .modal-tools-costumizer > .costumizer-title > span").text(titleConfig);
			$(domTarget+" > .modal-tools-costumizer").css({height : (typeof config.subMenuTools != "undefined") ? 80 : 45});
			$(domTarget+" > .contains-view-admin").css({"top": $(domTarget+" > .modal-tools-costumizer").outerHeight()});
			
			$(domTarget+" > .contains-view-admin").html(view);
			if(typeof config.subMenuTools != "undefined"){
				if($(domTarget+" > .modal-tools-costumizer .sub-nav").length <= 0)
					$(domTarget+" > .modal-tools-costumizer").append("<div class='sub-nav'></div>");
				costumizer.multiSpaces=[costumizer.space];
				$(domTarget+" > .modal-tools-costumizer .sub-nav").html(config.subMenuTools);
				costumizer.events.bindLivePanel();
			}
			else
				$(domTarget+" > .modal-tools-costumizer .subMenuTools").remove();
			if(typeof config.footer != "undefined"){
				$(domTarget+" > .modal-tools-costumizer-footer").show();
				$(domTarget+" > .contains-view-admin").css({"bottom": $(domTarget+" > .modal-tools-costumizer-footer").outerHeight()});
			}
			else{
				$(domTarget+" > .modal-tools-costumizer-footer").hide();
				$(domTarget+" > .contains-view-admin").css({"bottom": 0});
				
			}
			//$(domTarget).fadeIn(500);
			$(domTarget).addClass("active")
			//Bind events for space costumizer opened 
			if(typeof costumizer[costumizer.space].events != "undefined"
				&& typeof costumizer[costumizer.space].events.bind === "function")
				costumizer[costumizer.space].events.bind();
				//Function initCallback for workspace costumizer opened
			if(typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions.init === "function")
				costumizer[costumizer.space].actions.init();
			
		},
		/**
		 * 
		 * @param {'left'|'right'} targetPosition 
		 * @param {'open'|'close'} action 
		 */
		toogleSidePanel: function(targetPosition, action=null){
			var $target = $(`.cmsbuilder-${targetPosition}-content`),
				open = action?(action == "open"):!$target.hasClass("active");

			if(open)
				$target.addClass("active")
			else{
				costumizer.actions.cancel("#menu-right-costumizer");
				$target.removeClass("active")
			}
			coInterface.initHtmlPosition();
		},

		/**
		 * @param {string|object} views
		 * @param {object} options
		 * @param {number} [options.width]
		 * @param {number} [options.distanceToRight]
		 * @param {string} [options.className]
		 * @param {function} [options.onInitialized]
		 * @param {object} [options.header]
		 * @param {string} options.header.title
		 * @param {"top"|"left"} options.header.position
		 */
		openRightSubPanel: function(views, options={}){
			var config = $.extend(true, {}, {
				width:150,
				distanceToRight:300,
				className:"",
				header:{
					title:"",
					position:"top"
				}
			}, options)

			var $container = $(`#right-sub-panel-container`)

			$container.html("")
			$container.css({ width:config.width+"px", right:config.distanceToRight+"px",display:"block"}).addClass("active")
			
			/* begin:header */
			var $header = $(`
				<div class="right-sub-panel-header right-sub-panel-header-${config.header.position}">
					<span>${config.header.title}</span>
					<button><i class="fa fa-${ (config.header.position==="top")?"times":"chevron-right" }" aria-hidden="true"></i></button>
				</div>
			`)
			$header.find("button").click(function(){
				$container.css({ width:0 }).removeClass("active")
			})
			$container.append($header)
			/* end:header */

			var $body = $(`<div class="right-sub-panel-body ${config.className}"></div>`)
			$body.append(views)
			$container.append($body)

			if(typeof config.onInitialized == "function")
				config.onInitialized()
			
			//Bind events for space costumizer opened 
			if(typeof costumizer[costumizer.space].events != "undefined"
				&& typeof costumizer[costumizer.space].events.bind === "function")
				costumizer[costumizer.space].events.bind();
				//Function initCallback for workspace costumizer opened
			if(typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions.init === "function")
				costumizer[costumizer.space].actions.init();
		},
		closeRightSubPanel:function(){
			$(`#right-sub-panel-container`).html("").css({width:0}).removeClass("active")
		},
		/**
		 * 
		 * @param {object} params
		 * @param {string} params.space
		 * @param {string} params.title
		 * @param {string} [params.domTarget]
		 * @param {boolean} [params.showSaveButton] 
		 */
		openCostumizer: function(params){
			this.closeRightSubPanel()

			var config = $.extend(true, {}, {
				domTarget:"#modal-content-costumizer-administration",
				showSaveButton:false
			}, params)

			if($("#toolsBar-edit-block").is(':visible')){
				$("#toolsBar-edit-block").hide();  
				coInterface.initHtmlPosition();
			};

			costumizer.space=config.space;
			costumizer.multiSpaces=[];
			coInterface.showLoader(config.domTarget+" .contains-view-admin");
			costumizer.liveChange = false
			
			// Initialization of views
			if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].init === "function")
				costumizer[costumizer.space].init();

			if(config.domTarget == "#menu-right-costumizer"){
				costumizer.actions.tabs.init(
					"#right-panel",
					[
						{
							key:"design",
							icon:"paint-brush",
							label:config.title,
							content:costumizer[costumizer.space].views.init(),
							containerClassname:"contains-view-admin"
						}
					],
					{
						footer:`
							<button class="btn btn-sm btn-danger save-costumizer disabled"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</button>
						`,
						onInitialized:function(){
							costumizer.events.save()
							
							//Bind events for space costumizer opened 
							if(typeof costumizer[costumizer.space].events != "undefined"
								&& typeof costumizer[costumizer.space].events.bind === "function")
								costumizer[costumizer.space].events.bind();
								//Function initCallback for workspace costumizer opened
							if(typeof costumizer[costumizer.space].actions != "undefined"
								&& typeof costumizer[costumizer.space].actions.init === "function")
								costumizer[costumizer.space].actions.init();
						}
					}
				)
			}else{
				costumizer.initView(config);
			}

			if(config.showSaveButton)
				$("#modal-content-costumizer-administration .save-costumizer").show().addClass("disabled");
			else
				$("#modal-content-costumizer-administration .save-costumizer").hide();

			$("#ajax-modal.portfolio-modal.modal").css({"z-index":"100003","top" : "100px", "left": "40px"});
		}
	},
	responsive : {
		md : function(){
			$(".cmsbuilder-center-content").removeClass("overflow-hidden")
			$(".cmsbuilder-content-wrapper").addClass("d-none")
			$("#cmsbuilder-content-md").removeClass("d-none")
		},
		switchToMobileView: function(format, marginVertical=0){
			var $container = $("#cmsbuilder-content-"+format)

			$(".cmsbuilder-center-content").addClass("overflow-hidden")
			$(".cmsbuilder-content-wrapper").addClass("d-none")
			$container.removeClass("d-none")
			$(".device-time").html(moment().format('HH:mm'))
			var adjustScale = function(){
				var scaleHeight =  ($(".cmsbuilder-center-content").height()-marginVertical)/$container.height(),
					scaleWidth = $(".cmsbuilder-center-content").width()/$container.width();

				$container.css({
					"transform":`scale(${ scaleHeight < scaleWidth ? scaleHeight:scaleWidth })`
				})	
			}

			adjustScale()

			var $iframe = $(`<iframe id="iframe-xs" src="${baseUrl}/costum/co/index/slug/${costum.slug}/${location.hash}" frameborder="0"></iframe>`)
			$iframe.on("load", function(){
				$iframe.contents().find("head").append(`
					<style>
						body::-webkit-scrollbar {
							display: none !important;
						}
					</style>
				`)
			})
			$container.find(".device-page").html($iframe)
		},
		sm : function(){
			this.switchToMobileView("sm")
		},
		xs : function(){
			this.switchToMobileView("xs", 60)
		}
	},
	valueImage : "",
	
	generalInfos : {
		views : {
			init : function(){
				return str = '<div class="col-xs-12">' +
								'<div id="costum-info" class="col-xs-12 col-sm-12 col-md-8"></div>' +
								'<div class="col-xs-12 col-sm-12 col-md-4 position-sticky">'+
									'<div class="contain-info">'+
										'<div id="avatar">'+
											'<img src="'+costumizer.obj.logo+'" />'+
										'</div>'+
										'<div id="cover"></div>'+
										'<div id="info">'+
											'<h4><i class="fa fa-info-circle"></i> '+tradCms.costumGeneralSettingsUpdate+'</h4>'+
											'<p></p>'+
										'</div>'+
									'</div>'+
									'<div id="costum-other-info" class="well contain-other-info"></div>'+
								'</div>'+
							  '</div> ';
			}

		},
		actions : {
			callback : function(data){
				/* dyFObj.commonAfterSave(costumizer.obj,function(){}); */
				$(".modal-content-costumizer-administration").hide();
			},
			setLanguage: function(lang){
				$.cookie('lang', lang, { expires: 365, path: "/" });
				if(userId != ""){
					param={
						name : "language",
						value : lang,
						pk : userId
					};
					ajaxPost(
					  null,
					  baseUrl+"/" + moduleId + "/element/updatefields/type/citoyens",
					  param,
					  function(data){ 
						if(data.result){
							toastr.success(data.msg);
							
						}
					  }
					);
				}
			},
			initImageByPath : function(key, path){
				if(path.indexOf(costum.assetsUrl) === -1 && path.indexOf("/upload/") === -1 && path.indexOf("/images/thumbnail-default.jpg") === -1 )
					jsonHelper.setValueByPath(costumizer.obj, key, costum.assetsUrl+path);
				else{
					if(key == "faviconSourceImage" && path != "")
						jsonHelper.setValueByPath(costumizer.obj, key, path);
				}
			},
			
			getImageDocument: function(id , type, subkey, contentKey = null){
				costumizer.valueImage="";
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/getimagedocument",
					{
						id: id,
						type: type,
						subkey: subkey,
						contentKey : contentKey
					},
					function(data){
						costumizer.valueImage = data;
					},
					null,
					"json",
					{
						async: false
					}

				);

			}, 
		},
		events :{ 
			bind : function(){
				var listPageIntoJSON = {};
				$.each(costumizer.obj.app, function(k, v){
					if(typeof v.lbhAnchor == "undefined" || (typeof v.lbhAnchor != "undefined" && (v.lbhAnchor == false || v.lbhAnchor == "false")))
						listPageIntoJSON[k.replace("#", "")] = (typeof v.subdomainName != "undefined") ? v.subdomainName : k.replace("#", "");				
				}); //redirection page
				

				if(typeof costumizer.obj["favicon"] != "undefined" && costumizer.obj["favicon"] == "" && typeof costumizer.obj["faviconSourceImage"] == "undefined"){
					costumizer.generalInfos.actions.getImageDocument(costum.contextId, costum.contextType, "favicon", "slider");
					if(typeof costumizer.valueImage != "undefined" && typeof costumizer.valueImage[0] != "undefined" && typeof costumizer.valueImage[0].imagePath != "undefined"){
						costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.valueImage[0].imagePath);
					}
					//costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.obj["favicon"])
				}

				if(typeof costumizer.obj["favicon"] != "undefined" && costumizer.obj["favicon"] != "" && ((typeof costumizer.obj["faviconSourceImage"] == "undefined") || (typeof costumizer.obj["faviconSourceImage"] != "undefined" && costumizer.obj["faviconSourceImage"] == ""))){
					costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.obj["favicon"])
				}

				if(typeof costumizer.obj["faviconSourceImage"] != "undefined" && costumizer.obj["faviconSourceImage"] != ""){
					costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.obj["faviconSourceImage"])
				}

				if(typeof costumizer.obj["logo"] != "undefined" && costumizer.obj["logo"] != ""){
					costumizer.generalInfos.actions.initImageByPath("logo", costumizer.obj["logo"])
				}

				if(typeof costumizer.obj["logoMin"] != "undefined" && costumizer.obj["logoMin"] != ""){
					costumizer.generalInfos.actions.initImageByPath("logoMin", costumizer.obj["logoMin"])
				}

				if(typeof costumizer.obj["metaImg"] != "undefined" && costumizer.obj["metaImg"] != ""){
					costumizer.generalInfos.actions.initImageByPath("metaImg", costumizer.obj["metaImg"])
				}
				
				var defaultValues = costumizer.obj;

				new CoInput({
					container : "#costum-info",
					inputs :[
						{
							type : "inputSimple",
							options: {
								name : "title",
								label : tradCms.title,
								defaultValue : (typeof defaultValues["title"] != "undefined") ? defaultValues["title"]:"",
								icon : "chevron-down",
							}
						},
						{
							type : "inputSimple",
							options: {
								name : "adminEmail",
								label :  tradCms.emailAdmin,
								defaultValue : (typeof defaultValues["admin"] != "undefined" && typeof defaultValues["admin"]["email"] !="undefined")? defaultValues["admin"]["email"]:"",
								icon : "chevron-down",
								payload : {
									path : "admin.email",
								}
							},
						},
						{
							type : "textarea",
							options: {
								name : "description",
								label : tradCms.shortDescription,
								defaultValue :(typeof defaultValues["description"] != "undefined") ? defaultValues["description"]:"",
								icon : "chevron-down",
								/* rows : 1 */
							},
						},
						{
							type : "inputSimple",
							options: {
								name : "metaTitle",
								label : tradCms.metaTitle,
								defaultValue : (typeof defaultValues["metaTitle"] != "undefined") ? defaultValues["metaTitle"]:"",
								icon : "chevron-down",
							},
						},
						{
							type : "tags",
							options : {
								name : "tags",
								label : tradDynForm.keyWords,
								options : [""],
								defaultValue : (typeof defaultValues["tags"] != "undefined") ? defaultValues["tags"]:"",
								icon : "chevron-down"
							}
						},
						{
							type: "matomoInput",
							options: {
								name : "matomo",
								label: "Activer matomo",
								defaultValues : (typeof defaultValues["matomo"] != "undefined") ? defaultValues["matomo"]: ""
							}
						},
						
						{
							
							type : "inputFileImage",
							options : {
								name : "favicon",
								label : tradCms.favicon,
								collection : "documents",
								defaultValue : (typeof defaultValues["faviconSourceImage"] != "undefined") ? defaultValues["faviconSourceImage"]: "",
								payload : {
									path : "faviconSourceImage",
								},
								icon : "chevron-down",
								class : "faviconUploader",
								endPoint : "/subKey/favicon",
								domElement : "favicon",		
								filetype : ["jpeg", "jpg", "gif", "png"], 
							}
						},
						{
							
							type : "inputFileImage",
							options : {
								name : "logo",
								label : tradCms.logo,
								collection : "documents",
								defaultValue : (typeof defaultValues["logo"] != "undefined") ? defaultValues["logo"]: "",
								payload : {
									path : "logo",
								},
								icon : "chevron-down",
								class : "logoUploader",
								endPoint : "/subKey/logo",
								domElement : "logo",		
							}
						},
						{
							
							type : "inputFileImage",
							options : {
								name : "logoMin",
								label : tradCms.logoMin,
								collection : "documents",
								defaultValue : (typeof defaultValues["logoMin"] != "undefined") ? defaultValues["logoMin"]: "",
								payload : {
									path : "logoMin",
								},
								icon : "chevron-down",
								class : "logoMinUploader",
								endPoint : "/subKey/logoMin",
								domElement : "logoMin",		
							}
						},
						{
							
							type : "inputFileImage",
							options : {
								name : "metaImage",
								label : tradCms.metaImage,
								collection : "documents",
								defaultValue : (typeof defaultValues["metaImg"] != "undefined") ? defaultValues["metaImg"]: "",
								payload : {
									path : "metaImg",
								},
								icon : "chevron-down",
								class : "metaImageUploader",
								endPoint : "/subKey/metaImg",
								domElement : "metaImg",		
							}
						},
					],
					onchange:function(name, value, payload){
						var finalPath = costumizer.obj;
						var path = `${payload.path}`;
						mylog.log("change");
						if(exists(payload.path)){
							jsonHelper.setValueByPath(finalPath, path, value);
							costumizer.actions.change(path);
						}
						else{
							jsonHelper.setValueByPath(costumizer.obj, name, value);
							costumizer.actions.change(name);
						}

						if(name == "logo"){ //Emplacement du logo
							$(".image-menu").attr("src", costumizer.obj.logo);
						}

						if(name == "favicon"){ //Pour vider value favicon
							jsonHelper.setValueByPath(costumizer.obj, "favicon", "");
							costumizer.actions.change("favicon");
						}
					}
				});

				
	
				new CoInput({
					container : "#costum-other-info",
					inputs:[
						{
							type : "select",
							options : {
								name : "language",
								label : tradCms.language,
								options : Object.keys(themeParams.languages).map(function(value) {
									return {
										label : trad[themeParams.languages[value]],
										value : value
									}
								}),
								icon : "language",
								defaultValue : (typeof userConnected.language != "undefined") ? userConnected.language:"",
							}
						},
						{
							type : "select",
							options : {
								name : "logged",
								label : tradCms.redirectionHomePageLogged,
								options : Object.keys(listPageIntoJSON).map(function(value) {
									return {
										label: listPageIntoJSON[value],
										value: value
									}
								}),
								icon : "home",
								payload : {
									path : `htmlConstruct.redirect.logged`,
								},
								defaultValue : (typeof defaultValues["htmlConstruct"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"]["logged"] != "undefined" && typeof costumizer.obj.app["#"+defaultValues["htmlConstruct"]["redirect"]["logged"]] != "undefined") ? defaultValues["htmlConstruct"]["redirect"]["logged"]: "",
							}
						},
						{
							type : "select",
							options : {
								name : "unlogged",
								label : tradCms.redirectionHomePageUnlogged,
								options : Object.keys(listPageIntoJSON).map(function(value) {
									return {
										label : listPageIntoJSON[value] ,
										value : value
									}
								}),
								icon : "home",
								payload : {
									path : `htmlConstruct.redirect.unlogged`,
								},
								defaultValue : (typeof defaultValues["htmlConstruct"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"]["unlogged"] != "undefined" && typeof costumizer.obj.app["#"+defaultValues["htmlConstruct"]["redirect"]["unlogged"]] != "undefined") ? defaultValues["htmlConstruct"]["redirect"]["unlogged"]: "",
							}
						}

					],
					onchange:function(name, value, payload){
						var finalPath = costumizer.obj;
						var path = `${payload.path}`;
						if(name == "language"){
							costumizer.generalInfos.actions.setLanguage(value);
						}
						else if(exists(payload.path)){
							jsonHelper.setValueByPath(finalPath, path, value);
							costumizer.actions.change(path);
						}
						else{
							jsonHelper.setValueByPath(costumizer.obj, name, value);
							costumizer.actions.change(name);
						}
					}
				})

				$('#costum-info #btn-submit-form').hide();
				$('#costum-info .mainDynFormCloseBtn').hide();
			},
		}
		
	},
	loader : {
		options: {
			loading_modal : {
				view : "loading_modal",
				name : tradCms.doubleRing
			},
			loader_1 : {
				view : "loader_1",
				name : tradCms.fourBlock
			},
			loader_2 : {
				view : "loader_2",
				name : tradCms.dyingLight
			},
			loader_3 : {
				view : "loader_3",
				name : tradCms.horizontalBar
			},
			loader_4 : {
				view : "loader_4",
				name : tradCms.newtonsCradle
			},
			loader_5 : {
				view : "loader_5",
				name : tradCms.square
			},
			loader_6 : {
				view : "loader_6",
				name : tradCms.circle
			},
			loader_7 : {
				view : "loader_7",
				name : tradCms.spanne
			},
			loader_8 : {
				view : "loader_8",
				name : tradCms.ball
			}

		},
		views : {
			init : function(){
				var bgLoader = costumizer.obj.css.loader.background;
				$('style#cssLoader').html("");
				var  str=
					'<div class="col-xs-12 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12">';
						$.each(costumizer.loader.options, function(e, v){
						str+=`<div class="card_loader col-md-4 col-sm-6 col-xs-12 ${ typeof costumizer.obj.css.loader.loaderUrl &&  costumizer.obj.css.loader.loaderUrl == v.view || typeof costumizer.obj.css.loader.loaderUrl == "undefined" && v.view == "loading_modal" ? "active" : "" }" >
								<div class="loader_section">
									<div class="box-loader"  style="background-color: ${bgLoader};">
										<div class="loader-position">
											${costumizer.loader.views.typeLoader(v.view)}
										</div>
									</div>
									<div class="box-content">
										<h3 class="title">${v.name}</h3>
										${ typeof costumizer.obj.css.loader.loaderUrl &&  costumizer.obj.css.loader.loaderUrl == v.view || typeof costumizer.obj.css.loader.loaderUrl == "undefined" && v.view == "loading_modal" ? '<span class="post">'+tradCms.currentLoader+'</span>' : '<a href="javascript:;" class="activeLoader" data-name="'+v.view+'"><span class="post">'+trad.activate+'</span></a>' }
									</div>
								</div>
							</div>`;
						});
				str+=`</div>
					`;


				return  str;
			},
			typeLoader : function(name){
				var objLoader = costumizer.obj.css.loader;
				var ring1 = objLoader.ring1; var ring2 = objLoader.ring2;
				var loading_modal =
					'<div class="processingLoader">'+
					'<center>'+
					'<div class="lds-css ng-scope">'+
					'<div style="width:100%; height:100%;" class="lds-doubl-ring">'+
					'<div id="ring1" style="border-color: transparent '+ring1.color+'; "></div>'+
					'<div id="ring2" style="border-color: transparent '+ring2.color+';"></div>'+
					'<img src="'+costumizer.obj.logo+'">'+
					'</div>'+
					'</div>'+
					'</center>'+
					'</div>';
				var loader_1 =
					'<div class="loader_1">' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'</div>';

				var loader_2 =
					'<div class="dl">' +
					'  <div class="dl_container">' +
					'    <div class="dl_corner--top" style="color:'+ring1.color+';"></div>' +
					'    <div class="dl_corner--bottom" style="color:'+ring2.color+';"></div>' +
					'  </div>' +
					'  <div class="dl_square"><img class="dl_img" src="'+costumizer.obj.logo+'"></div>' +
					'</div>';
				var loader_3 =
					'<div class="horizontal_bar">' +
					'    <div class="horizontal_bar_logo">' +
					'        <img src="'+costumizer.obj.logo+'">' +
					'    </div>' +
					'    <div class="horizontal_bar_loader" style="background-color:'+ring2.color+';height:'+ring2.borderWidth+';">' +
					'        <div class="horizontal_bar_loading" style="background-color:'+ring1.color+';height:'+ring1.borderWidth+';"></div>' +
					'    </div>' +
					'</div>';
				var loader_4 =
					'<img class="newtons-cradle-img" src="'+costumizer.obj.logo+'">'+
					'<div class="newtons-cradle">'+
						'<div style="background-color: '+ring1.color+';"></div>'+
						'<div style="background-color: '+ring1.color+';"></div>'+
						'<div style="background-color: '+ring2.color+';"></div>'+
						'<div style="background-color: '+ring2.color+';"></div>'+
					'</div>';
				var loader_5 =
					'<div class="loader-square">' +
					'    <img src="'+costumizer.obj.logo+'">' +
					'</div>';
				var loader_6 =
					'<div class="loader-circle-2" style="border: '+ring1.borderWidth+' solid '+ring1.color+';">' +
					'    <img src="'+costumizer.obj.logo+'">' +
					'</div>';
				var loader_7 =
					'<div class="loader-spanne">' +
					'    <div class="loader-spanne-img">' +
					'        <img src="'+costumizer.obj.logo+'">' +
					'    </div>' +
					'    <span style="height: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'    <span style="width: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'    <span style="height: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'    <span style="width: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'</div>';
				var loader_8 =
					'<img class="loader-ball-img" src="'+costumizer.obj.logo+'">' +
					'<div class="loader-ball">' +
					'<div class="dot1" style="background-color: '+ring1.color+';"></div>' +
					'<div class="dot2" style="background-color: '+ring1.color+';"></div>' +
					'<div class="dot3" style="background-color: '+ring1.color+';"></div>' +
					'<div class="dot4" style="background-color: '+ring1.color+';"></div>' +
					'</div>';

				if (name == "loader_1") {
					$('style#cssLoader').append('@keyframes loaderBlock {0%, 30% {transform: rotate(0);}55% {background-color: '+ring2.color+';}100% {transform: rotate(360deg);}}@keyframes loaderBlockInverse {0%, 20% {transform: rotate(0);}55% {background-color: '+ring2.color+';}100% {transform: rotate(-360deg);}}');
				} else if (name == "loader_5") {
					$('style#cssLoader').append('.loader-square:before{ border: solid '+ring1.borderWidth+' '+ring1.color+'; } .loader-square:after{ border: solid '+ring2.borderWidth+' '+ring2.color+'; }');
				} else if (name == "loader_6") {
					$('style#cssLoader').append('.loader-circle-2::before {background-color: '+ring2.color+';}');
				} else if (name == 'loader_7') {
					$('style#cssLoader').append('@keyframes spanMoveAnimation { 0% {transform: scale(1);margin: 0;} 20% {transform: scale(1);margin: 0;} 60% {transform: scale(0.5);margin: 20px;background: '+ring2.color+';}  100% {  transform: scale(1);  margin: 0;  }  }');
				}else if (name == "loader_8") {
					$('style#cssLoader').append('.loader-ball div::after {background-color: '+ring2.color+';}');
				}

				return eval(name);
			}
		},
		events : {
			bind: function () {
				$(".activeLoader").off().on("click", function (e) {
					valueLoader = $(this).data("name");
					var finalPath = costumizer.obj;
					var path = "css.loader.loaderUrl";

					jsonHelper.setValueByPath(costumizer.obj, path, valueLoader);
					costumizer.actions.change(path);
				});
			}
		}
	},
	pages : {
		views : {
			init : function(){
				var str="";
				if(!notEmpty(costumizer.obj.app)){
					str=`<div><span>`+tradCms.emptyCostum+`</span></div>
							<button class="btn-primary create-page btn costumizer-button-add"><i class="fa fa-plus"></i> `+tradCms.createNewPage+`</button>`;
				}else{
					str=`<div class='pages-toolbar'>
							<button class="btn create-page btn costumizer-button-add"><i class="fa fa-plus"></i> `+tradCms.createNewPage+`</button>
							<button class="delete-page multiple-delete-page disabled btn costumizer-button-delete" data-id="delete_all"><i class="fa fa-trash"></i> `+trad.delete+`</button>
						</div>`;
					str+=`<table class="tabling-pages table  table-hover col-xs-12">
					            <thead>
					                <tr>
					                    <th></th>
					                    <th class='col title'>`+tradCms.pageName+`</th>
					                    <th class='col key-app'>`+tradCms.keyslug+`</th>
					                    <th class='col key-app'>`+trad.type+`</th>
										<th class='col key-app'>`+tradCms.visibleToAll+`</th>
										<th class='col key-app'>`+tradCms.status+`</th>
					                    <th class='col toolsEdit'>`+trad.actions+`</th>
					                </tr>
					            </thead>
					            <tbody>`;
								if(Array.isArray(costumizer.obj.app)){
									costumizer.obj.app = Object.assign({}, costumizer.obj.app);
								}
								$.each(costumizer.obj.app, function(k, e){
					str+=			costumizer.pages.views.linePage(k,e);
								});
					str+=		`</tbody>
					   	 	</table>`;
				}
				return str;
			},
			linePage : function(k,v){
				var htmlStr ="";
				var forAll = ["green", tradCms.true];
				var status = ["green", tradCms.published];
				var btnPublie = " <a href='javascript:;' class='brouillon-page tooltips' data-key='"+k+"''><i class='fa fa-share-square-o'></i> "+tradCms.draftPage+"</a>";
				var typePage="pageCMS"
		
				if((typeof v.appTypeName != "undefined" && v.appTypeName != "") || typeof v.useFilter != "undefined"){
					typePage="advancedApp";
				}
				if(typeof v.lbhAnchor != "undefined" && v.lbhAnchor == true)
					typePage="anchorMenu";
				if(typeof v.target != "undefined" && v.target == true && typeof v.urlExtern != "undefined" && notEmpty(v.urlExtern))
					typePage="externalLink";
				if(typeof v.restricted != "undefined"){
					if(typeof v.restricted.admins != "undefined"){
						if( v.restricted.admins == true || v.restricted.admins == "true"){
							forAll = ["red", tradCms.visibleToAdminOnly];
						}
					}
					if(typeof v.restricted.draft != "undefined" && v.restricted.draft == true){
						status = ["red", tradCms.draft];
						btnPublie = " <a href='javascript:;' class='publie-page tooltips' data-key='"+k+"''><i class='fa fa-share-square-o'></i> "+tradCms.publishPage+"</a>";
					}
				}
				
				htmlStr += "<tr id='keyPages-"+k+"' class='pages-line'>"+
							"<th class='col checkbox-tools'>";
							if (k!="#welcome")
								htmlStr +=	"<input type='checkbox' class='selectPages' value='"+k+"'/>" ;
				htmlStr +=	"</th>"+
							"<td  data-key='"+k+"'' class='col title editPage'><span class=' tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>";
							if(typeof v.subdomainName !='undefined')
								htmlStr += v.subdomainName;
							else
								htmlStr += k.replace("#", "") ;
							htmlStr += "</span></td>"+
										"<td  data-key='"+k+"'' class='col key-app editPage' ><span class='tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+k+"</span></td>"+
										"<td  data-key='"+k+"'' class='col key-app editPage' ><span class='tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+tradCms[typePage]+"</span></td>";
							htmlStr += "<td  data-key='"+k+"'' class='col key-app editPage' style='color:"+forAll[0]+"'><span class=' tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+forAll[1]+"</span></td>" ;
							htmlStr += "<td  data-key='"+k+"'' class='col key-app editPage' style='color:"+status[0]+"'><span class=' tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+status[1]+"</span></td>" ;
							htmlStr+= "<td class='col toolsEdit'>"
							htmlStr+= "<div class='dropdown'>"+
											"<button class='btn btn-primary dropdown-toggle' type='button' id='"+k+"' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"+
											"<i class='fa fa-cog'></i>"+
											"</button>"+
											"<ul class='dropdown-menu pull-right' aria-labelledby='"+k+"'>";
												if(typePage != "anchorMenu" && typePage != "externalLink")
							htmlStr+=				"<li><a href='"+k+"' class='lbh-costumizer-edit-page tooltips'><i class='fa fa-eye'></i> "+tradCms.showPage+"</a></li>";
							htmlStr+=			"<li><a href='javascript:;' class='editPage ' data-key='"+k+"''><i class='fa fa-pencil '></i> "+trad.edit+"</a></li>";
										 		if(typePage != "anchorMenu")
							htmlStr+=			"<li><a href='javascript:;' class='duplicate-page tooltips' data-key='"+k+"''><i class='fa fa-copy'></i> "+tradCms.duplicate+"</a></li>";
												if (k!="#welcome" && typePage != "externalLink"){
							htmlStr+= 				"<li>"+btnPublie+"</li>";
												}
												if (k!="#welcome" && typePage != "anchorMenu"){
							htmlStr+=				"<li><a href='javascript:;' class='delete-page text-red' data-id='delete_one' id='"+k+"' ><i class='fa fa-trash '></i> "+trad.delete+"</a></li>";
												}
											"</ul>"+
										"</div>";
							htmlStr+= "</td>"+
									"</tr>";
							
				return htmlStr;
			},
			alertBoxContain : function (title, body, confirmation) {
				var htmlStr = "";
				htmlStr += '<div class="alert_box">'+
					'<div class="icon">'+
					'<img src="'+assetPath+'/cmsBuilder/img/warning.gif" alt="Computer man" style="width: 80px;height: auto;">'+
					'</div>'+
					'<header>'+title+'</header>'+
					'<p>'+body+' !</p>'+
					'<p>'+confirmation+'</p>'+
					'</div>';
					return htmlStr;
			}
		},
		events :{ 
			bind : function(){
				$(".delete-page").off().on("click",function(e){
					costumizer.pages.actions.delete(e);
				});
				
				$(".editPage, .create-page").off().on("click",function(){
					costumizer.pages.actions.save($(this).data("key"),costumizer.obj.app[$(this).data("key")]);
				});
				$(".duplicate-page").off().on("click", function(){
					costumizer.pages.actions.duplicate($(this).data("key"));
				});

				$(".brouillon-page").off().on("click", function(){
					costumizer.pages.actions.publiePage($(this).data("key"), "brouillon");
				});
				$(".publie-page").off().on("click", function(){
					costumizer.pages.actions.publiePage($(this).data("key"), "publie");
				});

				$(".lbh-costumizer-edit-page").off().on("click", function(){
					urlCtrl.loadByHash($(this).attr("href"));
					costumizer.actions.closeModal();
				});
				$(".checkbox-tools .selectPages").off().on("click",function(){
					if($(".checkbox-tools .selectPages:checked").length > 0)
						$(".multiple-delete-page").removeClass("disabled");
					else
						$(".multiple-delete-page").addClass("disabled");
				});
			},
			change : function(name, value, payload, keyApp=null, anchorClassHideOrShow="", paramsToSend, urlExternOptions = ""){
				var deletelbhAnchor = ["metaDescription", "metaTags", "selectApp", "appTypeName","target"];

				if(typeof $("#costum-form-pages #btn-submit-form").attr("disabled") != "undefined" && $("#costum-form-pages #btn-submit-form").attr("disabled") != false){
					$("#costum-form-pages #btn-submit-form").removeAttr("disabled");
				}

				if(name != "keySlug"){
					if(typeof payload != "undefined" && typeof payload.path != "undefined"){
						paramsToSend[payload.path] = value;
					}
					else{
						paramsToSend[name] = value;
					}	
				}	

				if((name == "subdomainName" && keyApp==null) || name == "keySlug"){
					let slugApp=slugify(value).toLowerCase();			
					$("#costum-form-pages input[name=keySlug]").val(slugApp);	
					costumizer.pages.actions.checkUniqueApp(value, keyApp);
					paramsToSend["keySlug"] = slugApp;
				}

				if(name == "lbhAnchor"){
					if(value == true || value == "true"){
						$(anchorClassHideOrShow).hide();
						
						$.each(deletelbhAnchor, function(kk, val){
							if(typeof paramsToSend[val] != "undefined"){
								delete paramsToSend[val];
							}		
						});
					}
					else{
						$(anchorClassHideOrShow).show();
						if($("#costum-form-pages .selectApp .btn-group .btn-primary").data("value") == "false" || $("#costum-form-pages .selectApp .btn-group .btn-primary").data("value") == false){
							$("#costum-form-pages .appTypeName").hide();
						}
											
					}
				}

				if(name == "selectApp"){
					if(value == true || value == "true"){
						$("#costum-form-pages .appTypeName").show();
					}
					else{
						$("#costum-form-pages .appTypeName").hide();
					}
				}

				if(name == "target"){
					if(value == true || value == "true"){
						$(urlExternOptions).hide();
						$("#costum-form-pages .urlExtern").show();
					}	
					else{
						$(urlExternOptions).show();
						$("#costum-form-pages .urlExtern").hide();
					}
				}
			
			}
		},
		actions : {
			load: function() {
			},

			callback : function(data){	
				$("#content-btn-delete-page").remove();
				$("#menu-right-costumizer").fadeOut();
				
			},
			deletePage : function(pages) {
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/deletepage",
					{
						costumId : costum.contextId,
						costumType : costum.contextType,
						pages: pages
					},
					function(data) {
						if ( data && data.result ) {
							$.each(pages,function(key,value){
								if (exists(costumizer.obj.app[value])){
									delete costumizer.obj.app[value];
									$(".tabling-pages tr[id='keyPages-"+value+"']").remove();
								}
								if (exists(themeParams.pages[value])){
									delete themeParams.pages[value];
								}
								if (exists(urlCtrl.loadableUrls[value])){
									delete urlCtrl.loadableUrls[value];
								}
								if (exists(costum.appConfig.pages[value])){
									delete costum.appConfig.pages[value];
								}
								
							});
							if (typeof data.pathToDelete != "undefined" && notEmpty(data.pathToDelete)) {
								if (data.pathToDelete.length > 1) 
									data.pathToDelete.sort((a, b) => b.split(".").length - a.split(".").length);
								Object.values(data.pathToDelete).forEach(function(value) {
									jsonHelper.deleteByPath(costumizer.obj, value);
									costumizer.actions.change(value);
									let newPath = "";
									let objSplice = value.split(".");
									for (let index = 0; index < objSplice.length - 2; index++) {
										if (index == 0)
											newPath = objSplice[index];
										else 
											newPath +="."+objSplice[index];
									}
									let menuParent = objSplice[objSplice.length - 3];
									if (menuParent.charAt(0) == "#") {
										let objNewPath = jsonHelper.getValueByPath(costumizer.obj, newPath+".buttonList");
										if (typeof objNewPath != "undefined" && !notEmpty(objNewPath)) {
											jsonHelper.setValueByPath(costumizer.obj, newPath, true);
											costumizer.actions.change(newPath);
										}
									} else {
										let objNewPath = jsonHelper.getValueByPath(costumizer.obj, newPath+".buttonList");
										if (typeof objNewPath != "undefined" && !notEmpty(objNewPath)) {
											jsonHelper.deleteByPath(costumizer.obj, newPath+".buttonList");
											costumizer.actions.change(newPath);
										}
									}
								})
								var pathValueToUpdate = {};
								$.each(costumizer.actions.pathToValue, function(k,v) {
									var valueToUp = jsonHelper.getValueByPath(costumizer.obj, v);
									pathValueToUpdate[v] = valueToUp;
								})
								
								costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
								costumizer.actions.update();
							}
						} else {
							toastr.error("Something went wrong!! please try again. " + data.msg);
						}
					}
				);
			},
			delete: function(e){
				var pages = [];
				if ($(e.currentTarget).data("id") == "delete_all") {
					//if ($('.selectPages:checked').length == 0) {
					//	toastr.error(tradCms.checkAtLeastOne);
					//} else {
						bootbox.confirm({
							message: costumizer.pages.views.alertBoxContain(tradBadge.confirm, tradCms.unrecoverablePage, tradCms.confirmDeletePage),
							buttons: {
								confirm: {
									label: tradCms.yesDelete,
									className: 'btn-danger'
								},
								cancel: {
									label: trad.cancel,
									className: 'btn-default'
								}
							},
							callback: function (response) {
								if (response) {
									$('.selectPages:checked').each(function () {
										pages.push($(this).val());
									});
									costumizer.pages.actions.deletePage(pages);
								}
							}
						});
					//}
				} else {
					bootbox.confirm({
						message: costumizer.pages.views.alertBoxContain(tradBadge.confirm, tradCms.unrecoverablePage, tradCms.confirmDeletePage),
						buttons: {
							confirm: {
								label: tradCms.yesDelete,
								className: 'btn-danger'
							},
							cancel: {
								label: trad.cancel,
								className: 'btn-default'
							}
						},
						callback: function(response) {
							if (response) {
								pages.push($(e.currentTarget).attr("id"));
								costumizer.pages.actions.deletePage(pages);
								$("#menu-right-costumizer").fadeOut();
								$("#content-btn-delete-page").remove();
							}
						}
					});
				}
			},
			duplicate : function(datakey){
				let data = {
					"page": datakey,
					"costumId": costumizer.obj.contextId,
					"costumType": costumizer.obj.contextType,
				};
				ajaxPost(
					null,
					baseUrl + "/co2/cms/duplicate",
					data,
					function(response) {
						mylog.log("response", response);
						$(".newPage").remove();
						$.each(response.page, function(k, v) {
							costumizer.obj.app[k] = v;
							urlCtrl.loadableUrls[k] = v;
							$(".tabling-pages > tbody").append(costumizer.pages.views.linePage(k, v));
						});
						// coInterface.bindEvents();
						//costumizer.initEvents();
						// costumizer.obj.htmlConstruct.header.menuTop=response.costum.costum.htmlConstruct.header.menuTop;
						costumizer.pages.events.bind();
					}
				);

				// alert("Le travail à réaliser pour dupliquer "+datakey+" est expliqué en commentaire dans la fonction @costumizer.pages.actions.duplicate");

				
			},
			publiePage: function(datakey, mode){

				var message = (mode == "brouillon") ? tradCms.confirmDraft: tradCms.confirmPublish;
				
				bootbox.confirm({
					message: costumizer.pages.views.alertBoxContain(tradBadge.confirm, message,tradCms.confirmToContinue),
					buttons: {
						confirm: {
							label: trad.yes,
							className: 'btn-danger'
						},
						cancel: {
							label: trad.cancel,
							className: 'btn-default'
						}
					},
					callback: function(response) {
						if (response) {

							costumizer.actions.change("app."+datakey+".restricted.draft");
							if(mode == "brouillon"){
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.draft", true);
								
								costumizer.actions.change("app."+datakey+".restricted.admins");
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.admins", true);
							}
							else{
								costumizer.actions.change("app."+datakey+".restricted.admins");
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.admins", "$unset" );

								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.draft", "$unset");
							}
								
								
							costumizer.actions.update()
							$("#menu-right-costumizer").fadeOut();
							var publish = costumizer.pages.views.linePage(datakey, costumizer.obj.app[datakey]);		
							$(".tabling-pages tr[id='keyPages-"+datakey+"']").replaceWith(publish);	
							costumizer.pages.events.bind();
							
						}
					}
				});
			},
			save : function(keyApp, values, htmlConstruct){
	
				//Todo 
				// Utiliser cette fonction pour le create et le update 
				// Créer les inputs dans la modal right avec le max d'options
					/*
						[x] slug key
						[x] subDomainName
						[x] icon 
						[x] admin  
						[x] shortdescription (le jour où on se préocuppe des méta data)
						[x] metatags (le jour où on se préocuppe des méta data)
						[x] Static page or app 

						[x] Si app selecteur (agenda, live, and others )
						[x] use filter
						[x] type filtre  
						[x] trier par
					*/
				// Créer un ctk dans cmsControlleur => SavePagesAction
				// Au retour enregistrer la nouvelle entrée ou changer la valeur existante 
				// rafraichisser la vue du tableau tbody et bind events
				
				var titleRightCostumizer=(notEmpty(keyApp)) ? tradCms.editPage : tradCms.createNewPage;
				var createPage=(notEmpty(keyApp)) ? false : true;

				var anchorClassHideOrShow=".metaDescription, .metaTags, .selectApp, .appTypeName, .target, .urlExtern";
				var urlExternOptions=".metaDescription, .metaTags, .selectApp, .appTypeName, .lbhAnchor";
				var paramsToSend = {};
				var indexOfInput = {subdomainName : 0, keySlug : 1, icon : 2, lbhAnchor : 3, metaDescription : 6, 
					metaTags : 7, isAdmin : 8, selectApp : 9, appTypeName : 10, draft : 11, target : 4, urlExtern : 5};

				if(notEmpty(values) && notEmpty(values.lbhAnchor)){
					values.lbhAnchor = (values.lbhAnchor == "false" || values.lbhAnchor == false) ? false : true;
				}
				
				costumizer.actions.openRightSubPanel(
					"<form id='costum-form-pages' style='padding:0px 10px;'></form>",
					{
						width:300,
						distanceToRight: costumizer.liveChange ? 300:0,
						className:"contains-view-admin",
						header:{
							title:titleRightCostumizer
						},
						onInitialized: function(){
							var rulesForm={};
							var defaultDraft = false;
							var defaultIsAdmin = false;

							if(notEmpty(keyApp)){
								defaultValues = costumizer.obj.app[keyApp];
							}
							else{
								if(typeof defaultValues != "undefined")
									delete defaultValues;
								defaultDraft = true;
								defaultIsAdmin = true;
							}
							paramsAppType = {
								search : trad.search,
								live : tradCms.journal,
								projects : tradCategory.projects,
								annonces : tradCategory.classifieds,
								agenda : trad.agenda,
								dda : trad.survey,
								map : trad.map,
							};
							if(!$("#costum-form-pages").hasClass(`cmsbuilder-tabs`))
								$("#costum-form-pages").addClass(`cmsbuilder-tabs`);

							$("#costum-form-pages").append(`<div class="containte-form-page cmsbuilder-tabs-body col-xs-12"></div>`);
							var myCoInput = {
								container : ".containte-form-page",
								inputs :[
									{
										type : "inputSimple",
										options: {
											name : "subdomainName",
											label :  tradCms.pageName,
											class : "subdomainName",
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["subdomainName"] != "undefined") ? defaultValues["subdomainName"]:"",
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "keySlug",
											label :  tradCms.urlKey,
											class : "keySlug",
											defaultValue : (notEmpty(keyApp)) ? keyApp.replace("#", "") : "",
										}
									},
									{
										type: "inputIcon",
										options: {
											name: "icon",
											label: tradCms.icon,
											class : "icon",
											defaultValue: (typeof defaultValues != "undefined" && typeof defaultValues["icon"] != "undefined" && notEmpty(defaultValues["icon"])) ? "fa fa-"+defaultValues["icon"]:tradCms.noIcon,
										}
									},		
									{
										type : "groupButtons",
										options: {
											name : "lbhAnchor",
											label :  tradCms.simpleAnchorForMenu,
											class : "lbhAnchor",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["lbhAnchor"] != "undefined") ? defaultValues["lbhAnchor"]:false,
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "target",
											label :  tradCms.addExternalLink,
											class : "target",
											options : [{"label" : "Oui", "value" : true, "icon" : ""},{"label" : "Non", "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["target"] != "undefined") ? defaultValues["target"]:false,
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "urlExtern",
											label :  tradCms.externalLink,
											class : "urlExtern",
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["urlExtern"] != "undefined") ? defaultValues["urlExtern"]:"",
										}
									},
									{
										type : "textarea",
										options: {
											name : "metaDescription",
											label : tradCms.shortDescription,
											class : "metaDescription",
											defaultValue :(typeof defaultValues != "undefined" && typeof defaultValues["metaDescription"] != "undefined") ? defaultValues["metaDescription"]:"",
											rows : 1 
										},
									},
									{
										type : "tags",
										options : {
											name : "metaTags",
											label : tradDynForm.keyWords,
											class : "metaTags",
											options : [""],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["metaTags"] != "undefined") ? defaultValues["metaTags"]:"",
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "selectApp",
											label :   tradCms.usedAvancedApp,
											class : "selectApp",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["selectApp"] != "undefined") ? defaultValues["selectApp"]:false,
										}
									},
									{
										type : "select",
										options : {
											name : "appTypeName",
											label : trad.type +" App",
											class : "appTypeName",
											options : Object.keys(paramsAppType).map(function(value) {
												return {
													label: paramsAppType[value],
													value: value
												}
											}),
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["appTypeName"] != "undefined") ? defaultValues["appTypeName"]:false,
										}
									},
									
								],
								onchange:function(name, value, payload){
									costumizer.pages.events.change(name, value, payload, keyApp, anchorClassHideOrShow, paramsToSend, urlExternOptions);
								}
							}
							
							new CoInput(myCoInput);

							$(".containte-form-page").append(`
							<div class="panel-group restriction-container" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab">
										<p class="panel-title">
											<a  role="button" data-toggle="collapse" data-parent="#accordion" href="#restrictionPage" aria-expanded="true" aria-controls="collapse">
												<span  class="fa fa-exclamation-triangle" aria-hidden="true"></span>`+ tradCms.accessRestriction +
											`</a>
										</p>
									</div>
								</div>
							</div>
							<div id="restrictionPage" class="panel-collapse collapse">
											
							</div>
							
							`)
							

							new CoInput({
								container : "#restrictionPage",
								inputs :[
									{
										type : "groupButtons",
										options: {
											name : "members",
											label :  tradCms.visibleMembers,
											class : "members",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined" && typeof defaultValues["restricted"]["members"] != "undefined") ? defaultValues["restricted"]["members"]:false,
											payload : {
												path : "restricted.members",
											}
										}
									},
									
									{
										type : "groupButtons",
										options: {
											name : "isAdmin",
											class : "isAdmin",
											label :   tradCms.visibleToAdminOnly,
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined" && typeof defaultValues["restricted"]["admins"] != "undefined") ? defaultValues["restricted"]["admins"]:defaultIsAdmin,
											payload : {
												path : "restricted.admins",
											}
										}
									},
									{
										type : "tags",
										options : {
											name : "roles",
											label : tradCms.visibleRoles,
											class : "roles",
											options : ["Financeur","Partenaire","Sponsor","Organisateur","Président","Directeur","Conférencier","Intervenant"],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined" && typeof defaultValues["restricted"]["roles"] != "undefined") ? defaultValues["restricted"]["roles"]:"",
											payload : {
												path : "restricted.roles",
											}
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "draft",
											label :   "status",
											class : "draft",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined"  && typeof defaultValues["restricted"]["draft"] != "undefined") ? defaultValues["restricted"]["draft"]: defaultDraft,
											payload : {
												path : "restricted.draft",
											}
										}
									},
								],
								onchange:function(name, value, payload){
									costumizer.pages.events.change(name, value, payload, keyApp, anchorClassHideOrShow, paramsToSend, urlExternOptions);
								}
							});

							$("#restrictionPage .draft").css("display", "none");
							if(!notEmpty(keyApp)){
								$("#costum-form-pages .keySlug").removeClass("has-success").addClass("has-error");
							}
							
							$("#costum-form-pages .keySlug, .subdomainName, .appTypeName").append("<span class='help-block'></span>"); 
							

							if(myCoInput.inputs[indexOfInput.selectApp].options.defaultValue == false || myCoInput.inputs[indexOfInput.selectApp].options.defaultValue == "false" ){
								$("#costum-form-pages .appTypeName").hide(); 
							}
							if(myCoInput.inputs[indexOfInput.target].options.defaultValue == false || myCoInput.inputs[indexOfInput.target].options.defaultValue == "false" ){
								$("#costum-form-pages .urlExtern").hide(); 
							}

							if(typeof values != "undefined" && values != null){
								if(typeof values.lbhAnchor != "undefined" && values.lbhAnchor)
									$(anchorClassHideOrShow).hide();

								if(typeof values.target != "undefined" && values.target)
									$(urlExternOptions).hide();
							}
							
							$("#costum-form-pages").append(`<div class="cmsbuilder-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;">
								<a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled><i class="fa fa-save"></i> `+tradCms.register+`</a>
							</div></div>`);
							
							$("#costum-form-pages #btn-submit-form").click(function() {

								if(notEmpty($("#costum-form-pages input[name=subdomainName]").val())){
									$("#costum-form-pages .subdomainName .help-block").removeClass('letter-red').addClass('letter-green').text("");
									
									if (!$("#costum-form-pages .keySlug").parent().hasClass("has-error")) {
										formData = {};

										$.each(myCoInput.inputs, function(index, val){
											var path = (typeof val.options.payload != "undefined" && typeof val.options.payload.path != "undefined") ? val.options.payload.path : val.options.name;
											if(val.type == "inputSimple" && val.options.name != "isAdmin"  && val.options.name != "keySlug")
											if( $("input[name="+val.options.name+"]").val() != false && $("input[name="+val.options.name+"]").val() != "false")
												formData[path] = $("input[name="+val.options.name+"]").val();

											if(val.type == "textarea")
												formData["metaDescription"] = $("textarea[name="+val.options.name+"]").val();
										});

										var newKey = "#" + $("#costum-form-pages input[name=keySlug]").val();	
										formData.restricted = {};

										if($("#costum-form-pages .selectApp .btn-group .btn-primary").data("value") == "true" || $("#costum-form-pages .selectApp .btn-group .btn-primary").data("value") == true){
											formData.selectApp = true;
											formData.appTypeName = $("select[name=appTypeName]").val();
										}

										if(!formData.selectApp || (formData.selectApp && typeof formData.appTypeName != "undefined" && notEmpty(formData.appTypeName))){
											$("#costum-form-pages .appTypeName .help-block").removeClass('letter-red').addClass('letter-green').text("");
											if (notEmpty(keyApp)) {
												if (typeof costumizer.obj.app[keyApp].urlExtra == "undefined" || formData.selectApp === true)
													formData.urlExtra = "/page/" + keyApp.replace("#", "");

												if(typeof costumizer.obj.app[keyApp].icon != "undefined")
													formData.icon = costumizer.obj.app[keyApp].icon;
												else
													formData.icon = "";
		
												if(typeof costumizer.obj.app[keyApp].metaTags != "undefined")
													formData.metaTags =costumizer.obj.app[keyApp].metaTags;
												else
													formData.metaTags = [];

												if(typeof costumizer.obj.app[keyApp].restricted != "undefined" && typeof costumizer.obj.app[keyApp].restricted.roles != "undefined")
													formData.restricted["roles"] = costumizer.obj.app[keyApp].restricted.roles;
												else
													delete formData.restricted["roles"];
											} else {
												formData.urlExtra = "/page/" + newKey.replace("#", "");
													
												formData.restricted["draft"] = true;												
											}

											if(typeof paramsToSend["icon"] != "undefined")
												formData.icon = paramsToSend["icon"];
											
											if(typeof paramsToSend["metaTags"] != "undefined"/* && notEmpty(paramsToSend["metaTags"])*/)
												formData.metaTags = paramsToSend["metaTags"].split(", ");
												
											
												
											if (formData.selectApp && typeof urlCtrl.loadableUrls["#" + formData.appTypeName] != "undefined") {
												$.each(urlCtrl.loadableUrls["#" + formData.appTypeName], function (e, v) {
													if ($.inArray(e, ["subdomainName", "icon", "metaDescription", "metaTags"]) <= 0) {
														if (e == "module") delete formData.urlExtra;
															formData[e] = v;
													}
												});
												if(formData.appTypeName != "dda" && formData.appTypeName != "annonces" )
													formData.urlExtra = "/page/" + formData.appTypeName;
											}
											else
												formData.hash = "#app.view";

											if ($("#costum-form-pages .lbhAnchor .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .lbhAnchor .btn-group .btn-primary").data("value") === true) {
												formData.lbhAnchor = true;
												formData.metaDescription = "";
												formData.metaTags = [];
												formData.appTypeName = "";
												formData.urlExtern = "";
												formData.urlExtra = "/page/welcome";
												formData.hash = "#app.view";
												
												var appValueToDelete = ["module", "initFilters", "filterObj", "type", "useFilter", "useFooter"];

												if(notEmpty(keyApp)){
													$.each(appValueToDelete, function(key, value){
														if(typeof formData[value] != "undefined"){
															delete formData[value];
														}
													});		
												}
											}
											if ($("#costum-form-pages .target .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .target .btn-group .btn-primary").data("value") === true) {
												formData.target = true;
												formData.metaDescription = "";
												formData.metaTags = [];
												formData.urlExtern = $("#costum-form-pages input[name=urlExtern]").val();	
												formData.hash = "#app.view";
												
												var appValueToDelete = ["module", "initFilters", "filterObj", "type", "useFilter", "useFooter"];

												if(notEmpty(keyApp)){
													$.each(appValueToDelete, function(key, value){
														if(typeof formData[value] != "undefined"){
															delete formData[value];
														}
													});		
												}
											}
											else{ 
												delete formData.urlExtern;
											}
											if(notEmpty(keyApp)){
												if(typeof costumizer.obj.app[keyApp] != "undefined" && typeof costumizer.obj.app[keyApp].useFilter != "undefined"  && typeof costumizer.obj.app[keyApp].selectApp == "undefined" ){
													var tmpFormData = formData;
													formData = {};

													$.each(costumizer.obj.app[keyApp], function(key, val){
														formData[key] = val;
													});
													formData.subdomainName = tmpFormData.subdomainName;
													formData.icon = tmpFormData.icon;
												}

												if(typeof costumizer.obj.app[keyApp].urlExtra != "undefined" && costumizer.obj.app[keyApp].urlExtra.indexOf("/page/") != -1)
													formData.urlExtra = costumizer.obj.app[keyApp].urlExtra.replace(keyApp.replace("#", ""), newKey.replace("#", ""));
												if ($("#costum-form-pages .target .btn-group .btn-primary").data("value") === "false" || $("#costum-form-pages .target .btn-group .btn-primary").data("value") === false) 
													formData.urlExtra = "/page/" + newKey.replace("#", "");
												else
													delete formData.urlExtra;
												
												
											} 
											if(typeof formData.restricted == "undefined")
												formData.restricted = {};

											if ($("#costum-form-pages .isAdmin .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .isAdmin .btn-group .btn-primary").data("value") === true) {
												formData.restricted["admins"] = true;
												($("#costum-form-pages .draft .btn-group .btn-primary").data("value") == "true" || $("#costum-form-pages .draft .btn-group .btn-primary").data("value") == true) ? formData.restricted["draft"] = true : delete formData.restricted["draft"];
											}
											else { 
												delete formData.restricted["admins"];
												delete formData.restricted["draft"];
											}

											if ($("#costum-form-pages .members .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .members .btn-group .btn-primary").data("value") === true) {
												formData.restricted["members"] = true;
											}else{
												delete formData.restricted["members"];
											} 
											if(typeof paramsToSend["restricted.roles"] != "undefined" && notEmpty(paramsToSend["restricted.roles"]))
												formData.restricted["roles"] = paramsToSend["restricted.roles"].split(",");		
											jsonHelper.setValueByPath(costumizer.obj, "app." + newKey, formData);
											jsonHelper.setValueByPath(themeParams.pages, newKey, formData);
											jsonHelper.setValueByPath(costum, "appConfig.pages." + newKey, formData);
											jsonHelper.setValueByPath(urlCtrl.loadableUrls, newKey, formData);
											
											if (notEmpty(keyApp) && newKey != keyApp) {
												if(typeof costumizer.obj.app[keyApp] != "undefined" && typeof costumizer.obj.app[keyApp].urlExtra != "undefined" && costumizer.obj.app[keyApp].urlExtra.indexOf("/page/") != -1){
													ajaxPost(
														null,
														baseUrl+"/"+moduleId+"/cms/updatevaluepagecms",
														{
															params : {'keyApp' : keyApp.replace("#",""), 'newKey' : newKey.replace("#","")}
														},
														function(data){
															return true;
														}			
													);
												}	
												jsonHelper.deleteByPath(costumizer.obj, "app." + keyApp);
												jsonHelper.deleteByPath(themeParams, "pages." + keyApp);
												jsonHelper.deleteByPath(costum, "appConfig.pages." + keyApp);
												$("a[href='" + keyApp + "']").attr("href", newKey);
											}
											if (notEmpty(htmlConstruct)) {
												if(notEmpty(keyApp))
													costumizer.htmlConstruct.actions.replacePage(htmlConstruct.path, keyApp, newKey);
												else
													costumizer.htmlConstruct.actions.addPageInMenu(htmlConstruct.path, newKey, htmlConstruct.domAppend);
												$("#right-sub-panel-container").fadeOut(100);
											} else {
												var newLine = costumizer.pages.views.linePage(newKey, formData);	
														
												if(notEmpty(keyApp)){
													$(".tabling-pages tr[id='keyPages-"+keyApp+"']").replaceWith(newLine);
												}else{
													$(".tabling-pages tbody").append(newLine);
												}
												costumizer.pages.events.bind();
											}
											var params = {};
											params["app"] = costumizer.obj.app;
											var pathHtmlConstruct = costumizer.htmlConstruct.actions.findIfKeyPageExist(costumizer.obj.htmlConstruct, keyApp);
											if (notEmpty(pathHtmlConstruct)) {
												costumizer.htmlConstruct.actions.editPageInMenu(keyApp, newKey, pathHtmlConstruct);
												Object.values(pathHtmlConstruct).forEach(function(value) {
													params["htmlConstruct."+value] = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, value);
												})
											}
											
											ajaxPost(
												null,
												baseUrl+"/"+moduleId+"/cms/updatecostum",
												{
													params: params
												},
												function(data){
													costum.app=jQuery.extend(true,{},costumizer.obj.app);
													toastr.success(tradCms.updateConfig);
													$("#right-sub-panel-container").fadeOut();
												}			
											);
										}
										else{
											$("#costum-form-pages .appTypeName .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.thisFieldIsRequired);
										}
									} 
								}else{
									$("#costum-form-pages .subdomainName .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.thisFieldIsRequired);
									$(".right-sub-panel-body #costum-form-pages .subdomainName")[0].scrollIntoView({
										block : "end",
										behavior : "smooth"
									});
								}
							});
							
						}
					}
				)			
        	},
			callback : function(){
							
			},
			checkUniqueApp :function(newKey, oldKey){
				oldKey=(notEmpty(oldKey)) ? oldKey.replace("#", "") : "";
				newKey = slugify(newKey).toLowerCase();
				if(newKey != oldKey && typeof themeParams.pages["#"+newKey] != "undefined"){
					$("#costum-form-pages .keySlug").removeClass("has-success").addClass("has-error");
					$("#costum-form-pages .keySlug .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.keyUsed);
				}
				else{
					$("#costum-form-pages .keySlug").removeClass("has-error").addClass("has-success");
					$("#costum-form-pages .keySlug .help-block").removeClass('letter-red').addClass('letter-green').text(tradCms.keyAvailable);
				}
			}
		},	
	},
	htmlConstruct  : {
		init : function(){
			costumizer.liveChange=false;
			// Initialisation de la config htmlConstruct du costum @keyConf 
			if(costumizer.htmlConstruct.actions.isActivated("menuLeft")){
				if(costumizer.htmlConstruct.actions.isActivated("header.banner"))
					costumizer.htmlConstruct.keyConf= "configFour"; 
				else
					costumizer.htmlConstruct.keyConf = (costumizer.htmlConstruct.actions.isActivated("header.menuTop")) ? "configThree" : "configFive";
			}else
				costumizer.htmlConstruct.keyConf = (costumizer.htmlConstruct.actions.isActivated("header.banner")) ? "configTwo" : "configOne";
			
		},
		keyConf : "",
		map : {
			header:{
				titleClass: "labelEditMenu",
				containerClass: "col-xs-12 main-section editMenuCostumizer",
				title : tradCms.headerSection,
				subList : {
					banner : {
						title : tradCms.banner,
						view : "blockCms",
						activable : "disabled"
					},
					menuTop: {
						title : tradCms.topMenu,
						activable : true,
						subList : {
							left : {
								title : tradCms.leftSection,
								domId : "menuTopLeft",
								view : "menuConstruct",
								containerClass : "col-xs-12 col-sm-4 manageMenusCostum column-menu-costumizer"
								
							},
							center : {
								view : "menuConstruct",
								title : tradCms.centerSection,
								domId : "menuTopCenter",
								containerClass : "col-xs-12 col-sm-4 manageMenusCostum column-menu-costumizer"
							}	,
							right : {
								view : "menuConstruct",
								title : tradCms.rightSection,
								domId : "menuTopRight",
								containerClass : "col-xs-12 col-sm-4 manageMenusCostum column-menu-costumizer"
							}
						}
					}
				}
			},
			menuLeft : {
				title : tradCms.leftMenu,
				view : "menuConstruct",
				domId : "menuLeft",
				activable : true
			},
			subMenu : {
				title : tradCms.subMenu,
				view : "menuConstruct",
				domId : "subMenu",
				activable : true
			},
			footer : {
				title : tradCms.footer,
				view : "blockCms",
				activable : true
			}
		}, 
		options: {
			configOne : {
				img : assetPath+"/cmsBuilder/img/configOne.png",
				map: {
					header : {
						banner : false,
						menuTop : true
					},
					menuLeft : false,
					subMenu : true,
					footer : false
				}
			},
			configTwo : {
				img : assetPath+"/cmsBuilder/img/configTwo.png",
				map: {
					header : {
						banner : true,
						menuTop : true
					},
					menuLeft : false,
					subMenu : false,
					footer : false
				}
			},
			configThree : {
				img : assetPath+"/cmsBuilder/img/configThree.png",
				map: {
					header : {
						banner : false,
						menuTop : true
					},
					menuLeft : true,
					subMenu : false,
					footer : false
				}
			},
			configFour : {
				img : assetPath+"/cmsBuilder/img/configFour.png",
				map: {
					header : {
						banner : true,
						menuTop : true
					},
					menuLeft : true,
					subMenu : false,
					footer : false
				}
			},
			configFive : {
				img : assetPath+"/cmsBuilder/img/configFive.png",
				map: {
					header : {
						banner : false,
						menuTop : false
					},
					menuLeft : true,
					subMenu : false,
					footer : false
				}
			}
		},
		buttonValue:{},
		//btnNavigation: [],
		//btnNavigationTrue: [],
		listInput: {
			logo : {
				"height" : {
					label : tradCms.height,
					type : "text"
				},
				"width" : {
					label : tradCms.width,
					type : "text"
				},
				"heightMin" : {
					label : tradCms.heightXs,
					type : "text"
				},
				"widthMin" : {
					label : tradCms.widthXs,
					type : "text"
				},
				labelString : false,
				icon : false,
				spanTooltip : false,
				labelClass : false,
				class : false
			},
			app : {
				"icon" : {
					label : tradCms.showIcon,
					type : "select", 
					options : ["false", "true"]
				},
				"label" : {
					label : tradCms.showLabel,
					type : "select", 
					options : ["false", "true"]
				},
				"spanTooltip" : {
					label : tradCms.showTooltip,
					type : "select", 
					options : ["false", "true"]
				}//,
				//generatePage : true
			},
			userProfil : {
				"img" : {
					label : tradCms.showImage,
					type : "select", 
					options : ["false", "true"]
				},
				"dashboard" : {
					label : tradCms.showDashboard,
					type : "select", 
					options : ["false", "true"]
				},
				label : false,
				icon : false,
				labelClass: false,
				class: false
			},
			cotools : {
				"dashboard" : {
					label : tradCms.showDashboard,
					type : "select", 
					options : ["false", "true"]
				}
			},
			editcms : {
				"dashboard" : {
					label : tradCms.showDashboard,
					type : "select", 
					options : ["false", "true"]
				}
			},
			searchBar : {
				"dropdownResult" : {
					label : tradCms.displayResultsDropDown,
					type : "select",
					options : ["false", "true"]
				},
				"label" : {
					label : tradCms.showLabel,
					type : "select", 
					options : ["false", "true"]
				},
			}
		},
		formOpt : {
			"label" : {
				label : tradCms.buttonLabel,
				type : "text"
			},
            "icon" : {
				label : tradCms.buttonIcon,
				type : "selectFontAwesome",
				options: Object.keys(fontAwesome).map(function(key, value) {
					return key
				})
			},
			"spanTooltip" : {
				label : tradCms.tooltip,
				type : "text"
			},
			"labelClass" : {
				label : tradCms.buttonLabelClass,
				type : "text"
			},
			"class" : {
				label : tradCms.ButtonClass,
				type : "text"
			}
		},
		views : {
			init : function(){
				var str="";
				str+=costumizer.htmlConstruct.views.helper()+
				 	"<div class='config-table-menu col-xs-12 margin-bottom-50'>";
							/*"<h2 class='costumizer-title'>Configuration of menus</h2>";*/
							$.each(costumizer.htmlConstruct.options[costumizer.htmlConstruct.keyConf].map, function(e , v){
				str+=			costumizer.htmlConstruct.views.siteMap(e, e, v);
							});
				str+="</div>"; 
				return str;
			},
			helper : function(){
				var str='<div class="pages-toolbar padding-15">'+
							'<div class="explain-admin-costumizer col-xs-12 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-1"> '+
								'<h3>'+tradCms.sitemapCurrentlyActivated+'</h3>';
								$.each(costumizer.htmlConstruct.options, function(e, v){
									var activeClass=(e==costumizer.htmlConstruct.keyConf) ? " active col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3 col-xs-12" : "col-md-4 col-sm-6 col-xs-12 notActive";
									var displayHide = (e==costumizer.htmlConstruct.keyConf) ? "" : "display: none;";
					str+=			'<div class="parentSitemapOptions '+activeClass+'" style="'+displayHide+'">'+
										'<div class="box-mockup configOptions" data-key="'+e+'">'+
											'<img  src = "'+baseUrl+v.img+'" class="img-responsive"/>'+
											'<div class="box-content">'+
												'<h3 class="title">'+
													e+
												'</h3>';
									if(e==costumizer.htmlConstruct.keyconf)
					str+=						'<span class="post">'+tradCms.mapActivated+'</span>';
					str+=					'</div>'+
										'</div>'+
									'</div>';
								});
					str+=		'<div class="col-xs-12 text-center">'+
									'<a href=""javascript:;" class="showAllSitemapOptions">'+
										'<i class="fa fa-caret-down"></i>'+tradCms.seeFeasibleWebsiteModels+
									'</a>'+
									'<h5>'+tradCms.enableDisableFollowingMenuSections+'.</h5>'+
									'<span>'+
										'<i class="fa fa-angle-double-left"></i> '+tradCms.WarningChangeOfConfiguration+' <i class="fa fa-angle-double-right"></i>'+
									'</span>'+
									'<label type="submit" class="icon-info badge"><i class="fa fa-info-circle"></i></label>'+
								'</div>'+
							'</div>	'+
						'</div>';
				
				return str;
			
			},
			siteMap : function(pathMap, key, activate, level=0){
				if (typeof jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key) != "undefined") {
					if (typeof jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key+".activated") != "undefined" && typeof jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key+".activated") == "boolean") {
						activate = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key+".activated");
					}
				}
				if(typeof jsonHelper.getValueByPath(costumizer.htmlConstruct.map, pathMap) != "undefined"){
					var objMap= jsonHelper.getValueByPath(costumizer.htmlConstruct.map, pathMap);
					var str="";
					var containerClass=(typeof objMap.containerClass != "undefined") ? objMap.containerClass : "editMenuCostumizer col-xs-12"; 
					str+=  "<div class='"+containerClass+" level-tree-"+level+"'>";
							if(typeof objMap.title != "undefined"){
								classTit=( typeof objMap.titleClass != "undefined") ? objMap.titleClass : "";
					str+=		"<div class='"+classTit+" titleSection'>"+
									"<span>"+objMap.title+"</span>";
								if(typeof objMap.activable != "undefined" && objMap.activable){
									attrDisabled=(objMap.activable=="disabled")	? " disabled" : "";
					str+=			'<input type="checkbox" name="section-sitemap" value="'+activate+'" data-type="'+key+'" data-size="mini" data-label-width="1" data-on-text="Activé" data-off-text="Désactivé" data-handle-width="1" '+attrDisabled+'/>';
								}
					str+=		"</div>";
							}
							if(typeof objMap.subList != "undefined"){
								level++;
								$.each(objMap.subList, function(e, v){
									let activableValue=jsonHelper.getValueByPath(costumizer.htmlConstruct.options[costumizer.htmlConstruct.keyConf].map, key+"."+e);
					str+=			costumizer.htmlConstruct.views.siteMap(pathMap+".subList."+e, key+"."+e, activableValue, level);
								});
								
							}
							else if(typeof objMap.view != "undefined"){
								let params=(typeof objMap.params != "undefined") ? objMap.params : null;
					//			alert("htmlConstruct."+key);
					str+=		costumizer.htmlConstruct.views[objMap.view]("htmlConstruct."+key, objMap.params);
							}
					return str+= "</div>";
				}

			},
			blockCms : function(key){
				var str= 	`<span class="col-xs-12 badge comming-soon-badge">
								<div class='col-xs-12 padding-10 manageMenusCostum'>
								<button class="btn btn-primary custom-footer" data-path="${key}">Edit the ${key}</button>
						</div>`;
				return str;
			},
			menuConstruct : function(pathJson, params){
				var obj= jsonHelper.getValueByPath(costumizer.obj,pathJson);
				var str="";
				str+= 	"<div class='padding-10 manageMenusCostum col-xs-12'>";
				str+=		'<div class="sortable-list col-xs-12 no-padding" data-ref="'+pathJson+'.buttonList">';
								if(notNull(obj) && typeof obj.buttonList != "undefined"){
									$.each(obj.buttonList, function(e,v){
										str+=costumizer.htmlConstruct.views.buttons.view(pathJson+".buttonList", e, v, "draggable col-xs-12", true);
									});
								}
				str+=   	`</div>
						 	<div class='button-menu-costumizer button-show-list text-center'>
								<i class='fa fa-plus'></i>
							</div>	
						</div>`;
				return str;
			},
			footerConstruct : function(){
				setTimeout(function () {
						if (costum.htmlConstruct.footer.activated == false) {
							$(".footer-cms").html("")
							$(".block-footer").remove()
						}else{
							let required = {
								displayFooter : true
							}
							ajaxPost(
								null, 
								baseUrl+"/costum/blockcms/loadfootercms",
								required,
								function(data){
									if (data.footerCMS) {
										$(".footer-cms").html(data.html) 
										var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
										if($(".footer-cms").length > 0){
											$(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
										}
									}else{
										// localStorage.setItem("parentCmsIdForChild", "tpl-footer");
										cmsConstructor["blockParent"] = "tpl-footer"
										$("#modal-blockcms").addClass("open")
										$("#blocklist-search-text").parent().hide()
										$("#blocklist-search-text").val("Template footer")
										$("#blocklist-search-text").keyup()
									}
								},
								{async:true}
								);
						}
					},200)
			},
			buttons : {
				view : function(keyMenu, keyButton, value, classes, edit){
					// @ThemeParams.mainMenuButtons est une entrée qui hérite du json présent dans co2/config/buttons/main.json
					if( typeof themeParams.mainMenuButtons[keyButton] != "undefined"
							&& typeof themeParams.mainMenuButtons[keyButton].costumizer != "undefined"){
						var entryBtn=themeParams.mainMenuButtons[keyButton];
						var labelBtnTranslate=(typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
						var statusBtn="";
						if(typeof entryBtn.connected != "undefined"){
							if(entryBtn.connected === true)
								statusBtn="<span class='statusButtonMenu elipsis'>"+tradCms.forConnectedUser+"</span>";
							else
								statusBtn="<span class='statusButtonMenu elipsis'>"+tradCms.forDisconnectedUser+"</span>";	
						}
						if(typeof entryBtn.restricted != "undefined")
							statusBtn="<span class='statusButtonMenu elipsis'>"+tradCms.forAdministrator+"</span>";
						var classLabelStatus=(notEmpty(statusBtn)) ? "labelStatusMenuButton" : "labelMenuButton";
						var str="<div class='button-menu-costumizer "+classes+"' data-key-menu='"+keyMenu+"' data-key-button='"+keyButton+"'>"+
								"<div class='iconMenuContains'><i class='fa fa-"+entryBtn.costumizer.icon+"'></i></div>"+
								"<div class='"+classLabelStatus+"'>"+
									"<span class='titleButtonMenu elipsis'>"+labelBtnTranslate+"</span>"+
									statusBtn+
								"</div>";
							if(edit)
								str+=costumizer.htmlConstruct.views.buttons.editTools(keyMenu, keyButton, labelBtnTranslate);
							if(notEmpty(value)){
								var dataPath = "";
								var selectValue = value.buttonList;
								/*if (keyButton == "add" && notEmpty(costumizer.obj.typeObj)) { 
									dataPath = ""+keyMenu+"."+keyButton+"";
									selectValue = costumizer.obj.typeObj;
								} else {*/ 
									dataPath = ""+keyMenu+"."+keyButton+".buttonList";
								//}

								if (keyButton == "xsMenu" && edit && typeof jsonHelper.getValueByPath(costumizer.obj, dataPath) != "undefined") {
									if (notEmpty(jsonHelper.getValueByPath(costumizer.obj, dataPath))) {
										str += '<div class="xsMenuButtonCollapse">'+
											'<button type="button" data-toggle="collapse" data-target=".collaspe'+keyMenu.replaceAll(".", "")+'" aria-expanded="false" aria-controls="collaspe'+keyMenu.replaceAll(".", "")+'"><i class="fa fa-bars"></i></button>'+
										'</div>';
									}
								}
								str+='<div class="sortable-list col-xs-12 no-padding '+(keyButton == "dropdown" ? "sortable-dropdown" : "")+' '+(keyButton == "xsMenu" ? "xsMenuCollapse" : "")+' '+(keyButton == "xsMenu" && typeof jsonHelper.getValueByPath(costumizer.obj, dataPath) != "undefined" && notEmpty(jsonHelper.getValueByPath(costumizer.obj, dataPath)) ? "collapse collaspe"+keyMenu.replaceAll(".", "")+"" : "")+'" data-ref="'+dataPath+'">';
								if( typeof selectValue != "undefined" && notNull(selectValue)){
									$.each(selectValue,function(k, v){
										str+=costumizer.htmlConstruct.views.buttons.view(dataPath, k, v, "col-xs-12", true);
									});
								}
								if(keyButton == "app" && edit/* || keyButton == "add"*/){
									str+='<div class="col-xs-12 add-page-in-nav no-padding" data-path="'+dataPath+'">'+
											'<div class="select-navigation-page col-xs-10 no-padding">'+costumizer.htmlConstruct.views.buttons.form.selectPageMenu(selectValue, keyButton)+'</div>'+
											'<button class="btn btn-success add-navigation-menu-btn col-xs-2"><i class="fa fa-check"></i></button>'+
											'<button class="btn create-newpage costumizer-button-add"><i class="fa fa-plus"></i> '+tradCms.createNewPage+'</button>'+
										'</div>';
								}
								str+=`</div>`;
							}
						str+="</div>";
					}else{
						var str="<div class='button-menu-costumizer draggable "+classes+"' data-key-menu='"+keyMenu+"' data-key-button='"+keyButton+"'>"+
						// Si il y a des boutons indépendants créé via un costum on le génére ici avec la même map json que les btn présent co2/config/buttons/main.json
							 	"<span class='pull-left no-padding'>"+keyButton+"</span>"+
							 	costumizer.htmlConstruct.views.buttons.editTools(keyMenu, keyButton, "", true )+
								"<div class='sortable-list col-xs-12 no-padding sortable-subMenu' data-ref='"+keyMenu+"."+keyButton+".buttonList' data-key="+keyButton+" data-path="+keyMenu+">";
								if (typeof value.buttonList != "undefined") {
									$.each(value.buttonList ,function(k, v){
										str+=costumizer.htmlConstruct.views.buttons.view(keyMenu+"."+keyButton+".buttonList", k, v, "col-xs-12", true);
									});
								}
							str+="</div>"+
						"</div>";
					}
					return str;
				},
				editTools : function(kMenu, kButton, title, onlyDelete){
					if(notEmpty(onlyDelete)) { 
						var str='<div class="pull-right toolsButtonEdit no-padding">'+
							'<a href="javascript:;" class="badge bg-blue editMenuBtn noLabel" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
								'<i class="fa fa-pencil"></i>'+
							'</a>'+		
							'<a href="javascript:;" class="removeMenuBtn badge bg-red noLabel margin-left-5" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
								'<i class="fa fa-trash"></i>'+
							'</a>'+
						'</div>';
						/*if (kMenu.split(".")[kMenu.split(".").length - 1] == "add") {
							var str='<div class="pull-right toolsButtonEdit no-padding">'+
								'<a href="javascript:;" class="removeMenuBtn badge bg-white text-red margin-left-5" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
									'<i class="fa fa-trash"></i>'+
								'</a>'+
								'<a href="javascript:;" class="editMenuBtn badge bg-white" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'" data-title="'+title+'">'+
									'<i class="fa fa-pencil"></i>'+
								'</a>'+
							'</div>';
						}*/
					}
					else{
						var str='<div class="col-xs-12 toolsButtonEdit">'+
								'<a href="javascript:;" class="editMenuBtn badge bg-blue" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'" data-title="'+title+'">'+
									'<i class="fa fa-pencil"></i> '+trad.edit+
								'</a>'+
								'<a href="javascript:;" class="removeMenuBtn badge bg-red margin-left-5" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
									'<i class="fa fa-trash"></i> '+trad.delete+
								'</a>'+
							'</div>';
					}
					return str;
				},
				form : {
					init :  function(pathJson, keyButton) {
						var formOptions = {};
						//costumizer.htmlConstruct.btnNavigationTrue = [];
						var str = "";
						/*str += '<div class="title-edit-mockcostumizer.htmlConstruct.btnNavigationup-menu" style="display: flex; align-items: center; width: 100%; justify-content: center; padding: 5px; border-bottom: 1px solid #f5f5f5; margin-bottom: 5px">'+
								'<button class="btn btn-danger" id="btn-save-mockup-menu" data-path="'+pathJson+'.'+keyButton+'">'+tradCms.validate+'</button>'+
							'</div>';*/
						formOptions[keyButton] = JSON.parse(JSON.stringify(costumizer.htmlConstruct.formOpt));
						if (typeof costumizer.htmlConstruct.listInput[keyButton] != "undefined") {
							$.each(costumizer.htmlConstruct.listInput[keyButton], function(k, v) {
								if (v === false) {
									delete formOptions[keyButton][k];
								} else {
									formOptions[keyButton][k] = v;
								}
							})
						}
						str += costumizer.htmlConstruct.views.buttons.form.inputs(formOptions[keyButton], keyButton, pathJson);
						// if (typeof costumizer.htmlConstruct.btnNavigation === 'object' && costumizer.htmlConstruct.btnNavigation.length != 0) {
						// 	str += costumizer.htmlConstruct.views.buttons.form.btnNavigationInputs();
						// }
						return str;
					},
					allMenuSelected : function(menu) {
						if (typeof menu == "object" && typeof menu != "undefined") {
							$.each(menu, function(key, value) {
								if (typeof key == "string") {
									if (key.charAt(0) == "#")
										costumizer.htmlConstruct.btnNavigation.push(key);
								}
								if (typeof value == "object") {
									costumizer.htmlConstruct.views.buttons.form.allMenuSelected(value);
								}
							})
						}
					},
					/*btnNavigationInputs: function() {
						costumizer.htmlConstruct.btnNavigationTrue = costumizer.htmlConstruct.btnNavigation;
						let html = "";
						html += `
						<div class="panel panel-default" id="panel-navigation-content">
							<div class="panel-heading" role="tab" id="btn-navigation-heading">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-btn-navigation" aria-expanded="true" aria-controls="collapse-btn-navigation">
										<span  class="fa fa-bars" aria-hidden="true"></span>Ajouter ou supprimer un bouton de navigation
									</a>
								</h4>
							</div>
							<div id="collapse-btn-navigation" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="btn-navigation-heading">
								<div class="panel-body" id="inputs-navigation-btn">
									<div id="form-group-navigation-menu">`;
									Object.values(costumizer.htmlConstruct.btnNavigation).forEach(function (value){
					    html+=      	`<div class="form-group menu-item">
											<label>${value}</label>
											<button class="btn btn-danger" id="remove-navigation-menu-true" data-value="true" data-path="buttonList.${value}" data-key="${value}"><i class="fa fa-times"></i></button>
										</div>`;
									})
						html+=  	`</div>
									<div id="select-navigation-page"></div>
									<button class="btn btn-success" id="add-navigation-menu-btn">Ajouter</button>
								</div>
							</div>
						</div>`;
						costumizer.htmlConstruct.btnNavigation = [];
						return html;
					},*/
					selectPageMenu : function(menuList/*, keyButton*/) {
						costumizer.htmlConstruct.btnNavigation = [];
						let elementNotSelected = []
						costumizer.htmlConstruct.views.buttons.form.allMenuSelected(menuList);
						let menuNotSelected = Object.keys(costumizer.obj.app).filter((value) => {
							return !Object.values(costumizer.htmlConstruct.btnNavigation).find(all => (all === value));
						});
						defaultSelect=(menuNotSelected.length > 0) ? tradCms.selectPage : tradCms.allPagesAreSelected;
						/*if (keyButton == "add") {
							$.each(typeObj, function(k, v){
								if (v.add == false) {
									elementNotSelected.push(k);
								}
							})
							menuNotSelected = elementNotSelected	
							mylog.log("srghosr", menuNotSelected)
						defaultSelect=(menuNotSelected.length > 0) ? tradCms.selectAnElement : tradCms.allElementsAreSelected;
						}*/			
						
						$("#add-navigation-menu-btn").addClass("disabled");
						var html = `<div class="select-menu-page-form">
								<div class="form-group">
									<select class="form-control navigation-menu-select-form">
										<option value="" selected><italic>${defaultSelect}</italic></option>`;
										if(menuNotSelected.length){
											$.each(menuNotSelected, function(e, value) {
									html += `<option value="${value}">${value}</option>`;
										});
									}
								html += `
									</select>
								</div>
								</div>`;
						return html;
					},
					inputs : function(value, keyButton, pathJson, keyPath="", level=0, title="") {
						let html = "";
						$.each(value, function(k, v) {
							let valInput = "";
							if (typeof costumizer.htmlConstruct.buttonValue != "undefined") {
								if (typeof costumizer.htmlConstruct.buttonValue[k] != "undefined") {
									valInput = costumizer.htmlConstruct.buttonValue[k];
								} else {
									if (typeof jsonHelper.getValueByPath(costum, "appConfig") != "undefined") {
										if (typeof jsonHelper.getValueByPath(costum, `appConfig.${pathJson.slice(14)}.${keyButton}`) != "undefined") {
											let path = jsonHelper.getValueByPath(costum, `appConfig.${pathJson.slice(14)}.${keyButton}`);
											valInput = path[k];
										} else if (typeof jsonHelper.getValueByPath(costum, `appConfig.mainMenuButtons.${keyButton}`) != "undefined") {
											let path = jsonHelper.getValueByPath(costum, `appConfig.mainMenuButtons.${keyButton}`);
											valInput = path[k];
										}
									}
								}
							}
							if (v.type == "text") {
								html +=
								`<div class="form-group input-group-sm">
									<label>${v.label}</label>
									<input type="text" class="form-control" name="${k}" data-path="${pathJson}" data-key="${keyButton}" value="${(typeof valInput != "undefined")? valInput:""}">
								</div>`
							} else if (v.type == "select") {
								html += 
								`<div class="form-group input-group-sm">
									<label>${v.label}</label>
									<select class="form-control" name="${k}" data-path="${pathJson}" data-key="${keyButton}">`;
										$.each(v.options, function(kOptions, vOptions) {
								html +=		`<option value="${vOptions}" ${(typeof valInput != "undefined" && valInput === parseBool(vOptions)) ? "selected" : ""}>${vOptions}</option>`;
										})
								html +=
									`</select>
								</div>`;
							} else if (v.type == "selectFontAwesome") {
								html += 
								`<div class="form-group input-group-sm">
									<label>${v.label}</label>
									<select class="form-control" name="${k}" data-path="${pathJson}" data-key="${keyButton}">`;
										$.each(v.options, function(kOptions, vOptions) {
								html +=		`<option value="${vOptions}" ${(typeof valInput != "undefined" && valInput === vOptions) ? "selected" : ""}><span><i class="fa fa-${vOptions.trim()}"></i> ${vOptions}</span></option>`;
										})
								html +=
									`</select>
								</div>`;
							}
							/*if (k === "generatePage") {
								if (notNull(costumizer.htmlConstruct.buttonValue.buttonList) && costumizer.htmlConstruct.buttonValue.buttonList != "") {
									if (typeof costumizer.htmlConstruct.buttonValue.buttonList != "undefined") {
										costumizer.htmlConstruct.views.buttons.form.allMenuSelected(costumizer.htmlConstruct.buttonValue.buttonList);
									}
								}
								// html += costumizer.htmlConstruct.views.buttons.form.btnNavigationInputs();
								$(".add-new-menu-content").html("");
								$(`.add-new-menu-content.${pathJson.replaceAll(".", "")}`).append(costumizer.htmlConstruct.views.buttons.form.btnNavigationInputs());
							}*/
						})
						return html;
					}
				}
			}
			
		},
		events :{ 
			bind : function(){
				$(".showAllSitemapOptions").off().on("click", function(){
					if($(this).hasClass("toHide")){
						$(this).removeClass('toHide');
						$(".parentSitemapOptions.notActive").fadeOut(600);
						$(this).html("<i class='fa fa-caret-down'></i> Voir les maquettes de site web réalisables");
					}else{
						$(this).addClass('toHide');
						$(".parentSitemapOptions.notActive").fadeIn(600);
						$(this).html("<i class='fa fa-caret-up'></i>Cacher les maquettes de site web réalisables");
					}
				});
				costumizer.htmlConstruct.events.activateSection();
				//costumizer.htmlConstruct.events.addPageInNav();
				costumizer.htmlConstruct.events.draggable();
				costumizer.htmlConstruct.events.sortUpdate();
				costumizer.htmlConstruct.events.toolsButton();
			},
			toolsButton : function(){
				// Delete button in menu
				$(".toolsButtonEdit .editMenuBtn").off().on("click", function(){
					if(notNull($(this).data("construct")) && 
						typeof costumizer.htmlConstruct.actions.edit[$(this).data("construct")] == "function"){
						costumizer.htmlConstruct.actions.edit[$(this).data("construct")]($(this).data("key-menu"), $(this).data("key-button"), $(this).data("title"));
					}else if($(this).data("key-menu").indexOf("app.buttonList") > 0){
						costumizer.pages.actions.save($(this).data("key-button"),costumizer.obj.app[$(this).data("key-button")], {path :$(this).data("key-menu")});
					}else {
						costumizer.htmlConstruct.actions.edit.common($(this).data("key-menu"), $(this).data("key-button"), $(this).data("title"));
					}
				});
				// Edit config button in menu
				$(".toolsButtonEdit .removeMenuBtn").off().on("click", function(){
					costumizer.htmlConstruct.actions.delete($(this).data("key-menu"), $(this).data("key-button"));
					$(this).parents().eq(1).fadeOut().remove();

					var objDelete = jsonHelper.getValueByPath(costumizer.obj, $(this).data("key-menu"));
					if (typeof objDelete != "undefined") {
						if (!notEmpty(objDelete)) {
							let pathParent = "";
							let allKey = $(this).data("key-menu").split(".");
							let keyHastag = allKey[allKey.length - 2];
							if (keyHastag.charAt(0) == "#") {
								for (let index = 0; index < allKey.length - 1; index++) {
									if (index == 0)
										pathParent = allKey[index];
									else
									pathParent +="."+allKey[index];
								}
								jsonHelper.setValueByPath(costumizer.obj, pathParent, true);
							}
						}
						let menuDeleted = $(this).data("key-button");
						$(".navigation-menu-select-form").append('<option value='+menuDeleted+'>'+menuDeleted+'</option>')
					}
				});
				// Add new button in menu 
				$(".button-menu-costumizer.button-show-list").off().on("click", function(){
					costumizer.htmlConstruct.actions.add();
				});
				$(".add-navigation-menu-btn").off().on("click", function(e) {
					e.stopImmediatePropagation();
					var selectorInput=$(this).parent().find(".navigation-menu-select-form");
					var pageSelected = selectorInput.val();
					var parentPath = $(this).parent().data("path");
					//let splitParentPath = parentPath.split(".")
					//let valueInsert = true;	

					if (notEmpty(pageSelected)) {
						costumizer.htmlConstruct.actions.addPageInMenu(parentPath, pageSelected, $(this).parent());
						
						selectorInput.find("option[value='"+pageSelected+"']").remove();

							//$(this).parent().data("path");
							//costumizer.htmlConstruct.btnNavigationTrue.push(menuSelected);	
							//$("#navigation-menu-select-form").val("");
							//jsonHelper.setValueByPath(costumizer.htmlConstruct.buttonValue, keyPath, true );
					} 
				});
				$(".create-newpage").off().on("click",function(){
					htmlConstructLink={path :$(this).parent().data("path"), domAppend : $(this).parent()};
					costumizer.pages.actions.save(null,null, htmlConstructLink);
				});

				$(".custom-footer").off().on("click", function (){	
					$(".save-costumizer").click()
					costumizer.htmlConstruct.views.footerConstruct()
					
				})
				$(".save-costumizer").on("mouseup", function (e){
					e.stopImmediatePropagation()
					if (Object.keys(costumizer.actions.prepData())[0] == "htmlConstruct.footer")
						costumizer.htmlConstruct.views.footerConstruct()
				})
			
			},
			/*addPageInNav : function(){
				$(".add-navigation-menu-btn").off().on("click", function(e) {
					e.stopImmediatePropagation();
					costumizer.htmlConstruct.views.buttons.form.selectPageMenu(costumizer.obj.app);
				});
			},*/
			updateButtons : function(){
				$("#form-mockup-menu-edit .form-group input, #form-mockup-menu-edit .form-group select").on("change", function(e) {
					e.stopImmediatePropagation();
					var objBtn = jsonHelper.getValueByPath(costumizer.obj, $(this).data("path")+"."+$(this).data("key"));
					if (typeof objBtn == "boolean"){
						jsonHelper.setValueByPath(costumizer.obj, $(this).data("path")+"."+$(this).data("key"), {});
					}
					jsonHelper.setValueByPath(costumizer.obj, $(this).data("path")+"."+$(this).data("key")+"."+$(this).attr("name"), ($(this).val() == "true" || $(this).val() == "false") ? parseBool($(this).val()) : $(this).val());

					costumizer.htmlConstruct.actions.change($(this).data("path"));
				});

			},
			sortUpdate : function() {
				let inc=0;
			      $(".sortable-list").each(function(){
				        sortable('.sortable-list')[inc].addEventListener('sortupdate', function(e) {
							e.stopImmediatePropagation();
				        	if($(e.detail.item).hasClass("dragNewBtn")){
				            	/*
									tODO : transformer le btn en btn draggable 
									=> [X] vue à remplacer : costumizer.htmlConstruct.views.buttons.view("htmlConstruct.menuLeft.buttonList", e, v, "draggable col-xs-12", true);
									=> [X] bind les events sur le nouveau html
									=> [X] ajouter l'entrée themeParams.mainMenuButtons[this.dataKey] à l'objet costumizer.obj
									=> ouvrir la modal right en edition du bouton pour le configurer (la même chose que "edit" click sur les boutons déjà présent)

				            	*/
				            	let finalContainerPath=$(e.detail.destination.container).data("ref");
				            	let buttonKey=$(e.detail.item).data("key-button");
				            	let paramsButton=themeParams.mainMenuButtons[buttonKey];
				            	if(typeof paramsButton.buttonList != "undefined")
				            		paramsButton.buttonList={};
				            	jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, finalContainerPath,buttonKey, paramsButton, e.detail.destination.index);
				            	$(e.detail.item).replaceWith(costumizer.htmlConstruct.views.buttons.view(finalContainerPath,buttonKey, paramsButton, "draggable col-xs-12", true));
				            	costumizer.htmlConstruct.events.toolsButton();
				            	costumizer.htmlConstruct.events.dragDestroy();
								let entryBtn = themeParams.mainMenuButtons[buttonKey];
								let title = (typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
								costumizer.htmlConstruct.actions.edit.common(finalContainerPath, buttonKey, title);
				            	//$(".button-menu-costumizer[data-key-menu='"+finalContainerPath+"'][data-key-button='"+buttonKey+"'] .toolsButtonEdit .editMenuBtn").trigger("click");
				            }else{
					            let moveObj=jsonHelper.getValueByPath(costumizer.obj,$(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
								if($(e.detail.origin.container).data("ref") != $(e.detail.destination.container).data("ref")){
									if ($(e.detail.item).data("key-button").charAt(0) == "#") {
										var dataKey = $(e.detail.destination.container).data("key");
										var obj = jsonHelper.getValueByPath(costumizer.obj, $(e.detail.destination.container).data("path"));
										if (typeof obj != "undefined" && typeof dataKey != "undefined") {
											if (typeof obj[dataKey] != "undefined" && typeof obj[dataKey] != "object") {
												obj[dataKey] = {};
												obj[dataKey]["buttonList"] = {};
											}
										}
									}
									jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, $(e.detail.destination.container).data("ref"), $(e.detail.item).data("key-button"), moveObj, e.detail.destination.index);
				            		jsonHelper.deleteByPath(costumizer.obj, $(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
									let objOrigin = jsonHelper.getValueByPath(costumizer.obj, $(e.detail.origin.container).data("ref"));
									if (typeof objOrigin != "undefined") {
										if (!notEmpty(objOrigin)) {
											let keyOrigin = $(e.detail.origin.container).data("key");
											let objOriginChange = jsonHelper.getValueByPath(costumizer.obj, $(e.detail.origin.container).data("path"));
											if (typeof objOriginChange != "undefined") {
												objOriginChange[keyOrigin] = true;
											}
										}
									}
									var path = `${$(e.detail.destination.container).data("ref")}.${$(e.detail.item).data("key-button")}`;
									var paramsButton = jsonHelper.getValueByPath(costumizer.obj, path);
									$(e.detail.item).replaceWith(costumizer.htmlConstruct.views.buttons.view($(e.detail.destination.container).data("ref"), $(e.detail.item).data("key-button"), paramsButton, "draggable col-xs-12", true));
									costumizer.htmlConstruct.events.draggable();
									costumizer.htmlConstruct.events.sortUpdate();
									costumizer.htmlConstruct.events.toolsButton();
				           		}else{
				           			var inc=e.detail.destination.index;
				           			if(e.detail.origin.index < e.detail.destination.index && e.detail.destination.index > 0 && e.detail.origin.index > 0)
				           				inc--;
				           			jsonHelper.deleteByPath(costumizer.obj, $(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
									jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, $(e.detail.origin.container).data("ref"),$(e.detail.item).data("key-button"), moveObj, inc);
								}
							}
							// Todo Pousser un peu dans  le up jusqu'au container parent ex:
							// costumizer.pathToUp.htmlConstruct.header.menuTop o||r costumizer.pathToUp.htmlConstruct.menuLeft
				          	if(typeof $(e.detail.destination.container).data("ref") != "undefined" && notEmpty($(e.detail.destination.container).data("ref")))
				          	 	costumizer.htmlConstruct.actions.change($(e.detail.destination.container).data("ref"));
				           	if(typeof $(e.detail.origin.container).data("ref") != "undefined" && notEmpty($(e.detail.origin.container).data("ref")) && $(e.detail.origin.container).data("ref") != $(e.detail.destination.container).data("ref"))
				            	costumizer.htmlConstruct.actions.change($(e.detail.origin.container).data("ref"));
							$(".sortable-subMenu").removeClass("subMenuSortableContainer");
				        });
						sortable('.sortable-list')[inc].addEventListener('sortstart', function(e) {
							if ($(e.detail.item).data("key-button") == "app") {
								$(".xsMenuCollapse").addClass("subMenuSortableContainer");
							} else if ($(e.detail.item).data("key-button").charAt(0) == "#") {
								$(".sortable-subMenu").addClass("subMenuSortableContainer");
								$(e.detail.item).find('.sortable-subMenu').removeClass("subMenuSortableContainer");
							}
							if ($(".sortable-dropdown").length > 0)
								$(".sortable-dropdown").addClass("subMenuSortableContainer");
				        });
						sortable('.sortable-list')[inc].addEventListener('sortstop', function(e) {
							$(".sortable-list").removeClass("subMenuSortableContainer");
				        });
				        inc++;
			        });
			    //}
			},
			activateSection : function(){
				if(!$("[name='section-sitemap']").parent().hasClass("bootstrap-switch-container")){
					$("[name='section-sitemap']").off();
					$("[name='section-sitemap']").bootstrapSwitch();
					$("[name='section-sitemap']").on("switchChange.bootstrapSwitch", function (event, state) {
						mylog.log("state = "+state );
						pathInJson=$(this).data("type");
						if (state == true) {
							$(this).val(1);
							if(typeof jsonHelper.getValueByPath(costumizer.obj, "htmlConstruct."+pathInJson) == "undefined"){
								valueToInsert=jsonHelper.getValueByPath(htmlConstructParams, pathInJson);
								jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathInJson, valueToInsert);
							}
							jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathInJson+".activated", true);
							$(this).parents().eq(3).find(".manageMenusCostum").fadeIn(500);
						} else {
							jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathInJson+".activated", false);
							$(this).val(0);
							$(this).parents().eq(3).find(".manageMenusCostum").fadeOut(500);
						}
						costumizer.htmlConstruct.actions.change("htmlConstruct."+pathInJson);		
					});
					$("[name='section-sitemap']").each(function(){
						if($.inArray($(this).val(),["true","1"]) >=0){
							$(this).bootstrapSwitch('state', true, true);
						}else{
							$(this).parents().eq(3).find(".manageMenusCostum").hide();
						}
					});
				}
				
			},
			dragDestroy : function(){


				//sortable(".sortable-list" , "destroy" );
				costumizer.htmlConstruct.events.draggable();	
			},
			draggable : function(){
				/*
					TODO Pouvoir editer bouger l'ensemble des boutons dans les sections des menus mais aussi entre section
					[ ] Bien finir la partie js simple de mouvement des boites
					[ ] A la fin du drag, mettre à jour l'entrée concerné 
						[ ] dans le bouton il y a les valeurs:
							data-key-menu (ex : costum.htmlConstruct.header.menuTop.left.buttonList)
							data-key-bouton (ex : app, userProfil, etc)
						[ ] Le supprime
				*/
				sortable(".sortable-list", {
		            placeholderClass: "ph-class",
		            hoverClass: "hover",
		            acceptFrom : ".sortable-list, .add-to-list-sort"
		        });
			}
		},
		actions: {
			isActivated: function(path){
				// La variable htmlConstruct.menuLeft.activated permet de désactiver ou activer le menu sans effacer l'ensemble de l'objet 
				menuVal=jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, path);
				if(notEmpty(menuVal) 
					&& (typeof menuVal.activated == "undefined" || menuVal.activated===true))
					return true;
				else
					return false;
			},
			init : function(){
				// Variable utilisé pour le backup au cas où l'utilisateur effectue des changemets et veut restaurer l'ancien plutot que de sauvegarder  
				//costumizer.htmlConstruct.copy=$.extend({},costumizer.obj);
			},
			cancel : function(/*callback*/){

				if(costumizer.liveChange){
					var pathValueToUpdate={};
					$.each(costumizer.actions.pathToValue, function(e, v){
						if(v.indexOf("htmlConstruct") >= 0){
							var valueToUp=jsonHelper.getValueByPath(costum,v);
							if (typeof valueToUp != "undefined") {
								if(typeof valueToUp.buttonList != "undefined" && !notEmpty(valueToUp.buttonList))
								valueToUp="$unset";
							}
							pathValueToUpdate[v]=valueToUp;
						}
					});
					costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costum.htmlConstruct);
					//if(typeof callback == "function") callback();
				}
			},
			change : function(path){
				costumizer.actions.change(path);
				if(costumizer.liveChange){
					var pathValueToUpdate={};
					$.each(costumizer.actions.pathToValue, function(e, v){
						var valueToUp=jsonHelper.getValueByPath(costumizer.obj,v);
						if (typeof valueToUp != "undefined") {
							if(typeof valueToUp.buttonList != "undefined" && !notEmpty(valueToUp.buttonList))
							valueToUp="$unset";
						}
						pathValueToUpdate[v]=valueToUp;
					});
					costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
				}

			},
			prepData : function(data){
				$.each(data, function(k, v){
					if (typeof v != "undefined") {
						if(typeof v.buttonList != "undefined" && !notEmpty(v.buttonList))
						data[k]="$unset";
					}
				});
				return data;
			},
			replacePage: function(parentPath, oldKey, newkey){
				$(".button-menu-costumizer[data-key-menu='"+parentPath+"'][data-key-button='"+oldKey+"']").replaceWith(costumizer.htmlConstruct.views.buttons.view(parentPath, newkey, true, "col-xs-12", true));
				costumizer.htmlConstruct.events.bind();
			
			},
			addPageInMenu: function(parentPath, pageSelected, $domAppendBefore){
				posPage=(notEmpty(jsonHelper.getValueByPath(costumizer.obj, parentPath))) ? Object.keys(jsonHelper.getValueByPath(costumizer.obj, parentPath)).length :0;
				jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, parentPath, pageSelected, true, posPage);
				costumizer.htmlConstruct.actions.change(parentPath);
				$domAppendBefore.before(costumizer.htmlConstruct.views.buttons.view(parentPath, pageSelected, true, "col-xs-12", true));
				costumizer.htmlConstruct.events.bind();
			},
			findIfKeyPageExist: function(obj, element, path = "", paths = []) {
				for (const key in obj) {
					if (obj.hasOwnProperty(key)) {
						const currentPath = path ? `${path}.${key}` : key;
						if (key === element) {
							const splitPath = currentPath.split(".");
							splitPath.pop();
							paths.push(splitPath.join("."));
						}
						if (typeof obj[key] === "object") {
							costumizer.htmlConstruct.actions.findIfKeyPageExist(obj[key], element, currentPath, paths);
						}
					}
				}
				return paths;
			},
			renameKeyPage: function (obj, oldKey, newKey) {
				var res = {};
				Object.keys(obj).forEach(function(key){
					var value = obj[key],
						key = (key==oldKey) ? newKey : key;
					res[key] = value;
				})
				return res;
			},
			editPageInMenu: function(oldKey, newKey, paths) {
				if (notEmpty(paths)) {
					var pathValueToUpdate = {};
					Object.values(paths).forEach(function(value) {
						if (oldKey != newKey) {
							var obj = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, value);
							var newObj = costumizer.htmlConstruct.actions.renameKeyPage(obj, oldKey, newKey);
							jsonHelper.setValueByPath(costumizer.obj.htmlConstruct, value, newObj);

						}
						var valueToUp = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, value);
						pathValueToUpdate["htmlConstruct."+value] = valueToUp;
					})
					costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
				}
			},
			refreshmenu : function(pathToUp, obj){
				var reloadMenu=[];
				$.each(pathToUp, function(e, v){
					if(e.indexOf("htmlConstruct") >= 0){
						//if(e.indexOf(
						var pathJson="";
						var alreadyRefresh=false;
						$.each(["menuLeft","header.menuTop.left","header.menuTop.center", "header.menuTop.right", "menuRight", "menuBottom", "subMenu","header.menuTop"], function(k,menu){
							if(e.indexOf(menu) >= 0){
								pathJson=menu;
								if($.inArray(menu, reloadMenu) >= 0)
									alreadyRefresh=true;
									reloadMenu.push(menu);
							}
						});
						if(!alreadyRefresh){
							var menuSelect =jsonHelper.getValueByPath(themeParams, pathJson+".id");
							var domMenu = typeof  menuSelect == "undefined" ? pathJson : menuSelect;

							$(domMenu).addClass("sp-is-loading");
							if((typeof v =="string" && v==='$unset') || (typeof v.activated != "undefined" && !v.activated)){
								$("#"+domMenu).hide();
								coInterface.initHtmlPosition();
							}
							else{
								var paramsViewMenu=jsonHelper.getValueByPath(obj, pathJson);
								ajaxPost(
									null, 
									baseUrl+"/"+moduleId+"/cms/refreshmenu", 
									{
										menuConfig:paramsViewMenu,
										path: pathJson,
										pagesConfig : costum.appConfig.pages,
										connectedMode : costumizer.connectedMode
									},
									function(data){
										if($('#'+domMenu).length <= 0){
											$("body").append(data);
										}else{
											if(!$('#'+domMenu).is(":visible"))
												$('#'+domMenu).show();
											$('#'+domMenu).replaceWith(data);
										}
										
										if(typeof costum.typeObj != "undefined")
											costum.initTypeObj(costum.typeObj);
											themeObj.init();

										coInterface.initHtmlPosition();
										coInterface.bindLBHLinks();
										costumizer.events.editMenu();
									},
									null, 
									"html"
								);
							}
						}
					}
				});
			},
			callback : function(data){
				costumizer.htmlConstruct.actions.refreshmenu(data.params, costum.htmlConstruct);
			},
			edit : {
				common : function(pathJson, keyButton, title){
					// alert("pathJson de l'entrée dans costum:"+pathJson+"// Clé du bouton :"+keyButton+" Travail à réaliser décris dans @costumizer.htmlConstruct.actions.edit.common");
					
					/* Todo 
						[ ] récupérer les valeurs du bouton à éditer 
							Liste non exhaustive des params à mettre en place 
							[] Name 
							[] icon 
							[] href - Pages ? 
							[] visible non visible en mode xs -sm - md
							[] Autres class style etc etc 
						[ ] Afficher le formulaire 
					*/
					var value= jsonHelper.getValueByPath(costumizer.obj,pathJson+"."+keyButton);

					costumizer.htmlConstruct.buttonValue=(typeof value == "object") ? jQuery.extend(true,{}, value) : value;
					if(typeof costumizer.htmlConstruct.buttonValue != "object" && 
						(costumizer.htmlConstruct.buttonValue===true || costumizer.htmlConstruct.buttonValue==="true") && 
						 typeof themeParams.mainMenuButtons[keyButton])
						costumizer.htmlConstruct.buttonValue=jQuery.extend(true,{},themeParams.mainMenuButtons[keyButton]);
					var subView=`<div id="form-mockup-menu-edit" style="padding:10px;">
							${costumizer.htmlConstruct.views.buttons.form.init(pathJson, keyButton)}
						</div>`;

					var configRight = {
						width:300,
						header:{
							title:`Edition - `+title,
							position:"top"
						}
					};
					if(!costumizer.liveChange)
						configRight.distanceToRight = 0;

					costumizer.actions.openRightSubPanel(subView, configRight)

					costumizer.htmlConstruct.events.updateButtons();
					//costumizer.htmlConstruct.clone = JSON.parse(JSON.stringify(costumizer.obj.htmlConstruct));
				}
			},
			add : function(){
				var str="<div class='col-xs-12 add-to-list-sort'>";
					$.each(themeParams.mainMenuButtons, function(e, v){
						if(typeof v.costumizer != "undefined")
							str+=costumizer.htmlConstruct.views.buttons.view("", e, v, "col-xs-6 dragNewBtn");					
					});
				str+="</div>";
				/* var configRight = {width : 150, title : tradCms.dragIt, subRight : false};
				if(costumizer.liveChange) configRight.subRight=true; */
					
				//costumizer.actions.openRightPanel(str, configRight);
				var configRight = {
					width:150,
					header:{
						title:tradCms.dragElement,
						position:"left"
					}
				};
				if(!costumizer.liveChange)
					configRight.distanceToRight = 0;
				costumizer.actions.openRightSubPanel(str, configRight)
					
			//	$(domSub+" .contains-view-admin").html(str);
				 sortable(".add-to-list-sort", {
						copy:true
						//appendTo : ".column-menu-costumizer"
						//connectToSortable : ".column-menu-costumizer"
					});
				 sortable(".sortable-list", {
			            placeholderClass: "ph-class",
			            hoverClass: "hover",
			            acceptFrom : ".sortable-list, .add-to-list-sort"
			        });
				//costumizer.htmlConstruct.events.draggable();
				
			},
			delete : function(pathJson, keyButton, change){
				// Todo Rien du tout juste à la sauvergarde 
				// jsonHelper.deleteByPath(costumizer.obj, pathJson+"."+keyButton);
				//if()
				// costumizer.htmlConstruct.actions.change(pathJson);
				/*mylog.log("sogi", pathJson);
				if (pathJson.split(".")[pathJson.split(".").length - 1] == "add"){ 
					costumizer.typeObj.actions.change(pathJson+".delete."+keyButton);
				}
				else { */
					jsonHelper.deleteByPath(costumizer.obj, pathJson+"."+keyButton);
					costumizer.htmlConstruct.actions.change(pathJson);
				//}
			}
		}
	},
	medias : {
		views : {
			initAjax : function(dom){
				mylog.log("pageProfil.views.gallery");
				var url = "gallery/index/type/cms/docType/image/contentKey/slider/source/"+costum.contextSlug;
				ajaxPost(dom, baseUrl+'/'+moduleId+'/'+url, 
					{initGallery:true},
					function(){
						$("#breadcrumGallery").hide();
						$(".dropdown-add-file .dropdown-menu, .btn-add-folder, .dropdown-collection.btn-move-collection").remove();
						$(".dropdown-add-file .show-nav-add").attr("href", "javascript:dyFObj.openForm('addPhoto')");
					});

			}
		}
	},
	design : {
		cssClone: {},
		activeObjCss : {},
		activePathDesign : "",
		fontUploadFiles: {},
		fontUploadExist: false,
		formOptions : {
			container : {
				inputs: [
					"background","margin","padding","display","textAlign","columnWidth","hideOnDesktop","hideOnTablet","hideOnMobil"
				]
			},
			button : {
				inputs: [
					"background","color","fontSize","fontWeight","padding","borderRadius","border","textTransform",
					{
						type: "section",
						options: {
							name: "hover",
							label: "hover",
							inputs: ["background","color","border"]
						}
					}
				]
			}
		},
		cssGeneralMap: {
			loader : {
				name :tradCms.loader,
				icon : 'spinner',
					inputs:[
						"background",
						{
							type:"section",
							options:{
								name:"ring1",
								label: tradCms.color+' 1',
								inputs:["color","borderWidth","height","width","top","left"]
							}
						},
						{
							type:"section",
							options:{
								name:"ring2",
								label: tradCms.color+' 2',
								inputs:["color","borderWidth","height","width","top","left"]
							}
						}
					]
			},
			progress : {
				name :tradCms.progress,
				icon : 'tasks',
				inputs:[
					{
						type:"section",
						options:{
							name:"bar",
							label:tradCms.bar,
							inputs:["background"]
						}
					},
					{
						type:"section",
						options:{
							name:"value",
							label:tradCms.value,
							inputs:["background"]
						}
					}
				]
			},
			menuTop : {
				name : tradCms.menuTop,
				icon : 'outdent',

				inputs: [
					"background", "padding", "boxShadow","hideOnDesktop","hideOnTablet","hideOnMobil"
				]
			},
			menuLeft : {
				name: tradCms.menuLeft,
				icon: 'outdent',
				inputs: [
					"background",
					"boxShadow",
					"marginTop",
				]
			},
			color : {
				name :tradCms.color,
				icon : 'paint-brush',
				inputs: [
					{
						type: "inputMultiple",
						options: {
							name: "multiColor",
							inputs: [
								[
									{
										type: "select",
										options: {
											label: tradCms.type,
											name: "type",
											options: [
												"black","blue","brown","lightblue","lightpurple","darkblue","green","orange","red","yellow","yellow-k","purple","azure","pink","phink","dark","green-k","red-k","blue-k","nightblue","turq","green-poi","bg-color","label-color","border-color"
											]
										}
									},
									"color"
								]
							]
						}
					}
				]
			},
			font: {
				name : tradCms.font,
				icon : "font",
				inputs : [
					{
						type: "selectFont",
						options: {
							name: "fontSelect",
							label: tradCms.selectFont
						}
					},
					{
						type: "inputFileFont",
						options: {
							name: "uploadFont",
							label: tradCms.fontUpload,
							file: null,
							accept: [".ttf"]
						}
					}
				]
			}
		},
		views : {
			init : function(){
				$("#modal-content-costumizer-administration").fadeOut(1000);
				$(".cmsbuilder-right-content").addClass("active")
				Object.keys(costumizer.design.cssGeneralMap).forEach(function(key) {
					costumizer.design.activeObjCss = {};
					if (key == "menuLeft") {
						if (!costumizer.obj.css.hasOwnProperty(key)) {
							if (!costumizer.obj.htmlConstruct.hasOwnProperty(key)) {
								delete costumizer.design.cssGeneralMap[key];
							}
						}
					}
				})
				return costumizer.design.views.css();
			},
			css : function () {
				var objOptions = (notEmpty(costumizer.design.activeObjCss)) ? costumizer.design.activeObjCss : costumizer.design.cssGeneralMap ;
				var $form = $(`
					<form method="post" action="" enctype="multipart/form-data" id="costum-design">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>	
					</form>
				`)

				$.each(objOptions, function(e, v){
					$form.find("#accordion").append(`
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading${e}">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse${e}" aria-expanded="true" aria-controls="collapse${e}">
										<span  class="fa fa-${v.icon}" aria-hidden="true"></span>${v.name}
									</a>
								</h4>
							</div>
							<div id="collapse${e}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${e}">
								<div class="panel-body" id="inputs-${e}">
									<div class="input-wrapper"></div>
								</div>
							</div>
						</div>
					`)
				});
				return $form;
				
			},
			loaderView : function() {
				var objLoader = costumizer.obj.css.loader;
				let bgLoader = objLoader.background; let ring1 = objLoader.ring1; let ring2 = objLoader.ring2;
				var str =
					'<a href="javascript:;" class="loader-costumizer" data-dom="#modal-content-costumizer-administration" data-title="Loader" data-link="loader"><div class="col-xs-12 text-center loader-contain" style="background-color: '+bgLoader+';"></div></a>';

				var objProgress = costumizer.obj.css.progress;
				let bar = objProgress.bar;
				let value = objProgress.value;
				var progress =
				'<div class="col-xs-12" style="padding: 20px 0">'+
					'<div class="progress" style="background-color: '+bar.background+';">'+
						'<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%; background-color: '+value.background+'">'+
							'<span class="sr-only">60% Complete</span>'+
							'<span class="progress-type">Progress</span>'+
						'</div>'+
					'</div>'+
				'</div>';

				$( "#collapseloader .panel-body" ).prepend(str);
				$( "#collapseprogress .panel-body" ).prepend(progress);
				$('style#cssLoader').html("");
				if (typeof(costumizer.obj.css.loader.loaderUrl) == "undefined") {
					$(".loader-contain").html(costumizer.loader.views.typeLoader("loading_modal"));
				}else {
					$(".loader-contain").html(costumizer.loader.views.typeLoader(costumizer.obj.css.loader.loaderUrl));
				}
				$(".loader-costumizer").off().on("click", function(e){
					e.stopPropagation()
					$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")

					var params = {
						space:$(this).data("link"),
						title:$(this).data("title"),
						showSaveButton:typeof $(this).data("save-button") == "undefined"
					}
					if(typeof $(this).data("dom") != "undefined")
						params.domTarget = $(this).data("dom")

					costumizer.actions.openCostumizer(params)
				});
			},
		},
		events : {
			bind : function() {	
				if(costumizer.liveChange){
					$('#costum-design .panel-collapse.collapse').addClass("in");
					setTimeout(function () {
						$('#costum-design .section-buttonList .panel-collapse.collapse').addClass("in");
						$.each($(".switch input[type='checkbox']"), function(){
							if($(this).val()=="on"){
								$(this).prop('checked', true);
							}
						});
					},200)
					$('#costum-design .input-section').hide();
					$(`#costum-design .input-section.section-${costumizer.design.activePathDesign.pop()}`).show();
				}
				costumizer.design.costumCssClone = JSON.parse(JSON.stringify(costumizer.obj.css));
				costumizer.design.events.getDataFont();
				$(".sp-color-picker").spectrum({
					type: "text",
					showInput: "true",
					showInitial: "true"
				});
			},
			generateObj: function(ObjPath) {
				var objSource = jsonHelper.getValueByPath(costumizer.obj,ObjPath);
				costumizer.design.activePathDesign = ObjPath.split('.');
				var cssMenuPath = costumizer.design.activePathDesign.pop();

				var objCss = {};
				objCss[cssMenuPath] = {
					name : tradCms.container,
					icon : 'outdent',
					inputs:	JSON.parse(JSON.stringify(costumizer.design.formOptions.container.inputs))
				}
				if (typeof objSource != "undefined") {
					$.each(objSource, function (k, v) {
						if (typeof v == 'object') {
							if (k == "buttonList") {
								$.each(v, function (key, val) {
									if ((typeof val == 'object' && exists(val["buttonList"])) ) {
										var entryBtn = themeParams.mainMenuButtons[key];
										var labelBtnTranslate = (typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
										var iconBtn = "";
										if (key == "xsMenu" || key == "dropdown" || "toolbarAdds" ){
											iconBtn = {
												type: "section",
												options: {
													name: "icon",
													label: "icon",
													inputs: ["fontSize","color","padding"]
												}
											}
										}

										objCss[cssMenuPath]["inputs"].push({
											type: "section",
											options: {
												name: key,
												label: labelBtnTranslate,
												inputs: [
													"padding",
													{
														type: "section",
														options: {
															name: k,
															label: k,
															category : "subSection",
															inputs: costumizer.design.formOptions.button.inputs
														}
													},
													iconBtn
												]
											}
										});


									} else if (typeof val == 'object' || typeof val) {
										var entryBtn = themeParams.mainMenuButtons[key];
										var labelBtnTranslate = "";
										var badgeBtn = "";
										if (key == "notifications" || key == "chat" ){
											badgeBtn = {
												type: "section",
												options: {
													name: "badge",
													label: "badge",
													inputs: ["background","fontSize","color","padding","border"]
												}
											}
										}
										if (typeof entryBtn.costumizer != "undefined") {
											labelBtnTranslate = (typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
										}else {
											labelBtnTranslate = entryBtn.label;
										}

										if (key == "logo" || key == "searchBar" || key == "userProfil") {
											objCss[cssMenuPath]["inputs"].push({
												type: "section",
												options: {
													name: key,
													label: labelBtnTranslate,
													inputs: ["background", "padding", "borderRadius", "border"]
												}
											});
										} else {
											objCss[cssMenuPath]["inputs"].push({
												type: "section",
												options: {
													name: key,
													label: labelBtnTranslate,
													inputs: [
														"background","color","fontSize","fontWeight","padding","borderRadius","border","textTransform",badgeBtn
													]
												}
											});
										}
									}
								});
							}
						}
					})
				}
				return objCss;

			},
			addFont: function () {
				var fd = new FormData();
				var files = costumizer.design.fontUploadFiles;
				if (files != {}) {
					fd.append('qquuid','fsdfsdf-yuiyiu-khfkjsdf-dfsd');
					fd.append('qqfilename',files.name);
					fd.append('qqtotalfilesize',files.size);
					fd.append('qqfile',files);
					fd.append('costumSlug', costumizer.obj.contextSlug);
					fd.append('costumEditMode', costum.editMode);
					ajaxPost(
						null,
						baseUrl+'/co2/document/upload-save/dir/communecter/folder/'+costumizer.obj.contextType+'/ownerId/'+costumizer.obj.contextId+'/input/qqfile/docType/file/subKey/costumFont',
						fd,
						function(data){
							if(data.result){
								toastr.success(data.msg);
							}else{
								toastr.error(data.msg);
							}
						},
						null,
						null,
						{
							contentType: false,
							processData: false
						}
					);
				} else {
					toastr.error("Something went wrong !!");
				}
			},
			getDataFont: function () {
				var urlToSend = baseUrl+"/"+moduleId+"/cms/getlistfile";
				var params = {
					costumId : costum.contextId,
					costumType : costum.contextType,
					"doctype" : "file",
					"subKey" : "costumFont"
				}
				ajaxPost(
					null,
					urlToSend,
					params,
					function(data){
						if (data && data.result.length != 0) {
							$.each(data.result, function(k,v) {
								$('#inputs-font input[name="uploadFont"]').val(v.name);
								$("#add-font-upload").hide();
								$("#delete-font-upload").show();
								$("#delete-font-upload").attr("data-id", v._id.$id);
								costumizer.design.fontUploadExist = true;
							});
						} else {
							$("#add-font-upload").show();
							$("#delete-font-upload").hide();
							costumizer.design.fontUploadExist = false;
						}
					}
				);
			},
			deleteFontUpload: function() {
				$("#inputs-font .input-wrapper #delete-font-upload").off().on("click", function(e) {
					e.stopImmediatePropagation();
					bootbox.confirm({
						message: trad.areyousuretodelete,
						buttons: {
							confirm: {
								label: trad.yes,
								className: 'btn-success'
							},
							cancel: {
								label: trad.no,
								className: 'btn-danger'
							}
						},
						callback: function(response) {
							if (response) {
								if (typeof $("#delete-font-upload").attr("data-id") != "undefined") {
									var id = $("#delete-font-upload").attr("data-id");
									var urlToSend = baseUrl+"/co2/document/deletedocumentbyid/slug/"+costumizer.obj.contextSlug+"/id/"+id;
									ajaxPost(
										null,
										urlToSend,
										null,
										function(data){
											if ( data && data.result ) {
												$('#inputs-font input[name="uploadFont"]').val('');
												$("#add-font-upload").show();
												$("#delete-font-upload").hide();
												valueFont = { isFontExist: false };
												uploadedFont = "";
												jsonHelper.setValueByPath(costumizer.obj, "css.font", valueFont);
												fontUpdate = {};
												fontUpdate.id = costum.contextId;
												fontUpdate.collection = costum.contextType;
												fontUpdate.path = "costum.css.font";
												fontUpdate.value = valueFont;
												dataHelper.path2Value(fontUpdate, function(params){
													toastr.success(tradCms.elementDeleted);
													costum.css.font = jQuery.extend(true, {}, costumizer.obj.css.font);
													costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
													costumizer.design.fontUploadExist = false;
												})
											} else {
												toastr.error(tradCms.somethingWrong);
											}
										}
									);
								}
							}
						}
					})
				});
			},
			chooseFontForm: function() {
				$('.btn-upload-false').off().on('click',  function() {
					costumizer.design.actions.useSelectFont();
					if ($('#inputs-font select[name="fontSelect"]').val() != "") {
						costumizer.obj.css['font'] = {};
						valueFont = { url: $('#inputs-font select[name="fontSelect"]').val(), useUploader: false,  isFontExist: true }
						costumizer.design.actions.change("css.font", valueFont);
					}
				});
				$('.btn-upload-true').off().on('click',  function() {
					costumizer.design.actions.useUploadFont();
					if ($('#inputs-font input[name="uploadFont"]').val() != "") {
						costumizer.obj.css['font'] = {};
						valueFont = { url: "", useUploader: true,  isFontExist: true }
						costumizer.design.actions.change("css.font", valueFont);
					}
				});
			},
			/*onChangeCssInput:function(name, value, payload){
				if ($('#collapseloader').hasClass('in')) {
					costumizer.design.actions.customizeLoader();
				} else if($('#collapseprogress').hasClass('in')) {
					costumizer.design.actions.customizeProgress();
				} else if (!$(`#inputs-${key} input[name="boxShadow"]`).hasClass('collapsed')) {
					costumizer.design.actions.shadowgenerator(key);
				}
				var path = '';
				if (typeof payload != "undefined" && notEmpty(payload)) {
					if (typeof payload.path != "undefined")
						path=payload.path;
					if(typeof payload.sectionPath != "undefined")
						path += "."+payload.sectionPath;
				}
				var valueToSet={};
				if (key == "color") {
					Object.values(value).forEach(function(value) {
						valueToSet[value.type] = value.color;
					})
				} else if (key == "font") {
					if (name == "fontSelect" && typeof value == "string" && value != "") {
						valueToSet = { url: value, useUploader: false,  isFontExist: true }
					} else if (name == "font" && typeof value == 'object') {
						valueToSet = { url: "", useUploader: true,  isFontExist: true }
						costumizer.design.fontUploadFiles = value;
						$('#inputs-font input[name="font"]').val(value.name);
					} else {
						valueToSet = { isFontExist: false };
					}
				} else {
					valueToSet=value;
					path+="."+name;
				}
				delete costumizer.obj.css['color']['multiColor'];
				if(notEmpty(path) && notEmpty(valueToSet))
					costumizer.design.actions.change(path, valueToSet);
			} */
		},
		actions : {
			init : function(){
				var objOptions = (notEmpty(costumizer.design.activeObjCss)) ? costumizer.design.activeObjCss : costumizer.design.cssGeneralMap ;
				var defaultValues = {};

				Object.keys(objOptions).forEach(function(key) {
					if (key === "left" || key === "right" || key === "center") {
						//cssHelpers.form.formatData(path,defaultValues);
						if (typeof costumizer.obj.css.menuTop[key] != "undefined") {
							defaultValues = JSON.parse(JSON.stringify(costumizer.obj.css.menuTop[key]));
						} else {
							mylog.log("objOptions",objOptions);
							//delete code default valu old costum
						}
					} else {
						defaultValues = costumizer.obj.css[key];
						mylog.log("costumizer.obj.css[key]",costumizer.obj.css[key]);
						if(key === "color") {
							var multiColor = Object.keys(defaultValues).map(function(key){
								return {
									type:key,
									color:defaultValues[key]
								}
							})
							defaultValues.multiColor = multiColor
						} else if (key === "font") {
							if (defaultValues) {
								if(defaultValues.url != "") {
									defaultValues.fontSelect = defaultValues.url;
								}
							}
						}
					}
					var savePath = (key == "left" || key == "right" || key == "center") ? `css.menuTop.${key}` : `css.${key}`

					// call coInput
					var container = `#inputs-${key} .input-wrapper`;
					var inputs = objOptions[key].inputs.map(function(input) {
						if (input.options?.name == "fontSelect") {
							var fontOptions = Object.keys(fontObj).map(function(value) {
								return {
									value: value,
									label: fontObj[value]
								}
							})
							input.options.options = fontOptions;
						}
						return input;
					});
					var payload = {
						path: savePath,
					};
					var onchange = function(path, valueToSet, name, payload, value){
						if ($('#collapseloader').hasClass('in')) {
							costumizer.design.actions.customizeLoader();
						} else if($('#collapseprogress').hasClass('in')) {
							costumizer.design.actions.customizeProgress();
						}
						if (key == "color") {
							valueToSet = {};
                            Object.values(value).forEach(function(value) {
                                valueToSet[value.type] = value.color;
                            })
                        } else if (key == "font") {
                            if (name == "fontSelect" && typeof value == "string" && value != "") {
                                valueToSet = { url: value, useUploader: false,  isFontExist: true }
                            } else if (name == "uploadFont" && typeof value == 'object') {
								var index = value.name.lastIndexOf(".");
								var extension = value.name.substring(index + 1);
								if (extension != "ttf") {
									toastr.error(tradCms.fontTypeNotAccepted);
									$('#inputs-font input[name="uploadFont"]').val("");
								} else {
									valueToSet = { url: "", useUploader: true,  isFontExist: true }
									costumizer.design.fontUploadFiles = value;
									$('#inputs-font input[name="uploadFont"]').val(value.name);
								}
                            } else {
                                valueToSet = { isFontExist: false };
                            }
                        }
						/* else if (!$(`#inputs-${key} input[name="boxShadow"]`).hasClass('collapsed')) {
							costumizer.design.actions.shadowgenerator(key);
							alert("onChange");
						}*/
						 if(notEmpty(path) && notEmptyAllType(valueToSet))
                            costumizer.design.actions.change(path, valueToSet,name);
					};

					cssHelpers.form.launch(container,inputs,payload,defaultValues,{},onchange);
				})
				costumizer.design.actions.checkFontsExist();
				costumizer.design.views.loaderView();
				costumizer.design.events.deleteFontUpload();
				costumizer.design.events.chooseFontForm();
			},
			change : function(path, value,name){
				//mylog.log("pathnamevalue",path, value,name);
				if (path === "css.font") {
					jsonHelper.setValueByPath(costumizer.obj, path, value);
					costumizer.actions.change(path);
					costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");	
				} else if ($.inArray(name, ["hideOnDesktop", "hideOnTablet", "hideOnMobil","columnWidth"])>= 0){
					jsonHelper.setValueByPath(costumizer.obj, path, value);
					costumizer.actions.change(path);
					var objVal = jsonHelper.getValueByPath(costumizer.obj, path.replace("."+name, ''));
					costum.addClassSection(objVal,path.replace("."+name, ''));
				}else {
					if (typeof value == "object") {
						if (path == "css.color") {
							jsonHelper.setValueByPath(costumizer.obj, path, value);
							costumizer.actions.change(path);
						} else {
							$.each(value, function(keyChange, valueChange) {
								jsonHelper.setValueByPath(costumizer.obj, path+"."+keyChange, valueChange);
								costumizer.actions.change(path+"."+keyChange);
								cssHelpers.render.addStyleUI(path+"."+keyChange, valueChange, "cosDyn");
							})	
						}
					} else {
						jsonHelper.setValueByPath(costumizer.obj, path, value);
						costumizer.actions.change(path);
						cssHelpers.render.addStyleUI(path, value, "cosDyn");				
					}
				}
				//costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");	
			},
			cancel : function(){
				//var updatePathCss=[];
				//ccssCloneostumizer.design.cssClone=jQuery.extend(true,{},costum);
				arrayStyleToRemove=[];
				$.each(costumizer.actions.pathToValue, function(e, path){
					if(path.indexOf("css")>= 0){
						arrayStyleToRemove.push(path.slice(path.indexOf(".")+1,path.lastIndexOf(".")));
					}
				});
				if(notEmpty(arrayStyleToRemove))
					cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");
				//costum.initCssCustom(costumizer.design.cssClone.css, "costumizer.design.cssClone.css");*/
				//costum.initCssCustom();
				//costum
				//if(typeof callback == "function") callback();
			},
			close: function(){
				costumizer.design.activeObjCss={};
			},
			afterUpdate : function () {
				var arrayStyleToRemove=[];
				var objToUp={};
				$.each(costumizer.actions.pathToValue, function(e, path){
					if(path.indexOf("css")>= 0){
						objToUp[path.replace("css.", "")]=jsonHelper.getValueByPath(costumizer.obj, path);
						arrayStyleToRemove.push(path.slice(path.indexOf(".")+1,path.lastIndexOf(".")));
					}
				});
				if(notEmpty(objToUp))
					cssHelpers.render.addStyleInDomByPath(objToUp,"#amazing-costumizer-css", "cosDyn");
				//if(notEmpty(arrayStyleToRemove))
				//	cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");

			},
			customizeLoader: function() {
				let background = $('#inputs-loader input[name="background"]').val();
				let r1borderColor = $('#inputs-loader .input-section:first input[name="color"]').val();
				let r1borderwidth = $('#inputs-loader .input-section:first input[name="borderWidth"]').val();
				let r1height = $('#inputs-loader .input-section:first input[name="height"]').val();
				let r1width = $('#inputs-loader .input-section:first input[name="width"]').val();
				let r1top = $('#inputs-loader .input-section:first input[name="top"]').val();
				let r1left = $('#inputs-loader .input-section:first input[name="left"]').val();
				let r2borderColor = $('#inputs-loader .input-section:last input[name="color"]').val();
				let r2borderwidth = $('#inputs-loader .input-section:last input[name="borderWidth"]').val();
				let r2height = $('#inputs-loader .input-section:last input[name="height"]').val();
				let r2width = $('#inputs-loader .input-section:last input[name="width"]').val();
				let r2top = $('#inputs-loader .input-section:last input[name="top"]').val();
				let r2left = $('#inputs-loader .input-section:last input[name="left"]').val();

				$('#collapseloader .panel-body .loader-contain').css({"background-color": `${background}`});
				//loading_modal
				$('#collapseloader .panel-body #ring1').attr("style",
					`border-width: ${r1borderwidth}; border-color: transparent ${r1borderColor}; height: ${r1height}; width: ${r1width}; left: ${r1left}; top: ${r1top}`
				);
				$('#collapseloader .panel-body #ring2').attr("style", 
					`border-width: ${r2borderwidth}; border-color: transparent ${r2borderColor}; height: ${r2height}; width: ${r2width}; left: ${r2left}; top: ${r2top}`
				);
				//loader 1
				$('#collapseloader .panel-body .loader_1 span').attr("style",`background-color: ${r1borderColor};`);
				var loaderStyle = '.loader-square:before{ border: solid '+r1borderwidth+' '+r1borderColor+'; } .loader-square:after{ border: solid '+r2borderwidth+' '+r2borderColor+';}';
				//loader_2
				$('#collapseloader .panel-body .dl .dl_corner--top').attr("style",`color: ${r1borderColor};`);
				$('#collapseloader .panel-body .dl .dl_corner--bottom').attr("style",`color: ${r2borderColor};`);
				//loader_3
				$('#collapseloader .panel-body .horizontal_bar_loader').attr("style",`background-color: ${r2borderColor};height: ${r2borderwidth};`);
				$('#collapseloader .panel-body .horizontal_bar_loading').attr("style",`background-color: ${r1borderColor};height: ${r1borderwidth};`);
				//loader_4
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(1)').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(2)').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(3)').attr("style",`background-color: ${r2borderColor};`);
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(4)').attr("style",`background-color: ${r2borderColor};`);
				//loader 5
				loaderStyle += ' @keyframes loaderBlock { 0%, 30% {transform: rotate(0);} 55% {background-color: '+r2borderColor+';} 100% {transform: rotate(360deg);}}@keyframes loaderBlockInverse { 0%, 20% {transform: rotate(0);} 55% {background-color: '+r2borderColor+';} 100% {transform: rotate(-360deg);}}';
				//loader 6
				loaderStyle += ' .loader-circle-2::before {background-color: '+r2borderColor+';}'
				$('#collapseloader .panel-body .loader-circle-2').attr("style",`border: ${r1borderwidth} solid ${r1borderColor};`);
				//loader 7
				loaderStyle += '@keyframes spanMoveAnimation { 0% {transform: scale(1);margin: 0;} 20% {transform: scale(1);margin: 0;} 60% {transform: scale(0.5);margin: 20px;background: '+r2borderColor+';}  100% {  transform: scale(1);  margin: 0;  }  }'
				//loader 8
				$('#collapseloader .panel-body .loader-ball div.dot1').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .loader-ball div.dot2').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .loader-ball div.dot3').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .loader-ball div.dot4').attr("style",`background-color: ${r1borderColor};`);
				loaderStyle += '.loader-ball div::after {background-color: '+r2borderColor+';}';

				$('style#cssLoader').html("");
				$('style#cssLoader').append(loaderStyle);

			},
			customizeProgress: function() {
				let bar = $('#inputs-progress .input-section:first input[name="background"]').val();
				let value = $('#inputs-progress .input-section:last input[name="background"]').val();
				$( "#collapseprogress .panel-body .progress").css({"backgroundColor": `${bar}`});
				$( "#collapseprogress .panel-body .progress-bar").css({"backgroundColor": `${value}`});
			},
			useSelectFont : function() {
				$('#inputs-font .input-wrapper .form-group:first').show();
				$('#inputs-font .input-wrapper .form-group:last').hide();
				$('.btn-upload-false').css({"color":"#ffffff !important", "background-color":"#34a853"});
				$('.btn-upload-true').css({"color":"#34a853 !important", "border-color":"#ccc", "background-color":"transparent"});
				$('.btn-upload-true').removeClass("disabled");
				$('.btn-upload-false').addClass("disabled");
			},
			useUploadFont : function() {
				$('#inputs-font .input-wrapper .form-group:first').hide();
				$('#inputs-font .input-wrapper .form-group:last').show();
				$('.btn-upload-true').css({"color":"#ffffff !important", "background-color":"#34a853"});
				$('.btn-upload-false').css({"color":"#34a853 !important", "border-color":"#ccc", "background-color":"transparent"});
				$('.btn-upload-true').addClass("disabled");
				$('.btn-upload-false').removeClass("disabled");
			},
			checkFontsExist : function() {
				$("#inputs-font").prepend(`
					<div class="form-group">
						<div class="button-font" style="display: flex; justify-content: center; aligns-items: center; width: 100%; flex-direction: column; padding: 10px 0">
							<button type="button" class="btn btn-sm btn-default text-bold btn-upload-true">${tradCms.fontUpload}</button>
							<button type="button" class="btn btn-sm btn-default text-bold btn-upload-false" style="margin-top: 5px;">${tradCms.selectFont}</button>
						</div>
					</div>
				`);
				if (costumizer.obj.css.font) {
					if (costumizer.obj.css.font.url != "" && costumizer.obj.css.font.useUploader === false) {
						costumizer.design.actions.useSelectFont();
					} else if (costumizer.obj.css.font.url == "" && costumizer.obj.css.font.useUploader === true) {
						costumizer.design.actions.useUploadFont();
					} else {
						costumizer.design.actions.useSelectFont();
					}
				} else {
					costumizer.design.actions.useSelectFont();
				}
			},
			
		}
	},
	css : {
		views : {
			init : function(){
				let cssFile = costumizer.css.actions.get();
				//$(".save-costumizer").css({"visibility":"hidden"});

				// <button class="btn btn-danger save-cssFile" style="margin-left: 50px;"><i class="fa fa-save"></i> ${trad.save}</button>
				var str=`
						<div style="display: flex; justify-content: space-around;">
							<div class=" form-group cssCodecustom" style="width: 80vw;">
								<label class="col-xs-12 text-left control-label no-padding" style="color:#333;" for="cssCode">
									<i class="fa fa-chevron-down"></i> ${tradCms.cssCode}
								</label>
								<div class="col-xs-12 text-left div-wrapper-high" style="padding-left: 0px;padding-right: 0px; display: flex; justify-content: space-around;">
									<textarea id="cssFileLeft" maxlength="" class="form-control textarea col-xs-12 no-padding valid" name="cssFileLeft" is="highlighted-code" language="css" tab-size="1" style="position: relative; overflow: unset; height: 70vh; width: 80vw ; resize: none; tab-size: 1; white-space: pre; font-family: monospace; color: transparent; background-color: rgb(250, 250, 250); caret-color: rgb(56, 58, 66);">${cssFile}</textarea>
								</div>
							</div>
						</div>
					`;
				return str;
			},
		},
		actions : {
			get: function () {
			
				(async ({chrome, netscape}) => {
    
					// add Safari polyfill if needed
					if (!chrome && !netscape)
					  await import('https://unpkg.com/@ungap/custom-elements');
				
				  
					const {default: HighlightedCode} =
					  await import('https://unpkg.com/highlighted-code');
				
				  
					HighlightedCode.useTheme('atom-one-light');
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				  })(self);

				var result = new Array();
				ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cssFile/getbycostumid",
                    {id: costum.contextId},
                    function(data){ 
                        $.each(data, function(key,value) {
                            if (notEmpty(value.css)){
                                result.push({"css" : value.css})
                            }
                         });
                      },
                    null,
                    "json",
                    extraParamsAjax
                );

                if (result.length > 0){
                    return result[0]["css"];
                }
                else{
                    return "";
                }

				
			},

			save: function () {
				var input = document.querySelector('#cssFileLeft');
				// let validate = input.value.replace(/[^\S+]/g,"");
				// validate = validate.replace(/\/\*[\s\S]*?\*\//g,"");
				// validate = validate.replace(/http:\/\//gm,"");
				// validate = validate.replace(/((#|[.]|[*]){0,1}[\w\-[\]^="':>~+,()])+\s*\{((?:[A-Za-z\- \s]+[:]\s*([#'"0-9\w .,\/()\-!%]+\;)))+\}(?:\s*)/mg,"");
				// validate = validate.replace(/@importurl\([\"']([^)]*)[\"']\);|@import(\"|\')([^)]*)(\"|\');/g,"");
				// validate = validate.replace(/(@.*?:?[^}{@]+(\{(?:[^}{]+|\{(?:[^}{]+|\{[^}{]*\})*\})*\}))/g,"")
				

				const validation = csstreeValidator.validate(input.value);
				params = {  
					"id" : costum.contextId, 
					"css" : input.value
				};

				let saveButton = `<button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> ${trad.save}</button>`;

				if(validation.length > 0){ 
					toastr.error(validation[0]["message"]+": line "+validation[0]["line"]+",column "+validation[0]["column"]);
					$(".save-cssFile").replaceWith(saveButton);
				}
                else {
                    ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/cssFile/insert",
				        params,
					);
					toastr.success(tradCms.saved);
                    location.reload();
					$(".save-cssFile").replaceWith(saveButton);
                }
			}
				
		},
		events :{ 
			bind : function(){

				$("#cssFileLeft").on("mouseenter", function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				});

				$("#cssFileLeft").on("click", function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				});

				$("#highlighted-code").on("change", function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				});

				$('#cssFileLeft').keydown(function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
					if($('#cssFileLeft').val() != "") {
						$(".save-costumizer").replaceWith(`<button class="btn btn-danger save-cssFile" onclick="costumizer.css.actions.save()" style="margin-left: 50px;"><i class="fa fa-save"></i> ${trad.save}</button>`);
					}
				}); 

				$(".close-dash-costumizer").off().on("click", function(){
					costumizer.actions.cancel($(this).data("dom"));
					
						
					if ($("#amazing-costumizer-css").length > 0 ){
						$("#amazing-costumizer-css").attr("type", "text/css");	
					} else if ($("#amazing-costumizer-css-new").length > 0){
						$("#amazing-costumizer-css-new").attr("type", "text/css");
					}
					//$($(this).data("dom")).fadeOut(1000);
					$("#content-btn-delete-page").remove();
					$(".save-cssFile").replaceWith(`<button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> ${trad.save}</button>`);
				});

				// $(".save-cssFile").on("click", function(){
				// 	costumizer.css.actions.save();
				// });
			}
		},

	},
	communityAdmin: {
		views : {
			initAjax:function(dom){
				adminPanel.views.community(dom);
			}
		}
	},
	typeObj: {
		views: {
			init: function () {
				// Travail @Mahefa costumizer.typeObj
				// Initialiser la vue typeObj avec des élements (travail html et css à partir de la données)  :
				//	=> Afficher que les elements suivant : citoyens, project, organizations (nGo, governemental, business, group), events, classified
				//  => Pour chaque élément, afficher la couleur, l'icon et le nom afficher dans typeObj 
				// 	=> exemple des données pour une personne: {add: true,color: "yellow-k",createLabel: "Inviter vos amis",name: "Personne"}
				// Pouvoir éditer ces dernieres entrées  
				// => le add => true , avec switcher et expliquer que s'il est activé l'ajout d'elt sera disponible sur le bouton create element
				// Ajouter des nouveaux éléments (articles, formulaire, etc) => Deuxième temps !!!
				$.each(typeObj, function (e, obj) {
					var entryObj = e;
					if (typeof obj.sameAs != "undefined")
						entryObj = obj.sameAs;
					if (typeof costum.typeObj[e] != "undefined") {
						if (typeof costum.typeObj[e] == "object") {
							$.each(costum.typeObj[e], function (entry, value) {
								typeObj[entryObj][entry] = value;
							});
						}
					} else if (typeof obj.add != "undefined" && obj.add) {
						typeObj[entryObj].add = false;
					}
				});
				$.each(costum.typeObj, function (e, v) {
					if (typeof typeObj[e] == "undefined")
						typeObj[e] = v;
				});
				
				return costumizer.typeObj.views.showElements();
			},
			showElements: function () {
				let str = "", defaultObj = {}, specificObj = {}, newTypeObj = {}, costumTypeObj = [];
				$.each(typeObj, function (e, v) {
					if (( v.col == "organization" || v.col == "organizations" || v.col == "citoyens" || v.col == "projects" || v.col == "events" || v.col == "classifieds") && v.name != undefined) {
						jsonHelper.setValueByPath(v, "defaultElt", true)
						defaultObj[e] = v;
					}
				});
				
				Object.keys(defaultObj).map(function(k, v) {
					Object.keys(costum.typeObj).map(function(key, value) {
						if (k == key) {
							costumTypeObj.push(key);
						}
					})
				})

				$.each(costum.typeObj, function (e, v) {
					if (typeof typeObj[e].sameAs != "undefined" && typeof defaultObj[typeObj[e].sameAs] != "undefined") {
						delete defaultObj[typeObj[e].sameAs];
					}					
					if(!costumTypeObj.includes(e)) {
						jsonHelper.setValueByPath(v, "defaultElt", false)
						specificObj[e] = v;
					}
				})
				newTypeObj = {...defaultObj, ...specificObj}
				str += "<div class='row eltToggle' id='standard-element'>"
				$.each(newTypeObj, function (e, v) {
					if (jsonHelper.pathExists("costumizer.obj.typeObj."+e) && jsonHelper.getValueByPath(costumizer.obj.typeObj, e) === '$unset') {
						delete costumizer.obj.typeObj[e];
						v.add = false;
					}
						
					let context = (jsonHelper.pathExists("contextType")) ? contextType : (jsonHelper.pathExists("costum.contextType")) ? costum.contextType : "";

					if (context !== "" && typeObj.get(e).ctrl === "person") {
						jsonHelper.setValueByPath(costumizer.obj.typeObj, e+".explainText","")
						v.createLabel = (jsonHelper.pathExists("tradDynForm."+context+"Invite")) ? tradDynForm[context+"Invite"] : tradDynForm.defaultInvite;
						v.color = "red";
						v.icon = "plus-circle";

						if (jsonHelper.pathExists("costum.title"))
							v.createLabel = v.createLabel + " " + costum.title;
					} 

					v.name = (typeof v.addLabel !== "undefined") ? v.addLabel : v.name;

					if ( typeof jsonHelper.getValueByPath(typeObj, e + ".add") == "undefined")
						jsonHelper.setValueByPath(typeObj, e + ".add", false);

					let defaultLabel = e.split(/(?=[A-Z])/).map(function(value) {
						return value.charAt(0).toLowerCase() + value.slice(1);
						});

					const defaultValues = {
						color: "dark",
						createLabel: defaultLabel.join(" ").replace(/^./, defaultLabel.join(" ").charAt(0).toUpperCase()),
						name: defaultLabel.join(" ").replace(/^./, defaultLabel.join(" ").charAt(0).toUpperCase()),
						icon: "no-icon"
					};
						
					const setValue = (e, prop) =>  costumizer.typeObj.views.setElementValue(e, prop, v[prop]) || defaultValues[prop];
					
					for (let prop in defaultValues) {
						v[prop] = setValue(e, prop);
					}
					
					const escapeSingleQuote = (str) => {
						if (str !== undefined && str !== null) {
							return str.replace(/'/g,"&#146;");
						}
						return str;
					};
					
					if (typeof v.name != "undefined" && typeof v.createLabel !== "undefined" && (typeof v.createLabel === "string" || typeof v.name === "string")) {
						v.name = escapeSingleQuote(v.name);
						v.createLabel = (typeof costumizer.obj.typeObj[e] !== "undefined" && typeof costumizer.obj.typeObj[e].createLabel !== "undefined")? costumizer.obj.typeObj[e].createLabel : (trad["add"+e] !== undefined)? trad["add"+e] : v.createLabel;
					}

					const isHexColor = (color) => {
						if (costumizer.typeObj.actions.testHexColor(color)) {
							return '<div id="'+e+'-color-icon" class="icon" style="background-color:'+color+'">';
						} else {
							return '<div id="'+e+'-color-icon" class="icon bg-'+color+'" ">';
						}
					}

					str += 
					'<div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 standard-element-item">' +
						`<div id="elt-${e}" class="standard-element-content ${(!v.add) ? "element-disabled" : ""}">`+
							'<div class="mid">'+
								'<label class="rocker">'+
									`<input type="checkbox" class="active-input" data-default=${v.defaultElt} data-value="${e}" ${(v.add) ? "checked" : ""}>`+
									'<span class="switch-left"><i class="fa fa-check"></i></span>'+
									'<span class="switch-right"><i class="fa fa-times"></i></span>'+
								'</label>'+
							'</div>'+	
							'<div class="icon-standard-element">'+
									isHexColor(v.color)+
									'<i id="'+e+'-icon-'+v.icon+'" class="fa fa-'+ v.icon + '"></i>'+
								'</div>'+
								'<div class="label-content">'+
									'<span id="label-content-'+e+'" >' + v.createLabel + ' </span>'+
									'<span id="name-content-'+e+'">(' + v.name + ')</span>'+
								'</div>' +
								`<a href="javascript:;" style="z-index: inherit;"
									onclick='costumizer.typeObj.events.edit("${e}","${escapeSingleQuote(v.createLabel)}","${v.name}","${v.color}","${v.icon}","${v.formType}","${v.formSubType}","${v.defaultElt}")'  class='btn-elt-${e} editElement badge'>` +
									'<i class="fa fa-pencil"></i> ' + trad.edit +
								'</a>'+
							'</div>' +
						'</div>'+
					'</div>';
				});

				str += "</div>";
				
				if (!notEmpty(str)) 
					str = "+ Ajouter des elements";

				return str;
			},
			setElementValue : function(elt, atr, value) {
				return 	(typeof jsonHelper.getValueByPath(costumizer.obj.typeObj[elt], atr) != "undefined")? 
						jsonHelper.getValueByPath(costumizer.obj.typeObj[elt], atr) : (typeof value == "undefined")? 
						(typeof typeObj[elt].sameAs != "undefined")? typeObj[typeObj[elt].sameAs][atr] : typeObj[elt][atr]
						: value;
			}
			
		},
		events: {
			bind : function(){	
				$(".active-input").on("change", function(e) {
					e.stopImmediatePropagation();
					if ($(this).is(":checked")) {
						$(this).closest('.standard-element-content').removeClass('element-disabled');
						jsonHelper.setValueByPath(costumizer.obj.typeObj, $(this).data("value")+".add", true);
						costumizer.typeObj.actions.change("typeObj."+$(this).data("value")+".add");
						$(".btn-elt"+$(this).data("value")).css({"pointer-events":""})
					} else {
						$(this).closest('.standard-element-content').addClass('element-disabled');
						if ($(this).data("default")) {
							jsonHelper.setValueByPath(costumizer.obj.typeObj, $(this).data("value"), '$unset');
							costumizer.typeObj.actions.change("typeObj."+$(this).data("value"));
						} else {
							jsonHelper.setValueByPath(costumizer.obj.typeObj, $(this).data("value")+".add", false);
							costumizer.typeObj.actions.change("typeObj."+$(this).data("value")+".add");
						}
									
						$(".btn-elt"+$(this).data("value")).css({"pointer-events":"none"});
					}
					// costumizer.typeObj.actions.change("typeObj."+$(this).data("value")+".add");
				})

				// if (screen.width > 560) {
					if ($("#right-sub-panel-container").attr("class") === "active") { 
						let contentWidth = $(".contains-view-admin").width() - 300;
							$('.eltToggle').animate({ width: contentWidth + "px" }, {duration: 300, queue: false, easing: 'swing'});
					}
					$("#right-sub-panel-container").find("button").click(function(){
						$('.eltToggle').animate({ width: "100%" }, {duration: 300, queue: false, easing: 'swing'});
						$(".selected-element").removeAttr("style").removeClass("selected-element");
					})
				// }

			},
			edit: function (key, eltLabel, eltName, eltColor, eltIcon, type, subType, defaultElt) {
				let infoValue = (jsonHelper.pathExists("costum.typeObj."+key+".dynFormCostum.beforeBuild.properties.info.html")) ? costum.typeObj[key].dynFormCostum.beforeBuild.properties.info.html : 
				                (jsonHelper.pathExists("costum.typeObj."+key+".dynFormCostum.onload.actions.html.infocustom")) ? costum.typeObj[key].dynFormCostum.onload.actions.html.infocustom : "";
				let formType = (typeObj.get(key).formType !== undefined)? typeObj.get(key).formType : (typeObj.get(key).ctrl !== undefined)? typeObj.get(key).ctrl : key;
					if (defaultElt === "true") {
						currentKFormType = (subType) ? subType : null;
						formType = (type !== "undefined")? type : key;
					} 

				let panelContent = {
					"key": key, 
					"createLabel": eltLabel, 
					"name": eltName, 
					"color": eltColor, 
					"icon": eltIcon, 
					"explainText": ""
				}
			
				if (defaultElt == "false") {
					panelContent.explainText = infoValue;
					costumizer.typeObj.events.openPanel(panelContent);
					formType = "none"
				}

				if (typeof $("#elt-"+key).html() === "string"){
					if ($(".selected-element").html() !== undefined)
						$(".selected-element").removeAttr("style").removeClass("selected-element");

					$("#elt-"+key).css("border", "2px solid").css("border-color", "#086784")
					$("#elt-"+key).addClass("selected-element")
				}

				if (formType !== undefined && formType !== "none"){
					dyFObj.getDynFormObj(formType, function() { 					
						try {
							infoValue = dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties.info.html
						} 
						catch (e) {}
			
						if (typeof currentKFormType !== "undefined" && currentKFormType !== null) {
								if(typeof typeObj[currentKFormType] !== "undefined" && typeof typeObj[currentKFormType].explainText !== "undefined") 
									infoValue = typeObj[currentKFormType].explainText;
						}

						panelContent.explainText = infoValue;
						costumizer.typeObj.events.openPanel(panelContent);
					});
				}
			  },
			  
			openPanel: function (eltValue) {
				let configRight = {
					width:300,
					header:{
						title:tradCms.ModifyElement,
						position:"top"
					}
				};

				for (let prop in costumizer.obj.typeObj[eltValue.key]) {
					$.each(eltValue, function (e, v) {
						if (prop == e) {
							eltValue[e] = costumizer.obj.typeObj[eltValue.key][prop]
						}
					})
				}
				
				if(!costumizer.liveChange)
					configRight.distanceToRight = 0;

				costumizer.actions.openRightSubPanel(
					'<div id="form-mockup-menu-edit" style="padding:0px 10px; margin-bottom:50px"></div>',
					configRight
				);

				let panelContent = {
					container: "#form-mockup-menu-edit",
					inputs: [
						{
							type: "inputSimple",
							options: {
								name: "createLabel",
								label: tradCms.label,
								defaultValue: eltValue.createLabel.replace(/<i[^>]*>|<\/i>/g, ''),
							}
						},
						{
							type: "inputSimple",
							options: {
								name: "name",
								label: trad.Name,
								defaultValue: eltValue.name,
							}
						},
						{
							type: "colorPicker",
							options: {
								name: "eltColor",
								label: tradCms.color,
								defaultValue: (!costumizer.typeObj.actions.testHexColor(eltValue.color))?
											  $('#'+eltValue.key+'-color-icon').css('background-color') : eltValue.color,
							}
						},
						{
							type: "inputIcon",
							options: {
								name: "icon",
								label: tradCms.icon,
								defaultValue: (eltValue.icon === "") ? "" : "fa fa-"+eltValue.icon,
							}
						},
						{
							type : "textarea",
							options: {
								name : "info",
								label : tradCms.elementInfo,
								defaultValue : eltValue.explainText.replace(/(<([^>]+)>)/ig, "")
							}
						},
					],
					onchange: function (name, value, payload) {
						let finalPath = costumizer.obj;
						let path = `typeObj.${eltValue.key}.${name}`;
						if (exists(value)) {
							if (name == "eltColor") {
								$('#'+eltValue.key+'-color-icon')	
									.attr('class', 'icon')
									.attr('style', 'background-color:'+value);
								path = `typeObj.${eltValue.key}.color`;
							}
							if (name == "icon") {
								let etlIcon = (eltValue.icon === "") ? "no-icon" : eltValue.icon;
								$('#'+eltValue.key+'-icon-'+etlIcon).attr('class', 'fa fa-'+value);
								jsonHelper.setValueByPath(costumizer.obj.typeObj[eltValue.key], "dynFormCostum.onload.actions.setTitle", "<i class='fa fa-"+value+"'></i> "+eltValue.createLabel);
								if (!costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle");
							}
							if (name == "createLabel") {
								$('#label-content-'+eltValue.key).replaceWith(`
									<span id="label-content-${eltValue.key}" >${value} </span>
								`); 
								jsonHelper.setValueByPath(
									costumizer.obj.typeObj[eltValue.key], 
									"dynFormCostum.onload.actions.setTitle", 
									`<i class='fa fa-${eltValue.icon}'></i> ${value}`
								);

								if (!costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle");
									
							}
							if (name == "name") {
								$('#name-content-'+eltValue.key).replaceWith(`
									<span id="name-content-${eltValue.key}">(${value})</span>
								`);

								if (jsonHelper.pathExists("typeObj."+eltValue.key+".addLabel") && !costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".addLabel")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".addLabel");
									
							}
							if (name == "info") { 
								let infoTemplate = "";
								if (value !== "")
									infoTemplate = "<p class='text-"+eltValue.color+"'><i class='fa fa-info-circle'></i> "+value
								jsonHelper.setValueByPath(
									costumizer.obj.typeObj[eltValue.key], 
									"dynFormCostum.beforeBuild.properties.info.html", 
									infoTemplate
								);
								path =`typeObj.${eltValue.key}.explainText`;
								if (!costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".dynFormCostum.beforeBuild.properties.info.html")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".dynFormCostum.beforeBuild.properties.info.html");
							}
							costumizer.typeObj.actions.updateChanges(finalPath, path, value);
						}
					}
				}
				if (new CoInput(panelContent)) {
					$('input[name="eltColor"]').parent().removeClass("sp-original-input-container").removeClass("sp-colorize-container");
				}
			}
		},
		actions: { 
			updateChanges: function(finalPath, path, value) {
				jsonHelper.setValueByPath(finalPath, path, value);
				costumizer.typeObj.actions.change(path);
			},
			callback : function(data){
				costumizer.htmlConstruct.actions.refreshmenu(data.params, costum.htmlConstruct);
			},
			testHexColor : function (color) {
				const hexColorRegex = /^#([0-9A-Fa-f]{6}|[0-9A-Fa-f]{8})$/;
					if (hexColorRegex.test(color)) {
						return true
					}
					return false
			},
			change: function (path) {
				costumizer.actions.change(path);

				if (typeof $(".show-bottom-add").html() != "undefined") {
					if ($(".show-bottom-add").parent().length > 1) {
						$.each($(".show-bottom-add").parent(), function (id, content) {
							let btnPath = $(this).data("path");
							if (!costumizer.actions.pathToValue.includes("htmlConstruct."+btnPath)) {
								costumizer.actions.pathToValue.push("htmlConstruct."+btnPath);
							}	
						})
					} 
					else {
						costumizer.actions.change("htmlConstruct."+$(".show-bottom-add").parent().data("path"));
					}
				}
			}
		}
	},
	template  : {
		views : {
			init : function(){	
				return `<div class=" col-md-12">
						<div class='headerSearchIncommunity no-padding col-xs-12' style="margin-top: 20px;"></div>
						<div id='filterContainer' class='searchObjCSS'></div>
							<div class='bodySearchContainer'>
								<div class='no-padding col-xs-12' id='dropdown_search'>
								</div>
								<div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
							</div>
						</div>`;
			}
	  	},
		actions : {
			init: function(){
				costumizer.template.actions.templateList();
				var interval = setInterval(function(){
					if($(".use-this-template").length > 0){
						clearInterval(interval)
						$(".use-this-template").click(function(e){
							$(this).addClass('disabled');
							$(this).html('<span style=""><i class="fa fa-spin fa-circle-o-notch"></i> <span>'+trad.processing+' ...</span></span>');
							costumizer.template.actions.use($(this).data("id"), $(this).data("collection"),$(this).data("action"))
							e.stopImmediatePropagation();
						})
						$(".share-this-template").click(function(e){
							let $this = $(this);
							if ($this.hasClass("action-hide")) {
								shareTpl = {
									id : $this.data("id"),
									collection : $this.data("collection"),
									path : "shared",
									value : true
								}
								dataHelper.path2Value( shareTpl, function(params) {
									$("#cmsBuilder-right-actions .lbh-costumizer").click()
									toastr.success("Shared!");
								})
							}else{
								hideTpl = {
									id : $this.data("id"),
									collection: $this.data("collection"),
									path : "shared"
								}
								dataHelper.unsetPath( hideTpl, function(params) {
									$("#cmsBuilder-right-actions .lbh-costumizer").click()
									toastr.success("hidden!");        
								} );
							}
							e.stopImmediatePropagation();
						})
					}
				}, 100)
			},
			changeCollection : function(id, collectionInit){
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/element/get/type/"+collectionInit+"/id/"+id+"",
					{},
					function (data) {    
						if (data.map != undefined) {
							var tplCtx = {
								collection: collectionInit,
								costumId: contextData.id,
								costumSlug: contextData.slug,
								costumType: contextData.collection,
								value: data.map,
								id: id,
							}
							delete tplCtx.value._id
							delete tplCtx.value.costumId
							delete tplCtx.value.costumType
							delete tplCtx.value.created
							delete tplCtx.value.creator
							delete tplCtx.value.modified
							delete tplCtx.value.updated
							delete tplCtx.value.source

							if(data.map.subtype == undefined) {
								tplCtx.collection = "cms",
								tplCtx.value.collection = "cms"
							}
							if (data.map.subtype == "site") {
								tplCtx.collection = "templates",
								tplCtx.value.collection = "templates"
							}
							dataHelper.path2Value( tplCtx, function(params) {
								dyFObj.commonAfterSave(params,function(){ 
									if (params.saved.map.subtype == "site") {
										costumizer.template.actions.delete(params.saved.id, "cms","delete")
									} 
									if (params.saved.map.subtype == undefined) {
										costumizer.template.actions.delete(params.saved.id, "templates","delete")
									}
								}); 
							})
						}   
					}      
				);   
			},
			delete: function(id,collection, del) {
				var params = {
					"type": "template",
					"collection": collection
				}
				var urlToSend = baseUrl + "/co2/cms/deletetemplate/id/" + id;
				if (del == "delete") {
					ajaxPost(
						null,
						urlToSend,
						params,
						function(data) {
							if (data.result){
								costumizer.template.actions.reload();
							} else {
								toastr.error(data.msg);
							}
						},
						null,
						"json"
					);
				}  
				if (del == undefined) {
					bootbox.confirm({message: trad.areyousuretodelete,
						buttons: {
							confirm: {
								label: trad.yes,
								className: 'btn-success'
							},
							cancel: {
								label: trad.no,
								className: 'btn-danger'
							}
					}, 
					callback: function(response) {
						if (response) {
							ajaxPost(
								null,
								urlToSend,
								params,
								function(data) {
									if (data.result){
										costumizer.template.actions.reload();
										toastr.success(data.msg);
										urlCtrl.loadByHash( location.hash );
									} else {
										toastr.error(data.msg);
									}
								},
								null,
								"json"
							);
						}
					}});
				}
			},	
			reload: function() {
				$(".lbh-costumizer[data-link='template']").trigger("click");
			},
			update: function(id, collection) {
				var tplId = id;
				var tplCollection = collection;
				var tplCtx = {};
                var activeForm = {
                    "jsonSchema" : {
                        "title" : tradCms.registerTpl,
                        "type" : "object",
                        "properties" : {
                            name : {
                                label: tradCms.nameTpl,
                                inputType : "text",
                                rules:{
                                    "required":true
                                }
                            },
                            type : { 
                                "inputType" : "hidden",
                                value : "template" 
                            },
                            subtype : { 
                                "inputType" : "select",
                                label : tradCms.saveOptions,
                                rules:{
                                    "required":true
                                },
								value: "",
                                options : {
                                    site : tradCms.allSite,
                                    page : tradCms.thisPageOnly
                                },
                                placeholder : tradCms.selectAnOption
                            },
                            category : {
                                label : trad.category,
                                inputType : "select",
                                rules:{
                                    "required":true
                                },
                                options : cmsBuilder.config.tpl.listCategories,
                                placeholder : tradCms.selectAnOption,
								value: "" 
                            },
                            image : dyFInputs.image(),
                            description : {
                                label: tradDynForm.description,
                                inputType : "text",
                                value : ""
                            }
                        },
                        save : function () {
                            tplCtx.value = {};
                            params = {};
                            $.each( activeForm.jsonSchema.properties , function(k,val) { 
                                tplCtx.value[k] = $("#"+k).val();
                            });
                            tplCtx.value.parent={};
                            tplCtx.value.parent[costum.contextId] = {
                                type : costum.contextType, 
                            };
                            if (tplCtx.value.subtype == "site"){    
                                params["allPage"] = true;
                                tplCtx.collection = "templates"
                                tplCtx.value.collection = "templates"
                                tplCtx.value.siteParams = {}
                                tplCtx.value.siteParams.css = costumizer.obj.css;
                                tplCtx.value.siteParams.app = costumizer.obj.app
                                tplCtx.value.siteParams.htmlConstruct = costumizer.obj.htmlConstruct;
                                ajaxPost(
                                    null,
                                    baseUrl+"/"+moduleId+"/cms/getallcms",
                                    params,
                                    function(data){ 
                                        mylog.log("allcsm",data.allCms);    
                                        tplCtx.value.cmsList = data.allCms;                
                                    },
                                    null,
                                    null,
                                    {
                                        async: false
                                    }
                                );

                            }else{  
                                tplCtx.collection = "cms"
                                tplCtx.value.collection = "cms"
                                tplCtx.value.cmsList = cmsBuilder.config.block.idCmsMerged;
                                delete tplCtx.value.subtype;
                            }

                            tplCtx.value.tplsUser={};
                            tplCtx.value.tplsUser[costum.contextId] = {};
                            tplCtx.value.tplsUser[costum.contextId][cmsBuilder.config.page] ="using";
                            if(typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
								ajaxPost(
									null,
									baseUrl+"/"+moduleId+"/cms/updatetemplate",
									tplCtx,
									function(data){
										dyFObj.commonAfterSave(params,function(){
											toastr.success(tradCms.elementwelladded);
											$("#ajax-modal").modal('hide');           
										});
									}
								);
                                if(cmsBuilder.config.tpl.activeId != ""){
                                // cmsBuilder.tpl.saveTplUser(cmsBuilder.config.tpl.activeId,"backup");
                                }
                                //if "save as new", duplicate and leave currently cms then save it into the new template
                                if (Object.keys(cmsBuilder.config.block.cmsInUseId).length > 0) {                
                                    // cmsBuilder.block.duplicate(cmsBuilder.config.block.cmsInUseId,tplCtx.id);
                                }
                                // cmsBuilder.tpl.removeCmsStat(tplCtx.id,cmsBuilder.config.block.newCmsId);
                                // urlCtrl.loadByHash(location.hash);
                            }       
                        },
                    }
                };
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/element/get/type/"+tplCollection+"/id/"+tplId+"",
					{},
					function (data) {    
							// if (data.map.subtype == undefined) {
							// 	dynFormCostum.beforeBuild.properties.subtype.value = "page"
							// }   
							tplCtx.id=tplId;
							tplCtx.collection=tplCollection;
							// dynFormCostum.beforeBuild.properties.name.value = data.map.name;
							// dynFormCostum.beforeBuild.properties.description.value = data.map.description;
							// dynFormCostum.beforeBuild.properties.category.options = cmsBuilder.config.tpl.listCategories;
							// dynFormCostum.beforeBuild.properties.category.value = data.map.category;
							activeForm.jsonSchema.properties.subtype.value = (data.map.subtype) ? data.map.subtype : "page";
							activeForm.jsonSchema.properties.category.value = new DOMParser().parseFromString(data.map.category, "text/html").documentElement.textContent;
							dyFObj.openForm(activeForm, null,data.map);
						}      
				);          
			},
			templateList: function () {
				var category = {};
				var params = {
					"action": "getType",
					"type": "template"
				}
				ajaxPost(
					null,
					baseUrl + "/co2/cms/getcmsaction",
					params,
					function (data) {
						if (data != undefined) {
							$.each(data, function (id, content) {
								category[content.category.replace(/&amp;/g, "&amp;amp;")] = content.category
							})
						}
					},
					null,
					"json",
					{ async: false }
				);
				var paramsTemplateList = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: "#filterContainer",
					interface : {
						events : {
							scroll : true,
							scrollOne : true
						}
					},
					header: {
						dom: ".headerSearchIncommunity",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding',
								group: {
									count: true,
									types: true
								}
							}
						},
					},
					defaults: {
						notSourceKey: true,
						types: ["cms","templates"],
						filters: {
							type: "template",
							shared: true
						},
						fields : ["userCounter","shared"]

					},
					results: {
						dom: ".bodySearchContainer",
						smartGrid: true,
						renderView: "directory.cmsPanelHtml",
						map: {
							active: false
						}
					},
					filters: {
						text: true,
						typeSubtype: {
							view: "dropdownList",
							type: "filters",
							action: "filters",
							name: tradCms.content,
							typeList : "object",
							event : "exists",
							keyValue: false,
							list: {
								"site" : {
                                    label:  tradCms.site,
                                    field : "subtype",
                                    value : true,
                                },
                                "page" : {
                                    label: trad.page,
                                    field : "subtype",
                                    value : false,
                                }
							},
						},

						categorie: {
							view: "dropdownList",
							type: "filters",
							name: tradCms.choooseCategory,
							field: "category",
							action: "filters",
							event: "filters",
							keyValue: false,
							list: category,
						},
					}
				}

				if (isSuperAdminCms){
					delete paramsTemplateList.defaults.filters.shared;
					paramsTemplateList.filters.typeSubtype.list["shared"] = {
						label: "Restricted",
						field : "shared",
						value : false
					}
				}

				typeObj.cms.createLabel = " " + tradCms.addSite + "";
				typeObj.cms.color = "green";
				typeObj.template.color = "green";
				var filterGroupTemplate = searchObj.init(paramsTemplateList);
				filterGroupTemplate.search.init(filterGroupTemplate);
				// cmsBuilder.bindEvents();
			},
			use : function (idTplSelected, collection,action) {
				let tpl_params = {"idTplSelected" : idTplSelected, "collection" : collection, "action" : action}
				costumizer.actions.useTemplate(tpl_params)
			}
		},
	},
	icon : {
		selected : "",
		state : "",
		openIcon : function(defaultIcon){
            let displayHtml = ""
            let newIconHtml = ""
            mylog.log("icon", defaultIcon)            
            if (typeof defaultIcon !== "undefined") 
              defaultIcon = `<i class='`+defaultIcon+`' style='font-size:80px;color:'></i><br><br><button class='btn sp-rem-icon'>Effacer</button>`
            else
              defaultIcon = "<br>Choisir une icône"
            // $.each( cmsBuilder.iconList , function(k,val) { 
            //   iconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='"+val+"'><i class='fa "+val+"'></i></a>"
            // });
            $.each( fontAwesome , function(k,val) { 
                newIconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='fa-"+k+"'><i class='fa fa-"+k+"'></i></a>"
            });
            displayHtml = "<div class='col-md-12 padding-bottom-10 bg-white'>"+
                            "<div class='col-md-5 sp-display-icon text-center' style='padding-top: 9%;height:190px;border: solid 1px #cccccc; left:1px;background-color:;'>"+
                                defaultIcon+
                            "</div>"+
                            "<div class='col-md-7 no-padding' style='height:190px;background-color: #fff;border: solid 1px #cccccc;'>"+
                                "<input type='text' style='border-radius: inherit;' class='form-control' id='iconSearcher' onkeyup='costumizer.icon.searchIcon()' placeholder='Taper pour filtrer'>"+
                                "<div id='sp-icon-list' style='position: relative;clear: both;float: none;padding: 12px 0 0 12px;background: #fff;margin: 0;overflow: hidden;overflow-y: auto;max-height: 154px;display: flex;flex-wrap: wrap;'>"+
                                    newIconHtml+
                                "</div>"+
                            "</div>"+
                          "</div>";
             bootbox.confirm({  
              title : "Choix d'icône",
              message : displayHtml,
              callback: function (result) {
               if (result) {   
               	if (costumizer.icon.state == "use") {
                  costumizer.icon.selected = $(".sp-display-icon i").data("icon").substring(3)               		
               	}
                 $(document).trigger("icon-changed");
               }
             }
             });
			 $(".this-icon").on("click", function(e) {
				e.stopImmediatePropagation();
				$(".sp-display-icon").html("<div>fa "+$(this).data("icon")+"</div><i class='fa "+$(this).data("icon")+"' data-icon='"+$(this).data("icon")+"' style='font-size:80px;color:'></i>")
				costumizer.icon.state = "use"
			 })
			 $(".sp-rem-icon").on("click", function(e) {
				e.stopImmediatePropagation();
				$(".sp-display-icon").html("<br>Choisir une icône")
				costumizer.icon.state = "delete"
			 })
         },
        searchIcon : function () {
            var input, filter, i;
            input = $('#iconSearcher');
            filter = input.val().toUpperCase();

			var newIconHtml = "";
			var newIconList = Object.keys(fontAwesome).map(function(key, value) {
				return "fa-"+key;
			});
			$.each( newIconList , function(k,val) { 
			newIconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='"+val+"'><i class='fa "+val+"'></i></a>"
			});

            ti = Object.keys(newIconList).length;
            for (i = 0; i < ti; i++) {
                paragraph = newIconList[i]
                paragraph = paragraph.replace("fa-","")         
                paragraph = paragraph.split('-').join(' ');
                paragraph = paragraph.toUpperCase();
                if (paragraph.includes(filter))
                    $(".this-icon ."+newIconList[i]).parent().show();
                else
                    $(".this-icon ."+newIconList[i]).parent().hide();
            }
        },
		rigthPanelIcon : function(defaultIcon, element) {
			let displayHtml = ""
            let newIconHtml = ""
            // mylog.log("iconnnn", costumizer.obj, `typeObj.${element}.icon`, splitIconClass[1])            
            $.each( fontAwesome , function(k,val) { 
                newIconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;' data-icon='fa-"+k+"'><i class='fa fa-"+k+"'></i></a>"
            });
            displayHtml = "<div id='edit-icon' data-element='"+element+"' style='padding: 0px;' class='col-md-12 padding-bottom-10'>"+
                            "<div class=' sp-display-icon text-center' style='padding-top: 9%;height:150px; left:1px;'>"+
                                "<div>"+defaultIcon+"</div>"+
								"<i class='"+defaultIcon+"' data-icon='' style='font-size:70px;color:'></i>"+
                            "</div>"+
				
                            "<div class=' no-padding' style='height:150px;'>"+
								"<input type='text' style='border-radius: inherit;' class='form-control' id='iconSearcher' onkeyup='costumizer.icon.searchIcon()' placeholder='Taper pour filtrer'>"+
                                "<div id='sp-icon-list' style='position: relative;clear: both;float: none;padding: 12px 0 0 12px;background: #fff;margin: 0;overflow: hidden;overflow-y: auto;max-height: 154px;display: flex;flex-wrap: wrap;'>"+
                                    newIconHtml+
                                "</div>"+
                            "</div>"+
                          "</div>";
			if ($(".fa-"+defaultIcon).html() == '') {
				$(".sp-display-icon").html("<div>fa "+$(this).data("icon")+"</div><i class='fa "+$(this).data("icon")+"' data-icon='"+$(this).data("icon")+"' style='font-size:70px;color:'></i>")
			}         
			return displayHtml;
		}
	}
}