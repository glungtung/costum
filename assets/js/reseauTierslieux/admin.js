adminPanel.views.communitys = function(){
    var data={
        title : trad.community,
        context : {
            id : costum.contextId,
            collection : costum.contextType
        },
        invite : {
            contextId : costum.contextId,
            contextType : costum.contextType,
        },
        table : {
            name: {
                name : "Membres"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Validation pour être membres",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "citoyens","organizations"],
                fields : [ "name","slug", "email", "links", "collection","hasRC","username"],
                notSourceKey : true,
                indexStep: 50
            },
            filters : {
                text : true,
                types : {
                    lists : ["citoyens" ,"organizations"]
                }
                
            }
        },
        actions : {
            openchat : true,
            admin : true,
            roles : true,
            disconnect : true
        }
    };

    data.paramsFilter.defaults.filters = {};

    if(costum.contextType=="projects"){
        data.paramsFilter.defaults.filters["links."+costum.contextType+"."+costum.contextId] = {
            '$exists' : 1 
        }
    }else{
        data.paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {
            '$exists' : 1 
        };
    }
    adminDirectory.actions.openchat = function(e, id, type, aObj){    
        chatSlug = (e.slug) ? e.slug : ""; 
        chatUsername = (e.username) ? e.username : "";	  
        str = '<button class="btn btn-xs btn-default col-xs-12 btn-open-chat pull-right contact'+id+'" '+
        					'data-name-el="'+e.name+'"  data-slug="'+chatSlug+'" data-username="'+chatUsername+'" data-type-el="'+type+'"  data-open="'+( (typeof e.preferences != "undefined" && e.preferences.isOpenEdition)?true:false )+'"  data-hasRC="'+( ( e.hasRC )?true:false)+'" data-id="'+id+'">'+
        				'<i class="fa fa-comments"></i> Discussion </button>'
        return str ;
    }; 

    var searchAdminType=(typeof paramsAdmin != "undefined" 
    && typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["community"] != "undefined"
    && typeof paramsAdmin["menu"]["community"]["initType"] != "undefined") ? paramsAdmin["menu"]["community"]["initType"]: ["citoyens"];
    data.paramsFilter.defaults.types=searchAdminType; 
      ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory', data, function(){
          ajaxPost(null, baseUrl+'/'+moduleId+'/admin/mailing', {}, function(memberMailingHtml){
              $('#content-view-admin').append(memberMailingHtml);
          },"html");
      },"html");

    adminDirectory.bindCostum = function(aObj){
        $("#"+aObj.container+"  .btn-open-chat").off().on("click", function(){
            var nameElo = $(this).data("name-el");
            var idEl = $(this).data("id");
            var usernameEl = $(this).data("username");
            var slugEl = $(this).data("slug");
            var typeEl = dyFInputs.get($(this).data("type-el")).col;
            var openEl = $(this).data("open");
            var hasRCEl = ( $(this).data("hasRC") ) ? true : false;
            var ctxData = {
                name : nameElo,
                type : typeEl,
                id : idEl
            }
            if(typeEl == "citoyens")
                ctxData.username = usernameEl;
            else if(slugEl)
                ctxData.slug = slugEl;
            rcObj.loadChat(nameElo ,typeEl ,openEl ,hasRCEl, ctxData );
        });
    }
}