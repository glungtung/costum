costum[costum.slug].extendInit=function(){

      
         var lighterMain1 = lightenColor(costum.colors.main2,10);
         var darkerMain1 = lightenColor(costum.colors.main2,-10);
        document.documentElement.style.setProperty('--lighterMain1', lighterMain1);
        document.documentElement.style.setProperty('--darkerMain1', darkerMain1);
        dyFObj.formInMap.forced.showMap=true;
        paramsMapCO = $.extend(true, {}, paramsMapCO, {
            
            mapCustom:{
                icon: {
                    getIcon:function(params){
                        var elt = params.elt;
                        mylog.log("icone ftl", elt.tags);
                        var myCustomColour = costum.colors.main3;
                        
                    
                        var markerHtmlStyles = `
                            background-color: ${myCustomColour};
                            width: 3.5rem;
                            height: 3.5rem;
                            display: block;
                            left: -1.5rem;
                            top: -1.5rem;
                            position: relative;
                            border-radius: 3rem 3rem 0;
                            transform: rotate(45deg);
                            border: 1px solid #FFFFFF`;
                    
                        var myIcon = L.divIcon({
                            className: "my-custom-pin",
                            iconAnchor: [0, 24],
                            labelAnchor: [-6, 0],
                            popupAnchor: [0, -36],
                            html: `<span style="${markerHtmlStyles}" />`
                        });
                        return myIcon;
                    }
                },
                getClusterIcon:function(cluster){
                    var childCount = cluster.getChildCount();
                    var c = ' marker-cluster-';
                    if (childCount < 20) {
                        c += 'small-ftl';
                    } else if (childCount < 50) {
                        c += 'medium-ftl';
                    } else {
                        c += 'large-ftl';
                    }
                    return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
                }
            }
        });       //alert("extend");
        

        var main3Color = costum.colors.main3;
        document.documentElement.style.setProperty('--main3', main3Color);
        $("#main-search-bar").attr("placeholder","Quel Espace Du Faire recherchez-vous ?");
        $("#second-search-bar").attr("placeholder","Quel Espace Du Faire recherchez-vous ?");
        $('.form-register').find(".agreeContent").html("");
        costum.equipment = {
            "Bois" : {
                    "Scie à format" : "Scie à format",
                    "Scie à ruban" : "Scie à ruban",
                    "Raboteuse" : "Raboteuse",
                    "Mortaiseuse" : "Mortaiseuse",
                    "Défonceuse stationnaire" : "Défonceuse stationnaire",
                    "Calibreuse" : "Calibreuse",
                    "Perceuse à colonne" : "Perceuse à colonne",
                    "Tour à bois conventionnel" : "Tour à bois conventionnel",
                    "Tour à bois numérique" : "Tour à bois numérique",
                    "Fraiseuse Numérique 3 axes" : "Fraiseuse Numérique 3 axes",
                    "Fraiseuse Numérique 5 axes" : "Fraiseuse Numérique 5 axes",
                    "Découpeuse laser" : "Découpeuse laser",
                    "Imprimante 3D résine" : "Imprimante 3D résine",
                    "Imprimante 3D FDM" : "Imprimante 3D FDM",
                    "Electroportatif" : "Electroportatif",
                    "Petit outillage à main" : "Petit outillage à main"
                },
            "Métal" : {
                    "Cisaille" : "Cisaille",
                    "Plieuse" : "Plieuse",
                    "Presse" : "Presse",
                    "Cintreuse" : "Cintreuse",
                    "Rouleuse" : "Rouleuse",
                    "Perceuse à colonne" : "Perceuse à colonne",
                    "Scie à ruban" : "Scie à ruban",
                    "Scie fraise" : "Scie fraise",
                    "Tronçonneuse" : "Tronçonneuse",
                    "Marteau pilon" : "Marteau pilon",
                    "Roue anglaise" : "Roue anglaise",
                    "Bordureuse" : "Bordureuse",
                    "Étaux limeur" : "Étaux limeur",
                    "Tour à métaux conventionnel" : "Tour à métaux conventionnel",
                    "Tour à métaux numérique" : "Tour à métaux numérique",
                    "Découpeur plasma numérique" : "Découpeur plasma numérique",
                    "Découpeur plasma manuel" : "Découpeur plasma manuel",
                    "Découpe laser" : "Découpe laser",
                    "Découpe jet d'eau" : "Découpe jet d'eau",
                    "Découpe électro érosion" : "Découpe électro érosion",
                    "Fraiseuse conventionnelle" : "Fraiseuse conventionnelle",
                    "Fraiseuse numérique 3 axes" : "Fraiseuse numérique 3 axes",
                    "Fraiseuse numérique 5 axes" : "Fraiseuse numérique 5 axes",
                    "Poste à souder MIG/MAG" : "Poste à souder MIG/MAG",
                    "Poste à souder TIG" : "Poste à souder TIG",
                    "Poste à souder à l'arc" : "Poste à souder à l'arc",
                    "Soudeuse par point" : "Soudeuse par point",
                    "Chalumeau" : "Chalumeau",
                    "Imprimante 3D métal" : "Imprimante 3D métal",
                    "Electroportatif" : "Electroportatif",
                    "Petit outillage à main" : "Petit outillage à main"
                },
            "Céramique" : {
                    "Tour à poterie" : "Tour à poterie",
                    "Imprimante 3D" : "Imprimante 3D",
                    "Four" : "Four",
                    "Malaxeur" : "Malaxeur",
                    "Scie à ruban" : "Scie à ruban",
                    "Scie circulaire" : "Scie circulaire",
                    "Découpe jet d'eau" : "Découpe jet d'eau",
                    "Chalumeau" : "Chalumeau",
                    "Imprimante 3D Argile" : "Imprimante 3D Argile",
                    "Imprimante 3D frittage de poudre" : "Imprimante 3D frittage de poudre",
                    "Electroportatif" : "Electroportatif",
                    "Petit outillage à main" : "Petit outillage à main"
                } ,
            "Textile" : {
                    "Machine à coudre (familiale, un seul entraînement, double entrainement, triple entrainement)" : "Machine à coudre (familiale, un seul entraînement, double entrainement, triple entrainement)",
                    "Bordureuse" : "Bordureuse",
                    "Surjeteuse industrielle" : "Surjeteuse industrielle",
                    "Surjeteuse familiale" : "Surjeteuse familiale",
                    "Brodeuse numérique" : "Brodeuse numérique",
                    "Tricoteuse conventionnelle" : "Tricoteuse conventionnelle",
                    "Tricoteuse numérique" : "Tricoteuse numérique",
                    "Cardeuse" : "Cardeuse",
                    "Imprimante textile" : "Imprimante textile",
                    "Métier à tisser" : "Métier à tisser",
                    "Découpe fil chaud" : "Découpe fil chaud",
                    "Petit outillage à main" : "Petit outillage à main"
                },
            "Communication" : {
                    "Appareil photo" : "Appareil photo",
                    "Caméra" : "Caméra",
                    "Eclairage " : "Eclairage",
                    "Fond vert" : "Fond vert",
                    "Station de montage" : "Station de montage",
                    "Station de modélisation" : "Station de modélisation",
                    "Poste informatique général" : "Poste informatique général",
                    "Table de mixage" : "Table de mixage",
                    "Sonorisation" : "Sonorisation",
                    "Microphone" : "Microphone"
                },
            "Electronique" : {
                    "Oscilloscope" : "Oscilloscope",
                    "Analyseur de spectre" : "Analyseur de spectre",
                    "Analyseur de réseau" : "Analyseur de réseau",
                    "Alimentation stabilisée" : "Alimentation stabilisée",
                    "Générateur de fréquence" : "Générateur de fréquence",
                    "Station de soudure" : "Station de soudure",
                    "Imprimante à circuit" : "Imprimante à circuit",
                    "Four à refusion" : "Four à refusion",
                    "Fraiseuse à circuit" : "Fraiseuse à circuit",
                    "Insoleuse + bac à révélation des circuits imprimés" : "Insoleuse + bac à révélation des circuits imprimés",
                    "Machine pick and place" : "Machine pick and place",
                    "Multimètre" : "Multimètre",
                    "cartes de prototypage (arduino, raspberry etc)" : "cartes de prototypage (arduino, raspberry etc)",
                    "Petit outillage à main" : "Petit outillage à main"
                },
            "Foodlab" : {
                    "Cuisine équipée" : "Cuisine équipée",
                    "Four particulier" : "Four particulier",
                    "Moulin" : "Moulin",
                    "Robot particulier" : "Robot particulier",
                    "Chambre froide" : "Chambre froide",
                    "Laboratoire de transformation" : "Laboratoire de transformation",
                    "Séchoir" : "Séchoir",
                    "Fumoir" : "Fumoir",
                    "Stérilisateur" : "Stérilisateur"
                },
            "Plastique" : {
                    "Broyeuse" : "Broyeuse", 
                    "Extrudeuse" : "Extrudeuse", 
                    "Presse de compression" : "Presse de compression", 
                    "Presse d'injection manuel" : "Presse d'injection manuel", 
                    "Thermoformeuse" : "Thermoformeuse"
                },
            "Autre" : {
                    "Nettoyeur haute pression" : "Nettoyeur haute pression",
                    "Engin de levage" : "Engin de levage",
                    "Tracteur" : "Tracteur",
                    "Autre véhicule" : "Autre véhicule",
                    "Sableuse" : "Sableuse",
                    "Cabine de peinture" : "Cabine de peinture",
                    "Autre imprimante 3D" : "Autre imprimante 3D",
                    "Bras robotisé" : "Bras robotisé"
                }
        };

    costum.equipmentArr=[];
    $.each(costum.equipment,function(mat,equip){
        costum.equipmentArr= costum.equipmentArr.concat(Object.values(equip)); 
    });

    var colorRff={
        green:"#00ba53",
        blue:"#4d56b0",
        rouge:"#e50023"
    };
    var mapDynf2Answer = {
        "name" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7ox2q6lbmkpg58zmw7",
        "shortDescription" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7ox93a3ahuh4m3tw86",
        "email" : "rfflabs192022_1213_0.multitextvalidationrfflabs592022_1727_1l7pu3a5vnykycffllml",
        "url" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7pt66bwj56ln8c0b6g",
        "mobile" : "rfflabs192022_1213_0.multitextvalidationrfflabs592022_1727_1l7pu5pkqrjzjhof8gfq",
        "video" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7oy5dcm97goppm1077",
        //"openingHours" : "rfflabs592022_1727_1.rfflabs592022_1727_1l7ptpt0jpec0lketz67",
        "address" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7pt78jor1cjxf9eppa",
        "socialNetwork" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7oydd71z9dxl2qq5s",
        "tags" : {
            "typePlace" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7ox66g90kqn43z40pj",
            "state" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7ox40xporju9s1bm7i",
            "manageModel" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7ptsamkoia6ziy8d0q",
            "thirdPlaceNetwork" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7pu83wkl6socwjosuh",
            "fablabNetwork" : "rfflabs192022_1213_0.rfflabs592022_1727_1l7pu8qe36o03b7tvczx",
            "services" : "rfflabs692022_924_3.rfflabs692022_924_3l7pvlmexz6f6l9swb9",
            "greeting" : "rfflabs692022_924_3.rfflabs692022_924_3l7pw8ohaohy9j0mvrc"
        }
        // "openingHours" : "rfflabs592022_1727_1.rfflabs592022_1727_1l89m9k3hvepct3jvsul"
    };

    var mapAns2Elem = {
        "multiCheckboxPlusrfflabs692022_104_4l7pwv5ibvllnc6ac7kr" : "Bois",
        "multiCheckboxPlusrfflabs692022_104_4l7px2crz283plhollbu" : "Métal",
        "multiCheckboxPlusrfflabs692022_104_4l7pxf69b1cts6ex1lgzi" : "Céramique",
        "multiCheckboxPlusrfflabs692022_104_4l7pxjw1vdwnzvutctqw" : "Textile",
        "multiCheckboxPlusrfflabs692022_104_4l7pxpum739uqh4d5r81" : "Communication",
        "multiCheckboxPlusrfflabs692022_104_4l7pyxtjzn7cjhjm3t0q" : "Electronique",
        "multiCheckboxPlusrfflabs692022_104_4l7pz6trp6tn9s27drwx" : "Foodlab",
        "multiCheckboxPlusrfflabs692022_104_4l7pzcouksinvsyve53" : "Plastique",
        "multiCheckboxPlusrfflabs692022_104_4l7pze7bqeqjblrouepg" : "Autre"

    };

    $.each(mapAns2Elem, function(kunik,mat){
        costum[kunik+"Callback"] = function (params){
        
            equipObj={};
            $.each(params.text,function(ind,equip){
                mylog.log("equip",Object.keys(equip)[0]);
                key=equip[Object.keys(equip)[0]].value;
                val=(equip[Object.keys(equip)[0]].type=="simple") ? equip[Object.keys(equip)[0]].value : equip[Object.keys(equip)[0]].textsup;
                equipObj[key]=val;

            });
            if(typeof params.elt.links.organizations !="undefined" && Object.keys(params.elt.links.organizations).length>0){
                var idElem=Object.keys(params.elt.links.organizations)[0];
                var typeElem=params.elt.links.organizations[Object.keys(params.elt.links.organizations)[0]].type;
                var equiElem={
                    id:idElem,
                    collection :typeElem, 
                    path : "equipment."+mat,
                    value : equipObj
                }
                dataHelper.path2Value(equiElem , function(params) { 
                
        
                });
            }      
        };
         
    });

    costum.nationalRffProjects=[
        "Le Mois de la Fabrication Distribuée",
        "eXtrême Défi",
        "Métiers de Demain",
        "Participation à un Groupe de Travail du RFFLabs"
    ];

    costum.multiCheckboxPlusrfflabs692022_924_3l7pwft8el3oq001tkjCallback = function(params){
        if(params.text.length>0){
            var rffProj=[];
            $.each(params.text, function(ind,val){
                mylog.log("yooyo",Object.keys(val)[0]);
                if(costum.nationalRffProjects.includes(Object.keys(val)[0])){
                    rffProj.push(Object.keys(val)[0]);            
                }
            });
            if(typeof params.elt.links.organizations !="undefined" && Object.keys(params.elt.links.organizations).length>0){
                var idElem=Object.keys(params.elt.links.organizations)[0];
                var typeElem=params.elt.links.organizations[Object.keys(params.elt.links.organizations)[0]].type;  
                var projTagsElem={
                    id:idElem,
                    collection :typeElem, 
                    path : "tags",
                    value : {'$each' : rffProj},
                    arrayForm : true
                };
                dataHelper.path2Value(projTagsElem , function(params) {         
                });
            }    

        }        
    }

    costum[costum.slug].organizations.afterBuild = function(data){
        $('.timeInput').focus(function (){
            $(this).timepicker('showWidget');
        });
        $('.addHoursRange').hide();
    };

        

    // ajaxPost(
    //     null, 
    //     baseUrl+"/costum/blockgraph/getdashboarddata", 
    //     {
    //         "costumId":costum.contextId, 
    //         "costumSlug": costum.contextSlug,
    //         "costumType":costum.contextType,
    //         "specificBlock":[]
    //     }, 
    //     function(res){
    //         costum["dashboardData"] = res["data"];
    //         costum["dashboardGlobalConfig"] = res["global"];
    //     },
    //     null,
    //     null,
    //     {async:false}
    // );

    mylog.log("extendInit costum",costum);

    costum.typeObj.organizations.dynFormCostum.prepData=function(data){
        //alert("afterbuild");
        var listObj=costum.lists;
        delete listObj.network;
        $.each(listObj, function(e, v){
            constructDataForEdit=[];
            $.each(v, function(i, tag){
                if($.inArray(tag, data.map.tags) >=0){
                    constructDataForEdit.push(tag);
                 
                    data.map.tags.splice(data.map.tags.indexOf(tag),1);
                }
            });
            data.map[e]=constructDataForEdit;
        });

         mylog.log("beforeBuild rff",data);


        $.each(costum.typeObj.organizations.dynFormCostum.beforeBuild.properties,function(key,val){
            if(val.inputType=="openingHours"){
                costum.typeObj.organizations.dynFormCostum.beforeBuild.properties[key].init=dyFInputs[val.inputType];
            }
            else if(key=="video"){
                costum.typeObj.organizations.dynFormCostum.beforeBuild.properties[key].init=dyFInputs.videos.init;
            }

                //uploader

        });

        return data;

    };

    waitForAfterSave();
    
    
        



        function waitForAfterSave(){
            if(typeof costum[costum.slug].organizations !="undefined" && typeof costum[costum.slug].organizations.afterSave == "function"){             

                costum[costum.slug].organizations.afterSave = function(data){
                    uploadObj.afterLoadUploader=false;

                    if(typeof answerObj!="undefined" && typeof answerObj._id.$id!="undefined"){
                        var savedData = data.map;
                        var tplCtx = {
                            id : answerObj._id.$id,
                            collection : "answers"
                        };
                        $.each(savedData , function(key,val){
                            if(typeof mapDynf2Answer[key]!="undefined"){
                            tplCtx.path = "answers."+mapDynf2Answer[key];
                            tplCtx.value = val;
                            if(key=="tags"){
                                $.each(costum.lists,function(kl,vl){
                                  if(Object.keys(mapDynf2Answer[key]).includes(kl)){  
                                    var filteredValues = val.filter(value => vl.includes(value));
                                    mylog.log("tags'common values",kl,filteredValues);
                                      if(kl=="state" || kl=="manageModel"){
                                        filteredValues=filteredValues[0];
                                    }
                                    tplCtx.path = "answers."+mapDynf2Answer[key][kl];
                                    tplCtx.value = filteredValues;
                                    dataHelper.path2Value( tplCtx, function(params) { 
                                        var stepId= mapDynf2Answer[key][kl].split(".")[0] ;  
                                        var inputKey=mapDynf2Answer[key][kl].split(".")[1];                       
                                        reloadInput(inputKey, stepId);
                                    });
                                  }  

                                });
                            }
                            else if(key=="address"){
                                ansAddrObj={};
                                val["name"] = val["streetAddress"]+", "+val["postalCode"]+", "+val["addressLocality"]+", "+val["level1Name"]
                                ansAddrObj[key]=val;
                                tplCtx.value=ansAddrObj;
                            }
                            else if(key=="socialNetwork"){
                                var socialTable = [];
                                socialTable.push(["Plateformes","Url"]);
                                $.each(val, function(ind,sonet){
                                    socialTable.push([sonet["platform"],sonet["url"]]);
                                });
                                tplCtx.value=socialTable;
                            }
                            else if(key=="video"){
                                tplCtx.value=val[0];
                            }

                            if(key!="tags"){
                            
                                dataHelper.path2Value( tplCtx, function(params) { 
                                    var stepId= mapDynf2Answer[key].split(".")[0] ;  
                                    var inputKey=mapDynf2Answer[key].split(".")[1];                       
                                    reloadInput(inputKey.substring(inputKey.length-39), stepId);
                                });

                            }    
                        

                        }
                        });
                    }

                    dyFObj.commonAfterSave(data, function(params){
                        mylog.log("callbackcommonAfterSave",params,uploadObj);
                        dyFObj.closeForm();
            // ------TO DO save file in answer---------
                        // setTimeout(
                        // ajaxPost(
                        //     null,
                        //     baseUrl+"/"+moduleId+"/document/getlistbyid/type/"+params.map.collection+"/id/"+params.id,
                        //     {},
                        //     function(data){
                        //         if(Object.keys(data).length>0 && typeof(answerObj)!="undefined" && typeof(answerObj._id.$id)!="undefined"){
                        //             var imgObj=data[Object.keys(data)[0]];
                        //             imgObj.subKey= "rfflabs592022_1727_1.rfflabs592022_1727_1l7oxg2gx8vmzv749k4";
                        //             imgObj.restricted= "true";
                        //             imgObj.id = answerObj._id.$id;
                        //             imgObj.type = "answers";
                        //             imgObj.folder = "answers/"+answerObj._id.$id+"/restricted/file";
                        //             imgObj.contentKey = "presentation";
                        //             delete imgObj.current;

                        //             ajaxPost(
                        //                 null,
                        //                 baseUrl+"/"+moduleId+"/document/save",
                        //                 imgObj,
                        //                 function(data){
                        //                     alert("saved doument");
                        //                 });
                        //         }

                        //     },
                        //     null,
                        //     null,
                        //     {async:false}
                        // ),8000);



                    });
                };    

            }
            else{
                setTimeout(waitForAfterSave, 250);
            }
        }

            
    


};