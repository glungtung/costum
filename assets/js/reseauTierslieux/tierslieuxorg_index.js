costum[costum.slug].extendInit=function(){

    // ajaxPost(
    //     null, 
    //     baseUrl+"/costum/blockgraph/getdashboarddata", 
    //     {
    //         "costumId":costum.contextId, 
    //         "costumSlug": costum.contextSlug,
    //         "costumType":costum.contextType,
    //         "specificBlock":[]
    //     }, 
    //     function(res){
    //         costum["dashboardData"] = res["data"];
    //         costum["dashboardGlobalConfig"] = res["global"];
    //     },
    //     null,
    //     null,
    //     {async:false}
    // );
    
    dyFObj.formInMap.forced.showMap=true;
    paramsMapCO = $.extend(true, {}, paramsMapCO, {
        activePreview: true,
        mapCustom:{
            icon: {
                getIcon:function(params){
                    var elt = params.elt;
                    mylog.log("icone ftl", elt.tags);
                    var myCustomColour = costum.colors.main1;
                    
                
                    var markerHtmlStyles = `
                        background-color: ${myCustomColour};
                        width: 3.5rem;
                        height: 3.5rem;
                        display: block;
                        left: -1.5rem;
                        top: -1.5rem;
                        position: relative;
                        border-radius: 3rem 3rem 0;
                        transform: rotate(45deg);
                        border: 1px solid #FFFFFF`;
                
                    var myIcon = L.divIcon({
                        className: "my-custom-pin",
                        iconAnchor: [0, 24],
                        labelAnchor: [-6, 0],
                        popupAnchor: [0, -36],
                        html: `<span style="${markerHtmlStyles}" />`
                    });
                    return myIcon;
                }
            },
            getClusterIcon:function(cluster){
                var childCount = cluster.getChildCount();
                var c = ' marker-cluster-';
                if (childCount < 20) {
                    c += 'small-ftl';
                } else if (childCount < 50) {
                    c += 'medium-ftl';
                } else {
                    c += 'large-ftl';
                }
                return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
            },
            getPopup: function(data){
                var id = data._id ? data._id.$id:data.id;
                var imgProfil = mapCO.getOptions().mapCustom.getThumbProfil(data);

                var eltName = data.title ? data.title:data.name;
                var popup = "";
                popup += "<div class='padding-5' id='popup" + id + "'>";
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                if(data.tags && data.tags.length > 0){
                    popup += "<div style='margin-top : 5px;'>";
                    var totalTags = 0;
                    $.each(data.tags, function(index, value){
                        
                        if (totalTags < 2 && value!=="Compagnon France Tiers-Lieux") {
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                            totalTags++;
                        }
                    })
                    popup += "</div>";
                }
                if(data.address){
                    var addressStr="";
                    if(data.address.streetAddress)
                        addressStr += data.address.streetAddress;
                    if(data.address.postalCode)
                        addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                    if(data.address.addressLocality)
                        addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                    popup += "<div class='popup-address text-dark'>";
                    popup +=    "<i class='fa fa-map-marker'></i> "+addressStr;
                    popup += "</div>";
                }
                if(data.shortDescription && data.shortDescription != ""){
                    popup += "<div class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Description</div>";
                    popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                    popup += "</div>";
                }
                if((data.url && typeof data.url == "string") || data.email || data.telephone){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contacts</div>";
                    
                    if(data.url && typeof data.url === "string"){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                        popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                        popup += "</div>";
                    }

                    if(data.email){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                        popup += "</div>";
                    }

                    if(data.telephone){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-phone fa_phone'></i> ";
                        var tel = ["fixe", "mobile"];
                        var iT = 0;
                        $.each(tel, function(keyT, valT){
                            if(data.telephone[valT]){
                                $.each(data.telephone[valT], function(keyN, valN){
                                    if(iT > 0)
                                        popup += ", ";
                                    popup += valN;
                                    iT++; 
                                })
                            }
                        })
                        popup += "</div>";
                    }

                    popup += "</div>";
                    popup += "</div>";
                }
                var url = '#page.type.' + data.collection + '.id.' + id;
                popup += "<div class='popup-section'>";
                if(paramsMapCO.activePreview)
                    popup += "<a href='" + url + "' class='lbh-preview-element item_map_list popup-marker' id='popup" + id + "'>";
                else
                    popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
                popup += '<div class="btn btn-sm btn-more col-md-12">';
                popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                popup += '</div></a>';
                popup += '</div>';
                popup += '</div>';

                return popup;
            }
        }
    });

      
    

    waitForAfterSave();
    
    
    
        



        function waitForAfterSave(){
            if(typeof costum[costum.slug].organizations !="undefined" && typeof costum[costum.slug].organizations.afterSave == "function"){             

                costum[costum.slug].organizations.afterSave = function(data){
                    uploadObj.afterLoadUploader=false;

                   

                    dyFObj.commonAfterSave(data, function(params){
                        mylog.log("callbackcommonAfterSave",params,uploadObj);
                        dyFObj.closeForm();
            // ------TO DO save file in answer---------
                        // setTimeout(
                        // ajaxPost(
                        //     null,
                        //     baseUrl+"/"+moduleId+"/document/getlistbyid/type/"+params.map.collection+"/id/"+params.id,
                        //     {},
                        //     function(data){
                        //         if(Object.keys(data).length>0 && typeof(answerObj)!="undefined" && typeof(answerObj._id.$id)!="undefined"){
                        //             var imgObj=data[Object.keys(data)[0]];
                        //             imgObj.subKey= "rfflabs592022_1727_1.rfflabs592022_1727_1l7oxg2gx8vmzv749k4";
                        //             imgObj.restricted= "true";
                        //             imgObj.id = answerObj._id.$id;
                        //             imgObj.type = "answers";
                        //             imgObj.folder = "answers/"+answerObj._id.$id+"/restricted/file";
                        //             imgObj.contentKey = "presentation";
                        //             delete imgObj.current;

                        //             ajaxPost(
                        //                 null,
                        //                 baseUrl+"/"+moduleId+"/document/save",
                        //                 imgObj,
                        //                 function(data){
                        //                     alert("saved doument");
                        //                 });
                        //         }

                        //     },
                        //     null,
                        //     null,
                        //     {async:false}
                        // ),8000);



                    });
                };    

            }
            else{
                setTimeout(waitForAfterSave, 250);
            }
        }

    // $("#main-search-bar").attr("placeholder","Quel KPA recherchez-vous ?");
    // $("#second-search-bar").attr("placeholder","Quel KPA recherchez-vous ?");    

            
    


};