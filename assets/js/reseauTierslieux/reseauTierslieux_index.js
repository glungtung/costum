
 var main1Color = costum.colors.main1;
 var main2Color = costum.colors.main2;
 var scopeCountryParam= ["FR","RE","MQ","GP","GF","YT"];
 var scopeLevelParam = ["3"];
  var scopeLevelParam = "";
 getDataCostum();
 function paramsMapLatLon(lat,lon){
	mylog.log("latlon",lat,lon);
	// forced coordinates centered on FR or RE
	// lat = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? "-21.115141" : "46.7342232";
	// lon = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ?	"55.536384" : "2.74686000";
	// zoom = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? 9 : 5 ;
	// //
	// dyFObj.formInMap.forced.countryCode=(costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? "RE" : "FR";
	// 	dyFObj.formInMap.forced.map={"center":[lat, lon],"zoom" : zoom};
	// 	dyFObj.formInMap.forced.showMap=true;
	paramsMapCO = $.extend(true, {}, paramsMapCO, {
		
		mapCustom:{
			icon: {
				getIcon:function(params){
					var elt = params.elt;
					mylog.log("icone ftl", elt.tags);
					// if (typeof elt.tags != "undefined" && elt.tags != null && typeof costum.settings!="undefined" && typeof costum.settings.compagnonFtl!="undefined" && costum.settings.compagnonFtl){
					// 	if($.inArray("Compagnon France Tiers-Lieux", elt.tags)==-1){
					// 		var myCustomColour = main1Color;
					// 	}else {
					// 		var myCustomColour = main2Color;
					// 	}
					// }else{
						var myCustomColour = main1Color;
					// }
				
					var markerHtmlStyles = `
						background-color: ${myCustomColour};
						width: 3.5rem;
						height: 3.5rem;
						display: block;
						left: -1.5rem;
						top: -1.5rem;
						position: relative;
						border-radius: 3rem 3rem 0;
						transform: rotate(45deg);
						border: 1px solid #FFFFFF`;
				
					var myIcon = L.divIcon({
						className: "my-custom-pin",
						iconAnchor: [0, 24],
						labelAnchor: [-6, 0],
						popupAnchor: [0, -36],
						html: `<span style="${markerHtmlStyles}" />`
					});
					return myIcon;
				}
			},
			getClusterIcon:function(cluster){
				var childCount = cluster.getChildCount();
				var c = ' marker-cluster-';
				if (childCount < 20) {
					c += 'small-ftl';
				} else if (childCount < 50) {
					c += 'medium-ftl';
				} else {
					c += 'large-ftl';
				}
				return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
			}
		}
	});
	if (notNull(lat) && notNull(lon)){
		paramsMapCO.forced =
			{
				latLon : [ 
			    	lat,
			    	lon
			    ]
			};
	}		
	//mapObj.init(paramsMapCO);

 }

$("#show-button-map").addClass("changelabel bg-main1");
$("#main-search-bar").attr("placeholder","Quel tiers-lieu recherchez-vous ?");

$("#btn-filters").parent().css('display','none');

//$("#input_name_filter").attr("placeholder","Filtre par nom ...");
$('.form-register').find(".agreeMsg").removeClass("letter-red pull-left").html("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie.");
//$(".form-email-activation .container .row div:first-child .name img").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");


var changeLabel = function() {
		$("#menuRight").on("click", function() {
				    if($(this).find("a").hasClass("changelabel")) {
				    	$(this).find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
				    }
				    else{
				    	$(this).find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
				    }
				//    text.nodeValue =  "Afficher "+ ($(this).find("a").hasClass("changelabel") ? "chap" : "la carte");
				    $(this).find("a").toggleClass("changelabel");
 			  	});
	}


function getDataCostum(){
			ajaxPost(
				null,
				baseUrl+"/co2/element/get/type/"+costum.contextType+"/id/"+costum.contextId,
				null,
				function(data){
					mylog.log("callback contextorga",data);
					mylog.log("costum object",costum);
					if (typeof data.map.level!="undefined"){
						var data = data.map;
						var level=data.level;
						//alert(level);

						var levelId = data.address[level]
						//alert(levelId);
						var levelNb = level.substr(5);

						if (typeof levelId=="undefined" && levelNb=="5"){
							alert("Nous ne trouvons pas la Métropole au sein de laquelle votre réseau agit. Nous vous proposons une portée départementale. Veuillez s'il vous plaît nous contacter afin de nous remonter la problématique à contact@communecter.org.")
							levelNb=levelNb-1;
							levelId=data.address["level"+levelNb];
						}

						
						//var scopeLevel = 
						//if(level!="level1"){
							scopeCountryParam = [data.address.addressCountry];
							scopeLevelParam = (level=="1") ? [Number(levelNb)+2] : [Number(levelNb)+1];
							if(data.address.addressCountry!="FR"){
								scopeLevelParam=["6"];

							}
							upperLevelId=levelId;
							costum.address=data.address;
							costum.parentId=data._id.$id;
						//}
						//alert(level);
						ajaxPost(
							null, 
							baseUrl+"/costum/blockgraph/getdashboarddata", 
							{
								"costumId":costum.contextId,  
								"costumSlug": costum.contextSlug,
								"costumType":costum.contextType,
								"specificBlock":[],
								"scopeId":upperLevelId
							}, 
							function(res){
								costum["dashboardData"] = res["data"];
								costum["dashboardGlobalConfig"] = res["global"];
							},
							null,
							null,
							{async:false}
						);
						getZone(data.address[level],level);	
						referenceTl(levelNb,data.address[level]);
					}		        	
				}, 
				null,
				null,
				{async:false}
			);
		}
function getZone(zoneId,level){
			mylog.log("getzone",zoneId,level);
			ajaxPost(
				null,
				baseUrl+"/co2/zone/getscopebyids",
				{zones:{zoneId}},
				function(data){ 
					//alert(level);
					var coord = {};
					mylog.log("scopes",data.scopes[zoneId+level].latitude);
					var latit = (typeof data.scopes[zoneId+level].latitude !="undefined") ? data.scopes[zoneId+level].latitude : null;
					//var longit = ;
					coord = {
						latitude : data.scopes[zoneId+level].latitude,
						longitude : data.scopes[zoneId+level].longitude
					};
					mylog.log("coord1",coord);

					//Ne pas forcer coordonnée
					//coord.latitude,coord.longitude
					paramsMapLatLon();						          	
				}, 
				null,
				null,
				{async:false}
			);
		}

function referenceTl(levelNb,levelId){
	mylog.log("referenceTl",levelNb,levelId);
	
	
	if (typeof costum.loaded=="undefined" || costum.loaded==false){
		// alert("here");
		
		var dataCustom = {};
		dataCustom.id = costum.contextId;
		dataCustom.collection = costum.contextType;
		dataCustom.path = "costum.loaded";
		dataCustom.value=true;
		dataCustom.tplGenerator=true;
 		dataHelper.path2Value(dataCustom,function(response){
	        if(response.result){
				toastr.success("added");
	            mylog.log("loaded costum",response);       	
			}
	    });
	 //    dataCustom.path = "costum.settings.compagnonFtl";
		// dataCustom.value=false;
		// //dataCustom.tplGenerator=true;
 	// 	dataHelper.path2Value(dataCustom,function(response){
	 //        if(response.result){
		// 		//toastr.success("added");
	 //            mylog.log("default setting for compagnon : false",response);       	
		// 	}
	 //    });
	 

		
		// var originId = (typeof window.location.href.split('originId=').pop()!="undefined") ? window.location.href.split('originId=').pop() : "" ;		
		// if(originId!=""){
		// 	ajaxPost(
		// 		null,
		// 		baseUrl+"/survey/form/duplicatetemplate/type/template/templateCostumId/"+originId+"/active/true",
		// 		null,
		// 		function(data){
		// 			toastr.success("Formulaire dupliqué");
		// 			mylog.log("Formulaire dupliqué",data);
		// 			if(typeof(costum.dashboard.config)!="undefined"){
		// 				$.each(costum.dashboard.config, function(k,v){
		// 			    	if(typeof(v.graph.data.coform)!="undefined"){
		// 			       		v.graph.data.coform=data.map._id.$id;
		// 				    }
		// 				});
		// 				dataCustom.path = "costum.dashboard";
		// 				dataCustom.value=costum.dashboard;
		// 				dataHelper.path2Value(dataCustom,function(response){
		// 			        if(response.result){
		// 						toastr.success("added");
		// 			            mylog.log("loaded costum",response);       	
		// 					}
		// 			    });
		// 			}
					
	    //             data=data.map;
		// 		},
		// 		null,
		// 		null,
		// 		{async:false}
		// 	);
		// }
		
		// ajaxPost(
		// 	null,
		// 	baseUrl+"/costum/blockcms/getcmsbywhere",
		// 	{"source_key" : costum.assetsSlug, type:"template" },
		// 	function(data){
		// 		mylog.log("get cms template callback",data);
		// 		var key = Object.keys(data)[0];
		// 		var par = data[key];
		// 		mylog.log("get cms template callback",par);
		// 		var params = {
		// 	        "cmsList" : (par['cmsList'])?par['cmsList']:"",
		// 	        "tplParent"  : par._id.$id,
		// 	        "page"       : par.page,
		// 	        "parentId"   : costum.contextId,
		// 	        "parentSlug" : costum.contextSlug,
		// 	        "parentType" : costum.contextType,
		// 	        "action" : "duplicate" 
		// 	    }

		// 	    dataCustom.id = par._id.$id;
		// 		dataCustom.collection = "cms";
		// 		dataCustom.path = "tplsUser."+costum.contextId+".welcome";
		// 		dataCustom.value="using";
		// 		dataCustom.tplGenerator=true;
		// 		dataHelper.path2Value(dataCustom,function(response){
		// 			if(response.result){
		// 			toastr.success("Modèle graphique importé");
		// 			mylog.log("tplsUSER ADDED",response);       	
		// 			}
		// 		});

		// 		ajaxPost(
		// 			null,
		// 			baseUrl+"/costum/blockcms/getcms",
		// 			params,
		// 			function(data){
		// 				mylog.log("duplicate cms template callback",data);


		// 				//  	var dataCustom = {};
		// 				// //dataCustom.arrayForm = true;

		// 				// // duplicate coform
		// 				// var contextParams = {
		// 				//        "contextId"   : costum.contextId,
		// 				//        "contextSlug" : costum.contextSlug,
		// 				//        "contextType" : costum.contextType,
		// 				//        "creator" : userId
		// 				//    }

		// 				// $.ajax({
		// 				//        type : 'POST',
		// 				//        data : contextParams,
		// 				//        url : baseUrl+"/costum/tierslieux/duplicateform",
		// 				//        dataType : "json",
		// 				//        async : false,
		// 				//        success : function(data){
		// 				//        	toastr.success("Terminé!!");
		// 				//        	//document.location.reload();
		// 				//        	bootbox.dialog({message:`<div class="alert-white text-center">
		// 				// 			<br>
		// 				// 			<strong>Vous pouvez maintenant configurer le formulaire de connaissance de vos tiers lieux</strong>
		// 				// 			<br>
		// 				// 			<br>
		// 				// 			<a href="${baseUrl}/#${costumSlug}.view.forms" class="btn btn-info">Aller à la paramètrage du formulaire</a>
		// 				// 			</div>`}
		// 				// 		);
		// 				//        }
		// 				//    });
		// 			},
		// 			null,
		// 			"json",
		// 			{async : false}
		// 		);
		// 	},
		// 	null,
		// 	null,
		// 	{async:false}
		// );
		//urlCtrl.loadByHash("#welcome");

		// if(Object.keys(refTags).length==0 && typeof(costum.reference)!="undefined" && typeof(costum.reference.themes)!="undefined"){
		// 	refTags=costum.reference.themes;
		// }	

		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/search/globalautocomplete",
			{
				"searchType" : "templates",
				"sourceKey" : costum.assetsSlug,
				"filters" : {
					"subtype" : "site"
				}
			},
			function(data){
				var networkTplId=Object.keys(data.results)[0];
				var params = {
					"id"		 : networkTplId,
					// "page"		 : page,
					"collection" : "templates",
					"parentId"   : costum.contextId,
                    "parentSlug" : costum.contextSlug,
                    "parentType" : costum.contextType,
					"action" : ""
				}
				ajaxPost(
					null,
					baseUrl + "/co2/cms/usetemplate",
					params,
					function (data) {
						window.location.reload()
					},
					null,
					null,
					{ async: false }
				);

			},
			null,
			null,
			{async : false}
		);
		var refTags = {};

		
		
	}

	if(location.hash.indexOf("?") > -1){
		searchInterface.init();
	 }	

		if (typeof searchObject.tags !="undefined" && searchObject.tags.length>0){
			mylog.log("searchObject.tags",searchObject.tags);
			refTags = searchObject.tags;
		}else{
			var allTypePlace=costum.lists.typePlace;
			allTypePlace.pop();
			refTags=allTypePlace;
		}

		mylog.log("refTags",refTags);
		// No matter which typeplace thirdPlace
		// if(Object.keys(refTags).length>0){
			//alert("fere");

			
			// ajaxPost(
			// 			null,
			// 			baseUrl+"/costum/francetierslieux/multireference/type/organizations",
			// 			{zoneNb:levelNb,zoneId:levelId,tags:[]},
			// 			function(data){ 
			// 				mylog.log("taggedTl",data);	  
			// 			}, 
			// 			null,
			// 			null,
			// 			{async:false}
			// 		);
		// }
	
	
	
}

function lightenColor(color, percent) {
var num = parseInt(color.replace("#",""),16),
amt = Math.round(2.55 * percent),
R = (num >> 16) + amt,
B = (num >> 8 & 0x00FF) + amt,
G = (num & 0x0000FF) + amt;
return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
};

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? 
    parseInt(result[1], 16)+","+
    parseInt(result[2], 16)+","+
    parseInt(result[3], 16)
  : null;
}



 var lighterMain1 = lightenColor(main1Color,10);
 var darkerMain1 = lightenColor(main1Color,-10);
 var lightestMain1Rgb = hexToRgb(costum.colors.main1);

		  				

dyFObj.unloggedMode=true;
costum[costum.slug]={
	init : function(){	
		var contextData=null;
		 var scopeCountryParam= ["FR","RE","MQ","GP","GF","YT"];
 		var scopeLevelParam = ["3"];
 		var upperLevelId = "";
 		//costum.address={};
		document.documentElement.style.setProperty('--main1', main1Color);
		document.documentElement.style.setProperty('--main2', main2Color);
		document.documentElement.style.setProperty('--lighterMain1', lighterMain1);
		document.documentElement.style.setProperty('--lightestMain1Rgb', lightestMain1Rgb);
		document.documentElement.style.setProperty('--darkerMain1', darkerMain1);
		
		changeLabel();
		//getNetwork();

		function getNetwork(){
		    mylog.log("---------getNetwork");

		    var params = {
		        contextId : costum.contextId,
		        contextType : costum.contextType
		    };

		    ajaxPost(
		        null,
		        baseUrl + "/costum/reseautierslieux/getnetwork",
		        params,
		        function (data) {
		        	//alert("dd");
		        	mylog.log("callbacknet",data);
		        	$.each(data, function(e, v){
		        		var posNet=$.inArray(v.name,costum.lists.network);
		        		if (posNet>-1){
		        			mylog.log("networks",posNet,costum.lists.network);
		        			costum.lists.network.splice(posNet, 1);


		        			// var posNet=	$.inArray(v,costum.lists.network);
		        			// alert(posNet);
		        		}
		        	});	
		        }
		    );    

		}


		costum.typeObj.organizations.dynFormCostum.prepData=function(data){
		    //alert("afterbuild");
		    var listObj=costum.lists;
		    // delete listObj.network;
		    $.each(listObj, function(e, v){
		        constructDataForEdit=[];
		        $.each(v, function(i, tag){
		            if($.inArray(tag, data.map.tags) >=0){
		                constructDataForEdit.push(tag);
		             
		                data.map.tags.splice(data.map.tags.indexOf(tag),1);
		            }
		        });
		        data.map[e]=constructDataForEdit;
		    });

		     mylog.log("beforeBuild rff",data);

		    // $.each(costum.typeObj.organizations.dynFormCostum.beforeBuild.properties,function(key,val){
		    //     if(val.inputType=="openingHours"){
		    //         costum.typeObj.organizations.dynFormCostum.beforeBuild.properties[key].init=dyFInputs[val.inputType];
		    //     }
		    //     else if(key=="video"){
		    //         costum.typeObj.organizations.dynFormCostum.beforeBuild.properties[key].init=dyFInputs.videos.init;
		    //     }
		    // });

		    return data;

		};


		if(typeof costum[costum.slug].extendInit=="function"){
		    costum[costum.slug].extendInit();
		}

	},
	"organizations" : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData In",data);

			if(typeof finder != "undefined" && Object.keys(finder.object).length > 0){
				$.each(finder.object, function(key, object){
					if(typeof formData[key]!="undefined"){
						delete formData[key];
					}
				});
			}

			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string"){
							if(e=="manageModel" && v=="Autre"){
								v="Autre mode de gestion";
							}
							data.tags.push(v);
							
						}
							
						else{
							$.each(v, function(i,tag){
								if(e=="typePlace" && tag=="Autre"){
									tag="Autre famille de tiers-lieux";
								}
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			if(data.tags.includes("Autre") || data.tags.includes("Autres")){
				$.each(data.tags,function(i,tag){
                    if(tag=="Autre" || tag=="Autres"){
                    	delete data.tags[i];
                    }
				});
			}

			if(data.socialNetwork){
				$.each(data.socialNetwork,function(i,net){
					if(typeof net["url"]=="undefined" || net["url"]==""){
						delete data.socialNetwork[i];
					}
				})
			}

            mylog.log("formData return",data);
			return data;
		},
		afterBuild : function(data){
			mylog.log("afterbuild",data);
			if(data.extraManageModel){
			    $("#ajaxFormModal .extraManageModeltext").show();
			}
			if(data.extraTyplePlace){
			    $("#ajaxFormModal .extraTypePlacetext").show();
			}

			$("#divCity input").attr("placeholder","Saisir votre code postal");

			            

			//mylog.log("manageModel param",typeof param, param);
			$("#ajaxFormModal .manageModelselect select").on('click', function(){
				mylog.log("Saving previous manageModel " + $(this).val());
				$(this).data('manageModel', $(this).val());
			});
						    
			$("#ajaxFormModal .manageModelselect select").change(function(){
			 //alert("change");
			    var prevManageModel = $(this).data('manageModel');
			    var modelStatus=$(this).val();
			    if(prevManageModel=="Autre" && modelStatus!="Autre"){
			       $("#ajaxFormModal .extraManageModeltext").hide();
			       $("#ajaxFormModal .extraManageModeltext input").val("");
			    }
			    //alert(modelStatus);
			    var manageModel2TypeOrga={
			        "Association" : "NGO", 
			        "Collectif citoyen" : "Group", 
			        "Universités / Écoles d’ingénieurs ou de commerce / EPST" : "GovernmentOrganization", 
			        "Établissements scolaires (Lycée, Collège, Ecole)" : "GovernmentOrganization", 
			        "Collectivités (Département, Intercommunalité, Région, etc)" : "GovernmentOrganization", 
			        "SARL-SA-SAS" : "LocalBusiness", 
			        "SCIC" : "Cooperative", 
			        "SCOP" : "Cooperative", 
			        "Autre"	: "Group"
			    }; 
			    $("#ajaxFormModal .typeselect select").val(manageModel2TypeOrga[modelStatus]);
			    if(modelStatus=="Autre"){
			       $("#ajaxFormModal .extraManageModeltext").show();
			       $("#ajaxFormModal .extraManageModeltext input").focus();
			    }
			});
			            
			           
			$("#ajaxFormModal .typePlaceselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var placeCat=$(this).val();
			    if(placeCat.includes("Autre")==false){
			       $("#ajaxFormModal .extraTypePlacetext").hide();
			       $("#ajaxFormModal .extraTypePlacetext input").val("");
			    }
			    if(placeCat.includes("Autre")){
			       $("#ajaxFormModal .extraTypePlacetext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


			$("#ajaxFormModal .themeNetworkselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var themeNetwork=$(this).val();
			    if(themeNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraThemeNetworktext").hide();
			       $("#ajaxFormModal .extraThemeNetwork input").val("");
			    }
			    if(themeNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraThemeNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});

			$("#ajaxFormModal .localNetworkselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var localNetwork=$(this).val();
			    if(localNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraLocalNetworktext").hide();
			       $("#ajaxFormModal .extraLocalNetwork input").val("");
			    }
			    if(localNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraLocalNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


            //init openingHours
            $(".btn-select-day").off().on("click",function(){
                mylog.log("selectDay", openingHoursResult, $(this).data("key"));
                key=$(this).data("key");
                if($(this).hasClass("active")){
                    $(this).removeClass("active");
                    $.each(openingHoursResult, function(e,v){
                    if(v.dayOfWeek==key)
                        openingHoursResult[e].disabled=true;
                    });
                    $("#contentDays"+key).fadeOut();
                }else{
                    $(this).addClass("active");
                    $.each(openingHoursResult, function(e,v){
                        if(v.dayOfWeek==key)
                        	 openingHoursResult[e].disabled;
                    });
                    $("#contentDays"+key).fadeIn();
                }
            });


		    $('.timeInput').off().on('changeTime.timepicker', function(e) {
			    mylog.log("changeTimepicker");
			    var typeInc=$(this).data("type");
			    var daysInc=$(this).data("days");
			    var hoursInc=$(this).data("value");
			    var firstEnabled=null;
			    var setFirst=false
			    $.each(openingHoursResult, function(i,v){
				    if(!setFirst && typeof openingHoursResult[i].disabled=="undefined"){
				       mylog.log("firtEnable",v.dayOfWeek);
				       firstEnabled=v.dayOfWeek;
				       setFirst=true;
			        }
	                if(firstEnabled==daysInc){
	        	        mylog.log("first enabled change causes recursive change",v.dayOfWeek);
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
                        var typeHours="";
	        	        if(typeInc=="opens"){
	        		        typeHours="start";
	        	        }else if(typeInc=="closes"){
	        		        typeHours="end";
	        	        }
	        	        if(typeHours!=""){
	        		        $("#"+typeHours+"Time"+v.dayOfWeek+hoursInc).val(e.time.value);
	        	        }
	                }else if(v.dayOfWeek==daysInc){
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
	                }
		        });
		    });

			$('.timeInput').on("focus",function (){
			    $(this).timepicker('showWidget');
			});

			$('.addHoursRange').hide();

			//init videos
			dyFInputs.videos.init();   

			// AFTERLOAD
			dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(data){
				mylog.log("afterload FTL data", data);
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				$("#divMapLocality").addClass("hidden");
				
				if(Object.keys(data).length==0 || typeof data.address=="undefined" || (typeof data.address!="undefined" && Object.keys(data.address).length==0)){
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				}else if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
					$('#ajaxFormModal #divNewAddress').hide();
				}


				dyFObj.formInMap.bindStreetResultsEvent = function(){	
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                        // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                        dyFInputs.locationObj.elementLocations=[];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
				    dyFObj.formInMap.valideLocality = function(country){
        			    mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

        			    if(notEmpty(dyFObj.formInMap.NE_lat)){
        			    	locObj = dyFObj.formInMap.createLocalityObj();
        			    	mylog.log("formInMap copyMapForm2Dynform", locObj);
        			    	dyFInputs.locationObj.copyMapForm2Dynform(locObj);

        			    }
        			    // Div récapitulatif de l'adresse 
        			    dyFObj.formInMap.resumeLocality();
        			    // Cache le bouton de validation d'adresse
        			    dyFObj.formInMap.btnValideDisable(false);
        			    toastr.success("Adresse enregistrée");
        			
        			    dyFObj.formInMap.initHtml();
        			    $("#btn-submit-form").prop('disabled', false);
        		    };

        		    // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
        	        $("#ajaxFormModal .item-street-found").off().click(function(){
        		        if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
        		        	dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
        		        	setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
        		        	$("#divMapLocality").removeClass("hidden");
        		        	$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
        		        }
        		        var streetAddressName= $(this).text().split(",")[0].trim();
    
        		        $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
    
        		        dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
    
        		        mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
        		        $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
        		        $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
        		        $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
    

        		        dyFObj.formInMap.NE_lat = $(this).data("lat");
        		        dyFObj.formInMap.NE_lng = $(this).data("lng");
        		        dyFObj.formInMap.showWarningGeo(false);

        		        //Valider au click du résultat
        		        dyFObj.formInMap.valideLocality();
    
        		        $("#ajaxFormModal #sumery").show();

    
        		        dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
				    	    var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
		                    mylog.log('marker drop event', latLonMarker);
		                    dyFObj.formInMap.NE_lat = latLonMarker.lat;
				    	    dyFObj.formInMap.NE_lng = latLonMarker.lng;
        
				        	dyFObj.formInMap.valideLocality();
				        	toastr.success("Coordonnées mises à jour");
		                });
        	        });
                };
            };
		},
		beforeSave : function(data){
			mylog.log("beforeSave data",data);
            if(notNull(data.newElement_city)){
			    delete data.newElement_city;
            }

		    if(notNull(data.newElement_street)){
			    delete data.newElement_street;
		    }
		    return data;
		},
		afterSave:function(data){
			uploadObj.afterLoadUploader=false;
			var mapDynf2Answer={
                "tags" : { 
                    "territory" : "franceTierslieux62ab1d587f3bb.franceTierslieux28102021_1857_335",   
			        "typePlace" : "franceTierslieux62ab1d5880058.franceTierslieux28102021_1939_41",
			        "services" : "franceTierslieux62ab1d5880058.franceTierslieux28102021_1939_443",
			        "manageModel" : "franceTierslieux62ab1d5880df2.franceTierslieux29102021_912_51"
			        //"state" : "multiRadiofranceTierslieux28102021_184_01",        
			        
			    }    
			};

			

			

		//	alert("nicos ici le callBack number 1");
			dyFObj.commonAfterSave(data, function(){
				mylog.log("callback aftersaved",data)
				dyFObj.closeForm();
				if(typeof answerObj!="undefined" && typeof answerObj._id.$id!="undefined"){
					//alert("answer");
				    var savedData = data.map;
				    var tplCtx = {
				        id : answerObj._id.$id,
				        collection : "answers"
				    };
				    $.each(savedData , function(key,val){
				        if(typeof mapDynf2Answer[key]!="undefined"){
				        tplCtx.path = "answers."+mapDynf2Answer[key];
				        tplCtx.value = val;
				        if(key=="tags"){
				            $.each(costum.lists,function(kl,vl){
				              if(Object.keys(mapDynf2Answer[key]).includes(kl)){  
				                var filteredValues = val.filter(value => vl.includes(value));
				                mylog.log("tags'common values",kl,filteredValues);
				                  if(kl=="state" || kl=="manageModel" || kl=="territory"){
				                    filteredValues=filteredValues[0];
				                }
				                tplCtx.path = "answers."+mapDynf2Answer[key][kl];
				                tplCtx.value = filteredValues;
				                dataHelper.path2Value( tplCtx, function(params) { 
				                    var stepId= mapDynf2Answer[key][kl].split(".")[0] ;  
				                    var inputKey=mapDynf2Answer[key][kl].split(".")[1];                       
				                    reloadInput(inputKey, stepId);
				                });
				              }  

				            });
				        }
				        else if(key=="address"){
				            ansAddrObj={};
				            val["name"] = val["streetAddress"]+", "+val["postalCode"]+", "+val["addressLocality"]+", "+val["level1Name"]
				            ansAddrObj[key]=val;
				            tplCtx.value=ansAddrObj;
				        }
				        else if(key=="socialNetwork"){
				            var socialTable = [];
				            socialTable.push(["Plateformes","Url"]);
				            $.each(val, function(ind,sonet){
				                socialTable.push([sonet["platform"],sonet["url"]]);
				            });
				            tplCtx.value=socialTable;
				        }
				        else if(key=="video"){
				            tplCtx.value=val[0];
				        }

				        if(key!="tags"){
				        
				            dataHelper.path2Value( tplCtx, function(params) { 
				                var stepId= mapDynf2Answer[key].split(".")[0] ;  
				                var inputKey=mapDynf2Answer[key].split(".")[1];                       
				                reloadInput(inputKey.substring(inputKey.length-39), stepId);
				            });

				        }    
				    

				        }
				    });
				}
				//$("#addConfirmModal").modal('show');
				// if(map.category=="network" && map.name){
					

				// // 	data.name
				//  }
				if(userId==""){
					if(!dyFObj.unloggedProcess.isAlreadyRegister)
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite électronique</span>");

	// 				$('#addConfirmModal').on('hidden.bs.modal', function () {
	// 					onchangeClick=false;
	// 					history.replaceState({}, null, uploadObj.gotoUrl);
	// //					location.hash(uploadObj.gotoUrl);
	// 				  	window.location.reload();
	// 				});
				}else{
					urlCtrl.loadByHash(uploadObj.gotoUrl);
				}
			});
		}
		
	}
};
	
