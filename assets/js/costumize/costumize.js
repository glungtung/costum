$(function(){
    /** * * * * * * * * * * * * * * * * * * *
    *
    * Script for appel à project
    * 
    * * * * * * * * * * * * * * * * * * * * * */
   function aapSctipt(){
        if((costum.type=="aap" || costum.contextSlug == 'proceco')){
            var callFormProjectObj = {
                init : function(cfaObj){
                        cfaObj.events(cfaObj);
                        //cfaObj.CACs.init(cfaObj,costum.contextSlug);
                },
                events : function(cfaObj){
                        var hashAap = location.hash.substring(1);
                        var hashArray = hashAap.split('.');
                        if(hashArray.includes("formid") && hashArray.includes("slug") && hashArray.includes("aappage")){
                            var formid = hashArray.indexOf("formid")+1;
                            var slugaap = hashArray.indexOf("slug")+1;
                            //$('#mainNav [href="#agenda"].menu-app,#mainNav [href="#agenda"].lbh-menu-app').attr("href", "#welcome.slug."+hashArray[slugaap]+".formid."+hashArray[formid]+".aappage.agenda");
                            //$('#mainNav [href="#projet"].menu-app,#mainNav [href="#projet"].lbh-menu-app,#mainNav [href="#projets"].menu-app,#mainNav [href="#projets"].lbh-menu-app').attr("href", "#welcome.slug."+hashArray[slugaap]+".formid."+hashArray[formid]+".page.project");
                            //cfaObj.CACs.init(cfaObj,costum.contextSlug);
                            ajaxPost('',baseUrl+"/"+moduleId+"/search/globalautocomplete",{  
                                searchType: ["organizations"],
                                notSourceKey: true,
                                filters:{
                                    slug : hashArray[slugaap]
                                },
                                fields:["name"]
                            },
                            function(data){
                                $.each(data.results,function(k,v){
                                    $('.cosDyn-app').before('<a href="javascript:;" class="aap-small-context text-dark menu-app hidden-xs lbh-urlExtern " style="font-size: 16px !important; font-weight: 200 !important; float: left; font-style: italic; text-decoration: none;padding:0px 20px !important;cursor:default"><span class="label-menu padding-left-10">'+v.name+'<br />'+(notEmpty(v.shortDescription) ? v.shortDescription : "" )+'</span></a>')
                                
                                    function lazyLoadAapLogo(time){
                                        if($(".aap-lists-btn").length !=0 ){
                                            $(".aap-lists-btn").replaceWith(`
                                                <a href="#welcome" class="aap-lists-btn lbh-menu-app">
                                                    <img src="${exists(v.profilMediumImageUrl)? v.profilMediumImageUrl:defaultImage}" class="image-menu hidden-xs pull-left padding-left-10" height="50" width="auto">
                                                    <img src="${exists(v.profilMediumImageUrl)? v.profilMediumImageUrl:defaultImage}" class="image-menu visible-xs pull-left padding-left-10" height="40">
                                                </a>
                                            `);
                                        }else
                                            setTimeout(function(){
                                                lazyLoadAapLogo(time+200)
                                            }, time);
                                    }
                                    lazyLoadAapLogo(200);
                                })
                            },null);
                        }else{
                            setTimeout(() => {
                                if(typeof contextDataAap != "undefined")
                                    $('.cosDyn-app').before('<a href="javascript:;" class="text-dark menu-app hidden-xs lbh-urlExtern " style="font-size: 16px !important; font-weight: 200 !important; float: left; font-style: italic; text-decoration: none;padding:0px 20px !important;cursor:default"><span class="label-menu padding-left-10">'+contextDataAap.name+'<br />'+(exists(contextDataAap) && notEmpty(contextDataAap.shortDescription) ? contextDataAap.shortDescription : "" )+'</span></a>')
                            }, 1500);
                        }

                        var checkLogoExist = function(time){
                            if($(".aap-lists-btn").length !=0 ){
                                setTimeout(() => {
                                    /*$(".aap-lists-btn").on("click",function(){
                                        if(location.hash.indexOf("#oceco")!=-1){
                                            var aapDialog = bootbox.dialog({ 
                                                title: '',
                                                message: '<div id="exists-aap-lists"></div>',
                                                size: 'large',
                                                onEscape: true,
                                                backdrop: true,
                                            })
                                            aapDialog.init(function(){
                                                aapDialog.attr("id","exists-aap-lists-dialog");
                                                $("#exists-aap-lists-dialog").css("background-color","rgb(0 0 0 / 66%)");
                                                if(costum != null){
                                                    ajaxPost('#exists-aap-lists', baseUrl + "/costum/aap/listaap/slug/"+costum.slug,
                                                        null,
                                                        function (data) {
                        
                                                        }, null
                                                    );
                                                }
                                            });
                                        }else{
                                            urlCtrl.loadByHash("#welcome");
                                        }
                                    })*/
                                }, 900);
                            }else
                                setTimeout(function(){
                                    checkLogoExist(time+200)
                                }, time);
                        }
                        checkLogoExist(0);
                },
                
            }
            
            var copyCallFormProjectObj = {...callFormProjectObj};
            copyCallFormProjectObj.init(copyCallFormProjectObj);
        }
   }
   aapSctipt();
    
})