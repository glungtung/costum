

adminPanel.views.communitys = function(dom) {
	var data={
        title : trad.community,
        context : {
            id : costum.contextId,
            collection : costum.contextType
        },
        invite : {
            contextId : costum.contextId,
            contextType : costum.contextType,
        },
        table : {
            name: {
                name : "Membres",
                class : "col-xs-2 text-center"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-1 text-center"
            },
            isInviting : {
                name : "Validation pour être membres",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            },
            zone : {
                name : "Territoire",
                class : "col-xs-2 text-center"
            },
            created : {
                name : "Date d'inscription",
                class : "col-xs-2 text-center"
            },
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "citoyens"],
                fields : [ "name", "email", "links", "collection","zone","created"],
                notSourceKey : true,
                indexStep: 50
            },
            filters : {
                text : true
            }
        },
        actions : {
            admin : true,
            roles : true,
            disconnect : true
        }
    };
    data.paramsFilter.defaults.filters = {};

    if(costum.contextType=="projects"){
        data.paramsFilter.defaults.filters["links."+costum.contextType+"."+costum.contextId] = {
            '$exists' : 1 
        }
    }else{
        data.paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {
            '$exists' : 1 
        };
    }

    var searchAdminType=(typeof paramsAdmin != "undefined" 
    && typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["community"] != "undefined"
    && typeof paramsAdmin["menu"]["community"]["initType"] != "undefined") ? paramsAdmin["menu"]["community"]["initType"]: ["citoyens"];
    data.paramsFilter.defaults.types=searchAdminType;
    let domTarget=(notEmpty(dom)) ? dom :'#content-view-admin'; 
    ajaxPost(domTarget, baseUrl+'/'+moduleId+'/admin/directory', data, function(){
        ajaxPost(null, baseUrl+'/'+moduleId+'/admin/mailing', {}, function(memberMailingHtml){
            $('#content-view-admin').append(memberMailingHtml);
        },"html");
    },"html");
}
adminPanel.views.directorys = function() {
    var data={
        title : "Gestion des utilisateurs-trices",
        //types : [ "citoyens" ],
        table : {
            name: {
                name : "Nom"
            },
            email : { 
                name : "E-mail"
            },
            zone : {
                name : "Territoire",
                class : "col-xs-2 text-center"
            },
            created : {
                name : "Date d'inscription",
                class : "col-xs-2 text-center"
            }
        },
        actions:{
            switchToUser : true,
            banUser : true,
            deleteUser : true,
            addAsMember : true
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "citoyens" ],
                fields : [ "name", "email", "collection","zone","created" ],
                sort : { name : 1 }
            },
            filters : {
                text : true
            }
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
}
adminDirectory.values.zone = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.zone != "undefined") {
        str = e.zone;
    }
    return str;
};
adminDirectory.values.created = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.created != "undefined") { 
        if(typeof e.created == "number"){
            var date = directory.decomposeDate(e.created, "dayNumber-month-year",true); 
            str += date;
        }else if(typeof e.created == "object"){
            var date = directory.decomposeDate(e.created.sec, "dayNumber-month-year",true);
            console.log("dateeee",date)
            str += date;
            

        }
    }
    return str;
};
adminDirectory.actions["addAsMember"]=function(e, id, type, aObj){
    mylog.log("adminDirectory.actions.addAsMember", id, type, aObj);
    var label = "Ajouter comme membre";
    var str = '<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 addAsMemberBtn btn bg-red text-white"><i class="fa fa-user-plus"></i> '+label+'</button>';
    return str ;
}

adminDirectory.events["addAsMember"] = function(aObj){
    $("#"+aObj.container+" .addAsMemberBtn").off().on("click",function (){
        mylog.log("adminDirectory.events.addAsMemberBtn ", $(this).data("id"), $(this).data("type"));
        var id = $(this).data("id");
        var type = $(this).data("type");
        var parentType = costum.contextType;
        var parentId = costum.contextId;
        //var elt = aObj.getElt(id, type) ;
        var label = "confirm please !!";

        bootbox.confirm(label, function(result) {
            if (result) {
                //links.validate(parentType, parentId, id, type, "isInviting", null);
                links.connectAjax(parentType, parentId, id, type, null, null, null)
            }
        });
    });
}