poi.filters["tree"] = {
"label": "Arbre", "key": "tree", "icon": "tree"
};

tradCategory["tree"] = "Arbre";

dyFObj.unloggedMode=true;

dyFObj.formInMap.forced.countryCode="FR";
dyFObj.formInMap.forced.map={"center":["45.439695", "4.3871779"], zoom: 5};
dyFObj.formInMap.forced.showMap=true;

paramsMapCO = $.extend(true, {}, paramsMapCO, {
	mapCustom:{
		icon: {
			getIcon:function(params){
				var elt = params.elt;
				
				if(typeof elt.collection != "undefined" && elt.collection=="poi" && typeof elt.type != "undefined" && elt.type=="tree"){
						var markerHtmlStyles = `
							background-color: white;
							width: 3.5rem;
						    height: 3.5rem;
						    left: -1.5rem;
						    top: -1.5rem;
						    display: block;
						    font-size: 22px;
						    color: green;
						    border: solid 2px;
						    padding: 0.6rem;
						    position: relative;
						    border-radius: 3rem;`;
						var markerIcon = "tree";
						var html=`<i style="`+markerHtmlStyles+`" class="fa fa-`+markerIcon+`" ></i>`;
				}
				else{
					var markerCollection={
						default: modules.map.assets + '/images/markers/citizen-marker-default.png',
		                organization: modules.map.assets + '/images/markers/ngo-marker-default.png',
		                classified: modules.map.assets + '/images/markers/classified-marker-default.png',
		                proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
		                poi: modules.map.assets + '/images/markers/poi-marker-default.png',
		                project: modules.map.assets + '/images/markers/project-marker-default.png',
		                event: modules.map.assets + '/images/markers/event-marker-default.png',
		                answer: modules.map.assets + '/images/markers/services/tools-hardware.png'
		            };
					var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
	                if (typeof elt.profilThumbImageUrl !== "undefined" && elt.profilThumbImageUrl != "")
	                    imgProfilPath = baseUrl + elt.profilThumbImageUrl;
	                else if (typeof elt.collection!="undefined"){
	                	var markerType=(elt.collection.substring(elt.collection.length-1) == "s") ?  elt.collection.substring(0, elt.collection.length-1) : elt.collection ;
    
	                    imgProfilPath = markerCollection[markerType];
	                }    
	                //alert(imgProfilPath);
					var html="<img src='" + imgProfilPath + "' height='45' width='40' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				}
			

			
				var myIcon = L.divIcon({
					className: "my-custom-pin",
					iconAnchor: [0, 24],
					labelAnchor: [-6, 0],
					popupAnchor: [0, -36],
					html: html
				});
				return myIcon;
			},
			getClusterIcon:function(cluster){
				var childCount = cluster.getChildCount();
				return L.divIcon({ html: '<div>' + childCount + '</div>', className : "bg-green", iconSize: new L.Point(40, 40) });
			}
		}
	}
});