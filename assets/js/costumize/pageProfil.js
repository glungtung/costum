try {
    pageProfil.views.detail = function () {
        mylog.log("pageProfil.views.detail");
        var url = "element/about/type/" + contextData.type + "/id/" + contextData.id;
        ajaxPost('#central-container', baseUrl + '/' + moduleId + '/' + url + '?tpl=ficheInfoElement', null, function () {
            if(contextData.zone || contextData.zero || contextData.profil || contextData.secteur){
                $("#pod-infoGeneral div.row").eq(1).append(`
                <div class="col-xs-12 margin-top-0 margin-bottom-50">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card features">
                            <div class="card-body">
                            ${((typeof contextData.zone == "undefined"))?"": (`
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-info-circle gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10" id="webAbout">
                                            <i>Zone : ${contextData.zone}</i>
                                        </p>
                                    </div>
                                </div>`)
                            }

                            ${ (typeof contextData.zero== "undefined")? "": (`
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-info-circle gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10" id="fixeAbout">
                                            <i>${contextData.zero}</i>
                                        </p>
                                    </div>
                                </div>
                            `)}

                            ${ (typeof contextData.profil == "undefined")?"": (`
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-info-circle gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10" id="mobileAbout">
                                            <i>Profil : ${contextData.profil || "( Non renseigné(e) )"}</i>
                                        </p>
                                    </div>
                                </div>
                            `)}
                            ${ (typeof contextData.secteur == "undefined")?"" : (`
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-info-circle gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10" id="mobileAbout">
                                            <i>Secteur : ${contextData.secteur || "( Non renseigné(e) )"}</i>
                                        </p>
                                    </div>
                                </div>
                            `)}
                            </div>
                        </div>
                    </div>
                </div>
            `);
            }

            $(".btn-update-info").off().on("click", function () {
                var form = {
                    saveUrl: baseUrl + "/co2/element/updateblock/",
                    dynForm: {
                        jsonSchema: {
                            title: "Modifier les informations générales",
                            afterSave: function (data) {
                                //$("#ajax-modal").fadeOut("slow");
                                let customData = {};
                                customData["zone"] = $("#zone").val();
                                customData["zero"] = $("#zero").val();
                                customData["profil"] = $("#profil").val();
                                customData["secteur"] = $("#secteur").val();

                                let context = {
                                    id: contextData.id,
                                    collection: contextData.type,
                                    path: "allToRoot",
                                    value: customData
                                };

                                dataHelper.path2Value(context, function (params) {
                                    toastr.success("Vos données a été mis à jour");
                                    $("#ajax-modal").modal('hide');
                                    dyFObj.closeForm();
                                    urlCtrl.loadByHash(location.hash);
                                });
                                //location.reload();
                            },
                            properties: {
                                block: dyFInputs.inputHidden(),
                                typeElement: dyFInputs.inputHidden(),
                                isUpdate: dyFInputs.inputHidden(true),
                                name: dyFInputs.name(),
                                //date: dyFInputs.date("Date de naissance"),
                                email: dyFInputs.text(),
                                fixe: {
                                    inputType: "text",
                                    label: "fixe"
                                },
                                mobile: {
                                    inputType: "text",
                                    label: "Mobile"
                                },
                                zone: {
                                    "placeholder": trad.select,
                                    "inputType": "select",
                                    "label": "Zone :",
                                    "class": "form-control",
                                    "options": costum.lists.zone
                                },
                                zero: {
                                    "placeholder": trad.select,
                                    "inputType": "selectMultiple",
                                    "isSelect2": true,
                                    "label": "Zéro :",
                                    "class" : "form-control",
                                    "options": costum.lists.zero
                                },
                                profil: {
                                    "placeholder": trad.select,
                                    "inputType": "select",
                                    "label": "Profil :",
                                    "class": "form-control",
                                    "options": costum.lists.profil
                                },
                                secteur: {
                                    "placeholder": trad.select,
                                    "inputType": "selectMultiple",
                                    "isSelect2": true,
                                    "label": "Secteur :",
                                    "class": "multi-select",
                                    "options": costum.lists.secteur
                                },
                                tags: dyFInputs.tags(),
                                url: dyFInputs.inputUrl()
                            }
                        }
                    }
                };

                var dataUpdate = {
                    block: "info",
                    typeElement: contextData.type,
                    id: contextData.id,
                    name: contextData.name,
                    tags: contextData.tags,
                    email: contextData.email,
                    fixe: contextData.fixe,
                    mobile: contextData.mobile,
                    url: contextData.url,
                    zero: contextData.zero || "",
                    zone: contextData.zone || "",
                    profil: contextData.profil || "",
                    secteur: contextData.secteur || "",
                };

                dyFObj.openForm(form, "markdown", dataUpdate);
            });
        }, "html");
    }
} catch (e) {

}