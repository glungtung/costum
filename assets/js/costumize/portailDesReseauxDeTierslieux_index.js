
paramsMapCO = $.extend(true, {}, paramsMapCO, {
	mapCustom:{
		icon: {
			getIcon:function(params){
				var elt = params.elt;
				mylog.log("icone ftl", elt.tags);
				var myCustomColour = '#4ab5a1';
			
				var markerHtmlStyles = `
					background-color: ${myCustomColour};
					width: 3.5rem;
					height: 3.5rem;
					display: block;
					left: -1.5rem;
					top: -1.5rem;
					position: relative;
					border-radius: 3rem 3rem 0;
					transform: rotate(45deg);
					border: 1px solid #FFFFFF`;
			
				var myIcon = L.divIcon({
					className: "my-custom-pin",
					iconAnchor: [0, 24],
					labelAnchor: [-6, 0],
					popupAnchor: [0, -36],
					html: `<span style="${markerHtmlStyles}" />`
				});
				return myIcon;
			}
		},
		getClusterIcon:function(cluster){
			var childCount = cluster.getChildCount();
			// var c = ' marker-cluster-';
			// if (childCount < 100) {
			// 	c += 'small-ftl';
			// } else if (childCount < 1000) {
			// 	c += 'medium-ftl';
			// } else {
			// 	c += 'large-ftl';
			// }
			return L.divIcon({ html: '<div style="color: white;font-size: 14px;font-weight: 700;text-align: center;border-radius: 50%!important;margin-left: -27px !important;margin-top: -27px !important;padding: 10px !important;width: 40px !important;height: 40px !important;background-image: none;background-color: #4ab5a1 !important;">' + childCount + '</div>', className: '', iconSize: new L.Point(40, 40) });
		},
		getThumbProfil: function (data) {
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
                    imgProfilPath = baseUrl + data.profilThumbImageUrl;
                else
                    imgProfilPath = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
    
                return imgProfilPath;
            },
		getPopup: function(data){
                var id = data._id ? data._id.$id:data.id;
                var imgProfil = paramsMapCO.mapCustom.getThumbProfil(data)

                var eltName = data.title ? data.title:data.name;
                var popup = "";
                popup += "<div class='padding-5' id='popup" + id + "'>";
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                if(data.tags && data.tags.length > 0){
				    popup += "<div style='margin-top : 5px;'>";
                    var totalTags = 0;
                    $.each(data.tags, function(index, value){
                        totalTags++;
                        if (totalTags < 3) {
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                        }
                    })
                    popup += "</div>";
                }
                if(data.address){
                    var addressStr="";
                    if(data.address.streetAddress)
                        addressStr += data.address.streetAddress;
                    if(data.address.postalCode)
                        addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                    if(data.address.addressLocality)
                        addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                    popup += "<div class='popup-address text-dark'>";
                    popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
                    popup += "</div>";
                }
                if(data.shortDescription && data.shortDescription != ""){
                    popup += "<div class='popup-section'>";
					popup += "<div class='popup-subtitle'>Description</div>";
					popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
				    popup += "</div>";
                }
                if((data.url && typeof data.url == "string") || data.email || data.telephone){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contacts</div>";
                    
                    if(data.url && typeof data.url === "string"){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                        popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                        popup += "</div>";
                    }

                    if(data.email){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                        popup += "</div>";
                    }

                    if(data.telephone){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-phone fa_phone'></i> ";
                        var tel = ["fixe", "mobile"];
                        var iT = 0;
                        $.each(tel, function(keyT, valT){
                            if(data.telephone[valT]){
                                $.each(data.telephone[valT], function(keyN, valN){
                                    if(iT > 0)
                                        popup += ", ";
                                    popup += valN;
                                    iT++; 
                                })
                            }
                        })
                        popup += "</div>";
                    }

                    popup += "</div>";
				    popup += "</div>";
                }
                var url = baseUrl+'/costum/co/index/slug/' + data.slug ;
                popup += "<div class='popup-section'>";
                popup += "<a href='" + url + "' target='_blank' class='item_map_list popup-marker' id='popup" + id + "'>";
                popup += '<div class="btn btn-sm btn-more col-md-12">';
                popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                popup += '</div></a>';
                popup += '</div>';
                popup += '</div>';

                return popup;
            }
	}
});

dyFObj.unloggedMode=true;

costum.searchExist = function (type,id,name,slug,email) { 
			mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
			var data = {
				type : type,
				id : id,
				map : { slug : slug }
			}
			//alert("here");
			$("#similarLink").hide();
			$("#ajaxFormModal #name").val("");


			// TODO - set a condition ONLY if can edit element (authorization)
			dyFObj.editElement( type,id, null, costum.typeObj[type].dynFormCostum);
			
			
	    
		};

costum[costum.slug]={
	init : function(){
		dyFObj.formInMap.forced.countryCode="FR";
		dyFObj.formInMap.forced.map={"center":["46.7342232", "2.74686000"], zoom: 5};
		dyFObj.formInMap.forced.showMap=true;  
	},
	"organizations" : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData",data);
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined" && e!="level"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.category != "undefined" && data.category=="network"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push("Réseau de tiers-lieux");
				//delete data.mainTag;
			}
			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			return data;
		},
		afterSave:function(data){

			uploadObj.afterLoadUploader=false;
			dyFObj.commonAfterSave(data);
				mylog.log("callback aftersaved",data);

			var tplCtx={};

			tplCtx.id = data.id;
			data=data.map;
			tplCtx.collection = data.collection;
			tplCtx.tplGenerator = true;

			tplCtx.path = "allToRoot";
			tplCtx.value = {
				level : data.level
			};

			dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        });

			tplCtx.path = "costum.slug";
			tplCtx.value ="reseauTierslieux";

			dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        });

	        tplCtx.path = "costum.colors";
			tplCtx.value = {
				"main1" : data.main1,
				"main2" : data.main2
			};
			dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        } );

	        tplCtx.path = "main1";
			dataHelper.unsetPath( tplCtx, function(params) { 	                        
	        } );

	        tplCtx.path = "main2";
			dataHelper.unsetPath( tplCtx, function(params) { 	                        
	        } );

			var urlTags = "";
			var arrayTags =[];
			$.each(data.tags, function(e, v){
				if($.inArray(v,costum.lists.typePlace)>-1){
					urlTags+= v+"," ;
					arrayTags.push(v);
				}
			});
			tplCtx.path="costum.reference.themes";
            tplCtx.value=arrayTags;
            dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        } );
			if (urlTags!="" && urlTags.slice(-1)==","){
				urlTags=urlTags.slice(0, -1);
			}	
	        
            mylog.log("Themes in url",urlTags);

            $("#ajax-modal").hide();

             var urlNetworkRedirect = baseUrl+`/costum/co/index/slug/`+data.slug+`#?tags=`+urlTags;

            				function redirectNetwork(urlNetworkRedirect){
            					bootbox.confirm({
                                   title: "Accéder à sa plateforme réseau",
                                   backdrop: true,
                                   message: "Cliquez sur le lien suivant pour accéder à votre plateforme : <br><a target='_blank' href='"+urlNetworkRedirect+"'>J'accède à ma plateforme réseau</a>",
                                   buttons: {
                                        cancel: {
                                            label: '<i class="fa fa-times"></i> Cancel'
                                        },
                                        confirm: {
                                            label: '<i class="fa fa-check"></i> Confirm'
                                        }
                                    },
                                    callback: function (result) {
                                        console.log('This was logged in the callback: ' + result);
                                        window.open(urlNetworkRedirect);
                                    }
                                });
            				}
            				
            					if(!dyFObj.unloggedProcess.isAlreadyRegister){
            						bootbox.alert({
            						title : "Votre compte a été créé",
            						    backdrop: true,
            						    message: $("#modalRegisterSuccess .modal-body:eq(1)").html()+" <h5><i class='fa fa-warning'></i> Nous vous incitons vivement à valider tout de suite votre compte afin d'être redirigé.e vers votre plateforme réseau</h5>",
            						    callback: function (result) {
            						        redirectNetwork(urlNetworkRedirect);
            						    }
            						});
            				}else{
            					redirectNetwork(urlNetworkRedirect)
            				}	        
	  			
		}
		
	}
};

costum.lists= {
        "level" : {
            "level1" : "National",
            "level3" : "Régional",
            "level4" : "Départemental",
            "level5" : "Communauté de communes"
        },
        "typePlace" : [ 
            "Ateliers artisanaux partagés", 
            "Bureaux partagés / Coworking", 
            "Cuisine partagée / Foodlab", 
            "Fablab / Makerspace / Hackerspace (Espaces du Faire)", 
            "LivingLab / Laboratoire d'innovation sociale", 
            "Tiers-lieu nourricier", 
            "Tiers-lieu culturel / Lieux intermédiaires et indépendants", 
            "Autre"
        ],
        "services" : [ 
            "Accompagnement des publics", 
            "Action sociale", 
            "Aiguillage / Orientation", 
            "Bar / café", 
            "Boutique / Épicerie", 
            "Coopérative d'Activités et d'Emploi / Groupement d'employeur", 
            "Cantine / restaurant", 
            "Centre de ressources", 
            "Chantier participatif", 
            "Complexe évènementiel", 
            "Conciergerie ", 
            "Domiciliation", 
            "Espace de stockage", 
            "Espace détente", 
            "Espace enfants", 
            "Formation / Transfert de savoir-faire / Éducation", 
            "Habitat", 
            "Incubateur", 
            "Lieu d'éducation populaire et nouvelles formes d'apprentissage", 
            "Maison de services au public", 
            "Marché", 
            "Média et son", 
            "MediaLab", 
            "Médiation numérique", 
            "Offre artistique ou culturelle", 
            "Pépinière d'entreprises", 
            "Point d'appui à la vie associative", 
            "Point Information Jeunesse", 
            "Point d'information touristique", 
            "Pratiques de soin (art thérapie, massage, médecine douce, méditation...)", 
            "Résidences d'artistes", 
            "Ressourcerie / recyclerie", 
            "Service enfance-jeunesse", 
            "Services liés à la mobilité"
        ],
        "manageModel" : [ 
            "Association", 
            "Collectif citoyen", 
            "Universités / Écoles d’ingénieurs ou de commerce / EPST", 
            "Établissements scolaires (Lycée, Collège, Ecole)", 
            "Collectivités (Département, Intercommunalité, Région, etc)", 
            "SARL-SA-SAS", 
            "SCIC", 
            "SCOP", 
            "Autre"
        ],
        "state" : [ 
            "En projet", 
            "Ouvert", 
            "Fermé"
        ],
        "spaceSize" : [ 
            "Moins de 60m²", 
            "Entre 60 et 200m²", 
            "Plus de 200m²"
        ],
        "network" : [ 
            "A+ C'est Mieux", 
            "Actes‐IF", 
            "Artfactories/Autresparts", 
            "Bienvenue dans la Canopée", 
            "Cap Tiers‐Lieux", 
            "Cédille Pro", 
            "Collectif des Tiers‐Lieux Ile‐de‐France", 
            "Collectif Hybrides", 
            "Compagnie des Tiers‐Lieux", 
            "Coopérative des Tiers‐Lieux", 
            "Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)", 
            "Cowork'in Tarn", 
            "Coworking Grand Lyon", 
            "CRLII Occitanie", 
            "GIP Recia", 
            "Hub France Connectée", 
            "L'ALIM (l'Assemblée des lieux intermédiaires marseillais)", 
            "La Trame 07", 
            "Label Tiers‐Lieux Occitanie", 
            "Label C3", 
            "Label Tiers‐Lieux Normandie", 
            "Le DOG", 
            "Le LIEN", 
            "Lieux Intermédiaires en région Centre", 
            "Réseau des tiers‐lieux Bourgogne Franche Comté", 
            "Réseau Français des Fablabs", 
            "Réseau Médoc", 
            "Réseau TELA", 
            "Tiers‐Lieux Edu"
        ],
        "regionalNetwork" : [ 
            "Réseau Relief (Auvergne Rhône-Alpes)", 
            "Réseau Tiers-lieux Bourgogne-France-Comté (BFC)", 
            "Réseau Bretagne Tiers-Lieux (Bretagne)", 
            "Réseau Ambition Tiers Lieux (Centre-Val-de-Loire)", 
            "Réseau Da Locu (Corse)", 
            "Réseau Tiers-Lieu Grand Est", 
            "Réseau La Compagnie des Tiers-Lieux (Hauts-de-France)", 
            "Réseau Ile de France Tiers Lieux (Consortium réunissant A+ c’est mieux, Actifs, le Collectif des Tiers-lieux, Makers IDF)", 
            "Réseau TILINO (Normandie)", 
            "Réseau La Coopérative Tiers-Lieux (Nouvelle Aquitaine)", 
            "Réseau La Rosée (Occitanie)", 
            "Réseau CAP Tiers-Lieux (Pays-de-la-Loire) et la CRESS Pays de la Loire", 
            "Réseau Sud Tiers-Lieux (Provence Alpes Côte d’Azur)", 
            "Réseau La Réunion des Tiers-Lieux (La Réunion)", 
            "Réseau régional des Tiers-lieux de Martinique", 
            "Réseau régional des Tiers-lieux de Guadeloupe"
        ],
        "themeNetwork" : [ 
            "Artfactories/Autresparts", 
            "Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)", 
            "Réseau Français des Fablabs, Espaces et Communautés du Faire (RFFLabs)", 
            "Tiers-Lieux Edu", 
            "Réseau National des Ressourceries et Recycleries", 
            "Réseau Cocagne", 
            "Réseau des CREFAD", 
            "Réseau des Cafés Culturels et Cantines Associatifs (RECCCA)", 
            "Les tiers-lieuses", 
            "Autres"
        ],
        "localNetwork" : [ 
            "Actes-If - Lieux intermédiaires et indépendants en Ile de France", 
            "A+ c’est mieux !", 
            "Cédille Pro", 
            "Collectif Hybrides", 
            "Collectif des Tiers-Lieux Ile-de-France", 
            "Coworking Grand Lyon", 
            "Cowork’in Tarn", 
            "La Trame 07", 
            "AliiCe - Association des Lieux Intermédiaires indépendants en région Centre", 
            "Réseau Médoc", 
            "Réseau TELA", 
            "RedLab (Réseau des Labs d'Occitanie)", 
            "Makers IDF", 
            "Le LIEN", 
            "CRLii Occitanie", 
            "L’ALIM (l’Assemblée des lieux intermédiaires marseillais) / Collectif indéterminé", 
            "Autre"
        ],
        "hubFranceConnecte" : [ 
            "Hub du Sud", 
            "Hubik", 
            "Les Assembleurs", 
            "MedNum Bourgogne-Franche-Comté", 
            "Hub PiNG", 
            "Hub Ultra Numérique", 
            "Hub AURA", 
            "Hubert", 
            "Hub Ile-de-France", 
            "Hub Occitanie", 
            "Hub Antilles-Guyane", 
            "Autres"
        ],
        "greeting" : [ 
            "Conseil et orientation avec contact téléphonique", 
            "Point d'échange physique proposé pour monter en compétences respectivement", 
            "Voyage apprenant pour découverte de lieux", 
            "Accueil d'élus pour acculturation", 
            "Visioconférence de présentations", 
            "Étude d'opportunité", 
            "Aide à la réalisation de dossiers pour AMI ou subventions", 
            "Conseil projet", 
            "Incubation de projets", 
            "Mentoring / coaching de porteurs de projet", 
            "Parcours de formation pour des porteurs de projet"
        ],
        "certification" : [ 
            "Fabrique de Territoire", 
            "Fabrique Numérique de Territoire"
        ],
        "compagnon" : [ 
            "Compagnon France Tiers-Lieux"
        ],
        "territory" : [ 
            "En agglomérations", 
            "En métropole", 
            "En milieu rural", 
            "En ville moyenne (entre 20000 et 100000 habitants)"
        ]
};

// costum.typeObj.organizations.dynFormCostum={
//                     "beforeBuild" : {
//                         "properties" : {
//                             "formLocality" : {
//                                 "rules" : {
//                                     "required" : true
//                                 }
//                             },
//                             "url" : {
//                                 "label" : "Site internet (commençant par \" http:// \")"
//                             },
//                             "name" : {
//                                 "label" : "Nom du réseau",
//                                 "placeholder" : "Nom du réseau"
//                             },
//                             "email" : {
//                                 "label" : "Email contact du réseau"
//                             },
//                             "shortDescription" : {
//                                 "order" : 3
//                             },
//                             "physicalLocation" : {
//                                 "order" : 4,
//                                 "inputType" : "checkboxSimple",
//                                 "label" : "Lieu physique d'accueil",
//                                 "params" : {
//                                     "onText" : "Oui",
//                                     "offText" : "Non",
//                                     "onLabel" : "Existence d'un lieu",
//                                     "offLabel" : "Absence de lieu",
//                                     "labelText" : "Lieu physique d'accueil"
//                                 },
//                                 "checked" : false,
//                                 "optionsValueAsKey" : true
//                             },
//                             "typePlace" : {
//                                 "order" : 4,
//                                 "inputType" : "select",
//                                 "label" : "Thématique(s) du réseau (enlever celle(s) en trop)",
//                                 "placeholder" : "Choisir une ou plusieurs thématiques",
//                                 "list" : "typePlace",
//                                 "groupOptions" : false,
//                                 "groupSelected" : false,
//                                 "optionsValueAsKey" : true,
//                                 "select2" : {
//                                     "multiple" : true
//                                 },
//                                 "rules" : {
//                                     "required" : true
//                                 },
//                                 "value" : [ 
//                                     "Coworking", 
//                                     "Ateliers artisanaux partagés", 
//                                     "Fablab / Atelier de Fabrication Numérique", 
//                                     "Tiers-lieu culturel", 
//                                     "Tiers-lieu agricole", 
//                                     "Cuisine partagée / Foodlab", 
//                                     "LivingLab / Laboratoire d'innovation sociale"
//                                 ]
//                             },
//                             "manageModel" : {
//                                 "inputType" : "select",
//                                 "order" : 6,
//                                 "label" : "Mode de gestion",
//                                 "placeholder" : "Quel(s) type(s) de structure(s) gère(nt) et anime(nt) votre espace ?",
//                                 "list" : "manageModel",
//                                 "groupOptions" : false,
//                                 "groupSelected" : false,
//                                 "optionsValueAsKey" : true,
//                                 "select2" : {
//                                     "multiple" : false
//                                 },
//                                 "rules" : {
//                                     "required" : true
//                                 }
//                             },
//                             "state" : {
//                                 "inputType" : "select",
//                                 "label" : "Etat du projet",
//                                 "order" : 7,
//                                 "placeholder" : "Choisir un état d'avancement",
//                                 "list" : "state",
//                                 "groupOptions" : false,
//                                 "groupSelected" : false,
//                                 "optionsValueAsKey" : true,
//                                 "select2" : {
//                                     "multiple" : false
//                                 }
//                             },
//                             "greeting" : {
//                                 "inputType" : "select",
//                                 "order" : 10,
//                                 "label" : "Actions d’accueil, d’orientation et d’accompagnement que vous mettez en place pour d’autres porteurs de projet de tiers-lieux",
//                                 "placeholder" : "Sélectionner la(es) action(s)",
//                                 "list" : "greeting",
//                                 "groupOptions" : false,
//                                 "groupSelected" : false,
//                                 "optionsValueAsKey" : true,
//                                 "select2" : {
//                                     "multiple" : true
//                                 }
//                             },
//                             "certification" : {
//                                 "inputType" : "select",
//                                 "order" : 11,
//                                 "label" : "Etes-vous Fabrique de Territoire ?",
//                                 "placeholder" : "Choisir un label",
//                                 "list" : "certification",
//                                 "groupOptions" : false,
//                                 "groupSelected" : false,
//                                 "optionsValueAsKey" : true,
//                                 "select2" : {
//                                     "multiple" : false
//                                 }
//                             },
//                             "description" : {
//                                 "inputType" : "textarea",
//                                 "markdown" : true,
//                                 "label" : "description longue"
//                             },
//                             "level" : {
//                                 "inputType" : "select",
//                                 "order" : 12,
//                                 "label" : "Périmètre géographique de votre réseau",
//                                 "placeholder" : "Périmètre géographique",
//                                 "list" : "level",
//                                 "value" : "level3",
//                                 "groupOptions" : false,
//                                 "groupSelected" : false,
//                                 "optionsValueAsKey" : false,
//                                 "select2" : {
//                                     "multiple" : false
//                                 },
//                                 "rules" : {
//                                     "required" : true
//                                 }
//                             },
//                             "image" : {
//                                 "inputType" : "uploader",
//                                 "docType" : "image",
//                                 "label" : "Logo de votre réseau",
//                                 "showUploadBtn" : false,
//                                 "template" : "qq-template-gallery",
//                                 "filetypes" : [ 
//                                     "jpeg", 
//                                     "jpg", 
//                                     "gif", 
//                                     "png"
//                                 ],
//                                 "rules" : {
//                                     "required" : true
//                                 }
//                             },
//                             "main1" : {
//                                 "label" : "Couleur principale de votre charte/identité graphique",
//                                 "inputType" : "colorpicker",
//                                 "rules" : {
//                                     "required" : true
//                                 }
//                             },
//                             "main2" : {
//                                 "label" : "Couleur secondaire de votre charte/identité graphique",
//                                 "inputType" : "colorpicker",
//                                 "rules" : {
//                                     "required" : true
//                                 }
//                             },
//                             "mobile" : {
//                                 "label" : "Contact téléphone",
//                                 "placeholder" : "Renseigner un numéro de téléphone",
//                                 "inputType" : "text"
//                             },
//                             "mainTag" : {
//                                 "inputType" : "hidden"
//                             },
//                             "category" : {
//                                 "inputType" : "hidden",
//                                 "value" : "network"
//                             }
//                         }
//                     },
//                     "afterSave" : "costum[costum.slug].organizations.afterSave",
//                     "formData" : "costum[costum.slug].organizations.formData",
//                     "onload" : {
//                         "actions" : {
//                             "setTitle" : "<span class='text-title'>Ajoutez votre réseau et générez votre portail</span>",
//                             "html" : {
//                                 "infocustom" : ""
//                             },
//                             "presetValue" : {
//                                 "mainTag" : "TiersLieux",
//                                 "role" : "admin"
//                             },
//                             "hide" : {
//                                 "tagstags" : 1,
//                                 "publiccheckboxSimple" : 1,
//                                 "categoryselect" : 1,
//                                 "roleselect" : 1
//                             }
//                         }
//                     }
// };
	
