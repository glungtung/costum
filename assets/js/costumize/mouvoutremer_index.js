paramsMapCO = $.extend(true, {}, paramsMapCO, {
	mapCustom:{
		activeCluster:false,
		icon: {
			getIcon:function(params){
				var elt = params.elt;
				mylog.log("icone ftl", elt.zone);
				if (typeof elt.zone != "undefined" && elt.zone != null ){
					if( elt.zone.toLowerCase()=="océan indien"){
						var myCustomColor = '#e26d38';
					}else if(elt.zone.toLowerCase()=="océan pacifique"){
						var myCustomColor = '#c4d552';
					}else if(elt.zone.toLowerCase()=="antilles - guyane"){
						var myCostumColor = '#00b2cd'; // à changer
					}
				}else{
					var myCustomColor = '#4623C9';
				}
			
				var markerHtmlStyles = `
					background-color: ${myCustomColor};
					width: 3.5rem;
					height: 3.5rem;
					display: block;
					left: -1.5rem;
					top: -1.5rem;
					position: relative;
					border-radius: 3rem 3rem 0;
					transform: rotate(45deg);
					border: 1px solid #FFFFFF`;
			
				var myIcon = L.divIcon({
					className: "my-custom-pin",
					iconAnchor: [0, 24],
					labelAnchor: [-6, 0],
					popupAnchor: [0, -36],
					html: `<span style="${markerHtmlStyles}" />`
				});
				return myIcon;
			}
		},
		getClusterIcon:function(cluster){
			var childCount = cluster.getChildCount();
			var c = ' marker-cluster-';
			if (childCount < 100) {
				c += 'small-mouv';
			} else if (childCount < 1000) {
				c += 'medium-mouv';
			} else {
				c += 'large-mouv';
			}
			return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
		}
	}
});


costum["tools"] = {
	"chat": {
		"discutions":[
			{
				"name":"Discution Principal",
				"url":"https://chat.communecter.org/group/pBn2muhcR8xEpCaTJ"
			},
			{
				"name": "Discution Océan Indien",
				"url": "https://chat.communecter.org/group/RB9vosvXspFWomZf9",
				"zone": "Océan Indien"
			},
			{
				"name": "Discution Antilles Guyanne",
				"url": "https://chat.communecter.org/group/HSeEfJyFNfJtdtCoK",
                "zone" : "Antilles - Guyane"
			},
			{
				"name": "Discution Océan Pacifique",
				"url": "https://chat.communecter.org/group/kQgLvFLEoteDA4ZP9",
				"zone" : "Océan Pacifique",
			},
			{
				"name": "J'ai ma pub",
				"url": "https://chat.communecter.org/group/hRrZzJn7YkPHXwDLA"
			},
			{
				"name":"mode Durable",
				"url":"https://chat.communecter.org/group/enHhrwKSKPFEmxt2u"
			},
			{
				"name":"Financement de projet",
				"url":"https://chat.communecter.org/group/BT4L7GKZDHtQktWAy"
			},
			{
				"name":"Alimentation durable",
				"url":"https://chat.communecter.org/group/axc4CABZnMdnWxaEm"
			},
			{
				"name": "Référent",
				"url" : "https://chat.communecter.org/group/gmSDpLtnGMCvdfxBh",
				"roles" : "Référent"
			},
			{
				"name":"Formateur",
				"url" : "https://chat.communecter.org/group/bJgq2NpGWhQhSwMai",
				"roles" : "Formateur"
			}
		]
	}
}

costum[costum.slug] = { 
	init: function () {
		let toHash = $(".dropdown.menu-btn-top.tools-dropdown span.label-menu").text();
		$(".dropdown.menu-btn-top a.lbh-dropdown").attr("href", "#" + toHash.toLowerCase().replace(" ", ""));
		$(".dropdown.menu-btn-top a.lbh-dropdown").addClass("lbh");
		$(document).off("click",".mouv-anchor-btn").on("click",".mouv-anchor-btn", function(e){
			e.stopPropagation()
			var idBtnLink = $(this).attr("href");
			$('html,body').animate({
					scrollTop: $(idBtnLink).offset().top},
				'slow');
		});
	},

	/*initialize: function(){
		if(typeof userConnected!="undefined" && 
			typeof userConnected.links!="undefined" && 
			typeof userConnected.links.memberOf != "undefined" &&
			typeof userConnected.links.memberOf[costum.contextId] !="undefined" &&
			typeof userConnected.links.memberOf[costum.contextId].addedToDiscutions!="undefined"
		){
			costum[costum.contextSlug].addMeToDiscution();
		}
	},

	addMeToDiscution: function(){
		var rooms = [];
		$.each(costum.tools.chat.discutions, function(index, discution){
			let hashRef = (discution["url"] != "undefined")?discution["url"].split("/"):[];
			let room = "";
			if(hashRef.length>0){
				room = hashRef[hashRef.length-1];
			}

			let hasRole = (typeof userConnected!="undefined" && 
				typeof userConnected.links!="undefined" && 
				typeof userConnected.links.memberOf != "undefined" &&
				typeof userConnected.links.memberOf[costum.contextId] !="undefined" &&
				typeof userConnected.links.memberOf[costum.contextId].roles!="undefined" &&
				typeof userConnected.links.memberOf[costum.contextId].roles.includes(discution.roles));

			let isInZone = (typeof userConnected !="undefined" && typeof
				discution.zone!="undefined" && typeof userConnected.zone!="undefined" && userConnected.zone==discution.zone);

			if((typeof discution.roles == "undefined" && typeof discution.zone == "undefined") || hasRole || isInZone){
					rooms.push(room);
			}
		});

		costum[costum.contextSlug].inviteToDiscution(rooms);		
	},*/

	/* superButtonCallback: function(params){
		let username = (userConnected && userConnected.username)?userConnected.username:"";
		let hashRef = (params["href"] != "undefined")?params["href"].split("/"):[];
		let room = "";
		if(hashRef.length>0){
			room = hashRef[hashRef.length-1];
		}
		if(room!="" && typeof username!="undefined"){
			costum[costum.contextSlug].inviteToDiscution(room, username);
		}else{
			toastr.info("Vous devez être connecté")
		}
	},*/

	inviteToDiscution: function(rooms){
		/*ajaxPost(
            null, 
            baseUrl+"/co2/rocketchat/invitetodiscution", 
            {
                "rooms": rooms,
				"contextType":costum.contextType||"",
            }, 
            function(res){
				var dataCustom = {};

				dataCustom.id = userId;
				dataCustom.collection = userConnected.collection;
				dataCustom.path = "links.memberOf."+costum.contextId+".addedToDiscutions";
				dataCustom.value = true;
				dataCustom.removeCache=true;
				
				dataHelper.path2Value(dataCustom, function (response) {
					toastr.success(response.msg);
				});
            },
            function(error){
				mylog.log("Error", error);
			}
        )*/
	}
}

// costum[costum.slug].initialize();

/*
cmsConstructor.block.events.bindElementsBlock = function(){
	$(".btn-functionLink").off('click').on('click', function(){
		var link = $(this).attr("data-link");
		var external = $(this).attr("data-external");
		var type = $(this).attr("data-type");
		if(!costum.editMode){
			if(typeof costum[costum.slug].superButtonCallback == "function" && $(this).hasClass("openDiscution")){
				costum[costum.slug].superButtonCallback({"href":link});
			}else if (!notEmpty(link) && !notEmpty(external)){
				bootbox.alert({
					message: `<br>
					<div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
					<p class="padding-left-20">L'URL de ce bouton est vide!</p>
					</div>`
					,
					size: 'medium'
				});
			} else {
				if (type == "lbh"){
					urlCtrl.loadByHash(link);
				} else if (type == "external") {
					window.open(external, "_blank");
				// } else if (type == "blank") {
				//     window.open(link, "_blank");
				} else {
					window.open(link);
				}
			}
		}
	});
}
*/

//cmsConstructor.block.events.init();

costum.lists.secteur = {};

costum.lists.secteur[tradCategory.food] =  tradCategory.food;

costum.lists.secteur[tradCategory.productionandservices] =  tradCategory.productionandservices;

costum.lists.secteur[tradCategory.education] =  tradCategory.education;

costum.lists.secteur[tradCategory.dwelling] =  tradCategory.dwelling;

costum.lists.secteur[trad.finance] =  trad.finance

costum.lists.secteur[tradCategory.health] =  tradCategory.health

costum.lists.secteur[trad.energie] =  trad.energie

costum.lists.secteur[tradCategory.citizen] =  tradCategory.citizen

costum.lists.secteur[trad.travelandhospitality] =  trad.travelandhospitality
