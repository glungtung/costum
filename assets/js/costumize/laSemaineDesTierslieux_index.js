costum[costum.slug] = {
    init : function(){
        //alert("here");
        dyFObj.unloggedMode=true;
        dyFObj.formInMap.forced.countryCode="RE";
        dyFObj.formInMap.forced.map={"center":["-21.115141","55.536384"],"zoom" : 9};
        dyFObj.formInMap.forced.showMap=true;
        costum.map = (typeof costum.map!="undefined") ? costum.map : {};
        costum.map.searchFields=["urls","address","geo","geoPosition", "tags", "type","email","maximumAttendees","organizer","organizerName","timeZone"];

        finder.addSelectedToForm=function(keyForm, multiple){
                        mylog.log("finder.addSelectedToForm", keyForm, multiple);
                        if(Object.keys(finder.selectedItems[keyForm]).length > 0){
                            if(!multiple){
                                finder.object[keyForm]={};
                                $(".finder-"+keyForm+" .form-list-finder").html("");
                            }
                            $.each(finder.selectedItems[keyForm], function(e, v){
                                typeCol=(typeof v.collection != "undefined") ? v.collection : v.type;
                                v.name = (notNull(finder.field[keyForm]) ? jsonHelper.getValueByPath(v,finder.field[keyForm]) : v.name)
                                finder.addInForm(keyForm, e, typeCol, v.name, v.profilThumbImageUrl);
                            });
                            if(typeof finder.callback[keyForm] != "undefined") finder.callback[keyForm](finder.selectedItems[keyForm]);
                        }else if(keyForm=="organizer"){
                            if($("#populateFinder").val()!=""){
                                $("#organizerName").val($("#populateFinder").val());
                                $(".organizerNametext").show();
                            }
                        }
                    };
                    
        finder.searchAndPopulateFinder = function(keyForm, text, typeSearch, multiple,field=null){
                        mylog.log("finder.searchAndPopulateFinder", keyForm, text, typeSearch, multiple);
                        //finder.isSearching=true;

                        var dataSearch = {
                            searchType : typeSearch, 
                            name: text
                        };
                        if(notNull(field)){
                            dataSearch["textPath"] = field;
                            dataSearch["fields"] = [field.split('.')[0]]
                        }
                        if(notNull(finder.search) && notNull(finder.search[keyForm])){
                            dataSearch = Object.assign({}, dataSearch, finder.search[keyForm]);
                        }
                        if(finder.filters[keyForm]){
                            dataSearch['filters'] = finder.filters[keyForm];
                        }
                        $.ajax({
                            type: "POST",
                            url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                            data: dataSearch,
                            dataType: "json",
                            success: function(retdata){
                                
                                if(!retdata){
                                    toastr.error(retdata.content);
                                } else {
                                    mylog.log(retdata);
                                    if(retdata.results.length == 0 && finder.invite === true){
                                        $("#form-invite").removeClass("hidden");
                                        $("#list-finder-selection").addClass("hidden");

                                        var search =  "#finderSelectHtml #populateFinder" ;
                                        var id = "#finderSelectHtml #form-invite" ; 
                                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                        if(emailReg.test( $(search).val() )){
                                            $(id+' #inviteEmail').val( $(search).val());
                                            var nameEmail = $(search).val().split("@");
                                            $(id+" #inviteName").val(nameEmail[0]);
                                        }else{
                                            $(id+" #inviteName").val($(search).val());
                                            $(id+" #inviteEmail").val("");
                                        }

                                    } else{
                                        $("#form-invite").addClass("hidden");
                                        $("#list-finder-selection").removeClass("hidden");
                                        finder.populateFinder(keyForm, retdata.results, multiple);
                                        if(keyForm=="organizer"){
                                            if($("#finder-tip").length==0){
                                                $("#list-finder-selected").before("<div id='finder-tip'></div>");
                                            }
                                            $("#finder-tip").html('<span style="font-size:15px"><i class="fa fa-info"></i> Parmi la liste ci-dessous, cliquez sur le nom de votre structure, puis sur le bouton "Ajouter". Si elle n\'apparaît pas, cliquez directement sur "Ajouter"</span>');
                                        }
                                    }
                                }
                            }   
                        });
                    };

        paramsMapCO = $.extend(true, {}, paramsMapCO, {
            activePreview: true,
            mapCustom:{
                icon: {
                    getIcon:function(params){
                        var elt = params.elt;
                        mylog.log("icone semaine TL", elt.tags);
                        var myCustomColour = costum.colors.main1;
                        
                    
                        var markerHtmlStyles = `
                            background-color: ${myCustomColour};
                            width: 3.5rem;
                            height: 3.5rem;
                            display: block;
                            left: -1.5rem;
                            top: -1.5rem;
                            position: relative;
                            border-radius: 3rem 3rem 0;
                            transform: rotate(45deg);
                            border: 1px solid #FFFFFF`;
                    
                        var myIcon = L.divIcon({
                            className: "my-custom-pin",
                            iconAnchor: [0, 24],
                            labelAnchor: [-6, 0],
                            popupAnchor: [0, -36],
                            html: `<span style="${markerHtmlStyles}" />`
                        });
                        return myIcon;
                    }
                },
                getClusterIcon:function(cluster){
                    var childCount = cluster.getChildCount();
                    // var c = ' marker-cluster-';
                    var c = 'bg-orange';
                    // if (childCount < 20) {
                    //     c += 'small-ftl';
                    // } else if (childCount < 50) {
                    //     c += 'medium-ftl';
                    // } else {
                    //     c += 'large-ftl';
                    // }
                    return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster-stl', iconSize: new L.Point(40, 40) });
                },
                getThumbProfil: function (data) {
                    var imgProfilPath = costum.logo;
                    if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != ""){
                        imgProfilPath = baseUrl + data.profilThumbImageUrl;
                    }
                    return imgProfilPath;
                },
                getPopup: function(data){
                    mylog.log("preview map",data);
                    //mylog.log("preview map this" ,this);
                    var id = data._id ? data._id.$id:data.id;
                    var imgProfil = this.getThumbProfil(data);

                    var eltName = data.title ? data.title:data.name;
                    var popup = "";
                    popup += "<div class='padding-5' id='popup" + id + "'>";
                    popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                    popup += "<span class='text-orange' style='margin-left : 5px; font-size:18px; font-weight:700'>" + eltName + "</span>";

                    // if(data.tags && data.tags.length > 0){
                    //     popup += "<div style='margin-top : 5px;'>";
                    //     var totalTags = 0;
                    //     $.each(data.tags, function(index, value){
                            
                    //         if (totalTags < 2 && value!=="Compagnon France Tiers-Lieux") {
                    //             popup += "<div class='text-orange popup-tags'>#" + value + " </div>";
                    //             totalTags++;
                    //         }
                    //     })
                    //     popup += "</div>";
                    // }
                    if(data.startDate && data.startDate.sec && data.endDate && data.endDate.sec){
                        popup += "<div class='popup-date text-dark'>";
                        popup +=    "<i class='fa fa-calendar text-orange'></i> Début : "+moment(data.startDate.sec*1000).tz(data['timeZone'] || 'Indian/Reunion').format('llll');
                        popup +=    "</br><i class='fa fa-calendar text-orange'></i> Fin : "+moment(data.endDate.sec*1000).tz(data['timeZone'] || 'Indian/Reunion').format('llll');
                        popup += "</div>";
                    }
                    if(data.address){
                        var addressStr="";
                        if(data.address.streetAddress)
                            addressStr += data.address.streetAddress;
                        if(data.address.postalCode)
                            addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                        if(data.address.addressLocality)
                            addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                        popup += "<div class='popup-address text-dark'>";
                        popup +=    "<i class='fa fa-map-marker text-orange'></i> Lieu : "+addressStr;
                        popup += "</div>";
                    }
                    // if(data.shortDescription && data.shortDescription != ""){
                    //     popup += "<div class='popup-section'>";
                    //     popup += "<div class='popup-subtitle'>Description</div>";
                    //     popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                    //     popup += "</div>";
                    // }
                    if(data.maximumAttendees){
                        var capacity=(data.maximumAttendees=="0") ? "Places illimitées" : data.maximumAttendees+ " places";
                        popup += "<div class='popup-capacity text-dark'>";
                        popup +=    "<i class='fa fa-thermometer text-orange'></i> Capacité : "+capacity;
                        popup += "</div>";
                    }

                    if(data.organizer || data.organizerName){
                        var organizer=(typeof data.organizer!="undefined" && typeof data.organizer[Object.keys(data.organizer)[0]].name!="undefined") ? data.organizer[Object.keys(data.organizer)[0]].name : (typeof data.organizerName!="undefined" ? typeof data.organizerName!="undefined" : "Temporairement introuvable");
                        popup += "<div class='popup-organizer text-dark'>";
                        popup +=    "<i class='fa fa-users text-orange'></i> Organisateur : "+organizer;
                        popup += "</div>";
                    }


                    if(data.email){
                        popup += "<div class='popup-info-profil' style='margin:unset;'>";
                        popup +=     "<i class='fa fa-envelope fa_email text-orange'></i> Contact : " + data.email;
                        popup += "</div>";
                    }
                    var url = '#page.type.' + data.collection + '.id.' + id;
                    popup += "<div class='popup-section'>";
                    if(paramsMapCO.activePreview)
                        popup += "<a href='" + url + "' class='lbh-preview-element item_map_list popup-marker' id='popup" + id + "'>";
                    else
                        popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
                    popup += '<div class="btn btn-sm btn-more col-md-12">';
                    popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                    popup += '</div></a>';
                    popup += '</div>';
                    popup += '</div>';

                    return popup;
                }
            }
        }); 

        eventTypes={
            workshop : "workshop",
            participativeWork:"participativeWork",
            conference : "conference",
            getTogether : "getTogether",
            tour : "tour"
        };
    },
    "events" : {
        afterBuild: function() {
            $(".parentfinder").hide();
            $(".publiccheckboxSimple").hide();
            //$("#recurrency").val(true);
            $(".recurrencycheckbox").hide();
            $(".organizerNametext").hide();
            $(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");


        },
        afterSave: function(data) {
            var post_save = {};

            post_save['parent'] = {};
            post_save['parent'][costum.contextId] = {
                "type": costum.contextType,
                "name": costum.title
            };
            post_save['links'] = {};
            post_save['links']['attendees'] = {};
            post_save['links']['attendees'][data['map']['creator']] = {
                type: 'citoyens',
                isAdmin: true
            };
            const params = {
                id: data['id'],
                collection: data['map']['collection'],
                path: 'allToRoot',
                value: post_save
            };
            dataHelper.path2Value(params, (callback) => {
                dyFObj.commonAfterSave(params, function() {
                    urlCtrl.loadByHash(currentUrl);
                    
                });
            });
            $("#ajax-modal").modal('hide');
        }
    }
};