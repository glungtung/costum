try {
    if (costum.slug == "ries") {
        pageProfil.views.detail = function () {
            var url = "element/about/type/" + contextData.type + "/id/" + contextData.id;
            ajaxPost('#central-container', baseUrl + '/' + moduleId + '/' + url + "?tpl=ficheInfoElement", null, function () {
                let editButton = ``;
                if (typeof edit_org == 'function') {
                    editButton = `<button onclick="edit_org();" class="btn btn-default pull-right margin-right-10 btn-close-preview">
                            <i class="fa fa-pencil"></i> ${trad.edit}
                        </button>`;
                }
                let contactInfo = `<div class="col-sm-12 col-md-10 col-sm-12 col-sm-8 col-sm-12 col-xs-12">
                    <span class="text-nv-3">
                        <i class="fa fa-address-card-o"></i> Altre Informazioni
                    </span>
                    ${editButton}
                <hr></div>`;

                if (elementData["extraInfo"] && elementData["extraInfo"]["contacts"]) {

                    contactInfo += `<div class="col-sm-12 col-lg-6 card features padding-top-10 padding-bottom-10 margin-left-10 margin-bottom-20">
                    <h5 class="text-primary">Principali destinatari dell’attività</h5>`;
                    if (Array.isArray(elementData["extraInfo"]["contacts"])) {
                        for (let value of elementData["extraInfo"]["contacts"]) {
                            contactInfo += "<span style='text-transform: capitalize;'>" + value + ", </span>";
                        }
                    }

                    contactInfo += "<hr>";


                    if (elementData["extraInfo"] && elementData["extraInfo"]["dimensions"]) {
                        contactInfo += `<h5 class="text-primary">${trad.dimensions} TERRITORIALE</h5>`;

                        contactInfo += "<span style='text-transform: uppercase;'>" + ((typeof trad[elementData["extraInfo"]["dimensions"]] != "undefined") ? elementData["extraInfo"]["dimensions"] : elementData["extraInfo"]["dimensions"]) + "</span>";
                        //contactInfo += "<script> dimensions = '".$elementData["extraInfo"]["dimensions"]."'<hr>  ";
                        contactInfo += "<hr>";

                    }

                    if (elementData["certificationsAwards"]) {
                        contactInfo += `<h5 class="text-primary">${trad.certificationsorawards}</h5>`;
                        if (Array.isArray(elementData["certificationsAwards"])) {
                            for (let value of elementData["certificationsAwards"]) {
                                //contactInfo + = "<script> certifications.push('"+value["certAward"]+"')</script>";
                                contactInfo += "<span style='text-transform: uppercase;'>" + value["certAward"] + ", </span>";

                            }
                        }
                    }

                    contactInfo += "</div>";
                }

                let rete = "";

                if (elementData["extraInfo"] && elementData["extraInfo"]["reteofapartment"]) {
                    rete += `<div class="col-sm-12 col-md-4 card features padding-top-10 padding-bottom-10 margin-left-10 margin-bottom-20">
                            <h5 class="text-primary">${(typeof trad.supplychainnetwork != "undefined") ? trad.supplychainnetwork : "RETI E FILIERE"}</h5>`;
                    if (Array.isArray(elementData["extraInfo"]["reteofapartment"])) {
                        for (let value of elementData["extraInfo"]["reteofapartment"]) {
                            rete += "<span style='text-transform: capitalize;'>" + value + ", </span>";
                        }
                    } else if (elementData["extraInfo"] && typeof elementData["extraInfo"]["reteofapartment"] == "string") {
                        rete += "<span style='text-transform: capitalize;'>" + elementData["extraInfo"]["reteofapartment"] + "</span>";
                    }

                    rete += "<hr></div>";
                }

                let activityPlace = "";

                if (elementData["activityPlace"]) {
                    activityPlace += `<div class="col-sm-12 col-md-4 card features padding-top-10 padding-bottom-10 margin-left-10 margin-bottom-20">
                            <h5 class="text-primary">luogo di attività</h5>`;
                    activityPlace += "<span style='text-transform: capitalize;'>" + (typeof trad[elementData["activityPlace"]] != "undefined" ? trad[elementData["activityPlace"]] : elementData["activityPlace"]) + "</span>";
                    activityPlace += "<hr></div>";
                }

                let transformative = "";

                if (elementData["extraInfo"] && elementData["extraInfo"]["transformativehorizons"]) {
                    transformative += `<div class="col-sm-12 col-md-4 card features padding-top-10 padding-bottom-10 margin-left-10 margin-bottom-20">
                                <h5 class="text-primary">${trad.transformativehorizons}</h5>`;
                    if (Array.isArray(elementData["extraInfo"]["transformativehorizons"])) {
                        for (let value of elementData["extraInfo"]["transformativehorizons"]) {
                            // To translate
                            transformative += "<span style='text-transform: capitalize;'>" + (typeof trad[value] != "undefined" ? trad[value] : value) + ", </span>";
                        }
                    } else if (typeof elementData["extraInfo"]["transformativehorizons"] == "string") {
                        //to translate
                        transformative += "<span style='text-transform: capitalize;'>" + (typeof trad[elementData["extraInfo"]["transformativehorizons"]] != "undefined" ? trad[elementData["extraInfo"]["transformativehorizons"]] : elementData["extraInfo"]["transformativehorizons"]) + "</span>";
                    }

                    transformative += "<hr></div>";
                }



                let involvedPeople = "";
                if (elementData["extraInfo"] && elementData["extraInfo"]["peopleinvolved"]) {
                    involvedPeople += `<div class='col-sm-12 col-md-6 card features padding-top-10 padding-bottom-10 margin-left-10 margin-bottom-20'>
                        <h5 class="text-primary">Persone coinvolte nelle attività</h5>`;
                    if (elementData["extraInfo"]["peopleinvolved"]["workers"]) {
                        involvedPeople += `<p style="font-weight: bolder;">${trad.workers}: ${elementData["extraInfo"]["peopleinvolved"]["workers"]}</p>`;
                    }
                    if (elementData["extraInfo"]["peopleinvolved"]["volunteers"]) {
                        involvedPeople += `<p style="font-weight: bolder;">${trad.volunteers}: ${elementData["extraInfo"]["peopleinvolved"]["volunteers"]}</p>`
                    }
                    involvedPeople += `<hr></div>`;
                }

                $('#central-container').append("<div class='col-sm-12 col-lg-10 col-sm-12 col-md-10 margin-left-10 col-sm-12 col-sm-12 col-sm-12 col-xs-12 about-section1'>" + contactInfo + activityPlace + rete + transformative + involvedPeople + "</div>");

            }, null, "html");
        }

        pageProfil.directory.init = function () {
            pageProfil.directory.initView("#central-container");
            var contextDataByLink = pageProfil.directory.communityLinks[pageProfil.params.dir];
            if (typeof costum.lists != "undefined" && typeof costum.lists.types != "undefined") {
                contextDataByLink.types = Object.keys(costum.lists.types);
                contextDataByLink.types.unshift("citoyens");
            }
            var paramsFilter = {
                container: "#filterContainer",
                header: {
                    dom: ".headerSearchIncommunity"
                },
                defaults: {
                    notSourceKey: true,
                    types: contextDataByLink.types,
                    forced: {
                        filters: {}
                    },
                    filters: {
                    }
                },
                results: {
                    smartGrid: true,
                    renderView: "directory." + ((exists(contextDataByLink.render)) ? contextDataByLink.render : "elementPanelHtml"),
                    community: {
                        links: (typeof contextData.links != "undefined") ? contextData.links : null,
                        connectType: pageProfil.params.dir,
                        edit: canEdit
                    }
                },
                filters: {
                    text: true
                }
            };
            // Si canSee on voit les elt privée car on appartient à la communauté

            if (canParticipate) {
                paramsFilter.urlData = baseUrl + "/co2/search/globalautocompleteadmin/type/" + contextData.collection + "/id/" + contextData.id + "/canSee/true";
            }
            if (typeof contextDataByLink.links != "undefined") {
                var linksConnectRequest = (typeof contextDataByLink.links == "string") ? contextDataByLink.links : contextDataByLink.links[contextData.collection];
            }
            if (typeof linksConnectRequest != "undefined"
                && typeof contextDataByLink.parent != "undefined") {
                paramsFilter.defaults.forced.filters["$or"] = {};
                paramsFilter.defaults.forced.filters["$or"]["links." + linksConnectRequest + "." + contextData.id] = { '$exists': true };
                paramsFilter.defaults.forced.filters["$or"]["parent." + contextData.id] = { '$exists': true };
            }
            if (typeof contextDataByLink.organizer != "undefined") {
                paramsFilter.defaults.forced.filters["$or"]["organizer." + contextData.id] = { '$exists': true };
            }
            else if (typeof linksConnectRequest != "undefined") {
                paramsFilter.defaults.forced.filters["links." + linksConnectRequest + "." + contextData.id] = { '$exists': true };
            } else if (typeof contextDataByLink.parent != "undefined") {
                paramsFilter.defaults.forced.filters["parent." + contextData.id] = { '$exists': true };
            }
            if (contextDataByLink.types.length > 1) {
                paramsFilter.filters.types = {
                    "name": "Tipologia",
                    lists: contextDataByLink.types
                };
            }
            if (typeof contextDataByLink.loadEvent != "undefined") {
                paramsFilter.loadEvent = contextDataByLink.loadEvent;
            }
            if (typeof contextDataByLink.type != "undefined") {
                paramsFilter.defaults.forced.filters["type"] = contextDataByLink.type;
            }

            if (typeof contextDataByLink.urlData != "undefined")
                paramsFilter.urlData = contextDataByLink.urlData;
            if (typeof contextDataByLink.filters != "undefined") {
                if (typeof contextDataByLink.filters == "object" && !Array.isArray(contextDataByLink.filters)) {
                    if (typeof contextDataByLink.initList != "undefined") {
                        $.each(contextDataByLink.initList, function (e, v) {
                            contextDataByLink.filters[v].list = contextListFilter[v];
                        });
                        paramsFilter.filters = $.extend(contextDataByLink.filters, paramsFilter.filters);
                    } else
                        $.extend(paramsFilter.filters, contextDataByLink.filters);
                }
                else {
                    if ($.inArray("roles", contextDataByLink.filters) >= 0) {
                        paramsFilter.filters["roles"] = {
                            view: "horizontalList",
                            type: "filters",
                            dom: "#listRoles",
                            field: "links." + linksConnectRequest + "." + contextData.id + ".roles",
                            name: "<i class='fa fa-filter'></i> " + trad.sortbyrole + ":",
                            active: true,
                            typeList: "object",
                            event: "inArray",
                            classList: "pull-left favElBtn btn",
                            list: contextData.rolesLists
                        }
                        //$("#central-container #listRoles").hide();
                    }
                    if ($.inArray("status", contextDataByLink.filters) >= 0) {
                        paramsFilter.filters["status"] = {
                            view: "dropdownList",
                            type: "filters",
                            name: "Ruolo",
                            action: "filters",
                            typeList: "object",
                            event: "exists",
                            list: {
                                "admin": {
                                    label: trad["administrator"],
                                    field: "links." + linksConnectRequest + "." + contextData.id + ".isAdmin",
                                    value: true,
                                    count: (exists(contextData.counts.admin)) ? contextData.counts.admin : 0
                                },
                                "members": {
                                    label: trad[pageProfil.params.dir + "Active"],
                                    field: "links." + linksConnectRequest + "." + contextData.id + ".isInviting&&links." + linksConnectRequest + "." + contextData.id + ".toBeValidated",
                                    value: false,
                                    count: (exists(contextData.counts[pageProfil.params.dir + "Active"])) ? contextData.counts[pageProfil.params.dir + "Active"] : 0
                                },
                                "isInviting": {
                                    label: trad["unconfirmedinvitation"],
                                    field: "links." + linksConnectRequest + "." + contextData.id + ".isInviting",
                                    value: true,
                                    count: (exists(contextData.counts.isInviting)) ? contextData.counts.isInviting : 0
                                },
                                "toBeValidated": {
                                    label: trad["waitingValidation"],
                                    field: "links." + linksConnectRequest + "." + contextData.id + ".toBeValidated",
                                    value: true,
                                    count: (exists(contextData.counts.toBeValidated)) ? contextData.counts.toBeValidated : 0
                                }
                            }
                        }
                    }
                }
            }
            filterGroup = searchObj.init(paramsFilter);
            filterGroup.search.init(filterGroup);
            pageProfil.directory.initEvents();
        }
    }

    if (typeof costum != "undefined" && (costum.slug == "cressReunion" || costum.slug == "cressReunionNetwork")) {
        pageProfil.views.detail = function () {
            mylog.log("pageProfil.views.detail");
            var url = "element/about/type/" + contextData.type + "/id/" + contextData.id + "/view/costum.views.custom.cressReunion.about";
            ajaxPost('#central-container', baseUrl + '/' + moduleId + '/' + url + '?tpl=ficheInfoElement', null, function () {
                $(".menu-linksBtn").addClass("customFollowBtn");
                $(".costumFollowBtn").removeClass("menu-linksBtn");
                $(".costumFollowBtn").removeClass("menu-btn-link")
            }, "html");
        }

        pageProfil.initCallB = function () {
            //mylog.log(" -------------------------------------------- pageProfil.initCallB buildCreateButton ", contextData);
            if (typeof contextData != "undefined" &&
                typeof contextData.type != "undefined" &&
                contextData.type == "citoyens") {
                var filterAddType = ["organizations"];
                typeObj.buildCreateButton(".elementCreateButton", false, {
                    addClass: "col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
                    bgIcon: true,
                    textColor: true,
                    inElement: true,
                    //allowIn:true,
                    contextType: contextData.type,
                    bgColor: "white",
                    explain: true,
                    inline: false
                }, contextData, filterAddType);
            }
        }
    }
} catch (e) {
    alert(JSON.strignify(e));
}


// pageProfil.views.coop;

//pageProfile.views.detail();
