var paramsFilter;

function createFilters(filterContainer, rete, isMapActive, mapContainer=null){

	//var pageApp= window.location.href.split("#")[1];

    //var appConfig=<?php echo json_encode(@$appConfig); ?>;

	var thematic = {};

	var defaultType = {};

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.types != "undefined"){
		defaultType = Object.keys(costum.lists.types);
		//defaultType.push("organizations");
		//Object.assign(defaultType, costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"]["options"]);
	}else{
		defaultType = Object.keys({"NGO" : trad.ong,
						"Cooperative" : trad.servicepublic,
						"Group":trad.group,
						"LocalBusiness":trad.LocalBusiness,
						"GovernmentOrganization":trad.GovernmentOrganization
					});
		//defaultType = Object.keys(costum.lists.types);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.theme != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.theme);
	}

	let defaultScopeList = [];

	if(costum && costum.slug && costum.slug=="ries"){
		defaultScopeList = ["IT"];
	}else{
		defaultScopeList = ["FR","RE"];
	}

	/*var mapFilter = new CoMap({
            container : mapContainer,
            activePopUp : true,
            mapOpt:{
                btnHide : false,
                doubleClick : true,
                zoom : 2,
            },
            mapCustom:{
                tile : "maptiler"
            },
            elts : []
        });
*/
	//$("#filters-nav").addClass("nav navbar-nav");

	let defaultFilters = {};

	if(rete){
		defaultFilters["extraInfo.reteofapartment"] = rete;
	}

	paramsFilter= {
		container : filterContainer,//"#filters-nav",
		mapObj : {
			parentContainer : mapContainer,
		},
		defaults : {
		 	types : ["organizations"],
			forced: defaultFilters
		},
		filters : {
			theme : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			remove0: true,
	 			countResults: true,
	 			name : trad.searchbytheme,
	 			event : "tags",
	 			keyValue: true,
	 			list : thematic
	 		},
	 		category : {
		 		view : "dropdownList",
		 		type : "filters",
				field: "type",
		 		name : trad.searchbytype,
		 		event : "filters",
		 		list : defaultType
		 	},
 			scopeList : {
	 			name : trad.searchbyplace,//"<?php echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList,
	 				level : ["3"]
	 			}
	 		},
	 		text : {
				placeholder: "Cerca per #tag o testo"
			}
	 	},
	 	results : {
	 		map: {active: isMapActive},
			renderView: "directory.classifiedPanelHtml",
		 	smartGrid : true
		},
	};

	if(isMapActive){
		$("#mapContent").show();
	}
}
