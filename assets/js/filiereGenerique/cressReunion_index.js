/*var eventTypes = {
	workshop: "Atelier",
	tasting : "Dégustation",
	secondHandStoreJumbleSale : "Dépôt-vente, Braderie",
	radioShow : "Emission de radio",
	exhibition: "Exposition",
	trainingAwareness : "Formation, sensibilisation",
	forumMeetingTradeFair : "Forums, Recontres, salons",
	openHouse : "Portes-ouvertes",
	filmProjection : "Projection de film",
	others: "Autres"
};*/
var eventTypes = {
	workshop: "Atelier",
	meeting: "Conférence - Table ronde",
	tasting: "Dégustation",
	secondHandStoreJumbleSale: "Dépôt-vente - braderie",
	radioShow: "Emission de radio/TV",
	sportifEvent: "Evénement sportif - Spectacle",
	exhibition: "Exposition",
	trainingAwareness: "Formation - Sensibilisation",
	forumMeetingTradeFair: "Forum - Rencontres - Salons",
	openHouse: "Portes-ouvertes",
	filmProjection: "Projection de film",
	businessMeet: "Rencontre d'affaires",
	others: "Autres"
};

directory.eventPanelHtml = function(params){
	return costum.cressReunion.directoryEvent(params.id, params);
};


// mapObj.dataMarker= function(mObj,key,params,latCom,lonCom,nameCity){

// 			if (typeof params.opt == "undefined" || params.opt == null)
// 								params.opt = {};

// 			params.opt.icon = mObj.addIcon(params);
// 			var latLon = [latCom, lonCom];
// 			mObj.distanceTo(latLon);
// 			var marker = L.marker(latLon, params.opt);

// 			mObj.markerList[key] = marker;

// 			if (typeof mObj.activePopUp != "undefined" && mObj.activePopUp === true)
// 				mObj.addPopUp(marker, params.elt);

// 			mObj.arrayBounds.push(latLon);
// 			if (mObj.activeCluster === true) {
// 				//mylog.log("giiiiiiiiiiiiiiiiii", marker);
// 				mObj.markersCluster.addLayer(marker);
// 			}
// 			else {
// 				marker.addTo(mObj.map);
// 				if (typeof params.center == "undefined" || params.center === true) {
// 					//mylog.log("mObj.panTo");
// 					mObj.map.panTo(latLon);
// 				}
// 			}	
// 			marker.off().on('click', function (e) {
// 				mylog.log("markerClick",params, nameCity);
// 	            pacte.caseCollectif(params, nameCity);
// 				coInterface.bindLBHLinks();
// 			});
		
// 			if(mObj.mapOpt.mouseOver === true){
// 				marker.on('mouseover', function (e) {
// 					mObj.openPopup();
// 					coInterface.bindLBHLinks();
// 				});
				
// 				marker.on('mouseout', function (e) {
// 					var thismarker = mObj;
// 					setTimeout(function(){
// 						thismarker.closePopup();
// 					}, 2000);
// 				});	
// 			}

// 		};

// mapObj.addMarker = function (params) {
// 	var obj = this;
// 	if (typeof params.elt != "undefined" && params.elt != null){

		
// 		if( typeof params.elt.scope != "undefined" && params.elt.scope != null){
// 			i = 0;
// 			$.each(params.elt.scope, function(e,v){
// 				if(	typeof v.geo != "undefined" && v.geo != null &&
// 					typeof v.geo.latitude != "undefined" && v.geo.latitude != null &&
// 					typeof v.geo.longitude != "undefined" && v.geo.longitude != null) {
// 					var latCom = v.geo.latitude;
// 					var lonCom = v.geo.longitude;
// 					var nameCity = v.cityName;
// 					obj.dataMarker(obj,(params.elt.id+i),params,latCom,lonCom,nameCity);
// 				}
// 				i++;
// 			});
// 		} 
			
// 		if(typeof params.elt.geo != "undefined" && params.elt.geo != null && 
// 			typeof params.elt.geo.latitude != "undefined" && params.elt.geo.latitude != null &&
// 			typeof params.elt.geo.longitude != "undefined" && params.elt.geo.longitude != null) {
// 				var latCom = params.elt.geo.latitude;
// 				var lonCom = params.elt.geo.longitude;
// 				var nameCity = "";
// 				if(typeof params.elt.scope != "undefined" && params.elt.scope != null){
// 					var keyScope = Object.keys(params.elt.scope)[0];
// 					nameCity = params.elt.scope[keyScope].cityName;
// 				}
// 				obj.dataMarker(obj,params.elt.id,params,latCom,lonCom,nameCity);
				
// 		}
// 	}
// };

mapCustom.markers = {
		eventess : modules.map.assets + '/images/markers/logo-lemois.png',
		orgaess : modules.map.assets + '/images/markers/ngo-ess.png',
		default: modules.map.assets + '/images/markers/citizen-marker-default.png',
		organization: modules.map.assets + '/images/markers/ngo-marker-default.png',
		classified: modules.map.assets + '/images/markers/classified-marker-default.png',
		proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
		poi: modules.map.assets + '/images/markers/poi-marker-default.png',
		project: modules.map.assets + '/images/markers/project-marker-default.png',
		event: modules.map.assets + '/images/markers/event-marker-default.png',
		answer: modules.map.assets + '/images/markers/services/tools-hardware.png'
};

mapCustom.markers.getMarker = function (data) {
			//mylog.log("mapObj.getMarker", data.type);
			var imgM = mapCustom.markers.default;
			if (data.collection=="events" && typeof data.tags != "undefined" && $.inArray("MoisESS2021", data.tags) > -1 )
				imgM = mapCustom.markers.eventess;
			else if (data.collection=="organizations" && typeof data.tags != "undefined" && $.inArray("MoisESS2021", data.tags) > -1 )
				imgM = mapCustom.markers.orgaess;
			else if (typeof data.profilMarkerImageUrl !== "undefined" && data.profilMarkerImageUrl != "")
				imgM = baseUrl + data.profilMarkerImageUrl;
			else if (typeof data.collection != "undefined" && data.collection == null)
				imgM = mapCustom.markers.default;
			else if (jQuery.inArray(data.collection, ["organization", "organizations", "NGO", "Cooperative"]) != -1)
				imgM = mapCustom.markers.organization;
			else if (jQuery.inArray(data.collection, ["project", "projects"]) != -1)
				imgM = mapCustom.markers.project;
			else if (jQuery.inArray(data.collection, ["event", "events"]) != -1)
				imgM = mapCustom.markers.event;
			else if (jQuery.inArray(data.collection, ["classified", "classifieds"]) != -1)
				imgM = mapCustom.markers.classified;
			else if (jQuery.inArray(data.collection, ["proposal", "proposals"]) != -1)
				imgM = mapCustom.markers.proposal;
			else if (jQuery.inArray(data.collection, ["poi"]) != -1)
				imgM = mapCustom.markers.poi;
			else
				imgM = mapCustom.markers.default;
			if (jQuery.inArray(data.collection, ["answers"]) != -1)
				imgM = mapCustom.markers.poi;
			//mylog.log("mObj imgM", imgM);
			return imgM;
		}

// mapCustom.addIcon = function(elt){
// 	mylog.log("elt addicon custom", elt);
// 	if (elt.collection=="organizations" && typeof elt.tags != "undefined" && $.inArray("MoisESS2020", elt.tags) > -1 ){
// 			var myCustomColour = '#428bca';
		
// 	}else{
// 		mapCustom.markers.getMarker(elt);
// 	}

// 	var markerHtmlStyles = `
// 		background-color: ${myCustomColour};
// 		width: 3.5rem;
// 		height: 3.5rem;
// 		display: block;
// 		left: -1.5rem;
// 		top: -1.5rem;
// 		position: relative;
// 		border-radius: 3rem 3rem 0;
// 		transform: rotate(45deg);
// 		border: 1px solid #FFFFFF`;

// 	var myIcon = L.divIcon({
// 		className: "my-custom-pin",
// 		iconAnchor: [0, 24],
// 		labelAnchor: [-6, 0],
// 		popupAnchor: [0, -36],
// 		html: `<span style="${markerHtmlStyles}" />`
// 	});
// 	return myIcon;
// };


costum.cressReunion = {
	init : function(){
		dyFObj.formInMap.forced.countryCode="RE";
		dyFObj.formInMap.forced.map={"center":[-21.115141, 55.536384],"zoom" : 9};
		dyFObj.formInMap.forced.showMap=true;
	},
	organisations : {
		afterSave : function(data){
			/*toastr.success(trad.waitingValidation);
			dyFObj.commonAfterSave(data, callB);*/
			
		}
	},
	"events" : {
		results:[],
		formData : function(data){
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}

			});
			if(typeof data.monthEss != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				if(data.monthEss == "Oui") {
					data.tags.push("MoisESS2021");
				}	
				//delete data.mainTag;
			}
			if(typeof data.format != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				if(data.format == "En ligne (Webinaire, webcast, etc)" || data.format == "Les deux, présentiel et distanciel") {
					data.tags.push("Événement en ligne");
				}
				//delete data.mainTag;
			}
			if(typeof data.type != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
					data.tags.push(eventTypes[data.type]);
			}

			if(typeof data.amount != "undefined"){
				data["amout"] = amount;
			}

			return data;
		}
	},
		directoryEvent : function(keyE, valE){
		mylog.log("costum.cressReunion.directoryEvent", keyE, valE);
			costum.cressReunion.events.results[keyE] = valE;
				var str = ''; 
				var style = '';
		// 		if (typeof valE.tags != "undefined" && $.inArray("MoisESS2020", valE.tags) > -1 ){
		// 			style= 'background-color:#9fd8c6 !important;'
		// }
		str += '<div class=\'col-xs-12 searchEntityContainer '+valE.containerClass+' contain_'+valE.collection+'_'+valE.id+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+valE.id+'\' style="'+style+'">';

		// if(valE.updated != null && valE.updated.indexOf(trad.ago)>=0)
		// 	valE.updated = trad.rightnow;
		// if(Date.parse(valE.startDate) > Date.now()){
		// 	valE.updated='';
		// }
		// if(valE.updated != null && valE.updated!= '')
		// 	str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> ' + valE.updated + '</div>';

		var dateFormated = directory.getDateFormated(valE);
		// var hoursFormated = directory.showDatetimePost(valE);
		valE.attendees = '';
		var cntP = 0;
		var cntIv = 0;

		if(typeof valE.links != 'undefined')
			if(typeof valE.links.attendees != 'undefined'){
				$.each(valE.links.attendees, function(key, val){ 
					if(typeof val.isInviting != 'undefined' && val.isInviting == true)
						cntIv++; 
					else
						cntP++; 
				});
			}

		valE.attendees = '<hr class=\'margin-top-10 margin-bottom-10\'>';
        
		var isFollowed=false;
		if(typeof valE.isFollowed != 'undefined' ) isFollowed=true;

			var isShared = false;
			var tip = trad['interested'];
			var actionConnect='follow';
			var icon='chain';
			var classBtn='';
			if(isFollowed){
				actionConnect='unfollow';
				icon='unlink';
				classBtn='text-green';
			}

			var btnLabel  = (actionConnect=="follow")?"S'INSCRIRE":"DÉJÀ INSCRIT-E"

			valE.attendees += "<div class='col-xs-12' style='display: flex;align-items: center;'>";


			valE.attendees += '<button class=\'btn btn-warning btn-sm tooltips customFollowBtn \'' + 
                      ' data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                      ' data-ownerlink=\''+actionConnect+'\' data-id=\''+valE.id+'\' data-type=\''+valE.collection+'\' data-name=\''+valE.name+'\''+
                      ' data-isFollowed=\''+isFollowed+'\'>'+
                      '<i class=\'fa fa-'+icon+'\'></i> '+btnLabel+
                    '</button>';

      
		valE.attendees += '<span class=\'light margin-left-10 margin-right-5 tooltips pull-left\'  '+
                                    'data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad['attendee-s']+'\'>' + 
                              cntP + ' <i class=\'fa fa-street-view\'></i><span class="hidden-xs"> '+trad['attendee-s']+
                            '</span></span>';

        valE.attendees += '<span class="margin-left-5 margin-right-5">-</span>';                    	
		valE.attendees += '<span class=\'light margin-left-10 tooltips pull-left\'  '+
                                    'data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad['guest-s']+'\'>' +
                               cntIv + ' <i class=\'fa fa-envelope\'></i><span class="hidden-xs"> '+trad['guest-s']+
                            '</span>'+
                            '</span>';
		if(userId != null && userId != '' && valE.id != userId){

				valE.attendees += '<button id=\'btn-share-event\' style="padding: 5px !important;background-color:#bfd6ec" class=\'padding-5 margin-left-10 text-dark btn btn-link no-padding margin-left-10 elementButtonHtml ssmla btn-share\''+
                              ' data-action="share" data-id=\''+valE.id+'\' data-type=\''+valE.collection+'\' '+//data-name='"+valE.name+"'"+
                              ' data-isShared=\''+isShared+'\'>'+
                              '<i class=\'fa fa-retweet\'></i> '+trad['share']+'</button>';
      	
      	//valE.attendees += ` <a href="javascript:;" class="elementButtonHtml ssmla btn btn-default btn-menu-tooltips " data-action="share" data-toggle="tooltip" data-placement="bottom" title=""><span class=" tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Partager"><i class="fa fa-share"></i></span><span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">Partager</span><span class="hidden-sm">Partager</span></a>`;
			}	                            	
		valE.attendees +="</div>";

		

		var countSubEvents = ( valE.links && valE.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(valE.links.subEvents).length+' '+trad['subevent-s']  : '' ; 
		str += '<div class="col-xs-12 col-sm-4 col-md-4 no-padding">'+
                  '<a href="'+valE.hash+'" class="container-img-profil lbh-preview-element add2fav block">'+valE.imageProfilHtml+'</a>'+  
                '</div>';
        
		//if(userId != null && userId != '' && valE.id != userId /*&& !inMyContacts(valE.typeSig, valE.id)*/){
		/*	var tip = trad['interested'];
			var actionConnect='follow';
			var icon='chain';
			var classBtn='';
			if(isFollowed){
				actionConnect='unfollow';
				icon='unlink';
				classBtn='text-green';
			}
			str += '<a href=\'javascript:;\' class=\'btn btn-default btn-sm btn-add-to-directory bg-white tooltips customFollowBtn '+classBtn+'\'' + 
                      'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                      ' data-ownerlink=\''+actionConnect+'\' data-id=\''+valE.id+'\' data-type=\''+valE.collection+'\' data-name=\''+valE.name+'\''+
                      ' data-isFollowed=\''+isFollowed+'\'>'+
                      '<i class=\'fa fa-'+icon+'\'></i> Participer'+
                    '</a>';
		}*/

		if (typeof valE.tags != "undefined" && $.inArray("MoisESS2021", valE.tags) > -1 ){
			str +="<img class='imgEss col-md-2 col-sm-2 col-xs-12' src='" + moduleUrl + "/images/cressReunion/logo-moisess20.png'>";
		}
		str += '<div class=\'col-md-6 col-sm-6 col-xs-12 margin-top-25\'>';
		str += dateFormated;
		// str += hoursFormated;
		str += '</div>';
        

		str += '<div class=\'col-md-8 col-sm-8 col-xs-12 entityRight padding-top-10 margin-top-10 pull-right\' style=\'border-top: 1px solid rgba(0,0,0,0.2);\'>';

		var thisLocality = '';
		if(valE.fullLocality != '' && valE.fullLocality != ' ')
			thisLocality = '<small class=\'margin-left-5 letter-red\'><i class=\'fa fa-map-marker\'></i> ' + valE.fullLocality + '</small>' ;
		else thisLocality = '';
                
		var typeEvent = (typeof tradCategory[valE.typeEvent] != 'undefined') ? tradCategory[valE.typeEvent] : trad.event;
        
		str += '<h5 class=\'text-dark lbh add2fav no-margin\'>'+
                  '<i class=\'fa fa-reply fa-rotate-180\'></i> ' + tradCategory[valE.type] +
               '</h5>';

		str += '<a href=\''+valE.hash+'\' class=\'entityName text-dark lbh-preview-element add2fav\'>'+
                  valE.name + 
               '</a>';
        
		if('undefined' != typeof valE.organizer && valE.organizer != null){ 
			var countOrganizer=Object.keys(valE.organizer).length;
			str += '<div class=\'col-xs-12 margin-top-10 no-padding\'>';
			str+='<span class=\'bold\'>'+tradDynForm.organizedby+' : </span>';
			$.each(valE.organizer, function(e,v){
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl!='' ) ? baseUrl+'/'+v.profilThumbImageUrl: assetPath + '/images/thumb/default_'+v.type+'.png';  
				str+='<a href="#page.type.'+v.type+'.id.'+e+'" class="lbh-preview-element tooltips" ';
				if(countOrganizer>1) str+= 'data-toggle="tooltip" data-placement="top" title="'+v.name+'"';
				str+='>'+
              '<img src="'+imgIcon+'" class="img-circle margin-right-10" width="35" height="35"/>';
				if(countOrganizer==1) str+=v.name;
				str+='</a>';     
			});
			str+='</div>';

		}
		// str +=    '<div class=\'entityDescription margin-bottom-10\'>' + 
  //                   valE.description + 
  //                 '</div>';
		// str +=    '<div class=\'margin-bottom-10 col-md-12 no-padding\'>' + 
  //                   valE.attendees + 

  //                 '</div>';

  		str +=    '<div class="margin-bottom-10 col-md-12 no-padding">' + 
                    valE.attendees + 

                  '</div>';
        if(typeof valE.tags != "undefined" && valE.tags != null){          
		str +=  '<div class=\'col-xs-12 tagsContainer\'>';
			$.each(valE.tags,function(i,tv){
	 	    	str += "<span style='background-color:transparent;color:#0054a7' class='padding-right-5 bold btn-tag tag' data-tag-value="+tv+" data-tag-label="+tv+">#" +tv+ "</span>";	
	 	   	});

		str +=	'</div>';
	}



		str += '</div>';
            
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	}

};

costum.calendar = {
	convert : function(eventObj, taskCal){
		mylog.log("calendarObj.results.convert costum", taskCal);	
		var backgroundColor = "#FFA200 !important";
		$.each(costum.paramsData.poles, function(kT, vT){
			//mylog.log("directoryEvent poles", kT, vT);
			if(typeof  eventObj.tags != "undefined" && 
				 eventObj.tags != null &&
				  eventObj.tags.length > 0 &&
				  $.inArray(kT, eventObj.tags) > -1)
				backgroundColor = vT.color+ " !important" ;
		});

		taskCal.backgroundColor = backgroundColor;

		return taskCal;
	}
};


// function searchMapCress(){
// 	mapCO.showLoader();
// 	var paramsSearchMap=searchInterface.constructObjectAndUrl();
// 	mylog.log("fObj searchMapCress paramsSearchMap", paramsSearchMap);
// 	$.ajax({
//         type: "POST",
//         url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
//         data: paramsSearchMap,
//         dataType: "json",
//         error: function (data){
//             mylog.log(">>> error autocomplete search"); 
//             mylog.dir(data);   
//             $("#dropdown_search").html(data.responseText);  
//             //signal que le chargement est terminé
//             loadingData = false;     
//         },
//         success: function(data){ 
//         mylog.log(">>> fObj searchMapCress success autocomplete search !!!! ", data); //mylog.dir(data);
//             if(!data){ 
//                 toastr.error(data.content); 
//             } 
//             else{ 
//                 mapCO.clearMap();
//                 mapCO.addElts(data.results);
//             }
//         }
//     }); 
// }
if(typeof dyFCustom != "undefined"){
	dyFCustom.timeoutSiret = 0 ;
	dyFCustom.siret =  function(){

		$("#ajaxFormModal #siret").off().on("keypress keyup blur", function(e){
				mylog.log("#ajaxFormModal #siret success", $("#ajaxFormModal #siret").val());
				$(this).val($(this).val().replace(/[^\d].+/, ""));
	            if ((e.which < 48 || e.which > 57)) {
	                e.preventDefault();
					if($("#ajaxFormModal #siret").val().trim().length > 1 ){
						if(notNull(scopeObj.timeoutSiret)) 
							clearTimeout(scopeObj.timeoutSiret);

						scopeObj.timeoutSiret = setTimeout(function(){
							var paramsSiret = {
								filters : {
									siret : [ $("#ajaxFormModal #siret").val().trim() ]
								},
								searchType : [ "organizations" ],
								sourceKey : "cressReunion"
							}
							ajaxPost(
								null,
								baseUrl+"/co2/search/globalautocomplete",
								paramsSiret,
								function (data){
									mylog.log("#ajaxFormModal #siret success", data);
									if($("#ajaxFormModal #listsiret").length > 0)
										$("#ajaxFormModal #listsiret").remove();
									var showInput = false;
									if(Object.keys(data.results).length > 0){
										$("#btn-submit-form").prop("disabled",true);
										var str = "<div id='listsiret'>";
										$.each(data.results, function(id, elem) {
											str += 	'<div class="col-xs-12 no-padding">'+
												"<a href='#page.type."+ elem.type +".id."+ id +"'  "+
												" target='_blank' "+
												"class='btn btn-default col-xs-12 text-left padding-5 margin-5'>"+
												'<h5 class="text-green"><i class="fa fa-user"></i> '+
												elem.name+'</h5>'+
												"</a>"+
												'</div>';
										});
										str += "</div>";
										$("#ajaxFormModal #siret").after(str);

									}else{
										$("#btn-submit-form").prop("disabled",false);
										showInput = true;
									}

									$.each(dyFObj.elementObj.dynForm.jsonSchema.properties, function(kP, vP){
										if(kP != "info" && kP != "siret"){
											if(showInput === true)
												$("#ajaxFormModal ."+kP+vP.inputType).show();
											else
												$("#ajaxFormModal ."+kP+vP.inputType).hide();
										}
									});
								},
								function (data){
									mylog.log("error"); mylog.dir(data);
									$("#btn-submit-form").html('Valider <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false);
								},
								"json"
							);
						}, 500);
					}
				}
			});
	}
}
/*function showOrganisationInCitoyens(){
	  setTimeout(function(){
	  	if (contextData.type == "citoyens") {
			var filterAddType = ["organizations"] ;
			typeObj.buildCreateButton(".elementCreateButton", false, {
				addClass:"col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
				bgIcon:true,
				textColor:true,
				inElement:true,
				allowIn:true,
				contextType: contextData.type,
				bgColor : "white",
				explain:true,
				inline:false
			}, null, filterAddType);
	  	}
	},2000)
}
*/

costum.checkboxSimpleEvent = {
    true : function(id){
        if(id=="registrationRequired"){
            $("#ajaxFormModal .expectedAttendeesnumber").show();
        }
        if(id=="isPaying"){
        	$("#ajaxFormModal .amounthidden").prepend("<label>Tarif En Euro</label>" );
        	$("#ajaxFormModal #amount").prop("type", "number");
        	$("#ajaxFormModal #amount").prop("class", "form-control");
        }
    },
    false : function(id){
        if(id=="registrationRequired"){
            $("#ajaxFormModal .expectedAttendeesnumber").hide();
        }
        if(id=="isPaying"){
        	$("#ajaxFormModal .amounthidden label").text("");
        	$("#ajaxFormModal #amount").prop("type", "hidden");
        }
    }
};

var thisElement;

$(document).on("click", ".customFollowBtn", function(event){
	let eventData = costum.cressReunion.events.results[$(this).data("id")];
	thisElement = $(this);
	let isExpAttFull = (typeof eventData != "undefined" && typeof eventData.links.attendees!="undefined" && typeof eventData.expectedAttendees !="undefined" && Object.keys(eventData.links.attendees).length==eventData.expectedAttendees);
	
	if(isExpAttFull==false){
		sectionDyfConfirm = {};
		sectionDyfConfirm.params = {
	    	"jsonSchema" : {
	        "title" : "Confirmez votre email",
	        "description" : "",
	        "icon" : "fa-cog",
	        "properties" : {
	          	"email" : {
	            	"inputType" : "text",
	            	"label" : "Votre mail de notification",
	            	"rules" : {
	            		"required": true,
	            		"email" : true
	            	}
	          	}
	        },
	        afterBuild: function(){
	        	if($("#email-login").length!=0 && $("#email-login").val()!=""){
	        		$("#email").val($("#email-login").val());
	        		subscribeToEvent(thisElement);
	        	}

	        	if(userConnected && userConnected.email){
	        		$("#email").val(userConnected.email);
	        	}
	        },
	        save: function(dynData){
	          	if(typeof eventData != 'undefined' && typeof eventData.mailReminder != "undefined"){      		
		      		let organizers = "";
		      		if(typeof eventData.organizer != "undefined"){
		      			let organizerKeys = Object.keys(eventData.organizer);
		      			for (var k = 0; k < organizerKeys.length; k++) {
		      				organizers+=eventData.organizer[organizerKeys[k]]["name"];
		      				organizers+=(k!=organizerKeys.length-1 && organizerKeys.length!=1)?", ":"";
		      			}
		      		}

		      		let mailReminder;
		      		let eventLocality = "- adresse : ";
		      		if(Array.isArray(eventData.mailReminder)){
		      			mailReminder = eventData.mailReminder;
		      		}else{
		      			mailReminder = [eventData.mailReminder];
		      		}

		      		if(typeof eventData.address != "undefined"){
		      			eventLocality+=(typeof eventData.address.streetAddress!="undefined" && eventData.address.streetAddress != "")?eventData.address.streetAddress+", " : "";
								eventLocality+=(typeof eventData.address.postalCode!="undefined" && eventData.address.postalCode!="")?eventData.address.postalCode+", " : "";
								eventLocality+=(typeof eventData.address.addressLocality!="undefined" && eventData.address.addressLocality!="")?eventData.address.addressLocality+", " :"";
		      		}else{
		      			eventLocality = "";
		      		}

		      		mailReminder.push("0");
		      		let jourj = "";

		        	for (var i = 0; i < mailReminder.length; i++){
		        		let theDate = new Date(eventData.startDate);
		        		theDate.setDate(theDate.getDate() - parseInt(mailReminder[i]));
		        		
		        		jourj = mailReminder[i];
			        	
			        	let paramsmail = {
		            		tpl : "basic",
		            		tplObject : ((jourj!="0")?"J-"+jourj+" avant ":"C’est aujourd’hui : ")+eventData.name,
		            		tplMail : $("#email").val(),
		            		sendOn : JSON.stringify(theDate),
		            		html: (jourj!="0")?`Bonjour, plus que ${jourj} jours avant ${eventData.name} organisé par ${organizers}, auquel vous vous êtes inscrit.e. 
												A bientôt`:`Bonjour, ${eventData.name} organisé par ${organizers}, se tiendra aujourd’hui:
													-${theDate.getHours()}:${theDate.getMinutes()}
													${eventLocality}
													`,
		            		btnRedirect : {
		            			hash : "#agenda?preview=events."+eventData.id,
		            			label : "Détails de l'évenement"
		            		}
		            	}
						ajaxPost(null,
		              		baseUrl+"/co2/mailmanagement/schedule",
		              		paramsmail,
		              		function(data){}
			        		);
		        		}
		          		toastr.success("Merci pour votre inscription. Vous êtes susceptible de recevoir un rappel par e-mail pour ne pas manquer l'événement. A bientôt !");
		      		}
	        		urlCtrl.loadByHash(window.location.hash);
	      		}					
	      	}
	    }

		if(userId){
			if (thisElement.data("ownerlink")=="follow"){
	  			dyFObj.openForm(sectionDyfConfirm.params, null, {});
		  	}
		  	subscribeToEvent(thisElement);
		}else{
		  	$("#modalLogin").on('show.bs.modal' , function(e){
		  		if($("#infoBL").length==0){
		  			$("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
						Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
						<a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
					</div>`);
		  		}
		  	});
		  	$("#modalLogin").on('hide.bs.modal' , function(e){
		  		$("#infoBL").remove();
		  	});
		  	dyFObj.openFormAfterLogin = {
				type : sectionDyfConfirm.params, 
				afterLoad : null,
				data : {}
			};
			toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
		  	Login.openLogin();
		}
	}else{
		if(userId){
			subscribeToEvent(thisElement);
		}
	}

	if(typeof eventData != "undefined" && isExpAttFull && thisElement.data("ownerlink")=="follow"){
		bootbox.dialog({message:`<div class="alert-white text-center"><br>
      <strong>Désolé ! Il n'y a plus de place. Le nombre maximal de participants a été atteint</strong>
      <br><br>
      <button class="btn btn-default bootbox-close-button">OK</button>
      </div>`});
	}
});

function subscribeToEvent(eventSource, theUserId = null){
	var labelLink = "";
	var parentId = eventSource.data("id");
	var parentType = eventSource.data("type");
	var childId = (theUserId)?theUserId:userId;
	var childType = "citoyens";
	var name = eventSource.data("name");
	var id = eventSource.data("id");
	//traduction du type pour le floopDrawer
	eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
	var connectType = (parentType == "events") ? "connect" : "follow";
	
	if (eventSource.data("ownerlink")=="follow"){
	    callback=function(){
	    	labelLink=(parentType == "events") ? "DÉJÀ INSCRIT-E" : trad.alreadyFollow;
	      	if(eventSource.hasClass("btn-add-to-directory"))
	        	labelLink="";
	      	eventSource.html("<small><i class='fa fa-unlink'></i> "+labelLink.toUpperCase()+"</small>");
	      	eventSource.addClass("active");
	      	eventSource.data("ownerlink","unfollow");
	      	eventSource.data("original-title", labelLink);
	    }
	    if(parentType=="events")
	    	links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
	    else
	    	links.follow(parentType, parentId, childId, childType, callback);
	}else{
	    //eventSource.data("ownerlink")=="unfollow"
	    connectType = (parentType == "events") ? "attendees" : "followers";
	    callback=function(){
	    	labelLink=(parentType == "events") ? "S'INSCRIRE":"DÉJÀ INSCRIT-E";
	      	if(eventSource.hasClass("btn-add-to-directory"))
	        	labelLink="";
	      	$(eventSource).html("<small><i class='fa fa-chain'></i> "+labelLink.toUpperCase()+"</small>");
	      	$(eventSource).data("ownerlink","follow");
	      	$(eventSource).data("original-title", labelLink);
	      	$(eventSource).removeClass("text-white");
	    };
	    links.disconnectAjax(parentType, parentId, childId,childType,connectType, null, callback);
	}
}


// User Register form modal

let zoneResidenceHTML = "";
let situationOptionsHTML = "";

if(typeof (costum.lists.arrondissement) != "undefined"){
	let arrondList = costum.lists.arrondissement;
	zoneResidenceHTML += '<option>séléctionner</option>';
	for (var k = 0; k < arrondList.length; k++) {
	  zoneResidenceHTML += '<option value="'+arrondList[k]+'">'+arrondList[k]+'</option>';
	}
}

if(typeof (costum.lists.situation) != "undefined"){
	let situationList = costum.lists.situation;
	situationOptionsHTML += '<option>séléctionner</option>';
	for (var k = 0; k < situationList.length; k++) {
	  situationOptionsHTML += '<option value="'+situationList[k]+'">'+situationList[k]+'</option>';
	}
}


	Login.runRegisterValidator  = function(params) { 
		var form4 = $('.form-register');
		var errorHandler3 = $('.errorHandler', form4);
		var createBtn = null;
		$(".form-register .container").removeClass("col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12");

		if($("#gender").length==0){
			$("#modalRegister .form-register-inputs div:eq(5)").append(`
				<div class="telephoneRegister">
		            <label class="letter-black"><i class="fa fa-phone"></i> Téléphone </label><br>
		            <input class="form-control" id="telephone" name="telephone" placeholder="Numéro téléphone"><br/>
		        </div>

				<div class="genderRegister">
		            <label class="letter-black"><i class="fa fa-user"></i> Vous êtes un.e </label><br>
		            <select class="form-control" id="gender" name="gender">
		            	<option value="Homme">Homme</option>
		            	<option value="Femme">Femme</option>
		            </select><br/>
		        </div>

		        <div class="situationRegister">
		            <label class="letter-black"><i class="fa fa-chevron-down"></i> Précisez votre situation actuelle </label><br>
		            <select class="form-control" id="situation" name="situation">
		            	${situationOptionsHTML}
		            </select><br/>
		        </div>

		        <div class="arrondissementRegister">
		            <label class="letter-black"><i class="fa fa-bullseye"></i> Votre arrondissement de résidence </label><br>
		            <select class="form-control" id="arrondissement" name="arrondissement">
		            	${zoneResidenceHTML}
		            </select><br/>
		        </div>
			`);
		}
		
		form4.validate({
			rules : {
				name : {
				required : true,
				minlength : 4
			},
			telephone : {
				required : true
			},
			gender : {
				required : true
			},
			situation : {
				required : true
			},
			arrondissement : {
				required : true
			},
			username : {
				required : true,
				validUserName : true,
				rangelength : [4, 32]
			},
			email3 : {
				required : { 
				 	depends:function(){
				 		$(this).val($.trim($(this).val()));
				 		return true;
				 	}
				},
				email : true
			},
			password3 : {
				minlength : 8,
				required : true
			},
			passwordAgain : {
				equalTo : "#password3",
				required : true
			}
		},
		submitHandler : function(form) { 
			errorHandler3.hide();
			$(".createBtn").prop('disabled', true);
    		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
			var params = { 
			   "name" : $('.form-register #registerName').val(),
			   "username" : $('.form-register #username').val(),
			   "email" : $(".form-register #email3").val(),
			   "gender" : $(".form-register #gender").val(),
			   "situation" : $(".form-register #situation").val(),
			   "telephone" : $(".form-register #telephone").val(),
			   "arrondissement" : $(".form-register #arrondissement").val(),
			   "pendingUserId" : pendingUserId,
			   "pwd" : $(".form-register #password3").val()
            };
            if($('.form-register #isInvitation').val())
            	params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
		  		params.scope = scopeObj.selected;
		  	}
		  	if($(".form-register #newsletter").is(":checked"))
		  		params.newsletter=true;
		  	if($(".form-register #newsletterCollectif").is(":checked"))
		  		params.newsletterCollectif=true;
		  	var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
		  	
		  	//if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
		  	//	params.msgGroup=$(".form-register #textMsgGroup").val();
		  	ajaxPost(
	            null,
	            baseUrl+"/costum/cressreunion/register",
	            params,
	            function(data){ 
	                if(data.result) {
	                	mylog.log("Register Formulaire",data.result);
		    		  	//createBtn.stop();
						$(".createBtn").prop('disabled', false);
	    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
						$("#registerName").val("");
						$("#username").val("");
						$("#email3").val("");
						$("#password3").val("");
						$("#passwordAgain").val("");
						$("#passwordAgain").val("");
						$("#registerSurname").val("");
						$(".form-register #gender").val("");
			   			$(".form-register #situation").val("");
			   			$(".form-register #telephone").val("");
			   			$(".form-register #arrondissement").val("");
						$('#newsletter').prop('checked', true);
						$('#newsletterCollectif').prop('checked', true);
						$('#redirectLaunchCollectif').prop('checked', true);
					   	toastr.success("Merci, votre soutien a bien été prise en compte");
	    		  		$('.modal').modal('hide');
	    		  		
	    		  		$("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
		    		  	$("#modalRegisterSuccess").modal({ show: 'true' }); 
		    		  	/*$("#modalRegisterSuccess").on('hide.bs.modal' , function(e){
					  		if(typeof thisElement !="undefined"){
					        	subscribeToEvent(thisElement, data.id);
					        }
					  	});*/
		    		  	$('#modalRegisterSuccess .btn-default').click(function() {
					        mylog.log("hide modale and reload");
					        $('.modal').modal('hide');
					        if(typeof thisElement == "undefined"){
					    		window.location.reload();
					        }
					    	//urlCtrl.loadByHash(window.location.hash);
					    });
		    		}else {
		    		  	toastr.error(data.msg);
		    		  	$('.modal').modal('hide');	    		  	
	    		  		scopeObj.selected={};
		    		}
	            },function(data) {
		    	  	toastr.error(trad["somethingwentwrong"]);
		    	  	$(".createBtn").prop('disabled', false);
					$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
		    	  	//createBtn.stop();
		    	}
	        );
		  	return false;
		},
		invalidHandler : function(event, validator) {//display error alert on form submit
			errorHandler3.show();
			$(".createBtn").prop('disabled', false);
    		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
			//createBtn.stop();
		}
	});
};
Login.runRegisterValidator();



/*
$(document).ready(function(){
	var pageConfirmeReady = window.location.href.split("&");
	if(typeof pageConfirmeReady[1] != "undefined" && pageConfirmeReady[1].includes("confirm=")){
		$(".filters-activate").remove();
		let eventId = pageConfirmeReady[1].replace("confirm=","");
		alert("Ready"+eventId);
		setTimeout(()=>{
			alert(JSON.stringify(costum.cressReunion.events.results));
		}, 1000)
		toastr.success("Merci pour la confirmation de votre inscription");
	}
});*/

/**

var pageConfirme = window.location.href.split("&");
searchObj.search.callBack = function(fObj, results){
	mylog.log("searchObj.search.callBack", results);
	fObj.search.currentlyLoading = false;
	fObj.search.obj.count=false;
	fObj.search.obj.countType=[];
	fObj.filters.actions.text.spin(fObj, false);
	if(Object.keys(results).length < fObj.search.obj.indexStep){
		fObj[fObj.search.loadEvent.active].stopPropagation=true;
		$(fObj.results.dom+" .processingLoader").remove();
	}
	if(fObj.results.smartGrid)
		fObj.results.smartCallBack(fObj, 0);
	if(typeof fObj[fObj.search.loadEvent.active].callBack != "undefined") fObj[fObj.search.loadEvent.active].callBack(fObj,results);

	if(typeof pageConfirme[1] != "undefined" && typeof pageConfirme[2] != "undefined" && pageConfirme[1].includes("confirm=") && pageConfirme[2].includes("email=")){
			if(results.length!=0){
				let eventId = pageConfirme[1].replace("confirm=","");
				let email = pageConfirme[2].replace("email=","");
				let eventData = results.filter(ev => ev["_id"]["$id"] == eventId)[0];
				$(".filters-activate").remove();
				
				
			}
		}
	}

	*/