Login.registerOptions.mode="normal";
Login.registerOptions.loginAfter=true;
//jQuery(document).ready(function() {
//Login.runRegisterValidator();
costum.loadByHash=function(h){
	if($(".show-menu-xs").is(":visible") && $(".btn-menu-xs").is(":visible")){
		$(".show-menu-xs").html('<i class="fa fa-th"></i> Menu</a>');
		$(".btn-menu-xs").fadeOut();
	}
	return h;
}

costum.chtitube={
	init : function(){
		poi={
			filters : {
				"recoveryCenter": { "label": "Ressourcerie <br/>(déchèterie, composteur, poubelle)","key":"recoveryCenter","icon":"recycle"},
		        "rehabilitatedPlace": { "label": "Friche et bâti abandonné","key":"rehabilitatedPlace","icon":"home"},
		        "freeService": { "label": "Libre service <br/>(bibliothèque partagée, mobilier urbain, etc...)","key":"freeService","icon":"heart"},
		        "history"       : { "label": "Patrimoine historique <br/>(place, statue, monument, etc)","key":"history","icon":"clock-o"},
		        "funPlace"      : { "label": "Terrain de jeu","key":"funPlace","icon":"smile-o"},
		        "streetArt"     : { "label": "Art de rue, peinture murale, fresque","key":"streetArts","icon":"paint-brush"},
		        "naturalSpace"     : { "label": "Espace vert et naturel<br/>(parc, étendue d’eau, jardin public, jardin partagé)","key":"naturalSpace","icon":"tree"}
			}
		}
	},
	dynForm: {
		setCommonDynForm: function(form){
			dyFObj.formInMap.forced.countryCode="FR";
			dyFObj.formInMap.forced.map={"center":["50.28382900", "2.74686000"], zoom: 8};
			form.formData = "costum.chtitube.dynForm.formatData";
			//form.afterSave = "costum.chtitube.dynForm.afterSave";
			return form;
		},
		formatData : function(data){
			//alert("oui")
			//data.public=false;
			return data;
		},
		afterSave:function(data){
			uploadObj.afterLoadUploader=false;
			dyFObj.commonAfterSave(data, function(){
				
				if(dyFObj.editMode){
					dyFObj.closeForm();
					urlCtrl.loadByHash(location.hash);
				}
				else{
					dyFObj.closeForm();
					$("#modalAddSuccess").modal('show');
				}
			});
		}
	}
}
costum.bindResizeEvent = function(){
	if(!$(".show-menu-xs").is(":visible"))
		$(".btn-menu-xs").show();
	else
		$(".btn-menu-xs").hide();
	if(height > heightParent){
		$("#menuApp.menuLeft-chtitube").css({"overflow-y":"scroll"});
	}
	else{
		$("#menuApp.menuLeft-chtitube").css({"overflow-y":"hidden"});
	}
}

costum.setTopPosition = function(left, pos){
	$("#modalAddSuccess").css("left", left);
	height=$(".content-menu-left-chtitube").height();
	heightParent=$("#menuApp.menuLeft-chtitube").height();
	if(height > heightParent){
		$("#menuApp.menuLeft-chtitube").css({"overflow-y":"scroll"});
	}
	else{
		$("#menuApp.menuLeft-chtitube").css({"overflow-y":"hidden"});
	}
}
var filterObj = {
	container : "#filterContainer" ,
	urlData : baseUrl+"/" + moduleId + "/search/globalautocomplete",
	init : function(pInit = null){
		mylog.log("fObj init",pInit);
		//Init variable
		var copyFilters = jQuery.extend(true, {}, filterObj);
		copyFilters.initVar(pInit);
		return copyFilters;

	},
	initVar : function(pInit){
		mylog.log("fObj initVar",pInit);
		this.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : "#filterContainer" );
		this.urlData =  ( (pInit != null && typeof pInit.urlData != "undefined") ? pInit.urlData : "#filterContainer" );
		this.initDefaults(pInit);
		this.initViews(pInit);
		this.initActions(pInit);
	},
	initDefaults : function(pInit){
		mylog.log("fObj initDefaults",pInit);
		var str = "";
		var fObj = this;
		if(typeof pInit.defaults != "undefined"){
			mylog.log("fObj initDefaults defaults");
			if(typeof pInit.defaults.fields != "undefined"){
				$.each(pInit.defaults.fields,function(k,v){
					mylog.log("fObj initDefaults fields",k, v);
					if(typeof searchObject.filters == "undefined" )
						searchObject.filters = {};
					if(typeof searchObject.filters[k] == "undefined" )
						searchObject.filters[k] = [];
					searchObject.filters[k].push(v);
					
				});
			}
		}
	},
	initActions : function(pInit){
		mylog.log("fObj initActions",pInit);
		var str = "";
		var fObj = this;
		if(typeof pInit.filters != "undefined"){
			$.each(pInit.filters,function(k,v){
				mylog.log("fObj initActions each", k,v);
				if(typeof v.action != "undefined" && typeof fObj.actions[v.action] != "undefined"){
					fObj.actions[v.action](fObj);
				}
			});
		}
	},
	initViews : function(pInit){
		mylog.log("fObj initViews",pInit);
		var str = '';
		var fObj = this;
		if(typeof pInit.filters != "undefined"){
			$.each(pInit.filters,function(k,v){
				mylog.log("fObj initViews each", k,v);
				if(typeof v.view != "undefined" && typeof fObj.views[v.view] != "undefined"){
					str += fObj.views[v.view](k,v, fObj);
				}
			});
		}
		str+='<div id="activeFilters" class="col-xs-12"></div>';
		$(fObj.container).html(str);
		fObj.initFilters();

	},
	initFilters : function(){
		fObj=this;
		getParamsUrls=location.hash.split("?");
		if(typeof getParamsUrls[1] != "undefined" ){
			var parts = getParamsUrls[1].split("&");
	        var $_GET = {};
	         var initScopesResearch={"key":"open","ids":[]};
	        for (var i = 0; i < parts.length; i++) {
	            var temp = parts[i].split("=");
	            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
	        }
	        if(Object.keys($_GET).length > 0){
	            $.each($_GET, function(e,v){
	                v=decodeURI(v);
	                if(e=="tags"){
	                	tags=v.split(",");
	                	$.each(tags, function(i, tag){
	                		fObj.manage.addActive(fObj,"tags",tag);
	                	});
	                }
	                if(e=="cities" || e=="zones"){
	                	if($.inArray(e,["cities","zones","cp"]) > -1) $.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
	                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
	                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
	                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
			                    	$.each(myScopes.open, function(i, scope){
			                    		if(typeof scope.active != "undefined" && scope.active)
			                				fObj.manage.addActive(fObj, "scope",scope.name,i);
			                		});
			                	}
	                    	});
	                    
	                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
	                    	$.each(myScopes.open, function(i, scope){
	                    		if(typeof scope.active != "undefined" && scope.active)
	                				fObj.manage.addActive(fObj, "scope",scope.name,i);
	                		});
	                	}
	                }    
	         	});
	        }
	    }
	},
	search : function(){
		searchObject.nbPage=0;
		searchObject.count=true;
		startSearch(0);
	},
	searchBar : {
		launch : function(fObj){
			searchObject.text = $(fObj.container+" .searchBar-filters #main-search-bar").val();
			fObj.searchBar.spin(fObj, true);
			fObj.search();
		},
		spin : function(fObj, bool){
			removeClass= (bool) ? "fa-arrow-circle-right" : "fa-spin fa-circle-o-notch";
		    addClass= (bool) ? "fa-spin fa-circle-o-notch" : "fa-arrow-circle-right";
		    $(fObj.container+" .searchBar-filters .main-search-bar-addon").find("i").removeClass(removeClass).addClass(addClass);
		}
	},
	types : {
		add : function(fObj, key){
			searchObject.types=[];
			if(notNull(key))
				searchObject.types.push(key);
			$("#activeFilters .filters-activate[data-type='types']").each(function(){
				searchObject.types.push($(this).data('key'));
			});
			if(searchObject.types.length==0) searchObject.types=fObj.types.initList;
		},
		remove: function(fObj, key){
			searchObject.types.splice(searchObject.types.indexOf(key),1);
			if(searchObject.types.length==0) searchObject.types=fObj.types.initList;
		},
		initList : [] 
	},
	views : {
		text : function(k, v){
			placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : trad.whatlookingfor;
			str='<div class="searchBar-filters pull-left">'+
                '<input type="text" class="form-control pull-left text-center main-search-bar search-bar" id="main-search-bar" placeholder="'+placeholder+'">'+
                '<span class="text-white input-group-addon pull-left main-search-bar-addon " id="main-search-bar-addon">'+
                    '<i class="fa fa-arrow-circle-right"></i>'+
                '</span>'+
            	"</div>";
            	return str;
		},
		types : function(k, v, fObj){
			mylog.log("fObj views selectList", k,v);
			label=(typeof v.name != "undefined") ? v.name: "Types";
			fObj.types.initList=v.lists;
			str='<li class="dropdown">'+
					'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
						label+' <i class="fa fa-angle-down margin-left-5"></i>'+
					'</a>'+
					'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
						'<div class="list-filters">';
				        $.each(v.lists, function(k,type){
				            elt=directory.getTypeObj(type);
				            textColor= (typeof elt.color != "undefined") ? "text-"+elt.color : "";
				            str+='<button class="btn padding-10 col-xs-12 '+textColor+'" data-field="'+v.type+'" data-key="'+type+'" data-type="'+v.type+'" data-value="'+elt.name+'">'+
				                    '<i class="fa fa-'+elt.icon+'"></i> '+
				                    '<span class="elipsis label-filter '+textColor+'">'+elt.name+'</span>'+
				                    '<span class="count-badge-filter '+textColor+'" id="count'+type+'"></span>'+
				                '</button>';
				        });
					str+='</div>'+
					'</div>'+
				'</li>';
			return str;
		},
		tags : function(k,v){
			mylog.log("fObj views tags", k,v);
			var str = "";
			return str;
		},
		select : function(k,v){
			mylog.log("fObj views select", k,v);
			var str = "<select>";
			str += '<option value="-1" >ALL</option>';
			if(typeof v.list != "undefined"){
				$.each( v.list ,function(kL,vL){
					str +='<option value="'+vL+'" >'+vL+'</option>';
				});
			}
			str += "</select>";
			return str;
		},
		selectList : function(k,v){
			mylog.log("fObj views selectList", k,v);
			label=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
			str='<li class="dropdown">'+
					'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
						label+' <i class="fa fa-angle-down margin-left-5"></i>'+
					'</a>'+
					'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
						'<div class="list-filters">';
						if(typeof v.list != "undefined"){
							includeSubList=(typeof v.list=="object" && !Array.isArray(v.list)) ? true : false;
							$.each( v.list ,function(kL,vL){
							if(includeSubList){
									str +='<button class="mainTitle col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+kL+'" >'+kL+'</button>';
									if(Object.keys(vL).length > 0){
										$.each(vL, function(i, sub){
											str +='<button class="col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+sub+'" >'+sub+'</button>';
										});
									}
								}else{
									str +='<button class="col-xs-12" data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+vL+'" >'+vL+'</button>';
								}
							});
						}
					str+='</div></div>'+
				'</li>';
			return str;
		},
		scope : function(){
			mylog.log("fObj views scope");
			return 	'<div id="costum-scope-search"><div id="input-sec-search">'+
							'<div class="input-group shadow-input-header">'+
								'<span class="input-group-addon scope-search-trigger">'+
									'<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'+
								'</span>'+
								'<input type="text" class="form-control input-global-search" autocomplete="off"'+
									' id="searchOnCity" placeholder="Où ?">'+
							'</div>'+
							'<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" '+
								' style="display: none;">'+
								'<div class="content-result">'+
								'</div>'+
							'</div>'+
					'</div></div>';
		}
	},
	
	actions : {
		text : function(fObj){
			$(fObj.container+" #main-search-bar").off().on("keyup",delay(function (e) {
				if(e.keyCode != 13)
					fObj.searchBar.launch(fObj);
					
			}, 750)).on("keyup", function(e){
				if(e.keyCode == 13)
					fObj.searchBar.launch(fObj);
				
			});
			$(fObj.container+"  #main-search-bar-addon").off().on("click", function(){
				fObj.searchBar.launch(fObj);
            });
		},
		types : function(fObj){
			mylog.log("fObj actions type");
			$("button[data-type='types']").off().on("click",function(){
				fObj.types.add(fObj, $(this).data("key"));
				//searchObject.types.push($(this).data("key"));
				fObj.search();
				fObj.manage.addActive(fObj,"types",$(this).data("value"), $(this).data("key"), $(this).data("field"));
			});
		},
		tags : function(fObj){
			mylog.log("fObj actions tags");
			$("button[data-type='tags']").off().on("click",function(){
				searchObject.tags.push($(this).data("value"));
				fObj.search();
				fObj.manage.addActive(fObj,"tags",$(this).data("value"), $(this).data("key"), $(this).data("field"));
			});
		},
		filters : function(fObj){
			mylog.log("fObj actions filters");
			$("button[data-type='filters']").off().on("click",function(){
				mylog.log("fObj actions filters button[data-type='filters']");
				if(typeof $(this).data("field") != "undefined"){
					
					if(typeof searchObject.filters == "undefined" )
						searchObject.filters = {};
					
					if(typeof searchObject.filters[$(this).data("field")] == "undefined" )
						searchObject.filters[$(this).data("field")] = [];
                    searchObject.filters[$(this).data("field")].push($(this).data("value"));
                    //searchObject.searchType[] = "organizations";
					fObj.search();
					fObj.manage.addActive(fObj, "filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
				}
				
			});
		},
		scope : function(fObj){
			mylog.log("fObj actions scope");
			myScopes.open={};
			bindSearchCity("#costum-scope-search", function(){
				$(".item-globalscope-checker").off().on('click', function(){ 
					mylog.log("fObj actions scope .item-globalscope-checker");
					$("#costum-scope-search .input-global-search").val("");
					$(".dropdown-result-global-search").hide(700).html("");
					myScopes.type="open";
					mylog.log("fObj actions scope globalscope-checker",  $(this).data("scope-name"), $(this).data("scope-type"));
					newScope=scopeObject(myScopes.search[$(this).data("scope-value")]);
					$.each(newScope, function(e, v){
						if(typeof v.active == "undefined" || !v.active)
							delete newScope[e];
						else
							newKeyScope=e;
					});
					myScopes.open[newKeyScope]=newScope[newKeyScope];
					localStorage.setItem("myScopes",JSON.stringify(myScopes));
					fObj.manage.addActive(fObj, "scope",$(this).data("scope-name"),newKeyScope);
					fObj.search();
				});
			});
		}
	},
	manage:{
		addActive: function(fObj, type, value, key, field){
			mylog.log("fObj manage addActive", type, value, key);
			var dataKey = (typeof key != "undefined") ? 'data-key="'+key+'" ' : "" ;
			var dataField = (typeof field != "undefined") ? 'data-field="'+field+'" ' : "" ;
			str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
						dataKey +
						dataField +
						'data-value="'+value+'" '+
						'data-name="'+name+'" '+
						'data-type="'+type+'">' + 
					"<i class='fa fa-times-circle'></i>"+
					"<span "+
						"class='activeFilters' "+
						dataKey +
						dataField +
						'data-value="'+value+'" '+
						'data-scope-name="'+name+'" '+
						'data-type="'+type+'">' + 
						value + 
					"</span>"+
				"</div>";
			$(fObj.container+" #activeFilters").append(str);
			coInterface.initHtmlPosition();
			$(fObj.container+" .filters-activate").off().on("click",function(){
				fObj.manage.removeActive(fObj, $(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"));
				$(".tooltip[role='tooltip']").remove();
				$(this).fadeOut(200).remove();
				coInterface.initHtmlPosition();
			});
		},
		removeActive : function(fObj, type, value, key, field){
			mylog.log("fObj manage removeActive", type, value, key, field);
			if(type=="filters"){
				mylog.log("fObj manage removeActive filters", searchObject);
				if( typeof searchObject.filters != "undefined" && typeof searchObject.filters[field] != "undefined")
					searchObject.filters[field].splice(searchObject.filters[field].indexOf(value),1);
			}else if(type=="types")
				fObj.types.remove(fObj, key);
			else if(type=="tags")
				searchObject.tags.splice(searchObject.tags.indexOf(value),1);
			else if(type=="scope"){
				delete myScopes.open[key];
			}
			fObj.search();
		}
	}
};