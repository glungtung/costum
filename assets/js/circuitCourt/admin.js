adminPanel.views.organizations = function(){
	var data={
		title : "Structures",
		table : {
            name: {
                name : "Nom"
            },
            validated : { 
                name : "Valider",
                class : "col-xs-2 text-center"
            },
            tags : { 
                name : "Tags",
                class : "col-xs-2 text-center"
            },
            actions : {
                class : "col-xs-3 text-center"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations" ],
                //private : true
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
            validated : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};


adminPanel.views.category = function(){
    var data={
        title : "Catégorie",
        table : {
            name: {
                name : "Nom"
            },
            tags : { 
                name : "Tags",
                class : "col-xs-2 text-center"
            }
        },
         paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "badges" ],
                //private : true
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : {
                subType : "category"
            },
            delete : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};