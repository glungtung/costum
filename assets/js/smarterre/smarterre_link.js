function stepOneLink(id,mid){

    var params = {
        "parentId" : id,
        "thematique" : window.location.href.split("#")[1]
    }
	
    //async : false
    ajaxPost(
        null,
        baseUrl+"/costum/smarterre/searchurl",
        params,
        function(data){ 
            if(data.result == true){
				stepTwoLink(data,mid);
			}
        }
    ); 
}

//Permet d'afficher les résultats
function stepTwoLink(data,mid){
	
	var d = "<ul>";
	$.each(data.urls,function(key,value){
		
		d += "<li><a href='"+value.url+"' target='_blank'>"+value.name+"</a></li>";
	});

	d += "</ul>";
	$(".affiche"+mid).html(d);

	
}
