adminPanel.views.users = function(){
	var data={
		title : "Gestion des utilisateurs-trices",
		//types : [ "citoyens" ],
		table : {
            name: {
                name : "Nom"
            },
            email : { 
            	name : "E-mail"
            }
        },
        paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "citoyens" ],
				fields : [ "name", "email", "collection" ],
				sort : { name : 1 }
			},
			filters : {
				text : true,
			}
		}
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};