function getFormMapping(data){
	var formsMappings = null;
	if(typeof costum.forms != "undefined"){
		$.each(costum.forms, function(kF, vF){
			if(typeof vF.mapping != "undefined")
				formsMappings = vF.mapping;
		});
	}
	mylog.log("MappingForm", formsMappings);
	if(formsMappings != null){
		data.paramsFilter.defaults.mapping = formsMappings;
	}

	return data;
}

adminPanel.views.openform = function(){
	var idF = null;
	if(typeof costum.forms != "undefined"){
		$.each(costum.forms, function(kF, vF){
			idF = kF;
		});
	}

	if(idF != null)
		ajaxPost('#content-view-admin', baseUrl+'/survey/form/edit/id/'+idF, {}, function(){},"html");
};

adminPanel.views.listforms = function(){
	//ajaxPost('#content-view-admin', baseUrl+'/survey/form/edit/id/', {}, function(){},"html");

	var data={
		title : "Gestion des formulaires",
		id : costum.contextId,
		collection : costum.contextType,
		slug : costum.contextSlug,
		// url : baseUrl+'/survey/form/admindirectory/slug/'+costum.contextSlug,
		table : {
			name: {
				name : "Form"
			}
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "forms" ],
				fields : {}

			},
			filters : {
				text : true
			}
		}
	};
	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
		|| (typeof canEdit != "undefined" && canEdit) ){
		data.actions={};
	}
			
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory', data, function(){},"html");
};




adminPanel.views.community = function(){
	var data={
		        title : trad.community,
		        context : {
		        	id : costum.contextId,
		        	collection : costum.contextType
		        },
		        invite : {
					contextId : costum.contextId,
					contextType : costum.contextType,
		        },
		        table : {
		            name: {
		                name : "Membres"
		            },
		            tobeactivated : {
		                name : "Validation de compte",
		                class : "col-xs-2 text-center"
		            },
		            isInviting : {
		                name : "Validation pour être membres",
		                class : "col-xs-2 text-center"
		            },
		            roles : {
		                name : "Roles",
		                class : "col-xs-1 text-center"
		            },
		            admin : {
		                name : "Admin",
		                class : "col-xs-1 text-center"
		            }
		        },
		        paramsFilter : {
					container : "#filterContainer",
					defaults : {
						types : [ "citoyens", "organizations"],
						fields : [ "name", "email", "links", "collection" ],
						notSourceKey : true
					},
					filters : {
						text : true
					}
				},
		        actions : {
		            admin : true,
		            roles : true,
		            disconnect : true
		        }
		    };

		    data.paramsFilter.defaults.filters = {};
			data.paramsFilter.defaults.filters["links.members."+costum.contextId] = {
				'$exists' : 1 
			};
		  	 ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/id/'+costum.contextId+'/type/'+costum.contextType, data, function(){},"html");				
};

adminPanel.views.candidatures = function(){
	//alert("candidatures");
	var data={
		title : "Gestion des dossiers",
		// id : costum.contextId,
		// collection : costum.contextType,
		// slug : costum.contextSlug,
		// url : baseUrl+'/survey/answer/admindirectory/slug/'+costum.contextSlug,
		table : {
			nameAnswer: {
				name : "Dossier"
			},
			comment : {
				name : "Commentaires"
			}
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "answers" ],
				fields : {}

			},
			filters : {
				text : true,
				statusAnswers : {
		 			view : "dropdownList",
		 			type : "filters",
		 			field : "priorisation",
		 			name : "Statuts",
		 			action : "filters",
		 			event : "selectList",
		 			list :  [ 
								"Dossier validé",
								"Dossier en maturation",
								"Dossier refusée",
								"Dossier en attente de traitement"
			                ]
		 		}
			}
		},
		csv : [
				{
					name : "Toutes les candidatures",
					defaults : {
						indexStep : 0
					},
					url : baseUrl+'/co2/export/csvelement/type/answers/slug/deal/'
				} 
			]
	};
	data = getFormMapping(data);
	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
		|| (typeof canEdit != "undefined" && canEdit) ){
		data.actions={
			statusAnswer : {
				list : [ 
					"Dossier validé",
					"Dossier en maturation",
					"Dossier refusée",
					"Dossier en attente de traitement"
                ]
            },
			privateanswer : true,
			statusAnswer : false,
			pdf : false,
			csv : false,
			deleteAnswer: true
		};
	}
			
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory', data, function(){},"html");
};

adminDirectory.values.nameAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.nameAnswer", id, type, aObj);
	//mylog.log(e);
	var str = "Réponse vide";
	if( typeof e.answers != "undefined" && 
		typeof e.answers.deal1 != "undefined" &&
		typeof e.answers.deal1.deal111 != "undefined" ){
		str = '<a href="#answers.index.id.'+id+'" class="" target="_blank">'+e.answers.deal1.deal111+'</a>';
	}
	mylog.log("adminDirectory.values actions return", str);
	return str;
};
// adminDirectory.values.email = function(e, id, type, aObj){
// 	mylog.log("adminDirectory.values.email", e, id, type);
// 	var str="Aucun email";
// 	if(typeof e.answers != "undefined" && typeof e.answers.deal1 != "undefined" && typeof e.answers.deal1.email != "undefined")
// 		str=e.answers.deal1.email;
// 	return str;
// };
// adminDirectory.values.name = function(e, id, type, aObj){
// 	mylog.log("adminDirectory.values.name", e, id, type);
// 	var str="Réponse vide";
// 	if(type == "answers" && typeof e.answers != "undefined"){
// 		if(typeof e.answers.deal1 != "undefined" && typeof e.answers.deal1.deal13x != "undefined")
// 			str=e.answers.deal1.deal13x;

// 		if(typeof e.answers.deal1 != "undefined" && typeof e.answers.deal1.deal13 != "undefined")
// 			str+=e.answers.deal1.deal13;

// 		return '<a href="#answer.index.id.'+id+'" class="" target="_blank">'+str+'</a>';
// 	}else if (type == "forms"){
// 		return '<a href="#form.edit.id.'+id+'" class="" target="_blank">'+e.name+'</a>';
// 	}

// 	return str; 
	
// };
adminDirectory.values.privateanswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values privateanswer", e, id, type, aObj);
	var str = "";
	if( typeof e.project != "undefined" && typeof e.project.id != "undefined"){
		if( typeof e.project.preferences != "undefined" && 
			typeof e.project.preferences.private != "undefined" && 
			e.project.preferences.private === true ){
			str = '<span id="private'+id+'" class="label label-danger"> Privé </span>';
		}else{
			str = '<span id="private'+id+'" class="label label-success"> Public </span>';
		}
	}
	return str;
};

adminDirectory.actions.privateanswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions privateanswer", e, id, type);
	var val = false ;
	var str = "&nbsp;";
	if( typeof e.project != "undefined" && typeof e.project.id != "undefined"){
		if( typeof e.project.preferences != "undefined" && 
			typeof e.project.preferences.private != "undefined" && 
			e.project.preferences.private === true ){
			val = true;
		}
		str ='<button data-parentid="'+id+'" data-parenttype="'+type+'" data-id="'+e.project.id+'" data-type="'+e.project.type+'" data-private="'+val+'" data-path="preferences.private" class="col-xs-12 privateAnswerBtn btn bg-green-k text-white">Rendre public</button>';
	}
	
	return str ;
};
adminDirectory.values.statusAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values statusAnswer", e, id, type, aObj);
	var col = "white";
	var icon = "";
	var str = "En attente de traitement" ;
	if( typeof e.priorisation != "undefined"){
		//col = states[e.priorisation].color;
		//icon = states[e.priorisation].icon;
		str = e.priorisation ;
	}
	mylog.log("adminDirectory.values statusAnswer end", str);
	return str;
};

adminDirectory.actions.statusAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions statusAnswer", e, id, type, aObj);

	var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
				'data-formid="'+e.formId+'" '+
				'data-userid="'+e.user+'" ' +
				'class="prioritize  col-xs-12 btn btn-default" >Changer le statut'+ 
			'</a>';
	return str;
};

// adminDirectory.actions.deleteAnswer = function(e, id, type, aObj){
// 	mylog.log("adminDirectory.actions.deleteAnswer", e, id, type, aObj);

// 	var str = '<a href="javascript:;" data-id="'+id+'"'+
// 				'class="deleteAnswer col-xs-12 btn btn-error" >Supprimer l\'action '+ 
// 			'</a>';
// 	return str;
// };
adminDirectory.bindCostum = function(aObj){
	mylog.log("adminDirectory.bindCostum ", aObj);


	// $("#"+aObj.container+" .deleteAnswer").off().on("click", function(){
	// 	id = $(this).data("id");
	// 	bootbox.dialog({
	// 	  title: "Confirmez la suppression de la candidature",
	// 	  message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
	// 	  buttons: [
	// 	    {
	// 	      label: "Ok",
	// 	      className: "btn btn-primary pull-left",
	// 	      callback: function() {
	// 	        getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
	// 	        	//urlCtrl.loadByHash(location.hash);
	// 	        	//pageProfil.views.home();
	// 	        	toastr.success("La candidature a bien été supprimée");
	// 	        	aObj.search(0);
	// 	        },"html");
	// 	      }
	// 	    },
	// 	    {
	// 	      label: "Annuler",
	// 	      className: "btn btn-default pull-left",
	// 	      callback: function() {}
	// 	    }
	// 	  ]
	// 	});
	// });
}
adminPanel.views.importZourit = function () {
	ajaxPost('#content-view-admin', baseUrl + '/costum/deal/importzourit/', {}, function () { }, "html");
};