	
	
	function afficheFiche(idFiche){
		//alert(idFiche);
		params = {
			"idFiche" : idFiche
		};
		var htmlContent= "";
		ajaxPost(
			null,
			baseUrl+"/costum/ficheprojet/getansweraction",
			params,
			function(data){
				mylog.log("successss", data);
				ficheProjet = (typeof data.projet2872020_1530_0 != "undefined") ? data.projet2872020_1530_0 : "" ;
				nomProjet = (typeof ficheProjet.projet2872020_1530_01 != "undefined" || ficheProjet.projet2872020_1530_01 != "null") ? ficheProjet.projet2872020_1530_01 : "";
				slogan = (typeof ficheProjet.projet2872020_1530_02 != "undefined")?ficheProjet.projet2872020_1530_02 : "";
				description = (typeof  ficheProjet.projet2872020_1530_04 != "undefined") ? ficheProjet.projet2872020_1530_04 : "";
				cibleCoeur = (typeof ficheProjet.projet2872020_1530_05 != "undefined") ? ficheProjet.projet2872020_1530_05 : "";
				cibleElargie = (typeof ficheProjet.projet2872020_1530_06 != "undefined") ? ficheProjet.projet2872020_1530_06 : "";
				commentaire = (typeof ficheProjet.projet2872020_1530_14 != "undefined") ? ficheProjet.projet2872020_1530_14 : "";


				//GRAPHISME
				graphisme = (typeof data.projet2972020_164_1 != "undefined") ? data.projet2972020_164_1 : "" ;

				// DEVELOPMENT
				dev = (typeof data.projet2972020_187_2 != "undefined") ? data.projet2972020_187_2 : "" ;
				objectif = (typeof dev.projet2972020_187_25 != "undefined") ? dev.projet2972020_187_25 : "" ;

				//ANIMATION
				animation = (typeof data.projet3072020_1652_3 != "undefined") ? data.projet3072020_1652_3 : "" ;
				publiCible = (typeof animation.projet3072020_1652_31 != "undefined") ? animation.projet3072020_1652_31 : "" ;
				dateAnimation = (typeof animation.projet3072020_1652_34 != "undefined") ? animation.projet3072020_1652_34 : "" ;

				htmlContent +=
					'<div class="container">'+
					'<div class="col-md-12 col-sm-12 col-xs-12 ">'+
					'<h1 class="letter-turq text-center"> Fiche projet : '+ nomProjet+'</h1>'+
					'<div class=" well ">'+
					'<p class="paragraph text-center" >'+description+' </p>'+
					'</div>'+
					'</div>'+
					'<div class="row">'+
					'<div class="col-md-4 col-sm-4 col-xs-4 padding-20" >'+

					'<div>'+
					'<u><p>Slogan: </p></u>'+slogan +
					'</div>'+
					'<hr>'+
					'<div >'+
					'<u><p>Cible coeur: </p></u>'+cibleCoeur +
					'</div>'+
					'<hr>'+
					'<div >'+
					'<u><p>Cible élargie:</p></u>'+cibleElargie +
					'</div>'+
					'<hr>'+

					'<div>'+
					'<p> Actions à mettre en oeuvre : </p>';
				if(typeof ficheProjet.projet2872020_1530_11 != "undefined"){
					$(ficheProjet.projet2872020_1530_11).each(function(key,value){
						mylog.log("action",value.liste_row);
						htmlContent+=
							'<li> '+
							value.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'<hr>'+
					'<div>'+
					'<p>Liste des tâches et besoins :</p>';
				if(typeof ficheProjet.projet2872020_1530_12 != "undefined" ){
					$(ficheProjet.projet2872020_1530_12).each(function(keyTa,valueTa){
						mylog.log("action",valueTa.liste_row);
						htmlContent+=
							'<li> '+
							valueTa.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'<div class="col-md-8 col-sm-8 col-xs-8 padding-20" >'+
					'<p> Référents </p>'+
					'<table>'+
					'<tr>'+
					'<th>Nom</th>'+
					'<th>Email</th>'+
					'<th>Type</th>'+
					'</tr>';
				if (typeof ficheProjet.projet2872020_1530_03 != "undefined" ) {
					$(ficheProjet.projet2872020_1530_03).each(function(keyR,valueR){
						htmlContent +=
							'<tr>'+
							'<td>'+  valueR.nom +'</td>'+
							'<td>'+  valueR.email +'</td>'+
							'<td>'+ valueR.type +'</td>'+
							'</tr>';
					})
				}
				htmlContent +=
					'</table>'+

					'<p > Budget global : </p>'+
					'<table>'+
					'<tr>'+
					'<th>Groupe</th>'+
					'<th>Nature de l\'action</th>'+
					'<th>Poste de dépense</th>'+
					'<th>Montant</th>'+
					'</tr>';
				if (typeof ficheProjet.projet2872020_1530_07 != "undefined" ) {
					$(ficheProjet.projet2872020_1530_07).each(function(keyB,valueB){
						htmlContent +=
							'<tr>'+
							'<td>'+  valueB.group +'</td>'+
							'<td>'+  valueB.nature +'</td>'+
							'<td>'+ valueB.poste +'</td>'+
							'<td>'+ valueB.price +'</td>'+
							'</tr>';
					})
				}
				htmlContent +=
					'</table>'+
					'</div>'+
					'</div>'+
					'<div class=" well ">'+
					'<p class="label-question">Commentaires de la personne ayant lancé le projet:</p>'+
					'<p class="paragraph">'+ commentaire +'</p>'+
					'</div>'+

					// Spécifique pour Graphisme

					'<h4 >Spécifique pour Graphisme</h4>'+
					'<p> Charte graphique:'+
					//getFile(idFiche , "projet2972020_164_1.projet2972020_164_11");
					//mylog.log ("charteee",charteGraphisme);
					'</p>'+

					'<div class="row">'+
					'<div class="col-md-4">'+
					'<div  class="card1"  >'+
					'<div class="col-md-11">'+
					'<p>Tonalité :</p>';
				if(typeof graphisme.projet2972020_164_12 != "undefined" ){
					$(graphisme.projet2972020_164_12).each(function(keyTo,valueTo){
						mylog.log("Tonalite",valueTo.liste_row);
						htmlContent+=
							'<li> '+
							valueTo.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+
					'<div class="col-md-8">'+
					'<div  class="card1"  >'+
					'<p>Valeurs à transmettre:</p>';
				if(typeof graphisme.projet2972020_164_13 != "undefined" ){
					$(graphisme.projet2972020_164_13).each(function(keyTrans,valueTrans){
						mylog.log("valeur à transmettre",valueTrans.liste_row);
						htmlContent+=
							'<li> '+
							valueTrans.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+

					'<p> Si des éléments existent déjà, qu’est-ce qui doit être gardé ?</p>'+

					'<div class="row">'+
					'<div class="col-md-6">'+
					'<div  class="card1"  >'+
					'<div class="col-md-11">'+
					'<p> Eléments récurrents :</p>';
				if(typeof graphisme.projet2972020_164_15 != "undefined" ){
					$(graphisme.projet2972020_164_15).each(function(keyRe,valueRe){
						mylog.log("Eléments récurrents",valueRe.liste_row);
						htmlContent+=
							'<li> '+
							valueRe.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+

					'<div class="col-md-6">'+
					'<div  class="card1"  >'+
					'<p> Eléments qui peuvent disparaître : </p>';
				if(typeof graphisme.projet2972020_164_16 != "undefined" ){
					$(graphisme.projet2972020_164_16).each(function(keyDisp,valueDisp){
						mylog.log("Eléments qui peuvent disparaître",valueDisp.liste_row);
						htmlContent+=
							'<li> '+
							valueDisp.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+


					'<div class="row">'+
					'<div class="col-md-6">'+
					'<div  class="card1"  >'+
					'<div class="col-md-11">'+
					'<p>Idée qu\'on peut voir en visuel:</p>';
				if(typeof graphisme.projet2972020_164_17 != "undefined" ){
					$(graphisme.projet2972020_164_17).each(function(keyIdee,valueIdee){
						mylog.log("Idée qu'on peut voir en visuel",valueIdee.liste_row);
						htmlContent+=
							'<li> '+
							valueIdee.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+
					'<div class="col-md-6">'
				'<div  class="card1"  >'
				'<p>Elément à fournir:</p>'
				if(typeof graphisme.projet2972020_164_18 != "undefined" ){
					$(graphisme.projet2972020_164_18).each(function(keyElm,valueElm){
						mylog.log("Idée qu'on peut voir en visuel",valueElm.liste_row);
						htmlContent+=
							'<li> '+
							valueElm.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+


					//Spécifique pour les développement
					'<h4> Spécifique pour les développement</h4>'+
					'<div class="row">'+
					'<div class="col-md-4">'+
					'<p> Logo : </p>'+
					// <?php //foreach ($logo as $key => $value) {
					// 	echo "<img src='".$value["docPath"]."'height=250>";
					// }?>

					'</div>'+
					'<div class="col-md-8">'+
					'<p>Ressources graphiques : '+
					// <?php //foreach ($ressourceGraphique as $keyG => $valueG) {
					// 	echo "<a src='".$valueG["docPath"]."'> ".$valueG["name"]."
					// 	</a>";
					// }?>

					'</p>'+
					'<p>Contenu :' +
					// <?php //foreach ($contenu as $keyC => $valueC) {
					// 	echo "<a src='".$valueC["docPath"]."'> ".$valueC["name"]."
					// 	</a>";
					// }?>

					'</p>'+
					'</div>'+
					'</div>'+
					'<div class="row">'+
					'<div class="col-md-6">'+
					'<div  class="card1"  >'+
					'<div class="col-md-11">'+
					'<p> Modules nécessaires :</p>';
				if(typeof dev.projet2972020_187_21 != "undefined" ){
					$(dev.projet2972020_187_21).each(function(keyM,valueM){
						mylog.log("Modules nécessaires",valueM.liste_row);
						htmlContent+=
							'<li> '+
							valueM.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>'+
					'</div>'+
					'<div class="col-md-6">'+
					'<div  class="card1"  >'+
					'<p>Objectif :'+ objectif +'</p>'+
					'</div>'+
					'</div>'+
					'</div>'+


					//Spécifique pour des animations
					'<h4>Spécifique pour des animations</h4>'+
					'<div class="row">'+
					'<div class="col-md-4 col-sm-4 col-xs-4 padding-20" >'+
					'<div>'+
					'<p>Date de l\'animation: '+ dateAnimation +'  </p>'+
					'</div>'+
					'<hr>'+
					'<div>'+
					'<p>Public cible: '+ publiCible +'</p> '+
					'</div>'+
					'<hr>'+
					'</div>'+
					'<div class="col-md-8 col-sm-8 col-xs-8 padding-20">'+
					'<p>Moyens matériels:</p>';
				if(typeof animation.projet3072020_1652_32 != "undefined" ){
					$(animation.projet3072020_1652_32).each(function(keyMm,valueMm){
						mylog.log("Modules nécessaires",valueMm.liste_row);
						htmlContent+=
							'<li> '+
							valueMm.liste_row
						'</li>';
					});
				}
				htmlContent +=
					'</div>'+
					'</div>';


				$("#ficheProjet").html(htmlContent);
			},
			null,
			"json",
			{async : false}
		);

		$('#liste').toggle('show');
	}
	function getFile(id, subKey){
		params = {
			"idFiche" : id,
			"subKey" : subKey
		};
		var htmlContent= "";
		ajaxPost(
			null,
			baseUrl+"/costum/ficheprojet/getfileaction",
			params,
			function(data){
				mylog.log("listefile", data);
			},
			null,
			"json",
			{async : false}
		);
	}