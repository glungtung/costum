/*var fontStyles = "<style>";
var fontOptions = "";
$.each(fontObj,function(k,v){
	v = v.split(" ").join("_");
	fontStyles += `
	    @font-face {
	        font-family: "${v}";
	        src: url("${costumAssetsPath}/font/blockcms/fontpack/${k}");
	  }
	`;
	fontOptions +=`<option style="font-family:'${v.split(' ').join('_')}'" value="${k}">${v}</option>`;
});
fontStyles += "</style>";

$(function(){
	$("head").append(fontStyles);
})*/

var fontStyles = "<style>";
var fontOptions = "";
if(fontObj != null)
$.each(fontObj,function(k,v){
	v = v.split(" ").join("_");
	var fontPath = assetPath+k;
	if (k.indexOf("/upload/") != -1){
		fontPath = k;
	}
	fontStyles += `
	    @font-face {
	        font-family: "${v}";
	        src: url("${fontPath}");
	  }
	`;
	fontOptions +=`<option style="font-family:'${v.split(' ').join('_')}'" value="${v.split(' ').join('_')}">${v}</option>`;
});
fontStyles += "</style>";

$(function(){
	$("head").append(fontStyles);
})