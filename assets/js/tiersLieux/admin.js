adminPanel.views.tiersLieux = function(){
	var data={
		title : "Gestion des Tiers-lieux",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations"]
            },
            filters : {
                text : true
            
            }
        },
		table : {
            name: {
                name : "Nom"
            },
            created: {
                name : "Créé le"
            },
            description : {
            	name : "Description",
            	class : "col-xs-2 text-center"
            },
            private : { 
            	name : "Affichage",
        		class : "col-xs-1 text-center"
        	}
        },
        actions : {
            private : true,
            delete : true
        }
    };
		
		
	
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.existingReference = function () {
    ajaxPost('#content-view-admin', baseUrl + '/costum/francetierslieux/existingreference', {}, function () { }, "html");
};