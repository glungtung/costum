if (typeof jQuery !== 'undefined') {
    (function ($) {
        $(function () {
            $(document).on('change', '[data-conditional-target]', function () {
                const self = $(this);
                const targets = $(self.attr('data-conditional-target'));
                if (targets.length) {
                    targets.each(function (__, __element) {
                        const target = $(__element);
                        switch (self.attr('type')) {
                            case 'checkbox':
                                const condition_satisfied = JSON.parse(target.attr('data-show-condition')) && self.is(':checked');
                                if (condition_satisfied) target.css('display', '');
                                else target.css('display', 'none');
                                break;
                            case 'radio':
                                if (target.attr('data-show-condition') === self.val()) target.css('display', '');
                                else target.css('display', 'none');
                                break;
                            default:
                                break;
                        }
                    });
                }
            });
        });
    })(jQuery);
}