const width = 800,
    height = width;
const homeRadius = 100;
const menuRadius = 70;
const homeMargin = 20;
const imageSize = 100;
const dxFromCircleMenu = 1000,
    dyFromCircleMenu = dxFromCircleMenu;

var menuData = {
    "menu-competence": {
        id: "menu-competence",
        href: "competences",
        menu: "Mes Competences",
        count: 4
    },
    "menu-contact": {
        id: "menu-contact",
        href: "contacts",
        menu: "Me Contacter",
        count: 1
    },
    "menu-parcours": {
        id: "menu-parcours",
        href: "parcours",
        menu: "Mon Parcours",
        count: 2
    },
    "menu-projet": {
        id: "menu-projet",
        href: "projets",
        menu: "Mes Projets",
        count: 3
    }
};


const root_svg = d3
    .select("svg#graph")
    // .attr("width", width)
    // .attr("height", height)
    .attr("preserveAspectRatio", "xMidYMid slice")
    .style("max-height", "calc(100vh - 63px)")
    .attr("viewBox", [0, 0, width, height]);
const svg = root_svg.append("g");
const zoom = d3
    .zoom()
    .filter((e) => false)
    .on("zoom", (e) => {
        svg.attr("transform", e.transform);
    });
root_svg.call(zoom);
const cx = width / 2;
const cy = height / 2;
const home_svg_g = svg
    .append("svg")
    .attr("id", "home-svg")
    .append("g")
    .attr("id", "home-group")

const home_group = home_svg_g.append("g")
    .style("transform-box", "fill-box")
    .style("transform-origin", "center")

const home = home_group.append("g");
const g = home_group.append("g").attr("id", "menu-group")
    // const home_circle = home
    //     .append("rect")
    //     .classed("home-circle", true)
    //     .attr("r", homeRadius)
    //     .attr("x", cx - homeRadius * Math.PI / 4)
    //     .attr("y", cy - homeRadius * Math.PI / 4)
    //     .attr("width", (d) => homeRadius * Math.cos(Math.PI / 4) * 2)
    //     .attr("height", (d) => homeRadius * Math.sin(Math.PI / 4) * 2)
    //     .attr("fill", "#455a64");
const home_svg = home
    .append("svg")
    .style("overflow", "visible")
    .attr("x", (d) => cx - homeRadius * Math.cos(Math.PI / 4))
    .attr("y", (d) => cy - homeRadius * Math.sin(Math.PI / 4))
    .attr("width", (d) => homeRadius * Math.cos(Math.PI / 4) * 2)
    .attr("height", (d) => homeRadius * Math.sin(Math.PI / 4) * 2);
const home_g = home_svg.append("g");

const home_foreign = home_g
    .append("foreignObject")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", homeRadius * Math.cos(Math.PI / 4) * 2)
    .attr("height", homeRadius * Math.sin(Math.PI / 4) * 2);
home_foreign.append("xhtml:div")
    .style("height", "100%")
    .style("width", "100%")
    .style("display", "flex")
    .style("align-items", "center")
    .style("justify-content", "center")
    .style("background-color", "#455a64")
    .append("xhtml:img")
    .attr("src", me)
    .style("display", "block")
    .style("height", "auto")
    .style("width", "70%")

const position = d3
    .scaleLinear()
    .domain([0, Object.values(menuData).length])
    .range([0, Math.PI * 2]);
const color = d3.scaleOrdinal(d3.schemePastel2);
const menu = g
    .selectAll("g")
    .data(Object.values(menuData))
    .join("g")
    .classed("menu-cv", true);

function updateCount(data) {
    g.data(Object.values(data))
    g.selectAll("span").text(d => d.count)
}
menu.each((d, i) => {
    d.cos = Math.cos(position(i));
    d.sin = Math.sin(position(i));
    d.cx = cx - (homeRadius + menuRadius + homeMargin) * d.cos;
    d.cy = cy - (homeRadius + menuRadius + homeMargin) * d.sin;
    d.dx =
        cx - (homeRadius + menuRadius + homeMargin + width) * d.cos - width / 2;
    d.dy =
        cy - (homeRadius + menuRadius + homeMargin + height) * d.sin - height / 2;
});
const menu_circle = menu
    .append("circle")
    .classed("menu-circle", true)
    .attr("cx", (d, i) => d.cx)
    .attr("cy", (d, i) => d.cy)
    .attr("r", menuRadius)
    .attr("fill", (d, i) => color(i));

const menu_f = menu
    .filter(d => d.count > 0)
    .append("foreignObject")
    .attr("x", (d, i) => d.cx + (menuRadius + 10) * Math.cos(Math.PI / 3))
    .attr("y", (d, i) => d.cy - (menuRadius + 10) * Math.sin(Math.PI / 3))
    .attr("width", 30)
    .attr("height", 30)

const menu_f_div = menu_f.append("xhtml:div")
    .style("height", "100%")
    .style("width", "100%")
    .style("border-radius", "4px")
    .style("background-color", "#356ED5")
    .style("display", "flex")
    .style("align-items", "center")
    .style("justify-content", "center")

menu_f_div.append("span")
    .style("color", "white")
    .style("font-size", "10px")
    .text(d => d.count)

const menu_texts = menu
    .append("text")
    .attr("text-anchor", "middle")
    .attr("x", (d, i) => d.cx)
    .attr("y", (d, i) => d.cy)
    .text((d) => d.menu);

menu.on("mouseover", function(e) {
    d3.select(this).style("transform", "scale(1.1)");
});
menu.on("mouseout", function(e) {
    d3.select(this).style("transform", "scale(1)");
});
menu.on("click", (e, d) => zoomTo(d));
$(() => {
    const hash = window.location.hash
    history.replaceState({
        menu: "Home",
        href: "#"
    }, "Accueil", "#")
    menu.each((d) => {
        const currHash = "#" + d.href;
        console.log(currHash, hash);
        if (currHash == hash) {
            setTimeout(() => {
                zoomTo(d, 0, false);
                history.replaceState(d, d.menu, "#" + d.href);
            }, 50);

        }
        $("#menuTopRight a[href$='" + currHash + "']").removeClass("lbh-menu-app");
        $("#menuTopRight a[href$='" + currHash + "']")
            .unbind()
            .on("click", (e) => {
                e.preventDefault();
                zoomTo(d);
            });
    });
});

$("#menuTopRight a[href$='#home']").removeClass("lbh-menu-app");
$("#menuTopRight a[href$='#home']")
    .unbind()
    .on("click", (e) => {
        e.preventDefault();
        reset();
    });

function parseTransform(a) {
    var b = {};
    for (var i in (a = a.match(/(\w+\((\-?\d+\.?\d*e?\-?\d*,?)+\))+/g))) {
        var c = a[i].match(/[\w\.\-]+/g);
        b[c.shift()] = c;
    }
    return b;
}
var menuZoom = false;
var homeZoom = d3.zoom().on("zoom", (e) => {
    home_svg_g.attr("transform", e.transform);
});
d3.select("svg#graph")
    .call(homeZoom);

function zoomTo(d, duration = 750, state = true) {
    homeZoom.filter((e) => false);
    const toZoom = d3.select("#" + d.id);
    const toZoomG = toZoom.select("svg").select("g");
    menuZoom = d3.zoom().on("zoom", (e) => {
        toZoomG.attr("transform", e.transform);
    });
    const transform = toZoomG.attr("transform");
    if (!transform) {
        d3.select("svg#graph")
            .call(menuZoom)
            .call(menuZoom.transform, d3.zoomIdentity.translate(0, 0).scale(1));
    } else {
        const t = parseTransform(transform);
        d3.select("svg#graph")
            .call(menuZoom)
            .call(
                menuZoom.transform,
                d3.zoomIdentity.translate(...t.translate).scale(...t.scale)
            );
    }
    const x0 = d.dx,
        y0 = d.dy,
        x1 = d.dx + width,
        y1 = d.dy + height;
    svg
        .transition()
        .duration(duration)
        .call(
            zoom.transform,
            d3.zoomIdentity
            .translate(width / 2, height / 2)
            .scale(Math.min(8, 1 / Math.max((x1 - x0) / width, (y1 - y0) / height)))
            .translate(-(x0 + x1) / 2, -(y0 + y1) / 2)
        );
    if (state) {
        history.pushState(d, d.menu, "#" + d.href);
    }
}

function reset() {
    if (menuZoom) {
        menuZoom.on("zoom", null);
    }
    homeZoom.filter((e) => true);

    const transform = home_group.attr("transform");
    if (!transform) {
        d3.select("svg#graph")
            .call(homeZoom)
            .call(homeZoom.transform, d3.zoomIdentity.translate(0, 0).scale(1));
    } else {
        const t = parseTransform(transform);
        d3.select("svg#graph")
            .call(homeZoom)
            .call(
                homeZoom.transform,
                d3.zoomIdentity.translate(...t.translate).scale(...t.scale)
            );
    }
    svg.transition().duration(750).call(zoom.transform, d3.zoomIdentity);
    history.pushState({
        menu: "Home",
        href: "#"
    }, "Accueil", "#");
}

const g_menu_content = svg
    .append("g")
    .attr("id", "menu-content")
    .selectAll("g")
    .data(Object.values(menuData))
    .enter()
    .append("g")
    .attr("id", (d) => d.id);
const svg_menu_content = g_menu_content
    .append("svg")
    .attr("x", (d) => d.dx)
    .attr("y", (d) => d.dy)
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .style("transform-box", "fill-box");