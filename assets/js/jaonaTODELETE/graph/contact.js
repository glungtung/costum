var dyfContact = {
    beforeBuild: {
        properties: {
            name: {
                label: "Information",
                placeholder: "admin@admin.com",
                order: 4,
                init: function() {
                    setTimeout(() => {
                        $('.imageuploader, .urlsarray, .descriptiontextarea, .categoryselect,.publiccheckboxSimple,.typeselect,.imageuploader,.recurrencycheckbox,.formLocalityformLocality,.emailtext,.tagstags,.parentfinder,.organizerfinder,.urltext,.infocustom').remove(),
                            $(".fa.fab").removeClass("fa")
                    }, 100);
                }
            },
            sectionBtn: {
                label: "Contact?",
                list: {
                    facebook: {
                        label: "Facebook",
                        labelFront: "Facebook",
                        key: "facebook",
                        icon: "facebook fab",
                    },
                    email: {
                        label: "Email",
                        labelFront: "Email",
                        key: "email",
                        icon: "envelope",
                    },
                    website: {
                        label: "Site Web",
                        labelFront: "Site Web",
                        key: "siteweb",
                        icon: "globe",
                    },
                    mobile: {
                        label: "Mobile",
                        labelFront: "Mobile",
                        key: "mobile",
                        icon: "mobile-alt",
                    },
                    fixe: {
                        label: "Phone",
                        labelFront: "Phone",
                        key: "fixe",
                        icon: "phone",
                    },
                    adresse: {
                        label: "Adresse",
                        labelFront: "Adresse",
                        key: "adresse",
                        icon: "map-marker-alt",
                    },
                }
            }
        },
    },
    onload: {
        actions: {
            setTitle: "Ajouter un contact",
            src: {
                infocustom: "Remplir le champ",
            },
            presetValue: {
                public: true,
            },
            hide: {
                categoryselect: 1,
                publiccheckboxSimple: 1,
                typeselect: 1,
                imageuploader: 1,
                recurrencycheckbox: 1,
                formLocalityformLocality: 1,
                emailtext: 1,
                tagstags: 1,
                parentfinder: 1,
                organizerfinder: 1,
                urltext: 1,
                infocustom: 1,
            },
        },
    },
};

dyfContact.afterSave = function(data) {
    dyFObj.commonAfterSave(data, function() {
        dyFObj.closeForm();
        refreshContact(width, height)
    });
};


var mapIcon = {
    facebook: "fab fa-facebook-f",
    fixe: "fa fa-phone",
    mobile: "fa fa-mobile-alt",
    adresse: "fa fa-map-marker-alt",
    website: "fa fa-globe",
    email: "fa fa-envelope"
}

function contact(w, h, rawData, isAdmin = true, groupNode = null) {
    var links = [];
    const data = rawData.slice()
    for (const {
            id
        }
        of data) {
        links.push({
            source: "root",
            target: id
        })
    }
    console.log(links)

    data.push({
        id: "root",
        type: "contact",
        name: "contact",
        icon: "fa fa-address-card"
    })

    const width = w,
        height = h;
    var svg = groupNode ? groupNode : d3.select("g#menu-contact").select("svg").attr("height", height)
        .attr("width", width)
        .attr("viewBox", [-width / 2, -height / 2, width, height])
        .select("g");
    if (svg.select("g.root-g").node()) {
        svg.select("g.root-g").remove();
    }
    svg = svg.append("g").classed("root-g", true);

    var color = d3.scaleOrdinal(d3.schemeCategory10)

    var simulation = d3.forceSimulation(data)
        .force("link", d3.forceLink(links).id(d => d.id).distance(250).strength(1))
        .force("charge", d3.forceManyBody().strength(-1000))
        .force("x", d3.forceX())
        .force("collide", d3.forceCollide().radius(100).iterations(2))
        .force("y", d3.forceY()).stop()

    const link = svg.append("g")
        .selectAll("line")
        .data(links)
        .join("line")
        .attr("stroke-width", 1.1)
        .attr("stroke-opacity", 0.3)
        .attr("stroke", "#999")

    const node = svg
        .selectAll("svg")
        .data(data)
        .join("svg")
        .style("overflow", "visible")

    const leaf = node.filter(d => d.type && d.type != "contact")
    const group = node.filter(d => !(d.type && d.type != "contact"))
    node.append("circle")
        .attr("r", 60)
        .classed("big", true)
        .attr("stroke", (d, i) => color(i))
        .attr("stroke-width", "2px")
        .attr("fill", "white");
    node.append("circle")
        .classed("small", true)
        .attr("r", 50)
        .attr("fill", (d, i) => color(i));

    group.select("circle.small")
        .attr("fill", _ => color(10));
    group.select("circle.big")
        .attr("stroke", _ => color(10));

    const div = node.append("foreignObject")
        .attr("x", -50)
        .attr("y", -50)
        .attr("width", 100)
        .attr("height", 100)
        .append("xhtml:div")
        .style("width", "100%")
        .style("height", "100%")
        .style("display", "flex")
        .style("justify-content", "center")
        .style("align-items", "center")

    const i = div
        .append("xhtml:i")
        .style("color", "white")
        .attr("class", d => "fa-3x " + d.icon)
    if (isAdmin) {
        group.on("mouseover", (e, d) => {
            d3.select(e.currentTarget).select("i").attr("class", "fa-3x fa fa-plus")
            group.on("click", (e) => {
                dyFObj.openForm("poi", null, null, null, dyfContact)
            })
        })
        group.on("mouseout", (e, d) => {
            d3.select(e.currentTarget).select("i").attr("class", d => "fa-3x " + d.icon)
            group.on("click", null)
        })
        leaf.on("mouseover", (e, d) => {
            const curr = d3.select(e.currentTarget).select("div")
            curr.select("i.fa-3x").style("display", "none")
            curr.select("i.fa-edit").style("display", "block")
            curr.select("i.fa-trash").style("display", "block")

        })
        leaf.on("mouseout", (e, d) => {
            const curr = d3.select(e.currentTarget).select("div")
            curr.select("i.fa-3x").style("display", "block")
            curr.select("i.fa-edit").style("display", "none")
            curr.select("i.fa-trash").style("display", "none")
        })
        const edit = div.filter(d => d.type && d.type != "contact")
            .append("xhtml:i")
            .style("display", "none")
            .style("color", "white")
            .style("padding", "3px")
            .attr("class", "fa-2x fa fa-edit")
            .on("click", (e, data) => {
                dyFObj.editElement("poi", data.id, data.type, dyfContact)
            })
        const trash = div.filter(d => d.type && d.type != "contact")
            .append("xhtml:i")
            .style("display", "none")
            .style("color", "white")
            .style("padding", "3px")
            .attr("class", "fa-2x fa fa-trash")
            .on("click", (e, data) => {
                console.log(data)
                bootbox.confirm("Voulez vous vraiment supprimer ce contact?", (res) => {
                    if (res) {
                        var url = baseUrl + "/" + moduleId + "/element/delete/id/" + data.id + "/type/poi";
                        ajaxPost(
                            null,
                            url,
                            null,
                            function(data) {
                                if (data.result) {
                                    toastr.success("Ce contact a été supprimé avec succes");
                                    refreshContact(width, height);
                                } else {
                                    toastr.error(data.msg);
                                }
                            }
                        );
                    }
                })
            })
    }
    node.append("title")
        .text(d => d.name);
    leaf.append("foreignObject")
        .style("overflow", "visible")
        .attr("x", -50)
        .attr("y", 70)
        .attr("width", 100)
        .attr("height", 100)
        .append("xhtml:div")
        .style("overflow", "visible")
        .style("width", "100%")
        .style("text-align", "center")
        .text(d => d.name)

    for (let i = 0; i < 100; i++) {
        simulation.tick();
    }
    link
        .attr("x1", d => d.source.x)
        .attr("y1", d => d.source.y)
        .attr("x2", d => d.target.x)
        .attr("y2", d => d.target.y);

    node
        .attr("x", d => d.x)
        .attr("y", d => d.y);
}