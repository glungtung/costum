var coSinDniObj = {
    sousOrganisationName : "CACs",
    filterCacs : {},
    init : function(coObj,formParent=null){
        coObj.sousOrganizationFilter(coObj);
    },
    getcacs : function(cobj){
        var cacs = {};
        ajaxPost(
            null,
            baseUrl + "/costum/aap/cacs/action/listcacs/slug/"+costum.contextSlug,
            {
                searchType: ["organizations"],
                count: true,
                countType : ["organizations"],
                indexStep: 0  ,
                notSourceKey: true
            },
            function(data){ 
                cacs = data.results
            },null,null,{async:false}
        );
        return cacs;
    },
    sousOrganizationFilter : function(cobj){
        var cacs = cobj.getcacs(cobj);
        $.each(cacs,function(k,v){ 
            cobj.filterCacs[v.aapForm] = v.name;
            trad[v.aapForm] = v.name
        })
    },
    paramsFilters : function(coObj,formParent=null,elementForm){
        var params = {
            year : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.year",
                name : trad.year,
                event : "filters",
                list : generateIntArray(2022,new Date().getFullYear()).map(v => v = v.toString())
            }
        }
        if(notNull(costum) && exists(elementForm["el"]) && exists(elementForm["el"]["slug"]) && costum.slug == elementForm["el"]["slug"]){
            params["sousOrganization"] = {
                view : "accordionList",
                type : "filters",
                field : "sousOrganisation",
                name : coObj.sousOrganisationName,
                event : "filters",
                keyValue : false,
                list : coObj.filterCacs
            }
        }
        return params;
    },
    defaultFilters: function(coObj,formParent,elementForm){
        var params = {
            notSourceKey : true,
            indexStep: "15",
            types:["answers"],
            forced:{
                filters:{
                }
            },
            filters:{
                'form' : formParent["_id"]["$id"],
                'answers.aapStep1.titre' : {'$exists':true},
                'status':{'$not':"finish"},
                'project.id' : {'$exists': "false"},
                //"answers.aapStep1.year" : new Date().getFullYear(),
            },
            sortBy:{"updated":-1},
            tagsPath:"answers.aapStep1.tags",
        }
        if(notNull(costum) && exists(elementForm["el"]) && exists(elementForm["el"]["slug"]) && costum.slug == elementForm["el"]["slug"]){
            params.filters["allSousOrganisation"] = Object.keys(coObj.filterCacs);
        }
        return params;
    },
}