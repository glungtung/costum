var coSindniSmarterreObj = {
    init : function(coObj,formParent){

    },
    interventionArea : {
        "Centre Ville":"Centre Ville", 
        "Marcadet (QPV)":"Marcadet (QPV)", 
        "Bas de la Rivière (QPV)" : "Bas de la Rivière (QPV)", 
        "Butor (QPV)":"Butor (QPV)", 
        "Sainte Clotilde (QPV)":"Sainte Clotilde (QPV)", 
        "Montagne 8ème":"Montagne 8ème", 
        "Montagne 15ème":"Montagne 15ème", 
        "Source / Bellepierre (QPV)":"Source / Bellepierre (QPV)", 
        "Brûlé ":"Brûlé ", 
        "Vauban (QPV)":"Vauban (QPV)", 
        "Camélias (QPV)":"Camélias (QPV)", 
        "Providence":"Providence", 
        "Saint François":"Saint François", 
        "Montgaillard":"Montgaillard", 
        "Chaudron (QPV)":"Chaudron (QPV)", 
        "Primat (QPV)":"Primat (QPV)", 
        "Moufia Les Hauts (QPV)":"Moufia Les Hauts (QPV)", 
        "Moufia Les Bas (QPV)":"Moufia Les Bas (QPV)", 
        "Bois de Nèfles":"Bois de Nèfles", 
        "Bretagne":"Bretagne", 
        "Domenjod (QPV)":"Domenjod (QPV)", 
        "La Chaumière (Quartier en Veille)":"La Chaumière (Quartier en Veille)"
    },
    communicationTools : {
        templates : {
            "template-1-coSindniSmarterre" : {label : "Avis favorable QPV"},
            "template-2-coSindniSmarterre" : {label : "Avis favorable hors QPV"},
            "duplicate-action-coSindniSmarterre" : {label :"Dupliquer le dossier",event : "coSindniSmarterreObj.communicationTools.events.duplicateActionCoSindniSmarterre()"}
        },
        events : {
            duplicateActionCoSindniSmarterre : function(){
                duplYear = new Date().getFullYear();
                bootbox.prompt({ 
                    size: "small",
                    title: "Demande de duplication en quelle année ?",
                    required : true,
                    value : duplYear,
                    callback: function(result){ 
                        communicationToolsObj.payload.duplicateyear = result;
                        communicationToolsObj.sendMailOrPreview(communicationToolsObj,true,communicationToolsObj.choosedTemplate);
                    }
                });
                return duplYear;
            }
        }
    },
    getFinancors : function(coObj,formParent){
        var financors = {};
        ajaxPost('',baseUrl+'/costum/aap/getfinancor/id/'+formParent._id.$id, 
        null,
        function(data){
            financors = data;
            var parse = {};
            $.each(financors,function(k,v){
                parse[k+"-idAndName-"+v] = v
            })
            financors = parse;
        },
        null,
        null,
        {async : false})
        return financors;
    },
    getFonds :function(coObj,formParent){
        var results = [];
        var params = {     
            searchType : ["lists"],
            fields : ["name"],
            filters :{
                form : formParent["_id"]["$id"],
                type:"fonds"
            },
            notSourceKey : true     
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                results = data.results;
                var allFonds = [];
                $.each(results,function(k,v){
                    allFonds.push(v.name);
                })
                results = allFonds;
            },null,null,{async:false}
        )
        return results;
    },
    getFonds2 :function(coObj,formParent){
        var results = [];
        var params = {     
            searchType : ["answers"],
            fields : ["answers.aapStep1.depense.financer.line"],
            filters :{
                "answers.aapStep1.depense.financer.line" : {'$exists':true},
                "answers.aapStep1.titre" : {'$exists':true},
                form : formParent["_id"]["$id"],
            },
            notSourceKey : true     
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                $.each(data.results,function(k,v){
                   $.each(v.answers.aapStep1.depense,function(kk,vv){
                        if(exists(vv.financer)){
                            $.each(vv.financer,function(kkk,vvv){
                                if(exists(vvv.line) && (vvv.line != "" || vvv.line != "" || vvv.line != null)){
                                    if(results.includes(vvv.line) === false){
                                        results.push(vvv.line);
                                    }
                                }
                            })
                        }
                    })
                })
            },null,null,{async:false}
        )
        mylog.log("datako",results)
        results = [...new Set(results)];
        results = results.filter(function (el) {
            return el != null && el != '';
        })
        return results;
    },
    getAssociation : function(coObj){
        var association = [];
        var params = {     
            searchType : ["answers"],
            fields : ["answers.aapStep1.association"],
            filters :{
                "answers.aapStep1.association" : {'$exists':true},
                "answers.aapStep1.titre" : {'$exists':true},
                form : aapObject.formParent._id.$id,
            },
            notSourceKey : true     
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                let allAnswers = data.results;
                $.each(allAnswers,function(k,v){
                    if(notNull(v.answers) && notNull(v.answers.aapStep1) && notNull(v.answers.aapStep1.association) && !association.includes(v.answers.aapStep1.association))
                        association.push(v.answers.aapStep1.association);
                })
            },null,null,{async:false}
        )
        return association;
    },
    paramsFilters : function(coObj,formParent){
        var params = {
            admissibility : {
                view : "accordionList",
                type : "filters",
                field : "admissibility",
                name : "Admissible et Inadmissible",
                event : "filters",
                keyValue : false,
                list : {
                    "admissible" : "Admissible",
                    "inadmissible" : "Inadmissible",
                }
            },
            association : {
                view : "text",
                field : "answers.aapStep1.association",
                event : "text",
                placeholder : "Nom de l'association"
            },
            associations : {
                view : "megaMenuAccordion",
                type : "filters",
                field : "answers.aapStep1.association",
                remove0: true,
                countResults: true,
                activateCounter:true,
                countFieldPath: "answers.aapStep1.association",
                remove0: true,
                name : "Trier par Association",
                event : "filters",
                list : coObj.getAssociation(coObj),
                classDom : "full-width",
                classList : "col-xs-4",
            },
            principalDomaine : {
                view : "megaMenuAccordion",
                type : "filters",
                field : "answers.aapStep1.principalDomaine",
                remove0: true,
                countResults: true,
                activateCounter:true,
                countFieldPath: "answers.aapStep1.principalDomaine",
                remove0: false,
                name : "Pilier",
                event : "filters",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["principalDomaine"]) && exists(formParent["params"]["principalDomaine"]["list"]) && notNull(formParent["params"]["principalDomaine"]["list"]) ? formParent["params"]["principalDomaine"]["list"] : [] 
            },
            interventionArea : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.interventionArea",
                name : "Quartier(s) d'intervention",
                event : "filters",
                keyValue : false,
                list : coObj.interventionArea,
            },
            financors : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.depense.financer.idAndName",
                name : "Financeurs",
                event : "filters",
                keyValue : false,
                list : coObj.getFinancors(coObj,formParent),
            },
            fond : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.depense.financer.line",
                name : "Fonds",
                event : "filters",
                list : coObj.getFonds2(coObj,formParent),
            },
            typologie : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep2.typologie",
                name : "Typologie du projet",
                event : "filters",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["checkboxNewtypologie"]) && exists(formParent["params"]["checkboxNewtypologie"]["list"]) && notNull(formParent["params"]["checkboxNewtypologie"]["list"]) ? formParent["params"]["checkboxNewtypologie"]["list"] : [] 
            },
            year : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.year",
                name : trad.year,
                event : "filters",
                list : generateIntArray(2022,new Date().getFullYear()).map(v => v = v.toString())
            },
            session : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep2.aapStep2lg3a7leik6jhs2chajq",
                name : "Session",
                event : "filters",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]) && exists(formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]["list"]) && notNull(formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]["list"]) ? formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]["list"] : [] 
            },
            qpv : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep2.qpv",
                name : "QPV ou Hors QPV",
                event : "filters",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["radioNewqpv"]) && exists(formParent["params"]["radioNewqpv"]["list"]) && notNull(formParent["params"]["radioNewqpv"]["list"]) ? formParent["params"]["radioNewqpv"]["list"] : []
            }
            /*missingData : {
                view : "dropdownList",
                type : "filters",
                field : "answers.aapStep1",
                name : "Données manquantes",
                typeList : "object",
                action : "filters",
                event : "exists",
                keyValue : false,
                list :  {
                    "titre":{
                        "label" : "Pas de titre",
                        "field" : "answers.aapStep1.titre",
                        "value" : false
                    },
                    email : {
						"label" : "Pas d\"email",
						"field" : "answers.aapStep1.email",
						"value" : false
					}
                }
            }*/
            
        }
        return params;
    },
    defaultFilters: function(coObj,formParent){
        var params = {
            notSourceKey : true,
            indexStep: "15",
            types:["answers"],
            forced:{
                filters:{
                }
            },
            filters:{
                'form' : formParent["_id"]["$id"],
                'answers.aapStep1.titre' : {'$exists':true},
                'status':{'$not':"finish"},
                "answers.aapStep1.year" : new Date().getFullYear(),
                'project.id' : {'$exists': "false"}
            },
            sortBy:{"updated":-1},
            tagsPath:"answers.aapStep1.tags",
        }
        return params;
    },
    events : function(coObj,formParent){

    }
}