$("#mainNav .btn-show-map").html("Cacher la carte <i class='fa fa-angle-double-right'></i>");
$("#mainNav .btn-show-map").data("onmap", true);
$("#main-search-bar").attr("placeholder","Quel tiers-lieu recherchez-vous ?");
//$("#panel_map").css('display','block');
$(".menuRight_header_title").text("Résultat");
$("#btn-filters").parent().css('display','none');
$("#btn-panel").html('<i class="fa fa-tags"></i><span>Filtre par mot-clé</span>');
//$("#result").parent().attr("class","col-xs-4");
$("#btn-panel").parent().attr("class","col-xs-6 pull-right");
//$("#btn-panel").parent().css("overflow","hidden")
$("#input_name_filter").attr("placeholder","Filtre par nom ...");



costum.templateCostumDesTiersLieux={
	"organizations" : {
		formData : function(data){
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						console.log("vvvvvvvvv", v);
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.mainTag != "undefined"){
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			return data;
		}
	}
};
	
var filterObj = {
	container : "#filterContainer" ,
	urlData : baseUrl+"/" + moduleId + "/search/globalautocomplete",
	init : function(pInit = null){
		mylog.log("fObj init",pInit);
		//Init variable
		var copyFilters = jQuery.extend(true, {}, filterObj);
		copyFilters.initVar(pInit);
		return copyFilters;

	},
	initVar : function(pInit){
		mylog.log("fObj initVar",pInit);
		this.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : "#filterContainer" );
		this.urlData =  ( (pInit != null && typeof pInit.urlData != "undefined") ? pInit.urlData : "#filterContainer" );
		this.initDefaults(pInit);
		this.initViews(pInit);
		this.initActions(pInit);
	},
	initDefaults : function(pInit){
		mylog.log("fObj initDefaults",pInit);
		var str = "";
		var fObj = this;
		if(typeof pInit.defaults != "undefined"){
			mylog.log("fObj initDefaults defaults");
			if(typeof pInit.defaults.fields != "undefined"){
				$.each(pInit.defaults.fields,function(k,v){
					mylog.log("fObj initDefaults fields",k, v);
					if(typeof searchObject.filters == "undefined" )
						searchObject.filters = {};
					if(typeof searchObject.filters[k] == "undefined" )
						searchObject.filters[k] = [];
					searchObject.filters[k].push(v);
					
				});
			}
		}
	},
	initActions : function(pInit){
		mylog.log("fObj initActions",pInit);
		var str = "";
		var fObj = this;
		if(typeof pInit.filters != "undefined"){
			$.each(pInit.filters,function(k,v){
				mylog.log("fObj initActions each", k,v);
				if(typeof v.action != "undefined" && typeof fObj.actions[v.action] != "undefined"){
					fObj.actions[v.action](fObj);
				}
			});
		}
	},
	initViews : function(pInit){
		mylog.log("fObj initViews",pInit);
		var str = '';
		var fObj = this;
		if(typeof pInit.filters != "undefined"){
			$.each(pInit.filters,function(k,v){
				mylog.log("fObj initViews each", k,v);
				if(typeof v.view != "undefined" && typeof fObj.views[v.view] != "undefined"){
					str += fObj.views[v.view](k,v);
				}
			});
		}
		str+='<div id="activeFilters" class="col-xs-12"></div>';
		$(fObj.container).html(str);
		fObj.initFilters();

	},
	initFilters : function(){
		fObj=this;
		getParamsUrls=location.hash.split("?");
		if(typeof getParamsUrls[1] != "undefined" ){
			var parts = getParamsUrls[1].split("&");
	        var $_GET = {};
	         var initScopesResearch={"key":"open","ids":[]};
	        for (var i = 0; i < parts.length; i++) {
	            var temp = parts[i].split("=");
	            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
	        }
	        if(Object.keys($_GET).length > 0){
	            $.each($_GET, function(e,v){
	                v=decodeURI(v);
	                if(e=="tags"){
	                	tags=v.split(",");
	                	$.each(tags, function(i, tag){
	                		fObj.manage.addActive(fObj,"tags",tag);
	                	});
	                }
	                if(e=="cities" || e=="zones"){
	                	if($.inArray(e,["cities","zones","cp"]) > -1) $.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
	                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
	                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
	                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
			                    	$.each(myScopes.open, function(i, scope){
			                    		if(typeof scope.active != "undefined" && scope.active)
			                				fObj.manage.addActive(fObj, "scope",scope.name,i);
			                		});
			                	}
	                    	});
	                    console.log(myScopes, "checkherescopes" );
	                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
	                    	$.each(myScopes.open, function(i, scope){
	                    		if(typeof scope.active != "undefined" && scope.active)
	                				fObj.manage.addActive(fObj, "scope",scope.name,i);
	                		});
	                	}
	                }    
	         	});
	        }
	    }
	},
	search : function(){
		searchObject.nbPage=0;
		searchObject.count=true;
		startSearch(0/*, null, function(){ if($(".searchEntityContainer").length == 0) $(".projectsProgress").show(); }*/);
	},
	views : {
		tags : function(k,v){
			mylog.log("fObj views tags", k,v);
			var str = "";
			return str;
		},
		select : function(k,v){
			mylog.log("fObj views select", k,v);
			var str = "<select>";
			str += '<option value="-1" >ALL</option>';
			if(typeof v.list != "undefined"){
				$.each( v.list ,function(kL,vL){
					str +='<option value="'+vL+'" >'+vL+'</option>';
				});
			}
			str += "</select>";
			return str;
		},
		selectList : function(k,v){
			mylog.log("fObj views selectList", k,v);
			label=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
			str='<li class="dropdown">'+
					'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
						label+' <i class="fa fa-angle-down margin-left-5"></i>'+
					'</a>'+
					'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
						'<div class="list-filters">';
						if(typeof v.list != "undefined"){
							includeSubList=(typeof v.list=="object" && !Array.isArray(v.list)) ? true : false;
							$.each( v.list ,function(kL,vL){
							if(includeSubList){
									str +='<button class="mainTitle col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+kL+'" >'+kL+'</button>';
									if(Object.keys(vL).length > 0){
										$.each(vL, function(i, sub){
											str +='<button class="col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+sub+'" >'+sub+'</button>';
										});
									}
								}else{
									str +='<button class="col-xs-12" data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+vL+'" >'+vL+'</button>';
								}
							});
						}
					str+='</div></div>'+
				'</li>';
			return str;
		},
		scope : function(){
			mylog.log("fObj views scope");
			return 	'<div id="costum-scope-search"><div id="input-sec-search">'+
							'<div class="input-group shadow-input-header">'+
								'<span class="input-group-addon scope-search-trigger">'+
									'<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'+
								'</span>'+
								'<input type="text" class="form-control input-global-search" autocomplete="off"'+
									' id="searchOnCity" placeholder="Où ?">'+
							'</div>'+
							'<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" '+
								' style="display: none;">'+
								'<div class="content-result">'+
								'</div>'+
							'</div>'+
					'</div></div>';
		}
	},
	actions : {
		tags : function(fObj){
			mylog.log("fObj actions tags");
			$("button[data-type='tags']").off().on("click",function(){
				searchObject.tags.push($(this).data("value"));
				fObj.search();
				fObj.manage.addActive(fObj,"tags",$(this).data("value"), $(this).data("key"), $(this).data("field"));
			});
		},
		filters : function(fObj){
			mylog.log("fObj actions filters");
			$("button[data-type='filters']").off().on("click",function(){
				mylog.log("fObj actions filters button[data-type='filters']");
				if(typeof $(this).data("field") != "undefined"){
					
					if(typeof searchObject.filters == "undefined" )
						searchObject.filters = {};
					
					if(typeof searchObject.filters[$(this).data("field")] == "undefined" )
						searchObject.filters[$(this).data("field")] = [];
                    searchObject.filters[$(this).data("field")].push($(this).data("value"));
                    //searchObject.searchType[] = "organizations";
					fObj.search();
					fObj.manage.addActive(fObj, "filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
				}
				
			});
		},
		scope : function(fObj){
			mylog.log("fObj actions scope");
			myScopes.open={};
			bindSearchCity("#costum-scope-search", function(){
				$(".item-globalscope-checker").off().on('click', function(){ 
					mylog.log("fObj actions scope .item-globalscope-checker");
					$("#costum-scope-search .input-global-search").val("");
					$(".dropdown-result-global-search").hide(700).html("");
					myScopes.type="open";
					mylog.log("fObj actions scope globalscope-checker",  $(this).data("scope-name"), $(this).data("scope-type"));
					newScope=scopeObject(myScopes.search[$(this).data("scope-value")]);
					$.each(newScope, function(e, v){
						if(typeof v.active == "undefined" || !v.active)
							delete newScope[e];
						else
							newKeyScope=e;
					});
					myScopes.open[newKeyScope]=newScope[newKeyScope];
					localStorage.setItem("myScopes",JSON.stringify(myScopes));
					fObj.manage.addActive(fObj, "scope",$(this).data("scope-name"),newKeyScope);
					fObj.search();
				});
			});
		}
	},
	manage:{
		addActive: function(fObj, type, value, key, field){
			mylog.log("fObj manage addActive", type, value, key);
			var dataKey = (typeof key != "undefined") ? 'data-key="'+key+'" ' : "" ;
			var dataField = (typeof field != "undefined") ? 'data-field="'+field+'" ' : "" ;
			str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
						dataKey +
						dataField +
						'data-value="'+value+'" '+
						'data-name="'+name+'" '+
						'data-type="'+type+'">' + 
					"<i class='fa fa-times-circle'></i>"+
					"<span "+
						"class='activeFilters' "+
						dataKey +
						dataField +
						'data-value="'+value+'" '+
						'data-scope-name="'+name+'" '+
						'data-type="'+type+'">' + 
						value + 
					"</span>"+
				"</div>";
			$(fObj.container+" #activeFilters").append(str);
			coInterface.initHtmlPosition();
			$(fObj.container+" .filters-activate").off().on("click",function(){
				fObj.manage.removeActive(fObj, $(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"));
				$(this).fadeOut(200);
				coInterface.initHtmlPosition();
			});
		},
		removeActive : function(fObj, type, value, key, field){
			mylog.log("fObj manage removeActive", type, value, key, field);
			if(type=="filters"){
				mylog.log("fObj manage removeActive filters", searchObject);
				if( typeof searchObject.filters != "undefined" && typeof searchObject.filters[field] != "undefined")
					searchObject.filters[field].splice(searchObject.filters[field].indexOf(value),1);
			}
			else if(type=="tags")
				searchObject.tags.splice(searchObject.tags.indexOf(value),1);
			else if(type=="scope"){
				delete myScopes.open[key];
			}
			fObj.search();
		}
	}
};
jQuery(document).ready(function() {
    // $('.menu-btn-top').attr('href','#search');
   // alert("initCostum");
    $("#mainNav .btn-show-map").html("Afficher la carte <i class='fa fa-angle-double-right'></i>");
 	$(".btn-show-map").off().on("click",function(){
	    if($(this).data("onmap")){
	    	showMap(false);
	    	$(this).data("onmap", false);
	    	$(this).html("Afficher la carte <i class='fa fa-angle-double-right'></i>");
	    }else{
	    	splithash=location.hash.split("?");
	    	if(splithash[0] != "" && splithash[0] != "#" && splithash[0] != "#search" )
	    		urlCtrl.loadByHash("");
	    	else
		    	showMap(true);
	    	$(this).data("onmap", true);
	    	$(this).html("Cacher la carte <i class='fa fa-angle-double-left'></i>");	    	
	    }
	});  
      
 });
