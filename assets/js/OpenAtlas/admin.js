

adminPanel.views.pledge = function(){

      var data={
        title : "Promesses de don",
        //types : ["crowdfunding"],
        table : {
          name: {
            name : "Nom",
            preview : true
          },
          amount : {
            name : "Montant",
            sum : true
          },
          type : {
            name : "type",
            preview : true
          }
          // validateField :{
          //   field : "type",
          //   value : "donation"
          // }

        },  
        actions:{
           delete : true,
           validatePledge : true
        },
        paramsFilter : {
            container : "#filterContainer",
            options : {
          tags : {
            verb : '$all'
          }
        },
            defaults : {
              types : [ "crowdfunding" ],
              //type : "pledge"
              filters : {
                type : "pledge"
              }
            },
            filters : {
          publicData : {
            view : "dropdownList",
            type : "filters",
            name : "Données publiques",
            //event : "selectList",
            field : "publicDonationData",
            action : "filters",
                  event : "filters",
            keyValue : false,
            list : {
              "true" : "Données publiques",
              "false" : "Données privées"
            }
          },
          invoice : {
            view : "dropdownList",
            type : "filters",
            name : "Demande de facture",
            //event : "selectList",
            field : "invoice",
            action : "filters",
                  event : "filters",
            keyValue : false,
            list : {
              "true" : "Facture demandée",
              "false" : "Facture non demandée"
            }

            
          }
        }
          },
          csv : [
              {
                  url : baseUrl+'/co2/export/csv/',
                  defaults : {
                          indexStep : 0,
                          fields : [
                              "name","behalf","type","civility","surname","donatorName","email","amount","invoice","invoiceName","invoiceAddress","invoiceSiret","publicDonationData"
                          ]
                  }
              }
          ]
          
      };
      ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
    };
adminPanel.views.donation = function(){

      var data={
        title : "Dons validés",
        //types : ["crowdfunding"],
        table : {
          name: {
            name : "Nom",
            preview : true
          },
          amount : {
            name : "Montant",
            sum : true
          },
          type : {
            name : "type",
            preview : true
          }
        },  
        actions:{
           delete : true
        },
        options : {
        tags : {
          verb : '$all'
        }
      },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
              types : [ "crowdfunding" ],
              //type : "pledge"
              filters : {
                type : "donation"
              }
            },
            filters : {
          publicData : {
            view : "dropdownList",
            type : "filters",
            name : "Données publiques",
            field : "publicDonationData",
            action : "filters",
            event : "filters",
            keyValue : false,
            list : {
              "true" : "Données publiques",
              "false" : "Données privées"
            }
          },
          invoice : {
            view : "dropdownList",
            type : "filters",
            name : "Demande de facture",
            //event : "selectList",
            field : "invoice",
            action : "filters",
                  event : "filters",
            keyValue : false,
            list : {
              "true" : "Facture demandée",
              "false" : "Facture non demandée"
            }

            
          }
        }
        },
        csv : [
              {
                  url : baseUrl+'/co2/export/csv/',
                  defaults : {
                          indexStep : 0,
                          fields : [
                              "name","civility","surname","donatorName","email","amount","invoice","behalf","invoiceName","invoiceAddress","invoiceSiret","publicDonationData"
                          ]
                  }
              }
          ]
          
      };
      ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

