jQuery(document).ready(function() {
	showAvancement();
});
function showAvancement () {
	$(".status").mouseover(function(){
		$(this).children(".textStatus").css("visibility","visible");
	});
	$(".status").mouseleave(function(){
		$(this).children(".textStatus").css("visibility","hidden");
	});
}

function addAfterPledgeModal() {
	let objs = $("#after-pledge-modal");
	if (objs.length > 0)
		return;
	// already exists?
	// html string
	let html_string = `
<div id="after-pledge-modal"
		class="modal fade in"
		tabindex="-1" role="dialog" aria-hidden="true" style="z-index:100000">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="pt-5 mb-0 text-secondary text-confirm text-center">Votre promesse de don a bien été enregistrée</h3>
		</div>
		<div class="modal-body text-left">
			<p>Nous avons bien reçu votre promesse de don pour un montant de <span style="color:#F08336;font-weight:700" class="pledge-amount"></span> euros 
			  pour la campagne <span style="color:#F08336;font-weight:700" class="campaign-name"></span> portant sur <span class="project-name" style="color:#F08336;font-weight:700;"></span>.</p>
			<!-- <p><strong>Notez cette information</strong></p> -->
			<p>Nous vous l'avons également envoyée par email, mais nous constatons que les emails que nous envoyons 
			  arrivent parfois dans les courriers indésirables ("spams") des destinataires. Voilà pourquoi il est 
			  préférable que vous notiez dès maintenant ces informations.</p>
			<p>Vous pouvez envoyer votre don de deux manières :</p>
			<dl style="font-size: 17px;">
				<dt style="color:#05323E;">Par paiement sur la plateforme</dt>
				<dd>Lien vers la plateforme de don choisie : <a style="color:#F08336;" class="campaign-href-url"  target="_blank" href=""><span class="campaign-ext-url"></span></a></dd>
				<dt style="color:#05323E;">Par virement bancaire</dt>
				<dd>IBAN : <span style="color:#F08336;" class="camp-iban"></dd>
			</dl>
            <p><i class="fa fa-warning"></i> Veillez à bien préciser la référence <span class="pledge-reference" style=color:#F08336;></span> quelque soit votre mode de paiement (ordre de virement ou commentaire HelloAsso).</>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary close-btn" data-bs-dismiss="modal">Fermer</button>
		</div>
	</div>
</div>
<script>
	$("#after-pledge-modal .close-btn").click(function() { $("#after-pledge-modal").modal("hide"); });
</script>
	`;

	// insertion as a child of the body
	$("body").append(html_string);
	$("#after-pledge-modal").hide();
}

costum.linkWithFields=function(name){
    $("#ajaxFormModal #orgaName").val(name);

};

// function linkElementField(type,id,name,slug,fieldId){
//     var dom="#ajaxFormModal";
//     var field=fieldId;
//     var params ={
//             inputType : "finder",
//             label : "Organisation liée à la promesse",
//             initMe : false,
//             rules : {
//                 required : true,
//                 lengthMin : [ 
//                     1, 
//                     "parent"
//                 ]
//             }

//     };
//     params["id"]=fieldId;
//     params["initType"]=[type];
//     params["inputType"]="finder";
//     //var fieldObj=dyFInputs.links(params);
//     var formValues = {};
//     formValues[field]={};
//     formValues[field][id] = {};
//     formValues[field][id]["name"] = name;
//     formValues[field][id]["type"] = type;
//     mylog.log("formValues",formValues); 

         


//   dyFObj.buildInputField(dom, field, params,formValues,null);
//   dyFObj.initFieldOnload[field+"Finder"]();

// }

costum.donationLink={};
costum.donationLink.searchExist = function(type,id,name,slug){
    finder.addInForm("donationLink",id,type,name);
    //linkElementField(type,id,name,slug,"donationLink");
    costum.linkWithFields(name);
    $("#ajaxFormModal #similarLink").hide();
    //$("#ajaxFormModal .donationLinkfinder").hide();
};

costum.checkboxSimpleEvent = {
    true : function(id){
        if(id=="behalf"){
            $("#ajaxFormModal .orgaNametext").show();
            $("#ajaxFormModal #orgaName").prop('required',true); 

        }
        else if(id=="invoice" && $("#ajaxFormModal #behalf").val()=="true"){
            $("#ajaxFormModal #invoiceName").val($("#ajaxFormModal #orgaName").val());
            $("#ajaxFormModal .invoiceNametext").show();
            $("#ajaxFormModal #invoiceName").prop('required',true); 
            $("#ajaxFormModal .invoiceAddresstextarea").show();
            $("#ajaxFormModal #invoiceAddress").prop('required',true); 
            $("#ajaxFormModal .invoiceSirettext").show();
            $("#ajaxFormModal #invoiceSiret").prop('required',true); 
        }    
    },
    false : function(id){
        if(id=="behalf"){
            $("#ajaxFormModal .orgaNametext").hide();
            $("#ajaxFormModal #orgaName").prop('required',false);
        }
        else if(id=="invoice"){
            $("#ajaxFormModal .invoiceNametext").hide();
            $("#ajaxFormModal #invoiceName").prop('required',false); 
            $("#ajaxFormModal .invoiceAddresstextarea").hide();
            $("#ajaxFormModal #invoiceAddress").prop('required',false); 
            $("#ajaxFormModal .invoiceSirettext").hide();
            $("#ajaxFormModal #invoiceSiret").prop('required',false); 
        }
    }
};

costum[costum.slug] ={
    init : function(){
        addAfterPledgeModal();
    },
	donation : {
        setUnloggedMode : function(){
            
            dyFObj.unloggedMode=true;
           // alert(dyFObj.unloggedMode);
                
        },
        afterBuild : function(data){
            //alert("kjk");
            mylog.log("afterbuild data",data);

            $('#amount').attr('readonly', true);
            $("#ajax-modal #associate").off().on("change",function(){
                //alert($(this).val());
                var chosenAssociate=$(this).val();

                // function getKeyByValue(object, value) {
                //   return Object.keys(object).find(key => object[key] === value);
                // }

                // var associateType="";

                // $.each(Object.values(costum.lists.associate),function(index,val){
                //     if(typeof getKeyByValue(val,chosenAssociate)=="string"){
                //         associateType= getKeyByValue(val,chosenAssociate);
                //     }
                // })
                // alert(associateType);
                var amount = costum.mapping[chosenAssociate];
                
                $("#ajax-modal #amount").val(amount);
            });

            var params = {
                source : costum.contextSlug,
                element : "crowdfunding",
                id : contextData.id
            };

            var projectName = "";
            var campaignId = "";
            var campaignName = "";
            var projectId = "";
            var pledgeNumber = 0;
            // mylog.log("commonName,campaignId,campaignName",commonName,campaignId,campaignName);
            

            ajaxPost(
                null,
                baseUrl+"/costum/filiere/get-related-elements",
                params,
                function(data){
                    var data = data.elt;

                    var parent={};
                    var receiver = {};
                    if(Object.keys(data).length==1){
                        // parent = data.parent;
                        // receiver[data._id.$id]= {
                        //     type : data.collection,
                        //     name : data.name
                        // } 
                        data=data[Object.keys(data)[0]];
                        mylog.log("callback getelem",data);

                        projectId=Object.keys(data.parent)[0];

                        projectName =data.parent[projectId].name;

                        campaignId = data._id.$id;
                        campaignName = data.name;

                        

                        pledgeNumber=data.pledgeIteration + 1;
                        mylog.log("pledgeNumber",pledgeNumber);
                        var reference = projectName+"_"+campaignName+"_"+pledgeNumber.toString();
                        reference =slugify(reference);
                        //alert(reference);
                        setTimeout(function(){
                            $("#ajaxFormModal #name").val(reference);
                            finder.addInForm("receiver",projectId,data.parent[projectId].type,projectName);
                            finder.addInForm("parent",campaignId,data.collection,campaignName);
                        },5000);
                    }

                    
                },
                null,
                null,
                {async : false}
            );    

            




            $( "#ajax-modal .input-group").after("<span class='col-xs-12' style='text-align: justify;'><i class='fa fa-info'></i> Il est nécessaire de créer un compte pour authentifier la déclaration. Vous ne recevrez pas de mails de notre part.</span>");
            //alert("afterbuild");
            // var commonName = data.receiver[Object.keys(data.receiver)[0]]["name"];
            // var campaignId = Object.keys(data.parent)[0];
            // var campaignName = data.parent[Object.keys(data.parent)[0]]["name"];
            // var pledgeNumber = 0;
            // mylog.log("commonName,campaignId,campaignName",commonName,campaignId,campaignName);
            // ajaxPost(
            //      null,
            //      baseUrl+"/" + moduleId + "/crowdfunding/getpledgesfromcampaignid/id/"+campaignId+"/type/pledge",
            //      null,
            //      function(data){
            //         pledgeNumber=data.pledgeIteration + 1;
            //         mylog.log("pledgeNumber",pledgeNumber);
            //         var reference = commonName+"_"+campaignName+"_"+pledgeNumber.toString();
            //         reference =slugify(reference);
            //         //alert(reference);
            //         setTimeout(function(){
            //             $("#ajaxFormModal #name").val(reference);
            //         },5000);
                    
            //      },
            //      null,
            //      null,
            //      {async : false}
            // );

            $("#ajaxFormModal .orgaNametext").hide();
            $("#ajaxFormModal .invoiceNametext").hide();
            $("#ajaxFormModal .invoiceAddresstextarea").hide();
            $("#ajaxFormModal .invoiceSirettext").hide();
            //$("#ajaxFormModal .invoiceselect label").after("<span class='col-xs-12' style='font-size: small;text-align: justify;'><span>Le montant indiqué dans la demande de don est considéré comme HT. Notre association relève du régime fiscal et la TVA à 20% sera ajoutée à la facture.</span></span>");
            $("#ajaxFormModal #orgaName").off().on("blur",function(){
                dyFObj.searchExist($(this).val(),"organizations",null,"donationLink");
            });  
        },
        beforeSave : function(data){
            mylog.log("beforeSave donation",data);
            //alert("beforeSave");

            if ($("#ajaxFormModal #orgaName").val()!="" && notNull($("#ajaxFormModal #orgaName").val())){
                var nameOrga = $("#ajaxFormModal #orgaName").val();
                var ref = $("#ajaxFormModal #name").val();
                $("#ajaxFormModal #name").val(ref+"_"+nameOrga);
            }
        },
        // beforeBuild : function(data) {
        //     mylog.log("beforeBuild donation",data);
        //     var values={};
        //     values.parent={};
        //     values.parent[contextData.id]={
        //         "name" : costum.assetsSlug,
        //         "type" : data.contextType
        //     };
        //     dyFObj.openForm("project",null,values,null,dyFProject);
        // },
        afterSave : function(data){
            mylog.log("aftersave donation",data);

            var data=data.map;

            var reference = data.name;
            var amount = data.amount;

            var email=data.email;

            toastr.success("Votre promesse a bien été ajoutée");
            $("#after-pledge-modal .pledge-reference").html(reference);
            $("#after-pledge-modal .pledge-amount").html(amount);
            var results ={};
            campId=Object.keys(data.parent)[0];
            results[campId]={"name":"ok"};

            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/crowdfunding/getcampaignandcounters",
                {results},
                function(data){
                    var data=data.results[campId];
                    mylog.log("callback after donation",data);
                    var iban = (typeof data.iban!="undefined") ? data.iban : "Non renseigné";
                    var donationPlatform = (typeof data.donationPlatform!="undefined") ? data.donationPlatform : "Non renseigné";
                    var donationUrlPlatform = (typeof data.donationPlatform!="undefined") ? data.donationPlatform : "";
                    var nameCampaign = data.name;
                    var projectName = data.parent[Object.keys(data.parent)[0]]["name"];
                    $("#after-pledge-modal .camp-iban").html(iban);
                    $("#after-pledge-modal .campaign-ext-url").html(donationPlatform);
                    $("#after-pledge-modal .campaign-href-url").attr("href", donationUrlPlatform );
                    $("#after-pledge-modal .campaign-name").html(nameCampaign);  
                    $("#after-pledge-modal .project-name").html(projectName);           
                    var paramsmail={
                        tpl : "basic",
                        tplObject : "Votre promesse de don a bien été enregistrée - Référence "+reference ,
                        tplMail : email,
                        html : '<div class="modal-content"><div class="modal-header"><h3 class="pt-5 mb-0 text-secondary text-confirm text-center">Votre promesse de don a bien été enregistrée</h3></div><div class="modal-body text-left"><p>Nous avons bien reçu votre promesse de don pour un montant de <span style="color:#F08336;font-weight:700" class="pledge-amount">'+amount+'</span> euros pour la campagne <span style="color:#F08336;font-weight:700" class="campaign-name">'+ nameCampaign +'</span> portant sur <span class="project-name" style="color:#F08336;font-weight:700;">'+projectName+'</span>.</p>Vous pouvez envoyer votre don de deux manières :</p><dl style="font-size: 17px;"><dt style="color:#05323E;">Par paiement sur la plateforme</dt><dd>Lien vers la plateforme de don choisie : <a style="color:#F08336;" class="campaign-href-url"  target="_blank" href="'+donationUrlPlatform+'"><span class="campaign-ext-url">'+donationPlatform+'</span></a></dd><dt style="color:#05323E;">Par virement bancaire</dt><dd>IBAN : <span style="color:#F08336;" class="camp-iban">'+iban+'</dd></dl><p><i class="fa fa-warning"></i> Veillez à bien préciser la référence <span class="pledge-reference" style=color:#F08336;>'+reference+'</span> quelque soit votre mode de paiement (ordre de virement ou commentaire HelloAsso).</></div></div>'
                        //html: '<div class="modal-body text-left"><p>Nous avons bien reçu votre promesse de don pour un montant de <span class="pledge-amount">'+amount+'</span> euros pour la campagne <span class="campaign-name">'+nameCampaign+'</span> sous la référence <span>'+reference+'</span>.<br><p>Vous pouvez envoyer, dès à présent, votre don de deux manières :</p><dl><dt>Par paiement sur la plateforme</dt><dd>Lien vers la plateforme de don choisie : <a target="_blank" href="'+donationPlatform+'"><span class="campaign-ext-url">'+donationPlatform+'</span></a></dd><dt>Par virement bancaire</dt><dd>IBAN : <span class="camp-iban">'+iban+'</span></dd></dl></div>'
                    };
                    ajaxPost(
                        null,
                        baseUrl+"/co2/mailmanagement/createandsend",
                        paramsmail,
                        function(data){ 
                            
                        }
                    );
                }
            );
            
            $("#after-pledge-modal").modal('show');
                            
            urlCtrl.loadByHash(location.hash);
            if(costum.unloggedMode!=dyFObj.unloggedMode){
                dyFObj.unloggedMode=costum.unloggedMode;
            }

           
        }    
    }
};


