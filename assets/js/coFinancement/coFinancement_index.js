// function addDonation(){
//     		$("#page-top").on("click", ".addDonation", function() {
			
// 			 var dataEdit = {};
// 	         dataEdit.receiver={};

// 	        var keyId = $(this).data("id");
// 	        dataEdit.receiver[keyId]= {
// 		        collection : $(this).data("type"),
// 		        name : $(this).data("name")
// 		    };

// 		    dataEdit.parent={};
// 		    var idCamp = $(this).data("id-camp");
// 		    dataEdit.parent[idCamp] = {
// 		    	collection : "crowdfunding",
// 		    	name : $(this).data("name-camp")
// 		    };
// 		    dataEdit.type = "pledge";
// 		    //dataEdit.collection ="crowdfunding";	 
//             mylog.log("today",dataEdit.name);
// 		    dyFObj.openForm(sectionDyf.donation,null, dataEdit);	
// 			});
// }			


function addAfterPledgeModal() {
	let objs = $("#after-pledge-modal");
	if (objs.length > 0)
		return;
	// already exists?
	// html string
	let html_string = `
<div id="after-pledge-modal"
		class="modal fade in"
		tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="pt-5 mb-0 text-secondary text-confirm text-center">Votre promesse de don a bien été enregistrée</h3>
		</div>
		<div class="modal-body text-left">
			<p>Nous avons bien reçu votre promesse de don pour un montant de <span style="color:#F08336;font-weight:700" class="pledge-amount"></span> euros 
			  pour la campagne <span style="color:#F08336;font-weight:700" class="campaign-name"></span> portant sur <span class="project-name" style="color:#F08336;font-weight:700;"></span>.</p>
			<!-- <p><strong>Notez cette information</strong></p> -->
			<p>Nous vous l'avons également envoyée par email, mais nous constatons que les emails que nous envoyons 
			  arrivent parfois dans les courriers indésirables ("spams") des destinataires. Voilà pourquoi il est 
			  préférable que vous notiez dès maintenant ces informations.</p>
			<p>Vous pouvez envoyer votre don de deux manières :</p>
			<dl style="font-size: 17px;">
				<dt style="color:#05323E;">Par paiement sur la plateforme</dt>
				<dd>Lien vers la plateforme de don choisie : <a style="color:#F08336;" class="campaign-href-url"  target="_blank" href=""><span class="campaign-ext-url"></span></a></dd>
				<dt style="color:#05323E;">Par virement bancaire</dt>
				<dd>IBAN : <span style="color:#F08336;" class="camp-iban"></dd>
			</dl>
            <p><i class="fa fa-warning"></i> Veillez à bien préciser la référence <span class="pledge-reference" style=color:#F08336;></span> quelque soit votre mode de paiement (ordre de virement ou commentaire HelloAsso).</>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary close-btn" data-bs-dismiss="modal">Fermer</button>
		</div>
	</div>
</div>
<script>
	$("#after-pledge-modal .close-btn").click(function() { $("#after-pledge-modal").modal("hide"); window.location.reload(); });
</script>
	`;

	// insertion as a child of the body
	$("body").append(html_string);
	$("#after-pledge-modal").hide();
}

costum.linkWithFields=function(name){
    $("#ajaxFormModal #orgaName").val(name);

};

function linkElementField(type,id,name,slug,fieldId){
    var dom="#ajaxFormModal";
    var field=fieldId;
    var params ={
            inputType : "finder",
            label : "Organisation liée à la promesse",
            initMe : false,
            rules : {
                required : true,
                lengthMin : [ 
                    1, 
                    "parent"
                ]
            }

    };
    params["id"]=fieldId;
    params["initType"]=[type];
    //var fieldObj=dyFInputs.links(params);
    var formValues = {};
    formValues[field]={};
    formValues[field][id] = {};
    formValues[field][id]["name"] = name;
    formValues[field][id]["type"] = type;
    mylog.log("formValues",formValues); 

         


  dyFObj.buildInputField(dom, field, params,formValues,null);
  dyFObj.initFieldOnload[field+"Finder"]();

}

costum.searchExist = function(type,id,name,slug,email){
    linkElementField(type,id,name,slug,"donationLink");
    costum.linkWithFields(name);
    $("#ajaxFormModal #similarLink").hide();
    $("#ajaxFormModal .donationLinkfinder").hide();
};

costum.checkboxSimpleEvent = {
    true : function(id){
        if(id=="behalf"){
            $("#ajaxFormModal .orgaNametext").show();
            $("#ajaxFormModal #orgaName").prop('required',true); 

        }
        else if(id=="invoice" && $("#ajaxFormModal #behalf").val()=="true"){
            $("#ajaxFormModal #invoiceName").val($("#ajaxFormModal #orgaName").val());
            $("#ajaxFormModal .invoiceNametext").show();
            $("#ajaxFormModal #invoiceName").prop('required',true); 
            $("#ajaxFormModal .invoiceAddresstextarea").show();
            $("#ajaxFormModal #invoiceAddress").prop('required',true); 
            $("#ajaxFormModal .invoiceSirettext").show();
            $("#ajaxFormModal #invoiceSiret").prop('required',true); 
        }    
    },
    false : function(id){
        if(id=="behalf"){
            $("#ajaxFormModal .orgaNametext").hide();
            $("#ajaxFormModal #orgaName").prop('required',false);
        }
        else if(id=="invoice"){
            $("#ajaxFormModal .invoiceNametext").hide();
            $("#ajaxFormModal #invoiceName").prop('required',false); 
            $("#ajaxFormModal .invoiceAddresstextarea").hide();
            $("#ajaxFormModal #invoiceAddress").prop('required',false); 
            $("#ajaxFormModal .invoiceSirettext").hide();
            $("#ajaxFormModal #invoiceSiret").prop('required',false); 
        }
    }
};

costum[costum.slug] = {
	init : function(){
        // dyFObj.unloggedMode=true;
		//addDonation();
		addAfterPledgeModal();
	},
    organizations : {
        afterSave : function(data){
            toastr.success("Votre structure est bien enregistrée. Veuillez renseigner les informations relatives au commun.");
            var dataEdit ={};
            mylog.log("aftersave data orga cofin",data);
            dataEdit.parent={};
            var parentId = data.id;
            dataEdit.parent[parentId] = {};
            dataEdit.parent[parentId].type=data.map.collection;
            dataEdit.parent[parentId].name=data.map.name;
            dyFObj.openForm("project",null,dataEdit);
            $( "#ajax-modal").scrollTop(0);
        }
    },
	projects : {
		afterSave : function(data){
            uploadObj.gotoUrl=location.hash;
            dyFObj.commonAfterSave(data, function(data){
    			toastr.success("Votre commun a bien enregistré. Veuillez précisez les modalités de la campagne de co-financement.");
    			mylog.log("aftersave data projects cofin",data);
                // var dataEdit ={};
                // dataEdit.parent={};
                // var parentId = data.id;
                // dataEdit.parent[parentId] = {};
                // dataEdit.parent[parentId].type=data.map.collection;
                // dataEdit.parent[parentId].name=data.map.name;
                // dyFObj.openForm("crowdfunding",null,dataEdit);
                //$( "#ajax-modal").scrollTop(0);
                dyFObj.closeForm();
                urlCtrl.loadByHash(location.hash);

            });
		}
	},
    donation : {
        setUnloggedMode : function(){
            
            dyFObj.unloggedMode=true;
           // alert(dyFObj.unloggedMode);
                
        },
        afterBuild : function(data){
            //alert("kjk");
            mylog.log("afterbuild data",data);
            $( "#ajax-modal .input-group").after("<span class='col-xs-12' style='text-align: justify;'><i class='fa fa-info'></i> Il est nécessaire de créer un compte pour authentifier la déclaration. Vous ne recevrez pas de mails de notre part. Ce compte vous permet d’accèder aux autres espaces communecter comme la cartographie nationale de France Tiers-Lieux</span>");
            //alert("afterbuild");
            var commonName = data.receiver[Object.keys(data.receiver)[0]]["name"];
            var campaignId = Object.keys(data.parent)[0];
            var campaignName = data.parent[Object.keys(data.parent)[0]]["name"];
            var pledgeNumber = 0;
            mylog.log("commonName,campaignId,campaignName",commonName,campaignId,campaignName);
            ajaxPost(
                 null,
                 baseUrl+"/" + moduleId + "/crowdfunding/getpledgesfromcampaignid/id/"+campaignId+"/type/pledge",
                 null,
                 function(data){
                    pledgeNumber=data.pledgeIteration + 1;
                    mylog.log("pledgeNumber",pledgeNumber);
                    var reference = commonName+"_"+campaignName+"_"+pledgeNumber.toString();
                    reference =slugify(reference);
                    //alert(reference);
                    setTimeout(function(){
                        $("#ajaxFormModal #name").val(reference);
                    },5000);
                    
                 },
                 null,
                 null,
                 {async : false}
            );

            $("#ajaxFormModal .orgaNametext").hide();
            $("#ajaxFormModal .invoiceNametext").hide();
            $("#ajaxFormModal .invoiceAddresstextarea").hide();
            $("#ajaxFormModal .invoiceSirettext").hide();
            $("#ajaxFormModal .invoiceselect label").after("<span class='col-xs-12' style='font-size: small;text-align: justify;'><span>Le montant indiqué dans la demande de don est considéré comme HT. Notre association relève du régime fiscal et la TVA à 20% sera ajoutée à la facture.</span></span>");
            $("#ajaxFormModal #orgaName").off().on("blur",function(){
                dyFObj.searchExist($(this).val(),"organizations",null,null);
                //searchExist : function (searchValue,types,contact,extraOnBlur)

            }); 
            // $('#ajaxFormModal #behalf').focus(function() {
            //         prev_val = $(this).val();
            //     }).change(function(){
            //                 $(this).unbind('focus');
            //                 var conf = confirm('Are you sure want to change status ?');

            //                 if(conf == true){
            //                     //your code
            //                 }
            //                 else{
            //                     $(this).val(prev_val);
            //                     $(this).bind('focus');
            //                     return false;
            //                 }
            //     });
            


            //alert($("#ajaxFormModal #behalf").val());
            // $("#ajaxFormModal #behalf").unbind("click").on("click", function(){
            //         alert("yooy");
            //             // mylog.log("behal");
            //             //  if($(this).val()=="true"){
            //             //     mylog.log("half true");
            //             //     $("#ajaxFormModal .invoiceNametext").show();
            //             //     $("#ajaxFormModal .invoiceAddresstextarea").show();
            //             //     $("#ajaxFormModal .invoiceSirettext").show();
            //             // }
            //             // else{
            //             //     $("#ajaxFormModal .invoiceNametext").hide();
            //             //     $("#ajaxFormModal .invoiceAddresstextarea").hide();
            //             //     $("#ajaxFormModal .invoiceSirettext").hide();    
            //             // }

                   
                
            //     // else{
            //     //     $("#ajaxFormModal .invoiceNametext").hide();
            //     //     $("#ajaxFormModal .invoiceAddresstext").hide();
            //     //     $("#ajaxFormModal .invoiceSirettext").hide();  
            //     // }
            // });

            //coInterface.bindEvents();            
                //     $("#ajaxFormModal .invoiceselect #select2-chosen-3").text()                   
                // }
                // invoiceselect
                // invoiceDetailstextarea   

        },
        beforeSave : function(data){
            mylog.log("beforeSave donation",data);
            //alert("beforeSave");

            if ($("#ajaxFormModal #orgaName").val()!="" && notNull($("#ajaxFormModal #orgaName").val())){
                var nameOrga = $("#ajaxFormModal #orgaName").val();
                var ref = $("#ajaxFormModal #name").val();
                $("#ajaxFormModal #name").val(ref+"_"+nameOrga);
            }
        },    
        afterSave : function(data){
            mylog.log("aftersave donation",data);

            var data=data.map;

            // if(typeof data.behalf!="undefined" && notNull(data.behalf)){
            //     dyFObj.openForm("organization",null,{"name" : data.behalf}); 
            //     $("#ajax-modal .infocustom p").html("Vous avez fait un don au nom d'une structure. Veuillez s'il vous plaît vérifier que celle-ci est bien référencée. Sinon, nous vous invitons à l'ajouter.");
            // }

            var reference = data.name;
            var amount = data.amount;

            // var email=json_encode("<?= Yii::app()->session['userEmail'] ?>");
            var email=data.email;

            toastr.success("Votre promesse a bien été ajoutée");
            $("#after-pledge-modal .pledge-reference").html(reference);
            $("#after-pledge-modal .pledge-amount").html(amount);
            var results ={};
            campId=Object.keys(data.parent)[0];
            results[campId]={"name":"ok"};

            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/crowdfunding/getcampaignandcounters",
                {results},
                function(data){
                    var data=data.results[campId];
                    mylog.log("callback after donation",data);
                    var iban = (typeof data.iban!="undefined") ? data.iban : "Non renseigné";
                    var donationPlatform = (typeof data.donationPlatform!="undefined") ? data.donationPlatform : "Non renseigné";
                    var donationUrlPlatform = (typeof data.donationPlatform!="undefined") ? data.donationPlatform : "";
                    var nameCampaign = data.name;
                    var projectName = data.parent[Object.keys(data.parent)[0]]["name"];
                    $("#after-pledge-modal .camp-iban").html(iban);
                    $("#after-pledge-modal .campaign-ext-url").html(donationPlatform);
                    $("#after-pledge-modal .campaign-href-url").attr("href", donationUrlPlatform );
                    $("#after-pledge-modal .campaign-name").html(nameCampaign);  
                    $("#after-pledge-modal .project-name").html(projectName);           
                    var paramsmail={
                        tpl : "basic",
                        tplObject : "Votre promesse de don a bien été enregistrée - Référence "+reference ,
                        tplMail : email,
                        html : '<div class="modal-content"><div class="modal-header"><h3 class="pt-5 mb-0 text-secondary text-confirm text-center">Votre promesse de don a bien été enregistrée</h3></div><div class="modal-body text-left"><p>Nous avons bien reçu votre promesse de don pour un montant de <span style="color:#F08336;font-weight:700" class="pledge-amount">'+amount+'</span> euros pour la campagne <span style="color:#F08336;font-weight:700" class="campaign-name">'+ nameCampaign +'</span> portant sur <span class="project-name" style="color:#F08336;font-weight:700;">'+projectName+'</span>.</p>Vous pouvez envoyer votre don de deux manières :</p><dl style="font-size: 17px;"><dt style="color:#05323E;">Par paiement sur la plateforme</dt><dd>Lien vers la plateforme de don choisie : <a style="color:#F08336;" class="campaign-href-url"  target="_blank" href="'+donationUrlPlatform+'"><span class="campaign-ext-url">'+donationPlatform+'</span></a></dd><dt style="color:#05323E;">Par virement bancaire</dt><dd>IBAN : <span style="color:#F08336;" class="camp-iban">'+iban+'</dd></dl><p><i class="fa fa-warning"></i> Veillez à bien préciser la référence <span class="pledge-reference" style=color:#F08336;>'+reference+'</span> quelque soit votre mode de paiement (ordre de virement ou commentaire HelloAsso).</></div></div>'
                        //html: '<div class="modal-body text-left"><p>Nous avons bien reçu votre promesse de don pour un montant de <span class="pledge-amount">'+amount+'</span> euros pour la campagne <span class="campaign-name">'+nameCampaign+'</span> sous la référence <span>'+reference+'</span>.<br><p>Vous pouvez envoyer, dès à présent, votre don de deux manières :</p><dl><dt>Par paiement sur la plateforme</dt><dd>Lien vers la plateforme de don choisie : <a target="_blank" href="'+donationPlatform+'"><span class="campaign-ext-url">'+donationPlatform+'</span></a></dd><dt>Par virement bancaire</dt><dd>IBAN : <span class="camp-iban">'+iban+'</span></dd></dl></div>'
                    };
                    ajaxPost(
                        null,
                        baseUrl+"/co2/mailmanagement/createandsend",
                        paramsmail,
                        function(data){ 
                            
                        }
                    );
                }
            );
            

            //$("#ajax-modal").modal('hide');
            $("#after-pledge-modal").modal('show');
           // $( "#ajax-modal").scrollTop(0);
                            
            urlCtrl.loadByHash(location.hash);
            if(costum.unloggedMode!=dyFObj.unloggedMode){
                dyFObj.unloggedMode=costum.unloggedMode;
                // alert("dif");
                // 
            }

           
        }    
    }	
};


// directory.specific = function(params){
// 	mylog.log("specificPanel",params);
// 	let pic_url = baseUrl + params.profilMediumImageUrl;

	// function get_progress_values() {
	// 	let currency_symbol = "€";
	// 	let min_perc = 2;  // We'd like to show something even if either quantity equals zero.

	// 	if (params.goal === undefined) {
	// 		return {
	// 			goal: "Undefined",
	// 			donations: params.donationNumber.toString() + currency_symbol,
	// 			pledges: params.pledgeNumber.toString() + currency_symbol,
	// 			donations_perc: min_perc.toString(),
	// 			pledges_perc: min_perc.toString()
	// 		};
	// 	}

	// 	// Compute amounts
	// 	let goal = parseInt(params.goal);
	// 	let donations = (typeof params.donationNumber!="undefined") ? parseInt(params.donationNumber) : 0;
	// 	let pledges = (typeof params.pledgeNumber!="undefined") ? parseInt(params.pledgeNumber) : 0;
	// 	let rest = goal - donations - pledges;

	// 	// Compute percentages
	// 	let donations_perc = Math.ceil(donations / goal * 100);
	// 	let pledges_perc = "";
	// 	if (rest <= 0) {
	// 		pledges_perc = 100 - donations_perc;
	// 	} else {
	// 		pledges_perc = Math.ceil(pledges / goal * 100);
	// 	}

	// 	// Clamp
	// 	if (donations_perc <= min_perc) {
	// 		donations_perc = min_perc;
	// 		pledges_perc = Math.min(pledges_perc, 100 - min_perc);
	// 	}
	// 	if (pledges_perc <= min_perc) {
	// 		pledges_perc = min_perc;
	// 		donations_perc = Math.min(donations_perc, 100 - min_perc);
	// 	}


	// 	return {
	// 		goal: params.goal.toString() + currency_symbol,
	// 		donations: params.donationNumber.toString() + currency_symbol,
	// 		pledges: params.pledgeNumber.toString() + currency_symbol,
	// 		donations_perc: donations_perc.toString(),
	// 		pledges_perc: pledges_perc.toString()
	// 	};
	// }

//     function get_progress_values() {
//             var currency_symbol = "€";
//             var min_perc = 2;  // We'd like to show something even if either quantity equals zero.

//             // if (typeof params.goal == "undefined") {
//             //  return {
//             //      goal: "Undefined",
//             //      donations: params.donationNumber.toString() + currency_symbol,
//             //      pledges: params.pledgeNumber.toString() + currency_symbol,
//             //      donations_perc: min_perc.toString(),
//             //      pledges_perc: min_perc.toString()
//             //  };
//             // }

//             // Compute amounts
//             var goal = (typeof params.goal!="undefined") ? parseInt(params.goal) : 0;
//             var donations = (typeof params.donationNumber!="undefined") ? parseInt(params.donationNumber) : 0;
//             var pledges = (typeof params.pledgeNumber!="undefined") ? parseInt(params.pledgeNumber) : 0;
//             var rest = goal - donations - pledges;

//             // Compute percentages
//             var donations_perc = Math.ceil(donations / goal * 100);
//             var pledges_perc = "";
//             if (rest <= 0) {
//                 pledges_perc = 100 - donations_perc;
//             } else {
//                 pledges_perc = Math.ceil(pledges / goal * 100);
//             }

//             // Clamp
//             if (donations_perc <= min_perc) {
//                 donations_perc = min_perc;
//                 pledges_perc = Math.min(pledges_perc, 100 - min_perc);
//             }
//             if (pledges_perc <= min_perc) {
//                 pledges_perc = min_perc;
//                 donations_perc = Math.min(donations_perc, 100 - min_perc);
//             }


//             return {
//                 goal: goal.toString() + currency_symbol,
//                 donations: donations.toString() + currency_symbol,
//                 pledges: pledges.toString() + currency_symbol,
//                 donations_perc: donations_perc.toString(),
//                 pledges_perc: pledges_perc.toString()
//             };
//         }
// 	let prog = get_progress_values();

// 	let str = `
// <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 searchEntityContainer simplePanelHtml
// 	${params.containerClass} contain_${params.collection}_${params.id} campaign-vignette">
// 	<div class="searchEntity" id="entity${params.id}">
// 		<div class="panel-heading row">
// 			<div class="col-xs-12 col-sm-6 col-md-6">
// 			    <a href="${params.hash}" target="_blank">
//                     <h3 id="col2" class="no-margin">${params.name}</h3><br>
//                     <h4 id="col2" class="no-margin">${params.nameCamp}</h4>
//                 </a>
// 			</div>
// 			<div class="col-xs-12 col-sm-6 col-md-6 group-amount-target-ctn">
// 				<a href="#${params.hash}" target="_blank">
// 					<span class="btn btn-default pull-right">
// 						Objectif : <span class="badge">${prog.goal}</span>
// 					</span>
// 				</a>
// 			</div>
// 		</div>
// 		<div class="panel-body row">
// 			<div class="col-xs-12 col-sm-3 col-md-3">
// 				<div class="square-pic-group">
// 					<img src="${pic_url}" class="img-responsive"/>
// 				</div>
// 			</div>
// 			<div class="col-md-9 col-sm-9 col-xs-12">
// 				<div class="row">
// 					<div class="col-md-12">
// 						<div class="progress">
// 							<div class="progress-bar donation-bar" role="progressbar"
// 								style="width:${prog.donations_perc}%"></div>
// 							<div class="progress-bar pledge-bar" role="progressbar"
// 								style="width:${prog.pledges_perc}%"></div>
// 						</div>
// 					</div>
// 				</div>
// 				<div class="row">
// 					<div class="col-md-4 col-xs-12 progress-bar-key">
// 						<div><span class="label donation-label">Dons reçus : ${prog.donations}</span></div>
// 						<div><span class="label pledge-label">Dons promis : ${prog.pledges}</span></div>
// 					</div>
// 					<div class="col-md-6 col-xs-12 text-center">
// 						<a data-id="${params.id}" data-name="${params.name}" data-type="${params.collection}"
// 							data-id-camp="${params.idCamp}" data-name-camp="${params.nameCamp}"
// 							class="addDonation btn btn-lg btn-primary">Je cofinance cette campagne !</a>
// 					</div>
// 				</div>
// 			</div>
// 		</div>
// 	</div>
// </div>
// 	`;
// 	return str;
// };


 // var sectionDyf={};
	// 	sectionDyf.donation = {
	//         "jsonSchema" : {    
	//             "title" : "Votre promesse de don",
	//             "icon" : "fa-cog",
	//             "properties" : {
	//             	"info" : {
	// 					"inputType" : "custom",
	// 					"html":"<p class='text-yellow"+
	// 					//"Faire connaître votre Organisation n'a jamais été aussi simple !<br>" +
	// 					"Votre promesse de don<hr>" +
	// 				 "</p>",
	// 				},
 //                    "civility" : {
 //                    	"inputType" : "select",
 //                    	"label" : "Civilité",
 //                    	"placeholder" : "Choisir ...",
 //                        "list" : "civility",
 //                    	"groupOptions" : false,
 //                        "groupSelected" : false,
 //                    	"optionsValueAsKey":true,
 //                        "select2" : {
 //                        	"multiple" : false
 //                        },
 //                        "rules": {
 //                            "required":true
 //                        }
 //                    },
 //                    "surname" : {
 //                        "inputType" : "text",
 //                        "label" : "Nom de famille",
 //                        "placeholder" : "Nom de famille",
 //                        "rules": {
 //                            "required":true
 //                        }
 //                    },
 //                    "donatorName" : {
 //                        "inputType" : "text",
 //                        "label" : "Prénom",
 //                        "placeholder" : "Prénom",
 //                        "rules": {
 //                            "required":true
 //                        }
 //                    },
 //                    "email" : {
 //                        "label" : "Email",
 //                        "placeholder" : "Email",
 //                        "rules": {
 //                            "required":true,
 //                            "email": true
 //                        }
 //                    },
 //                    "telephone" : {
 //                        "inputType" : "text",
 //                        "label" : "Téléphone",
 //                        "placeholder" : "Téléphone",
 //                        "rules": {
 //                            "required":true
 //                        }
 //                    },
 //                    "amount" : {
 //                        "inputType" : "text",
 //                        "label" : "Montant de votre promesse de don, en Euros",
 //                        "placeholder" : "Montant en Euros",
 //                        "rules": {
 //                            "required":true
 //                        }
 //                    },
 //                    "invoice" : {
 //                    	"inputType" : "select",
 //                    	"placeholder" : "",
 //                    	"label" : "Souhaitez-vous une facture ?",
 //                        "options" : [
 //                        	"Non",
 //                        	"Oui"
 //                        ],	
 //                    	"groupOptions" : false,
 //                        "groupSelected" : false,
 //                    	"optionsValueAsKey":true,
 //                        "select2" : {
 //                        	"multiple" : false
 //                        }
 //                    },
 //                    "receiver" : {
 //                        "inputType" : "finder",
 //                        "id" : "receiver",
 //                        "label" : "A quel commun s'adresse la promesse de don",
 //                        "initType" : [
 //                            "projects"
 //                        ],
 //                        "initMe" : false,
 //                        "rules" : {
 //                            "required" : true,
 //                            "lengthMin" : [ 
 //                            	1, 
 //                                "parent"
 //                            ]
 //                        }  
 //                    },
 //                    "parent" : {
 //                        "inputType" : "finder",
 //                        "id" : "receiver",
 //                        "label" : "Campagne financée",
 //                        "initType" : [
 //                            "crowdfunding"
 //                        ],
 //                        "initMe" : false,
 //                        "rules" : {
 //                            "required" : true,
 //                            "lengthMin" : [ 
 //                            	1, 
 //                                "parent"
 //                            ]
 //                        }  
 //                    },
 //                    "type" : {
 //                    	"inputType" : "hidden"
 //                    },
 //                    "name" : {
 //                    	"inputType" : "hidden"
 //                    }

	//             },
	//             save : function (data) {
	//             	 mylog.log("pledge",data);
	//             	 	var today = new Date();
	// 	   				 mylog.log("today",today.getDate());
 //            			data.name = today.getDate() + '' + (today.getMonth()+1) + '' +today.getFullYear()+'' + today.getHours()+ '' +today.getMinutes();
	//             	 	data.source = {
	//             	 		insertOrign : "costum",
	// 				        keys : [ 
	// 				            costum.contextSlug
	// 				        ],
	// 				        key : costum.contextSlug

	//             	 	};
	//             	 	var	tplCtx = {};
	//             	 	data.collection ="crowdfunding";
	// 					tplCtx.value=data;
	// 					tplCtx.collection = "crowdfunding";
	// 					tplCtx.path = "allToRoot";
 //                        // $.each( sectionDyf.donation.jsonSchema.properties , function(k,val) {
 //                        //     if(val.inputType == "properties")
 //                        //     tplCtx.value[k] = getPairsObj('.'+k+val.inputType);
 //                        // else if(val.inputType == "array")
 //                        //     tplCtx.value[k] = getArray('.'+k+val.inputType);
 //                        // else
 //                        //     tplCtx.value[k] = $("#"+k).val();
 //                        // mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
 //                    //});
 //                    mylog.log("pledge tplCtx",tplCtx);
 //                    if(typeof tplCtx.value == "undefined")
 //                        toastr.error('value cannot be empty!');
 //                    else {
 //                        //tplCtx.updatePartial=true;
 //                        //tplCtx.arrayForm = true;
 //                        dataHelper.path2Value( tplCtx, function(params) {
 //                            // Set dynamic values onto the after-pledge modal
 //                            $("#after-pledge-modal .pledge-reference").html(data.name);
 //                            $("#after-pledge-modal .pledge-amount").html(data.amount);
 //                            // FIXME: replace hardcoded placeholders with dynamic values
 //                            $("#after-pledge-modal .org-iban").html("&lt;IBAN de l'organisation&gt;");
 //                            $("#after-pledge-modal .campaign-ext-url").html("&lt;url plateforme de la campagne&gt;");
 //                            $("#after-pledge-modal .campaign-name").html("&lt;nom de la campagne&gt;");

 //                            $("#ajax-modal").modal('hide');
 //                            $("#after-pledge-modal").modal('show');
 //                            toastr.success("Bien ajouté");
 //                            urlCtrl.loadByHash(location.hash);
 //                            // window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug+'#?tags='+urlTags;
 //                        } );
 //                    }
	//             }
	//         }
 //    	};





  //   	function addDonation(){
  //   		$("#page").on("click", ".addDonation", function() {
		// // 		tplCtx = {};
		// 		tplCtx.id = "";
		// 		tplCtx.collection = "crowdfunding";
		// 		tplCtx.path = "allToRoot";
		//         dyFObj.openForm(sectionDyf.donation,null, null);	
		// 	});
	 // //        // };
	 // //          var dataEdit = {};
	 // //         dataEdit.receiver={};

	 // //        var keyId = $(this).data("id");
	 // //         dataEdit.receiver[keyId]= {
		// //          		collection : $(this).data("type"),
		// //          		name : $(this).data("name")
		// //       	 };
	 // //         dyFObj.openForm('pledge', null, dataEdit);
	 // //    });
			
	 //    }    



    
