directory.elementPanelHtml = function(params){

		mylog.log('directory.elementPanelHtml mednumRhoneAlpe',params.type,params.name,params.elTagsList, params);
		var str = '';

		var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? 'grayscale' : '' ) ;
		var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad['Wait for confirmation'] : '' ) ;
		var classType=params.type;
		classType =(typeof params.category != 'undefined' && notNull(params.category)) ? ' '+params.category : '';
		if(params.type=='events') classType='';

		str += '<div class=\'col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+grayscale+' '+classType+' '+params.elTagsList+' '+params.elRolesList+' contain_'+params.type+'_'+params.id+'\'>';
		str +=    '<div class="searchEntity" id="entity'+params.id+'">';

		if(typeof params.edit  != 'undefined' && notNull(params.edit))
			str += this.getAdminToolBar(params);

		if( params.tobeactivated == true ){
			str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+trad['Wait for confirmation']+' </span></div>';
		}else{
			var timeAction= trad.actif;

			var update = params.updated ? params.updated : null; 


			if (params.type != null && 
            params.type == 'projects' &&
            (  typeof params.properties != 'undefined' &&
               typeof params.properties.avancement != 'undefined') ) {

				if (params.properties.avancement == 'inprogress') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-play-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> En cours</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'stopped') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-pause-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> Stoppé</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        ' </span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'archived') {
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-check-circle iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> Archivé</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
				else if (params.properties.avancement == 'upcoming'){
					str += '<div class=\'dateUpdated\'>'+
                        '<span class=\'status\' style=\'margin-left:23px;\'>'+
                          '<i style=\'position:absolute;margin-left:-24px;font-size:18px;\' class=\'fa fa-lightbulb-o iconesStatus\'></i>'+
                            '<p class=\'textStatus\' style=\'position:absolute;color:black;visibility:hidden\'> A venir</p>'+
                        '</span> <i class=\'fa fa-flash\'></i>'+
                        '<span class=\'hidden-xs\'>'+timeAction+
                        '</span>' + update + 
                      '</div>';
				}
			}
			else{
				str += '<div class=\'dateUpdated\'> <i class=\'fa fa-flash\'></i> <span class=\'hidden-xs\'>'+timeAction+' </span>' + update + '</div>';
			}
		}
      
		var linkAction = ( $.inArray(params.type, ['poi','classifieds'])>=0 ) ? ' lbh-preview-element' : ' lbh';

		if( typeof directory.costum != 'undefined' &&
        directory.costum != null &&
        typeof directory.costum.preview != 'undefined' && 
        directory.costum.preview === true ){
			linkAction = ' lbh-preview-element';
		}

		if(typeof params.imgType !='undefined' && params.imgType=='banner'){
			str += '<a href=\''+params.hash+'\' class=\'container-img-banner add2fav '+linkAction+'>' + params.imgBanner + '</a>';
			str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

			str += '<div class=\'entityRight banner no-padding\'>';

			if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'container-thumbnail-profil add2fav '+linkAction+'\'>' + params.imgProfil + '</a>';
				str +=    '<a href=\''+params.hash+'\' class=\'add2fav pull-right margin-top-15 '+linkAction+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}else{
			str += '<a href=\''+params.hash+'\' class=\'container-img-profil add2fav '+linkAction+'\'>' + params.imgMediumProfil + '</a>';
			str += '<div class=\'padding-10 informations tooltips\'  data-toggle=\'tooltip\' data-placement=\'top\' data-original-title=\''+tipIsInviting+'\'>';

			str += '<div class=\'entityRight profil no-padding\'>';

			if(typeof params.size == 'undefined' || params.size == undefined || params.size == 'max'){
				str += '<div class=\'entityCenter no-padding\'>';
				str +=    '<a href=\''+params.hash+'\' class=\'add2fav pull-right '+linkAction+'\'>' + params.htmlIco + '</a>';
				str += '</div>';
			}
		}


		var iconFaReply ='';
		str += '<a  href=\''+params.hash+'\' class=\''+params.size+' entityName bold text-dark add2fav '+linkAction+'\'>'+
					iconFaReply + params.name +
				'</a>';  
		
		if(typeof(params.statusLink)!='undefined'){
			if(typeof(params.statusLink.isAdmin)!='undefined'
         && typeof(params.statusLink.isAdminPending)=='undefined'
          && typeof(params.statusLink.isAdminInviting)=='undefined'
            && typeof(params.statusLink.toBeValidated)=='undefined')
				str+='<span class=\'text-red\'>'+trad.administrator+'</span>';
			if(typeof(params.statusLink.isAdminInviting)!='undefined'){
				str+='<span class=\'text-red\'>'+trad.invitingToAdmin+'</span>';
			}
			if(typeof(params.statusLink.toBeValidated)!='undefined' || typeof(params.statusLink.isAdminPending)!='undefined')
				str+='<span class=\'text-red\'>'+trad.waitingValidation+'</span>';
		}

		if(params.rolesLbl != '')
			str += '<div class=\'rolesContainer\'>'+params.rolesLbl+'</div>';
 

	

		var thisLocality = '';
		if(params.fullLocality != '' && params.fullLocality != ' ')
			thisLocality = '<a href=\''+params.hash+'\' data-id=\'' + params.dataId + '\'  class=\'entityLocality add2fav'+linkAction+'\'>'+
							'<i class=\'fa fa-home\'></i> ' + params.fullLocality + '</a>';
		else thisLocality = '';

		str += thisLocality;

		var devise = (typeof params.devise != 'undefined') ? params.devise : '';
		if(typeof params.price != 'undefined' && params.price != '')
			str += '<div class=\'entityPrice text-azure\'><i class=\'fa fa-money\'></i> ' + params.price + ' ' + devise + '</div>';
 
		if($.inArray(params.type, ['classifieds','ressources'])>=0 && typeof params.category != 'undefined'){
			str += '<div class=\'entityType col-xs-12 no-padding\'><span class=\'uppercase bold pull-left\'>' + tradCategory[params.section] + ' </span><span class=\'pull-left\'>';
			if(typeof params.category != 'undefined' && params.type != 'poi') str += ' > ' + tradCategory[params.category];
			if(typeof params.subtype != 'undefined') str += ' > ' + tradCategory[params.subtype];
			str += '</span></div>';
		}
		if(notEmpty(params.typeEvent))
			str += '<div class=\'entityType\'><span class=\'uppercase bold\'>' + tradCategory[params.typeEvent] + '</span></div>';  
    
		if(params.type=='events'){
			var dateFormated = directory.getDateFormated(params, true);
			var countSubEvents = ( params.links && params.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(params.links.subEvents).length+' '+trad['subevent-s']  : '' ;
			str += dateFormated+countSubEvents;
		}

		if (params.typeSig == 'organizations' && typeof params.typePlace != "undefined") {
			str += '<br><span style="font-size:14px;" class="typePlace">'+params.typePlace+'</span>';
		}

		str += '<div class=\'entityDescription\'>' + ( (params.shortDescription == null ) ? '' : params.shortDescription ) + '</div>';
		// if (params.itemType == "projects") {
		// 	if (params.categ != null) {
		// 	str += '<div style="font-size:12px;margin-bottom:1%;" class=\'entityCateg\'>' + ( (params.categ == null ) ? '' : params.categ ) + ( (params.subcateg == null ) ? '' : ' > ' + params.subcateg ) + ( (params.subsubcateg == null ) ? '' : ' > '+params.subsubcateg )+'</div>';
		// 	}
  //  		}

  		if (params.typeSig == 'organizations' && typeof params.category != "undefined") {
  			str += '<div style="font-size:12px;margin-bottom:1%;" class=\'entityCateg\'> Catégorie : ' + params.category + '</div>';
  		}

		str += '<div class=\'tagsContainer text-red\'>'+params.tagsLbl+'</div>';
		if(typeof params.counts != 'undefined'){
			str+='<div class=\'col-xs-12 no-padding communityCounts\'>';
			$.each(params.counts, function (key, count){
				var iconLink=(key=='followers') ? 'link' : 'group';
				str +=  '<small class=\'pull-left lbh letter-light bg-transparent url elipsis bold countMembers margin-right-10\'>'+
                      '<i class=\'fa fa-'+iconLink+'\'></i> '+ count + ' ' + trad[key] +
                    '</small>';
			});
			str+='</div>';
		}

		if(userId != null && userId != '' && typeof params.id != 'undefined' && typeof params.type != 'undefined') 
			str+=directory.socialToolsHtml(params);  
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		str += '</div>';
		return str;
}