costum[costum.contextSlug] = {
	categoriesTL: {},
	init: function () {
		if (typeof costum.tags != "undefined" && typeof costum.lists.typePlace != "undefined" && typeof costum.lists.services != "undefined") {
			$.each(costum.tags, function (index, tag) {
				let existInTypePlace = $.grep(costum.lists.typePlace, function (tltp, i) {
					return (tltp && tltp.toLowerCase() == tag.toLowerCase());
				})

				let existInServices = $.grep(costum.lists.services, function (tls, i) {
					return (tls && tls.toLowerCase() == tag.toLowerCase());
				})

				if (existInTypePlace.length != 0 || existInServices.length != 0) {
					costum[costum.contextSlug].categoriesTL[tag + ""] = tag + "";
				}
			})

			if (typeof costum.lists == "undefined") {
				costum.lists = {};
			}

			costum.lists["thematic"] = costum[costum.contextSlug].categoriesTL;
		}

		if (typeof costum.loaded == "undefined" || costum.loaded == false) {
			costum[costum.contextSlug].helpers.duplicateTemplate();
		}
	},
	"organizations": {
		formData: function (data) {
			return costum[costum.contextSlug].helpers.pushListsInTags(data);
		}
	},
	"events": {
		formData: function (data) {
			return costum[costum.contextSlug].helpers.pushListsInTags(data);
		},

		afterSave : function(data){
			var referenceEvent = {
				id : data.id,
				collection: "events",
                path : "reference.costum",
                value : costum.originKey,
                arrayForm : true
			};
			dataHelper.path2Value(referenceEvent, function(params) {
				urlCtrl.loadByHash(location.hash);
		    });
		}
	},
	"projects": {
		formData: function (data) {
			return costum[costum.contextSlug].helpers.pushListsInTags(data);
		},

		afterSave: function (data) {
			costum[costum.contextSlug].helpers.describeElementInAnswer(data);
			dyFObj.closeForm();
		}
	},
	"badges": {
		formData: function (data) {
			return costum[costum.contextSlug].helpers.pushListsInTags(data);
		}
	},
	"news" : {
		afterSave : function(data){
			var referenceNews = {
				id : data.id,
				collection: "news",            
                path : "reference.costum",
                value : costum.originKey,
                arrayForm : true
			};
			dataHelper.path2Value(referenceNews, function(params) {
				urlCtrl.loadByHash(location.hash);
		    }); 
		}
	},
	helpers : {
		pushListsInTags:function(data){
			$.each(data, function(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];

						if (typeof v == "string") {
							data.tags.push("" + v + "");
						} else {
							$.each(v, function (i, tag) {
								data.tags.push(...tag);
							});
						}
					}
					delete data[e];
				}
			});
			return data;
		},

		describeElementInAnswer: function (data) {
			let dataParams = data;
			dataParams["cid"] = costum.contextId;
			dataParams["cname"] = costum.title;
			dataParams["ctype"] = costum.contextType;

			ajaxPost(
				null,
				baseUrl + "/costum/tierslieuxgenerique/describeproject",
				dataParams,
				function (res) {
					if (res.answerId)
						bootbox.dialog({
							message: `<div class="alert-white text-center">
							<br>
							<strong>${res.msg}</strong>
							<br><br>
							<a href="${baseUrl}/#@${costum.contextSlug}.view.forms.dir.answer.${res.answerId}" class="btn btn-info" target="_blank"> ${res.action} </a>
						</div>`}
						);
				},
				function (error) {
					toastr.error("Une erreur s'est produite");
				}
			);
		},

		duplicateTemplate: function () {
			var dataCustom = {};
			dataCustom.id = costum.contextId;
			dataCustom.type = costum.contextType;
			dataCustom.collection = costum.contextType;
			dataCustom.slug = costum.contextSlug;

			ajaxPost(
				null,
				baseUrl + "/costum/tierslieuxgenerique/generatefromtemplate",
				dataCustom,
				function (data) {
					dataCustom.path = "costum.loaded";
					dataCustom.value = true;
					dataCustom.removeCache=true;
					dataCustom.tplGenerator = true;
					dataHelper.path2Value(dataCustom, function (response) {
						toastr.success(data.msg);
						if(typeof costum.communityLinks.projects!="undefined" && Object.keys(costum.communityLinks.projects).length>0){
							smallMenu.openAjaxHTML(baseUrl + '/costum/tierslieuxgenerique/setprojectcategories');
						}
						urlCtrl.loadByHash("#welcome");
					});
				},
				null,
				null,
				{async:false}
			);
		}
	}
}