var meteolamerCardThemes = {
    getWindTheme: function(speed){
        var WIND_COLORS = [
            {
                speed:[0, 5],
                bgColor:"#0101aa",
                bgColorShade:"#010198", 
                contentColor:"light"
            },
            {
                speed:[5, 10],
                bgColor:"#0100ff",
                bgColorShade:"#0000eb", 
                contentColor:"light"
            },
            {
                speed:[10, 20],
                bgColor:"#0060df",
                bgColorShade:"#0058cc", 
                contentColor:"light"
            },
            {
                speed:[20, 30],
                bgColor:"#01bfbf",
                bgColorShade:"#01adad", 
                contentColor:"dark"
            },
            {
                speed:[30, 40],
                bgColor:"#aeff57",
                bgColorShade:"#a4ff42", 
                contentColor:"dark"
            },
            {
                speed:[40, 50],
                bgColor:"#ffff00",
                bgColorShade:"#ebeb00", 
                contentColor:"dark"
            },
            {
                speed:[50, 60],
                bgColor:"#ffbf00",
                bgColorShade:"#ebb000", 
                contentColor:"dark"
            },
            {
                speed:[60, 70],
                bgColor:"#ff8000",
                bgColorShade:"#eb7500", 
                contentColor:"dark"
            },
            {
                speed:[70, 80],
                bgColor:"#cc5900",
                bgColorShade:"#b85000", 
                contentColor:"light"
            },
            {
                speed:[80, Infinity],
                bgColor:"#993300",
                bgColorShade:"#852c00", 
                contentColor:"light"
            }
        ];
        var themeColor = null;
        WIND_COLORS.forEach(function(item){
            if(item.speed[0] < speed && speed <= item.speed[1])
                themeColor = item;
        })
        return themeColor;
    },
    getWaveTheme: function(height){
        var WAVE_COLORS = [
            {
                height: [0, 0.5],
                bgColor:"#0101aa",
                bgColorShade:"#010198", 
                contentColor:"light"
            },
            {
                height: [0.5, 1],
                bgColor:"#0100ff",
                bgColorShade:"#0000eb", 
                contentColor:"light"
            },
            {
                height: [1, 1.5],
                bgColor:"#0060df",
                bgColorShade:"#0058cc", 
                contentColor:"light"
            },
            {
                height: [1.5, 2],
                bgColor:"#01bfbf",
                bgColorShade:"#01adad", 
                contentColor:"dark"
            },
            {
                height: [2, 2.5],
                bgColor:"#aeff57",
                bgColorShade:"#a4ff42", 
                contentColor:"dark"
            },
            {
                height: [2.5, 3],
                bgColor:"#ffff00",
                bgColorShade:"#ebeb00", 
                contentColor:"dark"
            },
            {
                height: [3, 4],
                bgColor:"#ffbf00",
                bgColorShade:"#ebb000", 
                contentColor:"light"
            },
            {
                height: [4, 4.5],
                bgColor:"#ff8000",
                bgColorShade:"#eb7500", 
                contentColor:"light"
            },
            {
                height: [4.5, 5],
                bgColor:"#cc5900",
                bgColorShade:"#b85000", 
                contentColor:"light"
            },
            {
                height: [5, Infinity],
                bgColor:"#993300",
                bgColorShade:"#852c00", 
                contentColor:"light"
            }
        ]
        var themeColor = null;
        WAVE_COLORS.forEach(item => {
            if(item.height[0] < height && height <= item.height[1])
                themeColor = item;
        })
        return themeColor;
    }
}

$.fn.extend({
    meteolamerWave: function(options){
        this.data = options.data;

        var render = ()=>{
            var themeColor = meteolamerCardThemes.getWaveTheme(this.data.height);
            var content = `
                <div class="card-forecast">
                    <div class="card-forecast-header">
                        <img src="${ASSETS_URL + "/images/meteolamer/big-wave.png"}">
                        Vague
                    </div>
                    <div class="card-forecast-body">
                        <ul>
                            <li class="forecast-item wave-height">
                                <span>Hauteur</span>
                                <span>${parseFloat(this.data.height).toFixed(2)}m</span>
                            </li>
                            <li class="forecast-item wave-dir">
                                <span>Direction</span>
                                <span>
                                    <img 
                                        src="${ASSETS_URL + "/images/meteolamer/dir.png"}" 
                                        class="direction" 
                                        style="transform: rotate(${DIR_DEGREE.getWindDegree(this.data.direction.toLocaleLowerCase())}deg);"
                                    />
                                </span>
                            </li>
                            <li class="forecast-item wave-periode">
                                <span>Periode</span>
                                <span>${parseFloat(this.data.period).toFixed(2)}s</span>
                            </li>
                        </ul>
                    </div>
                </div>
            `;
            this.html(content);
            this.find('.card-forecast').css({
                "background-image": `linear-gradient(to bottom, ${themeColor.bgColor}, ${themeColor.bgColorShade})`,
                "color": themeColor.contentColor === "dark" ? "black":"white"
            })
            this.find('img').css({
                "filter": `invert(${(themeColor.contentColor==="dark"?1:0)})`
            })
            this.find('.card-forecast-header').css({
                "border-color": (themeColor.contentColor==="dark")?"rgba(0, 0, 0, .1)":"rgba(255, 255, 255, .5)"
            })
        }
        render();

        this.update = (data) => {
            this.data = data;
            render();
        }

        return this;
    },
    meteolamerWind: function(options){
        this.data = options.data;

        var render = () => {
            var themeColor = meteolamerCardThemes.getWindTheme(this.data.speed);
            var content = `
                <div class="card-forecast">
                    <div class="card-forecast-header">
                        <img src="${ASSETS_URL + "/images/meteolamer/wind.png"}">
                        Vent
                    </div>
                    <div class="card-forecast-body">
                        <ul>
                            <li class="forecast-item">
                                <span>Vitesse</span>
                                <span>${parseFloat(this.data.speed).toFixed(2)}knot</span>
                            </li>
                            <li class="forecast-item">
                                <span>Direction</span>
                                <span>
                                    <img 
                                        src="${ASSETS_URL + "/images/meteolamer/dir.png"}" 
                                        class="direction"
                                        style="transform: rotate(${DIR_DEGREE.getWindDegree(this.data.direction.toLocaleLowerCase())}deg);"
                                    />
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            `;
            this.html(content)
            this.find('.card-forecast').css({
                "background-image": `linear-gradient(to bottom, ${themeColor.bgColor}, ${themeColor.bgColorShade})`,
                "color": themeColor.contentColor === "dark" ? "black":"white"
            })
            this.find('img').css({
                "filter": `invert(${(themeColor.contentColor==="dark"?1:0)})`
            })
            this.find('.card-forecast-header').css({
                "border-color": (themeColor.contentColor==="dark")?"rgba(0, 0, 0, .1)":"rgba(255, 255, 255, .5)"
            })
        }
        render();

        this.update = (data)=>{
            this.data = data;
            render();
        }

        return this;
    },
    meteolamerTide: function(options){
        this.data = options.data;

        var render = () => {
            var content = `
                <div class="tind-forecast-header">
                    <img src="${ASSETS_URL + "/images/meteolamer/tide.png"}" alt="">
                    Marée
                </div>
                <div class="tind-forecast-body">
                    <ul>
                        <li>Haute</li>
                        <li id="day-tide-high-time-1">${this.data["high"][0].time}</li>
                        <li id="day-tide-high-level-1">${this.data["high"][0].level}m</li>
                    </ul>
                    <ul>
                        <li>Basse</li>
                        <li id="day-tide-low-time-1">${this.data["low"][0].time}</li>
                        <li id="day-tide-low-level-1">${this.data["low"][0].level}m</li>
                    </ul>
                    <ul>
                        <li>Haute</li>
                        <li id="day-tide-high-time-2">${this.data["high"][1].time}</li>
                        <li id="day-tide-high-level-2">${this.data["high"][1].level}m</li>
                    </ul>
                    <ul>
                        <li>Basse</li>
                        <li id="day-tide-low-time-2">${this.data["low"][1].time}</li>
                        <li id="day-tide-low-level-2">${this.data["low"][1].level}m</li>
                    </ul>
                </div>
            `
            this.html(content)
        }
        render();

        this.update = (data)=>{
            this.data = data;
            render();
        }

        return this;
    },
    meteolamerWeather: function(options){
        this.data = options.data;

        var render = () => {
            var content = `
                <div class="weather-forecast-header">
                    <img src="${ASSETS_URL + "/images/meteolamer/weather.png"}" alt="">
                    Météo
                </div>
                <div class="weather-forecast-body">
                    ${
                        this.data ?(`
                            <div id="weather-data-available" style="display:flex;flex-direction:column;align-items:center;">
                                <div id="day-cloud" style="background-image: url('${ASSETS_URL}/images/meteolamer/meteo/${this.data.cloud.toLowerCase()}.png');"></div>
                                <p class="temperature" id="day-temperature">${Math.round(this.data.temperature - 273.15)}°C</p>
                                <p id="day-atmospheric-pressure">${Math.round(this.data.atmosphericPressure / 100)}hPa</p>
                            </div>
                        `):(`
                            <div id="weather-data-inavailable">
                                <img src="${ASSETS_URL + "/images/meteolamer/weather-no-data.png"}" alt="">
                                <span>Donnée indisponible.</span>
                            </div>
                        `)
                    }
                </div>
            `;
            this.html(content);
        }
        render();

        this.update = (data)=>{
            this.data = data;
            render();
        }

        return this;
    }
})