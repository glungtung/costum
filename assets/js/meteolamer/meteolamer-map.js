var meteolamerMap = null;

function getSpotsProject(callback){
    var params = searchInterface.constructObjectAndUrl();
    params.searchType = ["projects"];
    params.indexStep=0;
    params.private = true;
    params.sourceKey = costum.contextSlug;

    ajaxPost(
        null,
        baseUrl+'/'+moduleId+'/search/globalautocomplete',
        params,
        function(data){
            var spots = []
            Object.keys(data.results).forEach(key=>{
                spots.push(data.results[key])
            })
            callback(spots)
        }
    )
}

function getDataMeteolamer(spot, startDate, endDate=null){
    var api = `/costum/meteolamer/getdata/spot/${spot}/start_date/${moment(startDate).format('YYYY-MM-DD')}`;
    if(endDate !== null)
        api=`${api}/end_date/${moment(endDate).format('YYYY-MM-DD')}`;
    
    return new Promise(function(resolve, reject){
        $.get(api, function(res){
            var data = Object.keys(res).map(key => res[key])
            resolve(data)
        })
    })
}

function initializeInputSearch(state){
    $(".meteolamer-map-search-input").autocomplete({
        source: state.spots,
        select: function( _, ui ) {
            var item = ui.item;
            state.set('spot', item)
        }
    });
}

function bindEventTimeNavigation(state){
    $(".time-navigation li").click(function(){
        $(".time-navigation li").removeClass('active');
        $(this).addClass('active');
        var time = $(this).data('time');
        state.set("activeTime", time);
    })
}

function bindEventDayNavigation(state){
    $('.btn-day-navigation').unbind("click").on('click', function(e){
        e.stopPropagation();

        var action = $(this).data("action"),
            newActiveWeekDay = state.activeWeekDay;
        if(action === "next" && (newActiveWeekDay+1 < state.data.length))
            newActiveWeekDay++;
        else if(action === "prev" && (newActiveWeekDay-1 > -1))
            newActiveWeekDay--;

        state.set("activeWeekDay", newActiveWeekDay);
    })
}

function updateComponents(state){
    if(!state.data[state.activeWeekDay] || !state.data[state.activeWeekDay].data[`h${state.activeTime}`])
        return false;
        
    var currentData = state.data[state.activeWeekDay].data[`h${state.activeTime}`],
        wave = currentData.wave,
        tide = currentData.tide,
        wind = currentData.wind,
        spotActiveMarkersData = {
            latLng: [state.spot.geo.latitude, state.spot.geo.longitude],
            wind: {
                degree: DIR_DEGREE[wave.windDirection.toLocaleLowerCase()],
                speed: wave.windSpeed
            },
            wave: {
                degree: DIR_DEGREE[wave.direction.toLocaleLowerCase()],
                height: wave.height,
                period: wave.period
            }
        },
        votes = currentData.votes,
        moyenneVotes = votes ? parseInt(votes.total / votes.users.length):0;
        
    //update wave data
    $("#meteolamer-map-wave-container").meteolamerWave({ data: wave })
    //update wind data
    $("#meteolamer-map-wind-container").meteolamerWind({
        data: {
            speed: wave.windSpeed,
            direction: wave.windDirection
        }
    })
    //update tide data
    $('#meteolamer-map-tide-container').meteolamerTide({ data: tide })
    //update weather data
    $("#meteolamer-map-weather-container").meteolamerWeather({ data: wind })
    //update map layers
    meteolamerMap.addActiveSpotMarkers(spotActiveMarkersData);
    //update date
    $('.date-forecast').text(
        moment.unix(state.data[state.activeWeekDay].date).format("dddd DD MMM")
    )

    $(".note-general").find("li").each(function(index){
        if((index + 1) <= moyenneVotes)
            $(this).find('i').removeClass("fa-star-o").addClass("fa-star")
        else
            $(this).find('i').removeClass("fa-star").addClass("fa-star-o")
    })

}



function setState(state, key, value){
    state[key]=value;
    switch(key){
        case "spots":
            state.spot = state.spots[0];
            var spotName = state.spot.tags[0];

            meteolamerMap = $('#meteolamerMapContainer').meteolamerMap({
                spots:state.spots,
                onClickSpot: (spot)=>{
                    state.set('spot', spot)
                }
            })

            for(let i in state.spots){
                state.spots[i].label = state.spots[i].name
            }

            getDataMeteolamer(spotName, state.startDate, state.endDate).then(function(spotData){
                state.set("data", spotData);
            })

            initializeInputSearch(state)
            bindEventDayNavigation(state)
        break;
        case "data":
            updateComponents(state)
            meteolamerMap.coMap.centerOne(state.spot.geo.latitude, state.spot.geo.longitude);
            $(".meteolamer-map-search-input").val(state.spot.name);
        break;
        case "activeTime":
            updateComponents(state)
        break;
        case "activeWeekDay":
            updateComponents(state)
        break;
        case "spot":
            var spotName = state.spot.tags[0];
            getDataMeteolamer(spotName, state.startDate, state.endDate).then(function(spotData){
                state.set("data", spotData);
            })
            $(".meteolamer-map-search-input").val(state.spot.name)
        break;
    }
}

$(function(){
    var state = {
        startDate:moment().format("YYYY-MM-DD"),
        endDate:moment().add(6, 'days').format("YYYY-MM-DD"),
        spots:[],
        spot:null,
        data:[],
        activeWeekDay:0,
        activeTime:"00",
        set:function(key, value){
            state[key]=value;
            switch(key){
                case "spots":
                    if(state.spots && state.spots[0]){
                        state.spot = state.spots[0];
                        var spotName = state.spot.tags[0];

                        meteolamerMap = $('#meteolamerMapContainer').meteolamerMap({
                            spots:state.spots,
                            onClickSpot: (spot)=>{
                                state.set('spot', spot)
                            }
                        })

                        for(let i in state.spots){
                            state.spots[i].label = state.spots[i].name
                        }

                        getDataMeteolamer(spotName, state.startDate, state.endDate).then(function(spotData){
                            state.set("data", spotData);
                        })

                        initializeInputSearch(state)
                        bindEventDayNavigation(state)
                        bindEventTimeNavigation(state)
                    }
                break;
                case "data":
                    updateComponents(state)
                    meteolamerMap.coMap.centerOne(state.spot.geo.latitude, state.spot.geo.longitude);
                    $(".meteolamer-map-search-input").val(state.spot.name);
                break;
                case "activeTime":
                    updateComponents(state)
                break;
                case "activeWeekDay":
                    updateComponents(state)
                break;
                case "spot":
                    var spotName = state.spot.tags[0];
                    console.log(spotName);
                    getDataMeteolamer(spotName, state.startDate, state.endDate).then(function(spotData){
                        state.set("data", spotData);
                    })
                    $(".meteolamer-map-search-input").val(state.spot.name)
                break;
            }
        }
    }

    $.get("/costum/meteolamer/getspots/merge/true", function(res){
        var spots = [];
        Object.keys(res).forEach(function(key){
            var spot = res[key]
            spots.push({
                name: spot.label,
                tags: [spot.name],
                geo: {
                    latitude:spot.lat,
                    longitude:spot.lon
                }
            })
        })
        state.set("spots", spots)
    })
})