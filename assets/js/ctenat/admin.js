adminPanel.views.importctenat = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/importctenat/', {}, function(){},"html");
};

adminPanel.views.indicators=function(){
	var data={
		title : "Liste des indicateurs",
		table : {
            name: {
                name : "Nom",
                preview : true
            },
            actions: {
				name : "Domaine d'actions"
			},
			cibleDD : {
				name : "Cible DD"
			},
			type : {
		 			name : "Dispositif",
		 	}
        },
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "poi" ],
				filters : {
		 			type : "indicator"
		 		}
			}
		}
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.csv = [
			{
				url : baseUrl+'/co2/export/csv/',
				name : "Indicateurs",
				fields : [
			        "id",
			        "name",
			        "nameLong",
			        "type",
			        "echelleApplication",
					"definition",
					"domainAction",
					"methodeCalcul",
					"ObjectifDD"
    			]
			}
		];
		data.actions={
			update : {
				subType:"indicator"
			},
			delete : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.indecolos=function(){
	var data={
		title : "Liste des indicateurs d'états écologiques",
		table : {
            name: {
                name : "Nom",
                preview : true
            }
        },
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "poi" ],
				filters : {
		 			type : "indecolo"
		 		}
			}
		}
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.csv = [
			{
				url : baseUrl+'/co2/export/csv/',
				name : "Indicateurs",
				fields : [
			        "id",
			        "name",
			        "obj_environnemental",
			        "definition"
    			]
			}
		];
		data.actions={
			update : {
				subType:"indecolo"
			},
			delete : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};



function getFormMapping(data){
	var formsMappings = null;
	if(typeof costum.forms != "undefined"){
		$.each(costum.forms, function(kF, vF){
			if(typeof vF.mapping != "undefined")
				formsMappings = vF.mapping;
		});
	}
	mylog.log("MappingForm", formsMappings);
	if(formsMappings != null){
		data.paramsFilter.defaults.mapping = formsMappings;
	}

	return data;
}

var listFilterStatusAction = costum.listStatutAction; 
adminPanel.views.actionsCTE=function(){
	var data={
		title : "Gestion des actions",
		admin : true,
		table : {			
			actionsProject: {
				name : "Projet",
                sort : {
                	field : "answers.action.project.0.name",
                	up : true,
                	down : true
                }
			},
			idDispositif: {
				name : "ID Dispositif",
                sort : {
                	field : "answers.action.porteur.dispositifId",
                	up : true,
                	down : true
                }
			},
			dispositif: {
				name : "Dispositif",
                sort : {
                	field : "answers.action.porteur.dispositif",
                	up : true,
                	down : true
                }
			},
			actionsParent: {
				name : "Porteur",
                sort : {
                	field : "answers.action.parents.0.name",
                	up : true,
                	down : true
                }
			},
			actions: {
				name : "Domaine d'actions"
			},
			actionsCter: {
				name : "Dispositif",
				sort : {
                	field : "cterSlug",
                	up : true,
                	down : true
                }
			}, 
			region  : {
        		name : "Région",
            	class : "col-xs-1 text-center",
                sort : {
                	field : "address.level3Name",
                	up : true,
                	down : true
                }
        	},
        	departement : {
        		name : "Département",
            	class : "col-xs-1 text-center",
                sort : {
                	field : "address.level4Name",
                	up : true,
                	down : true
                }
        	},
			privateanswer :{
				name : "Privé",
				class : "col-xs-1 text-center",
				sort : {
                	field : "project.preferences.private",
                	up : true,
                	down : true
                }
			},
			statusAnswer : {
				name : "Statuts",
				class : "col-xs-1 text-center",
				sort : {
                	field : "priorisation",
                	up : true,
                	down : true
                }
			},
			comment : {
				name : "Commentaires",
				class : "col-xs-1 text-center"
			}
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "answers" ],
				filters : {}
			},
			filters : {
				text : true,
				dispositif : {
					view : "dropdownList",
					type : "filters",
					field : "answers.action.porteur.dispositif",
					name : "Dispositif",
					event : "selectList",
					keyValue : true,
					list : costum.lists.dispositif
				},
				statusAnswers : {
		 			view : "dropdownList",
		 			type : "filters",
		 			field : "priorisation",
		 			name : "Statuts",
		 			event : "selectList",
		 			list :  costum.listStatutAction
		 		},
		 		scopeList : {
		 			name : "Region",
		 			params : {
		 				countryCode : ["FR","RE","MQ","GP","GF","YT"], 
		 				level : ["3"],
		 				sortBy:"name"
		 			}
		 		},		 		
		 		departement:{
					view : "scopeList",
		 			name : "Département",
		 			params : {
		 				countryCode : ["FR","RE","MQ","GP","GF","YT"],
		 				level : ["4"],
		 				sortBy : "name"
		 			}
		 		}
		 	}
		}
	};
	data.paramsFilter.defaults.filters["answers.action.project.0"] = {
		'$exists' : 1 };

	// var formsMappings = null;
	// if(typeof costum.forms != "undefined"){
	// 	$.each(costum.forms, function(kF, vF){
	// 		if(typeof vF.mapping != "undefined")
	// 			formsMappings = vF.mapping;
	// 	});
	// }
	// mylog.log("MappingForm", formsMappings);
	// if(formsMappings != null){
	// 	data.paramsFilter.defaults.mapping = formsMappings;
	// }

	data = getFormMapping(data);
		
	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
		|| (typeof canEdit != "undefined" && canEdit) ){
		data.csv = [
			// {
			// 	url : baseUrl+'/costum/ctenat/answerscsv/slug/ctenat/admin/true'
			// },
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv',
				name : "Les Actions",
				defaults : {
					indexStep : 0
				}
			},
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv/typecsv/indicateurs',
				name : "Les Actions partie indicateurs",
				defaults : {
					indexStep : 0,
				}
			},
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv/typecsv/financements',
				name : "Les Actions partie financements",
				defaults : {
					indexStep : 0,
				}
			}
		];
		// data.pdf = {
		// 	urls : [
		// 		{
		// 			url : baseUrl+'/costum/ctenat/allanswers/slug/'+contextData.slug+"/admin/true"
		// 		} 
		// 	]
		// };
		data.actions={
			statusAnswer : {
				list : costum.listStatutAction
     //            list : [ 
					// "Action validée",
					// "Action en maturation",
					// "Action lauréate",
					// "Action refusée",
					// "Action Candidate"
     //            ]
            },
			privateanswer : true,
			statusAnswer : true,
			pdf : true,
			deleteAnswer : true,
			addNum : true
		};
	}

	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.candidatures = function(){
	//alert("candidatures");
		var data={
			title : "Gestion des actions",
			types : [ "answers" ],
			// id : contextData.id,
			// collection : contextData.type,
			// slug : contextData.slug,
			// url : baseUrl+'/costum/ctenat/answersadmin',
			table : {
				actionsProject: {
					name : "Projet"
				},
				actionsParent: {
					name : "Porteur"
				},
				orientations: {
					name : "orientations",
					list : cterOrientations
				},
				privateanswer :{
					name : "Privé",
					class : "col-xs-1 text-center"
				},
				statusAnswer : {
					name : "Statuts",
					class : "col-xs-1 text-center"
				},
				comment : {
					name : "Commentaires"
				}
				// ,
				// pdf : {
				// 	name : "PDF"
				// }
			},
			paramsFilter : {
				container : "#filterContainer",
				defaults : {
					types : [ "answers" ],
					fields : {}

				},
				filters : {
					text : true,
					statusAnswers : {
			 			view : "dropdownList",
			 			type : "filters",
			 			field : "priorisation",
			 			name : "Statuts",
			 			action : "filters",
			 			event : "selectList",
			 			list :  costum.listStatutAction
			 		}
				}
			}
		};

	data.paramsFilter.defaults.filters = {};
	data.paramsFilter.defaults.filters["context."+contextData.id] = {
		'$exists' : 1 
	}
	data.paramsFilter.defaults.filters["answers.action.project.0"] = {
		'$exists' : 1 };

	data = getFormMapping(data);

	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
		|| (typeof canEdit != "undefined" && canEdit) ){
		data.actions={
			statusAnswer : {
					list : costum.listStatutAction
            },
			addOrientations:true,
			privateanswer : true,
			statusAnswer : true,
			pdf : true,
			deleteAnswer: true
		};


		// data.csv = {
		// 	urls : [
		// 		{
		// 			url : baseUrl+'/costum/ctenat/answersctercsv/slug/'+contextData.slug+"/"
		// 		} 
		// 	]
		// };
		data.csv = [
			// {
			// 	url : baseUrl+'/costum/ctenat/answerscsv/slug/ctenat/admin/true'
			// },
			// {
			// 	url : baseUrl+'/co2/export/csvelement/type/answers/slug/ctenat/'
			// }
		
		 	{
		 		name : "Tableau de financement",
				defaults : {
					indexStep : 0
				},
		 		url : baseUrl+'/costum/ctenat/answersctercsv/slug/'+contextData.slug+'/id/'+contextData.id+"/type/"+contextData.type+"/"
			} ,
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv/id/'+contextData.id+"/type/"+contextData.type+"/",
				name : "Les Actions",
				defaults : {
					indexStep : 0
				}
			},
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv/typecsv/indicateurs/id/'+contextData.id+"/type/"+contextData.type+"/",
				name : "Les Actions avec les indicateurs",
				defaults : {
					indexStep : 0,
				}
			},
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv/typecsv/financements/id/'+contextData.id+"/type/"+contextData.type+"/",
				name : "Les Actions avec les financements",
				defaults : {
					indexStep : 0,
				}
			},
			{
				//url : baseUrl+'/co2/export/csv/',
				url : baseUrl+'/costum/ctenat/answerscsv/typecsv/indicateursfinancements/id/'+contextData.id+"/type/"+contextData.type+"/",
				name : "Les Actions avec les indicateurs et financements",
				defaults : {
					indexStep : 0,
				}
			}
		]

		data.pdf = [
			{
				url : baseUrl+'/costum/ctenat/allanswers/cterId/'+contextData.id+'/slug/ctenat',
				name : "Les Actions"
			}
/*			{
				url : baseUrl+'/costum/ctenat/allanswers/cterId/'+contextData.id+'/slug/ctenat/status/valid',
				name : "Les Actions validées"
			},
			{
				url : baseUrl+'/costum/ctenat/allanswers/cterId/'+contextData.id+'/slug/ctenat/status/maturing',
				name : "Les Actions en maturation "
			},*/
		];
		// data.table["statusAnswer"]={
		// 	name : "Statuts",
		// 	class : "col-xs-1"
		// };
	}
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/id/'+contextData.id+'/type/'+contextData.type, data, function(){},"html");
};
adminPanel.views.territory = function(){
	var data={
		title : "Gestion des Territoires",
		types : [ "projects" ],
		table : {
            name: {
                name : "Nom",
                sort : {
                	field : "name",
                	up : true,
                	down : true
                }
            },
            private : { 
            	name : "Privé",
        		class : "col-xs-1 text-center",
                sort : {
                	field : "preferences.private",
                	up : true,
                	down : true
                }
        	},
            dispositif : { 
            	name : "Dispositif",
        		class : "col-xs-1 text-center",
                sort : {
                	field : "dispositif",
                	up : true,
                	down : true
                }
        	},
            //pdf : { name : "PDF"}, 
        	region  : {
        		name : "Région",
            	class : "col-xs-2 text-center",
                sort : {
                	field : "address.level3Name",
                	up : true,
                	down : true
                }
        	},
        	departement : {
        		name : "Département",
            	class : "col-xs-2 text-center",
                sort : {
                	field : "address.level4Name",
                	up : true,
                	down : true
                }
        	}
        },
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				filters : {
		 			category : "cteR"
		 		},
				types : [ "projects" ],
				//private : true
			},
			filters : {
				text : true,
		 		dispositif : {
		 			view : "dropdownList",
		 			type : "filters",
		 			field : "dispositif",
		 			name : "Dispositif",
		 			event : "selectList",
		 			keyValue : true,
		 			list : costum.lists.dispositif
		 		},
				scopeList : {
		 			name : "Region",
		 			params : {
		 				countryCode : ["FR","RE","MQ","GP","GF","YT"],
		 				level : ["3"],
		 				sortBy : "name"
		 			}
		 		},
		 		departement:{
					view : "scopeList",
		 			name : "Département",
		 			params : {
		 				countryCode : ["FR","RE","MQ","GP","GF","YT"],
		 				level : ["4"],
		 				sortBy : "name"
		 			}
		 		}
			}
		}
	};

	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		// data.csv = {
		// 	project : {
		// 		key : "ctenat",
		// 		category : "cteR"
		// 	}
		// };
		data.csv = [
			// {
			// 	url : baseUrl+'/costum/ctenat/answerscsv/slug/ctenat/admin/true'
			// },
			{
				url : baseUrl+'/co2/export/csv/',
				defaults : {
					indexStep : 0,
					fields : [
						"name","scope", "session","why","filRouge","siren","nbHabitant","category","parent","address.level3Name","address.level4Name","source.status.ctenat","links", rolePorteurDispositif, "Partenaire", "dispositifId", "dispositif"
					],
					roles : [rolePorteurDispositif, "Partenaire"]
				}}
		];

		data.actions ={
			status : {
                list : [ 
					"Territoire Candidat",
					"Territoire Candidat refusé", 
					"Territoire lauréat", 
					"Dossier Territoire Complet",
					"Territoire Signé",
					"Territoire Archivé"
                ]
            },
            session : {
                list : costum.lists.sessionCte
            },
            //pdf : true,
            private : true,
            //aap : true,
			delete : true,

			sessionCte : true

		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};
adminPanel.views.strategyCter= function(){
	//alert("HZE");
	var data={
		title : "Gestion des orientations",
		table : {
			name: {
				name : "Nom",
				notLink : true
			},
			pdfOrientations: {
				name : "COPIL"
			}
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				filters : {
		 			category : "strategy"
		 		},
				types : [ "badges" ],
				//private : true
			},
			filters : {
				text : true
			}
		}
	};
	data.paramsFilter.defaults.filters = {};
	data.paramsFilter.defaults.filters["parent."+contextData.id] = {
		'$exists' : 1 
	}
	
	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin)
		|| (typeof canEdit != "undefined" && canEdit)){
		data["actions"]={
			updateBadges : true,
			pdfOrientations: true,
			delete : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/type/'+contextData.type+"/id/"+contextData.id, data, function(){},"html");
};

adminPanel.views.adminambitions = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/adminambitions/type/projects/id/'+contextData.id, {}, function(){},"html");
};

adminPanel.views.newRocketChat = function () {
	//alert("HZE");
	var data = {
		title: "Chat",
		id: contextData.id,
		collection: contextData.type,
		url: baseUrl + '/co2/settings/chatmanager/type/' + contextData.type + '/id/' + contextData.id,
	};
	ajaxPost('#content-view-admin', data.url, data, function () { }, "html");
};
adminDirectory.actions.updateBadges = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions.update", id, type, aObj);
	//mylog.log(e);
	mylog.log("adminDirectory.actions.update type", type);
	var str ='<button data-id="'+id+'" class="col-xs-12 updateStrategyBtn btn bg-green-k text-white"><i class="fa fa-pencil"></i> Modifier</button>';		
	return str ;
		
};
adminDirectory.actions.addOrientations = function(e, id, type, aObj){
	var str ='<button data-id="'+id+'" class="col-xs-12 updateStrategyFromProjectBtn btn"><i class="fa fa-tags"></i> modifier les orientations</button>';		
	
	return str;
};

adminPanel.views.users = function(){
	var data={
		title : "Gestion des utilisateurs-trices",
		//types : [ "citoyens" ],
		table : {
            name: {
                name : "Nom"
            },
            email : { 
            	name : "E-mail"
            }
        },
        paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "citoyens" ],
				fields : [ "name", "email", "collection" ],
				sort : { name : 1 }
			},
			filters : {
				text : true,
			}
		}
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			switchToUser : true,
			banUser : true,
			deleteUser : true
		};
	}
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.organizations = function(){
	var data={
		title : "Les organisations sur le CTE NAT",
		//types : [ "organizations" ],
		table : {
            name: {
                name : "Nom"
            },
            tags : { name : "Mots clés"}
        },
        paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "organizations" ],
				//private : true
			},
			filters : {
				text : true
			}
		},
        actions : {
        	//banUser : true,
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.community = function(){
    //alert("HZE");
    var data={
        title : "Membres",
        // id : costum.contextId,
        // collection : costum.contextType,
        // types : [ "citoyens"],
        context : {
        	id : costum.contextId,
        	collection : costum.contextType
        },
        invite : {
			contextId : costum.contextId,
			contextType : costum.contextType,
        },
        table : {
            name: {
                name : "Membres"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Validation pour être membres",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            }
        },
        paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "citoyens" ],
				fields : [ "name", "email", "links", "collection" ],
				notSourceKey : true
			},
			filters : {
				text : true
			}
		},
        actions : {
            admin : true,
            roles : true,
            //relaunchInviation : true,
            disconnect : true
        }
    };

    data.paramsFilter.defaults.filters = {};
	data.paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {
		'$exists' : 1 
	}
    //ajaxPost('#content-view-admin', data.url, data, function(){},"html");
    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.badges = function(){

	var data={
		title : "Etiquetages (badges)",
		//types : [ "badges"],
		table : {
			// type:{
			// 	name : "Type",
			// 	notLink : true
			// },
			name: {
                name : "Nom",
                preview : true
            },
			description:{
                name : "Description"
            },
            category:{
                name : "Categorie"
            },
            tags:{
                name : "Mots clés"
            }
            
			//pdf : true,
			//status : true
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "badges" ],
				//private : true
			},
			filters : {
				text : true,
				type : {
		 			view : "dropdownList",
		 			type : "filters",
		 			field : "category",
		 			name : "Catégorie",
		 			action : "filters",
		 			event : "filters",
		 			keyValue : false,
		 			list :  {"cibleDD" : "Développement durable", 
		 					"domainAction":"Domaine d'action", 
		 					"strategy":"Orientation stratégique"}
		 		}
				
			}
		},
	};

	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			update : true,
			delete : true
		};
	}

	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.communityCter = function(){
	//alert("HZE");
	var data={
		title : "Membres",
		// id : contextData.id,
		// collection : contextData.type,
		// types : [ "citoyens", "organizations"],
		context : {
        	id : contextData.id,
        	collection : contextData.type
        },
        invite : {
			contextId : contextData.id,
			contextType : contextData.type,
        },
		table : {
			name: {
				name : "Membres"
			},
			roles : {
				name : "Roles"
			},
			admin : true
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				//types : [ "citoyens", "organizations" ],
				types : [ "citoyens" ],
				fields : [ "name", "links", "collection" ],
				notSourceKey : true
			},
			filters : {
				text : true
			}
		}
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin
		|| (typeof canEdit != "undefined" && canEdit)){
		data.actions = {
			admin : true,
			roles : true,
			disconnect : true
		};
	}
    data.paramsFilter.defaults.filters = {};
	data.paramsFilter.defaults.filters["links.projects."+contextData.id] = {
		'$exists' : 1 
	};
	//ajaxPost('#content-view-admin', data.url, data, function(){},"html");
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/type/'+contextData.type+'/id/'+contextData.id, data, function(){},"html");
};




adminPanel.views.importindicateur = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/importindicateur/', {}, function(){},"html");
};

adminPanel.views.importorga = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/ctenat/importorga/', {}, function(){},"html");
};



var states = costum.listStatutAction;

// var states = {
// 	"Action validée" : {
// 		color : "green",
// 		icon : "fa-thumbs-up",
// 		lbl : "Action validée"
// 	},
// 	"Action en maturation" : {
// 		color : "#d8e54b",
// 		icon : " fa-hand-pointer-o",
// 		lbl : "Action en maturation"
// 	},
// 	"Action lauréate" : {
// 		color : "orange",
// 		icon : "fa-question-circle",
// 		lbl : "Action lauréate"
// 	},
// 	"Action refusée" : {
// 		color : "red",
// 		icon : "fa-times",
// 		lbl : "Action refusée"
// 	},
// 	"Action Candidate" : {
// 		color : "red",
// 		icon : "fa-times",
// 		lbl : "Action Candidate"
// 	}
// };
adminDirectory.bindCostum = function(aObj){
	mylog.log("adminDirectory.bindCostum ", aObj);

	$("#"+aObj.container+" .btn-remove-document").off().on("click",function(){
		//documentManager.delete($(this).data("id"),  callback);
		documentManager.delete($(this).data("id"));
	});

	$("#"+aObj.container+" .generate-pdf-orientation").off().on("click",function(){
			//$(this).css("pointer-events", "none");
			var domTragetPdfOrientation=$(this).data("id");
			ajaxPost(
		        null,
		      	baseUrl+"/costum/ctenat/generateorientation/id/"+$(this).data("id"),
		        {},
		        function(data){ 
					toastr.success("Le copil a été créé avec succès");
					var str=	"<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id='"+data.id+"'>"+
						"<a href='"+baseUrl+"/"+data.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ data.fileName+"</a>"+
						"<a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='"+data.id+"'><i class='fa fa-trash'></i></a>"+
					"</div>";
					$("#badges"+domTragetPdfOrientation+" .pdfOrientations").append(str);
						
					aObj.bindCostum(aObj);
		        }
		    ); 
		});

	$("#"+aObj.container+" .updateStrategyBtn").off().on("click", function(){
		costum.ctenat.strategy.openForm($(this).data("id"), contextData.id);
	});
	$("#"+aObj.container+" .updateStrategyFromProjectBtn").off().on("click", function(){
		var orientations={}
		var answerIdAdmin=$(this).data("id");
		var eltToUp=aObj.getElt(answerIdAdmin, "answers");
		if(typeof eltToUp.project != "undefined" && typeof eltToUp.project.links != "undefined" && typeof eltToUp.project.links.orientations != "undefined"){
			orientations=eltToUp.project.links.orientations;
			$.each(orientations, function(e, v){

				if(typeof aObj.panelAdmin.table.orientations.list != "undefined" && typeof aObj.panelAdmin.table.orientations.list[e] != "undefined"){
					if(typeof orientations.name == "undefined" && 
						typeof aObj.panelAdmin.table.orientations.list[e].name != "undefined")
						orientations[e].name=aObj.panelAdmin.table.orientations.list[e].name;
					if(typeof aObj.panelAdmin.table.orientations.list[e].profilThumbImageUrl != "undefined")
						orientations[e].profilThumbImageUrl=aObj.panelAdmin.table.orientations.list[e].profilThumbImageUrl;
				}else
					delete orientations[e];
			});
		}
		var dataSearch={filters:{}};
		dataSearch.filters["parent."+contextData.id]= {'$exists': 1};
		var form = {
			saveUrl : baseUrl+"/costum/ctenat/updateorientationscandidatures",
			dynForm : {
				jsonSchema : {
					title : "Modifier les orientations du projets",
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal #collection").val("projects");
							$("#ajax-modal #contextId").val(contextData.id);
						}
					},
					afterSave : function(data){
						mylog.dir(data);
						//aObj.setElt(data.project, answerIdAdmin,  "answers", "project");
						if(typeof aObj.results[answerIdAdmin].project.links == "undefined") aObj.results[answerIdAdmin].project["links"]={};
							aObj.results[answerIdAdmin].project.links=data.project.links;
						orientHtml="";
						$.each(data.orientations, function(e, v){
							orientHtml+="<span class='text-white badge elipsis margin-top-5'>"+v.name+"</span>";
						});
						if(!notEmpty(orientHtml))
							orientHtml="<span class='italic'>Aucune orientation rattachée</span>";
						$("#answers"+answerIdAdmin+" .orientations").html(orientHtml);
						dyFObj.closeForm();
					
					},
				/*	dyfbadge.beforeBuild.properties["linksProjects"] = {
                        
                    };
                    prepData = function(data){
                        if(typeof data.map.links != "undefined" && data.map.links.projects != "undefined"){
                            data.map.linksProjects=data.map.links.projects;
                            delete data.map.links.projects;
                        }
                        return data;
                    }*/
					properties : {
						//collection : dyFInputs.inputHidden(),
						id : dyFInputs.inputHidden(),
						contextId :dyFInputs.inputHidden(),
						path : dyFInputs.inputHidden(""),
						orientations : {
							inputType : "finder",
	                        label : "Orientations liées à l'action",
	                        //multiple : true,
	                        initMe:false,
	                        placeholder:"Quelles orientations correspondent à cette action ?",
	                        initContext:false,
	                        initContacts:false,
	                        rules : { required : false}, 
	                        initType: ["badges"],
	                        search : dataSearch,
	                        initBySearch : true
	                       // openSearch :true
	                    }
					}
				}
			}
		};

		var dataUpdate = {
			orientations : orientations,
			collection : eltToUp.project.collection,
			id : eltToUp.project._id.$id,
			contextId : contextData.id
		};
		dyFObj.openForm(form, "sub", dataUpdate);
		//dyFObj.editElement($(this).data("type"),$(this).data("id"), null,dyfForm);
	});
	$("#"+aObj.container+" .deleteAnswer").off().on("click", function(){
		var id = $(this).data("id");
		var type = $(this).data("type");

		bootbox.dialog({
		  title: "Confirmez la suppression de la fiche action",
		  message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
		  buttons: [
		    {
		      label: "Ok",
		      className: "btn btn-primary pull-left",
		      callback: function() {
		      	
		        ajaxPost("",baseUrl+"/survey/co/delete/id/"+id,null,function(data){
		        	if(typeof data.result == "undefined" || !data.result){
		        		toastr.error(data.msg);
		        	}else{
			        	toastr.success("La candidature a bien été supprimée");
			        	 $("#"+type+id).fadeOut();
                    
			        	//aObj.search.init(aObj);
			        }
		        },"html");
		      }
		    },
		    {
		      label: "Annuler",
		      className: "btn btn-default pull-left",
		      callback: function() {}
		    }
		  ]
		});
	});

	$("#"+aObj.container+" .statusBtn").off().on("click", function(){
		mylog.log("adminDirectory.bindCostum .statusBtn ", $(this).data("id"), $(this).data("type"));
		var id = $(this).data("id");
		var type = $(this).data("type");
		var elt = aObj.getElt(id, type) ;
		var listStatus = {};
		var statusElt = "" ;

		if( typeof aObj.panelAdmin.actions.status.list != "undefined" ){
			$.each(aObj.panelAdmin.actions.status.list, function(key, value){
				listStatus[value] = value;
			});
		}

		if(	typeof elt != "undefined" && 
			typeof elt.source != "undefined" && 
			typeof elt.source.status != "undefined" &&
			typeof costum != "undefined" && 
			typeof costum.slug != "undefined" &&
			typeof elt.source.status[costum.slug] != "undefined" &&
			elt.source.status[costum.slug] ){
			statusElt = elt.source.status[costum.slug];
		}
			

		var form = {
			saveUrl : baseUrl+"/costum/ctenat/updatestatuscter",
			dynForm : {
				jsonSchema : {
					title : "Modifier les Statuts",
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal #collection").val(type);
						}
					},
					afterSave : function(data){
						mylog.dir(data);
						dyFObj.closeForm();
						if(typeof data.project.source != "undefined" ){
							elt.source = data.project.source ;
						}
						aObj.setElt(elt, id, type) ;
						$("#"+type+id+" .status").html( aObj.values.status( elt, id, type, aObj));
					},
					properties : {
						//collection : dyFInputs.inputHidden(),
						id : dyFInputs.inputHidden(),
						path : dyFInputs.inputHidden(""),
						value : dyFInputs.inputSelect("Choisir un statut", "Choisir un statut", listStatus, {required : true}),
					}
				}
			}
		};

		var dataUpdate = {
			value : statusElt,
			collection : type,
			id : id,
			path : "source.status."+costum.slug
		};
		mylog.log("adminDirectory .statusBtn form", form);
		dyFObj.openForm(form, "sub", dataUpdate);
	});


	$("#"+aObj.container+" .prioritize").off().on("click", function(){
		mylog.log("adminDirectory .prioritize ", $(this).data("id"), $(this).data("type"));
		var id = $(this).data("id");
		var type = "answers";
		var elt = aObj.getElt(id, type) ;
		var listStatus = states;
		var statusElt = "" ;
		var answerId = $(this).data("id");
		var formId = $(this).data("formid");
		var userId = $(this).data("userid");
		var prioValue = elt.priorisation;

		if(	typeof elt != "undefined" && 
			typeof elt.source != "undefined" && 
			typeof elt.source.status != "undefined" &&
			typeof costum != "undefined" && 
			typeof costum.slug != "undefined" &&
			typeof elt.source.status[costum.slug] != "undefined" &&
			elt.source.status[costum.slug] ){
			statusElt = elt.source.status[costum.slug];
		}
			
		var dataUpdate = {

			value : statusElt,
			collection : type,
			id : id,
			formId : formId,
			answerId : answerId,
			answerSection : "priorisation" ,
			answers : prioValue,
			answerUser : userId 
		};

		var form = {
			saveUrl : baseUrl+"/costum/ctenat/prio",
			dynForm : {
				jsonSchema : {
					title : "Modifier les Statuts",
					icon : "fa-key",
					onLoads : {
						sub : function(){
							$("#ajax-modal #collection").val(type);
						}
					},
					afterSave : function(data){
						mylog.dir(data);
						dyFObj.closeForm();
						
						if(typeof data.answer.priorisation != "undefined")
							elt.priorisation=data.answer.priorisation;
						aObj.setElt(elt, answerId, "answers") ;
						$("#"+"answers"+answerId+" .statusAnswer").html(aObj.values.statusAnswer(elt, answerId, "answers", aObj));
						// dyFObj.closeForm();
					},
					properties : {
						//collection : dyFInputs.inputHidden(),
						id : dyFInputs.inputHidden(),
						formId : dyFInputs.inputHidden(""),
						answerId : dyFInputs.inputHidden(""),
						answerSection : dyFInputs.inputHidden(""),
						answers : dyFInputs.inputHidden(""),
						answers : dyFInputs.inputHidden(""),
						answers : dyFInputs.inputSelect("Choisir un statut", "Choisir un statut", listStatus, {required : true}),
					}
				}
			}
		};

		
		mylog.log("adminDirectory .statusBtn form", form);
		dyFObj.openForm(form, "sub", dataUpdate);
	});

	$("#"+aObj.container+" .addnumbtn").off().on("click", function(){
		mylog.log("adminDirectory .addnumbtn ", $(this).data("id"), $(this).data("type"));
		var id=$(this).data("id");
		var type="answers";
		var elt=aObj.getElt(id, type);
		numValue = {};

		var form = {
			dynForm : {
				jsonSchema : {
					title : "Modifier le numéro",
					icon : "fa-key",
					properties : {
						numAction : dyFInputs.inputText("Numéro d'action", "Saisir un numéro d'action", {required : true}, ""),
					},
					save : function () {
                        numValue.value = $("#numAction").val();
                        
                        if(typeof numValue.value == "undefined"){
                            toastr.error('value cannot be empty!');
                        }
                        else {
                            dataHelper.path2Value( numValue, function(params) {
                                if(typeof params.elt.numAction != "undefined" ){
                                	elt.numAction = params.elt.numAction ;
								}
								aObj.setElt(elt, id, type) ;
								$("#"+type+id+" .addNum").html( aObj.values.addNum(elt, id, type, aObj));
					
                                dyFObj.closeForm();
                               	//aObj.search.init(aObj);
                            } );
                        }

                    }
				}
			}
		};

		numValue.id = $(this).data("id");
        numValue.collection = "answers";       
        numValue.path = "numAction";
		
		dyFObj.openForm(form);
	});

	$("#"+aObj.container+" .sessionBtn").off().on("click", function(){
		mylog.log("adminDirectory.bindCostum .sessionBtn ", $(this).data("id"), $(this).data("type"));
		var id = $(this).data("id");
		var type = "projects";
		var listSession = {};
		var sessionCt = {};
		var sessionElt = "" ;
		var elt = aObj.getElt(id, type) ;

		if( typeof costum.lists.sessionCte != "undefined" ){
			$.each(costum.lists.sessionCte, function(key, value){
				listSession[value] = value;
			});
		}

		if(	typeof elt != "undefined" && 
			typeof elt.session != "undefined" ){
			sessionElt = elt.session;
		}			

		var form = {
			dynForm : {
				jsonSchema : {
					title : "Modifier le session",
					icon : "fa-key",
					save : function () {
                        sessionCt.value = $("#sessionSelect").val();
                        
                        if(typeof sessionCt.value == "undefined"){
                            toastr.error('value cannot be empty!');
                        }
                        else {
                            dataHelper.path2Value( sessionCt, function(params) {
                                dyFObj.closeForm();
                              	if(typeof params.elt.session != "undefined" ){
									elt.session = params.elt.session ;
								}
								aObj.setElt(elt, id, type) ;
								$("#"+type+id+" .session").html( aObj.values.session( elt, id, type, aObj));
					
                              	//aObj.search.init(aObj);
                            } );
                        }

                    },
					properties : {
						sessionSelect : dyFInputs.inputSelect("Choisir un session", "Choisir un session", listSession, {required : true}),
					}
				}
			}
		};
		sessionCt.id = $(this).data("id");
        sessionCt.collection = "projects";       
        sessionCt.path = "session";

		var dataUpdate = {
			sessionSelect : sessionElt
		};

		dyFObj.openForm(form, "sub", dataUpdate);
	});

	return str;
} ;

adminDirectory.values.statusAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values statusAnswer", id, type, aObj);
	//mylog.log(e);
	var col = "white";
	var icon = "fa-star";
	var str = "" ;
	if( typeof e.priorisation != "undefined" && typeof states[e.priorisation] != "undefined" ){
		col = states[e.priorisation].color;
		icon = states[e.priorisation].icon;
		str = e.priorisation ;
	}
	// var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
	// 			'data-formid="'+e.formId+'" '+
	// 			'data-userid="'+e.user+'" ' +
	// 			'class="prioritize  btn btn-default" style="background-color:'+col+'">'+ 
	// 			'<i class="fa fa-2x '+icon+'"></i>'+
	// 		'</a>';
	mylog.log("adminDirectory.values statusAnswer end", str);
	return str;
};


adminDirectory.values.orientations = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.orientations",e, id, type, aObj);
	//mylog.log(e);
	var col = "white";
	var str = "";
	if(typeof e.project != "undefined" && typeof e.project.links != "undefined" && typeof e.project.links.orientations != "undefined"){
		$.each(e.project.links.orientations, function(id, val){
			if(typeof aObj.panelAdmin.table.orientations.list != "undefined" && typeof aObj.panelAdmin.table.orientations.list[id] != "undefined")
				str+="<span class='text-white badge elipsis margin-top-5'>"+aObj.panelAdmin.table.orientations.list[id].name+"</span>";

		});
	}
	if(!notEmpty(str))
		str="<span class='italic'>Aucune orientation rattachée</span>";
	// var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
	// 			'data-formid="'+e.formId+'" '+
	// 			'data-userid="'+e.user+'" ' +
	// 			'class="prioritize  btn btn-default" style="background-color:'+col+'">'+ 
	// 			'<i class="fa fa-2x '+icon+'"></i>'+
	// 		'</a>';
	mylog.log("adminDirectory.values.orientations end", str);
	return str;
};

adminDirectory.values.pdfOrientations = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.pdfOrientations", id, type, aObj);
	//mylog.log(e);
	var str = "";
	if(typeof e.files != "undefined" && Object.keys(e.files).length > 0){
		$.each(e.files, function(kFile,vFile){
			str+=	"<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5' id='"+kFile+"'>"+
						"<a href='"+baseUrl+"/"+vFile.docPath+"' target='_blank' class='link-files'><i class='fa fa-file-pdf-o text-red'></i> "+ vFile.name+"</a>"+
						"<a href='javascript:;' class='pull-right text-red btn-remove-document' data-id='"+kFile+"'><i class='fa fa-trash'></i></a>"+
					"</div>";
		});
	}
	mylog.log("adminDirectory.values.pdfOrientations end", str);
	return str;
};

adminDirectory.actions.pdfOrientations = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions statusAnswer", id, type, aObj);
	//mylog.log(e);

	var str = '<a href="javascript:;" data-id="'+id+'" data-type="badge" '+
				'class="generatepdfbtnstyle generatepdfbtn generate-pdf-orientation col-xs-12 btn btn-default" ><i class="fa fa-file"></i> Générer un pdf'+
			'</a>';

	return str;
};





adminDirectory.values.session = function(e, id, type, aObj){
	mylog.log("adminDirectory.values sessionCte", id, type, aObj);
	//mylog.log(e);
	var str = "";
	if(typeof e.session != "undefined" ){
		str+="<span class=''>"+e.session+"</span>";
	}
	return str;
};



adminDirectory.values.addNum = function(e, id, type, aObj){
	mylog.log("adminDirectory.values num", e, id, type, aObj);
	var col = "white";
	var icon = "fa-star";
	var str = "" ;
	if( typeof e.numAction != "undefined"){
		str = '<span class="bold col-xs-12 text-center padding-10">'+e.numAction+'</span>' ;
	}
	// var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
	// 			'data-formid="'+e.formId+'" '+
	// 			'data-userid="'+e.user+'" ' +
	// 			'class="prioritize  btn btn-default" style="background-color:'+col+'">'+ 
	// 			'<i class="fa fa-2x '+icon+'"></i>'+
	// 		'</a>';
	mylog.log("adminDirectory.values nuuummmm end", str);
	return str;
};

adminDirectory.values.idDispositif = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.dispositifid", e, id, type, aObj);
	var col = "white";
	var icon = "fa-star";
	var str = "" ;
	if(typeof e.answers != "undefined" && typeof e.answers.action!= "undefined" && typeof e.answers.action.porteur != "undefined" && typeof e.answers.action.porteur.dispositifId != "undefined")
		str += '<span class="bold col-xs-12 text-center padding-10">'+e.answers.action.porteur.dispositifId+'</span>' ; 
	return str;
};

adminDirectory.values.dispositif = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.dispositif", e, id, type, aObj);
	var col = "white";
	var icon = "fa-star";
	var str = "" ;
	if( typeof e.answers != "undefined" && typeof e.answers.action!= "undefined" &&  typeof e.answers.action.porteur != "undefined" && typeof e.answers.action.porteur.dispositif != "undefined"){
		str += '<span class="bold col-xs-12 text-center padding-10">'+e.answers.action.porteur.dispositif+'</span>' ;
	} else if(typeof e.dispositif  != "undefined"){
		str += '<span class="bold col-xs-12 text-center padding-10">'+e.dispositif+'</span>' ;
	} 

	return str;
};

adminDirectory.actions.statusAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions statusAnswer", id, type, aObj);
	//mylog.log(e);

	var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
				'data-formid="'+e.formId+'" '+
				'data-userid="'+e.user+'" ' +
				'class="prioritize col-xs-12 btn btn-default" >Changer le statut'+ 
			'</a>';
	return str;
};

adminDirectory.actions.addNum = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions statusAnswer", id, type, aObj);
	//mylog.log(e);

	var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
				'data-formid="'+e.formId+'" '+
				'data-userid="'+e.user+'" ' +
				'class="addnumbtn col-xs-12 btn btn-default" >Mettre à jour le numéro d\'action'+ 
			'</a>';
	return str;
};

adminDirectory.actions.sessionCte = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions sessionBtn", e, id, type, aObj);

	var str = '<a href="javascript:;" data-id="'+id+'" id="prio'+id+'" '+
				'data-formid="'+e.formId+'" '+
				'data-userid="'+e.user+'" ' +
				'class="sessionBtn col-xs-12 btn btn-default" >Ajouter session'+ 
			'</a>';
	return str;
};

adminDirectory.actions.deleteAnswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions.deleteAnswer", id, type, aObj);
	//mylog.log(e);

	var str = '<a href="javascript:;" data-id="'+id+'" data-type="'+type+'" '+
				'class="deleteAnswer col-xs-12 btn btn-error" >Supprimer l\'action '+ 
			'</a>';
	return str;
};

adminDirectory.values.actions = function(e, id, type, aObj){
	mylog.log("adminDirectory.values actions", id, type, aObj);
	//mylog.log(e);
	var str = "";
	
	if( typeof e.formId != "undefined" &&
		typeof e.answers != "undefined" && 
		typeof e.answers[ e.formId ] != "undefined" &&
		typeof e.answers[ e.formId ].answers != "undefined" &&
		typeof e.answers[ e.formId ].answers.caracter != "undefined" ){
		

		if(typeof e.answers[ e.formId ].answers.caracter.actionPrincipal != "undefined"){
			str += e.answers[ e.formId ].answers.caracter.actionPrincipal;
		}

		if(typeof e.answers[ e.formId ].answers.caracter.actionSecondaire!= "undefined" &&
			e.answers[ e.formId ].answers.caracter.actionSecondaire.length > 0 ){
			$.each(e.answers[ e.formId ].answers.caracter.actionSecondaire, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
		
	}
	else if( typeof e.domainAction != "undefined" ) {
		if(typeof e.domainAction == "string"){
			str += e.domainAction;
		}else if(typeof e.domainAction != "undefined" &&
			e.domainAction.length > 0 ){
			$.each(e.domainAction, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
	}
	mylog.log("adminDirectory.values actions return", str);
	return str;
};

adminDirectory.values.actionsProject = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.actionsProject", id, type, aObj);
	//mylog.log(e);
	var str = "";
	if( typeof e.answers != "undefined" && 
		typeof e.answers.action != "undefined" &&
		typeof e.answers.action.project != "undefined" &&
		typeof e.answers.action.project[0] != "undefined" ){
		str = '<a href="#page.type.projects.id.'+e.answers.action.project[0].id+'" class="" target="_blank">'+e.answers.action.project[0].name+'</a>';
	}
	mylog.log("adminDirectory.values actions return", str);
	return str;
};

adminDirectory.values.actionsParent = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.actionsParent", id, type, aObj);
	//mylog.log(e);
	var str = "";
	if( typeof e.answers != "undefined" && 
		typeof e.answers.action != "undefined" &&
		typeof e.answers.action.parents != "undefined" &&
		typeof e.answers.action.parents[0] != "undefined" ){
		str = '<a href="#page.type.organizations.id.'+e.answers.action.parents[0].id+'" class="" target="_blank">'+e.answers.action.parents[0].name+'</a>';
	}
	mylog.log("adminDirectory.values actions return", str);
	return str;
};

adminDirectory.values.actionsCter = function(e, id, type, aObj){
	mylog.log("adminDirectory.values.actionsCter", id, type, aObj);
	//mylog.log(e);
	var str = "";
	if( typeof e.context != "undefined" ){
		$.each(e.context, function(k, v){
			if( k != costum.contextId )
				str = '<a href="#page.type.'+v.type+'.id.'+k+'" class="" target="_blank">'+v.name+'</a>';
		});
	}
	mylog.log("adminDirectory.values actions return", str);
	return str;
};

adminDirectory.values.cibleDD = function(e, id, type, aObj){
	mylog.log("adminDirectory.values cibleDD", id, type, aObj);
	//mylog.log(e);
	var str = "";
	
	if( typeof e.formId != "undefined" &&
		typeof e.answers != "undefined" && 
		typeof e.answers[ e.formId ] != "undefined" &&
		typeof e.answers[ e.formId ].answers != "undefined" &&
		typeof e.answers[ e.formId ].answers.caracter != "undefined" ){
		

		if(typeof e.answers[ e.formId ].answers.caracter.cibleDDPrincipal == "string"){
			str += e.answers[ e.formId ].answers.caracter.cibleDDPrincipal;
		}else if(typeof e.answers[ e.formId ].answers.caracter.cibleDDPrincipal!= "undefined" &&
			e.answers[ e.formId ].answers.caracter.cibleDDPrincipal.length > 0 ){
			$.each(e.answers[ e.formId ].answers.caracter.cibleDDPrincipal, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
		
	} 
	else if( typeof e.objectifDD != "undefined" ) {
		if(typeof e.objectifDD == "string"){
			str += e.objectifDD;
		}else if(typeof e.objectifDD != "undefined" &&
			e.objectifDD.length > 0 ){
			$.each(e.objectifDD, function(e,v){
				if(str != "")
					str += "<br>";
				str += v;
			});
		}
	}
	return str;
};


// adminDirectory.actions.aap = function(e, id, type, aObj){
// 	mylog.log("adminDirectory.values aap", id, type, aObj);
//mylog.log(e);
// 	var str = '<button data-slug="'+e.slug+'" class="ssmla generateAAPBtn btn btn-open-projects col-xs-12" data-view="generateAAP"><i class="fa fa-pencil"></i> Générer AAP</button>';

// 	return str;
// };

/*pageProfil.views.generateAAP = function(){
	//alert("Generating APP for "+contextData.slug);
	// ajaxPost('#central-container', baseUrl+"/survey/co/index/id/"+contextData.slug+"/pId/"+contextData.id+"/pType/"+contextData.type+"/copy/ficheAction",
	// 	null,
	// 	function(){},"html");
	window.open(baseUrl+"/survey/co/index/id/"+contextData.slug+"/copy/ficheAction");
};*/

adminDirectory.values.privateanswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.values privateanswer", id, type, aObj);
	//mylog.log(e);
	var str = "";
	if( typeof e.project != "undefined" && typeof e.project.id != "undefined"){
		if( typeof e.project.preferences != "undefined" && 
			typeof e.project.preferences.private != "undefined" && 
			e.project.preferences.private === true ){
			str = '<span id="private'+id+'" class="label label-danger"> Privé </span>';
		}else{
			str = '<span id="private'+id+'" class="label label-success"> Public </span>';
		}
	}
	return str;
};

adminDirectory.actions.privateanswer = function(e, id, type, aObj){
	mylog.log("adminDirectory.actions privateanswer", id, type);
	//mylog.log(e);
	var val = false ;
	var str = "&nbsp;";
	var labelAct="Rendre privé";
			
	if( typeof e.project != "undefined" && typeof e.project.id != "undefined"){
		if( typeof e.project.preferences != "undefined" && 
			typeof e.project.preferences.private != "undefined" && 
			e.project.preferences.private === true ){
			val = true;
			labelAct="Rendre public";
		}
		str ='<button data-parentid="'+id+'" data-parenttype="'+type+'" data-id="'+e.project.id+'" data-type="'+e.project.collection+'" data-private="'+val+'" data-path="preferences.private" class="col-xs-12 privateAnswerBtn btn bg-green-k text-white">'+labelAct+'</button>';
	}
	
	return str ;
};
