var formParentId=null;

function initCtenatSearchObj(){
	var paramsFilter= {
		container : "#filterContainer",
		header :{
			dom:".headerSearchProjects"
		},
		defaults : {
			types : [ "projects" ],
			filters : {
				category : "ficheAction"
		   },
		   indexStep : 0
	   },
	   results : {
			smartGrid : false,
			renderView : "directory.elementPanelHtmlFullWidth"
		},
		filters : {
			text : true,
			domainAction : {
				view : "dropdownList",
				type : "tags",
				name : "Domaine d'action",
				event : "tags",
				list : costum.lists.domainAction
			},
			cibleDD:{
				view : "dropdownList",
				type : "tags",
				name : "Objectif",
				event : "tags",
				list : costum.lists.cibleDD
			},
			scope : true
		}
	};	 

	if(contextData.category == "ficheAction")
		paramsFilter.urlData = baseUrl + "/costum/ctenat/getaction/id/"+contextData.id;
	else
		paramsFilter.defaults.filters["links.projects."+contextData.id] = {'$exists' :1};
	
	var filterSearch = searchObj.init(paramsFilter);
	filterSearch.search.init(filterSearch);
}

pageProfil.initCallB = function(){
	if(contextData.type=="projects"){
		rolesList=["Partenaire"];
		if(typeof contextData.category != "undefined" && contextData.category=="ficheAction")
			rolesList.push("Porteur d'action", "Financeur");
		if(typeof contextData.category != "undefined" && contextData.category=="cteR")
			rolesList.push(rolePorteurDispositif, "Financeur");
	}else if(contextData.type=="organizations"){
		rolesList=[roleReferent, "Salarié"];
	}
}
pageProfil.menuLeftShow = function(){
	if(!$(".menu-xs-only-top").is(":visible"))
		$(".menu-xs-only-top").show(200);
	else
		$(".menu-xs-only-top").hide(200);
};

pageProfil.views.home = function(){
	if(contextData.type=="projects" && typeof contextData.category != "undefined"){
		if(typeof contextData.category != "undefined" && contextData.category=="cteR")
			formParentId=contextData.id;
		ajaxPost(
			'#central-container', 
			baseUrl+'/costum/ctenat/elementhome/type/'+contextData.type+"/id/"+contextData.id, 
			null,
			function(){
				initCtenatSearchObj();
			},
			"html"
		);
	}else{
		pageProfil.views.detail();
	}
};

pageProfil.views.ficheAction = function(){
	formParentId=contextData.id;
	// // ajaxPost('#central-container', baseUrl+"/survey/co/new/id/"+contextData.slug+"/session/1",
	// // 	null,
	// // 	function(){},"html");
	// var idF = null;
	// if(typeof costum.forms != "undefined"){
	// 	$.each(costum.forms, function(kF, vF){
	// 		idF = kF;
	// 	});
	// }
	// // if(idF != null)
	// // 	ajaxPost('#central-container', baseUrl+"/survey/answer/index/id/new/form/"+idF+"/contextId/"+contextData.id+"/contextType/"+contextData.collection,
	// // 		null,
	// // 		function(){},"html");
	// if(idF != null)
	// 	ajaxPost('#central-container', baseUrl+"/survey/answer/index/id/new/form/"+idF+"/contextId/"+contextData.id+"/contextType/"+contextData.collection,
	// 		null,
	// 		function(){},"html");
	if(typeof costum.forms != "undefined" && Object.keys(costum.forms)[0] != null){
		// ajaxPost('#central-container', baseUrl+"/survey/answer/new/id/"+costum.contextId+"/contextId/"+contextData.id+"/contextType/"+contextData.collection+"/tpl/costum.views.custom.ctenat.home_new_answer",
		// 	null,
		// 	function(){},"html");
		pageProfil.form = formObj.init({});
		pageProfil.form.urls.answer(pageProfil.form, "new", Object.keys(costum.forms)[0], null, contextData.id, contextData.collection);
	}
};

pageProfil.views.answers= function(){
	if(typeof contextData.links != "undefined" && typeof contextData.links.projects != "undefined")
		formParentId=Object.keys(contextData.links.projects)[0];
	if(typeof contextData.links != "undefined" && typeof contextData.links.answers != "undefined"){
		// ajaxPost('#central-container', baseUrl+"/survey/answer/index/id/"+Object.keys(contextData.links.answers)[0]+"/mode/w",
		// 	null,
		// 	function(){
		// 		//alert("pageProfil.params.subview : "+pageProfil.params.subview)
		// 		if(pageProfil.params.subview && $("#"+pageProfil.params.subview).length )
		// 			$("#"+pageProfil.params.subview).removeClass("hide");
		// 		else 
		// 			$("#explain").removeClass("hide");
		// 	},"html");
		pageProfil.form = formObj.init({});
		mylog.log("pageProfil.views.answers ", Object.keys(contextData.links.answers)[0]);
		if(notEmpty(contextId) && notEmpty(contextType))
			pageProfil.form.urls.answer(pageProfil.form, Object.keys(contextData.links.answers)[0], null , "w",contextId,contextType);
		else
			pageProfil.form.urls.answer(pageProfil.form, Object.keys(contextData.links.answers)[0], null , "w");
	}else if(notEmpty(pageProfil.params.dir)){
		ajaxPost('#central-container', baseUrl+"/survey/co/answer/id/"+pageProfil.params.dir+"/view/answerEdit",
			null,
			function(){},"html");
	}else{
		$('#central-container').html( "<span class='alert alert-warning col-xs-12 padding-20 text-center'>"+
			"Désolé, nous n'avons trouvé aucune candidature correspondante à ce projet<br/>"+
			"Veuillez-vous rapprocher des administrateurs du site afin que l'on puisse régler le problème"+
		"</span>");
	}
};
pageProfil.views.ambitions= function(){
	ajaxPost('#central-container', baseUrl+'/costum/ctenat/ambitions/type/projects/id/'+contextData.id, 
		null,
		function(){				
		},"html");
};
pageProfil.views.projects= function(){
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/app/search/page/projects', 
		null,
		function(){
			$("#filters-nav").empty();
			$("#central-container").prepend("<div id='filterContainer' class='searchObjCSS'></div><div  class='headerSearchProjects'></div>");
			initCtenatSearchObj()			
		},"html");
};

//pageProfil.projectsOthers
pageProfil.views.dashboard= function(){
	ajaxPost('#central-container', baseUrl+'/costum/ctenat/dashboard/slug/'+contextData.slug, 
					null,
					function(){},"html");
};

pageProfil.views.actionDash= function(){
	ajaxPost('#central-container', baseUrl+'/costum/ctenat/dashboard/action/'+contextData.slug, 
					null,
					function(){},"html");
};
pageProfil.views.gallery= function(){
	pageProfil.params.dir="file";
	var url = "gallery/index/type/"+contextData.collection+"/id/"+contextData.id+"/docType/file";
	if(notNull(pageProfil.params.key))
		url+="/contentKey/"+pageProfil.params.key;

	if(notNull(pageProfil.params.folderId))
		url+="/folderId/"+pageProfil.params.folderId;
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
		null,
		function(){
			$(".dropdown-add-file").html(
				'<a class="btn show-nav-add" href="javascript:dyFObj.openForm(\'addFile\')"><i class="fa fa-plus-circle"> Ajouter des fichiers</a>');
		});
};
	
pageProfil.views.adminCter = function(){
	formParentId=contextData.id;
	authorizedSubview=["candidatures", "communityCter", "adminambitions", "newRocketChat", "strategyCter"];
	subUrl="";
	if(typeof pageProfil != "undefined" && typeof pageProfil.params.subview != "undefined"){
		if($.inArray(pageProfil.params.subview, authorizedSubview) >=0)
			subUrl=(typeof pageProfil != "undefined" && typeof pageProfil.params.subview != "undefined") ? "/subview/"+pageProfil.params.subview : "";
	}
	subUrl+="/contextType/"+contextData.type+"/contextId/"+contextData.id;
	$("#central-container").hide().parent().append("<div class='simulate-central-container col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 shadow2'></div>");
	coInterface.showLoader(".simulate-central-container");
	ajaxPost('#central-container', baseUrl+"/"+moduleId+"/app/admin"+subUrl,
		{
			subView : true,
			options : { "menu" : false, addButton : false}
		},
		function(){
			str='<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin col-xs-12 col-sm-8 col-sm-offset-2 btn-cter-create" data-view="candidatures" style="cursor:pointer;">'+
							'<i class="fa fa-tasks"></i> Gestion des actions'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin col-xs-12 col-sm-8 col-sm-offset-2 btn-cter-create" data-view="communityCter" style="cursor:pointer;">'+
							'<i class="fa fa-group"></i> Membres'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin col-xs-12 col-sm-8 col-sm-offset-2 btn-cter-create" data-view="strategyCter" style="cursor:pointer;">'+
							'<i class="fa fa-tags"></i> Gestion des orientations'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin col-xs-12 col-sm-8 col-sm-offset-2 btn-cter-create" data-view="adminambitions" style="cursor:pointer;">'+
							'<i class="fa fa-heartbeat"></i> État des lieux écologique'+
					'</a>'+
				'</li>'+
				'<li class="text-center list-group-item col-xs-12  col-sm-offset-1 col-sm-10">'+
					'<a href="javascript:;" class="btnNavAdmin col-xs-12 col-sm-8 col-sm-offset-2 btn-cter-create" data-view="newRocketChat" style="cursor:pointer;">'+
							'<i class="fa fa-plus-circle"></i> Créer un nouveau canal de chat personnalisé'+
					'</a>'+
				'</li>';
			
			$(".menuAdmin").html(str);
			//copyFicheActionFrom=false;
			strExplain="<div class='col-xs-12 no-padding text-center margin-10'>"+
				'<a href="#forum" class="lbh"> Pour poser vos questions, rendez vous sur le forum <i class="fa fa-info-circle fa-2x"></i></a><br/>'+
			"</div>";

			strMenu="";
			if(canEdit){
				if(contextData.dispositif != "CRTE"){
					// if(typeof contextData.duplicateCRTE != "undefined"){
					// 	strExplain="<div class='col-xs-12 alert alert-success'>"+
					// 		"<span class='col-xs-12'>Le CTE a été dupliqué en CRTE</span>"+
					// 		"<a href='#page.type.projects.id."+contextData.duplicateCRTE+"' target='_blank' class='col-xs-8 col-xs-offset-2 btn btn-success margin-top-20'>Retrouver le CRTE</a>"+
					// 	"</div>";
					// }else{
	            		strExplain+="<div class='col-xs-12 alert alert-success'>"+
							"<span class='col-xs-12'>Vous pouvez désormais passer <br/>du dispositif CTE <br/>à celui du CRTE</span>"+
							"<button class='col-xs-8 col-xs-offset-2 btn btn-success bindMoveToCRTE margin-top-20'>C'EST ICI !!</button>"+
						"</div>";
					//}
				}
				/*if(notEmpty(contextData.links) 
				&& typeof contextData.links.forms != "undefined"){
					$.each(contextData.links.forms, function(e,v){
						if(typeof v.copyForm != "undefined" && v.copyForm=="ficheAction")
							copyFicheActionFrom=true;
					});
				}*/
				//copyFicheActionFrom=true;
				//if(copyFicheActionFrom){
					isOnAAP=(typeof contextData.showAAP != "undefined" && contextData.showAAP) ? "checked" : ""; 
					strMenu+="<div class='col-xs-12 no-padding text-center'>"+
						"<span class='col-xs-12'>"+
							"<i class='fa fa-info-circle'></i> Rendre public à tous sur votre accueil le formulaire d'appel à projet"+
						"</span>"+
						'<div class="col-xs-12 no-padding activeAAP">'+
							'<input id="" type="checkbox" data-off-text="Off" data-on-text="On" class="BSswitch" '+isOnAAP+'>'+
						'</div>'+
			
						//'<input id="" type="checkbox" data-off-text="Désactivé" data-on-text="Activé" data-sub="invite" checked="false" class="activeAAP">'+
			
						"</div>";
				//}
				//strExplain+=(copyFicheActionFrom) ? "Activer l'appel à projet" :"";
					strMenu+='<div class="col-xs-12 margin-top-20 margin-bottom-20 text-center">';
					//if(copyFicheActionFrom)
						strMenu+='<button class="ssmla btn col-xs-12 col-sm-8 col-sm-offset-2 bg-blue btn-cter-create" data-view="ficheAction"><i class="fa fa-plus-circle"></i> Ajouter une action</button>';
					
					strMenu+='<button class="btn-add-organization btn-cter-create btn col-xs-12 bg-blue col-sm-8 col-sm-offset-2 margin-top-10"><i class="fa fa-plus-circle"></i> Ajouter une organisation</button>';
					strMenu+='<button class="btn-cter-create btn col-xs-12 bg-blue col-sm-8 col-sm-offset-2 margin-top-10" onclick="dyFObj.openForm(\'event\');"><i class="fa fa-plus-circle"></i> Ajouter un événement</button>';
					strMenu+='<button class="btn-add-badge btn-cter-create btn col-xs-12 bg-blue col-sm-8 col-sm-offset-2 margin-top-10"><i class="fa fa-plus-circle"></i> Ajouter une orientation</button>';
					if(contextData.copyOf)
						strMenu+='<button class="btn-copy-orientation btn-cter-create btn col-xs-12 bg-blue col-sm-8 col-sm-offset-2 margin-top-10"><i class="fa fa-plus-circle"></i> Copier les orientations CTE</button>';

				strMenu+='</div>';
			}

			$(".infoPanelAddContent").html(strExplain);
			$(".contain-admin-add").html(strMenu);
			$("#central-container").show();
			$(".simulate-central-container").remove();
			$(".activeAAP .BSswitch").bootstrapSwitch();
		   	$(".activeAAP .BSswitch").on("switchChange.bootstrapSwitch", function (event, state) {
		   		dataHelper.path2Value( {
                collection : contextData.type,
                id : contextData.id,
                path : "showAAP",
                value : state
            	}, function(data){ 
            		contextData.showAAP=data.elt.showAAP; 
            		msg=(typeof data.elt.showAAP != "undefined" && data.elt.showAAP) ? "L'appel à projet est désormais activé sur la page d'accueil" : "L'appel à projet est désormais désactivé";
            		toastr.success(msg)
            	});
		    	//settings.savePreferencesNotification("notifications",state, "citoyens", userId, $(this).data("sub"));
		    });

		   	$(".bindMoveToCRTE").off().on("click", function(){
		   		
		   		var crteOptions = "<option value=''>Choisissez un CRTE</option>";
				cotmp.crteList = {};	

		   		$.each(costum.lists.crteAnct,function(i,c){
                    if(jsonHelper.notNull("elementData.address.postalCode")){
                    	if(c.departement.indexOf(";")){
                    		$.each(c.departement.split(';'),function(ii,depi){
	                    		if( ( depi == 3 && elementData.address.postalCode.substring(0,3) == depi ) ||
	                    	  		elementData.address.postalCode.substring(0,2) == depi ){
		                    		crteOptions += "<option value='"+c.code+"'>"+c.name+"</option>";
		                    		cotmp.crteList[c.code] = {name:c.name};
		                    	}
	                    	});
                    	}
                    	else if( ( c.departement.length == 3 && elementData.address.postalCode.substring(0,3) == c.departement ) ||
                    	  elementData.address.postalCode.substring(0,2) == c.departement ){
                    	crteOptions += "<option value='"+c.code+"'>"+c.name+"</option>";
                    	cotmp.crteList[c.code] = {name:c.name};
                    	}
                	}
                });
                var crteInputs = "";
                if(Object.keys(cotmp.crteList).length > 0){
                	crteInputs = '<label class="col-md-12 no-padding" for="nameCRTE">Nom du CRTE : </label><br> ' +                        
			                          '<select id="crteSelect" style="font-size: 20px;margin-bottom: 10px;" >'+
			                          	crteOptions+
			                          "</select>"+
			                          '<br>'+'<br>';
                }
                else {
                	crteInputs = '<label class="col-md-12 no-padding text-red" for="nameCRTE">aucun CRTE dans la base ANCT</label><br> ' +  
                				  '<label class="col-md-12 no-padding" for="nameCRTE">Nom du CRTE : </label><br> ' +                        
		                          '<input type="text" id="nameCRTE" class="nameSearch wysiwygInput form-control" value="'+elementData.name+'" style="width: 100%" placeholder="Nom du CRTE..."></input>'+
		                          '<br>'+
		                          
		                          '<label class="col-md-12 no-padding" for="dispositifId">Identifiant unique CRTE : </label><br> ' +                        
		                          '<input type="text" id="dispositifId" class="nameSearch wysiwygInput form-control" value="" style="width: 100%" placeholder="crte-11-77-12..."></input>'+
		                          '<br>';
                }	
		   		var message = "<span class='warning'>Veuillez sélectionner dans la liste nationale des CRTE le nom du CRTE sur lequel vous souhaitez dupliquer les actions du CTE. En cas de périmètres distincts entre le CTE et le CRTE, il vous est possible de dupliquer le CTE sur chacun des CRTE correspondants.</span><br>";
					var boxBookmark = bootbox.dialog({
				            title: message,
				            message: '<div class="row">  ' +
			                      '<div class="col-md-12"> ' +
			                        '<form class="form-horizontal"> ' +
			                          
			                          crteInputs+
                
			                       /* '<label class="col-xs-12 no-padding" for="duplicate">Voulez-vous passer le CTE totalement en CRTE</label> ' +
			                        '<span class="col-xs-12 no-padding italic">Si oui, le cte n\'existera plus et deviendra le CRTE<br/>Si non, l\'ensemble du CTE sera dupliqué pour créer une nouvelle entité CRTE </span> ' +
			                        '<div class="col-md-4 col-sm-4"> <div class="radio no-padding"> <label for="duplicate-0"> ' +
			                          '<input type="radio" name="duplicate" id="duplicate-0" value="false"  checked="checked"> ' +
			                          tradDynForm.yes+' </label> ' +
			                          '</div><div class="radio no-padding"> <label for="duplicate-1"> ' +
			                          '<input type="radio" name="duplicate" id="duplicate-1" value="true"> '+tradDynForm.no+' </label> ' +
			                          '</div> ' +*/
			                    '</div> </div>' +
			                    '</form></div></div>',
				            buttons: {
				                success: {
			                    label: trad.save,
			                    className: "btn-primary",
			                    callback: function () {
			                    	var crteName = "";
			                    	var dispositifId = "";
			                    	if( Object.keys(cotmp.crteList).length > 0 && $("#crteSelect").val() == "" ){
			                    		toastr.error("Veuillez choisir un CRTE");
			                    	} else if( Object.keys(cotmp.crteList).length == 0 ){
			                    		crteName = $("#nameCRTE").val();
				                        dispositifId = $("#dispositifId").val();
			                    	}
			                    	else {
				                        crteName = cotmp.crteList[ $("#crteSelect").val() ].name;
				                        dispositifId = $("#crteSelect").val();
				                    }

				                    if( crteName != "" && dispositifId != "" )
				                    {
				                    	var formData = {
				                          cteId : contextData.id ,
				                          duplicate : true,//$("input[name='duplicate']:checked").val() ,
				                          name : crteName ,
				                          dispositifId : dispositifId ,
				                          dispositifType : "CRTE" 
				                        }
				                        mylog.log("bindMoveToCRTE",formData);
				                        ajaxPost( null, baseUrl+"/costum/ctenat/updatectetocrte", formData,function(data){ 
									        	mylog.log(data);
											    if(data.result){
											    	var succesmsg="Félicitations !!<br/>";
											    	if(data.newCRTE)
											    		succesmsg += "Le CRTE a bien été créé avec ses actions dupliquées<br/>Vous avez été redirigé sur la page du CRTE";
											    	else
											    		succesmsg += "Le CTE actuel est bien passé en CRTE<br/>La page a été rafraichie";
											    	urlCtrl.loadByHash(data.hash);
													var successModalCRTE = '<div class="portfolio-modal modal fade text-center py-5"  id="successModalCRTE" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
																	    '<div class="modal-dialog modal-md alert alert-success" role="document">'+
																	        '<div class="modal-content" style="background:transparent;">'+
																	        	'<div class="close-modal" data-dismiss="modal">'+
																                    '<div class="lr">'+
																                        '<div class="rl">'+
																                        '</div>'+
																                    '</div>'+
																                '</div>'+
																	            '<div class="modal-body" style="font-size: 22px">'+
																	                '<div id="text-head-collectif" class="justify-marge">'+succesmsg+'</div>'+
																	            '</div>'+
																	        '</div>'+
																	    '</div>'+
																	'</div>';
													$(".main-container").append(successModalCRTE);
													$("#successModalCRTE").modal("show");
					                            }
					                            else{
					                              
					                                toastr.error(data.msg);
					                            }
									  		} ); 
			                    	}
			                	}
			                },
			                cancel: {
								label: trad.cancel,
								className: "btn-secondary",
								callback: function() {
								}
			                }
			            }
					});
		   	});
			adminPanel.params.hashUrl="#@"+contextData.slug+".view.adminCter";
			$(".btn-add-organization").click(function(){
				//var dyfbadge=jQuery.extend(true, {}, typeObj.badge.dynFormCostum);
				//var dyfbadge=typeObj['badge']["dynFormCostum"]);
				var dyforga={
					"beforeBuild":{
						"properties":{
							"roles" : {
								"inputType" : "tags",
								"label":tradDynForm["addroles"],
								"placeholder":tradDynForm["addroles"],
								"values":rolesList
							},
							"links[projects]":{
								"inputType" : "hidden"
							}
						}
					},
					 "onload" : {
                        "actions" : {
                        	"html":{
                        		"infocustom" : "<p style='font-size: 20px;'><i class='fa fa-info-circle'></i> Si l'organisation existe déjà, invitez depuis le bouton 'inviter des contributeurs' disponible dans le bandeau du territoire</p>"
                        	},
                            "presetValue" : {
                                "role" : "admin",
                                "links[projects]" : {
                                    "eval" : "contextData.id"
                                }
                            },
                            "hide" : {
                                "urltext" : 1,
                                "tagstags" : 1,
                                "parentfinder" : 1,
                                "categoryselect":1,
                                "publiccheckboxSimple" : 1,
                                "roleselect" : 1
                            }
                         }
                    }
				};
				//return false;
				dyFObj.openForm('organization', null, null,null,dyforga);
			});

			$(".btn-copy-orientation").click(function(){
				bootbox.dialog({
		            title: "ATTENTION",
		            message: 'Le CRTE ne comportera pas automatiquement les données des orientations issues du CTE car les données des orientations ne sont pas modifiables de manière indépendante entre CRTE et CTE. Toute modification de l’orientation dans le CRTE conduira à modifier le fichier source dans le CTE.Si toutefois les orientations du CTE et du CRTE demeurent dans un cadre de gouvernance identique et de continuité entre les deux dispositifs, vous avez la possibilité d’effectuer cette duplication des orientations en sélectionnant dans le portail d\'administration de l\’espace territoire en sélection le bouton: “Copier les orientations CTE”.',
		            buttons: {
						success: {
		                    label: "OK",
		                    className: "btn-primary",
		                    callback: function () {
		                    	ajaxPost('#central-container', baseUrl+'/costum/ctenat/copyorientations/type/projects/id/'+contextData.id, 
									null,function(){
										urlCtrl.loadByHash("#@"+contextData.slug+".view.ambitions");
										toastr.success('Les données des orientations stratégiques du CTE sont désormais liées à celles du CRTE');
									},"html");
		                    }
		                },
					    cancel: {
							label: trad.cancel,
							className: "btn-secondary",
							callback: function() {
								$('.bootbox').remove();
							}
		                }
		            }
		        })
				
			})
			
			
			$(".btn-add-badge").click(function(){
				costum.ctenat.strategy.openForm();
				/*var dyfbadge=jQuery.extend(true, {}, typeObj.badge.dynFormCostum);
				//var dyfbadge=typeObj['badge']["dynFormCostum"]);
				dyfbadge.beforeBuild.properties.parent = {
					inputType : "finder",
					label : "Porteur de l'orientation stratégique",
					//multiple : true,
	                initMe:false,
	                placeholder:"Rechercher le porteur de l'orientation ?",
					rules : { required : false, lengthMin:[1, "parent"]}, 
					initType: ["projects"],
					openSearch :true
				};
				dyfbadge.beforeBuild.properties.name.label="Titre de l'orientation stratégique";
				dyfbadge.beforeBuild.properties.description={"label":"Principaux enjeux et objectifs de l’orientation"};
				dyfbadge.beforeBuild.properties.synergie = {
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "Synergie et articulation avec d’autres démarches territoriales",
                    "rules" : {
                        "maxlength" : 2000
                    }
                }; 
				dyfbadge.onload.actions.hide["categoryselect"]=1;
				dyfbadge.onload.actions.hide["infocustom"]=1;
				dyfbadge.onload.actions.presetValue["category"]="strategy";
				
				//return false;
				dyFObj.openForm('badge', null, null,null,dyfbadge);*/
			});
			adminPanel.bindViewActionEvent();
			pageProfil.bindViewActionEvent();
			coInterface.bindLBHLinks();
		},"html");
};
	
pageProfil.actions.generateAAP = function(){
	ajaxPost(
        null,
       	baseUrl+"/survey/co/index/id/"+contextData.slug+"/copy/ficheAction",
        {},
        function(data){ 
            toastr.success("L'appel à projet a été généré avec succès");
			urlCtrl.loadByHash(location.hash);
        }
    );
}