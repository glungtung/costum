//$("#filter-scopes-menu").remove();
/*
"links" : {
                            "order" : 6,
                            "inputType" : "finder",
                            "label" : "Nom des élus porteurs de la candidature ?",
                            "multiple" : true,
                            "invite" : true,
                            "type" : [ 
                                "persons"
                            ],
                            "roles" : [ 
                                "Elu"
                            ],
                            "openSearch" : true
                        },
                        ,
        "#slide" :{
            "inMenu" : true,
            "hash" : "#ctenat.slide",
            "icon" : "desktop",
            "subdomainName" : "Presentation",
            "module":"costum"
        }
                        
*/
var rolePorteurDispositif="Porteur du dispositif";
var roleReferent="Référent";

$("#mainNav .btn-show-map").html("Carte <i class='fa fa-angle-double-right'></i>");
costum.ctenat={
	init : function(){
		mylog.log("CTENAT INDEXJS");
		if($.inArray(location.hash,["#live", "#documentation", "#forum", "#press"]) >= 0 
			&& (typeof costum.isMember == "undefined"
				|| !costum.isMember)){	
			urlCtrl.loadByHash("");
			bootbox.dialog({message:'<div class="alert-danger text-center"><strong>Vous n\'êtes pas autorisé.e à accéder à cet espace de l\'application</strong></div>'});
			//return false;
		}
		if(typeof costum.rolesCTE != "undefined"){
			costum.ctenat.floopInit();
		}
		emojiconReactions=[
	         {"emocion":"love","TextoEmocion":trad.ilove, "class" : "amo", "color": "text-red" },
	         {"emocion":"bothered","TextoEmocion":trad.bothering,"class" : "molesto", "color": "text-orange"},
	         {"emocion":"scared","TextoEmocion":trad.what, "class" : "asusta", "color": "text-purple"},
	         {"emocion":"enjoy","TextoEmocion":trad.toofunny, "class" : "divierte","color": "text-orange"},
	         {"emocion":"like","TextoEmocion":"+1", "class" : "gusta", "color": "text-red"},
	         {"emocion":"sad","TextoEmocion":trad.sad, "class" : "triste", "color": "text-azure"},
	         {"emocion":"glad","TextoEmocion":trad.cool, "class" : "alegre", "color":"text-orange"}
	    ];
	},
	floopInit : function(){
		htmlFloop="";
		appendBadge=false;
		addBadge="<span class='badge-roles-ctenat faa-pulse animated'><i class='fa fa-id-badge'></i></span>";
		$.each(costum.rolesCTE, function(e,v){
			if(v==true || ( typeof v == "object" && Object.keys(v).length >0)){
				appendBadge=true;return true;
			}
		});
		if(appendBadge){
			$(".menu-name-profil").removeClass("lbh").addClass("open-modal-ctenat margin-right-10").append(addBadge).find(".tooltips-top-menu-btn").text("Tableau de bord");
			$("body").append(
				"<div class='dashboard-cte modal' style='display:none; overflow-y:scroll;background-color: rgba(0,0,0,0.9);z-index: 100000;position:fixed;top:"+$("#mainNav").outerHeight()+"px;left:"+$("#menuApp.menuLeft").width()+"px;'>"+
				"</div>");
			ajaxPost(".dashboard-cte", baseUrl+"/costum/ctenat/dashboardaccount",{roles:costum.rolesCTE}, function(){}, "html");
			 $(".open-modal-ctenat").off().on('click',function(){
                $(".dashboard-cte").show(200);
            });
			setTimeout(function(){ $(".badge-roles-ctenat").removeClass("faa-pulse animated")}, 8000);
		}
	},
	badges : {
		formData : function(data){
			if(typeof data.linksProjects){
				if(typeof data.links == "undefined") data.links={};
				data.links.projects=data.linksProjects;
				delete data.linksProjects;
				
				
			}
			return data;
		},
		afterSave : function(data){
			if(dyFObj.editMode){
				
				if($.inArray(data.map.category, ["cibleDD", "domainAction"]) >= 0){
					oldName=costum.badges[data.map.category][data.id].name;
					if(data.map.name!=oldName){
						if(typeof costum.badges[data.map.category][data.id].parent != "undefined"){
							parentNameB=costum.badges[data.map.category][Object.keys(costum.badges[data.map.category][data.id].parent)[0]].name;
							costum.lists[data.map.category][parentNameB].splice(costum.lists[data.map.category][parentNameB].indexOf(oldName), 1,data.map.name);
						}else{
							costum.lists[data.map.category][data.map.name]=costum.lists[data.map.category][oldName];
							delete costum.lists[data.map.category][oldName];
						}
						costum.badges[data.map.category][data.id].name=data.map.name;
					}
				}
			}
			dyFObj.commonAfterSave();
		}
	},
    strategy : {
        openForm : function(dataId){
            var dyfbadge=jQuery.extend(true, {}, typeObj.badge.dynFormCostum);
                //var dyfbadge=typeObj['badge']["dynFormCostum"]);
                if(typeof contextData != "undefined" && notNull(contextData) && notNull(contextData.id)){
                    dyfbadge.beforeBuild.properties["linksProjects"] = {
                        inputType : "finder",
                        label : "Actions liées à l'orientation",
                        //multiple : true,
                        initMe:false,
                        placeholder:"Quelles actions correspondent à cette orientation ?",
                        initContext:false,
                        initContacts:false,
                        rules : { required : false}, 
                        initType: ["projects"],
                        search : {"links" : [{ type:"projects", id:contextData.id }], "private" : true},
                        initBySearch : true,
                        openSearch :true
                    };
                    dyfbadge.formData ="costum.ctenat.badges.formData";
                    dyfbadge.prepData = function(data){
                        if(typeof data.map.links != "undefined" && data.map.links.projects != "undefined"){
                            data.map.linksProjects=data.map.links.projects;
                            delete data.map.links.projects;
                        }
                        return data;
                    }
                
                }
                dyfbadge.beforeBuild.properties.name.label="Titre de l'orientation stratégique";
                dyfbadge.beforeBuild.properties.description={"label":"Principaux enjeux et objectifs de l’orientation"};
                dyfbadge.beforeBuild.properties.synergie = {
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "Synergie et articulation avec d’autres démarches territoriales",
                    "rules" : {
                        "maxlength" : 2000
                    }
                };
                dyfbadge.onload.actions.setTitle="Ajouter une orientation stratégique à votre dispositif";
                dyfbadge.onload.actions.hide["categoryselect"]=1;
                dyfbadge.onload.actions.hide["parentfinder"]=1;
                dyfbadge.onload.actions.presetValue["category"]="strategy";
                if(notNull(dataId)){
                    //return false;
					/*ajaxPost(
						null,
						baseUrl+"/co2/element/get/type/badges/id/"+dataId,
						{},
						function (data) {
							dyFObj.openForm("badge",null, data.map,null, dyfbadge);
						}
					);*/
                    dyFObj.editElement('badges', dataId, null,dyfbadge);
                }else{

                    setParent= {
                        parent : {}
                    };
                    setParent.parent[contextData.id]={
                        type : contextData.collection,
                        name : contextData.name
                    };
                    dyFObj.openForm('badge', null, setParent,null,dyfbadge);
                }
        }
    },
	indicator : {
		afterSave : function(data){
			//if(dyFObj.editMode){
				
				dyFObj.closeForm();
				urlCtrl.loadByHash(location.hash);
				// if($.inArray(data.map.category, ["cibleDD", "domainAction"]) >= 0){
				// 	oldName=costum.badges[data.map.category][data.id].name;
				// 	if(data.map.name!=oldName){
				// 		if(typeof costum.badges[data.map.category][data.id].parent != "undefined"){
				// 			parentNameB=costum.badges[data.map.category][Object.keys(costum.badges[data.map.category][data.id].parent)[0]].name;
				// 			costum.lists[data.map.category][parentNameB].splice(costum.lists[data.map.category][parentNameB].indexOf(oldName), 1,data.map.name);
				// 		}else{
				// 			costum.lists[data.map.category][data.map.name]=costum.lists[data.map.category][oldName];
				// 			delete costum.lists[data.map.category][oldName];
				// 		}
				// 		costum.badges[data.map.category][data.id].name=data.map.name;
				// 	}
				// }
			//}
			//dyFObj.commonAfterSave();
		}
	},
	article : {
		afterSave : function(data){	
			dyFObj.commonAfterSave(data, function() {
				urlCtrl.loadByHash(location.hash);	
			});
			
		}
	}
};



function autoCompleteSearchGS(search, indexMin, indexMax, input, callB){
	mylog.log("autoCompleteSearchGS",search);

	var data = {"name" : search, "locality" : "", "searchType" : searchTypeGS, "searchBy" : "ALL",
	"indexMin" : indexMin, "indexMax" : indexMax, "category" : searchObject.category  };

	if(!notNull(input)){
		data.indexStep=10;
		data.count=true;
		data.sourceKey=costum["slug"];
		data.countType = [  "organizations", "projects", "events" ];
		data.searchType = [ "organizations", "projects", "events" ];
	}
	if(typeof costum != "undefined" && notNull(costum) && typeof costum.filters != "undefined" && typeof costum.filters.searchTypeGS != "undefined" 
		&& (!notNull(input) || $.inArray(input, ["#filter-scopes-menu", "#scopes-news-form"]) < 0)){
		data.countType = costum.filters.searchTypeGS;
		data.searchType = costum.filters.searchTypeGS;
	}
	var domTarget = (notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
	var dropDownVisibleDom=(notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
	 if($(domTarget+" .content-result").length > 0 && domTarget != ".dropdown-result-global-search")
        domTarget+=" .content-result";
             
	showDropDownGS(true, dropDownVisibleDom);
	if(indexMin > 0)
		$("#btnShowMoreResultGS").html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...");
	else{
		$(domTarget).html("<h5 class='text-dark center padding-15'><i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...</h5>");  
	}

//	showIsLoading(true);

	if(search.indexOf("co.") === 0 ){
		searchT = search.split(".");
		if( searchT[1] && typeof co[ searchT[1] ] == "function" ){
			co[ searchT[1] ](search);
			return;
		} else {
			co.mands();
		}
	}
	ajaxPost(
        null,
        baseUrl+"/" + moduleId + "/search/globalautocomplete",
        data,
        function(data){ 
            spinSearchAddon();
			if(!data){ toastr.error(data.content); }
			else{
				mylog.log("DATA GS");
				mylog.dir(data);

				var countData = 0;
				if(typeof data.count != "undefined")
					$.each(data.count, function(e, v){countData+=v;});
				else
					$.each(data.results, function(i, v) { if(v.length!=0){ countData++; } });

				totalDataGS += countData;

				str = "";
				var city, postalCode = "";

				if(totalDataGS == 0)      totalDataGSMSG = "<i class='fa fa-ban'></i> "+trad.noresult;
				else if(totalDataGS == 1) totalDataGSMSG = totalDataGS + " "+trad.result;   
				else if(totalDataGS > 1)  totalDataGSMSG = totalDataGS + " "+trad.results;   

				if(totalDataGS > 0){
					labelSearch=(Object.keys(data.results).length == totalDataGS) ? trad.extendedsearch : "Voir tous les résultats";
					str += '<div class="text-left col-xs-12" id="footerDropdownGS" style="">';
						str += "<label class='text-dark margin-top-5'><i class='fa fa-angle-down'></i> " + totalDataGSMSG + "</label>";
						str += '<a href="#search?text='+encodeURI(search)+'" class="btn btn-default btn-sm pull-right lbh" id="btnShowMoreResultGS">'+
									'<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> '+labelSearch+
								'</a>';
					str += '</div>';
					str += "<hr style='margin: 0px; float:left; width:100%;'/>";
				}
              //parcours la liste des résultats de la recherche
				$.each(data.results, function(i, o) {
					mylog.log("globalsearch res : ", o);
					var typeIco = i;
					var ico = "fa-"+typeObj["default"].icon;
					var color = mapColorIconTop["default"];

					mapElementsGS.push(o);
					if(typeof( typeObj[o.type] ) == "undefined")
						itemType="poi";
					typeIco = o.type;
					//if(directory.dirLog) mylog.warn("itemType",itemType,"typeIco",typeIco);
					if(typeof o.typeOrga != "undefined")
						typeIco = o.typeOrga;

					var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"] ;
					ico =  "fa-"+obj.icon;
					color = obj.color;
					
					htmlIco ="<i class='fa "+ ico +" fa-2x bg-"+color+"'></i>";
					if("undefined" != typeof o.profilThumbImageUrl && o.profilThumbImageUrl != ""){
						var htmlIco= "<img width='80' height='80' alt='' class='img-circle bg-"+color+"' src='"+baseUrl+o.profilThumbImageUrl+"'/>"
					}

					city="";

					var postalCode = o.postalCode
					if (o.address != null) {
						city = o.address.addressLocality;
						postalCode = o.postalCode ? o.postalCode : o.address.postalCode ? o.address.postalCode : "";
					}

					var id = getObjectId(o);
					var insee = o.insee ? o.insee : "";
					type = o.type;
					if(type=="citoyens") type = "person";
					//var url = "javascript:"; //baseUrl+'/'+moduleId+ "/default/simple#" + o.type + ".detail.id." + id;
					var url = (notEmpty(o.type) && notEmpty(id)) ? 
					        '#page.type.'+o.type+'.id.' + id : "";

					//var onclick = 'urlCtrl.loadByHash("#' + type + '.detail.id.' + id + '");';
					var onclickCp = "";
					var target = " target='_blank'";
					var dataId = "";
					var tags = "";
					if(typeof o.tags != "undefined" && o.tags != null){
						$.each(o.tags, function(key, value){
							if(value != "")
								tags +=   "<a href='javascript:' class='badge bg-red btn-tag'>#" + value + "</a>";
						});
					}

					var name = typeof o.name != "undefined" ? o.name : "";
					var postalCode = (	typeof o.address != "undefined" &&
										o.address != null &&
										typeof o.address.postalCode != "undefined") ? o.address.postalCode : "";

					if(postalCode == "") postalCode = typeof o.postalCode != "undefined" ? o.postalCode : "";
					var cityName = (typeof o.address != "undefined" &&
									o.address != null &&
									typeof o.address.addressLocality != "undefined") ? o.address.addressLocality : "";
                  	var countryCode=(typeof o.address != "undefined" && notNull(o.address) && typeof o.address.addressCountry != "undefined") ? "("+o.address.addressCountry+")" : ""; 
					var fullLocality = postalCode + " " + cityName+" "+countryCode;
					if(fullLocality == " Addresse non renseignée" || fullLocality == "" || fullLocality == " ") 
						fullLocality = "<i class='fa fa-ban'></i>";
					mylog.log("fullLocality", fullLocality);

					var description = (	typeof o.shortDescription != "undefined" &&
										o.shortDescription != null) ? o.shortDescription : "";
					if(description == "") description = (	typeof o.description != "undefined" &&
															o.description != null) ? o.description : "";
           
					var startDate = (typeof o.startDate != "undefined") ? "Du "+dateToStr(o.startDate, "fr", true, true) : null;
					var endDate   = (typeof o.endDate   != "undefined") ? "Au "+dateToStr(o.endDate, "fr", true, true)   : null;

					var followers = (typeof o.links != "undefined" && o.links != null && typeof o.links.followers != "undefined") ?
					                o.links.followers : 0;
					var nbFollower = 0;
					if(followers !== 0)                 
						$.each(followers, function(key, value){
						nbFollower++;
					});

					target = "";
					classA="lbh";
					if(type != "city" && type != "zone" ){ 
						str += "<a href='#@"+o.slug+"' class='lbh col-xs-12 no-padding searchEntity'>";
						str += "<div class='col-md-2 col-sm-2 col-xs-2 no-padding entityCenter text-center'>";
						str +=   htmlIco;
						str += "</div>";
						str += "<div class='col-md-10 col-sm-10 col-xs-10 entityRight'>";

						str += "<div class='entityName text-dark'>" + name + "</div>";

						str += '<div data-id="' + dataId + '"' + "  class='entityLocality'>"+
						"<i class='fa fa-home'></i> " + fullLocality;

						if(nbFollower >= 1)
						str +=    " <span class='pull-right'><i class='fa fa-chain margin-left-10'></i> " + nbFollower + " follower</span>";

						str += '</div>';

						str += "</div>";

						str += "</a>";
					}else{
						o.input = input;
	         	 		mylog.log("Here",o, typeof o.postalCode);

	         	 		
						if(type == "city"){
							var valuesScopes = {
								city : o._id.$id,
								cityName : o.name,
								postalCode : (typeof o.postalCode == "undefined" ? "" : o.postalCode),
								postalCodes : (typeof o.postalCodes == "undefined" ? [] : o.postalCodes),
								country : o.country,
								allCP : o.allCP,
								uniqueCp : o.uniqueCp,
								level1 : o.level1,
								level1Name : o.level1Name
							}

							if( notEmpty( o.nameCity ) ){
								valuesScopes.name = o.nameCity ;
							}

							if( notEmpty( o.uniqueCp ) ){
								valuesScopes.uniqueCp = o.uniqueCp;
							}
							if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
								valuesScopes.level5 = o.level5 ;
								valuesScopes.level5Name = o.level5Name ;
							}
							if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
								valuesScopes.level4 = o.level4 ;
								valuesScopes.level4Name = o.level4Name ;
							}
							if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
								valuesScopes.level3 = o.level3 ;
								valuesScopes.level3Name = o.level3Name ;
							}
							if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
								valuesScopes.level2 = o.level2 ;
								valuesScopes.level2Name = o.level2Name ;
							}

							valuesScopes.type = o.type;
							valuesScopes.key = valuesScopes.city+valuesScopes.type+valuesScopes.postalCode ;
							o.key = valuesScopes.key;
							mylog.log("valuesScopes city", valuesScopes);
		         	 		myScopes.search[valuesScopes.key] = valuesScopes;
							str += directory.cityPanelHtml(o);
						}
						else if(type == "zone"){


							valuesScopes = {
								id : o._id.$id,
								name : o.name,
								country : o.countryCode,
								level : o.level
							}
							mylog.log("valuesScopes",valuesScopes);

							if(o.level.indexOf("1") >= 0){
								typeSearchCity="level1";
								levelSearchCity="1";
								valuesScopes.numLevel = 1;
							}else if(o.level.indexOf("2") >= 0){
								typeSearchCity="level2";
								levelSearchCity="2";
								valuesScopes.numLevel = 2;
							}else if(o.level.indexOf("3") >= 0){
								typeSearchCity="level3";
								levelSearchCity="3";
								valuesScopes.numLevel = 3;
							}else if(o.level.indexOf("4") >= 0){
								typeSearchCity="level4";
								levelSearchCity="4";
								valuesScopes.numLevel = 4;
							}else if(o.level.indexOf("5") >= 0){
								typeSearchCity="level5";
								levelSearchCity="5";
								valuesScopes.numLevel = 5;
							}
							if(notNull(typeSearchCity))
								valuesScopes.type = typeSearchCity;				

							mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);

							if( notEmpty( o.level1 ) && valuesScopes.id != o.level1){
								mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);
								valuesScopes.level1 = o.level1 ;
								valuesScopes.level1Name = o.level1Name ;
							}

							var subTitle = "";
							if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
								valuesScopes.level5 = o.level5 ;
								valuesScopes.level5Name = o.level5Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
							}
							if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
								valuesScopes.level4 = o.level4 ;
								valuesScopes.level4Name = o.level4Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
							}
							if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
								valuesScopes.level3 = o.level3 ;
								valuesScopes.level3Name = o.level3Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level3Name ;
							}
							if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
								valuesScopes.level2 = o.level2 ;
								valuesScopes.level2Name = o.level2Name ;
								subTitle +=  (subTitle == "" ? "" : ", ") +  o.level2Name ;
							}
							//objToPush.id+objToPush.type+objToPush.postalCode
							valuesScopes.key = valuesScopes.id+valuesScopes.type ;
							mylog.log("valuesScopes.key", valuesScopes.key, valuesScopes);
							myScopes.search[valuesScopes.key] = valuesScopes;

							mylog.log("myScopes.search", myScopes.search);
							o.key = valuesScopes.key;
							str += directory.zonePanelHtml(o);
						}
					}
				}); //end each

              extendMsg=trad.extendedsearch;
              extendUrl="#search?text="+encodeURI(search);
              

              str += '<div class="text-center padding-bottom-10" id="footerDropdownGS">';
              str += "<label class='text-dark'>" + totalDataGSMSG + "</label><br/>";
              str += '<a href="'+extendUrl+'" class="btn btn-default btn-sm lbh margin-bottom-10" id="btnShowMoreResultGS">'+
                        '<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> Voir tout'+
                      '</a>';
              str += '</div>';

           	if(countData==0 && searchTypeGS == "cities"){
           		str="<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'>"+
                      '<label class="text-dark italic"><i class="fa fa-ban"></i> Aucun résultat pour <italic>"'+search+'"</italic>+</label><br/>'+
                     "</div>";
              }


              //on ajoute le texte dans le html
                  	$(domTarget).html(str);
              //on scroll pour coller le haut de l'arbre au menuTop
              $(domTarget).scrollTop(0);
              
              //on affiche la dropdown
              showDropDownGS(true, dropDownVisibleDom);
              bindScopesInputEvent();
              if(notEmpty(callB)){
              	callB();
              }
              coInterface.bindLBHLinks();

            //signal que le chargement est terminé
            mylog.log("loadingDataGS false");
            loadingDataGS = false;

          }

          //si le nombre de résultat obtenu est inférieur au indexStep => tous les éléments ont été chargé et affiché
          if(indexMax - countData > indexMin){
            $("#btnShowMoreResultGS").remove(); 
            scrollEndGS = true;
          }else{
            scrollEndGS = false;
          }

          if(isMapEnd){
            //affiche les éléments sur la carte
            showDropDownGS(false);
            Sig.showMapElements(Sig.map, mapElementsGS, "globe", "Recherche globale");
          }

        },
        function (data) {
			mylog.log("error"); mylog.dir(data);  
		}
    );
                    
  };

function insert(element, array) {
  array.push(element);
  array.sort(function(a, b) {
  	if ( a < b ){
    	return -1;
  	}
  	if ( a > b ){
    	return 1;
  	}
  
    //return a - b;
  });
  return array;
}

var costumBindButton = {
    askDispositif : function(){
	  	$(".askDispositif").click(function(){
	  	 	$('.bootbox').remove();
	  	 	
	  	 	var listDispoExistant = "<select id='dispoExistant' style='width:80%'><option value='Autre'>Autre</option>";
	  	 	$.each(costum.lists.dispositif,function(i,dispo){
	  	 		if( $.inArray( dispo,[ "CRTE","cte" ] ) < 0 )
	  	 			listDispoExistant += "<option value='"+dispo+"'>"+dispo+"</option>";
	  	 	});
	  	 	listDispoExistant += "</select>";
	  	 	bootbox.dialog({
	            title: "Veuillez indiquer l’intitulé de votre dispositif",
	            message: '<div class="row">  ' +
	                  '<div class="col-md-12"> ' +
	                    '<form class="form-horizontal"> ' +
	                      '<label class="col-md-12 no-padding" for="nameCRTE">Dispositif existant : </label><br> ' + 
	                      listDispoExistant+

		                  '<label class="col-md-12 no-padding" for="nameCRTE">Nom du Dispositif : </label><br> ' +                        
			              '<input type="text" id="dispositifName" class="nameSearch wysiwygInput form-control" style="width: 100%" ></input>'+
			              '<br>'+
			              
			              // '<label class="col-md-12 no-padding" for="dispositifCode">Identifiant unique : </label><br> ' +
			              // '<input type="text" id="dispositifCode" class="nameSearch wysiwygInput form-control" style="width: 100%" ></input>'+
			              // '<br>'+
	                '</form></div></div>',
	            buttons: {
					success: {
	                    label: trad.save,
	                    className: "btn-primary btn-save-dispositif",
	                    callback: function () {
	                    	var dispositifName = $("#dispositifName").val();
	                    	//var dispositifCode = $("#dispositifCode").val();
	                    	$('.bootbox').remove();
	                    	dyFObj.openForm("project",null ,{ dispositif:dispositifName
	        										  		  //dispositifId : dispositifCode 
	        										  		})
	                    }
	                },
				    cancel: {
						label: trad.cancel,
						className: "btn-secondary",
						callback: function() {
							$('.bootbox').remove();
						}
	                }
	            }
	        }).init(function(){
	        	$("#dispositifName").val('Autre');
				if($("#dispositifName").val() != "")
					$(".btn-save-dispositif").removeAttr('disabled');
				else 
					$(".btn-save-dispositif").attr('disabled','disabled');
				$("#dispositifName").keyup(function(){
	        		if($("#dispositifName").val() != "")
						$(".btn-save-dispositif").removeAttr('disabled');
					else 
						$(".btn-save-dispositif").attr('disabled','disabled');
	        	})
	        	$("#dispoExistant").change(function(){
	        		$("#dispositifName").val($("#dispoExistant").val());
					if($("#dispositifName").val() != "")
						$(".btn-save-dispositif").removeAttr('disabled');
					else 
						$(".btn-save-dispositif").attr('disabled','disabled');
	        	})
	        })
	  	 })
	},
	crteOpenForm : function(){
		//alert("bind crteOpenForm");
		$(".crteOpenForm").click(function(){
	  	 	$('.bootbox').remove();
	  	 	//var dep = null;
	  	 	costumBindButton.dep = {};
			costumBindButton.depSorted = [];
	  	 	var bootHtmlDep = '<label class="col-md-12 no-padding" for="departementSelect">Choisissez un département : </label><br> ' +
	  	 					  '<select id="departementSelect" style="width:100%;font-size: 20px;margin-bottom: 10px;" >'+
	  	 		 				"<option value=''>Choisissez un département</option>";
	  	 	var bootHtml = '<label class="col-md-12 no-padding" for="departementSelect">Choisissez un CRTE : </label><br> ' +
	  	 				   '<select id="crteSelectAdd" style="width:100%;font-size: 20px;margin-bottom: 10px;" >'+
	  	 		 				"<option value=''>Choisissez un CRTE</option>";
	   		$.each(costum.lists.crteAnct,function(i,c){
	   				if(typeof costumBindButton.dep[c.departement] == "undefined"){
		   				//dep = c.departement;
		   				costumBindButton.dep[c.departement] = { html : "" };
						insert(c.departement, costumBindButton.depSorted);
		   			}
		   			/*if(c.departement=="974"){
		   				alert(c.name);
		   				alert(dep);
		   			}*/
	            	costumBindButton.dep[c.departement].html += "<option value='"+i+"'>"+c.name+"</option>";
	        	}
	        );
	        mylog.log("costumbindButton", costumBindButton);
	        $.each(costumBindButton.depSorted , function(i,dep){
		        bootHtmlDep += "<option value='"+dep+"'>"+dep+"</option>";
		        bootHtml += "<optgroup label='Département : "+dep+"'>";
				bootHtml += costumBindButton.dep[dep].html; 
		    })
		    bootHtml += "</select>";
		    bootHtmlDep += "</select>";
	  	 	bootbox.dialog({
	            title: "Veuillez sélectionner dans la liste nationale des CRTE le nom de votre CRTE.",
	            message: bootHtmlDep+"<br/>"+bootHtml
	        }).init(function(){
	        	$("#departementSelect").change(function(){
	        		var crteHtml = "<option value=''>Choisissez un CRTE dans le département "+$(this).val()+"</option>"
	        						+costumBindButton.dep[$(this).val()].html;
	        		$("#crteSelectAdd").html(crteHtml);
	        	})
	        	$("#crteSelectAdd").change(function(){
	        		//alert("#crteSelectAdd");
	        		$(".bootbox").remove();
	        		var crte = costum.lists.crteAnct[ $(this).val() ];
	        		//alert(crte.epci.toString())
	        		ajaxPost(
			          null,
			          baseUrl+"/" + moduleId + "/zone/getscopebyids",
			          { epci : crte.epci },
			          function(data){ 
			            mylog.log("data scopes", data);
		            	var crteData = { name : crte.name,
        										  dispositif:"CRTE",
        										  dispositifId : crte.code };
        				if(data.scopes)
        					crteData.scope = data.scopes;

						dyFObj.openForm("project",null ,crteData)
				    	
			          }
			        );
	        		
	        	})
	        });
	  	 })
	}
}
  $(function (){
  	 /*$('#mainNav').append("<div class='col-xs-12 hidden-xs text-center submenuTop'></div>");
  	 $('#menuTopRight .lbh-menu-app').appendTo(".submenuTop");
  	 $('#menuTopLeft').addClass("col-sm-12");*/
  	 $("#menuTopLeft .searchBarInMenu .second-search-bar-addon i").toggleClass('fa-arrow-circle-right fa-search');
  	 $(".crteOpenForm").click(function(){
  	 	$('.bootbox').remove();
  	 	alert("choisir un crte");
  	 	dyfObj.openForm("project")
  	 });
  });


 