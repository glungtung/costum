var eventTypes = {
	workshop: "Atelier",
	tasting : "Dégustation",
	secondHandStoreJumbleSale : "Dépôt-vente, Braderie",
	radioShow : "Emission de radio",
	exhibition: "Exposition",
	trainingAwareness : "Formation, sensibilisation",
	forumMeetingTradeFair : "Forums, Recontres, salons",
	openHouse : "Portes-ouvertes",
	filmProjection : "Projection de film",
	others: "Autres"
};

directory.eventPanelHtml = function(params){
	return costum.cressReunion.directoryEvent(params.id, params);
};


// mapObj.dataMarker= function(mObj,key,params,latCom,lonCom,nameCity){

// 			if (typeof params.opt == "undefined" || params.opt == null)
// 								params.opt = {};

// 			params.opt.icon = mObj.addIcon(params);
// 			var latLon = [latCom, lonCom];
// 			mObj.distanceTo(latLon);
// 			var marker = L.marker(latLon, params.opt);

// 			mObj.markerList[key] = marker;

// 			if (typeof mObj.activePopUp != "undefined" && mObj.activePopUp === true)
// 				mObj.addPopUp(marker, params.elt);

// 			mObj.arrayBounds.push(latLon);
// 			if (mObj.activeCluster === true) {
// 				//mylog.log("giiiiiiiiiiiiiiiiii", marker);
// 				mObj.markersCluster.addLayer(marker);
// 			}
// 			else {
// 				marker.addTo(mObj.map);
// 				if (typeof params.center == "undefined" || params.center === true) {
// 					//mylog.log("mObj.panTo");
// 					mObj.map.panTo(latLon);
// 				}
// 			}	
// 			marker.off().on('click', function (e) {
// 				mylog.log("markerClick",params, nameCity);
// 	            pacte.caseCollectif(params, nameCity);
// 				coInterface.bindLBHLinks();
// 			});
		
// 			if(mObj.mapOpt.mouseOver === true){
// 				marker.on('mouseover', function (e) {
// 					mObj.openPopup();
// 					coInterface.bindLBHLinks();
// 				});
				
// 				marker.on('mouseout', function (e) {
// 					var thismarker = mObj;
// 					setTimeout(function(){
// 						thismarker.closePopup();
// 					}, 2000);
// 				});	
// 			}

// 		};

// mapObj.addMarker = function (params) {
// 	var obj = this;
// 	if (typeof params.elt != "undefined" && params.elt != null){

		
// 		if( typeof params.elt.scope != "undefined" && params.elt.scope != null){
// 			i = 0;
// 			$.each(params.elt.scope, function(e,v){
// 				if(	typeof v.geo != "undefined" && v.geo != null &&
// 					typeof v.geo.latitude != "undefined" && v.geo.latitude != null &&
// 					typeof v.geo.longitude != "undefined" && v.geo.longitude != null) {
// 					var latCom = v.geo.latitude;
// 					var lonCom = v.geo.longitude;
// 					var nameCity = v.cityName;
// 					obj.dataMarker(obj,(params.elt.id+i),params,latCom,lonCom,nameCity);
// 				}
// 				i++;
// 			});
// 		} 
			
// 		if(typeof params.elt.geo != "undefined" && params.elt.geo != null && 
// 			typeof params.elt.geo.latitude != "undefined" && params.elt.geo.latitude != null &&
// 			typeof params.elt.geo.longitude != "undefined" && params.elt.geo.longitude != null) {
// 				var latCom = params.elt.geo.latitude;
// 				var lonCom = params.elt.geo.longitude;
// 				var nameCity = "";
// 				if(typeof params.elt.scope != "undefined" && params.elt.scope != null){
// 					var keyScope = Object.keys(params.elt.scope)[0];
// 					nameCity = params.elt.scope[keyScope].cityName;
// 				}
// 				obj.dataMarker(obj,params.elt.id,params,latCom,lonCom,nameCity);
				
// 		}
// 	}
// };

mapCustom.markers = {
		eventess : modules.map.assets + '/images/markers/logo-lemois.png',
		orgaess : modules.map.assets + '/images/markers/ngo-ess.png',
		default: modules.map.assets + '/images/markers/citizen-marker-default.png',
		organization: modules.map.assets + '/images/markers/ngo-marker-default.png',
		classified: modules.map.assets + '/images/markers/classified-marker-default.png',
		proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
		poi: modules.map.assets + '/images/markers/poi-marker-default.png',
		project: modules.map.assets + '/images/markers/project-marker-default.png',
		event: modules.map.assets + '/images/markers/event-marker-default.png',
		answer: modules.map.assets + '/images/markers/services/tools-hardware.png'
};

mapCustom.markers.getMarker = function (data) {
			//mylog.log("mapObj.getMarker", data.type);
			var imgM = mapCustom.markers.default;
			if (data.collection=="events" && typeof data.tags != "undefined" && $.inArray("MoisESS2020", data.tags) > -1 )
				imgM = mapCustom.markers.eventess;
			else if (data.collection=="organizations" && typeof data.tags != "undefined" && $.inArray("MoisESS2020", data.tags) > -1 )
				imgM = mapCustom.markers.orgaess;
			else if (typeof data.profilMarkerImageUrl !== "undefined" && data.profilMarkerImageUrl != "")
				imgM = baseUrl + data.profilMarkerImageUrl;
			else if (typeof data.collection != "undefined" && data.collection == null)
				imgM = mapCustom.markers.default;
			else if (jQuery.inArray(data.collection, ["organization", "organizations", "NGO", "Cooperative"]) != -1)
				imgM = mapCustom.markers.organization;
			else if (jQuery.inArray(data.collection, ["project", "projects"]) != -1)
				imgM = mapCustom.markers.project;
			else if (jQuery.inArray(data.collection, ["event", "events"]) != -1)
				imgM = mapCustom.markers.event;
			else if (jQuery.inArray(data.collection, ["classified", "classifieds"]) != -1)
				imgM = mapCustom.markers.classified;
			else if (jQuery.inArray(data.collection, ["proposal", "proposals"]) != -1)
				imgM = mapCustom.markers.proposal;
			else if (jQuery.inArray(data.collection, ["poi"]) != -1)
				imgM = mapCustom.markers.poi;
			else
				imgM = mapCustom.markers.default;
			if (jQuery.inArray(data.collection, ["answers"]) != -1)
				imgM = mapCustom.markers.poi;
			//mylog.log("mObj imgM", imgM);
			return imgM;
		}

// mapCustom.addIcon = function(elt){
// 	mylog.log("elt addicon custom", elt);
// 	if (elt.collection=="organizations" && typeof elt.tags != "undefined" && $.inArray("MoisESS2020", elt.tags) > -1 ){
// 			var myCustomColour = '#428bca';
		
// 	}else{
// 		mapCustom.markers.getMarker(elt);
// 	}

// 	var markerHtmlStyles = `
// 		background-color: ${myCustomColour};
// 		width: 3.5rem;
// 		height: 3.5rem;
// 		display: block;
// 		left: -1.5rem;
// 		top: -1.5rem;
// 		position: relative;
// 		border-radius: 3rem 3rem 0;
// 		transform: rotate(45deg);
// 		border: 1px solid #FFFFFF`;

// 	var myIcon = L.divIcon({
// 		className: "my-custom-pin",
// 		iconAnchor: [0, 24],
// 		labelAnchor: [-6, 0],
// 		popupAnchor: [0, -36],
// 		html: `<span style="${markerHtmlStyles}" />`
// 	});
// 	return myIcon;
// };


costum.cressReunion = {
	organisations : {
		afterSave : function(data){
			//alert("test");
			/*toastr.success(trad.waitingValidation);
			dyFObj.commonAfterSave(data, callB);*/
			
		}
	},
	"events" : {
		formData : function(data){
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}

			});
			if(typeof data.monthEss != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				if(data.monthEss == "Oui") {
					data.tags.push("MoisESS2020");
				}	
				//delete data.mainTag;
			}
			if(typeof data.format != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				if(data.format == "En ligne (Webinaire, webcast, etc)" || data.format == "Les deux, présentiel et distanciel") {
					data.tags.push("Événement en ligne");
				}
				//delete data.mainTag;
			}			
			return data;
		}
	},
		directoryEvent : function(keyE, valE){
		mylog.log("costum.cressReunion.directoryEvent", keyE, valE);
				var str = ''; 
				var style = '';
		// 		if (typeof valE.tags != "undefined" && $.inArray("MoisESS2020", valE.tags) > -1 ){
		// 			style= 'background-color:#9fd8c6 !important;'
		// }
		str += '<div class=\'col-xs-12 searchEntityContainer '+valE.containerClass+' contain_'+valE.collection+'_'+valE.id+' \'>';
		str +=    '<div class=\'searchEntity\' id=\'entity'+valE.id+'\' style="'+style+'">';

		// if(valE.updated != null && valE.updated.indexOf(trad.ago)>=0)
		// 	valE.updated = trad.rightnow;
		// if(Date.parse(valE.startDate) > Date.now()){
		// 	valE.updated='';
		// }
		// if(valE.updated != null && valE.updated!= '')
		// 	str += '<div class=\'dateUpdated\'><i class=\'fa fa-flash\'></i> ' + valE.updated + '</div>';

		var dateFormated = directory.getDateFormated(valE);
		// var hoursFormated = directory.showDatetimePost(valE);
		valE.attendees = '';
		var cntP = 0;
		var cntIv = 0;

		if(typeof valE.links != 'undefined')
			if(typeof valE.links.attendees != 'undefined'){
				$.each(valE.links.attendees, function(key, val){ 
					if(typeof val.isInviting != 'undefined' && val.isInviting == true)
						cntIv++; 
					else
						cntP++; 
				});
			}

		valE.attendees = '<hr class=\'margin-top-10 margin-bottom-10\'>';
        
		var isFollowed=false;
		if(typeof valE.isFollowed != 'undefined' ) isFollowed=true;
          
		if(userId != null && userId != '' && valE.id != userId){

			var isShared = false;

			valE.attendees += '<button id=\'btn-share-event\' class=\'text-dark btn btn-link no-padding margin-left-10 btn-share\''+
                              ' data-ownerlink=\'share\' data-id=\''+valE.id+'\' data-type=\''+valE.type+'\' '+//data-name='"+valE.name+"'"+
                              ' data-isShared=\''+isShared+'\'>'+
                              '<i class=\'fa fa-retweet\'></i> '+trad['share']+'</button>';
		}

		valE.attendees += '<small class=\'light margin-left-10 tooltips pull-right\'  '+
                                    'data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad['attendee-s']+'\'>' + 
                              cntP + ' <i class=\'fa fa-street-view\'></i>'+
                            '</small>';


		valE.attendees += '<small class=\'light margin-left-10 tooltips pull-right\'  '+
                                    'data-toggle=\'tooltip\' data-placement=\'bottom\' data-original-title=\''+trad['guest-s']+'\'>' +
                               cntIv + ' <i class=\'fa fa-envelope\'></i>'+
                            '</small>';

		var countSubEvents = ( valE.links && valE.links.subEvents ) ? '<br/><i class=\'fa fa-calendar\'></i> '+Object.keys(valE.links.subEvents).length+' '+trad['subevent-s']  : '' ; 
		str += '<div class="col-xs-12 col-sm-4 col-md-4 no-padding">'+
                  '<a href="'+valE.hash+'" class="container-img-profil lbh add2fav block">'+valE.imageProfilHtml+'</a>'+  
                '</div>';
        
		if(userId != null && userId != '' && valE.id != userId /*&& !inMyContacts(valE.typeSig, valE.id)*/){
			var tip = trad['interested'];
			var actionConnect='follow';
			var icon='chain';
			var classBtn='';
			if(isFollowed){
				actionConnect='unfollow';
				icon='unlink';
				classBtn='text-green';
			}
			str += '<a href=\'javascript:;\' class=\'btn btn-default btn-sm btn-add-to-directory bg-white tooltips followBtn '+classBtn+'\'' + 
                      'data-toggle="tooltip" data-placement="left" data-original-title="'+tip+'"'+
                      ' data-ownerlink=\''+actionConnect+'\' data-id=\''+valE.id+'\' data-type=\''+valE.collection+'\' data-name=\''+valE.name+'\''+
                      ' data-isFollowed=\''+isFollowed+'\'>'+
                      '<i class=\'fa fa-'+icon+'\'></i>'+
                    '</a>';
		}

		if (typeof valE.tags != "undefined" && $.inArray("MoisESS2020", valE.tags) > -1 ){
			str +="<img class='imgEss col-md-2 col-sm-2 col-xs-12' src='" + moduleUrl + "/images/cressReunion/logo-moisess20.png'>";
		}
		str += '<div class=\'col-md-6 col-sm-6 col-xs-12 margin-top-25\'>';
		str += dateFormated;
		// str += hoursFormated;
		str += '</div>';
        

		str += '<div class=\'col-md-8 col-sm-8 col-xs-12 entityRight padding-top-10 margin-top-10 pull-right\' style=\'border-top: 1px solid rgba(0,0,0,0.2);\'>';

		var thisLocality = '';
		if(valE.fullLocality != '' && valE.fullLocality != ' ')
			thisLocality = '<small class=\'margin-left-5 letter-red\'><i class=\'fa fa-map-marker\'></i> ' + valE.fullLocality + '</small>' ;
		else thisLocality = '';
                
		var typeEvent = (typeof tradCategory[valE.typeEvent] != 'undefined') ? tradCategory[valE.typeEvent] : trad.event;
        
		str += '<h5 class=\'text-dark lbh add2fav no-margin\'>'+
                  '<i class=\'fa fa-reply fa-rotate-180\'></i> ' + tradCategory[valE.type] +
               '</h5>';

		str += '<a href=\''+valE.hash+'\' class=\'entityName text-dark lbh add2fav\'>'+
                  valE.name + 
               '</a>';
        
		if('undefined' != typeof valE.organizer && valE.organizer != null){ 
			var countOrganizer=Object.keys(valE.organizer).length;
			str += '<div class=\'col-xs-12 margin-top-10 no-padding\'>';
			str+='<span class=\'bold\'>'+tradDynForm.organizedby+' : </span>';
			$.each(valE.organizer, function(e,v){
				var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl!='' ) ? baseUrl+'/'+v.profilThumbImageUrl: assetPath + '/images/thumb/default_'+v.type+'.png';  
				str+='<a href="#page.type.'+v.type+'.id.'+e+'" class="lbh-preview-element tooltips" ';
				if(countOrganizer>1) str+= 'data-toggle="tooltip" data-placement="top" title="'+v.name+'"';
				str+='>'+
              '<img src="'+imgIcon+'" class="img-circle margin-right-10" width="35" height="35"/>';
				if(countOrganizer==1) str+=v.name;
				str+='</a>';     
			});
			str+='</div>';

		}
		// str +=    '<div class=\'entityDescription margin-bottom-10\'>' + 
  //                   valE.description + 
  //                 '</div>';
		// str +=    '<div class=\'margin-bottom-10 col-md-12 no-padding\'>' + 
  //                   valE.attendees + 

  //                 '</div>';

  		str +=    '<div class="margin-bottom-10 col-md-12 no-padding">' + 
                    valE.attendees + 

                  '</div>';
        if(typeof valE.tags != "undefined" && valE.tags != null){          
		str +=  '<div class=\'col-xs-12 tagsContainer text-red\'>';
			$.each(valE.tags,function(i,tv){
	 	    	str += "<span style='background-color:transparent;' class='padding-left-5 bold text-red btn-tag tag' data-tag-value="+tv+" data-tag-label="+tv+">#" +tv+ "</span>";	
	 	   	});

		str +=	'</div>';
	}



		str += '</div>';
            
		str += '</div>';
		str += '</div>';

		str += '</div>';
		return str;
	}

};

costum.calendar = {
	convert : function(eventObj, taskCal){
		mylog.log("calendarObj.results.convert costum", taskCal);	
		var backgroundColor = "#FFA200 !important";
		$.each(costum.paramsData.poles, function(kT, vT){
			//mylog.log("directoryEvent poles", kT, vT);
			if(typeof  eventObj.tags != "undefined" && 
				 eventObj.tags != null &&
				  eventObj.tags.length > 0 &&
				  $.inArray(kT, eventObj.tags) > -1)
				backgroundColor = vT.color+ " !important" ;
		});

		taskCal.backgroundColor = backgroundColor;

		return taskCal;
	}
};


// function searchMapCress(){
// 	mapCO.showLoader();
// 	var paramsSearchMap=searchInterface.constructObjectAndUrl();
// 	mylog.log("fObj searchMapCress paramsSearchMap", paramsSearchMap);
// 	$.ajax({
//         type: "POST",
//         url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
//         data: paramsSearchMap,
//         dataType: "json",
//         error: function (data){
//             mylog.log(">>> error autocomplete search"); 
//             mylog.dir(data);   
//             $("#dropdown_search").html(data.responseText);  
//             //signal que le chargement est terminé
//             loadingData = false;     
//         },
//         success: function(data){ 
//         mylog.log(">>> fObj searchMapCress success autocomplete search !!!! ", data); //mylog.dir(data);
//             if(!data){ 
//                 toastr.error(data.content); 
//             } 
//             else{ 
//                 mapCO.clearMap();
//                 mapCO.addElts(data.results);
//             }
//         }
//     }); 
// }
dyFCustom.timeoutSiret = 0 ;
dyFCustom.siret =  function(){

	$("#ajaxFormModal #siret").off().on("keypress keyup blur", function(e){
			mylog.log("#ajaxFormModal #siret success", $("#ajaxFormModal #siret").val());
			$(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((e.which < 48 || e.which > 57)) {
                e.preventDefault();
				if($("#ajaxFormModal #siret").val().trim().length > 1 ){
					if(notNull(scopeObj.timeoutSiret)) 
						clearTimeout(scopeObj.timeoutSiret);

					scopeObj.timeoutSiret = setTimeout(function(){
						var paramsSiret = {
							filters : {
								siret : [ $("#ajaxFormModal #siret").val().trim() ]
							},
							searchType : [ "organizations" ],
							sourceKey : "cressReunion"
						}
						ajaxPost(
							null,
							baseUrl+"/co2/search/globalautocomplete",
							paramsSiret,
							function(data){
								mylog.log("#ajaxFormModal #siret success", data);
								if($("#ajaxFormModal #listsiret").length > 0)
									$("#ajaxFormModal #listsiret").remove();
								var showInput = false;
								if(Object.keys(data.results).length > 0){
									$("#btn-submit-form").prop("disabled",true);
									var str = "<div id='listsiret'>";
									$.each(data.results, function(id, elem) {
										str += 	'<div class="col-xs-12 no-padding">'+
											"<a href='#page.type."+ elem.type +".id."+ id +"'  "+
											" target='_blank' "+
											"class='btn btn-default col-xs-12 text-left padding-5 margin-5'>"+
											'<h5 class="text-green"><i class="fa fa-user"></i> '+
											elem.name+'</h5>'+
											"</a>"+
											'</div>';
									});
									str += "</div>";
									$("#ajaxFormModal #siret").after(str);

								}else{
									$("#btn-submit-form").prop("disabled",false);
									showInput = true;
								}

								$.each(dyFObj.elementObj.dynForm.jsonSchema.properties, function(kP, vP){
									if(kP != "info" && kP != "siret"){
										if(showInput === true)
											$("#ajaxFormModal ."+kP+vP.inputType).show();
										else
											$("#ajaxFormModal ."+kP+vP.inputType).hide();
									}
								});
							},
							function (data){
								mylog.log("error"); mylog.dir(data);
								$("#btn-submit-form").html('Valider <i class="fa fa-arrow-circle-right"></i>').prop("disabled",false);
							},
							"json"
						);
					}, 500);
				}
			}
		});
}

/*function showOrganisationInCitoyens(){
	  setTimeout(function(){
	  	if (contextData.type == "citoyens") {
			var filterAddType = ["organizations"] ;
			typeObj.buildCreateButton(".elementCreateButton", false, {
				addClass:"col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
				bgIcon:true,
				textColor:true,
				inElement:true,
				allowIn:true,
				contextType: contextData.type,
				bgColor : "white",
				explain:true,
				inline:false
			}, null, filterAddType);
	  	}
	},2000)
}*/




