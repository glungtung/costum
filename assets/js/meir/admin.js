
adminPanel.views.organizations = function() {
    var data = {
        title: "Organisations",
        types: ["organizations"],
        table: {
            name: {
                name: "Dénomination"
            },
            jobFamily: {
                name: "Famille de métier(s)"
            },
            mobile: {
                name: "Téléphone"
            },
            email: {
                name: "Mèl dispositif financier "
            },
            objective: {
                name: "Objectif(s)",
                class : "col-xs-2 text-center"
            },
            responsable: {
                name: "Responsable"
            },
            address: {
                name: "Adresse"
            },
            legalStatus: {
                name: "Statut juridique"
            }
        },
        paramsFilter: {
            container: "#filterContainer",
            defaults: {
                notSourceKey : true,
                types: ["organizations"],
                fields: ["category","name", "jobFamily", "thematic", "typeFinancing", "legalStatus","updated", "address", "responsable", "link", "objective", "linkFinancialDevice", "mobile", "email", "financialPartners", "maximumAmount", "publicCible", "conditionsEligibility","tags","toBeValidated","collection","source","reference"],
                forced :{
                    filters : {                      
                        toBeValidated : {'$exists':true},
                        category : "acteurMeir",
                    }
                },

            },
            filters: {
                text: true,
            }
        },
        actions : {
            accept : true,
            delete : true,
            sendMail : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl + '/' + moduleId + '/admin/directory/', data, function() {

    }, "html");
};
adminDirectory.values.name = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.name != "undefined") {
        if(type == "organizations")
		    str = '<a href="#page.type.organizations.id.'+e._id.$id+'" class="" target="_blank">'+e.name+'</a>';
        else if (type == "projects")
            str = e.name;
        else 
            str = '<a href="#page.type.'+type+'.id.'+e._id.$id+'" class="" target="_blank">'+e.name+'</a>';
	}
    return str;
};
adminDirectory.values.toBeValidated = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.toBeValidated != "undefined") {
        str = e.toBeValidated;
    }
    return str;
};
adminDirectory.values.collection = function(e, id, type, aObj) {
  
};
adminDirectory.values.jobFamily = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.jobFamily != "undefined") {
        str = e.jobFamily;
    }
    return str;
};
adminDirectory.values.typeFinancing = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.typeFinancing != "undefined") {
        str = e.typeFinancing;
    }
    return str;
};
adminDirectory.values.legalStatus = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.legalStatus != "undefined") {
        str = e.legalStatus;
    }
    return str;
};
adminDirectory.values.responsable = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.responsable != "undefined") {
        str = e.responsable;
    }
    return str;
};
adminDirectory.values.link = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.link != "undefined") {
        str = e.link;
    }
    return str;
};
adminDirectory.values.objective = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.objective != "undefined") {
        str = e.objective;
    }
    return str;
};
adminDirectory.values.linkFinancialDevice = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.linkFinancialDevice != "undefined") {
        str = e.linkFinancialDevice;
    }
    return str;
};
adminDirectory.values.mobile = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.mobile != "undefined") {
        str = e.mobile;
    }
    return str;
}; 
adminDirectory.actions.accept = function(e, id, type, aObj){ 
	var str ='<button data-id="'+id+'" data-type="organizations" class="col-xs-12 acceptActor btn bg-green-k text-white"><i class="fa fa-check-circle"></i>Valider</button>';		
	return str ;
}; 
adminDirectory.actions.delete = function(e, id, type, aObj){  
    var str = ""; 
    if( typeof e.reference !="undefined" && typeof e.reference.costum !="undefined" && (e.reference.costum).indexOf("meir")>=0)
	    str +='<button data-id="'+id+'" data-type="organizations" class="col-xs-12 deleteActor btn bg-red-k text-white" data-deletetype="noadmin"><i class="fa fa-trash"></i> Supprimer la fiche</button>';		
	else
        str +='<button data-id="'+id+'" data-type="organizations" class="col-xs-12 deleteActor btn bg-red-k text-white" data-deletetype="delete"><i class="fa fa-trash"></i> Supprimer la fiche</button>';
        return str ;
};
adminDirectory.actions.sendMail = function(e, id, type, aObj){  
	var str =' <div style="margin-top:2px"><a href="javascript:;" data-id="'+id+'" data-email="'+e.email+'" class="tooltips btn openFormContact text-center " data-toggle="modal" data-target="#myModal-contact-us"    data-id-receiver=""  data-email="" data-name=""> Envoyer un email</a>';		
    
    return str ;
};
adminDirectory.bindCostum = function(aObj){
    $("#"+aObj.container+" .acceptActor").on("click", function(){  
        var tplCtx = {};
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("type");
        tplCtx.value = null;
        tplCtx.path = "toBeValidated";
        dataHelper.path2Value(tplCtx, function(params) {
            toastr.success("Acteur ajouté"); 
            urlCtrl.loadByHash(location.hash);
        } );
    });
    $("#"+aObj.container+" .deleteActor").on("click", function(){
        id = $(this).data("id");
        type = $(this).data("type"); 
        deleteType = $(this).data("deletetype");
        var params = [];
        var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
        if(deleteType == "noadmin" ){
            urlToSend = baseUrl+"/"+moduleId+"/admin/setsource/action/remove/set/reference";
                params = {
                id:  $(this).data("id"),
                type: $(this).data("type"),
                origin: "costum",
                sourceKey: "meir"
            }
        }
        if(deleteType == "delete" ){
            urlToSend =  baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
            params = null
        }
        bootbox.confirm("voulez vous vraiment supprimer cet acteur !!",
            function(result)
            {
                if (!result) { 
                    return;
                } else { 
                    ajaxPost(
                        null,
                        urlToSend,
                        params,
                        function(data){
                            if ( data && data.result ) {
                                toastr.success("acteur effacé");
                                $("#"+type+id).remove();
                                urlCtrl.loadByHash(location.hash);
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        }
                    );
                }
            }
        );
    });
    $("#"+aObj.container+" .refuseActor").on("click", function(){
        var tplCtx = {};
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("type");
        tplCtx.value = null;
        tplCtx.path = "category";  
        dataHelper.path2Value( tplCtx, function(params) { 
            toastr.success("Acteur refusé");
            urlCtrl.loadByHash(location.hash);
        } );
    });
    $("#"+aObj.container+" .openFormContact").on("click", function(){
        email = $(this).data("email");
        id = $(this).data("id");
        var  modal = `<div class=" fade in" id="formContact-${id}" tabindex="-1" role="dialog" aria-hidden="true"> 
                    <div class="  padding-top-15">                         
                        <div class="container bg-white">
                        <div class="close-modal" data-dismiss="modal">
                            <div style="background-color:#2C3E50" class="lr">
                                <div style="background-color:#2C3E50" class="rl">
                                </div>
                            </div>
                        </div>
                        <div id="form-group-contact">
                        <div class="col-md-10 text-left padding-top-60 form-group">
                        <h3>
                            <i class="fa fa-send letter-blue"></i> 
                            <small class="letter-blue"> <?php echo Yii::t('cms', 'Send an e-mail to')?> : </small>
                            <span id="contact-name" style="text-transform: none!important;"></span><br>
                            <small class="">
                                <small class="">Ce message sera envoyé à </small>
                                <b><span class="contact-email"></span></b>
                            </small>
                        </h3>
                        <hr><br>
                        <div class="col-md-12">
                            <label for="objet"><i class="fa fa-angle-down"></i> Objet de votre message</label>
                            <input class="form-control" placeholder="Objet de votre message" id="subject">
                            <div  style =" display: none;  width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;" class="invalid-feedback subject">Objet requis.</div><br>
                        </div>
                    </div>
                    <div class="col-md-12 text-left form-group">
                        <div class="col-md-12">
                            <label for="message"><i class="fa fa-angle-down"></i> Votre message</label>
                            <textarea placeholder="Votre message..." class="form-control txt-mail"
                        id="message" style="min-height: 200px;"></textarea>
                    <div style =" display: none;  width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;" class="invalid-feedback message">Votre message est vide.</div><br>
                    <br>
                    <div class="col-md-12 margin-top-15 pull-left">            
                        <button type="submit" class="btn btn-success pull-right" id="btn-send-mail">
                        <i class="fa fa-send"></i> <?php echo Yii::t('cms', 'Send the message')?>
                        </button>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    
    </div>`;
        smallMenu.open(modal);    
        $("#btn-send-mail").click(function(){ 
            sendEmail();
        });  
        $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
        $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
        if (notNull(email)){
            $("#formContact-"+id+" .contact-email").html(email); 
        } else{
            bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
        } 
    });
}
  
  function sendEmail(){
    var acceptFields = true;
    $.each(["subject","message"],(k,v)=>{
        if($("#"+v).val() == ""){
          $("."+v).show();
          acceptFields=false
        }
  
    });
    var seconds = Math.floor(new Date().getTime() / 1000);
    var allowedTime = localStorage.getItem("canSend"); 
    if(acceptFields){
        if(seconds < allowedTime){
            return bootbox.alert("<p class='text-center text-dark'><?php echo Yii::t('cms', 'Send back after')?> "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
        }
        localStorage.removeItem("canSend"); 
        var subject = $("#subject").val(); 
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
            tpl : "contactForm",
            tplMail : emailFrom,
            fromMail: costum.admin.email, 
            tplObject:subject,
            subject :subject,  
            emailSender:costum.admin.email,
            message : message,
            logo:"",
        };    
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
            params,
            function(data){ 
                if(data.result == true){
                    localStorage.setItem("canSend", (seconds+300));
                    toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                    bootbox.alert("<p class='text-center text-green-k'><?php echo Yii::t('cms', 'Email sent to')?> "+emailFrom+" !</p>");
                    $.each(["subject","message"],(k,v)=>{$("#"+v).val("")});
                    urlCtrl.loadByHash(location.hash);
                }else{
                    toastr.error("An error occurred while sending your message");
                    bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
                }   
            },
            function(xhr, status, error){
                toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
            }
        );
    }   
}