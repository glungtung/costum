pageProfil.views.home = function() {
    if(isInterfaceAdmin){
        if (contextData.type == "organizations"){
            $("#social-header").css("display","none");
            $("#menu-top-profil-social").css("display","none");
            ajaxPost(
                '#central-container',
                baseUrl + '/costum/meir/elementhome/type/' + contextData.type + "/id/" + contextData.id,
                null,
                function() {},
                "html"
            );
        } else {
            pageProfil.views.detail();
        }
    }else{
        $("#social-header").css("display","none");
        $("#menu-top-profil-social").css("display","none");
        $(".social-main-container").css("min-height","0");
        $(".acteurMeir .divNotConnected").css("height","900px !important"); 
        $(".acteurMeir").html("<div class='text-center divNotConnected'> BIENTOT DISPONIBLE </div>")
    }
};