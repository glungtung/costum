'use strict';

function carousel(parameter) {
    let output = {
        animationDuration: typeof parameter['animationDuration'] !== 'undefined' ? parameter['animationDuration'] : 500,
        delay: typeof parameter['delay'] !== 'undefined' ? parameter['delay'] : 2000,
        autoplay: typeof parameter['autoplay'] !== 'undefined' ? parameter['autoplay'] : true,
        navigation: {
            indicator: parameter['navigation'] && typeof parameter['navigation']['indicator'] !== 'undefined' ? parameter['navigation']['indicator'] : false,
            button: {
                show: parameter['navigation'] && parameter['navigation']['button'] && typeof parameter['navigation']['button']['show'] !== 'undefined' ? parameter['navigation']['button']['show'] : false,
                next: parameter['navigation'] && parameter['navigation']['button'] && typeof parameter['navigation']['button']['next'] !== 'undefined' ? parameter['navigation']['button']['next'] : 'next',
                prev: parameter['navigation'] && parameter['navigation']['button'] && typeof parameter['navigation']['button']['prev'] !== 'undefined' ? parameter['navigation']['button']['prev'] : 'prev'
            },
        },
        container: parameter['container'],
        size: {
            width: parameter['container'].width(),
            height: parameter['container'].height(),
        },
        elements: parameter['container'].children(),
        length: 0,
        current: 0,
        previous: 0,
        next: 0,
        interval: null,
        animated: [],
        init: function () {
            this.current = this.current % this.length;
            this.current = this.current < 0 ? (this.length + this.current) % this.length : this.current;
            this.previous = (this.current - 1) % this.length;
            this.previous = this.previous < 0 ? (this.length + this.previous) % this.length : this.previous;
            this.next = (this.current + 1) % this.length;
            if (this.length > 1) {
                this.recreateSlide();
                if (this.navigation.indicator) {
                    this.buildNavigation();
                }
            }
        },
        onSlide: function (element) { },
        onResize: function () { },
        recreate: function (container) {
            this.elements = container.children();
            this.length = this.elements.length;
            this.init();
        },
        recreateSlide: function () {
            this.container.find('.slide').remove();
            this.animated = [
                $(this.elements[this.previous]).clone(),
                $(this.elements[this.current]).clone(),
                $(this.elements[this.next]).clone()
            ];
            this.container.append(this.animated[0]);
            this.container.append(this.animated[1]);
            this.container.append(this.animated[2]);
            this.animated[0].css({ marginLeft: `-${this.size.width}px` });
            this.animated[1].css({ marginLeft: 0 });
            this.animated[2].css({ marginLeft: 0 });
        },
        start: function () {
            if (this.length > 1 && this.autoplay) {
                if (this.interval != null) clearInterval(this.interval);
                this.interval = setInterval(() => {
                    this.gotoNext();
                }, this.delay);
            }
        },
        gotoNext: function (callback) {
            if (this.length > 1) {
                this.animated[0].animate({
                    marginLeft: `-=${this.size.width}px`
                }, this.animationDuration, () => {
                    this.current++;
                    this.init();
                    this.start();
                    this.onSlide(this.animated[1]);
                    if (callback) callback();
                });
            }
        },
        gotoPrevious: function (callback) {
            if (this.length > 1) {
                this.animated[0].animate({
                    marginLeft: `+=${this.size.width}px`
                }, this.animationDuration, () => {
                    this.current--;
                    this.init();
                    this.start();
                    this.onSlide(this.animated[0]);
                    if (callback) callback();
                });
            }
        },
        jumpTo: function (old, target, callback) {
            if (old < target) {
                this.next = target;
                this.recreateSlide();
                this.animated[0].animate({
                    marginLeft: `-=${this.size.width}px`
                }, this.animationDuration, () => {
                    this.current = target;
                    this.init();
                    this.start();
                    this.onSlide(this.animated[1]);
                    if (callback) callback();
                })
            } else if (old > target) {
                this.previous = target;
                this.recreateSlide();
                this.animated[0].animate({
                    marginLeft: `+=${this.size.width}px`
                }, this.animationDuration, () => {
                    this.current = target;
                    this.init();
                    this.start();
                    this.onSlide(this.animated[0]);
                    if (callback) callback();
                })
            }
        },
        buildNavigation: function () {
            while (this.container.parent().children().length > 1) {
                this.container.parent().children()[this.container.parent().children().length - 1].remove();
            }
            let html = '<ul class="slide-indicators">';
            const length = this.elements.length;
            if (this['navigation']['button']['show']) html += `<li class="slide-navigator prev">${this['navigation']['button']['prev']}</li>`;
            for (let i = 0; i < length; i++) {
                html += `<li class="slide-indicator${i === this.current ? ' active' : ''}"></li>`;
            }
            if (this['navigation']['button']['show']) html += `<li class="slide-navigator next">${this['navigation']['button']['next']}</li>`;
            html += '</ul>';
            this.container.parent().append(html);
            this.container.next('.slide-indicators').find('.slide-indicator').each((index, element) => {
                $(element).on('click', () => {
                    $(element).off();
                    this.jumpTo(this.current, index);
                })
            })
            this.container.next('.slide-indicators').on('mouseover', () => {
                if (this.interval != null) clearInterval(this.interval);
            })
            this.container.next('.slide-indicators').on('mouseout', () => {
                if (this.autoplay) this.start();
            })
            if (this['navigation']['button']['show']) {
                this.container.next('.slide-indicators').find('.slide-navigator.next').on('click', () => this.gotoNext());
                this.container.next('.slide-indicators').find('.slide-navigator.prev').on('click', () => this.gotoPrevious());
            }
        }
    }
    output.length = output.elements.length;
    const oldWindow = $(window);
    let width = oldWindow.width();
    let height = oldWindow.height();
    setInterval(() => {
        const newWindow = $(window);
        let resised = false;
        if (newWindow.width() !== width) {
            width = newWindow.width();
            output['size']['width'] = output['container'].width();
            resised = true;
        }
        if (newWindow.height() !== height) {
            height = newWindow.height();
            output['size']['height'] = output['container'].height();
            resised = true;
        }
        if (resised) {
            output.onResize();
            output.recreateSlide();
        }
    }, 500);
    return output;
}