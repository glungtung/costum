// costum.hinauraCategory = {
// }
costum.hinauraCategory = {
    "Formations" : {
        "label":"Formations",
        "subList" : {
            "Compétences de base" : {
                "label":"Compétences de base",
                "subList" : {
                    "Faire un diagnostic des competences" : {"label" : "Faire un diagnostic des competences"},
                    "Découvrir le matériel et son environnement :" : {"label" : "Découvrir le matériel et son environnement :"},
                    "Ordinateurs" : {"label" : "Ordinateurs"},
                    "Smartphone" : {"label" : "Smartphone"},
                    "Tablette" : {"label" : "Tablette"},
                    "Périphériques (imprimantes, scanners, cles USB…)" : {"label" : "Périphériques (imprimantes, scanners, cles USB…)"},
                    "Creer et gerer son adresse mail" : {"label" : "Creer et gerer son adresse mail"},
                    "Gérer ses documents" : {"label" : "Gérer ses documents"},
                    "Écrire un document" : {"label" : "Écrire un document"},
                    "Gérer ses donnees numériques (mot de passe…)" : {"label" : "Gérer ses donnees numériques (mot de passe…)"},
                    "Naviguer sur des sites web" : {"label" : "Naviguer sur des sites web"},
                    "Rechercher des informations sur internet" : {"label" : "Rechercher des informations sur internet"}
                }
            },
            "Accès aux droits" : {
                "label":"Accès aux droits",
                "subList" : {
                    "Comprendre et utiliser France-Connect" : {"label" : "Comprendre et utiliser France-Connect"},
                    "Formation CAF" : {"label" : "Formation CAF"},
                    "Formation Pole Emploi" : {"label" : "Formation Pole Emploi"},
                    "Formation CARSAT" : {"label" : "Formation CARSAT"},
                    "Formation Logement" : {"label" : "Formation Logement"},
                    "Formation impots" : {"label" : "Formation impots"},
                    "Formation CPAM (Ameli)" : {"label" : "Formation CPAM (Ameli)"}
                }
            },
            "Éducation/aide à la parentalité" : {
                "label":"Éducation/aide à la parentalité",
                "subList" : {
                    "Formation Comprendre et utiliser un espace Numérique de Travail" : {"label" : "Formation Comprendre et utiliser un espace Numérique de Travail"}
                }
            },
            "Culture numérique" : {
                "label":"Culture numérique",
                "subList" : {
                    "Formation gérer son identité numerique" : {"label" : "Formation gérer son identité numerique"},
                    "Formation utiliser le numérique à des fins créatives et récréatives" : {"label" : "Formation utiliser le numérique à des fins créatives et récréatives"}
                }
            }
        }
    },
    "Accompagnement" : {
        "label":"Accompagnement",
        "subList" : {
            "Accès libre avec un accompagnement" : {"label" : "Accès libre avec un accompagnement"} ,
            "Accompagnement individuel sur rendez-vous" : {"label" : "Accompagnement individuel sur rendez-vous"},
            "Accompagnement individuel sans rendez-vous" : {"label" : "Accompagnement individuel sans rendez-vous"},
            "Accompagnement en groupe sur inscription" : {"label" : "Accompagnement en groupe sur inscription"},
            "Accompagnement en groupe sans inscription" : {"label" : "Accompagnement en groupe sans inscription"},
            "Accompagnement CAF" : {"label" : "Accompagnement CAF"},
            "Accompagnement Pôle Emploi" : {"label" : "Accompagnement Pôle Emploi"},
            "Accompagnement CARSAT" : {"label" : "Accompagnement CARSAT"} ,
            "Accompagnement Logement" : {"label" : "Accompagnement Logement"} ,
            "Accompagnement impôts" : {"label" : "Accompagnement impôts"} ,
            "Accompagnement CPAM (Ameli)" : {"label" : "Accompagnement CPAM (Ameli)"},
            "Atelier de réparation de matériel informatique" : {"label" : "Atelier de réparation de matériel informatique"}
        }
    },
    "Accès" : {
        "label":"Accès",
        "subList" : {
            "Réseau WiFi" : {"label" : "Réseau WiFi"},
            "Accès libre à du matériel informatique" : {"label" : "Accès libre à du matériel informatique"}
        }
    },
    "Sensibilisation" : {
        "label":"Sensibilisation",
        "subList" : {
            "culturenumerique" : {
                "label":"Culture numérique",
                "subList" : {
                    "Éducation aux médias et à l’informations" : {"label" : "Éducation aux médias et à l’informations"},
                    "Vie privée" : {"label" : "Vie privée"}
                }
            },
            "Éducation/aide à la parentalité" : {
                "label":"Éducation/aide à la parentalité",
                "subList" : {
                    "Sensibilisation : la parentalité a l’ère du numérique" : {"label" : "Sensibilisation : la parentalité a l’ère du numérique"},
                    "Sensibilisation : connaitre les pratiques des usages des jeunes" : {"label" : "Sensibilisation : connaitre les pratiques des usages des jeunes"}
                }
            }
        }
    }
};