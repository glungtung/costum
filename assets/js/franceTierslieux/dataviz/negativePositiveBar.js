var showNegativePositiveBar = function (containerChart) {
    var dataset = [
        {
          "MinVolatility": 2.0,
          "MaxVolatility": 12.0,
          "MaxLossPercent": 4.0,
          "MaxGainPercent": 5.0
        }
      ];
      
      var svg = d3.select("svg"),
          margin = {right: 120, left: 100},
          width = +svg.attr("width") - margin.left - margin.right,
          height = +svg.attr("height");
      
      var x = d3.scale.linear()
          .domain([2, 3])
          .range([0, (width / 2) + 30])
          .clamp(true);
      console.log(x);
      var y = d3.scale.linear()
          .domain([-4,5])
          .range([height/2, 0]);
      
      var slider = svg.append("g")
          .attr("class", "slider")
          .attr("transform", "translate(" + (margin.left + 3) + "," + height/1.1 + ")");
      
      var negPosHandle = slider.insert("circle", ".track-overlay")
          .attr("class", "negPosHandle")
          .attr("cx", 350)
          .attr("r", 18)
          .style('filter', 'url(#drop-shadow)')
          .style('stroke', 'transparent');
      
      var xAxis = svg.append("g")
          .attr("transform", "translate(" + margin.left + "," + height/2 + ")")
          .attr('class', 'axis-color')
          .call(d3.svg.axis().scale(x).ticks(0).outerTickSize(0));
      
      var positive = svg.selectAll('rect.positiveBar')
        .data(dataset)
        .enter()
          .append('rect')
          .attr('x', "400")
          .attr('y', height/4 - 13)
          .attr('fill', 'rgb(0, 131, 126)')
          .attr('width', (55))
          .attr('height', function(d) {return 140});
      
      var negative = svg.selectAll('rect.negativeBar')
        .data(dataset)
        .enter()
          .append('rect')
          .attr('x', "400")
          .attr('y', height/2 + 1)
          .attr('fill', 'rgb(116, 31, 24)')
          .attr('width', (55))
          .attr('height', function(d) {return 109})
          .attr('clip-path', 'url(#clipContainer)')
      
      var xText = svg.append("text")
          .attr("class", "nodetext")
          .attr("dx", 25)
          .attr("dy", ".35em");
      
      xText.append("tspan")
          .attr("dy", height/2 - 10)
          .attr("x",100)
          .text('+ Revenus')
          .attr("font-family", "Arial")
          .style('fill', '#00837e')
          .style("font-size", "30px")

        xText.append("tspan")
            .attr("dy", "1.3em")
            .attr("x",128)
            .text("- Dépenses")
            .attr("font-family", "Arial")
            .style('fill', '#741f18')
            .style("font-size", "30px");

        var clipContainer = svg.append('svg')
            .attr('width',  x.range()[1])
            .attr('height', height/2)
            .attr('x', margin.left - 15)
            .attr('y', height/4 - 14)
            .attr('viewBox', '0 0 100 100')
            .attr('preserveAspectRatio', 'none')
            .style('position', 'fixed')

        var chart = $(".main-vis"),
            aspect = chart.width() / chart.height(),
            container = chart.parent();
        $(window).on("resize", function() {
        var targetWidth = container.width();
        chart.attr("width", targetWidth);
        chart.attr("height", Math.round(targetWidth / aspect));
        }).trigger("resize");

}