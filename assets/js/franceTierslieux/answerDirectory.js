
var filterDropdown = [];
var Obj = {
    events : function(cobj){

    },
    prepareData : function(params){
        var results = {
            reseau : notEmpty(params?.links?.organizations) ? Object.values(params?.links?.organizations)[0].name : null,
            reseauId : notEmpty(params?.links?.organizations) ? Object.keys(params?.links?.organizations)[0] : null,
            created : ucfirst(moment.unix(params.created).format('dddd Do MMMM YYYY h:mm:ss')),
            creator : params.user
        }
        return results;
    }
}


directory.modalPanel = function(params){
    let nameAndImageUrl = params[0];
    let organisationArrayId = [];
    let network = "";
    params = params[1];
    let answerId = params["_id"]["$id"];
    let likeNumber = params?.voteCount?.like;
    let fillHeart = (params?.vote != undefined && Object.keys(params.vote).length > 0 && typeof userId != "undefined") ? (Object.keys(params.vote).includes(userId)) ? true : false : false;
    likeNumber = (likeNumber == undefined) ? 0 : likeNumber;
    if (exists(params?.links?.organizations))
        organisationArrayId = Object.keys(params.links.organizations);
    if ( typeof params.answers != "undefined" && typeof params.answers['franceTierslieux2022023_753_10'] != "undefined" && typeof params.answers['franceTierslieux2022023_753_10']['franceTierslieux2022023_753_10lemfa0njory1oxpszb'] != "undefined") {
        network = params.answers['franceTierslieux2022023_753_10']['franceTierslieux2022023_753_10lemfa0njory1oxpszb'];
    }
    var params = Obj.prepareData(params);   
    // let imagePath = (organisationArrayId.length != 0 && getNameOrUrl("organizations",organisationArrayId[0]) != "") ? getNameOrUrl("organizations",organisationArrayId[0]) : (exists(params?.answers?.aapStep1?.images) && exists(params.answers?.aapStep1?.images[0])) ? params.answers?.aapStep1?.images[0] : defaultImage;
    let imagePath = (exists(params?.answers?.aapStep1?.images) && exists(params.answers?.aapStep1?.images[0])) ? params.answers?.aapStep1?.images[0] : defaultImage;
    imagePath = (typeof nameAndImageUrl.profilImageUrl["profilImageUrl"] != "undefined") ? nameAndImageUrl.profilImageUrl["profilImageUrl"] : imagePath;
    let personWhoAnswered = (typeof nameAndImageUrl.nameUser["name"] != "undefined") ? "Par " + nameAndImageUrl.nameUser["name"] : "";
    var str = `
        <div class="team">
            <div class="card-team">
                <div class="img-box">
                    <img class="img-responsive-action" alt="Responsive Team Profiles" src="${imagePath}" />
                </div>
                <h1>
                    <a href="javascript:" class="${ (params.reseau != null) ? params.reseau.replace(" ", "-") : "" }" onclick='showAllNetwork.loadPreview("${answerId}","${imagePath}","${params?.reseau?.replace("'", "#")}","${organisationArrayId[0]}","tiersLieux", "${network}")'>
                        ${ params.reseau }
                    </a>
                </h1>
                <h2>
                    <a href="javascript:" onclick="showAllNetwork.loadPreview('','${defaultImage}','${network}','' ,'reseaux')" style="text-decoration: none;" target="_blank">
                        ${network}
                    </a>
                </h2>
  
                <hr />
                <p class="text-left desc">${params.created}<br>
                    <span class="">${personWhoAnswered}</span>
                </p>
                <div class="btn-group btn-group-xs pull-right like-project-container">
                    <a style="cursor: pointer;color:red;" class="like-orga-${answerId}" onclick="showAllNetwork.likeThis('${answerId}','.like-orga-${answerId}')"> ${(likeNumber == 0) ? '<i style="font-size: 33px;" class="fa fa-heart-o"></i>' : (fillHeart) ?  '<i style="font-size: 33px;" class="fa fa-heart"></i> ' + likeNumber : '<i style="font-size: 33px;" class="fa fa-heart-o"></i> ' + likeNumber} </a>
                </div>
   
            </div>
        </div>
    `;
    if (params.reseau == "null" || params.reseau == null) {
        str = "";
    }
    return str;
}

var showAllNetwork = {
    likeThis : function (orgaId="", selector="") {
        if (orgaId != null && orgaId != undefined) {
            var params = {
                "action" : "vote",
                "collection" : "answers",
                "id" : orgaId,
                "details" : {
                    status : "like"
                }
            };
            $.ajax({
                url: baseUrl+'/'+moduleId+"/action/addaction/",
                data: params,
                type: 'post',
                global: false,
                dataType: 'json',
                success: 
                    function(data) {
                        if (data.result && selector != "") {
                            let dataVote = data?.element?.vote
                            if (dataVote != undefined && typeof dataVote == 'object' && Object.keys(dataVote).length > 0) {
                                let heatIcon = (typeof userId != "undefined" && Object.keys(dataVote).includes(userId)) ? '<i style="font-size: 33px;" class="fa fa-heart"></i>' : '<i style="font-size: 33px;" class="fa fa-heart-o"></i>';
                                $(selector).html(heatIcon + ' ' + Object.keys(dataVote).length);
                            } else {
                                $(selector).html('<i style="font-size: 33px;" class="fa fa-heart-o"></i>');
                            }
                        }
                    },
                error: 
                    function(data) {
                        toastr.error("Error calling the serveur : contact your administrator.");
                    }
            });         
        }
    },
    initView : function (title="", footer="", col="", isTiers="fasle") {
        let initView = `
            <div class="row">
                <h2 class="text-left"> ${(title != "") ? title : (isTiers == "true")? 'Les tiers-lieux' : 'Les réseaux'} </h2>
                <hr />
            </div>
            <div class="${(col == "") ? "col-md-12" : col} no-padding">
                <div id='filterContainercacs' class='searchObjCSS'></div>
                <div class='headerSearchIncommunity no-padding col-xs-12'>
                    <div id="activeFilters" class="col-xs-12 no-padding"></div>
                </div>
                <div class='bodySearchContainer margin-top-30'>
                    <div class='no-padding col-xs-12' id='dropdown_search'>
                    </div>
                </div>
                <div class='padding-top-20 col-xs-12 text-left ${(footer != "") ? footer : "footerSearchContainer"}'></div>
            </div>
        `;
        return initView;
    },
    paramsFilters : {
        container : "#filterContainercacs",
        interface : {
            events : {
                // page : true,
                scroll : true,
                //scrollOne : true
            }
        },
        header : {
            dom : ".headerSearchIncommunity",
            options : {
                left : {
                    classes : 'col-xs-8 elipsis no-padding',
                    group:{
                        count : true,
                        types : true
                    }
                }
            },
        },
        defaults : {
            notSourceKey : true,
            textPath : "answers.aapStep1.titre",
            types : ["answers"],
            indexStep: 25,
            forced: {
                filters: {
                }
            },
            fields:[],
            sortBy:{updated : -1}
            },
        results : {
            dom:".bodySearchContainer",
            smartGrid : true,
            renderView :"directory.PanelHtml",
            map : {
                active : false
            },
            smartGrid: true,
            events : function(fObj){
                $(fObj.results.dom+" .processingLoader").remove();
                Obj.events(Obj);
                coInterface.bindLBHLinks();
                directory.bindBtnElement();
                coInterface.bindButtonOpenForm();
            },
        },
        filters : {
            text : true,
        }
    },
    showElements : function (title,footer) {
        $(".ans-dir").html(showAllNetwork.initView(title,footer, "col-md-9 tiersLieuxContent", "true"));
        var filter = searchObj.init(showAllNetwork.paramsFilters);
        filter.search.init(filter);
    },
    loadPreview : function (answerId="", answerImage="", answerName="", orgaId="", page="", network="", callBack=()=>{}) {
        let previewData = {};
        let inputsWithTitle = {};
        let groupAnswers = {};
        let countValues = {};

        previewData ["id"] = answerId;
        previewData ["image"] = answerImage;
        previewData ["name"] = answerName;
        previewData ["orgaId"] = orgaId;
        previewData ['page'] = page;
        previewData ['network'] = network;
        previewData ['pathSectionTitle'] = {};
        previewData ['economicModelAnswer'] = {};
        previewData ['allTiers'] = {};
        previewData ['markerData'] = {};
        previewData ['supplAnswers'] = {};

        if (page == "tiersLieux") {
            previewData['sections'] = {
                info : "Information générale",
                activity : "Activités proposées",
                principleAndValue : "Valeurs et principes",
                equipment : "Types équipements",
                domain : "Domaines",
                frequentation : "Fréquentation",
                ageFrequentation : "Fréquentation par âge",
                employeeContract : "Ressource humaines",
                gouvernance : "Gouvernance",
                numericUsage : "Outils numériques",
                "Modèle économique" : {
                    createValue: "Creation de valeur",
                    distValue : "Distribution de valeur",
                    captValue : "Capture de valeur",
                    tiersHealth : "Santé du tiers lieux",
                    resume : "Résumé"
                },    
                reseaux : "reseaux"
            }
        }

        if (page == "reseaux") {
            let economicModelSection = { 
                    "Modèle économique" : {
                    createValue: "Creation de valeur",
                    distValue : "Distribution de valeur",
                    captValue : "Capture de valeur",
                    // tiersHealth : "Sante du tiers lieux",
                    resume : "Résumé"
                }   
            }

            inputsWithTitle = { 
                "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab": "Cartographie des activités",
                "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op" : "Cartographie des valeurs" ,
                "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4" : "Cartographie des équipements",
                // "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q"    
                "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gowlj39f2a2e" : "Fréquentation par catégorie sociale",
                "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v" : "Fréquentation par âge",
                "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout8liagfm99v2r" : "Metiers pour faire fonctionner le réseau (Ressources humaines)",
                "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt" : "Les Contrats du réseau (Ressources humaines)"
                // "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt",  
                // "answers.franceTierslieux1922023_1417_7.multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes",   
            }
            let dataAnswers = {};
            let params = {
                show: [
                    "links.organizations", 
                    "_id",   
                ],
                inputs : inputsWithTitle,     
                where : {
                    "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" : answerName,
                }
            };
            previewData ['pathSectionTitle'] = inputsWithTitle;
            dataAnswers = showAllNetwork.getAnswers("/form/63e0a8abeac0741b506fb4f7", params).allAnswers;
            groupAnswers = showAllNetwork.groupAnswers(dataAnswers, inputsWithTitle);
            countValues = showAllNetwork.countObjectValues(groupAnswers);
            previewData['sections'] = {reseaux: "Le réseau", ...countValues, ...economicModelSection};
        }
        urlCtrl.openPreview('view/url/costum.views.custom.franceTierslieux.answerPreview', previewData)
        callBack()
    },
    groupAnswers : function (allAnswers={}, inputs) {
        if (Object.keys(allAnswers).length > 0) {
            $.each(allAnswers, function(key, value) {
                if (Object.keys(value).length > 0) {
                    $.each(value.answers, function(inputKey, inputValue) {
                        Object.keys(inputs).map(function(num){
                            let element = num.split(".")[2];
                            if (inputValue[element] != undefined) {
                                if (typeof allAnswers[element] == "undefined")
                                    allAnswers[element] = [];
                                allAnswers[element] = [ ...allAnswers[element], ...inputValue[element]];
                            }
                        })        
                    })
                }

            })
        }  
        mylog.log("sdoivhos", allAnswers);
        return allAnswers;
    },
    countObjectValues : function (obj={}) {
        const result = {};
        if (Object.keys(obj).length > 0) {
            for (const key in obj) {
                if (Array.isArray(obj[key])) {
                    const count = {};
                    for (const item of obj[key]) {
                        if (typeof item == "string") {
                            const value = item;
                            count[value] = (count[value] || 0) + 1;
                        } else {
                            for (const prop in item) {
                                const value = item[prop].value;
                                count[value] = (count[value] || 0) + 1;
                            }
                        }
                    }
                    result[key] = count;
                }
            }
        }     
        return result;
    },
    showModal : function (networkName="", filterField = "") {
        smallMenu.open(`<div id="tiers-modal" class="container padding-bottom-20 padding-left-0 padding-right-0 ans-dir"></div>`);
        $("#tiers-modal.ans-dir").html(showAllNetwork.initView(networkName,"searchModalFooter"));
        let modalParams = showAllNetwork.paramsFilters;
        initPagination = $(".reseauxFooter").html();
        modalParams.filters['networkFilter'] = {
            name : "filtre par réseau",
            view : "dropdownList",
            type : "filters",
            field : filterField,
            event : "filters",
            keyValue : false,
            list : showAllNetwork.arrayToObject(filterDropdown),
        }
        modalParams.results.renderView = "directory.modalPanel";
        modalParams['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7/filterOrga/true";
        modalParams.container = "#tiers-modal #filterContainercacs";
        modalParams.header.dom = "#tiers-modal .headerSearchIncommunity";
        modalParams.results.dom = "#tiers-modal .bodySearchContainer";
        searchObj.footerDom = "#tiers-modal .searchModalFooter";
        let modalFilter = searchObj.init(modalParams);
        modalFilter.search.init(modalFilter);
        setTimeout(() => { $('[data-key="'+networkName+'"]').trigger('click');}, 100);
    },
    arrayToObject : function (arr=[]) {
        return arr.reduce(function(obj, key) {
            obj[key] = key;
            return obj;
        }, {});
    },
    getAnswers : function (path="", params)  {
        let results = {}
        if (path != "") {
            ajaxPost(
                null,
                baseUrl + '/costum/francetierslieux/getallanswers/type/answers' + path,
                params,
                function (data) {   
                    if (data.result) { 
                        results = data;
                    } 
                },
                null,
                "json",
                { async: false }     
            );
        }
        return results;
    },
    markerParams : function (network) {
        let params =
        { 
            where: {
            "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" : network
            },
            arrayFormPath : {
                "645b300b6d70bb35426fc0e3" : "answers.lesCommunsDesTierslieux1052023_949_0.finderlesCommunsDesTierslieux1052023_949_0lat3sl0xwwg06k856jc", 
                "6486d24e9cad105cbf29a777" : "answers.lesCommunsDesTierslieux1262023_1215_0.finderlesCommunsDesTierslieux1262023_1215_0lix46h1uhnukfgtzj6s", 
                "646498a5f2b3d6423d74b7f6" : "answers.lesCommunsDesTierslieux1752023_1312_0.finderlesCommunsDesTierslieux1752023_1312_0lix4a7257iht8lp282r"
            },
            showAnswers : "true"
        }
        return params;
    },
    getElement : function (type, id) {
        let result = {};
            ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/element/get/type/"+type+"/id/"+id+"",
                {},
                function (data) {   
                    if (data.result) {
                        result = data.map;
                    } 
                },
                null,
                "json",
                { async: false }     
            );
        return result;
    }
}


