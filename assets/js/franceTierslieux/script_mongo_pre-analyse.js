    //generateur de preAnalyse 
    var me = db.getCollection('citoyens').findOne({ slug: "devchriston" }, { _id: 1 })
    var slug = "franceTierslieux"
    var orga = db.getCollection('organizations').findOne({ slug: slug })
    var parentObj = {}
    parentObj["" + orga._id] = {
        type: "organizations",
        name: orga.name
    }
    var pageSlug = "testib"
    var ctxt = {
        page: pageSlug,
        blockParent: "646ee704ba19771879409ea3",
        coform: "63e0a8abeac0741b506fb4f7",
        creator: "" + me._id,
        parent: parentObj,
        source: {
            "insertOrign": "costum",
            "key": slug,
            "keys": [
                slug
            ]
        }
    };

    var globalListInputs = ["tpls.forms.cplx.multiCheckboxPlus", "tpls.forms.cplx.multiRadio", "tpls.forms.cplx.radiocplx", "tpls.forms.cplx.checkboxcplx"]
    var listInputs = ["tpls.forms.cplx.radioNew", "tpls.forms.cplx.checkboxNew"]

    var bcmsTypes = {
        bar: "tpls.blockCms.graph.chartForCoform",
        //doughnut: "tpls.blockCms.graph.chartForCoform",
        doughnut: "tpls.blockCms.graph.progressCircleMultiple",
        pie: "tpls.blockCms.graph.chartForCoform",
        line: "tpls.blockCms.graph.chartForCoform",
        percent: "tpls.blockCms.graph.progressCircle",
        text: "tpls.blockCms.graph.textWithValue",
        number: "tpls.blockCms.graph.textWithValue",
        "manuel": "tpls.blockCms.graph.textWithValue",
        "manuel-doughnut": "tpls.blockCms.graph.progressCircleMultiple",
        "manuel-number": "tpls.blockCms.graph.textWithValue",
        titleh3: "tpls.blockCms.superCms.elements.supertext",
        titleh4: "tpls.blockCms.superCms.elements.supertext",
        manuel: "tpls.blockCms.superCms.elements.supertext"
    }

    var bcmsParams = {
        "chartForCoform": {
            completeColor: "#f0506a",
            legendPosition: "right",
            percentColor: "white",
            showLegend: true
        },
        "progressCircleMultiple": {
            completeColor: "#c7d326",
            emptyColor: "#ddd",
            percentColor: "#c7d326",
            titleBottom: "&nbsp;",
            titleTop: "&nbsp;",
        },
        "progressCircle": {
            completeColor: "#c7d326",
            emptyColor: "#ddd",
            percentColor: "#c7d326",
            titleBottom: "&nbsp;",
            titleTop: "&nbsp;",
        },
        "textWithValue": {
            roundMillionValue: true,
            valueColor: "#9b6fac",
            withStaticTextLeft: false,
            withStaticTextRight: true
        }
    }
    var blockList = [];

    var coform = db.getCollection('forms').findOne({ _id: ObjectId(ctxt.coform) });
    var subFormsInputs = {};
    db.getCollection('forms').find( {id:{$in:coform.subForms}} ).forEach(function(f){
        subFormsInputs[""+f.id] = f.inputs;
    })

    db.getCollection('ftl').find({ name: { $ne: "" } }).forEach(function (line) {
        var o = {
            name: line.name,
        }
        //print(line.name)
        if (line.answerPath){
            o.answerPath = line.answerPath;
            
            const inputKey = line.answerPath.split(".")[1];
            if (typeof inputKey!="undefined" && (inputKey.includes("radioNew") || inputKey.includes("checkboxNew") || inputKey.includes("multiRadio") || inputKey.includes("multiCheckboxPlus"))) {
                
                if (globalListInputs.indexOf(line.inputType) >= 0 && coform.params[line.answerPath.split(".")[1]]){
                    o.answerValue = coform.params[line.answerPath.split(".")[1]].global.list
                }else if (listInputs.indexOf(line.inputType) >= 0 && coform.params[line.answerPath.split(".")[1]]){
                    o.answerValue = coform.params[line.answerPath.split(".")[1]].list
                } 
            }
        }
        
        if(line.unit && line.unit!=""){
            o.unit=line.unit;
        }

        if (line.type && bcmsTypes[line.type]) {

            o.path = bcmsTypes[line.type];

            if (o.path == "tpls.blockCms.graph.chartForCoform")
                o.chartType = line.type
            else if (o.path == "tpls.blockCms.superCms.elements.supertext") {
                if (line.type == "titleh3")
                    o.valueFontSize = "50"
                else if (line.type == "titleh4")
                    o.valueFontSize = "30"
            }

            var bcmsName = o.path.split(".")
            bcmsName = bcmsName[bcmsName.length - 1]
            //o.bcmsName = bcmsName;
            if (bcmsName && bcmsParams[bcmsName]) {
                Object.keys(bcmsParams[bcmsName]).forEach(function (p) {
                    o[p] = bcmsParams[bcmsName][p]
                })
            }
        }
        try{
            if (line.answerValue != "") {
                o.answerValue = line.answerValue.split(";");
            }
        }catch(e){
            o.answerValue = line.answerValue;
        }
        
        if ( line.inputType != "" && typeof line.inputType != "undefined" ) {
            o.inputType = line.inputType;
        }

        
    /*
        if(line.answerValue==""){
            // à dynamiser
            if(typeof line.answerPath !="undefined" && line.answerPath!=""){
                var av = line.answerPath.split("."); // .replace(/(checkboxNew)|(radioNew)|(multiCheckboxPlus)|(multiRadio)/, "")
                if(av[0] && av[1] && coform.params[av[1]]){
                    line.answerValue = coform.params[av[1]]
                }
            }
        }*/

        blockList.push(o)
    })

    var ct = 0
    var allCMS = []
    blockList.forEach(function (bcms) {

        var titleBCms = {
            "path": "tpls.blockCms.superCms.elements.supertext",
            "page": ctxt.page,
            "order": "0",
            "subtype": "undefined",
            "blockParent": ctxt.blockParent,
            "parent": ctxt.parent,
            "collection": "cms",
            "modified": ISODate("2023-05-23T15:06:56.000Z"),
            "updated": 1684854710,
            "creator": ctxt.creator,
            "created": 1684854416,
            "source": ctxt.source,
        }
        
        //titre de section ou du graph
        var fontSize = (bcms.valueFontSize) ? bcms.valueFontSize : 20;
        var titleStyle = "margin-top:20px;";
        var anchorSelector = "";
        if(fontSize > 30){
            titleStyle = "margin-top:40px;background:#ddd;"
            anchorSelector = "id='title"+parseInt(ct)+"' class='pa-title'"
        }
        
        if(ctxt.position){
            ct = ctxt.position;
        }
        titleBCms.position = ct
        titleBCms.text = "<div "+anchorSelector+" style=\""+titleStyle+"text-align:center;font-size:" + fontSize + "px;\"><b style=\"font-size:" + fontSize + "px;\">" + bcms.name + "</b></div>";
        db.getCollection('cms').insert(titleBCms)
        if(fontSize  > 30 ){
            var imageBCms = {
                "path": "tpls.blockCms.superCms.elements.image",
                "page": ctxt.page,
                "order": "0",
                "subtype": "undefined",
                "blockParent": ctxt.blockParent,
                "parent": ctxt.parent,
                "collection": "cms",
                "modified": ISODate("2023-05-23T15:06:56.000Z"),
                "updated": 1684854710,
                "creator": ctxt.creator,
                "created": 1684854416,
                "source": ctxt.source,
                "image" : "/upload/communecter/organizations/"+orga._id+"/album/pastedimage.png",
                "css" : {
                    "width":"100%",
                    "objectFit": "contain"
                },
                "position":parseInt(ct)
            }
            db.getCollection('cms').insert(imageBCms)
        }
        allCMS.push(titleBCms)
        ct++

        if (bcms.path != "tpls.blockCms.superCms.elements.supertext") {
            var graphBcms = {
                "collection": "cms",
                "created": 1683094620,
                "creator": ctxt.creator,
                "modified": ISODate("2023-05-03T06:17:00.000Z"),
                "parent": ctxt.parent,
                "source": ctxt.source,
                "type": "blockChild",
                "updated": 1683101166,
                "page": ctxt.page,
                "blockParent": ctxt.blockParent,
                "coform": ctxt.coform
            }
            Object.keys(bcms).forEach(function (k) {
                graphBcms[k] = bcms[k];
            })
            graphBcms.position = ct
            db.getCollection('cms').insert(graphBcms)
            allCMS.push(graphBcms)
            ct++
        }

    })

    blockList
    allCMS