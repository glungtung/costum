pageProfil.initCallB = function(){
	mylog.log("pageProfil.initCallB", contextData);
	if(typeof contextData.source != "undefined" &&
		contextData.source != null &&
		contextData.source.toBeValidated != "undefined" &&
		contextData.source.toBeValidated != null &&
		contextData.source.toBeValidated.hva != "undefined" &&
		contextData.source.toBeValidated.hva  != null &&
		contextData.source.toBeValidated.hva == true){
		mylog.log("pageProfil.initCallB if", contextData);
		$('#menu-top-btn-group [data-action="create"]').hide();
	}
}


pageProfil.views.detail = function(){
	mylog.log("pageProfil.views.detail");
	var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url+'?tpl=ficheInfoElement', null, function(){
		if(contextData.type == "events"){
			$("#divTypeAbout").hide();
		}
		$("#divParentAbout").hide();
		$("#divDiaspora").hide();
		$("#divMastodon").hide();
		$("#divGpplus").hide();
		$("#divGithub").hide();

	},"html");
}


pageProfil.views.directory = function(){
	mylog.log("hva pageProfil.views.directory");
	var dataIcon = (!notEmpty(pageProfil.params.dir)) ? "users" : $(".smma[data-type-dir="+pageProfil.params.dir+"]").data("icon");
	pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[contextData.type] : pageProfil.params.dir;
	var sub=(!notEmpty(pageProfil.params.sub)) ? "" : "/sub/"+pageProfil.params.sub;

	var sort = ( ( pageProfil.params.dir == "events" ) ? "/sort/1" : "" );


	getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
				'/id/'+contextData.id+'/dataName/'+pageProfil.params.dir+sub+sort+'?tpl=json',
				function(data){ 
					var type = ($.inArray(pageProfil.params.dir, ["poi","ressources","vote","actions","discuss"]) >=0) ? pageProfil.params.dir : null;
					mylog.log("pageProfil.views.directory canEdit" , canEdit);
					if(typeof canEdit != "undefined" && canEdit)
						canEdit=pageProfil.params.dir;
					mylog.log("pageProfil.views.directory edit" , canEdit);
					displayInTheContainer(data, pageProfil.params.dir, dataIcon, type, canEdit);
					if(typeof mapCO != "undefined"){
						mapCO.clearMap();
			            mapCO.addElts(data);
			            mapCO.getMap().invalidateSize();
					}
					coInterface.bindButtonOpenForm();

					if(typeof callBack != "undefined" && callBack != null)
						callBack();
				}
	,"html");
}