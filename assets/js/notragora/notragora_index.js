function replaceMulti(name){
    name.replace("-"," ");
    if(name.indexOf("-")==true){
        replaceMulti(name);
    }else{
        return name ;
    }
}

urlCtrl.loadableUrls["#collection"] = { subdomain: "collection", subdomainName: "Collections",action:function(hash){costum.notragora.loadUrl(hash)}};
		urlCtrl.loadableUrls["#genre"] = { subdomain: "genre", subdomainName: "Genres",action:function(hash){costum.notragora.loadUrl(hash)}};
		urlCtrl.loadableUrls["#poi"] = { subdomain: "poi", subdomainName: "Productions",action:function(hash){costum.notragora.loadUrl(hash)}};
		urlCtrl.shortVal = ["p","s","o","e","pr","c","cl"/* "s","v","a", "r",*/];
		urlCtrl.shortKey = [ "citoyens","siteurl","organizations","events","projects" ,"cities" ,"classifieds"/*"entry","vote" ,"action" ,"rooms" */];

costum.notragora={
	floopContactTypes : [ 
		{ name : "organizations", 	color: "green" 	, icon:"group", label : "Groupe de travail & Partenaires" },
		{ name : "citoyens",  		color: "yellow"	, icon:"user"			},
		{ name : "events", 			color: "orange"	, icon:"calendar"		} 
	],
	poi : {
		afterSave : function(data){
			//if(dyFObj.editMode){
				dyFObj.closeForm();
				getPoiNA();
		}
	},
	badges : {
		afterSave : function(data){
			
			//uploadObj.gotoUrl = null;
			// if(dyFObj.editMode){
			// 	if($.inArray(data.map.category, ["collections", "genres"]) >= 0){
			// 		oldName=costum.badges[data.map.category][data.id].name;
			// 		if(data.map.name!=oldName){
			// 			var idB = $.inArray(data.map.name, costum.lists[data.map.category] ) ;
			// 			if(idB > -1){
			// 				costum.lists[data.map.category][idB]=data.map.name;
			// 			}
			// 			costum.badges[data.map.category][data.id]=data.map;
			// 		}
			// 	}
			// } else{
			// 	if($.inArray(data.map.category, ["collections", "genres"]) >= 0){
			// 		costum.lists[data.map.category].push(data.map.name);
			// 		costum.badges[data.map.category][data.id]=data.map;
			// 	}
			// }
			dyFObj.commonAfterSave();
			urlCtrl.loadByHash(location.hash);
		}
	},
	loadUrl : function(hash,returnUrl=false){
		mylog.log("loadUrl notragora", hash);
		var splitUrl = hash.split(".");
		var collec=(splitUrl[0]=="#poi") ? "poi" : "badges";

			
			var colName= (collec=="poi") ? splitUrl[1] : replaceMulti(splitUrl[1]);
			ajaxPost(
				null,
				baseUrl+"/costum/notragora/getcollection/type/"+collec,
				{name:colName},
				function(data){
					mylog.log("callback loadUrl",data);
					if(data!=null){
					var url="#page.type."+collec+".id."+data._id.$id;
					if(!returnUrl){
						if(data._id)
							urlCtrl.jsController(url);
					}else{
						return url;
					}
				    }
				    else{
				    	return urlCtrl.loadByHash("#welcome");
				    }
				}
			);
		
		
	},
	init : function(){
		mylog.log("init NA");
		$("#modalLogin > div > div.container.col-lg-offset-3.col-sm-offset-2.col-lg-6.col-sm-8.col-xs-12 > div.col-xs-12.text-left > label:nth-child(1)").html("<i class='fa fa-envelope'></i> Email / Nom d'utilisateur ");
		urlCtrl.loadableUrls["#collection"] = { subdomain: "collection", subdomainName: "Collections",action:function(hash){costum.notragora.loadUrl(hash)}};
		urlCtrl.loadableUrls["#genre"] = { subdomain: "genre", subdomainName: "Genres",action:function(hash){costum.notragora.loadUrl(hash)}};
		urlCtrl.loadableUrls["#poi"] = { subdomain: "poi", subdomainName: "Productions",action:function(hash){costum.notragora.loadUrl(hash)}};
		urlCtrl.shortVal = ["p","s","o","e","pr","c","cl"/* "s","v","a", "r",*/];
		urlCtrl.shortKey = [ "citoyens","siteurl","organizations","events","projects" ,"cities" ,"classifieds"/*"entry","vote" ,"action" ,"rooms" */];
	},
	autoCompleteSearchGS : function(search, indexMin, indexMax, input, callB){
		mylog.log("globalsearch.js autoCompleteSearchGS", search, indexMin, indexMax, input, callB);

		var data = {"name" : search, "locality" : "", "searchType" : searchTypeGS,
		"indexMin" : indexMin, "indexMax" : indexMax  };

		if(!notNull(input)){
			data.indexStep=0;
			data.count=true;
			data.countType = [ "organizations", "poi" ];
			data.searchType = [ "organizations", "poi" ];
		}

		indexMax = 0;
		
		if(typeof costum != "undefined" && notNull(costum) && typeof costum.filters != "undefined" && (!notNull(input) || $.inArray(input, ["#filter-scopes-menu", "#scopes-news-form"]) < 0)){
			if(typeof costum.filters.searchTypeGS != "undefined" && !notNull(input)){ 
				data.countType = costum.filters.searchTypeGS;
				data.searchType = costum.filters.searchTypeGS;
			}
			if(typeof costum.filters.sourceKey != "undefined"){ 
				data.sourceKey=[costum.slug];
			}
		}

		var domTarget = (notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
		var dropDownVisibleDom=(notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
		 if($(domTarget+" .content-result").length > 0)
	        domTarget+=" .content-result";
	             
		showDropDownGS(true, dropDownVisibleDom);
		if(indexMin > 0)
			$("#btnShowMoreResultGS").html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...");
		else{
			$(domTarget).html("<h5 class='text-dark center padding-15'><i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...</h5>");  
		}

	//	showIsLoading(true);

		if(search.indexOf("co.") === 0 ){
			searchT = search.split(".");
			if( searchT[1] && typeof co[ searchT[1] ] == "function" ){
				co[ searchT[1] ](search);
				return;
			} else {
				co.mands();
			}
		}
		mylog.log("globalsearch.js autoCompleteSearchGS data", data);
		ajaxPost("", 
			baseUrl+"/" + moduleId + "/search/globalautocomplete", 
			data, 
			function(data){
				spinSearchAddon();
				if(!data){ toastr.error(data.content); }
				else{
					mylog.log("costum globalsearch.js autoCompleteSearchGS", data);

					var countData = 0;
					if(typeof data.count != "undefined")
						$.each(data.count, function(e, v){countData+=v;});
					else
						$.each(data.results, function(i, v) { if(v.length!=0){ countData++; } });

					totalDataGS += countData;

					str = "";
					var city, postalCode = "";

					if(totalDataGS == 0)      totalDataGSMSG = "<i class='fa fa-ban'></i> "+trad.noresult;
					else if(totalDataGS == 1) totalDataGSMSG = totalDataGS + " "+trad.result;   
					else if(totalDataGS > 1)  totalDataGSMSG = totalDataGS + " "+trad.results;   

					if(totalDataGS > 0){
						str += '<div class="text-left col-xs-12 padding-10" id="footerDropdownGS" style="">';
							str += "<label class='text-dark margin-top-5'><i class='fa fa-angle-down'></i> " + totalDataGSMSG + "</label>";
						str += '</div>';
						str += "<hr style='margin: 0px; float:left; width:100%;'/>";
					}
	              	//parcours la liste des résultats de la recherche
					$.each(data.results, function(i, o) {
						mylog.log("costum globalsearch.js autoCompleteSearchGS results: ", o);
						var typeIco = i;
						var ico = "fa-"+typeObj["default"].icon;
						var color = mapColorIconTop["default"];

						var bg = "bg-green";
						if(o.collection == "poi")
							bg = "bg-green-poi";

						var url = (notEmpty(o.collection) && notEmpty(o._id)) ? '#page.type.'+o.collection+'.id.' + i : "";
						city="";
						postalCode="";
						if(typeof o.address  != "undefined" && o.address != null) {
							if(typeof o.address.addressLocality  != "undefined" && o.address.addressLocality != null)
								city = o.address.addressLocality;
							if(typeof o.address.postalCode  != "undefined" && o.address.postalCode != null)
								postalCode = o.address.postalCode ;
						}
						str += '<div class="col-md-12 col-sm-12 col-xs-12 no-padding searchEntity">'+
									'<div class="col-md-2 col-sm-2 col-xs-2 no-padding entityCenter text-center">'+
										'<a href="'+url+'" class="lbh">'+
											'<img width="80" height="80" class="img-circle '+bg+'" src="'+o.profilThumbImageUrl+'">'+
										'</a>'+
									'</div>'+
									'<div class="col-md-10 col-sm-10 col-xs-10 entityRight">'+
										'<a href="'+url+'" class="lbh entityName text-dark">'+
											o.name+
										'</a>';
										if(postalCode != "" && city != ""){
											str += 	'<a href="'+url+'" class=" lbh entityLocality">'+
														'<i class="fa fa-home"></i> '+postalCode+' '+city+
													'</a>';
										}
										
						str += 		'</div>'+
								'</div>';
						
					}); //end each

	              // extendMsg=trad.extendedsearch;
	              // extendUrl="#search";
	              // if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchOpenMenu != "undefined"){
	              // 	extendMsg=costum.searchOpenMenu.msg;
	              // 	extendUrl=costum.searchOpenMenu.url;
	              // }

	              //on ajoute le texte dans le html
	              $(domTarget).html(str);
	              //on scroll pour coller le haut de l'arbre au menuTop
	              $(domTarget).scrollTop(0);
	              
	              //on affiche la dropdown
	              showDropDownGS(true, dropDownVisibleDom);
	              bindScopesInputEvent();
	              if(notEmpty(callB)){
	              	callB();
	              }

	              coInterface.bindLBHLinks();

	            //signal que le chargement est terminé
	            mylog.log("loadingDataGS false");
	            loadingDataGS = false;

	          }

				//si le nombre de résultat obtenu est inférieur au indexStep => tous les éléments ont été chargé et affiché
				if(indexMax - countData > indexMin){
					$("#btnShowMoreResultGS").remove(); 
					scrollEndGS = true;
				}else{
					scrollEndGS = false;
				}
			}, 
			function (data){
				mylog.log("error"); mylog.dir(data);          
			}, 
			null);
	}
};

// costum.loadByHash = function(oldHash){
// 	mylog.log("costum.loadByHash start", oldHash);
// 	var hash = "";
// 	if(typeof oldHash == "undefined"){
// 		hash = "";
// 	} else if(oldHash.indexOf("#element.detail.type") >= 0){
// 		mylog.log("costum.loadByHash if 1");
// 		var splitHash = oldHash.split(".");
// 		hash = "#page.type."+splitHash[3]+".id."+splitHash[5];
// 	} else if(oldHash.indexOf("#person.detail.id") >= 0){
// 		mylog.log("costum.loadByHash if 1");
// 		var splitHash = oldHash.split(".");
// 		hash = "#page.type.citoyens.id."+splitHash[3];
// 	}
// 	else if(oldHash.indexOf("#default.directoryjs?type=organizations") >= 0){
// 		mylog.log("costum.loadByHash if 2");
// 		hash = "#search";
// 	} else if(oldHash.indexOf("#default.directoryjs?type=poi") >= 0){
// 		mylog.log("costum.loadByHash if 2");
// 		hash = "#production";
// 	}  else if(oldHash.indexOf("#default.apropos") >= 0){
// 		mylog.log("costum.loadByHash if 2");
// 		hash = "#apropos";
// 	} else if(oldHash.indexOf("#default.home") >= 0){
// 		mylog.log("costum.loadByHash if 2");
// 		hash = "#welcome";
// 	// } 
// 	// else if(oldHash.indexOf("#poi") >= 0 || oldHash.indexOf("#collection") >= 0 || oldHash.indexOf("#genre") >= 0){
// 	// 	mylog.log("costum.poi genre collection if 2");
// 	// 	(oldHash);
// 	} else if(oldHash.indexOf("") >= 0){
// 		mylog.log("costum.loadByHash if 2");
// 		hash = "#welcome";	
// 	} else {;
// 		mylog.log("costum.loadByHash else");
// 		hash = oldHash;
// 	}
// 	//alert("loadByHash "+hash);
// 	mylog.log("costum.loadByHash end", hash);
// 	return hash;
// };

directory.elementPanelHtml = function(params){
		mylog.log("elementPanelHtml notragora","Params",params);

		var hashHover=(params.collection=="poi") ? "#poi."+escape(slugify(params.name.toLowerCase().replace(/^\-+|\-+$/g, '').replace(/&quot;/g,''))) : (($.inArray(params.category,["collections,genres"])>-1) ? "#"+params.category+"."+slugify(params.name.toLowerCase().replace("?","").replace("#","").replace(/^\-+|\-+$/g, '').replace(/&quot;/g,'')) : params.hash);
		//var invited= (params.collection=="citoyens" && typeof contextData!="undefined" && typeof contextData._id!="undefined" && typeof contextData._id.$id!="undefined" && typeof params.links!="undefined" && typeof params.links.memberOf!="undefined" && typeof params.links.memberOf[contextData._id.$id]!="undefined" && params.links.memberOf[contextData._id.$id].isInviting==true) ? "Invité" : null;
		var dateStr="";
		if(typeof params.updated != "undefined" && notNull(params.updated))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
		else if(typeof params.created != "undefined" && notNull(params.created))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
		var str='';	
		str +='<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.containerClass+'">'+
				'<div class="item-slide">'+
						'<div class="entityCenter" style="position: absolute;">'+
							'<span><i class="fa fa-'+params.icon+' bg-'+params.color+'"></i></span>'+
						'</div>'+
						'<div class="img-back-card">'+
							params.imageProfilHtml +
							'<div class="text-wrap searchEntity">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="uppercase '+params.hashClass+'">'+params.name+'</a>'+
								'</h4>'+
								'<div class="small-infos-under-title text-center">';
									if (typeof params.type != 'undefined') {
		str +=							'<div class="text-center entityType">'+	
											'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type);
										if(typeof params.city!='undefined' && notEmpty(params.city)){
		str	+=								' - '+params.city+' </span>';
										}
		str	+=							'</div>';
									}
									if(notEmpty(params.statusLinkHtml))
		str+=							params.statusLinkHtml;	
									if(notEmpty(params.rolesHtml))
		str += 							'<div class=\'rolesContainer elipsis\'>'+params.rolesHtml+'</div>';
		// 							if (notNull(params.localityHtml)) {
		// str +=							'<div class="entityLocality no-padding">'+
		// 									'<span>'+params.localityHtml+'</span>'+
		// 								'</div>';
		// 							}
		str +=					'</div>'+
								'<div class="entityDescription"></div>'+
							'</div>'+
						'</div>'+
						//hover
						'<div class="slide-hover co-scroll">'+
							'<div class="text-wrap">'+
								'<a href="'+hashHover+'" class="'+params.hashClass+'">'+
									'<h4 class="entityName">'+
										params.name+
									'</h4>';
									if (typeof params.type != 'undefined') {
		str +=							'<div class="entityType">'+	
											'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
										'</div>';
									}
									if(notEmpty(params.statusLinkHtml))
		str+=							params.statusLinkHtml;	
									if(notEmpty(params.rolesHtml))
		str += 							'<div class=\'rolesContainer\'>'+params.rolesHtml+'</div>';
									if(typeof params.edit  != 'undefined' && notNull(params.edit))
		str += 							directory.getAdminToolBar(params);

									if (notNull(params.localityHtml)) {
		str +=							'<div class="entityLocality text-center">'+
											'<span> '+params.localityHtml+'</span>'+
										'</div>';
									}
//		str +=						'<hr>';
		str +=						'<p class="p-short">'+params.descriptionStr+'</p>';
									if(typeof params.tagsHtml != "undefined")
		str +=							'<ul class="tag-list">'+params.tagsHtml+'</ul>';
 		str +=					'</a>';
		str +=					directory.countLinksHtml(params);
								if(typeof params.id != 'undefined' && typeof params.collection != 'undefined') 
		str+=						directory.socialToolsHtml(params);
		str+=				'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		return str;
	};


	// directory.elementPanelHtml = function(params){
	// 	//if(directory.dirLog) mylog.log("----------- elementPanelHtml",params.type,params.name,params.elTagsList);
	// 	mylog.log("----------- Notragora directory.elementPanelHtml",params.type,params.name,params.elTagsList, params);
	// 	str = "";

	// 	if(params.type == "poi"){
	// 		params.htmlIco = "<i class='fa fa-video-camera fa-2x bg-green-poi'></i> ";
	// 	}else
	// 		params.htmlIco = ( ($.inArray("group", params.categoryNA) > -1) ? "<i class='fa fa-users fa-2x bg-green'></i>" : "<i class='fa fa-handshake-o fa-2x bg-green'></i>" );

	// 	var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? "grayscale" : "" ) ;
	// 	var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad["Wait for confirmation"] : "" ) ;
	// 	var classType=params.type;
	// 	if(params.type=="events") classType="";
	// 	//str += "<div class='col-lg-3 col-md-4 col-sm-6 col-xs-12 searchEntityContainer "+grayscale+" "+classType+" "+params.elTagsList+" "+params.elRolesList+" contain_"+params.type+"_"+params.id+"'>";
	// 	str += "<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer "+grayscale+" "+classType+" "+params.elTagsList+" "+params.elRolesList+" contain_"+params.type+"_"+params.id+"'>";
	// 	str +=    '<div class="searchEntity" id="entity'+params.id+'">';


	// 	//var addFollowBtn = ( $.inArray(params.type, ["poi","ressources"])>=0 )  ? false : true;
	// 	if(typeof params.edit  != "undefined" && notNull(params.edit))
	// 		str += this.getAdminToolBar(params);

	// 	if( params.tobeactivated == true ){
	// 		str += "<div class='dateUpdated'><i class='fa fa-flash'></i> <span class='hidden-xs'>"+trad["Wait for confirmation"]+" </span></div>";
	// 	}else{
	// 		timeAction= /*(params.type=="events") ? trad.created :*/ trad.actif;
	// 		if(params.updated != null )
	// 			str += "<div class='dateUpdated'><i class='fa fa-flash'></i> <span class='hidden-xs'>"+timeAction+" </span>" + params.updated + "</div>";
	// 	}

	// 	var linkAction = " lbh";
	// 	var parentStr = "" ;
	// 	if(typeof params.imgType !="undefined" && params.imgType=="banner"){
	// 		str += "<a href='"+params.hash+"' class='container-img-banner add2fav "+linkAction+">" + params.imgBanner + "</a>";
	// 		str += "<div class='padding-10 informations tooltips'  data-toggle='tooltip' data-placement='top' data-original-title='"+tipIsInviting+"'>";

	// 		str += "<div class='entityRight banner no-padding'>";



	// 		if(typeof params.size == "undefined" || params.size == undefined || params.size == "max"){
	// 			str += "<div class='entityCenter no-padding'>";
	// 				str += "<a href='"+params.hash+"' class='container-thumbnail-profil add2fav "+linkAction+"'>" + params.imgProfil + "</a>";
	// 				str += "<a href='"+params.hash+"' class='add2fav pull-right margin-top-15 "+linkAction+"'>" + params.htmlIco + "</a>";
	// 			str += "</div>";
	// 		}
	// 	} else {
	// 		var imgProfil = params.imgMediumProfil ;
	// 		if(typeObj[params.type] && typeObj[params.type].col == "poi" 
	// 		&& typeof params.medias != "undefined" && typeof params.medias[0] != "undefined" 
	// 		&& typeof params.medias[0].content != "undefined"  && typeof params.medias[0].content.image != "undefined")
	// 		imgProfil= "<img class='img-responsive' src='"+params.medias[0].content.image+"'/>";
	// 		str += "<a href='"+params.hash+"' class='container-img-profil add2fav "+linkAction+"'>" + imgProfil + "</a>";
	// 		str += "<div class='padding-10 informations tooltips'  data-toggle='tooltip' data-placement='top' data-original-title='"+tipIsInviting+"'>";

	// 		str += "<div class='entityRight profil no-padding'>";

			
	// 		if(typeof params.parent != "undefined" && typeObj[params.type] && typeObj[params.type].col == "poi"){
	// 			$.each(params.parent, function(keyP, valP){
	// 				parentStr +='<a href="#page.type.'+valP.type+'.id.'+keyP+'" style="font-size: 13px!important" class="entityName text-green lbh add2fav text-light-weight margin-bottom-5 "><i class="fa fa-users"></i> '+valP.name+'</a>';
	// 				// str += "<a  href='#page.type."+valP.type+".id."+keyP+"' class=' entityName bold text-green add2fav "+linkAction+"'>"+
	// 				//         "<i class='fa fa-users'></i> "+valP.name+
	// 				//       "</a>";  
	// 			});
	// 		}

	// 		if(typeof params.size == "undefined" || params.size == undefined || params.size == "max"){
	// 			str += "<div class='entityCenter no-padding'>";
	// 			str +=    "<a href='"+params.hash+"' class='add2fav pull-right "+linkAction+"'>" + params.htmlIco + "</a>";
	// 			str += "</div>";
	// 		}
	// 	}
	    
	// 	if(notEmpty(params.typePoi)){
	// 	//	str += "<span class='typePoiDir'><i class='fa fa-chevron-right'></i> " + tradCategory[params.typePoi] + "<hr></span>";  
	// 	}

	// 	var iconFaReply ="";// notEmpty(params.parent) ? "<i class='fa fa-reply fa-rotate-180'></i> " : "";
	// 	str += parentStr+"<a  href='"+params.hash+"' class='"+params.size+" entityName bold text-dark add2fav "+linkAction+"'>"+
	// 				iconFaReply + params.name +
	// 			"</a>";  
		
	// 	if(typeof(params.statusLink)!="undefined"){
	// 		if( typeof(params.statusLink.isAdmin)!="undefined" && 
	// 			typeof(params.statusLink.isAdminPending)=="undefined" && 
	// 			typeof(params.statusLink.isAdminInviting)=="undefined" && 
	// 			typeof(params.statusLink.toBeValidated)=="undefined")
	// 			str+="<span class='text-red'>"+trad.administrator+"</span>";

	// 		if(typeof(params.statusLink.isAdminInviting)!="undefined")
	// 			str+="<span class='text-red'>"+trad.invitingToAdmin+"</span>";
			
	// 		if(typeof(params.statusLink.toBeValidated)!="undefined" || typeof(params.statusLink.isAdminPending)!="undefined")
	// 			str+="<span class='text-red'>"+trad.waitingValidation+"</span>";
	// 	}

	// 	if(params.rolesLbl != "")
	// 		str += "<div class='rolesContainer'>"+params.rolesLbl+"</div>";


	// 	var thisLocality = "";
	// 	if(params.fullLocality != "" && params.fullLocality != " ")
	// 		thisLocality = "<a href='"+params.hash+"' data-id='" + params.dataId + "'  class='entityLocality add2fav"+linkAction+"'>"+
	// 						"<i class='fa fa-home'></i> " + params.fullLocality + "</a>";
	// 	else thisLocality = "";

	// 	str += thisLocality;

	// 	var devise = (typeof params.devise != "undefined") ? params.devise : "";
	// 	if(typeof params.price != "undefined" && params.price != "")
	// 		str += "<div class='entityPrice text-azure'><i class='fa fa-money'></i> " + params.price + " " + devise + "</div>";

	// 	if($.inArray(params.type, ["classifieds","ressources"])>=0 && typeof params.category != "undefined"){
	// 		str += "<div class='entityType col-xs-12 no-padding'><span class='uppercase bold pull-left'>" + tradCategory[params.section] + " </span><span class='pull-left'>";
	// 		if(typeof params.category != "undefined" && params.type != "poi") 
	// 			str += " > " + tradCategory[params.category];
	// 		if(typeof params.subtype != "undefined") str += " > " + tradCategory[params.subtype];
	// 			str += "</span></div>";
	// 	}

	// 	if(notEmpty(params.typeEvent))
	// 		str += "<div class='entityType'><span class='uppercase bold'>" + tradCategory[params.typeEvent] + "</span></div>";  

	// 	if(params.type=="events"){
	// 		var dateFormated = directory.getDateFormated(params, true);
	// 		var countSubEvents = ( params.links && params.links.subEvents ) ? "<br/><i class='fa fa-calendar'></i> "+Object.keys(params.links.subEvents).length+" "+trad["subevent-s"]  : "" ;
	// 		str += dateFormated+countSubEvents;
	// 	}
	// 	str += "<div class='entityDescription'>" + ( (params.shortDescription == null ) ? "" : params.shortDescription ) + "</div>";

	// 	str += "<div class='tagsContainer text-red'>"+params.tagsLbl+"</div>";
	// 	if(typeof params.counts != "undefined"){
	// 		str+="<div class='col-xs-12 no-padding communityCounts'>";
	// 		$.each(params.counts, function (key, count){
	// 			iconLink=(key=="followers") ? "link" : "group";
	// 			str +=  "<small class='pull-left lbh letter-light bg-transparent url elipsis bold countMembers margin-right-10'>"+
	// 			"<i class='fa fa-"+iconLink+"'></i> "+ count + " " + trad[key] +
	// 			"</small>";
	// 		});
	// 		str+="</div>";
	// 	}

	// 	str += "</div>";
	// 	str += "</div>";
	// 	str += "</div>";
	// 	str += "</div>";
	// 	str += "</div>";
	// 	return str;
	// };
	

directory.headerHtml = function(){
    mylog.log("-----------headerHtml ");
    headerStr = '';
    if(!directory.isCostum("header") || directory.getCostumValue("header")){
        mylog.log("-----------headerHtml :",searchObject.count);
        if((typeof searchObject.count != "undefined" && searchObject.count) || searchObject.indexMin==0 ){
            countHeader=0;
            mylog.log("-----------headerHtml countHeader:",countHeader);
            if(searchObject.countType.length > 1 && typeof searchObject.ranges != "undefined"){
                $.each(searchAllEngine.searchCount, function(e, v){
                    countHeader+=v;
                });
                mylog.log("-----------headerHtml countHeader2:",countHeader);
                //posClass="right"
            } else {
                typeCount = (searchObject.types[0]=="persons") ? "citoyens" : searchObject.types[0];
                if(typeof searchAllEngine.searchCount[typeCount] != "undefined")
                    countHeader=searchAllEngine.searchCount[typeCount];
                mylog.log("-----------headerHtml countHeader3:",countHeader);
                // posClass=(typeCount == "classified") ? "left" : "right";
            }


            mylog.log("-----------headerHtml :"+countHeader, searchObject.initType);
            var resultsStr = "Productions des groupes de travail";
            var titleSize= "col-md-8 col-sm-8";
            var toolsSize= "col-md-4 col-sm-4";
            var colorH = "text-brown";

            if(searchObject.types[0]=="organizations"){
                resultsStr = "Groupes de travail";
                colorH = "text-green";
            }

            headerFilters = "";
            // if( typeof costum != "undefined" && costum != null && 
            //     typeof costum.filters != 'undefined' && 
            //     typeof costum.filters.searchFilters != 'undefined'){
            //     $.each(costum.filters.searchFilters,function(k,p) { 
            //         colorClass = (typeof p.colorClass != "undefined") ? p.colorClass : "";
            //         headerFilters += "<a href='javascript:;' data-filter='"+k+"' class='btn-coopfilter btn btn-xs "+colorClass+"'> "+p.label+"</a> ";
            //     });
            // }

            headerStr +='<div class="col-xs-12 margin-bottom-10">'+
                    '<h4 class="elipsis '+titleSize+' '+colorH+' col-xs-10 no-padding">'+
                        "<i class='fa fa-angle-down'></i> " + countHeader + " "+resultsStr+" ";
            // if( !notNull(directory.costum) 
            // || typeof directory.costum.header == "undefined"
            // || typeof directory.costum.header.searchTypeHtml == "undefined"
            // || directory.costum.header.searchTypeHtml === false){
            //     headerStr += '<small>'+directory.searchTypeHtml()+'</small>';
            // }
            headerStr += headerFilters+'</h4>'+ 
                        '<div class="'+toolsSize+' col-xs-2 pull-right no-padding text-right headerSearchTools" style="padding-top:3px !important;">';
            // TODO CLEM :: ADD SURVEY  
            // if(searchObject.types.length == 1 && searchObject.types[0]=="vote"){
            //  headerStr +='<a href="javascript:;" data-form-type="survey" class="addBtnFoot btn-open-form btn btn-default addBtnAll letter-turq margin-bottom-10">'+ 
            //                  '<i class="fa fa-plus"></i>'+
            //                  '<span>'+tradDynForm.createsurvey+'</span>'+
            //              '</a>';
            // }
            mylog.log("headerStr ", directory.appKeyParam);
            if( notNull(costum) 
	              && typeof costum.app != "undefined"
	              && typeof directory.appKeyParam != "undefined"
	              && typeof costum.app[directory.appKeyParam] != "undefined"
	              && typeof costum.app[directory.appKeyParam].map != "undefined"
	              && typeof costum.app[directory.appKeyParam].map.hash != "undefined"){
	        	mylog.log("headerStr if");
	            headerStr+= '<button class="lbh-menu-app btn-map-na hidden-xs" data-hash="'+costum.app[directory.appKeyParam].map.hash+'" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
	                          '<i class="fa fa-map-marker"></i> '+trad.map
	                        '</button>';
	        } else if( !notNull(directory.costum) 
						|| typeof directory.costum.header == "undefined"
						|| typeof directory.costum.header.map== "undefined"
						|| directory.costum.header.map) {
	        	mylog.log("headerStr else");
	            headerStr+=       '<button class="btn-show-map hidden-xs" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
	              '<i class="fa fa-map-marker"></i> '+trad.map
	            '</button>';
	        }

            if(userId!="" && searchObject.initType=="classifieds"){
                headerStr+='<button class="btn btn-default letter-blue addToAlert margin-right-5 tooltips" data-toggle="tooltip" data-placement="bottom" title="'+trad.bealertofnewitems+'" data-value="list" onclick="directory.addToAlert();"><i class="fa fa-bell"></i> <span class="hidden-xs">'+trad.alert+'</span></button>';
            }

            if(/*userId != "" 
            && */notNull(directory.costum) 
            && typeof directory.costum.header != "undefined" 
            && typeof directory.costum.header.add != "undefined"
            // && typeof directory.costum.header.add[searchObject.initType] != "undefined"
            && typeof directory.costum.header.add[searchObject.types] != "undefined"
            ){
                headerStr+="<div class='pull-right'>";
                headerStr+=directory.createBtnHtml();
                headerStr+="</div>";
            }
            if(!notNull(directory.costum) 
            || typeof directory.costum.header == "undefined"
            || typeof directory.costum.header.viewMode == "undefined"
            || directory.costum.header.viewMode)  {
                headerStr+='<button class="btn switchDirectoryView ';
                if(directory.viewMode=="list") headerStr+='active ';
                headerStr+=     'margin-right-5" data-value="list"><i class="fa fa-bars"></i></button>'+
                            '<button class="btn switchDirectoryView ';
                if(directory.viewMode=="block") headerStr+='active ';
                headerStr+=     '" data-value="block"><i class="fa fa-th-large"></i></button>';
            }


            headerStr+= '</div>';      
        }
    }

    mylog.log("-----------headerHtml headerStr", headerStr);
    return headerStr;
}




searchObj.results.events = function(){
			mylog.log("searchObj.results.events");
			
			$("#dropdown_search .processingLoader").remove();     
			//active les link lbh
			coInterface.bindLBHLinks();
			//initialise les boutons pour garder une entité dans Mon répertoire (boutons links)
			directory.bindBtnElement();
			coInterface.bindButtonOpenForm();
			$("#poiSelectedHead").hide();
};

$(function (){
	$('#mainNav #menuTopRight .searchBarInMenu .dropdown-result-global-search').removeClass("hidden-xs");
	$('#mainNav #menuTopRight .searchBarInMenu').removeClass("hidden-xs");

});
