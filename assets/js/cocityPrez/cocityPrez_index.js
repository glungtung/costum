costum.lists.theme[tradTags.food] = {
    "agriculture": "agriculture",
    "alimentation": "alimentation",
    "nourriture": "nourriture",
    "AMAP": "AMAP"
}
costum.lists.theme[tradTags.health] = {
    "santé": "santé"
}


costum.lists.theme[tradTags.waste] = {
    "déchets": "déchets"
}


costum.lists.theme[tradTags.transport] = {
    "Urbanisme": "Urbanisme",
    "transport": "transport",
    "construction": "construction"
}


costum.lists.theme[tradTags.education] = {
    "éducation": "éducation",
    "petite enfance": "petite enfance"
}

costum.lists.theme[tradTags.citizenship] = {
    "citoyen": "citoyen",
    "society": "society"
}


costum.lists.theme[tradTags.economy] = {
    "ess": "ess",
    "économie social solidaire": "économie social solidaire"
}

costum.lists.theme[tradTags.energy] = {
    "énergie": "énergie",
    "climat": "climat"
}


costum.lists.theme[tradTags.culture] = {
    "culture": "culture",
    "animation": "animation"
}


costum.lists.theme[tradTags.environment] = {
    "environnement": "environnement",
    "biodiversité": "biodiversité",
    "écologie": "écologie"
}

costum.lists.theme[tradTags.numeric] = {
    "informatique": "informatique",
    "tic": "tic",
    "internet": "internet",
    "web": "web"
}

costum.lists.theme[tradTags.sport] = {
    "sport": "sport"
}