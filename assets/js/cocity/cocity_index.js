costum.lists.theme[tradTags.food] = {
    "agriculture": "agriculture",
    "alimentation": "alimentation",
    "nourriture": "nourriture",
    "AMAP": "AMAP"
}
costum.lists.theme[tradTags.health] = {
    "santé": "santé"
}


costum.lists.theme[tradTags.waste] = {
    "déchets": "déchets"
}


costum.lists.theme[tradTags.transport] = {
    "Urbanisme": "Urbanisme",
    "transport": "transport",
    "construction": "construction"
}


costum.lists.theme[tradTags.education] = {
    "éducation": "éducation",
    "petite enfance": "petite enfance"
}

costum.lists.theme[tradTags.citizenship] = {
    "citoyen": "citoyen",
    "society": "society"
}


costum.lists.theme[tradTags.economy] = {
    "ess": "ess",
    "économie social solidaire": "économie social solidaire"
}

costum.lists.theme[tradTags.energy] = {
    "énergie": "énergie",
    "climat": "climat"
}


costum.lists.theme[tradTags.culture] = {
    "culture": "culture",
    "animation": "animation"
}


costum.lists.theme[tradTags.environment] = {
    "environnement": "environnement",
    "biodiversité": "biodiversité",
    "écologie": "écologie"
}

costum.lists.theme[tradTags.numeric] = {
    "informatique": "informatique",
    "tic": "tic",
    "internet": "internet",
    "web": "web"
}

costum.lists.theme[tradTags.sport] = {
    "sport": "sport"
}
$("#menuTopLeft .menu-btn-top").before(costum.title);

function getNumberElement(slug, template) {
    var htmlNbElm = '';
    var params = {
        searchType: ["organizations", "event", "projects", "citoyens"],
        countType: ["organizations", "event", "projects", "citoyens"],
        count: true,
        notSourceKey: true,
        filters: {
            $or: {
                "source.keys": slug
            }
        }
    };
    ajaxPost(
        null,
        baseUrl + "/" + moduleId + "/search/globalautocomplete",
        params,
        function(data) {
            console.log("vvvv", data.count);
            if (template == "unnotremonde") {
                htmlNbElm += '<div class="main-section">';
                if (data.count.organizations != 0)
                    htmlNbElm += '<div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-users" aria-hidden="true"></i><br>' +
                    '<small>Organisations</small>' +
                    '<p>' + data.count.organizations + '</p>' +
                    '</div>' +
                    '</div>';
                if (data.count.projects != 0)
                    htmlNbElm += ' <div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-lightbulb-o" aria-hidden="true"></i><br>' +
                    '<small>Projets</small>' +
                    '<p>' + data.count.projects + '</p>' +
                    '</div>' +
                    '</div>';
                if (data.count.citoyens != 0)
                    htmlNbElm += ' <div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-user" aria-hidden="true"></i><br>' +
                    '<small>Personnes</small>' +
                    '<p>' + data.count.citoyens + '</p>' +
                    '</div>' +
                    '</div>';

                if (data.count.event != 0)
                    htmlNbElm += ' <div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-calendar" aria-hidden="true"></i><br>' +
                    '<small>Evenements</small>' +
                    '<p>' + data.count.event + '</p>' +
                    '</div>' +
                    '</div>';
                '</div>';
            } else if (template == "cocity") {
                htmlNbElm += '<div class="main-section">';
                if (data.count.citoyens != 0)
                    htmlNbElm += '<i class="fa fa-user" aria-hidden="true"></i>' + data.count.citoyens + "   ";
                if (data.count.event != 0)
                    htmlNbElm += '<i class="fa fa-calendar" aria-hidden="true"></i>' + data.count.event;

                if (data.count.organizations != 0)
                    htmlNbElm += "<br>  " + '<i class="fa fa-users" aria-hidden="true"></i>' + data.count.organizations + "   ";
                if (data.count.projects != 0)
                    htmlNbElm += '<i class="fa fa-lightbulb-o" aria-hidden="true"></i>' + data.count.projects;

                '</div>';
            }
            $(".element" + slug).html(htmlNbElm);
        }
    );
    return htmlNbElm;
}