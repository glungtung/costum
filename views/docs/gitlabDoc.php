<?php
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);

$cssJS = array('/js/docs/docs.js');
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl());
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "http://communecter74-dev/interop/gitlab/tree?url=https://gitlab.adullact.net/&repo=pixelhumain/codoc");
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, 0);
$ret = curl_exec($curl);
$dataDecode = json_decode($ret, true);
curl_close($curl);
?>


<?php
if (false && @Yii::app()->session["costum"] && @Yii::app()->session["costum"]["docTpl"]) {
    $this->renderPartial(Yii::app()->session["costum"]["docTpl"], true);
} else {  //var_dump($dataDecode);//exit;?>

<style type="text/css">
#docs-main-container{
	position: absolute;
	bottom: 0;
	top: 0px;
	left: 0;
	right: 0;
	z-index: 1000000
}
	#menu-left{
		position: fixed;
    	z-index: 100000;
	    bottom: 0;
	    top: 60px;
	    left: 0;
	    padding: 0;
	    overflow-y: scroll;
	    	background-color: white;
	}
	#header-doc{
		position: fixed;
		z-index: 100000;
		top: 0px;
		left: 0px;
		height: 60px;
		right: 0px;
		padding-top: 10px;
		background-color: white;
	}
	#header-doc h2{
		float: left;
	    color: #354C57;
	    font-size: 20px;
	    font-variant: small-caps;
	    line-height: 41px;
	    padding: 0px 10px;
	}
	#menu-left ul li{
		list-style: none;
	}
	#menu-left > ul > li > a{
		font-size: 20px;
	}
	ul.subMenu > li > a{
		font-size:16px;
	}
	#menu-left > ul > li > a, ul.subMenu > li > a{
		color: #354C57;
		width: 100%;
	    float: left;
	    padding: 5px 20px;
	    text-align: left;
	}
	#menu-left ul li .subMenu, #menu-left > ul > li > a{
		border-bottom: 1px solid #ccc;
	}
	#menu-left > ul > li > a.active, #menu-left > ul > li > a:hover{
		text-decoration: none;
		background-color:#65BA91;
		color: white;
		font-size: 22px;
	}
	ul.subMenu > li > a.active, ul.subMenu > li > a:hover{
		border-left: 4px solid #65BA91;
		color: #65BA91;
		font-size:18px;
		text-decoration: none;
	}
	#menu-left ul li a.active span.text-red, #menu-left ul li a:hover span.text-red{
		color:#354C57 !important;
	}
	.close-modal{
		top: 10px !important;
    	right: 10px !important;
     	z-index: 100000000000000 !important;
    	position: fixed !important;
	}
	.close-modal .lr, .close-modal .rl{
		height: 40px !important;
	}
	/* ul.subMenu{
		display:none;

	} */
	ul.subMenu{
		padding-left: 30px
	}
#show-menu-xs, #close-docs{
	    padding: 7px 15px;
    font-size: 20px;
}
.keypan .panel-heading{
	margin-top: 20px;
    min-height: 70px;
}
.keypan{
	border: none;
    margin-bottom: 10px;
    box-shadow: none;
}
.keypan, .keypanList{
	box-shadow: none;	
}
.keypanList .panel-title i{
	margin-right: 10px;
}
.keypanList .panel-body ul{
	padding-left: 0px;
}
.keypanList .panel-title span{
	font-size: 24px !important;
}
.keypan .panel-body{
	min-height: 200px;
}
.keypan hr {
	width: 75%;
    margin: auto;
}
#header-docs .panel-title, .subtitleDocs .panel-title {
	font-size: 40px;
}
#header-docs .panel-title .sub-title, .subtitleDocs .panel-title .sub-title{
	font-size: 20px !important;
	font-style: italic;	
}
#container-docs{
	background-color: white;
	z-index: 10000;
	top: 60px;
}
@media (max-width: 991px) {
 /* .open-type-filter{
        display: block;
    position: absolute;
    right: -33px;
    height: 50px;
    width: 50px;
    border: 1px solid #dadada;
    border-radius: 100%;
    text-align: right;
    padding-right: 8px;
    z-index: -1;
    font-size: 20px;
  }*/
  #menu-left{
    width: 56%;
    left: -56%;
	bottom: 0px;
	}
  
}

@media (min-width: 991px) {
  #menu-left {
    left:0 !important;
  }
}
#menu-left ul.subMenu > li > a {
    background: #129488;
    color: #ffffff;
}
#menu-left ul.subMenu > ul.subMenu > li > a {
    background: #414956;
    color: #ffffff;
}
</style>
<div id="docs-main-container">
	<div id="header-doc" class="shadow2">
		<a href='javascript:;' id="show-menu-xs" class="visible-xs visible-sm pull-left" data-placement="bottom" data-title="Menu"><i class="fa fa-bars"></i></a>
		<h2 class="elipsis no-margin"><i class="fa fa-book hidden-xs"></i> <?php echo Yii::t("docs", "All <span class='hidden-xs'>you need to know</span> about") ?></h2>
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGOS/CO2/logo-head-search.png"
	                     class="logo-menutop main pull-left" height=30>
	    <a href='javascript:;' class="lbh pull-right" id="close-docs"><span><i class="fa fa-sign-out"></i> <?php echo Yii::t("common", "Back") ?></span></a>
	</div>

	<div id="menu-left" class="col-md-3 col-sm-2 col-xs-12 shadow2">
	  	<ul class="col-md-12 col-sm-12 col-xs-12 no-padding">
			<li class="col-xs-12 no-padding">
				<a href="javascript:" class="link-docs-menu down-menu" data-type="welcome" data-dir="<?php echo Yii::app()->language ?>">
					<i class="fa fa-angle-right"></i> <?php echo Yii::t("docs", "WEL<span class='text-red'>CO</span>ME"); ?>
				</a>
			</li>
<?php
/////////
function makeSubLi($key, $value)
{
	if ($key == 'noPath') {
		$name = $path = $value;
	} else {
		$name = $value;
		$path = $key."/".$value;
	}
	echo '	<li class="col-xs-12 no-padding ">
	<a href="javascript://" class="link-docs-menu gitlab" data-path="'.$path.'" data-dir="'.Yii::app()->language.'" >
	'.$name.'</a>
	</li>';
}

function makeSubMenu($data, $parentK)
{
    $level = 1;
	foreach ($data as $k => $v) {
		if (is_array($v)) {
			echo '<li class="col-xs-12 no-padding menu-2 collapsed" data-toggle="collapse" data-target="#menu2-lvl'.$level.'">
					<a href="javascript:" class="link-docs-menu down-menu " data-type="'.$k.'" data-dir="'.Yii::app()->language.'">
				    <i class="fa fa-angle-right"></i> '.$k.'</a>
				</li>
					<ul class="subMenu col-xs-12 no-padding collapse" id="menu2-lvl'.$level.'">';
			foreach ($v as $subK => $subV) {
				if (is_array($subV)) {
					makeSubMenu($subV, $parentK."/".$k);
				} else {
					makeSubLi($parentK."/".$k, $subV);
				}
			}
            echo '	</ul>';
            /*'</li>';*/
		} else {
			makeSubLi($parentK, $v);
		}

        $level++;
	}
}
//////////	
$lvl = 1;
foreach ($dataDecode as $k => $v) {
	//var_dump($dataDecode);exit;
	if (is_array($v)) {
		echo '<li class="col-xs-12 no-padding collapsed" data-toggle="collapse" data-target="#level-'.$lvl.'" >
				<a href="javascript:" class="link-docs-menu down-menu menu-1" data-type="'.$k.'" data-dir="'.Yii::app()->language.'">
				<i class="fa fa-angle-right"></i> '.$k.'</a>
			</li>
				<ul class="subMenu col-xs-12 no-padding collapse" id="level-'.$lvl.'">';
		//foreach ($v as $subK => $subV) {
			makeSubMenu($v, $k);
		//}
		echo '	</ul>';
			  /*'</li>';*/
	} else {
		makeSubLi($k, $v);
	}
    $lvl++;
}
?>
		</ul>
	</div>

	<div id="container-docs" class="col-md-offset-3 col-md-9 col-sm-12 col-xs-12 no-padding text-center">
	</div>
<?php }?>


</div>
<script type="text/javascript">

var page="<?php echo @$page ?>";
var dir="<?php echo @$dir ?>";

jQuery(document).ready(function() {
	mylog.log("render","co2.views.docs.index");
	var dataMenu = "";

	dir=(dir=="") ? mainLanguage : dir;

	if(costum == null){
		if(page != "")
			initDocs(page, dir);
		else
			initDocs("welcome", mainLanguage);
	}
	bindLinkDocs()


	

});

function bindLinkDocs() {

	setTimeout(function(){
	$(".link-docs-menu").off().on("click",function(){
		
		if($(this).hasClass("down-menu")){
			$("#menu-left > ul > li > a").removeClass("active").find("i").removeClass("fa-angle-down").addClass("fa-angle-right");
			$(".subMenu .link-docs-menu").removeClass("active");
			$(this).addClass("active").find("i").removeClass("fa-angle-right").addClass("fa-angle-down");
		} else { 
			$(".subMenu .link-docs-menu").removeClass("active");
			$(this).addClass("active");
			if(!$(this).parents().eq(2).find(".link-docs-menu:first").hasClass("active")){
				$("#menu-left > ul > li > a").removeClass("active").find("i").removeClass("fa-angle-down").addClass("fa-angle-right");
				$(this).parents().eq(2).find(".link-docs-menu:first").addClass("active").find("i").removeClass("fa-angle-right").addClass("fa-angle-down");
			}
		}
		if($("#show-menu-xs").is(":visible")){
			$("#show-menu-xs").removeClass("show-dir");
			$("#menu-left").animate({ left : "-56%" }, 400 );
		}

		if ($(this).hasClass("gitlab")) {
			var urlInterop = baseUrl+"/interop/gitlab/page?url=https://gitlab.adullact.net/pixelhumain/codoc/-/blob/master/"+$(this).data('path');
			getAjax('', urlInterop, function (data) {
						descHtml = dataHelper.convertMardownToHtml(data.content);
						$("#container-docs").html(descHtml).removeClass("text-center").attr("style", "margin-left: 27%; max-width: 72%");
					} , "html");
		} else {
			if(!$("#container-docs").hasClass("text-center")) 
				$("#container-docs").addClass("text-center").removeAttr("style--");
			
		}
	});
	
	$("#show-menu-xs").click(function(){
	    if(!$(this).hasClass("show-dir")){
	      $(this).addClass("show-dir").data("title", "<?php echo Yii::t("common", "Close") ?>").find("i").removeClass("fa-chevron-right").addClass("fa-times");
	      $("#menu-left").animate({ left : "0%" }, 400 );
	    }else{
	      $(this).removeClass("show-dir").data("title", "<?php echo Yii::t("common", "Open filtering by type") ?>").find("i").removeClass("fa-times").addClass("fa-chevron-right");
	      $("#menu-left").animate({ left : "-56%" }, 400 );

	    }
	  });
},100)
 }

</script>
			
