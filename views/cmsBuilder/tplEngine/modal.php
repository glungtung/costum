<?php 
  	$categoryFilter = array();
 ?>
<div id="listeTemplate" style="display:none;">
     <div class="row">
      <div class="col-lg-12" style="border-bottom: 1px solid rgba(100,100,100,0.1);text-align: center;">
         <div class="col-xs-12 padding-20" style="z-index: 1">
            <div style="width: 100%">
              <h3 class="bg-azure" style="padding: 20px; width: 50%; border-radius: 50px; margin : auto; "><?php echo Yii::t("cms", "Choose a template")?></h3>
            </div>
         </div>
      </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="text-left">
              <div id="tpl-themes-filter" class="btn-group">
                
              </div>
            </div>
            <div class="">
              <div class="table table-filter">
                <div class="text-left">
                  <?php foreach ($listTemplate as $kTpl => $value) {
                  	$valCategory = isset($value["category"]) ? preg_replace("/[^a-zA-Z]+/", "",$value["category"]): "Autre" ;
                    if(!isset($categoryFilter[$valCategory]))
                  		$categoryFilter[$valCategory]=array("count"=>1, "label"=> Yii::t("cms", $value["category"]));
                  	else
                  		$categoryFilter[$valCategory]["count"]++;
                    $listCms = isset($value["cmsList"])?$value["cmsList"]:"";
                    $img = isset($value["img"])?$value["img"]:"";                     
                    $description = isset($value['description'])?$value['description']:'';
                    $name = isset($value['name'])? $value['name']:'';
                    $type = isset($value["type"])? $value["type"]:"";
                    $profil = isset($value["profilImageUrl"]) ?$value["profilImageUrl"]:"";
                    $currentTplsUser = isset($value["tplsUser"]) ?$value["tplsUser"]:[];
                    $counts = array();
                    foreach ($currentTplsUser as $key=>$subarr) {
                      $counts[] = $subarr;
                    }          
                    $countUsing = 0;
                    $countBackup = 0;

                    forEach($counts as $k => $v){
                      $arrayVal = array_values($v);
                      forEach($arrayVal as $k => $v){
                        if($v =="using") $countUsing ++;
                        if($v =="backup") $countBackup ++;
                      }

                    }
                    $tplKeys = $kTpl;
                    $initImage = Document::getListDocumentsWhere(array("id"=> $tplKeys, "type"=>'cms'), "file");
                    $arrayImg = [];
                    foreach ($initImage as $k => $v) {
                      $arrayImg[]= $v["docPath"];
                    }

                     ?>
                  <div class="tplItems" data-status="<?=  $valCategory ?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">          
                    <?php if($tplUsingId == $value["_id"]){ ?>      
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px #5dd55d, 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }elseif($listCms == ""){?>  
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px rgb(198 43 43), 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }else{ ?>                     
                       <div class="item-slide">
                    <?php } ?> 
                          <div class="entityCenter">
                             <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                             </a>
                          </div>
                          <div class="img-back-card">
                             <div class="div-img">
                                <img src="<?= $profil?>">
                             </div>
                             <div class="text-wrap searchEntity">
                                <div class="entityRight profil no-padding">
                                   <a href="#" class="entityName letter-orange">
                                     <font >
                                     <?= $name ?>                  
                                     </font>
                                   </a>    
                                </div>
                             </div>
                          </div>
                          <!-- hover -->
                          <div class="slide-hover co-scroll">
                             <div class="text-wrap">
                                   <div class="entityPrice">
                                         <font class="letter-orange" ><?= $name ?></font>
                                   </div>
                                   <div class="entityType col-xs-12 no-padding">
                                      <p class="p-short">
                                         <font >
                                         <?= $description ?>                    
                                         </font>
                                      </p>
                                   </div>
                                   <hr>
                                   <ul class="tag-list">
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                      <i class="fa fa-user"></i> <?= $countUsing ?> <?php echo Yii::t("cms", "Users")?>
                                      </font>
                                      </span>
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                          <i class="fa fa-eye"></i> <?= $countUsing+$countBackup ?> <?php echo Yii::t("cms", "Seen")?>
                                      </font>
                                      </span>
                                   </ul>
                                <hr>
                                <div class="desc">
                                   <div class="socialEntityBtnActions btn-link-content">                                   
                                      <a href="<?= $profil ?>" class="btn btn-info btn-link thumb-info" data-title="Aperçu de <?= $name ?>" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye"></i><?php echo Yii::t("cms", "Preview")?></font>
                                      </a>
                                       <?php
                                        if (Yii::app()->session["userId"]== $value['creator'] || Yii::app()->session["userIsAdmin"]){ ?>
                                        <a href="javascript:;" class="btn btn-info btn-link editDesc" data-id="<?= $value["_id"]?>" data-category="<?= @$value["category"]?>" data-name='<?= $name ?>' data-desc='<?php echo $description ?>' data-img='<?= json_encode($initImage) ?>'>
                                          <font > <?php echo Yii::t("cms", "Edit")?></font>
                                        </a> 
                                      <?php } ?>   
                                      <?php if($tplUsingId == $value["_id"]){ ?>      
                                        <p class="text-green bold"><?php echo Yii::t("cms", "Your current template")?></p>
                                      <?php }elseif($listCms == ""){?>  
                                        <p class="text-red bold"><?php echo Yii::t("cms", "Not available")?></p>
                                      <?php }else{ ?>                             
                                      <a href="javascript:;" class="btn btn-info btn-link" onclick='tplObj.chooseConfirm(<?php echo json_encode($listCms)?>,"<?php echo $value["_id"] ?>");'>
                                        <font ><?php echo Yii::t("cms", "Choose")?></font>
                                      </a> 
                                      <?php } ?>    
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                  </div>
                  <?php }?>              
                </div>
              </div>
              <div id="backupTplsContainer"></div> 
            </div>
          </div>
        </div>
     </div>
  </div> 
<script type="text/javascript">
  	var categoryFilter=<?php echo json_encode($categoryFilter); ?>;
  	jQuery(document).ready(function() {
      var tot=0;
      var str='';
  			$.each(categoryFilter, function(e, v){
  				str+= '<button type="button" class="btn btn-default btn-filter" data-target="'+e.replace("/[^a-zA-Z]+/", "")+'">'+
  						      v.label+
                		'<span class="filter-badge">'+v.count+'</span>'+
             	'</button>';
              tot+=v.count;
  			});
  		str+='<button type="button" class="btn btn-default btn-filter btnRctlyTpl" data-target="backup" onclick="tplObj.backup()">Old template</button>';
      var allBtn='<button type="button" class="btn btn-default btn-filter" data-target="all">'+trad.all+'<span class="filter-badge">'+tot+'</span></button>';
      
  		$("#listeTemplate #tpl-themes-filter").html(allBtn+str);
  	});
                   
 </script>
