<?php 
    $base_url = Yii::app()->getBaseUrl(true);
    $host = @$_GET["host"];

    $sw_scope = str_contains($base_url, $host) ? "./":"/";

?>
<script>

    //ajouter mylog ici pour n'est pas attendre co.js se charge pour le pouvoir utiliser
    var mylog = (function () {
        return {
            log: function() {
            if(debug){
                var args = Array.prototype.slice.call(arguments);
                console.log.apply(console, args);
                }
            },
            warn: function() {
                if( debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.warn.apply(console, args);
                }
            },
            debug: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.debug.apply(console, args);
                }
            },
            info: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.info.apply(console, args);
                }
            },
            dir: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.warn.apply(console, args);
                }
            },
            error: function() {
                if(debug){
                    var args = Array.prototype.slice.call(arguments);
                    console.error.apply(console, args);
                }
            }
        }
    }());
</script>

<script>
    var cmsChildren = {},
        sectionDyf = {},
        tplCtx = {},
        callByName = {},
        tplKey = '<?= @$tplKey ?>',
        isInterfaceAdmin = false,
        isSuperAdmin = false,
        isSuperAdminCms = false,
        page = "<?=$page?>",
        divSelection,
        current_full_Url = window.location.href,
        hideOnPreview = '.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.command-block-pos,.previewTpl,.openListTpls,.content-btn-action,.hiddenPreview';
        var intervalToCheckLink = setInterval(function(){
            if(typeof convAndCheckLink == 'function'){
                clearInterval(intervalToCheckLink)                
                convAndCheckLink(".sp-text")
            }   
            //Remove all element useless in preview mode
            if (costum.editMode != true) {
              $(hideOnPreview).remove();
          }
        }, 100)

    <?php if(Authorisation::isInterfaceAdmin()){ ?>
        isInterfaceAdmin = true;
    <?php }
      if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){  ?>
        isSuperAdmin = true;
        isSuperAdminCms = true;
    <?php }     
      if (Authorisation::isUserSuperAdminCms()){  ?>
        isSuperAdminCms = true;
    <?php } ?>

    $(function(){
        cmsBuilder.config={
            tpl : {
                activeId : "",
                description : "<?php echo isset($insideTplInUse["description"]) ? $insideTplInUse["description"] : "" ?>",
                name : "<?php echo isset($insideTplInUse["name"]) ? $insideTplInUse["name"] : "" ?>",
                category : "<?php echo isset($insideTplInUse["category"]) ? htmlspecialchars_decode($insideTplInUse["category"]) : "" ?>",
                listCategories : {
                    "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                    "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                    "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                    "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                    "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                    "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                    "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                    "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                    "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                    "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                    "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                    "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                    "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                    "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                    "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                    "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                    "Other"  :"<?php echo Yii::t("cms", "Others")?>"
                }
            },
            context : {
                type : costum.contextType ,
                id :  costum.contextId ,
                slug :  costum.contextSlug 
            },
            page : "<?=$page?>",
            block:{
            idCmsMerged: <?php echo json_encode(array_keys(@$cmsList)) ?>,
            cmsInUseId : <?php echo json_encode(@$cmsInUseId) ?>,
            newCmsId : <?php echo json_encode(@$newCmsId) ?>
            }
        };

        setTimeout(function(){     
            $("#sortablee").removeClass("row")
        },10)

        if(window.cmsConstructor)
            cmsConstructor.init();
    })
</script>
<div id="all-block-container" class="col-xs-12 no-padding">
    <?php
    $i = 0;
    foreach ($cmsList as $e => $v) {
        // ??????
        if(isset($v["dontRender"]) && $v["dontRender"] == "true"){
          continue;
        }
        $data = array(
          "v" => $v,
          "el" => [],
          "page" => $page,
          "cmsList" => $cmsList,
          "costumData" => $costum,
          "canEdit"    => @$costum["editMode"]
        );
        if (isset($v["path"])) {
            $path = $v["path"];
            $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
            $pathExplode = explode('.', $v["path"]);
            $count = count($pathExplode);
            $content = isset($v['content']) ? $v['content'] : [];
            $kunik = $pathExplode[$count - 1] . $v["_id"];
            $blockKey = (string)$v["_id"];
            $blockName = (string)@$v["name"];
            $params = [
                "cmsList"   => $cmsList,
                "blockKey"  => $blockKey,
                "blockCms"  => $v,
                "page"      => $page,
                "canEdit"   => @$costum["editMode"],
                "type"      => $path,
                "kunik"     => $kunik,
                "content"   => $content,
                'blockName' => $blockName,
                  "costum"=>$costum,
                  "el"=> [],
                'range'     => $i,
                "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg",
                "clienturi" => @$_POST["clienturi"]
            ];

            $width = [
                "modeLg" =>  isset($v["modeLg"]) ? $v["modeLg"] : "12",
                "modeMd" =>  isset($v["modeMd"]) ? $v["modeMd"] : "12",
                "modeSm" =>  isset($v["modeSm"]) ? $v["modeSm"] : "12",
                "modeXs" =>  isset($v["modeXs"]) ? $v["modeXs"] : "12"
            ];

            if (is_file($this->getViewFile("costum.views." . $path, $params))) {
                $dataAnchorTarget = (!empty($v["anchorTarget"])) ? " data-anchor-target='" . substr($v["anchorTarget"], 1) . "' " : ""; ?>
                <div 
                    id="<?= $v["_id"] . "-" . $i ?>" 
                    class="cmsbuilder-block <?= ($v["path"]==="tpls.blockCms.superCms.container")?"cmsbuilder-block-droppable":"" ?> sortable-<?= $kunik ?> custom-block-cms col-xs-12 no-padding col-lg-<?= $width["modeLg"] ?> col-md-<?= $width["modeMd"] ?> col-sm-<?= $width["modeSm"] ?> col-xs-<?= $width["modeXs"] ?> block-container-<?= $kunik ?>  sp-bg" 
                    data-blocktype="section"
                    data-sptarget="background" 
                    data-path="<?= $v["path"] ?>"  
                    data-id="<?= $v["_id"]?>" 
                    <?php echo  $dataAnchorTarget; ?>
                    data-kunik="<?= $kunik ?>" 
                    data-name="<?= ($v["path"]==="tpls.blockCms.superCms.container")?"Section":$blockName ?>"
                  >
                  
                    <div class="block-container-html">
                      <?php echo $this->renderPartial("costum.views." . $path, $params); ?>
                    </div>
                </div>
<?php 
            } else { 
?>
                <div class="col-xs-12 text-center" id="<?php echo (string)$v["_id"]; ?>">
                  <?php echo $this->renderPartial("costum.views.tpls.blockNotFound", ["blockKey" => $blockKey]) ?>
                </div>
<?php       }
        }
        $i++;
    }
?>
</div>