<?php 
/*if(Authorisation::isInterfaceAdmin()){
   echo $this->renderPartial("costum.views.cmsBuilder.tplEngine.toolsbar",array()); ?>
   <div id="cms-layer-Bar" class="toolsBar tools text-left unselectable"></div>
    <div id="toolsBar-edit-block" class="toolsBar tools text-left unselectable" style="right: 0;color: #e5e5e5;width: 400px;"></div>
    
<?php
}*/
?>

<script type="text/javascript">
    var cmsChildren = {};
    var sectionDyf = {};
    var tplCtx = {};
    var callByName = {};
    var tplKey = '<?= @$tplKey ?>';
    $(function(){
      cmsBuilder.config={
        tpl : {
            activeId : "<?//= $tplUsingId ?>",
            description : "<?php echo isset($insideTplInUse["description"]) ? $insideTplInUse["description"] : "" ?>",
            name : "<?php echo isset($insideTplInUse["name"]) ? $insideTplInUse["name"] : "" ?>",
            category : "<?php echo isset($insideTplInUse["category"]) ? htmlspecialchars_decode($insideTplInUse["category"]) : "" ?>",
            listCategories : {
                "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                "Other"  :"<?php echo Yii::t("cms", "Others")?>"
            }
        },
        context : {
            type : costum.contextType ,
            id :  costum.contextId ,
            slug :  costum.contextSlug 
        },
        page : "<?=$page?>",
        block:{
          idCmsMerged: <?php echo json_encode(array_keys(@$cmsList)) ?>,
          cmsInUseId : <?php echo json_encode(@$cmsInUseId) ?>,
          newCmsId : <?php echo json_encode(@$newCmsId) ?>
        }
    };
    })

    var isInterfaceAdmin = false;
    var isSuperAdmin=false;
    <?php if(Authorisation::isInterfaceAdmin()){ ?>
      isInterfaceAdmin = true;
    <?php }
      if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])){  ?>
        isSuperAdmin = true
    <?php } ?>;
   // var isInterfaceAdmin = false;
    var hideOnPreview = '.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.command-block-pos,.previewTpl,.openListTpls,.content-btn-action,.hiddenPreview';
    // /* Init mode in preview or edit {w : writing mode ; v : view mode}*/
    // if (costum.editMode == false) {          
    //     $(".saveTemplate").show();
    //     $(".sp-text").fadeIn();
    //     $(hideOnPreview).fadeIn();
    //     cmsBuilder.tpl.edit();
    //     $(".hiddenEdit").html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
    // }else{
    //     cmsBuilder.tpl.convAndCheckLink(".sp-text");
    //     $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
    //     $(".sp-text").removeClass("edit-mode-textId");
    //     $(".sp-text").removeClass("selected-mode-textId");
    //     cmsBuilder.tpl.preview();
    // }
  
    var page = "<?=$page?>";
    var divSelection;
    var current_full_Url = window.location.href;
    setTimeout(function(){     
      $("#sortablee").removeClass("row")
    },10)
  
</script>
    <?php
    $param=[
      "page"       => $page,
      "cmsList"    => $cmsList,
      "costum"     => $costum,
      "paramsData" => $paramsData,
    ]; 
    echo $this->renderPartial("costum.views.cmsBuilder.blockEngine.index",$param); 
  
    ?>


<!-- Documentation -->
<?php // echo $this->renderPartial("costum.views.tpls.blockDocumentation",array("canEdit" => $canEdit)); ?>

<script type="text/javascript">

/********************Menu********************/
  //superCms.init();
  $(function(){
    cmsConstructor.init();
  })
 
    /*$('#mainNav').off().on('contextmenu', function(e) {
      let $this = $(this);
      let menuEditor = [
      {
        class : "editappallAppParams",
        label : "Modifier la barre de menus",
        icon : "bars",
        data : {
          id : contextId,
          key : "app",
        //  collection : contextData.collection,
          path : "costum.app"
        }
      },
      {
        class : "editcssmenuTopParams",
        label : "Apparence du menu",
        icon : "paint-brush",
        data : {
          id : contextId,
          key : "css",
          //collection : contextData.collection,
          path : "costum.css.menuTop"
        }
      }
      ] 
      cmsCtxMenu.add(menuEditor);
      e.preventDefault()
    });*/
    /*$('#mainNav a').off().on('contextmenu', function(e) {
      let $this = $(this);
      if (!$this.children("span").hasClass("name-real-time")) {
        let jsonListePage = {}
        $.each(pageListe , function(k,hash) {
          let $itsPath = "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList"
          let $itsChecked = false;
          let $thisIcon = "";
          if (typeof costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList[k] !== "undefined") {
            $thisIcon = "check";
            $itsPath = costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList[k];
            $itsChecked = true;
          }
            jsonListePage[k] = {
             class : "ctx-page",
             label : hash.subdomainName ? hash.subdomainName : k,
             icon : $thisIcon,
             data : {
              id : contextId,
              collection : '<?php //echo$this->costum["contextType"]; ?>',
              path: $itsPath,
              label : hash.subdomainName ? hash.subdomainName : k,
              page : k,
              show : $itsChecked
            }
          }
          })
        let textMenuEditor = [
        {
          class : "edit-infos-page",
          label : "Modifier",
          icon : "i-cursor",
          data : {
          //  id : contextId,
            key : "app",
         //   collection : contextData.collection,
            path : "costum.app"
          }
        },
        {
          class : "",
          label : "Menus affiché",
          icon : "eye-slash",
          submenu : {
          }
        },
        {
          class : "change-icon",
          label : "Modifier l'icon",
          icon : "smile-o",
          data : {
          //  id : contextId,
            key : "app",
         //   collection : contextData.collection,
            path : "costum.app"
          }
        }
        ] 
        textMenuEditor[1].submenu = jsonListePage;
        // mylog.log("menuShow", jsonListePage)
        cmsCtxMenu.add(textMenuEditor); 
        setTimeout(function(){  
          $(".edit-infos-page").on("click", function() {
            cmsBuilder.costumizer.openLayer(costumizer.obj.app, "edit-infos-page", "Header")
            costumizer.pages.actions.save($this.attr("href"),costumizer.obj.app[$this.attr("href")]);
          })

          $(".change-icon").on("click", function() {
            costumizer.icon.openIcon($this.parent().children("i").attr("class"))             
          })
          $(document).on("icon-changed", function() {   
            let tplCtx = {
              id : contextId,
              collection : contextType,
              path : 'costum.app.'+$this.parent().attr('href')+'.icon',
              removeCache : true,
              format : true
            }    
            if (costumizer.icon.state == "use"){                      
              tplCtx.value = costumizer.icon.selected ;
              dataHelper.path2Value( tplCtx, function(params) {
                location.reload();
              })
            }else if (costumizer.icon.state == "delete"){  
              dataHelper.unsetPath( tplCtx, function(params) {
                location.reload();
              })
            }
          })
          $(".ctx-page").on("click", function() {
            let tplCtx = {
             id :'<?php //echo    $this->costum["contextId"]; ?>',
             collection : '<?php //echo    $this->costum["contextType"]; ?>',
             path : 'costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.'+$(this).data("page"),
             removeCache : true,
             format : true
            }
            if ($(this).data("show") == true) {
              // alert("true")
              tplCtx.path = 'costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.'+$(this).data("page")
              dataHelper.unsetPath( tplCtx, function(params) { 
                location.reload();
              } );
            }else{
              tplCtx.value = true;
              dataHelper.path2Value( tplCtx, function(params) {
               location.reload();
             }) 
            }
          })
        },11)
        e.preventDefault()
      }
    });

    $(document).on("blur","#mainNav .editing span",function () {
      $(this).parent().removeClass("editing")
      $(this).attr("contenteditable", false)
      // $(this).parent().attr('href')
      // alert($(this).parent().attr('href'))
      if (spDefaultText !== $(this).text()) {
        let tplCtx = {
          id :'<?php //echo    $this->costum["contextId"]; ?>',
          collection : '<?php //echo    $this->costum["contextType"]; ?>',
          path : 'costum.app.'+$(this).parent().attr('href')+'.subdomainName',
          removeCache:true,
          value : $(this).text()
        }
        dataHelper.path2Value( tplCtx, function(params) {    
          location.reload();
        });
      }
    })*/
  <?php //} ?>

 /* var tplHtmlCtxMenu = $(".sp-cms-context-menu").html();
  var cmsCtxMenu = {
    menulists : function(list,name) {
      let menuHtml = "";
      let listSeparator = "";
        // mylog.log("sub length",Object.getPrototypeOf(list) === Object.prototype)
        $.each(list, function(ks,val) {
          if (typeof val.submenu == 'undefined') {

            let listData = '';
            if (typeof val.data !== "undefined") {
              $.each(val.data, function(k,val) {
               listData += 'data-'+k+' = "'+val+'"';
             })
            }
            if (costum.editMode == true || typeof val.preview == "undefined") {
              menuHtml += `
              <li class="tpl-cms-menu-item">
              <button type="button" ${val.id ? "id ='"+val.id+"'" : ""} class="tpl-cms-menu-btn ${val.class ? val.class : ''}" ${listData ? listData : ''}>
              ${val.icon ? '<i class="fa fa-'+val.icon+'"></i>' : ''}        
              <span class="tpl-cms-menu-text">${val.label ? val.label : ''}</span>
              </button>
              </li>
              `         
             listSeparator = '<li class="tpl-cms-menu-separator"></li>';
            }      
          }
        })
        return menuHtml+listSeparator;
      },

      add : function(menu) {
        mylog.log("ctxMenu", menu)
        let html = "";
        let separator = "";
        let visible = "";
        let uniqueEl = [];
        $.each(menu, function(i, menuEl){
          if($.inArray(menuEl, uniqueEl) === -1) uniqueEl.push(menuEl);
        });
        $(".sp-cms-context-menu").html(tplHtmlCtxMenu);
        html += cmsCtxMenu.menulists(uniqueEl)
        $.each(uniqueEl, function(ks,val) {
          if (typeof val.submenu !== 'undefined') {
            if (costum.editMode == false || typeof val.preview == "undefined") {
             html += `
             <li class="tpl-cms-menu-item tpl-cms-menu-item-submenu ${costum.editMode ? val.preview : ""}">
             <button type="button" ${val.id ? "id ='"+val.id+"'" : ""} class="tpl-cms-menu-btn ${val.class ? val.class : ''}">
             ${val.icon ? '<i class="fa fa-'+val.icon+'"></i>' : ''}        
             <span class="tpl-cms-menu-text">${val.label ? val.label : ''}</span>
             </button>
             <ul class="tpl-cms-menu">
             ${cmsCtxMenu.menulists(val.submenu)}
             </ul>
             </li>
             `
             separator = '<li class="tpl-cms-menu-separator"></li>';
           }
         }
       }) 
        // mylog.log("ctxMenu", html)
        setTimeout(function(){
          $(".sp-cms-context-menu").append(html+separator);
           cmsBuilder.tpl.initEvent();    
            cmsBuilder.block.menuSelector.initEvent();
                    
        },10)
      }
    }
    // Séparer le travail de cms block et de template !!!!!!!!!!!
  //  var tplObj = {};
*/

  /*****************Right click options*****************/
/*  <?php if(Authorisation::isInterfaceAdmin()){  ?>
    var menu = document.querySelector('.tpl-cms-menu');
    let remainsRight = 0;
    let remainsBottom = 0;

    function showMenu(x, y){
      if(costum.editMode){
        $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
      }else{
        $(".hiddenEdit").html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
      }
      menu.style.left = x + 'px';
      menu.style.top = y + 'px';
      menu.classList.add('tpl-menu-show');
      $(".sp-text").attr('contenteditable','false');
      $(".sp-text").removeClass('edit-mode-textId');
      $("#toolsBar").html('');
      $("#toolsBar").hide()
    }

    $(document).bind('contextmenu', function(e) {
      e.preventDefault()
      $(".sp-cms-context-menu").html(tplHtmlCtxMenu);
      $(".sp-cms-context-menu").removeClass("hidden")
      $(document).trigger('rightclick');
      let bodyH = $("body").height();
      let bodyW = $("body").width();
      let menuPositionTop = e.pageY;
      let menuPositionLeft = e.pageX;
       remainsRight = bodyW - menuPositionLeft
       remainsBottom = pageYOffset + bodyH - menuPositionTop
      
      setTimeout(function () {
        let menuH = $(".sp-cms-context-menu").height();
        let menuW = $(".sp-cms-context-menu").width();
        let menuChildW = 0;
        // position main menu from top 
        if (remainsBottom < menuH) {
          menuPositionTop = menuPositionTop - menuH
        }
        // position main menu from right 
        if (remainsRight < menuW) {
          menuPositionLeft = menuPositionLeft - menuW
        }

        $(document).on("mouseenter",".tpl-cms-menu-item-submenu" ,function(ev) {
          let submenu = $(this).children(".tpl-cms-menu")
          menuChildW += submenu.width();
          submenu.css("display","flex");
          submenu.css("top", -1)

          // position submenu from top 
          if (pageYOffset + bodyH - ev.pageY < submenu.height()) {
            submenu.css("top", 25-$(this).find(".tpl-cms-menu").height())  
          } 

          // prosition submenu from right 
          if (remainsRight < menuChildW+menuW) {
            submenu.css("left", -menuW) 
          }
        }) 
        $(document).on("mouseleave",".tpl-cms-menu-item-submenu" ,function() {   
          menuChildW = 0;
        })
       showMenu(menuPositionLeft, menuPositionTop);  
      },11)

     
    });


    $('body').click(function() {
      $(".tpl-menu-show").removeClass('tpl-menu-show');
    });
    $('block-parent').click(function() {
      $('.sp-text').blur()
    });
    
  <?php }  ?>

*/
/*$(".btn_tpl_filter").click(function(){
    params = {
    "category" : $(this).data("tplcategory"),
    "type" : "template"
  };
  ajaxPost(
      null,
      baseUrl+"/co2/cms/gettemplatebycategory",
      params,
      function(data){
          $.each(data,function(key,value){
              // console.log("Template"+ value.name);
          })
      },
      null,
      "json",
      {async : false}
  );

      cmsBuilder.init();
});

//$(".editDesc").click(function(){
$('body').on('click',".editDesc", function () {
  // mylog.log("sary", $(this).data("img"));
  var tplCtx = {};
  var tplId = $(this).data("id");
  tplCtx.collection = "cms";
  var activeFormEdit = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t("cms", "Modification of the template")?>",
          "type" : "object",
          "properties" : {
            name : {
              label: "<?php echo Yii::t("cms", "Name of the template")?>",
              inputType : "text",
              value : $(this).data("name")
            },
            type : { 
              "inputType" : "hidden",
              value : "template" 
            },
            category : {
              label : trad.category,
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                  "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                  "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                  "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                  "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                  "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                  "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                  "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                  "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                  "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                  "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                  "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                  "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                  "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                  "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                  "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                  "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                  "Other"  :"<?php echo Yii::t("cms", "Others")?>"
              },
              value : $(this).data("category")
            },
            image : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t("cms", "Background image")?>",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : $(this).data("img")
          },
            description : {
              label: "<?php echo Yii::t("common", "Description")?>",
              inputType : "text",
              value : $(this).data("desc")
            }
          },

          beforeBuild : function(){
            uploadObj.set("cms",tplId);
          },

          save : function () {
            tplCtx.id = tplId;
            tplCtx.value = {};
            $.each( activeFormEdit.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            // mylog.log(tplCtx);
            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
              tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
             dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                if(params.result){
                  toastr.success("Modification enregistré!");
                  $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              }else
              toastr.error(params.msg);
            });
            } );
           }
         }
       }
     }; 
  dyFObj.openForm( activeFormEdit );
});*/

/*$(".saveChangeTpl").click(function(){
  tplCtx.id = cmsBuilder.config.tpl.activeId;
  tplCtx.collection = "cms";
    var activeForm = {
      "jsonSchema" : {
        "title" : "Enregistrement du template",
        "type" : "object",
        onLoads : {
          onload : function(data){
            $(".parentfinder").css("display","none");
          }
        },
        "properties" : {
          name : {
            label: trad.name,
            inputType : "text",
            value : cmsBuilder.config.tpl.name
          }, 
          category : {
              label : trad.category,
              inputType : "select",
              rules:{
                "required":true
              },
              options : cmsBuilder.config.tpl.listCategories,
              value : cmsBuilder.config.tpl.category
            },
          image : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t("cms", "Background image")?>",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : <?php echo json_encode($tplInitImage); ?>
          },
          description : {
            label: tradDynForm.description,
            inputType : "text",
            value : cmsBuilder.config.tpl.description
          }
        }
      }
    };          
     activeForm.jsonSchema.afterBuild = function(){     
            uploadObj.set("cms",cmsBuilder.config.tpl.activeId);
      }; 

    activeForm.jsonSchema.save = function () {
      tplCtx.path = "allToRoot";
      tplCtx.value = {};
      $.each( activeForm.jsonSchema.properties , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      tplCtx.value.cmsList = [];
      tplCtx.value.cmsList = <?php echo json_encode(@$idCmsMerged) ?>;
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        // mylog.log("activeForm save tplCtx",tplCtx);
        tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
        tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
        dataHelper.path2Value( tplCtx, function(params) {
          dyFObj.commonAfterSave(params,function(){
            if(params.result){
              toastr.success("Modification enregistré!");
              $("#ajax-modal").modal('hide'); 
              urlCtrl.loadByHash(location.hash);
              smallMenu.open(tplMenu);
            }  else {
              toastr.error(params.msg);
            tplObj.removeCmsStat(tplCtx.id,cmsBuilder.config.block.newCmsId);
            }
          });
        } );
      } 
    }
    dyFObj.openForm( activeForm );
});
*/

/**************Supercms required*************
 **************Keep selected text when focus on input color*************/
 /*var saveSelection, restoreSelection;

if (window.getSelection && document.createRange) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var range = win.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;

        return {
            start: start,
            end: start + range.toString().length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var charIndex = 0, range = doc.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = win.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
} else if (document.selection) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var selectedTextRange = doc.selection.createRange();
        var preSelectionTextRange = doc.body.createTextRange();
        preSelectionTextRange.moveToElementText(containerEl);
        preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
        var start = preSelectionTextRange.text.length;

        return {
            start: start,
            end: start + selectedTextRange.text.length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var textRange = doc.body.createTextRange();
        textRange.moveToElementText(containerEl);
        textRange.collapse(true);
        textRange.moveEnd("character", savedSel.end);
        textRange.moveStart("character", savedSel.start);
        textRange.select();
    };
}
*/

/*************END Keep selected text when focus on input color*************/
 
var writeatexthere = "<?php echo Yii::t("cms", "Write a text")?>...";
// Initialise le texte par défault 
$(".sp-text").each(function(i, sptext) {
  let $sptext = ($(sptext).text()).toLowerCase();
  if ($(sptext).text() == "") {
    $(sptext).html(writeatexthere);
  }
})
</script>
