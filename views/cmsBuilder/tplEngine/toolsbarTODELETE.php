<ul class="tpl-cms-menu sp-cms-context-menu blur">
    <li class="tpl-cms-menu-item" style="order : 90">
        <button type="button" class="tpl-cms-menu-btn add-cms-here">
            <i class="fa fa-level-down"></i>
            <span class="tpl-cms-menu-text"><?php echo Yii::t("common", "Add a block in the page")?></span>
        </button>
    </li>
<!--     <li class="tpl-cms-menu-item" style="order : 100">
        <a class="tpl-cms-menu-btn" href="javascript:;"> 
            <i class="fa fa-eye"></i>
            <span class="tpl-cms-menu-text hiddenEdit"><i class="fa fa-pencil"> </i> Éditer cms</span>
        </a>
    </li> -->
    <li class="tpl-cms-menu-item sp-ctx-hiddable hidden">
        <button type="button" class="tpl-cms-menu-btn edit-cms editSectionBtns cms-edition  cms-up-date-btn" data-collection="cms">
            <i class="fa fa-pencil-square"></i>        
            <span class="tpl-cms-menu-text">Configure this block</span>
        </button>
    </li>

    <li class="tpl-cms-menu-item sp-ctx-hiddable hidden">
        <button type="button" class="tpl-cms-menu-btn  deleteLine cms-deleteLine cms-up-date-btn" data-collection="cms">
            <i class="fa fa-trash"></i>        
            <span class="tpl-cms-menu-text">Delete this block</span>
        </button>
    </li>

    <li class="tpl-cms-menu-item sp-ctx-hiddable hidden">
        <button type="button" class="tpl-cms-menu-btn cms-duplicate-btn cms-up-date-btn" data-collection="cms">
            <i class="fa fa-files-o"></i>        
            <span class="tpl-cms-menu-text">Duplicate this block</span>
        </button>
    </li>

 <!--    <li class="tpl-cms-menu-item">
        <button type="button" class="tpl-cms-menu-btn save-as-cms" data-id="6346c777efdc16343c55bf33">
            <i class="fa fa-save"></i>        
            <span class="tpl-cms-menu-text">Save as block CMS</span>
        </button>
    </li> -->

    <li class="tpl-cms-menu-item sp-ctx-hiddable hidden">
        <button type="button" class="tpl-cms-menu-btn add-cms-here" data-collection="cms">
            <i class="fa fa-arrow-circle-up"></i>        
            <span class="tpl-cms-menu-text">Add a block above</span>
        </button>
    </li>

    <li class="tpl-cms-menu-item sp-ctx-hiddable hidden">
        <button type="button" class="tpl-cms-menu-btn add-cms-here" data-desire="below" data-collection="cms">
            <i class="fa fa-arrow-circle-down"></i>        
            <span class="tpl-cms-menu-text">Add a block cms below</span>
        </button>
    </li>
    <li class="tpl-cms-menu-separator sp-ctx-hiddable hidden"></li>
    <?php ?>               
  <li class="tpl-cms-menu-item" style="order : 102">
    <a href="javascript:;" class="saveThisTpls tpl-cms-menu-btn" onclick="cmsBuilder.tpl.saveThis();">    
    <i class="fa fa-download"></i>
    <span class="tpl-cms-menu-text"> <?php echo Yii::t("common", "Save as new template")?> </span>
  </a>
</li>
<?php  ?>
</ul>