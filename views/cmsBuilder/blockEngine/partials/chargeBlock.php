<?php

if (isset($v["path"])) {
  $path = $v["path"];
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
  $pathExplode = explode('.', $v["path"]);
  $count = count($pathExplode);
  $content = isset($v['content']) ? $v['content'] : [];
  $kunik = $pathExplode[$count - 1] . $v["_id"];
  $blockKey = (string)$v["_id"];
  $blockName = (string)@$v["name"];
  $params = [
    "cmsList"   => $cmsList,
    "blockKey"  => $blockKey,
    "blockCms"  => $v,
    "page"      => $page,
    "canEdit"   => $canEdit,
    "type"      => $path,
    "kunik"     => $kunik,
    "content"   => $content,
    'blockName' => $blockName,
    "costum"=>$costumData,
    "el"=> [],
    'range'     => $i,
    "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg"
  ];

  $width = [
    "modeLg" =>  isset($v["modeLg"]) ? $v["modeLg"] : "12",
    "modeMd" =>  isset($v["modeMd"]) ? $v["modeMd"] : "12",
    "modeSm" =>  isset($v["modeSm"]) ? $v["modeSm"] : "12",
    "modeXs" =>  isset($v["modeXs"]) ? $v["modeXs"] : "12"
  ];

  if (is_file($this->getViewFile("costum.views." . $path, $params))) { 
    $dataAnchorTarget=(!empty($v["anchorTarget"])) ? " data-anchor-target='".substr($v["anchorTarget"], 1)."' " : "";
    $classLoading=($i>=2) ? "is-loading-block" : "";
    if (!@$v["blockParent"]) { ?>

      <div 
      id="<?= $v["_id"] . "-" . $i ?>" 
      class="cmsbuilder-block <?= ($v["path"]==="tpls.blockCms.superCms.container")?"cmsbuilder-block-droppable":"" ?> sortable-<?= $kunik ?> custom-block-cms col-xs-12 no-padding col-lg-<?= $width["modeLg"] ?> col-md-<?= $width["modeMd"] ?> col-sm-<?= $width["modeSm"] ?> col-xs-<?= $width["modeXs"] ?> block-container-<?= $kunik ?> <?= $classLoading ?>  sp-bg" 
      data-blocktype="section"
      data-sptarget="background" 
      data-path="<?= $v["path"] ?>"  
      data-id="<?= $v["_id"]?>" 
      <?php echo  $dataAnchorTarget; ?>
      data-kunik="<?= $kunik ?>" 
      data-name="<?= ($v["path"]==="tpls.blockCms.superCms.container")?"Section":$blockName ?>"
      >

        <div class="block-container-html">
          <?php echo $this->renderPartial("costum.views.".$path, $params); ?>
        </div>
      </div>
    <?php }else
      echo $this->renderPartial("costum.views.".$path, $params);

  } else { ?>
    <div class="col-xs-12 text-center" id="<?php echo (string)$v["_id"]; ?>">
      <?php echo $this->renderPartial("costum.views.tpls.blockNotFound", ["blockKey" => $blockKey]) ?>
    </div>
    <?php
  }
}
?>