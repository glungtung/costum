<!--  -->
  <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 821.8 613" style="enable-background:new 0 0 821.8 613;" xml:space="preserve">

<a href="#economie" class="lbh" title="Economie"><?php echo Yii::t("home","") ?>
  <polygon id="dessus" points="310.4,208.5 310.4,101.8 218,48.5 125.7,101.8 125.7,208.5 218,261.8 "/>
  <polygon id="1" class="hex" points="310.4,208.5 310.4,101.8 218,48.5 125.7,101.8 125.7,208.5 218,261.8 " fill="url(#img1)"/>

    <defs>
      <pattern id="img1" patternUnits="userSpaceOnUse" width="500" height="500">  
        <image  xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/hexagone.png" width="436" height="275" />
        </image>
      </pattern>
    </defs>
    <text class="text1" font-size="20" x="215" y="172" text-anchor="middle">ECONOMIE</text>
</a>

<a href="#citoyennete" class="lbh" title="Citoyenneté"><?php echo Yii::t("home","") ?>
  <polygon id="dessus" points="501.7,208.5 501.7,101.8 409.3,48.5 317,101.8 317,208.5 409.3,261.8 "/>
  <polygon id="2" class="hex" points="501.7,208.5 501.7,101.8 409.3,48.5 317,101.8 317,208.5 409.3,261.8 " fill="url(#img2)"/>

  <defs>
      <pattern  id="img2" patternUnits="userSpaceOnUse" width="268" height="262">
        <image  xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/citoyennete.jpg" width="800" height="366" />
      </pattern>
    </defs>
    <text class="text2" font-size="20" x="413" y="168" text-anchor="middle">CITOYENNETE</text>
</a>

<a href="#sante" class="lbh" title="Alimentation et Santé"><?php echo Yii::t("home","") ?>
  <polygon id="dessus"  points="693,208.5 693,101.8 600.6,48.5 508.3,101.8 508.3,208.5 600.6,261.8 "/>
  <polygon id="3" class="hex" points="693,208.5 693,101.8 600.6,48.5 508.3,101.8 508.3,208.5 600.6,261.8 " fill="url(#img3)"/>

  <defs>
      <pattern id="img3" patternUnits="userSpaceOnUse" width="229.5" height="265">
        <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/alimentation.png" width="335" height="306" />
      </pattern>
    </defs>
    <text class="text3" font-size="20" x="602" y="168" text-anchor="middle">ALIMENTATION</text>
    <text class="text3" font-size="20" x="602" y="191" text-anchor="middle">SANTE</text>
</a>

<a href="#construction" class="lbh" title="Construction"><?php echo Yii::t("home","") ?>
  <polygon id="dessus"  points="407.3,372.9 407.3,266.3 315,212.9 222.6,266.3 222.6,372.9 315,426.3 "/>
  <polygon id="5" class="hex" points="407.3,372.9 407.3,266.3 315,212.9 222.6,266.3 222.6,372.9 315,426.3 " fill="url(#img5)"/>

  <defs>
      <pattern id="img5" patternUnits="userSpaceOnUse" width="500" height="500">
        <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/construction.png" width="540" height="558" />
      </pattern>
    </defs>
    <text class="text5" font-size="18" x="318" y="352" text-anchor="middle">CONSTRUCTIONS</text>
    <text class="text5" font-size="20" x="315" y="329" text-anchor="middle">AMENAGEMENT</text>
</a>

<a href="#education" class="lbh" title="Education"><?php echo Yii::t("home","") ?>
  <polygon id="dessus" points="598.6,372.9 598.6,266.3 506.3,212.9 413.9,266.3 413.9,372.9 506.3,426.3"/>
  <polygon id="6" class="hex" points="598.6,372.9 598.6,266.3 506.3,212.9 413.9,266.3 413.9,372.9 506.3,426.3" fill="url(#img6)"/>

  <defs>
      <pattern id="img6" patternUnits="userSpaceOnUse" width="303" height="211">
        <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/education.png" width="361" height="220" />
      </pattern>
    </defs>
    <text class="text6" font-size="20" x="510" y="329" text-anchor="middle">EDUCATION</text>
</a>

<a href="#energie" class="lbh" title="Energie"><?php echo Yii::t("home","") ?>
  <polygon id="dessus" points="312.4,538.6 312.4,431.9 220,378.6 127.7,431.9 127.7,538.6 220,591.9 "/>
  <polygon id="8" class="hex" points="312.4,538.6 312.4,431.9 220,378.6 127.7,431.9 127.7,538.6 220,591.9 " fill="url(#img8)"/>

  <defs>
      <pattern id="img8" patternUnits="userSpaceOnUse" width="312" height="372">
        <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/energie.png" width="456" height="227" />
      </pattern>
    </defs>
    <text class="text8" font-size="20" x="220" y="490" text-anchor="middle">TRANSPORT</text>
    <text class="text8" font-size="20" x="220" y="510" text-anchor="middle">ENERGIE</text>
</a>

<a href="#dechets" class="lbh" title="Déchets"><?php echo Yii::t("home","") ?>
  <polygon points="503.7,538.6 503.7,431.9 411.3,378.6 319,431.9 319,538.6 411.3,591.9 "/>
  <polygon id="4" class="hex" points="503.7,538.6 503.7,431.9 411.3,378.6 319,431.9 319,538.6 411.3,591.9 " fill="url(#img4)"/>

  <defs>
      <pattern id="img4" patternUnits="userSpaceOnUse" width="277" height="299">
        <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/dechets.png" width="269" height="293" />
        </image>
      </pattern>
    </defs>
    <text class="text4" font-size="20" x="413" y="496" text-anchor="middle">DECHETS</text>
</a>

<a href="#commun" class="lbh" title="Les communs"><?php echo Yii::t("home","") ?>
  <polygon id="dessus" points="695,538.6 695,431.9 602.6,378.6 510.3,431.9 510.3,538.6 602.6,591.9 "/>
  <polygon id="7" class="hex" points="695,538.6 695,431.9 602.6,378.6 510.3,431.9 510.3,538.6 602.6,591.9 " fill="url(#img7)"/>

<defs>
      <pattern id="img7" patternUnits="userSpaceOnUse" width="233" height="376">
        <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/commun.png" x="-25" width="319" height="214" />
        </image>
      </pattern>
    </defs>
    <text class="text7" font-size="20" x="606" y="493" text-anchor="middle">LES COMMUNS</text>
</a>

<a href="#commun" title="Les communs"><?php echo Yii::t("home","") ?>
    <img id="im7" style="filter: invert(100%);position: absolute;width: 4%;margin-left: -28%;margin-top: 49%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/commun.svg">
  </a>

  <a href="#dechets" title="Déchets"><?php echo Yii::t("home","") ?>
    <img id="im4" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 46.5%;margin-top: -22.7%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/Dechets.svg">
  </a>

  <a href="#energie" title="Energie"><?php echo Yii::t("home","") ?>
    <img id="im8" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 23.4%;margin-top: -23%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/energie.svg">
  </a>

  <a href="#education" title="Education"><?php echo Yii::t("home","") ?>
    <img id="im6" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 57%;margin-top: -41%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/education.svg">
  </a>

  <a href="#construction" title="Construction"><?php echo Yii::t("home","") ?>
    <img id="im5" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 35%;margin-top: -41.2%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/Construction.svg">
  </a>

  <a href="#sante" title="Alimentation et Santé"><?php echo Yii::t("home","") ?>
    <img id="im3" style="filter:invert(100%);position: absolute;width: 4%;margin-left: 68.2%;margin-top: -60%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/alimentation.svg">
  </a>

  <a href="#citoyennete" class="lbh" title="Citoyenneté"><?php echo Yii::t("home","") ?>
    <img id="im2" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 46.5%;margin-top: -62%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/citoy.svg">
  </a>

  <a href="#economie" class="lbh" title="Economie"><?php echo Yii::t("home","") ?>
    <img id="im1" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 23.5%;margin-top: -61%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/economie.svg">
  </a>
</svg>

<script type="text/javascript">
    $("polygon").each(function(){
        
        $(".text"+$(this).attr('id')).css("filter", "invert(100%)");
        // $(".text"+$(this).attr('id')).css("visibility", "hidden");
    });
</script>