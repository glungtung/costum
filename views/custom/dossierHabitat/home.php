<?php 
$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"PTEMontreuil"),array("updated"=>-1), 3, 0);
?>
<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  .btn-main-menu{
    background: #71b62c;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #8a2f88;
    background-color: white;
    color: #8a2f88;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #8a2f88;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #71b62c;
  }
  .text-explain{
    color: #555;
    font-size: 25px;
  }
  .blue-bg {
  background-color: white;
  color: #8a2f88;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #71b62c;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #8a2f88;
    border: inset 3px #8a2f88;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #8a2f88;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}

.container-info-pod{
  min-height: 300px;
  box-shadow: 1px 0px 6px 2px rgba(0, 0, 0, 0.68);
  border-radius: 1px 20px 1px 20px;
  background-color: white;

}
.container-info-pod .title-encart{
  margin-top: -20px;
    margin-left: auto;
    margin-right: auto;
    height: 40px;
    line-height: 40px;
    font-size: 25px;
    font-weight: 800;
    width: 75%;
    padding: 0px 10px;
    border-radius: 3px;
    box-shadow: 1px 1px 2px 1px rgba(0, 0, 0, 0.5);
  }
.container-info-pod .encart-info{
  font-size: 18px;
    line-height: 30px;
    color: #676674 !important;

}

  .content-home-pod{
    margin-top: -150px; 
  }
  .title-home-banner{
    position: absolute;
    left: 0px;
    right: 0px;
    padding-left: 30%;
    width: 100%;
    background-color: rgba(0,0,0,0.3);
    margin-top: 75px;
    padding-top: 20px;
    padding-bottom: 15px;
  }
  .img-home-banner{
    z-index: 1;
  }
  .btn-asking-answer{
    color: white !important;
    font-size: 18px;
    font-weight: 900;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }

  }
  @media (max-width: 1200px){

      .content-home-pod{
        margin-top: -100px; 
      }
      #customHeader{
        margin-top: -1px;
      }
    }
  @media (min-width: 1400px){
      .content-home-pod{
        margin-top: -200px; 
      }
    }
    
    @media (max-width: 991px){
      .content-home-pod{
        margin-top: -50px; 
      }      
    }
    @media (max-width: 768px){

      .content-home-pod{
        margin-top: 0px; 
      }
      .title-home-banner{
        padding: 40px;
        top: 0px;
        bottom: 0px;
        text-align: center;
        margin-top: 0px;
      }
    }
    @media (max-width: 450px){
      .title-home-banner{
        padding:10px 20px;
        font-size: 14px;
      }
      .size-pod{
        width: 100% !important;
      }
    }
  
	
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>Mayenne Demain<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/deal/home_banner.png'> 
    <div style="position: absolute; top: 0px; bottom:0px; left: 0px;right: 0px;padding: 50px; ">
      <div class="col-xs-3 hidden-xs img-home-banner">
        <img class="img-responsive margin-auto" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/deal/logo.png' style="max-height: 250px;">
      </div>
      <div class="title-home-banner">
        <h2 class="text-white">La DEAL</h2>
        <h3 class="text-white">Direction de l'Environnement, de l'amménagement et du Logement</h3>
      </div>
    </div>
  </div>
  <!--<div class="col-xs-12 bg-green padding-20">
    <i class="fa fa-search text-white pull-left fa-3x margin-left-20"></i>
    <input type="text" name="" class="form-control pull-left" placeholder="Rechercher" style="width: 60%;border-radius: 25px;height: 45px;"/>
  </div>-->
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 padding-20 content-home-pod">
    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-0 padding-20 text-center size-pod">
      <div class="col-xs-12 container-info-pod">
        <div class="title-encart bg-green elipsis">
          <span class="text-white"><i class="fa fa-home"></i> Propriétaire</span>
        </div>
        <div class="encart-info margin-top-35">
          <span class="bold">Je suis propriétaire occupant<br/></span>
          <span class="">Vous êtes propriétaire du logement que vous habitez ou que vous habiterez bientôt ?<br/> Vous souhaitez faire des travaux ?<br/><br/></span>
          <a href="#answer.new" class="btn bg-green btn-asking-answer radius-10">Déposer une demande</a>
        </div>
      </div>
    </div>
    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-0 padding-20 text-center">
      <div class="col-xs-12 container-info-pod">
        <div class="title-encart bg-green elipsis">
          <span class="text-white"><i class="fa fa-building"></i> Copropriétaire</span>
        </div>
        <div class="encart-info margin-top-35">
         <span class="bold">Je représente une copropriété<br/></span>
          <span class="">Vous êtes membre ou représentant d’une copropriété ?<br/> Vous souhaitez faire des travaux dans les parties communes ?<br/><br/></span>
          <a href="#answer.new" class="btn bg-green btn-asking-answer radius-10">Déposer une demande</a>
        </div>   
      </div>
    </div>
    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0 padding-20 text-center">
      <div class="col-xs-12 container-info-pod">
        <div class="title-encart bg-green elipsis">
          <span class="text-white"><i class="fa fa-user-secret"></i> Opérateur</span>
        </div>
        <div class="encart-info margin-top-35">
          <span class="bold">J’ai été désigné comme mandataire<br/></span>
          <span class="">Un propriétaire vous a demandé<br/> de faire sa demande d’aide<br/> à sa place ?<br/><br/></span>
          <a href="#answer.new" class="btn bg-green btn-asking-answer radius-10">Entamer la démarche</a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12">
    <div class="col-xs-3 col-xs-offset-4 margin-top-35 margin-bottom-30" style="border: 2px solid #c7c7c7;"></div>
    <span class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2" style="font-size: 25px;text-align: center;color: #666574;padding-top: 40px;">
      La DEAL et son équipe vous propose de vous accompagner sur vos démarches de construction.<br/><br/>
      A la fois, transparent et numérique, la collecte des dossiers va permettre de mobiliser des équipes compétentes,<br/><br/>
      Opérateur, expertise, financement et suivi,<br/><br/>
      afin de vous permettre de mener à bien vos futures réalisations
    </span>
  </div>
<script type="text/javascript">

	jQuery(document).ready(function() {
		setTitle("La DEAL");
    coInterface.bindLBHLinks();	
	});


</script>