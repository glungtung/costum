<style type="text/css">
    .col-footer a{ color: #254c97 !important; font-size: 16px;}
    .borderHr{height:1px;background-color: #254c97 !important;}
    footer{
        color: #254c97;
    }
</style>
<footer class="text-center col-xs-12 pull-left no-padding margin-top-30">
    <hr class="col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 borderHr"></hr>
     <div class="">
        <div class="container padding-top-30">
            <div class="row">
                <div class="col-xs-12 text-center col-footer col-footer-step col-footer-left">
                    <!--<h5><i class="fa fa-info-circle hidden-xs"></i> <?php echo Yii::t("home", "General information"); ?></h5>-->
                    <a href="mailto:pacte@transition-citoyenne.org" class="">infos@deal.gouv.fr</a> ⋅
                    <a href="#welcome" class="lbh"><?php echo Yii::t("home","General information") ?></a> ⋅ 
                    <a href="#welcome" class="lbh"><?php echo Yii::t("home","Mentions légales") ?></a> 
                </div>
                <div class="col-xs-12 col-footer padding-bottom-15">
                   <!-- <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/PacteTransition/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/PacteTransition" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-twitter"></i>
                            </a>
                        </li>
                     </ul>-->
                    <span class="text-dark">Powered by <a href="https://gitlab.adullact.net/pixelhumain" target="_blank">PixelHumain</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
