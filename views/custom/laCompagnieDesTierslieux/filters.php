<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	defaults : {
	 		indexStep : 0,
	 		types:["organizations"]
	 	},
	 	filters : {
	 		scope :true,
	 		typePlace : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Type",
	 			event : "tags",
	 			list : costum.lists.typePlace
	 		},
	 		services:{
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Services",
	 			event : "tags",
	 			list : costum.lists.services
	 		},
	 		manageModel : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Modèle",
	 			event : "tags",
	 			list : costum.lists.manageModel
	 		},
	 		state : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Etat",
	 			event : "tags",
	 			list : costum.lists.state
	 		},
	 		spaceSize : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Taille",
	 			event : "tags",
	 			list : costum.lists.spaceSize
	 		}
	 	},
		results:{
			map:{
				active:true
			}
		},
		mapContent:{
			hideViews:["btnHideMap"]
		}
	};
	 
	// function lazyFilters(time){
	//   if(typeof searchObj != "undefined" )
	//     filterGroup = searchObj.init(paramsFilter);
	//   else
	//     setTimeout(function(){
	//       lazyFilters(time+200)
	//     }, time);
	// }

		var filterSearch={};

	jQuery(document).ready(function() {
		  filterSearch = searchObj.init(paramsFilter);
	});
</script>