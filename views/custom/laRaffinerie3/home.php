<?php
$raf = PHDB::findOne(	Organization::COLLECTION, 
												array( "slug" => $this->costum["slug"]) , 
												array("name", "slug", "links", "description") ) ;
Yii::import("parsedown.Parsedown", true);
$Parsedown = new Parsedown();
?>
<style>

#newsstream  .loader{
	display: none;
}
.list-inline {
  display: flex;
  justify-content: center;
}

.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}
.carousel-inner img.img-responsive {
	width: 100%;
}
#myCarouselRaf .carousel-inner>.item>img.img-responsive {
    max-height: fit-content!important;
}

.table-container {
	display: table;
	table-layout: fixed;
	width: 100%;
	height: 100%;
}
.table-container2 {
	display: table;
	table-layout: fixed;
	width: 130px;
	height: 130px;
	font-size: 20px;
}

.table-container .col-table-cell, .table-container2 .col-table-cell {
	display: table-cell;
	vertical-align: middle;
	float: none;
}

.glyphicon{
	font-family: 'Glyphicons Halflings' !important;
}

</style>
<div class="col-xs-12 no-padding">
	<?php 
			// echo $this->costum["contextType"];

			// echo $this->costum["contextId"];

			$folder = PHDB::findOne(Folder::COLLECTION, array("contextId" => $this->costum["contextId"], 
				"contextType" => $this->costum["contextType"],
				"name" => "Accueil"));

			if(!empty($folder)){
				$docs = PHDB::find(Document::COLLECTION, 
					array(	"id" => $this->costum["contextId"], 
							"type" => $this->costum["contextType"],
							"folderId" => (String)$folder["_id"]) ) ;
				

				if(!empty($docs)){ 
					
					 //$this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"articleCarousel","nbItem"=>count($docs),"carousel_indicators_color"=>"#C9462C")); 
					?>
					<div class="col-xs-12 no-padding">
		
						<div id="myCarouselRaf" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
							<ol class="carousel-indicators">

								<?php
								$count = 0;
									foreach ($docs as $keyD => $valD) {
										if($count == 0)
											echo '<li data-target="#myCarouselRaf" data-slide-to="'.$count.'" class="active"></li>';
										else
											echo '<li data-target="#myCarouselRaf" data-slide-to="'.$count.'"></li>';
										$count++;
									}
								?>
							</ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner">
								<?php
									$count = 0;
									foreach ($docs as $keyD => $valD) {
										$class = 'item';
										if($count == 0)
											$class = 'item active';
										
										echo '<div class="text-center '.$class.'">'.
												'<img class="img-responsive" src="'.Yii::app()->getRequest()->getBaseUrl(true).Document::getDocumentUrl($valD).'" alt="'.$valD['name'].'">'.
											'</div>';
										$count++;
									}
								?>
							</div>

							<!-- Left and right controls -->
							<a class="left carousel-control" href="#myCarouselRaf" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#myCarouselRaf" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				<?php
				}
			}
			
		?>
	
	<div class="col-xs-12 padding-10 text-center">
		<?php 
		if(!empty($raf["description"]))
			echo $Parsedown->text($raf["description"]); 
		?>
	</div>
		<div class="col-xs-12 text-center">
		<a href="#apropos" class="lbh ">En savoir plus</a>
	</div>

	<a href="https://raffinerie.tibillet.re/adhesions">
		<img class="img-responsive"  src="https://raffinerie.tibillet.re/media/images/Adhesion.png">
	</a>


	<?php
		$strPoles = "";
		$strPolesSelect = "";

		$strProjects = "";
		$strProjectsSelect = "";

		if(!empty($raf) && !empty($raf["links"])){
			if(!empty($raf["links"]["projects"])){
				$idArray = array();
				foreach ($raf["links"]["projects"] as $idSubProject => $valSubProject) {
					$idArray[] = new MongoId($idSubProject) ;
				}
				$allSubProject = PHDB::find(Project::COLLECTION, 
											array( "_id" => 
												array('$in' => $idArray)), 
											array("name", "slug", "tags", "preferences", "links") );
			}
		}

		$tagAdmin = array();
		$tagsSlug = array();
		foreach ($this->costum["paramsData"]["poles"] as $keyP => $valPoles) {
			$slugTag = InflectorHelper::slugify2($keyP);
			$tagsSlug[$slugTag] = $keyP;
			if(!empty($valPoles["isAdmin"]))
				$tagAdmin[$keyP] = 0;

			if(!empty($allSubProject )){
				foreach ($allSubProject as $key => $value) {

					if(!empty($value["tags"]) && in_array($keyP, $value["tags"])){
						$seeP = false ;
						if( !empty($value["preferences"]) && 
							!empty($value["preferences"]["private"]) ){

							$seeP = Link::isLinked($key, Project::COLLECTION, Yii::app()->session["userId"], @$value["links"]);
						}else{
							$seeP = true ;
						}


						

						if( $seeP ){
							if(isset($tagAdmin[$keyP]))
								$tagAdmin[$keyP]++;

							$strProjects .= "<li class='subProject ".$slugTag." '>".
												"<a href='#@".$value["slug"]."' class='lbh titleFont1' style='opacity: 0.8; background-color: ".$valPoles["color"]." ;'>".
												"<div class='table-container2 text-center no-padding'>".
													"<div class='col-table-cell col-xs-12 no-padding'>".$value["name"]."</div>".
												"</div></a>".
											"</li>";
							$strProjectsSelect .= "<li class='subProject ".$slugTag." '>".
													"<a href='#@".$value["slug"]."' class='lbh' style='background-color: ".$valPoles["color"]." ;'>".$value["name"]."</a>".
												"</li>";	
						}
					}
					
				}
			}
			$seePoles = false ;

			if( isset($tagAdmin[$keyP]) && $tagAdmin[$keyP] > 0 )
				$seePoles = true;
			else if(!isset($tagAdmin[$keyP]))
				$seePoles = true;

			if($seePoles == true){
				$strPolesSelect .= "<option value='".$slugTag."'>".$keyP."</option>";
				$strPoles .= "<li class=''>".
								"<a href='javascript:;' class='linkMenu poles titleFont1' data-pole='".$slugTag."' data-polename='".$keyP."'" .
									"style='background-color: ".$valPoles["color"]."'>".
								"<div class='table-container text-center no-padding'>".
									"<div class='col-table-cell col-xs-12 no-padding'>".$keyP."</div>".
								"</div></a>".
							"</li>";
			}

			
			
		}

		//var_dump($tagAdmin);
	?>
			<!-- col-xs-1 col-xs-offset-1 -->
				

	<div id="" class="col-xs-12 hidden-sm hidden-md hidden-lg no-padding">
		<div class='col-xs-12 '>
			<select id="chooseTags" class='col-xs-12'>
				<option value="">Choisir un pole</option>
				<?php echo $strPolesSelect; ?>
			</select>
		</div>
		<div class='col-xs-12 '>
			<ul class='col-xs-12 '>
				<?php echo $strProjectsSelect; ?>
			</ul>
		</div>
	</div>
	<div id="projectChildren" class="col-sm-12 hidden-xs no-padding">
		<div class='row listPoles'>
			<ul class="list-inline">
				<?php echo $strPoles; ?>
			</ul>
		</div>
		<div class='col-xs-12 '>
			<ul class='list-inline'>
				<?php echo $strProjects; ?>
			</ul>
		</div>
	</div>
	<div class="col-xs-12 col-sm-offset-1 col-sm-10 margin-top-50 no-padding">
		<div  class="col-xs-12 no-padding">
			<h2 class="titleFont2 titleSectionHome "> Agenda <span class="namePole titleFont2"></span> </h2>
			<hr style="margin-top: 5px; border-bottom: 1px dashed #000000;"/>
		</div>
		<div id="lastevents" class="col-xs-12 no-padding">
		</div>
		<div id="" class="col-xs-12">
			<a href="#@laRaffinerie3.view.directory.dir.events" class="lbh btn btn-default pull-right" style="">Accès à l’agenda</a>
		</div>
    </div>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 no-padding margin-top-50">
		<div  class="col-xs-12 no-padding">
			<h2 class="titleFont2 titleSectionHome"> Actualités <span class="namePole titleFont2"></span> </h2>
			<hr style="margin-top: 5px; border-bottom: 1px dashed #000000;"/>
		</div>
		<div id="newsstream" class="hidden-xs hidden-sm col-xs-12 no-padding">
		</div>
		<div id="" class="col-xs-12 text-center">
			<a href="#@laRaffinerie3" class="lbh btn btn-default pull-right hidden-xs hidden-sm" style="">Accès à l’actualité</a>

			<a href="#@laRaffinerie3" class="lbh btn btn-default visible-xs visible-sm" style="">Accès à l’actualité</a>
		</div>
    </div>
</div>

<script type="text/javascript">



var slugTag = "";
var tagsSlug = <?php echo json_encode($tagsSlug) ?>;

var hashUrlPage= "#welcome";
    
var poleGet = "<?php echo @$_GET['pole']; ?>";
jQuery(document).ready(function() {
	// mylog.log("HERE laRaffinerie3");
	$(".subProject").hide();

	$(".poles").off().click(function () {
		$(".subProject").hide();
		//$("."+$(this).data("pole")).show();
		slugTag = $(this).data("pole");
		var nameSlug = $(this).data("polename");
		$(".namePole").html($(this).data("pole"));
		var style = "color: #d6d6d6;";
		$.each(costum.paramsData.poles, function(kT, vT){
			mylog.log("namePole poles", kT, vT, nameSlug);
			if(kT == nameSlug)
				style = "color: "+vT.color+";";
		});
		$(".namePole").attr("style", style);
		$(".titleSectionHome").addClass("initpole");
		loadPole();
	});

	$("#chooseTags").change(function () {
		$(".subProject").hide();
		//$("."+$(this).val()).show();

		slugTag = $(this).val();
		loadPole();
	});

	if(typeof poleGet != "undefined" && poleGet != null && typeof tagsSlug[poleGet] != "undefined" )
		slugTag = poleGet;
    loadPole();
	
});


function loadPole(){
	mylog.log("loadPole", slugTag);


	// if(location.hash != hashUrlPage+".pole."+slugTag){
	// 	if(slugTag == ""){
	// 		history.pushState(null, null, hashUrlPage);
	// 	}else 
	//  		history.pushState(null, null, hashUrlPage+".pole."+slugTag);
	// }

	if(slugTag == ""){
		getEvents();
		getActu();
		$(".subProject").hide();
	}else{
		$("."+slugTag).show();
		getEvents([tagsSlug[slugTag]]);
		getActu([tagsSlug[slugTag]]);	
	}

	$(".initpole").off().click(function () {
		slugTag = "";
		$(".namePole").html("");
		$(".titleSectionHome").removeClass("initpole");
		loadPole();
	});
	
}

function getDateTodayRaffinerie(){

	//TODO refaire la fonction et rendre custumisable la date de debut et de fin 
	mylog.log('directory.js getDateTodayRaffinerie');

	var today = new Date();
	today = new Date(today.setSeconds(0));
	today = new Date(today.setMinutes(0));
	today = new Date(today.setHours(0));
	var dateToday = today.setDate(today.getDate());
	return dateToday;

}

function getEvents(tags){
	ajaxPost("",baseUrl+"/co2/element/getlastevents",
    			{
    				col:costum.contextType, 
    				id:costum.contextId, 
    				nbEvent:4,
    				tags : tags,
    				startDateUTC : moment(getDateTodayRaffinerie()).format()
    			}, function(events){
    	//mylog.log("raffinerie2 getlastevents events", events);
    	var str = "<div class='col-xs-12 padding-10 margin-top-10'>";
    	if(typeof events != "undefined" && 
    		events != null && 
    		Object.keys(events).length > 0 ){
    		$.each(events, function(keyE, valE){
    			str += costum.laRaffinerie3.directoryEvent(keyE, valE);
    		});
    		$("#lastevents").html(str);
    	} else {
    		str += "Il n'y a pas d'événement." ;
    	}
    	str +=  "</div>";
    	$("#lastevents").html(str);
    }, "json");
}

function getActu(tags){
	var urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false/";
	searchObj.live.init = function(fObj){		
		fObj.live.options.dateLimit=0;
		coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
		var scrollTopPosFilter=(typeof pageProfil != "undefined" && typeof pageProfil.affixPageMenu != "undefined") ? pageProfil.affixPageMenu+5 : 0;
	}
	ajaxPost("#newsstream",baseUrl+"/"+urlNews,{
		typeNews : [ "news" ],
		indexStep : 4, 
		search:true, 
		formCreate:false, 
		scroll:false,
		searchTags : tags
	}, function(news){

	}, "html");
}
</script>
