<?php

HtmlHelper::registerCssAndScriptsFiles( 
		array( 
			'/vendor/colorpicker/js/colorpicker.js',
			'/vendor/colorpicker/css/colorpicker.css',
			'/css/default/directory.css',
			'/css/default/settings.css',	
			'/css/profilSocial.css',
			'/css/calendar.css',
		), Yii::app()->theme->baseUrl. '/assets'
	);


$cssAndScriptFilesTheme = array(
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	// MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
	'/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
	'/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesTheme, Yii::app()->request->baseUrl);
$cssAndScriptFilesModule = array(
	'/js/default/calendar.js',
	'/js/links.js',
	'/js/default/profilSocial.js',
	'/js/default/editInPlace.js',
    '/js/default/settings.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

$cssJsCostum=array();
array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/pageProfil.js');
HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
$canEdit = false;
$openEdition = false;
if (Authorisation::isElementAdmin($id, $type, @Yii::app()->session["userId"])) {
	$canEdit = true;
} else if (Authorisation::isSourceAdmin($id, $type, @Yii::app()->session["userId"])) {
	$canEdit = true;
}

?>
<!-- CUSTOM STYLE -->

<style>
	
	.customBlockImg{
		padding-bottom: 50%;
		background-position: center;
		background-size: cover;
	}

	#social-header{
		padding: 0px;
	}

	#social-header .section-badges, #social-header .contentHeaderInformation{
		display: none;
	}

	#nameElt{
		background:#7aba59;
		border-radius:50%;
		width:160px;
		height:160px;
		border:2px solid #7aba59;
		position: absolute;
		top: 25px;
		left: 75px;
		/*line-height: 150px;*/
		color: white;
		font-size: 22px;

	}
	#nameElt div{
		width: 160px;
    	height: 160px;
    	display: table-cell;
	    vertical-align: middle;
	    float: none;
	}

	.social-main-container{
		background-color: white;
	}

	.projectHeaderOptions{
		background-color: rgba(93, 95, 93, 0.6);
	    position: absolute;
	    color: white;
	    font-size: 22px;
	    /*top: 204px;*/
	    height: 76px;
	}

	#projectShortDescription, #projectShortDescription a{
		font-size: 16px !important;
	}

@media (min-width: 991px){
	.section-date {
		top: 80px;
	}	
}


</style>
<div class="project no-padding">
	
	<!-- <div class="projectWrapper"> -->
	<?php
		echo $this->renderPartial('co2.views.element.header', 
			        			array(	
			        					"type"=>$type,
			        					"element"=>$element,
			        					"pageConfig"=>$pageConfig,
			        					"linksBtn"=>$linksBtn,
		        						"invitedMe"=>@$invitedMe,
			        					"elementId"=>(string)$element["_id"],
			        					"elementType"=>$type,
			        					"elementName"=> $element["name"],
			        					"edit" => @$canEdit,
			        					"openEdition" => @$openEdition) 
			        			);
	?>
	<?php if(in_array($type, [Event::COLLECTION])){ 
			if(@$element['parent'] || @$element['organizer'] ){ ?>
				<div class="section-date pull-right">
					<?php if($type==Event::COLLECTION){ ?>
						<div class="event-infos-header"  style="font-size: 14px;font-weight: none;"></div>
					<?php } ?>
					<div style="font-size: 14px;font-weight: none;">
						<div id="parentHeader" >
							<?php 
							if(@$element['parent']){
								$count=count($element["parent"]);
								$msg = ($type==Event::COLLECTION) ? Yii::t("common","Planned on") : Yii::t("common","Carried by") ;
								echo $msg. " : ";
								foreach($element['parent'] as $key =>$v){
									$heightImg=($count>1) ? 35 : 25;
									$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
									<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
										class="lbh tooltips"
										<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
										<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
										<?php  if ($count==1) echo $v['name']; ?>
									</a>
									 
							<?php } ?> <br> 
						<?php } ?>
						</div>
						<div id="organizerHeader" >
							<?php 
							if(@$element['organizer']){
								$count=count($element["organizer"]);
								echo Yii::t("common","Organized by"). " : ";
								foreach($element['organizer'] as $key =>$v){
									$heightImg=($count>1) ? 35 : 25;
									$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
								<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
										class="lbh tooltips"
										<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
										<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
										<?php  if ($count==1) echo $v['name']; ?>
									</a>
										 
								<?php 
								} 
							} ?>
						</div>
					</div>
			    </div>
			<?php }
	 		} ?>
	<div id="nameElt">
		<div class="text-center titleFont1"><?php echo $element["name"] ; ?></div>
	</div>
	<div class="projectHeaderOptions padding-5 col-xs-12">

		<div class="col-xs-12 no-padding projectHeaderOptionsCont2">
			
			<div id="projectShortDescription" class="hidden-xs col-sm-6 col-md-12">
				<?php if(!empty($element["shortDescription"])){ ?>
				<div class=" col-xs-12 padding-5"><?php echo $element["shortDescription"] ; ?></div>
				<?php } ?>
				
			</div>
			<div id="" class="col-xs-12 col-sm-6 col-md-12">
				<a href="javascript:;" class="btn btn-default margin-right-10 projectNavTriggerMobile pull-left hidden-md hidden-lg"><i class="fa fa-bars tooltips"></i></a>
				<a href="javascript:;" id="apropos" class="pull-left" style="color: white; font-size: 16px">+ d'infos / contact</a>
				<?php if(@Yii::app()->session["userId"] && $canEdit==true){ ?>
					<div class="dropdown pull-right margin-right-10">
						<button class="btn btn-default bg-green dropdown-toggle" type="button" id="dropdownMenuList" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-cog fa-x2"></i>
						</button>
						<ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuList">
							<li class="text-left">
								<a href="javascript:;" id="community" class="bg-white ssmla">
									<?php echo Yii::t("common", "Community"); ?>
								</a>
							</li>
							<li class="text-left">
								<a href="javascript:;" id="projectContacts" class="bg-white ssmla"><?php echo Yii::t("common", "Contacts"); ?></a>
							</li>
							<li class="text-left">
								<a href="javascript:;" id="aproposAdmin" class="bg-white ssmla" style=""><?php echo Yii::t("common", "Update"); ?></a>
							</li>
							<?php
							if( /* $type == Project::COLLECTION || */ $type == Organization::COLLECTION){ ?>
								<li class="text-left">
									<a href="javascript:;" id="projectsAdmin" data-type-dir="projects" class="bg-white ssmla" style=""><?php echo Yii::t("common", "Projects"); ?></a>
								</li>
							<?php } ?>
							<!-- <li class="text-left">
								<a href="javascript:;" id="inviteRaf" data-type-dir="projects" class="bg-white ssmla" style=""></a>
							</li> -->
							<li class="text-left">
								<a href="#element.invite.type.<?php echo $type ;?>.id.<?php echo $id ;?>" class="lbhp"><?php echo Yii::t("common", "Invite"); ?></a>
							</li>
							<li class="text-left">
								<a href="javascript:;" id="documents" class="bg-white ssmla">Fichiers</a>
							</li>
							<li class="text-left">
								<a href="javascript:;" id="settings" class="bg-white ssmla">
									<?php echo Yii::t("common", "Settings"); ?>
								</a>
							</li>
							<li class="text-left">
								<a href="javascript:;" id="deletedElt" class="bg-red white ssmla">
									<?php echo Yii::t("common", "Remove"); ?>
								</a>
							</li>
						</ul>
					</div>
					<?php 
					if( $type == Project::COLLECTION || $type == Organization::COLLECTION){ ?>
						<a href="javascript:;" id="create" class="btn btn-default bg-green ssmla pull-right margin-right-10">
							<i class="fa fa-plus"></i>
						</a>
					<?php } ?>	
					<a href="https://www.laraffinerie.re/nextcloud" class="hidden-sm hidden-xs btn btn-default bg-green ssmla pull-right margin-right-10" target="_blank"><i class="fa fa-folder"></i></a>
				<?php } ?>
					<a id="follows" class="hidden-sm hidden-xs btn btn-default bg-green pull-right margin-right-10" style="color: white" href="javascript:;" data-isco="false" data-col="<?php echo $type ; ?>"  data-id="<?php echo $id ; ?>" class=""><?php echo ( ( Project::COLLECTION == $type &&  Organization::COLLECTION == $type ) ? "Participer au projet" : "Participer" ) ; ?> </a>
					<a href="javascript:;" id="btn-share-Group" class="hidden-sm hidden-xs btn btn-default bg-green pull-right margin-right-10 btn-link btn-share-panel" style="color: white" data-ownerlink="share" data-id="<?php echo $id ; ?>" data-type="<?php echo $type ; ?>"> Partager</a>
					<a href="https://meet.jit.si/<?php echo $element['slug'] ; ?>" target="_blank"  id="btnChatRaffinerie" class="hidden-sm hidden-xs btn btn-default bg-green pull-right margin-right-10" style="color: white">Visio</a>
					<a href="https://chat.communecter.org/channel/<?php echo $element['slug'] ; ?>" target="_blank"  id="btnChatRaffinerie" class="hidden-sm hidden-xs btn btn-default bg-green pull-right margin-right-10" style="color: white">Tchat</a>
							
					<div class="dropdown pull-right margin-right-10 hidden-md hidden-lg">
						<button class="btn btn-default bg-green dropdown-toggle" type="button" id="dropdownMenuList2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-angle-double-down  fa-x2"></i>
						</button>
						<ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuList2">
							<li class="text-left">
								<a id="follows" class="bg-white ssmla" href="javascript:;" data-isco="false" data-col="<?php echo $type ; ?>"  data-id="<?php echo $id ; ?>" class=""><?php echo ( ( Project::COLLECTION == $type &&  Organization::COLLECTION == $type ) ? "Participer au projet" : "Participer" ) ; ?> </a>
							</li>
							<li class="text-left">
								<a href="https://meet.jit.si/<?php echo $element['slug'] ; ?>" target="_blank"  id="" class="bg-white ssmla">Visio</a>
							</li>
							<li class="text-left">
								<a href="https://chat.communecter.org/channel/<?php echo $element['slug'] ; ?>" target="_blank"  id="" class="bg-white ssmla">Tchat</a>
							</li>
							<li>
								<a href="javascript:;" id="btn-share-Group" class="bg-white ssmla btn-link btn-share-panel" data-ownerlink="share" data-id="<?php echo $id ; ?>" data-type="<?php echo $type ; ?>"> Partager</a>
							</li>
							<!-- <li class="text-left">
								<a href="https://www.laraffinerie.re/nextcloud"  class="bg-white ssmla" target="_blank">Nextcloud</a>
							</li> -->
						</ul>
					</div>
			</div>
		</div>
	</div>

	<div class="projectNav col-sm-2">
		<div class="projectNavContent">
			<!-- <button class="toggleAllProjects">Ouvrir / Fermer sous-projets</button> -->
			<?php
				$strListProject = "";
				$raf = PHDB::findOne(	Organization::COLLECTION, 
									array( "slug" => $this->costum["slug"]) , 
									array("name", "slug", "links") ) ;

				//Rest::json($raf);
				if(!empty($raf) && !empty($raf["links"])){

					$strListProject .= "<ul class='projectNavFirstLvl'>".
											"<li>".
												"<a href='#@".$raf["slug"]."' class=' lbh linkOrganization'>".$raf["name"]."</a>".
											"</li>";
					// LEVEL 2

					if(!empty($raf["links"]["projects"])){
						$idArray = array();
						foreach ($raf["links"]["projects"] as $idSubProject => $valSubProject) {
							$idArray[] = new MongoId($idSubProject) ;
						}

						$allSubProject = PHDB::find(Project::COLLECTION, 
													array( "_id" => 
														array('$in' => $idArray)), 
													array("name", "slug", "tags", "preferences", "links") );
						$strListProject .= "<ul class='projectNavSecondLvl'>";
						$tagAdmin = array();

						foreach ($this->costum["paramsData"]["poles"] as $keyP => $valPoles) {
							if(!empty($valPoles["isAdmin"]))
								$tagAdmin[$keyP] = 0;

							

							$strSubProject = "" ;
							foreach ($allSubProject as $key => $value) {

								$seeP = false ;
								if( !empty($value["preferences"]) && 
									!empty($value["preferences"]["private"]) ){
									$seeP = Link::isLinked($key, Project::COLLECTION, Yii::app()->session["userId"], @$value["links"]);
								}else
									$seeP = true ;

								if( $seeP && !empty($value["tags"]) && in_array($keyP, $value["tags"]) ){
									if(isset($tagAdmin[$keyP]))
										$tagAdmin[$keyP]++;
									$strSubProject .="<li> <a href='#@".$value["slug"]."' class='lbh '>".$value["name"]."</a></li>";
										
								}
							}

							$seePoles = false ;

							if( isset($tagAdmin[$keyP]) && $tagAdmin[$keyP] > 0 )
								$seePoles = true;
							else if(!isset($tagAdmin[$keyP]))
								$seePoles = true;

							if($seePoles == true){
								$strListProject .="<li>".
													"<i class='toggleProjects showHidePole fa fa-plus' aria-hidden='true'></i>".
														"<a href='javascript:;' class='showHidePole projectBgColorAfterHover projectBgColorAfterActive' >".$keyP."</a></li>";
								$strListProject .= "<ul class='projectNavThirdLvl'>";
								$strListProject .= $strSubProject;
								$strListProject .= "</ul>";
							}


							
						}
						$strListProject .= "</ul>";
					}
					$strListProject .= "</ul>";
				}

				echo $strListProject;
			?>
		</div>
	</div>
	<div class="col-xs-12 col-md-10">

	
	

		<!-- <div class="projectHeader">
			<a href="" class="projectAdmin" target="_blank"><i class="fa tooltips fa-cog" data-original-title="" title=""></i></a>
			<p class="projectShortDescription"></p>
			<div class="projectBanner"></div>
			<div class="projectThumb"></div>
			
		</div> -->
		
		<?php 
		$description = (empty($element["description"]) ? "" : $element["description"]);
		?>
		<div id="descriptionMarkdown" class="hidden"><?php echo (empty($description) ? "" : $description ); ?></div>
		<div id="projectDescription" class="col-xs-12 padding-10 customTab" >
			<div id="descriptionAbout" class="col-xs-12" style="font-size: 16px !important;"></div>
			<hr class="">
			<div id="listContact" class="col-xs-12">
				<?php 
				$strC = "" ;
				if(!empty($element["contacts"])){
					
					foreach ($element["contacts"] as $keyC => $valC) {
						$strC .= "<div class='col col-xs-6 col-md-3'>";
			 			$strC .= "<h4>" . (!empty($valC['name'] ) ? $valC['name'] : "") . "</h4>";
			 			if(!empty($valC['email']))
			 				$strC .= "<a href='mailto:".$valC['email']."'>".$valC['email']."</a>" ;
			 			if(!empty($valC['telephone']) && !empty($valC['telephone'][0]))
			 				$strC .= "<br><a href='tel:".$valC['telephone'][0]."'>".$valC['telephone'][0]."</a>";
			 			$strC .= "</div>";
					}
				}
				echo $strC ; ?>
			</div>
			<button class="closeCustomTab">X</button>
			<!-- <button class="customBtnTrigger">Lire la suite <i class="fa fa-angle-down"></i></button> -->
		</div>
		<!-- <div id="projectContacts" class="customBlock customTab">
			<button class="closeCustomTab">X</button>
			<div class="row"><?php 

			// $.each(el.map.contacts, function (key,value){
		 // 		//mylog.log("Project : " + this.toSource() );
		 // 			var htmlContacts = "<div class='col col-xs-6 col-md-3'>";
		 // 			htmlContacts += "<h4>" + (typeof value['name'] != "undefined" ? value['name'] : "") + "</h4>";
		 // 			if(typeof value['email'] != "undefined")
		 // 				htmlContacts += "<a href='mailto:"+value['email']+"'>"+value['email']+"</a>" ;
		 // 			if(typeof value['telephone'] != "undefined" && typeof value['telephone'][0] != "undefined")
		 // 				htmlContacts += "<br><a href='tel:"+value['telephone'][0]+"'>"+value['telephone'][0]+"</a>";
		 // 			htmlContacts += "</div>";
		 // 			$(htmlContacts).appendTo('#projectContacts .row');
		 
		 	// });


			?></div>
		</div> -->
		<div class="col-xs-12 padding-50 links-main-menu" id="div-select-create">
			<div class="col-md-12 col-sm-12 col-xs-12 padding-15 shadow2 bg-white ">
		       
		       	<h4 class="text-center margin-top-15" style="">
			       <!-- 	<img class="img-circle" src="<?php //echo $thumbAuthor; ?>" height=30 width=30 style="margin-top:-10px;"> -->
			       	<a class="btn btn-link pull-right text-dark" id="btn-close-select-create" style="margin-top:-10px;">
			       		<i class="fa fa-times-circle fa-2x"></i>
			       	</a>
			       	<span class="name-header"><?php echo @$element["name"]; ?></span>
			       	<br>
			       	<i class="fa fa-plus-circle"></i> <?php echo Yii::t("form","Create content linked to this page") ?>
			       	<br>
			       	<small><?php echo Yii::t("form","What kind of content will you create ?") ?></small>
		       	</h4>
		        <div class="col-md-12 col-sm-12 col-xs-12 elementCreateButton"><hr></div>
		    </div>
	    </div>
		<div class="projectInfos customBlock col-xs-12">
			<div class="projectInfosHeader">
				<ul class="row nav nav-tabs">
					<?php if($type == Event::COLLECTION){ ?>
						<li class="nav-tab col-xs-6">
                           <a class="tab" href="javascript:;" id="journal">Journal</a>
						</li>
                        <li class="nav-tab col-xs-6">
                            <a class="tab active" href="javascript:;" id="detail">detail</a>
                        </li>
						<!-- <li class="nav-tab col-xs-6">
							<a class="tab" href="javascript:;" id="documents">Fichiers</a>
						</li> -->
					<?php } else { ?>
						<li class="nav-tab col-xs-6">
							<a class="tab active" href="javascript:;" id="journal">Journal</a>
						</li>
						<li class="nav-tab col-xs-6">
							<a class="tab" href="javascript:;" id="agenda" data-type-dir="events">Agenda</a>
						</li>
						<!-- <li class="nav-tab col-xs-4">
							<a class="tab" href="javascript:;" id="documents">Fichiers</a>
						</li> -->
					<?php } ?>
				</ul>
			</div>
			<!-- <div class="projectInfosContent tab-content">

				<div id="journal" class="tab-pane active row">
					<div id="journalTimeline"></div>
				</div>
				<div id="agenda" class="tab-pane row">
					<div id='profil-content-calendar' class='col-xs-12 margin-bottom-20'></div>
					<div id='list-calendar' class='col-xs-12 margin-bottom-20'></div>
				</div>
				<div id="documents" class="tab-pane row"></div>
			</div> -->
			<div id="central-container" class="padding-10"></div>
		</div>
	</div>
	<!-- </div> -->
</div>
<?php
echo $this->renderPartial('../element/confirmDeleteModal', array("id" =>(String)$element["_id"], "type"=>$type));
//$edit = Authorisation::canEditItem(Yii::app()->session["userId"], $type, $id);
// $thumbAuthor =  @$element['profilThumbImageUrl'] ? 
// 		                      Yii::app()->createUrl('/'.@$elt['profilThumbImageUrl']) 
// 		                      : $this->module->assetsUrl.'/images/thumbnail-default.jpg';

//$openEdition = Authorisation::isOpenEdition($id, $type, @$elt["preferences"]);
//$iconColor = Element::getColorIcon($type) ? Element::getColorIcon($type) : "";
// $params = array(  "element" => @$elt, 
// 	                "type" => @$type, 
// 	                "edit" => @$edit,
// 	                "thumbAuthor"=>@$thumbAuthor,
// 	                "openEdition" => $openEdition,
// 	                "iconColor" => $iconColor
// 	            );
// $this->renderPartial('dda.views.co.pod.modals', $params ); 

?>
<script type="text/javascript">
	// var subView="<?php // echo @$_GET['idLaR']; ?>";
	// var elt = <?php // echo json_encode(@$elt); ?> ;
	// var typeElt = <?php // echo json_encode(@$type); ?> ;
	// var idElt = <?php // echo json_encode(@$id); ?> ;
	// var color = <?php // echo json_encode(@$id); ?> ;
	// var defaultBannerUrl = ` <?php // echo Yii::app()->theme->baseUrl. '/assets/img/background-onepage/connexion-lines.jpg';?> `;
	var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>; 
	mylog.log("init contextData", contextData);
    //var params = <?php //echo json_encode(@$params); ?>; 
    var canEdit =  <?php echo json_encode(@$canEdit) ?>;
	var canParticipate =  <?php echo json_encode(@$canParticipate)?>;
	var canSee =  <?php echo json_encode(@$canSee) ?>;
	var elementParams =  <?php echo json_encode(@$elementParams) ?>;
	var openEdition = ( ( '<?php echo (@$openEdition == true); ?>' == "1") ? true : false );
    var dateLimit = 0;
    
    var connectTypeElement="<?php echo Element::$connectTypes[$type] ?>";
    var liveScopeType = "";
    var navInSlug=false;
   	var pageConfig=<?php echo json_encode($pageConfig) ?>;
   	if(typeof contextData.slug != "undefined")
     	navInSlug=true;


	var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
						"#@"+contextData.slug : 
						"#page.type."+contextData.type+".id."+contextData.id);

	if(location.hash.indexOf("#agendaRaffinerie")>=0){
		history.replaceState("#agendaRaffinerie", "", hashUrlPage+".view.directory.dir.events");
	}else if(location.hash.indexOf("#page")>=0){
		strHash="";
		if(location.hash.indexOf(".view")>0){
			hashPage=location.hash.split(".view");
			strHash=".view"+hashPage[1];
		}
		replaceSlug=true;
		history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", hashUrlPage+strHash);
	}


	pageProfil.params={
    	view : "<?php echo @$_GET['view']; ?>",
    	subview : "<?php echo @$_GET['subview']; ?>",
    	action : null,
		dir: "<?php echo @$_GET['dir']; ?>",
		key : "<?php echo @$_GET['key']; ?>",
		folderKey : "<?php echo @$_GET['folder']; ?>",
	};

    if (contextType == "events") {
        pageProfil.params.view = "detail";
    }

	pageProfil.views.newspaper = function(){
			mylog.log("pageProfil.views.newspaper");
			//coInterface.scrollTo("#profil_imgPreview");
			setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
				ajaxPost('#central-container', baseUrl+"/news/co/index/type/"+contextData.type+"/id/"+contextData.id, 
					{nbCol:2},
					function(){},"html");
			}, 700);
	};

	var initEvent = false;
	var initDoc = false;
	var initNews = true;
	
	jQuery(document).ready(function() {
		// var topB = "204px";
		// if(contextData.profilBannerUrl){
		// 	topB = "260px";
		// 	topB = $("#contentBanner").height() - $(".projectHeaderOptions").height() -10 ;

		// }
		// $(".projectHeaderOptions").css( "top", topB );
		$(".projectHeaderOptions").css( "top", $("#contentBanner").height() - $(".projectHeaderOptions").height() -10 );
		setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
			$(".projectHeaderOptions").css( "top", $("#contentBanner").height() - $(".projectHeaderOptions").height() -10 );
		}, 700);

		$("#projectDescription").hide();
		if(location.hash.indexOf("#@laRaffinerie3.view.directory.dir.events")>=0){
			$(".projectInfosHeader .tab").removeClass('active');
			$("#agenda").addClass('active');
		}else if(location.hash.indexOf("#@laRaffinerie3.view.gallery")>=0){
			$(".projectInfosHeader .tab").removeClass('active');
			$("#documents").addClass('active');
		}


		var filterAddType = ( costum.contextId == contextData.id ? ["events", "projects"] : ["events"] )
		typeObj.buildCreateButton(".elementCreateButton", false, {
			addClass:"col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
			bgIcon:true,
			textColor:true,
			inElement:true,
			allowIn:true,
			contextType: contextData.type,
			bgColor : "white",
			explain:true,
			inline:false
		}, null, filterAddType);
		

		

		if( typeof userId != "undefined" &&
			userId != null &&
			userId != "" &&
			typeof contextData != "undefined" && 
			typeof contextData.links != "undefined" && 
			( 	( 	contextData.type == "projects" &&
					typeof contextData.links.contributors != "undefined" && 
					typeof contextData.links.contributors[userId] != "undefined" ) ||
				(	contextData.type == "organizations" &&
					typeof contextData.links.members != "undefined" && 
					typeof contextData.links.members[userId] != "undefined") ) )  {
			$('#follows').html("Désinscrire");
			$('#follows').data("isco", true);
		} else {
			var strP = ( contextData.type == "projects" ) ? "Participer au projet" : "Participer" ;
			$('#follows').html(strP);
			$('#follows').data("isco", false);
		}

		if(typeof pageProfil.params.view != "undefined" &&
			pageProfil.params.view != null && 
			pageProfil.params.view == "directory" && 
			typeof pageProfil.params.dir != "undefined" &&
			pageProfil.params.dir != null && 
			pageProfil.params.dir == "events")
			pageProfil.params.sub = "1";
		pageProfil.init();

		$('#agenda').off().click(function () {
			mylog.log("agenda pageProfil ");

			$(".projectInfosHeader .tab").removeClass('active');
			$(this).addClass('active');
			pageProfil.params.dir = $(this).data("type-dir");
			pageProfil.params.sub = "1";
			//history.pushState remplace le location.hash car il recharge la page .
			//location.hash=hashUrlPage+".view.directory.dir."+pageProfil.params.dir;
			history.pushState(null, null, hashUrlPage+".view.directory.dir."+pageProfil.params.dir);
			pageProfil.views.directory(function(){
				$("#showHideCalendar").trigger("click");
			});
			$(".fc-month-button").trigger("click");
		});

		$('#projectsAdmin').off().click(function () {
			mylog.log("projectsAdmin pageProfil ");

			$(".projectInfosHeader .tab").removeClass('active');
			$(this).addClass('active');
			pageProfil.params.dir = $(this).data("type-dir");
			pageProfil.params.sub = "1";
			//history.pushState remplace le location.hash car il recharge la page .
			//location.hash=hashUrlPage+".view.directory.dir."+pageProfil.params.dir;
			history.pushState(null, null, hashUrlPage+".view.directory.dir."+pageProfil.params.dir);
			pageProfil.views.directory(function(){
				$("#showHideCalendar").trigger("click");
			});
			$(".fc-month-button").trigger("click");
		});

		$('#documents').off().click(function () {
			mylog.log("agenda pageProfil ");
			$(".projectInfosHeader .tab").removeClass('active');
			$(this).addClass('active');
			pageProfil.params.dir = null;
			pageProfil.params.sub = null;
			pageProfil.views.gallery();
		});
		$('#journal').off().click(function () {
			mylog.log("agenda pageProfil ");
			$(".projectInfosHeader .tab").removeClass('active');
			$(this).addClass('active');
			history.pushState(null, null, hashUrlPage);
			pageProfil.params.sub = null;
			pageProfil.views.newspaper(false);
		});
        $('#detail').off().click(function () {
            mylog.log("agenda pageProfil ");
            $(".projectInfosHeader .tab").removeClass('active');
            $(this).addClass('active');
            history.pushState(null, null, hashUrlPage);
            pageProfil.params.sub = null;
            pageProfil.views.detail();
        });

		$('#aproposAdmin').off().click(function () {
			mylog.log("agenda pageProfil ");
			$(".projectInfosHeader .tab").removeClass('active');
			// $(this).addClass('active');
			history.pushState(null, null, hashUrlPage+".view.detail");
			pageProfil.params.sub = null;
			pageProfil.views.detail();
		});

		$('#projectContacts').off().click(function () {
			mylog.log("agenda pageProfil ");
			$(".projectInfosHeader .tab").removeClass('active');
			// $(this).addClass('active');
			history.pushState(null, null, hashUrlPage+".view.contacts");
			pageProfil.views.contacts();
		});

		$('#settings').off().click(function () {
			mylog.log("agenda pageProfil settings ");
			$(".projectInfosHeader .tab").removeClass('active');
			// $(this).addClass('active');
			pageProfil.params.sub = null;
			history.pushState(null, null, hashUrlPage+".view.settingsCommunity");
			pageProfil.views.settingsCommunity();
		});

		$('#deletedElt').off().click(function () {
			mylog.log("agenda pageProfil deletedElt ");
			pageProfil.actions.delete();
		});


		$('#community').off().click(function () {
			mylog.log("agenda pageProfil community ");
			$(".projectInfosHeader .tab").removeClass('active');
			// $(this).addClass('active');
			pageProfil.params.sub = null;
			history.pushState(null, null, hashUrlPage+".view.community");
			pageProfil.views.community();
		});
		
		$('#create').off().click(function () {
			mylog.log("agenda pageProfil community ");
			
			// $(this).addClass('active');
			pageProfil.params.sub = null;
			pageProfil.actions.create();
		});

		$('#inviteRaf').off().click(function () {
			mylog.log("agenda pageProfil community ");
			
			// $(this).addClass('active');
			pageProfil.params.sub = null;
			pageProfil.actions.create();
		});

		// $('.toggleAllProjects').click(function(){
		// 	if( $(this).hasClass('open') ) {
		// 		$('.projectNavThirdLvl').hide();
		// 		$(this).removeClass('open');
		// 		$('.toggleProjects').removeClass('fa-minus').addClass('fa-plus');
		// 	}
		// 	else{
		// 		$('.projectNavThirdLvl').show();
		// 		$(this).addClass('open');
		// 		$('.toggleProjects').removeClass('fa-plus').addClass('fa-minus');
		// 	}
		// });

		$('.toggleProjects, .showHidePole').click(function(){
			if($(this).hasClass('toggleProjects')){
				if ( $(this).hasClass('fa-plus') ){
					$(this).removeClass('fa-plus').addClass('fa-minus');
				} else {
					$(this).removeClass('fa-minus').addClass('fa-plus');
				}
			}
			
			$(this).parents('li').next('.projectNavThirdLvl').toggle();
		});

		$('#follows').click(function(){
			var id = $(this).data("id");
			var isco = $(this).data("isco");
			var col = $(this).data("col");
			if(isco == false){
				links.connectAjax(col,id,userId,'citoyens',links.linksTypes[col]["citoyens"], null, function(){
					urlCtrl.loadByHash(location.hash);
				});
			}else{
				links.disconnectAjax(col,id,userId,'citoyens',links.linksTypes[col]["citoyens"], null, function(){
					urlCtrl.loadByHash(location.hash);
				});
			}
			
		});


		$('#apropos').click(function(){
			if($('#projectDescription').is(":visible")){
				$('#projectDescription').hide();
			}
			else{
				$('#projectDescription').show();
			}
		});

		$('.closeCustomTab').click(function(e){
			e.preventDefault();
			var tab = $(this).parents('.customTab');
			tab.hide();
			// $tab.hide().removeClass('active');
		});

		$('.projectNavTriggerMobile').click(function(e){
			e.preventDefault();
			if(!$('.projectNav').hasClass('isAnimated')){

				$('.projectNav').addClass('isAnimated');
				if($(this).hasClass('active')){
					projectCloseMobileNav();
				}
				else{
					projectOpenMobileNav();
				}
			}
		});

		initDescs();


		directory.eventPanelHtml = function(params){
			return costum.laRaffinerie3.directoryEvent(params.id, params);
		};


	});

function initDescs() {
	mylog.log("inintDescs");
	var descHtml = "<i>"+trad.notSpecified+"</i>";
	if($("#descriptionMarkdown").html().length > 0){
		descHtml = dataHelper.markdownToHtml($("#descriptionMarkdown").html()) ;
	}
	$("#descriptionAbout").html(descHtml);
	mylog.log("descHtml", descHtml);
}

function projectCloseMobileNav(){
	$('.projectNav').animate({left: '-100%'}, 200, function(){
		$('.projectNavTriggerMobile').removeClass('active');
		$('.projectNav').removeClass('isAnimated');
	});
}
function projectOpenMobileNav(){
	$('.projectNav').animate({left: '0'}, 300, function(){
		$('.projectNavTriggerMobile').addClass('active');
		$('.projectNav').removeClass('isAnimated');
	});
}
</script>