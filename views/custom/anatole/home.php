<?php
$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    //MARKDOWN
    '/plugins/to-markdown/to-markdown.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"mayenneDemain"),array("updated"=>-1), 3, 0);
?>

<script type="text/javascript" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/progressbar.js"></script>

<div class="cv">
<div id="container" class="a-body container">
    <!-- Left Menu / Logo-->
    <aside class="a-menu" id="menu">
        <div class="a-logo">
            <!-- Logo image-->
            <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/profil.png" width="140" height="140" alt="" class="a-img"/>
            <!-- Logo Nom-->
            <span>Anatole Rakotoson</span></div>
        <!-- Mobile Navigation-->
        <a href="#menu1" class="a-link a-menu-link"></a>
        <!-- Left Navigation-->
        <nav id="menu1" role="navigation"> <a class="a-link" href="#chapterintroduction"> <a class="a-link" href="#chapterabout"><span id="link_about">Profil</span></a> <a class="a-link" href="#chapterskills"><span id="link_skills">Competences Techniques</span></a> <a class="a-link" href="#chapterexperience"><span id="link_experience">Experience</span></a> <a class="a-link" href="#chaptereducation"><span id="link_education">Formations</span></a> </nav>

    </aside>
    <!-- Go to top link for mobile device -->
    <a href="#menu" class="a-link totop-link">Go to the top</a>
    <div class="content-scroller">
        <div class="content-wrapper">

            <!-- About -->
            <article class="a-content about white-bg" id="chapterabout">
                <div class="a-inner">

                    <h2 class="a-h2"><span></span><br>
                        Anatole Rakotoson</h2>
                    <span class="title">Développeur</span>

                    <div class="title-divider"></div>
                    <div class="about-con">
                        <ul>
                            <li>Nom: Rakotoson</li>
                            <li>Prenom: Anatole Clémenceau</li>
                            <li>Email: <a class="a-link" href="mailto:anatolerakotoson@gmail.com">anatolerakotoson@gmail.com</a></li>
                            <li>Numéro: (261) - 34 79 597 70</li>
                            <li>Date de naissance: 11 Avril 1996</li>
                            <li>Adresse: Villa les rêves Tsianolondroa </li>
                        </ul>
                    </div>
                </div>
            </article>

            <!-- Competences Techniques -->
            <article class="a-content skills gray-bg" id="chapterskills">
                <div class="a-inner">
                    <h2 class="a-h2">Competences Techniques</h2>
                    <div class="title-divider"></div>
                    <h3 class="a-h3">Langage de programmation </h3>
                    <div class="col-6 m-margin-top30">
                        <div class="contact-social margin-top30">
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img " src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-c-programming-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-c++-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-java-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-php-logo-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-javascript-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-python-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                        </div>
                    </div>
                    <div class="full-divider"></div>
                    <h3 class="a-h3">Framework </h3>
                    <div class="col-6 m-margin-top30">
                        <div class="contact-social margin-top30">
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-angularjs-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-jquery-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-symfony-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/codeigniter-512.png" style="height: 50px; width: 50px" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                        </div>
                    </div>
                    <div class="full-divider"></div>
                    <h3 class="a-h3">SGBD </h3>
                    <div class="col-6 m-margin-top30">
                        <div class="contact-social margin-top30">
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-mysql-logo-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/icons8-postgresql-50.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                        </div>
                    </div>
                    <div class="full-divider"></div>
                    <h3 class="a-h3">Gestion de projet </h3>
                    <div class="col-6 m-margin-top30">
                        <div class="contact-social margin-top30">
                            <a class="a-link" href="#">
                                <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/anatole/agile.png" style="height: 50px; width: 50px" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">
                            </a>
                        </div>
                    </div>
                    <div class="full-divider"></div>
                    <div class="container-sub">
                        <div class="a-row">
                            <h3 class="a-h3">Language</h3>
                            <div class="progress2bar-main margin-top50">
                                <div class="progress2-bar-description">Français</div>
                                <div id="progress2Bar" class="progress2">
                                    <div class="progress2-value"></div>
                                </div>
                            </div>
                            <div class="progress2bar-main margin-top40">
                                <div class="progress2-bar-description">Anglais</div>
                                <div id="progress2Bar2" class="progress2">
                                    <div class="progress2-value"></div>
                                </div>
                            </div>
                            <div class="progress2bar-main margin-top40">
                                <div class="progress2-bar-description">Allemand</div>
                                <div id="progress2Bar3" class="progress2">
                                    <div class="progress2-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <!-- Experience -->
            <article class="a-content experience white-bg" id="chapterexperience">
                <div class="a-inner">
                    <h2 class="a-h2">Experience</h2>
                    <div class="experience-con">
                        <div class="container-sub">
                            <div class="a-row">
                                <h3 class="a-h3" class="a-h3">Experiences professionelles</h3>
                                <div class="experience-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-briefcase"></i></div>
                                        <div class="a-flot-left">
                                            <h5>ITDC Mada</h5>
                                            <h4>Développement web</h4>
                                            <span>Déc. 2018 - Oct. 2019</span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top"> Développement d’une application web : système de gestion de risque et
                                        catastrophe - BNGRC. </div>
                                </div>
                                <h3 class="a-h3">Autres experiences</h3>
                                <div class="full-divider"></div>
                                <div class="experience-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-briefcase"></i></div>
                                        <div class="a-flot-left">
                                            <h5>EMIT </h5>
                                            <h4>Machine Learning</h4>
                                            <span>Fév. 2019 - Avr. 2019 </span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top">
                                        Préparation de mini-mémoire en vue de passage en 2eme année de
                                        cycle MASTER, le sujet traité est « La réseau de neurone appliquée à la
                                        gestion d’état civil, cas : détection de fraude d’identité »                  </div>
                                </div>
                                <div class="full-divider"></div>
                                <div class="experience-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-briefcase"></i></div>
                                        <div class="a-flot-left">
                                            <h5>ITDC Mada </h5>
                                            <h4>Développement web</h4>
                                            <span>Déc. 2017 - Mar. 2018 </span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top">
                                        Stage sur le thème « Conception et réalisation d’une plateforme de
                                        recrutements »
                                    </div>
                                </div>
                                <div class="full-divider"></div>
                                <div class="experience-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-briefcase"></i></div>
                                        <div class="a-flot-left">
                                            <h5>Direction de la Région Haute Matsiatra</h5>
                                            <h4>Développement web</h4>
                                            <span>Déc. 2016 - Mar. 2017 </span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top">
                                        Stage sur le thème «Conception et la réalisation d’une application web
                                        monographie de la Région Haute Matsiatra».                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <!-- Education -->
            <article class="a-content education gray-bg" id="chaptereducation">
                <div class="a-inner">
                    <h2 class="a-h2">Education</h2>
                    <div class="title-divider"></div>
                    <div class="education-con">
                        <div class="container-sub">
                            <div class="a-row">


                                <div class="education-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-laptop"></i></div>
                                        <div class="a-flot-left">
                                            <h5>EMIT </h5>
                                            <h4>Licence en Développement d’application intranet/internet (DA2I)</h4>
                                            <span>2014 -2017</span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top">
                                        Trois année de formation de licence professionnelle en développement
                                        d’application intranet/internet                  </div>
                                </div>
                                <div class="full-divider"></div>


                                <div class="education-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-laptop"></i></div>
                                        <div class="a-flot-left">
                                            <h5>Lycée privée Liantsoa School Fianarantsoa </h5>
                                            <h4>Baccalauréat Série C (Scientifique)</h4>
                                            <span>2015</span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top">
                                        Terminal Scientifique                   </div>
                                </div>
                                <div class="full-divider"></div>
                                <div class="education-details">
                                    <div class="a-col-6 margin-bottom50 margin-top50">
                                        <div class="a-col-3 icon-block"><i class="fa fa-laptop"></i></div>
                                        <div class="a-flot-left">
                                            <h5>Baccalauréat Série A (Littéraire)</h5>
                                            <h4>Première Scientifique </h4>
                                            <span>2014</span> </div>
                                    </div>
                                    <div class="a-col-6 margin-bottom50 margin-top50 no-margin-top">
                                        Première Scientifique
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <!-- Introduction -->
            <article class="a-content " id="chapterthankyou">
                <div class="a-inner">

                </div>
            </article>
        </div>
    <!-- content-wrapper -->
    </div>
    <!-- content-scroller -->
</div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/head.min.js"></script>


<script>
    head.js(
        { mousewheel : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery.mousewheel.js" },
        { mwheelIntent : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/mwheelIntent.js" },
        { jScrollPane : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery.jscrollpane.min.js" },
        { history : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery.history.js" },
        { stringLib : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/core.string.js" },
        { easing : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery.easing.1.3.js" },
        { smartresize : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery.smartresize.js" },
        { page : "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/jquery.page.js" }
    );
</script>


<script type="text/javascript" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/anatole/settings.js"></script>