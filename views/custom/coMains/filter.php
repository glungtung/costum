<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}

	.fc-unthemed .fc-popover {
	   	position: absolute;
	    top: 30px !important;
	    left: 40% !important;
	}
</style>
<script type="text/javascript">

	var paramsFilter= {
	 	container : "#filters-nav",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	results : {
	 		renderView : "directory.classifiedPanelHtml",
	 		smartGrid : true
	 	},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		section : {
	 			name : "Offres/Besoins",
	 			label : "Besoins",
	 			type : "section",
	 			view : "dropdownList",
	 			event : "filters",
	 			list : sectionsClassified
	 		},
	 		category : {
 				view : "complexeSelect",
	 			name : "Catégories", 
	 			event : "selectList", 
	 			list : categoriesList,
	 			multiple:true,
	 			levelOptions : [
	 				{
	 					level : 1,
	 					type : "category",
	 					separator : true,
	 					visible : true 
	 				},
	 				{
	 					level : 2,
	 					class : "col-xs-12 no-padding",
	 					type : "subType",
	 					visible : true 
	 				}
	 			]
	 		}
	 	}
	};
	if(notNull(typeClassified) && typeClassified=="classifieds"){
		paramsFilter.filters.price=true;
	}
	paramsFilter.defaults={
		fields : ["price","devise","description","contactInfo", "section", "subType"],
		types : ["ressource"],
		forced : {
			type: "ressource"
		}
	};

	if(notNull(sectionClassified))
		paramsFilter.defaults.section=sectionClassified;
	var filterSearch={};
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});


	var filterSearch={};

	jQuery(document).ready(function() {
		mylog.log("HVA filters/search.php");
		filterSearch = searchObj.init(paramsFilter);
	});
</script>