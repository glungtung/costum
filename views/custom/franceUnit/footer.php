<footer style="background-color : #32a2a4">
    <div class="row" style="margin-top: 1%;margin-bottom: 1%;">
        <div class="col-xs-12 col-sm-12 col-lg-12 text-center col-footer col-footer-step">
            <a class="col-lg-6 col-xs-6" style="color:white" href="https://communecter.org" target="_blank">
            	Logiciel libre
            	<br>
            	<center>
            		<img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-co.png" class="img-responsive" style="width: 10%;">
            	</center>
            </a>
            <a class="col-lg-6 col-xs-6" style="color: white" href="https://www.open-atlas.org/" target="_blank">
            	Propuslé par
            	<br>
            	<center>
            		<img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-openatlas.png" class="img-responsive" style="width: 20%;">
            	</center>
            </a>
        </div>
    </div>
</footer>