<?php
$cssAnsScriptFilesModule = array(
	'/css/ctenat/filters.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>

<div class="projectsProgress col-xs-12 col-md-10 col-md-offset-1 margin-bottom-20">
	<div class="col-xs-10 col-xs-offset-1">
		<!-- <img class="img-responsive col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/images/ctenat/light-bulb.png"/>
		<span class="col-sm-6 col-sm-offset-3 margin-top-10" style="font-size: 20px;text-align: center;text-transform: uppercase;">Contenu à venir rapidement</span> -->
	</div> 
</div>
<!-- <div id="filterContainer" class="col-xs-12 col-md-10 col-md-offset-1">
</div> -->

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];

	// if (pageApp == "filiere") {
		//category : pageApp,
		
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
				notSourceKey: true,
      			types: ["organizations"],
      			indexStep: 50,
				forced: {
        			filters: {"links.memberOf.5e25910c690864233b8b4671":{ "$exists": true}}
      			}
		 	},
		 	filters : {
		 		text : true,
	 			scope : true,
		 		domainAction : {
		 			view : "dropdownList",
		 			type : "tags",
		 			name : "Rechercher par :",
		 			event : "tags",
		 			list : costum.lists.domainAction
		 		}
		 	},
		 	results : {
				renderView: "directory.classifiedPanelHtml",
			 	smartGrid : true
			},
		};
	// }

	if(pageApp!="search" && pageApp!="filiere"){
		paramsFilter.defaults["filters"] = {"category": [pageApp]};
	}

	//setTimeout(function(){
	//	$("#filtersContainer").empty();
	//	$('.btn-filters-select[data-key="'+pageApp+'"]').click();
	//},10);
    

	jQuery(document).ready(function() {
		if (pageApp == "search") {
		 	$(".btn-hide-map").css("display","none");
		}
		
		filterSearch = searchObj.init(paramsFilter);
		//filterSearch.filters.manage.addActive(this, $('.btn-filters-select[data-key="'+pageApp+'"]').attr("data-type"), $('.btn-filters-select[data-key="'+pageApp+'"]').attr("data-value"), pageApp, $('.btn-filters-select[data-key="'+pageApp+'"]').attr("data-field"), $('.btn-filters-select[data-key="'+pageApp+'"]').attr("data-event"));
		//.manage.addActive()
	});

</script>