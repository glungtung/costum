<?php
if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
  $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"]);
  $graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );

  // $cssAnsScriptFilesModule = array(
  //   '/assets/css/v1.0/main.css'
  // );

  // HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);
}
?>
<script>
  jQuery(document).ready(function() {
    contextData = <?php echo json_encode($el); ?>;
    var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
    var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName = <?php echo json_encode($el["name"]); ?>;
    contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
  });
</script>
<style>
  #graph-container {
    height: 100vh;
    width: 100vw;
    overflow: hidden;
  }
  .graph-btn{
    background-color: #00B2AB;
    color: white;
    border: 2px solid transparent;
    margin: 0px 3px !important;
    font-size: 14px !important;
    border-radius: 4px !important;
    height: 34px !important;
    transition: .2s all;
  }
  .graph-btn:hover{
    background-color: transparent;
	  color: #00B2AB;
	  border: 2px solid #00B2AB;
  }

  a[href="#mindmap"] i.fa-sitemap{
    transform: rotateZ(-90deg);
  }
</style>

<div id="test" class="col-xs-12 searchObjCSS">
</div>
<div class="headerSearchContainer no-padding"></div>
<div id="graph-container">
</div>


<script>
  let hash = window.location.hash.replace("#", "");
	hash = hash.split("?")[0];
	
  var l = {
    container: "#test",
    loadEvent: {
      default: "graph"
    },
    defaults: {
      notSourceKey: true,
      types: ["organizations"],
      indexStep: 50,
      forced: {
        filters: {"links.memberOf.5e25910c690864233b8b4671":{ "$exists": true}}
      }
    },
    results: {
      dom: "#loader-container"
    },
    graph: {
      dom: "#graph-container",
      defaultGraph : 'relation',
      authorizedTags: []
    },
    filters: {
      text: true,
      
    },
    header: {
      options : {
		  }
    }
  };
  if(costum.lists.domainAction.Visualisation){
    l.graph.authorizedTags = costum.lists.domainAction.Visualisation;
    l.filters.tags = {
        view: "dropdownList",
        list: costum.lists.domainAction.Visualisation
      };
  }

  if(searchObj.graph.validGraphs.includes(hash)){
    l.graph.defaultGraph = hash;
	}

  var p = {};
  setTimeout(() => {
    p = searchObj.init(l);
    p.graph.init(p);
    p.search.init(p);
  }, 200)
</script>
