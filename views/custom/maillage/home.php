
<div class="pageContent">


    <style type="text/css">
      #customHeader{
        margin-top: 0px;
      }
      #costumBanner{
       /* max-height: 375px; */
      }
      #costumBanner h1{
        position: absolute;
        color: white;
        background-color: rgba(0,0,0,0.4);
        font-size: 29px;
        bottom: 0px;
        padding: 20px;
      }
      #costumBanner h1 span{
        color: #eeeeee;
        font-style: italic;
      }
      #costumBanner img{
        min-width: 100%;
      }
      .btn-main-menu{
        background: #3595a8;
        border-radius: 20px;
        padding: 20px !important;
        color: white;
        cursor: pointer;
        border:3px solid transparent;
        /*min-height:100px;*/
      }
      .btn-main-menu:hover{
        border:2px solid #3595a8;
        background-color: white;
        color: #1b7baf;
      }
      .ourvalues img{
        height:70px;
      }
      .main-title{
        color: #3595a8;
      }

      .ourvalues h3{
        font-size: 25px;
      }
      .box-register label.letter-black{
        margin-bottom:3px;
        font-size: 13px;
      }

       .participate {
    background-color: white;
      }
     
       .participate i{
      
    float: left;
    margin-right: 20px;
    color: #ef5b2b ;

      }

      .participate-content{
        font-size: 17px;
      }
     

      @media screen and (min-width: 450px) and (max-width: 1024px) {
        .logoDescription{
          width: 60%;
          margin:auto;
        }
      }

      @media (max-width: 1024px){
        #customHeader{
          margin-top: -1px;
        }
      }
      @media (max-width: 768px){

      }
    </style>

    <div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
      <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">

    <?php

  //  $baseUrl = "127.0.0.1/ph";
  //  typeItem  organizations;
  //  contextData.id="5cab04021efec9fa1d253f32";
    
    ?> 
    
     <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
        <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/maillage/banner.png'> 
      </div>
    </div> 

   
      
    
<!--       <div id="bibliotheque" class="col-xs-12"></div>  -->
<!--       <div id="live-maillage" class="col-xs-12"></div> 
 -->    
      <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
         style=" position:relative;">-->
    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
        
          
      <div class="col-md-12 col-sm-12 col-xs-12 " style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="background-color: #fff;font-size: 14px;z-index: 5;">
              <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate" style="padding-left:100px; margin-top:0px;">
                  <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 text-justify" style="margin-top: -19px;margin-bottom: -18px;font-size: 14px;z-index: 5;">
                    <h2 class="col-xs-12 text-center" style="/*font-family:'Pacifico', Helvetica, sans-serif;*/color: #00a5a5;   font-size: 30px"><small>vous présente</small><br/>Son réseau interactif
                      <br/><br/>
                      <small style="color: #333;text-align:justify !important;">
                        Vous, membres du réseau Maillage, êtes au centre de cet outil qui rapproche non seulement les projets entre eux, mais également ceux qui les portent !<br/>
                        
                      </small>   
                    </h2><br/>
                  </div> 
<!--               <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
                <h3 class="col-xs-12 text-center">
                  
                  <hr style="width:40%; margin:20px auto; border: 1px solid #fbae55;">
                </h3>
                <div class="col-xs-12">
                  <a href="javascript:;" data-hash="#search" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
                      <div class="text-center">
                          <div class="col-md-12 no-padding text-center">
                              <h4 class="no-margin uppercase">
                                <i class="fa fa-hand-point-right faa-pulse"></i>
                                <?php echo Yii::t("home","Découvrir") ?>
                              </h4>
                          </div>
                      </div>
                  </a>
                </div>
                <h3 class="col-xs-12 text-center">
                  <hr style="width:40%; margin:20px auto; border: 1px solid #fbae55;">
                </h3>
              
              </div> -->
               
                  <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 text-justify" style="margin-top: -19px;margin-bottom: -18px;font-size: 14px;z-index: 5;">
                    <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
                      <h3 class="col-xs-12" style="/*font-family:'Pacifico', Helvetica, sans-serif;*/color: #ef5b2b;   font-size: 25px">
                       A votre niveau, participez !  
                        <br/>
                      <small style="color: #333;">
                      
                      <b>Vous pouvez alors être :</b>
                    </small>   
                        </h3>
                      <i class="fa fa-3x fa-info-circle"></i>
                      informé.e.s en temps réel de la dynamique du réseau : activités, événements, formations, actualités, nouveaux projets…
                    </div> 
                    <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
                      <i class="fa fa-3x fa-exchange"></i>
                      actif.ve.s en échangeant davantage : partage d’informations utiles, de matériel, dialogue interactif facilité avec les autres membres, consultation/sondage des membres portant sur des difficultés rencontrées, ... 
                        
                    </div>  
                    <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
                      <i class="fa fa-3x fa-bullhorn"></i>
                      connecté.e.s​ avec les membres du réseau pour vous connaître davantage, faire converger des projets complémentaires, bénéficier de l'expérience, mutualiser des ressources et des compétences de pair-à-pair, ... 
                    </div>
                     <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
                      <i class="fa fa-3x fa-newspaper-o"></i>
                      promoteur.rice.s ​de votre activité propre, associative, entrepreneuriale... 
                    </div>    
                 
                  </div>
          
              
              </div>
          </div>

        
        </div>
    
    </div>

</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
        setTitle("Maillage");

        
  
  });
 

</script>


