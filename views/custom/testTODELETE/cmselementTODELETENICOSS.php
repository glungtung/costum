<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 
$poiList = array();

// echo "<script type='text/javascript'> alert('commons home :".@["slug"]." : ".@["contextType"]." : ".@["contextId"]."'); </script>";

if(isset(["contextType"]) && isset(["contextId"])){
    $el = Element::getByTypeAndId(["contextType"], ["contextId"] );

} else {?>
  
  <div class="col-xs-12 text-center margin-top-50">
    <h2>Ce COstum est un template <br/>doit etre associé à un élément pour avoir un context.</h2>
  </div>

<?php exit;}?>


<div class="">

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
    

    <div class="col-xs-12 text-center margin-bottom-50" style="padding:0px;">
    

  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#1F2532; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                
                <small style="text-align: left">
                <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
                  <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
  color: #2b2b2b;
  text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">



  « <span class="text-red">CO</span>nnect a <span class="text-red">CMS</span> to an element   »</bloquote>

              </h3>
              
              <br/>



                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
              
              <div class="col-md-10 col-md-offset-1 col-xs-12">
                
                
                <?php 
                  $dynFormSource = PHDB::findOne( Costum::COLLECTION , array("slug"=>"ctenat") );
                  $costumProperties = $dynFormSource["typeObj"]["projects"]["dynFormCostum"]["beforeBuild"]["properties"];
                ?>
                <script type="text/javascript">
                jQuery(document).ready(function() {
                  dyFObj.dynFormSource = <?php echo json_encode($dynFormSource) ?>; 
                  dyFObj.dynFormCostum = dyFObj.dynFormSource.typeObj.projects.dynFormCostum; 
                  dyFObj.elementData = <?php echo json_encode($el) ?>; 
                });
                </script>
                <?php 
                echo $this->renderPartial("costum.views.tpls.elemCmsScriptBefore",null,true); 
                ?>

                <div  class="col-xs-12 no-padding support-section text-center">
                <?php 
                  $field = "description";
                  $params = array(
                      "value"           => @$el[$field],
                      "title"           => "Description",
                      "collection"      => "projects",
                      "id"              =>(string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => null,
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>

                <div  class="col-xs-6 no-padding support-section text-center">
                <?php
                  $field = 'dispositif';
                  $params = array(
                      "value"           => @$el[$field],
                      "title"           => $costumProperties[$field]["label"],
                      "collection"      => "projects",
                      "id"              =>(string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => $costumProperties[$field],
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>

                <div  class="col-xs-6 no-padding support-section text-center">
                <?php 
                  $field = "actions";
                  $params = array(
                      "value"           => @$el[$field],
                      "title"           => $costumProperties[$field]["label"],
                      "collection"      => "projects",
                      "id"              =>(string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => $costumProperties[$field],
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>

                <div  class="col-xs-12"></div>

                <div  class="col-xs-6 no-padding support-section text-center">
                  <h4><?php echo $costumProperties[$field]["label"] ?></h4>
                  <div class='text-align:left; markdown' ><?php echo $el['planPCAET'] ?></div>
                <?php 
                  $field = "planPCAET";
                  $params = array(
                      "noRender"        =>true,
                      "collection"      => "projects",
                      "id"              =>(string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => $costumProperties[$field],
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>

                <div  class="col-xs-6 no-padding support-section text-center">
                  <h4><?php echo "Dans quelle Partie ?" ?></h4>
                  <div class='text-align:left;' >
                  <?php 
                  foreach ($el['tags'] as $key => $t) {
                    echo "<span class='text-red'>#".$t."</span><br/>";   
                  }
                  ?>
                  </div>
                  
                  <?php
                  $field = "tags";
                  $params = array(
                      "noRender"        => true,
                      "collection"      => "projects",
                      "id"              => (string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => null,
                      // array(
                      //   "section" => array(
                      //       "inputType" => "tags",
                      //       "placeholder" => "Un Select?",
                      //       "data" => [
                      //           "what", 
                      //           "when", 
                      //           "where", 
                      //           "why", 
                      //           "who"
                      //       ],
                      //       "label" => [ 
                      //           "wh at", 
                      //           "wh en", 
                      //           "wh ere", 
                      //           "wh y", 
                      //           "wh o" 
                      //       ]
                      //   )
                      // ),
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>
                
                <div  class="col-xs-12"></div>

                <div  class="col-xs-6 no-padding support-section text-center">
                  <h4><?php echo "Dans quelle Partie ?" ?></h4>
                  <?php 
                  $field = "scope";
                  $params = array(
                      "noRender"        => true,
                      "collection"      => "projects",
                      "id"              => (string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => null,
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>

                <div  class="col-xs-6 no-padding support-section text-center">
                  <h4><?php echo "Une date peut etre ?" ?></h4>
                  <div class='text-align:left; markdown' ><?php echo @$el['datedate'] ?></div>
                <?php 
                  $field = "datedate";
                  $params = array(
                      "noRender"        => true,
                      "collection"      => "projects",
                      "id"              => (string)$el["_id"],
                      "field"           => $field,
                      //l'objet dynform.property decrivant le champs en question
                      "dynFormProperty" => array(                        
                            "inputType" => "datetime",
                            "label" => "Une date peut etre ",
                            "placeholder" => "Fin de la période de vote"
                      ),
                      "color"           => "#e6344d"
                  );
                  echo $this->renderPartial("costum.views.tpls.elemText",$params,true);
                ?>
                </div>
                
                <div class="text-center col-xs-12">
                  - still todo all form save in path2value<br/>
                  <a href="javascript:;" class="openAllAtOnce btn btn-default" data-id="<?php echo (string)$el["_id"] ?>" data-type="projects">Open All at once</a>

                  <a href="/costum/co/index/slug/testTerritoire1/page/ficheAction" class=" btn btn-default" >static page sample</a>

                  <a href="/costum/co/index/slug/testTerritoire1/page/stepper" class=" btn btn-default" >static survey stepper</a>

                  <a href="/costum/co/index/slug/testTerritoire1/page/forum" class=" btn btn-default" >Forum CMS</a>

                </div>
                <?php 
                echo $this->renderPartial("costum.views.tpls.elemCmsScript",$params,true); 
                //echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
                ?>

              </div>
            </div>
            <div class="text-center col-xs-12">
            <br/><br/>
            <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png">
            </div>
        </div>
          </div>

        </div>

      </div>
      


    </div>
  </div>
</div>




<?php 
/*
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>


<script type="text/javascript">
  jQuery(document).ready(function() {
    
    setTitle("CMS Element tests");
    contextData = {
        id : "<?php echo ["contextId"] ?>",
        type : "<?php echo ["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
    if(contextData.id == "")
      alert("Veuillez connecter ce costum à un élément!!")
    
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
    
});

</script>

