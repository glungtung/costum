<?php
$camp=Crowdfunding::getCampaignAndCountersByParentId((string)$element["_id"]);

$currency_symbol = "€";
$rest = $camp["goal"] - $camp["donationNumber"] - $camp["pledgeNumber"];
$rest_txt = (string)$rest . $currency_symbol;
$donations_txt = (string)$camp["donationNumber"] . $currency_symbol;
$pledges_txt = (string)$camp["pledgeNumber"] . $currency_symbol;
$requested_text = (string)$camp["goal"] . $currency_symbol;

// Compute progress percentages
$donations_perc = round($camp["donationNumber"] / $camp["goal"] * 100);
$pledges_perc = "";
if ($rest <= 0) {
	$pledges_perc = 100 - $donations_perc;
} else {
	$pledges_perc = round($camp["pledgeNumber"] / $camp["goal"] * 100);
}

// Clamp progress percentages
$min_perc = 2;
if ($donations_perc <= $min_perc) {
    $donations_perc = $min_perc;
    $pledges_perc = min($pledges_perc, 100 - $min_perc);
}
if ($pledges_perc <= $min_perc) {
    $pledges_perc = $min_perc;
    $donations_perc = min($donations_perc, 100 - $min_perc);
}

$donations_perc_txt = (string)$donations_perc;
$pledges_perc_txt = (string)$pledges_perc;

$eltId=key($element['parent']);
$eltType= $element['parent'][$eltId]['type'];
$eltName= $element['parent'][$eltId]['name'];
?>

<div class="campaign-detail container">
    <div class="row">
        <div class="col-xs-12">
            <h1><?php echo $element["name"]?><br>
                <small><?php echo $camp["nameCamp"]?></small>
                <span style="font-size:50%;text-transform: none;">Porté par
                    <a href="#page.type.<?php echo $eltType ?>.id.<?php echo $eltId ?>"
                       class="lbh" target="_blank"><?php echo $eltName ?></a>
                </span>
            </h1>
        </div>
    </div>
	<div class="row">
		<img class="img-responsive col-xs-12 col-sm-6 col-md-4" src="<?php echo $element["profilMediumImageUrl"]?>">
		<div class="description col-xs-12 col-sm-6 col-md-8">
			<h2>Description de l'action</h2>
			<p>#description-de-la-campagne#//#description-de-la-campagne#//#description-de-la-campagne#//
				#description-de-la-campagne#//#description-de-la-campagne#//#description-de-la-campagne#//
				#description-de-la-campagne#//#description-de-la-campagne#//#description-de-la-campagne#//
				#description-de-la-campagne#//#description-de-la-campagne#//#description-de-la-campagne#</p>
			<div class="campaign-progress">
				<div class="text-right">
					<strong>Reste à financer : <?php echo $rest_txt ?> / <?php echo $requested_text ?></strong>
				</div>
				<div class="progress">
					<div class="progress-bar donation-bar" role="progressbar"
						 style="width: <?php echo $donations_perc_txt ?>%"
						 aria-valuenow="<?php echo $donations_perc_txt ?>"
						 aria-valuemin="0" aria-valuemax="100"></div>
					<div class="progress-bar pledge-bar" role="progressbar"
						 style="width: <?php echo $pledges_perc_txt?>%"
						 aria-valuenow="<?php echo$pledges_perc_txt ?>"
						 aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<div>
                    <div><span class="label donation-label">Dons : <?php echo $donations_txt ?></span></div>
                    <div><span class="label pledge-label">Promesses : <?php echo $pledges_txt ?></span></div>
				</div>
			</div>
			<div class="text-center">
				<a href="javascript:;" class="btn btn-lg btn-primary addDonation"
				   data-id="<?php echo $element['_id'] ?>"
				   data-name="<?php echo $element['name'] ?>"
				   data-type="<?php echo $element['collection'] ?>"
				   data-id-camp="<?php echo $camp['idCamp'] ?>"
				   data-name-camp="<?php echo $camp['nameCamp']?>">Je cofinance cette campagne !</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		pageProfil.bindViewActionEvent();

		// $(".editPledge").click(function(){
		//     dyFObj.editMode=true;
		//     //uploadObj.set(type, id);
		//     uploadObj.update = true;
		//     dyFObj.currentElement={type : contextData.type, id : contextData.id};
		//     dataEdit=jQuery.extend(true, {},contextData);
		//     if(typeof contextData.typeOrga != "undefined")
		//         dataEdit.type=contextData.typeOrga;
		//     dyFObj.openForm('pledge', null, dataEdit);
		// });

		// displayCampaign($(".campaign")[0]);
	});
</script>
