<style type="text/css">
	.searchBar-filters{
		width: 60%;
		margin: auto !important;
		float: inherit !important;
	}
	.searchBar-filters .search-bar{
		width: 100%;
    	border: 1px solid #5b2648;
    }
    .searchBar-filters .main-search-bar-addon{
    	background-color: #5b2648 !important;
	    border: 1px solid #5a2647;
	    border-radius: 0px 20px 20px 0px;
	    border-right-width: 0px;
    }
    .container-filters-menu{
    	text-align: center;
    }
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var paramsFilter= {
	 	// container : "#searchInMesures",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	loadEvent : {
     	 		default : "crowdfunding"
    	},
	 	results :{
                renderView : "directory.crowdfundingPanelHtml"
            },
	 	defaults:{
	 		types : ["crowdfunding"],
	 		indexStep : 0,
	 		sortBy : [ 
                "name"
            ],
            forced : {
            	filters : {
            		type : "campaign"
            	}
            }	
	 	},
	 	fields : [
	 		"preferences"
	 	],
	 	filters : {
	 		text : {
	 			placeholder : "Rechercher parmi les groupes"
	 		}
	 	}
	};
	var filterSearch={};
	jQuery(document).ready(function() {

		mylog.log('filterSearch paramsFilter', paramsFilter);
		// searchObj.crowdfunding={};
		// searchObj.crowdfunding.render = function (fObj, results, data){
		// 	ajaxPost(
		// 		null,
		//// if directory.specific byparent else getcampaignandcounters only
		// 		baseUrl+"/" + moduleId + "/crowdfunding/getcampaignandcountersbyparent",
		// 		data,
		// 		function(data){
		// 			var results=data.results;
		// 			fObj.results.render(fObj, results, data);
		// 		},
		// 		null,
		// 		null,
		// 		{async:false}
		// 	);	
		// };	
	
		filterSearch = searchObj.init(paramsFilter);


		
	});
</script>