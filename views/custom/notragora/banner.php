<?php
// $cssAnsScriptFilesModule = array(
// 	'/js/notragora/sly/sly.min.js',
// );
// HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "costum" )->getAssetsUrl());

?>
<style>
	.searchPoiContainer{
		position:relative;
		display: inline;
		display: -webkit-inline-box;
		display: -moz-inline-box;
		padding : 0px;
	}

	#poiSelectedHead {
		font-size: 14px;
		display : none;
		color : rgb(92,75,62);
		word-wrap: break-word;
    	/*width: 300px;*/
	}

	#cycleitems{
		height: 140px;
	}

</style>
<?php  
	$topList = Poi::getPoiByTagsAndLimit(0, 15, array(), array("source.key" => "notragora"));
	$tagsPoiList = array();
	$tagsPoiListAdd = array();
	//var_dump($this->costum["genres"]);
	if(!empty($topList)){
		shuffle($topList);
		foreach ($topList as $key => $elem){
			if(@$elem["tags"]){
				foreach($elem["tags"] as $tagKey => $val){

					$topList[$key]["tags"][$tagKey] = strtolower( InflectorHelper::slugify2( $val ));
					//if(!in_array($val,Poi::$collectionsList) && !in_array($val,Poi::$genresList)){
						$found = false;
						foreach ($tagsPoiList as $ix => $value) {
							$value["text"]; 
							if(	$value["text"] == $val)
								$found = $ix;
						}
						if ( !$found )
							array_push($tagsPoiList,array(
									"text"=>$val,
									"weight"=>1,
									"link"=>array(
										// "href" => "javascript:addItemsToSly('".InflectorHelper::slugify2($val)."')",
										"href" => "javascript:;",
										"class" => "favElBtn ".InflectorHelper::slugify2($val)."Btn",
										"data-tag" => InflectorHelper::slugify2($val)
									)
								)
							);
						else
							$tagsPoiList[$found]["weight"]++;
					//}
				}
			}
			if(@$elem["medias"] && @$elem["medias"][0]["content"]["image"] && !empty($elem["medias"][0]["content"]["image"])){

				$imgBNA = $elem["medias"][0]["content"]["image"];
				$imgBNA= str_replace("1280x720","720x720",$imgBNA);
				$imgBNA= str_replace("filter/overlay?src0=https%3A%2F%2Fi.vimeocdn.com%2Fvideo%2F", "video/",$imgBNA);
				$imgBNA = str_replace("&src1=https%3A%2F%2Ff.vimeocdn.com%2Fimages_v6%2Fshare%2Fplay_icon_overlay.png","",$imgBNA);
				$topList[$key]["profilExternImageUrl"] = $imgBNA;
			}	else {
				$topList[$key]["profilExternImageUrl"] = $this->module->assetsUrl."/images/thumbnail-default.jpg";
			}

			$topList[$key]["typeSig"] = "poi";
			$topList[$key]["href"] = "#page.type.".Poi::COLLECTION.".id.".(string)$elem["_id"];
			if (@$elem["description"]){
				$topList[$key]["description"]=strip_tags($elem["description"]);
			} else
				$topList[$key]["description"]= "<i>Pas de description sur cette production</i>";
		}
	}
	
?>
<div class="col-xs-12 header-costum no-padding">
	<div class="col-xs-12 main-top-menu no-padding">
	<?php if (!empty($topList)) { ?>
	<!-- <div class="col-xs-12 no-padding main-gallery-top" >
		<div class="pull-left frame" id="cycleitems" >
			<ul class="slidee col-xs-12" style="width : 100% !important" >

			</ul>
		</div>
	</div> -->
	<div class="col-xs-12 no-padding main-gallery-top">
		<div class="frame" id="cycleitems">
			<ul class="slidee">
			</ul>
		</div>
	</div>
	<?php } ?>
	</div>
</div>


<script>
	var poiListTags = <?php echo json_encode($tagsPoiList) ?>;;
	var topList = <?php echo json_encode($topList) ?>;

	function displayableInCarousel(element) { 
	if(typeof element['carousel']!="undefined"){
	  return element['carousel'] == 'true';
	}}
	var displayable = topList.filter(displayableInCarousel);

	function activeMenuTop(thisJQ){
		$(".btn-menu-top").removeClass("active");
		thisJQ.addClass("active");
	}
	var slyOptions = {
		slidee : '.slidee',
		horizontal: true,
		itemNav: 'centered',
		smart: true,
		activateOn: 'click',
		mouseDragging: true,
		touchDragging: true,
		releaseSwing: true,
		startAt: 0,
		//scrollBar: $wrap.find('.scrollbar'),
		scrollBy: true,
		speed: 1000,
		elasticBounds: true,
		easing: 'linear',
		dragHandle: true,
		dynamicHandle: true,
		clickBar: true,

		// Cycling
		cycleBy: 'items',
		cycleInterval: 2500,
		pauseOnHover: true,
	}

	var filteredTopList = displayable;
	
	function addItemsToSly(tagFilter,locality=null,dataReturn=false){

		mylog.log("notragora here addItemsToSly", tagFilter, locality, displayable, dataReturn);
		//removeAll

		$(".slidee .searchPoiContainer").remove();
		
		var nameTag = tagFilter;
		//filter displayable
		 filteredTopList = displayable;

		if(tagFilter){
			filteredTopList = displayable.filter(item => ( typeof item.tags != "undefined" ? item.tags.includes(tagFilter) : null) );
		}

		if(locality){
			filteredTopList= filteredTopList.filter(item =>(typeof item.address != "undefined" && typeof item.address.localityId != "undefined" ? item.address.localityId==locality : null ));
		}
		
		var count = 0;

		mylog.log("filteredTopList",filteredTopList);
		
		if(typeof filteredTopList != "undefined" && filteredTopList != null && filteredTopList.length > 0){
			if(dataReturn){
				mylog.log("filteredTopList return",filteredTopList);
				return filteredTopList;
			}
			//mylog.log("notragora here filteredTopList", filteredTopList);

			$.each(filteredTopList, function(key, topElem){
				mylog.log("sly loop");
				var elem = '<li class="searchPoiContainer">' +
								'<span class="item-galley-top">' +
									'<a href="'+ topElem.href +'" class="lbh">' +
										'<img src="'+ topElem.profilExternImageUrl +'" class="img-galley-top">' +
									'</a>' +
								'</span>' +
								'<span class="description-poi" style="display: none;">' +
									'<div>' +
										'<h3>'+ topElem.name + '</h3>' +
										'<span class="poiTopDescription">' + topElem.description +'</span>' +
									'</div>' +
									'<a href="' + topElem.href +' " class="btn btn-default lbh"> Voir la réalisation </a>' +
								'</span>' +
							'</li>';
				count++;
				sly.add(elem);
			});
			// for (var topElem of filteredTopList) {
			// 	//mylog.log("notragora here for ", topElem);
			// 	var elem = '<li class="searchPoiContainer">' +
			// 					'<span class="item-galley-top">' +
			// 						'<a href="'+ topElem.href +'" class="lbh">' +
			// 							'<img src="'+ topElem.profilExternImageUrl +'" class="img-galley-top">' +
			// 						'</a>' +
			// 					'</span>' +
			// 					'<span class="description-poi" style="display: none;">' +
			// 						'<div>' +
			// 							'<h3>'+ topElem.name + '</h3>' +
			// 							'<span class="poiTopDescription">' + topElem.description +'</span>' +
			// 						'</div>' +
			// 						'<a href="' + topElem.href +' " class="btn btn-default lbh"> Voir la réalisation </a>' +
			// 					'</span>' +
			// 				'</li>';
			// 	count++;
			// 	sly.add(elem);
			// }
		}
		
		
		 sly.activate(5);

		if(typeof tagFilter != "undefined"){
			$("#tagSelectedHead").html("#" + nameTag);
			$("#countPoiHead").html(count + " productions");
			$("#poiSelectedHead").show();
		}else{
			$("#poiSelectedHead").hide();
		}
		
		$(".searchPoiContainer").mouseenter(function(){
			$(this).find(".description-poi").show();
		}).mouseleave(function(){
			$(this).find(".description-poi").hide();
		});

		coInterface.bindLBHLinks();
	}



	jQuery(document).ready(function() {
		//naObj.initVar();
		//mylog.log("notragora here jQuery", slyOptions, poiListTags, topList);
		//sly = null ;
		if( /*poiListTags.length > 0 && */ ( typeof sly == "undefined" || (typeof sly != "undefined" && sly instanceof Object == false) ) ) {
			sly = new Sly( '#cycleitems', slyOptions ).init(); 
			mylog.log("notragora here2", sly);
			sly.on('load', function (eventName) {
				 $("#cycleitems .slidee").css({
					width : '27000px', // largeur de 100%
				});

			});
		}


		var poiHead = '<span class="btn-menu-top tooltips pull-left"  id="poiSelectedHead">'+
							'<span id="tagSelectedHead" class="" ></span> '+
							': <span id="countPoiHead" class="text-green"></span>'+
							'<button class="btn btn-default btn-menu-top tooltips text-red"  onclick="addItemsToSly()" '+
							'style="background: transparent !important; border-color: transparent !important; " '+
									'data-toggle="tooltip" data-placement="bottom" title="Supprimer le tag sélectionné" alt="Supprimer le tag sélectionné">'+
									'<i class="fa fa-trash"></i>'+
							'</button>'+
						'</span>';
		$("#menuTopRight").before(poiHead);
		$("#menuTopRight #menu-thumb-profil").width(30);
		$("#menuTopRight #menu-thumb-profil").height(30);
		

		 addItemsToSly();
		
	});
</script>
