<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var paramsFilter= {
		container : "#filters-nav",
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true
        },
	 	defaults : {
	 	 	// indexStep : 30,
	 	 	indexMin : 30,
	 	 	types : ["NGO","LocalBusiness","Group","GovernmentOrganization"],
	 	 	filters : {
	 	 		categoryNA : [ "group" ]
	 	 	}
	 	},
	 	filters : {
		    scopeList : {
		    	name : "Territoire",
		    	params : {
	 				countryCode : ["FR"], 
	 				level : ["1"]
	 			}
		    }
		}
	 	
	}; 

var filterSearch={};
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);

		setTimeout(getDistinctTerritory,1000);
    
	function getDistinctTerritory(){
		var distinctCities ={};
		var htmlFilter = "";

		$.each(topList,function(ind,values){
			if(typeof values.address!="undefined" && typeof values.address.localityId!="undefined"){
				if(typeof distinctCities[values.address.localityId]=="undefined"){
					htmlFilter+='<button data-toggle="dropdown" data-id="'+values.address.localityId+'" class="col-xs-12" data-type="scopeList" data-value="'+values.address.addressLocality+'" >'+values.address.addressLocality+'</button>';

					distinctCities[values.address.localityId]={
						name : values.address.addressLocality,
						city: values.address.localityId,
						cityName: values.address.addressLocality,
						country: values.address.addressCountry,
						key: values.address.localityId+"city"+values.address.postalCode,
						level1: values.address.level1,
						level1Name: values.address.level1Name,
						level3: values.address.level3,
						level3Name: values.address.level3Name,
						level4: values.address.level4,
						level4Name: values.address.level4Name,
						postalCode: values.address.postalCode,
						type: "city",
						uniqueCp: true

					};
				}
			}
		});
		mylog.log("distinctCities prod",distinctCities);
		filterSearch.filters.lists.scopeList=distinctCities;

		$(".dropdown-menu.scopeList .list-filters").html(htmlFilter);
		filterSearch.filters.events.scopeList(filterSearch,"#filters-nav");
		


	}				
	});

</script>



