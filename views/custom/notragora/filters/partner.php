<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var paramsFilter= {
		container : "#filters-nav",
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true
        },
	 	defaults : {
	 	 	// indexStep : 30,
	 	 	indexMin : 30,
	 	 	types : ["NGO","LocalBusiness","Group","GovernmentOrganization"],
	 	 	filters : {
	 	 		categoryNA : [ "partner", "producors", "supports" ]
	 	 	}
	 	},
	 	filters : {
			partner : {
				view : "horizontalList",
		        type : "filters",
		        name : "Types de partenaires",
		        field : "categoryNA",
		        event : "filters",
		        action : "filters",
		        keyValue : false,
		        list : {
		            "partner" : "Partenaires", 
		            "producors" : "Producteurs", 
		            "supports" : "Soutiens"
		        }
		    }
		}	 	
	};



	// if (pageApp =="partner"){
		
	// 		paramsFilter.filters = {
	// 			partner : {
	// 				view : "horizontalList",
	// 	            type : "filters",
	// 	            name : "Types de partenaires",
	// 	            field : "categoryNA",
	// 	            event : "filters",
	// 	            action : "filters",
	// 	            keyValue : false,
	// 	            list : {
	// 	            	"partner" : "Partenaires", 
	// 	            	"producors" : "Producteurs", 
	// 	            	"supports" : "Soutiens"
	// 	            }
	// 	        }
	// 	    };
	// 	}    
	//     paramsFilter.defaults.filters.categoryNA = [];        
		
	
	 

var filterSearch={};
	jQuery(document).ready(function() {
		// if(typeof appConfig.filters != "undefined"){
		// 	if(typeof appConfig.filters.categoryNa != "undefined"){
		// 		paramsFilter.defaults.category=appConfig.filters.categoryNa;
		// 	}	
		// }



		
		  	searchObj.search.autocomplete = function(fObj, callBack){

		    		mylog.log("searchObj.search.autocomplete");
					var searchParams=fObj.search.constructObjectAndUrl(fObj);  
					mylog.log("searchParams notra",searchParams);
					if((typeof searchParams.filters!="undefined" && typeof searchParams.filters.categoryNA!="undefined" && searchParams.filters.categoryNA.length==0)||typeof searchParams.filters.categoryNA=="undefined") {
						searchParams.filters.categoryNA=[ "partner", "producors", "supports" ];
					}
					else if(typeof searchParams.filters!="undefined" && typeof searchParams.filters.categoryNA!="undefined" && searchParams.filters.categoryNA.length>3){
						searchParams.filters.categoryNA.splice(0,3);
					}

					mylog.log("searchParams notra mod",searchParams);
					
					if(fObj.results.map.active && !fObj.results.map.sameAsDirectory){
						searchParams["indexMin"]=0;
						searchParams["indexStep"]=0;
						searchParams["mapUsed"]=true;
						if(typeof searchParams.ranges != "undefined"){ delete searchParams.ranges;}
					}
					if(typeof fObj[fObj.search.loadEvent.active].searchParams != "undefined") 
						searchParams = fObj[fObj.search.loadEvent.active].searchParams(fObj, searchParams);
					ajaxPost(
						null,
						fObj.urlData,
						searchParams,
						function(data){ 
							//mylog.log("searchObj.search.autocomplete >>> success autocomplete search !!!! ", data); //mylog.dir(data);
							if(!data){ 
								toastr.error(data.content); 
							} else {
								if(typeof fObj[fObj.search.loadEvent.active].successComplete != "undefined"){
									fObj[fObj.search.loadEvent.active].successComplete(fObj, data)
								}else{
									if(fObj.search.countResult && typeof data.count != "undefined"){
										fObj.results.count=data.count;
										fObj.filters.actions.types.setBadges(fObj);
										fObj.results.countResults(fObj);
										fObj.header.set(fObj);
									}
									//Prepare results object for render 
									var results=data.results;
									if(typeof fObj.search.obj.ranges != "undefined"
										&& !fObj.results.map.active)
					                	results=fObj.search.multiCols.getResults(fObj, results);
					                //Render results
					                if(typeof fObj[fObj.search.loadEvent.active].render == "function")
										fObj[fObj.search.loadEvent.active].render(fObj, results, data)
									else
										fObj.results.render(fObj, results, data);

		    						//bind search interface event on init (callBack because number needs for pagination) 
									if(fObj.search.loadEvent.bind){
									 	fObj[fObj.search.loadEvent.active].event(fObj);
										fObj.search.loadEvent.bind=false;
									}

		    						//signal que le chargement est terminé
									if(typeof callBack == "function"){
										callBack(fObj, results);
									}

									if($(".badge-theme-count").length!=0 && !fObj.filters.actions.themes.isLoaded){
										fObj.filters.actions.themes.setThemesCounts(fObj);
									}
								}
							}
						},
						function (data){
							mylog.log("searchObj.search.autocomplete >>> error autocomplete search"); 
							mylog.dir(data);   
							$(fObj.results.dom).append(data.responseText);  
							//signal que le chargement est terminé
							fObj.search.currentlyLoading = false;   
						}
					);
		    	
		  	}
		  	
		  		
		  

		  filterSearch = searchObj.init(paramsFilter);
		// if($(".filters-activate").length>0){
		// 	paramsFilter.defaults.filters.categoryNA = ["supports"];
		// 	filterSearch = searchObj.init(paramsFilter);
		// }




		

		
	});

</script>



