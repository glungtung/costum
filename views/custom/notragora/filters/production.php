<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var paramsFilter= {
		container : "#filters-nav",
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true
        },
	 	defaults : {
	 	 	indexMin : 30,
	 	 	types : ["poi"],
	 	},
	 	filters : {
			collections : {
				view : "dropdownList",
		        type : "tags",
		        name : "Collection",
		        event : "tags",
		        list : costum.lists.collections
		    },
		    scopeList : {
		    	name : "Territoire",
		    	params : {
	 				countryCode : ["FR"], 
	 				level : ["1"]
	 			}
		    }
		}
	 	
	}; 
	if(pageApp=="map"){
		paramsFilter.results.map = {
				active:true
		};
	}		

var filterSearch={};
	jQuery(document).ready(function() {

		    	searchObj.search.autocomplete = function(fObj, callBack){
		    		mylog.log("searchObj.search.autocomplete");
					var searchParams=fObj.search.constructObjectAndUrl(fObj);
					if(fObj.results.map.active && !fObj.results.map.sameAsDirectory){
						searchParams["indexMin"]=0;
						searchParams["indexStep"]=0;
						searchParams["mapUsed"]=true;
						if(typeof searchParams.ranges != "undefined"){ delete searchParams.ranges;}
					}
					if(typeof fObj[fObj.search.loadEvent.active].searchParams != "undefined")
						searchParams = fObj[fObj.search.loadEvent.active].searchParams(fObj, searchParams);

					ajaxPost(
						null,
						fObj.urlData,
						searchParams,
						function(data){
							//mylog.log("searchObj.search.autocomplete >>> success autocomplete search !!!! ", data); //mylog.dir(data);
							if(!data){
								toastr.error(data.content);
							} else {
								if(typeof fObj[fObj.search.loadEvent.active].successComplete != "undefined"){
									fObj[fObj.search.loadEvent.active].successComplete(fObj, data)
								}else{
									if(fObj.search.countResult && typeof data.count != "undefined"){
										fObj.results.count=data.count;
										fObj.filters.actions.types.setBadges(fObj);
										fObj.results.countResults(fObj);
										fObj.header.set(fObj);
									}
									//Prepare results object for render
									var results=data.results;
									if(typeof fObj.search.obj.ranges != "undefined"
										&& !fObj.results.map.active)
					                	results=fObj.search.multiCols.getResults(fObj, results);
					                //Render results
					                if(typeof fObj[fObj.search.loadEvent.active].render == "function")
										fObj[fObj.search.loadEvent.active].render(fObj, results, data)
									else
										fObj.results.render(fObj, results, data);

		    						//bind search interface event on init (callBack because number needs for pagination)
									if(fObj.search.loadEvent.bind){
									 	fObj[fObj.search.loadEvent.active].event(fObj);
										fObj.search.loadEvent.bind=false;
									}

		    						//signal que le chargement est terminé
									if(typeof callBack == "function"){
										callBack(fObj, results);
									}

									if($(".badge-theme-count").length!=0 && fObj.filters.actions.themes.isLoaded==false){
										fObj.filters.actions.themes.setThemesCounts(fObj);
									}
								}
							}
						},
						function (data){
							mylog.log("searchObj.search.autocomplete >>> error autocomplete search");
							mylog.dir(data);
							$(fObj.results.dom).append(data.responseText);
							//signal que le chargement est terminé
							fObj.search.currentlyLoading = false;
						}
					);
		    	};

		filterSearch = searchObj.init(paramsFilter);
    setTimeout(getDistinctTerritory,1000);
    
	function getDistinctTerritory(){
		var distinctCities ={};
		var htmlFilter = "";

		$.each(topList,function(ind,values){
			if(typeof values.address!="undefined" && typeof values.address.localityId!="undefined"){
				if(typeof distinctCities[values.address.localityId]=="undefined"){
					htmlFilter+='<button data-toggle="dropdown" data-id="'+values.address.localityId+'" class="col-xs-12" data-type="scopeList" data-value="'+values.address.addressLocality+'" >'+values.address.addressLocality+'</button>';

					distinctCities[values.address.localityId]={
						name : values.address.addressLocality,
						city: values.address.localityId,
						cityName: values.address.addressLocality,
						country: values.address.addressCountry,
						key: values.address.localityId+"city"+values.address.postalCode,
						level1: values.address.level1,
						level1Name: values.address.level1Name,
						level3: values.address.level3,
						level3Name: values.address.level3Name,
						level4: values.address.level4,
						level4Name: values.address.level4Name,
						postalCode: values.address.postalCode,
						type: "city",
						uniqueCp: true

					};
				}
			}
		});
		mylog.log("distinctCities prod",distinctCities);
		filterSearch.filters.lists.scopeList=distinctCities;

		$(".dropdown-menu.scopeList .list-filters").html(htmlFilter);
		filterSearch.filters.events.scopeList(filterSearch,"#filters-nav");
		


	}			

				
	});

</script>



