<?php
HtmlHelper::registerCssAndScriptsFiles( 
	array( 
		'/vendor/colorpicker/js/colorpicker.js',
		'/vendor/colorpicker/css/colorpicker.css',
		'/css/default/directory.css',	
		'/css/profilSocial.css',
		'/css/calendar.css',
	), Yii::app()->theme->baseUrl. '/assets'
);

$cssAnsScriptFilesModule = array(
	'/js/default/calendar.js',
    '/js/default/profilSocial.js',
    '/js/default/editInPlace.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

$cssAnsScriptFilesTheme = array(
	"/plugins/jquery-cropbox/jquery.cropbox.css",
	"/plugins/jquery-cropbox/jquery.cropbox.js",
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/jquery.qrcode/jquery-qrcode.min.js',
	'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
    '/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
    '/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
    "/plugins/d3/d3.js",
    "/plugins/d3/d3-flextree.js",
    "/plugins/d3/view.mindmap.js",
    "/plugins/d3/view.mindmap.css",
    
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style>
	#onepage {
		background-color: white;
	}

	.acceptBtn{
		border-radius:3px !important;
		color: white;
		background-color: #71CE4E;
		padding: 5px 10px;
		margin-top: 5px;
	}
	.acceptBtn:hover{
		color: #71CE4E !important;
		background-color: white;
		border: 1px solid #71CE4E;
	}
	.acceptBtn i{
		font-size:12px;
	}
	.refuseBtn{
		border-radius:3px !important;
		color: white;
		background-color: #E33551;
		padding: 5px 10px;
		margin-top: 5px;

	}
	.refuseBtn:hover{
		color: #E33551 !important;
		background-color: white;
		border: 1px solid #E33551;
	}
	.refuseBtn i{
		font-size:12px;
	}
	.waitAnswer{
		position:absolute;
		left:38px;
	}
	.col-members{
		background-color: #fff !important;
	    min-height: 100%;
	    position: absolute;
	    right: 0px;
	    -webkit-box-shadow: 0px 5px 5px -2px #656565 !important;
	    -o-box-shadow: 0px 5px 5px -2px #656565 !important;
	    /* box-shadow: 0px -5px 5px -2px #656565 !important; */
	    filter: progid:DXImageTransform.Microsoft.Shadow(color=#656565, Direction=NaN, Strength=5) !important;
	}
	.img-header{
		max-height: 350px;
		width:100%;
		overflow: hidden;
		/*background-image: url("<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/tropic.jpg");
		background-size: 100%;*/
	}
	.main-col-search{
		padding:0px;
	}
	.mix{
		min-height: 100px;
		/*width: 31.5%;*/
		background-color: white;
		display: inline-block;
		border:1px solid #bbb;
		margin-right : 1.5%;
		border-radius: 10px;
		padding:1%;
		margin:-1px;
		-webkit-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		-moz-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
	}
	#grid .followers{
	display: none;
}
	.mix a{
		color:black;
		/*font-weight: bold;*/
	}
	.mix .imgDiv{
		float:left;
		width:30%;
		background: ;
		margin-top:0px;
	}
	.mix .detailDiv{
		float:right;
		width:70%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}

	.mix .toolsDiv{
		float:right;
		width:20%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		color:white;
	}

	.mix .text-xss{ font-size: 11px; }

	#Grid{
		margin-top: 20px;
		background-color: transparent;
		padding: 15px;
		border-radius: 4px;
		/*border-right: 1px solid #474747;*/
		padding: 0px;
		width:100%;
	}
	#Grid .mix{
		margin-bottom: -1px !important;
	}
	#Grid .item_map_list{
		padding:10px 10px 10px 0px;
		margin-top:0px;
		text-decoration:none;
		background-color:white;
		border: 1px solid rgba(0, 0, 0, 0.08); /*rgba(93, 93, 93, 0.15);*/
	}
	#Grid .item_map_list .left-col .thumbnail-profil{
		width: 75px;
		height: 75px;
	}
	#Grid .ico-type-account i.fa{
		margin-left:11px !important;
	}
	#Grid .thumbnail-profil{
		margin-left:10px;
	}
	#Grid .detailDiv a.text-xss{
		font-size: 12px;
		font-weight: 300;
	}

	.label.address.text-dark{
		padding:0.4em 0.1em 0.4em 0em !important;
	}
	.detailDiv a.thumb-info.item_map_list_panel{
		font-weight:500 !important;
	}

	.shadow {
	    -webkit-box-shadow: none;
	    -moz-box-shadow: none;
	    box-shadow: none;
	}
	#description .container{
		width: 60%;
		margin-left: 20%;
		font-size: 15px;
	}
	.section-title{
		text-transform: uppercase;
		font-weight: 700;
		color: rgb(92,75,62) !important;
	}

	#description .btn-edit-section{
		display: none;

	}

	.col-members h3{
		text-transform: uppercase;
		color:rgb(92,75,62);
		font-size: 0.7em;
		font-weight: 700;
	}
	.col-members h4{
		color:rgb(92,75,62);
		font-size: 0.9em;
		font-weight: 700;

	}
	.col-members .username-min{
		font-weight: 700;
		color:grey;
	}

	.elipsis{
		display: block;
	}

	.element-name{
		font-size:18px;
		padding:10px 20px;
		font-weight: 700;
		height:50px;
		margin-top:0px;
		background-color: rgba(255, 255, 255, 0.8);
	}
	.btn-follow{
		font-weight: 700;
		font-size:13px;
		border-radius:40px;
		border:none;
	}
	.menubar{
		-webkit-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 5px 5px -5px rgba(50, 50, 50, 0.75);
		margin-bottom: 40px;
	}
	.btn-menubar{
		font-weight: 700;
		font-size: 12px;
		border-radius: 40px;
		border: none;
		background-color: white;
		padding: 13px 20px;
	}

	.btn-menubar:hover{
		background-color: #4a4a4a;
		color:white;
		-webkit-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
	}
	iframe.fullScreen {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    top: 0;
	    left: 0;
	}
	.contentEntity{
		padding: 0px !important;
		margin: 0px !important;
		border-top: solid rgba(128, 128, 128, 0.2) 1px;
		margin-left: 0% !important;
		width: 100%;
		box-shadow: 0px 0px 5px -1px #d3d3d3;
	}
	.contentEntity:hover {
   	 background-color: rgba(211, 211, 211, 0.2);
	}
	.container-img-parent {
	    display: block;
	    width: 100%;
	    max-width: 100%;
	    /*min-height: 90px;*/
	    max-height: 90px;
	    overflow: hidden;
	    background-color: #d3d3d3;
	    text-align: center;
    }
    .container-img-parent i.fa {
	    margin-top: 20px;
	    font-size: 50px;
	    color: rgba(255, 255,255, 0.8);
	}

	.fileupload, .fileupload-preview.thumbnail, .fileupload-new .thumbnail,
	.fileupload-new .thumbnail img, .fileupload-preview.thumbnail img {
	    width: auto !important;
	}
	.user-image{
		background-color: white;
	}
	#fileuploadContainer{
		margin:-1px!important;
	}
	#fileuploadContainer .thumbnail{
		border-radius: 0px!important
	}
	#profil_imgPreview{}
	.removeLink{
		    display: none;
    position: absolute;
    right: -5px;
    top: -5px;
    border-radius: 12px;
    width: 25px;
    background-color: black;
    height: 25px;
    color: white;
    border: inherit;
	}
	.removeLink:hover{
		font-size: 15px;
		border: 1px solid white;
	}
	.contentItem{
		float:left;
		position: relative;
	}
	#divTags .tag {
	    background-color: #3A87AD !important;
	    border-color: #3A87AD !important;
	}
</style>
<div class="col-lg-10 col-md-10 col-sm-9 no-padding" id="onepage">
<?php
if ($type == "poi"){
	if(@$element["type"]=="video" && @$element["medias"] && @$element["medias"][0]["content"]["videoLink"]){
		$videoLink=str_replace ( "autoplay=1" , "autoplay=0" , @$element["medias"][0]["content"]["videoLink"]  );
	?>
		<div class="col-xs-12">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item fullScreen" src="<?php echo @$videoLink ?>" allowfullscreen></iframe>
			</div>
		</div>
	<?php } else if( @$element["medias"] && !empty($element["medias"][0]["content"]["url"]) ) {

		?>
		<div class="col-xs-12">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item fullScreen" src="<?php echo $element["medias"][0]["content"]["url"] ?>" allowfullscreen></iframe>
			</div>
		</div>
	<?php
	} else if( @$element["urls"] && !empty($element["urls"][0]) ) {

		?>
		<div class="col-xs-12">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item fullScreen" src="<?php echo $element["urls"][0] ?>" allowfullscreen></iframe>
			</div>
		</div>
	<?php
	} ?>
		<div class="col-md-12 col-sm-12 col-xs-12 text-dark center">
			<h1 class="center">
				<?php echo $element['name']; ?>
			</h1>
			<?php if(@Yii::app()->session["userId"]){ ?>
				<?php if ($canEdit==true || ($openEdition == true )) { ?>
					<a href="javascript:;" class="btn btn-xs text-dark btn-edit-preview editThisBtn"  data-type="poi" data-id="<?php echo (string)$element["_id"] ?>" ><i class="fa fa-pencil-square-o"></i> <?php echo Yii::t("common","Edit") ?></a>.
					<a href="javascript:;" class="btn btn-xs text-red deleteThisBtn" data-type="poi" data-id="<?php echo (string)$element["_id"] ?>" ><i class="fa fa-trash"></i> <?php echo Yii::t("common","Delete") ?></a>
					<div class="space1"></div>
				<?php } ?>
			<?php } ?>
		</div>

<?php 
}else{ ?>
	<div class="img-header">
		<div id="contentBanner" class="col-md-12 col-sm-12 col-xs-12 no-padding">
			<?php if (isset($element["profilMediumImageUrl"])){	
				$imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
					src="'.Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]).'">';
				if ($element["profilMediumImageUrl"] == ""){
					$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]).'"
								class="thumb-info"  
								data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
								data-lightbox="all">'.
								$imgHtml.
							'</a>';
				}
				echo $imgHtml;
			} ?>
		</div>
	</div>
<?php 
} 
?>
	<div class="col-xs-12" style="margin-top: 30px;">
		<div id="divProducors" class="col-sm-4 col-xs-12  padding-10">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<h4 class="col-md-8 col-sm-8 col-xs-8 no-margin margin-bottom-5 no-padding text-dark" style="font-size: 18px; font-weight: 400;">Producteurs</h4>
			<?php if(@Yii::app()->session["userId"]){ ?>
				<?php if ($canEdit==true || ($openEdition == true )) { ?>
			<button class="btn btn-xs bg-white text-dark pull-right editLink"><i class="fa fa-pencil"></i></button>
			<button class="btn btn-xs bg-white margin-left-5 text-green btn-menu-element-prodsupport tooltips pull-right"
				data-toggle='modal'
				data-placement="bottom"
				data-original-title="Ajouter des producteurs"
				data-target='#modal-scope'
				data-connect='producors'>
				<i class="fa fa-plus"></i>
			</button>
			<?php } } ?>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
			<?php if(@$element["producors"] && !empty($element["producors"])){
				foreach($element["producors"] as $key => $value){
					$orga=Organization::getSimpleOrganizationById($key);
					$urlImg=$this->module->assetsUrl.'/images/thumb/default_organizations.png';
					if(@$orga["profilMediumImageUrl"] && !empty($orga["profilMediumImageUrl"]) )
						$urlImg=Yii::app()->createUrl('/'.$orga["profilMediumImageUrl"]);
					?>
					<div class="contentItem contentItem<?php echo $key ?>">
						<a href="#page.type.<?php echo Organization::COLLECTION ?>.id.<?php echo $key ?>" data-placement="top" data-original-title="<?php echo $orga["name"] ?>" class=" lbh btn no-padding contentImg tooltips">
						<img width="75" height="75"  alt="image" class="" src="<?php echo $urlImg ?>">
						</a>
						<button class="removeLink" data-type="producors" data-id="<?php echo $key ?>"><i class="fa fa-remove"></i></button>
					</div>

			<?php }
			}  else {
				echo "<i>Aucun producteur ajouté</i>";
			}
			?>
			</div>
			<?php 
			echo $this->renderPartial('costum.views.custom.notragora.addSupportProductor',array("type"=>$type, "parentId" =>(string)$element['_id'])); ?>
		</div>
		<div id="divSupports" class="col-sm-4 col-xs-12  padding-10">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<h4 class="col-md-8 col-sm-8 col-xs-8 no-margin margin-bottom-5 no-padding text-dark" style="font-size:18px; font-weight: 400;">Soutiens</h4>
			<?php if(@Yii::app()->session["userId"]){ ?>
				<?php if ($canEdit==true || ($openEdition == true )) { ?>
			<button class="btn btn-xs bg-white text-dark pull-right editLink"><i class="fa fa-pencil"></i></button>
			<button class="btn btn-xs bg-white margin-left-5 text-green btn-menu-element-prodsupport tooltips pull-right"
				data-toggle='modal'
				data-placement="bottom"
				data-original-title="Ajouter des soutiens"
				data-target='#modal-scope'
				data-connect='supports'>
				<i class="fa fa-plus"></i>
			</button>
			<?php } } ?>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
			<?php if(@$element["supports"] && !empty($element["supports"])){
				foreach($element["supports"] as $key => $value){
					$orga=Organization::getSimpleOrganizationById($key);
					$urlImg=$this->module->assetsUrl.'/images/thumb/default_organizations.png';
					if(@$orga["profilImageUrl"] && !empty($orga["profilImageUrl"]) )
						$urlImg=Yii::app()->createUrl('/'.$orga["profilImageUrl"]);
					?>
					<div class="contentItem contentItem<?php echo $key ?>">
						<a href="#page.type.<?php echo Organization::COLLECTION ?>.id.<?php echo $key ?>" data-placement="top" data-original-title="<?php echo $orga["name"] ?>" class=" lbh btn no-padding contentImg tooltips">
						<img width="75" height="75"  alt="image" class="" src="<?php echo $urlImg ?>">
						</a>
						<button class="removeLink" data-type="supports" data-id="<?php echo $key ?>"><i class="fa fa-remove"></i></button>
					</div>

			<?php }
			}  else {
				echo "<i>Aucun soutien ajouté</i>";
			}
			?>
			</div>
		</div>
		<div id="divPartenaires" class="col-sm-4 col-xs-12 padding-10">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<h4 class="col-md-8 col-sm-8 col-xs-8 no-margin margin-bottom-5 no-padding text-dark" style="font-size: 18px; font-weight: 400;">Partenaires</h4>
			<?php if(@Yii::app()->session["userId"]){ ?>
				<?php if ($canEdit==true || ($openEdition == true )) { ?>
			<button class="btn btn-xs bg-white text-dark pull-right editLink"><i class="fa fa-pencil"></i></button>
			<button class="btn btn-xs bg-white margin-left-5 text-green btn-menu-element-prodsupport tooltips pull-right"
				data-toggle='modal'
				data-placement="bottom"
				data-original-title="Ajouter des partenaires"
				data-target='#modal-scope'
				data-connect='partner'>
				<i class="fa fa-plus"></i>
			</button>
			<?php } } ?>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
			<?php if(@$element["partner"] && !empty($element["partner"])){
				foreach($element["partner"] as $key => $value){
					$orga=Organization::getSimpleOrganizationById($key);
					$urlImg=$this->module->assetsUrl.'/images/thumb/default_organizations.png';
					if(@$orga["profilImageUrl"] && !empty($orga["profilImageUrl"]) )
						$urlImg=Yii::app()->createUrl('/'.$orga["profilImageUrl"]);
					?>
					<div class="contentItem contentItem<?php echo $key ?>">
						<a href="#page.type.<?php echo Organization::COLLECTION ?>.id.<?php echo $key ?>" data-placement="top" data-original-title="<?php echo $orga["name"] ?>" class=" lbh btn no-padding contentImg tooltips">
						<img width="75" height="75"  alt="image" class="" src="<?php echo $urlImg ?>">
						</a>
						<button class="removeLink" data-type="partner" data-id="<?php echo $key ?>"><i class="fa fa-remove"></i></button>
					</div>

			<?php }
			}  else {
				echo "<i>Aucun partenaire ajouté</i>";
			}
			?>
			</div>
			<?php 
			echo $this->renderPartial('costum.views.custom.notragora.addSupportProductor',array("type"=>$type, "parentId" =>(string)$element['_id'])); ?>
		</div>
	</div>

	<div id="section-home">
		<div class="col-xs-12 text-center">
		    <h2 class="section-title">
		        <span class="sec-title">Présentation</span><br>
		        <i class="fa fa-angle-down"></i>
		    </h2>
		</div>
		<div class="col-xs-8 col-xs-offset-2  no-padding item-desc" id="descriptionAbout"><?php echo !empty($element['description']) ? @$element['description'] : "<span class='padding-10'><center><i>- Pas de présentation -</center></i></span>"; ?></div>
		<div class="col-xs-8 col-xs-offset-2  no-padding item-desc hidden" id="descriptionMarkdown"><?php echo !empty($element['description']) ? @$element['description'] : ""; ?></div>
	</div>
	
	<div id="divTags" class="col-md-12 col-sm-12 col-xs-12 padding-10">
		<?php 
		if(!empty($element["tags"]) ){ 
				$i=0;
				foreach($element["tags"] as $tag){
					if($i<6) {
						$i++;?>
						<div class="tag label label-default" data-val="<?php echo  $tag; ?>" style="margin:5px;">
							<i class="fa fa-tag"></i> <?php echo  $tag; ?>
						</div>
		<?php 		}
				}
		} ?>
	</div>

	<section id="timeline" class="inline-block col-md-12 text-center"  style="background-color: #f8f6f6;">
		<h2 class="section-title text-dark">Commentaires</h2>
		<div id="comment-page" class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1" style="margin-top: 10px;"></div>
	</section>
</div>
<div class="col-lg-2 col-md-2 col-sm-3 col-members">
	<h3>Réalisé par</h3>
	<hr>
<?php
	if(!empty($element["parent"])){
		foreach ($element["parent"] as $key => $value) {
			$parent = Element::getElementById($key, $value["type"], array(), array("name", "slug", "profilImageUrl"));

			if(!empty($parent)){
	?>
	<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding'>
		<div class="contentEntity">
			<a href="#@<?php echo $parent["slug"] ?>" class="container-img-parent lbh add2fav">
				<?php
				$imgProfil = "<i class='fa fa-image fa-2x'></i>";
				if(@$parent["profilImageUrl"] && !empty($parent["profilImageUrl"])){
					$imgProfil= "<img class='img-responsive' src='".Yii::app()->createUrl($parent["profilImageUrl"])."'/>";
	    		}
				echo $imgProfil;
	        	?>
			</a>
			<div class="padding-10 informations">
				<a href='#@<?php echo $parent["slug"] ?>' class='entityName text-dark lbh add2fav text-light-weight margin-bottom-5'>
		            <?php echo @$parent["name"] ; ?>
		        </a>
	        </div>
        </div>
	</div>

	<?php 	} 
		} 
	} ?>
</div>
<script type="text/javascript">
var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>;
var canEdit = <?php echo json_encode($canEdit) ?>;
var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
						"#@"+contextData.slug : 
						"#page.type."+contextData.type+".id."+contextData.id);
    
if(location.hash.indexOf("#page")>=0){
	strHash="";
	if(location.hash.indexOf(".view")>0){
		hashPage=location.hash.split(".view");
		strHash=".view"+hashPage[1];
	}
	replaceSlug=true;
	history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", hashUrlPage+strHash);

}
jQuery(document).ready(function() {
	initMenuDetail();
	loadDataDirectory();
	inintDescs();
	directory.bindBtnElement();

	$(".editLink").click(function(){
		$(this).parents().eq(1).find(".removeLink").show();
	});

	$(".removeLink").click(function(){
		var connect=$(this).data("type");
		var id=$(this).data("id");
		ajaxPost(
			null,
			baseUrl+'/costum/notragora/deletelink',
			{connect:connect, id:id, parentId:contextData.id },
			function(data){ 
				if ( data && data.result ) {
			    	if(connect=="supports")
			    		msg="Bien supprimé des soutiens";
			    	else
			    		msg="Bien supprimé des producteurs";
			        toastr.info(msg);
			        $(".contentItem"+id).remove();
			        //loadByHash("#"+parentType.substr(0, parentType.length - 1)+".detail.id."+parentId);
			    } else {
			           toastr.error("Une erreur est survenue : ".data.msg);
			    }
			}
		);
	});

	getAjax("#comment-page",baseUrl+"/"+moduleId+"/comment/index/type/"+contextData.type+"/id/"+contextData.id,
					function(){  //$(".commentCount").html( $(".nbComments").html() ); 
					},"html");
});

function inintDescs() {
	mylog.log("inintDescs");
	if(canEdit == true)
		descHtmlToMarkdown();
	mylog.log("after");
	mylog.log("inintDescs", $("#descriptionAbout").html());
	var descHtml = "<i>"+trad.notSpecified+"</i>";
	if($("#descriptionAbout").html().length > 0){
		descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
	}
	$("#descriptionAbout").html(descHtml);
	//$("#descProfilsocial").html(descHtml);
	mylog.log("descHtml", descHtml);
}

function hideAllSections(){
	$("#section-home").hide();
	$("#section-gallery").hide();
	$("#section-stream").hide().html("");
	$("#section-directory").hide();
	$("#section-directory-all").hide();
	$("#Grid").hide();
}

function initMenuDetail(){
	$("#btn-menu-home").click(function(){
		hideAllSections();
		$("#section-home").show();
	});

	$("#btn-menu-stream").click(function(){
		hideAllSections();
		$("#section-stream").show();
		loadMyStream(contextData.type, contextData.id);
	});
	$("#btn-menu-gallery").click(function(){
		hideAllSections();
		$("#section-gallery").show();
		var url = "gallery/index/type/"+contextData.type+"/id/"+contextData.id;
		ajaxPost('#section-gallery', baseUrl+'/'+moduleId+'/'+url+"?renderPartial=true",
			null,
			function(){

		},"html");
	});

	$("#btn-menu-directory-poi").click(function(){
		hideAllSections();
		$("#section-directory").show();
		var poisHtml = directory.showResultsDirectoryHtml(pois, "poi");
		$("#section-directory").html(poisHtml);
		coInterface.bindLBHLinks();

	});

	$("#btn-menu-directory-all").click(function(){
		hideAllSections();
		$("#section-directory-all").show();
		$("#Grid").show();
	});

	// $(".btn-edit-preview").click(function(){
	// 	dyFObj.editElement("poi", contextData.id, "video" );
	// });

}

function loadMyStream(typeElt, id){
	mylog.log("loadMyStream", typeElt, id);
	$('#journalTimeline').html("");
    var url = "news/co/index/type/"+typeElt+"/id/"+id;///date/"+dateLimit;
    setTimeout(function(){ //attend que le scroll retourn en haut (kscrollto)
        showLoader('#journalTimeline');
        ajaxPost('#section-stream', baseUrl+'/'+url, 
            {
                nbCol: 1,
                inline : true
            },
            function(){ 
               // loadLiveNow();
            },"html");
    }, 700);
}

function loadDataDirectory(){ 
	mylog.log("loadDataDirectory");
	//showLoader('#central-container');
	//var dataIcon = $(".load-data-directory[data-type-dir="+dataName+"]").data("icon");
	//history.pushState(null, "New Title", hashUrlPage+".view.directory.dir."+dataName);
	// $('#central-container').html("<center><i class='fa fa-spin fa-refresh margin-top-50 fa-2x'></i></center>");return;
	getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
				'/id/'+contextData.id+'/dataName/members?tpl=json',
				function(data){ 
					mylog.log("loadDataDirectory HEHREH", data);
					var strMembers = "";
					var strAdmin = "";

					$.each(data,function(kElt, vElt){

					});
				}
	,"html");
}
</script>
