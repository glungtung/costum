<?php
	// HtmlHelper::registerCssAndScriptsFiles(
	// 	array(  '/css/onepage.css',
	// 			'/css/news/index.css',
	// 			'/css/timeline2.css',
	// 		  ) ,
	// Yii::app()->theme->baseUrl. '/assets');

	// $cssAnsScriptFilesModule = array(
	// 	'/js/news/index.js',
	// 	
	// 	'/js/news/newsHtml.js',
	// );
	// HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles( 
	array( 
		'/vendor/colorpicker/js/colorpicker.js',
		'/vendor/colorpicker/css/colorpicker.css',
		'/css/default/directory.css',	
		'/css/profilSocial.css',
		'/css/calendar.css',
	), Yii::app()->theme->baseUrl. '/assets'
);

$cssAnsScriptFilesModule = array(
	'/js/default/calendar.js',
    '/js/default/profilSocial.js',
    '/js/default/editInPlace.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

$isGroup = false; 
if(!empty($element["categoryNA"]) && in_array("group", $element["categoryNA"])){
	$isGroup = true;
}
?>

<style>
.create-survey{
	display : none;
}
.filter-folder-gallery .titleAlbum, .filter-albums-gallery .titleAlbum {
    color: white !important;
    font-size: 22px;
    font-weight: bold;
}

#listMembers h4 {
    text-transform: none !important;
}
	.acceptBtn{
		border-radius:3px !important;
		color: white;
		background-color: #71CE4E;
		padding: 5px 10px;
		margin-top: 5px;
	}
	.acceptBtn:hover{
		color: #71CE4E !important;
		background-color: white;
		border: 1px solid #71CE4E;
	}
	.acceptBtn i{
		font-size:12px;
	}
	.refuseBtn{
		border-radius:3px !important;
		color: white;
		background-color: #E33551;
		padding: 5px 10px;
		margin-top: 5px; 

	}
	.refuseBtn:hover{
		color: #E33551 !important;
		background-color: white;
		border: 1px solid #E33551;
	}
	.refuseBtn i{
		font-size:12px;
	}
	.waitAnswer{
		position:absolute;
		left:38px;
	}
	.col-members{
		background-color: #fff !important;
	   /* min-height: 100%;
	    position: absolute;
	    right: 0px;*/
	    -webkit-box-shadow: 0px 5px 5px -2px #656565 !important;
	    -o-box-shadow: 0px 5px 5px -2px #656565 !important;
	    /* box-shadow: 0px -5px 5px -2px #656565 !important; */
	    filter: progid:DXImageTransform.Microsoft.Shadow(color=#656565, Direction=NaN, Strength=5) !important;
	}
	.img-header{
		max-height: 350px;
		width:100%;
		overflow: hidden;
		/*background-image: url("<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/tropic.jpg");
		background-size: 100%;*/
	}
	.main-col-search{
		padding:0px;
	}
	.mix{
		min-height: 100px;
		/*width: 31.5%;*/
		background-color: white;
		display: inline-block;
		border:1px solid #bbb;
		margin-right : 1.5%;
		border-radius: 10px;
		padding:1%;
		margin:-1px;
		-webkit-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		-moz-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
	}
	#grid .followers{
	display: none;
}
	.mix a{
		color:black;
		/*font-weight: bold;*/
	}
	.mix .imgDiv{
		float:left;
		width:30%;
		background: ;
		margin-top:0px;
	}
	.mix .detailDiv{
		float:right;
		width:70%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}

	.mix .toolsDiv{
		float:right;
		width:20%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		color:white;
	}

	.mix .text-xss{ font-size: 11px; }

	#Grid{
		margin-top: 20px;
		background-color: transparent;
		padding: 15px;
		border-radius: 4px;
		/*border-right: 1px solid #474747;*/
		padding: 0px;
		width:100%;
	}
	#Grid .mix{
		margin-bottom: -1px !important;
	}
	#Grid .item_map_list{
		padding:10px 10px 10px 0px;
		margin-top:0px;
		text-decoration:none;
		background-color:white;
		border: 1px solid rgba(0, 0, 0, 0.08); /*rgba(93, 93, 93, 0.15);*/
	}
	#Grid .item_map_list .left-col .thumbnail-profil{
		width: 75px;
		height: 75px;
	}
	#Grid .ico-type-account i.fa{
		margin-left:11px !important;
	}
	#Grid .thumbnail-profil{
		margin-left:10px;
	}
	#Grid .detailDiv a.text-xss{
		font-size: 12px;
		font-weight: 300;
	}

	.label.address.text-dark{
		padding:0.4em 0.1em 0.4em 0em !important;
	}
	.detailDiv a.thumb-info.item_map_list_panel{
		font-weight:500 !important;
	}

	.shadow {
	    -webkit-box-shadow: none;
	    -moz-box-shadow: none;
	    box-shadow: none;
	}
	#description .container{
		width: 60%;
		margin-left: 20%;
		font-size: 15px;
	}
	.section-title{
		text-transform: uppercase;
		font-weight: 700;
		color: rgb(92,75,62) !important;
	}

	#description .btn-edit-section{
		display: none;

	}

	.col-members h3{
		text-transform: uppercase;
		color:rgb(92,75,62);
		font-size: 0.7em;
		font-weight: 700;
	}
	.col-members h4{
		color:rgb(92,75,62);
		font-size: 0.9em;
		font-weight: 700;

	}
	.col-members .username-min{
		font-weight: 700;
		color:grey;
	}

	.elipsis{
		display: block;
	}

	.element-name{
		font-size:18px;
		padding:10px 20px;
		font-weight: 700;
		height:50px;
		margin-top:0px;
		background-color: rgba(255, 255, 255, 0.8);
	}
	.btn-follow{
		font-weight: 700;
		font-size:13px;
		border-radius:40px;
		border:none;
	}
	.menubar{
		-webkit-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 5px 5px -5px rgba(50, 50, 50, 0.75);
		margin-bottom: 40px;
	}
	.btn-menubar{
		font-weight: 700;
		font-size: 12px;
		border-radius: 40px;
		border: none;
		background-color: white;
		padding: 13px 20px;
	}

	.btn-menubar:hover{
		background-color: #4a4a4a;
		color:white;
		-webkit-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
	}
	iframe.fullScreen {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    top: 0;
	    left: 0;
	}
	.contentEntity{
		padding: 0px !important;
		margin: 0px !important;
		border-top: solid rgba(128, 128, 128, 0.2) 1px;
		margin-left: 0% !important;
		width: 100%;
		box-shadow: 0px 0px 5px -1px #d3d3d3;
	}
	.contentEntity:hover {
   	 background-color: rgba(211, 211, 211, 0.2);
	}
	.container-img-parent {
	    display: block;
	    width: 100%;
	    max-width: 100%;
	    /*min-height: 90px;*/
	    max-height: 90px;
	    overflow: hidden;
	    background-color: #d3d3d3;
	    text-align: center;
    }
    .container-img-parent i.fa {
	    margin-top: 20px;
	    font-size: 50px;
	    color: rgba(255, 255,255, 0.8);
	}

	.fileupload, .fileupload-preview.thumbnail, .fileupload-new .thumbnail,
	.fileupload-new .thumbnail img, .fileupload-preview.thumbnail img {
	    width: auto !important;
	}
	.user-image{
		background-color: white;
	}
	#fileuploadContainer{
		margin:-1px!important;
	}
	#fileuploadContainer .thumbnail{
		border-radius: 0px!important
	}
	#profil_imgPreview{}
	.removeLink{
		    display: none;
    position: absolute;
    right: -5px;
    top: -5px;
    border-radius: 12px;
    width: 25px;
    background-color: black;
    height: 25px;
    color: white;
    border: inherit;
	}
	.removeLink:hover{
		font-size: 15px;
		border: 1px solid white;
	}
	.contentItem{
		float:left;
		position: relative;
	}
</style>

	<?php
		$classOnePage = "col-xs-12 no-padding" ;
		if($type==Organization::COLLECTION && $isGroup === true) 
			$classOnePage = "col-lg-10 col-md-10 col-sm-9 col-xs-12 no-padding" ;
	?>
	<div class="<?php echo $classOnePage ; ?> " id="onepage">
		<div class="img-header text-center">
			<?php
				if(@$element["profilMediumImageUrl"] && !empty($element["profilMediumImageUrl"]))
						 $images=array(
						 	"medium"=>$element["profilMediumImageUrl"],
						 	"large"=>$element["profilImageUrl"]
						 );
					else $images="";	
					
					echo $this->renderPartial('../pod/fileupload', 
									array("itemId" => $id,
										  "itemName" => $element["name"],
										  "type" => $type,
										  "resize" => false,
										  "contentId" => Document::IMG_PROFIL,
										  "show" => true,
										  "edit" => $canEdit,
										  "element" => $element,
										  "image" => $images,
										  "openEdition" => $openEdition) );
			?>
		</div>
		<div class="element-name text-dark">
			<?php 
			echo @$element["name"];
			if(!empty(Yii::app()->session["userId"])) {
				if (!@$deletePending) {
					if(  ( Authorisation::canEditItem( Yii::app()->session["userId"], $type, (string)$element["_id"]) || Yii::app()->session["userId"] == @$element["creator"] ) && $type == Organization::COLLECTION ){ ?>
						<a href='javascript:;' class="btn btn-edit-element" data-id="<?php echo (string)$element["_id"] ?>" data-type="<?php echo $type ?>"><i class="fa fa-pencil"></i> Editer</a>
					<?php
					} else if(  ( Authorisation::canEditItem( Yii::app()->session["userId"], $type, (string)$element["_id"]) || Yii::app()->session["userId"] == @$element["creator"] ) && $type == Person::COLLECTION  ){ ?>
						<a href='javascript:;' id="btn-edit-user" class="btn " data-id="<?php echo (string)$element["_id"] ?>" data-type="<?php echo $type ?>"><i class="fa fa-pencil"></i> Editer</a>
						<?php
						if ((string)$_GET["id"]==Yii::app()->session["userId"]){ ?>
						<a href='javascript:' id="settingsAccount" class='btn btn-default text-red pull-right'>
							<i class='fa fa-cogs'></i> <?php echo Yii::t("common","Settings"); ?>
						</a>
					<?php }
					}

					if ($type == Organization::COLLECTION || $type == Project::COLLECTION ) {
						//if (Authorisation::canDeleteElement((String)$element["_id"], $type, Yii::app()->session["userId"])) {
						
					  	if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){ ?>
					  	<a href="javascript:;" data-toggle="modal" data-target="#modal-delete-element" class="btn text-red pull-right"><i class="fa fa-trash" ></i> <?php echo Yii::t("common","Delete")?></a> 
					<?php }
						if(!empty($invitedMe)){
							echo '<div class="pull-right" >';
							$this->renderPartial('costum.views.custom.notragora.answerInvite', 
			    			array(  "invitedMe"      => $invitedMe,
			    					"elementId"   => (string)$element["_id"],
			    					"elementType" => $type
			    					) 
			    			); 
			    			echo '</div>';
						}
					}
				} else {
					echo " (Suppression en cours)";
				}
			}
			?>
			<?php if(!empty(Yii::app()->session["userId"]) && $type==Organization::COLLECTION){ ?>
				<div class="linkBtn pull-right">
					<a id="follows" href="#" data-isco="false" data-id="<?php echo (String)$element["_id"] ; ?>" class="btn text-red tooltips pull-right">S'inscrire</a>
				</div>
	        <?php } ?>
		</div>
		<div class="col-md-12 padding-15 menubar">
			<button class="btn btn-default btn-menubar" id="btn-menu-home">A PROPOS</button>
			<?php
			if($type==Person::COLLECTION || ($type==Organization::COLLECTION && $isGroup === true)){ ?>
				<button class="btn btn-default btn-menubar" id="btn-menu-stream">CARNET DE BORD</button>
			<?php } ?>
			<?php
			if($type==Organization::COLLECTION){ 

				if($isGroup === true){
				?>
					<button class="btn btn-default btn-menubar" id="btn-menu-directory-members">MEMBRES</button>
				<?php } ?>
				<button class="btn btn-default btn-menubar" id="btn-menu-gallery">GALERIE PHOTOS</button>
				<button class="btn btn-default btn-menubar" id="btn-menu-directory-poi">PRODUCTIONS</button>
				
			<?php } 

			if($type==Person::COLLECTION){ ?>
				<button class="btn btn-default btn-menubar" id="btn-menu-directory-groups">GROUPE</button>
			<?php } ?>

			
			<?php 
			if(	$type==Organization::COLLECTION &&
				$isGroup === true &&
				isset(Yii::app()->session["userId"]) && 
				Authorisation::canEditItem(Yii::app()->session['userId'], $type, (String)$element["_id"]) ){ ?>

				<button id="addProduction" class='btn btn-default pull-right btn-menubar'>
					<i class='fa fa-plus'></i> <i class='fa fa-video-camera'></i> Ajouter une production
				</button>
			<?php } ?>

			<?php 
			if(	$type==Person::COLLECTION &&
				Yii::app()->session["userId"] == (String)$element["_id"] ){ ?>
				<button id="addGroup" class='btn btn-default pull-right btn-menubar'>
					<i class='fa fa-plus'></i> <i class='fa fa-users'></i> Ajouter un groupe
				</button>
			<?php } ?>
		</div>
		<div class="col-md-12 no-padding">
			<div id="section-home" class="col-md-12">
				<div class="col-xs-12 text-center">
				    <h2 class="section-title">
				        <span class="sec-title">Présentation</span><br>
				        <i class="fa fa-angle-down"></i>
				    </h2>
				</div>
				<div class="col-xs-8 col-xs-offset-2  no-padding item-desc" id="descriptionAbout"><?php echo !empty($element['description']) ? @$element['description'] : "<span class='padding-10'><center><i>- Pas de présentation -</center></i></span>"; ?></div>
				<div class="col-xs-8 col-xs-offset-2  no-padding item-desc hidden" id="descriptionMarkdown"><?php echo !empty($element['description']) ? @$element['description'] : ""; ?></div>
				
			</div>
			<div id="section-stream" class="col-md-12"></div>
			<div id="section-gallery" class="col-md-12"></div>
			<div id="central-container" class="col-md-12"></div>
		</div>
	</div>
	<?php
	if($type==Organization::COLLECTION && $isGroup === true){ ?>
		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 col-members no-padding">
			<div id="listMembers" class="padding-5"></div>
			<?php
			if(@Yii::app()->session["userId"] && $type != Person::COLLECTION && Authorisation::canEditItem(Yii::app()->session['userId'], $type, (String)$element["_id"])){ 

				?>
			<div class="col-md-12 no-padding margin-top-5">
				<hr>
				<button id="inviteMembers"
						class="btn btn-default btn-menubar btn-menu-element btn-menu-element-addmembers tooltips col-xs-12"
						data-toggle='modal'
						data-placement="bottom"
						data-original-title="Ajouter des membres à ce groupe de travail"
						data-target='#modal-scope' >
					<i class="fa fa-send"></i> Inviter des membres
				</button>
				</div>
			<?php
  				//$this->renderPartial('../element/addMembersFromMyContacts',array("type"=>$type, "parentId" =>(string)$element['_id'], "users"=>@$members));
				}
			
		?>
		</div>
	<?php } ?>

<?php 
// if (Authorisation::canDeleteElement((String)$element["_id"], $type, Yii::app()->session["userId"]) && !@$deletePending) $this->renderPartial('../element/confirmDeleteModal');

// if (@$deletePending && (Authorisation::isElementAdmin((String)$element["_id"], $type, Yii::app()->session["userId"]) || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) $this->renderPartial('../element/confirmDeletePendingModal'); 

?>

<script type="text/javascript">
	var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>;
	var canEdit =  <?php echo json_encode($canEdit) ?>;
	var isGroup =  <?php echo json_encode($isGroup) ?>;
	var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
							"#@"+contextData.slug : 
							"#page.type."+contextData.type+".id."+contextData.id);
	var openEdition = false ;
	var connectTypeElement="<?php echo Element::$connectTypes[$type] ?>";
	if(location.hash.indexOf("#page")>=0){
		strHash="";
		if(location.hash.indexOf(".view")>0){
			hashPage=location.hash.split(".view");
			strHash=".view"+hashPage[1];
		}
		replaceSlug=true;
		history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", hashUrlPage+strHash);

	}

	
	//var peopleReference = false;
  	//var mentionsContact = [];
  	var nbMember = 0;
  	var nbAdmin = 0;
  	var nbMemberPending = 0;
  	var nbPending = 0;

  	if(contextData.type=="organizations"){
		nbMember = "<?php echo @$nbMember; ?>";
		nbAdmin = "<?php echo @$nbAdmin; ?>";
		nbPending= "<?php echo @$nbMemberPending+@$nbAdminPending ?>";
  	}

  	// <?php
  	// $entitiesPois = PHDB::find( Poi::COLLECTION, array("parentId"=>(String) $element["_id"],"parentType"=>$type)); ?>

  	//var pois = <?php //echo json_encode($entitiesPois); ?> ;
  	var viewLoading ="<?php echo (@$_GET["view"]) ? $_GET["view"] : "detail" ?>" ;
  	function getMembersList(name, slug, img, standby){
  		if(typeof img == "undefined" || img == null || img == "")
  			img = modules.co2.url + "/images/thumb/default.png";
  		var str = '<a href="#@'+slug+'" class="lbh col-md-12 no-padding margin-top-5 elipsis" style="color: rgb(198,198,198);">'+
				'<img class="img-circle" src="'+img+'" height="35" width="35"> '+
				'<span class="username-min">'+name+'</span>';

		if(typeof standby != "undefined" && standby === true)
			str += '<span style="font-style: italic;font-size: 10px;position: absolute;bottom: 0px;left: 38px;">En attente d\'inscription</span>';
		str +='</a>';
  		return str;
  	}

	jQuery(document).ready(function() {
		if($("#modalDirectoryForm").length > 0)
			$("#modalDirectoryForm").html("");
		if( typeof userId != "undefined" &&
			userId != null &&
			userId != "" &&
			typeof contextData != "undefined" && 
			typeof contextData.links != "undefined" && 
			typeof contextData.links.members != "undefined" && 
			typeof contextData.links.members[userId] != "undefined" ) {
			$('#follows').html("<i class='fa fa-unlink disconnectBtnIcon'></i> Quitter");
			$('#follows').removeClass("text-green").addClass("text-red");
			$('#follows').data("isco", true);
		} else {
			$('#follows').html("<i class='fa fa-link disconnectBtnIcon'></i> S'inscrire");
			$('#follows').removeClass("text-red").addClass("text-green");
			$('#follows').data("isco", false);
		}

		if(typeof contextData.links != "undefined" && typeof contextData.links.members != "undefined"){
			var members = contextData.links.members ;
			getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
					'/id/'+contextData.id+'/dataName/members/isInviting/true?tpl=json',
						function(data){ 
							//mylog.log("HERE data", data);
							var strAdmin = "";
							var strMembers = "";
							var strAdminPending = "";
							var strMembersPending = "";
							var nbAdmin = 0 ;
							var nbAdminPending = 0 ;
							var nbMembers = 0 ;
							var nbMembersPending = 0 ;

							$.each(data, function(keyM, valM){
								// mylog.log("HERE each ", members[keyM], ( typeof members[keyM]["isAdmin"] == "undefined" ||
								// 		members[keyM]["isAdmin"] === false ));
								if( typeof members[keyM] != "undefined" &&
									typeof members[keyM]["isAdmin"] != "undefined" && 
									members[keyM]["isAdmin"] === true ) {
									if(typeof members[keyM]["isAdminPending"] == "undefined"){
										strAdmin += getMembersList(valM.name, valM.slug, valM.profilThumbImageUrl, false) ;
									}
									else{
										strAdmin += getMembersList(valM.name, valM.slug, valM.profilThumbImageUrl, true) ;
										//nbAdminPending++;
									}
									nbAdmin++;
								}

								if( typeof members[keyM] != "undefined" &&
									( typeof members[keyM]["isAdmin"] == "undefined" ||
										members[keyM]["isAdmin"] === false ) ) {
									if(typeof members[keyM]["toBeValidated"] == "undefined"){
										strMembers += getMembersList(valM.name, valM.slug, valM.profilThumbImageUrl, false) ;
									}
									else{
										strMembers += getMembersList(valM.name, valM.slug, valM.profilThumbImageUrl, true) ;
										//nbMembersPending++;
									}
									nbMembers++;
								}
							});
							if(nbAdmin == 0)
								strAdmin = '<span style="font-style: italic;">Pas d\'admin sur ce groupe de travail</span>';
							if(nbMembers == 0)
								strMembers = '<span style="font-style: italic;">Pas de membre sur ce groupe de travail</span>';


							var str = '<h4 style="font-size: 14px; text-align: center;">Membres du groupe (<span id="nbMemberTotal">'+(nbAdmin+nbMembers)+'</span>)</h4>' +
								'<div class="col-md-12 no-padding margin-top-5">'+
									'<hr>'+
									'<h4>Administrateurs (<span id="nbAdmin">'+nbAdmin+'</span>)</h4>'+
									strAdmin+
								'</div>'+
								'<div class="col-md-12 no-padding margin-top-5">'+
									'<hr>'+
									'<h4>Membres (<span id="nbMember">'+nbMembers+'</span>)</h4>'+
									strMembers+
								'</div>';
							// mylog.log("HERE data", strAdmin);
							$("#listMembers").html(str);

							mylog.log("col-members 2", $(".social-main-container").height(), $(window).height());
							$(".col-members").css( "height", $(".social-main-container").height() );

							setTimeout(function(){ 
								$(".col-members").css( "height", $(".social-main-container").height() );
							}, 5000);
						}
			,"html");
		}
		
		if(canEdit == true)
			descHtmlToMarkdown();

		var descHtml = "<span class='padding-10'><center><i>- Pas de présentation -</center></i></span>";
		if($("#descriptionAbout").html().length > 0){
			descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
		}
		$("#descriptionAbout").html(descHtml);
		directory.bindBtnElement();
		
		$("#inviteMembers").off().on("click", function(){
			smallMenu.openAjaxHTML(baseUrl+'/co2/element/invite/type/'+contextData.type+'/id/'+contextData.id);
		});
		$(".editLink").click(function(){
			$(this).parents().eq(1).find(".removeLink").show();
		});
		/*$(".removeLink").click(function(){
			var connect=$(this).data("type");
			var id=$(this).data("id");
			ajaxPost(
				null,
				baseUrl+"/"+moduleId+"/poi/deletelink",
				{connect:connect, id:id, parentId:contextData.id },
				function(data){ 
					if ( data && data.result ) {
				    	if(connect=="supports")
				    		msg="Bien supprimé des soutiens";
				    	else
				    		msg="Bien supprimé des producteurs";
				        toastr.info(msg);
				        $(".contentItem"+id).remove();
				        //loadByHash("#"+parentType.substr(0, parentType.length - 1)+".detail.id."+parentId);
				    } else {
				           toastr.error("Une erreur est survenue : ".data.msg);
				    }
				}
			);
		}); */
		
		if(viewLoading=="directory"){
			hideAllSections();
			$("#section-directory-all").show();
			$("#Grid").show();
		}else{
			hideAllSections();
			$("#section-home").show();
		}
		
		setTitle("<span id='main-title-menu'>"+contextData.name+"</span>",contextData.name, contextData.name);
		$("#settingsAccount").click(function () {
  			mylog.log("settingsAccount");
  	 		//loadByHash('#person.changepassword.id.'+userId+'.mode.initSV', false);

  	 		//pageProfil.actions.settingsAccount();
  	 		hideAllSections();
    		$("#central-container").show();
  	 		pageProfil.views.settings();
  	  	});

		$(".btn-full-desc").click(function(){
            var sectionKey = $(this).data("sectionkey");
            if($("section#"+sectionKey+" .item-desc").hasClass("fullheight")){
                $("section#"+sectionKey+" .item-desc").removeClass("fullheight");
                $(this).html("<i class='fa fa-plus-circle'></i>");
            }else{
                $("section#"+sectionKey+" .item-desc").addClass("fullheight");
                $(this).html("<i class='fa fa-minus-circle'></i>");
            }
        });
		$(".tooltips").tooltip();
		$("#nbAdmin").html(nbAdmin);
		$("#nbMember").html(nbMember);
		$("#nbPending").html(nbPending);
		$("#nbMemberTotal").html(parseInt(nbAdmin)+parseInt(nbMember));

		/*var url = "news/index/type/"+contextData.type+"/id/"+contextData.id+"?isFirst=1&";
		if(contextData.type=="projects" || contextData.type=="citoyens"){
			ajaxPost('#timeline-page', baseUrl+'/'+moduleId+'/'+url+"renderPartial=true&tpl=co2&nbCol=2",
				null,
				function(){

			},"html");
		}*/
		if(contextData.type=="poi"){
			getAjax('#comment-page',baseUrl+'/'+moduleId+"/comment/index/type/"+contextData.type+"/id/"+contextData.id,function(){

			},"html");
		}

		$('#btn-edit-user').off().click(function(){
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update general information"],
						icon : "fa-key",
						type: "object",
						onLoads : {
							initUpdateInfo : function(){
								mylog.log("initUpdateInfo");
								$(".emailOptionneltext").slideToggle();
								$("#ajax-modal .modal-header").removeClass("bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");

								dataHelper.activateMarkdown("#ajaxFormModal #description");
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave");
							removeFieldUpdateDynForm(contextData.type);
					    },
						afterSave : function(data){
							mylog.dir(data);
							// if(data.result&& data.resultGoods && data.resultGoods.result){

							// 	if(typeof data.resultGoods.values.name != "undefined"){
							// 		contextData.name = data.resultGoods.values.name;
							// 		$(".element-name").html(contextData.name);
							// 	}
							// }
							dyFObj.closeForm();
							urlCtrl.loadByHash( location.hash );
							//changeHiddenFields();
						},
						properties : {
							block : dyFInputs.inputHidden(),
							name : dyFInputs.name(contextData.type),
							similarLink : dyFInputs.similarLink,
							typeElement : dyFInputs.inputHidden(),
							email : dyFInputs.email(tradDynForm.mainemail, tradDynForm.mainemail, { email: true, required : true }),
							description : dyFInputs.textarea(tradDynForm["longDescription"], "..."),
							isUpdate : dyFInputs.inputHidden(true)
						}
					}
				}
			};
			
			var dataUpdate = {
				block : "info",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name
			};

			// if(contextData.type == typeObj.person.col ){
			// 	if(notNull(contextData.username) && contextData.username.length > 0)
			// 		dataUpdate.username = contextData.username;
			// 	if(notEmpty(contextData.birthDate))
			// 		dataUpdate.birthDate = moment(contextData.birthDate).local().format("DD/MM/YYYY");
			// }
			
			//mylog.log("ORGA ", contextData.type, typeObj.organization.col, dataUpdate.type);
			
			

			if($.inArray(contextData.type, [typeObj.organization.col, typeObj.person.col, typeObj.project.col, typeObj.event.col]) > -1 ){
				//mylog.log("test email", contextData, contextData.email);
				if(notEmpty(contextData.email)) {
					//mylog.log("test email2", contextData, contextData.email);
					dataUpdate.email = contextData.email;
				}
			}

			if(notEmpty(contextData.description)) {
				//mylog.log("test description", contextData, contextData.description);
				//dataUpdate.description = dataHelper.htmlToMarkdown($(".contentInformation #descriptionAbout").html());
				dataUpdate.description = contextData.description;
			}
			
			//mylog.log("dataUpdate", dataUpdate);
			dyFObj.openForm(form, "initUpdateInfo", dataUpdate);
		});

		$('#follows').click(function(){
			var id = $(this).data("id");
			var isco = $(this).data("isco");
			if(isco == false){
				links.connectAjax(contextData.type,contextData.id,userId,'citoyens','members', null, function(){
					$('#follows').html("Désinscrire");
					$('#follows').data("isco", true);
				});
			}else{
				links.disconnectAjax(contextData.type,contextData.id ,userId,'citoyens','members', null, function(){
					$('#follows').html("S'inscrire");
					$('#follows').data("isco", false);
				});
			}
		});

		$(".deleteThisBtn").off().on("click",function ()
		{
			mylog.log("deleteThisBtn click");
	        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	        var btnClick = $(this);
	        var id = $(this).data("id");
	        var type = $(this).data("type");
	        var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;

	        bootbox.confirm("Etes vous sur de vouloir supprimer cet élément ?",
        	function(result)
        	{
				if (!result) {
					btnClick.empty().html('<i class="fa fa-trash"></i>');
					return;
				} else {
					ajaxPost(
						null,
						urlToSend,
						{},
						function(data){ 
							if ( data && data.result ) {
					        	toastr.info("Cet élément a été effacé avec succès.");
					        	$("#"+type+id).remove();
					        	loadByHash("#"+parentType.substr(0, parentType.length - 1)+".detail.id."+parentId);
					        } else {
					           toastr.error("Une erreur est survenue : ".data.msg);
					        }
						}
					);
				}
			});

		});
		$(".editThisBtn").off().on("click",function (){
	        $(this).empty(t).html('<i class="fa fa-spinner fa-spin"></i>');
	        var btnClick = $(this);
	        var id = $(this).data("id");
	        var type = $(this).data("type");
	        elementLib.editElement(type,id);
		});

		$("#addProduction").off().on("click",function (){
	        dyFObj.openForm("poi");
		});

		$("#addGroup").off().on("click",function (){
	        dyFObj.openForm("organization");
		});
		initMenuDetail();
		mylog.log("col-members ", $(".social-main-container").height(), $(window).height());
		$(".col-members").css( "height", $(".social-main-container").height() );
	});

function requestFullScreen(element) {
    // Supports most browsers and their versions.
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

function makeFullScreen() {
    document.getElementsByTagName("iframe")[0].className = "fullScreen";
    var elem = document.body;
    requestFullScreen(elem);
}

function initMenuDetail(){
	$("#btn-menu-home").click(function(){
    	hideAllSections();
    	$("#section-home").show();
    });

    $("#btn-menu-stream").click(function(){
    	hideAllSections();
    	$("#section-stream").show();
		showLoader("#section-stream");
		setTimeout(function(){ //attend que le scroll retourn en haut (coInterface.scrollTo)
				ajaxPost("#section-stream", baseUrl+"/news/co/index/type/"+contextData.type+"/id/"+contextData.id, 
					{nbCol:1},
					function(){

						$(".scopeShare[data-value='public']").trigger( "click" );
						$("#scopeListContainerForm").remove()

					},"html");
			}, 700);
    });

	$("#btn-menu-gallery").click(function(){
    	hideAllSections();
    	$("#section-gallery").show();
		showLoader("#section-gallery");
		var url = "gallery/index/type/"+contextData.type+"/id/"+contextData.id+"/docType/image";
		// if(notNull(pageProfil.params.dir))
		// 	url+="/docType/image"+pageProfil.params.dir;

		// if(notNull(pageProfil.params.key))
		// 	url+="/contentKey/"+pageProfil.params.key;

		// if(notNull(pageProfil.params.folderId))
		// 	url+="/folderId/"+pageProfil.params.folderId;
		ajaxPost("#section-gallery", baseUrl+'/'+moduleId+'/'+url, 
			null,
				function(){
					$("#folderbanner").hide();
					$("#breadcrumGallery > a:nth-child(1)").hide();
					$("#breadcrumGallery > i:nth-child(2)").hide();

					folder.costum.endOfExecution = function(){
						$("#folderbanner").hide();
						$("#breadcrumGallery > a:nth-child(1)").hide();
						$("#breadcrumGallery > i:nth-child(2)").hide();
					}

					

				},"html");
    });

    $("#btn-menu-directory-poi").click(function(){
    	getPoiNA();
    });

    $("#btn-menu-directory-members").click(function(){
    	hideAllSections();
    	$("#central-container").show();
    	showLoader("#central-container");
		getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
					'/id/'+contextData.id+'/dataName/members/isInviting/true?tpl=json',
					function(data){ 
						var type = null;
						//mylog.log("pageProfil.views.directory canEdit" , canEdit);
						if(typeof canEdit != "undefined" && canEdit)
							canEdit="members";

						var community=(typeof contextData!="undefined" && contextData!=null) ? contextData : null;

						if(community!=null){
							community.connectType="members";
						}
						
						//mylog.log("pageProfil.views.directory edit" , canEdit);
						$("#central-container").html( directory.showResultsDirectoryHtml(data, type, null, community) );

						const arr = document.querySelectorAll('img.lzy_img')
						arr.forEach((v) => {
							v.dom = "#content-results-profil";
							imageObserver.observe(v);
						});
						
						//displayInTheContainer(data, "members", "user", type, canEdit);
						//$("#menuCommunity .load-coummunity[data-type-dir='followers']").hide()
						if(typeof mapCO != "undefined"){
							mapCO.clearMap();
				            mapCO.addElts(data);
				            mapCO.getMap().invalidateSize();
						}
						directory.bindBtnElement();
						//initBtnAdmin();
						coInterface.bindButtonOpenForm();
					}
		,"html");
    });

    $("#btn-menu-directory-groups").click(function(){
    	hideAllSections();
    	$("#central-container").show();
    	showLoader("#central-container");
		getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
					'/id/'+contextData.id+'/dataName/organizations?tpl=json',
					function(data){ 
						var type = null;
						//mylog.log("pageProfil.views.directory canEdit" , canEdit);
						if(typeof canEdit != "undefined" && canEdit)
							canEdit="organizations";
						//mylog.log("pageProfil.views.directory edit" , canEdit);
						$("#central-container").html( directory.showResultsDirectoryHtml(data, type, null, canEdit) );

						const arr = document.querySelectorAll('img.lzy_img')
						arr.forEach((v) => {
							v.dom = "#content-results-profil";
							imageObserver.observe(v);
						});
						
						//displayInTheContainer(data, "members", "user", type, canEdit);
						//$("#menuCommunity .load-coummunity[data-type-dir='followers']").hide()
						if(typeof mapCO != "undefined"){
							mapCO.clearMap();
				            mapCO.addElts(data);
				            mapCO.getMap().invalidateSize();
						}
						directory.bindBtnElement();
						//initBtnAdmin();
						coInterface.bindButtonOpenForm();
					}
		,"html");
    });

}

function getPoiNA(){
	hideAllSections();
	$("#central-container").show();
	showLoader("#central-container");
	if(isGroup == true){
		getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+contextData.type+
					'/id/'+contextData.id+'/dataName/poi?tpl=json',
					function(data){ 
						var type = "poi";
						// mylog.log("pageProfil.views.directory canEdit" , canEdit);
						if(typeof canEdit != "undefined" && canEdit)
							canEdit="poi";
						// mylog.log("pageProfil.views.directory edit" , canEdit);
						$("#central-container").html( directory.showResultsDirectoryHtml(data, type, null, canEdit) );

						const arr = document.querySelectorAll('img.lzy_img')
						arr.forEach((v) => {
							v.dom = "#content-results-profil";
							imageObserver.observe(v);
						});

					//displayInTheContainer(data, "poi", "videos", type, canEdit);
						if(typeof mapCO != "undefined"){
							mapCO.clearMap();
				            mapCO.addElts(data);
				            mapCO.getMap().invalidateSize();
						}
					}	
		,"html");
	}else{
		getAjax('', baseUrl+'/costum/notragora/getproductionbypartner/id/'+contextData.id,
					function(data){ 
						var type = "poi";
						// mylog.log("pageProfil.views.directory canEdit" , canEdit);
						if(typeof canEdit != "undefined" && canEdit)
							canEdit="poi";
						// mylog.log("pageProfil.views.directory edit" , canEdit);
						$("#central-container").html( directory.showResultsDirectoryHtml(data, type, null, canEdit) );

						const arr = document.querySelectorAll('img.lzy_img')
						arr.forEach((v) => {
							v.dom = "#content-results-profil";
							imageObserver.observe(v);
						});

						//displayInTheContainer(data, "poi", "videos", type, canEdit);
						if(typeof mapCO != "undefined"){
							mapCO.clearMap();
				            mapCO.addElts(data);
				            mapCO.map.invalidateSize();
						}
						directory.bindBtnElement();
						//initBtnAdmin();
						coInterface.bindButtonOpenForm();
					}
		,"html");
	}
	
}
function hideAllSections(){
	$("#section-home").hide();
	$("#section-desc").hide();
	$("#section-gallery").hide();
	$("#section-stream").hide().html("");
	$("#central-container").hide();
	$("#section-directory-all").hide();
	$("#Grid").hide();
}
</script>
