<?php
HtmlHelper::registerCssAndScriptsFiles( 
	array( 
		'/vendor/colorpicker/js/colorpicker.js',
		'/vendor/colorpicker/css/colorpicker.css',
		'/css/default/directory.css',	
		'/css/profilSocial.css',
		'/css/calendar.css',
	), Yii::app()->theme->baseUrl. '/assets'
);

$cssAnsScriptFilesModule = array(
	'/js/default/calendar.js',
    '/js/default/profilSocial.js',
    '/js/default/editInPlace.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

$cssAnsScriptFilesTheme = array(
	"/plugins/jquery-cropbox/jquery.cropbox.css",
	"/plugins/jquery-cropbox/jquery.cropbox.js",
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	// slick carouse
	'/plugins/slick/slick.css',
	'/plugins/slick/slick-theme.css',
	//'/plugins/slick/jquery.min.js',
	'/plugins/slick/slick.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/jquery.qrcode/jquery-qrcode.min.js',
	'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
    '/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
    '/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
    "/plugins/d3/d3.js",
    "/plugins/d3/d3-flextree.js",
    "/plugins/d3/view.mindmap.js",
    "/plugins/d3/view.mindmap.css",
    
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style>
	#descriptionAbout p, #descriptionAbout .container span{
		font-size:14px;
		text-align: justify;
	}

	.morelink{
		color:#93C020;
		font-weight: 700;

	}

	.inactiveYear{
		opacity:0.4;
	}

	.container-img-card{
		/*width: 100%;*/
	    float: left;
	    /*min-height: 400px;
	    max-height: 400px;*/
	    /*min-height: 150px;*/
	    max-height: 250px;
	    background: white;
	    
	    /*z-index: -1;*/
	   
	    
	}
	.img-responsive{
	    /*z-index: inherit;*/
	    
	    max-height: inherit;
	   /* min-height: inherit;*/
	    margin:auto;
	}

	#yearFilter{
	background-color: white;
    position: fixed;
    /* padding: 2em; */
    bottom: 0;
    
    padding-left: 260px;
    z-index:1;
    right:0;

	}

	.social-main-container{
		background-color: white !important;
	}
	
/* CAROUSSEL STYLE*/
	
	.slick-prev:before, .slick-next:before{
		color:black;

	}
	.year .slick-slide{
		text-align: center;

	}

	.slick-list.draggable{
		height: unset !important;

	}

	.slick-slide:hover{
		cursor: pointer;

	}

	.slick-slide .inactiveYear:hover{
		cursor: default;

	}

	.slick-current{
		color:#93C020;
		font-size:x-large;
		/*width:auto !important;*/
	}

	.active-year{
		/*font-weight:700;*/
		font-size:x-large;
		opacity:1;
		/*width:auto !important;
		padding-left: 5px;
    	padding-right: 5px;*/
	}

	.sibling-year{
		/*font-weight:600;*/
		font-size:larger;
		/*width:auto !important;
		padding-left: 2px;
    	padding-right: 2px;*/
	}

	.city .slick-arrow{
		left: 50%;
	    transform: translate(-50%, 0) rotate(90deg);
	}    

	.city .slick-prev{
	    /* top: -30px; */
	    
	    top: -30px;
	} 

	.city .slick-next{
		top: unset;
	    bottom: -30px;
	} 

/* END CAROUSSEL STYLE*/

	#onepage {
		background-color: white;
		position:relative;
		border-right: 1px solid grey;
	}

	.acceptBtn{
		border-radius:3px !important;
		color: white;
		background-color: #71CE4E;
		padding: 5px 10px;
		margin-top: 5px;
	}
	.acceptBtn:hover{
		color: #71CE4E !important;
		background-color: white;
		border: 1px solid #71CE4E;
	}
	.acceptBtn i{
		font-size:12px;
	}
	.refuseBtn{
		border-radius:3px !important;
		color: white;
		background-color: #E33551;
		padding: 5px 10px;
		margin-top: 5px;

	}
	.refuseBtn:hover{
		color: #E33551 !important;
		background-color: white;
		border: 1px solid #E33551;
	}
	.refuseBtn i{
		font-size:12px;
	}
	.waitAnswer{
		position:absolute;
		left:38px;
	}
	.col-members{
		background-color: #fff !important;
	    min-height: 100%;
	    position: absolute;
	    right: 0px;
	    -webkit-box-shadow: 0px 5px 5px -2px #656565 !important;
	    -o-box-shadow: 0px 5px 5px -2px #656565 !important;
	    /* box-shadow: 0px -5px 5px -2px #656565 !important; */
	    filter: progid:DXImageTransform.Microsoft.Shadow(color=#656565, Direction=NaN, Strength=5) !important;
	}
	.img-header{
		max-height: 350px;
		width:100%;
		overflow: hidden;
		/*background-image: url("<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/tropic.jpg");
		background-size: 100%;*/
	}
	.main-col-search{
		padding:0px;
	}
	.mix{
		min-height: 100px;
		/*width: 31.5%;*/
		background-color: white;
		display: inline-block;
		border:1px solid #bbb;
		margin-right : 1.5%;
		border-radius: 10px;
		padding:1%;
		margin:-1px;
		-webkit-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		-moz-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
	}
	#grid .followers{
	display: none;
}
	.mix a{
		color:black;
		/*font-weight: bold;*/
	}
	.mix .imgDiv{
		float:left;
		width:30%;
		background: ;
		margin-top:0px;
	}
	.mix .detailDiv{
		float:right;
		width:70%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}

	.mix .toolsDiv{
		float:right;
		width:20%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		color:white;
	}

	.mix .text-xss{ font-size: 11px; }

	#Grid{
		margin-top: 20px;
		background-color: transparent;
		padding: 15px;
		border-radius: 4px;
		/*border-right: 1px solid #474747;*/
		padding: 0px;
		width:100%;
	}
	#Grid .mix{
		margin-bottom: -1px !important;
	}
	#Grid .item_map_list{
		padding:10px 10px 10px 0px;
		margin-top:0px;
		text-decoration:none;
		background-color:white;
		border: 1px solid rgba(0, 0, 0, 0.08); /*rgba(93, 93, 93, 0.15);*/
	}
	#Grid .item_map_list .left-col .thumbnail-profil{
		width: 75px;
		height: 75px;
	}
	#Grid .ico-type-account i.fa{
		margin-left:11px !important;
	}
	#Grid .thumbnail-profil{
		margin-left:10px;
	}
	#Grid .detailDiv a.text-xss{
		font-size: 12px;
		font-weight: 300;
	}

	.label.address.text-dark{
		padding:0.4em 0.1em 0.4em 0em !important;
	}
	.detailDiv a.thumb-info.item_map_list_panel{
		font-weight:500 !important;
	}

	.shadow {
	    -webkit-box-shadow: none;
	    -moz-box-shadow: none;
	    box-shadow: none;
	}
	#description .container{
		width: 60%;
		margin-left: 20%;
		font-size: 15px;
	}
	.section-title{
		text-transform: uppercase;
		font-weight: 700;
		color: rgb(92,75,62) !important;
	}

	#description .btn-edit-section{
		display: none;

	}

	.col-members h3{
		text-transform: uppercase;
		color:rgb(92,75,62);
		font-size: 0.7em;
		font-weight: 700;
	}
	.col-members h4{
		color:rgb(92,75,62);
		font-size: 0.9em;
		font-weight: 700;

	}
	.col-members .username-min{
		font-weight: 700;
		color:grey;
	}

	.elipsis{
		display: block;
	}

	.element-name{
		font-size:18px;
		padding:10px 20px;
		font-weight: 700;
		height:50px;
		margin-top:0px;
		background-color: rgba(255, 255, 255, 0.8);
	}
	.btn-follow{
		font-weight: 700;
		font-size:13px;
		border-radius:40px;
		border:none;
	}
	.menubar{
		-webkit-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 5px 5px -5px rgba(50, 50, 50, 0.75);
		margin-bottom: 40px;
	}
	.btn-menubar{
		font-weight: 700;
		font-size: 12px;
		border-radius: 40px;
		border: none;
		background-color: white;
		padding: 13px 20px;
	}

	.btn-menubar:hover{
		background-color: #4a4a4a;
		color:white;
		-webkit-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
	}
	iframe.fullScreen {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    top: 0;
	    left: 0;
	}
	.contentEntity{
		padding: 0px !important;
		margin: 0px !important;
		border-top: solid rgba(128, 128, 128, 0.2) 1px;
		margin-left: 0% !important;
		width: 100%;
		box-shadow: 0px 0px 5px -1px #d3d3d3;
	}
	.contentEntity:hover {
   	 background-color: rgba(211, 211, 211, 0.2);
	}
	.container-img-parent {
	    display: block;
	    width: 100%;
	    max-width: 100%;
	    /*min-height: 90px;*/
	    max-height: 90px;
	    overflow: hidden;
	    background-color: #d3d3d3;
	    text-align: center;
    }
    .container-img-parent i.fa {
	    margin-top: 20px;
	    font-size: 50px;
	    color: rgba(255, 255,255, 0.8);
	}

	.fileupload, .fileupload-preview.thumbnail, .fileupload-new .thumbnail,
	.fileupload-new .thumbnail img, .fileupload-preview.thumbnail img {
	    width: auto !important;
	}
	.user-image{
		background-color: white;
	}
	#fileuploadContainer{
		margin:-1px!important;
	}
	#fileuploadContainer .thumbnail{
		border-radius: 0px!important
	}
	#profil_imgPreview{}
	.removeLink{
		    display: none;
    position: absolute;
    right: -5px;
    top: -5px;
    border-radius: 12px;
    width: 25px;
    background-color: black;
    height: 25px;
    color: white;
    border: inherit;
	}
	.removeLink:hover{
		font-size: 15px;
		border: 1px solid white;
	}
	.contentItem{
		float:left;
		position: relative;
	}
	#divTags .tag {
	    background-color: #3A87AD !important;
	    border-color: #3A87AD !important;
	}
	#cityFilter{
		position:fixed;
		width: 10%;
		position: fixed;
		bottom: 100px;
		top: 250px;
		overflow-y: auto;
	}
</style> 

<?php
$img = isset($element["profilMediumImageUrl"]) ? $element["profilMediumImageUrl"] :  Yii::app()->getModule('costum')->assetsUrl."/images/templateCostum/no-banner.jpg";
//var_dump($element);
?>
<div class="col-xs-12 bg-white">
	<div class="col-xs-12 col-sm-10 no-padding" id="onepage" >
		<div class="col-sm-8 col-xs-12 text-dark no-padding">
			<h1 class="">
				<?php echo $element['name']; 
				if(!empty(Yii::app()->session["userId"])) {
				if (!@$deletePending) {
					if(  ( Authorisation::canEditItem( Yii::app()->session["userId"], $type, (string)$element["_id"]) || Yii::app()->session["userId"] == @$element["creator"] )){ ?>
						<a href='javascript:;' class="btn editElement" data-id="<?php echo (string)$element["_id"] ?>" data-type="<?php echo $type ?>" data-subtype="<?php echo $element['category'] ?>"><i class="fa fa-pencil"></i> Editer</a>
					<?php
					} 

					
						//if (Authorisation::canDeleteElement((String)$element["_id"], $type, Yii::app()->session["userId"])) {
						
					if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){ ?>
					  	<a href="javascript:;" data-toggle="modal" data-target="#modal-delete-element" class="btn text-red pull-right"><i class="fa fa-trash" ></i> <?php echo Yii::t("common","Delete")?></a> 
					<?php }
						if(!empty($invitedMe)){
							echo '<div class="pull-right" >';
							$this->renderPartial('costum.views.custom.notragora.answerInvite', 
			    			array(  "invitedMe"      => $invitedMe,
			    					"elementId"   => (string)$element["_id"],
			    					"elementType" => $type
			    					) 
			    			); 
			    			echo '</div>';
						}
					
				} else {
					echo " (Suppression en cours)";
				}
			}
			?>
			</h1>
			<div class="col-md-6 col-sm-6 col-xs-12 text-dark text-center">
				<!-- <h4>PRODUCTEURS</h4> -->
				<div id="producors"></div>
				<!-- 	<?php if(isset($element['producors'])){
					foreach($element['producors'] as $id => $el){
						$producor=Element::getElementByid($id,Organization::COLLECTION);	
						//var_dump($producor);			
					?>
					<a href="#page.type.organizations.id.<?php echo $id ?>" class="lbh tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo $producor['name'] ?>"><img src="<?php echo $producor['profilThumbImageUrl']?>" width="50px" height="50px" />

				<?php }
				}else{
					echo "<span><i>Pas de producteur renseigné</i></span>";
				} ?> -->

			</div>



			<div class="col-md-6 col-sm-6 col-xs-12 text-dark text-center">
				<!-- <h4>SOUTIENS</h4> -->
				<div id="supports"></div>
				<!-- 	<?php 
					if(isset($element['supports'])){ 	
					foreach($element['supports'] as $id => $el){
						$support=Element::getElementByid($id,Organization::COLLECTION);	
						//var_dump($producor);			
					?>
					<a href="#page.type.organizations.id.<?php echo $id ?>" class="lbh tooltips" data-toggle="tooltip" data-placement="left" title="<?php echo $support['name'] ?>"><img src="<?php echo $support['profilThumbImageUrl']?>" width="50px" height="50px" /></a>

				<?php }
				}else{
					echo "<span><i>Pas de soutien renseigné</i></span>";
				} ?> -->
			</div>

		</div>
		<div class="col-xs-12 no-padding" >
			<div class="col-xs-12 col-sm-8 margin-top-10 no-padding">
				<h4>Présentation de la collection</h4>
				<?php 
					$description = (empty($element["description"]) ? "" : $element["description"]);
				?>
				<div id="descriptionMarkdown" class="hidden"><?php echo (empty($description) ? "" : $description ); ?></div>
				<div class="col-xs-12 more no-padding item-desc" id="descriptionAbout"><?php echo $description ; ?></div>
				<!-- <div class="col-xs-12 col-sm-2"><img style="max-width: 100%;" /></div> -->
			</div>
			<div class="col-xs-12 col-sm-4 margin-top-50 container-img-card">
				<img src="<?php echo $img ?>" id="banner" class='img-responsive bg-light' style=""/>
			</div>

		</div>	
		<!-- <?php var_dump($element); ?> -->

	 
		
		
		<div class="col-xs-12 margin-top-20" style="margin-bottom: 100px;">
			<div class="col-xs-12 text-center">
			    <h4 class="section-title">
			        <span class="sec-title">Productions</span><br>
			        <i class="fa fa-angle-down"></i>
			    </h4>
			</div>
			<div class="col-xs-10 col-xs-offset-1 item-desc margin-top-20" id="listPoi"></div>
		
		</div>
		<div id="yearFilter" class="col-xs-12 margin-top-20">
			<div class="slider year col-xs-offset-1 col-xs-10 margin-30"></div>
		</div>

	</div>
	<div class="col-xs-12 col-sm-2">
			<div class="col-xs-12 col-sm-3 no-padding" id="cityFilter">
				<h4>TERRITOIRES</h4>
				<div class="slider city col-xs-12 margin-top-30 no-padding"></div>
			</div>

	</div>

	

	
</div>	

<?php
$slugVal = InflectorHelper::slugify2($element['name']) ;


?>
<script type="text/javascript">
var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>;
var canEdit = <?php echo json_encode($canEdit) ?>;
var slugVal = "<?php echo $slugVal ?>";
var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
						"#@"+contextData.slug : 
						"#page.type."+contextData.type+".id."+contextData.id);
    
// if(location.hash.indexOf("#page")>=0){
// 	strHash="";
// 	if(location.hash.indexOf(".view")>0){
// 		hashPage=location.hash.split(".view");
// 		strHash=".view"+hashPage[1];
// 	}
// 	replaceSlug=true;
// 	history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", "#@poi."+contextData.name);
// }
	mylog.log("contexteBadge",contextData);
	var data = {
		searchType : ["poi"], 
		filters : {
			tags : [$.trim(contextData.name)]	
		},
		sortBy : {
			"address.addressLocality" : 1
		}	
	};

	// paramsFilter = {
	// 	container : "#scopeFilter",
	// 	filters : {
	// 		scopeList :{
 // 				level : [],
 // 				countryCode : ["FR"]
	//  		}
	// 	},
	// 	defaults : {
	// 		types : ["poi"], 
	// 		tags : [$.trim(contextData.name)],
	// 		sort : {
	// 			"address.addressLocality" : 1
	// 		}
	// 	}
	// };	
	// filterSearch={};

jQuery(document).ready(function() {
	//alert("here");

	var elemData = <?php echo json_encode($element) ?> ;
	typeObj.badge.dynForm={
		jsonSchema : {
			beforeSave : function(data){
		    	mylog.log("beforeSave!!!",data);
		    	if(typeof data.criteria!="undefined"){
		    		unset(data.criteria);
		    	}
		    },
		    title : "Mettre à jour "+ elemData.name + " ("+elemData.category+")",
		    properties : costum.typeObj[elemData.category].dynFormCostum.beforeBuild.properties
		}

	};

	typeObj.badge.dynForm.jsonSchema.properties.image=dyFInputs.image();
	// dyFSpecBadge.jsonSchema.properties.collection={
	// 	inputType:"hidden",
	// 	value : elemData.collection
	// };

	$(".editElement").attr('onclick','dyFObj.editElement("badges","'+elemData._id.$id+'",null,'+JSON.stringify(costum.typeObj[elemData.category].dynFormCostum)+')');


	//$(".editElement").attr('onclick','dyFObj.openForm("badge",null,'+JSON.stringify(elemData)+',null,'+JSON.stringify(costum.typeObj[elemData.category].dynFormCostum)+')');

	// $(".editElement").attr('onclick','dyFObj.openForm('+JSON.stringify(dyFSpecBadge)+',null,'+JSON.stringify(elemData)+')');
	// $(".editElement").attr('onclick','dyFObj.openForm("badge",null,'+JSON.stringify(elemData)+',null,'+JSON.stringify(costum.typeObj[elemData.category].dynFormCostum)+')');
	//attr("data-subtype",JSON.stringify(typeObj.collections));

	function initDescs() {
		mylog.log("inintDescs");
		var descHtml = "<i>"+trad.notSpecified+"</i>";
		if($("#descriptionMarkdown").html().length > 0){
			descHtml = dataHelper.markdownToHtml($("#descriptionMarkdown").html()) ;
		}
		$("#descriptionAbout").html(descHtml);
		mylog.log("descHtml", descHtml);
	}

	function AddReadMore() {
	    var showChar = 300;
	    var ellipsestext = "...";
	    var moretext = "<?php echo Yii::t("common","Read more")?>";
	    var lesstext = "<?php echo Yii::t("common","Read less")?>";
	    $('.more').each(function() {
	      var content = $(this).html();
	      var textcontent = $(this).text();

	      mylog.log("textcontent",textcontent);

	      if (textcontent.length > showChar) {

	        var c = textcontent.substr(0, showChar);
	        c=c+ellipsestext
	        //var h = content.substr(showChar-1, content.length - showChar);

	        var html = '<span class="container "><span style="display:inline-block;">' + c + '</span>' + '</span><span class="morecontent" style="display:none;">' + content + '</span>';

	        $(this).html(html);
	        $(this).after('<a href="" class="morelink">' + moretext + '</a>');
	      }

	    });

	    $(".morelink").click(function() {
	      if ($(this).hasClass("less")==false) {
	      	$(this).addClass("less");
	        $(this).html(lesstext);
	        $(this).prev().children('.container').fadeToggle(100, function(){
	          $(this).next().fadeToggle(100);
	        });
	       
	       
	      } else {
	      	 $(this).removeClass("less");
	        $(this).html(moretext);
	        $(this).prev().children('.morecontent').fadeToggle(100, function(){
	          $(this).prev().fadeToggle(100);
	        });
	        
	      }
	      //$(this).prev().children().fadeToggle();
	      //$(this).parent().prev().prev().fadeToggle(500);
	      //$(this).parent().prev().delay(600).fadeToggle(500);
	      
	      return false;
	    });
	}    

    $(function() {
        //Calling function after Page Load
        AddReadMore();
    });



    addItemsToSly(slugVal);
	initDescs();
	initCollection();


	$(document).on('mouseover','.slick-slide',
		function(){
			if($(this).not('.slick-current')){
				$(this).addClass('active-year').siblings().removeClass('active-year sibling-year');
				$(this).find('.yearFilter.inactiveYear').parent().parent().removeClass('active-year');
				$(this).prev().not('.slick-current').find('.yearFilter').not('.inactiveYear').parent().parent().addClass('sibling-year');
				$(this).next().not('.slick-current').find('.yearFilter').not('.inactiveYear').parent().parent().addClass('sibling-year');
				//$(this).find('.inactiveYear').removeClass('sibling-year');
			}
		}
	);	
	$(document).on('mouseout','.slick-slide',function(){
		 	//if($('.slick-current')){
		 	$(this).removeClass('active-year').siblings().removeClass('sibling-year');	
			//	$(this).siblings().removeClass('active-year sibling-year');
			//}
		 }
	);

});



	function bindCityFilter(){
		mylog.log("binding city filter");
		$(".cityFilter").off().on("click",function(){
			mylog.log("city filter onclick");
				var params = {
							searchType : ["poi"], 
							filters : {
								tags : [$.trim(contextData.name)]	
							}

				};	

				if($(this).hasClass("active")==false){
					$(this).addClass("active");
					params.locality={};
					params.locality[$(this).data("id")]= {
						type : "cities",
						id : $(this).data("id"),
						postalCode : $(this).data("postalcode")
					};

				addItemsToSly(slugVal,$(this).data("id"));	
				
				}else{
					params.sortBy = {
						"address.addressLocality" : 1
					};	
					$(this).removeClass("active");
					addItemsToSly(slugVal);
				}

				ajaxPost(
					null,
					baseUrl + "/" + moduleId + "/search/globalautocomplete",
					params,
					function(data){ 
						//$("#cityFilter").append(html);
						var type = "poi";
						// mylog.log("pageProfil.views.directory canEdit" , canEdit);
						if(typeof canEdit != "undefined" && canEdit)
						canEdit="poi";
						// mylog.log("pageProfil.views.directory edit" , canEdit);
						var res=(Object.keys(data.results).length>0) ? directory.showResultsDirectoryHtml(data.results, "directory.elementPanelHtml", true, null) : "<span> Aucune production à afficher </span>";
						
						$("#listPoi").html(res);
								
						coInterface.bindLBHLinks();
						directory.bindBtnElement();
						coInterface.bindButtonOpenForm();

						const arr = document.querySelectorAll('img.lzy_img')
						arr.forEach((v) => {
							v.dom = "#content-results-profil";
							imageObserver.observe(v);
						});
					},
					"html"
				);

		});
	}	

	function initCollection(){

		coInterface.showLoader("#listPoi");

		// use of loaded data for performance, add collection/genre params or push collection into tags
		// var prod=addItemsToSly(slugVal,null,true);
		// mylog.log("prod",prod);

		ajaxPost(null,
			baseUrl + "/" + moduleId + "/search/globalautocomplete",
			data,
			function(data){ 


					var distinctCities = [];
					var distinctYears = [];
					var distinctSupports = {}
					var distinctProducors={};

					var html = "";

					// if use addItemTosly
					//data.results=prod;

					if(Object.keys(data.results)!="undefined" && Object.keys(data.results).length>0){
						$.each(data.results,function(key,values){
							if(typeof values.address!="undefined" && typeof values.address.localityId!="undefined"){
								if($.inArray(values.address.addressLocality,distinctCities)==-1){
									html+='<div data-toggle="dropdown" data-id="'+values.address.localityId+'" class="col-xs-12 cityFilter" data-postalcode="'+values.address.postalCode+'" >'+values.address.addressLocality+'</div>';

									distinctCities.push(values.address.addressLocality);
									

									mylog.log("distinctCities",distinctCities);
								}	
							}
							if(typeof values.year!="undefined" && values.year>2000 && $.inArray(values.year,distinctYears)==-1)
									distinctYears.push(values.year);
								mylog.log(distinctYears,"distinctYears");


							if(typeof values.producors!="undefined" && Object.keys(values.producors).length>0){
								$.each(values.producors,function(kp,vp){
									if(typeof distinctProducors[kp] =="undefined"){
										distinctProducors[kp]=vp;

									}
								});
								mylog.log(distinctProducors,"distinctProducors");	
							}
							if(typeof values.supports!="undefined" && Object.keys(values.supports).length>0){
								$.each(values.supports,function(ks,vs){
									if(typeof distinctSupports[ks] =="undefined"){
										distinctSupports[ks]=vs;
									}
								});
								mylog.log(distinctSupports,"distinctSupports");	
							}	

						});

						// find the distinct partners of the collection
						ajaxPost(
							"#producors",
							baseUrl + "/costum/notragora/getbyarrayid/type/organizations/contextId/"+contextData.id+"/contextType/"+contextData.type,
							{arrayIds:distinctProducors,className:"producors",title:"Producteurs",number:3},
							function(data){ 
								mylog.log("producors",data);
								if($("#producors").find("a").length==0){
									$("#producors").append("<span><i>Aucun soutien renseigné</i></span>");

								}else if($(".listsItemsPod.producors .seeAll")){
									$(".listsItemsPod.producors .seeAll").attr("href","javascript:;");
									$(".listsItemsPod.producors .seeAll").off().on("click",function(){
										ajaxPost(
											"#producors",
											baseUrl + "/costum/notragora/getbyarrayid/type/organizations/contextId/"+contextData.id+"/contextType/"+contextData.type,
											{arrayIds:distinctProducors,className:"producors",title:"Producteurs",number:20},
											function(data){ 
												mylog.log("all producors",data);
												$(".listsItemsPod.producors .seeAll").bind("click");
											}
										);

											
										
									});
								}	
							}
						);

						ajaxPost(
							"#supports",
							baseUrl + "/costum/notragora/getbyarrayid/type/organizations/contextId/"+contextData.id+"/contextType/"+contextData.type,
							{arrayIds:distinctSupports,className:"supports",title:"Soutiens",number:3},
							function(data){ 
								mylog.log("supports",data);
								if($("#supports").find("a").length==0){
									$("#supports").append("<span><i>Aucun soutien renseigné</i></span>");

								}else if($(".listsItemsPod.supports .seeAll")){
									$(".listsItemsPod.supports .seeAll").attr("href","javascript:;");
									$(".listsItemsPod.supports .seeAll").off().on("click",function(){
										ajaxPost(
											"#supports",
											baseUrl + "/costum/notragora/getbyarrayid/type/organizations/contextId/"+contextData.id+"/contextType/"+contextData.type,
											{arrayIds:distinctSupports,className:"supports",title:"Soutiens",number:20},
											function(data){ 
												mylog.log("all supports",data);
												$(".listsItemsPod.supports .seeAll").bind("click");
												
											}
										);
									});
								}									
							}
						);



						

					}else{
						html+="<span class='no-territory'><i>Aucun territoire pour cette collection</span></i>";
						$("#cityFilter").css("position","unset");
					}	

					// START CITY CAROUSEL PARAMS
					$(".city").append(html);
					
					//$("#cityFilter").css("position","fixed");

						$('.city').on('init', function(event, slick){
							  $('.city .slick-current').removeClass("slick-current");
							  firstLoad=true;
						});

						

						var cityCounter=distinctCities.length;
						var slidesToShow=cityCounter-1;

						mylog.log("cityCounter",cityCounter);

						function getCitySliderSetting(){
							return {dots: false,
							respondTo:'slider',
						  	slidesToShow: cityCounter,
						  	centerPadding: '0px',
						  	slidesToScroll: 1,
						  	vertical : true,
						  	touchMove: false,infinite:false,focusOnChange:true,centerMode: true,focusOnSelect:true}
						}

						$('.city').slick(getCitySliderSetting());
						

						var sameSlide=false;
						var firstLoad=true;

						$('.city').on('beforeChange', function(event, slick, currentSlide, nextSlide){
							
							if($(this).find(".slick-slide[data-slick-index='"+nextSlide+"']").find(".inactiveYear")){
								event.preventDefault();
							}
							
							if(nextSlide==currentSlide){
								//alert("same");
								sameSlide=true;
							}

							if(typeof firstLoad!="undefined" && firstLoad==true){
								sameSlide=false;
							}
							firstLoad=false;
							
						});	

						$('.city').on('afterChange', function(event, slick, currentSlide){
							coInterface.showLoader("#listPoi");


							if(typeof urlParams=="undefined"){
								 urlParams=new URLSearchParams(window.location.search);
							}

							var params = {
								searchType : ["poi"], 
								filters : {
									tags : [$.trim(contextData.name)]	
								}
							};


							//alert(sameSlide);
							if(sameSlide==true){	
								$(this).find(".slick-slide.slick-current").removeClass("slick-current");
								$('.city').slick('unslick'); 
								$(this).find(".slick-slide[data-slick-index='"+currentSlide+"']").remove();
								//var setObj=$.extend(getYearSliderSetting(),{initialSlide:currentSlide});
								$('.city').slick(getCitySliderSetting());
								sameSlide=false;
								params.sortBy = {
									"address.addressLocality" : 1
								};	
								urlParams.delete("cityId");
								urlParams.delete("postalCode");
								//$(this).removeClass("active");
								addItemsToSly(slugVal);
							}else{
								var cityId=$(this).find(".slick-slide[data-slick-index='"+currentSlide+"']").find(".cityFilter").data("id");
								var cityPostalCode=$(this).find(".slick-slide[data-slick-index='"+currentSlide+"']").find(".cityFilter").data("postalcode");

								urlParams.set("cityId",cityId);
								urlParams.set("cityPostalCode",cityPostalCode);


								params.locality={};
								params.locality[cityId]= {
									type : "cities",
									id : cityId
									//postalCode : cityPostalCode
								};
								addItemsToSly(slugVal,cityId);
							}	

							if(notNull(urlParams.get("year"))){
								year=urlParams.get("year");
								params.filters={
								  year:year
								};  
							}							

								ajaxPost(
									null,
									baseUrl + "/" + moduleId + "/search/globalautocomplete",
									params,
									function(data){ 
										//$("#cityFilter").append(html);
										var type = "poi";
										// mylog.log("pageProfil.views.directory canEdit" , canEdit);
										if(typeof canEdit != "undefined" && canEdit)
										canEdit="poi";
										// mylog.log("pageProfil.views.directory edit" , canEdit);
										var res=(Object.keys(data.results).length>0) ? directory.showResultsDirectoryHtml(data.results, "directory.elementPanelHtml", true, null) : "<span> Aucune production à afficher </span>";
										
										$("#listPoi").html(res);

										coInterface.bindLBHLinks();
										directory.bindBtnElement();
										coInterface.bindButtonOpenForm();

										const arr = document.querySelectorAll('img.lzy_img')
										arr.forEach((v) => {
											v.dom = "#content-results-profil";
											imageObserver.observe(v);
										});
									},
									"html"
								);
								
						});

						// END CITY CAROUSEL PARAMS


						// START YEAR CAROUSEL PARAMS

						var currentYear= new Date().getFullYear();

						var yearCarousel="";
						var yearCounter=0;
						for(i=2009;i<=currentYear;i++){
							var num = i.toString();
							var inactive="";
							if($.inArray(num,distinctYears)<0){
								inactive="inactiveYear";
							}
							yearCarousel+="<div class='yearFilter "+inactive+"'>"+num+"</div>";
							yearCounter++;
						}

						$(".year").append(yearCarousel);
						mylog.log("yearCarousel",yearCarousel);
						mylog.log("initialSlide",initialSlide);
						var slidesToShow=7;
						centerPosition=Math.round(slidesToShow/2);
						var initialSlide=yearCounter-centerPosition;

						var firstLoad=true;

						$('.year').on('init', function(event, slick){
							firstLoad=true;
							//alert("init");
							$('.year .slick-current').removeClass("slick-current");

						});
						

						function getYearSliderSetting(){
							return{dots: false,
							respondTo:'slider',
						  	slidesToShow: slidesToShow,
						  	centerPadding: '0px',
						  	slidesToScroll: 4,
						  	touchMove: false,infinite:false,focusOnChange:true,centerMode: true,focusOnSelect:true,initialSlide:initialSlide}
						}

						$('.year').slick(getYearSliderSetting());

						

						var sameSlide=false;

						$('.year').on('beforeChange', function(event, slick, currentSlide, nextSlide){
							//alert(currentSlide);
							//alert(nextSlide);
							
							if(nextSlide==currentSlide){
								//alert("same");
								sameSlide=true;
								
							}
							//alert("firstLoad : "+firstLoad);
							if(typeof firstLoad!="undefined" && firstLoad==true){
								sameSlide=false;
							}
							firstLoad=false;
							
						});	

						$('.year').on('afterChange', function(event, slick, currentSlide){
							coInterface.showLoader("#listPoi");

							if(typeof urlParams=="undefined"){
								 urlParams=new URLSearchParams(window.location.search);
							}

							var params = {
								searchType : ["poi"], 
								filters : {
									tags : [$.trim(contextData.name)]	
								}
							};
							//alert("sameSlide : "+sameSlide);
							if(sameSlide==true){	
								$(this).find(".slick-slide.slick-current").removeClass("slick-current");
								sameSlide=false;
								$('.year').slick('unslick'); 
								$(this).find(".slick-slide[data-slick-index='"+currentSlide+"']").remove();
								//var setObj=$.extend(getYearSliderSetting(),{initialSlide:currentSlide});
								
								$('.year').slick(getYearSliderSetting());
								params.sortBy = {
									"address.addressLocality" : 1
								};	
								urlParams.delete("year");
								//$(this).removeClass("active");
								addItemsToSly(slugVal);
							}else{
								var year=$(this).find(".slick-slide[data-slick-index='"+currentSlide+"']").find(".yearFilter").html();
								//alert(year);

								params.filters.year=year;
								  

								urlParams.set("year",year);
								//urlParams.set("cityPostalCode",cityPostalCode);

								



								

							// if($(this).hasClass("active")==false){
							// 	$(this).addClass("active");
								
								addItemsToSly(slugVal,cityId);
							}
							if(notNull(urlParams.get("cityId")) && notNull(urlParams.get("cityPostalCode"))){
								
								var cityId=urlParams.get("cityId");
								var cityPostalCode= urlParams.get("cityPostalCode");
								params.locality={};
								params.locality[cityId]= {
									type : "cities",
									id : cityId,
									postalCode : cityPostalCode
								};
								
							}								

								ajaxPost(
									null,
									baseUrl + "/" + moduleId + "/search/globalautocomplete",
									params,
									function(data){ 
										//$("#cityFilter").append(html);
										var type = "poi";
										// mylog.log("pageProfil.views.directory canEdit" , canEdit);
										if(typeof canEdit != "undefined" && canEdit)
										canEdit="poi";
										// mylog.log("pageProfil.views.directory edit" , canEdit);
										var res=(Object.keys(data.results).length>0) ? directory.showResultsDirectoryHtml(data.results, "directory.elementPanelHtml", true, null) : "<span> Aucune production à afficher </span>";
										
										$("#listPoi").html(res);
												
										coInterface.bindLBHLinks();
										directory.bindBtnElement();
										coInterface.bindButtonOpenForm();

										const arr = document.querySelectorAll('img.lzy_img')
										arr.forEach((v) => {
											v.dom = "#content-results-profil";
											imageObserver.observe(v);
										});
									},
									"html"
								);
								
						});

						// END YEAR CAROUSEL PARAMS

						


					// filterSearch = searchObj.init(paramsFilter);
					// $("#costum-scope-search").append(html);

					//$("#cityFilter").append(html);
					var type = "poi";
					// mylog.log("pageProfil.views.directory canEdit" , canEdit);
					if(typeof canEdit != "undefined" && canEdit)
						canEdit="poi";
					// mylog.log("pageProfil.views.directory edit" , canEdit);
					$("#listPoi").html( directory.showResultsDirectoryHtml(data.results, "directory.elementPanelHtml", true, null) );
					
					coInterface.bindLBHLinks();
			
					directory.bindBtnElement();
					//initBtnAdmin();
					coInterface.bindButtonOpenForm();

					const arr = document.querySelectorAll('img.lzy_img')
					arr.forEach((v) => {
						v.dom = "#content-results-profil";
						imageObserver.observe(v);
					});
				}
		,"html");
	}

</script>
