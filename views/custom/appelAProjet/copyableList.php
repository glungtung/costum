<h1 class="text-center">Copiable CoForm</h1>
<style type="text/css">
</style>
<div class="col-xs-12 col-sm-10 col-sm-offset-1">
    <div class="no-padding col-xs-12 text-left headerSearchContainer"></div>

    <div id="filterCMS"></div>
    <div id="appCmsCurrent">

    </div>
    <div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div>
</div>

<script type="text/javascript">

    var paramsFilter= {
        container : "#filterCMS",
        loadEvent : {
            default : "pagination"
        },
        defaults : {
            types : ["forms"],
            fields : ["subForms"],
            notSourceKey: true,
            filters : {
                "copyable" : {"$in" : ["true"]}
            }
        },
        results : {
            dom : '#appCmsCurrent',
            //renderView : "directory.cmsPanelHtml"
        },
        filters : {
            text : true
        }
    };
    var searchTemplate={};
    jQuery(document).ready(function() {
        searchTemplate = searchObj.init(paramsFilter);
        $(".headerSearchContainer .headerSearchright .btn-show-map").remove();
        $("#appCmsCurrent").mouseover(function(){
            $("html").css("overflow-y", "hidden");
        });
        searchTemplate.results.render=function(fObj, results, data){
            if(Object.keys(results).length > 0){
                ajaxPost(fObj.results.dom, baseUrl+'/costum/aap/getviewbypath/path/costum.views.custom.appelAProjet.copyableListCard',
                    {
                        results:results,
                        contextData:contextData,
                    },
                    function(){
                        fObj.results.events(fObj);
                    });
            }else
                $(fObj.results.dom).append(fObj.results.end(fObj));
            fObj.results.events(fObj);
        };
        searchTemplate.pagination.callBack = function(fObj){
            //cmsBuilder.bindEvents();
        };
        searchTemplate.search.init(searchTemplate,0);

    });
</script>