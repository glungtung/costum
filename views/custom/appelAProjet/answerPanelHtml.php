<style type="text/css">
	.answerLi{
		box-shadow: 0px 0px 3px -1px rgba(0,0,0,0.5);
	}
	.answerLi:hover{
		background-color: #254c960a;
    	box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.5);
	}
	.answerLi .btn-open-answer{
		/*color: white;
	    font-size: 12px;
	    font-weight: 800;
	    padding: 3px 10px;
	    background-color: #254c97;*/
	}
	.answerLi .btn-retain-reject{
		/*font-size: 12px;
	    font-weight: 800;
	    padding: 3px 10px;
		width : 96px;*/
	}
	.answerLi .titleAnsw{
		color: #2c4b89;
	    font-size: 22px;
	    text-transform: inherit;
	}
	.answerLi b{
	    color: #183875;

	}
	.answerLi .infoAnsw{
		font-size: 14px;
	}
	.answerLi .container-infos{
		padding:0px;
    	padding-bottom: 5px !important;
    }
</style>

<!-- <pre>
	<?php //print_r($allAnswers); ?>
</pre> -->
<?php $what="Dossier " ?>
	<div id="allAnswersContainer" class="col-xs-12 no-padding">
	<?php 
		$lbl = "Dossier ";
		$ct = 0;
		$globalLinks = [];
		$gUids = [];
		if(!empty($allAnswers)){ 
		foreach ($allAnswers as $k => $ans) {
			if(isset($ans["answers"])){
			$ct++;
			$lbl = $ct."- ".(!empty(@$ans["answers"]["aapStep1"]["titre"]) ? @$ans["answers"]["aapStep1"]["titre"] : @$ans["answers"]["aapStep1"]["proposition"]);
			$address="<i class='fa fa-map-marker'></i> ";
			$user= PHDB::findOne(Citoyen::COLLECTION, array("_id" => new MongoId($ans["user"]) ));
            if(!empty($user)) $nameProp = $user["name"];
			$descrTravaux= "Aucune";
			if(isset($form["mapping"]) && !isset($ans["mappingValues"]) ){
				$ans["mappingValues"]=Answer::getMappingValues($form["mapping"], $ans);
			}
			$address.= !empty($ans["answers"]["aapStep1"]["adress"]["address"]["name"]) ? $ans["answers"]["aapStep1"]["adress"]["address"]["name"] : " adresse non renseignée";

			if(!empty($ans["answers"]["aapStep1"]["description"])) $descrTravaux = $ans["answers"]["aapStep1"]["description"];

			$lblp = "";
			$percol = "danger";
			$statecol = "danger";
			$lblstate = "Pas d'opérateur";
			$step = "ouvert";
			$icon = "folder-open-o";
			$localLinks = [];
			$uids = [];
			$todo = 0;
			$done = 0;
			$tasksPerson = [];
			$imgAnsw=(isset($ans["profilMediumImageUrl"])) ? Yii::app()->createUrl($ans["profilMediumImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; 
			$retain = [
				"label" => "Non retenu",
				"class" => "btn-default",
				"retain" => false
			];
			if(isset($ans["acceptation"]) && $ans["acceptation"] == "retained"){
				$retain["label"] = "Retenu";
				$retain["class"] = "btn-success";
				$retain["retain"] = true;
			}
			if(!isset($ans["answers"])) {
				$lblp = "no answers" ;
				$percent = 0;
			} else {
				$totalInputs = 0;
				$answeredInputs = 0;
				if (isset($forms)) {
					foreach ($forms as $fid => $f) 
					{
						if (isset($f["inputs"])) {
							$totalInputs += count($f["inputs"]);
							//echo "|".$f['id']."-fi=".count($f["inputs"]);
							if( isset( $ans["answers"][$fid] ) ){
								$answeredInputs += count( $ans["answers"][$fid] );
								$step = $f['name'];
								//echo "|".$f['id']."-ai=".count( $ans["answers"][$f['id']] )."<br/>";
							}
						}

						//todo lists are on depense for the moment 
						//todo genereaclly not with a fixed input ID
						if( isset( $ans["answers"][$fid]["depense"] ) )
						{
							foreach ( $ans["answers"][$fid]["depense"]  as $ix => $dep) 
							{
								if( isset( $dep["todo"] ) )
								{
									foreach ($dep["todo"] as $ixx => $t) 
									{
										if(!isset($t["done"]) || $t["done"] == "0")
										{
											$todo++;
											$whos = (is_array($t["who"])) ? $t["who"] : explode(",",$t["who"]);
											foreach ( $whos as $whoix => $who ) {
												if( !isset( $tasksPerson[ $who ] ) ) 
													$tasksPerson[ $who ] = [];
												$tasksPerson[ $who ][] = $t["what"];
											}
											
										}
										else
											$done++;
									}
								}

							}
						}
					}
				}
				if(isset($ans["links"])) 
				{
					foreach ( $ans["links"] as $type => $ls ) 
					{
						if(!isset($localLinks[$type]))
							$localLinks[$type] = [];

						if(!isset($globalLinks[$type]))
							$globalLinks[$type] = [];

						if($type == "operators"){
							$lblstate = "Opérateur à valider";
							$statecol = "warning";
							foreach ($ls as $oid => $ov) {
								if($ov != "0"){
									$lblstate = "Opérateur OK";
									$statecol = "primary";
								}
							}
						}
						

						foreach ($ls as $uid => $time) {
							if(is_string($uid) && strlen($uid) == 24 && ctype_xdigit($uid)){
								if(!in_array($uid, $localLinks[$type] ))
									$localLinks[$type][] = $uid;	
								if(!in_array($uid, $uids ))
									$uids[] = new MongoId( $uid );
								if(!in_array($uid, $globalLinks[$type] ))
									$globalLinks[$type][] = $uid;
								if(!in_array($uid, $gUids ))
									$gUids[] = new MongoId( $uid );
							}
						}
						
					}
				}

				//echo "tot".$totalInputs."-ans".$answeredInputs;
				if ($totalInputs != 0) {
					$percent = floor( $answeredInputs*100 / $totalInputs );
				} else {
					$percent = 0;
				}
				
				$percol = "primary";
				$lblp = $percent."%";
			}

			if( $percent > 50 )
				$percol = "warning";
			if( $percent > 75 )
				$percol = "success";
				
			$liBg = ($todo>0) ? "style='background-color:lightGreen'" : "";
		?>

		<!--<a href="#answer.index.id.<?php echo $k ?>.mode.rplus" target="_blank" class="lbh answerLi col-xs-12 no-padding margin-bottom-10" <?php echo $liBg ?>>-->
		<div class="answerLi col-xs-12  no-padding margin-bottom-10" <?php echo $liBg ?>>
				<div class="col-xs-4 padding-top-10">
					<img src="<?php echo $imgAnsw ?>" class="img-responsive" style="height:200px;object-fit: cover;">
				</div>
				<div class="col-xs-8 container-infos">
					<div class="col-xs-12 no-padding">
						<h3 class="margin-top-5 titleAnsw"> <?php echo $lbl ?></h3> 
					</div>
					<span class="info-answ bold text-red"><?php echo $address ?></span><br/>
					<span class="info-answ bold"> <i class="fa fa-calendar"></i> <?php echo date("d/m/y H:i",$ans["created"]); ?></span>
					<?php if ( isset($ans["updated"] )) {?>
					<span class="info-answ bold"> <i class="fa  fa-edit"></i> <?php echo date("d/m/y H:i",$ans["updated"]); ?></span>
					<?php } ?>

					<span class="info-answ col-xs-12 no-padding"> <b>Déposé par :</b> <?php echo $nameProp; ?></span>

					<span class="info-answ col-xs-12 no-padding"> <b>Description :</b> <?php echo $descrTravaux; ?></span>
						
					<span class="info-answ"> <i class="fa fa-<?php echo $icon; ?>"></i> <?php echo $step; ?></span>
					
					<br/>
					
					<!-- <span class="label label-<?php echo $percol ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo $lblp ?> </span> -->

					<!-- <span class="label label-<?php echo $statecol ?> margin-left-5"> <i class="fa fa-black-tie"></i> <?php echo $lblstate ?> </span> -->
					
					<br/>
					<?php if ( $percent!= 0 && count($uids)) {?>
						<?php if ( count($tasksPerson)) {?>
						<!-- <a href="javascript:;" data-id='<?php //echo $ans["_id"] ?>' class='answerTasksBtn btn btn-xs btn-default '> <i class="fa  fa-cogs "></i>Tasks <span class="margin-5  label label-primary"> <i class="fa fa-square-o"></i> <?php echo $todo ?> </span> <span class="margin-5  label label-success"> <i class="fa   fa-check-square-o"></i> <?php echo $done ?> </span></a> -->
						<?php } ?>
					<?php } ?>
					<div class="col-xs-12 no-padding">
						<!-- <a href="#page.type.answers.id.<?php // echo $k ?>" class="lbh-preview-element margin-top-5 btn btn-open-answer">
							<i class="fa fa-sign-in"></i> Prévisualiser
						</a> -->
						<button type="button" class="btn btn-sm <?= $retain["class"] ?>  btn-retain-reject" data-id="<?php echo @$ans["_id"]['$id']; ?>" data-retain="<?= $retain["retain"] ? 'true' : 'false' ?>">
							 <?= $retain["label"] ?>
						</button>
						<!-- <a href="#answer.index.id.<?php // echo $k ?>.mode.rplus" target="_blank" class="lbh margin-top-5 btn btn-open-answer">
							<i class="fa fa-sign-in"></i> Ouvrir
						</a> -->
						<button type="button" class="aapgoto btn btn-sm btn-primary btn btn-open-answer" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo @$el_slug; ?>.aappage.form.answer.<?php echo @$ans["_id"]['$id']; ?>"><i class="fa fa-plus-square-o"></i> Voir</button>
						<button type="button" class="btn-delete-answer  btn-sm btn btn-danger" data-id="<?= @$ans["_id"]['$id']; ?>"><i class="fa fa-cancel"></i> Supprimer</button>
					</div>
				</div>
		</div>
		
	<?php 
			}
		} 
	} ?>
</div>
<script>
	//reject or retain pro
	$('.btn-retain-reject').on('click', function(){
		var btn = $(this);
		var id = btn.data("id");
		var retain = btn.text();
		var params = {
			id : id,
			collection: "answers",
			path : "allToRoot",
			format : true,
			value : {
				acceptation : ((retain == "Retenu") ? "rejected" : "retained")
			}
		}
		btn.html('<i class="fa fa-refresh fa-spin "></i>');
		dataHelper.path2Value( params, function(res) {
			if(res.result){
				if(exists(res.elt) && exists(res.elt.acceptation)){
					if(res.elt.acceptation == "retained"){
						btn.text("Retenu").fadeIn(500);
						btn.removeClass("btn-default").addClass("btn-success");
					}else{
						btn.text("Non retenu").fadeIn(500);
						btn.removeClass("btn-success").addClass("btn-default");
					}
				}
			}
			mylog.log("retenu",res);
		});
	});
	/*$(".btn-delete-answer").off().on('click',function(){
		$this = $(this);
		ajaxPost(null, baseUrl+'/costum/aap/deleteaap/answer/'+$this.data("id"), 
			{
				formCreator: "<?= $form["creator"] ?>",
				contextData:contextData,
			},
			function(){
				fObj.results.events(fObj);
			});
	})*/
	$('.btn-delete-answer').off().click( function(){
		  $this = $(this);
	      id = $(this).data("id");
	      bootbox.dialog({
	          title: trad.confirmdelete,
	          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
	          buttons: [
	            {
	              label: "Ok",
	              className: "btn btn-primary pull-left",
	              callback: function() {
	                getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
	                	toastr.success("Projet supprimé avec succès !");
	                	$this.parent().parent().parent().remove();
	                },"html");
	              }
	            },
	            {
	              label: "Annuler",
	              className: "btn btn-default pull-left",
	              callback: function() {}
	            }
	          ]
	      });
    });
</script>