						
<style type="text/css">
    .text-black{
        color : black;
    }
    .pdfAnswers{
        background-color :#f8f8f8;
    }
    .titleStep{
        font-size: 23px;
        color: #7DA53D;
        font-weight: bold;
    }
    .titleTable{
        text-align: center;
        font-weight: bold ;
        font-size: 18px;
        color: #000000;
        text-transform: capitalize
    }
    .titleSection{
        font-weight: bold ;
        font-size: 18px;
        color: #000000;
        text-decoration: underline;
        text-transform: none
    }
    .columnbgWhite{
        background-color: #ffffff;
    }
    .text-bleu{
        color:#2C3E50;
        font-size: 13px;
        text-align: justify;
    }
    .title-input{
        color:#2C3E50;
        font-size: 13px;
        text-align: justify;
    }
    .title-input-tableau{
        color:#2C3E50;
        font-size:17px;
        text-align: justify;
    }
    .text-input{
        color:#2C3E50;
        font-size: 13px;
        text-align: justify;
    }
    .trBg{
        background-color:#eeeeee ;
        color:#7da53d; 
        font-size:17px;
        font-weight:bold;
        padding: 20px;
        
    }
    .bg-green{
        background-color:#93C020;
        font-size: 14px;
        background: linear-gradient(to left, #ff0000 50%, #0000ff 50%);
    }
    .text-green{
        font-size: bold;
        font-size: 14px;
        background-color: #7da53d;
        color: white;
    }
    .text-black{
        color:  #2C3E50;
    }

</style>
<?php
$properties = [
    "group" => [
        "placeholder" => "Groupé", 
        "inputType" => "tags", 
        "tagsList" => "budgetgroupe", 
        "rules" => ["required" => true], 
        "maximumSelectionLength" => 3
    ], 
    "nature" => [
        "placeholder" => "Nature de l’action", 
        "inputType" => "tags", 
        "tagsList" => "budgetnature", 
        "rules" => ["required" => true], 
        "maximumSelectionLength" => 3
    ], 
    "poste" => [
        "inputType" => "text", 
        "label" => "Poste de dépense", 
        "placeholder" => "Poste de dépense", 
        "rules" => ["required" => true]
    ]
];
?>
<div class="pdfAnswers" >
    <?php if(isset($imageProfil) && $imageProfil != "empty"){?>
        <table cellspacing="7" cellpadding="7" style="width:100%">
            <tr>
                <td style="width:70%"> 
                    <span class="text-black " style=" text-align:center ;font-size:18px"><b><i><?php if (isset($answers["answers"]["aapStep1"]["titre"])) echo $answers["answers"]["aapStep1"]["titre"];?></i></b> </span>
                </td>
                <td  style="width:30%">
                    <?php if($imageProfil != "empty") {
                        $src = $imageProfil;
                    }else 
                        $src =  "";
                    ?>
                    <img  style="height:150px;width:250px;text-align:right;float:right" src = "<?= $src?>">
                </td>
            </tr>
        </table>
    <?php }else{ ?>
        <p class="text-black " style="text-align:center ;font-size:18px"><b><?php if (isset($answers["answers"]["aapStep1"]["titre"])) echo $answers["answers"]["aapStep1"]["titre"];?></b> </p>
    <?php } ?>
    <?php if(isset($parentForm["textBeforePdf"])){ ?>
        <p class="text-bleu">     <?= $parentForm["textBeforePdf"] ?></p>
    <?php } ?>
    <?php 
        $arrayOther = [
            "email",
            "year"
        ];
        $arrayAsso = [
            "association",
            "aapStep1l1lzd7y6jak23ytnjpd", 
            "adress",
            "dateCreationAssociation",
            "aapStep1l2g51xdwpw6ink6lzl",
            "aapStep1kzwnpynzynwwdpp124",
            "aapStep1l1xf6yz7kf8d13q00i",
            "aapStep1kzs3cxcd3o1q1bpgnze",
            "aapStep1l0kxnpaxnoa5ai1ugdf",
            "aapStep1l0kxsduw8zfkk382spx",
            "aapStep1l0kxvxg5f5ssf0sx1pd",
            "aapStep1l0kxwmu7gj22sln50ts",
            "aapStep1kzwmxl3ye1rjneax5gh",
            "numberOfMembers",
            "numberOfVolunteers",
            "aapStep1kzs3e6yefx3bhtvappj",
            "aapStep1kzs3e6yefx3bhtvappj",
            "aapStep1l0s26866dfa9o5mn79",
            "aapStep1l0s2aoqca1cmd7vjzz"
        ];
        $arrayProject = [
            "aapStep1l0ru4g66a6r7lfwtay",
            "titre",
            "interventionArea",
            "tags",
            "axesTFPB",
            "aapStep1l1xfdtuazek4jhgcco",
            "aapStep1l0kythybqx6i2gldi1",
            "description",
            "aapStep1l50oowhxvnsknq9su1q",
            "nameProjectManager",
            "phoneProjectManager",
            "aapStep1l0m5iq15701exxuxwq" 
        ];
        $arrayBudget = [
            "budgetTable",
            "budget",
            "depense"
        ];
        $arrayModality = [
            "startDate",
            "plannedDuration",
            "exactLocationAction",
            "aapStep1l1xfy8ox73zuib530a3",
            "numberStakeholdersProject",
            "aapStep1l1xfp33q5bg1bz8mwbg",
            "totalNumberBeneficiaries",
            "aapStep1l1xft16v6ohm6m5o0wu",
            "aapStep1l1m457loinyrcupshn"
        ];
        ?>
        <?php
            if(isset($parentForm["pdfField"][$user_session_id])){
                $aapOther = [];
                $aapAssociation = [];
                $aapProject = [];
                $aapBudget = []; 
                $aapModality = []; 
                $aapStep2 = [];
                $aapStep3 = [];
                $aapStep4 = [];
                foreach($parentForm["pdfField"][$user_session_id] as $kFiel => $field){
                    if(in_array($field["value"],$arrayAsso)){
                      $aapAssociation[$kFiel] = $field; 
                    }
                    if(in_array($field["value"],$arrayOther)){
                      $aapOther[$kFiel] = $field; 
                    } 
                    if(in_array($field["value"],$arrayProject)){
                      $aapProject[$kFiel] = $field; 
                    } 
                    if(in_array($field["value"],$arrayBudget)){
                      $aapBudget[$kFiel] = $field; 
                    } 
                    if(in_array($field["value"],$arrayModality)){
                      $aapModality[$kFiel] = $field; 
                    } 
                    if($field["step"] == "aapStep2"){ 
                        $label = isset($parentForm["pdfLabels"][$user_session_id][$field["value"]])?$parentForm["pdfLabels"][$user_session_id][$field["value"]]:$field["label"];
                        $aapStep2[$kFiel] = $field;
                        $aapStep2[$kFiel]["label"] = $label;
                    }
                    if($field["step"] == "aapStep3"){ 
                        $label = isset($parentForm["pdfLabels"][$user_session_id][$field["value"]])?$parentForm["pdfLabels"][$user_session_id][$field["value"]]:$field["label"];
                        $aapStep3[$kFiel] = $field;
                        $aapStep3[$kFiel]["label"] = $label;
                    }
                    if($field["step"] == "aapStep4"){ 
                        $label = isset($parentForm["pdfLabels"][$user_session_id][$field["value"]])?$parentForm["pdfLabels"][$user_session_id][$field["value"]]:$field["label"];
                        $aapStep4[$kFiel] = $field;
                        $aapStep4[$kFiel]["label"] = $label;
                    }
                } 
                 //var_dump($aapAssociation);
               // var_dump($aapProject);EXIT;
                // var_dump($aapBudget);
         //  var_dump($aapStep3);exit;
                ?>
                <?php if($aapOther || $aapAssociation || $aapBudget || $aapProject ||  $aapModality){?>
                    <table  cellspacing="7" cellpadding="7">
                        <tr>
                            <td style=" background-color: #93C020 ;border:2px solid black; text-align:center"><b>PROPOSITION</b> </td>
                        </tr>
                    </table><br>     
                <?php }  ?>    
                <?php if($aapOther) {?>       
                    <table cellspacing="5" cellpadding="5">
                        <tr>
                            <td>
                            <?php foreach($aapOther as $ko => $vo){?>
                                <br><b class="title-input"><?php echo $vo["label"]?>:</b>  
                                <?php 
                                    $inputsBr = str_replace("\n", "<br>", $answers["answers"][$vo["step"]][$vo["value"]]);                                
                                     echo ' <span class="text-input"> '.$inputsBr .'</span>';  
                                ?>
                            <?php }?>
                            </td>
                        </tr>
                    </table>   
                <?php }  ?><br>
                <?php if($aapAssociation){?>       
                    <table cellspacing="5" cellpadding="5">
                        <tr> 
                            <td style="background-color: #ddd ;border:1px solid black;">  Association </td> 
                        </tr>
                        <tr>
                            <td  style="border-left:3px solid black;" >
                            <?php foreach($aapAssociation as $kAsso => $vAsso){?>
                                <br><b class="title-input"><?php echo $vAsso["label"]?>:</b> 
                                <?php if (isset($answers["answers"][$vAsso["step"]][$vAsso["value"]]) && !is_array($answers["answers"][$vAsso["step"]][$vAsso["value"]])) {
                                    
                                    $inputsBr = str_replace("\n", "<br>", $answers["answers"][$vAsso["step"]][$vAsso["value"]]);
                                    echo ' <span class="text-input"> '.$inputsBr .'</span>'
                                    ?>                                
                                   
                                <?php }else if(isset($answers["answers"][$vAsso["step"]][$vAsso["value"]]) && is_array($answers["answers"][$vAsso["step"]][$vAsso["value"]]) && $vAsso["value"]!="budget" && $vAsso["value"]!="depense" && $vAsso["value"]!="adress"){
                                    
                                    if($vAsso["type"] == "tpls.forms.cplx.element") {

                                    }else{
                                        foreach($answers["answers"][$vAsso["step"]][$vAsso["value"]] as $kf => $vf){
                                            $vfBr = str_replace("\n", "<br>", $vf); 
                                            echo '<br> <span class="text-input"> - '.$vfBr .'</span>'; 
                                        }
                                    }
                                }else if(isset($answers["answers"][$vAsso["step"]][$vAsso["value"]]) && $vAsso["value"] == "adress" ){
                                    echo $answers["answers"][$vAsso["step"]][$vAsso["value"]]["address"]["name"];
                                }?>
                            <?php }?>
                            </td>
                        </tr>
                    </table>
                <?php } ?><br><br>
                <?php if($aapProject) {?> 
                    <table cellspacing="5" cellpadding="5">
                        <tr> 
                            <td style="background-color: #ddd ;border:1px solid black;"> Projet</td> 
                        </tr>
                        <tr>
                        <td  style="border-left:3px solid black;" >
                            <?php foreach($aapProject as $kPj => $vPj){?>
                            <br> <b class="title-input"><?php echo $vPj["label"]?>:</b> 
                                <?php if (isset($answers["answers"][$vPj["step"]][$vPj["value"]]) && !is_array($answers["answers"][$vPj["step"]][$vPj["value"]])) {
                                    
                                    $inputsBr = str_replace("\n", "<br>", $answers["answers"][$vPj["step"]][$vPj["value"]]);                                
                                     echo ' <span class="text-input"> '.$inputsBr .'</span>';
                                }else if(isset($answers["answers"][$vPj["step"]][$vPj["value"]]) && is_array($answers["answers"][$vPj["step"]][$vPj["value"]]) && $vPj["value"]!="budget" && $vPj["value"]!="depense" && $vPj["value"]!="adress"){
                                    if($vPj["type"] == "tpls.forms.cplx.element") {
                                
                                    }else{
                                        foreach($answers["answers"][$vPj["step"]][$vPj["value"]] as $kf => $vf){
                                            $vfBr = str_replace("\n", "<br>", $vf); 
                                            echo '<br> <span class="text-input"> - '.$vfBr .'</span>';
                                        }
                                    }
                                }else if(isset($answers["answers"][$vPj["step"]][$vPj["value"]]) && $vPj["value"] == "adress" ){
                                    echo $answers["answers"][$vPj["step"]][$vPj["value"]]["address"]["name"];
                                }?>
                            <?php }?>
                            </td>
                        </tr>
                    </table>
                <?php } ?><br><br>
                <?php if($aapBudget){?>
                    <table  >
                        <tr> 
                            <td style="background-color: #ddd ;border:1px solid black;"> Budget </td> 
                        </tr>
                        <tr>
                            <td>
                                <?php 
                                foreach($aapBudget as $kBdj => $vBdj){?>
                                    <br><br> <b class="title-input-tableau"><?php echo $vBdj["label"]?>:</b> <br>
                                    <?php if(isset($answers["answers"][$vBdj["step"]][$vBdj["value"]]) && $vBdj["value"] == "budget" ){?>
                                        <br>
                                        <table border="2" cellpadding="5">
                                            <?php 
                                            if(!Answer::is_true($parentForm["params"]["budgetbudget"]["activeGroup"])) unset($properties["group"]);                
                                            if(!Answer::is_true($parentForm["params"]["budgetbudget"]["activeNature"])) unset($properties["nature"]);
                                            foreach ($parentForm["params"]["budgetbudget"]["amounts"] as $k => $l) {
                                                $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l ];
                                            }
                                            ?>
                                            <tr class="trBg">
                                                <?php foreach ($properties as $i => $inp) {
                                                    echo "<th>".$inp["placeholder"]."</th>";
                                                } ?>
                                            </tr>  
                                            <?php
                                            $totalPrice = 0;
                                            $totalSubvention = 0;
                                            $showSubvetion = false;
                                            $icont = 1;
                                            foreach ($answers["answers"]["aapStep1"]["budget"] as $depId => $dep) {
                                                if(!empty($dep["price"]))
                                                    $totalPrice += (float) $dep["price"];
                                                ?>
                                                <tr data-id="<?= $icont ?>">
                                                    <?php foreach ($properties as $i => $inp) {?>
                                                        <td class="text-bleu">
                                                            <?php 
                                                                if( $i == "price" && !empty($dep[$i]) && is_numeric($dep[$i])  ) {  ?>
                                                                    <span> <?php echo trim(strrev(chunk_split(strrev($dep["price"]),3, ' '))) ?>€</span>
                                                            <?php                                                        
                                                                } elseif(isset($dep[$i])){
                                                                    if($i == "poste" && (strpos(strtolower($dep[$i]), "subvention") !== false || strpos(strtolower($dep[$i]), "subventions") !== false)){
                                                                        $showSubvetion = true;
                                                                        $totalSubvention += (float) @$answers["answers"]["aapStep1"]["budget"][$depId]["price"];
                                                                    }?>
                                                                <?= $dep[$i] ?>
                                                                <?php } ?>
                                                        </td>
                                                    <?php  } ?>
                                                </tr>
                                            <?php  }  ?>
                                            <tr style="background-color:#dcdcdc ;">
                                                <td class="text-bleu" style="text-align:right ">Total :   </td>
                                                <td class="text-bleu" > <span>   <?php echo trim(strrev(chunk_split(strrev($totalPrice),3, ' '))) ?></span>  €</td>
                                            </tr>
                                            <?php if($showSubvetion){ ?>
                                                <tr>
                                                    <td colspan="4" class="text-bleu" >
                                                        <i  style=" ">L'association sollicite une subvention totale de <span  ><?= $totalSubvention ?> € </span></i>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    <?php } else if(isset($answers["answers"][$vBdj["step"]][$vBdj["value"]]) && $vBdj["value"] == "depense"){?>
                                        <br>
                                        <table border="2" cellpadding="5">
                                            <?php 
                                            if(isset($parentForm["params"]["budgetdepense"]["activeGroup"]) && !Answer::is_true($parentForm["params"]["budgetdepense"]["activeGroup"])) unset($properties["group"]);                
                                            if(isset($parentForm["params"]["budgetdepense"]["activeNature"]) &&!Answer::is_true($parentForm["params"]["budgetdepense"]["activeNature"])) unset($properties["nature"]);
                                            if(isset($parentForm["params"]["budgetdepense"]["amounts"])){
                                                foreach ($parentForm["params"]["budgetdepense"]["amounts"] as $k => $l) {
                                                    $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l ];
                                                }
                                            }
                                            ?>
                                            <tr class="trBg" >
                                                <?php foreach ($properties as $i => $inp) {
                                                    echo "<th>".$inp["placeholder"]."</th>";
                                                } ?>
                                            </tr>  
                                            <?php
                                            $totalPrice = 0;
                                            $totalSubvention = 0;
                                            $showSubvetion = false;
                                            $icont = 1;
                                            foreach ($answers["answers"]["aapStep1"]["depense"] as $depId => $dep) {
                                                if(!empty($dep["price"]))
                                                    $totalPrice += (float) $dep["price"];
                                                ?>
                                                <tr>
                                                    <?php 
                                                   // var_dump($dep);
                                                    foreach ($properties as $i => $inp) {?>
                                                        <td class="text-bleu" >
                                                            <?php 
                                                                if( $i == "price" && !empty($dep[$i]) && is_numeric($dep[$i])  ) {  ?>
                                                                    <span> <?php echo trim(strrev(chunk_split(strrev($dep["price"]),3, ' '))) ?>€</span>
                                                                <?php } elseif( $i == "poste" && isset($dep[$i])){
                                                                    if(strpos(strtolower($dep[$i]), "subvention") !== false || strpos(strtolower($dep[$i]), "subventions") !== false ){
                                                                        $showSubvetion = true;
                                                                        $totalSubvention += (float) @$answers["answers"]["aapStep1"]["depense"][$depId]["price"];
                                                                    }
                                                                    ?>
                                                                <?php echo $dep[$i] ?>
                                                                <?php } ?>
                                                        </td>
                                                    <?php  } ?>
                                                </tr>
                                            <?php  }  ?>
                                            <tr style="background-color:#dcdcdc ;">
                                                <td class="text-bleu" style="text-align:right ">Total :   </td>
                                                <td class="text-bleu" > <span>   <?php echo trim(strrev(chunk_split(strrev($totalPrice),3, ' '))) ?></span>  €</td>
                                            </tr>
                                            <?php if($showSubvetion){ ?>
                                            <tr style="background-color:#dff0d8">
                                                <td colspan="4"  >L'association sollicite une subvention totale de <b  ><?= $totalSubvention ?> € </b></td>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    <?php }else if($vBdj["value"] == "budgetTable"){ 
                                        
                                        $tableOptions = [
                                            "budget"  => array(
                                                "columnLabel" => "Budget"
                                            ),
                                            "depense" => array(
                                                "columnLabel" => "Plan de Financement"
                                            ),
                                        ];?>
                                        <br>
                                        <table>
                                            <tr>
                                                <td>
                                                    <?php if(isset($answers["answers"][$vBdj["step"]]["budget"])){?>
                                                        <table border="2" cellpadding="5">
                                                            <?php 
                                                            if(!Answer::is_true($parentForm["params"]["budgetbudget"]["activeGroup"])) unset($properties["group"]);                
                                                            if(!Answer::is_true($parentForm["params"]["budgetbudget"]["activeNature"])) unset($properties["nature"]);
                                                            foreach ($parentForm["params"]["budgetbudget"]["amounts"] as $k => $l) {
                                                                $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l ];
                                                            }
                                                            ?>
                                                            <tr class="trBg">
                                                                <th>Budget <br></th>
                                                                <th>Montant</th> 
                                                            </tr>  
                                                            <?php
                                                            $totalPrice = 0;
                                                            $totalSubvention = 0;
                                                            $showSubvetion = false;
                                                            $icont = 1; 
                                                            foreach ($answers["answers"]["aapStep1"]["budget"] as $depId => $dep) {
                                                                if(!empty($dep["price"]))
                                                                    $totalPrice += (float) $dep["price"];
                                                                ?>
                                                                <tr data-id="<?= $icont ?>">
                                                                    <?php foreach ($properties as $i => $inp) {?>
                                                                        <td class="text-bleu" height="85">
                                                                            <?php 
                                                                                if( $i == "price" && !empty($dep[$i]) && is_numeric($dep[$i])  ) {  ?>
                                                                                    <span> <?php echo trim(strrev(chunk_split(strrev($dep["price"]),3, ' '))) ?>€</span>
                                                                            <?php                                                        
                                                                                } elseif(isset($dep[$i])){
                                                                                    if($i == "poste" && (strpos(strtolower($dep[$i]), "subvention") !== false || strpos(strtolower($dep[$i]), "subventions") !== false)){
                                                                                        $showSubvetion = true;
                                                                                        $totalSubvention += (float) @$answers["answers"]["aapStep1"]["budget"][$depId]["price"];
                                                                                    }?>
                                                                                <?= $dep[$i] ?>
                                                                                <?php } ?>
                                                                        </td>
                                                                    <?php  }  
                                                                    ?>
                                                                    
                                                                </tr>
                                                            <?php  }  ?>
                                                            <?php if(count($answers["answers"]["aapStep1"]["budget"]) < count($answers["answers"]["aapStep1"]["depense"])){
                                                                $dif1 = count($answers["answers"]["aapStep1"]["depense"]) - count($answers["answers"]["aapStep1"]["budget"]);
                                                                for($i = 1; $i <= $dif1; $i++ ){
                                                                    ?>
                                                                    <tr>
                                                                    <td class="text-bleu"height="85"> &nbsp; </td>
                                                                    <td class="text-bleu"height="85"> &nbsp; </td>

                                                                    </tr>
                                                                <?php }
                                                            }?>
                                                            <tr style="background-color:#dcdcdc ;">
                                                                <td class="text-bleu" style="text-align:right ">Total :   </td>
                                                                <td class="text-bleu" > <span>   <?php echo trim(strrev(chunk_split(strrev($totalPrice),3, ' '))) ?></span>  €</td>
                                                            </tr>
                                                            <?php if($showSubvetion){ ?>
                                                                <tr>
                                                                    <td colspan="4" class="text-bleu" >
                                                                        <i  style=" ">L'association sollicite une subvention totale de <span  ><?= $totalSubvention ?> € </span></i>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </table>
                                                    <?php }?>
                                                </td>
                                                <td>
                                                    <?php  if(isset($answers["answers"][$vBdj["step"]]["depense"])){?>
                                                        <table border="2" cellpadding="5">
                                                            <?php 
                                                            if(isset($parentForm["params"]["budgetdepense"]["activeGroup"]) && !Answer::is_true($parentForm["params"]["budgetdepense"]["activeGroup"])) unset($properties["group"]);                
                                                            if(isset($parentForm["params"]["budgetdepense"]["activeNature"]) &&!Answer::is_true($parentForm["params"]["budgetdepense"]["activeNature"])) unset($properties["nature"]);
                                                            if(isset($parentForm["params"]["budgetdepense"]["amounts"])){
                                                                foreach ($parentForm["params"]["budgetdepense"]["amounts"] as $k => $l) {
                                                                    $properties[$k] = ["inputType" => "number", "label" => $l, "propType" => "amount", "placeholder" => $l ];
                                                                }
                                                            }
                                                            ?>
                                                            <tr class="trBg" >
                                                                <th>Plan de Financement</th>
                                                                <th>Montant</th> 
                                                            </tr>  
                                                            <?php
                                                            $totalPrice = 0;
                                                            $totalSubvention = 0;
                                                            $showSubvetion = false;
                                                            $icont = 1;
                                                            foreach ($answers["answers"]["aapStep1"]["depense"] as $depId => $dep) {
                                                                if(!empty($dep["price"]))
                                                                    $totalPrice += (float) $dep["price"];
                                                                ?>
                                                                <tr>
                                                                    <?php
                                                                    foreach ($properties as $i => $inp) {?>
                                                                        <td class="text-bleu" height="85">
                                                                            <?php 
                                                                                if( $i == "price" && !empty($dep[$i]) && is_numeric($dep[$i])  ) {  ?>
                                                                                    <span> <?php echo trim(strrev(chunk_split(strrev($dep["price"]),3, ' '))) ?>€</span>
                                                                                <?php } elseif( $i == "poste" && isset($dep[$i])){
                                                                                    if(strpos(strtolower($dep[$i]), "subvention") !== false || strpos(strtolower($dep[$i]), "subventions") !== false ){
                                                                                        $showSubvetion = true;
                                                                                        $totalSubvention += (float) @$answers["answers"]["aapStep1"]["depense"][$depId]["price"];
                                                                                    }
                                                                                    ?>
                                                                                <?php echo $dep[$i] ?>
                                                                                <?php } ?>
                                                                        </td>
                                                                    <?php  } ?>
                                                                   
                                                                </tr>
                                                            <?php  }  ?>
                                                            <?php if(count($answers["answers"]["aapStep1"]["depense"]) < count($answers["answers"]["aapStep1"]["budget"])){
                                                                $dif = count($answers["answers"]["aapStep1"]["budget"]) - count($answers["answers"]["aapStep1"]["depense"]);
                                                                for($i = 1; $i <= $dif;$i++ ){
                                                                    ?>
                                                                    <tr>
                                                                    <td class="text-bleu"height="85"> &nbsp; </td>
                                                                    <td class="text-bleu"height="85"> &nbsp; </td>

                                                                    </tr>
                                                                <?php }
                                                            }?>
                                                            <tr style="background-color:#dcdcdc ;">
                                                                <td class="text-bleu" style="text-align:right ">Total :   </td>
                                                                <td class="text-bleu" > <span>   <?php echo trim(strrev(chunk_split(strrev($totalPrice),3, ' '))) ?></span>  €</td>
                                                            </tr>
                                                            <?php if($showSubvetion){ ?>
                                                            <tr style="background-color:#dff0d8">
                                                                <td colspan="4"  >L'association sollicite une subvention totale de <b  ><?= $totalSubvention ?> € </b></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </table>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        </table>
                                    <?php } ?>
                                <?php }?>
                            </td>
                        </tr>
                    </table>
                <?php } ?><br><br>
                <?php if($aapModality) {?>
                    <table cellspacing="5" cellpadding="5">
                        <tr> 
                            <td style="background-color: #ddd ;border:1px solid black;"> Modalités de mise en oeuvre du projet </td> 
                        </tr>
                        <tr>
                            <td  style="border-left:3px solid black;" >
                            <?php foreach($aapModality as $kMd => $vMd){?>
                            <br> <b class="title-input"><?php echo $vMd["label"]?>:</b> 
                            <?php if (isset($answers["answers"][$vMd["step"]][$vMd["value"]]) && !is_array($answers["answers"][$vMd["step"]][$vMd["value"]])) {
                                    
                                    $inputsBr = str_replace("\n", "<br>", $answers["answers"][$vMd["step"]][$vMd["value"]]);                                
                                     echo ' <span class="text-input"> '.$inputsBr .'</span>';
                                }else if(isset($answers["answers"][$vMd["step"]][$vMd["value"]]) && is_array($answers["answers"][$vMd["step"]][$vMd["value"]]) && $vMd["value"]!="budget" && $vMd["value"]!="depense" && $vMd["value"]!="adress"){
                                    if($vMd["type"] == "tpls.forms.cplx.element") {
                                
                                    }else{
                                        foreach($answers["answers"][$vMd["step"]][$vMd["value"]] as $kf => $vf){
                                            $vfBr = str_replace("\n", "<br>", $vf); 
                                            echo '<br> <span class="text-input"> - '.$vfBr .'</span>';
                                        }
                                    }
                                } ?>
                            <?php }?>
                            </td>
                        </tr>
                    </table>
                <?php } ?><br><br>
                <?php if($aapStep2) { ?>
                    <table  cellspacing="7" cellpadding="7">
                        <tr>
                            <td style=" background-color: #93C020 ;border:2px solid black; text-align:center"> <b>SELECTION</b> </td>
                        </tr>
                    </table>   
                    <table>
                        <tr>
                            <td>
                                <?php foreach($aapStep2 as $kS2 => $vS2){?>
                                    <?php if($vS2["value"] == "decide"){
                                        echo '<br><br><b class=" title-input-tableau">'.$vS2["label"].':</b><br><br>';
                                    }else{ 
                                        echo '<br><b class=" title-input">'.$vS2["label"].':</b>';
                                    } ?>
                                    <?php if (isset($answers["answers"][$vS2["step"]][$vS2["value"]]) && !is_array($answers["answers"][$vS2["step"]][$vS2["value"]])) {
                                        $inputsBr = str_replace("\n", "<br>", $answers["answers"][$vS2["step"]][$vS2["value"]]);                                
                                         echo ' <span class="text-input"> '.$inputsBr .'</span>';
                                    }else if(isset($answers["answers"][$vS2["step"]][$vS2["value"]]) && is_array($answers["answers"][$vS2["step"]][$vS2["value"]]) && $vS2["value"]!="budget" && $vS2["value"]!="depense" && $vS2["value"]!="adress"){
                                        if($vS2["type"] == "tpls.forms.cplx.element") {
                                        }else{
                                            foreach($answers["answers"][$vS2["step"]][$vS2["value"]] as $kf => $vf){
                                                $vfBr = str_replace("\n", "<br>", $vf); 
                                                echo '<br> <span class="text-input"> - '.$vfBr .'</span>';
                                            }
                                        }
                                    }else if($vS2["value"] == "decide"){                                        
                                        if(isset($parentForm["inputConfig"]["multiDecide"]) && $parentForm["inputConfig"]["multiDecide"] =="tpls.forms.aap.selection"){?>
                                            <table border="1"  cellpadding="2"> 
                                                <tr class="trBg">
                                                    <th colspan="2"><br>Critère (Coeff) <br> </th>
                                                    <th colspan="1"><br>Valeur <br></th>
                                                    <th colspan="1"><br>Note sur <?= $noteMax ?> <br></th>
                                                </tr>
                                                <tbody>
                                                    <?php foreach ($acceptedPreparedData as $kprep => $vprep) {
                                                    
                                                        $currentNote =  isset($answers["answers"]["aapStep2"]["selection"][Yii::app()->session['userId']][$kprep]) ? $answers["answers"]["aapStep2"]["selection"][Yii::app()->session['userId']][$kprep] : "";
                                                        $showValues = "";
                                                        if(!(array_keys($vprep["value"]) !== range(0, count($vprep["value"]) - 1)) && is_array($vprep["value"])){ //sequantial array
                                                            foreach ($vprep["value"] as $kk => $vv) {
                                                                $showValues .= "- ".$vv."<br>";
                                                            }
                                                        }elseif(is_string($vprep["value"]) || is_int($vprep["value"]) || is_float($vprep["value"])){
                                                            $showValues = $vprep["value"];
                                                        }
        
                                                        //specific auto evaluation
                                                        if(@$this->costum["contextSlug"]=="coSindniSmarterre"){
                                                            if(($kprep =="axesTFPB" && is_string($showValues) && strpos(strtolower($showValues),"axe 5") !== false) ||
                                                                ($kprep =="aapStep1l1xfdtuazek4jhgcco" && is_string($showValues) && strpos(strtolower($showValues),strtolower("Aucune de ces spécificités")) === false && $showValues !="")
                                                                ){
                                                                $currentNote = 5;
                                                            }   
                                                        }?>
                                                        <tr>
                                                            <td colspan="2" class="text-bleu"><br><b>  <?= !empty(@$vprep["fieldLabel"]) ?@$vprep["fieldLabel"] : @$vprep["label"] ?></b> <span class="class="text-bleu"">(<?= @$vprep["coeff"] ?>)</span><br></td>
                                                            <td colspan="1" class="text-bleu" style="text-align:center ;"><br><?= $showValues ?><br></td>
        
                                                            <!-- note -->
                                                            <?php if($voteType == "noteCriterionBased"){ ?>
                                                                <td colspan="1" class="text-bleu" style="text-align:center ;">
                                                                    <?= $currentNote  ?>
                                                                </td>
                                                            <?php } ?>
        
                                                            <!-- star -->
                                                            <?php if($voteType == "starCriterionBased"){ ?>
                                                                <td colspan="1">
                                                                    <div class="text-bleu" style="text-align:center ;">
                                                                        <?php 
                                                                        if($currentNote != ''){
                                                                            echo $currentNote." étoiles"; 
                                                                        }?> 
                                                                    </div>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td colspan="2" >&nbsp;</td><td class="bg-green"><b><br>Mon vote(Moy) :</b> <br></td><td class=" bg-green"><br>  <?=  $myMean ?><br></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td><td class="bg-green"><b><br>Tous les votants(Moy) :</b> <br></td><td class=" bg-green"><br>  <?= $allUserMean?><br></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <?php }
        
                                    }
                                }?>
                            </td>
                        </tr>
                    </table>   
                <?php } ?><br>
                <?php if($aapStep3) { ?>
                    <table  cellspacing="7" cellpadding="7">
                        <tr>
                            <td style=" background-color: #93C020 ;border:2px solid black; text-align:center"> <b>FINANCEMENT</b> </td>
                        </tr>
                    </table>  
                    <table>
                        <tr>
                            <td>
                                <?php foreach($aapStep3 as $kS3 => $vS3){?>
 
                                    <br><b class="title-input-tableau"><?php echo $vS3["label"]?>:</b>
                                    <?php if (isset($answers["answers"][$vS3["step"]][$vS3["value"]]) && !is_array($answers["answers"][$vS3["step"]][$vS3["value"]])) {
                                    
                                        $inputsBr = str_replace("\n", "<br>", $answers["answers"][$vS3["step"]][$vS3["value"]]);
                                        echo ' <span class="text-input"> '.$inputsBr .'</span>'
                                        ?>                                
                                   
                                    <?php } ?>
                                    <?php if($vS3["value"] == "financer"){  
                                        echo "<br>";
                                        $mappingInput = "";
                                        $mappingForm = "";
                                        if (isset($parentForm["mapping"]["depense"]))
                                        {
                                            $mapping = $parentForm["mapping"]["depense"];
                                            $mappingExplode = explode(".", $mapping);
                                            $mappingInput = $mappingExplode[2];
                                            $mappingForm = $mappingExplode[1];
                                        }

                                        if(isset($depense)) {
                                            foreach($depense as $kd => $vd){ 
                                                ?>
                                                <table border="0"  cellspacing="10" cellpadding="5" style="background-color:#dbdbdb ;">
                                                    <tr>
                                                        <td colspan="2" class="text-bleu" style="font-size:14px ;">
                                                            <?php if (isset($vd["poste"])) echo $vd["poste"];?>
                                                        </td>
                                                        <td colspan="2" class="text-bleu">
                                                                <?php 
                                                                if(isset($totalfi[$kd]))
                                                                    echo $totalfi[$kd]."€";
                                                                else
                                                                    echo "0€";
                                                                if(isset($price[$kd]))
                                                                    echo "/ ".$price[$kd]."€";                        
                                                                ?>
                                                                (<?php
                                                                        if (empty($vd["price"])){
                                                                            $vd["price"] = 0;
                                                                        }
                                                                        if (isset($vd["price"]) && !empty($vd["price"]) || $vd["price"] == 0 && isset($totalde[$kd]) && is_numeric($totalde[$kd]))
                                                                    {
                                                                        if($totalde[$kd] == 0){

                                                                        }
                                                                        echo rtrim(rtrim(number_format($totalde[$kd], 3, ".", " ") , '0') , '.');
                                                                    } ?>€ dépensé) <br>
                                                            <?php 
                                                            if ($depensed[$kd] < $percent[$kd]){
                                                                ?>
                                                                <?php if( $depensed[$kd] != 0){?>
                                                                    <span style="font-size:14px;background-color:#2C3E50;font-weight:bold;color:#ffffff">  Dépensé <?php echo $depensed[$kd] ?> %  </span>
                                                                <?php } ;
                                                                if($percent[$kd] - $depensed[$kd] != 0){?>
                                                                        <span style="font-size:14px;background-color:#93C020;font-weight:bold">  Financé <?php echo $percent[$kd] - $depensed[$kd] ?> %  </span>
                                                                <?php } 
                                                            } elseif($depensed[$kd] = $percent[$kd]) { ?>
                                                                <?php if($percent[$kd] != 0){?>
                                                                        <span style="font-size: 14px;background-color:#2C3E50;font-weight:bold;color:#ffffff">  Depensé <?php echo $percent[$kd] ?> %  </span>
                                                                <?php }
                                                            } else { ?>
                                                                <?php if($percent[$kd] != 0){?>
                                                                        <span style="font-size: 14px;background-color:#93C020;font-weight:bold">  Financé <?php echo $percent[$kd] ?> %  </span>                                    
                                                                <?php }
                                                                if($depensed[$kd] - $percent[$kd] != 0){ ?>
                                                                    <span style="font-size:14px;background-color:#c3ccba;" class="text-bleu">  Non financé <?php echo $depensed[$kd] - $percent[$kd] ?> %  </span>
                                                                <?php }
                                                            }?>
                                                                <span style="font-size: 14px;background-color:#c3ccba;" class="text-bleu">  Non financé <?php echo 100 - ($percent[$kd] ) ?> %   </span>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                <?php  if(isset($vd["financer"])) {?>
                                                    <div> 
                                                        <table border="1">
                                                            <tbody>
                                                                <?php foreach ($vd["financer"] as $ix => $fin) { ?>
                                                                    <tr>
                                                                        <td colspan='2' class="text-bleu"><br>
                                                                            <?php if (!empty($fin["id"])) {
                                                                                $o = PHDB::findOne(Organization::COLLECTION, ["_id" => new MongoId($fin["id"])], ["name", "slug","profilImageUrl"]);?>
                                                                                <?php //var_dump((String)$o["_id"]);?>
                                                                                <?php if(isset($o["profilImageUrl"])) {
                                                                                    $src = $o["profilImageUrl"]
                                                                                    ?>
                                                                                    <img style="height:auto; width:40px ; " src = "<?=$src?>"><br>
                                                                                <?php }?> 
                                                                                <?php  echo $o["name"] ?>
                                                                            <?php } else if (isset($fin["name"]) && !empty($fin["email"])) { ?>
                                                                                <?= $fin["name"] ?> <small class='text-muted'> <?= $fin["email"] ?></small>
                                                                            <?php } else if (isset($parentForm["parent"])) { 
                                                                                foreach ($parentForm["parent"] as $fp => $fv) {
                                                                                    $o = PHDB::findOne($fv["type"], ["_id" => new MongoId($fp)], ["name", "slug"]);?>
                                                                                    <?= $o["name"] ?>
                                                                                <?php }?>
                                                                            <?php } ?><br>
                                                                        </td>
                                                                        <td colspan='2' class="text-bleu"><br>
                                                                            <?php if (!empty($fin["line"])) { 
                                                                                echo $fin["line"];
                                                                            } ?><br>
                                                                        </td>
                                                                        <td colspan='2' class="text-bleu"><br>
                                                                            <?php if (!empty($fin["amount"])) { 
                                                                                echo$fin["amount"] . "€";
                                                                            } ?><br>
                                                                        </td>
                                                                        <td colspan='2' class="text-bleu"><br>
                                                                            <?php if (!empty($fin["amount"]) && isset($vd["price"])) {
                                                                                $percents = $fin["amount"] * 100 / (int)$vd["price"];?>
                                                                                <span><?= floor($percents)?> %</span>
                                                                            <?php } ?><br>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                <?php }  echo "<div></div>"  ;     ?>
                                                
                                            <?php } ?>                         
                                       <?php } ?>
                                        
                                    <?php  }else if($vS3["value"] == "generateproject"){
                                        echo "<br>";
                                        if(isset($answers["project"]["id"])){
                                            $el = Element::getElementById($answers["project"]["id"],Project::COLLECTION,null,["name","description","shortDescription", "profilImageUrl","profilMediumImageUrl"] );
                                            if (isset($el["profiImageUrl"])){?>
                                                <div class="text-center">
                                                    <img src="<?php echo Yii::app()->createUrl('/'.$el["profilImageUrl"]); ?>" style="width:100px;height:auto"/>
                                                </div>

                                                <?php 
                                            } else {?>
                                                <br><span class='text-bleu' style="font-size:13px">- <?= @$el["name"] ?></span>
                                            <?php }
                                        }
                                    }
                                }?>
                            </td>
                        </tr>
                    </table>   
                <?php } ?> <br>
                <?php if($aapStep4){?>
                    <table  cellspacing="7" cellpadding="7">
                        <tr>
                            <td style=" background-color: #93C020 ;border:2px solid black; text-align:center"> <b>SUIVI</b>     </td>
                        </tr>
                    </table>   
                    <table>
                        <tr>
                            <td>
                                <?php foreach($aapStep4 as $kS4 => $vS4){?>
                                    <br><b class="title-input-tableau"><?php echo $vS4["label"]?>:</b> <br><br> 
                                    <?php if($vS4["value"] == "suivredepense"){ 
                                        if(isset($answers["project"]["id"])){
                                            $projectId = "";
                                            $actions = [];
                                            $project = [];
                                            if (isset($answers["project"]["id"]) && MongoId::isValid($answers["project"]["id"])){
                                                $projectId = $answers["project"]["id"];
                                                $project = PHDB::findOneById(Project::COLLECTION, $projectId);
                                                $actions = PHDB::find(Actions::COLLECTION, ["parentId" => $projectId , "parentType" => Project::COLLECTION]);                                
                                            }                               
                                            if (!empty($answers["answers"]["aapStep1"]["depense"])){?>
                                                    <?php $icont = 1;
                                                    foreach ($answers["answers"]["aapStep1"]["depense"] as $depId => $dep){?>
                                                        <table  border="0"   style="background-color:#dbdbdb" >
                                                            <tr>
                                                                <?php if (isset($dep["validFinal"]["valid"]) && $dep["validFinal"]["valid"] == "validated" ){?>
                                                                    <td colspan="2"  style="color: #7da53d; font-size: 14px">
                                                                        <?php if(isset($dep["poste"])) echo $dep["poste"]; ?>
                                                                    </td>
                                                                <?php } else { ?>
                                                                    <td colspan="2" class="text-blue" style="font-size: 14px">
                                                                        <?php if(isset($dep["poste"])) echo $dep["poste"]; ?>
                                                                    </td>
                                                                <?php } ?>
                                                                <td colspan="1" >
                                                                    <?php
                                                                        $totaltask = 0;
                                                                        $completedtask = 0;
                                                                        $allactid = "";
                                                                        if (!empty($answers["answers"])) {
                                                                            if (isset($answers["project"]["id"])){
                                                                                if (!empty($projectId) && MongoId::isValid($projectId)) {
                                                                                    if (!isset($dep["actionid"])) {
                                                                                        $actions = PHDB::find(Actions::COLLECTION, ["parentId" => $projectId, "parentType" => Project::COLLECTION, "name" => $dep["poste"]]);
                                                                                    }else{
                                                                                        $actions = PHDB::findOneById(Actions::COLLECTION, $dep["actionid"]);
                                                                                        $actions = [$actions];
                                                                                    }
                                                                                }else{
                                                                                    $actions = [];
                                                                                }
                                                                                foreach ($actions as $aid => $aval){
                                                                                    if (!isset($dep["actionid"])) {
                                                                                        $allactid = $aid;
                                                                                    }else{
                                                                                        $allactid = $dep["actionid"];
                                                                                    }
                                                                                }

                                                                                $actions = array_reduce($actions, function ($carry, $item){
                                                                                    if (!empty($item["tasks"])){
                                                                                        $carry = array_merge($carry, $item["tasks"]);
                                                                                    }
                                                                                    return $carry;
                                                                                }, []);
                                                                                foreach ($actions as $uid => $esti){
                                                                                    if (isset($esti["checked"]) && $esti["checked"] == "true"){
                                                                                        $completedtask++;
                                                                                    }
                                                                                    $totaltask++;
                                                                                }
                                                                            }
                                                                        }
                                                                        $progress = 0;
                                                                        if ($completedtask != 0 && $totaltask != 0){
                                                                            $progress = intval(($completedtask / $totaltask) * 100);
                                                                        }
                                                                        if (intval($progress) > 0){?>
                                                                            <span style="font-size:14px;background-color:#93C020;font-weight:bold"> <?php echo $progress ?>%</span>
                                                                        <?php } ?>
                                                                </td>
                                                                <?php  if (isset($dep["validFinal"]["valid"]) && $dep["validFinal"]["valid"] == "validated" ){ ?>
                                                                    <td style="border:solid 2px #7da53d;color: #7da53d;font-size:15px;" >
                                                                        Terminer 
                                                                    </td>
                                                                <?php } ?>
                                                                <?php if (!empty($allactid)){
                                                                    $communityLinksAct = Element::getCommunityByTypeAndId(Actions::COLLECTION, $allactid);
                                                                }
                                                                if (isset($communityLinksAct[Yii::app()->session["userId"]])){ ?>
                                                                    <td colspan="1" style="border:solid 2px #7da53d;color: #7da53d;font-size:15px;" >
                                                                        Participant
                                                                    </td>
                                                                <?php } ?>
                                                            </tr>  
                                                        </table>
                                                        <table border="1">
                                                            <?php
                                                            $i = 1;
                                                            foreach ($actions as $actionid => $action){
                                                                if(isset($action["checked"]) && $action["checked"] == "true"){ 
                                                                ?> 
                                                                    <tr style="background-color:#dbdbdb">
                                                                <?php } else{ ?>
                                                                    <tr>
                                                                <?php } ?>
                                                                    <td>
                                                                        <span class="text-bleu"><?php echo @$action["task"] ?> </span>
                                                                    </td>
                                                                    <td>
                                                                        <span class="text-bleu"><?php echo @$action["credits"] ?> €</span>
                                                                    </td>
                                                                    <td>
                                                                        <?php if (isset($action["contributors"])){ ?>
                                                                            <span class="text-bleu" style="font-style: italic;">Assigné à : &nbsp;</span>
                                                                            <?php foreach ($action["contributors"] as $ckey => $cvalue){
                                                                                $wrkr = PHDB::findOneById(Person::COLLECTION, $ckey);
                                                                                if (isset($wrkr["profilImageUrl"])){?>
                                                                                    <span class="text-bleu"><?php echo $wrkr["username"] ?></span>
                                                                                    <img  src="<?php echo Yii::app()->createUrl('/' . $wrkr["profilImageUrl"]) ?>" style="height:30px; width:30px;">  
                                                                                <?php }else{?>
                                                                                    <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl . '/images/thumb/default_organizations.png' ?>"  style="height:30px; width:30px;">
                                                                                    <span class="text-bleu"><?php echo $wrkr["username"] ?></span>
                                                                                <?php } ?>
                                                                                <?php $i++;
                                                                            }
                                                                        }?>
                                                                    </td>
                                                                    <td>
                                                                        <?php
                                                                        $endDateaction = "";
                                                                        if (!empty($action["endDate"])) {
                                                                            if (gettype($action["endDate"]) == "string") {
                                                                                $endDateaction = $action["endDate"];
                                                                            } elseif (gettype($action["endDate"]) == "object") {
                                                                                $endDateaction = date('d-m-Y', $action["endDate"]->sec);
                                                                            }
                                                                        } ?>
                                                                        <span class="text-bleu" style="font-style: italic;"> <?php if (!empty($endDateaction)){ ?>date limite : <b><?php echo ($endDateaction) ?></b>  <?php } ?> </span>
                                                                    </td>

                                                                    <?php if(isset($action["checked"]) && $action["checked"] == "true"){ ?> 
                                                                        <td class="text-green">
                                                                    <?php } else{ ?>
                                                                        <td>
                                                                    <?php } ?>
                                                                        <?php if(isset($action["checked"]) && $action["checked"] == "true"){  ?>
                                                                            <br><span  >    Terminé</span>
                                                                    <?php } ?>
                                                                    </td>
                                                                </tr><br>
                                                            <?php  }?>
                                                        </table> 
                                                        <div></div>
                                                    <?php }?> 
                                            <?php }
                                        }
                                    }
                                  } ?>
                            </td>
                        </tr>
                    </table> 
                <?php } ?>
            <?php } ?>
     <?php if(isset($parentForm["textAfterPdf"])){ ?>
        <p class="text-bleu">    <?= $parentForm["textAfterPdf"] ?></p>
    <?php } ?>
    

</div>
    <table>
        <tr><td></td></tr>
    </table>
    <div style=" text-align: right;float: right;"> 
    
        <?php if(isset($depense)) {
            foreach($depense as $kdd => $vdd){ 
                ?>
                <?php  if(isset($vdd["financer"])) {  
                    foreach ($vdd["financer"] as $ixa => $fina) { 
                    ?> 
                        <?php if (isset($fina["id"])) { 
                            $orga = PHDB::findOne(Organization::COLLECTION, ["_id" => new MongoId($fina["id"])], ["name", "slug","profilImageUrl"]);?>
                            <?php //var_dump((String)$o["_id"]);?>
                            <?php if(isset($orga["profilImageUrl"])) {
                                $src = $orga["profilImageUrl"]
                                ?>
                                <img  style="object-fit:contain" src ="<?=$src?>" width="60" height="60">
                            <?php }
                        } 
                    } 
                } 
            }
        }?>
        <?php if(isset($imageSign)){ ?>
            <img  style="object-fit:contain" src = "<?= $imageSign?>" width="60" height="60"> 
        <?php } ?>
        
    </div>