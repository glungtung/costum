<style>
    .portfolio-modal .modal-content .aap-tpl-container {
        text-align: initial;
    }
    .blogicon
    {
        font-size: 217px;
        color: #5CB85C;
    }

    .ratetext
    {
        font-size: 37px;
        text-decoration: underline;
        padding-bottom: 10px;
    }

    .votes
    {
        font-size: 47px;
        padding-right: 20px;
        color: #197BB5;
    }

    a.list-group-item
    {
        height: auto;
        min-height: 150px;
    }

    a.list-group-item:hover, a.list-group-item:focus
    {
        border-left: 10px solid #5CB85C;
        border-right: 10px solid #5CB85C;
    }

    a.list-group-item
    {
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
    }
</style>
<?php
$el = $contextData;
$template = PHDB::find(Form::COLLECTION,array(
    "parent.".$el['_id']['$id'] => array('$exists'=> true),
    "type" => "template"
));
//var_dump($template);
?>
<div class="col-md-12 col-xs-12 aap-tpl-container margin-top-30">
    <div class="row">
        <div class="col-xs-12 padding-top-5 padding-bottom-10">
            <button type="button" class="btn btn-block btn-lg btn-primary generateAapConfig"> <i class="fa fa-plus"></i> &nbsp; Créer un template pour votre <?= $el["collection"]." ".$el["name"] ?> </button>
        </div>
    </div>
    <div class="row">
        <div class="well">
            <div class="list-group">
                <?php //var_dump($contextData"]) ?>
                <?php
                if(isset($results)){
                    foreach ($results as $idtemp => $vtemp) {
                        //var_dump($vtemp);
                        $name = @$vtemp["name"];
                        $description =  !empty($vtemp["description"]) ? $vtemp["description"] : "Pas de description";
                        $formParent = [
                            "name" => $name,
                            "description" => $description,
                            "type" => "templatechild",
                            "config"=> $idtemp,
                            "parent" => [
                                $el["id"] => [
                                    "type" => $el["collection"],
                                    "name" => $el["name"]
                                ]
                            ]
                        ];

                        ?>
                        <a href="javascript:;" class="list-group-item margin-bottom-5 generateParentFormTemplate" data-form='<?= json_encode($formParent) ?>' data-template='<?= $idtemp ?>'>
                            <div class="media col-md-3">
                                <figure class="pull-left">
                                    <!-- <i class="fa fa-cloud-upload blogicon"></i> -->
                                    <img src="<?= Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg"; ?>" alt="" style="height:128px;width:auto"/>
                                </figure>
                            </div>
                            <div class="col-md-6">
                                <h4 class="list-group-item-heading"><?= $name ?></h4>
                                <p class="list-group-item-text">
                                    <?= $description ?>
                                </p>
                            </div>
                            <div class="col-md-3 text-center">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <!-- <div class="ratetext">20 utilisateur</div> -->
                                        <!-- <i class="fa fa-thumbs-o-up votes"></i><i class="fa fa-thumbs-o-down votes"></i> -->
                                        <!-- <div class="stars"></div> -->
                                        <h2>  <small></small></h2>
                                        <div id="rate1"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php }
                }
                ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        var  copyFormObj =formObj.init();
        $('.generateParentFormTemplate').off().on('click',function(){
            $this = $(this);
            bootbox.prompt({
                title: "Les changements faite dans le template originale serons automatiquement appliquée",
                /*message: '<p>Please select an option below:</p>',*/
                inputType: 'radio',
                inputOptions: [
                    {
                        text: 'Confirmer',
                        value: 'confirm',
                    },
                    {
                        text: 'Annuler',
                        value: 'cancel',
                    },
                ],
                callback: function (result) {
                    if(result == "confirm"){
                        var formToGenerate = $this.data("form");

                        var tplCtx = {
                            collection: "forms",
                            value: formToGenerate,
                            format: true,
                            path: "allToRoot"
                        };
                        dataHelper.path2Value(tplCtx, function (params) {
                            if(params.result){
                                toastr.success("Formulaire créé");
                                urlCtrl.loadByHash(location.hash);
                            }
                        });
                    }else if(result == "cancel"){

                    }
                }
            });
        });

    })
</script>