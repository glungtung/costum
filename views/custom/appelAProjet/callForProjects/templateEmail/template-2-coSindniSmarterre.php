<style>@media (min-width: 1200px){.email-template-container {width: 1170px;}}@media (min-width: 992px){.email-template-container {width: 970px;}}</style>
<div class="email-template-container" style="background-color:#fff;padding-right: 15px;padding:0 50px;margin: auto;">
    <p style="text-align:left">
        <img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/logo.png" width="60" height="auto" alt="" />
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        <b>Direction Générale Ville Citoyenne <br/>
        Direction du Développement des territoires <br/>
        Politique de la Ville/Contrat de Ville <br/><br/></b>
        Affaire suivie par : Magalie CESBRON <br/>
        politique.ville@saintdenis.re <br/>
        0262 40 45 30
    </p>

    <table style="width:100%">
        <tr>
            <td style="width: 50%;">&nbsp;</td>
            <td style="width: 50%;font-size:9px">
                <br/>
                A Saint-Denis le, {prop.today} <br/><br/>
                Madame la Présidente, <br/>
                Monsieur le Président, <br/>
                De l’association : {prop.association}
            </td>
        </tr>
    </table><br/>
    <p style="font-size:9px;margin:0px;">
        <br/>
        <u>Objet :</u> <b>Notification d’attribution de subvention dans le cadre de l’Appel à Projet Contrat de Ville 2022</b>
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Madame la Présidente,<br/>
        Monsieur le Président,
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
    Votre association a répondu à l’appel à projet lancé par la Ville de Saint-Denis dans le cadre de la Politique de la Ville 2022.  
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Nous vous remercions de l’intérêt que vous portez, à nos côtés, au développement du territoire dionysien. <br/>
        Ce sont plus de 400 projets qui ont été déposés et analysés. 
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Le projet <b><i>"{prop.title}"</i></b> proposé par votre association a retenu toute notre attention et  a eté validé lors du Conseil Municipal du 23 juin 2022. 
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        En effet, celui-ci  correspond aux enjeux et axes prioritaires identifiés par la Ville. 
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Une subvention de <b>{prop.subvention}</b> vous a été accordée.  
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Pour rappel, c’est un acompte qui vous sera versé dans un premier temps. Ce projet devra faire l’objet d’un bilan pour l’obtention  du solde. 
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Veuillez agréer, Madame la Présidente, Monsieur le Président, nos salutations distinguées.
    </p>
    <p style="text-align:right">
        <img src="{prop.signature}" width="auto" height="120" alt="">
    </p>
    <img style="display:none" src="{prop.fakeImg}" alt="">
</div>
<p style="text-align:center;display:pdf">
    <a target="_blank" href="<?= Yii::app()->getRequest()->getBaseUrl(true)?>/costum/aap/attachedfile/answerid/{prop.id}/template/template-2-coSindniSmarterre">Télécharger le fichier PDF</a>
</p>