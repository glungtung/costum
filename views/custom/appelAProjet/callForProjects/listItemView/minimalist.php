<?php 
    $name = $ansVar["name"];
    $description = $ansVar["description"];
    $completedFields = $ansVar["completedFields"];
    $percentageTasks = $ansVar["percentageTasks"];
    $dataPer = $ansVar["dataPer"];
    $iconTasks = $ansVar["iconTasks"];
    $haveDepense = $ansVar["haveDepense"];
    $nbFinancedDepense  = $ansVar["nbFinancedDepense"];
    $seen = $ansVar["seen"];
    $haveProject = $ansVar["haveProject"];
    $countTask = $ansVar["countTask"];
    $imgAnsw = $ansVar["imgAnsw"];
    $retain = $ansVar["retain"];
    $actions  = $ansVar["actions"];
    $urgentState  = $ansVar["urgentState"];
    $cummulMean  = $ansVar["cummulMean"];
    $votantCounter  = $ansVar["votantCounter"];
    $actions = $ansVar["actions"];
    if(!empty($anscoremuVar)){
        $coremu = $anscoremuVar["coremu"];
        $needCandidate = $anscoremuVar["needCandidate"];
        $coremuLDD = $anscoremuVar["coremuLDD"];
        $totalprice = $anscoremuVar["totalprice"];
        $totalvalid = $anscoremuVar["totalvalid"];
        $precentageenveloppe = round((($totalvalid / $totalprice) * 100) , 2) + 0;
    }
?>
<div data-pos="<?= $pos ?>" class="row list-group-item"  style="margin-bottom:20px; <?= $haveProject['is'] ? 'background-color:#7da53d42' :'' ?>" data-label="nouv." id="list-<?= (string) $answer["_id"] ?>" data-id="<?= (string) $answer["_id"] ?>">
    <?php if(empty($answer["views"][Yii::app()->session['userId']])){ ?> 
        <div class="ribbon ribbon-top-left"><span>New</span></div>
    <?php } ?>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center" style="background-image:url('<?=@$imgAnsw?>');background-position: center;background-size: cover;background-repeat: no-repeat; min-height: 80px">
    </div>
    
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-top-5">
        <!-- <h4 class="list-group-item-heading">Blog Title</h4> -->
        <h4 class="list-group-item-heading"  style="position: inherit; z-index: 1; width: 88%;display: flex;font-size:13px">
            <a href="javascript:;" data-url = 'slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string) $answer["_id"] ?>' class="aapgetaapview aaptitlegoto minimalist-name" data-id="<?= (string) $answer["_id"] ?>" style="display:inline; white-space: break-spaces;"> <?php echo $name ?> </a>
            <a href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo $slug; ?>#oceco.slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string) $answer["_id"] ?>" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Ouvrir dans un nouvel onglet">
                <i class=" open-external-link fa fa-external-link-square"></i>
            </a>
        </h4>
        <?php 
            $classForBtn = '';
            if(count($actions) > 0 || $haveProject['is']) {
                $classForBtn = "pos-top-max";
            }
            if(count($actions) === 0 && $haveProject['is'] === false) {
                $classForBtn = "pos-top-min";
            }

        ?>
        <div class="row prop-btn-group <?= $classForBtn ?>" style="display: flex; justify-content: end; position: absolute; width: 100%; right: 18px; top: 8px">
            <div class="<?=count($actions) === 0 ? 'without-action': 'with-action'?>"><?= $haveProject['is'] ? $haveProject["html"] : ''?></div>
            <?php if(count($actions) > 0) { ?>
                <div class="prop-compact-progression" style="<?=count($actions) === 0 ? "display:none" : "display:inline-block; transform: scale(0.8)" ?>">
                    <div class="progress lt-progress" data-percentage="<?php echo $dataPer ?>">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">
                            <div>
                                <?php echo $percentageTasks ?><br>
                                <span><i class="fa <?php echo $iconTasks ?>"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.listItemButtons",array("answer" => $answer,"data" => $data, "ansVar"=>$ansVar)) ?>
        </div>

        <ul class="tag-list margin-top-5">
            <?php
            if (!empty($tags) && is_array($tags)){
                foreach ($tags as $ktag => $vtag){
                    if(!empty($vtag))
                    { ?>
                        <a href="javascript:;" class="btn-tag-panel template-tag-panel" data-tag="<?=$vtag ?>">
                            <span class="badge  btn-tag tag" data-tag-value="cte" data-tag-label="cte">#<?=$vtag ?></span>
                        </a>
                        <?php
                    }
                }
            }
            ?>
        </ul>
    </div>

    <div class=" no-padding text-center col-xs-12">
        <div class="col-xs-12">
            <hr class="margin-0 margin-bottom-5" style="border-top: 1px dashed #ddd;margin: 0 auto; width: 100%;">
            <div class=" no-padding panel-body evaluator-panel col-xs-12 padding-5" style="display: flex;align-items: center;min-height: 50px;">
                <div class="col-xs-12 text-right padding-left-5 padding-right-5  no-padding">
                    <div class="small col-xs-12 padding-bottom-0 padding-top-5 collapse-container" >
                        <?php if($coremu){ ?>
                            <div class="padding-left-10 padding-right-10 text-center coremu-progress">
                                    Enveloppe<br>
                                <div class="progress-bar-outer" style="background: #dceeef;">
                                    <div class="progress-inner">
                                        <div class="progress" style="background: #00b7f9;">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: <?= $precentageenveloppe?>%;">
                                                <?= $precentageenveloppe?>%
                                            </div>
                                        </div>
                                        <div class="progress-value"><?= $totalvalid?>€ / <?= $totalprice ?>€</div>
                                    </div>
                                </div>
                            </div>

                            <div class="or-spacer-vertical left">
                                <div class="mask"></div>
                            </div>

                            <div class="padding-left-10 padding-right-10 text-center">
                                    Candidature<br>
                                <div class="dropdown">
                                    <div>
                                        <?php if (@$needCandidate){ echo "<span style='color: blue' class='tooltips' data-toggle='tooltip' data-html='true' data-placement='bottom' data-original-title='En appel à candidature'><i class='fa fa-user-plus'></i> En appel</span>"; }; ?>
                                    </div>
                                    <div>
                                        <button class="btn btn-xs getcoremutable" type="button" data-depenseid="<?php echo $answerid ?>" >
                                            Voir
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="or-spacer-vertical left">
                                <div class="mask"></div>
                            </div>

                        <?php } ?>

                        <div class="padding-left-10 padding-right-10 text-center">
                            Evaluation <br> 
                            <span class="bold" style="color: #000"><?= $votantCounter ?></span>
                        </div>
                        <div class="padding-left-10 padding-right-10 text-center">
                            Financement <br> 
                            <span class="bold" style="color: #000"><?= $nbFinancedDepense."/". count(@$answer["answers"]["aapStep1"]["depense"]) ?></span>
                        </div>
                        <div class="padding-left-10 padding-right-10 text-center">
                            Action <br> 
                            <span class="bold" style="color: #000"><?= count($actions) ?></span>
                        </div>

                        <div class="or-spacer-vertical left">
                            <div class="mask"></div>
                        </div>

                        <?php //echo Aap::budgetStatus($answer,$data["el"],$data["inputs"]); ?>
                        <?php echo Aap::budgetStatus2($answer,$data["el"],$data["inputs"])["html"]; ?>
                        <br>
                        <br>
                        <?php echo Aap::contributors($answer)["html"];?>

                        <div class="or-spacer-vertical left">
                            <div class="mask"></div>
                        </div>

                        <div class="padding-top-5 padding-left-10 padding-right-10">
                            <div class="rater-admin text-center tooltips" data-rating="<?= is_numeric($cummulMean) ? $cummulMean : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?=htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . (is_numeric($cummulMean) ? round($cummulMean, 3) : 0) ." / 5" ?>"></div><br>
                            <div class="col-xs-12 no-padding ">
                                <!-- <hr class="margin-0" style="border-top: 1px dashed #ddd;margin: 0 auto; width: 100%;"> -->
                                <?php echo Aap::heartVote($answer,$data["el"],$data["elform"],$data["me"]) ?>
                                <?php if(isset(Yii::app()->session['userId'])){ ?>
                                    <span class="btn-group-custom-container pull-right">
                                    <a href="javascript:;" id="btn-comment-<?= (string)$answerid ?>" class=" btn-sm margin-right-5 openAnswersComment tooltips btn-custom" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="font-size :12px;border:0">
                                        <?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?> <i class='fa fa-commenting'></i>
                                    </a>
                                    </span>
                                <?php } ?>
                                <a href="javascript:;" class="like-project pull-right tooltips margin-right-10" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Nombre de visite">
                                    <i class="fa fa-eye"></i> <span id ="countv"  style="font-size: 12px"> <?php echo (isset($answer["visitor_counter"]["count"]) ? $answer["visitor_counter"]["count"] : 0 ) ?> </span>
                                </a>
                
                                <a href="javascript:;" class="votant-modal pull-right tooltips margin-right-10" data-subtype="<?= $data["elform"]["subType"] ?>" data-answerid="<?= (string) $answer["_id"] ?>">
                                    <i class="fa fa-gavel"></i> <span id ="countv"  style="font-size: 12px"> <?= $votantCounter ?> </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>