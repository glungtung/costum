<?php
    $name = $ansVar["name"];
    $description = $ansVar["description"];
    $completedFields = $ansVar["completedFields"];
    $percentageTasks = $ansVar["percentageTasks"];
    $dataPer = $ansVar["dataPer"];
    $iconTasks = $ansVar["iconTasks"];
    $haveDepense = $ansVar["haveDepense"];
    $nbFinancedDepense  = $ansVar["nbFinancedDepense"];
    $seen = $ansVar["seen"];
    $haveProject = $ansVar["haveProject"];
    $countTask = $ansVar["countTask"];
    $imgAnsw = $ansVar["imgAnsw"];
    $retain = $ansVar["retain"];
    $actions  = $ansVar["actions"];
    $urgentState  = $ansVar["urgentState"];
    $cummulMean  = $ansVar["cummulMean"];
    $votantCounter  = $ansVar["votantCounter"];
    $actions = $ansVar["actions"];
    if(!empty($anscoremuVar)){
        $coremu = $anscoremuVar["coremu"];
        $needCandidate = $anscoremuVar["needCandidate"];
        $coremuLDD = $anscoremuVar["coremuLDD"];
        $totalprice = $anscoremuVar["totalprice"];
        $totalvalid = $anscoremuVar["totalvalid"];
        $precentageenveloppe = round((($totalvalid / $totalprice) * 100) , 2) + 0;
        if(is_nan($precentageenveloppe)){
            $precentageenveloppe = 0;
        }
    }
?>

<div data-pos="<?= $pos ?>" class="row list-group-item list-group-item-detailed"  style="margin-bottom:20px; <?= $haveProject['is'] ? 'background-color:#7da53d42' :'' ?>" data-label="nouv." id="list-<?= (string) $answer["_id"] ?>" data-id="<?= (string) $answer["_id"] ?>">
    <?php if(empty($answer["views"][Yii::app()->session['userId']])){ ?>
        <div class="ribbon ribbon-top-left"><span>New</span></div>
    <?php } ?>
    <div class="media col-md-4 col-lg-4 no-padding">
        <div class="contain-img-left no-padding bg-grey">
            <figure class="bg-dark">
                <a href="javascript:;" data-url = 'slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string) $answer["_id"] ?>' class="aapgetaapview aaptitlegoto" data-id="<?= (string) $answer["_id"] ?>">
                    <img class="lzy_img" data-src="<?=$imgAnsw?>" src="<?php echo Yii::app()->controller->module->assetsUrl ?>/images/thumbnail-default.jpg" alt="projet" class="img-responsive " data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$data["costum"]["slug"]; ?>#oceco.slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.form.answer.<?php echo (string)$answerid; ?>" style="width: 100%;">
                </a>
            </figure>
        </div>
    </div>

    <div class="col-md-8 col-lg-8 col-xs-12 padding-top-5">
        <h4 class="list-group-item-heading"  style='position: inherit; z-index: 1; width:83%'>
            <a href="javascript:;" data-url = 'slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string) $answer["_id"] ?>' class="aapgetaapview aaptitlegoto minimalist-name" data-id="<?= (string) $answer["_id"] ?>" style="display:inline; white-space: break-spaces;"> <?php echo $name ?> </a>
            <a href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo $slug; ?>#oceco.slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string) $answer["_id"] ?>" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Ouvrir dans un nouvel onglet">
                <i class=" open-external-link fa fa-external-link-square"></i>
            </a>
        </h4>
            <?php
                $classForBtn = '';
                if(count($actions) > 0 || $haveProject['is']) {
                    $classForBtn = "pos-top-max-detailed";
                }
                if(count($actions) === 0 && $haveProject['is'] === false) {
                    $classForBtn = "pos-top-min-detailed";
                }
            ?>
            <div class="row prop-btn-group <?= $classForBtn ?>" style="display: flex; justify-content: end; position: absolute; width: 100%; right: 18px; top: 8px">
                <div class="<?=count($actions) === 0 ? 'without-action': 'with-action'?>"><?= $haveProject['is'] ? $haveProject["html"] : ''?></div>

                <?php
                    if(count($actions) > 0) {
                ?>
                    <div class="list-item-progress prop-compact-progression" style="<?=count($actions) === 0 ? "display:none" : "display:inline-block;float:right;" ?>">
                        <div class="progress lt-progress" data-percentage="<?php echo $dataPer ?>">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                            <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>
                            <div class="progress-value">
                                <div>
                                    <?php echo $percentageTasks ?><br>
                                    <span><i class="fa <?php echo $iconTasks ?>"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>
                <?php echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.listItemButtons",array("answer" => $answer,"data" => $data, "ansVar"=>$ansVar)) ?>
            </div>

            <?php if($data["isActiveSousOrga"]){ ?>
                <small>Parent : <a href="#page.type.<?= Aap::cacs($data["cacs"],$answer)['cacValue']['type'] ?>.id.<?= Aap::cacs($data["cacs"],$answer)['cacId'] ?>" class="letter-green lbh-preview-element"><?= !empty(Aap::cacs($data["cacs"],$answer)['cacValue']['name']) ? Aap::cacs($data["cacs"],$answer)['cacValue']['name'] : "" ?></a></small><br>
            <?php } ?>
            <?php if(!empty($answer["answers"]["aapStep1"]["association"])){ ?>
                <small>Association : <a href="javascript:;" class="letter-green btn-search-association"><?= $answer["answers"]["aapStep1"]["association"] ?></a></small><br>
            <?php } ?>
            <small><i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?=@$data["users"][$answer["user"]]["name"] ?></i></b>&nbsp;&nbsp;&nbsp;&nbsp; Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y  ', $answer["created"]); ?></i></b>&nbsp; à &nbsp;<i class="fa fa-clock-o" style="color: #7da53d"></i> <b><i><?php echo date('H:i:s ', $answer["created"]); ?></i></b> </small><br>

            <hr style="border-top: 2px dashed #3f4e58;margin-bottom: 10px;">

            <small>
                <i><b>
                        <?=count($actions) !== 0 && isset($answer["projectEndDateStory"]) && count($answer["projectEndDateStory"]) !== 0 ? "La date de fin a été changé " . count($answer["projectEndDateStory"]) . " fois" : "" ?>
                </i></b>
            </small>

            <div id="less-<?= (string) $answer["_id"] ?>" class="list-group-item-text project-description list-item-description list-markdown margin-left-15"><?=substr($description, 0, 200) ?><?= strlen($description) > 200 ? '<b>. . .</b> <button type="button" onclick="document.getElementById(\'less-'.(string) $answer["_id"].'\').style.display = \'none\';document.getElementById(\'more-'.(string) $answer["_id"].'\').style.display = \'table\';" class="moreLinkaap btn-xs bg-transparent">'.Yii::t("form","Read more").'</button>' : "" ?>
            </div>
            <?php if(strlen($description) > 200){ ?>
                <div id="more-<?= (string) $answer["_id"] ?>" style="display:none" class="list-group-item-text project-description list-item-description list-markdown margin-left-15"><?= $description ?><?=  '<button type="button" onclick="document.getElementById(\'less-'.(string) $answer["_id"].'\').style.display = \'table\';document.getElementById(\'more-'.(string) $answer["_id"].'\').style.display = \'none\';" class="lessLinkaap btn-xs bg-transparent">'.Yii::t("common","Read less").'</button>' ?>
                </div>
            <?php } ?>

            <ul class="tag-list margin-top-5">
                <?php
                if (!empty($tags) && is_array($tags)){
                    foreach ($tags as $ktag => $vtag){
                        if(!empty($vtag))
                        { ?>
                            <a href="javascript:;" class="btn-tag-panel template-tag-panel" data-tag="<?=$vtag ?>">
                                <span class="badge  btn-tag tag" data-tag-value="cte" data-tag-label="cte">#<?=$vtag ?></span>
                            </a>
                            <?php
                        }
                    }
                }
                ?>
            </ul>
    </div>

    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12 pull-right">
        <div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 statustags-container no-padding" >
                <span class="padding-5">
                    <small>
                        <i><?php
                        if(!empty($answer["status"])){
                            if(empty($answer["statusUpdated"]) && !empty($answer["updated"])){
                                $answer["statusUpdated"] = $answer["updated"];
                            }else if(!empty($answer["statusUpdated"]) && !empty($answer["updated"])){
                                $answer["statusUpdated"] = time();
                            }
                            $answer["statusUpdated"] = $answer["updated"];
                            $dt = new DateTime("now", new DateTimeZone($timezone));
                            $dt->setTimestamp($answer["statusUpdated"]);
                            echo Yii::t("common","{what} updated on {date}",array("{what}" => "Statut","{date}"=>$dt->format('d/m/Y, H:i:s')));
                        }
                        ?>
                        </i><br>
                    </small>
                </span>
                <span class='label <?= @$urgentState == "Urgent" ? " urgent-state" : "hidden" ?> '><i class="fa fa-exclamation-triangle"></i>&nbsp;<?= @$urgentState ?></span>
                <select data-id="<?= $answerid ?>" class="statustags" multiple disabled>
                    <option disabled></option>
                    <?php
                    /*if(!empty($answer["status"]) && in_array("notified",$answer["status"]) && in_array("notificationSent",$answer["status"])){
                        $answer["status"] = array_diff($answer["status"],["notificationSent"]);
                    }*/
                    foreach (Aap::$badgelabel as $bdgid => $bdglbl){
                        ?>
                        <option <?php if (!empty($answer["status"]) && in_array($bdgid, $answer["status"])) { echo "selected"; } ?> value="<?php echo $bdgid ?>"><?php  echo $bdglbl ?></option>
                    <?php } ?>

                </select>
                <?php if ($data["canAdmin"]){ ?>
                    <div class="col-xs-12 no-padding">
                    <a href="javascript:;" class="btn btn-xs st-enable-btn" data-loc="edit" data-id="<?= $answerid ?>" type="button" ><i class="fa fa-pencil"></i> Modifier status</a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class=" no-padding text-center col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="col-xs-12">
            <hr class="margin-0 margin-bottom-5 hidden" style="border-top: 1px dashed #ddd;margin: 0 auto; width: 100%;">


            <div class="panel-group margin-top-5">
                <div class="panel panel-default">
                    <div class="panel-heading padding-top-5 padding-bottom-5 " style="<?= !empty($answer["project"]["id"]) ? "background-color: #7da53d42;" : "" ?>" >
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse1<?= $answerid ?>"><i class="fa fa-chevron-down"></i></a>
                    </h4>
                    </div>
                    <div id="collapse1<?= $answerid ?>" class="panel-collapse collapse collapse-detailed">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 padding-bottom-5">
                            <?php if(!empty($data["inputs"]["inputs"])){
                                $percentField = round(($completedFields*100)/count($data["inputs"]["inputs"]));

                                ?>
                                <span class="pull-right small"><?= $completedFields ?> complète(s) / <?= count($data["inputs"]["inputs"])  ?> champ(s) <b class="<?= $percentField >= 50 ? "text-green":"text-red" ?>"><?= $percentField ?>%</b></span>
                            <?php } ?>
                        </div>
                        <div class=" no-padding panel-body evaluator-panel col-xs-12 ">
                            <div class="col-xs-12 text-right padding-left-5 padding-right-5">
                                <div class="small col-xs-12 padding-bottom-0 padding-top-5 collapse-container" >
                                    <?php if($coremu){ ?>
                                        <div class="padding-left-10 padding-right-10 text-center coremu-progress">
                                                Enveloppe<br>
                                            <div class="progress-bar-outer" style="background: #dceeef;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #00b7f9;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: <?= $precentageenveloppe?>%;">
                                                            <?= $precentageenveloppe ?>%
                                                        </div>
                                                    </div>
                                                    <div class="progress-value"><?= $totalvalid?>€ / <?= $totalprice ?>€</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="or-spacer-vertical left">
                                            <div class="mask"></div>
                                        </div>

                                        <div class="padding-left-10 padding-right-10 text-center">
                                                Candidature<br>


                                            <div class="dropdown">
                                                <div>
                                                    <?php
                                                    $totalcandidattovalid = 0;
                                                    $totalpricetovalid = 0;
                                                    $totalneedcandidat = 0;
                                                    if(!empty($answer["answers"]["aapStep1"]["depense"])) {
                                                        $depenseAction = Aap::getDepenseAction($answer["answers"]["aapStep1"]["depense"]);
                                                        $totalcandidattovalid = $depenseAction["totalcandidattovalid"];
                                                        $totalpricetovalid = $depenseAction["totalpricetovalid"];
                                                        $totalneedcandidat = $depenseAction["totalneedcandidat"];
                                                    }

                                                    $totalansweraction = $totalcandidattovalid + $totalpricetovalid + $totalneedcandidat;
                                                    ?>
                                                    <button class="btn btn-xs getcoremutable" type="button" data-depenseid="<?php echo $answerid ?>" >
                                                        Voir
                                                        <span class="listnumberAction">
                                                            <?= $totalansweraction ?>
                                                        </span>
                                                    </button>
                                                </div>
                                                <div>
                                                    <?php if (@$needCandidate){ echo "<span style='color: blue' class='tooltips' data-toggle='tooltip' data-html='true' data-placement='bottom' data-original-title='En appel à candidature'><i class='fa fa-user-plus'></i> En appel</span>"; }; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="or-spacer-vertical left">
                                            <div class="mask"></div>
                                        </div>

                                    <?php } ?>

                                    <div class="padding-left-10 padding-right-10 text-center">
                                        Evaluation <br>
                                        <span class="bold" style="color: #000"><?= $votantCounter ?></span>
                                    </div>
                                    <div class="padding-left-10 padding-right-10 text-center">
                                        Financement <br>
                                        <span class="bold" style="color: #000"><?= $nbFinancedDepense."/". count(@$answer["answers"]["aapStep1"]["depense"]) ?></span>
                                    </div>
                                    <div class="padding-left-10 padding-right-10 text-center">
                                        Action <br>
                                        <span class="bold" style="color: #000"><?= count($actions) ?></span>
                                    </div>

                                    <div class="or-spacer-vertical left">
                                        <div class="mask"></div>
                                    </div>

                                    <?php //echo Aap::budgetStatus($answer,$el,$data["inputs"]); ?>
                                    <?php echo Aap::budgetStatus2($answer,$data["el"],$data["inputs"])["html"]; ?>
                                    <br>
                                    <br>
                                    <?php echo Aap::contributors($answer)["html"];?>

                                    <div class="or-spacer-vertical left">
                                        <div class="mask"></div>
                                    </div>

                                    <div class="padding-top-5 padding-left-10 padding-right-10">
                                        <div class="rater-admin text-center tooltips" data-rating="<?= is_numeric($cummulMean) ? $cummulMean : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?=htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . (is_numeric($cummulMean) ? round($cummulMean, 3) : 0) ." / 5" ?>"></div><br>
                                        <div class="col-xs-12 no-padding ">
                                            <!-- <hr class="margin-0" style="border-top: 1px dashed #ddd;margin: 0 auto; width: 100%;"> -->
                                            <?php echo Aap::heartVote($answer,$data["el"],$data["elform"],$data["me"]) ?>
                                            <?php if(isset(Yii::app()->session['userId'])){ ?>
                                                <span class="btn-group-custom-container pull-right">
                                                <a href="javascript:;" id="btn-comment-<?= (string)$answerid ?>" class=" btn-sm margin-right-5 openAnswersComment tooltips btn-custom" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="font-size :12px;border:0">
                                                    <?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?> <i class='fa fa-commenting'></i>
                                                </a>
                                                </span>
                                            <?php } ?>
                                            <a href="javascript:;" class="like-project pull-right tooltips margin-right-10" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Nombre de visite">
                                                <i class="fa fa-eye"></i> <span id ="countv"  style="font-size: 12px"> <?php echo (isset($answer["visitor_counter"]["count"]) ? $answer["visitor_counter"]["count"] : 0 ) ?> </span>
                                            </a>

                                            <a href="javascript:;" class="votant-modal pull-right tooltips margin-right-10" data-subtype="<?= $data["elform"]["subType"] ?>" data-answerid="<?= (string) $answer["_id"] ?>">
                                                <i class="fa fa-gavel"></i> <span id ="countv"  style="font-size: 12px"> <?= $votantCounter ?> </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
