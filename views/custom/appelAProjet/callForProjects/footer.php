<div class="aap-footer">
  <div class="row">
  <hr>
    <div class="col-xs-12 margin-bottom-20 text-center">
      <div class="col-md-8">
        <!-- <a href="#">Terms of Service</a> | <a href="#">Privacy</a>     -->
        Je suis un : 
        <?php 
            if($el["myLinks"]===null || (!empty($el["myLinks"]["toBeValidated"]))) echo " <strong>Visiteur</strong> |";
            else echo " <strong>Membre</strong> |";
            if((isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) || Form::canAdmin((string)$elform["_id"])) echo " <strong>Admin</strong> |";
            if(!isset($el["myLinks"]["toBeValidated"]) && !empty($el["myLinks"]["roles"]) && is_array($el["myLinks"]["roles"])){
                foreach ($el["myLinks"]["roles"] as $kmr => $vmr) {
                    echo " <strong>".$vmr."</strong> |";
                }
            }
        ?>
      </div>
      <div class="col-md-4">
        <!-- <p class="muted pull-right">© 2013 Company Name. All rights reserved</p> -->
      </div>
    </div>
  </div>
</div>