<?php 
    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/jkanban.css","/css/aap/kanban.css","/js/blockcms/jkanban.js"], Yii::app()->getModule('costum')->assetsUrl);
?>
<div class="col-xs-12">
    <div class="kanban-container">
        <h5>Proposition</h5>
        <div id="myKanban1">
            <div></div>
        </div>
        <div id="myKanban" class="margin-bottom-30"></div>
    </div>
</div>

<script>
    $(function(){
        var elFormId = <?= json_encode($el_form_id) ?>;
        var isFormAdmin = <?= json_encode((isset(Yii::app()->session['userId']) && Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) || Form::canAdmin((string)$el_form_id)) ?>;
        var allAns = <?= json_encode($allAnswers) ?>;
        var elSlug = <?= json_encode($el_slug) ?>;  
        
        var kanbanProjectObj = {
            proposeItems : [],
            underconstructionItems:[],
            evaluationItems:[],
            projectstateItems:[],
            financeItems:[],
            progressItems:[],
            finishedItems:[],
            suspendedItems:[],
            prevalidedItems:[],
            validprojectItems:[],
            votedItems:[],
            init: function(kanObj){
                kanbanProjectObj.initItems(kanbanProjectObj);
                kanbanProjectObj.initKanban(kanbanProjectObj);
                kanbanProjectObj.events(kanbanProjectObj);
            },
            itemsAction : function(k,v){
                var seen = false;
                if (notEmpty(userId) && exists(v["visitor_counter"]) && exists(v["visitor_counter"]["user"]) && v["visitor_counter"]["user"].indexOf(userId) != -1){
                    seen = true ;
                }
                if (notEmpty(userId) && exists(v["user"]) && v["user"] == userId ){
                    seen = true ;
                }
                return {
                    id: k,
                    title: v.answers.aapStep1.titre,
                    /*`<div class="col-xs-12">
                        ${v.answers.aapStep1.titre}
                    </div>`*/
                    drag: function(el, source) {
                        mylog.log("START DRAG: " + el.dataset.eid);
                    },
                    dragend: function(el) {
                        mylog.log("END DRAG: " + el.dataset.eid);
                    },
                    drop: function(el, target, source, sibling) {
                        if(isFormAdmin)
                            dataHelper.path2Value({
                                id : el.dataset.eid,
                                collection : "answers",
                                path : "status",
                                value : [target.parentNode.dataset.id],
                            }, function(params) {
                                toastr.success(trad.saved);
                            });
                        else
                            toastr.error(tradDynForm["You are not authorized"]);
                        mylog.log("DROPPED: " + el.dataset.eid);
                        mylog.log("rakoto",target.parentNode.dataset.id);
                    },
                    click: function(el) {
                        // alert("click"+v.answers.aapStep1.titre);
                    },
                    context: function(el, e){
                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                    },
                    class: [
                        "cursor-pointer",
                        "aapgetaapview",
                        seen == false ? (notEmpty(userId)?"unseen-proposition":"seen-proposition") :"seen-proposition"
                    ],
                    label:"nouv",
                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.aappage.sheet.answer.'+k,
                    status: v.status
                }
            },
            initItems : function(kanObj){
                var proposeItem = [];
                $.each(allAns,(k,v)=>{
                    if(exists(v.answers) ){
                        mylog.log("prepareBoard",v.answers.aapStep1);
                        if(typeof v.status == "undefined" || (exists(v.status) && v.status.length == 0 || v.status.indexOf("newaction") != -1))
                            kanObj.proposeItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("suspend") != -1 )
                            kanObj.suspendedItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("underconstruction") != -1 )
                            kanObj.underconstructionItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("projectstate") != -1 )
                            kanObj.projectstateItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("finish") != -1 )
                            kanObj.finishedItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("progress") != -1)
                            kanObj.progressItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("finance") != -1)
                            kanObj.financeItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("vote") != -1)
                            kanObj.evaluationItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("prevalided") != -1)
                            kanObj.prevalidedItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("validproject") != -1)
                            kanObj.validprojectItems.push({...kanObj.itemsAction(k,v)});
                        else if(v.status.indexOf("voted") != -1)
                            kanObj.votedItems.push({...kanObj.itemsAction(k,v)});
                    } 
                })
            },
            events : function(kanObj){
                var kanbanContainerWidth= $("#myKanban .kanban-container" ).width()
                $("#myKanban1 div").css("width",kanbanContainerWidth);
                $("#myKanban1").scroll(function(){
                    $("#myKanban").scrollLeft($("#myKanban1").scrollLeft());
                });
                $("#myKanban").scroll(function(){
                    $("#myKanban1").scrollLeft($("#myKanban").scrollLeft());
                });

                $('.aapgetaapview').off().on('click',function(e){
                    e.stopPropagation();
                    coInterface.showLoader(".aapinputcontent");
                    var ansurl = $(this).data('url');
                    smallMenu.openAjaxHTML(baseUrl+'/survey/form/getaapview/urll/'+ansurl);
                });
                setTimeout(() => {
                    $(".list-aap-container .d-flex").removeClass("container").addClass("container-fluid").css("width","100%");
                    $('.content-answer-search').removeClass("col-md-9 col-lg-9")
                }, 900);
                
                // $.getScript( "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.0/slick.js", function( data, textStatus, jqxhr ) {
                    
                // })
            },
            initKanban: function(kanbanProjectObj){
                var dragTo = ["progress","finished","abandoned","newaction","call","projectstate","finance","vote","underconstruction","prevalided","validproject","voted"];
                var KanbanTest = new jKanban({
                    element: "#myKanban",
                    gutter: "10px",
                    widthBoard: "300px",
                    itemHandleOptions:{
                        enabled: true,
                        // customCssHandler:"",
                        // customCssIconHandler:"fa fa-bars",
                        // customItemLayout:""
                    },
                    click: function(el) {
                        mylog.log("Trigger on all items click!");
                    },
                    context: function(el, e) {
                        mylog.log("Trigger on all items right-click!");
                    },
                    dropEl: function(el, target, source, sibling){
                        mylog.log(target.parentElement.getAttribute('data-id'));
                        mylog.log(el, target, source, sibling)
                    },
                    buttonClick: function(el, boardId) {
                        // var ansurl = "slug.formcarryconfig1.formid.6180e84cddcd1223be6ec912.aappage.form";
                        // smallMenu.openAjaxHTML(baseUrl+'/survey/form/getaapview/urll/'+ansurl);
                        
                    },
                    itemAddOptions: {
                        // enabled: true,
                        // content: '+ Nouvelle proposition',
                        // class: 'custom-button',
                        // header: true,
                        // footer:true
                    },
                    boards: [
                        {
                            id: "newaction",
                            title: "Proposer <i class='fa fa-envelope pull-right'></i>",
                            class: "info,good",
                            dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.proposeItems
                        },
                        {
                            id: "underconstruction",
                            title: "En construction <i class='fa fa-exclamation-triangle pull-right'></i>",  
                            class: "info",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.underconstructionItems
                        },
                        {
                            id: "vote",
                            title: "En évaluation <i class='fa fa-gavel pull-right'></i>",
                            class: "warning",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.evaluationItems
                        },
                        {
                            id: "projectstate",
                            title: "En projet <i class='fa fa-exclamation-triangle pull-right'></i>",  
                            class: "dark",
                            dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.projectstateItems
                        },
                        {
                            id: "voted",
                            title: "Voté <i class='fa fa-gavel pull-right'></i>",
                            class: "warning",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.votedItems
                        },
                        {
                            id: "finance",
                            title: "Financement <i class='fa fa-euro pull-right'></i>",
                            class: "dark",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.financeItems
                        },
                        {
                            id: "prevalided",
                            title: "Pré validé <i class='fa fa-gavel pull-right'></i>",
                            class: "warning",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.prevalidedItems
                        },
                        {
                            id: "validproject",
                            title: "Projet validé <i class='fa fa-gavel pull-right'></i>",
                            class: "dark",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.validprojectItems
                        },
                        {
                            id: "progress",
                            title: "En cours <i class='fa fa-hourglass fa-pulse pull-right'></i>",
                            class: "info",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.progressItems
                        },
                        {
                            id: "finish",
                            title: "Terminé <i class='fa fa-check pull-right'></i>",
                            class: "success",
                             dragTo: isFormAdmin ? dragTo :[],
                            item: kanbanProjectObj.finishedItems
                        },
                        {
                            id: "suspend",
                            title: "Suspendu <i class='fa fa-exclamation-triangle pull-right'></i>",  
                            class: "error",
                             dragTo: isFormAdmin ? dragTo :[], 
                            item: kanbanProjectObj.suspendedItems
                        }
                    ]
                });
            // var toDoButton = document.getElementById("addToDo");
                // toDoButton.addEventListener("click", function() {
                //     KanbanTest.addElement("newaction", {
                //         title: "Test Add"
                //     });
                // });

                // var toDoButtonAtPosition = document.getElementById("addToDoAtPosition");
                // toDoButtonAtPosition.addEventListener("click", function() {
                //     KanbanTest.addElement("newaction", {
                //         title: "Test Add at Pos"
                //     }, 1);
                // });

                // var addBoardDefault = document.getElementById("addDefault");
                // addBoardDefault.addEventListener("click", function() {
                //     KanbanTest.addBoards([
                //         {
                //         id: "_default",
                //         title: "Kanban Default",
                //         item: [
                //             {
                //             title: "Default Item"
                //             },
                //             {
                //             title: "Default Item 2"
                //             },
                //             {
                //             title: "Default Item 3"
                //             }
                //         ]
                //         }
                //     ]);
                // });

                // var removeBoard = document.getElementById("removeBoard");
                // removeBoard.addEventListener("click", function() {
                //     KanbanTest.removeBoard("finance");
                // });

                // var removeElement = document.getElementById("removeElement");
                // removeElement.addEventListener("click", function() {
                //     KanbanTest.removeElement("_test_delete");
                // });

                // var allEle = KanbanTest.getBoardElements("newaction");
                // allEle.forEach(function(item, index) {
                // //mylog.log(item);
                // });
            }
        }

        //************************************************************************* */
        kanbanProjectObj.init(kanbanProjectObj);
        function __onButtonClickHandler (nodeItem, boardId) {
        nodeItem.addEventListener('click', function (e) {
            e.preventDefault()
            alert("gova");
        })
        }
    })
</script>