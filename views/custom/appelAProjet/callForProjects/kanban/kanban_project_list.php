<?php
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/jkanban.css", "/css/aap/kanban.css", "/js/blockcms/jkanban.js"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(['/plugins/clipboard/clipboard.min.js'], Yii::app()->request->baseUrl);
$timeline_files = [
    '/plugins/jquery-confirm/jquery-confirm.min.css',
    '/plugins/jquery-confirm/jquery-confirm.min.js',
];
HtmlHelper::registerCssAndScriptsFiles($timeline_files, Yii::app()->request->baseUrl);
if (!isset($showProjectPanel))
    $showProjectPanel = true;
else
    $showProjectPanel = ($showProjectPanel == "false" ? false : true);
?>
<style> 
    body .bootbox.modal {
        z-index: 2000000;
    }

    .btn-delete-action-img {
        position: absolute;
        top: 10px;
        right: 10px;
    }

    .footerSearchContainer,
    .headerSearchleft {
        display: none;
    }

    #modal-preview-comment .title-comment {
        text-transform: none !important;
        font-size: 15px !important;
        width: 83.33333333% !important;
    }

    .kanban-container .kanban-item {
        padding: 0px !important;
        background-color: #fff0;
        cursor: pointer;
    }

    .kanban-container .drag_handler {
        background: #302f2f00;
        top: 1px;
        z-index: 1;
        width: 17px;
        display: none;
        margin-right: 4px;
    }

    .kanban-container .drag_handler .drag_handler_icon {
        width: 13px;
    }

    /*#myKanban .kanban-container{
        width: 100% !important;
        display: flex;
        flex-direction: row;
    }
    #myKanban .kanban-container .kanban-board{
        flex-grow: 1;
    }*/
    .kanban-container .full-table,
    .kanban-container .cancel-action,
    .kanban-container .edit-action {
        display: none;
    }

    .tasks-modal {
        position: fixed;
        width: 80%;
        z-index: 100000;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        /* border: 1px solid; */
        padding: 20px !important;
        background: #fff;
        box-shadow: 0px 0px 47px 10px;
        border-radius: 5px;
    }

    @media only screen and (max-width: 600px) {
        .tasks-modal {
            width: 100%;
        }
    }

    .kanban-project-list {
        position: relative;
        height: 579px;
        overflow-y: auto;
    }

    .kanban-project-list-container ul {
        background-color: #80808036;
    }

    .kanban-project-list-container .action-name {
        position: relative;
        display: flow-root;
    }

    .showHide-filters-xs {
        display: none !important;
    }

    .kanban-board-header {
        text-align: left;
        cursor: pointer;
    }

    .kanban-item {
        margin-bottom: 10px !important
    }

    #myKanban .kanban-board .kanban-drag {
        /*min-height: 200px;*/
        /* padding: 20px; */
        height: 456px;
        position: relative;
        overflow-y: auto;
        overflow-x: hidden;
        padding: 10px 20px;
        display: flex;
        flex-direction: column;
    }

    .kanban-container footer {
        margin-top: 0px !important
    }

    .kanban-container .kanban-item:hover .drag_handler,
    .kanban-container .kanban-item:hover .full-table,
    .kanban-container .kanban-item:hover .cancel-action,
    .kanban-container .kanban-item:hover .edit-action {
        display: inline;
    }

    .kanban-contributors {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 21px;
    }

    .kanban-contributors img {
        width: 20px;
        height: 20px;
        border-radius: 50%;
        position: absolute;
        top: 0;
    }

    .kanban-board header {
        padding: 0 !important;
    }

    .kanban-board header>.kanban-title-board>span {
        padding: 15px !important;
        display: block;
    }

    #centerFilters-active [data-type="projectid"],
    #centerFilters-active [data-type="user"],
    #centerFilters-active [data-type="showprojectpanel"] {
        display: none;
    }

    .select2-drop-multi {
        z-index: 10000000;
    }
    .portfolio-modal .close-modal .lr .rl, .portfolio-modal .close-modal .lr {
        background-color: #0c0c0c !important;
    }
    .modal-content>.close-modal{
        display: none;
    }
</style>

<?php if (!empty($showProjectPanel) && $showProjectPanel) { ?>
    <div id="act-detail"></div>
    <div class="col-xs-2 section-project-panel">
        <div class="kanban-project-list-container" >
            <h5><?= Yii::t("common","projects") ?></h5>
            <?php if($canAdmin){ ?>
                <button type="button" class="btn btn-success btn-block margin-bottom-10 kanban-add-project" >
                <?= Yii::t("common","Create a new project") ?>
                </button>
            <?php } ?>
            <input type="text" class="form-control search-project-field margin-bottom-5" placeholder="<?= ucfirst(Yii::t("common","Search")." ".Yii::t("common","Project")) ?>">
            <ul class="nav nav-pills nav-stacked projects-list kanban-project-list"></ul>
        </div>
    </div>
<?php } ?>
<div class="section-kanban-panel <?= $showProjectPanel ? "col-xs-10" : "col-xs-12"  ?> ">
    <div class="kanban-container">
        <h5 class="text-left">Actions personnelles de <span id="current-action-of"></span> <span id="current-project-of-text"></h5>
        <h5 class="text-left"><?= Yii::t("common", "project") ?>
            <span id="current-project" class="letter-green-k"></span>
            <span id="current-action-counter" class="letter-green-k"></span>
            <a href="javascript:;" class="btn btn-success bold" id="copy-project-link" data-clipboard-text="" data-clipboard-action="copy" data-toggle="tooltip" data-placement="bottom" data-original-title="Copier lien kanban du projet">
                <i class="fa fa-copy"></i>
            </a>
        </h5>
        <div class="col-xs-12 col-md-4 no-padding">
            <input type="text" class="form-control search-action-field margin-bottom-5" placeholder="<?= Yii::t("common", "Search") ?> action">
        </div>

        <input type="text" class="pull-right margin-right-10" id="search-person" placeholder="Voir l'action d'une personne">
        <div id="myKanban1">
            <div></div>
        </div>
        <div id="myKanban" class="margin-bottom-30">

        </div>
        <div class="loader-kanban hidden">
            <i class="fa fa-4x fa-spinner fa-spin"></i>
        </div>
    </div>
</div>

<script>
    $(function(){
        mylog.log(<?= json_encode($canAdmin)?>,"gratata");
        var arrAnswers = <?= !empty($arrAnswers) ? json_encode($arrAnswers) :  json_encode([])  ?>;
        let showProjectPanel = <?= !empty($showProjectPanel) ? $showProjectPanel : json_encode(false)  ?>;
        if (location.hash.indexOf("showprojectpanel=false") != -1)
            showProjectPanel = false;
        kanbanProjectObj = {
            kanban : null,
            defaultProject : null,
            userId : null,
            isAdmin : <?= !empty($canAdmin) ? json_encode($canAdmin) : json_encode(false) ?>,
            context : {},
            tags : {
                "totest" : "totest",
                "todiscuss" : "discuter",
            },
            init: function(kpObj) {
                kpObj.getCurrentUserIdInHash(kpObj);
                kpObj.getUserInfo(kpObj);
                kpObj.createProjectList(kpObj);
                kpObj.getActions(kpObj, [kpObj.defaultProject], {
                    many: true
                }, function(data) {
                    mylog.log(data, "mamimamy")
                    data = data.results;
                    kpObj.createBoardItemArray(kpObj, data);
                    kpObj.initKanban(kpObj);
                    kpObj.columnCounter(kpObj);
                    kpObj.events(kpObj);
                    coInterface.bindTooltips();
                })
            },
            getActions: function(kpObj, ids, options, callBack) {
                ajaxPost(
                    null,
                    baseUrl + "/costum/aap/actiondetail/", {
                        "ids": ids,
                        "userId": kpObj.userId
                    },
                    function(data) {
                        if (exists(options.many) && options.many)
                            kpObj.context.actions = data.results;
                        $('#current-action-counter').text(" (" + Object.keys(data.results).length + " action(s))")
                        callBack(data);
                        //$('.loader-kanban').hide();
                    }, null, null, {
                        async: false
                    }
                );
            },
            getProjects: function(kpObj, callBack) {
                ajaxPost(
                    null,
                    baseUrl+"/costum/aap/getprojectsbyanswers",
                    {
                        "answerIds" : arrAnswers,
                        "elt" : aapObject.elementAap.id
                    },
                    function(data){
                        kpObj.context.projects = data;
                        callBack(data);
                        //$('.loader-kanban').hide();
                    }, null, null, {
                        async: false
                    }
                );
            },
            getAnswerByprojectId: function(kpObj, idProject, callBack) {
                params = {
                    searchType: ["answers"],
                    filters: {
                        'project.id': idProject,
                    },
                    fields: ["answers.aapStep1.titre"]
                };
                ajaxPost(
                    null,
                    baseUrl + "/" + moduleId + "/search/globalautocomplete",
                    params,
                    function(data) {
                        callBack(data);
                    }, null, null, {
                        async: false
                    }
                )
            },
            getCurrentUserIdInHash: function(kpObj) {
                if (location.hash.indexOf("?") != -1) {
                    var params = location.hash.split("?")[1];
                    if (params.indexOf("&") != -1) {
                        var arr = params.split("&");
                        $.each(arr, function(k, v) {
                            if (v.indexOf("user=") != -1) {
                                var user = v.split("=")[1];
                                kpObj.userId = user;
                            }
                        })
                    }
                }
            },
            getUserInfo: function(kpObj) {
                if (kpObj.userId != null) {
                    ajaxPost('', baseUrl + "/co2/element/get/type/citoyens/id/" + kpObj.userId,
                        null,
                        function(data) {
                            $("#current-action-of").html(`<a href="#page.type.${data.map.collection}.id.${data.map._id.$id}" class="letter-green lbh-preview-element">${data.map.name}</a>`);
                            coInterface.bindLBHLinks();
                        }, null, null, {
                            async: false
                        });
                }
            },
            createTaskTable: function(kpObj, k, v) {
                var taskTable = "";
                v.realisedTask = 0;
                v.countTask = (exists(v.tasks) ? v.tasks.length : 0);
                if (exists(v.tasks) && Array.isArray(v.tasks) && v.tasks.length != 0) {
                    v.tasks.forEach(function(vv, kk) {
                        vv.endDate = (notNull(vv.endDate) && notNull(vv.endDate.sec)) ? (typeof vv.endDate == "object" ? moment.unix(vv.endDate.sec).format('DD/MM/YYYY') : vv.endDate) : "";
                        //vv.contributors = vv.contributors;
                        if (exists(vv.checked) && (vv.checked == true || vv.checked == "true")) {
                            v.realisedTask++;
                        }
                    })
                };
                return {
                    "realisedTask": v.realisedTask,
                    "countTask": v.countTask
                };
            },
            createCard: function(kpObj, k, v) {
                var tasksObj = kpObj.createTaskTable(kpObj, k, v);
                v.name = escapeHtml(v.name);
                if (v?.links)
                    mylog.log(v, "vname");
                var taskLabelColor = "label-default";
                if (tasksObj["realisedTask"] < tasksObj["countTask"])
                    taskLabelColor = "label-danger"
                else if ((tasksObj["realisedTask"] == tasksObj["countTask"]) && tasksObj["countTask"] != 0)
                    taskLabelColor = "label-success"

                var borderLeftColor = ``;
                if(typeof v.tags != "undefined" /*&& Array.isArray(v.tags)*/ && v.tags.includes("bug")){
                    borderLeftColor = "2px solid #d41818";
                } else if (typeof v.tags != "undefined" && Array.isArray(v.tags) && v.tags.includes("easy"))
                    borderLeftColor = "2px solid #188fd4";

                if(exists(v.tags) /*&& Array.isArray(v.tags)*/ && (v.tags.includes("bug") || v.tags.includes("Bug")))
                $("head style").last().append(`
                    .action${k}{
                        order : -${v.j}
                    }
                `);
                var card = '';
                card += `<div id="card-${k}" class="row bg-white padding-top-0 padding-left-5 padding-right-5 text-left" style="line-height:1;position:relative;cursor:pointer;border-left:${borderLeftColor}">
                        <p class="action-name letter-light" data-id="${k}" style="cursor:pointer"><b style="font-size: 14px;">${ucfirst(v.name)}</b>
                            <button class="btn btn-default btn-xs full-table hidden" style="position: absolute;top: 0;right: 0;border:0" data-value="false"><i class="fa fa-expand"></i></button>
                            <div class="dropdown dropdown-action-ellipsis-v margin-right-5 ${notNull(v.ans) ? '' : 'hidden'}">
                                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-v"></i></button>
                                <ul class="dropdown-menu action-plus no-padding">
                                    <li><a href="javascript:;" class="aapgetinputsa" data-id="${notNull(v.ans) ? v.ans._id.$id : ''}" data-form="${aapObject && aapObject.formParent && aapObject.formParent._id && aapObject.formParent._id.$id ? aapObject.formParent._id.$id : ''}" data-formstep="aapStep3" data-inputid="financer"><small>Financement</small></a></li>
                                    <li><a href="javascript:;" class="aapgoto" data-url="/costum/co/index/slug/${costum.slug}#oceco.slug.${aapObject && aapObject.elementAap && aapObject.elementAap.el && aapObject.elementAap.el.slug ? aapObject.elementAap.el.slug : ''}.formid.${aapObject && aapObject.formParent && aapObject.formParent._id && aapObject.formParent._id.$id ? aapObject.formParent._id.$id : ''}.aappage.form.answer.${notNull(v.ans) ? v.ans._id.$id : ''}"><small>Formulaire</small></a></li>
                                    <!-- <li><a href="#">JavaScript</a></li>-->
                                </ul>
                            </div>
                            <!-- <button class="btn btn-sm btn-default pull-right"><i class="fa fa-ellipsis-v"></i></button>-->
                        </p>
                        <div class="col-xs-12 show-detail no-padding line-height-2 hidden">
                            <span class="small label label-success"> credits : ${v.credits}</span>
                            <span class="small">
                                Auteur : 
                                <a href="#page.type.${notEmpty(v.userAuthor) && notEmpty(v.userAuthor.collection) ? v.userAuthor.collection : ""}.id.${exists(v.userAuthor) && exists(v.userAuthor._id) && exists(v.userAuthor._id.$id) ? v.userAuthor._id.$id : ''}" class="lbh-preview-element">
                                    ${(exists(v.userAuthor) && exists(v.userAuthor.name)) ? v.userAuthor.name :""}
                                </a>
                            </span>
                        </div>
                        <div class="col-xs-12 show-detail no-padding hidden">
                            <div class="panel-group padding-0">
                                <div class="panel panel-default">
                                    <div class="panel-heading padding-5">
                                    <h4 class="panel-title text-center panel-title-${k}">
                                        ${(exists(v.tasks) && Array.isArray(v.tasks) && v.tasks.length !=0 )?
                                            '<a data-toggle="collapse" href="#collapse'+k+'" ><small>TACHE(S) <i class="fa fa-caret-down"></i></small></a>' : 
                                            '<a data-toggle="collapse" href="#collapse'+k+'" style="visibility:hidden"><small>TACHE(S) <i class="fa fa-caret-down"></i></small></a>'
                                        }
                                        <a href="javascript:;" class="btn bg-green hover-white btn-sm new-task" data-new-pos="${0}" data-id="${k}" data-project-slug="${exists(v.parent)&& notNull(v.parent) && exists(v.parent.slug) ? v.parent.slug:''}" data-action-name="${v.name}"><i class="fa fa-plus"></i></a>
                                    </h4>
                                    </div>
                                    
                                        <div class="panel-body padding-0" style="overflow-x:auto">
                                            <table class="table text-muted">
                                                <thead>
                                                    <tr>
                                                        <th>&nbsp;</th>
                                                        <th><small>Tache</small></th>
                                                        <th><small>Contrib</small></th>
                                                        <th><small>Credits</small></th>
                                                        <th><small>Fin</small></th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody-${k}">
                                                `;

                //card +=                     tasksObj["taskTable"]  

                card += `</tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer hidden">Panel Footer</div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 text-right padding-bottom-10">
                            <a href="javascript:;" id="btn-comment-${k}" class="pull-right padding-left-5 margin-right-5 openAnswersComment tooltips" onclick='commentObj.openPreview("actions","${k}","", "${v.name}")' data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" >
                                <small>${exists(v) && exists(v.commentCount) ? v.commentCount : "" }</small> <i class='fa fa-commenting'></i> 
                            </a>`;

                card += `<a href="javascript:;" id="btn-image-${k}" data-id="${k}" class="pull-right padding-left-5 margin-right-5 image-action tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${(exists(v.media) && exists(v.media.images)) ? Object.keys(v.media.images).length : 0} image(s)" >
                                <small>${(exists(v.media) && exists(v.media.images)) ? Object.keys(v.media.images).length : ""}</small> <i class='fa fa-picture-o'></i> 
                            </a>`;
                card += `<a href="javascript:;" id="btn-file-${k}" data-id="${k}" class="pull-right padding-left-5 margin-right-5 file-action tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${(exists(v.media) && exists(v.media.files)) ? Object.keys(v.media.files).length : 0} ${trad.files}" >
                    <small>${(exists(v.media) && exists(v.media.files)) ? Object.keys(v.media.files).length : ""}</small> <i class='fa fa-file'></i> 
                </a>`;
                /*if(v?.links?.contributors){
                    
                }*/
                if (v?.links?.contributors) {
                    card += `<div class="kanban-contributors">`;
                    var j = 0;
                    $.each(v?.links?.contributors, function(kk, vv) {
                        card += `<img src="${vv.profilThumbImageUrl}" alt="${vv.name}" style="left:${j}px" class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${vv.name}"  />`;
                        j = j + 10;
                    })
                    card += `</div>`;
                }

                card += `<a href="javascript:;" class="pull-right padding-left-5 label ${taskLabelColor} tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.task}(s)" style="cursor:default"> ${tasksObj["realisedTask"]  +"/"+tasksObj["countTask"]}</a>

                            <a href="javascript:;" class="pull-left padding-left-5 label label-primary tooltips edit-action" data-action-id="${k}" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.edit}"><i class="fa fa-edit"></i></a>
                            <a href="javascript:;" class="pull-left padding-left-5 label label-danger tooltips cancel-action" data-action-id="${k}" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.cancel}"><i class="fa fa-times"></i></a>
                            <!-- <a href="javascript:;" class="padding-right-10 pull-right see-more-action" data-action-id="${k}" data-project-id="${v.parentId}"><i>${trad.seemore}</i></a> -->
                        </div>
                    </div>`;
                return card;
            },
            createBoardItemTemlate: function(kpObj, k, v) {
                return {
                    id: k,
                    class: ["action" + k],
                    title: kpObj.createCard(kpObj, k, v),
                    drag: function(el, source) {
                        mylog.log("START DRAG: " + el.dataset.eid);
                    },
                    dragend: function(el) {
                        mylog.log("END DRAG: " + el.dataset.eid);
                    },
                    drop: function(el, target, source, sibling) {
                        mylog.log(el.dataset.tags)

                        if (!viewParams.projectOf) {
                            target.tplCtx = {
                                id: el.dataset.eid,
                                collection: "actions",
                                path: "status",
                                value: target.parentNode.dataset.id,
                                format: true,
                            }
                            target.removeTags = function(tagsArr, tagToRemove) {
                                var index = tagsArr.indexOf(tagToRemove);
                                if (index != -1) {
                                    delete target.tplCtx.arrayForm;
                                    delete target.tplCtx.edit;
                                    target.tplCtx.value = null;
                                    target.tplCtx.path = "tags." + index;
                                    target.tplCtx.pull = "tags";
                                    dataHelper.path2Value(target.tplCtx, function(params) {

                                    })
                                }
                            }

                            if (target.parentNode.dataset.id == "todo") {
                                dataHelper.path2Value(target.tplCtx, function(params) {
                                    if (notEmpty(params.elt.tags)) {
                                        target.removeTags(params.elt.tags, kpObj.tags.todiscuss);
                                        target.removeTags(params.elt.tags, kpObj.tags.totest)
                                    }
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatusaction/answerid/' + kpObj.context.answerId, {
                                        actname: kpObj.context.actions[el.dataset.eid]["name"],
                                        projectname: kpObj.context.projects[kpObj.context.projectId]["name"],
                                        channelChat: kpObj.context.projects[kpObj.context.projectId]["slug"],
                                        status: "todo"
                                    }, function() {}, "html")
                                });
                            }

                            if (target.parentNode.dataset.id == "done") {
                                dataHelper.path2Value(target.tplCtx, function(params) {
                                    if (notEmpty(params.elt.tags)) {
                                        target.removeTags(params.elt.tags, kpObj.tags.todiscuss);
                                        target.removeTags(params.elt.tags, kpObj.tags.totest)
                                    }
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatusaction/answerid/' + kpObj.context.answerId, {
                                        actname: kpObj.context.actions[el.dataset.eid]["name"],
                                        projectname: kpObj.context.projects[kpObj.context.projectId]["name"],
                                        channelChat: kpObj.context.projects[kpObj.context.projectId]["slug"],
                                        status: "done"
                                    }, function() {}, "html")
                                });
                            }
                            if (target.parentNode.dataset.id == "tracking") {
                                target.tplCtx.path = "tracking";
                                target.tplCtx.value = true;
                                dataHelper.path2Value(target.tplCtx, function(params) {
                                    if (notEmpty(params.elt.tags)) {
                                        target.removeTags(params.elt.tags, kpObj.tags.todiscuss);
                                        target.removeTags(params.elt.tags, kpObj.tags.totest)
                                    }
                                })
                            }
                            if (target.parentNode.dataset.id == "todiscuss") {
                                target.tplCtx.path = "tags";
                                target.tplCtx.value = kpObj.tags.todiscuss;
                                target.tplCtx.arrayForm = true;
                                target.tplCtx.edit = false;
                                dataHelper.path2Value(target.tplCtx, function(params) {
                                    if (notEmpty(params.elt.tags)) {
                                        target.removeTags(params.elt.tags, kpObj.tags.totest)
                                    }
                                })
                            }
                            if (target.parentNode.dataset.id == "totest") {
                                target.tplCtx.path = "tags";
                                target.tplCtx.value = kpObj.tags.totest;
                                target.tplCtx.arrayForm = true;
                                target.tplCtx.edit = false;
                                dataHelper.path2Value(target.tplCtx, function(params) {
                                    if (notEmpty(params.elt.tags)) {
                                        target.removeTags(params.elt.tags, kpObj.tags.todiscuss)
                                    }
                                })
                            }

                            if (target.parentNode.dataset.id != "tracking") {
                                dataHelper.path2Value({
                                    id: el.dataset.eid,
                                    collection: "actions",
                                    path: "tracking",
                                    format: true,
                                    value: "false",
                                }, function(params) {
                                    toastr.success(trad.saved);
                                })
                            }



                            /*if(["todiscuss","totest"].includes(source.parentNode.dataset.id)  && exists(el.dataset.tags)){
                                var tags =  el.dataset.tags.split(",");
                                let index = "";
                                switch (source.parentNode.dataset.id) {
                                    case "todiscuss":
                                        index = kpObj.tags.todiscuss;
                                        break;
                                    case "totest":
                                        index = kpObj.tags.totest;
                                        break;
                                    default:
                                        text = "";
                                }
                                if(index != -1){
                                    tags.splice(index,1)
                                    dataHelper.path2Value({
                                        id : el.dataset.eid,
                                        collection : "actions",
                                        path : "tags",
                                        format : true,
                                        value : tags,
                                    }, function(params) {
                                        toastr.success(trad.saved);
                                    })
                                }
                            }*/
                        }
                    },
                    tags: exists(v.tags) ? v.tags : [],
                    click: function(el) {
                        //$('#card-'+el.dataset.eid+' .action-name').trigger('click');
                    },
                    //context: function(el, e){
                    //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                    //},
                    // class: [
                    //     "cursor-pointer",
                    //     "aapgetaapview",
                    //     seen == false ? (notEmpty(userId)?"unseen-proposition":"seen-proposition") :"seen-proposition"
                    // ],
                    // label:"nouv",
                    // url: 'slug.'+elSlug+'.formid.'+elFormId+'.aappage.sheet.answer.'+k,
                    // status: v.status
                }
            },
            itemArray: {
                tracking: [],
                todo: [],
                done: [],
                todiscuss: [],
                totest: [],
            },
            createBoardItemArray: function(kpObj, actions) {
                var j = 1;
                $.each(actions, (k, v) => {
                    v.j = j;
                    if(notEmpty(v.status) && v.status != "disabled"){
                        if(exists(v.status) && (v.status == 'success' || v.status == 'done') ){
                            kpObj.itemArray.done.push(kpObj.createBoardItemTemlate(kpObj,k,v));
                        }else if((exists(v.tags) && Array.isArray(v.tags) && v.tags.includes(kpObj.tags.totest) ))
                            kpObj.itemArray.totest.push( kpObj.createBoardItemTemlate(kpObj,k,v));
                        else if((exists(v.tags) && Array.isArray(v.tags) && v.tags.includes(kpObj.tags.todiscuss) ))
                            kpObj.itemArray.todiscuss.push( kpObj.createBoardItemTemlate(kpObj,k,v));
                        else if((exists(v.tracking) && v.tracking) || v.status == "inprogress")
                            kpObj.itemArray.tracking.push( kpObj.createBoardItemTemlate(kpObj,k,v));
                        else if(exists(v.status) && (v.status == 'todo' || v.status == "disabled") )
                            kpObj.itemArray.todo.push(kpObj.createBoardItemTemlate(kpObj,k,v)); 
                    }
                    j++;
                })
            },
            createBoard: function(kpObj) {
                var kanbanBoards = [{
                        id: "todiscuss",
                        title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-comment'></i> A discuter <span class="kanban-column-counter"></span> <i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                        class: "warning",
                        dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                        item: kpObj.itemArray.todiscuss
                    },
                    {
                        id: "todo",
                        title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-thumb-tack'></i> To do <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                        class: "info,good",
                        dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                        item: kpObj.itemArray.todo
                    },
                    {
                        id: "tracking",
                        title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-star'></i> En cours <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                        class: "warning",
                        dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                        item: kpObj.itemArray.tracking
                    },
                    {
                        id: "totest",
                        title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-gavel'></i> A tester <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                        class: "info",
                        dragTo: ["todo", "done", "tracking", "todiscuss", "totest"],
                        item: kpObj.itemArray.totest
                    },
                    {
                        id: "done",
                        title: `<span class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="${trad.addAction}"><i class='hidden fa fa-check'></i> Terminé <span class="kanban-column-counter"></span><i class='fa fa-plus pull-right margin-top-5'></i></span> `,
                        class: "success",
                        dragTo: ["done", "tracking", "todiscuss", "totest"],
                        item: kpObj.itemArray.done
                    }
                ]
                return kanbanBoards;
            },
            createProjectList: function(kpObj) {
                kpObj.getProjects(kpObj, function(data) {
                    var html = '';
                    var i = 0;

                    $.each(data, function(k, v) {
                        if (kpObj.focusFromUrl() !== false && k == kpObj.focusFromUrl()) {
                            kpObj.defaultProject = k; //important
                            kpObj.context.projectId = k;
                            kpObj.context.idParentRoom = notEmpty(v.roomId) ? v.roomId : "";
                            kpObj.context.answerId = notEmpty(v.answerId) ? v.answerId : "";
                            kpObj.context.prLink = `${baseUrl}/costum/co/index/slug/${costum.slug}#oceco.slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.list?view=project_kanban&projectid=${kpObj.context.projectId}&showprojectpanel=false`;
                            $("#current-project").text(v.name);
                            $('#copy-project-link').attr("data-clipboard-text", kpObj.context.prLink);
                            html += `<li class="active"><a href="javascript:;" class="small item" data-id="${k}" data-room="${notEmpty(v.roomId) ? v.roomId : ''}" data-answer="${notEmpty(v.answerId) ? v.answerId : ''}" data-name="${escapeHtml(v.name)}">${v.name}</a></li>`;
                        } else if (i == 0 && kpObj.focusFromUrl() === false) {
                            kpObj.defaultProject = k; //important
                            kpObj.context.projectId = k;
                            kpObj.context.idParentRoom = notEmpty(v.roomId) ? v.roomId : "";
                            kpObj.context.answerId = notEmpty(v.answerId) ? v.answerId : "";
                            kpObj.context.prLink = `${baseUrl}/costum/co/index/slug/${costum.slug}#oceco.slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.list?view=project_kanban&projectid=${kpObj.context.projectId}&showprojectpanel=false`;
                            $("#current-project").text(v.name);
                            $('#copy-project-link').attr("data-clipboard-text", kpObj.context.prLink);
                            html += `<li class="active"><a href="javascript:;" class="small item" data-id="${k}" data-room="${notEmpty(v.roomId) ? v.roomId : ''}" data-answer="${notEmpty(v.answerId) ? v.answerId : ''}" data-name="${escapeHtml(v.name)}">${v.name}</a></li>`;
                        } else {
                            if (showProjectPanel)
                                html += `<li class=""><a href="javascript:;" class="small item" data-id="${k}" data-room="${notEmpty(v.roomId) ? v.roomId : ''}" data-answer="${notEmpty(v.answerId) ? v.answerId : ''}" data-name="${escapeHtml(v.name)}">${v.name}</a></li>`;
                        }
                        i++;
                    })
                    if (showProjectPanel)
                        $(".projects-list").html(html);

                    if (kpObj.focusFromUrl() !== false) {
                        setTimeout(() => {
                            var offset = $('.kanban-project-list li').first().position().top;
                            //$('.kanban-project-list').scrollTop($('[data-id="'+kpObj.focusFromUrl()+'"]').parent().position().top - offset);
                            $('.kanban-project-list').stop().animate({
                                    scrollTop: $('[data-id="' + kpObj.focusFromUrl() + '"]').parent().position().top - offset
                                },
                                1000, 'swing',
                                function() {}
                            )
                        }, 900);
                    }
                })
            },
            refresh: function(kpObj) {
                kpObj.actions.addActionToKanban(kpObj, kpObj.context.projectId, true);
            },
            getActionsOnClickProject: function(kpObj) {
                $('.projects-list .item').off().on('click', function() {
                    $('.loader-kanban').show();
                    var id = $(this).data("id");
                    kpObj.defaultProject = id;
                    kpObj.context.projectId = id;
                    kpObj.context.idParentRoom = $(this).data("room");
                    kpObj.context.answerId = $(this).data("answer");
                    kpObj.context.prLink = `${baseUrl}/costum/co/index/slug/${costum.slug}#oceco.slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.list?view=project_kanban&projectid=${kpObj.context.projectId}&showprojectpanel=false`;
                    $('#copy-project-link').attr("data-clipboard-text", kpObj.context.prLink);
                    $(".kanban-item").remove();
                    $('.projects-list .item').parent().removeClass("active");
                    $(this).parent().addClass("active");
                    $("#current-project").text($(this).text());
                    kpObj.actions.addActionToKanban(kpObj, id, true);
                    if (location.href.indexOf("projectid=")) {
                        let lhref = location.href.split("&projectid=")[0];
                        history.pushState(null, null, lhref + '&projectid=' + id)
                    }
                    kpObj.columnCounter(kpObj);
                })
            },
            columnCounter: function(kpObj) {
                //$('[data-id=todo] .kanban-drag').children().length;
                $.each(kpObj.itemArray, function(k) {
                    var cCounter = $('[data-id=' + k + '] .kanban-drag').children(".kanban-item:visible").length;
                    if (cCounter != 0)
                        $('[data-id=' + k + '] .kanban-board-header .kanban-column-counter').text(`(${cCounter})`);
                    else
                        $('[data-id=' + k + '] .kanban-board-header .kanban-column-counter').text('');
                })
            },
            focusFromUrl: function() {
                if (location.hash.indexOf("&projectid=") != -1) {
                    let projectFromUlr = location.hash.split("&projectid=")[1];
                    if (projectFromUlr.indexOf("&") != -1) {
                        projectFromUlr = projectFromUlr.split("&")[0]
                    }
                    return projectFromUlr;
                } else
                    return false;
            },
            events: function(kpObj) {
                var kanbanContainerWidth = $("#myKanban .kanban-container").width()
                kpObj.getActionsOnClickProject(kpObj);
                $("#myKanban1 div").css("width", kanbanContainerWidth);
                $("#myKanban1").scroll(function() {
                    $("#myKanban").scrollLeft($("#myKanban1").scrollLeft());
                });
                $("#myKanban").scroll(function() {
                    $("#myKanban1").scrollLeft($("#myKanban").scrollLeft());
                });
                
                $(".content-answer-search").removeClass("col-lg-9").addClass("col-lg-12")

                $('.new-action,.kanban-board-header').off().on('click', function() {
                    var btn = $(this);
                    var defaultValue = {
                        answer: {}
                    };
                    $('.search-action-field').val("").trigger('keyup');
                    kpObj.getAnswerByprojectId(kpObj,kpObj.defaultProject,function(data){
                        var i = Object.keys(data.results)[0];
                        var data = data;

                        if(exists(jsonHelper.getValueByPath(data.results[i], "answers.aapStep1.titre"))){
                            defaultValue.answer[i] = {
                                type : "answers",
                                name : data.results[i].answers.aapStep1.titre
                            }
                        }
                        
                        kpObj.context.key = btn.parent().data('id');
                        mylog.log(kpObj.context,"kpObj.context");
                        dyFObj.openForm(
                            kpObj.actions.dynforms(kpObj,kpObj.context)
                            ,null,
                            defaultValue,null,null,{
                                type: "bootbox",
                                notCloseOpenModal:true
                            }
                        )
                    })


                })
                $('.cancel-action').off().on('click', function() {
                    var actid = $(this).data('action-id');
                    bootbox.confirm(`<h6 class="text-center text-danger">${trad.areyousure}</h6>`, function(result) {
                        if (result) {
                            var tplCtx = {
                                id: actid,
                                collection: "actions",
                                path: "status",
                                value: "disabled",
                            }
                            dataHelper.path2Value(tplCtx, function(params) {
                                $('#card-' + actid).hide(600);
                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/cancelaction/answerid/' + kpObj.context.answerId, {
                                        actname: kpObj.context.actions[actid]["name"],
                                        projectname: kpObj.context.projects[kpObj.context.projectId]["name"],
                                        channelChat: kpObj.context.projects[kpObj.context.projectId]["slug"],
                                    },
                                    function(data) {}, "html");
                            })
                        }
                    });
                })
                $('.edit-action').off().on('click', function() {
                    var actid = $(this).data('action-id');
                    var currentValue = kpObj.context.actions[actid]
                    if (notEmpty(kpObj.context.actions[actid].links) && notEmpty(kpObj.context.actions[actid].links.contributors)) {
                        currentValue.contributors = kpObj.context.actions[actid].links.contributors

                        ajaxPost('', baseUrl + "/co2/element/get/type/citoyens", {
                                id: Object.keys(currentValue.contributors),
                                fields: ["links"]
                            },
                            function(result) {
                                $.each(currentValue.contributors, function(k, v) {
                                    currentValue.contributors[k]["name"] = result.map[k]["name"]
                                })
                            },
                            null,
                            null, {
                                async: false
                            }
                        )
                    }

                    dyFObj.openForm(
                        kpObj.actions.dynformsEdit(kpObj, actid), null,
                        currentValue, null, null, {
                            type: "bootbox",
                            notCloseOpenModal: true
                        }
                    )
                    delete currentValue;
                })
                $('.image-action').off().on('click', function() {
                    kpObj.actions.openImage(kpObj, $(this))
                });
                $('.file-action').off().on('click', function() {
                    kpObj.actions.openFile(kpObj, $(this))
                });

                /*$('.new-task').off().on('click',function(){
                    var idAction = $(this).data('id');
                    var projectSlug = $(this).data('project-slug');
                    dyFObj.openForm(kpObj.tasks.dynForms(kpObj,idAction,null,projectSlug))
                })
                $('.tablecheckicon').off().on('click', function(e){
                    var tablecheckiconpos = $(this).data("pos");
                    var tablecheckiconcollection = "actions";
                    var tablecheckiconid = $(this).data("id");
                    var checkKey = $(this).data("key");
                    var checkValue = $(this).data("value");
                    var taskName = $(this).data("task-name");
                    var projectSlug = $(this).data("project-slug");
                    var btn= $("#check"+tablecheckiconid+tablecheckiconpos);

                    tplCtx.collection = tablecheckiconcollection;
                    tplCtx.id = tablecheckiconid;
                    tplCtx.path = "tasks."+tablecheckiconpos+"."+checkKey;
                    tplCtx.value = checkValue;
                    tplCtx.format= true;

                    dataHelper.path2Value( tplCtx, function() {
                        tplCtx.path = "tasks."+tablecheckiconpos+".checkedUserId";
                        tplCtx.value = userId;
                        dataHelper.path2Value( tplCtx, function(params) {
                            var today = new Date();
                            tplCtx.path = "tasks."+tablecheckiconpos+".checkedAt";
                            tplCtx.value = today.toISOString();
                            if (typeof rcObj != "undefined" && (checkValue==true || checkValue== "true" )) {
                                rcObj.postMsg({
                                    "channel": "#" + projectSlug,
                                    "text": userConnected.name +
                                        "a terminé la sous-tâche " + taskName + "sur l'action " + params.elt.name
                                }).then(function (data) {
                                });
                            }
                            dataHelper.path2Value( tplCtx, function(params) {
                                if(exists(params.elt.tasks[tablecheckiconpos].payed) && params.elt.tasks[tablecheckiconpos].payed == true){
                                    btn.html("<i class='fa fa-euro'></i>");
                                    btn.removeClass('bg-green').removeClass('btn-default').addClass("bg-green-k");
                                }else if(exists(params.elt.tasks[tablecheckiconpos].checked) && params.elt.tasks[tablecheckiconpos].checked == true){
                                    btn.html("<i class='fa fa-check'></i>");
                                    btn.removeClass('btn-default').removeClass('bg-green-k').addClass("bg-green");
                                }else{
                                    btn.html("<i class='fa fa-hourglass fa-pulse'></i>")
                                    btn.removeClass('bg-green').removeClass('bg-green-k').addClass("btn-default");
                                }
                            } );
                        } );
                    } );
                });
                $('.edit-task').off().on('click',function(){
                    var idAction = $(this).data('id');
                    var position = $(this).data('pos');
                    var task = $(this).data('task');
                    var projectSlug = $(this).data('project-slug');
                    dyFObj.openForm(kpObj.tasks.dynForms(kpObj,idAction,position,projectSlug),null,task)
                })

                $('.delete-task').off().on('click',function() {
                    var pos = $(this).data("pos");
                    var line = '.line-'+$(this).data("id")+"-"+pos;
                    tplCtx = {};
                    tplCtx.collection = "actions";
                    tplCtx.id = $(this).data("id");
                    tplCtx.path = "tasks."+pos;
                    tplCtx.pull = "tasks";
                    tplCtx.value = null;
                    prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    dataHelper.path2Value( tplCtx, function(params){
                                        $(line).remove();
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });

                    prioModal.modal("show");
                });*/
                $('.contributor-counter').on('click', function() {
                    setTimeout(() => {
                        coInterface.bindLBHLinks();
                    }, 700);
                })

                /*$('.check-task').off().on("click",function() { 
                    var checkKey = $(this).data("key");
                    var taskName = $(this).data("task-name");
                    var actionName = $(this).data("action-name");
                    var projectSlug = $(this).data("project-slug");
                    var pos = $(this).data("pos");
                    var ans =  $(this).data("ans");
                    tplCtx = {
                        collection : "actions",
                        id : $(this).data("id"),
                        path : "tasks."+pos+".checked",
                        value : false,
                        format: true, 
                    }

                    if($(this).data('check') == true){
                        $(this).html('<i class="fa fa-uncheck"></i>')
                        $(this).data('check',false);
                        tplCtx.value = false;
                        tplCtx.path = "tasks."+pos+".checked";
                        dataHelper.path2Value( tplCtx, function(params) {
                            pObj.actions[params.id] = params.elt;
                        })
                    }else{
                        $(this).html('<i class="fa fa-check"></i>');
                        $(this).data('check',true);
                        tplCtx.value = true;
                        dataHelper.path2Value( tplCtx, function() {
                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/checktask/answerid/' + ans,
                            {
                                actname :actionName,
                                task : taskName,
                                url : window.location.href
                            },
                            function (data) {

                            }, "html");
                            tplCtx.path = "tasks."+pos+".checkedUserId";
                            tplCtx.value = userId;
                            dataHelper.path2Value( tplCtx, function(params) {
                                var today = new Date();
                                tplCtx.path = "tasks."+pos+".checkedAt";
                                tplCtx.value = today.toISOString();
                                dataHelper.path2Value( tplCtx, function(params) {
                                    pObj.actions[params.id] = params.elt;
                                } );
                            } );
                        } );
                    }
                });*/


                $(' .action-name').off().on('click', function(e) {
                    e.stopPropagation();
                    //open_action_detail($(this).data("id"));
                    var act_id = $(this).data("id");
                    var url = baseUrl + '/costum/project/action/request/action_detail_html';
                    var post = { 
                        id: act_id
                    };
                    var callback = function(__html) {
                        $('#act-detail').empty().html(__html);
                        var lazyLoadingModalAction = function(time) {
                            if ($('.action-modal').length != 0) {
                                $('.action-modal').on('hidden.bs.modal', function() {
                                    $(".kanban-item").remove();
                                    kpObj.refresh(kpObj);
                                })
                            } else {
                                setTimeout(() => {
                                    lazyLoadingModalAction(time)
                                }, time + 200);

                            }
                        }
                        lazyLoadingModalAction(0);
                    }
                    ajaxPost(null, url, post, callback, null, 'text');
                })
                /*$('.full-table').off().on('click',function(){
                    if($(this).data('value') == false){
                        $(this).parent().parent().addClass('tasks-modal').css("position","fixed");
                        $(this).parent().parent().find('.col-xs-12.show-detail').removeClass('hidden');
                        $(this).html('<i class="fa fa-compress"></i>');
                        $(this).data('value',true)
                    }else{
                        $(this).parent().parent().removeClass('tasks-modal').css("position","relative");
                        $(this).parent().parent().find('.col-xs-12.show-detail').addClass('hidden');
                        $(this).html('<i class="fa fa-expand"></i>')
                        $(this).data('value',false)
                    }

                })*/
                if (kpObj.userId == null) {
                    $('#current-action-of').parent().remove();
                    $('#search-person').remove();
                }
                if (showProjectPanel == false) {
                    $('.section-project-panel').remove();
                    $('.section-kanban-panel').removeClass("col-xs-10").addClass("col-xs-12");
                }

                kpObj.selectUserPersonnalActions(kpObj);
                kpObj.searchProjectByName(kpObj);
                kpObj.searchActionByName(kpObj);
                aapObject.sections.common.events(aapObject);
                kpObj.comment(kpObj);
                kpObj.copyLinkProject(kpObj);
                kpObj.projects.events(kpObj);
                $('.see-more-action').on('click',function(){
                    const actionId = $(this).data("action-id");
                    const projectId = $(this).data("project-id");
                    open_action_detail(kpObj.context.projects[projectId], kpObj.context.actions[actionId]);
                })
                kpObj.fullsize();
            },
            initKanban: function(kpObj) {
                kpObj.kanban = new jKanban({
                    element: "#myKanban",
                    gutter: "10px",
                    widthBoard: "270px",
                    itemHandleOptions: {
                        enabled: true,
                    },
                    click: function(el) {
                        mylog.log("Trigger on all items click!");
                    },
                    context: function(el, e) {
                        mylog.log("Trigger on all items right-click!");
                    },
                    dropEl: function(el, target, source, sibling) {
                        mylog.log(target.parentElement.getAttribute('data-id'));
                        mylog.log(el, target, source, sibling)
                    },
                    buttonClick: function(el, boardId) {

                    },
                    /*itemAddOptions: {
                        enabled: true,
                        content: '+ action',
                        class: 'new-action',
                        footer: true
                    },*/
                    boards: kpObj.createBoard(kpObj),

                });
            },
            tasks: {
                dynForms: function(kpObj, idAction, position = null, projectSlug = null) {
                    var today = new Date();
                    return {
                        "jsonSchema": {
                            "title": "Ajouter un tache",
                            "description": "Ajouter un tache pour votre action",
                            "icon": "fa-task",
                            "properties": {
                                task: {
                                    inputType: "text",
                                    label: "Nom tache",
                                    rules: {
                                        required: true,
                                        maxlength: 100
                                    }
                                },
                                userId: {
                                    inputType: "hidden",
                                    value: userId
                                },
                                checked: {
                                    inputType: "hidden",
                                    value: "false"
                                },
                                credits: {
                                    label: "Credits",
                                    inputType: "text",
                                    value: "0"
                                },
                                endDate: {
                                    label: "Date de fin",
                                    inputType: "date",
                                    rules: {
                                        required: true,
                                    }
                                },
                                contributors: {
                                    inputType: "finder",
                                    label: "Assigné à qui ?",
                                    multiple: true,
                                    rules: {
                                        required: true,
                                        lengthMin: [1, "contributors"]
                                    },
                                    initType: ["citoyens"],
                                    filters: {
									'$or': {
                                            ["links.members." + userId]: { "$exists": true },
                                            ["links.contributors." + userId]: { "$exists": true }
                                        }
                                    },
                                    initBySearch: true,
                                    initMe: true,
                                    initContext: false,
                                    initContacts: false,
                                    openSearch: true
                                },
                                createdAt: {
                                    inputType: "hidden",
                                    value: today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear(),
                                }
                            },
                            beforeBuild: function() {

                            },
                            afterSave: function() {

                            },
                            save: function(data) {
                                delete data.collection;
                                delete data.scope;
                                var tplCtx = {
                                    id: idAction,
                                    collection: "actions",
                                    path: "tasks",
                                    arrayForm: true,
                                    value: data,
                                    edit: false,
                                    format: true,
                                    setType: [{
                                            "path": "endDate",
                                            "type": "isoDate"
                                        },
                                        {
                                            "path": "createdAt",
                                            "type": "isoDate"
                                        }
                                    ]
                                }
                                if (position != null) {
                                    tplCtx.path = "tasks." + position;
                                    delete tplCtx.arrayForm;
                                    delete tplCtx.edit;
                                }
                                dataHelper.path2Value(tplCtx, function(params) {
                                    /*if (typeof rcObj != "undefined" && position==null){ //if add only
                                        rcObj.postMsg({ "channel": "#"+projectSlug, "text": userConnected.name +"a ajouté la sous-tâche : "+ data.task + "sur l'action "+ params.elt.name }).then(function(data) {});
                                    }*/
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newtask2/answerid/' + kpObj.context.answerId, {
                                            taskname: data.task,
                                            actname: params.elt.name,
                                            projectname: kpObj.context.projects[kpObj.context.projectId]["name"],
                                        },
                                        function(data) {}, "html");
                                    dyFObj.closeForm();
                                    ajaxPost('', baseUrl + '/costum/aap/getelementbyid/type/citoyens/id/' + (Object.keys(params.text.contributors)[0]),
                                        null,
                                        function(data) {
                                            params.text.contributors[Object.keys(params.text.contributors)[0]].data = {};
                                            params.text.contributors[Object.keys(params.text.contributors)[0]].data.profilThumbImageUrl = data.profilThumbImageUrl;
                                            params.text.status = "todo";
                                            params.text.checked = "false";
                                            kpObj.tasks.addTaskToCurrentActionKanban(kpObj, params.id);
                                        }, null);
                                });
                            }
                        }
                    }
                },
                addTaskToCurrentActionKanban: function(kpObj, actionId) {
                    kpObj.getActions(kpObj, [actionId], function(data) {
                        data = data.results;
                        $.each(data, function(k, v) {
                            //var tasksTable = kpObj.createTaskTable(kpObj,k,v);
                            //$("#tbody-"+k).html(tasksTable.taskTable);
                            $("[href='#collapse" + k + "']").remove();
                            $('.panel-title-' + k).prepend('<a data-toggle="collapse" href="#collapse' + k + '" ><small>TACHE(S) <i class="fa fa-caret-down"></i></small></a>')
                            setTimeout(() => {
                                if ($("[href='#collapse" + k + "']").hasClass("collapsed")) {
                                    $("[href='#collapse" + k + "']").trigger("click");
                                }
                            }, 800);
                        })
                    })
                    kpObj.events(kpObj);
                },
                popoverContributors: function(user) {
                    var html = `
                            <table class="table">
                                <tbody>`;
                    Object.entries(user).forEach(([k, v]) => {
                        html += `<tr>
                                        <td><img src="${exists(v.data) && exists(v.data.profilThumbImageUrl) ? v.data.profilThumbImageUrl : defaultImage}" class="img-circle" width="30" height="30" alt="" /></td>
                                        <td><a href="#page.type.${v.collection}.id.${k}" class="lbh-preview-element">${v.name}</a></td>
                                    </tr>`
                    })

                    html += `</tbody>
                            </table>
                            `;
                    return escapeHtml(html);
                }
            },
            actions: {
                dynforms: function(kpObj, options = null) {
                    var today = new Date();
                    return {
                        "jsonSchema": {
                            "title": "Ajouter un action",
                            "icon": "fa-task",
                            "properties": {
                                idParentRoom: dyFInputs.inputHidden(options.idParentRoom),
                                name: dyFInputs.name("action"),
                                credits: {
                                    inputType: "hidden",
                                    order: 7,
                                    rules: {
                                        number: true
                                    },
                                    value: 1
                                },
                                tags: {
                                    inputType: "tags",
                                    label: trad.tags,
                                },
                                contributors: {
                                    inputType: "finder",
                                    label: "Assigné à qui ?",
                                    multiple: true,
                                    rules: {
                                        lengthMin: [1, "contributors"]
                                    },
                                    initType: ["citoyens"],
                                    search: {
                                        filters : {
                                            '$or': {
                                                ["links.memberOf." + aapObject.elementAap.id]: {
                                                    "$exists": true
                                                },
                                                ["links.projects." + aapObject.elementAap.id]: {
                                                    "$exists": true
                                                }
                                            }
                                        }
                                    },
                                    initBySearch: true,
                                    initMe: true,
                                    initContext: false,
                                    initContacts: false,
                                    openSearch: true
                                },
                                /*answer : {
                                    inputType : "finder",
                                    label : "Associer à quel proposition ?",
                                    multiple : false,
                                    rules : { required : true, lengthMin:[1, "parent"]}, 
                                    initType: ["answers"],
                                    field : "answers.aapStep1.titre",
                                    search : {  
                                        filters: {
                                            'form' :aapObject.formParent._id.$id,
                                            'answers.aapStep1.titre' : {'$exists':true},
                                            'project.id' : {'$exists':true}
                                        }
                                    },
                                    initBySearch : true,
                                    initMe:false,
                                    initContext:false,
                                    initContacts:false,
                                    openSearch :true,
                                    order: 8
                                },*/
                                status: dyFInputs.inputHidden("todo"),
                                idParentResolution: dyFInputs.inputHidden(""),
                                email: dyFInputs.inputHidden(((userId != null && userConnected != null) ? userConnected.email : "")),
                                idUserAuthor: dyFInputs.inputHidden(userId),
                                parentId: dyFInputs.inputHidden(options.projectId),
                                answerId: dyFInputs.inputHidden(options.answerId),
                                parentType: dyFInputs.inputHidden("projects"),
                                parentIdSurvey: dyFInputs.inputHidden(typeof form != "undefined" ? form : ""),
                                parentIdSurvey: dyFInputs.inputHidden(typeof form != "undefined" ? form._id.$id : ""),
                                parentTypeSurvey: dyFInputs.inputHidden(typeof form != "undefined" ? "forms" : ""),
                                role: dyFInputs.inputHidden(typeof role != "undefined" ? role : ""),
                            },
                            afterBuild: function(data) {
                                $("#name").focus();
                            },
                            beforeSave: function() {
                                /*if(exists(finder.object) && exists(finder.object.answer)){
                                    $("#ajaxFormModal #parentType").val("projects");
                                    $.each(finder.object.answer,function(k,v){
                                        $("#ajaxFormModal #answerId").val(k);
                                        ajaxPost('',baseUrl+"/"+moduleId+"/search/globalautocomplete", 
                                        {  
                                            searchType : ["answers"],
                                            filters:{
                                                '_id' : {'$in': [k]},
                                            },
                                            fields:["project"]
                                        },
                                        function(data){
                                            mylog.log(Object.values(data.results)[0]["project"]["id"],"dadatoaaaaaaa")
                                            $("#ajaxFormModal #parentId").val(Object.values(data.results)[0]["project"]["id"]);
                                        },null,"json",{
                                            async : false
                                        });
                                    })
                                }
                                delete finder.object.answer;*/
                            },
                            afterSave: function() {

                            },
                            save: function(data) {
                                data.collection = "actions";
                                delete data.scope;
                                data.parentId = $("#ajaxFormModal #parentId").val();
                                let links = {
                                    contributors: data.contributors
                                };
                                delete data.contributors;
                                delete data.id;
                                dataHelper.path2Value({
                                    collection: "actions",
                                    path: "allToRoot",
                                    value : data,
                                },function(p){
                                    kpObj.actions.updateStatusAction(kpObj,{id : p.saved.id,collection:"actions",key:options.key});

                                    //add contributors
                                    if (Object.keys(links.contributors).length != 0) {
                                        dataHelper.path2Value({
                                            id: p.saved.id,
                                            path: "links",
                                            collection: "actions",
                                            value: links
                                        }, function(parms) {})
                                    }

                                    let poste = $("#ajaxFormModal #name").val();
                                    let credit = $("#ajaxFormModal #credits").val();
                                    dyFObj.closeForm();
                                    kpObj.actions.addActionToKanban(kpObj, p.saved.id, false);
                                    //a ajouté une action ajouter une image sur la vue listing taille S dans Appel à Projet générique
                                    /*if (typeof rcObj != "undefined") {
                                        rcObj.postMsg({
                                            "channel": "#" + kpObj.context.projects[kpObj.context.projectId]["slug"],
                                            "text": userConnected.name +
                                                "a ajouté une action "+data.name+" dans "+kpObj.context.projects[kpObj.context.projectId]["name"]
                                        }).then(function (data) {
                                        });
                                    }*/
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newaction/'/*answerid/' + kpObj.context.answerId*/,
                                    {
                                        actname :data.name,
                                        projectname : kpObj.context.projects[kpObj.context.projectId]["name"],
                                        channelChat:kpObj.context.projects[kpObj.context.projectId]["slug"],
                                    },
                                    function (data) {}, "html");

                                    ajaxPost(
                                        null,
                                        baseUrl + "/" + moduleId + "/search/globalautocomplete", {
                                            searchType: ["answers"],
                                            filters: {
                                                '_id': {
                                                    '$in': [data.answerId]
                                                },
                                            },
                                            fields: ["answers"]
                                        },
                                        function(res){
                                            /*$.each(res.results,function(k,v){   
                                                //$('[data-id="todo"]').find(".kanban-drag").animate({ scrollTop: 100000 }, 1000);
                                                    let depKeys = Object.keys(v.answers.aapStep1.depense);
                                                    let deppos = depKeys.length != 0 ? (parseInt(depKeys[depKeys.length-1]) + 1) : 1
                                                    var depenseObj = {
                                                        id : data.answerId,
                                                        collection: "answers",
                                                        path: "answers.aapStep1.depense."+deppos,
                                                        value : {
                                                            "date" : moment().format('DD/MM/YYYY'),
                                                            "group" : "Feature",
                                                            "nature" : "fonctionnement",
                                                            "poste" : poste,
                                                            "price" : credit,
                                                            "actionid" : p.saved.id,
                                                            "financer" : [],
                                                            "votes" : {}
                                                        },
                                                        setType : [
                                                            {type : "int", path : "price"}
                                                        ]
                                                    }
                                                    dataHelper.path2Value(depenseObj, function(prms) {})
                                            })*/
                                        },null,null,{async:false}
                                    )

                                })
                            }
                        }
                    }
                },
                dynformsEdit: function(kpObj, idAction) {
                    var today = new Date();
                    return {
                        "jsonSchema": {
                            "title": "Editer un action",
                            "icon": "fa-task",
                            "properties": {
                                name: dyFInputs.name("action"),
                                tags: {
                                    inputType: "tags",
                                    label: trad.tags,
                                },
                                contributors: {
                                    inputType: "finder",
                                    label: "Assigné à qui ?",
                                    multiple: true,
                                    rules: {
                                        lengthMin: [1, "contributors"]
                                    },
                                    initType: ["citoyens"],
                                    search: {
                                        filters : {
                                            '$or': {
                                                ["links.memberOf." + aapObject.elementAap.id]: {
                                                    "$exists": true
                                                },
                                                ["links.projects." + aapObject.elementAap.id]: {
                                                    "$exists": true
                                                }
                                            }
                                        }
                                    },
                                    initBySearch: true,
                                    initMe: true,
                                    initContext: false,
                                    initContacts: false,
                                    openSearch: true
                                }
                            },
                            afterBuild: function(data) {
                                $("#name").focus();
                            },
                            save: function(data) {
                                data.collection = "actions";
                                delete data.scope;
                                let links = {
                                    contributors: data.contributors
                                };
                                delete data.contributors;
                                dataHelper.path2Value({
                                    id: idAction,
                                    collection: "actions",
                                    path: "allToRoot",
                                    value: data,
                                    updatePartial: true,
                                }, function(p) {
                                    //add contributors
                                    if (Object.keys(links.contributors).length != 0) {
                                        dataHelper.path2Value({
                                            id: idAction,
                                            path: "links",
                                            collection: "actions",
                                            value: links
                                        }, function(parms) {})
                                    }

                                    dyFObj.closeForm();
                                    $("#card-" + idAction).remove();
                                    kpObj.actions.addActionToKanban(kpObj, idAction, false);
                                })
                            }
                        }
                    }
                },
                updateStatusAction: function(kpObj, params) {
                    params.path = 'status';
                    switch (params.key) {
                        case 'todo':
                            params.value = params.key;
                            dataHelper.path2Value(params, function(data) {});
                            break;
                        case 'done':
                            params.value = params.key;
                            dataHelper.path2Value(params, function(data) {});
                            break;
                        case 'tracking':
                            params.path = 'tracking';
                            params.value = true;
                            dataHelper.path2Value(params, function(data) {});
                            break;
                        case 'todiscuss':
                            params.path = 'tags';
                            params.value = [kpObj.tags.todiscuss];
                            dataHelper.path2Value(params, function(data) {});
                            break;
                            break;
                        case 'totest':
                            params.path = 'tags';
                            params.value = [kpObj.tags.totest];
                            dataHelper.path2Value(params, function(data) {});
                            break;
                        default:
                            params = null;
                    }
                },
                addActionToKanban: function(kpObj, idAction, many) {
                    kpObj.getActions(kpObj, [idAction], {
                        many: many
                    }, function(data) {
                        var card = "";
                        data = data.results;
                        $.each(data, function(k, v) {
                            kpObj.context.actions[k] = v;
                            /*todiscuss
                            todo
                            tracking
                            totest
                            done*/
                            if (notEmpty(v.status) && v.status != "disabled") {
                                if (exists(v.status) && (v.status == 'success' || v.status == 'done')) {
                                    //kpObj.itemArray.done.push(kpObj.createBoardItemTemlate(kpObj,k,v));
                                    kpObj.kanban.addElement('done', kpObj.createBoardItemTemlate(kpObj, k, v) /*,0*/ );
                                } else if ((exists(v.tags) && v.tags.includes(kpObj.tags.totest)))
                                    //kpObj.itemArray.totest.push( kpObj.createBoardItemTemlate(kpObj,k,v));
                                    kpObj.kanban.addElement('totest', kpObj.createBoardItemTemlate(kpObj, k, v) /*,0*/ );
                                else if ((exists(v.tags) && v.tags.includes(kpObj.tags.todiscuss)))
                                    //kpObj.itemArray.todiscuss.push( kpObj.createBoardItemTemlate(kpObj,k,v));
                                    kpObj.kanban.addElement("todiscuss", kpObj.createBoardItemTemlate(kpObj, k, v) /*,0*/ );
                                else if ((exists(v.tracking) && v.tracking) || v.status == "inprogress")
                                    //kpObj.itemArray.tracking.push( kpObj.createBoardItemTemlate(kpObj,k,v));
                                    kpObj.kanban.addElement('tracking', kpObj.createBoardItemTemlate(kpObj, k, v) /*,0*/ );
                                else if (exists(v.status) && (v.status == 'todo' || v.status == "disabled"))
                                    //kpObj.itemArray.todo.push(kpObj.createBoardItemTemlate(kpObj,k,v)); 
                                    kpObj.kanban.addElement('todo', kpObj.createBoardItemTemlate(kpObj, k, v) /*,0*/ );
                            }
                            kpObj.columnCounter(kpObj);
                        })
                        kpObj.events(kpObj);
                    })
                },
                getCurrentProject: function() {
                    $("#current-project-of-text").hide();
                    if (viewParams.projectOf != "") {
                        ajaxPost('', baseUrl + "/co2/element/get/type/projects/id/" + viewParams.projectOf,
                            null,
                            function(data) {
                                data = data.map;
                                $("#current-project-of-text").show();
                                $("#current-project-of").html(`<a href="#page.type.${data.collection}.id.${data._id.$id}" class="letter-green lbh-preview-element">${data.name}</a>`);
                                coInterface.bindLBHLinks();
                            }, null);
                    }
                },
                actionImg: function(kpObj, idAction) {
                    return {
                        "jsonSchema": {
                            "title": "Ajouter une image",
                            "description": "Ajouter une image pour votre action",
                            "icon": "fa-task",
                            "properties": {
                                image: {
                                    "inputType": "uploader",
                                    "label": trad.images,
                                    "docType": "image",
                                    "itemLimit": 4,
                                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                                    "showUploadBtn": false,
                                    //initList : $(this).data("img")
                                },
                            },
                            beforeBuild: function() {
                                uploadObj.set("actions", idAction);
                            },
                            save: function() {
                                dyFObj.commonAfterSave(null, function() {
                                    params = {
                                        searchType: ["documents"],
                                        fields: ["id"],
                                        filters: {
                                            'id': idAction,
                                            doctype : "image"
                                        },
                                        notSourceKey: true,
                                    };
                                    ajaxPost(
                                        null,
                                        baseUrl + "/" + moduleId + "/search/globalautocomplete",
                                        params,
                                        function(data) {
                                            dataHelper.path2Value({
                                                id: idAction,
                                                collection: "actions",
                                                path: "media.images",
                                                value: Object.keys(data.results)
                                            }, function(p) {
                                                if (exists(p.elt) && exists(p.elt["media"])) {
                                                    kpObj.context.actions[idAction]["media"] = p.elt["media"];
                                                }

                                                $('.small.item[data-id="' + kpObj.context.projectId + '"]').trigger('click');

                                                $(".image-action[data-id='" + idAction + "']").trigger('click');
                                                //dyFObj.closeForm();
                                                $('#modal-form-bootbox').modal('hide');
                                            })
                                        }
                                    )
                                });
                            }
                        }
                    }
                },
                actionFiles: function(kpObj, idAction) {
                    return {
                        "jsonSchema": {
                            "title": "Ajouter un fichier",
                            "description": "Ajouter une fichier pour votre action",
                            "icon": "fa-task",
                            "properties": {
                                files: {
                                    "inputType": "uploader",
                                    "label": trad.files,
                                    "docType": "file",
                                    "doctype": "file",
                                    "contentKey" : "file",
                                    "itemLimit": 4,
                                    "filetypes": ["pdf", "csv"],
                                    "showUploadBtn": false,
                                    "template" : "qq-template-manual-trigger",
                                    "endPoint" :"/subKey/file",
                                    //initList : $(this).data("img")
                                },
                            },
                            beforeBuild: function() {
                                uploadObj.set("actions", idAction);
                            },
                            save: function() {
                                dyFObj.commonAfterSave(null, function() {
                                    params = {
                                        searchType: ["documents"],
                                        fields: ["id"],
                                        filters: {
                                            id: idAction,
                                            doctype : "file"
                                        },
                                        notSourceKey: true,
                                    };
                                    ajaxPost(
                                        null,
                                        baseUrl + "/" + moduleId + "/search/globalautocomplete",
                                        params,
                                        function(data) {
                                            dataHelper.path2Value({
                                                id: idAction,
                                                collection: "actions",
                                                path: "media.files",
                                                value: Object.keys(data.results)
                                            }, function(p) {
                                                if (exists(p.elt) && exists(p.elt["media"])) {
                                                    kpObj.context.actions[idAction]["media"] = p.elt["media"];
                                                }

                                                $('.small.item[data-id="' + kpObj.context.projectId + '"]').trigger('click');

                                                $(".file-action[data-id='" + idAction + "']").trigger('click');
                                                //dyFObj.closeForm();
                                                $('#modal-form-bootbox').modal('hide');
                                            })
                                        }
                                    )
                                });
                            }
                        }
                    }
                },
                openImage: function(kpObj, button) {
                    var idact = button.data("id");
                    ajaxPost(
                        null,
                        baseUrl + "/co2/document/getlistdocumentswhere", {
                            params: {
                                '_id': (exists(kpObj.context.actions[idact]["media"]) && exists(kpObj.context.actions[idact]["media"]["images"])) ? kpObj.context.actions[idact]["media"]["images"] : [],
                            }
                        },
                        function(data) {
                            var imgHtmL = `
                                <div class="row">
                                <div class="col-xs-11"><h3>${kpObj.context.actions[idact]["name"]}</h3></div>
                                <div class="col-xs-1">
                                    <div class="close-modal" data-dismiss="modal">
                                        <div class="lr">
                                            <div class="rl">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-11">
                                    <button class="action-add-image btn tooltips margin-bottom-15" data-idact="${idact}" data-cat="1" data-toggle="tooltip" data-placement="top" data-original-title="Ajouter une image">
                                            Ajouter une image
                                    </button>
                                </div>`;
                            $.each(data, function(k, v) {
                                imgHtmL += `<div class="col-xs-12 margin-bottom-15 padding-top-10 padding-bottom-10" style="background-color:#222">
                                            <img src="${v.docPath}" alt="" style="border-radius: 1px"/>
                                            <button class="btn btn-xs btn-danger btn-delete-action-img" data-idimg="${k}" data-posimg="0" data-idaction="${idact}"><i class="fa fa-2x fa-times"></i></button>
                                        </div>`;
                            });
                            imgHtmL += `</div>`;
                            smallMenu.open(imgHtmL, "");
                        }
                    )
                    setTimeout(() => {
                        $(".btn-delete-action-img").off().on('click', function(e) {
                            var btn = $(this);
                            var idimg = $(this).data('idimg');
                            var posimg = $(this).data('posimg');
                            var idaction = $(this).data('idaction');
                            var prioModal = bootbox.dialog({
                                title: trad.confirmdelete,
                                show: false,
                                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                                buttons: [{
                                        label: "Ok",
                                        className: "btn btn-primary pull-left",
                                        callback: function() {
                                            ajaxPost(
                                                null,
                                                baseUrl + "/" + moduleId + "/document/delete/id/" + idimg, {},
                                                function(data) {
                                                    if (data.result) {
                                                        tplCtx = {
                                                            collection: "actions",
                                                            id: idaction,
                                                            path: "media.images." + posimg,
                                                            pull: "media.images",
                                                            value: null,
                                                        };
                                                        dataHelper.path2Value(tplCtx, function(params) {
                                                            btn.parent().remove();
                                                            var j = kpObj.context.actions[idaction]["media"]["images"].indexOf(idimg);
                                                            kpObj.context.actions[idaction]["media"]["images"].splice(j, 1);
                                                            $('.small.item[data-id="' + kpObj.context.projectId + '"]').trigger('click');
                                                        });
                                                    }
                                                }
                                            );
                                        }
                                    },
                                    {
                                        label: "Annuler",
                                        className: "btn btn-default pull-left",
                                        callback: function() {}
                                    }
                                ]
                            });
                            prioModal.modal("show");
                        })
                        $('.action-add-image').off().on('click', function() {
                            var idAction = $(this).data('idact');
                            dyFObj.openForm(kpObj.actions.actionImg(kpObj, idAction), null, null, null, null, {
                                type: "bootbox",
                                notCloseOpenModal: true
                            });
                        })
                    }, 1000);
                },
                openFile: function(kpObj, button) {
                    var idact = button.data("id");
                    ajaxPost(
                        null,
                        baseUrl + "/co2/document/getlistdocumentswhere", {
                            params: {
                                '_id': (exists(kpObj.context.actions[idact]["media"]) && exists(kpObj.context.actions[idact]["media"]["files"])) ? kpObj.context.actions[idact]["media"]["files"] : [],
                            }
                        },
                        function(data) {
                            var imgHtmL = `
                                <div class="row">
                                <div class="col-xs-11"><h3>${kpObj.context.actions[idact]["name"]}</h3></div>
                                <div class="col-xs-1">
                                    <div class="close-modal" data-dismiss="modal">
                                        <div class="lr">
                                            <div class="rl">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-11">
                                    <button class="action-add-file btn tooltips margin-bottom-15" data-idact="${idact}" data-cat="1" data-toggle="tooltip" data-placement="top" data-original-title="${tradDynForm.addfile}">
                                            ${tradDynForm.addfile}
                                    </button>
                                </div>`;
                            $.each(data, function(k, v) {
                                imgHtmL += `<div class="col-xs-12 margin-bottom-15 padding-top-10 padding-bottom-10">
                                            <a href="${v.docPath}" target="_blank">${v.name}</a>
                                            <button class="btn btn-xs btn-danger btn-delete-action-img" data-idimg="${k}" data-posimg="0" data-idaction="${idact}"><i class="fa fa-2x fa-times"></i></button>
                                        </div>`;
                            });
                            imgHtmL += `</div>`;
                            smallMenu.open(imgHtmL, "");
                        }
                    )
                    setTimeout(() => {
                        $(".btn-delete-action-img").off().on('click', function(e) {
                            var btn = $(this);
                            var idimg = $(this).data('idimg');
                            var posimg = $(this).data('posimg');
                            var idaction = $(this).data('idaction');
                            var prioModal = bootbox.dialog({
                                title: trad.confirmdelete,
                                show: false,
                                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                                buttons: [{
                                        label: "Ok",
                                        className: "btn btn-primary pull-left",
                                        callback: function() {
                                            ajaxPost(
                                                null,
                                                baseUrl + "/" + moduleId + "/document/delete/id/" + idimg, {},
                                                function(data) {
                                                    if (data.result) {
                                                        tplCtx = {
                                                            collection: "actions",
                                                            id: idaction,
                                                            path: "media.files." + posimg,
                                                            pull: "media.files",
                                                            value: null,
                                                        };
                                                        dataHelper.path2Value(tplCtx, function(params) {
                                                            btn.parent().remove();
                                                            var j = kpObj.context.actions[idaction]["media"]["files"].indexOf(idimg);
                                                            kpObj.context.actions[idaction]["media"]["files"].splice(j, 1);
                                                            $('.small.item[data-id="' + kpObj.context.projectId + '"]').trigger('click');
                                                        });
                                                    }
                                                }
                                            );
                                        }
                                    },
                                    {
                                        label: "Annuler",
                                        className: "btn btn-default pull-left",
                                        callback: function() {}
                                    }
                                ]
                            });
                            prioModal.modal("show");
                        })
                        $('.action-add-file').off().on('click', function() {
                            var idAction = $(this).data('idact');
                            dyFObj.openForm(kpObj.actions.actionFiles(kpObj, idAction), null, null, null, null, {
                                type: "bootbox",
                                notCloseOpenModal: true
                            });
                        })
                    }, 1000);
                }
            },
            projects : {
                dynforms :function(kpObj){
                    var dfProject = {
                        "beforeBuild":{
                            "properties" : { 
                            }
                        },
                        "onload" : {
                            "actions" : {
                                "src" : { 
                                    "infocustom" : "Remplir le champ"
                                },
                                "hide" : {
                                    /*"shortDescriptiontextarea" :1,
                                    "tagstags":1,
                                    "emailtext" : 1,
                                    "formLocalityformLocality" : 1,
                                    "breadcrumbcustom" : 1,
                                    "urltext" : 1,
                                    "imguploader" : 1*/
                                }
                            }
                        }
                    };
                    if(kpObj.isAdmin){
                        dfProject.afterSave = function(data){
                            dyFObj.commonAfterSave(data, function(){
                                dyFObj.closeForm();
                                var id = data.id;
                                kpObj.defaultProject = id;
                                kpObj.context.projectId = id;
                                kpObj.context.prLink = `${baseUrl}/costum/co/index/slug/${costum.slug}#oceco.slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.list?view=project_kanban&projectid=${kpObj.context.projectId}&showprojectpanel=false`;
                                $('#copy-project-link').attr("data-clipboard-text",kpObj.context.prLink);
                                $(".kanban-item").remove();
                                $('.projects-list .item').parent().removeClass("active");
                                $(this).parent().addClass("active");
                                $("#current-project").text($(this).text());
                                kpObj.actions.addActionToKanban(kpObj,id,true);
                                if(location.href.indexOf("projectid=")){
                                    let lhref = location.href.split("&projectid=")[0];
                                    history.pushState(null,null,lhref+'&projectid='+id)
                                }
                                setTimeout(() => {
                                    $("[data="+id+"].item").trigger('click')
                                }, 800);

                                kpObj.createProjectList(kpObj);
                                kpObj.columnCounter(kpObj);
                            });
                        }
                    }

                    return dfProject;
                },
                events : function(kpObj){
                    if(kpObj.isAdmin){
                        $(".kanban-add-project").off().on('click',function(){
                            let customProject = kpObj.projects.dynforms(kpObj);
                            mylog.log(customProject,"customProject");
                            dyFObj.openForm("project",null,{
                                "parent" : {
                                    [aapObject.elementAap.id] : {
                                        type : aapObject.elementAap.type,
                                        name : aapObject.elementAap.el.name
                                    }
                                }
                            },null,customProject);
                        })
                    }
                }
            },
            selectUserPersonnalActions : function(kpObj){
                $("#search-person").select2({
                        minimumInputLength: 3,
                        "tokenSeparators": [','],
                        createSearchChoice: function(term, data) {
                            if (!data.length)
                                return {
                                    id: term,
                                    text: term
                                };
                        },
                        ajax: {
                            url: baseUrl + "/" + moduleId + "/search/globalautocomplete",
                            dataType: 'json',
                            type: "POST",
                            quietMillis: 50,
                            data: function(term) {
                                var fltrs = {
                                    '$or': []
                                };
                                var objFltrs = {};
                                if (aapObject && aapObject.elementAap && aapObject.elementAap.id) {
                                    objFltrs["links.memberOf." + aapObject.elementAap.id] = {
                                        "$exists": "true"
                                    }
                                    objFltrs["links.projects." + aapObject.elementAap.id] = {
                                        "$exists": "true"
                                    }
                                    fltrs['$or'] = objFltrs;
                                }

                                return {
                                    name: term,
                                    searchType: ["citoyens"],
                                    filters: fltrs
                                };
                            },
                            results: function(data) {
                                return {
                                    results: $.map(Object.values(data.results), function(item) {
                                        return {
                                            text: item.name,
                                            id: item._id.$id
                                        }
                                    })
                                };
                            }
                        }
                    })
                    .on('change', () => {
                        setTimeout(() => {
                            $('.kanban-container .select2-chosen').html("Voir l'action d'une personne")
                        }, 800);

                        kpObj.userId = $("#search-person").val();
                        location.hash = location.hash.split("?")[0];
                        kpObj.getUserInfo(kpObj)
                        history.pushState(null, null, location.hash + "?view=project&user=" + kpObj.userId);
                        kpObj.actions.addActionToKanban(kpObj, kpObj.defaultProject);
                    });
            },
            searchProjectByName: function(kpObj) {
                $(".search-project-field").off().on("keyup", function() {
                    var val = $(this).val();
                    $('.projects-list .item').each(function() {
                        mylog.log(new RegExp(val, "gi"));
                        if ($(this).text().match(new RegExp(val, "gi"))) {
                            $(this).show(500)
                        } else {
                            $(this).hide(500);
                        }
                    })
                })
            },
            searchActionByName: function(kpObj) {
                $(".search-action-field").on("keyup", function() {
                    setTimeout(() => {
                        var val = $(this).val();
                        $('.kanban-drag .kanban-item').each(function() {
                            //mylog.log(new RegExp(val,"gi"));
                            mylog.log($(this).data("eid"), "kanban-itemeid");
                            mylog.log(kpObj.context.actions, "kanban-itemeid");

                            if (exists(kpObj.context.actions[$(this).data("eid")]) &&
                                kpObj.context.actions[$(this).data("eid")]["name"].match(new RegExp(val, "gi"))
                            ) {
                                $(this).show(500)
                            } else {
                                $(this).hide(500);
                            }
                        })
                        kpObj.columnCounter(kpObj);
                    }, 1200);
                })
            },
            comment: function(kpObj) {
                commentObj.openPreview = function(type, id, path, title, coformKey, formId, options) {
                    let actionName = title;
                    if (typeof options == "undefined" || (typeof options != "undefined" && typeof options.notCloseOpenModal == "undefined"))
                        $("#openModal").modal("hide");
                    title = decodeURI(title);
                    hashPreview = "?preview=comments." + type + "." + id;
                    urlPreview = baseUrl + '/' + moduleId + "/comment/index/type/" + type + "/id/" + id;
                    if (notNull(path)) {
                        urlPreview += "/path/" + path;
                        hashPreview += "." + path;
                    }
                    //urlCtopenPreviewElement( baseUrl+'/'+moduleId+"/"+url); 
                    if (notNull(title))
                        hashPreview += "." + encodeURI(title);
                    else
                        title = "";
                    hashT = location.hash.split("?");
                    getStatus = searchInterface.getUrlSearchParams();
                    urlHistoric = hashT[0].substring(0) + hashPreview;
                    if (getStatus != "") urlHistoric += "&" + getStatus;
                    //history.replaceState({}, null, urlHistoric);      
                    $("#modal-preview-comment .title-comment").html(title);
                    coInterface.showLoader("#modal-preview-comment .comment-tree");
                    $("#modal-preview-comment").show(200);

                    getAjax('#modal-preview-comment .comment-tree', urlPreview, function() {
                        commentObj.bindModalPreview();
                    }, "html");
                    //reload coform input after added comment
                    if (coformKey && formId) {
                        commentObj.coformKey = coformKey;
                        commentObj.formId = formId;
                        commentObj.afterSaveReload = function() {
                            reloadInput(commentObj.coformKey, commentObj.formId);

                        }
                    }
                    commentObj.afterSaveReload = function() {
                        ajaxPost(
                            null,
                            baseUrl + '/' + moduleId + "/comment/countcommentsfrom", {
                                "type": type,
                                "id": id,
                                "path": path
                            },
                            function(data) {
                                $("#btn-comment-" + id).html(data.count + " <i class='fa fa-commenting'></i>")
                                // if (typeof rcObj != "undefined") {
                                //     rcObj.postMsg({
                                //         "channel": "#" + kpObj.context.projects[kpObj.context.projectId]["slug"],
                                //         "text": userConnected.name +
                                //             "a commenté l'action "+kpObj.context.actions[id]["name"]+" dans "+kpObj.context.projects[kpObj.context.projectId]["name"]
                                //     }).then(function (data) {
                                //     });
                                // }
                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/commentaction/answerid/' + kpObj.context.answerId, {
                                        actname: actionName,
                                        projectname: kpObj.context.projects[kpObj.context.projectId]["name"],
                                        channelChat: kpObj.context.projects[kpObj.context.projectId]["slug"],
                                    },
                                    function(data) {}, "html");
                            }
                        );
                    }
                };
                commentObj.closePreview = function() {
                    //mylog.log("close preview");
                    $(".main-container").off();
                    $("#modal-preview-comment").css("display", "none");
                    //urlCtrl.manageHistory(false);
                }
            },
            copyLinkProject: function(kpObj) {
                $("#copy-project-link").off().on().click(function() {
                    let paramsarg = {
                        text: kpObj.context.prLink,
                    }
                    if (!showProjectPanel)
                        paramsarg["container"] = document.getElementById('kanban-project-modal');
                    var clipboard = new ClipboardJS('#copy-project-link', paramsarg);
                    clipboard.on('success', function(e) {
                        toastr.success(trad.copy)
                        clipboard.destroy();
                    });
                })
            },
            fullsize : function(){
                $('.item-container-aap').removeClass("container").addClass("container-fluid");
                $(".list-aap-container .d-flex").removeClass("container").css("width", "100%");
                $(".list-aap-container").removeClass("container").addClass("container-fluid");
            }

        };
        kanbanProjectObj.init(kanbanProjectObj);
        setTimeout(() => {
            kanbanProjectObj.fullsize();
        }, 100);
    })
</script>