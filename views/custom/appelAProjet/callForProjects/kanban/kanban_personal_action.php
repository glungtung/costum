<style>
    div[data-id='Project']{
        width: 250px !important;
        overflow: hidden !important;
    }
    div[data-id='Project'] .kanban-drag{
        overflow-x: hidden !important;
        overflow-y: auto !important;
        padding: 5px !important;
    }
    div[data-id='Project'] .kanban-board-header button.new-action{
        display: none !important;
    }
    div[data-id='Project'] .kanban-drag .kanban-item{
        margin-bottom: 2px !important;
    }
    div[data-id='Project'] .kanban-drag .kanban-item.active{
        background-color: #2C98DE;
        color:#fff;
    }
    div[data-id='Project'] .kanban-drag .kanban-item:hover{
        box-shadow: 0px 0px 3px 1px;
        cursor: pointer !important;
    }
    div[data-id='Project'] .kanban-drag .kanban-item .item_handle.drag_handler{
        display: none !important;
    }
    .text-wrap{
        overflow-wrap: break-word;
    }
</style>
<?php
    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/jkanban.css","/css/aap/kanban.css","/js/blockcms/jkanban.js"], Yii::app()->getModule('costum')->assetsUrl);
    HtmlHelper::registerCssAndScriptsFiles(array( 
        '/css/graphbuilder.css',
        '/js/form.js',
        '/js/dashboard.js',
        '/css/aap/aapGlobalDashboard.css'
        ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

    $linksType= "memberOf";
    if($el["type"]=="projects")
        $linksType= "projects";

    $answers = PHDB::find(Answer::COLLECTION,array("form" => $el_form_id,"project.id"=>['$exists'=>true]),array("project.id"));
    $projectIdArray = [];
    foreach ($answers as $kans => $vans) {
        $projectIdArray[] = new MongoId($vans["project"]["id"]);
    }
    $projects = PHDB::findAndSort("projects",array("_id" => ['$in' => $projectIdArray]),array("name"=>1));
    //var_dump($arrActions);exit;
    foreach ($arrActions as $key => $value) {
        $value["parentId"] = isset($value["parentId"]['$id']) ? $value["parentId"]['$id'] : $value["parentId"];
        $arrActions[$key]["parentId"] = $value["parentId"];
        $arrActions[$key]["parent"] = PHDB::findOneById(Project::COLLECTION,$value["parentId"]); 
        $arrActions[$key]["userAuthor"] = PHDB::findOneById(Person::COLLECTION,$value["idUserAuthor"]);
        if(isset($value["answerId"]))
            $arrActions[$key]["ans"] = PHDB::findOneById(Form::ANSWER_COLLECTION,$value["answerId"]);
        if(isset($value["tasks"])){
            foreach ($value["tasks"] as $kTask => $vTask) {
                if(!empty($vTask["contributors"]))
                foreach ($vTask["contributors"] as $kTaskContrib => $vTaskContrib) {
                    $arrActions[$key]["tasks"][$kTask]["contributors"][$kTaskContrib]["data"] = PHDB::findOneById(Person::COLLECTION,$kTaskContrib,array("profilThumbImageUrl"));
                }
            }
        }
    }

    if(empty(Yii::app()->session['userId']))       
        echo "<h3 class='text-center'>Veuillez vous connecter pour voir toutes vos actions personnelles</h3>";
    else{
?>
    <div class="col-xs-12">
        <div class="kanban-container">
            <h5>Actions personnelles de <span id="current-action-of"></span> <span id="current-project-of-text">, Projet</span> <span id="current-project-of"></span>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle pull-right" type="button" data-toggle="dropdown"><i class="fa fa-align-justify"></i>
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu" style="top: 35px !important;left: 66% !important;right:0 !important">
                        <li><a href="javascript:;" class="btn-personal-observatory">Observatoire</a></li>
                        <li><a href="javascript:;" class="switch-action-view" data-view="action-by-state">Action par status</a></li>
                        <li><a href="javascript:;" class="switch-action-view" data-view="action-by-project">Action par projets</a></li>
                    </ul>
                </div>
                <?php if (isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]) )){?>
                    <input type="text" class="pull-right margin-right-10" id="search-person" placeholder="Voir l'action d'une personne">
                <?php } ?>
            </h5>
            <div id="myKanban1">
                <div></div>
            </div>
            <div id="myKanban" class="margin-bottom-30"></div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="portfolio-modal modal fade" id="modal-observatory-personal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content padding-top-15">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container bg-white">
                    <div class="col-md-12">
                        <h1>Observatoire</h1>
                    </div>
                    <div class="container">
                        <!-- ************************************************************ -->
                        <div class="row aapdashboard">
                            <div class="col-md-2 col-sm-12 col-xs-12 aaptilescont">
                                <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
                                    <span class="aaptilestitles1"> <h5> Actions en retard </h5> </span>
                                    <div class="aapinnertiles1 col-md-12 " style="">
                                        <div class="col-md-12 aapnumbertiles " id="late-action">
                                            0
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-8 col-sm-12 col-xs-12 aaptilescont">
                                <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
                                    <div class=" col-md-12" style="text-align: center;justify-content: center;">
                                        <h4> Action </h4>
                                    </div>
                                    <div class=" col-md-3" style="">
                                        <div class="ocecotitle state success">Au total</div>
                                        <div class="ocecotitle step3" id="total-action">0</div>
                                        <div style="margin: 5px; text-align: center;">
                                        </div>
                                    </div>
                                    <div class=" col-md-3" style="">
                                        <div class="ocecotitle state success">À faire</div>
                                        <div class="ocecotitle step3" id="todo-action">0</div>
                                    </div>
                                    <div class=" col-md-3" style="">
                                        <div class="ocecotitle state success">Términée</div>
                                        <div class="ocecotitle step3" id="success-action">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ************************************************************ -->
                        <div class="row aapdashboard">
                            <div class="col-md-2 col-sm-12 col-xs-12 aaptilescont">
                                <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
                                    <span class="aaptilestitles1"> <h5> Taches en retard </h5> </span>
                                    <div class="aapinnertiles1 col-md-12 " style="">
                                        <div class="col-md-12 aapnumbertiles " id="late-task">
                                            0
                                        </div>
                                        <!-- <div class="col-md-12 aapnumbertiles " id="">
                                            <button class="btn btn-sm voirbtn aapevent aapdashcallback" data-aapdashcallback="viewinfo" data-containerid="totalLateProject">voir</button>
                                        </div> -->

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-8 col-sm-12 col-xs-12 aaptilescont">
                                <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
                                    <div class=" col-md-12" style="text-align: center;justify-content: center;">
                                        <h4> Tâche </h4>
                                    </div>
                                    <div class=" col-md-3" style="">
                                        <div class="ocecotitle state success">Au total</div>
                                        <div class="ocecotitle step3" id="total-task"></div>
                                    </div>
                                    <div class=" col-md-3" style="">
                                        <div class="ocecotitle state success">En cours</div>
                                        <div class="ocecotitle step3" id="not-checked-task"></div>
                                    </div>
                                    <div class=" col-md-3" style="">
                                        <div class="ocecotitle state success">Términée</div>
                                        <div class="ocecotitle step3" id="checked-task"></div>
                                    </div>
                                    <div class=" col-md-3" style="">

                                        <div class="ocecotitle state success">Payée</div>
                                        <div class="ocecotitle step3" id="payed-task"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-md-2 col-sm-12 col-xs-12 aaptilescont">
                                <div class="col-md-12 col-sm-12 col-xs-12 aaptiles">
                                    <div class="aapinnertiles col-md-12" style="min-height: 100px;">
                                        <div class="col-md-12 aapicontiles ocecotitle oceco-gradient">
                                            <i class="fa fa-flag-checkered success"></i>
                                        </div>
                                        <div class="col-md-12 aapdatetiles" id="startDate"></div>
                                    </div>
                                    <div class="col-md-12 aapdatetiles"> il y a <span id="startDateC"></span>j</div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    var currentRoomId = "";
    var contextData = aapObject.elementAap.el;
    contextData.id= aapObject.elementAap.el._id.$id;
    //var projectsId = <?php // json_encode($projectsId) ?>;
    $(function(){
        var today = new Date();
        var $answers = <?= json_encode(array_keys($answers)) ?>;
        var $projects = <?= json_encode($projects) ?>;
        var arrActions = <?= json_encode($arrActions) ?>;
        mylog.log("arrActions",arrActions);
        var kanabanActionObj = {
            boardsObj : {}, // project view
            todoItems : [], 
            inProgressItems : [],//status view
            successItems:[],// status view
            init: function(kanObj){
                if(userId != ''){
                    kanabanActionObj.initItems(kanabanActionObj);
                    kanabanActionObj.initKanban(kanabanActionObj);
                    kanabanActionObj.events(kanabanActionObj);
                }
            },
            itemsAction : function(k,v,kanObj){
                    v.countTask = (exists(v.tasks) ? v.tasks.length : 0);
                    v.realisedTask = 0;
                    v.taskTable = "";
                    var card = "";
                    if(exists(v.tasks) && Array.isArray(v.tasks) && v.tasks.length !=0 ){
                        v.tasks.forEach(function(vv,kk){
                            vv.endDate = (notNull(vv.endDate) && notNull(vv.endDate.sec)) ? (typeof vv.endDate == "object" ? moment.unix(vv.endDate.sec).format('DD/MM/YYYY'):vv.endDate) : "";
                            vv.contributors = vv.contributors;
                            if(exists(vv.checked) && (vv.checked==true || vv.checked=="true")){
                                v.realisedTask++;
                            }
                                
                            v.taskTable +=  `<tr>
                                                <td>
                                                    <div class="dropdown">
                                                        <button id="check${k+kk}" class="btn btn-xs ${(exists(vv.checked) && vv.checked == "true") ? "bg-green" : (exists(vv.payed) && vv.payed == "true") ? "bg-green-k":"btn-default"} dropdown-toggle" type="button" data-toggle="dropdown" data-project-slug="${exists(v.parent) && notNull(v.parent) && exists(v.parent.slug)   ? v.parent.slug : ''}">
                                                            ${(exists(vv.checked) && vv.checked == "true") ? "<i class='fa fa-check'></i>" : 
                                                                (exists(vv.payed) && vv.payed == "true") ? "<i class='fa fa-euro'></i>" : "<i class='fa fa-hourglass fa-pulse'></i>"
                                                            }
                                                        </button>`;
                                                        if(vv.userId == userId){
                            v.taskTable +=              `<ul class="dropdown-menu padding-0" style="min-width: 80px;">
                                                            <li>
                                                                <a href="javascript:void(0);" data-id="${k}" data-pos="${kk}" data-task-name="${vv.task}" data-key="checked" data-value="false" data-color="btn-default" class="btn btn-xs btn-default radius-0 tablecheckicon">Non terminé</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0);" data-id="${k}" data-pos="${kk}" data-key="checked" data-value="true" data-color="bg-green" class="btn btn-xs bg-green radius-0 hover-white tablecheckicon">Terminé</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:void(0);" data-id="${k}" data-pos="${kk}" data-key="payed" data-value="true" data-color="bg-green" class="btn btn-xs bg-green-k radius-0 hover-white tablecheckicon">Validé</a>
                                                            </li>
                                                        </ul>`;
                                                        }
                            v.taskTable +=          `</div>
                                                </td>
                                                <td><b>${exists(vv.task)? vv.task : "" }</b></td>
                                                <td style="position:relative">`;
                                                    if(exists(vv.contributors)){
                                                        var firstContribKey = Object.keys(vv.contributors)[0];
                            v.taskTable +=              `
                                                        <a href="javascript:;" class="contributor-counter" data-html="true" data-toggle="popover" title="Contributeurs" data-content="${kanObj.tasks.popoverContributors(vv.contributors)}" data-placement="top" data-trigger="focus">
                                                            <img class="img-circle img-contributor" src="${exists(vv.contributors[firstContribKey].data) && exists(vv.contributors[firstContribKey].data.profilThumbImageUrl) ? vv.contributors[firstContribKey].data.profilThumbImageUrl : ""}" width="35" height="35" alt="" />
                                                            <span>${(Object.values(vv.contributors).length)}</span>
                                                        </a>`;       
                                                    }
                                                    
                            v.taskTable +=      `</td>
                                                <td>${exists(vv.credits) ? vv.credits : ""}</td>
                                                <td>${vv.endDate}</td>`;
                                                if(vv.userId == userId){
                            v.taskTable +=      `<td>
                                                    <div class="dropdown">
                                                        <button class="btn btn-xs btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                                        <span class="caret"></span></button>
                                                        <ul class="dropdown-menu padding-0" style="min-width:50px;right:0 !important">
                                                            <li><a href="javascript:;" class="btn btn-xs btn-default radius-0 edit-task" data-id="${k}" data-pos="${kk}" data-task="${escapeHtml(JSON.stringify(vv))}"><i class="fa fa-pencil"></i></a></li>
                                                            <li><a href="javascript:;" class="btn btn-xs radius-0 btn-danger delete-task" data-id="${k}" data-pos="${kk}" ><i class=" fa fa-times"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </td>`;
                                                }
                            v.taskTable +=  `</tr>`;
                        })
                    };

                    card += `<div class="row">
                        <p class="action-name"><b>${v.name}</b>
                            <div class="dropdown dropdown-action-ellipsis-v margin-right-5 ${notNull(v.ans) ? '' : 'hidden'}">
                                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-v"></i></button>
                                <ul class="dropdown-menu action-plus no-padding">
                                    <li><a href="javascript:;" class="aapgetinputsa" data-id="${notNull(v.ans) ? v.ans._id.$id : ''}" data-form="${aapObject.formParent._id.$id}" data-formstep="<?= $stepFinancor ?>" data-inputid="financer"><small>Financement</small></a></li>
                                    <li><a href="javascript:;" class="aapgoto" data-url="/costum/co/index/slug/${costum.slug}#oceco.slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.form.answer.${notNull(v.ans) ? v.ans._id.$id : ''}"><small>Formulaire</small></a></li>
                                    <!-- <li><a href="#">JavaScript</a></li>-->
                                </ul>
                            </div>
                            
                            <!-- <button class="btn btn-sm btn-default pull-right"><i class="fa fa-ellipsis-v"></i></button>-->
                        </p>
                        <div class="col-xs-12">
                            <span class="small"> Projet : 
                                <!--<a href="#page.type.${exists(v.parent) && notNull(v.parent) && exists(v.parent.collection) ? v.parent.collection : ""}.id.${exists(v.parent) && notNull(v.parent) && exists(v.parent._id) && exists(v.parent._id.$id) ? v.parent._id.$id : ""}" class="lbh-preview-element">
                                    ${(exists(v.parent) && notNull(v.parent) && exists(v.parent.name)) ? v.parent.name :""}
                                </a> -->
                                <a href="javascript:;" class="projectOf" data-project-id="${exists(v.parent) && notNull(v.parent) && exists(v.parent._id) && exists(v.parent._id.$id) ? v.parent._id.$id : ''}">
                                    ${(exists(v.parent) && notNull(v.parent) && exists(v.parent.name)) ? v.parent.name :""}
                                </a>
                            </span><br/>
                            <span class="small"> Proposition : 
                                <a href="javascript:;" data-url="slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.sheet.answer.${notNull(v.ans) ? v.ans._id.$id : ''}" class="aapgetaapview">
                                    ${(exists(v.ans) && notNull(v.ans) && exists(v.ans.answers.aapStep1.titre)) ? v.ans.answers.aapStep1.titre :""}
                                </a>
                            </span>
                            <span class="small label label-success pull-right"> credits : ${v.credits}</span> <br/>
                            <span class="small">
                                Auteur : 
                                <a href="#page.type.${exists(v.userAuthor) && exists(v.userAuthor.collection) ? v.userAuthor.collection : ""}.id.${exists(v.userAuthor) && exists(v.userAuthor._id) && exists(v.userAuthor._id.$id) ? v.userAuthor._id.$id : ''}" class="lbh-preview-element">
                                    ${(exists(v.userAuthor) && exists(v.userAuthor.name)) ? v.userAuthor.name :""}
                                </a>
                            </span>
                            <span class=" label label-danger pull-right"> réalisé : ${v.realisedTask+"/"+v.countTask}</span> <br/>
                        </div>
                        <div class="col-xs-12">
                            <div class="panel-group padding-0">
                                <div class="panel panel-default">
                                    <div class="panel-heading padding-5">
                                    <h4 class="panel-title text-center">
                                        ${(exists(v.tasks) && Array.isArray(v.tasks) && v.tasks.length !=0 )?
                                            '<a data-toggle="collapse" href="#collapse'+k+'" ><small>TACHE(S) <i class="fa fa-caret-down"></i></small></a>' : 
                                            '<a data-toggle="collapse" href="#collapse'+k+'" style="visibility:hidden"><small>TACHE(S) <i class="fa fa-caret-down"></i></small></a>'
                                        }
                                        <a href="javascript:;" class="btn bg-green hover-white btn-sm new-task" data-new-pos="${0}" data-id="${k}" data-project-slug="${exists(v.parent)&& notNull(v.parent) && exists(v.parent.slug) ? v.parent.slug:''}" data-action-name="${v.name}"><i class="fa fa-plus"></i></a>
                                    </h4>
                                    </div>
                                    <div id="collapse${k}" class="panel-collapse collapse">
                                    <div class="panel-body padding-0">
                                        <table class="table text-muted">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th><small>Tache</small></th>
                                                    <th><small>Contrib</small></th>
                                                    <th><small>Credits</small></th>
                                                    <th><small>Fin</small></th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody-${k}">
                                            `;

                card +=                     v.taskTable; //task table here       

                card +=                     `</tbody>
                                        </table>
                                    </div>
                                    <div class="panel-footer hidden">Panel Footer</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;

                return {
                    id: k,
                    class:["action"+k],
                    title: card,    
                    drag: function(el, source) {
                        mylog.log("START DRAG: " + el.dataset.eid);
                    },
                    dragend: function(el) {
                        mylog.log("END DRAG: " + el.dataset.eid);
                    },
                    drop: function(el, target, source, sibling) {
                        if(!viewParams.projectOf){
                            var pathx = "status";
                            var valuex = target.parentNode.dataset.id;
                            if(target.parentNode.dataset.id == "tracking"){
                                pathx = "tracking";
                                valuex = true
                            }

                            if(((v.realisedTask/v.countTask) == 1 && v.countTask !=0) ){
                                dataHelper.path2Value({
                                    id : el.dataset.eid,
                                    collection : "actions",
                                    path : pathx,
                                    value : valuex,
                                    format : true,
                                }, function(params) {
                                    if(target.parentNode.dataset.id != "tracking"){
                                        dataHelper.path2Value({
                                            id : el.dataset.eid,
                                            collection : "actions",
                                            path : "tracking",
                                            format : true,
                                            value : false,
                                        }, function(params) {
                                            toastr.success(trad.saved);
                                        })
                                    }
                                });
                            }else{
                                toastr.error("Taches non términé");
                                urlCtrl.loadByHash(location.hash);
                                kanObj.actions.scroll(el.dataset.eid)
                            }
                        }
                    },
                    click: function(el) {
                        // alert("click"+v.answers.aapStep1.titre);
                    },
                    context: function(el, e){
                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                    },
                    // class: [
                    //     "cursor-pointer",
                    //     "aapgetaapview",
                    //     seen == false ? (notEmpty(userId)?"unseen-proposition":"seen-proposition") :"seen-proposition"
                    // ],
                    // label:"nouv",
                    // url: 'slug.'+elSlug+'.formid.'+elFormId+'.aappage.sheet.answer.'+k,
                    // status: v.status
                }
            },
            createKanbanBoards : function(kanObj){
                    var kanbanBoards = [
                        {
                            id: "todo",
                            title: 
                            `<div class="row">
                                <div class="col-xs-12">
                                    To do <button class="btn btn-xs radius-0 btn-default pull-right new-action">Nouvelle action <i class='fa fa-plus '></i></button>
                                </div>
                            </div>`,
                            class: "info,good",
                            dragTo:["tracking","success"],
                            item: kanabanActionObj.todoItems
                        },
                        {
                            id: "tracking",
                            title: "En cours <i class='fa fa-star pull-right'></i>",
                            class: "warning",
                             dragTo: ["todo","success"],
                            item: kanabanActionObj.inProgressItems
                        },
                        {
                            id: "success",
                            title: "Terminé <i class='fa fa-check pull-right'></i>",
                            class: "success",
                             dragTo: ["todo","tracking"],
                            item: kanabanActionObj.successItems
                        },
                    ]
                if(location.hash.indexOf("actionByproject") >= 0){
                    //kanbanBoards.splice(1,1);
                    var projectItem = [];
                    var sortable = [];
                    for (var key in $projects) {
                        sortable.push([key, $projects[key]]);
                    }
                    sortable.sort((a,b) => {
                        if ( a[1].name.toLowerCase() < b[1].name.toLowerCase() ) return -1;
                        if ( a[1].name.toLowerCase() > b[1].name.toLowerCase() ) return 1;
                        return 0;
                    })
                    var objSorted = {}
                    sortable.forEach(function(item){
                        objSorted[item[0]]=item[1]
                    })
                    $.each(objSorted,function(kp,vp){
                        vp.class = ["text-wrap"];
                        vp.active = (viewParams.projectOf == kp ? "active":"not-active");
                        vp.class.push(vp.active);
                        projectItem.push({
                            id: kp,
                            class:vp.class,
                            title:  vp.name.charAt(0).toUpperCase() + vp.name.slice(1),    
                            click: function(el) {
                                var id = kp;
                                if(location.hash.indexOf('?') != -1)
                                    history.pushState(null,null,location.hash+"&projectOf="+id);
                                else{
                                    history.pushState(null,null,location.hash+"?projectOf="+id);
                                }
                                urlCtrl.loadByHash(location.hash);
                            },
                        })
                    })
                    kanbanBoards.unshift({
                        id: "Project",
                        title: 
                        `<div class="row">
                            <div class="col-xs-12">
                                Projet <button class="btn btn-xs radius-0 btn-default pull-right new-action">Nouvelle action <i class='fa fa-plus '></i></button>
                            </div>
                        </div>`,
                        class: "info,good",
                        dragTo:["success"],
                        item: projectItem
                    },)
                }
                return kanbanBoards;
            },
            initItems : function(kanObj){
                    kanObj.todoItems = [] ;kanObj.inProgressItems = []; kanObj.successItems = [];
                    $.each(arrActions,(k,v)=>{
                        if (exists(v.tracking) && v.tracking){
                            kanObj.inProgressItems.push({...kanObj.itemsAction(v._id.$id,v,kanObj)});
                        }else if(exists(v.status) && v.status == 'todo'  )
                            kanObj.todoItems.push({...kanObj.itemsAction(v._id.$id,v,kanObj)});
                        else if(exists(v.status) && (v.status == 'success' || v.status == 'done') )
                            kanObj.successItems.push({...kanObj.itemsAction(v._id.$id,v,kanObj)});
                    })
            },
            tasks : {
                dynForms : function(idAction,position=null,projectSlug=null) {
                    return {
                        "jsonSchema" : {    
                            "title" : "Ajouter un tache",
                            "description" : "Ajouter un tache pour votre action",
                            "icon" : "fa-task",
                            "properties" : {
                                task : {
                                    inputType:"text",
                                    label : "Nom tache",
                                    rules: {
                                        required : true,
                                        maxlength: 100
                                    }
                                },
                                // description : { 
                                //     label : "Description du tache", 
                                //     inputType:"textarea",
                                //     rules: {
                                //         maxlength: 140
                                //     }
                                // },
                                userId : {
                                    inputType : "hidden",
                                    value: userId
                                },
                                checked : { 
                                    inputType : "hidden",
                                    value: "false"
                                },
                                credits : {
                                    label: "Credits",
                                    inputType : "text",
                                    value:"0"
                                },
                                endDate :{
                                    label: "Date de fin",
                                    inputType : "date",
                                    rules: {
                                        required : true,
                                    }
                                },
                                contributors : {
                                    inputType : "finder",
                                    label : "Assigné à qui ?",
                                    multiple : true,
                                    rules : { required : true, lengthMin:[1, "contributors"]}, 
                                    initType: ["citoyens"],
                                    search : {
                                        filters: { "links.<?=$linksType ?>.<?=(string)$el["_id"] ?>": { "$exists": "true" }}
                                    },
                                    initBySearch : true,
                                    initMe:true,
                                    initContext:false,
                                    initContacts:false,
                                    openSearch :true
                                },
                                createdAt :{
                                    inputType: "hidden",
                                    value:today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear(),
                                }
                            },
                            beforeBuild : function(){
                                
                            },
                            afterSave : function(){
                                    
                            },
                            save : function (data) {  
                                kanObj = $.extend(kanabanActionObj);
                                delete data.collection;
                                delete data.scope;
                                var tplCtx = {
                                    id : idAction,
                                    collection: "actions",
                                    path: "tasks",
                                    arrayForm: true,
                                    value:data,
                                    edit:false,
                                    format:true,
                                    setType : [
                                        {
                                            "path": "endDate",
                                            "type": "isoDate"
                                        },
                                        {
                                            "path": "createdAt",
                                            "type": "isoDate"
                                        }
                                    ]
                                }
                                if(position!=null){
                                    tplCtx.path = "tasks."+position;
                                    delete tplCtx.arrayForm;
                                    delete tplCtx.edit;
                                }
                                dataHelper.path2Value( tplCtx, function(params) {
                                    if (typeof rcObj != "undefined" && position==null){ //if add only
                                        rcObj.postMsg({ "channel": "#"+projectSlug, "text": userConnected.name +"a ajouté la sous-tâche : "+ data.task + "sur l'action "+ params.elt.name }).then(function(data) {});
                                    }
                                    dyFObj.closeForm();
                                    ajaxPost('',baseUrl+'/costum/aap/getelementbyid/type/citoyens/id/'+(Object.keys(params.text.contributors)[0]), 
                                    null,
                                    function(data){
                                        params.text.contributors[Object.keys(params.text.contributors)[0]].data = {};
                                        params.text.contributors[Object.keys(params.text.contributors)[0]].data.profilThumbImageUrl = data.profilThumbImageUrl;
                                        params.text.status = "todo";
                                        params.text.checked = "false";
                                        if(!exists(arrActions[params.id].tasks))
                                            arrActions[params.id].tasks=[];
                                            arrActions[params.id].tasks.push(params.text);
                                        var copyKanbanOb = jQuery.extend({}, kanabanActionObj);
                                        copyKanbanOb.init(copyKanbanOb);
                                        kanObj.actions.scroll(params.id)
                                    },null);
                                } );
                            }
                        }
                    }
                },
                popoverContributors: function(user){
                    var html = `
                            <table class="table">
                                <tbody>`;
                                Object.entries(user).forEach(([k,v])=>{
                        html+=      `<tr>
                                        <td><img src="${exists(v.data) && exists(v.data.profilThumbImageUrl) ? v.data.profilThumbImageUrl : defaultImage}" class="img-circle" width="30" height="30" alt="" /></td>
                                        <td><a href="#page.type.${v.collection}.id.${k}" class="lbh-preview-element">${v.name}</a></td>
                                    </tr>`     
                                })
                                     
                        html+=  `</tbody>
                            </table>
                            `;
                        return escapeHtml(html);
                } 
            },
            actions:{
                dynforms : {
                    "beforeBuild" : {
                        "properties" : {
                            id : dyFInputs.inputHidden(""),
                            idParentRoom : dyFInputs.inputHidden(currentRoomId),
                            name : dyFInputs.name("action"),
                            description : dyFInputs.textarea(tradDynForm.longDescription, "...",null,true),
                            credits : {
                                inputType : "text",
                                label:"credits",
                                order:7,
                                rules:{
                                    number:true
                                },
                                value:0
                            },
                            answer : {
                                inputType : "finder",
                                label : "Associer à quel proposition ?",
                                multiple : false,
                                rules : { required : true, lengthMin:[1, "parent"]}, 
                                initType: ["answers"],
                                field : "answers.aapStep1.titre",
                                search : {  
                                    filters: {
                                        'form' :aapObject.formParent._id.$id,
                                        'answers.aapStep1.titre' : {'$exists':true},
                                        'project.id' : {'$exists':true}
                                    }
                                },
                                initBySearch : true,
                                initMe:false,
                                initContext:false,
                                initContacts:false,
                                openSearch :true,
                                order: 8
                            },

                            status: dyFInputs.inputHidden( "todo" ),
                            idParentResolution: dyFInputs.inputHidden( "" ),
                            tags : dyFInputs.tags(),
                            urls : dyFInputs.urls,
                            email : dyFInputs.inputHidden( ( (userId!=null && userConnected != null) ? userConnected.email : "" ) ),
                            idUserAuthor: dyFInputs.inputHidden(userId),
                            parentId : dyFInputs.inputHidden(typeof parentIdElement != "undefined" ? parentIdElement : contextData.id),
                            answerId : dyFInputs.inputHidden(),
                            parentType : dyFInputs.inputHidden(typeof parentTypeElement != "undefined" ? parentTypeElement : contextData.type),
                            parentIdSurvey : dyFInputs.inputHidden(typeof form != "undefined" ? form : ""),
                            parentIdSurvey : dyFInputs.inputHidden(typeof form != "undefined" ? form._id.$id : ""),
                            parentTypeSurvey : dyFInputs.inputHidden(typeof form != "undefined" ? "forms" : ""),
                            role : dyFInputs.inputHidden(typeof role != "undefined" ? role : ""),
                            // image : dyFInputs.image()
                        }
                    },
                    beforeSave : function(){
                        if(exists(finder.object) && exists(finder.object.answer)){
                            // $.each(finder.object.parent,function(k,v){
                            //     $("#ajaxFormModal #parentId").val(k);
                            //     $("#ajaxFormModal #parentType").val(v.type);    
                            // })
                            $("#ajaxFormModal #parentType").val("projects");
                            $.each(finder.object.answer,function(k,v){
                                $("#ajaxFormModal #answerId").val(k);
                                ajaxPost('',baseUrl+"/"+moduleId+"/search/globalautocomplete", 
                                {  
                                    searchType : ["answers"],
                                    filters:{
                                        '_id' : {'$in': [k]},
                                    },
                                    fields:["project"]
                                },
                                function(data){
                                    $("#ajaxFormModal #parentId").val(Object.values(data.results)[0]["project"]["id"]);
                                },null);
                            })
                        }
                        delete finder.object.answer;
                    },
                    afterSave : function(data){
                        dataHelper.path2Value( {//change answerId
                            id : data.id,path: "parentId",collection: "actions",value : $("#ajaxFormModal #parentId").val() 
                        }, function() {
                            kanObj = $.extend(kanabanActionObj);
                            var tplCtx = {
                                id : data.id,
                                collection: "actions",
                                path: "links",
                                value : {
                                    "contributors" :{}
                                }
                            }
                            tplCtx.value.contributors[userId] = {
                                "type" : "citoyens",
                                "name" : userConnected.name                        
                            }

                            dataHelper.path2Value( tplCtx, function(params) { //add contributors
                                var depenseObj = {
                                    id : $("#ajaxFormModal #answerId").val(),
                                    collection: "answers",
                                    path: "answers.aapStep1.depense",
                                    arrayForm: true,
                                    edit:false,
                                    value : {
                                        "date" : moment().format('DD/MM/YYYY'),
                                        "group" : "Feature",
                                        "nature" : "fonctionnement",
                                        "poste" : $("#ajaxFormModal #name").val(),
                                        "price" : $("#ajaxFormModal #credits").val(),
                                        "actionid" : params.id,
                                        "financer" : [],
                                        "votes" : {}
                                    }
                                }
                                dataHelper.path2Value(depenseObj, function() { //create ligne de depense
                                    urlCtrl.loadByHash(location.hash);
                                    kanObj.actions.scroll(params.id);
                                })
                            } );
                        })
                    },
                    "onload" : {
                        "actions" : {
                            "setTitle" : trad.addAction,
                            "html" : {
                                "infocustom" : "<br/>Une Action permet de faire avancer votre projet ou le fonctionnement de votre association"
                            },
                            "hide" : {
                                "breadcrumbcustom" : 1,
                                "tagstags":1,
                                "urlsarray":1,
                                "startDatedate":1,
                                "endDatedate":1
                            }
                        }
                    }
                },
                scroll : function(idAction){
                    setTimeout(function(){
                        var targetDivAction = $(".action"+idAction)
                        targetDivAction.parent().animate({scrollTop: targetDivAction.offset().top-580},'slow');
                        $('#collapse'+idAction).addClass('in');
                        targetDivAction.css('border',"3px solid green");
                        setTimeout(function(){targetDivAction.css('border',"0px");},2800)
                    },2500)
                },
                actionObservatory : function(){
                    $answers.push("61f3d6b2801c8d529c21b844");
                    params = {  
                        searchType : ["actions"],
                        filters:{
                            'answerId' : {'$in': $answers},
                        }
                    }
                    params.filters['links.contributors.'+viewParams.actionOf] = {'$exists': true}
                    ajaxPost(
                        null,
                        baseUrl+"/survey/answer/directory/source/"+costum.slug+"/form/"+aapObject.formParent._id.$id,
                        params,
                        function(data){
                            data.todoCount = 0;
                            data.successCount = 0;

                            data.totalTasks = 0;
                            data.notCheckedTask = 0;
                            data.checkedTask = 0;
                            data.payedTask = 0;
                            data.lateTasks = [];
                            data.lateActions = [];
                            
                            Object.values(data.results).forEach(function(value,index){
                                if(value.status == "todo") data.todoCount++; // count todo action
                                if(value.status == "success") data.successCount++; //count success action
                                if(exists(value["tasks"])){
                                    data.totalTasks += value["tasks"].length // total tasks
                                    value["tasks"].forEach((vt,kt)=>{
                                        vt.endDate = exists(vt.endDate) ? (typeof vt.endDate == "object" ? moment.unix(vt.endDate.sec).format('DD/MM/YYYY'):vt.endDate) : ""
                                        if(exists(vt.checked) && (vt.checked == true || vt.checked == "true"))
                                            data.checkedTask ++; // count checked task
                                        else
                                            data.notCheckedTask ++; //count not checked task
                                        if(exists(vt.payed) && (vt.payed == true || vt.payed == "true"))
                                            data.payedTask++; //count payed task
                                        if(exists(vt.endDate) && (typeof vt.checked == "undefined" || (typeof vt.checked != "undefined" && (vt.checked=="false" || vt.checked==false)))){
                                            vt.epoch = moment(vt.endDate, "D/M/YYYY").valueOf();
                                            vt.today = moment(today, "D/M/YYYY").valueOf();
                                            if(vt.epoch < vt.today){
                                                data.lateTasks.push(vt.epoch);
                                                if(data.lateActions.includes(value._id.$id) == false)
                                                    data.lateActions.push(value._id.$id)
                                            }
                                        }
                                    })
                                }
                            })

                            $('#total-action').text(Object.keys(data.results).length);
                            $('#todo-action').text(data.todoCount);
                            $('#success-action').text(data.successCount);
                            $('#total-task').text(data.totalTasks);
                            $('#not-checked-task').text(data.notCheckedTask);
                            $('#checked-task').text(data.checkedTask);
                            $('#payed-task').text(data.payedTask);
                            $('#late-task').text(data.lateTasks.length);
                            $('#late-action').text(data.lateActions.length);
                        }
                    )
                },
                searchByPerson : function(){
                    ajaxPost('',baseUrl+"/co2/element/get/type/citoyens/id/"+viewParams.actionOf, 
                    null,
                    function(data){
                        $("#current-action-of").html(`<a href="#page.type.${data.map.collection}.id.${data.map._id.$id}" class="letter-green lbh-preview-element">${data.map.name}</a>`);
                        coInterface.bindLBHLinks();
                    },null);

                    $("#search-person").select2({
                        minimumInputLength: 3,
                        // tags: true,
                        // multiple: false,
                        "tokenSeparators": [','],
                        createSearchChoice: function (term, data) {
                            if (!data.length)
                                return {
                                    id: term,
                                    text: term
                                };
                        },
                        ajax: {
                            url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                            dataType: 'json',
                            type: "POST",
                            quietMillis: 50,
                            data: function (term) {
                                return {
                                    name: term,
                                    searchType : ["citoyens"],
                                    filters : {
                                        "links.<?=$linksType ?>.<?=(string)$el["_id"] ?>": { "$exists": "true" }
                                    }
                                };
                            },
                            results: function (data) {
                                return {
                                    results: $.map(Object.values(data.results), function (item) {
                                        return {
                                            text: item.name,
                                            id: item._id.$id
                                        }
                                    })
                                };
                            }
                        }
                    })
                    .on('change',()=>{
                        location.hash = location.hash.split("?")[0]
                        history.pushState(null,null,location.hash+"?actionOf="+$('#search-person').val());
                        urlCtrl.loadByHash(location.hash);
                    });
                },
                getCurrentProject : function(){
                    $("#current-project-of-text").hide();
                    if(viewParams.projectOf != ""){
                        ajaxPost('',baseUrl+"/co2/element/get/type/projects/id/"+viewParams.projectOf, 
                        null,
                        function(data){
                            data=data.map;
                            $("#current-project-of-text").show();
                            $("#current-project-of").html(`<a href="#page.type.${data.collection}.id.${data._id.$id}" class="letter-green lbh-preview-element">${data.name}</a>`);
                            coInterface.bindLBHLinks();
                        },null);
                    }
                }
            },
            events : function(kanObj){
                kanObj.actions.searchByPerson();
                kanObj.actions.getCurrentProject();
                // if(projectOf != "" || projectOf != null)
                //     $("#current-project-of").html(`<a href="javascript:;" class="letter-green">${data.name}</a>`);
                var kanbanContainerWidth= $("#myKanban .kanban-container" ).width()
                $("#myKanban1 div").css("width",kanbanContainerWidth);
                $("#myKanban1").scroll(function(){
                    $("#myKanban").scrollLeft($("#myKanban1").scrollLeft());
                });
                $("#myKanban").scroll(function(){
                    $("#myKanban1").scrollLeft($("#myKanban").scrollLeft());
                });
                $('[data-toggle="popover"]').popover();
                $("#mapOfResultsAnswL").remove();
                $('.tablecheckicon').off().on('click', function(e){
                    var tablecheckiconpos = $(this).data("pos");
                    var tablecheckiconcollection = "actions";
                    var tablecheckiconid = $(this).data("id");
                    var checkKey = $(this).data("key");
                    var checkValue = $(this).data("value");
                    var taskName = $(this).data("task-name");
                    var projectSlug = $(this).data("project-slug");
                    var btn= $("#check"+tablecheckiconid+tablecheckiconpos);

                    tplCtx.collection = tablecheckiconcollection;
                    tplCtx.id = tablecheckiconid;
                    tplCtx.path = "tasks."+tablecheckiconpos+"."+checkKey;
                    tplCtx.value = checkValue;
                    tplCtx.format= true;

                    dataHelper.path2Value( tplCtx, function() {
                        tplCtx.path = "tasks."+tablecheckiconpos+".checkedUserId";
                        tplCtx.value = userId;
                        dataHelper.path2Value( tplCtx, function(params) {
                            var today = new Date();
                            tplCtx.path = "tasks."+tablecheckiconpos+".checkedAt";
                            tplCtx.value = today.toISOString();
                            if (typeof rcObj != "undefined" && (checkValue==true || checkValue== "true" )) {
                                rcObj.postMsg({
                                    "channel": "#" + projectSlug,
                                    "text": userConnected.name +
                                        "a terminé la sous-tâche " + taskName + "sur l'action " + params.elt.name
                                }).then(function (data) {
                                });
                            }
                            dataHelper.path2Value( tplCtx, function(params) {
                                if(exists(params.elt.tasks[tablecheckiconpos].payed) && params.elt.tasks[tablecheckiconpos].payed == true){
                                    btn.html("<i class='fa fa-euro'></i>");
                                    btn.removeClass('bg-green').removeClass('btn-default').addClass("bg-green-k");
                                }else if(exists(params.elt.tasks[tablecheckiconpos].checked) && params.elt.tasks[tablecheckiconpos].checked == true){
                                    btn.html("<i class='fa fa-check'></i>");
                                    btn.removeClass('btn-default').removeClass('bg-green-k').addClass("bg-green");
                                }else{
                                    btn.html("<i class='fa fa-hourglass fa-pulse'></i>")
                                    btn.removeClass('bg-green').removeClass('bg-green-k').addClass("btn-default");
                                }
                            } );
                        } );
                    } );
                });
                $('.projectOf').off().on('click',function(){
                    var id = $(this).data("project-id");
                    if(location.hash.indexOf('?') != -1)
                        history.pushState(null,null,location.hash+"&projectOf="+id+"&actionByproject=true");
                    else{
                        history.pushState(null,null,location.hash+"?projectOf="+id+"&actionByproject=true");
                    }
                    urlCtrl.loadByHash(location.hash);
                })
                $('.new-task').off().on('click',function(){
                    var idAction = $(this).data('id');
                    var projectSlug = $(this).data('project-slug');
                    dyFObj.openForm(kanObj.tasks.dynForms(idAction,null,projectSlug))
                })
                $('.edit-task').off().on('click',function(){
                    var idAction = $(this).data('id');
                    var position = $(this).data('pos');
                    var task = $(this).data('task');
                    var projectSlug = $(this).data('project-slug');
                    dyFObj.openForm(kanObj.tasks.dynForms(idAction,position,projectSlug),null,task)
                })

                $('.delete-task').off().on('click',function() {
                    var pos = $(this).data("pos");
                    tplCtx = {};
                    tplCtx.collection = "actions";
                    tplCtx.id = $(this).data("id");
                    tplCtx.path = "tasks."+pos;
                    tplCtx.pull = "tasks";
                    tplCtx.value = null;
                    prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    dataHelper.path2Value( tplCtx, function(params){
                                        urlCtrl.loadByHash(location.hash);
                                        kanObj.actions.scroll(params.id)
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });

                    prioModal.modal("show");
                });
                $('.contributor-counter').on('click',function(){
                    setTimeout(() => {
                        coInterface.bindLBHLinks();
                    }, 700);
                })

                $('.new-action').off().on('click',function(){
                    dyFObj.openForm('action',null, null,null,kanObj.actions.dynforms);
                })

                $('.btn-personal-observatory').off().on("click",function(){
                    $('#modal-observatory-personal').modal();
                    kanObj.actions.actionObservatory();
                })
                $('.switch-action-view').off().on('click',function(){
                    if($(this).data("view") =="action-by-state"){
                        location.hash = location.hash.split("?")[0];
                        history.pushState(null,null,location.hash+"?actionOf="+viewParams.actionOf);
                    }
                    else if($(this).data("view") =="action-by-project"){
                        if(location.hash.split("?").length != 0){
                            var lh = location.hash.split("?");
                            if(lh.indexOf("projectOf") && lh[1].split("&").length !=0){
                                lh = lh[1].split("&");
                                $.each(lh,function(k,v){
                                    if(typeof v == "string" && v.indexOf("projectOf") !=-1)
                                        lh.splice(lh.indexOf(v),1)
                                    if(typeof v == "string" && v.indexOf("actionByproject") !=-1)
                                        lh.splice(lh.indexOf(v),1)
                                });
                                location.hash = location.hash.split("?")[0]+ "?"+lh.join("")+"&actionByproject=true";
                                history.pushState(null,null,location.hash);
                            }
                        }
                    }
                    urlCtrl.loadByHash(location.hash)
                })
                $(".content-answer-search").removeClass("col-lg-9").addClass("col-lg-12")
                /*if(localStorage.getItem("action-by-project") == "true")
                    $('.drag_handler').css("visibility","hidden");*/
            },
            initKanban: function(kanObj){
                $('#myKanban').html("");
                var KanbanTest =null;
                KanbanTest = new jKanban({
                    element: "#myKanban",
                    gutter: "10px",
                    widthBoard: /*location.hash.indexOf("actionByproject") >= 0 ? "800px":*/"500px",
                    itemHandleOptions:{
                        enabled: true,
                        // customCssHandler:"",
                        // customCssIconHandler:"fa fa-bars",
                        // customItemLayout:""
                    },
                    click: function(el) {
                        mylog.log("Trigger on all items click!");
                    },
                    context: function(el, e) {
                        mylog.log("Trigger on all items right-click!");
                    },
                    dropEl: function(el, target, source, sibling){
                        mylog.log(target.parentElement.getAttribute('data-id'));
                        mylog.log(el, target, source, sibling)
                    },
                    buttonClick: function(el, boardId) {
                        // var ansurl = "slug.formcarryconfig1.formid.6180e84cddcd1223be6ec912.aappage.form";
                        // smallMenu.openAjaxHTML(baseUrl+'/survey/form/getaapview/urll/'+ansurl);
                        
                    },
                    itemAddOptions: {
                        // enabled: true,
                        // content: '+ Nouvelle proposition',
                        // class: 'custom-button',
                        // header: true,
                        // footer:true
                    },
                    boards: kanObj.createKanbanBoards(kanObj)
                });
            }
        }
        //************************************************************************* */
        var copyKanbanObj = jQuery.extend({}, kanabanActionObj);
        copyKanbanObj.init(copyKanbanObj);
    })
</script>