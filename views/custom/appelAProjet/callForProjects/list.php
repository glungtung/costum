<?php
	if(empty(Yii::app()->session['userId'])){
		echo "<h3 class='text-center text-green'>".Yii::t("common","Connect you")."</h3>";
		echo "<script> $('.form-login').modal('show'); </script>";
		return;
	}
	HtmlHelper::registerCssAndScriptsFiles(['/plugins/html5sortable/html5sortable.js', '/css/flex-layout.css'], Yii::app()->request->baseUrl);
	$canAdmin = false;
	$canEditEachOtherAnswer = false;
	$canReadEachOtherAnswer = false;
	$p_active = false;
	$elformId = gettype($elform["_id"]) == 'object' ? (string)$elform["_id"] : $elform["_id"]["$"."id"];
	if(!isset($elform['subType'])) {
		if(isset($elform['active']) && ($elform['active'] == true || $elform['active'] == 'true'))
			$p_active = true;
		if(
            (
              //is form active
              isset($elform["active"]) && 
              ($elform["active"] == true || $elform["active"] == 'true')
            )
            &&
            ( 
              //is form between start and end date 
              !isset($elform["startDate"]) || 
                  ( time() > strtotime(str_replace("/","-",$elform["startDate"])) && 
                    ( !isset($elform["endDate"]) || time() <= strtotime(str_replace("/","-",$elform["endDate"])))) ||
                    (isset($elform["endDate"]) && $elform["endDate"] == "")
            )
            &&
            (
              //private
              !isset($elform["private"]) ||
              (
                isset($elform["private"]) &&
                  ($elform["private"] == false || $elform["private"] == 'false')
              )
              ||
              (
                  isset($elform["private"]) &&
                  ($elform["private"] == true || $elform["private"] == 'true') && Link::isLinked((string) $el['id'], $el['type'], Yii::app()->session["userId"], null) 
              )
            )

		) {
            $canEditEachOtherAnswer = true;
		}
		if(
            (
              //is form active
              isset($elform["active"]) && 
              ($elform["active"] == true || $elform["active"] == 'true')
            )
            &&
            ( 
              //is form between start and end date 
              !isset($elform["startDate"]) || 
                  ( time() > strtotime(str_replace("/","-",$elform["startDate"])) && 
                    ( !isset($elform["endDate"]) || time() <= strtotime(str_replace("/","-",$elform["endDate"])))) ||
                    (isset($elform["endDate"]) && $elform["endDate"] == "")
            )
			&&
            (
              //private
              !isset($elform["private"]) ||
              (
                isset($elform["private"]) &&
                  ($elform["private"] == false || $elform["private"] == 'false')
              )
              ||
              (
                  isset($elform["private"]) &&
                  ($elform["private"] == true || $elform["private"] == 'true') && Link::isLinked((string) $el['id'], $el['type'], Yii::app()->session["userId"], null) 
              )
            )
            &&
            (
              //private
              !isset($elform["canReadOtherAnswers"]) ||
              (
                isset($elform["canReadOtherAnswers"]) &&
                  ($elform["canReadOtherAnswers"] == true || $elform["canReadOtherAnswers"] == 'true')
              )
            )

		) {
            $canReadEachOtherAnswer = true;
		}
	}
	if((isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) || Form::canAdmin($elformId))
		$canAdmin = true;

	$projects = PHDB::find(Project::COLLECTION,array("parent.".array_keys($elform['parent'])[0] => ['$exists'=>1]),array("_id"));
	$projectsId = array_keys($projects);

?>


	<div class="container no-padding fixed-breadcrumb-filters">
		<div class="col-xs-12 no-padding">
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="z-index: 1;">
			<ul class="breadcrumb bg-white" style="display: inline;">
				<li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
				<li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li>
				<li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $elformId ?>.aappage.list ">"<?= Yii::t("common", "List of proposals") ?>"</a></li>
			</ul>
			<!-- <a data-toggle="collapse" href="javascript:;" data-target="#displayType" class="dropmenu-item">
				<span class="dropmenu-item-label">Displayed items</span>
				<span class="dropmenu-item-content">5</span>
			</a> -->
			<ul id="displayType" class="margin-top-10" style="list-style: none;float:right">
				<li class="btn-breadcrumb">
					<!-- <a href="javascript:;" class="kanbanaap margin-right-5" ><i class="fa fa-2x fa-trello" aria-hidden="true"></i></a> -->
					<div class="margin-right-5 dropdown">
						<a href="javascript:;" class="dropdown-toggle" type="button" data-toggle="dropdown">
							<i class="fa fa-trello" aria-hidden="true"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="javascript:;" class="small change-list-view margin-right-5" data-action="kanbanaap">Propositions</a></li>
							<li><a href="javascript:;" class="small change-list-view margin-right-5" data-action="kanban-personal-action">Actions personnelles</a></li>
							<li><a href="javascript:;" class="small change-list-view margin-right-5" data-action="project_kanban">Projets</a></li>
						</ul>
					</div>
				</li>
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="3 par ligne">
					<a href="javascript:;" class="change-list-view margin-right-5 hidden" data-action="compact" ><i class="fa fa-th" aria-hidden="true"></i></a>
				</li>
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Liste">
					<a href="javascript:;" class="change-list-view margin-right-5" data-action="minimalist"><i class="fa fa-align-justify" aria-hidden="true"></i></a>
				</li>
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Complet">
					<a href="javascript:;" class="change-list-view margin-right-5" data-action="detailed"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
				</li>
				<?php if(isset($el["el"]["slug"]) && $el["el"]["slug"] == "openAtlas"){ ?>
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Powerpoint">
					<a href="javascript:;" class="change-list-view margin-right-5" data-action="ppt"><i class="fa fa-arrows-h" aria-hidden="true"></i></a>
				</li> 
				<?php } ?>
                <li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Tableau">
                    <a href="javascript:;" class="change-list-view margin-right-5" data-action="table"><i class="fa fa-table" aria-hidden="true"></i></a>
                </li>
				<?php if(!empty($elform["params"]["showMap"]) && $elform["params"]["showMap"]==true){ ?>
				<li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="<?= Yii::t("common", "Map") ?>">
					<a href="javascript:;" class="margin-bottom-10 tooltips list-aap-map" data-target="#list-aap-map" data-toggle="tooltip" data-placement="bottom" ><i class="fa fa-map"></i></a>
				</li>
				<?php } ?>
			</ul>
		
		</div>
	</div>

	<div class="list-aap-container container-fluid no-padding">
	<!-- <div class="list-aap-container <?= @$el_configform["showMap"]==true ? "cfm container-fluid" : "container" ?>"> -->
	<?= isset($elform['config']) ? (!empty($el["el"]["slug"]) && $el["el"]["slug"] == 'coSindniSmarterre' ? $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.exportation-csv-cosindnismarterre", ["form" => $elformId]) : $this->renderPartial("survey.views.custom.exportation-csv.appel_a_projet", ["form" => $elformId])) : '' ?>
		<div class="d-flex container no-padding justify-content-center flex-xs-column flex-sm-column flex-md-row flex-lg-row">
			<!-- <div class="col-xs-12 col-md-2 text-center no-padding"> -->
			<div class="col-xs-12 col-md-3 col-lg-3 text-center no-padding menu-filters-lg" style="display: none;" id="filterContainerAapOceco">
				<div id='filterContainerL' class='menu-verticale searchObjCSS'></div>
			</div>
			<button type="button" class="btn btn-default showHide-filters-xs hidden-xs" style="height: fit-content;" id="show-filters-lg">
				<span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
				<i class="fa fa-filter"></i>
			</button>
			<?php if($canAdmin && !isset($elform["subType"])) { ?>
				<a href="javascript:;" class="openFilterParams" style="display: none; height: fit-content; left: -16px; transform: scale(.8)">
					<i class="fa fa-gear fa-2x"></i>
				</a>
			<?php } ?>

			<div class="content-answer-search col-xs-12 no-padding"> 
				<div class="headerSearchContainerL col-xs-12 no-padding"></div>
				<div id="centerFilters-active" class="col-xs-12 no-padding"></div>
				<div id="centerBtnSaveFilter" class="center-save-btn"></div>
				<div class="bodySearchContainer margin-top-10">
					<div class="no-padding col-xs-12 text-left footerSearchContainer margin-bottom-30 pull-right"></div> 
					<div class="col-xs-12 propositions-aap-list" id="dropdown_searchL">
						<p class="text-center"><i class="fa fa-3x fa-spinner fa-spin"></i></p>
					</div>
					<div class="no-padding col-xs-12 text-left footerSearchContainer pull-right"></div>   
				</div>
			</div>
			<?php if(!empty($elform["params"]["showMap"]) && $elform["params"]["showMap"]==true){ ?>
				<div class="col-xs-12 col-sm-6 map-aap-container">
					<div id="mapOfResultsAnswL" style="height:700px; position: sticky;top: 80px;"></div>
				</div>
			<?php } ?>
		</div>
	</div>

    <div class="form-group col-xs-12 addfromcsvdiv" style="display: none" >
    <div>
        <label class="info text-center text-dark-blue"> <h4> Importer depuis un fichier csv </h4></label>
    </div>
    <label for="inputcsvdata" > <input type="file" id="inputcsvdata" class="inputcsvdata" name="inputcsvdata" accept=".csv"> </label>
    <div class="col-md-12">
        <div class="form-group col-md-4">
             <label for="delimiterinput">Delimiteur</label>
                <select name="delimiterinput" class="form-control delimiterinput">
                    <option value=","> Virgule </option>
                    <option value=";"> Point virgule </option>
                </select>
        </div>
        <div class="form-group col-md-4">
            <label for="delimiterendlineinput">Delimiteur fin de ligne</label>
            <select name="delimiterendlineinput" class="form-control delimiterendlineinput">
                <option value="ral"> Retour à la ligne </option>
                <option value="bs"> Backslash </option>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="headerinput">Avec intitulé à l'entete</label>
            <select name="headerinput" class="form-control headerinput" >
                <option value="true"> Oui </option>
                <option value="false"> Non </option>
            </select>
        </div>
    </div>

    <div class="col-md-12 containertablecss">
        <div class="addfromcsvdivrs csvdivcontainer">  </div>
    </div>
</div>

    <div class="modal fade" id="formmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
	//$(function(){
		//"use strict";
		var projectId = <?php  echo json_encode($projectsId) ?>;
		var isOcecoform = <?= isset($el_configform["subType"]) && $el_configform["subType"] == "ocecoformConfig" ? "true" : "false" ?>;
		var hash = location.hash.split("?")[1];
		const $subType = <?php echo isset($elform["subType"]) ? json_encode(true) : json_encode(false) ?>;
		var viewParams = {
			actionOf : userId,
			projectOf : "",
			actionByproject : false,
			actionByStatus : false,
			notSeen : false,
			view: (typeof formStandalone != "undefined" && formStandalone ? "compact" : ($subType ? "detailed" : 'compact')),
		}

		var $canAdmin = <?php echo json_encode($canAdmin)  ?>;
		var $canEditEachOtherAnswer = <?php echo json_encode($canEditEachOtherAnswer) ?>;
		var $canReadEachOtherAnswer = <?php echo json_encode($canReadEachOtherAnswer) ?>;
		var $p_active = <?php echo json_encode($p_active) ?>;
		if(exists(hash)){
			var arrHash = hash.split("&");
			$.each(arrHash,function(k,v){
				k = v.split('=')[0];
				v =  v.split("=")[1];
				if(k=="actionOf") viewParams.actionOf=v;
				if(k=="projectOf") viewParams.projectOf=v;
				if(k=="actionByproject") viewParams.actionByproject = v;
				//if(k=="notSeen") viewParams.notSeen = "notSeen";
				if(k=="view") viewParams.view = v
			})
			//if(location.hash.indexOf("actionOf=") != -1) viewParams.view = "kanban-personal-action";
		}

		if(exists(jsonHelper.getValueByPath(aapObject,"formParent.params.showMap")) && aapObject.useMap && aapObject.useMapInView())
			var mapAnsw = new CoMap({
				container : "#mapOfResultsAnswL",
				activePopUp : true,
				zoom : 0,
				mapOpt : {
					zoom: 0,
					latLon : ["-21.115141", "55.536384"],
				},
				mapCustom:{
					tile : "maptiler"
				},
				elts : [],
				buttons:{ 
					fullScreen: { allow: true }
				}
			});

		var listAapObj = {
			aapFiltersHistory : notEmpty(costum) ? window['history'+costum.slug] : window['history'+contextData.slug],
			activeFilters:{text : {}},
			init:function(userIdAction=null){
				var filterPrms = listAapObj.filtersParams(listAapObj);
				var filterSearch = listAapObj.initFilters(listAapObj,filterPrms);
				listAapObj.events(listAapObj,filterSearch);
				$.each(listAapObj.filtersParams(listAapObj).defaults.filters, function (idxdf , df){
					if( Array.isArray(df)){
						listAapObj.activeFilters[idxdf] = df;
					}else{
						listAapObj.activeFilters[idxdf] = [df]
					}
				});
				$.each(listAapObj.filtersParams(listAapObj).filters, function(index, value){
					if(typeof value.field != "undefined"){
						if(typeof activeFilters[index] == "undefined" && index != "text" && index != "interventionArea"){
							listAapObj.activeFilters[index] = [];
						} else if(index == "interventionArea"){
							listAapObj.activeFilters["answers.aapStep1.interventionArea"] = [];
						}
						if ((value.event == "text" || index == "text") && typeof listAapObj.activeFilters["text"][value.field] == "undefined"){
							listAapObj.activeFilters["text"][value.field] = "";
						}
					};
				});
			},
			filtersParams : function(lObj){
				let filterBuilded = {
				};
				if(aapObject.formParent.params) {
					if(aapObject.formParent.params.filterParams) {
						var filterParamsIter = 0;
						for(const [key, value] of Object.entries(aapObject.formParent.params.filterParams)) {
							if(key.toLocaleLowerCase().includes('text')) {
								filterBuilded[key] = {
									view : 'text',
									field : value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
									event : "text",
									placeholder : trad.search + ` ${trad.by} `+ value["labelFor"+ key.charAt(0).toUpperCase() + key.substring(1)]
								}
							}
							if(key.toLocaleLowerCase().includes('select')) {
								let listValue = [];
								if(aapObject.formParent.params[value["paramsKeyFor"+ key.charAt(0).toUpperCase() + key.substring(1)]]) {
									const keyUtil = key.charAt(0).toUpperCase() + key.substring(1);
									if(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["list"] || aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["options"]) 
										listValue = Object.values(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]])[0];
									if(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["global"]) {
										listValue = Object.values(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["global"])[0];
									}
								}
								const inputKeys = value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)].split(".");
								let inputType;
								if(aapObject.formInputs[inputKeys[1]]) {
									const inputData = Object.values(aapObject.formInputs[inputKeys[1]])[0]["inputs"];
									if(inputData[inputKeys[2]])
										inputType = inputData[inputKeys[2]].type
								}

								filterBuilded[key] = {
									view : 'accordionList',
									type : "filters",
									field : value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
									event : "filters",
									name : value["labelFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
									keyValue : inputType == "tpls.forms.select" ? false : true,
									list : listValue
								}
								// if(inputType == "tpls.forms.cplx.checkboxNew") filterBuilded[key]["multiple"] = true;
								mylog.log("input type", inputType)
								mylog.log("builded filter", filterBuilded)
							}
							if(filterParamsIter == 0) {
								if(Object.keys(filterBuilded)[0].includes("text")){
									delete filterBuilded.usersFilter
									filterBuilded.usersFilter = {}
								} else {
									filterBuilded = { usersFilter: {}, ...filterBuilded}
								}
							}
							filterParamsIter++
						}
					} else {
						filterBuilded.usersFilter = {}
					}
				}
				filterBuilded.sortBy = {
					view : "accordionList",
					type : "sortBy",
					name : trad.sortBy,
					event : "sortBy",
					list : {
						"Date de création croissant" : {
							"created" : 1,
						},
						"Date de création décroissant" : {
							"created" : -1,
						},
						"Date de mise à jour croissant" : {
							"updated" : 1,
						},
						"Date de mise à jour décroissant" : {
							"updated" : -1,
						},					
					}
				}
				/* filterBuilded.fav = {
					view : "savedFilters",
					tooltips : "Filtre enregisté",
					app : "search",
					event : "saveActiveFilters",
					appKey : "savedFilters",
					concernedForm : aapObject.formParent["_id"]["$id"]
				} */
				
				var paramsFilterL = {
					container : "#filterContainerL",
					urlData : baseUrl+"/costum/aap/directory/source/"+(notEmpty(costum) ? costum.slug : contextData.slug)+"/form/"+aapObject.formParent._id.$id,
					interface : {
						events : {
							page : true,
							/*scroll : true,
							scrollOne : true*/
						}
					},
					/*loadEvent:{
						default:"scroll"
					},*/
					filters : aapObject.formParent.subType ? {
						text : {
							view : "text",
							field : "answers.aapStep1.titre",
							event : "text",
							placeholder : "Nom du projet"
						},
						usersFilter : {
							
						},
						theme : {
							view : "megaMenuAccordion",
							type : "filters",
							field : "answers.aapStep1.tags",
							remove0: true,
							countResults: true,
							activateCounter:true,
							countFieldPath: "answers.aapStep1.tags",
							remove0: false,
							name : "<?php echo Yii::t("common", "search by theme")?>",
							event : "filters",
							list : exists(aapObject.formParent) && exists(aapObject.formParent["params"]) && exists(aapObject.formParent["params"]["tags"]) && exists(aapObject.formParent["params"]["tags"]["list"]) && notNull(aapObject.formParent["params"]["tags"]["list"]) ? aapObject.formParent["params"]["tags"]["list"] : []
						},
						acceptation : {
							view : "accordionList",
							type : "filters",
							field : "acceptation",
							name : "Retenu ou Rejeté",
							event : "filters",
							keyValue : false,
							list : {
								"retained" : "Retenu",
								"rejected" : "Rejeté",
								"pending" : "En attente"
							}
						},
						seen : {
							view : "accordionList",
							type : "filters",
							field : "seen",
							name : "Non vue",
							event : "filters",
							keyValue : false,
							list : {
								"notSeen" : "Non vue",
								"seen" : "Vue",
							}
						},
						urgency : {
							view : "accordionList",
							type : "filters",
							field : "answers.aapStep1.urgency",
							name : "Urgence",
							event : "filters",
							list :  {
								"Urgent" : "Urgent"
							}
						},
						status : {
							view : "accordionList",
							type : "filters",
							field : "status",
							name : "status",
							event : "filters",
							keyValue : false,
							list : <?= json_encode(Aap::$badgelabel)?>
						},
						sortBy : {
							view : "accordionList",
							type : "sortBy",
							name : trad.sortBy,
							event : "sortBy",
							list : {
								"Ordre alphabetique" : {
									"answers.aapStep1.titre" : 1,
								},
								"Plus de votes" : {
									"answers.aapStep2.allVotes" : -1,
								},
								"Moins de votes" : {
									"answers.aapStep2.allVotes" : 1,
								},
								"Date de création croissant" : {
									"created" : 1,
								},
								"Date de création décroissant" : {
									"created" : -1,
								},
								"Date de mise à jour croissant" : {
									"updated" : 1,
								},
								"Date de mise à jour décroissant" : {
									"updated" : -1,
								},					
							}
						},
					} : (
						aapObject.formParent.params ? (
							aapObject.formParent.params.filterParams ? filterBuilded :{
								usersFilter : {},
								sortBy : {
									view : "accordionList",
									type : "sortBy",
									name : trad.sortBy,
									event : "sortBy",
									list : {
										"Date de création croissant" : {
											"created" : 1,
										},
										"Date de création décroissant" : {
											"created" : -1,
										},
										"Date de mise à jour croissant" : {
											"updated" : 1,
										},
										"Date de mise à jour décroissant" : {
											"updated" : -1,
										},					
									}
								},
								/* fav : {
									view : "savedFilters",
									tooltips : "Filtre enregisté",
									app : "search",
									event : "saveActiveFilters",
									appKey : "savedFilters",
									concernedForm : aapObject.formParent["_id"]["$id"]
								} */
							}
						) : {
							sortBy : {
								view : "accordionList",
								type : "sortBy",
								name : trad.sortBy,
								event : "sortBy",
								list : {
									"Date de création croissant" : {
										"created" : 1,
									},
									"Date de création décroissant" : {
										"created" : -1,
									},
									"Date de mise à jour croissant" : {
										"updated" : 1,
									},
									"Date de mise à jour décroissant" : {
										"updated" : -1,
									},					
								}
							},
							/* fav : {
								view : "savedFilters",
								tooltips : "Filtre enregisté",
								app : "search",
								event : "saveActiveFilters",
								appKey : "savedFilters",
								concernedForm : aapObject.formParent["_id"]["$id"]
							} */
						}
					),
					notSourceKey:true,
					defaults:{
						notSourceKey : true,
						indexStep: "10",
						types:["answers"],
						forced:{
							filters:{
							}
						},
						filters: $subType == true ? {
							'form' :  aapObject.formParent._id.$id,
							'answers.aapStep1.titre' : {'$exists':true},
							'status':{'$not':"finish"},
							'project.id' : {'$exists': "false"}
						} : {
							'form' :  aapObject.formParent._id.$id,
							'updated' : {'$exists':true},
							'answers' : {'$exists': true}
						},
						sortBy:{"updated":-1},
						types: ["answers"],
						fields:["_id","answers.aapStep1","views"],
						tagsPath:"answers.aapStep1.tags" //default path of the tags
					},
					aapFooter : {
						subdom : 'aapSaveFilters',
						views : {
							fav : {
								view : "savedFilters",
								tooltips : "Filtre enregisté",
								// dom : "#aapSaveFilters",
								app : "search",
								event : "saveActiveFilters",
								appKey : "savedFilters",
								concernedForm : aapObject.formParent["_id"]["$id"]
							}
						}
					},
					header:{
						dom : ".headerSearchContainerL",
						options:{
							left:{
								classes :"col-xs-12 no-padding",
								group:{
								count : true,
								map : true
							}
							}
						},
						views : {
							map : function(fObj,v){
							/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
										'<i class="fa fa-map-marker"></i> '+trad.map+
									'</button>';*/
									return "";
						}
					},
					events : {
						map : function(fObj){
							/*setTimeout(() => {
								$(".list-group-item").click(function(e){
									e.stopPropagation();
									var pos = $(this).data('pos');
									mapAnsw.getOptions().markerList[pos].openPopup()
								})
							}, 2000);*/


							$(".btn-show-map-search").off().on("click", function(){
								$("#mapOfResultsAnswL").css({"left":"0px", "right":"0px", "bottom":"50px"});
								$("#mapOfResultsAnswL").show(200);
								$(".footerSearchContainer").addClass("affix");
								if(exists(jsonHelper.getValueByPath(aapObject,"formParent.params.showMap")) && aapObject.useMap && aapObject.useMapInView())
									mapAnsw.map.invalidateSize();
								$("body").css({"overflow":"hidden"});
								$("#mapOfResultsAnswL .btn-hide-map").off().on("click", function(){
									$("#mapOfResultsAnswL").hide(200);
									$("body").css({"overflow":"inherit"});
									$(".footerSearchContainer").removeClass("affix");	
								});
							});
						}
					}
					},
					results :{
						dom : "#dropdown_searchL",
						map : {
							showMapWithDirectory :true
						}
					}
				};

				if(notEmpty(costum) && typeof window[costum.slug+'Obj'] != "undefined"){
					window[costum.slug+'Obj'].init(window[costum.slug+'Obj']);
					if(exists(window[costum.slug+'Obj'].paramsFilters))
						paramsFilterL.filters = {...paramsFilterL.filters,...window[costum.slug+'Obj'].paramsFilters(window[costum.slug+'Obj'],aapObject.formParent,aapObject.elementAap)};
					if(exists(window[costum.slug+'Obj'].defaultFilters))
						paramsFilterL.defaults = {...paramsFilterL.defaults,...window[costum.slug+'Obj'].defaultFilters(window[costum.slug+'Obj'],aapObject.formParent,aapObject.elementAap)};
				}

				if(viewParams.view =="ppt"){
					paramsFilterL.defaults.indexStep = "15";
				}

                if (viewParams.view == "table" || viewParams.view == "kanbanaap"){
                    paramsFilterL.defaults.indexStep = "30";
					$('.footerSearchContainer').remove();
                }

				if(location.hash.split("?").length >= 2){ // filter by name on reload
					$.each(location.hash.split("?")[1].split('&'),function(k,v){
						if(v.indexOf("text=") >= 0){
							//v.split("=")[1]
							paramsFilterL.defaults.textPath = "answers.aapStep1.titre";
							paramsFilterL.defaults.text = v.split("=")[1];
						}
					})
				}

				if($canAdmin){
					aapObject.formParent.subType ? 
						paramsFilterL.filters.missingData = {
							view : "accordionList",
							type : "filters",
							field : "answers.aapStep1",
							name : "Données manquantes",
							typeList : "object",
							action : "filters",
							event : "exists",
							keyValue : false,
							list :  {
								"titre":{
									"label" : "Pas de titre",
									"field" : "answers.aapStep1.titre",
									"value" : false
								}
							}
						} : "";
					if(notEmpty(costum) && costum.contextSlug == "coSindniSmarterre"){
						paramsFilterL.filters.missingData.list.email = {
							"label" : "Pas d\"email",
							"field" : "answers.aapStep1.email",
							"value" : false
						}
					}
				}
				if(typeof formStandalone != "undefined" && formStandalone){
					paramsFilterL.defaults.filters.formStandalone = true;
				}

				if(viewParams.view == "project_kanban"){
					$.each(paramsFilterL.filters,function(k,v){
						if(k != "year" && k != "text"){
							delete paramsFilterL.filters[k]
						}
					})
					paramsFilterL.defaults = {
							notSourceKey : true,
							indexStep: 30,
							types:["answers"],
							filters:{
								'form' :  aapObject.formParent._id.$id,
								'answers.aapStep1.titre' : {'$exists':true},
								'project.id' : {'$exists':true},
							},
							sortBy:{"updated":-1},
							types: ["answers"],
							fields:["_id","answers.aapStep1.titre"],
					};
				}

				if(location.hash.indexOf("inproject=true") != -1){
					paramsFilterL.defaults.filters["project.id"] = {'$exists':true};
					$('.dynamic-breadcrumb').text(trad["Projects list"]);
				}
				if(exists(jsonHelper.getValueByPath(aapObject,"formParent.params.showMap")) && aapObject.useMap && aapObject.useMapInView())
					paramsFilterL["mapCo"]= mapAnsw;
				return paramsFilterL;
			},
			initFilters : function(lObj,paramsFilterL){
				var filterSearchL={};
				ajaxPost("", baseUrl+"/co2/person/getusersavedpref/type/citoyens/id/"+userId, 

					{"appKey": "savedFilters"},
					function(res){
						aapObject.userSavedFilters = {};
						if(res[aapObject.formParent._id['$id']]) {
							aapObject.userSavedFilters = res[aapObject.formParent._id['$id']];
						}
					},null,null,
					{
						async:false
					}
				);
				if(aapObject.formParent.subType) {
					/* paramsFilterL.filters.fav = {
						view : "savedFilters",
						tooltips : "Filtre enregisté",
						app : "search",
						event : "saveActiveFilters",
						appKey : "savedFilters",
						concernedForm : aapObject.formParent["_id"]["$id"]
					} */
				} else {
					aapObject.scrollTo = "#bodydashboard"
				}
				mylog.log("userSavedFilters", aapObject.userSavedFilters);
				if(Object.keys(aapObject.userSavedFilters).length > 1) {
					Object.assign(paramsFilterL.filters, {
						usersFilter: {
							view : "accordionListSavedFilter",
							name : trad.savedFilter,
							event : "activeSavedFilters",
							list : aapObject.userSavedFilters,
							typeList : "arrayOfObjects",
							tooltip : "bottom",
							appKey : "savedFilters",
							concernedForm : aapObject.formParent["_id"]["$id"]
						}
					})
				} else {
					if(paramsFilterL.filters.usersFilter) delete paramsFilterL.filters.usersFilter
				}
				filterSearchL = searchObj.init(paramsFilterL);
				//lObj.filtersHistory(lObj,filterSearchL);
				filterSearchL.header.set=function(fObj){
					var now = getCurrentDateTime();
					var statusDateHtml = "";
					var statusDate = isDateTimeInRange(now,aapObject.formParent.startDate,aapObject.formParent.endDate);
					if(statusDate.notStarted){
						statusDateHtml = '<span class="text-dark">| '+trad["You can start responding from"] +' : '+aapObject.formParent.startDate+' jusque au '+aapObject.formParent.endDate+'</span>';
					}else if(statusDate.finished){
						statusDateHtml = '<span class="text-red">| '+trad["Response time expired on"] +' : '+aapObject.formParent.endDate+'</span>';
						
					}else{
						statusDateHtml = '<span class="letter-green">| '+trad["Session in progress, End of session"] +' : '+aapObject.formParent.endDate+'</span>';
					}
					
					//var epoch = new Date(formattedDays[2], formattedDays[1] - 1, formattedDays[0]).getSeconds();
					var resultsStrL = (fObj.results.numberOfResults > 1) ? " dossiers" : "dossier";
					/*if(viewParams.view=="kanban-personal-action")
						resultsStrL = (fObj.results.numberOfResults > 1) ? " actions" : "action";*/
					if(notEmpty(costum) && typeof costum.hasRoles != "undefined" && $.inArray("Opérateur", costum.hasRoles)>=0  && !costum.isCostumAdmin)
						resultsStrL+= " en attente d'opérateur";
					$(fObj.header.dom+" .countResults").html('<i class=\'fa fa-angle-down\'></i> <span class="folderCount">' + fObj.results.numberOfResults + '</span> '+resultsStrL+' '+statusDateHtml);
				};

				filterSearchL.results.render=function(fObj, results, data){
					coInterface.showLoader(fObj.results.dom);
					var params = 
						{	
							el: {id : aapObject.elementAap.id,type:aapObject.elementAap.type},
							canEdit: data.canEdit,
							el_configform_id: aapObject.formConfig._id.$id,
							el_form_id : aapObject.formParent._id.$id,
							slug : notEmpty(costum) ? costum.slug : contextData.slug,
							view: viewParams.view,
							canAdmin : $canAdmin,
							types : filterSearchL.search.obj.types[0],
							allAnswers: Object.keys(results),
							is_coform: !$subType,
							p_active: $p_active,
							adminRight: $canAdmin,
							canEditEachOtherAnswer: $canEditEachOtherAnswer,
							canReadEachOtherAnswer: $canReadEachOtherAnswer,
							timezone : Intl.DateTimeFormat().resolvedOptions().timeZone,
						};
					ajaxPost(fObj.results.dom, baseUrl+"/survey/answer/views/tpl/costum.views.custom.appelAProjet.callForProjects.listItem", 
					params,
					function(){
						fObj.results.events(fObj);
					},null,null,
					{
						beforeSend : () => $(fObj.results.dom).html('<p class="text-center"><i class="fa fa-3x fa-spinner fa-spin"></i></p>'),
						async:false
					});
					fObj.results.events(fObj);
					fObj.results.addToMap(fObj, results);
				};

				filterSearchL.results.addToMap = function(fObj, results){
					var elts = [];
					$.each(results, function(resultKey, result){
						if(result.answers){
							$.each(result.answers, function(k, answer){
								if(exists(answer.adress) && exists(answer.adress.geo))
								elts.push(
									{
										id:result._id.$id,
										geo: answer.adress.geo,
										collection:"organization",
										name: "Association: "+notNull(answer.association)?answer.association:"",
									}
								)
							})
						}
					})
					if(exists(jsonHelper.getValueByPath(aapObject,"formParent.params.showMap")) && aapObject.useMap && aapObject.useMapInView())
						mapAnsw.addElts(elts);
				};

				filterSearchL.search.getUrlSearchParams = aapObject.sections.list.filters.getUrlSearchParams;			
				filterSearchL.search.init(filterSearchL);
				return filterSearchL;
			},
			filtersHistory : function(lObj,filterSearchL){
				if(notNull(JSON.parse(localStorage.getItem(lObj.aapFiltersHistory))) && (location.hash.indexOf('actionOf=')==-1 && location.hash.indexOf('view=kanban-personal-action')==-1)){
					filterSearchL.search.obj = JSON.parse(localStorage.getItem(lObj.aapFiltersHistory));
						if(location.hash.indexOf("?") && exists(location.hash.split('?')[1])){
							var paramsUrl = location.hash.split('?')[1].split('&');
							if(typeof paramsUrl != "undefined" && Array.isArray(paramsUrl)){
								$.each(paramsUrl,function(k,v){
									v = v.split('=');
									filterSearchL.search.obj[v[0]]=[v[1]];
								})
							}
						}
						if(exists(filterSearchL.search.obj.filters)){
							$.each(filterSearchL.search.obj.filters,function(k,v){
								if(Array.isArray(v)){
									$.each(v,function(kk,vv){
										if($('[data-key="'+vv+'"]').length!=0 && k != "allSousOrganisation")
											filterSearchL.filters.manage.addActive(filterSearchL, $('[data-key="'+vv+'"]'), true);
									})
								}else if(typeof v == "string" || typeof v == "number"){
									if(typeof v == "string" && v.indexOf('.')!=0)
										v = v.split('.').join('\\.');
									if($('[data-key="'+v+'"]').length!=0 && k != "allSousOrganisation")
										filterSearchL.filters.manage.addActive(filterSearchL, $('[data-key="'+v+'"]'), true);
								}
							})
						}
						if(exists(filterSearchL.search.obj.sortBy)){
							var selectorkey = Object.keys(filterSearchL.search.obj.sortBy)[0].split('.').join('\\.');
							var selectorvalue = Object.values(filterSearchL.search.obj.sortBy)[0];
							$('[data-key="'+selectorkey+'"][data-value="'+selectorvalue+'"]').click();
						}
						if(exists(filterSearchL.search.obj.text) && exists(filterSearchL.search.obj.textPath)){
							var v = filterSearchL.search.obj.textPath.split('.').join('\\.');
							$('[data-text-path="'+v+'"]').val(filterSearchL.search.obj.text)
							$('[data-text-path="'+v+'"]').keyup();
							//filterSearchL.filters.actions.text.launch(filterSearchL, $('[data-text-path='+v+']'));
						}
				}
			},
			scrollEvent: function(lObj, filterSearch) {
				if(aapObject.scrollTo) {
					position = $(window).scrollTop() -150;
					$('html, body').animate({ scrollTop: '+='+position+'px'}, 0);
				}
			},
			events : function(lObj,filterSearch){
				$('.change-list-view').off().on('click',function(e){
					var action = $(this).data('action');
					e.stopPropagation();
					if (action =="kanban-personal-action"){
						var lh = location.hash.split("?");
						if(lh.length != 0){
							lh = lh[0] + "?view=project_kanban&user="+userId;
							history.pushState(null,null,lh);
						}
					}else if((action =="kanbanaap")){
						location.hash = location.hash.split('?')[0];
						history.pushState(null,null,location.hash+"?view="+action)
					}else if((action =="project_kanban")){
						location.hash = location.hash.split('?')[0];
						history.pushState(null,null,location.hash+"?view="+action)
					}else{
						if(location.hash.indexOf("inproject=true") != -1){
							location.hash = location.hash.split('?')[0]
							history.pushState(null,null,location.hash+"?view="+action+"&inproject=true");
						}else{
							location.hash = location.hash.split('?')[0]
							history.pushState(null,null,location.hash+"?view="+action+"&inproject=false");
						}
					}
					urlCtrl.loadByHash(location.hash);
				})
				$.each(["minimalist","detailed","compact" ,"kanbanaap" ,"kanban-personal-action" , "table","ppt"],function(k,v){
					if(viewParams.view == v){
						
						if(v == "kanban-personal-action" && location.hash.indexOf("actionByproject=true")>0){
							$('.btn-breadcrumb [data-action="action-by-project"]').addClass("active-view");
						}else{
							$('.btn-breadcrumb [data-action="'+v+'"]').addClass("active-view");
						}
						if(v=="kanbanaap" || v== "kanban-personal-action")
							$('.btn-breadcrumb [data-action="'+v+'"]').parent().parent().parent().find('.dropdown-toggle').addClass("active-view");
					}

				})			

				$('.searchBarInMenu.pull-right').addClass('showHide-filters-xs-aap').html('<h6> Filtres <i class="fa fa-caret-down"></i></h6>');
				$('.showHide-filters-xs-aap').off().on('click', function() {
					if($(".menu-filters-xs").is(":visible"))	
						$(this).find("i").removeClass("fa-times").addClass("fa-filter");
					else
						$(this).find("i").removeClass("fa-filter").addClass("fa-times");
					$(".menu-filters-xs").slideToggle(500);
				});

				$('[data-type=actionOf],[data-type=projectOf],[data-type=actionByproject],[data-type=view]').remove();
				aapObject.sections.proposition.activeFilters = filterSearch.search.obj
				$("#filterContainerL .btn-filters-select").each(function(k,elmt){
					elmt.addEventListener("click",function(){
						//localStorage.setItem(lObj.aapFiltersHistory,JSON.stringify(filterSearch.search.obj))
						mylog.log("filter search def", filterSearch.search.obj)
						aapObject.sections.proposition.activeFilters = filterSearch.search.obj;
						lObj.scrollEvent()
					})
				})
				$("#filterContainerL .btn-saved-filters-select").each(function(k,elmt) {
					elmt.addEventListener("click",function(){
						mylog.log("filter search", filterSearch.search.obj);
						aapObject.sections.proposition.activeFilters = filterSearch.search.obj;
						lObj.scrollEvent()
					})
				})
				$('#activeFilters .filters-activate').each(function(k,elmt){
					elmt.addEventListener("click",function(){
						//localStorage.setItem(lObj.aapFiltersHistory,JSON.stringify(filterSearch.search.obj))
						aapObject.sections.proposition.activeFilters = filterSearch.search.obj;
						lObj.scrollEvent()
					})
				})

				$('#filterContainerL .main-search-bar-addon').each(function(k,elmt){
					elmt.addEventListener("click",function(){
						//localStorage.setItem(lObj.aapFiltersHistory,JSON.stringify(filterSearch.search.obj))
						aapObject.sections.proposition.activeFilters = filterSearch.search.obj;
						lObj.scrollEvent()
					})
				})

				$('#filterContainerL .main-search-bar').each(function(k,elmt){
					elmt.addEventListener("keyup",function(){
						setTimeout(() => {
							//localStorage.setItem(lObj.aapFiltersHistory,JSON.stringify(filterSearch.search.obj))
							aapObject.sections.proposition.activeFilters = filterSearch.search.obj;
							lObj.scrollEvent()
						}, 800);
					})
				})
				$('.btn-upload-csv').click(function () {
					prioModal = bootbox.dialog({
						message: $(".addfromcsvdiv").html(),
						show: false,
						size: "large",
						className: 'csvdialog',
						buttons: {
							success: {
								label: trad.save,
								className: "btn-primary",
								callback: function () {
									var ext = $('.csvdialog .inputcsvdata').val().split(".").pop();

									if ($.inArray(ext, ["csv"]) == -1) {
										toastr.error(tradDynForm.youMustUseACSVFormat);
										return false;
									}

									if ($('.csvdialog .inputcsvdata').prop('files')[0] != undefined) {

										var reader = new FileReader();

										reader.onload = function (e) {
											var csvval,
												csvdata = []
												allData = {};

											if ($(".csvdialog .delimiterendlineinput").val() == "ral"){
												var csvval = e.target.result.split("\n");
											} else if($(".csvdialog .delimiterendlineinput").val() == "bs"){
												var csvval = e.target.result.split("\\");
											}

											var collumnnumber = 0;
											var collummax = 0;

											$.each(csvval, function (ind, val) {
												var thh = val.split($(".csvdialog .delimiterinput").val());
												csvdata[ind] = thh;

												collumnnumber = 0;
												$.each(thh, function (thhid, thhhval) {
													collumnnumber++;
												});
												if(collumnnumber > collummax){
													collummax = collumnnumber ;
												}
											});

											var csvparams = [];

											var ii = 0

											while (ii < collummax) {
												var pscsv = {};
												pscsv["path"] = $(".csvdialog th[data-id='"+ii+"'] .inputpath").val();
												pscsv["type"] = $(".csvdialog th[data-id='"+ii+"'] .inputtype").val();
												if ($(".csvdialog th[data-id='"+ii+"'] .inputtitlepath").val() != ""){
													pscsv["tittle"] = $(".csvdialog th[data-id='"+ii+"'] .inputtitlepath").val();
												}
												csvparams.push(pscsv);
												ii++;
											}

											allData = {
												form : aapObject.formParent._id.$id,
												parent : {},
												params : csvparams,
												data : csvdata
											};


											if (typeof contextId != "undefined") {
												allData.parent[contextId] = {
													type: contextData.type,
													name: contextData.name,
												}
											}

											tot = allData["data"].length;
											totData = allData["data"];
											chunk = 50
											ix = 0;
											
											for( i=0; i-1 < tot/chunk ; i++ ){

												allData["data"] = totData.slice(ix, i*chunk);

												ajaxPost(
													'',
													baseUrl+"/survey/answer/importanswer",
													allData,
													function(data){
													},
													"html"
												);

												ix = i*chunk
											}

										};

										reader.readAsText($('.csvdialog .inputcsvdata').prop('files')[0]);

									} else {
										toastr.error(tradDynForm.weWereUnableToReadYourFile);
									}
									return false;
								}
							},
							cancel: {
								label: trad.cancel,
								className: "btn-secondary",
								callback: function () {
									prioModal.modal('hide');
								}
							}
						},
						onEscape: function () {
							prioModal.modal('hide');
						}
					});

					prioModal.on('shown.bs.modal', function (e) {
						$(".csvdialog select").change(function (e) {
							$(".inputcsvdata").trigger('change');
						});

						$(".inputcsvdata").change(function (e) {

							$(".addfromcsvdivrs").html('<span class="homestead"><i class="fa fa-spin fa-circle-o-noch"></i> ' + trad.currentlyloading + '...</span>')

							var ext = $(this).val().split(".").pop();

							if ($.inArray(ext, ["csv"]) == -1) {
								//toastr.error(tradDynForm.youMustUseACSVFormat);
								return false;
							}

							if (e.target.files != undefined) {

								var reader = new FileReader();

								reader.onload = function (e) {
									var csvval;
									if ($(".csvdialog .delimiterendlineinput").val() == "ral"){
										var csvval = csvdata = e.target.result.split("\n");
									} else if($(".csvdialog .delimiterendlineinput").val() == "bs"){
										var csvval = csvdata = e.target.result.split("\\");
									}

									var collumnnumber = 0;
									var collummax = 0;



									var str = '<table class="table table-bordered "><tbody>';
									$.each(csvval, function (ind, val) {
										str += '<tr>';
										var thh = val.split($(".csvdialog .delimiterinput").val());
										collumnnumber = 0;
										$.each(thh, function (thhid, thhhval) {
											str += '<th>' + thhhval + '</th>';
											collumnnumber++;
										});
										if(collumnnumber > collummax){
											collummax = collumnnumber ;
										}

										str += '</tr>';
									});

									var str2 = '<tr class="inputtr">';
									var ii = 0
									while (ii < collummax) {
										str2 += '<th class="inputth" data-id="'+ii+'">';
										str2 += '   <div class="form-group">';
										str2 += '       <label> Path </label>';
										str2 += '       <input style="min-width: 150px;" class="form-control inputpath" data-id"'+ii+'">';
										str2 += '   </div>';
										str2 += '   <div class="form-group">';
										str2 += '       <label> Type </label>';
										str2 += '       <select class="form-control inputtype" data-id="'+ii+'">'
										str2 += '           <option selected value=""> Chaine de caratéres</option>';
										str2 += '           <option value="int"> Nombre entier</option>';
										str2 += '           <option value="float"> Nombre </option>';
										str2 += '           <option value="isoDate"> Date (isoDate)</option>';
										str2 += '           <option value="timestamp"> Date (timestamp)</option>';
										str2 += '           <option value="checkbox"> checkbox</option>';
										str2 += '           <option value="radiobutton"> radiobutton</option>';
										str2 += '           <option value="array"> array</option>';
										str2 += '           <option value="financement"> financement</option>';
										str2 += '       </select>';
										str2 += '   </div>';
										str2 += '   <div class="form-group inputtitlepathdiv" data-id="'+ii+'" style="display: none">';
										str2 += '       <label> titre </label>';
										str2 += '       <input style="min-width: 150px;" class="form-control inputtitlepath" data-id"'+ii+'">';
										str2 += '   </div>';
										str2 += '</th>';
										ii++
									}
									str2 += '</tr>';

									str += '</tbody></table>';

									$(".addfromcsvdivrs")
										.html(str)
										.animate({scrollTop: 100000});

									setTimeout(function() {
										$(".addfromcsvdivrs .inputtype").change(function (e) {
											var thiss = $(this)
											if (thiss.val() == "financement") {
												$(".addfromcsvdivrs .inputtitlepathdiv[data-id='" + thiss.data('id') + "']").show();
											} else {
												$(".addfromcsvdivrs .inputtitlepathdiv[data-id='" + thiss.data('id') + "']").hide();
											}
										});
									}, 2000);

									$(".addfromcsvdivrs table").prepend(str2);

								};
								reader.readAsText(e.target.files.item(0));

							} else {
								toastr.error(tradDynForm.weWereUnableToReadYourFile);
							}
							return false;
						});
					});

					prioModal.modal("show");
				});
			}
		}
		listAapObj.init();
	//})
</script>
<!-- <h1><button id="gova">test socket</button></h1> -->
<script>
	$(document).ready(function() {
		$("#show-filters-lg .filter-xs-count").html("");
		$("#show-filters-lg .filter-xs-count").removeClass('animated bounceIn badge-success').addClass('hide')
		$(".filters-activate").on("click", function(e) {
			listAapObj.scrollEvent()
		})
	})
</script>