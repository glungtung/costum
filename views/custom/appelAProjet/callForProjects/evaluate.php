<style>
    .wrap #header{
        display: none;
    }
</style>
<div id="sticky-anchor"></div>
<div id="sticky">
    <div class="col-md-offset-1 col-md-10">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
            <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.aappage.evaluate ">Evaluation</a></li>
        </ul>
    </div>

</div>

<div id="mainDash" class="col-xs-12 col-lg-10 col-lg-offset-1">
    <i class="fa fa-2x fa-spinner fa-spin"></i>
</div>
<script>
    $(function(){
        var parentformid = "<?= (string) @$elform["_id"] ?>";
        var slug = "<?= (string) @$slug ?>";
        var el_slug = "<?= (string) @$el_slug ?>";
        ajaxPost("#mainDash", baseUrl+'/survey/answer/aapevaluate/parentformid/'+parentformid, 
            {
               slug :  slug,
               el_slug : el_slug
            },
            function(){
                // if (typeof hashUrlPage != "undefined") {
                // history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.evaluate."+parentformid);
                // }
                // $(".aap_plusdinfo").off().on("click", function () {
                //     window.location.href = $(this).data("url");
                //     urlCtrl.loadByHash(location.hash);
                // });
        },"html");
    })
</script>