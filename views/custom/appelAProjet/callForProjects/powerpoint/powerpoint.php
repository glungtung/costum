<?php  HtmlHelper::registerCssAndScriptsFiles([
    "/plugins/reveal-4.3.1/dist/reveal.css",
    /*"/plugins/reveal/css/theme/white.css",*/
    "/plugins/reveal-4.3.1/dist/reveal.js",
    /*"/plugins/reveal/lib/js/head.min.js",*/
    "/plugins/reveal-4.3.1/plugin/notes/notes.js",
    //"/plugins/reveal-4.3.1/plugin/markdown/marked.js",
    "/plugins/reveal-4.3.1/plugin/markdown/markdown.js",
    //"/plugins/reveal-4.3.1/plugin/highlight/highlight.js",
    ], Yii::app()->request->baseUrl); 

    $imgAnsw = (isset($answer["profilMediumImageUrl"])) ? Yii::app()->createUrl($answer["profilMediumImageUrl"]) : Yii::app()->getModule(Yii::app()
    ->params["module"]["parent"])
    ->getAssetsUrl() . "/images/thumbnail-default.jpg"; 
    ?>
<style>
    #ajax-modal.portfolio-modal.modal {
        top :0 !important;
        z-index: 1000001 !important;
    }
    .portfolio-modal.modal{
        z-index: 1000001 !important;
        top:0 !important;
    }
    .xdsoft_datetimepicker{
        z-index: 1000002 !important;
    }
    body .bootbox.modal {
        z-index: 2000000 !important;
    }
    .reveal-contatiner.reveal-contatiner-full{
        position: fixed;
        z-index: 1000000;
        display: flex;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        height: 100%;
        background-color: #fff;
    }
    .reveal-contatiner .menu-burget-ppt{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
    }
    .reveal-contatiner .menu-burget-ppt .dropdown-toggle{
        float: left; padding: 1px 0 0 5px;
    }
    .reveal-contatiner .reveal-full-screen,
    .reveal-contatiner .reveal-less-screen{
        position: absolute;
        top: 5px;
        right: 5px;
        z-index: 2;
    }
    .reveal-contatiner{
        position: relative;
        width:100%;
        height: 600px;
        border: 1px solid #ddd;
        border-radius: 5px;
    }
    .navigate-up,.navigate-down,.navigate-left,.navigate-right{
        /* background-color: #7da53d !important; */
        border-radius: 3px;
    }
    .reveal-viewport {
        overflow: auto !important;
    }
    .reveal-contatiner .scrollable-slide{
        display: block;
        height: 600px;
        position: relative;
        background: #fff;
        overflow-y: auto;
    }
    .reveal-contatiner .section-detail{
        height: 100%;
    }
    #modeSwitch,#col-banner{
        display: none;
    }
   /* .reveal .controls button.navigate-right,
    .reveal .controls button.navigate-left{
        visibility: visible !important;
        opacity: 1 !important;
    }*/
    .footerSearchContainer{
        display: none;
    }
    .budget-status{
        display: flex;
        justify-content: center;
        flex-direction:row;
    }
    .section-container{
        position:absolute;
        top:0;left:0;right: 0;bottom: 0;
    }
    #wizardcontainer{
        position:relative !important;;
        height: auto !important;;
        width: 100% !important;
        overflow-y: auto !important;
        overflow-x: hidden !important;
        padding : 0 10px;
    }
    /*.reveal .progress {
        position: absolute !important;
        display: none !important;
        height: 3px !important;
        width: 100% !important;
        bottom: 0 !important;
        left: 0 !important;
        z-index: 10 !important;
        background-color: rgba(0,0,0,.2) !important;
        color: #fff !important;
    }*/
    .preview-item{
        position: relative;
        width: 100%;
        height: 100px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 3px solid #ddd;
        margin: 2px 0;
        border-radius: 6px;
        cursor: pointer;
    }
    .preview-item.active{
        border: 3px solid #7da53d;
    }
    .preview-action{
        position: relative;
        height: 100%;
        overflow-y: auto;
        width: 215px;
    }
    .detail-action{ 
        padding: 10px;
        height: 100%;
        width: 100%;
        overflow-y: auto;
    }
    .action-container{
        display: flex;
        flex-direction: row;
        height: 100%;
    }
    .co-scroll2::-webkit-scrollbar-track {
        /* -webkit-box-shadow: inset 0 0 3px rgba(0, 0, 0, 0.9); */
        border-radius: 0px;
        background-color: #ddd;
    }
    .co-scroll2::-webkit-scrollbar-thumb {
        border-radius: 0px;
        background-color: #000;
        background-image: -webkit-linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.4) 50%, transparent, transparent)
    }
    .co-scroll2::-webkit-scrollbar-thumb:hover {
        background-color: #ddd;
        background-image: -webkit-linear-gradient(90deg, transparent, rgba(255, 255, 255, 0.4) 50%, transparent, transparent)
    }
    .co-scroll2::-webkit-scrollbar {
        width: 4px;
        background-color: #000;
    }
    .grid-action-img{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: center
    }
    .grid-action-img li{
        margin:5px
    }
    .grid-action-img li img{
        width: 218px;
        height: 172px;
        object-fit: cover;
    }
    .btn-delete-action-img{
        position: absolute;
        top: 0;
        right: 0;
        border-radius: 0;
    }
    .portfolio-item{
        position: relative;
    }
</style>

<div class="reveal-contatiner">
    <button class="btn btn-default btn-sm reveal-toogle-screen reveal-full-screen pull-right" data-value="full"> <i class="fa fa-expand"></i></button>
    <button class="btn btn-default btn-sm reveal-toogle-screen reveal-less-screen pull-right" data-value="less" style="display:none"> <i class="fa fa-compress"></i></button>
    <div class="reveal">
        <div class="slides">
            <?php foreach ($data["allAnswers"] as $answerid => $answer) { 
                    $ansVar = Aap::answerVariables($answer,$data,$data["stepEval"]);
                    $name= $ansVar["name"];
                    $description= $ansVar["description"];
                    $completedFields= $ansVar["completedFields"];
                    $percentageTasks= $ansVar["percentageTasks"];
                    $dataPer= $ansVar["dataPer"];
                    $iconTasks= $ansVar["iconTasks"];
                    $haveDepense= $ansVar["haveDepense"];
                    $seen= $ansVar["seen"];
                    $haveProject= $ansVar["haveProject"];
                    $countTask= $ansVar["countTask"];
                    $imgAnsw= $ansVar["imgAnsw"];
                    $retain= $ansVar["retain"];
                    $actions = $ansVar["actions"];
                    $urgentState = $ansVar["urgentState"];
                    $cummulMean = $ansVar["cummulMean"];
                    $votantCounter = $ansVar["votantCounter"];
                
                ?>
                <?php if(!empty($answer["answers"]["aapStep1"]["titre"])){ ?>
                    <section>
                        <section class="co-scroll" style="background-color:  rgb(255 255 255 / 87%);height: 100%;overflow:hidden" data-background-image="<?=$imgAnsw?>">
                            
                            <h1 class=""><?= @$answer["answers"]["aapStep1"]["titre"] ?></h1>
                            <div class="text-center">
                                <div class="">
                                    <i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?=@$users["data"][$answer["user"]]["name"] ?></i></b></small>
                                    Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y  ', $answer["created"]); ?></i></b></small>
                                </div>
                                <?php if($data["isActiveSousOrga"]){ ?>
                                    Parent : <a href="#page.type.<?= Aap::cacs($data["cacs"],$answer)['cacValue']['type'] ?>.id.<?= Aap::cacs($data["cacs"],$answer)['cacId'] ?>" class="letter-green lbh-preview-element"><?= Aap::cacs($data["cacs"],$answer)['cacValue']['name'] ?></a><br>
                                <?php } ?>

                                <ul class="tag-list margin-top-5">
                                    <?php
                                    if (!empty($answer["answers"]["aapStep1"]["tags"]) && is_array($answer["answers"]["aapStep1"]["tags"])){
                                        foreach ($answer["answers"]["aapStep1"]["tags"] as $ktag => $vtag){
                                            if(!empty($vtag))
                                            { ?>
                                                <a href="javascript:;" class="btn-tag-panel template-tag-panel" data-tag="<?=$vtag ?>">
                                                    <span class="badge  btn-tag tag" data-tag-value="cte" data-tag-label="cte">#<?=$vtag ?></span>
                                                </a>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                                
                            <div class="budget-status">
                            <div class="padding-left-10 padding-right-10 text-center">
                                Evaluation <br> 
                                <span class="bold" style="color: #000"><?= $votantCounter ?></span>
                            </div>
                            <div class="padding-left-10 padding-right-10 text-center">
                                Financement <br> 
                                <span class="bold" style="color: #000"><?= count(@$answer["answers"]["aapStep1"]["depense"]) ?></span>
                            </div>
                            <div class="padding-left-10 padding-right-10 text-center">
                                Action <br> 
                                <span class="bold" style="color: #000"><?= count($actions) ?></span>
                            </div>
                                <?php echo Aap::budgetStatus2($answer,$data["el"],$data["inputs"])["html"]; ?>
                            </div>

                            <div class="text-left padding-30 co-scroll" style="position:relative;width:100%;max-height:300px;overflow:auto">
                                <?php echo Aap::description($answer,600); ?>
                            </div>
                            <div class="menu-burget-ppt">
                                <?php echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.listItemButtons",array("answer" => $answer,"data" => $data, "ansVar"=>$ansVar)) ?>
                            </div>
                        </section>
                        <section class="co-scroll no-padding section-detail">
                            <h4 class="">Détails</h4>
                            <div class="text-left padding-30 co-scroll" style="position:relative;width:100%;max-height:100%;overflow:auto">
                            <?php if(!empty($data["inputs"]["inputs"])) 
                                foreach ($data["inputs"]["inputs"] as $kin => $vinp) {
                                    if(!empty($answer["answers"]["aapStep1"][$kin]) && !in_array($kin,["tags","description","depense","budget","titre"])){
                                        if(is_string($answer["answers"]["aapStep1"][$kin])){
                                            echo "<p> <b>".$vinp["label"]. "</b>: " .$answer["answers"]["aapStep1"][$kin]."</p><br>";
                                        }elseif(is_array($answer["answers"]["aapStep1"][$kin])){
                                            echo " <b>".$vinp["label"]. "</b>: ";
                                            echo "<ul>";
                                            foreach ($answer["answers"]["aapStep1"][$kin] as $kanss => $vanss) {
                                                if(is_string($vanss))
                                                    echo "<li class='margin-left-30'>".$vanss."</li>";
                                            }
                                            echo "</ul>";
                                            echo "<br>";
                                        }
                                    }
                                }
                            ?>
                            </div>
                        </section>
                        <!-- <section class="scrollable-slide co-scroll no-padding">
                            <div class="row form-container form-container-<?= $answerid ?>" data-id="<?= $answerid ?>">
                                <h1><i class="fa fa-spinner fa-spin fa-5" aria-hidden="true"></i></h1>
                            </div>
                        </section> -->
                        <section class="scrollable-slid co-scroll no-padding" style="height:100%">
                            <div class="action-container action-container-<?= $answerid ?>" data-id="<?= $answerid ?>">
                                <div class="preview-action preview-action-<?= $answerid ?> co-scroll2 no-padding"></div>
                                <div class="detail-action detail-action-<?= $answerid ?> co-scroll2" style="height:100%">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </div>
                            </div>
                        </section>
                    </section>
                <?php }  ?>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    "use strict";
    $(function(){
        var pptObj = {
            screen : notNull(localStorage.getItem("revealScreen")) ? localStorage.getItem("revealScreen") : "less",
            answers : <?= json_encode($data["allAnswers"]) ?>,
            imagesActions : [],
            actions : {},
            projects : {},
            answerid : null,
            getActions : function(pObj){
                ajaxPost(
                    null,
                    //baseUrl+"/costum/aap/getactionsbyanswer/answerid/61e6600a6d33797ee9652489",//+pObj.answer._id.$id,
                    baseUrl+"/costum/aap/getactionsbyanswer/answerid/"+pObj.answerid,
                    null,
                    function(data){
                        pObj.actions = data["actions"];
                        pObj.imagesActions = data["images"];
                        pObj.projects = data["projects"];
                        mylog.log(pObj.projects,"pObj.projects");
                    },null,null,{async:false}
                );
            },
            init : function(pObj){
                pObj.initReveal(pObj);
                pObj.toogleScreen(pObj);
            },
            previewMenu : function(pObj){
                var html = "";
                mylog.log(pObj.actions,"actionaka")
                if(Object.keys(pObj.actions).length > 0){
                    var j = 1;
                    $.each(pObj.actions,function(kact,vact){
                        var bgImg = (notEmpty(vact["media"]) && notEmpty(vact["media"]["images"]) && notEmpty(vact["media"]["images"][0])
                        && notEmpty(pObj.imagesActions) && notEmpty(pObj.imagesActions[(vact["media"]["images"][0])]) && 
                        notEmpty(pObj.imagesActions[(vact["media"]["images"][0])]["docPath"])) ? 
                            pObj.imagesActions[(vact["media"]["images"][0])]["docPath"] : "" ;
                        html += 
                        `<div class="col-xs-12">
                            <div class="preview-item" data-action-id="${kact}" style="background-image: url(${bgImg});background-size: cover;background-repeat: no-repeat;background-position: center;">
                                <a href="javascript:;">
                                    <h6 style="background: #ffffffb0;padding: 2px;">${j+"- "+vact.name}</h6>
                                </a>
                            </div>
                        </div>`;
                        j++;
                    })
                    html += 
                    `<div class="col-xs-12">
                        <div class="preview-item new-action">
                            <a href="javascript:;">
                                <h6 style="background: #ffffffb0;padding: 2px;"><i class="fa fa-plus fa-2x"></i></h6>
                            </a>
                        </div>
                    </div>`;
                }else{
                    html += 
                    `<div class="col-xs-12">
                        <div class="preview-item new-action">
                            <a href="javascript:;">
                                <h6 style="background: #ffffffb0;padding: 2px;"><i class="fa fa-plus fa-2x"></i></h6>
                            </a>
                        </div>
                    </div>`;
                }

                $('.preview-action-'+pObj.answerid).html(html);
                setTimeout(() => {
                    $('.preview-action-'+pObj.answerid).children(":first").children().addClass("active").click();
                    //$(".preview-item:first-child").addClass("active");
                }, 800);
                $('.preview-item').off().on('click',function(){
                    if($(this).hasClass("new-action")){
                        //dyFObj.openForm('action',null, null,null,pObj.dynForms.action(pObj));
                        $('.detail-action-'+pObj.answerid).html(`<h4>Action</h4><span class="text-red">Action vide</span>`);
                        dyFObj.openForm(pObj.dynForms.action(pObj))
                    }else{
                        var actionId = $(this).data('action-id');
                        var details = pObj.detailActions(pObj.actions[actionId]);
                        $(".preview-item").removeClass("active");
                        $(this).addClass("active");
                        $('.detail-action-'+pObj.answerid).html(`${details}`);
                        pObj.events(pObj);
                    }
                })

            },   
            detailActions : function(action){
                mylog.log(action,"actionko");
                var html = "<h4>Action</h4>";
                if(notEmpty(action)){
                    html += `
                        <h5>${notEmpty(action['name']) ? action['name'] : "Pas de nom" }</h5>
                        <ul class="list-unstyled col-xs-12 grid-action-img">`;
                        if(notEmpty(action["media"]) && notEmpty(action["media"]["images"]) && notEmpty(action["media"]["images"])){
                            $.each(action["media"]["images"],function(kimg,vimg){
                                var img = notEmpty(pptObj.imagesActions[vimg]) && notEmpty(pptObj.imagesActions[vimg]["docPath"]) ? pptObj.imagesActions[vimg]["docPath"] : "";
                                html+=`<li class="content_image_album mix  gallery-img no-padding li-${vimg}" data-cat="1" id="${vimg}">
                                    <div class="portfolio-item">
                                        <a class="thumb-info" href="${img}" data-lightbox="all">
                                            <img src="${img}"  class="img-responsive" alt="">
                                        </a>
                                        <button class="btn btn-xs btn-danger btn-delete-action-img" data-idimg="${vimg}" data-posimg="${kimg}" data-idaction="${action._id.$id}"><i class="fa fa-times"></i></button>
                                    </div>
                                </li>`;
                            })
                        }
                        html+=`<li class="action-add-image btn tooltips" data-id="${action._id.$id}" data-cat="1" data-toggle="tooltip" data-placement="top" data-original-title="${tradDynForm.addimage}">
                                    <div class="portfolio-item">
                                        <i class="fa fa-image fa-4x"></i>
                                    </div>
                                </li>`;
                        html += `</ul>
                        <button class="new-task btn btn-sm margin-bottom-5 btn-default pull-right" data-id="${action._id.$id}">
                            <i class="fa fa-plus"></i> ajouter une tâche
                        </button>
                        <table class="table table-striped">
                            <thead class="hidden">
                            <tr>
                                <th>checkbox</th>
                                <th>Taches</th>
                                <th>Cout</th>
                                <th>Assigné à</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody class="table-task table-task-${action._id.$id}">`;
                            if(notEmpty(action['tasks'])){
                                $.each(action['tasks'],function(kt,vt){
                                html += `<tr>
                                            <td class="text-left" style="width:15%">
                                                <div class="input_wrapper">
                                                    <input ${(notEmpty(vt.checked) && vt.checked) ? "checked":""} class="tablecheckbox check-task" value="true" data-id="${action._id.$id}" data-pos="${kt}" data-action-name="${escapeHtml(action['name'])}" data-task-name="${escapeHtml(vt.task)}" data-budgetpath="depense" data-form="aapStep1" project-slug="${(notEmpty(action.parentId) && notEmpty(action.parentId.name)) ? action.parentId.name : ''}" id="checkbox11" type="checkbox" >
                                                    <label for="checkbox11">
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="text-left" style="width:25%">${vt.task}</td>
                                            <td class="text-left" style="width:25%">${vt.credits} €</td>
                                            <td class="text-left" style="width:25%"><img src="${defaultImage}" width="30" height="30" alt="" style="border-radius:50%"/></td>
                                            <td class="text-right" style="width:15%">
                                                <button class="btn btn-default btn-xs edit-task" data-id="${action._id.$id}" data-pos="${kt}"> 
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-default btn-xs delete-task" data-id="${action._id.$id}" data-pos="${kt}"> 
                                                    <i class="fa fa-times"></i>
                                                </button>
                                                
                                            </td>
                                        </tr>`;
                                })
                            }else{
                                html += `<tr>
                                            <td class="text-center"><h6>Pas de tache</h6></td>
                                        </tr>`;
                            }
                    html +=`</tbody>
                    </table>`;
                }
                return html;
            },
            initReveal : function(pObj){
                Reveal.initialize({
                    hash: false,
                    history: false,
                    center: false,
                    disableLayout: true,
                    plugins: [ RevealMarkdown, RevealNotes ]
                });
                Reveal.on( 'slidetransitionend', event => {
                    $(".form-container").html('<h1><i class="fa fa-spinner fa-spin fa-5" aria-hidden="true"></i></h1>');
                    mylog.log("slidetransitionend",event);
                    if(Object.keys(Reveal.getHorizontalSlides()).length-1 == event.indexh){
                        if($(".navigate-right").attr("disabled")=="disabled" && $(".current.next").parent().hasClass('disabled')){
                            //
                        }else{
                            $('.reveal .controls .navigate-right').css({visibility:"visible",opacity:"1"}).removeAttr('disabled')
                            $('.reveal .controls .navigate-right').on('click',function(){
                                $('.page-link.next').trigger('click');
                            })
                        }
                    }
                    if(event.indexv == 2){
                        pObj.answerid = Reveal.getCurrentSlide().children[0].dataset.id;
                        pObj.getActions(pObj);
                        pObj.previewMenu(pObj);
                    }
                });
                Reveal.on( 'ready', event => {
                    if(Reveal.isFirstSlide()){
                        if($(".navigate-left").attr("disabled")=="disabled" && $(".current.prev").parent().hasClass('disabled')){
                            //
                        }else{
                            $('.reveal .controls .navigate-left').css({visibility:"visible",opacity:"1"}).removeAttr('disabled')
                            $('.reveal .controls .navigate-left').on('click',function(){
                                $('.page-link.prev').trigger('click');
                            })
                        }
                    }
                    $('.list-aap-container').removeClass('container').addClass('container-fluid');
                    $('#wizardcontainer').addClass("co-scroll");
                })
            },
            dynForms : {
                tasks : function(pObj,idAction,position=null,projectSlug=null) {
                    var today = new Date();
                    return {
                        "jsonSchema" : {    
                            "title" : "Ajouter un tache",
                            "description" : "Ajouter un tache pour votre action",
                            "icon" : "fa-task",
                            "properties" : {
                                task : {
                                    inputType:"text",
                                    label : "Nom tache",
                                    rules: {
                                        required : true,
                                        maxlength: 100
                                    }
                                },
                                userId : {
                                    inputType : "hidden",
                                    value: userId
                                },
                                checked : { 
                                    inputType : "hidden",
                                    value: "false"
                                },
                                credits : {
                                    label: "Credits",
                                    inputType : "text",
                                },
                                endDate :{
                                    label: "Date de fin",
                                    inputType : "date",
                                    rules: {
                                        required : true,
                                    }
                                },
                                contributors : {
                                    inputType : "finder",
                                    label : "Assigné à qui ?",
                                    multiple : true,
                                    rules : { required : true, lengthMin:[1, "contributors"]}, 
                                    initType: ["citoyens"],
                                    search : {
                                        filters: { "links.memberOf.<?=(string)$el["_id"] ?>": { "$exists": "true" }}
                                    },
                                    initBySearch : true,
                                    initMe:true,
                                    initContext:false,
                                    initContacts:false,
                                    openSearch :true
                                },
                                createdAt :{
                                    inputType: "hidden",
                                    value:today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear(),
                                }
                            },
                            beforeBuild : function(){
                                
                            },
                            afterSave : function(){
                                    
                            },
                            save : function (data) {
                                delete data.collection;
                                delete data.scope;
                                var tplCtx = {
                                    id : idAction,
                                    collection: "actions",
                                    path: "tasks",
                                    arrayForm: true,
                                    value:data,
                                    edit:false,
                                    format:true,
                                    setType : [
                                        {
                                            "path": "endDate",
                                            "type": "isoDate"
                                        },
                                        {
                                            "path": "createdAt",
                                            "type": "isoDate"
                                        }
                                    ]
                                }
                                if(position!=null){
                                    tplCtx.path = "tasks."+position;
                                    delete tplCtx.arrayForm;
                                    delete tplCtx.edit;
                                }
                                dataHelper.path2Value( tplCtx, function(params) {
                                    if(position == null){
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newtask/answerid/' + pObj.answerid,
                                        {
                                            task : data.task ,
                                            pos : params.elt.tasks.length - 1,
                                            url : window.location.href
                                        },
                                        function (data) {}, "html");
                                    }
                                    dyFObj.closeForm();
                                    pObj.actions[params.id]["tasks"] = params.elt.tasks;
                                    var details = pObj.detailActions(pObj.actions[params.id]);
                                    $('.detail-action-'+pObj.answerid).html(`${details}`);
                                    pObj.events(pObj);
                                } );
                            }
                        }
                    }
                },
                action : function(pObj){
                    var today = new Date();
                    return {
                        "jsonSchema" : {    
                            "title" : "Ajouter un action",
                            "description" : "Ajouter un tache pour votre action",
                            "icon" : "fa-task",
                            "properties" : {
                                id : dyFInputs.inputHidden(""),
                                idParentRoom : dyFInputs.inputHidden(""),
                                name : dyFInputs.name("action"),
                                description : dyFInputs.textarea(tradDynForm.longDescription, "...",null,true),
                                credits : {
                                    inputType : "text",
                                    label:"credits",
                                    order:7,
                                    rules:{
                                        number:true
                                    },
                                    value:0
                                },
                                /*startDate :{
                                    inputType : "date",
                                    label : tradDynForm.startDate,
                                    placeholder : tradDynForm.startDate
                                },
                                endDate :{
                                    inputType : "date",
                                    label : tradDynForm.endDate,
                                    placeholder : tradDynForm.endDate
                                },*/
                                /*answer : {
                                    inputType : "finder",
                                    label : "Associer à quel proposition ?",
                                    multiple : false,
                                    rules : { required : true, lengthMin:[1, "parent"]}, 
                                    initType: ["answers"],
                                    field : "answers.aapStep1.titre",
                                    search : {  
                                        filters: {
                                            'form' :aapObject.formParent._id.$id,
                                            'answers.aapStep1.titre' : {'$exists':true},
                                            'project.id' : {'$exists':true}
                                        }
                                    },
                                    initBySearch : true,
                                    initMe:false,
                                    initContext:false,
                                    initContacts:false,
                                    openSearch :true,
                                    order: 8
                                },*/
                                // parent : {
                                //     inputType : "finder",
                                //     label : "Choisir un parent (Projets) ?",
                                //     multiple : false,
                                //     rules : { required : true, lengthMin:[1, "parent"]}, 
                                //     initType: ["projects"],
                                //     search : {  
                                //         filters: { "_id": { '$in' : projectsId }}
                                //     },
                                //     initBySearch : true,
                                //     initMe:false,
                                //     initContext:false,
                                //     initContacts:false,
                                //     openSearch :true,
                                //     order: 8
                                // },
                                // contributors : {
                                //     inputType : "finder",
                                //     label : "Assigné à qui ?",
                                //     multiple : true,
                                //     rules : { required : true, lengthMin:[1, "contributors"]}, 
                                //     initType: ["citoyens"],
                                //     search : {
                                //         filters: { "links.<?php //$linksType ?>.<?php //(string)$el["_id"] ?>": { "$exists": "true" }}
                                //     },
                                //     initBySearch : true,
                                //     initMe:true,
                                //     initContext:false,
                                //     initContacts:false,
                                //     openSearch :true,
                                //     order: 9
                                // },
                                status: dyFInputs.inputHidden( "todo" ),
                                idParentResolution: dyFInputs.inputHidden( "" ),
                                // tags : dyFInputs.tags(),
                                // urls : dyFInputs.urls,
                                email : dyFInputs.inputHidden( ( (userId!=null && userConnected != null) ? userConnected.email : "" ) ),
                                idUserAuthor: dyFInputs.inputHidden(userId),
                                //type : dyFInputs.inputHidden( "action" ),

                                //prend le parentIdElement quand on est sur la page d'une resolution
                                parentId : dyFInputs.inputHidden(typeof parentIdElement != "undefined" ? parentIdElement : aapObject.elementAap.el._id.$id),
                                answerId : dyFInputs.inputHidden(pObj.answerid),
                                parentType : dyFInputs.inputHidden(typeof parentTypeElement != "undefined" ? parentTypeElement : aapObject.elementAap.el.type),
                                parentIdSurvey : dyFInputs.inputHidden(typeof form != "undefined" ? form : ""),
                                parentIdSurvey : dyFInputs.inputHidden(typeof form != "undefined" ? form._id.$id : ""),
                                parentTypeSurvey : dyFInputs.inputHidden(typeof form != "undefined" ? "forms" : ""),
                                role : dyFInputs.inputHidden(typeof role != "undefined" ? role : ""),
                                // image : dyFInputs.image()
                            },
                            beforeSave : function(){
                                if(exists(finder.object) && exists(finder.object.answer)){
                                    // $.each(finder.object.parent,function(k,v){
                                    //     $("#ajaxFormModal #parentId").val(k);
                                    //     $("#ajaxFormModal #parentType").val(v.type);    
                                    // })
                                    $("#ajaxFormModal #parentType").val("projects");
                                    $.each(finder.object.answer,function(k,v){
                                        $("#ajaxFormModal #answerId").val(k);
                                        ajaxPost('',baseUrl+"/"+moduleId+"/search/globalautocomplete", 
                                        {  
                                            searchType : ["answers"],
                                            filters:{
                                                '_id' : {'$in': [k]},
                                            },
                                            fields:["project"]
                                        },
                                        function(data){
                                            $("#ajaxFormModal #parentId").val(Object.values(data.results)[0]["project"]["id"]);
                                        },null);
                                    })
                                }
                                delete finder.object.answer;
                            },
                            afterSave : function(){
                                    
                            },
                            save : function (data) {
                                data.collection = "actions";
                                delete data.scope;
                                dataHelper.path2Value({
                                    collection: "actions",
                                    path: "allToRoot",
                                    value : data
                                },function(p){
                                    dataHelper.path2Value( {//change answerId
                                        id : data.id,path: "parentId",collection: "actions",value : $("#ajaxFormModal #parentId").val() 
                                    }, function(parms) {
                                            var tplCtx = {
                                                id : p.saved.id,
                                                collection: "actions",
                                                path: "links",
                                                value : {
                                                    "contributors" :{}
                                                }
                                            }
                                            tplCtx.value.contributors[userId] = {
                                                "type" : "citoyens",
                                                "name" : userConnected.name                        
                                            }

                                            dataHelper.path2Value( tplCtx, function(params) { //add contributors
                                                var depenseObj = {
                                                    id : $("#ajaxFormModal #answerId").val(),
                                                    collection: "answers",
                                                    path: "answers.aapStep1.depense",
                                                    arrayForm: true,
                                                    edit:false,
                                                    value : {
                                                        "date" : moment().format('DD/MM/YYYY'),
                                                        "group" : "Feature",
                                                        "nature" : "fonctionnement",
                                                        "poste" : $("#ajaxFormModal #name").val(),
                                                        "price" : $("#ajaxFormModal #credits").val(),
                                                        "actionid" : params.id,
                                                        "financer" : [],
                                                        "votes" : {}
                                                    }
                                                }
                                                dataHelper.path2Value(depenseObj, function(prms) {
                                                    pObj.getActions(pObj);
                                                    pObj.previewMenu(pObj);
                                                    setTimeout(() => {
                                                        $("[data-action-id='"+params.id+"']").trigger('click');
                                                    }, 1000);
                                                    $(".modal").modal("hide");
                                                    pObj.actions[params.id] = params.elt;
                                                    mylog.log(pObj.actions,"pObj.actions")
                                                    var details = pObj.detailActions(pObj.actions[params.id]);
                                                    $('.detail-action-'+pObj.answerid).html(`${details}`);
                                                    pObj.events(pObj);
                                                })
                                            });
                                        
                                    })
                                })
                            }
                        }
                    }
                },
                actionImg : function(pObj,idAction){
                    return {
                        "jsonSchema" : {    
                            "title" : "Ajouter une image",
                            "description" : "Ajouter une image pour votre action",
                            "icon" : "fa-task",
                            "properties" : {
                                image : {
                                    "inputType" : "uploader",
                                    "label" : trad.images,
                                    "docType": "image",
                                    "itemLimit" : 10,
                                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                                    "showUploadBtn": false,
                                    //initList : $(this).data("img")
                                },
                            },
                            beforeBuild : function(){
                                uploadObj.set("actions",idAction);
                            },
                            save : function () {
                                dyFObj.commonAfterSave(null, function() {
                                    params = {       
                                        searchType : ["documents"],
                                        fields : ["id"],
                                        filters:{
                                            'id' : idAction,
                                        },
                                        notSourceKey : true,          
                                    };
                                    ajaxPost(
                                        null,
                                        baseUrl+"/" + moduleId + "/search/globalautocomplete",
                                        params,
                                        function(data){
                                            dataHelper.path2Value({
                                                id : idAction,collection:"actions", path : "media.images",
                                                value : Object.keys(data.results)
                                            },function(params){
                                                pObj.getActions(pObj);
                                                pObj.previewMenu(pObj);
                                                setTimeout(() => {
                                                    $("[data-action-id='"+idAction+"']").trigger('click');
                                                }, 1000);
                                                $(".modal").modal("hide");
                                                pObj.actions[idAction] = params.elt;
                                                var details = pObj.detailActions(pObj.actions[idAction]);
                                                $('.detail-action-'+pObj.answerid).html(`${details}`);
                                                pObj.events(pObj);
                                            })
                                        }
                                    )
                                });
                            }
                        }
                    }
                }
            },
            toogleScreen : function(pObj){
                if(pObj.screen == "less"){
                    $('.reveal-full-screen').show();
                    $('.reveal-less-screen').hide();
                    $('.reveal-contatiner').removeClass("reveal-contatiner-full");
                }else if(pObj.screen == "full"){
                    $('.reveal-full-screen').hide();
                    $('.reveal-less-screen').show();
                    $('.reveal-contatiner').addClass("reveal-contatiner-full")
                }
                $('.reveal-toogle-screen').off().on('click',function(){
                    if($(this).data("value") == "full"){
                        pObj.screen = "full";
                        localStorage.setItem("revealScreen",pObj.screen)
                        $('.reveal-full-screen').hide();
                        $('.reveal-less-screen').show();
                        $('.reveal-contatiner').addClass("reveal-contatiner-full")
                    } 
                    else if($(this).data("value") == "less"){
                        pObj.screen = "less";
                        localStorage.setItem("revealScreen",pObj.screen)
                        $('.reveal-full-screen').show();
                        $('.reveal-less-screen').hide();
                        $('.reveal-contatiner').removeClass("reveal-contatiner-full");
                    } 
                })
            },
            events : function(pObj){
                $('.new-task').off().on('click',function(){
                    var idAction = $(this).data('id');
                    var projectSlug = $(this).data('project-slug');
                    //dyFObj.openForm(pObj.dynForms.tasks(idAction,null,projectSlug))
                    dyFObj.openForm(pObj.dynForms.tasks(pObj,idAction))
                })
                $('.edit-task').off().on('click',function(){
                    var idAction = $(this).data('id');
                    var position = $(this).data('pos');
                    var task = pObj.actions[idAction]["tasks"][position];
                    task.endDate = new Date(task.endDate.sec*1000).toLocaleDateString("fr-Fr");
                    var projectSlug = $(this).data('project-slug');
                    dyFObj.openForm(pObj.dynForms.tasks(pObj,idAction,position),null,task)
                })
                $('.delete-task').off().on('click',function() {
                    var pos = $(this).data("pos");
                    tplCtx = {};
                    tplCtx.collection = "actions";
                    tplCtx.id = $(this).data("id");
                    tplCtx.path = "tasks."+pos;
                    tplCtx.pull = "tasks";
                    tplCtx.value = null;
                    prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    dataHelper.path2Value( tplCtx, function(params){
                                        pObj.actions[params.id]["tasks"] = params.elt.tasks;
                                        var details = pObj.detailActions(pObj.actions[params.id]);
                                        $('.detail-action-'+pObj.answerid).html(`${details}`);
                                        pObj.events(pObj);
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/deletetask/answerid/' + pObj.answerid,
                                        {
                                            task : taskName,
                                            url : window.location.href
                                        },
                                        function (data) {}, "html");
                                    } );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });

                    prioModal.modal("show");
                });
                $('.check-task').change(function() { 
                    var pos = $(this).data("pos");
                    var checkKey = $(this).data("key");
                    var taskName = $(this).data("task-name");
                    var actionName = $(this).data("action-name");
                    var projectSlug = $(this).data("project-slug");
                    tplCtx = {
                        collection : "actions",
                        id : $(this).data("id"),
                        path : "tasks."+pos+".checked",
                        value : false,
                        format: true, 
                    }

                    if (this.checked) {
                        tplCtx.value = true;
                        dataHelper.path2Value( tplCtx, function() {
                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/checktask/answerid/' + pObj.answerid,
                            {
                                actname :actionName,
                                task : taskName,
                                url : window.location.href
                            },
                            function (data) {

                            }, "html");
                            tplCtx.path = "tasks."+pos+".checkedUserId";
                            tplCtx.value = userId;
                            dataHelper.path2Value( tplCtx, function(params) {
                                var today = new Date();
                                tplCtx.path = "tasks."+pos+".checkedAt";
                                tplCtx.value = today.toISOString();
                                if (typeof rcObj != "undefined" && (checkValue==true || checkValue== "true" )) {
                                    rcObj.postMsg({
                                        "channel": "#" + projectSlug,
                                        "text": userConnected.name +
                                            "a terminé la sous-tâche " + taskName + "sur l'action " + params.elt.name
                                    }).then(function (data) {
                                    });
                                }
                                dataHelper.path2Value( tplCtx, function(params) {
                                    mylog.log(params.elt,"params.elt")
                                    pObj.actions[params.id] = params.elt;
                                } );
                            } );
                        } );
                    } else {
                        tplCtx.value = false;
                        tplCtx.path = "tasks."+pos+".checked";
                        dataHelper.path2Value( tplCtx, function(params) {
                            pObj.actions[params.id] = params.elt;
                        })
                    }
                });
                $('.tooltips').tooltip();
                $('.action-add-image').off().on('click',function(){
                    var idAction = $(this).data('id');
                    dyFObj.openForm(pObj.dynForms.actionImg(pObj,idAction));
                })
                $(".btn-delete-action-img").off().on('click',function(e){
                    var idimg = $(this).data('idimg');
                    var posimg = $(this).data('posimg');
                    var idaction = $(this).data('idaction');
                    var prioModal = bootbox.dialog({
                        title: trad.confirmdelete,
                        show: false,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                        buttons: [
                            {
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    ajaxPost(
                                        null,
                                        baseUrl+"/"+moduleId+"/document/delete/id/"+idimg,
                                        {},
                                        function(data){ 
                                            if(data.result){
                                                tplCtx = {
                                                    collection : "actions",
                                                    id : idaction,
                                                    path : "media.images."+posimg,
                                                    pull : "media.images",
                                                    value : null,
                                                };
                                                dataHelper.path2Value( tplCtx, function(params){
                                                    $('.li-'+idimg).remove();
                                                } );
                                            }
                                        }
                                    );
                                }
                            },
                            {
                                label: "Annuler",
                                className: "btn btn-default pull-left",
                                callback: function() {}
                            }
                        ]
                    });
                    prioModal.modal("show");
                })
            }
        };
        pptObj.init(pptObj);
    })
</script>