<h1 class="hidden text-center">LISTES</h1>

<style>
    .switch-aap .lib-panel {
        margin-bottom: 20Px;
    }
    .switch-aap .lib-item{
        padding: 0 26px 0 26px;
    }
    .switch-aap .lib-panel img {
        width: 100%;
        background-color: transparent;
    }

    .switch-aap .lib-panel .row,
    .switch-aap .lib-panel .col-md-6 {
        padding: 0;
        background-color: #FFFFFF;
    }


    .switch-aap .lib-panel .lib-row {
        padding: 0 20px 0 20px;
    }

    .switch-aap .lib-panel .lib-row.lib-header {
        background-color: #FFFFFF;
        font-size: 20px;
        padding: 10px 20px 0 20px;
    }

    .switch-aap .lib-panel .lib-row.lib-header .lib-header-seperator {
        height: 2px;
        width: 26px;
        background-color: #d9d9d9;
        margin: 7px 0 7px 0;
    }

    .switch-aap .lib-panel .lib-row.lib-desc {
        position: relative;
        height: 100%;
        display: block;
        font-size: 13px;
    }
    .switch-aap .lib-panel .lib-row.lib-desc a{
        position: absolute;
        width: 100%;
        bottom: 10px;
        left: 20px;
    }

    .switch-aap .row-margin-bottom {
        margin-bottom: 20px;
    }

    .switch-aap .box-shadow {
        -webkit-box-shadow: 0 0 10px 0 rgba(0,0,0,.10);
        box-shadow: 0 0 10px 0 rgba(0,0,0,.10);
    }
    .switch-aap .lib-img-show{

    }
    .sticky-search-add{
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        position:sticky;
        top:1px;
        z-index: 111;
        background-color : rgb(0 0 0 / 66%);
        padding: 15px;
        border-radius: 5px;
    }
</style>
<?php 
//var_dump($parentForms,$elements);
//var_dump(Yii::app()->createUrl("/costum")."/co/index/slug/#");
//var_dump((string)$config["_id"]);
?>
<div class="row margin-bottom-25 sticky-search-add">
    
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <input type="text" class="form-control" id="search-exists-aap" placeholder="<?= Yii::t("common","Search") ?>">
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?php if(isset($elementConfig["oceco"]["subOrganization"]) && $elementConfig["oceco"]["subOrganization"] && Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )){ ?>
                <button class="btn btn-success pull-right btn-block" id="add-new-cac">
                    Ajouter une sous-organisation
                </button>
            <?php }  ?>
        </div>
</div>
<div class="row switch-aap">
    <?php foreach ($parentForms as $kf=> $vf) { 
        $parentId = array_keys($vf["parent"])[0];
        $status = (!empty($config["hiddenAap"]) && is_array($config["hiddenAap"]) && in_array($kf,$config["hiddenAap"])) ? "toshow" : "tohide";
        $statusLabel = (!empty($config["hiddenAap"]) && is_array($config["hiddenAap"]) && in_array($kf,$config["hiddenAap"])) ?  '<i class="fa fa-check"></i>'.ucfirst(Yii::t("form","Show")) : '<i class="fa fa-times"></i>'.ucfirst(Yii::t("common","hide"));
        $statusClass= (!empty($config["hiddenAap"]) && is_array($config["hiddenAap"]) && in_array($kf,$config["hiddenAap"])) ? "btn-info" : "btn-danger";
    ?>
        <?php if(
            (!empty($config["hiddenAap"]) && is_array($config["hiddenAap"]) && !in_array($kf,$config["hiddenAap"])) 
            || empty($config["hiddenAap"])
            //|| Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )
        ){ ?>
            <div class="col-md-6 col-sm-6 col-xs-10 lib-item lib-item-<?= $kf ?>" data-category="view">
                <div class="lib-panel">
                    <div class="row box-shadow">
                        <div class="col-md-6">
                            <img class="lib-img-show radius-5" src="<?= !empty($elements[$parentId]["profilImageUrl"]) ? $elements[$parentId]["profilImageUrl"] : Yii::app()->getModule( "co2" )->assetsUrl.'/images/thumbnail-default.jpg' ?>">
                        </div>
                        <div class="col-md-6">
                            <div class="lib-row lib-header">
                                <a href="<?= $costumUrl ?>/#oceco.slug.<?= !empty($elements[$parentId]["slug"]) ? $elements[$parentId]["slug"] : "" ?>.formid.<?= $kf ?>.aappage.list?view=detailed&inproject=false" class="cacs-item" target="_blank">
                                    <?= !empty($elements[$parentId]["name"]) ? $elements[$parentId]["name"] : ""?><br>
                                    <small style='color:grey;font-size:12px !important'><?= $vf["name"] ?></small>
                                </a>
                                <div class="lib-header-seperator"></div>
                            </div>
                            <div class="lib-row lib-desc">
                                <!-- description -->
                                <?php if(Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )){ ?>
                                    <button class="<?= $statusClass ?> btn-xs hide-exists-aap hidden" data-action="<?= $status ?>" data-form-id="<?= $kf ?>"><?= $statusLabel ?></button>
                                    
                                    <button class="btn-danger btn-xs delete-unused-aap" data-form-id="<?= $kf ?>">
                                        <i class="fa fa-times"></i> Cacher de la liste
                                    </button>
                                <?php } ?>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
    
</div>
<script>
    $(function(){
        'use strict';
        <?php if(Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )){ ?>
        var hiddenAapList = <?= !empty($config["hiddenAap"]) ? json_encode($config["hiddenAap"]) : json_encode([]) ?>;
        $("#add-new-cac").off().on('click',function(){
            if(costum != null){
                var newCacDialog = bootbox.dialog({ 
                        title: 'Créer sous-organisation',
                        message: `<div>
                            <form action="" id="form-new-cac">
                                <div class="form-group">
                                    <label for="">${trad.Name}</label>
                                    <input type="text" required class="form-control" maxlength="20" id="cac-name" />
                                </div>
                                <div class="form-group">
                                    <label for="">${tradDynForm.shortDescription}</label>
                                    <input type="text" class="form-control" maxlength="50" required id="cac-shortDescription"/>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success">${trad.save}</button>
                                </div>
                            </form>
                            </div>`,
                        size: 'small',
                        onEscape: true,
                        backdrop: true,
                    })
                    newCacDialog.init(function(){
                        if(costum != null){
                            $("#form-new-cac").on('submit',function(e){
                                e.preventDefault();
                                ajaxPost(null, baseUrl + "/costum/aap/sousorga/action/add/slug/"+costum.slug,
                                    {
                                        name : $("#cac-name").val(),
                                        shortDescription : $("#cac-shortDescription").val()
                                    },
                                    function (data) {
                                        if(data.result){
                                            toastr.success(data.msg);
                                            newCacDialog.modal('hide');
                                            ajaxPost('#exists-aap-lists', baseUrl + "/costum/aap/listaap/slug/"+costum.slug,
                                                null,
                                                function (data) {
                                                    $("#exists-aap-lists-dialog").animate({ scrollTop: 9999 }, 1000);
                                                    setTimeout(() => {
                                                        $("#exists-aap-lists-dialog").css({
                                                            "overflow-x" : "hidden",
                                                            "overflow-y" : "auto",
                                                        })
                                                    }, 600);
                                                }, null
                                            );
                                        }
                                    }, null
                                );
                            })
                        }
                    });
            }
        })
        <?php } ?>

        $("#search-exists-aap").on("keyup",function(){
            var val = $(this).val();
            $('.lib-header a').each(function(){
                mylog.log(new RegExp(val,"gi"));
                if($(this).text().match(new RegExp(val,"gi"))){
                    $(this).parent().parent().parent().parent().parent().show(500);
                }else{
                    $(this).parent().parent().parent().parent().parent().hide(500);
                }
            })
        })
        <?php if(Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )){ ?>
        /*$(".hide-exists-aap").off().on('click',function(){
            var btn = $(this);
            if(btn.data("action") == "tohide"){
                var tplCtx = {
                    id :  "<?= (string)$config["_id"] ?>",
                    collection: "forms",
                    path: "hiddenAap",
                    arrayForm: true,
                    value: btn.data("form-id"),
                    edit:false,
                    format:true,
                }
                dataHelper.path2Value( tplCtx, function(params) {
                    if(params.result){
                        toastr.success(trad.done);
                        btn.data("action","toshow");
                        btn.removeClass('btn-danger').addClass('btn-info').html(`<i class="fa fa-check"></i> ${tradDynForm.show}`)
                    }
                })
            }else if(btn.data("action") == "toshow"){
                dataHelper.path2Value({
                    id: "<?= (string)$config["_id"] ?>",
                    collection: `forms`,
                    path: `hiddenAap.${hiddenAapList.indexOf(btn.data("form-id"))}`,
                    pull: 'hiddenAap',
                    value: null
                }, function(params){
                    if(params.result){
                        toastr.success(trad.done);
                        btn.data("action","tohide");
                        btn.removeClass('btn-info').addClass('btn-danger').html(`<i class="fa fa-times"></i> ${trad.hide}`)
                    }
                });
            }
        })*/
        $('.delete-unused-aap').off().on('click',function(k,v){
            var btn = $(this);
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                {
                    searchType : ["answers"],
                    notSourceKey: true,
                    indexStep : 0,
                    filters : {
                        "form" : btn.data('form-id')
                    },
                },
                function(data){
                    bootbox.confirm(`
                    <h6 class="text-center text-danger">${trad.areyousure}</h6>
                    <p>Ce formulaire  ${Object.keys(data.results).length == 0 ? "n'a aucune réponse" :"a "+Object.keys(data.results).length+" réponses"}</p>
                    `, function(result){ 
                            if(result){
                                var tplCtx = {
                                    id :  "<?= (string)$config["_id"] ?>",
                                    collection: "forms",
                                    path: "hiddenAap",
                                    arrayForm: true,
                                    value: btn.data("form-id"),
                                    format:true,
                                }
                                dataHelper.path2Value( tplCtx, function(params) {
                                    if(params.result){
                                        toastr.success(trad.done);
                                        $('.lib-item-'+btn.data("form-id")).remove();
                                    }
                                })
                            }
                        });
                },
                function (data){
                            
                },null,{async:false}
            );
        })
        <?php } ?>
    })
</script>