<?php
		$name = isset($answer["answers"]["aapStep1"]["titre"]) ? $answer["answers"]["aapStep1"]["titre"] : '';
		$html = '<div class="col-btn-group-vertical dropdown aap-options-btn">
			<a href="javascript:;" class="dropdown-toggle" data-icon="fa-bars" data-toggle="dropdown">
				<i class="fa fa-bars fa-2x"></i>
				<span class="tooltips-menu-btn">Action</span>
			</a>
			<div class="btn-group-vertical btn-group-custom dropdown-menu">
					<span class="tooltips btn-group-custom-container hidden" data-toggle="tooltip" data-placement="bottom" data-original-title="'.($ansVar["haveProject"]["is"] ? Yii::t("common", "See the project in detail") : Yii::t("common", "See the proposal in detail")).'" style="display: block;">
						<button type="button" class="btn btn-sm btn-primary aapgoto tooltips no-margin btn-custom" data-url="'. Yii::app()->createUrl("/costum") .'/co/index/slug/'.(CacheHelper::getCostum() != false ? CacheHelper::getCostum()["slug"] : @$data["el"]["slug"]) .'#oceco.slug.'. @$data["el"]["slug"] .'.formid.'. (string) $data["elform"]["_id"] .'.aappage.form.answer.'. (string)$answer["_id"].'" data-elformid="'. $answer['form'] .'" style=""> <i class="fa fa-pencil-square"></i></button> 
						<span class="text-btn-custom aapgoto" style="cursor: pointer" data-url="'. Yii::app()->createUrl("/costum") .'/co/index/slug/'.(CacheHelper::getCostum() != false ? CacheHelper::getCostum()["slug"] : $data["el"]["slug"]) .'#oceco.slug.'. $data["el"]["slug"] .'.formid.'. (string) $data["elform"]["_id"] .'.aappage.form.answer.'. (string)$answer["_id"].'" data-elformid="'. $answer['form'] .'">'.($ansVar["haveProject"]["is"] ? Yii::t("common", "See the project in detail") : Yii::t("common", "See the proposal in detail")).'</span>
					</span>
					
					<span class="tooltips btn-group-custom-container hidden" data-toggle="tooltip" data-placement="bottom" data-original-title="'.($ansVar["haveProject"]["is"] ? Yii::t("common", "See the project in detail") : Yii::t("common", "See the proposal in detail")).'" style="display: block;">
						<button type="button" class="btn btn-sm btn-primary aapgetaapview aapmodalformm tooltips no-margin btn-custom" data-url="" data-elformid="'. $answer['form'] .'" style=""> <i class="fa fa-pencil-square"></i></button> 
						<span class="text-btn-custom aapgetaapview aapmodalformm" style="cursor: pointer" data-id="'. (string)$answer["_id"].'" data-elformid="'. $answer['form'] .'" data-url="">'.($ansVar["haveProject"]["is"] ? Yii::t("common", "See the project in detail") : Yii::t("common", "See the proposal in detail")).'</span>
					</span>

					<span class="tooltips btn-group-custom-container" data-toggle="tooltip" data-placement="bottom" data-original-title="'.($ansVar["haveProject"]["is"] ? Yii::t("common", "See the project in detail") : Yii::t("common", "See the proposal in detail")).'" style="display: block;">
						<button type="button" class="btn btn-sm btn-primary aapgetaapview2 aapmodalformm tooltips no-margin btn-custom" data-url="slug.'. @$data["el"]["slug"] .'.formid.'. (string) $data["elform"]["_id"] .'.aappage.form.answer.'. (string)$answer["_id"].'" data-elformid="'. $answer['form'] .'" style=""> <i class="fa fa-pencil-square"></i></button> 
						<span class="text-btn-custom aapgetaapview2 aapmodalformm" style="cursor: pointer" data-id="'. (string)$answer["_id"].'" data-url="slug.'. $data["el"]["slug"] .'.formid.'. (string) $data["elform"]["_id"] .'.aappage.form.answer.'. (string)$answer["_id"].'" data-elformid="'. $answer['form'] .'">'.($ansVar["haveProject"]["is"] ? Yii::t("common", "See the project in detail") : Yii::t("common", "See the proposal in detail")).'</span>
					</span>
					<span class="btn-group-custom-container">
						<button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepAction"] .'" data-inputid="suivredepense" data-toggle="tooltip" data-placement="bottom" data-original-title="Action ('. count($ansVar["actions"]) .')"> <i class="fa fa-pencil-square-o"></i></button>
						<span class="text-btn-custom aapopenmodalbtn aapgetinputsa" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepAction"] .'" data-inputid="suivredepense">Action ('. count($ansVar["actions"]) .')</span>
					</span>';
				if ($ansVar["haveProject"]["is"]){
					$html .= '<span class="btn-group-custom-container">
						<button type="button" class="btn btn-sm btn-primary  open-kanban tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-project="'.$answer["project"]["id"].'" data-toggle="tooltip" data-placement="bottom" data-original-title="Action ('. count($ansVar["actions"]) .')"> <i class="fa fa-trello"></i></button>
						<span class="text-btn-custom  open-kanban" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-project="'.$answer["project"]['id'].'">Kanban</span>
					</span>';
				}else{
					$html.= '<span class="btn-group-custom-container">
						<button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepFinancor"] .'" data-inputid="generateproject" data-toggle="tooltip" data-placement="bottom" data-original-title="Mettre en projet"> <i class="fa fa-refresh"></i></button>
						<span class="text-btn-custom aapopenmodalbtn aapgetinputsa" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepFinancor"] .'" data-inputid="generateproject">Mettre en projet</span>
					</span>';
				}
				if ($data["canAdmin"]){
					$html .= '<span class="btn-group-custom-container">
						<button type="button" class="btn btn-sm btn-primary btn-retain-reject tooltips btn-custom" data-id="'.(string) $answer["_id"] .'" data-retain="'.($ansVar["retain"]["retain"] ? "true" : "false") .'" data-toggle="tooltip" data-placement="bottom" data-original-title="'.$ansVar["retain"]["label"] .'"> <i class="fa fa-'.$ansVar["retain"]["icon"] .'"></i></button>
						<span class="text-btn-custom btn-retain-reject" style="cursor: pointer" data-id="'.(string) $answer["_id"] .'" data-retain="'.($ansVar["retain"]["retain"] ? "true" : "false") .'" data-toggle="tooltip" data-placement="bottom" data-original-title="'.$ansVar["retain"]["label"] .'">'.$ansVar["retain"]["label"] .'</span>
					</span>';
				}
				if(!isset($data["elform"]["hidden"]["btnObservatory"]) || (isset($data["elform"]["hidden"]["btnObservatory"]) && $data["elform"]["hidden"]["btnObservatory"]==false)){
					if ($ansVar["haveDepense"]){
						$html .= '<span class="btn-group-custom-container">
							<button type="button" class="btn btn-sm btn-primary aapgetobservatory tooltips btn-custom" data-id="'. (string)(string) $answer["_id"].'" data-toggle="tooltip" data-placement="bottom" data-original-title="Observatoire">
								<i class="fa fa-binoculars" aria-hidden="true"></i>
							</button>
							<span class="text-btn-custom aapgetobservatory" style="cursor: pointer" data-id="'. (string)(string) $answer["_id"].'">Observatoire</span>
						</span>';
					} else {
						$html .= '<span class="btn-group-custom-container">
							<button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa tooltips btn-custom" disabled  data-id="'. (string)(string) $answer["_id"].'" data-toggle="tooltip" data-placement="bottom" data-original-title="Observatoire">
								<i class="fa fa-binoculars" aria-hidden="true"></i>
							</button>
							<span class="text-btn-custom" style="cursor: not-allowed">Observatoire</span>
						</span>';
					}
				}
				if((!empty($answer["status"]) && in_array("vote", $answer["status"]) && $data["canEvaluate"]) || Form::canAdmin((string)$data["elform"]["_id"])){
					$html .= '<span class="btn-group-custom-container">
						<button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepEval"] .'" data-inputid="selection"  data-toggle="tooltip" data-placement="bottom" data-original-title="'.(isset($data["configform"]["subForms"]["aapStep2"]["name"]) ? $data["configform"]["subForms"]["aapStep2"]["name"] : "Evaluation").' ('. $ansVar["votantCounter"] .')">
							<i class="fa fa-gavel aria-hidden="true"></i>';
								if (!empty($answer["status"]) && in_array("vote", $answer["status"])){
									$html .= '<div class="notification-dot"></div>';
								}
						$html .= '</button>
						<span class="text-btn-custom aapopenmodalbtn aapgetinputsa" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepEval"] .'" data-inputid="selection">'.(isset($data["configform"]["subForms"]["aapStep2"]["name"]) ? $data["configform"]["subForms"]["aapStep2"]["name"] : "Evaluation").' ('. $ansVar["votantCounter"] .')</span>
					</span>';
				}
				$html .= '<span class="btn-group-custom-container">
						<button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepFinancor"] .'" data-inputid="financer"  data-toggle="tooltip" data-placement="bottom" data-original-title="'.(isset($data["configform"]["subForms"]["aapStep3"]["name"]) ? $data["configform"]["subForms"]["aapStep3"]["name"] : "Financement").' ('. count(@$answer["answers"]["aapStep1"]["depense"]) .')">
							<i class="fa fa-credit-card-alt" aria-hidden="true"></i>';
							if (!empty($answer["status"]) && in_array("finance", $answer["status"])){
								$html .= '<div class="notification-dot"></div>';
							}
					$html .= '</button>
						<span class="text-btn-custom aapopenmodalbtn aapgetinputsa" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-form="'. $answer["form"] .'" data-formstep="'.$data["stepFinancor"] .'" data-inputid="financer">'.(isset($data["configform"]["subForms"]["aapStep3"]["name"]) ? $data["configform"]["subForms"]["aapStep3"]["name"] : "Financement").' ('. count(@$answer["answers"]["aapStep1"]["depense"]) .')</span>
					</span>';
					if(!empty($data["el"]["slug"]) && $data["el"]["slug"] == 'coSindniSmarterre'){
						$html .= '<span class="btn-group-custom-container">
							<a href="javascript:;" class="btn btn-sm btn-primary tooltips margin-top-10 load-pdf-panel btn-custom" data-ans="'. (string) $answer["_id"].'">
								<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
							</a>
							<span class="text-btn-custom load-pdf-panel" style="cursor: pointer" data-ans="'. (string) $answer["_id"].'">Version pdf</span> 
						</span>';

					}
					if (isset(Yii::app()->session['userId']) && $data["canAdmin"] || ($answer["user"] == Yii::app()->session['userId']) ){
						$html .= '<span class="tooltips btn-group-custom-container" data-toggle="tooltip" data-placement="bottom" data-original-title="'.Yii::t("common", "Delete").'" style="display: block;">
							<button type="button" class="btn btn-sm btn-danger btn-delete-answer tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-proposition="'. htmlspecialchars($name , ENT_QUOTES, 'UTF-8') .'" data-project-id="'. @$answer["project"]["id"] .'" style="width:100%"><i class="fa fa-times"></i></button>
							<span class="text-btn-custom btn-delete-answer" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-proposition="'. htmlspecialchars($name , ENT_QUOTES, 'UTF-8') .'" data-project-id="'. @$answer["project"]["id"] .'">'.Yii::t("common", "Delete").'</span>
						</span>';
					}

				if(isset(Yii::app()->session['userId'])){
					$html .= '<span class="btn-group-custom-container">
						<a href="javascript:;" class="btn btn-sm btn-primary openAnswersComment tooltips btn-custom" onclick="commentObj.openPreview(\'answers\',\''. (string)(string) $answer["_id"] .'\',\''. (string)(string) $answer["_id"] .'\', \'Commentaire\')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire">
							'. PHDB::count(Comment::COLLECTION, array("contextId"=>(string)(string) $answer["_id"],"contextType"=>"answers", "path"=>(string)(string) $answer["_id"])).' <i class="fa fa-commenting"></i>
						</a>
						<span class="text-btn-custom openAnswersComment" style="cursor: pointer" onclick="commentObj.openPreview(\'answers\',\''. (string)(string) $answer["_id"] .'\',\''. (string)(string) $answer["_id"] .'\', \'Commentaire\')">Ajouter un commentaire</span>
					</span>';
				}

				if (isset(Yii::app()->session['userId']) && $data["canAdmin"] || ($answer["user"] == Yii::app()->session['userId']) ){
					$html .= '<span class="tooltips btn-group-custom-container" data-toggle="tooltip" data-placement="bottom" data-original-title="Transferer" style="display: block;">
						<button type="button" class="btn btn-sm btn-primary btn-transfer-answer tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-proposition="'. htmlspecialchars($name , ENT_QUOTES, 'UTF-8') .'" data-project-id="'. @$answer["project"]["id"] .'" style=""><i class="fa fa-share"></i></button>
						<span class="text-btn-custom btn-transfer-answer" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-proposition="'. htmlspecialchars($name , ENT_QUOTES, 'UTF-8') .'" data-project-id="'. @$answer["project"]["id"] .'">Transferer</span>
					</span>';
				}

				if (isset(Yii::app()->session['userId']) && $data["canAdmin"]) {
					$html .= '<span class="tooltips btn-group-custom-container" data-toggle="tooltip" data-placement="bottom" data-original-title="'.Yii::t("common", "Notification history").'" style="display: block;">
						<button type="button" class="btn btn-sm btn-primary btn-notification-history tooltips btn-custom" data-id="'. (string) $answer["_id"] .'" data-proposition="'. htmlspecialchars($name , ENT_QUOTES, 'UTF-8') .'" data-project-id="'. @$answer["project"]["id"] .'" style=""><i class="fa fa-envelope-open"></i></button>
						<span class="text-btn-custom btn-notification-history" style="cursor: pointer" data-id="'. (string) $answer["_id"] .'" data-proposition="'. htmlspecialchars($name , ENT_QUOTES, 'UTF-8') .'" data-project-id="'. @$answer["project"]["id"] .'">'.Yii::t("common", "Notification history").'</span>
					</span>';
				}

				$html .= '</div>
		</div>';
		echo $html;
?>
