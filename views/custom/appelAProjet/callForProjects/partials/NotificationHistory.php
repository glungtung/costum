<?php //var_dump($answer) ?>
<style>
    .portfolio-modal.modal.vertical .modal-content,
    .portfolio-modal.modal.vertical #openModalContent,
    .portfolio-modal.modal.vertical .list-aap-container.aappageform.container,
    .portfolio-modal.modal.vertical #customHeader,
    .portfolio-modal.modal.vertical .col-xs-12.text-center.bg-white{
        background-color: #00000000 !important;
    }
    .portfolio-modal.modal.vertical{
        background-color: #000000a3 !important;
    }
    .portfolio-modal.modal.vertical #wizardcontainer{
        background-color: #fff !important;
        border-radius: 5px;
    }
    .portfolio-modal.modal .aap-footer,
    .portfolio-modal.modal.vertical .col-xs-12.text-center.bg-white hr,
    .portfolio-modal.modal.vertical #modeSwitch{
        display: none;
    }
    .portfolio-modal.vertical .close-modal .lr .rl,
    .portfolio-modal.vertical .close-modal .lr {
        background-color: #fff ;
    }
</style>
<div class="container bg-white">
    <div class="row text-left">
        <div class="col-xs-12">
            <h1><?= Yii::t("common", "Notification history") ?></h1>
            <h4><?= $answer["answers"]["aapStep1"]["titre"] ?></h4>
        </div>
        <div class="col-xs-12">
            <table class="table table-sripped">
                <thead>
                    <tr>
                        <th>Objet</th>
                        <th>Date d'envoi</th>
                        <th>Vue</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(!empty($answer["notifications"]))
                    foreach ($answer["notifications"] as $key => $value) { ?>
                        <tr>
                            <td><?= !empty($value["tplObject"]) ? $value["tplObject"] :"" ?></td>
                            <td><?= !empty($value["status"]["sentDate"]) ? date("d/m/Y H:i",$value["status"]["sentDate"]) : "" ?></td>
                            <td><?= !empty($value["status"]["seenDate"]) ? date("d/m/Y H:i",$value["status"]["seenDate"]) : '<span class="text-red">'.Yii::t("common", "Not seen").'</span>' ?></td>
                        </tr>
            <?php   } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>