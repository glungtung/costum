<style type="text/css">
    @media only screen and (min-width: 992px) {
        .aappageform {

        }
    }

    .single_evaluate img {
        min-height: 0;
    }
</style>

<?php
if(!empty($answerid) && !empty($answers[$answerid])){
$answer = $answers[$answerid];

?>

    <div id="sticky-anchor"></div>
    <div id="sticky">
        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
                <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details ">Observatoire : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?></a></li>
            </ul>
        </div>

        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <div class="aapconfigdiv">
                <div class="text-center">
                    <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.form.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Editer proposition</button>
                    <button type="button"  class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string)$answer["_id"] ?>"> <i class="fa fa-edit"></i> Résumé de la proposition </button>
                </div>
            </div>
        </div>

    </div>

    <div class="aappageform" id="aapformcont"></div>

<script type="text/javascript">

    var answerid =  "<?php echo (!empty($answerid) ?$answerid : ''); ?>";

    var el_form = "<?php echo (isset($elform["_id"]) ? (string)$elform["_id"] : ''); ?>";

    $(document).ready(function() {
        ajaxPost("#aapformcont", baseUrl+'/survey/answer/ocecoformdashboard/answerid/'+answerid,
            null,
            function(data){

            },"html");
    });

</script>

    <?php
}
?>