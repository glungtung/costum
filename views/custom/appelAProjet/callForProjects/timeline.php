<style>

</style>
<?php
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/list.css"], Yii::app()->getModule('costum')->assetsUrl);
$sumCoeff = 0;
if (isset($el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"])) {
    foreach ($el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"] as $key => $value) {
        $sumCoeff = $sumCoeff + (float)$value["coeff"];
    }
}
$idEl = array_keys($elform["parent"])[0];
$typeEl = $elform["parent"][$idEl]["type"];
$allowedRoles = isset($el_configform["subForms"]["aapStep3"]["params"]["canEdit"]) ?
    $el_configform["subForms"]["aapStep3"]["params"]["canEdit"] : array();
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
$roles = isset($me["links"]["memberOf"][$idEl]["roles"]) ? $me["links"]["memberOf"][$idEl]["roles"] : null;
$isAdmin = isset($me["links"]["memberOf"][$idEl]["isAdmin"]) ? $me["links"]["memberOf"][$idEl]["isAdmin"] : false;
$canEvaluate =  false;
$intersectRoles = array_intersect($allowedRoles, $roles);
if (count($intersectRoles) != 0 || $isAdmin)
    $canEvaluate = true;
$community = Element::getCommunityByTypeAndId($typeEl, $idEl, "all", null, "Evaluateur", null, ["name", "profilMediumImageUrl"]);
$creator = Person::getById($elform["creator"]);
$community[(string)$creator["_id"]] = $creator;
if (!empty($answerid) && !empty($answers[$answerid])) {
    $answer = $answers[$answerid];
    $user = Person::getSimpleUserById(@$answer["user"]);


    //get admin and evaluator rating
    $evaluation = @$answer["answers"]["aapStep3"]["evaluation"];
    $criterions = $el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"];
    foreach ($community as $kcom => $vcom) {
        if (!isset($evaluation[$kcom])) {
            $evaluation[$kcom] = $criterions;
        }
        foreach ($criterions as $kcrit => $vcrit) {
            if (!isset($evaluation[$kcom][$kcrit]))
                $evaluation[$kcom][$kcrit] = $vcrit;
        }
        ksort($evaluation[$kcom]);
    }
    $cummulMean = 0;
    foreach ($evaluation as $key => $value) {
        $notes = 0;
        ksort($value);
        foreach ($value as $kv => $vv) {
            $notes = $notes + ((float)$vv["note"] * (float)$vv["coeff"]);
        }
        $cummulMean = $cummulMean + ($notes / $sumCoeff);
    }
    $cummulMean =  $cummulMean / count($evaluation);
?>
    <div id="sticky-anchor"></div>
    <div id="sticky">
        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li><a><?php if (isset($el["el"]["name"])) {
                            echo $el["el"]["name"];
                        } ?></a></li>
                <li><a><?php if (isset($elform["name"])) {
                            echo $elform["name"];
                        } ?></a></li>
                <li class="active"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.aappage.details ">Fiche du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?></a></li>
            </ul>
        </div>

        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <div class="aapconfigdiv">
                <div class="text-center">
                    <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.aappage.form.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Editer proposition</button>
                    <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.aappage.formdashboard.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Observatoire </button>
                    <!-- <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.aappage.timeline.answer.<?php echo (string)$answer["_id"] ?>"><i class="fa fa-plus-square-o"></i> Timeline </button> -->
                </div>
            </div>
        </div>
<?php  ?>
    </div>
    <?php 
    // $answers
    // $slug 
    // $el_slug 
    // $elform 
    // $el 
    // $el_configform 
        /* variables */
        $allItem = [
            
        ];
        /*get date proposition*/
        $dateProposition = "";

    ?>
    <div class="col-xs-12 col-lg-10 col-lg-offset-1 padding-0" >
        <section class="aap-timeline padding-5">
            <ul>
                <li>
                <div>
                    <time>1934</time> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                </div>
                </li>
                <li>
                <div>
                    <time>1937</time> Proin quam velit, efficitur vel neque vitae, rhoncus commodo mi. Suspendisse finibus mauris et bibendum molestie. Aenean ex augue, varius et pulvinar in, pretium non nisi.
                </div>
                </li>
                <li>
                <div>
                    <time>1940</time> Proin iaculis, nibh eget efficitur varius, libero tellus porta dolor, at pulvinar tortor ex eget ligula. Integer eu dapibus arcu, sit amet sollicitudin eros.
                </div>
                </li>
            </ul>
        </section>
    </div>

<?php
}
?>
<script>
    (function() {
        'use strict';
        var items = document.querySelectorAll(".aap-timeline li");
        function isElementInViewport(el) {
            var rect = el.getBoundingClientRect();
            return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }

        function callbackFunc() {
            for (var i = 0; i < items.length; i++) {
            if (isElementInViewport(items[i])) {
                items[i].classList.add("in-view");
            }
            }
        }

        // listen for events
        window.addEventListener("load", callbackFunc);
        window.addEventListener("resize", callbackFunc);
        window.addEventListener("scroll", callbackFunc);
        $(function() {
            var target = $('.aap-timeline');
            //target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top -50
            }, 1000);
            return false;
            }
        });
    })();
</script>