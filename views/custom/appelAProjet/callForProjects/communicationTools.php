<style>
    .xdebug-var-dump{
        text-align: left;
    }
    body .bootbox.modal {
        z-index: 100000;
        background: rgb(0 0 0 / 85%);
    }
</style>
<?php 
    HtmlHelper::registerCssAndScriptsFiles(["/js/appelAProjet/row-merge-bundle.min.js"], Yii::app()->getModule('costum')->assetsUrl); 
    $cssAnsScriptFilesTheme = array(
        /*'/plugins/DataTables/media/css/DT_bootstrap.css',
        '/plugins/DataTables/media/js/jquery.dataTables.min.1.10.4.js',
        '/plugins/DataTables/media/js/DT_bootstrap.js',*/
        "/plugins/simplemde-markdown-editor/simplemde.min.css",
        '/plugins/simplemde-markdown-editor/simplemde.min.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
    $initImage = Document::getListDocumentsWhere(
        array(
            "id"=> $params,
            "type"=>'form',
            "subKey"=>'imageSignatureEmail',
        ), "image"
    );
?>
<style>
    .table-all-email tr td{
        vertical-align: middle !important;
        font-weight: bold;
        border : 2px solid #ddd !important
    }
    .communication-tools-container .preview-item{
        position: relative;
        width: 100%;
        height: 100%;
        background-color: #dcdcdca8;
        margin-bottom: 5px;
        padding: 30px;
    }
    .communication-tools-container .preview{
        position: relative;
        width: 100%;
        max-height: 800px;
        overflow-y: auto;
    }

    .communication-tools-container .command-list{
        position: relative;
        width: 100%;
        max-height: 200px;
        overflow-y: auto;
        background-color: #dcdcdca8;
        padding: 15px;
        margin-bottom: 5px;
    }
    .communication-tools-container .mail-content{
        position: relative;
        background-color: #dcdcdca8;
        padding: 15px;
    }
    .communication-tools-container .editor-toolbar{
        z-index: 2 !important;
    }
    .communication-tools-container .popover,
    .communication-tools-container .popover-title,
    .communication-tools-container .popover-content{
        max-width: 100%;
        background: #f9f6f6 !important;
        color: #110101;
    }
    .communication-tools-container .popover{
        max-height: 400px;
        overflow-y: auto;
    }
    .communication-tools-container .popover .popover-content .btn{
        width: auto !important;
    }
    .communication-tools-container .preview-item .count{ 
        position: absolute;
        top: 4px;
        left: 7px;
        font-weight: bold;
    }
    .communication-tools-container .filters-activate{
        display: inline-block !important;
        padding: 5px 5px;
        background: #7da53d;
        margin: 0px 2px;
        border-radius: 5px;
        color: #1a1717;
        font-weight: bold;
    }
    .communication-tools-container .filters-activate .fa-times-circle{
        display: none;
    }
</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-left communication-tools-container" >
  <div class="col-md-12 col-sm-12 col-xs-12" id="navigationAdmin">
    <div class="col-sm-12 col-xs-12 text-center">
      <h3>Outils de communication</h3>
    </div>
    <div class="row">
        <div class="col-xs-12 active-filters">
            <h3>1 - DOSSIERS SÉLECTIONNÉS (<span class="dossier-counter" class="text-green-k"></span> dossier(s))</h3>
            <div class="dossier-filters">Pas de filtre</div>
        </div>
        <div class="col-xs-12">
            <h3>2 - SÉLECTIONNEZ LES CHAMPS DU FORMULAIRE QUI CONTIENNENT L'EMAIL</h3>
            <div class="aapStep1-fields">Aucun champs email</div>
        </div>
        <div class="col-xs-12">
            <h3>3 - EMAILS POSSIBLES</h3>
            <div>
                <div class="table-responsive co-scroll" style="max-height: 500px;overflow-y:auto;" >
                    <table class="table table-bordered  table-all-email">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Dossiers</th>
                                <th>Déposé par</th>
                                <th>Type email</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody class="all-email-values">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 container-command-list mail-content-container" style="display:none">
            <h3>4 - Email</h3>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="model-chooser"> Choisir un modèle</label>
                    <select name="" id="model-chooser" class="form-control">
                        <option value="custom-email">Par défaut</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <form method="post" action="" enctype="multipart/form-data" id="myform" class="add-signature-container" style="display:none">
                        <label class="text-center" style="display:block;">Signature en base à droite</label>
                        <div class='preview-signature' style="display:none">
                            <img src="" id="img" width="100" height="100" style="object-fit:contain;width:100%">
                        </div>
                        <div style="display: flex;margin-top:5px">
                            <input type="file" id="file" name="file" class="form-control" accept=".jpg,.png" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <br>
                    <button type="button" class="btn bg-green-k hover-white btn-default send-email-template margin-top-5" style="display:none">Envoyer email à <span class="dossier-counter"></span> dossier(s)</button>
                </div>
            </div>
            <!-- <div class="col-xs-12 template-container"></div> -->
            <div class="col-xs-12 template-container-2"></div>          
            <div class="custom-email col-xs-12">
                <div class="col-xs-12 col-sm-6 ">
                    <h5>Saisir le contenu de l'email</h5>
                    <div class="mail-content">
                        <div class="">
                            <div class="form-group">
                                <label>Objet</label>
                                <input type="text" name="mail-object" id="mail-object" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Contenus</label>
                                <textarea name="mail-content" id="mail-content" cols="30" rows="10"></textarea>
                            </div>
                            <button class="btn btn-default btn-lg pull-right bg-green-k hover-white send-email">Envoyer</button>
                            <button class="btn btn-default btn-lg pull-right bg-green-k hover-white preview-email">Aperçu</button>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 container-apercu">
                    <h5>Aperçu (<span class="dossier-counter" class="text-green-k"></span> dossier(s))</h5>
                    <div class="preview co-scroll"></div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<script>
    //$(function(){
        if( exists(aapObject.formParent.params) &&
            exists(aapObject.formParent.params.communicationToolsRoles) &&
            Array.isArray(aapObject.formParent.params.communicationToolsRoles) &&
            exists(aapObject.elementAap.el.links) &&
            exists(aapObject.elementAap.el.links.members) &&
            exists(aapObject.elementAap.el.links.members[userId]) &&
            exists(aapObject.elementAap.el.links.members[userId].roles) &&
            Array.isArray(aapObject.elementAap.el.links.members[userId].roles))
        {
            var filteredArray = aapObject.formParent.params.communicationToolsRoles.filter(function(n) {
                return aapObject.elementAap.el.links.members[userId].roles.indexOf(n) !== -1;
            });
            mylog.log(filteredArray,"filteredArray");
            if(filteredArray.length != 0){
                var simplemde = new SimpleMDE({
                    element: $("#mail-content")[0],
                    forceSync: true,
                    spellChecker:false,
                    toolbar: [
                        {	
                            name: "bold",
                            action: SimpleMDE.toggleBold,
                            className: "fa fa-bold",
                            title: trad.Bold,
                        },
                        {	
                            name: "italic",
                            action: SimpleMDE.toggleItalic,
                            className: "fa fa-italic",
                            title: trad.Italic,
                        },
                        {	
                            name: "strikethrough",
                            action: SimpleMDE.toggleStrikethrough,
                            className: "fa fa-strikethrough",
                            title: trad.Strikethrough,
                        },"|",
                        {	
                            name: "heading",
                            action: SimpleMDE.toggleHeadingSmaller,
                            className: "fa fa-header",
                            title: trad.Heading,
                        },
                        "|",
                        {	
                            name: "quote",
                            action: SimpleMDE.toggleBlockquote,
                            className: "fa fa-quote-left",
                            title: "Quote",
                        },"|",
                        {	
                            name: "unordered-list",
                            action: SimpleMDE.toggleUnorderedList,
                            className: "fa fa-list-ul",
                            title: trad.GenericList,
                        },
                        {	
                            name: "ordered-list",
                            action: SimpleMDE.toggleOrderedList,
                            className: "fa fa-list-ol",
                            title: trad.NumberedList,
                        },"|",
                        {	
                            name: "link",
                            action: SimpleMDE.drawLink,
                            className: "fa fa-link",
                            title: trad['Insert Hyperlink'],
                        },
                        {	
                            name: "image",
                            action: SimpleMDE.drawImage,
                            className: "fa fa-picture-o",
                            title: trad['Insert Image Hyperlink'],
                        },
                        {	
                            name: "horizontal-rule",
                            action: SimpleMDE.drawHorizontalRule,
                            className: "fa fa-minus",
                            title: trad["Insert Horizontal Line"],
                        },"|",
                        {	
                            name: "preview",
                            action: SimpleMDE.togglePreview,
                            className: "fa fa-eye",
                            title: trad.Preview,
                        },
                        {
                            name: "custom",
                            action: function customFunction(editor){
                                //bootbox.alert("fffffff");
                            },
                            className: "fa fa-superscript command-popover",
                            title: "Variable dynamique",
                        }
                    ],
                });
                simplemde.codemirror.on("blur", function(){
                    $($("#mail-content")[0]).blur();
                });

                var communicationToolsObj = {
                    init :function(obj){
                        obj.templateDropdown();
                        obj.getAapStep1Inputs(obj);
                        obj.getData(obj);
                        obj.getDepositors(obj);
                        obj.events(obj);
                        obj.commandes(obj);
                        obj.signature(obj);
                        $('.dossier-counter').text(obj.proposition.count.answers)
                    },
                    emailInputs : {},
                    emailValues : {},
                    proposition : {},
                    depositors : {},
                    payload : {},
                    choosedTemplate : "custom-email",
                    events : function(obj){
                        $("input[name='aapStepOneInputs']").off().on('click',function(){
                            obj.emailInputs = [];
                            $('.all-email-value').html("");
                            $.each($("input[name='aapStepOneInputs']:checked"), function(){            
                                obj.emailInputs[$(this).val()] = $(this).data('label');// get selected email inputs
                            });
                            obj.getAllEmail(obj);
                            if(Object.keys(obj.emailInputs).length != 0){
                                $('.mail-content-container').show(900)
                            }else{
                                $('.mail-content-container').hide(900)
                            }
                            //obj.rowMerge(obj);
                            obj.events(obj);
                            /*if(typeof directoryTable != "undefined")
                                directoryTable.dataTable().fnDestroy();*/
                            //directoryTable = $('.table-all-email').dataTable({});
                        })

                        $("input[name='emailValue']").off().on('click',function(){
                            obj.emailValues = {};
                            $.each($("input[name='emailValue']:checked"), function(){
                                if(typeof obj.emailValues[$(this).data("ansid")] == "undefined"){
                                    obj.emailValues[$(this).data("ansid")]={};
                                }            
                                obj.emailValues[$(this).data("ansid")][$(this).val()] = $(this).data('email');// get selected email inputs
                            });
                            if(Object.keys(obj.emailValues).length != 0){
                                $('.mail-content-container').show(900)
                            }else{
                                $('.mail-content-container').hide(900)
                            }
                            $('.communication-tools-container .dossier-counter').text(Object.keys(obj.emailValues).length)
                        })

                        $('.send-email').off().on('click',function(){
                            bootbox.confirm(`<h5 class='text-center' >${trad.confirm}</h5>`, function(result){ 
                                if(result)
                                    obj.sendMailOrPreview(obj)
                            });
                        })
                        $('.preview-email').off().on('click',function(){
                            obj.sendMailOrPreview(obj,true)
                        })
                        var htmlFilters = "";
                        if(aapObject.sections.proposition.activeFilters.text != ""){
                            htmlFilters = `<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" data-type="filters" data-key="vote" data-value="En evaluation" data-field="status" data-event="filters" data-filterk="status" data-original-title="" title=""><i class="fa fa-times-circle"></i><span class="activeFilters" data-type="filters" data-key="vote" data-value="En evaluation" data-field="status" data-event="filters" data-filterk="status">Contient : ${aapObject.sections.proposition.activeFilters.text}</span></div>`;
                        }
                        htmlFilters += $('#activeFilters').html();
                        $('.dossier-filters').html(htmlFilters);

                        $('.portfolio-modal.modal').addClass('co-scroll');
                        setTimeout(() => {
                            $('.command-popover').popover({
                                placement : "bottom",
                                trigger :"click",
                                html:true,
                                container:'.communication-tools-container',
                                content: "<div class='command-list-popover'></div>"  
                            }).on('shown.bs.popover', function(){
                                obj.commandes(obj)
                                $('.communication-tools-container .popover').addClass('co-scroll')
                            });
                            
                        }, 800);

                        $(".communication-tools-container #model-chooser").off().on('change',function(){
                            if($(this).val() == "custom-email"){
                                $(".communication-tools-container .custom-email").show("800");
                                $('.communication-tools-container .template-container-2,.communication-tools-container .send-email-template,.communication-tools-container .add-signature-container').hide("800");
                            }else{
                                if($(this).find(':selected').data('event') != "undefined"){
                                    eval($(this).find(':selected').data('event'));
                                }
                                $(".communication-tools-container .custom-email").hide("800");
                                $('.communication-tools-container .template-container-2,.communication-tools-container .send-email-template,.communication-tools-container .add-signature-container').show("800");
                                obj.choosedTemplate = $(this).val();
                                obj.sendMailOrPreview(obj,true,obj.choosedTemplate);
                            }
                            $('.communication-tools-container .send-email-template').prop('disabled', false)
                        })

                        //$(".template-container").html(window[aapObject.elementAap.el.slug+"Obj"].emailTemplate['template-1']);
                        $('.communication-tools-container .send-email-template').off().on('click',function(){
                            var btn = $(this)
                            bootbox.confirm(`<h5 class='text-center' >${trad.confirm}</h5>`, function(result){ 
                                if(result){
                                    obj.sendMailOrPreview(obj,false,$(".communication-tools-container #model-chooser").val());
                                    btn.prop('disabled', true);
                                }
                            });
                        })
                    },
                    getData : function(obj){
                        var params = aapObject.sections.proposition.activeFilters;
                        params.searchType = ["answers"];
                        params.name = params.text;
                        params.notIndexedQuery = true;
                        params.fields = ["collection", "user", "answers","created","form","links","parentId","answerId","tasks","status","project"];
                        //params.fields = ["answers.aapStep1"]
                        params.indexStep = "30";
                        ajaxPost(
                        null,
                        baseUrl+"/costum/aap/directory/source/"+costum.slug+"/form/"+aapObject.formParent._id.$id,
                        params,
                        function(data){
                                mylog.log("govalos",data.results);
                                obj.proposition = data;
                            },null,null,
                            {async : false}
                        )
                    },
                    getDepositors : function(obj){
                        mylog.log(userTemp,"userTemp")
                        var userTemp = [];
                        $.each(obj.proposition.results,function(k,v){
                            userTemp.push(v.user)
                        })
                        mylog.log(userTemp,"userTemp")
                        ajaxPost('',baseUrl+"/co2/element/get/type/citoyens", 
                            {id : userTemp,fields:["name","profilMediumImageUrl"]},
                            function(data){
                                obj.depositors=data.map;
                            },null,null,
                            {async : false})
                    },
                    getAapStep1Inputs : function(obj){
                        $.each(aapObject.formInputs,function(k,v){
                            if(exists(v.inputs) && exists(v.step) && v.step == "aapStep1"){
                                v.html = "";
                                $.each(v.inputs,function(kk,vv){
                                    if(vv.label.indexOf("mail") != -1){
                                        obj.emailInputs[kk] = vv.label; //init email fields
                                        v.html += `
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="aapStepOneInputs"  value="${kk}" data-label="${vv.label}">${vv.label}</label>
                                            </div>
                                        </div>
                                    `}
                                })
                                $('.aapStep1-fields').html(v.html);
                            }
                        })
                    },
                    getAllEmail : function(obj){
                        var emailFields = obj.emailInputs;
                        if(exists(obj.proposition.results)){
                            obj.proposition.results
                            var html = "";
                            var j = 1;
                            $.each(obj.proposition.results,function(k,v){
                                obj.emailValues[k] = {};
                               
                                $.each(v.answers.aapStep1,function(kk,vv){
                                    if(Object.keys(emailFields).includes(kk) && vv != ""){
                                        obj.emailValues[k][kk] = vv;
                                        /*html += `
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="emailValue" checked data-ansid="${k}" value="${kk}" data-email="${vv}">${obj.emailInputs[kk]} : ${vv}</label>
                                                </div>
                                            </div>`;*/
                                        html += `<tr>
                                                    <td>${j}</td>
                                                    <td>${v.answers.aapStep1.titre}</td>
                                                    <td>${obj.depositors[v.user].name}</td>
                                                    <td>${obj.emailInputs[kk]}</td>
                                                    <td><input type="checkbox" name="emailValue" checked data-ansid="${k}" value="${kk}" data-email="${vv}"> ${vv}</td>
                                                </tr>`
                                        j++;
                                    }
                                })
                            })
                            $('.all-email-values').html(html);
                            obj.events(obj);
                        }
                    },
                    extractEmails (obj,text ){
                        return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
                    },
                    sendMailOrPreview : function(obj,preview=false,template=null){
                        if(template == null && ($("#mail-object").val() == "" || $("#mail-content").val() == "")){
                            return bootbox.alert("<h5 class='text-center text-red' >Echec : Objet ou contenu de l'e-mail vide</h5>");
                        }
                        var url = baseUrl+"/costum/aap/sendmail";
                        if(preview){
                            url = baseUrl+"/costum/aap/sendmail/preview/true";
                        }
                        var emails = [];
                        $.each(obj.emailValues,function(k,v){
                            $.each(v,function(kk,vv){
                                if(!emails.includes(vv)){
                                    emails.push(vv);
                                }
                            })
                        })

                        var params={ 
                            formId : aapObject.formParent._id.$id,
                            emailValues : obj.emailValues,
                            useTemplate : template != null ? true : false,
                            tpl : "basic",
                            tplObject : $("#mail-object").val(),
                            template: template != null ? template : 
                                dataHelper.markdownToHtml($("#mail-content").val(),{
                                    parseImgDimensions:true,
                                    simplifiedAutoLink:true,
                                    strikethrough:true,
                                    tables:true,
                                    openLinksInNewWindow:true
                                }),
                            payload : obj.payload
                        };
                        ajaxPost(
                            null,
                            url,
                            params,
                            function(data){
                                if(preview){
                                    if(notNull(template)){
                                        $('.template-container-2').html(data);
                                    }else{
                                        $('.container-apercu .preview').html(data);
                                    }
                                }else{
                                    toastr.success("Le mail a été envoyé avec succès");
                                    obj.payload = {};
                                }
                            }
                        ); 
                    },
                    rowMerge : function(obj){
                        var table = $('.table-all-email').rowMerge({
                            excludedColumns: [3],
                        });
                        table.merge();
                    },
                    commandes : function(obj){
                        var commands = "";
                        
                        $.each(aapObject.formInputs,function(k,v){

                            if(exists(v.step) && exists(v.inputs) && v.step == "aapStep1"){
                                $.each(v.inputs,function(kk,vv){
                                    if(exists(vv.type) && vv.type == "tpls.forms.ocecoform.budget"){
                                        
                                        commands += `<h6 class="text-green-k">${vv.label}</h6>
                                        <p class="small"><code>{${kk}.total}</code> : total <button data-value="{${kk}.total}" class="btn btn-default btn-xs pull-right choose-command"><i class="fa fa-plus"></i></button></p>
                                        <p class="small"><code>{${kk}.funded}</code> : financé <button data-value="{${kk}.funded}" class="btn btn-default btn-xs pull-right choose-command"><i class="fa fa-plus"></i></button></p>
                                        <p class="small"><code>{${kk}.spent}</code> : dépensé <button data-value="{${kk}.spent}" class="btn btn-default btn-xs pull-right choose-command"><i class="fa fa-plus"></i></button></p>
                                        <p class="small"><code>{${kk}.subvention}</code> : subvention <button data-value="{${kk}.subvention}" class="btn btn-default btn-xs pull-right choose-command"><i class="fa fa-plus"></i></button></p>
                                        <p class="small"><code>{${kk}.subvention-funded}</code> : subvention financé <button data-value="{${kk}.subvention-funded}" class="btn btn-default btn-xs pull-right choose-command"><i class="fa fa-plus"></i></button></p>
                                        `;
                                    }
                                })
                            }
                        })
                        commands+= `<h6 class="text-green-k">Details</h6>
                        <p class="small"><code>{prop.title}</code> Nom du dossier <button data-value="{prop.title}" class="btn btn-default btn-xs pull-right choose-command"><i class="fa fa-plus"></i></button></p></p>
                        `;
                        //$('.command-list').html(commands);
                        $('.command-list-popover').html(commands);
                        $(".choose-command").off().on('click',function(){
                            var pos = simplemde.codemirror.getCursor();
                            simplemde.codemirror.setSelection(pos, pos);
                            simplemde.codemirror.replaceSelection('<h1>'+$(this).data('value')+'</h1>');
                        })
                    },
                    signature : function(obj){
                        var addSignature = function(){
                            var fd = new FormData();
                            var files = $('#file')[0].files;
                            if(files.length > 0 ){
                                fd.append('qquuid','fsdfsdf-yuiyiu-khfkjsdf-dfsd');
                                fd.append('qqfilename',files[0]['name']);
                                fd.append('qqtotalfilesize',files[0]['size']);
                                fd.append('qqfile',files[0]);
                                fd.append('costumSlug', aapObject.elementAap.el.slug);
                                $.ajax({
                                    url: baseUrl+'/co2/document/upload-save/dir/communecter/folder/form/ownerId/'+aapObject.formParent._id.$id+'/input/qqfile/docType/image/contentKey/slider/subKey/imageSignatureEmail',
                                    type: 'post',
                                    data: fd,
                                    contentType: false,
                                    processData: false,
                                    success: function(response){
                                        if(response.result){
                                            toastr.success(response.msg);
                                            $("#img").attr("src",response.docPath); 
                                            $(".preview-signature").show(800); // Display image element
                                            obj.sendMailOrPreview(obj,true,$(".communication-tools-container #model-chooser").val());
                                        }else{
                                            toastr.error(response.msg);
                                        }
                                    },
                                });
                            }else{
                                toastr.error("Ajouter une image");
                            }
                        };
                        $('#file').on('change',function(){
                            const [file] = $('#file')[0].files;
                            if (file) {
                                $(".preview-signature").show(800);
                                $(".preview-signature img").attr('src',URL.createObjectURL(file));
                                addSignature();
                            }
                        })
                    },
                    templateDropdown : function(obj){
                        if(exists(window[aapObject.elementAap.el.slug+"Obj"]) && exists(window[aapObject.elementAap.el.slug+"Obj"].communicationTools) && exists(window[aapObject.elementAap.el.slug+"Obj"].communicationTools.templates)){
                            var html = "";
                            $.each(window[aapObject.elementAap.el.slug+"Obj"].communicationTools.templates,function(k,v){
                                html +=`<option value="${k}" data-event="${v.event}">${v.label}</option>`;
                            });
                            $('#model-chooser').append(html);
                        }
                    }
                }
                if(aapObject.sections.proposition.activeFilters.types[0] == "answers"){
                    communicationToolsObj.init(communicationToolsObj);
                }
            }else{
                $(".communication-tools-container").html("<h3 class='text-center text-red '>Vous n'êtes pas autorisé</h3>");
            }
        }
    //})
</script>