<?php 
    $blockKey = $blockCms["_id"];
    $paramsData["buttonLabel"] = "GENERER UN APPEL A PROJET";

    if (isset($blockCms)) {
        $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
        foreach ($paramsData as $e => $v) {
            if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
                $paramsData[$e] = $tplsCms[$e];
            }
        }
    }
?>
<style>
    .block-container-<?= $kunik ?> .generate-conf-section{
        display: flex;
        flex-direction: column;
        align-items: center; justify-content: center;
        position: absolute;
        top: 0;left: 0;right: 0;bottom: 0;
        background-color: grey;
        border: 3px solid;
    }
    .block-container-<?= $kunik ?> .generate-conf-section button{
      background-color: #93C020;
	    border-color: #93C020;
    }
</style>

<div class="generate-conf-section">
   <button class="btn btn-success btn-lg btn-generate-conf">
      <?= $paramsData["buttonLabel"] ?>
    </button>
</div>
<script type="text/javascript">
    sectionDyf.<?= $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?= $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {
                "buttonLabel" : {
                    "inputType" : "text",
                    "label" : "Label du button",
                },      
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?= $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };
        sectionDyf.<?= $kunik ?>Params.jsonSchema.properties = Object.assign(sectionDyf.<?= $kunik ?>Params.jsonSchema.properties,properties<?= $kunik ?>)
        $(".edit<?= $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?= $kunik ?>Params,null, sectionDyf.<?= $kunik ?>ParamsData);
        });
    });

    var aapObj = {
        props : {
            aapConfig : {
                id : "aapConfig1",
                name : "Configuration Appel à projet",
                active : true,
                private: false,
                subForms : {
                    step1 : {
                        propositionInputs : "propositionsInputs1",
                        propositionParams : {}
                    },
                    step2 : {
                        evaluationInputs : "evaluationInputs1",
                        evaluationType : "",
                        evaluationParams : {}
                    },
                    step3 : {
                        financementInputs : "financementInputs1",
                        financementParams : {}
                    },
                    step4 : {
                        suivieInputs : "suivieInputs1",
                        suivieParams : {}
                    }
                }
            },
            formParent : {
                id :"aapForms1",
                name: "Formulaire Appel à projet",
                type : "aap",
                config : "aapConfig1"
            },
            stepInputs : {
                proposition : {
                    id : "propositionsInputs1",
                    isSpecific :true,
                    inputs : {
                      
                    }
                }
            },
        },
        init : function(){
          aapObj.bindEvents();
        },
        bindEvents : function(){
            $('.block-container-<?= $kunik ?> .btn-generate-conf').off().on('click',function(){
              aapObj.generateConfig();
            })
        },
        generateConfig : function(){
            ajaxPost(null,
              baseUrl+'/costum/aap/generateconfig',
              aapObj.props, 
              function(data){
                  if(data.result)
                      toastr.success(data.msg)
            })
        }
    }
    aapObj.init();
</script>