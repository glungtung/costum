    <style type="text/css">
        .aap-card {
            display: block;
            margin-bottom: 20px;
            line-height: 1.42857143;
            background-color: #fff;
            border-radius: 2px;
        /*
            box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
        */
            transition: box-shadow .25s;
            /*box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);*/
            box-shadow: 0px 0px 5px 1px rgb(200 200 200 / 80%);
        }
        .aap-card:hover {   
        box-shadow: 0 8px 17px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
        }
        .aap-card .card-title {
            border-bottom: 1px solid #D4D4D4;
        }

        .aap-card .card-title h4 {
            text-decoration: none !important;
            padding:10px;
            text-transform: uppercase;
            color: #a5d952;
        }

        .aap-card .card-title h4 button {
            display: none;
        }

        .aap-card:hover .card-title h4 button.aapeditbtn {
            display: block;
        }

        button.aapeditbtn {
            border: solid 2px #3f4e58;
            border-radius: 10px;
            font-weight: bold;
            background-color: #fff;
            color: #3f4e58;
            padding: 3px 8px;
        }

        .aap-card .card-body h5  {
            text-decoration: none !important;
            padding:10px;
            text-transform: uppercase;
            color: #65716d;
            font-size: 17px;
        }

        .aap-card .card-body span,.aap-card .list-group-item  {
            text-decoration: none !important;
            padding:10px;
            color: #65716d;
            font-weight: bold;
            font-size: 17px;
        }
        .aap-card .elformdescription.markdown{
            font-size: initial;
            font-weight: initial;
            padding: 0 11px;
        }
        .aap-card .markdown {
            white-space: initial;
        }
        .aap-card .card-body span{
            display: inline-block;
        }

        .card-body span.aap-list:before{
            content: '\f0a9';
            margin-right: 15px;
            font-family: FontAwesome;
            color: #d9534f;
        }

        .card-body span.aap-startdate {
            background-color: #dcd6d6;
            margin: 10px;
            color: green;
        }

        .card-body span.aap-enddate {
            background-color: #dcd6d6;
            margin: 10px;
            color: #d7524e;
        }

        .aapcanceledit {
            color: #b94a48;
            display: none;
        }

        .aapsaveedit {
            display: none;
        }

        .aapbtnsupr {
            border: solid 2px #d9534f;
            color: #d9534f;
            border-radius: 10px;
            padding: 0 5px;
        }

        .aapbtndeplace {
            border: solid 0px #d9534f;
            color: #4fb9d9;
            border-radius: 10px;
            padding: 0 5px;
        }


    </style>

<?php if (!empty($elform)){
        $isElementAdmin = false;
        if(isset($el["el"]["_id"]) && isset($el["el"]["collection"]) )
            $isElementAdmin = Authorisation::isInterfaceAdmin((string)$el["el"]["_id"],$el["el"]["collection"]);
    ?>
    <div class="container list-aap-container">
        <div class="container fixed-breadcrumb-filters no-padding">
            <ul class="breadcrumb" style="display: inline-block;">
                <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
                <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details ">details de l'appel à projet</a></li>
            </ul>
            <ul style="list-style: none;float:right;margin-top:5px">
                <li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Configurer formulaire">
                    <a href="javascript:;" class="aapgoto margin-right-5 tooltips" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.form.editform.true">
                        <i class="fa fa-2x fa-edit" aria-hidden="true"></i>
                    </a></li>
                <li class="btn-breadcrumb" data-toggle="tooltip" data-placement="bottom" data-original-title="Copier le lien du formulaire">
                    <a href="javascript:;" class="copyFormLink margin-right-5 tooltips" data-clipboard-text="http:/" data-clipboard-action="copy"><i class="fa fa-2x fa-copy" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="aap-card" data-dest="elformname">
                <div class="card-title">
                    <h4><i class="fa fa-circle"></i> Nom <?php if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])){ ?> <button class="btn pull-right aapcanceledit" data-dest="elformname"> <i class="fa fa-times"></i> </button> <button class="btn pull-right aapsaveedit" data-dest="elformname"> <i class="fa fa-check"></i> </button> <button class="btn pull-right aapeditbtn" data-dest="elformname"> <i class="fa fa-edit"></i> </button> <?php } ?></h4>

                </div>
                <div class="card-body">
                    <h5 class="elformname" data-path="name"> <?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </h5>
                </div>
            </div>

            <div class="aap-card" data-dest="elformdesc">
                <div class="card-title">
                    <h4 class=""><i class="fa fa-info-circle"></i> Description <?php if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])){ ?> <button class="btn pull-right aapcanceledit" data-dest="elformdescription"> <i class="fa fa-times"></i> </button> <button class="btn pull-right aapsaveedit" data-dest="elformdescription"> <i class="fa fa-check"></i> </button>  <button class="btn pull-right aapeditbtn editFormDetail" data-entry="description" > <i class="fa fa-edit"></i> </button> <?php } ?> </h4>
                </div>
                <div class="card-body">
                    <div class="elformdescription markdown padding-left-30" data-path="descr" > <?= isset($elform["description"]) ? $elform["description"] : $elform["description"] ?> </div>
                </div>
            </div>

            <div class="aap-card" data-dest="elformcible">
                <div class="card-title">
                    <h4><i class="fa fa-user-circle"></i> Qui peut répondre ? <?php if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])){ ?> <button class="btn pull-right aapcanceledit" data-dest="elformcible" data-type="multioption"> <i class="fa fa-times"></i> </button> <button class="btn pull-right aapsaveedit" data-dest="elformcible" data-type="multioption"> <i class="fa fa-check"></i> </button> <button class="btn pull-right aapaddrow" data-dest="elformcible"><i class="fa fa-plus"></i> </button> <button class="btn pull-right aapeditbtn" data-dest="elformcible" data-type="multioption"> <i class="fa fa-edit"></i> </button> <?php } ?> </h4>
                </div>
                <div class="card-body">
                    <span class="elformcible elformcibletext"> <?php if (isset($elform["cibletext"])) { echo $elform["cibletext"]; } ?> </span>
                    <div class="row elformciblelist" data-dest="elformcible">
                    </div>
                </div>
            </div>

            <div class="aap-card">
                <div class="card-title">
                    <h4><i class="fa fa-calendar"></i> les dates <?php if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])){ ?> <button class="btn pull-right aapeditbtn" data-dest="elformdate"> <i class="fa fa-edit"></i> </button> <?php } ?> </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12"><span> date debut : </span><span class="aap-startdate elformdate"><i class="fa fa-flag-checkered"></i> <?php if (isset($elform["startDate"])) { echo $elform["startDate"]; } ?> </span></div>
                        <div class="col-md-6 col-sm-12"><span> date fin : </span><span class="aap-enddate elformdate"><i class="fa fa-clock-o"></i> <?php if (isset($elform["endDate"])) { echo $elform["endDate"]; } ?> </span></div>
                    </div>
                </div>
            </div>

            <!-- <div class="aap-card">
                <div class="card-title">
                    <h4><i class="fa fa-calendar"></i> Objectifs <?php if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])){ ?> <button class="btn pull-right aapeditbtn"> <i class="fa fa-edit"></i> </button> <?php } ?> </h4>
                </div>
                <div class="card-body">
                    <span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad asperiores, aut nam obcaecati porro reprehenderit velit. Asperiores deleniti ducimus harum inventore libero molestias necessitatibus nihil quo sapiente voluptatum! Consequatur, tempora! </span>
                </div>
            </div> -->
            <?php if(isset($el_configform["subType"]) && $el_configform["subType"] != "ocecoformConfig"){  ?>
            <div class="aap-card characterization hidden">
                <div class="card-title">
                    <h4><i class="fa fa-calendar"></i> caractérisation <?php if (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])){ ?> <button class="btn pull-right aapeditbtn editFormDetail" data-entry="characterization"> <i class="fa fa-edit"></i> </button> <?php } ?> </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h6 class="padding-left-35">Objectif(s)</h6>
                            <ul class="list-group">
                                <?php if(isset($elform["objectives"])){ 
                                    foreach ($elform["objectives"] as $kobjtv => $vobjtv) {
                                        echo '<li class="list-group-item">'.($kobjtv+1).'- '.$vobjtv.'</li>';
                                    } 
                                 }else{
                                    echo '<li class="list-group-item">Objectif vide</li>';
                                } ?>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h6 class="padding-left-35">Domaine d'action(s)</h6>
                            <ul class="list-group">
                                <?php if(isset($elform["actionDomains"])){ 
                                    foreach ($elform["actionDomains"] as $kactDom => $vactDom) {
                                        echo '<li class="list-group-item">'.($kactDom+1).'- '.$vactDom.'</li>';
                                    } 
                                 }else{
                                    echo '<li class="list-group-item">Domaine d\'action vide</li>';
                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

<?php } ?>

    <script type="text/javascript">
        var detailForm = <?= json_encode($elform) ?>;
        var optlist = <?php echo (!empty($elform["ciblelist"]) ? json_encode($elform["ciblelist"]) : "[]") ?>;
        function initoptlist(optlist){
            if ($('.elformciblelist') && optlist.lenght != 0) {
                $('.elformciblelist').html('');
                $.each(optlist, function( index, value )
                {
                    $('.elformciblelist').prepend('<div class="col-md-6 col-sm-12 aap-opt" data-dest="elformcible" ><span class="aap-list elformcible">'+value+'</span></div>');
                });
                $('.elformciblelist').prepend('<div class="col-md-6 col-sm-12 aap-opt hide" data-dest="elformcible" ><span class="aap-list elformcible"></span></div>');

            }
        }

        $(document).ready(function() {
            initoptlist(optlist);
            $.each($(".markdown"), function(k,v){
                descHtml = dataHelper.markdownToHtml($(v).html());
                $(v).html(descHtml);
            });
            $('.tooltips').tooltip();
            $('.aapeditbtn').off().on("click", function () {
                var btnedit = $(this);

                $('.aapcanceledit[data-dest="'+btnedit.data("dest")+'"]').show();
                $('.aapsaveedit[data-dest="'+btnedit.data("dest")+'"]').show();
                if ($('.aapaddrow[data-dest="'+btnedit.data("dest")+'"]')){
                    $('.aapaddrow[data-dest="'+btnedit.data("dest")+'"]').show();
                }
                btnedit.hide();
                $('.aap-card[data-dest="'+btnedit.data("dest")+'"]').hover(function(){
                    btnedit.css("display" , "none");
                });

                $('.'+btnedit.data("dest")).attr('contenteditable','true');
                //dataHelper.activateMarkdown('.'+btnedit.data("dest"));

                var p = document.getElementsByClassName(btnedit.data("dest")),
                    s = window.getSelection(),
                    r = document.createRange();

                if ($('.'+btnedit.data("dest")).text() == ""){
                    p.innerHTML = '\u00a0';
                    r.selectNodeContents(p[0]);
                    s.removeAllRanges();
                    s.addRange(r);
                    document.execCommand('delete', false, null);
                }else{
                    r.setStart(p[0], 0);
                    r.setEnd(p[0], 0);
                    s.removeAllRanges();
                    s.addRange(r);
                }

                if (btnedit.data('type')){
                    if(btnedit.data('type') == "multioption"){
                        $('.aap-opt[data-dest="'+btnedit.data("dest")+'"]').each(function( index ) {
                            $(this).append("<button class='btn btn-default aapbtndeplace table-up'><i class='fa fa-arrow-up'></i></button><button class='btn btn-default aapbtndeplace table-down'><i class='fa fa-arrow-down'></i></button> <button class='btn btn-default aapbtnsupr table-remove'><i class='fa fa-times'></i></button>");
                        });
                        $('.table-up').click(function () {
                            var elemrow = $(this).parent();
                            elemrow.prev().before(elemrow.get(0));
                        });
                        $('.table-down').click(function () {
                            var elemrow = $(this).parent();
                            elemrow.next().after(elemrow.get(0));
                        });
                        $('.table-remove').click(function () {
                            $(this).parent().detach();
                        });
                    }
                }else{
                    $('.'+btnedit.data("dest")).attr('data-initvalue', $('.'+btnedit.data("dest")).text());
                }

            });

            $('.aapcanceledit').off().on("click", function () {
                var btncancel = $(this);
                btncancel.hide();
                $('.aapeditbtn[data-dest="'+btncancel.data("dest")+'"]').hide();
                $('.aapsaveedit[data-dest="'+btncancel.data("dest")+'"]').hide();
                if ($('.aapaddrow[data-dest="'+btncancel.data("dest")+'"]')){
                    $('.aapaddrow[data-dest="'+btncancel.data("dest")+'"]').hide();
                }

                $('.aap-card[data-dest="'+btncancel.data("dest")+'"]').mouseenter(function(){
                    $('.aapeditbtn[data-dest="'+btncancel.data("dest")+'"]').css("display" , "block");
                });

                $('.'+btncancel.data("dest")).attr('contenteditable','false');
                $('.'+btncancel.data("dest")).text($('.'+btncancel.data("dest")).data('initvalue'));

                if (btncancel.data('type')){
                    if(btncancel.data('type') == "multioption"){
                        $('.table-up').remove();
                        $('.table-down').remove();
                        $('.table-remove').remove();

                        initoptlist(optlist);
                    }
                }
            });

            $('.aapsaveedit').off().on("click", function () {
                var btnsave = $(this);
                btnsave.hide();
                $('.aapcanceledit[data-dest="'+btnsave.data("dest")+'"]').hide();
                $('.aapeditbtn[data-dest="'+btnsave.data("dest")+'"]').hide();
                if ($('.aapaddrow[data-dest="'+btnsave.data("dest")+'"]')){
                    $('.aapaddrow[data-dest="'+btnsave.data("dest")+'"]').hide();
                }

                $('.aap-card[data-dest="'+btnsave.data("dest")+'"]').mouseenter(function(){
                    $('.aapeditbtn[data-dest="'+btnsave.data("dest")+'"]').css("display" , "block");
                });

                if (btnsave.data('type')){
                    if(btnsave.data('type') == "multioption"){
                        $('.table-up').remove();
                        $('.table-down').remove();
                        $('.table-remove').remove();
                        var optlistsub = [];

                        $('.elformciblelist[data-dest="'+btnsave.data("dest")+'"] .aap-opt:not(.hide)').each(function(index){
                            optlistsub.push($(this).children().text());
                        });

                        $('.'+btnsave.data("dest")).attr('contenteditable','false');

                        var tplCtx = {};

                        tplCtx.id = "<?php echo @(string)$elform['_id']; ?>";
                        tplCtx.path = "cibletext";
                        tplCtx.collection = "<?php echo Form::COLLECTION ?>";

                        tplCtx.value = $('.elformcibletext').text();

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined"){

                        }
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                            } );
                        }

                        var tplCtx = {};

                        tplCtx.id = "<?php echo @(string)$elform['_id']; ?>";
                        tplCtx.path = "ciblelist";
                        tplCtx.collection = "<?php echo Form::COLLECTION ?>";

                        tplCtx.value = optlistsub;

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                toastr.success('saved');
                            } );
                        }

                        optlist = optlistsub;


                        initoptlist(optlist);
                    }
                }else {
                    $('.'+btnsave.data("dest")).attr('contenteditable','false');

                    var tplCtx = {};

                    tplCtx.id = "<?php echo @(string)$elform['_id']; ?>";
                    tplCtx.path = $('.'+btnsave.data("dest")).data("path");
                    tplCtx.collection = "<?php echo Form::COLLECTION ?>";

                    tplCtx.value = $('.'+btnsave.data("dest")).text();

                    mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success('saved');
                        } );
                    }
                }


            });

            $('.aapaddrow').off().on("click", function () {
                var addrowbtn = $(this);

                var aapoptclone = $('.aap-opt.hide').clone(true).removeClass('hide');
                $('.elformciblelist[data-dest="'+addrowbtn.data("dest")+'"]').append(aapoptclone);

            });
           $(".editFormDetail").off().on('click',function(){
                var  copyFormObj =formObj.init();
                copyFormObj.dynForm.addDescription(copyFormObj,detailForm,true);
                if($(this).data("entry")=="characterization")
                    $(".nameundefined,.descriptiontextarea,.fieldsetupload,.fieldsetactivation,.fieldsettest,.fieldsetdate,.temporarymembercanreplycheckboxSimple,.withconfirmationcheckboxSimple,.startDateNoconfirmationdate,.endDateNoconfirmationdate,.hasStepValidationsselect,.titleselect").hide();
                if($(this).data("entry")=="description")
                    $(".nameundefined,.actionDomainsarray,.objectivesarray,.fieldsetupload,.fieldsetactivation,.fieldsettest,.fieldsetdate,.temporarymembercanreplycheckboxSimple,.withconfirmationcheckboxSimple,.startDateNoconfirmationdate,.endDateNoconfirmationdate,.hasStepValidationsselect,.titleselect").hide();
           })
            lazyLoad( baseUrl+'/plugins/clipboard/clipboard.min.js','',
				function(){
                    $('.copyFormLink').off().on().click(function(){
                        $(this).attr("data-clipboard-text",location.href.split("#")[0]+"/#answer.index.id.new.form.<?= @(string)$elform['_id'] ?>");
                        var clipboard = new ClipboardJS('.copyFormLink');
                        clipboard.on('success', function(e) {
                            /*mylog.info('Action:', e.action);
                            mylog.info('Text:', e.text);
                            mylog.info('Trigger:', e.trigger);*/
                            e.clearSelection();
                            toastr.success("Copié");
                        });
                        
                        clipboard.on('error', function(e) {
                            /*mylog.error('Action:', e.action);
                            mylog.error('Trigger:', e.trigger);*/
                        });
                    })
				});

        });
    </script>
