<?php
HtmlHelper::registerCssAndScriptsFiles(array(
    '/plugins/jsontotable/JSON-to-Table.min.1.0.0.js',
    '/plugins/jqueryTablesorter/jquery.tablesorter.min.js',
    '/plugins/jqueryTablesorter/jquery.tablesorter.widgets.js',
    '/plugins/jqueryTablesorter/widget-grouping.js'
), Yii::app()->request->baseUrl);

HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/aap/aapGlobalDashboard.css'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$realpropositiondata = [];
foreach ($propobardata as $idl => $proposition){
    $realpropositiondata[] = array("label" => $propobarlabel[$idl] , "data" => $proposition , "stack" => "Stack 0" , "backgroundColor" => $mybarcolor[$idl] );
}

?>

<style type="text/css">
    /* extra css needed because there are 5 child rows */
    /* no zebra striping */
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
        /* with zebra striping */
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
        background: #d9d9d9;
    }
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
        background: #bfbfbf;
    }

    #dashboardTable {
        overflow: scroll;
    }

    /* Grouping widget css;
     * contained in widget.grouping.css (added v2.28.4)
     */
    tr.group-header td {
        background: #eee;
    }
    .group-name {
        text-transform: uppercase;
        font-weight: bold;
    }
    .group-count {
        color: #999;
    }
    .group-hidden {
        display: none !important;
    }
    .group-header, .group-header td {
        user-select: none;
        -moz-user-select: none;
    }
    /* collapsed arrow */
    tr.group-header td i {
        display: inline-block;
        width: 0;
        height: 0;
        border-top: 4px solid transparent;
        border-bottom: 4px solid #888;
        border-right: 4px solid #888;
        border-left: 4px solid transparent;
        margin-right: 7px;
        user-select: none;
        -moz-user-select: none;
    }
    tr.group-header.collapsed td i {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 5px solid #888;
        border-right: 0;
        margin-right: 10px;
    }

    .tablesorter .filtered {
        display: none;
    }

    /* ajax error row */
    .tablesorter .tablesorter-errorRow td {
        text-align: center;
        cursor: pointer;
        background-color: #e6bf99;
    }

    input.tablesorter-filter.form-control.disabled{
        display: none;
    }

    tr.group-header td {
        background: #eee;
    }
    .group-name {
        text-transform: uppercase;
        font-weight: bold;
    }
    .group-count {
        color: #999;
    }
    .group-hidden {
        display: none;
    }
    .group-header, .group-header td {
        user-select: none;
        -moz-user-select: none;
    }
    /* collapsed arrow */
    tr.group-header td i {
        display: inline-block;
        width: 0;
        height: 0;
        border-top: 4px solid transparent;
        border-bottom: 4px solid #888;
        border-right: 4px solid #888;
        border-left: 4px solid transparent;
        margin-right: 7px;
        user-select: none;
        -moz-user-select: none;
    }
    tr.group-header.collapsed td i {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 5px solid #888;
        border-right: 0;
        margin-right: 10px;
    }

    .smalltextgrey{
        color: grey;
    }

    /* REQUIRED CSS: change your reflow breakpoint here (35em below) */
    @media ( max-width: 50em ) {

        /* uncomment out the line below if you don't want the sortable headers to show */
        /* table.ui-table-reflow thead { display: none; } */

        /* css for reflow & reflow2 widgets */
        .ui-table-reflow td,
        .ui-table-reflow th {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            float: right;
            /* if not using the stickyHeaders widget (not the css3 version)
             * the "!important" flag, and "height: auto" can be removed */
            width: 100% !important;
            height: auto !important;
        }

        /* reflow widget only */
        .ui-table-reflow tbody td[data-title]:before {
            color: #469;
            font-size: .9em;
            content: attr(data-title);
            float: left;
            width: 50%;
            white-space: pre-wrap;
            text-align: bottom;
            display: inline-block;
        }

        /* reflow2 widget only */
        table.ui-table-reflow .ui-table-cell-label.ui-table-cell-label-top {
            display: block;
            padding: .4em 0;
            margin: .4em 0;
            text-transform: uppercase;
            font-size: .9em;
            font-weight: 400;
        }
        table.ui-table-reflow .ui-table-cell-label {
            padding: .4em;
            min-width: 30%;
            display: inline-block;
            margin: -.4em 1em -.4em -.4em;
        }

    } /* end media query */

    /* reflow2 widget */
    .ui-table-reflow .ui-table-cell-label {
        display: none;
    }

    /* additional selectors needed to increase css specificity */
    table.ui-table-reflow .ui-table-cell-label {
        padding: .4em;
        min-width: 30%;
        display: inline-block;
        margin: -.4em 1em -.4em -.4em;
    }

    /* additional selectors needed to increase css specificity */
    table.ui-table-reflow .ui-table-cell-label.ui-table-cell-label-top {
        display: block;
        padding: .4em 0;
        margin: .4em 0;
        text-transform: uppercase;
        font-size: .9em;
        font-weight: 400;
    }

    /* extra css needed because there are 5 child rows */
    /* no zebra striping */
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
        /* with zebra striping */
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
        background: #d9d9d9;
    }
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
        background: #bfbfbf;
    }

    /* Grouping widget css;
     * contained in widget.grouping.css (added v2.28.4)
     */
    tr.group-header td {
        background: #eee;
    }
    .group-name {
        text-transform: uppercase;
        font-weight: bold;
    }
    .group-count {
        color: #999;
    }
    .group-hidden {
        display: none !important;
    }
    .group-header, .group-header td {
        user-select: none;
        -moz-user-select: none;
    }
    /* collapsed arrow */
    tr.group-header td i {
        display: inline-block;
        width: 0;
        height: 0;
        border-top: 4px solid transparent;
        border-bottom: 4px solid #888;
        border-right: 4px solid #888;
        border-left: 4px solid transparent;
        margin-right: 7px;
        user-select: none;
        -moz-user-select: none;
    }
    tr.group-header.collapsed td i {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 5px solid #888;
        border-right: 0;
        margin-right: 10px;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.42;
        border-radius: 15px;
    }

    .coremudrbtn {
        cursor: pointer;
    }

    .coremubtninfo{
        color: #0185ac;
    }

    .coremubtnsuccess{
        color: #158550;
    }

    .coremubtnerror{
        color: #e26d62;
    }

    .coremudrline{
        font-size: 12px;
    }

    .dropdown-menu{
        Position: relative;
    }

</style>



<div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

    <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
        <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
            <span class="aaptilestitles3"> <h5> Tableau de bord </h5> </span>
            <?php
            if($tableonly){
                ?>

                <div class="dropdown actiondropdown">
                    <button class="btn tbdaction dropdown-toggle tooltips pull-right <?php echo ($totalansweraction > 0 ? "btn-warning" : "") ?>" data-placement="top"  data-original-title="Action en attente de d'interaction" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-exclamation"></i>(<label class="totalansweraction">  <?= $totalansweraction ?>  </label>)
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="dropdown-menu-item">
                            <label class="">Appel à candidature total</label> <label class=" actionnumber pull-right"> <b class="totalneedcandidat"> <?= $totalneedcandidat ?> </b></label>
                        </div>
                        <div class="dropdown-menu-item">
                            <label class="">Budget à valider total</label> <label class=" actionnumber pull-right"> <b class="totalpricetovalid"> <?= $totalpricetovalid ?></b> </label>
                        </div>
                        <div class="dropdown-menu-item">
                            <label class="">Candidat à valider total</label> <label class=" actionnumber pull-right"> <b class="totalcandidattovalid"> <?= $totalcandidattovalid ?></b> </label>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
            <div class="print button"></div>
            <div class="aapinnertiles3 col-md-12" id="dashboardTable" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

            </div>

        </div>
    </div>

</div>

<?php
if(!$tableonly){
?>

    <div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

        <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Budget par personne</h5> </span>
                <div class="print button"></div>
                <div class="aapinnertiles3 col-md-12" id="propositionpie" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                </div>

            </div>
        </div>

    </div>

    <div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

        <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Validé par personne</h5> </span>
                <div class="print button"></div>
                <div class="aapinnertiles3 col-md-12" id="propositionbar" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                </div>

            </div>
        </div>

    </div>

    <div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard">

        <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Sankey</h5> </span>
                <div class="print button"></div>
                <div class="aapinnertiles3 col-md-12" id="propositionsankey" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                </div>

            </div>
        </div>

    </div>

<?php
}

?>

<script type="text/javascript">
    var tableData = <?php echo ( !empty($tableAnswers) ? json_encode($tableAnswers) : "[]"); ?>;
    var tablePercentage = <?php echo ( !empty($tablepercentage) ? json_encode($tablepercentage) : "[]"); ?>;
    var tablePrice= <?php echo ( !empty($tableprice) ? json_encode($tableprice) : "[]"); ?>;
    var tableStatus= <?php echo ( !empty($tablestatus) ? json_encode($tablestatus) : "[]"); ?>;
    var tablecandidatename= <?php echo ( !empty($tablecandidatename) ? json_encode( $tablecandidatename) : "[]"); ?>;
    var tablecandidatestatus= <?php echo ( !empty($tablecandidatestatus) ? json_encode( $tablecandidatestatus) : "[]"); ?>;

    var sankeylink = '<?php echo json_encode($tableSankeyLinks); ?>';
    var sankeynode = '<?php echo json_encode($tableSankeyNodes); ?>';
    var depenseid = '<?php echo $depenseid; ?>';
    var el_form_id = '<?php echo $el_form_id; ?>';

    var isAjax = <?php echo ($tableonly? "true" : "false"); ?>;

    var tableonly = <?php echo ($tableonly? "true" : "false"); ?>;

    function htmlasignprice (labelvalue , pricevalue , number , maxprice , checkvalue , nh , th ) {
        let setmax  ="";
        if (maxprice !== null) {
            setmax = 'max="'+maxprice+'"'
        }
        if(th == 0){
            th = 30;
        }

        return '<tr class="row padding-10 rowprice" data-line="'+number+'">' +
            '            <td class="">' +
            '                    <input type="text" value="'+labelvalue+'" class="form-control arrayprice-label" aria-label="label">' +
            '             </td>' +
            '            <td class="">' +
            '                    <input type="number" data-line="'+number+'" min="0" value="'+th+'" class="form-control arrayprice-th" aria-label="price">' +
            '            </td>' +
            '            <td class="">' +
            '                    <input type="number" data-line="'+number+'" '+setmax+' min="0" value="'+nh+'" class="form-control arrayprice-nh" aria-label="Nombre d\'heure">' +
            '            </td>' +
            '            <td class="">' +
            '                    <input disabled type="number" data-line="'+number+'" '+setmax+' min="0" value="'+pricevalue+'" data-original="'+pricevalue+'" class="form-control arrayprice-price" aria-label="price">' +
            '            </td>' +
            '            <td class="">' +
            '                    <button class="btn btn-danger deletepriceline" type="button" data-line="'+number+'"><i class="fa fa-trash"></i></button>' +
            '            </td>' +
            '            <td><input class="arrayprice-check" type="text" hidden="true" value="'+checkvalue+'"></td>' +
            '        </tr>';

    }

    function setRowSpan(tableId, col){
        var rowSpanMap1 = {};
        var rowSpanMapB = {};
        var rowSpanMap = {};

        $(tableId).find('tr').each(function(){

            var valueOfTheSpannableCell1 = $($(this).children('td')[1]).text();
            $($(this).children('td')[1]).attr('data-original-value1', valueOfTheSpannableCell1);
            rowSpanMap1[valueOfTheSpannableCell1] = true;

            if(col != 1){

                $($(this).children('td')[col]).attr('data-original-value1', valueOfTheSpannableCell1);

                var valueOfTheSpannableCellBefore = $($(this).children('td')[col - 1]).text();
                $($(this).children('td')[col]).attr('data-original-valueB', valueOfTheSpannableCellBefore);
                rowSpanMapB[valueOfTheSpannableCellBefore] = true;

                if(col != 2){
                    var valueOfTheSpannableCellBeforeBefore = $($(this).children('td')[col - 2]).text();
                    $($(this).children('td')[col]).attr('data-original-valueBB', valueOfTheSpannableCellBefore);
                    rowSpanMapB[valueOfTheSpannableCellBefore] = true;
                }

                var valueOfTheSpannableCell = $($(this).children('td')[col]).text();
                $($(this).children('td')[col]).attr('data-original-value'+col+'', valueOfTheSpannableCell);
                rowSpanMap[valueOfTheSpannableCell] = true;
            }

        });


        if(col == 1){
            $.each(rowSpanMap1 , function(row, rowGroup) {
                var $cellsToSpan1 = $(tableId +' td[data-original-value1="' + row + '"]');
                var numberOfRowsToSpan1 = $cellsToSpan1.length;
                $cellsToSpan1.each(function (index) {
                    if (index == 0) {
                        $(this).attr('rowspan', numberOfRowsToSpan1);
                    } else {
                        $(this).hide();
                    }
                });
            });
        }else{
            $.each(rowSpanMap1 , function(row1, rowGroup1) {
                $.each(rowSpanMapB , function(rowB, rowGroupB) {
                    $.each(rowSpanMap , function(row, rowGroup) {
                        var $cellsToSpan = $(tableId +' td[data-original-value1="'+row1+'"][data-original-valueB="'+rowB+'"][data-original-value'+col+'="'+row+'"]');
                        var numberOfRowsToSpan = $cellsToSpan.length;
                        $cellsToSpan.each(function(index){
                            if(index==0){
                                $(this).attr('rowspan', numberOfRowsToSpan);
                            }else{
                                $(this).hide();
                            }
                        });
                    });
                });
            });
        }

    }

    function createPage1 (tableData , tableonly){
        $('#dashboardTable').createTable(tableData , {
            borderWidth:'1px',
            borderStyle:'solid',
            borderColor:'#DDDDDD',
            fontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',

            thBg:'#F3F3F3',
            thColor:'#0E0E0E',
            thHeight:'30px',
            thFontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',
            thFontSize:'14px',
            thTextTransform:'capitalize',

            trBg:'#FFFFFF',
            trColor:'#0E0E0E',
            trHeight:'25px',
            trFontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',
            trFontSize:'13px',

            tdPaddingLeft:'10px',
            tdPaddingRight:'10px'
        });
    }
    function createPage2 (tableData , tableonly){
        setRowSpan('#dashboardTable table', 1);
        setRowSpan('#dashboardTable table', 2);
        setRowSpan('#dashboardTable table', 3);
        setRowSpan('#dashboardTable table', 4);
        setRowSpan('#dashboardTable table', 5);
    }
    function createPage3 (tableData , tableonly){
        $("#dashboardTable table").addClass('ui-table-reflow');

        $("#dashboardTable table tr:nth-child(1) th:nth-child(2)").addClass('filter-select').data('placeholder' , "Sélectionner");
        $("#dashboardTable table tr:nth-child(1) th:nth-child(3)").addClass('filter-select').data('placeholder' , "Sélectionner");
        $("#dashboardTable table tr:nth-child(1) th:nth-child(5)").addClass('filter-select');
        $("#dashboardTable table tr:nth-child(1) th:nth-child(8)").addClass('filter-select').data('placeholder' , "Sélectionner");
        //$("#dashboardTable table tr:nth-child(1) th:nth-child(7)").addClass('filter-select').data('placeholder' , "Sélectionner");
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(4)" ).addClass('group-number-100');
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(2)" ).addClass('group-word');
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(3)" ).addClass('group-word');
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(5)" ).addClass('group-word');
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(4)" ).addClass('sorter-priceparser');
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(7)" ).addClass('sorter-statusparser');
        $( "#dashboardTable table tr:nth-child(1) th:nth-child(5)" ).addClass('filter-candidatename');

        $("#dashboardTable table tr:nth-child(1) th:nth-child(5)").addClass('filter-candidateparser');

        $( "#dashboardTable table tr" ).each(function(index) {
            //$( "#dashboardTable table tr:nth-child("+index+") td:nth-child(4)" ).data("percentage", tablePercentage[index]);
            $( "#dashboardTable table tr:nth-child("+index+") td:nth-child(4)" ).data("price", tablePrice[index-1]);
            $( "#dashboardTable table tr:nth-child("+index+") td:nth-child(7)" ).data("status", tableStatus[index-1]);
            $( "#dashboardTable table tr:nth-child("+index+") td:nth-child(5)" ).data("name", tablecandidatename[index-1]);
            //$( "#dashboardTable table tr:nth-child("+index+") td:nth-child(5)" ).data("status", tablecandidatestatus[index-1]);
        });

        $.tablesorter.themes.bootstrap = {
            // these classes are added to the table. To see other table classes available,
            // look here: http://getbootstrap.com/css/#tables
            table        : 'table table-bordered table-striped',
            caption      : 'caption',
            // header class names
            header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
            sortNone     : '',
            sortAsc      : '',
            sortDesc     : '',
            active       : '', // applied when column is sorted
            hover        : '', // custom css required - a defined bootstrap style may not override other classes
            // icon class names
            icons        : '', // add "bootstrap-icon-white" to make them white; this icon class is added to the <i> in the header
            iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
            iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
            iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
            filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
            footerRow    : '',
            footerCells  : '',
            even         : '', // even row zebra striping
            odd          : ''  // odd row zebra striping
        };

        // call the tablesorter plugin and apply the uitheme widget
        $.tablesorter.addParser({
            // set a unique id
            id: 'price',
            is: function(s, table, cell, $cell) {
                // return false so this parser is not auto detected
                return false;
            },
            format: function(s, table, cell, cellIndex) {
                var $cell = $(cell);
                // I could have used $(cell).data(), then we get back an object which contains both
                // data-lastname & data-date; but I wanted to make this demo a bit more straight-forward
                // and easier to understand.

                // first column (zero-based index) has lastname data attribute
                if (cellIndex == 3) {
                    //alert(s);
                    if(typeof $cell.data('price') != "undefined"){
                        return $cell.data('price') ;
                    }
                }else {
                    return s;
                }
                /*else if (cellIndex === 2) {
                    // return "mm-dd" that way we don't need to use "new Date()" to process it
                    return $cell.attr('data-date') || s;
                }*/

            },
            // flag for filter widget (true = ALWAYS search parsed values; false = search cell text)
            //parsed: true,
            // set type, either numeric or text
            type: 'text'
        });

        $.tablesorter.addParser({
            // set a unique id
            id: 'candidatename',
            is: function(s, table, cell, $cell) {
                // return false so this parser is not auto detected
                return false;
            },
            format: function(s, table, cell, cellIndex) {
                var $cell = $(cell);
                // I could have used $(cell).data(), then we get back an object which contains both
                // data-lastname & data-date; but I wanted to make this demo a bit more straight-forward
                // and easier to understand.

                // first column (zero-based index) has lastname data attribute
                if(typeof $cell.data('name') != "undefined") {
                    return $cell.data('name');
                }
                /*else if (cellIndex === 2) {
                    // return "mm-dd" that way we don't need to use "new Date()" to process it
                    return $cell.attr('data-date') || s;
                }*/

            },
            // flag for filter widget (true = ALWAYS search parsed values; false = search cell text)
            //parsed: true,
            // set type, either numeric or text
            type: 'text'
        });

        $.tablesorter.addParser({
            // set a unique id
            id: 'candidatestatus',
            is: function(s, table, cell, $cell) {
                // return false so this parser is not auto detected
                return false;
            },
            format: function(s, table, cell, cellIndex) {
                var $cell = $(cell);
                // I could have used $(cell).data(), then we get back an object which contains both
                // data-lastname & data-date; but I wanted to make this demo a bit more straight-forward
                // and easier to understand.

                // first column (zero-based index) has lastname data attribute
                if(typeof $cell.data('status') != "undefined") {
                    return $cell.data('status');
                }
                /*else if (cellIndex === 2) {
                    // return "mm-dd" that way we don't need to use "new Date()" to process it
                    return $cell.attr('data-date') || s;
                }*/

            },
            // flag for filter widget (true = ALWAYS search parsed values; false = search cell text)
            //parsed: true,
            // set type, either numeric or text
            type: 'text'
        });

        $.tablesorter.addParser({
            // set a unique id
            id: 'priceparser',
            is: function(s, table, cell, $cell) {
                // return false so this parser is not auto detected
                return false;
            },
            format: function(s, table, cell, cellIndex) {
                var $cell = $(cell);
                // I could have used $(cell).data(), then we get back an object which contains both
                // data-lastname & data-date; but I wanted to make this demo a bit more straight-forward
                // and easier to understand.

                // first column (zero-based index) has lastname data attribute
                if(typeof $cell.data('price') != "undefined") {
                    return $cell.data('price');
                }
                /*else if (cellIndex === 2) {
                    // return "mm-dd" that way we don't need to use "new Date()" to process it
                    return $cell.attr('data-date') || s;
                }*/

            },
            // flag for filter widget (true = ALWAYS search parsed values; false = search cell text)
            //parsed: true,
            // set type, either numeric or text
            type: 'numeric'
        });

        $.tablesorter.addParser({
            // set a unique id
            id: 'statusparser',
            is: function(s, table, cell, $cell) {
                // return false so this parser is not auto detected
                return false;
            },
            format: function(s, table, cell, cellIndex) {
                var $cell = $(cell);
                // I could have used $(cell).data(), then we get back an object which contains both
                // data-lastname & data-date; but I wanted to make this demo a bit more straight-forward
                // and easier to understand.

                // first column (zero-based index) has lastname data attribute
                if(typeof $cell.data('status') != "undefined") {
                    return $cell.data('status');
                }
                /*else if (cellIndex === 2) {
                    // return "mm-dd" that way we don't need to use "new Date()" to process it
                    return $cell.attr('data-date') || s;
                }*/

            },
            // flag for filter widget (true = ALWAYS search parsed values; false = search cell text)
            //parsed: true,
            // set type, either numeric or text
            type: 'text'
        });

        $.tablesorter.addParser({
            // set a unique id
            id: 'candidateparser',
            is: function(s, table, cell, $cell) {
                // return false so this parser is not auto detected
                return false;
            },
            format: function(s, table, cell, cellIndex) {
                var $cell = $(cell);
                // I could have used $(cell).data(), then we get back an object which contains both
                // data-lastname & data-date; but I wanted to make this demo a bit more straight-forward
                // and easier to understand.

                // first column (zero-based index) has lastname data attribute
                mylog.log("eeee" , s, table, cell, cellIndex);

                let substr = s.split(',');
                $.each(substr, function(index, value) {

                });

                /*else if (cellIndex === 2) {
                    // return "mm-dd" that way we don't need to use "new Date()" to process it
                    return $cell.attr('data-date') || s;
                }*/

            },
            // flag for filter widget (true = ALWAYS search parsed values; false = search cell text)
            //parsed: true,
            // set type, either numeric or text
            type: 'text'
        });

        var candidateFilterObj  = [
        ];

        var priceFilterObj  = [
        ];

        $.each(tablecandidatename , function(key, value) {
            if (!candidateFilterObj.some(candidate => candidate.text === value)) {
                candidateFilterObj.push({ value : value ,   text : value })
            }
        });

        $("#dashboardTable table").tablesorter({
            // this will apply the bootstrap theme if "uitheme" widget is included
            // the widgetOptions.uitheme is no longer required to be set
            theme : "bootstrap",

            widthFixed: true,

            headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

            // widget code contained in the jquery.tablesorter.widgets.js file
            // use the zebra stripe widget if you plan on hiding any rows (filter widget)
            widgets : [ "uitheme", "filter", "columns", "zebra" , "print" , "group" , "reflow"],

            widgetOptions : {
                columns: [ "primary", "secondary", "tertiary" ],
                print_now        : true,
                filter_reset : ".reset",
                filter_cssFilter: "form-control",
                group_collapsible : true,
                group_collapsed   : false,
                group_count       : true,
                filter_childRows  : true,
                filter_columnFilters   : true,
                headerTitle_useAria    : true,

                group_count       : " ({num})",

                reflow_className    : 'ui-table-reflow',
                // header attribute containing modified header name
                reflow_headerAttrib : 'data-name',
                // data attribute added to each tbody cell
                // it contains the header cell text, visible upon reflow
                reflow_dataAttrib   : 'data-title',

                filter_selectSource  : {
                    4 : candidateFilterObj,
                    7 : priceFilterObj
                }

            },

            headers: {
                3 : { sorter: 'priceparser' },
                5 : { filter: 'candidatename' },
                7 : { filter_cssFilter : 'a'},

            },

            group_callback    : function($cell, $rows, column, table) {
                // callback allowing modification of the group header labels
                // $cell = current table cell (containing group header cells ".group-name" & ".group-count"
                // $rows = all of the table rows for the current group; table = current table (DOM)
                // column = current column being sorted/grouped
                if (column === 3) {
                    var subtotal = 0;
                    $rows.each(function() {
                        subtotal += parseFloat( $(this).find("td").eq(column).text() );
                    });
                    $cell.find(".group-count").append("; subtotal: " + subtotal );
                }
            },
            // event triggered on the table when the grouping widget has finished work
            group_complete    : "groupingComplete",

            on_filters_loaded: function(o){
                o.SetFilterValue(paramCol, paramCriteria);
                o.Filter();

            }

        });

        $(".valideline").off().click(function() {
            var thisbtn = $(this);
            tplCtx.collection = "answers";
            tplCtx.id = thisbtn.data("id");
            tplCtx.path = thisbtn.data("path");
            if(thisbtn.data("action")) {
                tplCtx.value = thisbtn.data("action");
            }else{
                tplCtx.value = null;
            }

            if (typeof tplCtx.setType != "undefined"){
                delete tplCtx.setType;
            }

            dataHelper.path2Value(tplCtx, function(params) {
                ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                toastr.success('enregistré');
                ajaxPost("", baseUrl+'/survey/form/coremudashboard/tableonly/true/jsononly/true/el_form_id/'+el_form_id , {},
                        function(data) {
                            tableData = data.tableAnswers;
                            createPage (tableData , false);
                        } , "json"
                    );
            });
        });

        $(".validebudgetperc").off().click(function() {
            var thisbtn = $(this);
            tplCtx = {};
            tplCtx.collection = "answers";
            tplCtx.id = thisbtn.data("id");
            tplCtx.path = thisbtn.data("path");
            if(thisbtn.data("action")) {
                tplCtx.value = thisbtn.data("action");
            }else{
                tplCtx.value = null;
            }

            if (typeof tplCtx.setType != "undefined"){
                delete tplCtx.setType;
            }

            dataHelper.path2Value(tplCtx, function(params) {
                ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                toastr.success('enregistré');
                ajaxPost("", baseUrl+'/survey/form/coremudashboard/tableonly/true/jsononly/true/el_form_id/'+el_form_id , {},
                        function(data) {
                            tableData = data.tableAnswers;
                            createPage (tableData , false);
                        } , "json"
                    );
            });
        });

        $(".validebudgetasg").off().click(function() {
            var thisbtn = $(this);
            tplCtx = {};
            tplCtx.collection = "answers";
            tplCtx.id = thisbtn.data("id");
            tplCtx.path = thisbtn.data("path");
            if(thisbtn.data("action")) {
                tplCtx.value = thisbtn.data("action");
            }else{
                tplCtx.value = null;
            }

            if (typeof tplCtx.setType != "undefined"){
                delete tplCtx.setType;
            }

            dataHelper.path2Value(tplCtx, function(params) {
                ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                toastr.success('enregistré');
                ajaxPost("", baseUrl+'/survey/form/coremudashboard/tableonly/true/jsononly/true/el_form_id/'+el_form_id , {},
                        function(data) {
                            tableData = data.tableAnswers;
                            createPage (tableData , false);
                        } , "json"
                    );
            });
        });

        $(".candidatebtn").off().click(function() {
            tplCtx.pos = $(this).data("pos");
            tplCtx.collection = "answers";
            tplCtx.id = $(this).data("id");
            tplCtx.key = "depense";
            tplCtx.form = "aapStep1";
            var currentValue = [];
            tplCtx.value = {
                "name" : typeof userConnected.name != "undefined" ? userConnected.name : userId,
                "addedBy" : userId
            };

            tplCtx.path = "answers";
            // if( notNull(formInputs [tplCtx.form]) )
            tplCtx.path = "answers."+tplCtx.form;

            //tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+currentUserId;
            tplCtx.path = tplCtx.path+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+userId;

            tplCtx.setType = [
                {
                    "path": "price",
                    "type": "int"
                },
                {
                    "path": "date",
                    "type": "isoDate"
                }
            ];

            var today = new Date();
            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();

            tplCtx.value.date = today;

            dataHelper.path2Value(tplCtx, function(params) {
                ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                toastr.success('enregistré');
                ajaxPost("", baseUrl+'/survey/form/coremudashboard/tableonly/true/jsononly/true/el_form_id/'+el_form_id , {},
                        function(data) {
                            tableData = data.tableAnswers;
                            createPage (tableData , false);
                        } , "json"
                    );
            });
        });

        var formatState = function (state) {
            if (!state.id) { return state.text; }

            var $state = jQuery(
                '<span>' + state.text + '</span>'
            );
            return $state;
        };

        let optionSelect = {
            formatSelection: formatState,
            closeOnSelect: false,
            //width: '80%',
            placeholder: "Sélectionner"
            /*,containerCssClass : 'aapstatus-container',
            dropdownCssClass: "aapstatus-dropdown"*/
        };

        if(!tableonly){
            $( ".tablesorter-filter.form-control[data-column='1']").attr("disabled" , "disabled");
        }

        paramsAssignBudgetArray = {
            "jsonSchema" : {
                "title" : "Demander une CoRénumeration",
                "properties" : {
                    "AssignBudgetArray" : {
                        "label": "Budgets attribués à ce candidat",
                        "placeholder": "",
                        "inputType": "custom",
                        "html" : '<table id="" class="container assignpricelabel">' +
                            '<tr class="row padding-10 rowprice">' +
                            '<td>Label</td>' +
                            '<td>Taux horaire</td>' +
                            '<td>Nombre d\'heure</td>' +
                            '<td>Prix total</td>' +
                            '<td></td>' +
                            '</tr>        ' +
                            '    </table>' +
                            '<div class="row center"><button class="btn btn-secondary addpricelistline" type="button"><i class="fa fa-plus"></div>'
                    },
                },
                onLoads:{
                    onload : function(){
                        if (notNull(tplCtx.assignpricelist)) {
                            $.each(tplCtx.assignpricelist, function (index, val) {
                                valCheck = 'false';
                                if (typeof val.check != 'undefined') {
                                    valCheck = val.check;
                                }
                                let th = 0;
                                let nh = 0;
                                if (typeof val.hourlyRate != 'undefined') {
                                    th = val.hourlyRate;
                                }
                                if (typeof val.hour != 'undefined') {
                                    nh = val.hour;
                                }
                                $('.assignpricelabel').append(htmlasignprice(val.label, val.price , index , tplCtx.max , valCheck , nh , th));
                            });
                        }else{
                            $('.assignpricelabel').append(htmlasignprice("" , "" , $('.rowprice').size() , tplCtx.max , 'false' , 0 , 0));
                            $('.deletepriceline').off().click(function() {
                                let thisbtn = $(this);
                                $('.rowprice[data-line="'+thisbtn.data('line')+'"]').remove();
                            });
                            $('input.arrayprice-label').focus();
                        }

                        $('.arrayprice-nh , .arrayprice-th').on('keyup', function(e) {
                            let line = parseInt($(this).data('line'));
                            let nh = parseInt($('.arrayprice-nh[data-line="'+line+'"]').val());
                            let th = parseInt($('.arrayprice-th[data-line="'+line+'"]').val());
                            if(nh > 0 && th > 0){
                                $('.arrayprice-price[data-line="'+line+'"]').val(nh * th);
                            }
                        });

                        $('.addpricelistline').off().click(function() {
                            $('.assignpricelabel').append(htmlasignprice("" , "" , $('.rowprice').size() , tplCtx.max , 'false' , 0 , 0));
                            $('input.arrayprice-label').last().focus();
                            $('.deletepriceline').off().click(function() {
                                let thisbtn = $(this);
                                $('.rowprice[data-line="'+thisbtn.data('line')+'"]').remove();
                            });
                            var max = tplCtx.max;
                            var $inputs = $('.arrayprice-price');
                            $inputs.on('keyup', function(e) {
                                var $this = $(this);
                                var originalvalue = $this.attr('data-original');
                                max += originalvalue;
                                var sum = sumInputs($inputs.not(function(i, el) {
                                    return el === e.target;
                                }));
                                var value = parseInt($this.val(), 10) || 0;
                                if(sum + value > max) $this.val(max - sum);
                            });
                            $('.arrayprice-nh , .arrayprice-th').on('keyup', function(e) {
                                let line = parseInt($(this).data('line'));
                                let nh = parseInt($('.arrayprice-nh[data-line="'+line+'"]').val());
                                let th = parseInt($('.arrayprice-th[data-line="'+line+'"]').val());
                                if(nh > 0 && th > 0){
                                    $('.arrayprice-price[data-line="'+line+'"]').val(nh * th);
                                }
                            });
                        });

                        $('.deletepriceline').off().click(function() {
                            let thisbtn = $(this);
                            $('.rowprice[data-line="'+thisbtn.data('line')+'"]').remove();
                        });

                        var max = tplCtx.max;
                        var $inputs = $('.arrayprice-price');

                        $inputs.on('keyup', function(e) {
                            var $this = $(this);
                            var originalvalue = $this.attr('data-original');
                            var sum = sumInputs($inputs.not(function(i, el) {
                                return el === e.target;
                            }));
                            var value = parseInt($this.val(), 10) || 0;
                            //alert(sum +" "+ value +" "+ max +" "+  parseInt(originalvalue));
                            if(sum + value > (max + parseInt(originalvalue))) $this.val(max - sum);
                        });

                    }
                },
                save : function () {
                    let priceobj = [];
                    $('.rowprice').each(function(){
                        let priceobjline = {};
                        if ($(this).find('.arrayprice-label').val() || $(this).find('.arrayprice-price').val()) {
                            if ($(this).find('.arrayprice-label').val()) {
                                priceobjline["label"] = $(this).find('.arrayprice-label').val();
                            } else {
                                priceobjline["label"] = " ";
                            }
                            if ($(this).find('.arrayprice-label').val()) {
                                priceobjline["price"] = $(this).find('.arrayprice-price').val();
                            } else {
                                priceobjline["price"] = 0;
                            }
                            if ($(this).find('.arrayprice-nh').val()) {
                                priceobjline["hour"] = $(this).find('.arrayprice-nh').val();
                            } else {
                                priceobjline["hour"] = 0;
                            }
                            if ($(this).find('.arrayprice-th').val()) {
                                priceobjline["hourlyRate"] = $(this).find('.arrayprice-th').val();
                            } else {
                                priceobjline["hourlyRate"] = 0;
                            }
                            if ($(this).find('.arrayprice-label').val()) {
                                priceobjline["check"] = $(this).find('.arrayprice-check').val();
                            } else {
                                priceobjline["check"] = 0;
                            }

                            priceobj.push(priceobjline);
                        }


                    });

                    tplCtx.value = priceobj;
                    if (typeof tplCtx.setType != "undefined"){
                        delete tplCtx.setType;
                    }
                    var path2 = tplCtx.path;
                    tplCtx.path = tplCtx.path + "AssignBudgetArray";
                    dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.closeForm();
                        /*tplCtx.path = path2 + "validate";
                        tplCtx.value = null;
                        dataHelper.path2Value( tplCtx, function(params) {*/
                        ajaxPost("",baseUrl+'/survey/form/pingupdate/answerId/'+ answerId,{},function(data){});
                        /*});*/
                    });
                },

            }
        };

        $('.btnAssignBudgetArrayT').off().click(function() {
            var apl = $(this).data("assignpricelist");
            tplCtx.pos = $(this).data("pos");
            tplCtx.uid = $(this).data("uid");
            tplCtx.collection = "answers";
            tplCtx.id = $(this).data("id");
            tplCtx.key = $(this).data("key");
            tplCtx.form = $(this).data("form");
            tplCtx.assignpricelist = apl;
            tplCtx.max = $(this).data("max");
            tplCtx.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".";

            var tplCtx2 = {};
            tplCtx2.pos = $(this).data("pos");
            tplCtx2.uid = $(this).data("uid");
            tplCtx2.collection = "answers";
            tplCtx2.id = $(this).data("id");
            tplCtx2.key = $(this).data("key");
            tplCtx2.form = $(this).data("form");
            tplCtx2.assignpricelist = apl;
            tplCtx2.path = "answers."+tplCtx.form+"."+tplCtx.key+"."+tplCtx.pos+".estimates."+tplCtx.uid+".";

            dyFObj.openForm( paramsAssignBudgetArray ,null, { "AssignBudgetArray" : apl},  null , null, {
                type : "bootbox",
                notCloseOpenModal : true,
            });
        });
    }

    function createPage(tableData , tableonly) {
        /*var def = $.Deferred();
        def.then(createPage1(tableData , tableonly), createPage2(tableData , tableonly), createPage3(tableData , tableonly));
   */
        //$.when(createPage1(tableData , tableonly)).then(createPage2(tableData , tableonly)).then(createPage3(tableData , tableonly));
        createPage1(tableData , tableonly);
        createPage3(tableData , tableonly);
    }

    var mybarcolor = [
        "#b30040",
        "#50bd60",
        "#7c00e4",
        "#b67994",
        "#00c5e6",
        "#006b0f",
        "#f06f29",
    ];

    jQuery(document).ready(function() {

        if(!notNull(wsCO) || !wsCO.hasListeners('refresh-coform-answer')) {
            var wsCO = null;

            var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshCoformAnswerUrl;
            if (socketConfigurationEnabled) {
                wsCO = io(coWsConfig.serverUrl, {
                    query: {
                        answerId: answerId
                    }
                });

                wsCO.on('refresh-coform-answer', function () {

                        ajaxPost("", baseUrl+'/survey/form/coremudashboard/tableonly/false/jsononly/true/el_form_id/'+el_form_id , {},
                            function(data) {
                                tableData = data["tableAnswers"];
                                createPage (data["tableAnswers"] , false);

                            } , "json"
                        );
                });
            }
            createPage (tableData , true)
        }

       // createPage (tableData);

       // let $selectperson = $("table tr:nth-child(2) td[data-column='4'] .tablesorter-filter.form-control[data-column='4']").select2(optionSelect);

        ajaxPost("#propositionbar", baseUrl+'/graph/co/chart/',
            {
                id : "propositionbar",
                data : <?php echo json_encode($realpropositiondata); ?>,
                label : <?php echo json_encode($propobarlabel); ?>,
                labels : <?php echo json_encode($propopielabels); ?>,
                g : 'graph.views.co.ocecoform.barmulti',
                colors : mybarcolor
            }
            , function(){} ,"html");

        ajaxPost("#propositionpie", baseUrl+'/graph/co/chart/',
            {
                id : "propositionpie",
                data : <?php echo json_encode($propopiedata); ?>,
                label : 'Proposition',
                labels : <?php echo json_encode($propopielabels); ?>,
                g : 'graph.views.co.ocecoform.piechart',
                colors : mybarcolor
            }
            , function(){} ,"html");

        ajaxPost("#propositionsankey", baseUrl+'/graph/co/chart/',
            {
                id : "propositionsankey",
                data : {
                    "links": sankeylink,
                    "nodes": sankeynode
                },
                labels : [ "Candidats" ,  "Projets"],
                g : 'graph.views.co.ocecoform.sankey',
                colors : mybarcolor,
            }
            , function(){} ,"html");

    });

</script>

