<?php

use CacheHelper;
use Aap;

HtmlHelper::registerCssAndScriptsFiles(["/css/aap/aap.css"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(["/css/form/budget.css"], Yii::app()->getModule('survey')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(["/js/appelAProjet/aap.js"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(array(
    '/js/form.js'
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

$logoOceco = Yii::app()->getModule('co2')->assetsUrl . "/images/OCECO.png";
HtmlHelper::registerCssAndScriptsFiles(array(
    '/js/profilSocialaap.js',
), Yii::app()->getModule(Survey::MODULE)->getAssetsUrl());

if (isset($clienturi) && !empty(explode('#', $clienturi)[1]) && explode('#', $clienturi)[1] == "oceco" && !str_contains($clienturi, 'aappage')) {
    echo '<div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">
        <button class="btn btn-primary btn-lg aap-lists-btn-2">Voir la liste des oceco</button>
    </div>';
    echo '<script>aapObject.sections.common.events(aapObject);$(".aap-lists-btn-2").trigger("click");</script>';
    return;
}

if (empty($modalrender)) {
    $modalrender = false;
    $renderUrl = /*$_SERVER['REQUEST_URI'];*/ explode('#', $clienturi)[1];
} else {
    $modalrender = true;
    $renderUrl = explode('/', $urlR);
    $renderUrl = implode('.', $renderUrl);
}

$exp_url = explode(".", $renderUrl);
$el_slug = null;
$elform = null;
$isAdmin = false;
//$page = "list";
//$elanswers = [];
$el_form_id = "";
$istableview = false;
$exp_url_ = explode(".", $renderUrl);
if (
    in_array("slug", $exp_url) &&
    isset($exp_url[array_search('slug', $exp_url) + 1]) &&
    is_string($exp_url[array_search('slug', $exp_url) + 1]) &&
    $exp_url[array_search('slug', $exp_url) + 1] != ""
) {
    $el_slug = $exp_url[array_search('slug', $exp_url) + 1];
}

if (
    in_array("formid", $exp_url) &&
    isset($exp_url[array_search('formid', $exp_url) + 1]) &&
    is_string($exp_url[array_search('formid', $exp_url) + 1]) &&
    $exp_url[array_search('formid', $exp_url) + 1] != ""
) {
    $el_form_id = $exp_url[array_search('formid', $exp_url) + 1];
    $elform = PHDB::findOneById(Form::COLLECTION, $el_form_id);
    $inputs = PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => $el_form_id));
    if (!empty($elform)) {
        $el_configform = PHDB::findOneById(Form::COLLECTION, $elform["config"]);
        //$elanswers = PHDB::find(Form::ANSWER_COLLECTION,array( "form" => (string)$elform["_id"],"answers.aapStep1.titre"=>['$exists' => true]));
        $isAdmin = !$modalrender && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)@$elform["_id"]));
    }
}

if (
    in_array("aappage", $exp_url) &&
    isset($exp_url[array_search('aappage', $exp_url) + 1]) &&
    is_string($exp_url[array_search('aappage', $exp_url) + 1]) &&
    $exp_url[array_search('aappage', $exp_url) + 1] != ""
) {
    $page = $exp_url[array_search('aappage', $exp_url) + 1];
}

if (
    in_array("answer", $exp_url) &&
    isset($exp_url[array_search('answer', $exp_url) + 1]) &&
    is_string($exp_url[array_search('answer', $exp_url) + 1]) &&
    $exp_url[array_search('answer', $exp_url) + 1] != ""
) {
    $answerid = $exp_url[array_search('answer', $exp_url) + 1];
}

if (in_array("editform", $exp_url)) {
    $pageedit = true;
} else {
    $pageedit = false;
}

if (isset($this->costum["slug"])) {
    $slug = $this->costum["slug"];
    if (empty($this->costum["type"])) {
        PHDB::update($this->costum["contextType"], array("_id" => new MongoId($this->costum["contextId"])), array('$set' => ["costum.type" => "aap"]));
        CacheHelper::delete($this->costum["contextSlug"]);
        if (CacheHelper::get($_SERVER['SERVER_NAME']))
            CacheHelper::delete($_SERVER['SERVER_NAME']);
    }
}
$el = null;
if (!empty($el_slug)) {
    $el = Slug::getElementBySlug($el_slug, array("name", "type", "collection", "profilImageUrl", "slug", "oceco", "links"));
    if (isset($this->costum["slug"]) && isset($el["el"]["oceco"]["haveSpecificFilters"]) && $el["el"]["oceco"]["haveSpecificFilters"]) {
        HtmlHelper::registerCssAndScriptsFiles(["/js/appelAProjet/" . $this->costum["slug"] . ".js"], Yii::app()->getModule('costum')->assetsUrl);
    }

    $elCount = Element::getByTypeAndId($el["type"], $el["id"]);
    $el["type"] = $elCount["collection"];
    $el["links"] = $elCount["links"];

    $el["rolesLists"] = [];
    if (!isset($el["counts"])) $el["counts"] = $elCount["counts"];
    $el["counts"]["admin"] = 0;
    if (in_array($el["el"]["collection"], [Person::COLLECTION, Project::COLLECTION, Organization::COLLECTION, Event::COLLECTION, Proposal::COLLECTION, Action::COLLECTION])) {
        $el["counts"][Link::$linksTypes[$el["el"]["collection"]][Person::COLLECTION] . "Active"] = 0;

        if (isset($el["links"][Link::$linksTypes[$el["el"]["collection"]][Person::COLLECTION]])) {
            foreach ($el["links"][Link::$linksTypes[$el["el"]["collection"]][Person::COLLECTION]] as $e => $v) {
                if (isset($v["roles"])) {
                    foreach ($v["roles"] as $role) {
                        if (!empty($role)) {
                            if (!isset($el["rolesLists"][$role])) {
                                $el["rolesLists"][$role] = array("count" => 1, "label" => $role);
                            } else
                                $el["rolesLists"][$role]["count"]++;
                        }
                    }
                }
                if (isset($v[Link::IS_INVITING])) {
                    if (isset($el["counts"][Link::IS_INVITING]))
                        $el["counts"][Link::IS_INVITING]++;
                    else
                        $el["counts"][Link::IS_INVITING] = 1;
                } else if (isset($v[Link::TO_BE_VALIDATED]) || isset($v[Link::IS_ADMIN_PENDING])) {
                    if (isset($el["counts"]["toBeValidated"]))
                        $el["counts"]["toBeValidated"]++;
                    else
                        $el["counts"]["toBeValidated"] = 1;
                } else {
                    $el["counts"][Link::$linksTypes[$el["el"]["collection"]][Person::COLLECTION] . "Active"]++;
                    if (isset($v["isAdmin"])) {
                        $el["counts"]["admin"]++;
                    }
                }
            }
        }
    }

    if (in_array($el["el"]["collection"], [Person::COLLECTION, Project::COLLECTION, Organization::COLLECTION])) {
        $el["counts"]["poi"] = PHDB::count(Poi::COLLECTION, array("parent." . $el["id"] => array('$exists' => true)));
        $el["counts"]["crowdfunding"] = PHDB::count(Crowdfunding::COLLECTION, array("parent." . $el["id"] => array('$exists' => true), "type" => Crowdfunding::TYPE_CAMPAIGN));
        $el["counts"]["jobs"] = PHDB::count(Classified::COLLECTION, array("parent." . $el["id"] => array('$exists' => true), "type" => Classified::TYPE_JOBS));
        $el["counts"]["ressources"] = PHDB::count(Classified::COLLECTION, array("parent." . $el["id"] => array('$exists' => true), "type" => Classified::TYPE_RESSOURCES));
        $el["counts"]["classifieds"] = PHDB::count(Classified::COLLECTION, array("parent." . $el["id"] => array('$exists' => true), "type" => Classified::COLLECTION));
    }

    $elconfig = Slug::getElementBySlug($slug);
}

$elform["open"] = false;
if (
    (isset($elform["startDate"]) && isset($elform["endDate"]) &&
        ((time() >= strtotime(str_replace("/", "-", $elform["startDate"]))) && (time() <= strtotime(str_replace("/", "-", $elform["endDate"]))))
    ) || (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)@$elform["_id"]))
) {
    $elform["open"] = true;
}

//cms mode
$blockKey = @$blockCms["_id"];
$keyTpl = "callForProjects";

if (!empty($blockCms))
    $kunik = $keyTpl . (string)$blockCms["_id"];
else
    $kunik = "";

$paramsData = [
    "titleDetails" => "Details de l'appel à projet",
    "titlecoRemu" => "Observatoire CoRému",
    "titleList" => "Projets",
    "titleMyActions" => "Mes actions",
    "titleEvaluate" => "Evaluer",
    "titleObservatory" => "Observatoire global",
    "titleKanban" => "Kanban",
    "titleBtnNewProposition" => (!empty($elform["subType"]) && $elform["subType"] == "aap") ? "Fiche Action" : "Proposition",
    "homeTplAap" => " Vous êtes salarié, bénévole, prestataire sur une fonction de gestion de tiers-lieu, ce rapide sondage est fait pour vous !<br><br>


                🧾  Cette enquête vise à établir un état des lieux de l’emploi et des contributions en tiers-lieu en 2022 pour donner à voir l’évolution du secteur, adapter les modalités d’accompagnement des structures employeuses et les besoins en formation des salariés / bénévoles<br><br>


                📨  La Coopérative Tiers-Lieux, La Compagnie des Tiers-Lieux, Cap Tiers-Lieux Pays de la Loire, le consortium Île de France Tiers-Lieux, Illusion et Macadam, Bretagne Tiers-Lieux et le Réseau Sud Tiers-Lieux, auront accès aux réponses de ce questionnaire. Les données que vous nous transmettez seront traitées de manière anonyme.<br><br>


                ⌛️ Nous vous remercions de prendre le temps de répondre  à ce sondage. Attention, prévoyez bien 15 minutes pour le remplir et ne laisser pas la page inactive plus de 5 minutes sans quoi vos réponses s'auto-détruiront instantanément.<br><br>
"
];


if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<script type="text/javascript">
    $('.dropdown-submenu-aap a.dropdown-submenu-toggle').on("click", function(e) {
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
    setTimeout(() => {
        if (location.hash == "#oceco") {
            $(".aap-lists-btn").trigger('click');
        }
    }, 900);
</script>

<?php
if (!empty($page) && !empty($el_slug) && !empty($el_configform) && !empty($elconfig)) {
    echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.menuAap", array(
        "modalrender" => $modalrender,
        "page" => $page,
        "el_slug" => $el_slug,
        "el_form_id" => $el_form_id,
        "slug" => $slug,
        "elform" => $elform,
        "isAdmin" => $isAdmin,
        "paramsData" => $paramsData,
        "kunik" => $kunik,
        "el" => $el,
    ), true);

    if (array_keys($el_configform["parent"])[0] == $elconfig["id"]) {
        unset($el["rolesLists"]);
        unset($el["counts"]);

        if (!empty(Yii::app()->session["userId"]) && isset($el["links"]["members"][Yii::app()->session["userId"]]))
            $el["myLinks"] = $el["links"]["members"][Yii::app()->session["userId"]];
        else
            $el["myLinks"] = null;

        unset($el["links"]);

        $params = [
            "answerid" => @$answerid,
            "slug" => @$slug, // slug de l'element qui abbrite le form config
            "el_slug" => @$el_slug,  // element qui abbrite le form config
            "elform" => @$elform, //le form parent
            "el" => @$el, //element qui abbrite le form parent
            "el_configform" => @$el_configform,
            "el_form_id" => @$el_form_id,
            "modalrender" => $modalrender
            //"answers" => @$answers
        ];

        if (!empty($answerid)) {
            $params["answerid"] = $answerid;
        }
        if (!empty($el) && !empty($el["id"])) {
            $params["elid"] = $el["id"];
        }

        if (isset($pageedit)) {
            $params["pageedit"] = $pageedit;
        }
        $page = explode("?", $page);
        $page = $page[0]; ?>
        <script>
            aapObject.formParent = <?= json_encode($elform) ?>;
            aapObject.formConfig = <?= json_encode($el_configform) ?>;
            aapObject.elementAap = <?= json_encode($el) ?>;
            aapObject.formInputs = <?= json_encode($inputs) ?>;
            aapObject.statusicon = <?= json_encode(Aap::$statusicon) ?>;
            aapObject.init();
            var fObjj = formObj.init({});
            fObjj.container = "block-container-callForProjects60dd6a3860e5d2695e342d2b";
            fObjj.events.form(fObjj);
        </script>
    <?php echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects." . $page, $params, true);
        echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.footer", $params, true);
    } else { ?>
        <div class="container" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);">
            <div class="alert alert-danger">
                <strong><?= $slug ?></strong> n'est pas compatible avec votre formulaire
            </div>
        </div>
<?php }
}
?>
<?php if (!empty($clienturi) && str_contains($clienturi, 'aappage')) { ?>
    <script>
        if (typeof sectionDyf != "undefined")
            sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
        contextDataAap = <?= isset($elCount) ? json_encode($elCount) : json_encode(null) ?>;
        if (notNull(contextDataAap)) {
            contextDataAap["id"] = (notEmpty(contextDataAap) && notEmpty(contextDataAap["_id"])) ? contextDataAap["_id"]["$id"] : null;
            contextDataAap["type"] = (notEmpty(contextDataAap) && notEmpty(contextDataAap["_id"])) ? contextDataAap["collection"] : null;
        }

        var hashUrlPage = "#<?= !empty($page) ? $page : "" ?>";

        //all events is here
        var propostionObj = {
            refreshNewCounter: function(answerId = null) {
                if (notNull(aapObject.elementAap)) {
                    ajaxPost(
                        null,
                        baseUrl + '/survey/answer/countvisit/formId/' + aapObject.formParent._id.$id,
                        null,
                        function(data) {
                            if (data.notSeenCounter != 0) {
                                setTimeout(() => {
                                    setTitle('(' + data.notSeenCounter + ') ' + aapObject.elementAap["el"]["name"] + ' > ' + aapObject.formParent["name"])
                                }, 700);
                                $(".new-prop-counter").text(data.notSeenCounter).show().attr("data-original-title", data.notSeenCounter + " nouvelle(s) proposition(s)");
                            } else {
                                if (exists(aapObject.elementAap))
                                    setTimeout(() => {
                                        setTitle(aapObject.elementAap.el.name + ' > ' + aapObject.formParent["name"])
                                    }, 700);
                                $(".new-prop-counter").hide();
                            }

                            if (answerId != null) {
                                $('#list-' + answerId).removeClass('unseen-proposition');
                                $('#list-' + answerId + " .ribbon").remove()
                                $("[data-eid='" + answerId + "']").removeClass('unseen-proposition');
                            }
                        },
                        "json"
                    );
                }
            }
        };

        var paramsNotif = {
            "jsonSchema": {
                "title": "Configurer les notifications",
                "icon": "fa-cog",
                "properties": {
                    newproposition: {
                        "inputType": "checkboxSimple",
                        "label": "Nouvelle proposition",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.newproposition != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.newproposition) : true
                    },
                    deleteproposition: {
                        "inputType": "checkboxSimple",
                        "label": "Suppression d'une proposition",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.deleteproposition != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.deleteproposition) : true
                    },
                    "newstatus": {
                        "inputType": "checkboxSimple",
                        "label": "Modification de status",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.newstatus != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.newstatus) : true
                    },
                    "newdepense": {
                        "inputType": "checkboxSimple",
                        "label": "Nouvelle depense",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.newdepense != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.newdepense) : true
                    },
                    "deletedepense": {
                        "inputType": "checkboxSimple",
                        "label": "Suppression depense",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.deletedepense != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.deletedepense) : true
                    },
                    "newfinancement": {
                        "inputType": "checkboxSimple",
                        "label": "Nouvelle financement",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.newfinancement != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.newfinancement) : true
                    },
                    "deletefinancement": {
                        "inputType": "checkboxSimple",
                        "label": "Suppression financement",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.deletefinancement != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.deletefinancement) : true
                    },
                    "newpayement": {
                        "inputType": "checkboxSimple",
                        "label": "Nouvelle payment",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.newpayement != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.newpayement) : true
                    },
                    "deletepayement": {
                        "inputType": "checkboxSimple",
                        "label": "Suppression payment",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.deletepayement != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.deletepayement) : true
                    },
                    "newtask": {
                        "inputType": "checkboxSimple",
                        "label": "Nouvelle tache",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.newtask != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.newtask) : true
                    },
                    "deletetask": {
                        "inputType": "checkboxSimple",
                        "label": "Suppression tache",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.deletetask != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.deletetask) : true
                    },
                    "checktask": {
                        "inputType": "checkboxSimple",
                        "label": "Check tache",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.checktask != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.checktask) : true
                    },
                    "addperson": {
                        "inputType": "checkboxSimple",
                        "label": "Ajout d'un contributeur",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.addperson != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.addperson) : true
                    },
                    "rmperson": {
                        "inputType": "checkboxSimple",
                        "label": "Suppression d'un contributeur",
                        "params": checkboxSimpleParams,
                        "checked": (typeof aapObject.formParent.paramsNotification != "undefined" && typeof aapObject.formParent.paramsNotification.rmperson != "undefined") ? JSON.parse(aapObject.formParent.paramsNotification.rmperson) : true
                    },
                },
                onLoads: {
                    onload: function() {
                        alignInput2(paramsNotif.jsonSchema.properties, "proposition", 6, 6, null, null, "Contributeur", "#3f4e58", "");
                    }
                },
                save: function(data) {
                    tplCtx = {};
                    tplCtx.id = aapObject.formParent["_id"]["$id"];
                    tplCtx.collection = "forms";
                    tplCtx.path = "paramsNotification";
                    tplCtx.value = {};
                    $.each(paramsNotif.jsonSchema.properties, function(k, val) {
                        tplCtx.value[k] = $("#" + k).val();
                    });
                    mylog.log("save tplCtx", tplCtx);

                    if (typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value(tplCtx, function(params) {
                            dyFObj.commonAfterSave(params, function() {
                                toastr.success("Parametre des notifications sauvegardée");
                                dyFObj.closeForm();
                                urlCtrl.loadByHash(location.hash);
                            });
                        });
                    }

                }
            }
        };

        jQuery(document).ready(function() {
            var checkboxSimpleParams = {
                "onText": trad.yes,
                "offText": trad.no,
                "onLabel": trad.yes,
                "offLabel": trad.no,
                "labelText": "label"
            };

            $(".aapnotification").off().on("click", function() {
                tplCtx.id = aapObject.formParent["_id"]["$id"];
                tplCtx.collection = "Forms";
                tplCtx.path = "paramsNotification";
                dyFObj.openForm(paramsNotif);
            });

            if (typeof sectionDyf != "undefined")
                sectionDyf.<?php echo $kunik ?>Params = {
                    "jsonSchema": {
                        "title": "Configurer votre section",
                        "description": "Personnaliser votre liste",
                        "icon": "fa-cog",

                        "properties": {

                            "title": {
                                "inputType": "text",
                                "label": "Titre",

                                values: sectionDyf.<?php echo $kunik ?>ParamsData.title
                            },
                            "description": {
                                "inputType": "textarea",
                                "label": "description",
                                "markdown": true,
                                value: sectionDyf.<?php echo $kunik ?>ParamsData.description
                            },
                        },
                        beforeBuild: function() {
                            uploadObj.set("cms", "<?php echo $blockKey ?>");
                        },
                        save: function(data) {
                            tplCtx.value = {};
                            $.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, function(k, val) {
                                tplCtx.value[k] = $("#" + k).val();
                                if (k == "parent")
                                    tplCtx.value[k] = formData.parent;

                                if (k == "items")
                                    tplCtx.value[k] = data.items;
                                mylog.log("andrana", data.items)
                            });
                            mylog.log("save tplCtx", tplCtx);

                            if (typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value(tplCtx, function(params) {
                                    dyFObj.commonAfterSave(params, function() {
                                        toastr.success("Élément bien ajouter");
                                        $("#ajax-modal").modal('hide');
                                        urlCtrl.loadByHash(location.hash);
                                    });
                                });
                            }

                        }
                    }
                };
            $(".btn-download-csv-financer").off().on("click", function() {

                var thisbtnpdf = $(this);
                thisbtnpdf.addClass("pdfloading");

                ajaxPost(
                    "",
                    baseUrl + '/costum/aap/exportcsvfinancer', {
                        champs: [],
                        query: aapObject.sections.proposition.activeFilters
                    },
                    function(response) {
                        console.log("eeee", response);
                        const {
                            Parser
                        } = json2csv;
                        const fields = response.heads;
                        const json2csvParser = new Parser({
                            fields,
                            delimiter: ";"
                        });
                        const csv = json2csvParser.parse(response.bodies);
                        var exportedFilenmae = 'export-financer.csv';
                        //var universalBOM = "\uFEFF";
                        var blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
                            type: 'text/csv;charset=utf-8;'
                        });
                        if (navigator.msSaveBlob) { // IE 10+
                            navigator.msSaveBlob(blob, exportedFilenmae);
                        } else {
                            var link = document.createElement("a");
                            if (link.download !== undefined) { // feature detection
                                // Browsers that support HTML5 download attribute
                                var url = URL.createObjectURL(blob);
                                link.setAttribute("href", url);
                                link.setAttribute("download", exportedFilenmae);
                                link.style.visibility = 'hidden';
                                document.body.appendChild(link);
                                link.click();
                                document.body.removeChild(link);
                            }
                        }
                    },
                    "html"
                );

                return false;

            });

            // Exporter l'appel à projet avec les financements
            $('.btn-download-csv-aap').off('click').on('click', function(__e) {
                var url = baseUrl + '/costum/aap/exportcsvfinancer/request/aap';
                var post = {
                    filters: aapObject.sections.proposition.activeFilters
                };

                var exportation = function(__data) {
                    var Parser = json2csv && json2csv.Parser ? json2csv.Parser : null;
                    if (Parser) {
                        var headers = __data['headers'];
                        var parser = new Parser({
                            fields: headers,
                            delimiter: ';'
                        });
                        var csv = parser.parse(__data['bodies']);
                        // Universal BOM
                        var blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
                            type: 'text/csv;charset=utf-8;'
                        });

                        // Télécharger le fichier
                        var filename = 'Exportation Appel à Projet - ' + moment().format('YYYYMMDD_HHmmss');
                        if (typeof navigator !== 'undefined' && typeof navigator.msSaveBlob !== 'undefined') {
                            navigator.msSaveBlob(blob, filename);
                        } else {
                            var link = document.createElement("a");
                            if (link.download !== undefined) { // feature detection
                                // Browsers that support HTML5 download attribute
                                var url = URL.createObjectURL(blob);
                                link.setAttribute("href", url);
                                link.setAttribute("download", filename);
                                link.style.visibility = 'hidden';
                                document.body.appendChild(link);
                                link.click();
                                document.body.removeChild(link);
                            }
                        }
                    } else {
                        toastr.error('Une erreur est survenue. Veuillez vérifier ou contacter l\'admin sur l\'existence du module <b>json2csv.Parser</b>');
                    }
                };

                ajaxPost(null, url, post, exportation);
            });


            if (typeof sectionDyf != "undefined") {
                $(".edit<?php echo $kunik ?>Params").off().on("click", function() {
                    $(this).html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>')
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = "allToRoot";
                    dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData);
                });
            }

            $(".copylinkstandalone").off().on("click", function() {
                var sampleTextarea = document.createElement("textarea");
                document.body.appendChild(sampleTextarea);
                sampleTextarea.value = baseUrl + "/costum/co/index/slug/" + costum.slug + "#answer.index.id.new.form." + aapObject.formParent["_id"]["$id"] + ".mode.w.standalone.true"; //save main text in it
                sampleTextarea.select(); //select textarea contenrs
                document.execCommand("copy");
                document.body.removeChild(sampleTextarea);
                toastr.success('lien standalone copié');
            });

            var ocecoLogo = "<?= $logoOceco ?>";
            var el_slug = "<?= $el_slug ?>";

            if (typeof sectionDyf != "undefined") {
                sectionDyf.<?php echo $kunik ?>Params = {
                    "jsonSchema": {
                        "title": "Configurer votre section",
                        "description": "Personnaliser votre section",
                        "icon": "fa-cog",

                        "properties": {
                            "titleDetails": {
                                "inputType": "text",
                                "label": "Titre detail",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.titleDetails
                            },
                            "titleList": {
                                "inputType": "text",
                                "label": "Titre liste",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.titleList
                            },
                            "titleMyActions": {
                                "inputType": "text",
                                "label": "Titre mes actions",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.titleMyActions
                            },
                            "titleObservatory": {
                                "inputType": "text",
                                "label": "Titre obrervatoire",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.titleObservatory
                            },
                            "titleKanban": {
                                "inputType": "text",
                                "label": "Titre Kanban",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.titleKanban
                            },
                            /*"bannerTitleStandalone": {
                                "inputType": "text",
                                "label": "Titre standalone",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.bannerTitleStandalone
                            },*/
                            "titleBtnNewProposition": {
                                "inputType": "text",
                                "label": "Titre boutton",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.titleBtnNewProposition
                            },
                            /*"homeTplAap": {
                                "inputType": "textarea",
                                "markdown": true,
                                "label": " Texte sur homeTplAap",
                                values: sectionDyf.<?php echo $kunik ?>ParamsData.homeTplAap
                            }*/
                        },
                        beforeBuild: function() {
                            uploadObj.set("cms", "<?php echo $blockKey ?>");
                        },
                        save: function(data) {
                            var tplCtx = {
                                value: {},
                                collection: "cms",
                                id: "<?= (string)$blockCms["_id"] ?>",
                                path: "allToRoot"
                            }
                            $.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, function(k, val) {
                                tplCtx.value[k] = $("#" + k).val();
                            });
                            mylog.log("save tplCtx", tplCtx);

                            if (typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value(tplCtx, function(params) {
                                    dyFObj.commonAfterSave(params, function() {
                                        toastr.success("Élément bien ajouter");
                                        $("#ajax-modal").modal('hide');
                                        dyFObj.closeForm();
                                        urlCtrl.loadByHash(location.hash);
                                    });
                                });
                            }

                        }
                    }
                };

                $(".edit<?php echo $kunik ?>Params").off().on("click", function() {
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = "allToRoot";
                    dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData);
                });
            }

            if (Object.keys(aapObject.formParent).length != 0)
                propostionObj.refreshNewCounter();
            if (notNull(contextDataAap) && contextDataAap.collection) {
                setTimeout(() => {
                    contextDataAap['type'] = contextDataAap['collection'];
                    //contextData['type'] = contextDataAap['collection'];
                }, 1000);
            }

        })
    </script>
<?php } ?>
