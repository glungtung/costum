
<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12" style="z-index: 1;">
    <ul class="breadcrumb">
        <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
        <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li>
        <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list ">Kanban</a></li>
    </ul>
</div>

<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12" style="z-index: 1;">
    <div class="aapconfigdiv">
        <div class="">
			<div class="view-mode">
				<a href="javascript:;" class="kanban-view margin-right-20 tooltips" data-value="kanban_project_list" data-toggle="tooltip" data-placement="bottom" data-original-title="Tous les projets">
                    <i class="fa fa-2x fa-lightbulb-o" aria-hidden="true"></i>
                </a>
				<a href="javascript:;" class="kanban-view margin-right-20 tooltips" data-value="kanban_follow_action" data-toggle="tooltip" data-placement="bottom" data-original-title="Suivi des actions">
                    <i class="fa fa-2x fa-tasks" aria-hidden="true"></i>
                </a>
				<a href="javascript:;" class="kanban-view margin-right-20 tooltips" data-value="kanban_financement" data-toggle="tooltip" data-placement="bottom" data-original-title="Financement">
                    <i class="fa fa-2x fa-euro" aria-hidden="true"></i>
                </a>
                <a href="javascript:;" class="kanban-view margin-right-20 tooltips" data-value="kanban_personal_action" data-toggle="tooltip" data-placement="bottom" data-original-title="Personnel">
                    <i class="fa fa-2x fa-user-o" aria-hidden="true"></i>
                </a>
			</div>
            <!-- <div class="dropdown" style="display:inline-flex;">
                <button id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="aap-breadrumbbtn dropdown-toggle" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.form"><i class="fa fa-plus-square-o"></i> Changer de vue <span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="javascript:;" class="detaaap" >Detailé</a></li>
                    <li><a href="javascript:;" class="minimalaap" >Minimaliste</a></li>
                </ul>
            </div> -->
            <button type="button" class="aapgetaapview aap-breadrumbbtn" data-url="slug.<?= $el_slug; ?>.formid.<?= $el_form_id ?>.aappage.form"><i class="fa fa-plus-square-o"></i>&nbsp;Nouvelle proposition</button>
			<?php if(@$el_configform["showMap"]){ ?>
				<button class="btn-show-hide-map margin-bottom-10 aap-breadrumbbtn" data-showmap="<?= @$elform["showMap"]==true ? "true" : "false" ?>"></button>
			<?php } ?>
		</div>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12" style="z-index: 1;">
    <div class="form-group col-md-4 col-sm-4 col-xs-12">
        
    </div>
    <div class="col-xs-12 kaban-container"></div>
</div>
<script>
    var $allAnswers= <?php echo json_encode(PHDB::findAndSortAndLimitAndIndex(
        Form::ANSWER_COLLECTION,
        array('form'=>$el_form_id,'answers'=>['$exists'=>true]),
        array("updated"=>-1)
    ))  ?>;
    var $slug = <?php echo json_encode($slug)  ?>;
    var $el_slug = <?php echo json_encode($el_slug)  ?>;
    var $el_configform = <?php echo json_encode($el_configform)  ?>;
    var $el_form = <?php echo json_encode($elform)  ?>;
    var $el = <?php echo json_encode($el)  ?>;
    var $el_form_id = <?php echo json_encode($el_form_id)  ?>;
    
    var kabanObj = {
        events:()=>{
            $this = kabanObj;
            $this.getAllAnswerAndLoadView("kanban_project_list");
            $(".kanban-view").off().on('click',function(){
                $this.getAllAnswerAndLoadView($(this).data("value"));
            })
        },
        getAllAnswerAndLoadView:function(view){
            ajaxPost(null,
                baseUrl+"/survey/answer/directory/source/"+costum.slug+"/form/"+$el_form_id,{     
                    searchType : ["answers"],
                    filters:{
                        'answers.aapStep1.titre':{'$exists':true},
                        'form': $el_form_id
                    },
                    sortBy :{"updated":-1}       
                },
                function(data){
                    $this.loadView(view,data.results); 
                },null,null,{async:false}
            )
        },
        loadView : (view,results)=>{
            var params =  {
                elSlug : $el_slug,	
                el: $el,
                slug : $slug,
                elConfigform: $el_configform,
                elForm: $el_form,
                elFormId : $el_form_id,
                allAnswers : results,
            };
            ajaxPost(".kaban-container", baseUrl+"/costum/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.kanban."+view, 
                params,
                function(){
                    
                },null,null,
                {
                    beforeSend : () => {},
                    async:false

                }
            );
        }
    }
    kabanObj.events();
    $(function(){
        $("[data-dismiss=modal]").on("click",function(){
            $this.getAllAnswerAndLoadView("kanban_project_list");
        })
    })
</script>