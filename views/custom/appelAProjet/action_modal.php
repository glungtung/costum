<?php
    $blockKey = $blockKey ?? sha1(date('Y-m-d'));
    $css_js = [
        '/plugins/jquery-confirm/jquery-confirm.min.css',
        '/plugins/jquery-confirm/jquery-confirm.min.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
<style>
    #action-modal<?= "$blockKey " ?> {
        z-index: 100001;
        background-color: rgba(0, 0, 0, .4);
    }

    #action-modal <?= "$blockKey " ?>* {
        font-family: Raleway, sans-serif;
        font-size: 1.6rem;
    }

    #action-modal<?= "$blockKey " ?>.fa {
        font-family: FontAwesome;
    }

    #action-modal<?= "$blockKey " ?>.project_name {
        color: #e33551;
    }

    .list-group-item:last-child {
        border-bottom: 1px solid #ddd !important;
    }

    .task_list .list-group-item,
    .contributor_list .list-group-item {
        font-size: 14px;
        padding: 10px 15px;
        font-weight: bold;
    }

    .task-group-container {
        padding: 0 !important;
    }

    .task_list .list-group-item label.done {
        text-decoration: line-through;
        font-weight: normal;
    }

    .task_list .list-group-item.active,
    .contributor_list .list-group-item.active {
        font-weight: bold;
    }

    .item-avatar {
        min-height: 72px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        background-color: #fff;
        color: #444;
        position: relative;
        z-index: 2;
        margin: -1px;
        padding: 16px;
        font-size: 16px;
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 10px;
    }

    .item-avatar.bordered {
        border: 1px solid #ddd;
    }

    #action-modal<?= "$blockKey " ?>img {
        -webkit-user-drag: none;
        margin: 0;
        padding: 0;
        border: 0;
        width: 50px;
        height: 50px;
        vertical-align: baseline;
        font: inherit;
        font-size: 100%;
        border-radius: 50%;
    }

    #detail_contribution_indicator<?= $blockKey ?> {
        color: #ffc900;
        display: block;
        text-align: center;
        gap: 5px;
    }

    .task-line-container {
        display: flex;
        flex-direction: row;
        align-items: center;
        margin: 10px 15px !important;
    }

    .no-task {
        padding: 0 !important;
    }

    .no-task span {
        display: block;
        margin: 10px 15px;
    }

    .executor {
        color: #51e335;
        font-weight: normal;
    }

    .executor.show {
        display: block;
    }

    .executor.hide {
        display: none;
    }

    #action-modal<?= "$blockKey " ?>tr > td:nth-child(n+2) {
        text-align: left;
        padding: 5px 10px;
    }

    .new_task_form_container {
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 10px;
    }

    .new_task_form_container > input {
        border: none;
        outline: none;
        flex: 1;
        font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
        font-weight: normal;
    }

    .new_task_form_container > button {
        border: none;
        outline: none;
        background: transparent;
    }

    .cpl,
    #cpl-select {
        background-color: #f9f9f9;
        padding: 9px 10px;
        display: flex;
        align-items: center;
        gap: 5px;
    }

    #cpl-search {
        position: relative;
    }

    #cpl-search > input,
    #cpl-search > input:focus {
        border: 1px solid #cacaca;
        border-radius: 20px;
        line-height: 1.8rem;
        padding: 10px 10px 10px 35px;
        width: 100%;
        outline: none;
    }

    #cpl-search > .fa {
        position: absolute;
        top: 50%;
        left: 10px;
        transform: translateY(-50%);
        color: #cacaca;
        font-size: 2rem;
    }

    .cpl-text {
        -moz-user-select: none;
        -webkit-user-select: none;
        user-select: none;
        cursor: default;
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .cpl-actions {
        display: flex;
        flex-direction: row;
        position: relative;
    }

    .checkmark {
        display: block;
        height: 25px;
        width: 25px;
        background-color: #fff;
    }

    .checkmark:after,
    .checkmark:before {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    input:checked ~ .checkmark:after {
        display: block;
    }

    input:indeterminate ~ .checkmark:before {
        display: block;
    }

    /* Style the checkmark/indicator */
    .checkmark:after {
        left: 10px;
        top: 7px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .checkmark:before {
        left: 50%;
        top: 50%;
        width: 15px;
        height: 1px;
        border: solid white;
        border-width: 0 0 3px 0;
        -webkit-transform: translateX(-50%) translateY(50%);
        -ms-transform: translateX(-50%) translateY(50%);
        transform: translateX(-50%) translateY(50%);
    }

    .checkmark.default:after,
    .checkmark.default:before {
        border-color: #000;
    }

    .cpl-check {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    .cpl-check ~ .checkmark {
        border: 3px solid #000;
    }

    .cpl-check:checked ~ .checkmark {
        background-color: #fff;
    }

    #action-status-switcher {
        margin-bottom: 20px;
    }
</style>
<div class="modal fade action-modal" id="action-modal<?= $blockKey ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" style="text-transform: unset;"><span class="project_name"><?= $parent_name ?></span> : <span
                        class="action_name"><?= $action_name ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="btn-group btn-group-justified" id="action-status-switcher">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default <?= in_array($status, ['closed', 'totest']) ? 'disabled' : '' ?>"
                                data-target="totest"><span class="fa fa-flask"></span> Passer au test
                        </button>
                    </div>
                    <?php
                        if ($status === 'closed') { ?>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" data-target="todo">
                                    <span class="fa fa-unlock-alt"></span>
                                    Réouvrir
                                </button>
                            </div>
                            <?php
                        } else { ?>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" data-target="closed">
                                    <span class="fa fa-thumbs-up"></span>
                                    Fermer
                                </button>
                            </div>
                            <?php
                        } ?>
                </div>
                <div class="item-avatar bordered">
                    <img src="" data-src="" class="visible">
                    <p style="margin: 0;">
                        Auteur : <span class="creator"></span>
                    </p>
                </div>
                <br>
                <span>Action à réaliser commençant le <span id="begining_date<?= $blockKey ?>"></span>
                    <!-- il y a <span id="month_begining_computed<?= $blockKey ?>"></span> -->
                </span>
                <table>
                    <tbody>
                    <tr>
                        <td><i class="fa fa-credit-card" aria-hidden="true"></i></td>
                        <td>Vous gagnerez <strong id="detail_credit<?= $blockKey ?>"></strong> crédit</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-calendar" aria-hidden="true"></i></td>
                        <td>Début le <span id="detail_full_begining<?= $blockKey ?>"></span></td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-clock-o" aria-hidden="true"></i></td>
                        <td>Durée <span id="detail_duration<?= $blockKey ?>"></span></td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-male" aria-hidden="true"></i></td>
                        <td>Il y a <span id="detail_contributor<?= $blockKey ?>"></span> participant<span
                                id="detail_contributor_multiple<?= $blockKey ?>">s</span></td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></td>
                        <td>Pour un besoin minimum de <span id="detail_contributor_min<?= $blockKey ?>"></span> participant<span
                                id="detail_contributor_min_multiple<?= $blockKey ?>">s</span></td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></td>
                        <td>Et un maximum de <span id="detail_contributor_max<?= $blockKey ?>"></span> participant<span
                                id="detail_contributor_max_multiple<?= $blockKey ?>">s</span></td>
                    </tr>
                    </tbody>
                </table>
                <snap id="detail_contribution_indicator<?= $blockKey ?>"></snap>
                <br/>
                <ul class="list-group task_list">
                    <li class="list-group-item active">Liste de tâches</li>
                    <li class="list-group-item new_task_form_container">
                        <span class="fa fa-tasks"></span>
                        <input type="hidden" value="-1">
                        <input type="text" placeholder="Tâche...">
                        <button type="submit">
                            <i class="fa fa-plus"></i>
                        </button>
                    </li>
                    <li class="list-group-item">
                        <span class="task_number_recap"></span>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 0%">
                                <span class="sr-only">0% Complete (success)</span>
                            </div>
                        </div>
                    </li>
                </ul>
                <br>
                <ul class="list-group contributor_list">
                    <li class="list-group-item active">Ils participent à cette action</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>
<script type="text/javascript">
    (function ($, W) {
        // Traductions
        trad['totest'] = 'À tester';

        // Début variable globale
        var _action_id = '<?= $action ?>';
        var _action_name = JSON.stringify(<?= json_encode($action_name) ?>);
        var _parent_slug = '<?= $parent_slug ?>'
        var _parent_name = JSON.stringify(<?= json_encode($parent_name) ?>);
        // Fin variable globale


        // Début fonctions

        /**
         * Récupère le détail d'une action
         * @param {string} __id identifiant de l'action à récupérer
         * @return {Promise}
         */
        function get_action(__id) {
            return new Promise(function (__resolve) {
                var url = baseUrl + '/costum/project/action/request/action_detail_data';
                var post = {
                    id: __id
                };
                ajaxPost(null, url, post, __resolve);
            });
        }

        /**
         * Récupère le détail d'une tâche
         * @param {Number} __range Ordre de la tâche à récupérer
         * @param {String} __action Identifiant de l'action où on pourra trouver la tâche
         * @return {Promise}
         */
        function get_task(__action, __range) {
            return new Promise(function (__resolve) {
                var url = baseUrl + '/costum/project/action/request/task';
                var post = {
                    action: __action,
                    task  : __range
                };
                ajaxPost(null, url, post, __resolve);
            });
        }

        /**
         * Ajout d'une nouvelle tâche
         * @param {string} __value Le nom de tâche
         * @return {Promise}
         */
        function add_new_task(__value) {
            return new Promise(function (__resolve) {
                var value = {
                    taskId   : (new Date()).getTime(),
                    userId   : userConnected['_id']['$id'],
                    task     : __value,
                    createdAt: moment().format(),
                    checked  : false
                };

                var setType = [
                    {
                        path: 'checked',
                        type: 'boolean'
                    },
                    {
                        path: 'createdAt',
                        type: 'isoDate'
                    }
                ];

                var params = {
                    id        : _action_id,
                    collection: 'actions',
                    path      : 'tasks',
                    arrayForm : true,
                    edit      : false,
                    format    : true,
                    value,
                    setType
                };

                dataHelper.path2Value(params, function (__response) {
                    if (__response['result'] && __response['result'] === true) {
                        __resolve(__response['text']);
                    }
                });
            });
        }

        /**
         * Modifier la tâche
         * @param {String} __value Le nom de tâche
         * @param {Number} __range Rang de la tâche dans la le tableau
         * @return {Promise}
         */
        function update_task(__value, __range) {
            return new Promise(function (__resolve) {
                get_task(_action_id, __range).then(function (__task) {
                    __task['task'] = __value;
                    var setType = [
                        {
                            path: 'checked',
                            type: 'boolean'
                        },
                        {
                            path: 'createdAt',
                            type: 'isoDate'
                        }
                    ];

                    var params = {
                        id        : _action_id,
                        collection: 'actions',
                        path      : 'tasks.' + __range,
                        value     : __task,
                        setType
                    };

                    dataHelper.path2Value(params, function (__response) {
                        if (__response['result'] && __response['result'] === true) {
                            __resolve(__response['text']);
                        }
                    });
                });
            });
        }

        /**
         * Charger la tâche ajoutée
         * @param {Object} __task L'objet task inséré dans la base de données
         * @param {HTMLElement} __input le champ de saisie pour saisir le nom de la tâche qu'on va reinitialiser
         */
        function post_add_task(__task, __input) {
            __input.value = '';
            var task_html = template_task(__task);

            // Envoyer une notification rocket chat pour la nouvelle tâche
            var url = baseUrl + '/survey/answer/rcnotification/action/newtask2';
            var post = {
                projectname: _parent_name,
                taskname   : __task['task'],
                actname    : _action_name,
                channelChat: _parent_slug,
            };
            ajaxPost(null, url, post);

            if ($('.task-group-container').length === 0) {
                $('.task_list .list-group-item:last-child').slideUp({
                    complete: function () {
                        $(this).remove();
                        $('.task_list').append(task_html).find('.list-group-item').last().hide().slideDown();
                    }
                });
            } else {
                $('.task_list').append(task_html).find('.list-group-item').last().hide().slideDown();
            }
            update_task_recap();
        }

        /**
         * Charger la tâche modifiée
         * @param {Object} __task L'objet task modifié dans la base de données
         * @param {Number} __range Numéro d'ordre de la tâche dans le tableau
         * @param {HTMLElement} __input le champ de saisie pour saisir le nom de la tâche qu'on va reinitialiser
         */
        function post_update_task(__task, __range, __input) {
            __input.value = '';
            var task_line_container = $('.task-line-container').eq(__range);
            var task_input_container = $('.new_task_form_container');

            task_line_container.find('.cpl-check').data('name', __task['task']);
            task_input_container.find('input[type=hidden]').val('-1');
            task_input_container.find('button[type=submit] .fa').removeClass('fa-edit').addClass('fa-plus');
            task_line_container.find('label:not(.checkmark)').fadeOut({
                complete: function () {
                    $(this).text(__task['task']).fadeIn();
                }
            })
        }

        /**
         * Mettre à jour la vue de récapitulation et la barre de progression
         */
        function update_task_recap() {
            get_task_recap(_action_id).then(function (__recap) {
                if (__recap['total'] === 0) $('.task_list .task_number_recap').text('0');
                else $('.task_list .task_number_recap').text(__recap['done'] + '/' + __recap['total']);
                $('.task_list .progress-bar').attr('aria-valuenow', __recap['done']);
                $('.task_list .progress-bar').attr('aria-valuemax', __recap['total']);
                $('.task_list .progress-bar').css('width', __recap['percentage'] + '%');
            });
        }

        /**
         * Récupère la récapitulation des tâches
         * @param {String} __action Identifiant de l'action où on doit récupérer les tâches
         * @return {Promise}
         */
        function get_task_recap(__action) {
            return new Promise(function (__resolve) {
                var url = baseUrl + '/costum/project/action/request/task_recap';
                var post = {
                    id: __action
                };
                ajaxPost(null, url, post, __resolve);
            });
        }

        /**
         * Créer un template html pour la liste des tâches
         * @param {Object} __task
         * @return {String}
         */
        function template_task(__task) {
            var task = {
                id     : __task['taskId'] ? __task['taskId'] : __task['id'],
                name   : __task['task'] ? __task['task'] : __task['name'],
                checked: typeof __task['checked'] !== 'undefined' ? __task['checked'] : false,
                user   : __task['userId'] ? __task['userId'] : __task['user']
            };

            var html =
                '<li class="list-group-item task-group-container">' +
                '   <div class="task-line-container">' +
                '       <div style="position: relative; margin-right: 10px;">' +
                '           <input class="cpl-check" type="checkbox" id="task' + task['id'] + '" data-id="' + task['id'] + '" data-name="' + task['name'] + '" ' + (task['checked'] ? 'checked="checked"' : '') + '>' +
                '           <label class="checkmark default" for="task' + task['id'] + '"></label>' +
                '       </div>' +
                '       <div style="flex: 1 1 0%;">' +
                '           <label for="task' + task['id'] + '" class="' + (task['checked'] ? 'done' : '') + '" style="user-select: none;">' + task['name'] + '</label>' +
                '           <span class="executor' + (task['checked'] && task['user'] ? ' show' : ' hide') + '"><span class="fa fa-check"></span> ' + (task['user'] && task['user']['name'] ? task['user']['name'] : '') + '</span>' +
                '       </div>' +
                '       <div class="dropdown">' +
                '           <button class="btn btn-default dropdown-toggle" type="button" id="action-list-' + task['id'] + '" data-toggle="dropdown">' +
                '               <span class="fa fa-cog"></span>' +
                '               <span class="caret"></span>' +
                '           </button>' +
                '           <ul class="dropdown-menu" role="menu" aria-labelledby="action-list-' + task['id'] + '">' +
                '               <li role="presentation"><a class="task-edit" role="menuitem" onclick="return false" tabindex="-1" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</a></li>' +
                '               <li role="presentation"><a class="task-delete" role="menuitem" onclick="return false" tabindex="-1" href="#"><i class="fa fa-trash" aria-hidden="true"></i> Supprimer</a></li>' +
                '           </ul>' +
                '       </div>' +
                '   </div>' +
                '</li>';
            return html;
        }

        // Fin fonctions

        $(function () {
            $('#action-modal<?= $blockKey ?>').on('shown.bs.modal', function () {
                get_action(_action_id).then(function (__action) {
                    var start_moment = moment(__action['startDate']);
                    var end_moment = moment(__action['endDate']);

                    // informations générales
                    $('#action-modal<?= $blockKey ?> .item-avatar img').attr('src', baseUrl + __action['creator']['image'])
                    $('#action-modal<?= $blockKey ?> .item-avatar .creator').text(__action['creator']['name']);
                    $('#begining_date<?= $blockKey ?>').text(start_moment.format('LL'));
                    $('#detail_credit<?= $blockKey ?>').text(__action['credits']);
                    $('#detail_full_begining<?= $blockKey ?>').text(start_moment.format('LLL'));
                    $('#detail_duration<?= $blockKey ?>').text(moment.duration(start_moment.diff(end_moment)).humanize());
                    $('#detail_contributor<?= $blockKey ?>').text(__action['contributor_number']);
                    if (__action['contributor_number'] > 1) $('#detail_contributor_multiple<?= $blockKey ?>').show();
                    else $('#detail_contributor_multiple<?= $blockKey ?>').hide();
                    $('#detail_contributor_min<?= $blockKey ?>').text(__action['min']);
                    if (__action['min'] > 1) $('#detail_contributor_min_multiple<?= $blockKey ?>').show();
                    else $('#detail_contributor_min_multiple<?= $blockKey ?>').hide();
                    $('#detail_contributor_max<?= $blockKey ?>').text(__action['max']);
                    if (__action['max'] > 1) $('#detail_contributor_max_multiple<?= $blockKey ?>').show();
                    else $('#detail_contributor_max_multiple<?= $blockKey ?>').hide();

                    // tâches
                    $('.task_list .task_number_recap').text('0');
                    var task_html = $('.task_list').html();
                    if (__action['tasks'].length > 0) {
                        var checkeds = __action['tasks'].filter(function (__task) {
                            return __task['checked']
                        });

                        $('.task_list .task_number_recap').text(checkeds.length + '/' + __action['tasks'].length);
                        $('.task_list .progress-bar').attr('aria-valuenow', checkeds.length);
                        $('.task_list .progress-bar').attr('aria-valuemax', __action['tasks'].length);
                        $('.task_list .progress-bar').css({
                            width: Math.ceil(checkeds.length * 100 / __action['tasks'].length) + '%'
                        });

                        task_html = $('.task_list').html();
                        $.each(__action['tasks'], function (__, __task) {
                            task_html += template_task(__task);
                        });
                    } else {
                        task_html += '<li class="list-group-item no-task"><span>Aucune tâche associée</span></li>';
                    }
                    $('.task_list').empty().html(task_html);

                    // participants
                    if (notEmpty(userConnected) && typeof __action['contributors'][userConnected['_id']['$id']] !== 'undefined') {
                        $('#detail_contribution_indicator<?= $blockKey ?>').empty().html('<span class="fa fa-clock-o"></span> Vous participez à cette action');
                    }
                    var contributor_html = $('.contributor_list').html();
                    var contributor_keys = Object.keys(__action['contributors']);
                    if (contributor_keys.length) {
                        $.each(contributor_keys, function (__, __id) {
                            var contributor = __action['contributors'][__id];
                            contributor_html +=
                                '<li class="list-group-item">' +
                                '    <div class="item-avatar" style="min-height: auto; padding: 0;">' +
                                '        <img src="' + baseUrl + contributor['image'] + '" class="visible" alt="' + contributor['name'] + '" style="width: 50px; height: 50px;">' +
                                '        <p style="margin: 0;text-overflow: ellipsis; overflow: hidden;">' +
                                '            <span class="creator">' + contributor['name'] + '</span>' +
                                '        </p>' +
                                '    </div>' +
                                '</li>';
                        });
                    } else {
                        contributor_html += '<li class="list-group-item no-task"><span>Aucun participant</span></li>';
                    }
                    $('.contributor_list').empty().html(contributor_html);
                });
            });
            $('#action-modal<?= $blockKey ?>').modal('show');

            // Modification de tâche
            $('.task_list').on('click', '.task-edit', function (__e) {
                if (notEmpty(userConnected)) {
                    const index = $('.task-edit').index(this);
                    const container = $(this).parents('.task-line-container');
                    const input_container = $('.new_task_form_container');

                    input_container.find('button[type=submit] .fa').removeClass('fa-plus').addClass('fa-edit');
                    input_container.find('input[type=text]').val(container.find('.cpl-check').data('name'));
                    input_container.find('input[type=hidden]').val(index);
                }
            });

            // Suppression d'une tâche
            $('.task_list').on('click', '.task-delete', function (__e) {
                var self = this;
                var index = $('.task-delete').index(this);

                if (notEmpty(userConnected)) {
                    $.confirm({
                        title  : 'Supprimer',
                        content: 'Supprimer cette tâche',
                        buttons: {
                            no : {
                                text    : 'Non',
                                btnClass: 'btn btn-default'
                            },
                            yes: {
                                text    : 'Oui',
                                btnClass: 'btn btn-danger',
                                action  : function () {
                                    var params = {
                                        id        : _action_id,
                                        collection: 'actions',
                                        path      : 'tasks.' + index,
                                        pull      : 'tasks',
                                        value     : null
                                    };

                                    dataHelper.path2Value(params, function (__response) {
                                        if (__response['result'] && __response['result'] === true) {
                                            $(self).parents('.list-group-item').slideUp({
                                                complete: function () {
                                                    $(this).remove();

                                                    // Quand il n'y a plus de sous-tâche
                                                    if ($('.task-group-container').length === 0) {
                                                        $('.task_list').append('<li class="list-group-item no-task" style="display:none;"><span>Aucune tâche associée</span></li>');
                                                        $('.list-group-item:last-child').slideDown();
                                                    }
                                                    update_task_recap();
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            });


            // Ajout ou modification d'une tâche à partir de l'input
            $('.task_list').on('keydown', '.new_task_form_container input[type=text]', function (__e) {
                var fa = $('.new_task_form_container button[type=submit] .fa');
                var self = this;

                if (__e['keyCode'] === 13 && this.value && notEmpty(userConnected)) {
                    // enregistrer une nouvelle tâche
                    if (fa.hasClass('fa-plus')) {
                        add_new_task(this.value).then(function (__task) {
                            post_add_task(__task, self);
                        });
                    } else
                        // modifier la tâche existante
                    if (fa.hasClass('fa-edit')) {
                        var range = $(this).prev().val();
                        update_task(this.value, range).then(function (__task) {
                            post_update_task(__task, range, self);
                        });
                    }
                }
            });

            // Ajout ou modification d'une tâche à partir du bouton
            $('.task_list').on('click', '.new_task_form_container button[type=submit]', function (__e) {
                var fa = $('.new_task_form_container button[type=submit] .fa');
                var self = document.querySelector('.new_task_form_container input[type=text]');
                if (self.value && notEmpty(userConnected)) {
                    // enregistrer une nouvelle tâche
                    if (fa.hasClass('fa-plus')) {
                        add_new_task(self.value).then(function (__task) {
                            post_add_task(__task, self);
                        });
                    } else
                        // modifier la tâche existante
                    if (fa.hasClass('fa-edit')) {
                        var range = $(self).prev().val();
                        update_task(self.value, range).then(function (__task) {
                            post_update_task(__task, range, self);
                        });
                    }
                }
            });

            // check d'une tâche
            $('.task_list').on('change', '.task-line-container .cpl-check', function (__e) {
                var self = this;
                var index = $('.task-line-container .cpl-check').index(this);
                var setType = [
                    {
                        path: 'checked',
                        type: 'boolean'
                    },
                    {
                        path: 'checkedAt',
                        type: 'isoDate'
                    },
                    {
                        path: 'createdAt',
                        type: 'isoDate'
                    }
                ];

                get_task(_action_id, index).then(function (__task) {
                    __task['checked'] = self.checked;
                    if (self.checked) {
                        __task['checkedAt'] = moment().format();
                        __task['userId'] = userConnected['_id']['$id'];
                    } else __task['checkedAt'] = null;

                    var params = {
                        id        : _action_id,
                        collection: 'actions',
                        path      : 'tasks.' + index,
                        value     : __task,
                        setType
                    }

                    dataHelper.path2Value(params, function (__response) {
                        if (__response['result'] && __response['result'] === true) {
                            var label = $(self).parents('.task-line-container').find('label:not(.checkmark)');
                            var user = $(self).parents('.task-line-container').find('.executor');
                            if (self.checked) {
                                label.addClass('done');
                                user.addClass('show').removeClass('hide').empty().html('<span class="fa fa-check"></span> ' + userConnected['name']);
                            } else {
                                label.removeClass('done');
                                user.addClass('hide').removeClass('show').empty().html('');
                            }
                            update_task_recap();

                            // Envoyer une notification rocket chat pour la tâche terminée
                            var url = baseUrl + '/survey/answer/rcnotification/action/checktask2';
                            var post = {
                                projectname: _parent_name,
                                taskname   : __task['task'],
                                actname    : _action_name,
                                channelChat: _parent_slug,
                            };
                            ajaxPost(null, url, post);
                        }
                    });
                });
            });

            $('#action-status-switcher').on('click', 'button:not(.disabled)', function () {
                const self = $(this);
                const target = self.get(0).dataset.target;

                const params = {
                    collection: 'actions',
                    id        : _action_id,
                    path      : 'status',
                    value     : target
                };
                dataHelper.path2Value(params, function (__response) {
                    // Object regroupant les différents status d'une action
                    const status = {
                        totest: 'info',
                        todo  : 'info',
                        closed: 'danger'
                    };

                    if (__response.result) {
                        const totest_dom = $('#action-status-switcher button[data-target=totest]');
                        const element_container = $(`.action-tracking[data-id=${_action_id}]`).parents('.corner-element');
                        const to_removes = [
                            'label-warning',
                            'label-info',
                            'label-success',
                            'label-danger'
                        ];

                        element_container.get(0).dataset.status = target;
                        const span_dom = element_container
                            .find('span.label.pull-right:first-child')
                            .text(trad[target]);
                        $.each(to_removes, function (__, __to_remove) {
                            span_dom.removeClass(__to_remove);
                        });
                        span_dom.addClass(`label-${status[target]}`);

                        switch (target) {
                            case 'totest':
                                self.addClass('disabled');
                                break;
                            case 'closed':
                                totest_dom.addClass('disabled');
                                self.get(0).dataset.target = 'todo';
                                self.empty().html(`<span class="fa fa-unlock-alt"></span> Réouvrir`);
                                break;
                            case 'todo':
                                totest_dom.removeClass('disabled');
                                self.get(0).dataset.target = 'closed';
                                self.empty().html(`<span class="fa fa-thumbs-up"></span> Fermer`);
                                break;
                        }
                        toastr.success('Le changement de status est un succès');
                    }
                });
            })
        });
    })(jQuery, window);
</script>