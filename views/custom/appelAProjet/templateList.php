<h1 class="text-center">Templates coform</h1>
<style type="text/css">
</style>
<div class="col-xs-12 col-sm-10 col-sm-offset-1">
	<div class="no-padding col-xs-12 text-left headerSearchContainer"></div>   

	<div id="filterCMS"></div>
	<div id="appCmsCurrent">
	</div>
	<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div>   
</div>
  
<script type="text/javascript">
	
	var paramsFilter= {
	 	container : "#filterCMS",
	 	loadEvent : {
	 		default : "pagination"
	 	},
	 	defaults : {
 			types : ["forms"],
 			fields : ["params","subForms","creator","created","parent","subType","hasStepValidations"],
 			notSourceKey: true,
 			filters : {
                        //"copyable" : {"$in" : ["true"]},
                        "type" : ["aapConfig", "template"]

                /*"$or": [
                    {
                        "copyable": {
                            "$in": ["true"]
                        }
                    },
                    {
                        "type": ["aapConfig", "template"]
                    }
                ]*/
 			}
 		}, 
 		results : {
 			dom : '#appCmsCurrent',
 			//renderView : "directory.cmsPanelHtml"
 		},
	 	filters : {
	 		text : true
	 	}
	};

    var paramsFiltertwo= {
        container : "#filterCMS",
        loadEvent : {
            default : "pagination"
        },
        defaults : {
            types : ["forms"],
            fields : ["subForms"],
            notSourceKey: true,
            filters : {
                "copyable" : {'$in' : ["true"]}
            }
        },
        results : {
            dom : '#appCmsCurrent',
            //renderView : "directory.cmsPanelHtml"
        },
        filters : {
            text : true
        }
    };
	var searchTemplate={};
	jQuery(document).ready(function() {
		searchTemplate = searchObj.init(paramsFilter);
		$(".headerSearchContainer .headerSearchright .btn-show-map").remove();
		$("#appCmsCurrent").mouseover(function(){
		    $("html").css("overflow-y", "hidden");
		});
        searchTemplate.results.render=function(fObj, results, data){
        		//alert(JSON.stringify(results));
				ajaxPost(fObj.results.dom, baseUrl+'/costum/aap/getviewbypath/path/costum.views.custom.appelAProjet.templateListCard', 
				{	
					results:Object.keys(results),
					context : {id:contextId , type : contextType}
				},
				function(){
					fObj.results.events(fObj);
				},null,null,
				{
					async : false,
					beforeSend : () => $(fObj.results.dom).html('<i class="fa fa-4x fa-spinner fa-spin"></i>')
				});
				if(Object.keys(results).length >= 0)
					$(fObj.results.dom).append(fObj.results.end(fObj));
				
			fObj.results.events(fObj);
		};
		// searchTemplate.paginatemplateLition.callBack = function(fObj){
		// 	//cmsBuilder.bindEvents();
		// };
		searchTemplate.search.init(searchTemplate,0);
		
	});
</script>