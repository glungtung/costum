<?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        '/js/default/editInPlace.js',
        //'/css/element/about.css'

    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@$this->appConfig["element"];
    if(isset($this->costum)){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["js"] == "about.css")
            array_push($cssJsCostum, '/css/'.$this->costum["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }
?>

<?php if($edit){?>
    <div class="container-fluid  margin">
        <button class="themeBtn editElement"><i class="fa fa-pencil"></i> Editer les informations</button>
    </div>
<?php } ?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1 contain-char">
    <div class="col-xs-12 no-padding">
        <div class="why-edit-box">
            <h3><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>Description courte</i>'; ?></h3>
            <p><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/book-icon.png" alt="Book Icon">
        </div>
    </div>
    <div class="col-xs-12 contain-char">
        <h2 class="title-section"><?php echo Yii::t("common","Caractérisques") ?></h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bhoechie-tab-menu hidden-xs">
            <div class="list-group">

                 <?php $tabSpec= array("manageModel"=>"Modèle de gestion", "state"=>"Avancement","spaceSize"=>"Superficie");
                 //var_dump($tabSpec);
                 $inin = 1;
                 foreach($tabSpec as $key => $val){

                 ?>
                        <a href="javascript:;" class="list-group-item  <?php echo $inin == 1 ? "active" : "" ?> text-center">
                            <?php echo $val; ?>
                        </a>
                    <?php $inin++;  } ?>

            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bhoechie-tab">
            <!-- flight section -->
            <?php
                //var_dump($tabSpec );
            $in = 1;
            foreach($tabSpec as $k => $v){
                $strTags="";
                $listCurrent=$this->costum["lists"][$k];
                if(!empty($element["tags"])){
                    foreach($element["tags"] as $tag){
                        if(in_array($tag, $listCurrent)){
                            $strTags.=(empty($strTags)) ? $tag : ", ".$tag;
                        }

                    }
                }
                if(empty($strTags)) $strTags="<i>Non spécifié</i>";
                ?>
                <div class="bhoechie-tab-content hidden-xs <?php echo $in == 1 ? "active" : "" ?> ">
                    <center>
                        <!--<h1 class="glyphicon glyphicon-map-marker" style="font-size:70px;color:#55518a"></h1>-->
                        <img class="icon-category" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/<?php echo $v; ?>.png" alt="<?php echo $v; ?>">
                        <h2 class="plan-title" ><?php echo $v; ?></h2>
                        <div class="plan-name" ><?php echo $strTags ?></div>
                    </center>
                </div>
                <div class="visible-xs col-xs-12 contentInformation padding-10">
                    <span class="title-char"><b><?php echo $v; ?></b></span><br/><span class="list-char"><?php echo $strTags ?></span>
                    </div>
            <?php  $in++;   }  ?>


        </div>
    </div>
    </div>

    <div class="col-xs-12 contain-char">
        <div class="item-tags">
            <div class="item-content-block">
                <!--<div class="block-title"><i class="fa fa-tags gradient-fill"></i> Tags</div>-->
                <h2 class="title-section">Tags</h2>
            </div>
            <div class="item-content-block tags">
             <?php
                if(@$element["tags"]){
                foreach ($element["tags"] as $key => $tag) { ?>
                    <a  href="#search?tags=<?php echo $tag; ?>"  class="btn-tag" ><?php echo $tag; ?></a>
                <?php }
                } ?>

            </div>
        </div>
    </div>
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-section2 contain-char">
    <div class="col-xs-12 no-padding module come-in">
        <div class="con1">
            <h2 class="title-section">Contact</h2>
            <div class="tags-info">
                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-at gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>E-mail</span>
                        <p><?php echo (@$element["email"]) ? $element["email"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-phone gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>Contact</span>
                        <p><?php echo (@$element["mobile"]) ? $element["mobile"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>

                    </div>
                    <div class="clear"></div>
                </div>

                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-globe gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>Site web</span>
                        <p>
                            <a href="<?php echo (@$element["url"]) ? $element["url"] : '#'; ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;"><?php echo (@$element["url"]) ? $element["url"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></a>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xs-12 no-padding">
        <!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->
        <?php $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    </div>
    <div class="col-xs-12 no-padding">
        <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
        <?php $this->renderPartial('co2.views.pod.newSocialNetwork', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    </div>

</div>
<?php $levelTer=isset($element["level"]) ? $element["level"] : ""; ?>
<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function () {
        controleAffiche();
    });


    function afficheMap(){
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
        };

        var paramsMapContent = {
            container : "divMapContent",
            latLon : contextData.geo,
            activeCluster : false,
            zoom : 16,
            activePopUp : false
        };


        mapAbout = mapObj.init(paramsMapContent);

        var paramsPointeur = {
            elt : {
                id : contextData.id,
                collection : contextData.type,
                geo : contextData.geo
            },
            center : true
        };
        mapAbout.addMarker(paramsPointeur);
        mapAbout.hideLoader();


    };

    jQuery(document).ready(function() {
        bindDynFormEditable();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });


        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });

        coInterface.bindLBHLinks();

        //edit element
        pageProfil.bindViewActionEvent();
        $(".editElement").click(function(){
            dyFObj.editMode=true;
            //uploadObj.set(type, id);
            uploadObj.update = true;
            dyFObj.currentElement={type : contextData.type, id : contextData.id};

            var levelTer= <?php echo json_encode($levelTer) ?> ;
            if (levelTer !==""){
                contextData.level=costum.lists.level[levelTer];
            }
            var dataElem = contextData;
            dataElem.type=contextData.typeElement;
            dataEdit=jQuery.extend(true, {},dataElem);
            var formType= (typeof contextData.category!="undefined" && contextData.category=="network") ? "reseau" : "organization";
            if(typeof contextData.typeOrga != "undefined")
                dataEdit.type=contextData.typeOrga;
            dyFObj.openForm(formType, null, dataEdit);
        });
        $("#menuRight").find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
        $("#menuRight").find("a").toggleClass("changelabel");
        $(".BtnFiltersLieux").hide();
    });

    $(document).ready(function() {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });

</script>
