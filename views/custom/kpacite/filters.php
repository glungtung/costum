<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}

.subDropdown{
	position:absolute;
	left:100%;
	/*background: var(--main2);*/
	display:none;
	/*overflow-y: scroll;*/
	/*top: 0;*/
     bottom: 0; 
    max-height: 100%;
    overflow: auto;
    width:100%;
}

.subDropdown:hover{
	display: block;
}

.subDropdown button{
	width: 100% !important;
    background-color: white !important;
    color: var(--main2) !important;
    border-top: solid 1px var(--main2) !important;
    text-align:left;
}

.subDropdown button:hover{
    filter: brightness(0.9);
}


.mega-menu-container button:hover + .subDropdown{
    display: block;
}

/*.mega-menu-container button:hover .{
    position: absolute;
}
*/
/*.mega-menu-container button:hover + .subDropdown{
	display:block;
}
}*/
.mega-menu-container > button:hover{
	filter: brightness(0.7);
}	


/*.parent-button{
	display:block;
}

.subDropdown:hover{
	display:block;
}

.subDropdown:hover button{
	display:block;
	color:var(--main2);
}*/


.dropdown-menu.dropdown-menu-large{
	position:fixed !important;
    overflow: scroll !important;
    width: 94% !important;
    max-height: -webkit-fill-available;
    left : 65px !important;
    top:115px !important;
    height: 70%;
    padding:10px !important;
}    

.dropdown-menu-large > .col > div{
	    float: left;
    /* background-color: #c8caf1; */
    margin-bottom: 20px;
    height: -webkit-fill-available;
    padding: 3px;
    border: solid 2px #c8caf1;
}

#filters-nav .dropdown .dropdown-menu-large button{
	background-color: var(--main1);
	color:black;
	text-align: left;
}


#filters-nav .dropdown .dropdown-menu-large button:hover{
	background-color: #c8caf1;
}

</style>

<script type="text/javascript">
	

	var pageApp=<?php echo json_encode(@$page); ?>;
	var mapShown= (pageApp=="search") ? true : false;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	options : {
	 		tags : {
	 			verb : '$all'
	 		}
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true,
            map : {
            	active : mapShown
            }
        },
        header:{
	 		options:{
	 			left:{
	 				classes :"col-xs-6 no-padding",
	 				group:{
						count : false
					}
	 			},
	 			right:{
	 				classes :"col-xs-5 no-padding",
	 				group:{
						map : true
					}
	 			}
	 		}
	 	},	
	 // 		views : {
	 // 			map : function(fObj,v){
		// 			return  '<button class="btn-show-map-search pull-right bg-main1" style="" title="yoyo" alt="yoyoyo">'+
		// 						'<i class="fa fa-map-marker"></i> heyyou</button>';
		// 		}
		// 	}
		// },	
	 	defaults : {
	 		indexStep : 0,
	 		types : ["organizations"]
	 	},
	 	filters : {
	 		scope :true,
	 		scopeList : {
	 			params : {
	 				countryCode : ["FR"], 
	 				level : ["3"],
	 			}
	 		},
	 		state : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Etat du projet",
	 			// action : "tags",
	 			event : "tags",
	 			list :  costum.lists.state
	 		}
	 	}
	};

	if (typeof upperLevelId !="undefined")
		paramsFilter.filters.scopeList.upperLevelId=upperLevelId;

	//function lazyFilters(time){
	  //if(typeof searchObj != "undefined" )
	    //filterGroup = searchObj.init(paramsFilter);
	  //else
	    //setTimeout(function(){
	      //lazyFilters(time+200)
	    //}, time);
	//}
var filterSearch={};
	jQuery(document).ready(function() {


		  filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l'annuaire");
		if(!($("#menuRight").find("a").hasClass("changelabel"))){
			$("#menuRight").find("a").addClass("changelabel")
		}
  
		$("#menuRightmapContent").hide();	
		$(".BtnFiltersLieux").show();

	
		
	});

</script>



