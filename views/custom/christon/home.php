<style>
	li.mb-2{
    margin-bottom: 10px !important;
}
h2{
margin-bottom:10px !important;
}

.text-center{
text-align: center
}

.container{
width: 100% !important;
}

.resume-section-title{
color: #3a226c !important;
}

header.resume-header{
background-image : url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/christon/banner.png);
background-size: cover;
color: #fff;
padding: 50px;
}

.progress {
display: flex;
height: 1rem;
overflow: hidden;
font-size: 0.75rem;
background-color: #e9ecef;
border-radius: 0.25rem;
}

.progress-bar {
display: flex;
flex-direction: column;
justify-content: center;
color: #fff;
text-align: center;
white-space: nowrap;
background-color: #3a226c;
transition: width 0.6s ease;
}

#vertical{
display: none !important;
}

.fa{
padding: 8px;
border-radius: 40px;
border: 2px solid #fff;
font-size: 15pt;
}

.text-right{
text-align: right;
}

.icons{
font-size: 15pt
}

.badge-primary {
color: #fff;
background-color: #3a226c;
}
</style>

<?php
	// get CV document from collections
	$cv = PHDB::find("cv");
	$manage = $cv["5f7f60a3ab58c781870517a7"];
	
?>

<?php
	if(isset($manage) || count($manage)!=0){
	
?>

<header class="resume-header pt-md-0">
	<div class="row">
		<div class="col-md-3">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/christon/<?= $manage["person"]["avatar"]?>" alt="" width="150">
		</div>
		<div class="col-md-9">
			<h2 class="media-heading"><?= $manage["person"]["nom"]." ".$manage["person"]["prenom"] ?></h2>
			<br/>
			<h4 class="title mb-3"><?= $manage["person"]["fonction"] ?></h4>
			<br/>
			<div class="icons">
				<i class="fa fa-envelope mr-2" data-fa-transform="grow-3"></i> <?= $manage["person"]["email"]?> <br><br>
					<i class="fa fa-phone mr-2" data-fa-transform="grow-6"></i> <?= $manage["person"]["telephone"]?> <br>
				<div class="text-right">
					<span class="fa-container text-center mr-2"><i class="fa fa-linkedin"></i></span>&nbsp; &nbsp; &nbsp;
					<span class="fa-container text-center mr-2"><i class="fa fa-github-alt"></i></span>&nbsp; &nbsp; &nbsp;
					<span class="fa-container text-center mr-2"><i class="fa fa-codepen"></i></span> &nbsp; &nbsp; &nbsp;
					<span class="fa-container text-center mr-2"><i class="fa fa-globe"></i></span>
				</div>
			</div>
			
		</div>
	</div>
</header>
	
<article class="resume-wrapper text-center position-relative bg-secondary">
<div class="resume-wrapper-inner mx-auto text-left shadow-lg" style="padding-left:50px; padding-right:50px">
	<div class="resume-body">
		<div class="row">
			<div class="col-lg-8" style="padding-right:50px">
				<section class="resume-section experience-section mb-5">
					<h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">RÉALISATION</h2>
					<br>
					<div class="row">
					<?php foreach ($manage["realisations"] as $value) { ?>
					<div class="resume-section-content">
						<div class="resume-timeline position-relative">
							<article class="resume-timeline-item position-relative pb-5">
								<div class="resume-timeline-item-desc text-light">
									<?php foreach ($value["portfolio"] as $v) {?>
									<div class="col-md-5">
										<div class="thumbnail">
											<img width="100%" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.'/images/christon/'.$v ?>" alt="">
											<div class="caption">
												<h4 class="resume-position-title font-weight-bold mb-1"><?= $value["poste"] ?></h3>
												<div class="resume-company-name ml-auto"><?= $value["entreprise"] ?></div>
												<h6 class="resume-timeline-item-desc-heading font-weight-bold">Technologies utilisés:</h4>
												<ul class="list-inline">
													<?php foreach ($value["technologies"] as $v) {
														echo '<li class="list-inline-item"><span class="badge badge-primary badge-pill">'.$v.'</span></li>';
													} ?>
												</ul>
											</div>
										</div>
									<?php } ?>
									</div>
								</div>
							</article>
						</div>
					</div>
					<?php } ?>
					</div>
				</section>
				<section class="resume-section experience-section mb-5">
					<h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Compétitions</h2>
					<div class="resume-section-content">
						<br>
							<?php foreach ($manage["competition"] as $value) {?>
								<div class="row">
									<div class="col-md-3">
										<img height="60" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.'/images/christon/'.$value["logo"] ?>" alt="">
									</div>
									<div class="col-md-9">
										<h4><?= $value["titre"] ?></h4>
										<p>Organisé par <?= $value["organisateur"] ?></p>
									</div>
								</div>
								<br>
							<?php } ?>
					</div>
				</section>
			</div>

			<div class="col-lg-4">
				<section class="resume-section skills-section mb-5">
					<h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">compétence technique</h2>
					<br>
					<div class="resume-section-content">
						<div class="resume-skill-item">
							<h4 class="resume-skills-cat font-weight-bold">Frontend</h4>
							<ul class="list-unstyled mb-4">
							<?php foreach($manage["frontend"] as $value){ ?>
								<li class="mb-2">
									<div class="resume-skill-name"><?= $value["language"] ?></div>
									<div class="progress resume-progress">
										<div class="progress-bar theme-progress-bar-dark" role="progressbar" style="width: <?= $value["note"]*100/20 ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</li>
							<?php } ?>
								
							</ul>
						</div><!--//resume-skill-item-->
						<br>
						<div class="resume-skill-item">
							<h4 class="resume-skills-cat font-weight-bold">Backend</h4>
							<ul class="list-unstyled">
								<?php foreach($manage["backend"] as $value){ ?>
									<li class="mb-2">
										<div class="resume-skill-name"><?= $value["language"] ?></div>
										<div class="progress resume-progress">
											<div class="progress-bar theme-progress-bar-dark" role="progressbar" style="width: <?= $value["note"]*100/20 ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</li>
								<?php } ?>
							</ul>
						</div><!--//resume-skill-item-->
						<br>
						<div class="resume-skill-item">
							<h4 class="resume-skills-cat font-weight-bold">Autres</h4>
							<ul class="list-inline">
								<li class="list-inline-item"><span class="badge badge-light">Java</span></li>
								<li class="list-inline-item"><span class="badge badge-light">CodeIgniter</span></li>
								<li class="list-inline-item"><span class="badge badge-light">Symfony</span></li>
								<li class="list-inline-item"><span class="badge badge-light">Android</span></li>
								<li class="list-inline-item"><span class="badge badge-light">Ionic</span></li>
								<li class="list-inline-item"><span class="badge badge-light">leaflet</span></li>
								<li class="list-inline-item"><span class="badge badge-light">Git</span></li>
								<li class="list-inline-item"><span class="badge badge-light">MongoDB</span></li>
								<li class="list-inline-item"><span class="badge badge-light">MySQL</span></li>
								<li class="list-inline-item"><span class="badge badge-light">PostgresSQL</span></li>
								<li class="list-inline-item"><span class="badge badge-light">SQLite</span></li>
							</ul>
						</div><!--//resume-skill-item-->
					</div><!--resume-section-content-->
				</section><!--//skills-section-->
				<section class="resume-section education-section mb-5">
					<h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Education</h2>
					<div class="resume-section-content">
						<ul class="list-unstyled">
						<?php 
							foreach($manage["formations"] as $value){
						?>
							<li>
								<div class="resume-degree font-weight-bold"><b><?= $value["titre"] ?></b></div>
								<div class="resume-degree-org text-dark"><?= $value["lieu"] ?></div>
								<div class="resume-degree-time"><?= $value["periode"] ?></div>
								<br>
							</li>

						<?php } ?>
						</ul>
					</div>
				</section><!--//education-section-->
				<!--section class="resume-section language-section mb-5">
					<h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Language</h2>
					<div class="resume-section-content">
						<ul class="list-unstyled resume-lang-list">
						<?php 
							foreach($manage["langues"] as $value){
						?>
							<li class="mb-2"><span class="resume-lang-name font-weight-bold"><?= $value["langue"] ?></span> <small class="text-muted font-weight-normal">(<?= $value["note"] ?>)</small></li>
							<?php 
							}
						?>
						</ul>
					</div>
				</section--><!--//language-section-->
				
			</div>
		</div><!--//row-->
	</div><!--//resume-body-->
</div>
</article>
<?php }else{ ?>

	<h1 class="text-center">
		Le CV de Christon n'est pas dans la base de données
	</h1>

<?php } ?>