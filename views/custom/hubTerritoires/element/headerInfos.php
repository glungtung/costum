<?php $bannerConfig=$this->appConfig["element"]["banner"]; ?>
<style>
	#uploadScropResizeAndSaveImage i{
		position: inherit !important;
	}
	#uploadScropResizeAndSaveImage .close-modal .lr,
	#uploadScropResizeAndSaveImage .close-modal .lr .rl{
		z-index: 1051;
		height: 75px;
		width: 1px;
		background-color: #2C3E50;
	}
	#uploadScropResizeAndSaveImage .close-modal .lr{
		margin-left: 35px;
		transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-webkit-transform: rotate(45deg);
	}
	#uploadScropResizeAndSaveImage .close-modal .rl{
		transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-webkit-transform: rotate(90deg);
	}
	#uploadScropResizeAndSaveImage .close-modal {
		position: absolute;
		width: 75px;
		height: 75px;
		background-color: transparent;
		top: 25px;
		right: 25px;
		cursor: pointer;
	}
	.blockUI, .blockPage, .blockMsg{
		padding-top: 0px !important;
	} 

	#banner_element:hover{
	    color: #2c407a;
	    background-color: white;
	    border:1px solid #2c407a;
	    border-radius: 3px;
	    margin-right: 2px;
	}
	a.btn-favorite-link{
		display: none;
	}
	#banner_element{
	    background-color: #2c407a;
	    color: white;
	    border-radius: 3px;
	    margin-right: 2px;
	    display:none;
	}
	.header-address{
		font-size: 14px;
		padding-left: 5px;
	}
	#contentBanner img{
		min-height:280px;
	}
	.badgePH{
		padding: 10px;
	}
	#contentBanner{
		min-height: 280px;
	}
	.a-icon-banner{
		font-size: 25px;
	}

	.link-banner{
		padding: 0px 5px 0px 5px;
		color: white;
	}
	.bg-dark-green{
    background-color: #4caf50;
}
	.link-banner{
		max-width: 80px;	
	}
	.link-banner:hover{
		color: #2c407a;
	}
	.title-link-banner{
		font-size: 13px;
	}
	@media (min-width: 768px) and (max-width: 991px){
		.link-banner {
		    padding: 15px 6px;
		}
		.tag-list li.adress {
			color: #3f4e58;
		}
	}
	#btnHeaderEditInfos {
    padding: 0px!important;
    margin-top: 5px;
    margin-left: 0px!important;
	}
	.section-badges .dropdown-menu>li>a {
    color: #93C020 !important;
	}
	.section-badges .dropdown-menu>li>a:hover {
    background: #93C020 !important;
    color: white!important;
	}

	.section-badges .dropdown-menu {
		padding: 0px;
	}
	.icon-before-name{
    border-radius: 50%;
    width: 28px;
    height: 28px;
    font-size: 18px;
    color: white;
    text-align: center;
    line-height: 28px;
    margin-right: 10px;
    display: inline-block;
}
.event-infos-header small.pull-right {
    float: none!important;
    margin-left: 10px;
    color: #4caf50!important;
}
.event-infos-header .text-red{
	color: #4caf50!important;
}
#paramsMenu li ul li.text-left {
    text-align: left;
}
.visible-xs .event-infos-header h3 small{
	color: #fff;
}
.visible-xs .event-infos-header span.uppercase{
	color: #fff;
}
.boxBtnLink a.menu-linksBtn, .boxBtnLink .nav.navbar-nav, .boxBtnLink .dropdow, .AddFriendBtn {
    width: auto;
}
.AddFriendBtn a.menu-AddFriendBtn {
	padding: 9.3px;
    border: none;
    text-transform: none;
    margin: 0px;
}
.statuInvitation {
	padding: 10px;
	/*color: #2c407a;*/
}

@media (max-width: 767px){
	ul>li>ul.dropdown-menu{
	right: 0px;
    left: inherit;
    top: 40px;
	}
	.navbar-nav .open .dropdown-menu {
    position: absolute!important;
    background-color: #fff;
	}
	.section-badges .boxBtnLink, .section-badges .AddFriendBtn{
		border-radius: 30px;
	}
}

/***** BUTTON INVITER *******/
#col-banner .containInvitation{
	 top: 60px;
    position: absolute;
    z-index: 1;
    right: 15px;
    margin-top: 10px;
    background-color: #3f4e58;
    opacity: 0.8;
    border-radius: 15px;
    color: white;
    text-align: center;
}
#col-banner .containInvitation a.lbh {
	color: #2c407a;
}

#col-banner .containInvitation a.btn {
    font-weight: 500;
    font-size: 14px;
    letter-spacing: 1px;
    display: inline-block;
    padding: 9px 22px!important;
    border-radius: 50px!important;
    line-height: 1;
    color: #fff;
    /*background-color: transparent*/
}
#col-banner .containInvitation a.btn-accept {
	    border: 2px solid #1bbca3;
}
#col-banner .containInvitation a.btn-accept:hover {
	background: #fff;
}
#col-banner .containInvitation a.btn-refuse{
	border: 2px solid #E33551;
}
#col-banner .containInvitation a.btn-refuse:hover{
	background: #fff;
}
@media (max-width: 767px){
	#col-banner .containInvitation{
		display: none;
	}
}


</style>

<?php 
$thumbAuthor =  @$element['profilImageUrl'] ? 
Yii::app()->createUrl('/'.@$element['profilImageUrl']) : "";?>
		<?php 
			if (@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])){	
				$imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'1"
				src="'.Yii::app()->createUrl('/'.$element["profilBannerUrl"]).'">';
			}else{
			if(isset($pageConfig["banner"]["img"]) && !empty($pageConfig["banner"]["img"]))
				$url=Yii::app()->getModule( "costum" )->assetsUrl.$pageConfig["banner"]["img"];
			else if(in_array($element["collection"], [Event::COLLECTION, Project::COLLECTION, Person::COLLECTION, Organization::COLLECTION]))
				$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/'.$element["collection"].'.png';
			else		
				$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
			} 
		?>
	</div>

	
	<div class="btn-group section-badges pull-right no-padding" >
		
		
	<!-- Btn Link -->	
		<div class="pull-left no-padding" style="">
		<?php  
 			if(@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]){ ?>
			<div class="pull-left boxBtnLink no-padding bg-dark-green" style="border-radius: 30px; display: flex;">
        	<?php  echo $this->renderPartial('co2.views.element.menus.links',
    			array(  "linksBtn" => $linksBtn,
    					"element"   => $element,
    					"openEdition" => $openEdition ) 
    			); 
    		?>
			</div>
		<?php } ?>
		
		</div>
<!-- END Btn Link -->		
		
		
 			<?php if (!empty($element["preferences"]["private"])) { ?>
						<div class="badgePH pull-left hidden-xs" data-title="Only reserved to the community">
							<a href="javascript:;" class="editConfidentialityBtn">
							<span class="pull-right tooltips text-red" data-toggle="tooltip" data-placement="bottom" 
									title="<?php echo Yii::t("common","Private") ?>" >
								<i class="fa fa-lock" style="font-size: 17px;"></i> 
								
							</span>
							</a>
						</div>
					<?php } ?>

			<?php if(!empty($element["preferences"]["isOpenData"])){?>
				<div class="badgePH pull-left hidden-xs" data-title="OPENDATA">
					<?php if($edit){ ?>
						<a href='javascript:;' class="openConfidentialSettings pull-left tooltips" data-toggle="tooltip" data-placement="bottom" 
					title="<?php echo Yii::t("common","Open data") ?>"><?php } ?>
						<span class="fa-stack opendata" style="margin-top: -28px!important;height: auto;">
							<i class="fa fa-database main fa-stack-1x" style="font-size: 20px;"></i>
								<i class="fa fa-share-alt  mainTop fa-stack-1x text-white" 
								style="text-shadow: 0px 0px 2px rgb(15,15,15);"></i>
									</span> 
								<?php if($edit) { ?></a><?php } ?>
							</div>
					<?php //} 
					} ?>

					<?php if (!empty($element["preferences"]["isOpenEdition"])) { ?>
						<div class="badgePH pull-left hidden-xs" data-title="OPENEDITION">
							<?php if($edit){ ?>
							<a href="javascript:;" class="openConfidentialSettings btn-show-activity">
							<?php } ?>
							<span class="pull-right tooltips" data-toggle="tooltip" data-placement="bottom" 
									title="<?php echo Yii::t("common","Open edition") ?>">
								<i class="fa fa-creative-commons"  style="font-size: 17px;"></i> 
								
							</span>
							<?php if($edit){ ?></a><?php } ?>
						</div>
					<?php } ?>


<!-- Edite les information -->
		<?php if( ( $edit || $openEdition ) && !empty(Yii::app()->session["userId"])){ 
			$href=(isset($bannerConfig["editButton"]) 
				&& isset($bannerConfig["editButton"]["dynform"]) 
				&& $bannerConfig["editButton"]["dynform"]) ? "javascript:dyFObj.editElement('".Element::getControlerByCollection($element["collection"])."', '".(string)$element["_id"]."');" : "javascript:;";
			?>
			<div class="badgePH pull-left hidden-xs" data-title="Only reserved to the community">
				<a href="<?php echo $href ?>" class="editConfidentialityBtn" id="btnHeaderEditInfos">
					<span class="pull-right tooltips " data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common", "Edit information") ?>" >
						<i class="fa fa-pencil"  style="font-size: 17px;"></i> 
					</span>
				</a>	
			</div>

		<?php } ?>
<!-- END Edite les information -->

	</div>

<!-- BUTTON ACCEPET INVITATION AND REJETE INVITATION  
	<div class="section-btn-ivite pull-right">
		<a href="#" class="btn-invite-accept animated fadeInUp">
			Accepter
		</a>
		<a href="#" class="btn-invite-accept animated fadeInUp">
			Refuser
		</a>
	</div>	
	-->
	
		<div class="animated bounceInRight">
			<?php if (@Yii::app()->session["userId"] && Yii::app()->session["userId"] != $invitedMe["invitorId"]) { ?>
				<?php echo $this->renderPartial('co2.views.element.menus.answerInvite',
		    			array(  "invitedMe"      => $invitedMe,
		    					"element"   => $element
		    					) 
		    			); 
		    }else if (@Yii::app()->session["userId"] && Yii::app()->session["userId"] == $invitedMe["invitorId"]) {?>
				<div class="containInvitation">
					<div class="statuInvitation">
						<?php echo Yii::t("common", "Friend request sent") ?>
					</div>
				</div>
		    <?php }?>	
		</div>
	
	

	
	<div class="col-xs-12 col-sm-12 col-md-12  hidden-xs contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">

	    	<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10 text-white pull-right">
				<div class="col-xs-12 col-sm-7 col-md-7 text-left">
					<?php if (@$element["status"] == "deletePending") { ?> 
						<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
					<?php } ?>
					<h4 class="text-left">
						<span id="nameHeader">
							<span class="bg-<?php echo $iconColor; ?> pull-left icon-before-name">
								<i class="fa fa-<?php echo $icon; ?> text-center"></i>
							</span> 
							<div class="name-header"><?php echo @$element["name"]; ?></div>
						</span>
						<?php if($element["collection"]==Event::COLLECTION && !empty($element["type"])){ 
								$typesList=Event::$types;
						?>
							<span id="typeHeader" class="margin-left-10 pull-left">
								<i class="fa fa-x fa-angle-right pull-left"></i>
								<div class="type-header pull-left">
							 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
							 	</div>
							</span>
						<?php } ?>
					</h4>

					<div class="col-md-12 hidden-sm col-lg-12 hidden-xs no-pading">
				        <ul class="tag-list no-padding">
				        	<?php if(!empty($element["address"]["addressLocality"])){ ?>
				        	<li class="tag adress">
				        		<?php
				        		echo "<i class='fa fa-map-marker'></i> ";
									echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
									echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
									echo $element["address"]["addressLocality"];
								?>
				        	</li>
				        	<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden" ); ?>
										<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO ; ?>" style="font-size: 10px;line-height: 35px; color: #2c407a!important;">
											<i class="fa fa-circle-o"></i>
										</span>
									
								<?php } 
				            
				                if(!empty($element["tags"])){
				                    foreach ($element["tags"]  as $key => $tag) {
				                        echo '<li class="tag" style="margin-top: 3px;"><a href="#search?text=#'.$tag.'" class="text-green bold" style="border: 0px!important;padding-left: 1px!important;padding-right: 1px!important;background-color: transparent;"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</a></li>';
				                         }
				                }else{
				                    
				                } ?>
				        </ul>
				    </div>
				</div>	
				<div class="col-xs-12 col-sm-5 col-md-5 text-right">
					<?php if (($edit==true || $openEdition==true) && @Yii::app()->session["userId"]){ ?>
						<ul class="nav navbar-nav pull-right" id="paramsMenu">
							<li class="dropdown dropdown-profil-menu-params">
								<a href="javascript:;" type="button" class="pull-right text-center link-banner">
									<i class="fa fa-ellipsis-v a-icon-banner"></i> 
									<!--<span class="title-link-banner"></span>-->
						  		</a>
						  		<ul class="dropdown-menu arrow_box menu-params">
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-view="settings">
											<i class="fa fa-cogs"></i> <?php echo Yii::t("common", "My parameters") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="qrCode">
											<i class="fa fa-qrcode"></i> <?php echo Yii::t("common", "QR Code") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="chatSettings">
											<i class="fa fa-comments"></i> <?php echo Yii::t("common", "Chat settings") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-view="md">
											<i class="fa fa-file-text-o"></i> <?php echo Yii::t("common", "Markdown version") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="mindmap">
											<i class="fa fa-sitemap"></i> <?php echo Yii::t("common", "Mindmap view") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="downloadData">
											<i class="fa fa-download"></i> <?php echo Yii::t("common", "Download data") ?>
										</a>
									</li>
				                	<li class="text-left">
										<a href="javascript:;" class="bg-white ssmla " data-action="printOut">
											<i class="fa fa-print"></i> <?php echo Yii::t("common", "Print out") ?>
										</a>
									</li>
				                </ul>
				            </li>
				        </ul>

					
						<a href="javascript:;" class="ssmla pull-right text-center link-banner" data-action="chat">
							<i class="fa fa-comments a-icon-banner"></i> 
							<span class="title-link-banner hidden-sm"><br><?php echo Yii::t("common", "Chat") ?></span>
						</a>
					
						<a href="javascript:;" class="ssmla pull-right text-center link-banner" data-action="create">
					  		<i class="fa fa-plus-circle a-icon-banner"></i>
					  		<span class="title-link-banner hidden-sm"><br><?php echo Yii::t("common", "Create") ?></span> 
					  	</a>
							<?php 
						}
					?>
					<!--
					<a href="#" class="pull-right text-center link-banner">
						<i class="fa fa-plus-circle a-icon-banner"></i> <br>
						<span class="title-link-banner">Ajouter</span>
					</a>
					
					<a href="javascript:;" class="pull-right text-center link-banner">
						<i class="fa fa-user-plus a-icon-banner"></i> <br>
						<span class="title-link-banner">Inviter</span>
					</a>
					-->

					<?php 
						$urlLink = "#element.invite.type.".$element["collection"].".id.".(string)$element["_id"];
						$inviteLink = "people";
						if ($element["collection"] == Organization::COLLECTION) 
							$inviteLink = "members";
						else if ($element["collection"] == Project::COLLECTION ) 
							$inviteLink = "contributors";
						$whereConnect= ($element["collection"]!=Person::COLLECTION) ? 'to the '.Element::getControlerByCollection($element["collection"]) : ""; 
						$classHref=(isset($class) && !empty($class)) ? $class : "text-red"; 
						$classHref.=(isset($tooltip) && !empty($tooltip)) ? " tooltips" : ""; 
					?>

					<?php if (($edit==true || $openEdition==true) && @Yii::app()->session["userId"]){ ?>
						<a 	href="<?php echo $urlLink ;?>" 
							class="pull-right text-center link-banner lbhp "
							data-placement="bottom" 
							data-original-title="<?php echo Yii::t("common","Invite {what} {where}",array("{what}"=> Yii::t("common",$inviteLink),"{where}"=>Yii::t("common", $whereConnect))); ?>" > 
					        <i class="fa fa-user-plus a-icon-banner"></i> 
					        <span class="title-link-banner hidden-sm"> <br><?php echo Yii::t("common","Invite") ?></span> 
					    </a>
					
					<?php }

					if(in_array($element["collection"],[Organization::COLLECTION,Event::COLLECTION,Project::COLLECTION])){ 
						if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){ ?>
						<a href="javascript:;" class="ssmla pull-right text-center link-banner" data-action="share">
							<i class="fa fa-share-alt a-icon-banner"></i> 
							<span class="title-link-banner hidden-sm"><br><?php echo Yii::t("common", "Share") ?>
							</span>
						</a>
					<?php }
					} ?>

				</div>
		</div>

		</div>

		<div class="col-xs-12 visible-xs contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>" style="padding: 15px 15px;">
			<h5 class="text-center padding-left-15 no-margin text-white">
						<span id="nameHeader">
							<span class="bg-<?php echo $iconColor; ?>  icon-before-name">
								<i class="fa fa-<?php echo $icon; ?> text-center"></i>
							</span> 
							<span class="name-header"><?php echo @$element["name"]; ?></span>
						</span>
						<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
								$typesList=Event::$types;
						?>
							<span id="typeHeader" class="margin-left-10 pull-left">
								<i class="fa fa-x fa-angle-right pull-left"></i>
								<div class="type-header pull-left">
							 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
							 	</div>
							</span>
						<?php } ?>
					</h5>
					<div class="col-xs-12 text-center text-white">
			        	<?php if(!empty($element["address"]["addressLocality"])){ ?>
			        	<span class="adress" style="margin: 5px;">
			        		<?php
			        		echo "<i class='fa fa-map-marker'></i> ";
								echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
			        	</span>
			        	<?php } ?>
					</div>

		        	<div class="col-xs-12 text-center">
						<?php
						 if(in_array($element["collection"],[Event::COLLECTION,Project::COLLECTION, Organization::COLLECTION])) { ?>
							<div class="event-infos-header"></div>
						<?php } ?>
					</div>
		        	
		</div>

			<!-- DESCRIPTION MENU TOP HORIZONTAL -->

		<div class="col-lg-12 col-md-12 col-sm-12 social-sub-header hidden-xs no-padding">
			<div id=menuTopDesc class="col-md-offset-3 col-sm-offset-4 col-lg-offset-2 col-md-8 col-sm-8 col-lg-9 menuTopDesc">
			<div class="col-md-12 col-sm-12 col-lg-12 text-left padding-10 hidden-xs shortDescTop text-dark-blue elipsis">
				<?php echo @$element["shortDescription"]; ?>	
			</div>
			<div class="col-sm-12 visible-sm hidden-xs">
				        <ul class="tag-list no-padding">
				        	<?php if(!empty($element["address"]["addressLocality"])){ ?>
				        	<li class="tag adress">
				        		<?php
				        		echo "<i class='fa fa-map-marker'></i> ";
									echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
									echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
									echo $element["address"]["addressLocality"];
								?>
				        	</li>
				        	<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden" ); ?>
										<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO ; ?>" style="font-size: 10px;line-height: 35px; color: #2c407a!important;">
											<i class="fa fa-circle-o"></i>
										</span>
									
								<?php } 
				            
				                if(!empty($element["tags"])){
				                    foreach ($element["tags"]  as $key => $tag) {
				                        echo '<li class="tag" style="margin-top: 3px;"><a href="#search?text=#'.$tag.'" class="text-green bold" style="border: 0px!important;padding-left: 1px!important;padding-right: 1px!important;background-color: transparent;"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</a></li>';
				                         }
				                }else{
				                    
				                } ?>
				        </ul>
			<div class="pull-left">
				<?php
				if(in_array($element["collection"],[Event::COLLECTION,Project::COLLECTION, Organization::COLLECTION])) { ?>
					<div class="event-infos-header" style="text-align: left;"></div>
				<?php } ?>
			</div>
		</div>
		</div>
		

<!-- END DESCRIPTION MENU TOP HORIZONTAL -->
