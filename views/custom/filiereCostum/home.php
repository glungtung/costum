<?php 

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );
  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 
  $cssAndScriptFilesModule = array(
    '/js/default/profilSocial.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

    $cmsList = array();

    $page = ( isset($page) && !empty($page) ) ? $page : "welcome";

    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
        $cmsList = PHDB::find(Cms::COLLECTION, 
                            array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                                "page" => @$page) );
        // var_dump($cmsList);exit;
    }


    $bannerImg = @$el["profilRealBannerUrl"] ? Yii::app()->baseUrl.$el["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/filiereCostum/no-banner.jpg";
    $logo = @$this->costum["logo"] ? $this->costum["logo"] : null;
    $shortDescription = @$this->costum["description"] ? $this->costum["description"] : null;
    $bkagenda = @$this->costum["tpls"]["blockevent"]["background"] ? $this->costum["tpls"]["blockevent"]["background"] : "white";
    $titlenews = @$this->costum["tpls"]["news"]["title"] ? $this->costum["tpls"]["news"]["title"] : "Actualité";

?>
<style>
section {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
}
.header-filiere{
    background: #001146;
    background-image : url(<?= $bannerImg; ?>);
    width: 101%;
    height: auto;
    background-size: cover;
    padding-bottom: 58%;
}
.title {
    text-decoration: underline;
}
.logofn{
    width: 50%;
margin-top: 5%;
position: absolute;
margin-left: 3%;
}
.carto{
    margin-top : -7.9%;
}
.carto-h1 {
    color : white;
}
.carto-d {
    box-shadow: 0px 0px 20px -2px black;
    width: 80%;
    left: 10%;
    background : white;
    padding: 2%;
    font-size : 1.5vw;
}
.blockwithimg {
    margin-top : 2%;
    margin-bottom: 2%;
}
.actu{
    margin-top:3%;

}
.agenda{
    /* background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/fond/fond.png); */
    color : white; 
    margin-top :3%;
    background : <?= $bkagenda; ?>;
}
.agenda-h1{
    margin-left :12%;
    left: 4%;
    margin-top: 2%;
}
.plus-m {
    width : 2%;
}
.img-event{
    height: 250px;
    width: 500px;
}
.carto-p-m{
    font-size: 2.85vw;
    margin-top: 6%;
    line-height: 42px;
}
.carto-n{
    margin-top: 2%;
}
.description{
    padding : 5%; 
    top: 5%;
}
.title-carto{
    font-size : 2.81em;
}
button {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
}
#newsstream .loader{
    display: none;
}
@media (max-width:768px){
    .plus-m {
    width : 5%;
    }
    .carto-description{
        font-size: 2.5vw !important;
    }
    .img-event {
        height: 75px;
        width: 250px;
    }
    .carto-p-m{
        font-size: 2.5rem;
        margin-top: 2%;
        line-height: 19px;
    }
    .p-mobile-description{
        font-size : 1.5rem;
    }
    .description{
        padding : 5%; 
        margin-top: -3%;
    }
    .title-carto{
        font-size : 2.81vw;
    }
    .btn {
        padding: 3px 5px;
        font-size: 7px;
    }
    .card-mobile{
        margin-top : 3%;
    }
    .logofn{
        width: 100%;
        margin-top: 13%;
    }
}
</style>



<div class="header-filiere">
    <?php
    $params = [  "tpl" => "filiereCostum","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el, "page" => @$page ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
    ?>
    <div class="logofn col-md-9">
        <!-- Edit Banniere -->
        <?php 
        $params =  array(
            "type"=> @$el["type"], 
            "id"=>(string) @$el["_id"], 
            "name"=> @$el["name"],
            "canEdit" => $canEdit,
            "profilBannerUrl"=> @$el["profilRealBannerUrl"]);

        echo $this->renderPartial("costum.views.tpls.modalHeader", $params); 
        ?>

        <?php
        if(@$logo && $logo != null) { ?>
        <img src="<?= $logo; ?>" id="logoBanner" class="img-responsive" style="width: 65%;">
        <?php } ?>
    </div> 
</div>


<!-- Description -->

<div class="carto row">
    <div class="carto-h1 col-xs-6 col-sm-6 no-padding">
        <p class="title-carto" style="margin-left: 19.5%;" id="shortDescription"><?php echo $shortDescription; ?></p>
    </div>
    <div class="carto-img col-xs-6 col-sm-6" >
       <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="margin-top: -60%; margin-left:3%; position: absolute; opacity: 0.7;">
    </div>
    <div class="carto-d col-xs-12 col-sm-12">
        <p class="p-mobile-description" id="descriptions"> <?php //$this->renderPartial("costum.views.tpls.blockCms.textImg.text", array("cmsList" => $cmsList, "tag" => "descriptionsfil1", "page" => @$page));
    ?></p>
        </div>

  
</div>

<!-- Carto NEWS -->

<div class="carto-n row">
    <div class="c-description container">
    <?php //$this->renderPartial("costum.views.tpls.blockCms.textImg.blockwithimg", array("cmsList" => $cmsList, "canEdit" => $canEdit, "tags" => "block0")); ?>
    </div>
</div>

<!-- Join -->
<center>

    <?php 
        // if(isset($this->costum["custom"]["btnother"]))
        //     echo $this->renderPartial("costum.views.custom.filiereCostum.tpls.btn-other", array("cmsList"   =>  $cmsList, "canEdit" => $canEdit));
        // else
        //      echo $this->renderPartial("costum.views.custom.filiereCostum.tpls.btn", array("cmsList" => $cmsList,"page" => @$page));
    ?>

</center>

<?php
foreach ($cmsList as $e => $v) {
    // var_dump($v);exit;
    $params = [
        "cmsList"   =>  $cmsList,
        "blockCms"  =>  $v,
        "page"      =>  $page,
        "canEdit"   =>  $canEdit,
        "type"      =>  $v["type"]
    ];
    echo $this->renderPartial("costum.views.".$v["type"],$params);
}
?>

<?php if($canEdit){ ?> 
<!-- ESPACE ADMIN --> 
<hr>
<center>
    <div class="container">
        <a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent"  data-collection="cms"><i class="fa fa-plus"></i> Ajouter une section</a>
    </div>
</center>
<?php } ?>

<script>
jQuery(document).ready(function(){
    mylog.log("render","/modules/costum/views/custom/filiereCostum/home.php");
    setTitle(costum.title);

    contextData = <?php echo json_encode($el); ?>;
    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName=<?php echo json_encode($el["name"]); ?>;
    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;

});
</script>