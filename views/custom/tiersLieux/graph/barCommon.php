<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>



<div id="container" style="margin:20px auto;width:100%">
	<canvas id="<?php echo $id?>-canva"></canvas>
</div>


<script>
	var randomScalingFactor = function() {
		return Math.round(Math.random() * 100);
	};
	jQuery(document).ready(function() {
		mylog.log("render","/dev/modules/costum/views/custom/franceTierslieux/graph/barCommon.php");
		
			var config = {
					type: 'bar',
					data: <?php echo $id ?>Data,
					options: {
						responsive: true,
						legend : {display:false}
					}
				}
					
			var canvas = document.getElementById('<?php echo $id?>-canva');
			var ctx = canvas.getContext('2d');
			window.myBar = new Chart(ctx, config);
				
			
		

			
});
	</script>
