<style type="text/css">
	.blockFontPreview{
		font-size: 14px;
		color: #d4b500;
	}

	.badgeCompagnon{
	position: absolute;
    z-index: 1;
    left: -15px;
    background-color: #d4b500;
    opacity: 0.8;
    padding: 15px;
    padding-left: 25px;
    border-radius: 15px;
    color: white;
    text-align: center;
    font-size: 18px;
    font-variant: small-caps;
    font-weight: bold;
}
h2{
	font-size:unset;
}
#socialNetwork{
	font-size:20px;
}
</style>




<?php 
$cssAnsScriptFilesModule = array(
	'/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$previewConfig=@$this->appConfig["preview"];
$auth=(Authorisation::canEditItem( @Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;

$iconColor=(isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);


//var_dump("<pre>",$element,"</pre>");

?> 
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10 toolsMenu">
		<?php if(isset($previewConfig["toolBar"]["close"]) && $previewConfig["toolBar"]["close"]){ ?> 
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
		
		<a href="#@<?php echo @$element["slug"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
		<?php } 
		if(isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth){ ?>
			<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" 
			data-subtype="<?php echo $element["type"] ?>">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>	
	</div>
	<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">


			<?php if(isset($element["profilImageUrl"]) && !empty($element["profilImageUrl"])){ ?> 
				<div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 margin-top-10">
				
						<a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>">
							<img class="img-responsive" style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);margin: auto;max-height: 300px;" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
						</a>
				
				</div>
		<?php } 
		 ?>
		<?php
		if (isset($element["tags"])){
			if(in_array("Compagnon France Tiers-Lieux",$element["tags"])){
		?> 
				<div class="badgeCompagnon">
						<span>Tiers-lieux Compagnon</span>
				</div>	
	<?php }}?>
		<div class="preview-element-info col-xs-12">
		<?php 

					if(isset($element["name"])){ ?>
						<h3 class="text-center margin-top-40"><?php echo $element["name"] ?></h3>
					<?php } ?>
					<div class="col-xs-12" style="color: #d4b500;"> 
			<?php		if(isset($element["address"]["addressLocality"])){ ?>
						<div class="header-address col-xs-12 text-center blockFontPreview margin-top-20">
							<i class="fa fa-map-marker"></i> 
							<?php
								echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? 
										$element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
						</div>
					<?php } ?>
				</div>
				<?php
				if(isset($element["url"])){ ?>
					<div class="col-xs-10 col-xs-offset-1 margin-top-10 text-center">
						<?php $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ; ?>
						<a href="<?php echo $scheme.$element['url'] ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;color:#d4b500;"><?php echo $element["url"] ?></a>
					</div>
				<?php } ?>
				<?php 
				if (isset($element["socialNetwork"])){?>
					<div id="socialNetwork" class="col-xs-10 col-xs-offset-1 margin-top-10 text-center">
				<?php 

					if (isset($element["socialNetwork"]["facebook"])){ ?>		
			          <span id="divFacebook" class="margin-right-10">
			              <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["facebook"];?>" target="_blank" id="facebookAbout" >
			                <i class="fa fa-facebook" style="color:#1877f2;"></i>
			              </a>
			          </span>
			  <?php }
			         
			          if (isset($element["socialNetwork"]["twitter"])){ ?>	
			          <span id="divTwitter">
			              <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["twitter"];?>" target="_blank" id="twitterAbout" >
			                <i class="fa fa-twitter" style="color:#1da1f2;"></i>
			              </a>
			          </span>
			          <?php } ?>
			        </div> 
				
				<?php

					
				}						
					if(isset($element["shortDescription"])) { ?>
						<div class="col-xs-10 col-xs-offset-1 margin-top-10">
							<h2> Description courte</h2>
							<span class="col-xs-12 text-center" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
							</span>	
						</div>
					<?php } ?>
					<?php $tabSpec=array("typePlace"=>
											["name"=>"Typologie d'espace de travail",
											"icon"=>"briefcase"
											],
										"services"=>
											["name"=>"Services proposés",
											"icon"=>"tags"
											],
										"manageModel"=>	
											["name"=>"Mode de gestion",
											"icon"=>"adjust"
											],
										"state"=>	
											["name"=>"Etat du projet",
											"icon"=>"cubes"
											],
										"spaceSize"=>	
											["name"=>"Taille du lieu",
											"icon"=>"expand"
											],
										"certification"=>	
											["name"=>"Label obtenu",
											"icon"=>"medal"
											],
										"greeting"=>	
											["name"=>"Actions d'accueil",
											"icon"=>"info"
											],
										"network" =>	
											["name"=>"Réseau d'affiliation",
											"icon"=>"users"
											]
										);

			if(isset($element["tags"]))	{		
?>			
				<div class="col-xs-10 col-xs-offset-1 margin-top-20"> 
						<h2>Caractéristiques du lieu</h2>
						<table class='col-xs-12'>
				<?php	foreach($tabSpec as $k => $v){ 

							$strTags="";
							$tagsList="";
							$listCurrent=$this->costum["lists"][$k];
							if(!empty($element["tags"])){
								foreach($element["tags"] as $tag){
									if(in_array($tag, $listCurrent)){
										$strTags.=(empty($strTags)) ? $tag : "<br>".$tag; 
									}
									
									
								}

							}
							if(!empty($strTags)){
							?>
							
								
									<tr>
										<td class="col-xs-1 text-center" style="vertical-align: top;padding:10px;">
											<?php  
											if(isset($v["icon"])){
												echo "<i style='color: #d4b500;' class='fa fa-".$v["icon"]."'></i>";
											}
											?>
										</td>	
										<td class="col-xs-4" style="vertical-align: top;padding:8px;font-weight: bold;font-size: 17px;font-variant: small-caps;">
											<?php  
											if(isset($v["name"])){
												echo $v["name"];
											}
											
											?>		
										</td>
										<td class="col-xs-7" style="padding:8px;"><?php echo $strTags; ?></td>
									</tr>	
									
						
							
								
						<?php 
						}}
						 ?>
						</table>	
						

						
			<?php 	
						foreach($element["tags"] as $tag) {	
							if(!in_array($tag, $listCurrent)) {
								$tagsList.= "#".$tag." ";	
							}
						}	  
					?>
					<span style="color:#d4b500"><?php echo $tagsList ?></span>

				</div>	
			<?php } ?>		



				<div class="col-xs-12">
			<?php 
				if(isset($element["links"]) && isset($element["links"]["members"])){ 
					$this->renderPartial('co2.views.pod.listItems', array("title"=>"Les membres", "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"]));
				} ?>
			</div> 
				
		
	</div>
</div>

<script type="text/javascript">

	var eltPreview=<?php echo json_encode($element); ?>;
 	var typePreview=<?php echo json_encode($type); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
	jQuery(document).ready(function() {	
		var str = directory.getDateFormated(eltPreview, null, true);
		$(".event-infos-header").html(str);
		//coInterface.bindLBHLinks();
		//resizeContainer();
		directory.bindBtnElement();
	});
</script>