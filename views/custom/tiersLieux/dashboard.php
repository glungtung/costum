<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/jquery-counterUp/waypoints.min.js",
		"/plugins/jquery-counterUp/jquery.counterup.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style type="text/css">
    <?php if( !isset($css["border"]) || $css["border"]==true ){ ?>
	.counterBlock{min-height:340px;}
    .wborder{border : 2px solid #002861; border-radius : 20px;}
    .title2{font-size : 24px; font-weight : bold;}
    <?php }?>
    
</style>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-20 dashboard app-<?php echo @$page ?>">
    <div class="col-xs-12 text-center" style=" color:#24284D">
    	<h1 style="margin:0px"><?php echo $title ?></h1>
    </div>
   <div classs="col-xs-12" style="">
		<?php //echo $this->renderPartial("graph.views.co.menuSwitcher",array()); ?> 
		<div class="col-xs-12" id="graphSection">
   				<?php       					
   				
                 $colCount = 0;
                 $blocksize=6;
                $fontSize = "24px;font-weight:bold;";
                $borderClass = "wborder";
   				 foreach ($blocks as $id => $d) 
   				 { 
                    if (isset($d["width"]))
                        $blocksize=$d["width"];
                    // if($id == "pieGeo")
                    //     $blocksize=12;
                    if (isset($d["html"]))
                        $blocksize=4;
                   
                     if( isset($d["graph"]) || isset($d["html"]))  { 
                        ?>     
                        <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <h1><span class="<?php echo (!empty($d["counter"]))? "counter" : ""?>"><?php echo $d["counter"]?></span></h1>
                            <div style='font-size:<?php echo $fontSize?>'><?php echo $d["title"]?></div>
                            <div class="counterBlock <?php echo $borderClass; ?>" id="<?php echo $id?>" >
                                <?php if( isset($d["html"]) ) 
                                    echo $d["html"];?>
                            </div>
                        </div>
                     <?php 
                     
                     } ?>
             <?php }?>
   		</div>
   </div>

</div>



<script type="text/javascript">
//alert("/modules/costum/views/custom/ctenat/dashboard.php")
//prepare global graph variable needed to build generic graphs

<?php  
foreach ($blocks as $id => $d) {
	if( isset($d["graph"]) ) {
		?>
		var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
		mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
<?php }
} ?>

jQuery(document).ready(function() {
	setTitle("Observatoire des Tiers-lieux");
	mylog.log("render graph","/modules/costum/views/custom/tierslieux/dashboard.php");

	<?php  foreach ($blocks as $id => $d) {
		if( isset($d["graph"]) ) { ?>
			mylog.log('render ajaxPost url graphs',' <?php echo $d["graph"]["url"]?>','#<?php echo $id?>');
			ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
	<?php }
	} ?>

	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});

	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');

});
</script>


<style type="text/css">
    .dashElem{
        /*height:275px;*/
        overflow:hidden;
        /*border: 1px solid #bbb;*/
    }
    .grayimg{
        opacity:0.2;
    }
    .openDashModal{
        font-size: 10px;
    }
</style>
    


<script type="text/javascript">

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/tiersLieux/dashboard.php",<?php echo json_encode( $elements) ?>);
        coInterface.bindLBHLinks();
       
                
 
        //    });

    });
</script>