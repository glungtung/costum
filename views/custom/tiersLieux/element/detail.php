<div class="col-xs-12 bg-white shadow2">
	<div class="col-xs-12 text-center margin-top-10">
		<?php if($edit){?>
			<a href="javascript:;" class="btn btn-primary text-white editElement" style="color:white!important;"><i class="fa fa-pencil"></i> Editer les informations</a>
		<?php } ?>
			<!--<a href="javascript:;" class="ssmla margin-left-5 btn" data-view="history">
				<i class="fa fa-history"></i> <?php echo Yii::t("common","History")?> 
			</a>-->
	</div>
	<div id="ficheInfo" class="panel panel-white col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding shadow2 margin-top-20">	
		<div class="panel-body no-padding" style="padding:20px 0px !important;">
			<div class="col-md-8 col-sm-8 col-xs-12 contentInformation">
				<h4 class="panel-title pull-left margin-bottom-20">
					<?php echo Yii::t("common","About") ?>
				</h4>
				<div class="col-xs-12 valueAbout no-padding" 
						style="word-wrap: break-word; overflow:hidden;">
					<div id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 contentInformation">
				<h4 class="panel-title pull-left margin-bottom-20">
					<?php echo Yii::t("common","Caractérisques") ?>
				</h4>
				<div class="col-xs-12 valueAbout no-padding" 
						style="word-wrap: break-word; overflow:hidden;">
						<?php $tabSpec=(isset($element["category"]) && $element["category"]=="network") ? array("typePlace"=>"Thématique(s)","manageModel"=>"Modèle de gestion", "state"=>"Avancement","certification"=>"Label","greeting"=>"Actions d'accueil","compagnon"=>"Compagnon France Tiers-lieux") : array("typePlace"=>"Type", "services"=>"Services", "manageModel"=>"Modèle de gestion", "state"=>"Avancement","spaceSize"=>"Superficie","certification"=>"Label","greeting"=>"Actions d'accueil","network"=>"Réseau associé","compagnon"=>"Compagnon France Tiers-lieux");
						foreach($tabSpec as $k => $v){ 
							$strTags="";
							$listCurrent=$this->costum["lists"][$k];
							if(!empty($element["tags"])){
								foreach($element["tags"] as $tag){
									if(in_array($tag, $listCurrent)){
										if($tag=="Compagnon France Tiers-Lieux") {
											$strTags="<i>Oui</i>";
										}
										else
										$strTags.=(empty($strTags)) ? $tag : ", ".$tag; 
									}
									
								}  
							}
							if(empty($strTags)) $strTags="<i>Non spécifié</i>";
							?>
							<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
								<span><b><?php echo $v; ?></b><br/><?php echo $strTags ?></span>
							</div>
						<?php }  
							// $compagnon="";
							// if(isset($element["compagnon"])) {								
							// 			if($element["compagnon"]=="true") {
							// 				$compagnon="Oui";
							// 			}
							// 			else
							// 				$compagnon="Non"; 
							// 		}
							// 	else 
							// 		$compagnon="<i>Non spécifié</i>"; 
							 	?>
							 	<!-- <span><b><?php //echo "Compagnon France Tiers-lieux"; ?></b><br/><?php //echo $compagnon; ?></span>	 -->					
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-white col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding shadow2">
		<div class="panel-heading border-light col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #dee2e680;">
			<h4 class="panel-title pull-left"> 
				Contact
			</h4>
		</div>
		<div class="panel-body no-padding">
		  	<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-envelope"></i></span> <?php echo Yii::t("common","E-mail"); ?>
				</div>
				<div id="emailAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-envelope"></i> <?php echo Yii::t("common","E-mail"); ?> :</span><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
				</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-desktop"></i></span> <?php echo Yii::t("common","Website URL"); ?>
				</div>
				<div id="webAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5"><i class="fa fa-desktop"></i> <?php echo Yii::t("common","Website URL"); ?> :</span>
				<?php 
					if(@$element["url"]){
						//If there is no http:// in the url
						$scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
					 	echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
					}else
						echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
				<div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
					<span><i class="fa fa-mobile"></i></span> <?php echo Yii::t("common","Mobile"); ?>
				</div>
				<div id="mobileAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
					<span class="visible-xs pull-left margin-right-5">
						<i class="fa fa-mobile"></i> <?php echo Yii::t("common","Mobile"); ?> :
					</span>
					<?php
						$mobile = '<i>'.Yii::t("common","Not specified").'</i>';
						if( !empty($element["telephone"]["mobile"]))
							$mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);	
						echo $mobile;
					?>	
				</div>
			</div>
		</div>	
	</div>
	<div id="adressesAbout" class="panel panel-white col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding shadow2">
		<div class="panel-heading border-light padding-15" style="background-color: #dee2e680;">
			<h4 class="panel-title"> 
				<?php echo Yii::t("common","Localitie(s)"); ?>
			</h4>
		</div>
		<div class="panel-body no-padding">		
			<div class="col-md-12 col-xs-12 valueAbout no-padding" style="padding-left: 25px !important">
			<?php 
					$address = '<span id="detailAddress"> '.
									(( @$element["address"]["streetAddress"]) ? 
										$element["address"]["streetAddress"]."<br/>": 
										((@$element["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
					$address .= (( @$element["address"]["postalCode"]) ?
									 $element["address"]["postalCode"].", " :
									 "")
									." ".(( @$element["address"]["addressLocality"]) ? 
											 $element["address"]["addressLocality"] : "") ;
					$address .= (( @$element["address"]["addressCountry"]) ?
									 ", ".OpenData::$phCountries[ $element["address"]["addressCountry"] ] 
					 				: "").
					 			'</span>';
					echo $address;		
			?>
			</div>
		</div>
		<?php if( !empty($element["addresses"]) ){ ?>
			<div class="col-md-12 col-xs-12 labelAbout padding-10">
				<span><i class="fa fa-map"></i> <?php echo Yii::t("common", "Others localities") ?></span>
			</div>
			<div class="col-md-12 col-xs-12 valueAbout no-padding" style="padding-left: 25px !important">
			<?php	foreach ($element["addresses"] as $ix => $p) { ?>			
				<span id="addresses_<?php echo $ix ; ?>">
					<span>
					<?php 
					$address = '<span id="detailAddress_'.$ix.'"> '.
									(( @$p["address"]["streetAddress"]) ? 
										$p["address"]["streetAddress"]."<br/>": 
										((@$p["address"]["codeInsee"])?"":Yii::t("common","Unknown Locality")));
					$address .= (( @$p["address"]["postalCode"]) ?
									 $p["address"]["postalCode"].", " :
									 "")
									." ".(( @$p["address"]["addressLocality"]) ? 
											 $p["address"]["addressLocality"] : "") ;
					$address .= (( @$p["address"]["addressCountry"]) ?
									 ", ".OpenData::$phCountries[ $p["address"]["addressCountry"] ] 
					 				: "").
					 			'</span>';
					echo $address;
					?>
					</span>
				</span>
				<hr/>
			<?php } ?>
			</div>
		<?php } ?>
	</div>
	<?php
		$skype = (!empty($element["socialNetwork"]["skype"])? $element["socialNetwork"]["skype"]:"javascript:;") ;
		$telegram =  (!empty($element["socialNetwork"]["telegram"])? "https://web.telegram.org/#/im?p=@".$element["socialNetwork"]["telegram"]:"javascript:;") ;
		$diaspora =  (!empty($element["socialNetwork"]["diaspora"])? $element["socialNetwork"]["diaspora"]:"javascript:;") ;
		$mastodon =  (!empty($element["socialNetwork"]["mastodon"])? $element["socialNetwork"]["mastodon"]:"javascript:;") ;
		$facebook = (!empty($element["socialNetwork"]["facebook"])? $element["socialNetwork"]["facebook"]:"javascript:;") ;
		$twitter =  (!empty($element["socialNetwork"]["twitter"])? $element["socialNetwork"]["twitter"]:"javascript:;") ;
		$googleplus =  (!empty($element["socialNetwork"]["googleplus"])? $element["socialNetwork"]["googleplus"]:"javascript:;") ;
		$github =  (!empty($element["socialNetwork"]["github"])? $element["socialNetwork"]["github"]:"javascript:;") ;
		$instagram =  (!empty($element["socialNetwork"]["instagram"])? $element["socialNetwork"]["instagram"]:"javascript:;") ;
	?>
	<div id="socialAbout" class="panel panel-white col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding shadow2">
		<div class="panel-heading border-light col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #dee2e680;">
			<h4 class="panel-title pull-left"> 
				<?php echo Yii::t("common","Socials"); ?>
			</h4>
			<?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ) {?>
				<br/><a href="javascript:;" class="btn-update-network letter-blue tooltips" 
					data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update network") ?>">
					<i class="fa fa-pencil"></i> <?php echo Yii::t("common","Edit") ?>
				</a>
			<?php } ?>
		</div>
		<div class="panel-body no-padding">
			
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Diaspora">
				<span><i class="fa fa-diaspora"></i></span> 
				<span id="divDiaspora">
				<?php if ($diaspora != "javascript:;"){ ?>
					<a href="<?php echo $diaspora ; ?>" target="_blank" id="diasporaAbout" class="socialIcon "><?php echo  $diaspora ; ?></a>
				<?php } else { 
					echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
				} ?>
				</span>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Mastodon">
				<span><i class="fa fa-mastodon"></i></span> 
				<span id="divMastodon">
				<?php if ($mastodon != "javascript:;"){ ?>
					<a href="<?php echo $mastodon ; ?>" target="_blank" id="mastodonAbout" class="socialIcon "><?php echo  $mastodon ; ?></a>
				<?php } else { 
					echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
				} ?>
				</span>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Facebook">
				<span><i class="fa fa-facebook"></i></span>
				<span id="divFacebook">
					<?php if ($facebook != "javascript:;"){ ?>
						<a href="<?php echo $facebook ; ?>" target="_blank" id="facebookAbout" class="socialIcon "><?php echo  $facebook ; ?></a>
					<?php } else { 
						echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
					} ?>
				</span>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Twitter">
				<span><i class="fa fa-twitter"></i></span>
				<span id="divTwitter">
					<?php if ($twitter != "javascript:;"){ ?>
						<a href="<?php echo $twitter ; ?>" target="_blank" id="twitterAbout" class="socialIcon" ><?php echo $twitter ; ?></a>
					<?php } else { 
						echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
					} ?>
				</span>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Instagram">
				<span><i class="fa fa-instagram"></i></span> 
				<span id="divInstagram">
					<?php if ($instagram != "javascript:;"){ ?>
						<a href="<?php echo $instagram ; ?>" target="_blank" id="instagramAbout" class="socialIcon" ><?php echo $instagram ; ?></a>
					<?php } else { 
						echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
					} ?>
				</span>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Skype">
				<span><i class="fa fa-skype"></i></span>
				<span id="divSkype">
					<?php if ($skype != "javascript:;"){ ?>
						<a href="<?php echo $skype ; ?>" target="_blank" id="skypeAbout" class="socialIcon" ><?php echo $skype ; ?></a>
					<?php } else { 
						echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
					} ?>
				</span>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="GitHub">
				<span><i class="fa fa-github"></i></span>
				<span id="divGithub">
					<?php if ($github != "javascript:;"){ ?>
						<a href="<?php echo $github ; ?>" target="_blank" id="githubAbout" class="socialIcon" ><?php echo $github  ; ?></a>
					<?php } else { 
						echo '<i>'.Yii::t("common","Not specified").'</i>' ; 
					} ?>
				</span>
			</div>
		</div>	
    </div> 
</div>
<?php $levelTer=isset($element["level"]) ? $element["level"] : ""; ?>
<script type="text/javascript">
jQuery(document).ready(function() {
	pageProfil.bindViewActionEvent();
	descHtml="<i>Pas de description pour ce tiers-lieux</i>"
	if($("#descriptionAbout").html().length > 0){
		descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
	}
	$("#descriptionAbout").html(descHtml);
	$(".editElement").click(function(){
		dyFObj.editMode=true;
		//uploadObj.set(type, id);
		uploadObj.update = true;
		dyFObj.currentElement={type : contextData.type, id : contextData.id};
		
		var levelTer= <?php echo json_encode($levelTer) ?> ;
		contextData.level=costum.lists.level[levelTer]; 
		dataEdit=jQuery.extend(true, {},contextData);
		var formType= (typeof contextData.category!="undefined" && contextData.category=="network") ? "reseau" : "organizations";
		if(typeof contextData.typeOrga != "undefined")
			dataEdit.type=contextData.typeOrga;
		dyFObj.openForm(formType, null, dataEdit);
	});
	$("#menuRight").find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
	$("#menuRight").find("a").toggleClass("changelabel");
	$(".BtnFiltersLieux").hide();
});
</script>