
<style>
.header{
    background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/alfph/banner.jpg);
    width: 100%;
    /*height: auto;*/
    background-size: cover;
    padding-bottom: 30%;
}
.ressource{
    background : transparent;

   /* box-shadow: 5px 5px 20px #e4dbdb;*/
    margin-top: 240px;
}
.bloc{
    padding: 7%;
}
.bloc h2{
    text-transform: none;
}
.btn-perso{
    padding: 3%;
    background: white;
    border-radius: 5%;
    font-size: 2rem;
    box-shadow: 2px 2px 0px #746f6f;
    text-decoration: none !important;
}
.btn-perso:hover{
    background: #757ca0;
    color:white;

}
.bloc-section{
    
    font-size: 3rem;
}
.rejoins{
    background: white;
}
.logofn{
    margin-left: 3%;
}
.projet{
    background-color : #e7e6e6;
    padding : 3%;
    font-size: 1.5rem;
    text-transform: none;
}
.bloc-projet {
    background: white;
    width: 25%;
    margin-left: 6%;
    padding: 3%;
    font-size: 1rem;
    border-radius: 3%;
}
.bloc-projet h2{
    text-transform: none;
}
#content-projet {
    margin-top: 7%;
}
@media (max-width:768px){
    .bloc-projet{
        width: 100%;
        border-radius: 0%;
        margin-left: 0%;
    }
}
</style>


    <div class="header">
        <div class="logofn col-md-9">
            <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 99.65 41.24" style="width: 60%; filter: drop-shadow( 0px 0px 8px rgb(41, 40, 40));">
            	<defs>
            		<style>.cls-1{fill:#70b3a1;}.cls-2{fill:#36a2a5;}.cls-3{fill:#88bda2;}.cls-4{fill:#55aaa7;}.cls-5{font-size:7.5px;}.cls-5,.cls-8,.cls-9{fill:#fff;}.cls-5,.cls-8{font-weight:500;}.cls-6{font-size:7px;}.cls-7{letter-spacing:-0.02em;}.cls-8{font-size:2.88px;}
            	   </style>
            	</defs>
            	<title>logo blanc</title>
            	<polygon class="cls-1" points="33.81 2.3 11.94 2.3 22.88 8.62 33.81 2.3"/>
                <polygon class="cls-2" points="11.94 2.3 1 21.25 22.88 8.62 11.94 2.3"/>
                <polygon class="cls-3" points="44.75 21.25 33.81 2.3 22.88 8.62 44.75 21.25"/>
                <polygon class="cls-4" points="44.75 21.25 22.88 8.62 1 21.25 22.88 33.88 44.75 21.25"/>
                <polygon class="cls-3" points="22.88 33.88 33.81 40.19 44.75 21.25 22.88 33.88"/>
                <polygon class="cls-4" points="11.94 40.19 33.81 40.19 22.88 33.88 11.94 40.19"/>
                <polygon class="cls-2" points="1 21.25 11.94 40.19 22.88 33.88 1 21.25"/>
                <text class="cls-5" transform="translate(48.58 17.13)" style="text-shadow: 2px 2px black;font-family: 'customFont';">RECENSER
                    <tspan class="cls-6"></tspan><tspan class="cls-7" x="0" y="14.92"></tspan>
                    <tspan x="2.3" y="8.92">ET AGIR</tspan>
                </text>
                <text class="cls-8" transform="translate(60.12 33.06)" style="text-shadow: 2px 2px black;font-family: 'customFont';">Dans Lille et ses quartiers</text>
                <path class="cls-9" d="M12.24,18.78v-.87a.94.94,0,0,1,0-.16,9.37,9.37,0,0,1,.36-1.5,5.66,5.66,0,0,1,4.81-3.73,5.65,5.65,0,0,1,5.26,2.2l.17.2c.17-.21.32-.4.48-.58a5.78,5.78,0,0,1,3.8-1.84A5.65,5.65,0,0,1,31,13.55a5.86,5.86,0,0,1,1.63,7.83.27.27,0,0,1-.13.11H31v-3.2H25.35v3.2H22.14v5.62h3.21c0,.08,0,.15,0,.22,0,.93,0,1.86,0,2.8a.58.58,0,0,1-.12.33c-.74.91-1.48,1.81-2.23,2.71,0,0-.06.1-.11.17L21.81,32l-8.16-9.88a5.92,5.92,0,0,1-1.18-2.21C12.37,19.56,12.32,19.17,12.24,18.78Z"/>
                <path class="cls-9" d="M23.55,25.71V22.9h3.2V19.68h2.8v3.21h3.22v2.8h-3.2v3.22H26.76v-3.2Z"/>
            </svg>
        </div>

    </div>

    <div class="filigram">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="
        margin-top: -37.5%;
        position: absolute;
        opacity: 0.7;
        width: 51%;
        left: 53%;
        ">
    </div>

    <div class="ressource container">
        <div class="bloc col-md-6" style="background-color: #e7e6e6; background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/blocleft.png);background-position: right;background-repeat: no-repeat;">
            <h2> J'ai besoin d'aide </h2>
            <p style="margin-bottom: 10%; margin-top: 5%;">Je suis un habitant qui a des besoins spécifiques.</p>

            <a onclick="dyFObj.openForm('ressources', null, {'section' : 'need', 'category' : 'service', 'subtype' : 'other'});" href="javascript:;"  class="btn-perso"> Demander de l'aide </a>
        </div>

        <div class="bloc col-md-6" style="
            background-color:#32a2a4; 
            background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/blocright.png);
            background-position: right;
            background-repeat: no-repeat;    
            ">
            <h2> Je peux aider </h2>
            <p style="margin-bottom: 10%; margin-top: 5%;">Je fais partie d'une collectif qui souhaite agir à travers un action solidaire</p>

            <a onclick="dyFObj.openForm('projects', null, null);" href="javascript:;" class="btn-perso openProjet"> Proposer de l'aide </a>
        </div>
    </div>

    <!-- <div class="projet text-center bloc-section">
        <div class="container">
        <center>
            <span style="color: #36a2a5"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/projets.svg" width="2%"> 
            Projets</span>
        </center>
            <h3 style="text-transform: none;">Rejoignez des projets liés à des besoins</h3>

            <div id="content-projet" class="col-xs-12">
                <div class="bloc-projet col-md-4">
                    <h2>Production de Masque DIY</h2>

                    <center>
                        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/masque-02.png" class="img-responsive">
                    <a href="javascript:;" data-hash="#productor" class="lbh-menu-app">
                        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/plus.png" class="img-responsive" width="35%">
                    </a>    
                    </center>
                    
                </div>

                 <div class="bloc-projet col-md-4">
                    <h2>Produit désinfectant DIY</h2>

                    <center>
                        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/pratique-02.png" class="img-responsive">
                    <a href="javascript:;" data-hash="#product" class="lbh-menu-app">
                        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/plus.png" class="img-responsive" width="35%">
                    </a>
                    </center>

                </div>

                <div class="bloc-projet col-md-4">
                    <h2>Bonnes <br> pratiques</h2>
                    
                    <center>
                        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/produit-02.png" class="img-responsive">
                    <a href="javascript:;" data-hash="#pratice" class="lbh-menu-app">
                        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/franceUnit/plus.png" class="img-responsive" width="35%">
                    </a>
                    </center>

                </div>
            </div>
        </div>
    </div> -->

    <div class="rejoins bloc-section row">
        <div class="col-md-6 col-xs-12 text-center" style="padding: 3%;">

    <!-- Début news svg s-->

                <!-- <a onclick="dyFObj.openForm('bookmark', null, null);" href="javascript:;" class="col-lg-12 col-md-12 col-xs-12 btn-perso margin-10" style="text-decoration: none !important;"><i class="fa fa-link"></i> Partager un lien<small>(bonnes pratiques, ...)</small></a> -->
                <a href="javascript:;" data-hash="#search" class="lbh-menu-app col-lg-12 col-md-12 col-xs-12 btn-perso margin-10" style="text-decoration: none !important;"><i class="fa fa-search margin-right-5"></i>Rejoindre un projet</a>       
                <a href="javascript:;" data-hash="#annonces" class="lbh-menu-app col-lg-12 col-md-12 col-xs-12 btn-perso margin-10" style="text-decoration: none !important;"><i class="fa fa-heartbeat margin-right-5"></i>Consulter les besoins</a>
           

    <!-- fin news svgs -->

    <!-- Début news svg s-->

    <!-- fin news svgs -->

            <!-- Btn je souhaite -->
                    <!-- Début news svg--> 
    <!--                 <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 5 90.36 14" style="width: 85%;">
    	<defs>
    		<style>.cls-1{fill:#fff;}</style>
    	</defs>
    	<title>je souhaite</title>

    	<path class="cls-1" d="M87,5.69H5.64L2.27,11.53l3.37,5.83H87l3.37-5.83ZM13.54,12.76h-.95v1h-.84v-1H10.8v-.84h.95V11h.83v.95h1Zm0-1.28a.05.05,0,0,1,0,0H13v-.95H11.34v.95h-1v1.66h.95s0,.05,0,.07v.83a.42.42,0,0,1,0,.1l-.66.8,0,0-.32-.39c-.81-1-1.62-2-2.42-2.93a1.67,1.67,0,0,1-.36-.65c0-.12,0-.23-.06-.35v-.26a.06.06,0,0,1,0,0A2.28,2.28,0,0,1,7.57,10a1.73,1.73,0,0,1,3-.45l0,.06a1.29,1.29,0,0,1,.15-.17,1.72,1.72,0,0,1,1.12-.55A1.69,1.69,0,0,1,13,9.16a1.74,1.74,0,0,1,.72,1.17A1.72,1.72,0,0,1,13.5,11.48Z"/>
    	<text x="15" y="13.5" style="font-size: 0.55rem;">
                 <a onclick="dyFObj.openForm('organization', null, {tags : 'covid19'});" href="javascript:;" style="text-decoration: none !important;"> Proposer une organisation </a>
        </text>
    </svg>
     -->
            <!-- Fin news svg -->

            
        </div>
        <div class="col-md-6 col-xs-12 row" style="top: -1px;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/alfph/solidarite2.jpg" style="width: 103%;">
        </div>
    </div>    

    <div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1" style="text-align: center;
        min-height: 100px;">
            <p style="color:#333;font-size: 2rem;line-height: 35px;margin:inherit;"> 
                Repérons les besoins des personnes les plus vulnérables, et répondons-y à travers des actions collectives !
            </p>
    </div>


<script type="text/javascript">
jQuery(document).ready(function() {
    setTitle("Solidarité ALFPH");
    $("#bg-homepage").addClass("col-xs-12");
  $(".openProjet").click(function(){
        var dyFCostumAlfph=jQuery.extend(true, {}, typeObj.project.dynFormCostum);
        dyFCostumAlfph=costum.alfph.dynForm.setCommonDynForm(dyFCostumAlfph);
        dyFObj.openForm('project', null,null,null, dyFCostumAlfph)
  });      


});

</script>

