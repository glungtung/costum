<style >
	
	.container {
		background: #f8f8f8;
		font-family: Helvetica, Arial, Verdana, sans-serif;
		font-size: 20px;
	}
	#bg-homepage{
		width: 100%;
		border-top: 1px solid #ccc;
	}
	.card {
		/* Add shadows to create the "card" effect */
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		background: #fff;
		margin-bottom: 20px;
		border-radius: 10px;
		width: 99%;
		min-height: 250px;
		text-align: center;
	}
	.card-title{
		padding:20px;
		font-size: 25px;
	}
	.row{
		margin-top: 30px;
	}
	.h1{
		background: #ccc;
	}
	h4{
		text-align: center;
		font-size: 30px;

	}
	p {
		font-weight: bold;
	}
	
	table
	{
		border-collapse: collapse;
		border-width:1px; 
		border-style:solid; 
		border-color:#ddd;
		width:100%;
		margin-bottom: 20px;
		margin-top: 15px; 
	}
	td {
		border: 1px solid #ddd;
		padding: 8px;
		text-align: center;
	}
	th {
		padding-bottom: 12px;
		text-align: center;
		border: 1px solid #ddd;
	}
	

	hr {
		display: block;
		clear: both;
		font-family: arial;
		text-align: center;
		line-height: 1;
		border: dashed 0.5px #b7b8b5;
	}
	.card1 {
		/* Add shadows to create the "card" effect */
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
		margin-bottom: 20px;
		border-radius: 10px;
		width: 99%;
		min-height: 250px;
	}
	
	#containAnswer a {
		margin:   2%;
	}
	.listProjet h3{
		text-align: center;
	}

</style>
<?php 

$ficheId = PHDB::findOne(Form::COLLECTION, array('id' =>$this->costum["formId"],"name" => "Fiche projet"));
$reponse = PHDB::find(Form::ANSWER_COLLECTION,array('user' =>Yii::app()->session["userId"], "form" =>(string)$ficheId["_id"] ));
?>
<div class="container">
	<div id="liste" style="display:none">
		<a class=" btn btn-primary">
			Afficher la liste
		</a>
	</div>
	<div id="containAnswer"> </div>
	<div id="ficheProjet"> </div>
</div>
<script type="text/javascript">
	params = {
		"formId" : "<?= $this->costum["formId"] ?>"
	};
    ajaxPost(
        null,
        baseUrl+"/costum/ficheprojet/getallansweraction",
        params,
        function(data){
            mylog.log("successss", data);
            str = "";
            str +=
                '<div class="listProjet">' +
                '<h3 > Liste des projets </h2>'
            $(data).each(function(key,value){
                mylog.log("liste reponse1",value._id.$id);
                answers = (typeof value.answers != "undefined") ? value.answers : "";
                ficheProjet = (typeof answers.projet2872020_1530_0 != "undefined") ? answers.projet2872020_1530_0 : "" ;
                mylog.log("liste reponse", ficheProjet);
                //mylog.log("liste reponse2", ficheProjet);
                nomProjet = (typeof ficheProjet.projet2872020_1530_01 != "undefined" || ficheProjet.projet2872020_1530_01 != "null") ? ficheProjet.projet2872020_1530_01 : "";
                str+=
                    '<div class="col-md-4">'+
                    '<div class="card">'+
                    '<h5 class="card-title">'+
                    nomProjet +
                    '</h5>'+
                    '<a id="Afficher_'+value._id.$id+'" class=" bouton btn btn-primary" data-id= '+ value._id.$id +'> Afficher </a>'+
                    '<a href="#answer.index.id.'+value._id.$id+'.mode.w" class="btn btn-primary lbh"> Modifier </a>'+
                    '</div>'+
                    '</div>';
            });
            str += '</div>';

            $("#containAnswer").html(str);
        },
        null,
        "json",
        {async : false}
    );

	$(document).ready(function(){
		<?php
		foreach ($reponse as $key => $value ) {
			$cle=(string)$value["_id"];
			?>
			$("#Afficher_<?= $cle ?>").click(function(){
				var idFiche = $(this).data("id");
				//fiche(idFiche);
				afficheFiche(idFiche);
				$('#containAnswer').toggle('hide');
			});
		<?php }?>
			$("#liste").click(function(){
				$('#menu').toggle('show');
				$('#ficheProjet').toggle('hide');
				$('#liste').toggle('hide');
				$('#containAnswer').toggle('show');
			});
	});
	function afficheFiche(idFiche){
		//alert(idFiche);
		params = {
			"idFiche" : idFiche
		};
		var htmlContent= "";
        ajaxPost(
            null,
            baseUrl+"/costum/ficheprojet/getansweraction",
            params,
            function(data){
                mylog.log("successss", data);
                ficheProjet = (typeof data.projet2872020_1530_0 != "undefined") ? data.projet2872020_1530_0 : "" ;
                nomProjet = (typeof ficheProjet.projet2872020_1530_01 != "undefined" || ficheProjet.projet2872020_1530_01 != "null") ? ficheProjet.projet2872020_1530_01 : "";
                slogan = (typeof ficheProjet.projet2872020_1530_02 != "undefined")?ficheProjet.projet2872020_1530_02 : "";
                description = (typeof  ficheProjet.projet2872020_1530_04 != "undefined") ? ficheProjet.projet2872020_1530_04 : "";
                cibleCoeur = (typeof ficheProjet.projet2872020_1530_05 != "undefined") ? ficheProjet.projet2872020_1530_05 : "";
                cibleElargie = (typeof ficheProjet.projet2872020_1530_06 != "undefined") ? ficheProjet.projet2872020_1530_06 : "";
                commentaire = (typeof ficheProjet.projet2872020_1530_14 != "undefined") ? ficheProjet.projet2872020_1530_14 : "";


                //GRAPHISME
                graphisme = (typeof data.projet2972020_164_1 != "undefined") ? data.projet2972020_164_1 : "" ;

                // DEVELOPMENT
                dev = (typeof data.projet2972020_187_2 != "undefined") ? data.projet2972020_187_2 : "" ;
                objectif = (typeof dev.projet2972020_187_25 != "undefined") ? dev.projet2972020_187_25 : "" ;

                //ANIMATION
                animation = (typeof data.projet3072020_1652_3 != "undefined") ? data.projet3072020_1652_3 : "" ;
                publiCible = (typeof animation.projet3072020_1652_31 != "undefined") ? animation.projet3072020_1652_31 : "" ;
                dateAnimation = (typeof animation.projet3072020_1652_34 != "undefined") ? animation.projet3072020_1652_34 : "" ;

                htmlContent +=
                    '<div class="container">'+
                    '<div class="col-md-12 col-sm-12 col-xs-12 ">'+
                    '<h1 class="letter-turq text-center"> Fiche projet : '+ nomProjet+'</h1>'+
                    '<div class=" well ">'+
                    '<p class="paragraph text-center" >'+description+' </p>'+
                    '</div>'+
                    '</div>'+
                    '<div class="row">'+
                    '<div class="col-md-4 col-sm-4 col-xs-4 padding-20" >'+

                    '<div>'+
                    '<u><p>Slogan: </p></u>'+slogan +
                    '</div>'+
                    '<hr>'+
                    '<div >'+
                    '<u><p>Cible coeur: </p></u>'+cibleCoeur +
                    '</div>'+
                    '<hr>'+
                    '<div >'+
                    '<u><p>Cible élargie:</p></u>'+cibleElargie +
                    '</div>'+
                    '<hr>'+

                    '<div>'+
                    '<p> Actions à mettre en oeuvre : </p>';
                if(typeof ficheProjet.projet2872020_1530_11 != "undefined"){
                    $(ficheProjet.projet2872020_1530_11).each(function(key,value){
                        mylog.log("action",value.liste_row);
                        htmlContent+=
                            '<li> '+
                            value.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '<hr>'+
                    '<div>'+
                    '<p>Liste des tâches et besoins :</p>';
                if(typeof ficheProjet.projet2872020_1530_12 != "undefined" ){
                    $(ficheProjet.projet2872020_1530_12).each(function(keyTa,valueTa){
                        mylog.log("action",valueTa.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueTa.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-8 col-sm-8 col-xs-8 padding-20" >'+
                    '<p> Référents </p>'+
                    '<table>'+
                    '<tr>'+
                    '<th>Nom</th>'+
                    '<th>Email</th>'+
                    '<th>Type</th>'+
                    '</tr>';
                if (typeof ficheProjet.projet2872020_1530_03 != "undefined" ) {
                    $(ficheProjet.projet2872020_1530_03).each(function(keyR,valueR){
                        htmlContent +=
                            '<tr>'+
                            '<td>'+  valueR.nom +'</td>'+
                            '<td>'+  valueR.email +'</td>'+
                            '<td>'+ valueR.type +'</td>'+
                            '</tr>';
                    })
                }
                htmlContent +=
                    '</table>'+

                    '<p > Budget global : </p>'+
                    '<table>'+
                    '<tr>'+
                    '<th>Groupe</th>'+
                    '<th>Nature de l\'action</th>'+
                    '<th>Poste de dépense</th>'+
                    '<th>Montant</th>'+
                    '</tr>';
                if (typeof ficheProjet.projet2872020_1530_07 != "undefined" ) {
                    $(ficheProjet.projet2872020_1530_07).each(function(keyB,valueB){
                        htmlContent +=
                            '<tr>'+
                            '<td>'+  valueB.group +'</td>'+
                            '<td>'+  valueB.nature +'</td>'+
                            '<td>'+ valueB.poste +'</td>'+
                            '<td>'+ valueB.price +'</td>'+
                            '</tr>';
                    })
                }
                htmlContent +=
                    '</table>'+
                    '</div>'+
                    '</div>'+
                    '<div class=" well ">'+
                    '<p class="label-question">Commentaires de la personne ayant lancé le projet:</p>'+
                    '<p class="paragraph">'+ commentaire +'</p>'+
                    '</div>'+

                    // Spécifique pour Graphisme

                    '<h4 >Spécifique pour Graphisme</h4>'+
                    '<p> Charte graphique:'+
                    //getFile(idFiche , "projet2972020_164_1.projet2972020_164_11");
                    //mylog.log ("charteee",charteGraphisme);
                    '</p>'+

                    '<div class="row">'+
                    '<div class="col-md-4">'+
                    '<div  class="card1"  >'+
                    '<div class="col-md-11">'+
                    '<p>Tonalité :</p>';
                if(typeof graphisme.projet2972020_164_12 != "undefined" ){
                    $(graphisme.projet2972020_164_12).each(function(keyTo,valueTo){
                        mylog.log("Tonalite",valueTo.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueTo.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-8">'+
                    '<div  class="card1"  >'+
                    '<p>Valeurs à transmettre:</p>';
                if(typeof graphisme.projet2972020_164_13 != "undefined" ){
                    $(graphisme.projet2972020_164_13).each(function(keyTrans,valueTrans){
                        mylog.log("valeur à transmettre",valueTrans.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueTrans.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+

                    '<p> Si des éléments existent déjà, qu’est-ce qui doit être gardé ?</p>'+

                    '<div class="row">'+
                    '<div class="col-md-6">'+
                    '<div  class="card1"  >'+
                    '<div class="col-md-11">'+
                    '<p> Eléments récurrents :</p>';
                if(typeof graphisme.projet2972020_164_15 != "undefined" ){
                    $(graphisme.projet2972020_164_15).each(function(keyRe,valueRe){
                        mylog.log("Eléments récurrents",valueRe.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueRe.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+

                    '<div class="col-md-6">'+
                    '<div  class="card1"  >'+
                    '<p> Eléments qui peuvent disparaître : </p>';
                if(typeof graphisme.projet2972020_164_16 != "undefined" ){
                    $(graphisme.projet2972020_164_16).each(function(keyDisp,valueDisp){
                        mylog.log("Eléments qui peuvent disparaître",valueDisp.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueDisp.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+


                    '<div class="row">'+
                    '<div class="col-md-6">'+
                    '<div  class="card1"  >'+
                    '<div class="col-md-11">'+
                    '<p>Idée qu\'on peut voir en visuel:</p>';
                if(typeof graphisme.projet2972020_164_17 != "undefined" ){
                    $(graphisme.projet2972020_164_17).each(function(keyIdee,valueIdee){
                        mylog.log("Idée qu'on peut voir en visuel",valueIdee.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueIdee.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'
                '<div  class="card1"  >'
                '<p>Elément à fournir:</p>'
                if(typeof graphisme.projet2972020_164_18 != "undefined" ){
                    $(graphisme.projet2972020_164_18).each(function(keyElm,valueElm){
                        mylog.log("Idée qu'on peut voir en visuel",valueElm.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueElm.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+


                    //Spécifique pour les développement
                    '<h4> Spécifique pour les développement</h4>'+
                    '<div class="row">'+
                    '<div class="col-md-4">'+
                    '<p> Logo : </p>'+
                    // <?php //foreach ($logo as $key => $value) {
                    // 	echo "<img src='".$value["docPath"]."'height=250>";
                    // }?>

                    '</div>'+
                    '<div class="col-md-8">'+
                    '<p>Ressources graphiques : '+
                    // <?php //foreach ($ressourceGraphique as $keyG => $valueG) {
                    // 	echo "<a src='".$valueG["docPath"]."'> ".$valueG["name"]."
                    // 	</a>";
                    // }?>

                    '</p>'+
                    '<p>Contenu :' +
                    // <?php //foreach ($contenu as $keyC => $valueC) {
                    // 	echo "<a src='".$valueC["docPath"]."'> ".$valueC["name"]."
                    // 	</a>";
                    // }?>

                    '</p>'+
                    '</div>'+
                    '</div>'+
                    '<div class="row">'+
                    '<div class="col-md-6">'+
                    '<div  class="card1"  >'+
                    '<div class="col-md-11">'+
                    '<p> Modules nécessaires :</p>';
                if(typeof dev.projet2972020_187_21 != "undefined" ){
                    $(dev.projet2972020_187_21).each(function(keyM,valueM){
                        mylog.log("Modules nécessaires",valueM.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueM.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                    '<div  class="card1"  >'+
                    '<p>Objectif :'+ objectif +'</p>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+


                    //Spécifique pour des animations
                    '<h4>Spécifique pour des animations</h4>'+
                    '<div class="row">'+
                    '<div class="col-md-4 col-sm-4 col-xs-4 padding-20" >'+
                    '<div>'+
                    '<p>Date de l\'animation: '+ dateAnimation +'  </p>'+
                    '</div>'+
                    '<hr>'+
                    '<div>'+
                    '<p>Public cible: '+ publiCible +'</p> '+
                    '</div>'+
                    '<hr>'+
                    '</div>'+
                    '<div class="col-md-8 col-sm-8 col-xs-8 padding-20">'+
                    '<p>Moyens matériels:</p>';
                if(typeof animation.projet3072020_1652_32 != "undefined" ){
                    $(animation.projet3072020_1652_32).each(function(keyMm,valueMm){
                        mylog.log("Modules nécessaires",valueMm.liste_row);
                        htmlContent+=
                            '<li> '+
                            valueMm.liste_row
                        '</li>';
                    });
                }
                htmlContent +=
                    '</div>'+
                    '</div>';


                $("#ficheProjet").html(htmlContent);
            },
            null,
            "json",
            {async : false}
        );


		$('#liste').toggle('show');
	}
	function getFile(id, subKey){
		params = {
			"idFiche" : id,
			"subKey" : subKey
		};
		var htmlContent= "";
        ajaxPost(
            null,
            baseUrl+"/costum/ficheprojet/getfileaction",
            params,
            function(data){
                mylog.log("listefile", data);
            },
            null,
            "json",
            {async : false}
        );
	}
</script>

</div>