<?php 

$hashTagsExcluded=array("chiffre","citation");

$carousel_indicators_color="#C9462C";

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    //MARKDOWN
    '/plugins/to-markdown/to-markdown.js',              
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
/*$cssAnsScriptFilesModule = array(
    '/js/classes/CO3_Element.js',
    '/js/classes/CO3_Obj.js',
    '/js/classes/CO3_TypeObj.js',
    '/js/classes/CO3_Poi.js',
    '/js/classes/CO3_Article.js',
    '/js/classes/CO3_Event.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());*/
$chiffre=Poi::getPoiByWhereSortAndLimit(array("tags"=>array("chiffre"), "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 1, 0);

$citation=Poi::getPoiByWhereSortAndLimit(array("tags"=>array("citation"), "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 1, 0);
    
$evenements=Event::getEventByWhereSortAndLimit(array("startDate"=>array( '$gte' => new MongoDate(microtime(true))), "source.key"=>"journalInsoumisChambery"),array("startDate" => 1),3,0);

if(count($evenements)<3){
  $evenements=array_merge($evenements,Event::getEventByWhereSortAndLimit(array("startDate" => array( '$lte' => new MongoDate(microtime(true))), "source.key"=>"journalInsoumisChambery"),array("startDate" => -1),3-count($evenements),0));
}

//$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true"),array("updated"=>-1), 3, 0);
$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true","notags"=>$hashTagsExcluded, "source.key"=>"journalInsoumisChambery"),array("updated"=>-1), 3, 0);
//$articles=Poi::getPoiByWhereSortAndLimit(array('$or'=>array(array("rank"=>array('$exists'=>false)),array("rank"=>"false"))),array("updated"=>-1), 5, 0); 

$articles=Poi::getPoiByWhereSortAndLimit(array('$and'=>array(array("notags"=>$hashTagsExcluded, "source.key"=>"journalInsoumisChambery"))),array("updated"=>-1), 10, 0); 


$nb_articles=0;
foreach ($articles as $key => $value) {
  if($nb_articles>7){
    unset($articles[$key]);
  } else {
    foreach ($articles_une as $key_une => $value_une) {
      if(strcmp($key, $key_une)===0){
        unset($articles[$key]);
        break;
      }
    }
    if(isset($articles[$key])){
      $nb_articles=$nb_articles+1;
    }
  }
}

?>

<div id="a2k_page" class="w-100 mx-auto">

  <div id="a2k_main_conteneur" class="w-100">
    <div class="w-100 mw1000 mx-auto">
      <div id="a2k_left-column" class="col-sm-12 col-md-8 top-separator right-separator pl-0 pr-0">
        <div class="bloc_rubrique w-100 p-30">
          <div class="titre_rubrique w-100 b maj">
             A la une
          </div>         
          <div id="une_container" class="contenu_rubrique w-100">  
            <?php
            echo $this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"articleCarousel","nbItem"=>count($articles_une),"carousel_indicators_color"=>"#C9462C")); 
            ?>
          </div>       
        </div>
        <div class="bloc_rubrique w-100 top-separator my-0">
          <div class="col-xs-12 col-sm-6 right-separator p-30">
            <div class="titre_rubrique w-100 b maj">
               Chiffre du jour
            </div>
            <div id="chiffre_container" class="contenu_rubrique w-100">
              
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 left-separator p-30" style="margin-left : -2px;">
            <div class="titre_rubrique w-100 b maj">
               Citation du jour
            </div>
            <div id="citation_container" class="contenu_rubrique w-100">
              
            </div>
          </div>
        </div>
        <div class="bloc_rubrique w-100 top-separator p-30">
          <div class="titre_rubrique w-100 b maj">
             Agenda
          </div>
          <div id="agenda_container" class="contenu_rubrique w-100">
            <?php
            echo $this->renderPartial('co2.views.pod.sliderGeneric',array("idCarousel"=>"agendaCarousel","nbItem"=>count($evenements),"carousel_indicators_color"=>"#C9462C")); 
            ?>
          </div>
        </div>
      </div>
      <div id="a2k_right-column" class="col-sm-12 col-md-4 top-separator left-separator pl-0 pr-0" style="margin-left : -2px;">
        <div class="bloc_rubrique w-100 p-30">
          <div class="titre_rubrique w-100 b maj">
             Derniers articles
          </div>
          <div id="last_container" class="contenu_rubrique w-100">
          </div>
        </div>
      </div>
      <div id="a2k_full-column" class="col-xs-12 top-separator p-30">
        <div class="titre_rubrique w-100 b maj">
          Organisations amies
        </div>
        <div id="friend_container" class="contenu_rubrique mx-auto">
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://franceinsoumise.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-fi.png" alt="La France Insoumise">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('https://nupes-2022.fr/','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo_NUPES.png" alt="La NUPES">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.linsoumissionhebdo.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-hdp.png" alt="L'Heure du Peuple">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://avenirencommun.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-aec.png" alt="L'Avenir en Commun">
          </div>        
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.lepartidegauche.fr','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-pdg.png" alt="Le Parti de Gauche">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.fakirpresse.info','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-fp.png" alt="Fakir Presse">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('http://www.amd-savoie.com','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-amd-savoie.jpg" alt="Les Amis du Monde Diplomatique Savoie">
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3 friend" onclick="window.open('https://www.facebook.com/attac73/','_blank')">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/logo-attac-savoie.jpg" alt="Attac Savoie">
          </div>
        </div>
        <div class="titre_rubrique w-100 b maj margin-top-50">
          Nos élu⋅e⋅s
        </div>
        <div class="contenu_rubrique mx-auto">
          <div class="col-xs-4 col-sm-3 col-md-3" style="cursor:pointer;" onclick="window.open('https://www.facebook.com/profile.php?id=100084685325583','_blank')">
            <h3 class="col-xs-12">
              <span style="color:#46b0cb;font-size:18px;">Jean-François</span><br/> 
              <span class="bold" style="color:#e43b12;">Coulomme</span>
            </h3>
            <span class="col-xs-12">Député 4ème circo Savoie</span>
          </div>
           <div class="col-xs-4 col-sm-3 col-md-3" style="cursor:pointer;" onclick="window.open('https://www.facebook.com/emilie.narvaline.1','_blank')">
            <h3 class="col-xs-12">
              <span style="color:#46b0cb;font-size:18px;">Emilie</span><br/> 
              <span class="bold" style="color:#e43b12;">Marche</span>
            </h3>
            <span class="col-xs-12">Conseillère régionale AURA</span>
          </div>
           <div class="col-xs-4 col-sm-3 col-md-3" style="cursor:pointer;" onclick="window.open('https://www.facebook.com/profile.php?id=100017044410704','_blank')">
            <h3 class="col-xs-12">
              <span style="color:#46b0cb;font-size:18px;">Magali</span><br/> 
              <span class="bold" style="color:#e43b12;">Romaggi</span>
            </h3>
            <span class="col-xs-12">Conseillère régionale AURA</span>
          </div>
          <div class="col-xs-4 col-sm-3 col-md-3">
            <h3 class="col-xs-12">
              <span style="color:#46b0cb;font-size:18px;">Gabriel</span><br/> 
              <span class="bold" style="color:#e43b12;">Amard</span>
            </h3>
            <span class="col-xs-12">Député 6ème circo Rhône<br/>Conseiller régional AURA</span>
          </div>
        
         </div>
      </div> 
    </div>
  </div>
  <div id="a2k_footer" class="w-100">
      <a href="mailto:contact@journal-insoumis-chambery.com" style="color:#ffffff;">Contact</a> - <span id="mentionsLegalesBtn" style="cursor:pointer;" onclick="alert('Responsable de la publication : LARIGUET Georges');">Mentions Légales</span>
      <script>
        $("#mentionsLegalesBtn").mouseenter(function(){$(this).css('text-decoration','underline');}).mouseleave(function(){$(this).css('text-decoration','none');});
      </script>
  </div>
</div>

<script type="text/javascript">
var A2K_articles_une = null;
var A2K_articles = null;
var A2K_evenements = null;
var A2K_chiffre = null;
var A2K_citation = null;

function initWelcome(){
  let socialBarConfig={btnList:[{type:'twitter'},{type:'facebook'},{type:'co'}],btnSize:16};

  A2K_articles_une = new CO3_Article(null,<?php echo json_encode($articles_une); ?>);
  A2K_articles = new CO3_Article(null,<?php echo json_encode($articles); ?>);
  A2K_evenements = new CO3_Event(null,<?php echo json_encode($evenements); ?>);
  A2K_chiffre = new CO3_ArticleBadge(null,<?php echo json_encode($chiffre); ?>);
  A2K_citation = new CO3_ArticleBadge(null,<?php echo json_encode($citation); ?>);
  $("#articleCarousel .carousel-inner").html(A2K_articles_une.SetCarousable().SetSocialBarOn(socialBarConfig).SetDisplayImg("full").RenderHtml());
  $("#last_container").html(A2K_articles.SetSocialBarOn(socialBarConfig).SetDisplayImg("none").RenderHtml());
  
  $("#chiffre_container").html(A2K_chiffre.RenderHtml());
  $("#citation_container").html(A2K_citation.RenderHtml());

  $("#agendaCarousel .carousel-inner").html(A2K_evenements.SetCarousable().SetSocialBarOn(socialBarConfig).RenderHtml());
      
  jsOnLoad();        
  setTimeout(function() {jsOnLoad();}, 1000); 
  setTimeout(function() {jsOnLoad();}, 5000);
  //directory.checkImage=function(){return null;};
}

function resizeCarousels(){
  CO3_Article.forceListImRatio("#articleCarousel .container-img-profil",16/9,CO3_Article.carouselGetActiveWidth("#articleCarousel"));
  CO3_Event.forceListImRatio("#agendaCarousel .container-img-profil",1,CO3_Event.carouselGetActiveWidth("#agendaCarousel"));
  //forceListImRatio("#last_container .container-img-profil",16/9);
}

jQuery(window).resize(function() {
  resizeCarousels();  
});

function jsOnLoad(){
  coInterface.bindLBHLinks();
  directory.bindMediaSharingElt(); 
  resizeCarousels()
  start_articleCarousel();
  start_agendaCarousel();
}

function lazyWelcome(time){
  if(typeof CO3_Article != "undefined" && typeof CO3_ArticleBadge != "undefined")
    initWelcome();
  else
    setTimeout(function(){
      lazyWelcome(time+200)
    }, time);
} 

jQuery(document).ready(function() {
  setTitle("Journal de la FI Chambéry");
  lazyWelcome(0);
  // TODO arriver à choper le window on load !!!!
});

</script>


