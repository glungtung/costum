<!--
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
-->
<style type="text/css">
	.mh150{
    max-height:150px;
    }
    .h150{
    height:150px;
    }
    #headerBand{
    	z-index:127000;
    }
</style>
<div class="w-100 header-costum">
	<a href="#welcome" class="lbh">
		<div class="w-100 mw1000 mx-auto my-3" style="margin-top:1em;margin-bottom:1em;">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/journalInsoumisChambery/banner_jic.png" class="mh150 img-responsive mx-auto"/>
		</div>
	</a>

<div id="a2k_nav_rubrique" class="w-100" style="min-height:40px;">
    <div class="w-100 mw1000 mx-auto">
		<!-- iL FAUT BOOTSTRAP 4 BORDEL DE MERDE
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
	  	</button>
    	<div class="collapse navbar-collapse" id="navbarNav">
    	-->
	        <ul class="nav justify-content-center nav-pills"> 
	       		<li class="nav-item">
		          <a class="nav-link lbh" href="#">Accueil</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link lbh" href="#article?tags=grand%20chambery">Grand Chambéry</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link lbh" href="#article?tags=grand%20dossier">Grands Dossiers</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link lbh" href="#article?tags=actu%20politique">Actus Politiques</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link lbh" href="#article?tags=sujet%20debat">Sujets Débat</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link lbh" href="#article?tags=breve">Brèves</a>
		        </li>
	        </ul>
	    <!--
	    </div>
		-->
    </div>
</div>
</div>