<style type="text/css">
     .well-fiche {
        font-size: 12px;
        border-radius: 15px;
        word-break: break-word;
    }
    .label-fiche-title{
        text-align: center;
        color :  #275283;
        font-weight : bold;
        font-size: 14px;

    }
    .label-fiche1{
        color :  #093081;
        font-weight : bold;
        font-size : 14px;
    }
    a {
       color : #275283;
    }
    .fichePdf{
        background-color :#f8f8f8;
    }
    .text-blue-fiche{
        color: #093081;
        font-size : 12px;
        font-weight : 700;
    }
</style>
<div class="fichePdf"> 
    <table   cellspacing="2" cellpadding="2" >
        <tr>
            <td colspan="2" >
                
                <p class="text-blue-fiche" style="font-size:20px;text-align:center" ><b><i><?=$element["name"]?></i></b></p>
                <div style="text-align:center" >
                    <?php if (isset($element["jobFamily"])) {
                        if(is_array($element["jobFamily"])){
                            $img ="";
                            foreach($element["jobFamily"] as $kjobf => $vjobf){
                                if( $vjobf ==  "Accompagnement - Support")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Accompagnement.png";
                                if( $vjobf == "Financement")
                                        $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/financement.png";
                                if( $vjobf == "Mise en réseau")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Reseau.png";
                                if( $vjobf == "Tiers Lieux")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/TiersLieux.png";
                                if( $vjobf == "Formation")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Formation-recherche.png";
                                if( $vjobf == "Pilotage")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Pilotage.png";
                                if( $vjobf == "Structures de diffusion de technologies")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Transfert-de-technologie.png";
                                ?>
                                <img   style="height: 40px; width:40px ; " src = "<?=$img?>">
                                <?php
                            }
                        }else {?>
                            <span class="text-blue-fiche ">
                                <?php
                                if($element["jobFamily"] ==  "Accompagnement - Support")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Accompagnement.png";
                                if( $element["jobFamily"] == "Financement")
                                        $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/financement.png";
                                if( $element["jobFamily"] == "Mise en réseau")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Reseau.png";
                                if( $element["jobFamily"] == "Tiers Lieux")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/TiersLieux.png";
                                if($element["jobFamily"] == "Formation")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Formation-recherche.png";
                                if( $element["jobFamily"] == "Pilotage")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Pilotage.png";
                                if( $element["jobFamily"] == "Structures de diffusion de technologies")
                                    $img = Yii::app()->getModule("costum")->assetsUrl."/images/meir/icone/Transfert-de-technologie.png";
                                ?>
                                <img   style="height: 40px; width:40px ; " src = "<?=$img?>">
                            </span>  
                        <?php }
                    }       
                    ?>

                    <?php if(isset($element["thematic"]) && isset($element["category"]) && $element["category"] == "startup"){?>
                        
                            <?php if(is_array($element["thematic"])){
                                foreach($element["thematic"] as $kjobf => $vjobf){?>
                                    <span class="text-blue-fiche"> <?= $vjobf?></span>
                                <?php }?>
                            <?php }else {?>
                                <span class="text-blue-fiche"> <?= $element["thematic"]?></span>                           
                            <?php } ?>
                    <?php } ?>
                </div>                    
            </td>
            
            <td colspan="2" style="text-align:right;float:right">
                <br><br>
            <?php 
                if(isset($element["profilImageUrl"])) 
                    $src = $element["profilImageUrl"];
                else 
                    $src =  Yii::app()->getModule("costum")->assetsUrl."/images/meir/TIERS-LIEUX.png";
                ?>
                <img  style="height: auto; width: auto;object-fit: contain;object-position: center; " src = "<?= $src?>">
            </td>            
        </tr>
        <tr>
            <td colspan="2" style=" background-color: #90b2f8 ;">
            <small class="label" style="color:#093081;font-weight:bold;font-size:14px;">Informations administratives</small>
                    <?php if (isset($element["address"])){?>
                        <p class ="text-blue-fiche">Adresse : 
                        <?php if (isset($element["address"]["streetAddress"] ))	
                                echo $element["address"]["streetAddress"]." ";
                            echo $element["address"]["postalCode"]." ".$element["address"]["addressLocality"];	
                        ?></p> 
                    <?php }?>
                    <?php  if (isset($element["telephone"])){?>
                        <p class ="text-blue-fiche">Téléphone :  <?php
                            $replaced = str_replace(' ', '', $element["telephone"]["mobile"][0]);
                            if (strpos($element["telephone"]["mobile"][0],"+") !== false){
                                $result = sprintf("%s %s %s %s %s %s",
                                substr($replaced, 0, 4),
                                substr($replaced, 4, 1),
                                substr($replaced, 5, 2),
                                substr($replaced, 7, 2),
                                substr($replaced, 9, 2),
                                substr($replaced, 11));

                            } else{
                                $result = sprintf("%s %s %s %s %s %s",
                                substr($replaced, 0, 2),
                                substr($replaced, 2, 2),
                                substr($replaced, 4, 2),
                                substr($replaced, 6, 2),
                                substr($replaced, 8, 2),
                                substr($replaced, 10));
                            }
                                
                            echo $result; 
                       
                        ?>
                    </p>
                    <?php } ?>
                    <?php if (isset($element["email"])) { ?>
                        <p class ="text-blue-fiche">Mail : <?php 
                        echo $element["email"]; ?> </p>
                    <?php } ?>
                    <?php if (isset($element["link"])) { ?>
                        <p class ="text-blue-fiche">Lien :  <span class="markdown"><?php 
                                echo $element["link"]; 
                            ?> </span> 
                        </p>
                    <?php } ?>
                    <?php if(isset($element["category"]) && $element["category"] == "startup" && isset($element["yearEntryInpri"])){ ?>
                        <p class ="text-blue-fiche">Année d'entrée au sein de l'Incubateur de la Recherche Publique :  
                            <?php  echo $element["yearEntryInpri"]; ?>
                        </p>
                    <?php } ?> 
            </td>
            <td colspan="2" style=" background-color: #ffffff;    border: 2px solid #e3e3e3;">   
            <br><br> 
                <?php if(isset($element["category"]) && $element["category"] == "acteurMeir"){?>     
                    <small class="label" style="color:#093081;font-weight:bold;font-size:14px;">Informations juridiques </small>
                    <?php  if (isset($element["legalStatus"])) {?> <p class ="text-blue-fiche">Statut Juriqique : <?php echo $element["legalStatus"];?>  </p>  <?php } ?> 
                    <?php if (isset($element["responsable"]))  {?><p class ="text-blue-fiche">Responsable : <?php echo $element["responsable"]; ?></p>  <?php } ?> 
                <?php }else if(isset($element["category"]) && $element["category"] == "startup"){?>
                    <?php  if (isset($element["areaOfIntervention"]))  {?><p class ="text-blue-fiche">Domaine d'intervention :  <?php echo $element["areaOfIntervention"];?>  </p> <?php } ?>  
                    <?php if (isset($element["namesStartupers"])) {?>    
                        <p class ="text-blue-fiche">Nom(s) du(es) Startuper(uses)s : 
                            <?php 
                                    foreach($element["namesStartupers"] as $kName => $vName){
                                        if(isset($vName["nameStartuper"])){
                                            echo "<br><span class='text-blue-fiche'>-". $vName["nameStartuper"]. "</span> ";
                                        }; 
                                    }
                            ?> 
                        </p>  
                    <?php } ?>
                    <?php if (isset($element["objectiveOdd"])) { ?>
                        <p class ="text-blue-fiche">Objectif(s) ONUSIEN de Développement Durable : 
                            <?php 
                                if(is_array($element["objectiveOdd"])){
                                    foreach($element["objectiveOdd"] as $kodd => $vOdd){
                                        echo "<br><span class='text-blue-fiche'>-". $vOdd. "</span>";
                                    }
                                }else {
                                    echo $element["objectiveOdd"];
                                }
                            ?>
                        </p>  
                    <?php } ?>
                <?php } ?>
            </td>
        </tr>
    </table>
    <?php if(isset($element["category"]) && $element["category"] == "acteurMeir"){?>
        <?php if (isset($element["objective"])){?>
            <p class="label label-fiche-title " >Objectif(s)</p>    
            <p class ="text-blue-fiche" style="text-align:justify;  font-size:12px"><?php  echo   str_replace("\n","<br>",$element["objective"]); ?> </p>
        <?php } ?>
    
        <?php if (isset($element["serviceOffers"])){?>
            <p class="label label-fiche-title " >Offres de services</b>
            <p class ="text-blue-fiche" style="text-align:justify;  font-size:12px"><?php  echo str_replace("\n","<br>",$element["serviceOffers"]); ?> </p>
        <?php } ?>
        <?php if(isset($element["thematic"]) && isset($element["category"]) && $element["category"] == "acteurMeir"){?>
            <p class="label label-fiche-title " >Secteurs d'activités</b>                       
            <?php if(is_array($element["thematic"])){
                foreach($element["thematic"] as $kjobf => $vjobf){?>
                    <br><span class="text-blue-fiche"> - <?= $vjobf?></span>
                <?php }?>
            <?php }else {?>
                <span class="text-blue-fiche"> <?= $element["thematic"]?></span>                           
            <?php } ?>
            </p>
        <?php } ?>
       
        <?php if(isset($element["jobFamily"]) && ($element["jobFamily"] == "Financement" || (is_array($element["jobFamily"]) && in_array("Financement", $element["jobFamily"])))){?>           
            <?php if (isset($element["linkFinancialDevice"])) {?>
                <p class="label label-fiche-title " >Lien(s) dispositif(s) financier(s)</p>            
                <ul class="well-fiche ">
                    <?php foreach($element["linkFinancialDevice"] as $kLink => $vLink){                ?>
                        <li class="well-fiche text-blue-fiche"><?php if (isset($vLink["financialDevice"])) {?>  <a href="<?= $vLink["link"]?>" target="_blank"><?= str_replace("\n","<br>",$vLink["financialDevice"]); }?></a></li>
                    <?php }?><br>
                </ul>
            <?php  } ?>
            <table border="0" cellspacing="10" cellpadding="10">
                <tr>
                    <?php if (isset($element["financialPartners"])) {?>
                        <td colspan="2" style=" background-color: #ffffff;    border: 2px solid #e3e3e3;">
                            <div class="well well-fiche " >
                                <small class="label label-fiche1 ">Partenaire(s) financier(s)</small> 
                                    <ul class="well-fiche ">
                                        <?php 
                                            foreach($element["financialPartners"] as $kFin => $vFin){
                                            ?>
                                                <li class="well-fiche text-blue-fiche"><b> <?php if (isset($vFin["financialPartner"])) echo str_replace("\n","<br>",$vFin["financialPartner"]); ?></b></li>
                                            <?php }  ?>
                                    </ul>
                            </div>
                        </td>
                    <?php }  ?>
                    <td colspan="2" style=" background-color: #ffffff;    border: 2px solid #e3e3e3;">
                        <?php if (isset($element["modality"])){?><p class ="text-blue-fiche"><b>Modalités : </b><?php echo $element["modality"];?></p><?php } ?>
                        <?php if (isset($element["typeFinancing"])){?><p class ="text-blue-fiche"><b>Type de financement : </b><?php echo $element["typeFinancing"];?></p><?php } ?>
                        <?php if (isset($element["maximumAmount"])){?><p class ="text-blue-fiche"><b>Montant maximal : </b><?php echo $element["maximumAmount"];?></p><?php } ?>
                        <?php if (isset($element["publicCible"])){ ?> <p class ="text-blue-fiche"><b>Public cible : </b> 
                            <?php if(is_array($element["publicCible"])){
                                foreach($element["publicCible"] as $kpc => $vpc){?>
                                    <br><span class="text-blue-fiche"> - <?= $vpc?></span>
                                <?php }?>
                            <?php }else {?>
                                <span class="text-blue-fiche"> <?= $element["publicCible"]?></span>                           
                            <?php } ?>
                            </p>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        <?php } ?>
    <?php } ?>
    <?php if(isset($element["category"]) && $element["category"] == "startup" && isset($element["description"])){?>
            <table border="0" cellspacing="10" cellpadding="10">
                <tr>
                    <td collspan="1">
                        <p class="label label-fiche-title " >Offres de services</p>    
                        <p class ="text-blue-fiche" style="text-align:justify;  font-size:12px"><?php  echo   str_replace("\n","<br>",$element["description"]); ?> </p>
                    </td>
                </tr>
            </table>
       <?php }?>
       <div style="text-align: right; float: right;">
            <img   style="height: 67px; width:176px ; " src = "<?= Yii::app()->getModule("costum")->assetsUrl."/images/meir/logo.png"?>">
       </div>

</div> 
    
    