<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>

<style>
	#filters-nav{
		padding-top: .6em !important;
	}

	#filterContainer #input-sec-search .input-global-search, .searchBar-filters .search-bar, #filterContainer #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
		margin-top:0px !important;
		
	}

	.searchObjCSS .searchBar-filters {
		margin-top: 5px;
	}

</style>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var thematic = null;
	var typeFinancing = null;
	var greatFamily = null;
	var statu = null;
	/*
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.secteur != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.secteur);
	}
	*/
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.themes != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.themes);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.accompanimentTypeFilter != "undefined"){
		greatFamily = {};
		Object.assign(greatFamily, costum.lists.accompanimentTypeFilter);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.status != "undefined"){
		statu  = {};
		Object.assign(statu , costum.lists.status);
	}
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.typeFinancing != "undefined"){
		typeFinancing  = {};
		
		Object.assign(typeFinancing , costum.lists.typeFinancing);
	}

	var defaultScopeList = ["RE"];

	var defaultFilters = {'$or':{}};
    defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["source.keys"] = costum.slug;
    defaultFilters['$or']["reference.costum"] = costum.slug;
    defaultFilters['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
    defaultFilters["toBeValidated"]={'$exists':false};
    defaultFilters["category"] = "acteurMeir";
	let paramString = pageApp.split('?')[1];
	let queryString = new URLSearchParams(paramString);

	for (let pair of queryString.entries()) {
		if(pair[0] != "tags"){
			defaultFilters[pair[0]]=pair[1].replace("%", "");
		}else{ // remove this if & are removed from tags
			searchObj.search.obj.tags = [];
		}
	}

    /*defaultFilters['$or']['$and'] = [
    	{"tags" = ["numérique"]}
    ];*/

	var paramsFilter= {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
		 	types : ["organizations"],//(appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		 	fields: ["name","objectiveOdd", "category", "type", "typeFinancing"],
		 	filters: defaultFilters,
			sortBy: {"name": 1}
		},
		filters : {
			// secteurFamily:{
			// 	view : "megaMenuDropdown",
	 		// 	type : "tags",
	 		// 	remove0: false,
	 		// 	countResults: true,
	 		// 	name : "<?php //echo Yii::t("common", "search by theme")?>",
	 		// 	event : "tags",
	 		// 	keyValue: true,
	 		// 	list : {"secteurs":thematic, "Familles":greatFamily}
			// },
			status : {
				view : "dropdownList", 
		 		type : "tags",
				name : "Je suis",
				event : "tags",
	 			keyValue: false,
				list : statu		
			},
		 	family : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : "Je cherche",
	 			keyValue: false,
				event : "tags",
		 		list : greatFamily
		 	},
			
		 	secteur : {
	 			view : "dropdownList",
	 			type : "tags",
	 			countResults: true,
	 			name : "Secteurs",
				event : "tags",
	 			keyValue: true,
	 			list : thematic
	 		}, 
		 	typeFinancing : {
	 			view : "dropdownList",
	 			type : "tags",
				trad:false,
				field : "typeFinancing",
	 			name : "Type de financements",
				event : "tags", 
	 			list : typeFinancing
	 		},
 			/*scopeList : {
	 			name : "<?php //echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList, 
	 				level : ["3"]
	 			}
	 		},
	 		
			category : {
				view : "dropdownList",
				type : "category",
				name : "Catégorie",
				event : "filters",
				keyValue : false,
				list : {
					"startup" : "Startups",
					"acteurMeir" : "Acteurs du mapping"
				}
			},*/
	 		text : {
				placeholder : "Quel acteur recherchez-vous ?"
			}
	 	},
	 	results : {
			renderView: "directory.elementPanelHtmlFullWidth",
		 	smartGrid : false
		}
	}

	if(pageApp == "projects"){
		delete paramsFilter.filters["type"];
	}

	if(thematic==null || pageApp=="projects"){
		delete paramsFilter.filters["secteur"];
	}

	if(pageApp=="projects"){
		delete paramsFilter.filters["families"];
	}
	
	jQuery(document).ready(function() { 
		if(typeof paramsFilter.defaults.filters != "undefined" && typeof paramsFilter.defaults.filters.typeFinancing != "undefined"){  
			paramsFilter.defaults.filters.typeFinancing = paramsFilter.defaults.filters.typeFinancing.split(',');
		}
		filterSearch = searchObj.init(paramsFilter);
		
		//$("div[data-key*='etC']").hide()
		$(".count-badge-filter").remove();

		$(".searchBar-filters").append("<div class='text-right hidden-xs' style='padding-left: 24px;'><a href='javascript:;' class='initialisSearch btn '>Nouvelle recherche</a></div>");
		$(".menu-filters-xs").append("<div class='text-right hidden-lg hidden-md  visible-xs' style=''><a href='javascript:;' class='initialisSearch btn '>Nouvelle recherche</a></div>");
		/*$("#filterContainerInside").append(`
			<button id="toggleDisplayMode" type="button" class="btn text-dark float-right margin-top-5" style="" title="affichage par ligne"><i class="fa fa-list"></i></button>
		`);

		$("#toggleDisplayMode").click(function(e){
			if(filterSearch.results.renderView == "directory.elementPanelHtmlFullWidth"){
				filterSearch.results.renderView = "directory.classifiedPanelHtml";
				filterSearch = filterSearch.init({results:{renderView:"directory.classifiedPanelHtml"}});
			}else{
				filterSearch.results.renderView = "directory.elementPanelHtmlFullWidth";
				filterSearch = filterSearch.init({results:{renderView:"directory.elementPanelHtmlFullWidth"}});
			}
		})*/

		$(".initialisSearch").on("click", function(){
			urlCtrl.loadByHash("#search");
		})
		$(".filters-activate[data-key='startup'], .filters-activate[data-type='objectiveOdd']").on("click", function(){
			urlCtrl.loadByHash(location.hash);
		})
		
	});

</script>