<?php 
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array( 
  '/css/graphbuilder.css',
  '/js/form.js',
  '/js/dashboard.js'
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

echo $this->renderPartial("costum.views.custom.cordCollaborativeOpenRd.dashboardstyle", []);
?>
<!-- #ebebeb -->
<div class="ocecoform-body">
    <div class="col-md-12" style="margin-bottom: 40px; ">
        <div class="col-md-offset-2 col-md-8">
            <h1 class="" style="text-transform: none; text-align: center">Observatoire de l'organisation <?php echo (isset($title) ? $title : "" ) ?> </h1>
        </div>
    </div>

    <div class="col-md-12" id="bodygraph" >
      
        <!-- level 01 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->
              <div class="survey-item squareTile flex1 visiblehovercontainer grid-stack-item dangerSquare" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >

                        <div class="tilelabel ocecotitle">
                                <i class="fa fa-3 fa-clock-o warning" aria-hidden="true"></i>
                        </div>

                        <div class="ocecotitle step3" id="totalTasks">0</div>

                        <div class="tilelabel ocecotitle" style="">
                              Projet
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              en rétard
                        </div>

                    </div>
              </div>

              <div class="survey-item headerTile flex8 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="ocecotitle step" style="">
                          Propositions
                          <!-- <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="72"
                              aria-valuemin="0" aria-valuemax="100" style="width:69%">
                                <span class="sr-only">79% Complete</span>
                              </div>
                            </div> -->
                        </div>

                        <div id="" class="divitemheader">
                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Proposer</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div> -->
                                <div class="ocecotitle state success">Au total</div> 
                                <div class="ocecotitle step3" id="totalProp"></div>
                                <div style="margin: 5px; text-align: center;">
                                   
                                </div>
                            </div>
                            
                            <div class="dividerT"></div>
                                
                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Décider</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div> -->
                                <div class="ocecotitle state success">à valider</div> 
                                <div class="ocecotitle step3" id="inVoteProp"></div>
                                <div style="margin: 5px; text-align: center;">
                                 
                                </div>
                            </div>

                            <div class="dividerT"></div>

                            <div class="itemheaderTile">
                                
                                <div class="ocecotitle state success">à financer</div> 
                                <div class="ocecotitle step3" id="toFinanceProp"></div>
                                  <div style="margin: 5px; text-align: center;">
                                  
                                </div>
                            </div>

                            <div class="dividerT"></div>
                            
                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Suivre</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-circle warning" aria-hidden="true"></i></div>
                                <div class="ocecotitle state success">En attente</div> -->
                                <div class="ocecotitle state success">en cours</div> 
                                <div class="ocecotitle step3" id="inProgressProp"></div>
                                  <div style="margin: 5px; text-align: center;">
                                   
                                </div>

                            </div>

                        </div>
                    </div>
              </div>

              <div class="survey-item squareTile successSquare flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle">
                                <i class="fa fa-3 fa-flag success" aria-hidden="true"></i>
                        </div>
                        <div class="ocecotitle step3" id="totalTasks">0</div>
                        <div class="tilelabel ocecotitle" style="">
                              Projet
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              ouvert
                        </div>

                    </div>
                </div>
        </div>

        <!-- level 02 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->
              <div class="survey-item squareTile flex3 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                               Action en rétard
                        </div>
                       
                        <div id="activetask" class="pdt15">
                            
                        </div>
                    </div>
              </div>

              <div class="survey-item headerTile flex6 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        

                    </div>
              </div>

              <div class="survey-item squareTile successSquare flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle">
                                <i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i>
                        </div>
                        <div class="ocecotitle step3" id="totalTasks">0</div>
                        <div class="tilelabel ocecotitle" style="">
                              Projet
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              fermée
                        </div>

                    </div>
                </div>
        </div>



    </div>
    
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {

    });

        var standartcolors = [
            'rgba(46, 204, 113, 1)',
            'rgba(224, 224, 0, 1)',
            'rgba(221, 221, 221, 1)'
        ];

var standartcolors = [
            'rgba(46, 204, 113, 1)',
            'rgba(224, 224, 0, 1)',
            'rgba(221, 221, 221, 1)'
        ];

      var bigstandartcolors = [
            // "#F7464A",
            'rgba(46, 204, 113, 1)',
            "#46BFBD",
            "#949FB1",
            "#4D5360",
      ];

      var defaultimgprofil = '<?php echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_citoyens.png'; ?>';

      var groupe = ["Feature" , "Costume" , "Chef de projet" , "Data" , "Maintenance"];

    var ocecoform = {
      allData : <?php echo json_encode($allData) ?> ,

      allDataAns : <?php echo json_encode($allDataAns) ?> ,

      project : <?php echo json_encode($project) ?>,

      actions : <?php echo json_encode($action) ?> ,

      title : '<?php echo $title; ?>',

      feedback : <?php echo json_encode($feedback) ?> ,

      tiles : {
        "totalProp" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              
              ocecoform.tiles["totalProp"].values = Object.values(ocecoform.allDataAns).length;
            },
            values : 0
        },
        "inVoteProp" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              
            },
            values : 0
        },
        "toFinanceProp" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              
            },
            values : 0
        },
        "inProgressProp" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              
            },
            values : 0
        },

      },

      init : function(pInit = null){
          var copyFilters = jQuery.extend(true, {}, formObj);
          copyFilters.initVar(pInit);
          return copyFilters;
      },

      initvalues : function(ocecoform){
          $.each( ocecoform.tiles , function( dataId, dataValue ) {
              ocecoform.tiles[dataId].getandsetDatafunc(ocecoform);
          });
      },

      initviews : function(ocecoform){
          $.each( ocecoform.tiles , function( tilesId, tilesValue ) {
                if(tilesValue.type == "html"){
                    $("#"+tilesId).html(ocecoform.tiles[tilesId].values);
                } else if (tilesValue.type == "chart") {
                    if (typeof tilesValue.callback != "undefined") {
                        var cb = tilesValue.callback;
                    } else {
                        var cb = function(){};
                    }
                    ajaxPost("#"+tilesId, baseUrl+'/graph/co/chart/', ocecoform.tiles[tilesId].values, cb ,"html");
                } else if (tilesValue.type == "list") {
                    $.each(ocecoform.tiles[tilesId].values, function(tId, tVal){
                        $("#"+tilesId).append(ocecoform.tiles[tilesId].template(tVal));
                    })      
                }
          });
      },

      updateviewData : function(arData, chart){
        if (typeof tiles[chart] !== "undefined" && arData[chart]){
            if (typeof tiles[chart]["type"] == "html" ) {
                $('#'.chart).html(arData[chart]);
            }
        }
      },

      mergeArray : function(arr, type, path){
           var r = arr.reduce(function(accumulator, item){
              if (type == "root" && typeof item != "undefined") {
                accumulator = accumulator.concat(item);
                return accumulator;
              }
              else if (type == "path" && typeof item != "undefined" && typeof item[path] != "undefined") {
                accumulator = accumulator.concat(item[path]);
                return accumulator;
              } else {
                return accumulator;
              }
          },[]);
        return r;
      },

      selectOccur : function(arr, key, path){
        if (key != "null/false") {
           var r = arr.reduce(function(accumulator, item){
              if (typeof item != "undefined" && item[path] == key) {
                accumulator = accumulator.concat(item);
              }
              return accumulator;
          },[]);
          return r;
        } else {
          var r = arr.reduce(function(accumulator, item){
              if (typeof item != "undefined" && (typeof item[path] == "undefined" || item[path] == null || item[path] == false)) {
                accumulator = accumulator.concat(item);
              }
              return accumulator;
          },[]);
          return r;
        }
      }, 

      convertDate : function(dateString){
        var dateParts = dateString.split("/");

        var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
        return dateObject;
      },

      countDays : function(startDate, endDate){
          var difference_In_Time = startDate.getTime() - endDate.getTime();
          var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
          return Math.round(difference_In_Days);
      },

      countToday : function(arr, path){
          arr = arr.filter(function( va ) {
            return (new Date(va[path]).getDay() == new Date().getDay()) && (new Date(va[path]).getMonth() == new Date().getMonth()) && (new Date(va[path]).getYear() == new Date().getYear());
          });
          return arr.length;
      },

      countWeek : function(arr, path){
          arr = arr.filter(function( va ) {
            return ocecoform.getWeekStart(new Date(va[path])) == ocecoform.getWeekStart(new Date());
          });
          return arr.length;
      },

      sortbyDate : function(arr, path, isISO, direction = "asc"){
        var operators = {
          'asc': function(a, b) { return a - b },
          'desc': function(a, b) { return b - a }
        };

        if (path != "root") {
          arr = arr.filter(function( va ) {
              return typeof va[path] !== 'undefined';
          });
          if (isISO) {
            return arr.sort(function(a,b){return operators[direction](new Date(b[path]), new Date(a[path]));
            });
          } else {
              return arr.sort(function(a,b){return operators[direction](ocecoform.convertDate(b[path]), ocecoform.convertDate(a[path]));
            });
          }
        } else {
          if (isISO) {
            return arr.sort(function(a,b){return operators[direction](new Date(b) , new Date(a));
            });
          } else {
              return arr.sort(function(a,b){return operators[direction](ocecoform.convertDate(b) , ocecoform.convertDate(a));
            });
          }
        }
      },

      getAttr : function(arr, path){
          var r = arr.reduce(function(accumulator, item){
              if (typeof item != "undefined" && typeof item[path] != "undefined" ) {
                accumulator = accumulator.concat([item]);
              }
              return accumulator;
          },[]);
        return r;
      },

      getWeekStart : function (date) {
        var offset = new Date(date).getDay();
        return new Date(new Date(date) - offset * 24 * 60 * 60 * 1000)
          .toISOString()
          .slice(0, 10);
      },

      groupWeeks : function (dates, path, countpath, type, countreverse = 0, isISO) {
        
        const groupsByWeekNumber = dates.reduce(function(acc, item) {
          var jsdate = item[path];
          if (!isISO) {
              jsdate = ocecoform.convertDate(item[path])
          }
          const today = new Date(jsdate);
          const weekNumber = today.getWeek();

          // check if the week number exists
          if (typeof acc[weekNumber] === 'undefined') {
            acc[weekNumber] = [];
          }

          acc[weekNumber].push(item);

          return acc;
        }, []);

        if (type == "countpath" || type == "") {
            
          
          return groupsByWeekNumber.map(function(group) {
            return {
              weekStart: ocecoform.getWeekStart(ocecoform.convertDate(group[0][path])),
              count: group.reduce(function(acc, item) {
                return acc + parseInt(item[countpath]);
              }, 0)
            };
          });

        } else if (type == "count"){

          if (!isISO) {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(ocecoform.convertDate(group[0][path])),
                count: group.reduce(function(acc, item) {
                  return acc = acc + 1;
                }, 0)
              };
            });
          } else {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(group[0][path]),
                count: group.reduce(function(acc, item) {
                  return acc = acc + 1;
                }, 0)
              };
            });
          }

        } else if (type == "countreverse"){

          if (!isISO) {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(ocecoform.convertDate(group[0][path])),
                count: group.reduce(function(acc, item) {
                  return countreverse - 1;
                }, 0)
              };
            });
          } else {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(group[0][path]),
                count: group.reduce(function(acc, item) {
                  return countreverse = countreverse - 1;
                }, 0)
              };
            });
          }

        }

      },

      sumInt : function(arr){
          var sum = arr.reduce(function(a, b){
                  return parseInt(a) + parseInt(b);
          }, 0);
          return sum;
      }, 

      sumCumul : function(arr){

        const accumulate = arr => arr.map((sum => value => sum += value)(0));

        return accumulate(arr);  
      },

      getUserData : function(id) {
        var returngetUserData;
          ajaxPost("",
            baseUrl+"/co2/element/get/type/citoyens/id/"+id,
            null,
            function(data) {

              if(typeof data != "undefined"){
                 returngetUserData = data;
                  }
            },
            null,
            "json",
            {async : false}
          );
        return returngetUserData;

      },

      group : function(arr, path){
          var gr = arr.reduce(function(res, obj){
            res[obj[path]] = { 
              count: (obj[path] in res ? res[obj[path]].count : 0) + 1 
            }
            return res;      
          }, []);
          return gr;
      },

    };

        Date.prototype.getWeek = function(dowOffset) {
        /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.epoch-calendar.com */

        dowOffset = typeof dowOffset == 'int' ? dowOffset : 0; //default dowOffset to zero
        var newYear = new Date(this.getFullYear(), 0, 1);
        var day = newYear.getDay() - dowOffset; //the day of week the year begins on
        day = day >= 0 ? day : day + 7;
        var daynum =
          Math.floor(
            (this.getTime() -
              newYear.getTime() -
              (this.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) /
              86400000
          ) + 1;
        var weeknum;
        //if the year starts before the middle of a week
        if (day < 4) {
          weeknum = Math.floor((daynum + day - 1) / 7) + 1;
          if (weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1, 0, 1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
                 the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
          }
        } else {
          weeknum = Math.floor((daynum + day - 1) / 7);
        }
        return weeknum;
      };

  ocecoform.initvalues(ocecoform);
  ocecoform.initviews(ocecoform);

</script>
