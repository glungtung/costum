<?php  
// $cssAnsScriptFilesTheme = array(
//       // SHOWDOWN
//       '/plugins/showdown/showdown.min.js',
//       // MARKDOWN
//       '/plugins/to-markdown/to-markdown.js'            
//     );
//     HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

//      $cssAndScriptFilesModule = array(
//         '/js/default/profilSocial.js',
//     );
     
//     HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
?>

<style type="text/css">
    .main-container{
        background-color: #ffd400;
    }
</style>
  <div style="background-color: #ffd400;height: 40vw" class="col-xs-12 col-lg-12">
    <center>
      <img style="margin-top: 1vw;" class="img-responsive ultra" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/mednumRhoneAlpe/logo.png'>
    </center>
  </div>

    <div class="hidden-xs col-lg-12" id="searchBar" style="margin-left: 18vw;">
        <input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="je recherche une structure d’accompagnement, un atelier d’apprentissage, …">
        <a data-type="filters" href="javascript:;">
            <span id="second-search-bar-addon-mednum" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
                <i id="fa-search" class="fa fa-search"></i>
            </span>
        </a>
    </div>


    <div style="margin-left: 18.7vw;" id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding">
    </div>
    
<center>
  <?php //$this->renderPartial('costum.views.custom.mednumRhoneAlpe.modalProjects'); ?>
</center>
<center>
    <div style="border-radius: 10px;color:white;background-color: #ff5563;margin-top: 5vw;" class="text-center explication col-lg-11 col-sm-11 col-xs-11">
        <img style="width: 14vw;margin-top: 2vw;margin-bottom: 2vw;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/logo_hinaura2.svg">
        <h2 style="font-size: 48px;">C'EST QUOI ?</h2>
        <?php echo $this->renderPartial("costum.assets.images.mednumRhoneAlpe.flechebas"); ?>
    </div>
</center>

<div class="agenda-carousel col-sm-12 col-lg-12">
    <div style="margin-top: 6vw" class="no-padding carousel-border" >
        <div id="docCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner itemctr">
                <div class="item active">
                    <div class="afficheCard">
                        <div id="1" class="col-lg-3 col-xs-10 col-sm-3 text-center">
                            <div class="containCard">
                                <center>
                                    <img style="width: 13vw;margin-top: 3vw;margin-bottom: 4vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/developper.svg"><br>
                                </center>
                            </div><br>
                            <p class="col-lg-12 col-xs-12 text-card">
                            Développer un réseau<br>
                            "Numérique Inclusif", qualifier<br> 
                            les acteurs, aider à la <br>
                            structuration d'une politique<br> 
                            publique adapté aux enjeux de<br>
                            la dématérialisation.
                            </p>
                        </div>

                        <div id="2" class="col-lg-3 col-xs-10 col-sm-3 text-center card">
                            <div class="containCard">
                                <center>
                                    <img style="width: 13vw;margin-top: 3vw;margin-bottom: 4vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/former.svg"><br>
                                </center>
                            </div><br>
                            <p class="col-lg-12 col-xs-12 text-card">
                            Fomer tout les professionnels<br> 
                            à accompagner les usages<br>
                            numérique de leurs publics.
                            </p>
                        </div>

                        <div id="3" class="col-lg-3 col-xs-10 col-sm-3 text-center card">
                            <div class="containCard">
                                <center>
                                    <img style="width: 13vw;margin-top: 3vw;margin-bottom: 4vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/produire.svg"><br>
                                </center>
                            </div><br>
                            <p class="col-lg-12 col-xs-12 text-card">
                            Produire des ressources, les<br> 
                            recenser, les rendres accessibles<br> 
                            et les diffuser pour qu'elles<br>
                            soient utiles aux professinnels,<br>
                            aux publics et aux territoires.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div style="margin-right: -22vw;margin-top: 4vw;margin-left: 4Vw;">
                        <div id="3" class="col-lg-3 col-xs-10 col-sm-3 text-center card">
                            <div class="containCard2">
                                <center>
                                    <img style="width: 13vw;margin-top: 3vw;margin-bottom: 4vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/produire.svg"><br>
                                </center>
                            </div><br>
                            <p class="col-lg-12 col-xs-12 text-card">
                                Produire des ressources, les<br> 
                                recenser, les rendres accessibles<br> 
                                et les diffuser pour qu'elles<br>
                                soient utiles aux professinnels,<br>
                                aux publics et aux territoires.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="arrow-caroussel">
                <a style="opacity: 1" class="carousel-control" href="#docCarousel" data-slide="prev">
                    <?php echo $this->renderPartial("costum.assets.images.mednumRhoneAlpe.backLeft"); ?>
                    <span class="sr-only">Previous</span>
                </a>
                <a style="opacity:1;margin-left: 84vw;" class="carousel-control" href="#docCarousel" data-slide="next">
                     <?php echo $this->renderPartial("costum.assets.images.mednumRhoneAlpe.backRight"); ?>
                    <span class="sr-only">Next</span>
                </a>
            </div>   
        </div>
    </div> 
</div>

<div class="containExplain col-lg-12 col-sm-12">
  <div class="explicationActus row">
      <div class="explication-title col-xs-12 col-sm-12 col-lg-12">
          <div>
              <h2 class="titleBandeau">
                <img class="img-responsive img-actus" style="width: 4vw;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/mednumRhoneAlpe/icone_actus.svg">        
                Actualité
              </h2>
          </div>
      </div>
      <!-- NEWS -->
      <div style="margin-left: 7vw;width: 81vw;margin-top: 4vw;background-color: white" id="newsstream" class="col-xs-10 col-sm-12">
          <div style="background-color: white;">
          </div>
      </div>

      <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
          <a style="color:white;border-radius: 10px;border-color:white;border-width: 4px;padding-left: 53px;padding-right: 56px;padding-top: 15px;padding-bottom: 15px;font-size: 24px;font-weight: bold;" href="javascript:;" data-hash="#live" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg fill="white" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                  <path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/>
                </svg>
                PLUS D'ACTUS
            </a>
      </div>
  </div>
</div>

<div style="background-color: white;">
  <div class="explicationAgenda row">
      <div class="explication-title-agenda col-xs-12 col-sm-12 col-lg-12">
          <div>
              <h2 class="titleBandeau"><img class="img-responsive icoBandeau" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/agenda.svg">Agenda</h2>
          </div>
      </div>

        <div style="margin-top: 7vw;" id="containEvent" class="col-lg-12 col-xs-12">
            
        </div>

      <div style="margin-top: 3vw;margin-bottom:2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
          <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app btn-plus-event btn-redirect-home btn-small-orange">
                Plus D'évènements
            </a>
      </div>
  </div>
</div>

<script type="text/javascript">
  
  jQuery(document).ready(function() {
    
    setTitle("Hinaura");

    contextData = {
      id : costum.contextId,
      type : costum.contextType,
      name : costum.title,
      profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:true, scroll:false}, function(news){}, "html");

    afficheEvent();
});

function afficheEvent(){

  var params = {
    "id" : costum.contextId,
    "type" : costum.contextType
  };

  ajaxPost(
    null,
    baseUrl+"/costum/mednumrhonealpe/geteventaction",
    params,
    function(data){ 
      var str = "";
          var ctr = "";
          var itr = "";
          var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
          var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";
          
          var i = 1;
          
          if(data.result == true){
              $(data.element).each(function(key,value){
                  i++;
                  if (i <= 4) {
                    
                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;  

                    var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                    var img = (value.img != "none") ? baseUrl + value.img : url;
                    var description = (typeof value.shortDescription != "undefined" && value.shortDescription != null) ? value.shortDescription : "Aucune description"; 

                    
                    str += '<div class="card text-center">';
                      str += '<div id="event-affiche" class="card-color col-md-4">';
                        str += '<a class="undefined entityName bold text-dark add2fav  lbh-preview-element" href="#page.type.events.id.'+value.id+'">';
                          str += '<div style="background:white;border-radius: 10px;box-shadow: 2px 2px 32px -15px;" id="afficheImg">';
                            str += '<img style="border-radius: 4px;width: 28vw;margin-top: 1vw;" src="'+imgMedium+'">';
                            str += '<p class="date">'+startDate+'</p>';
                          str += '</div>';
                        str += '</a>';
                        str += '<p class="name">'+value.name+'</p>';
                        str += '<p class="text-center">'+description+'<p>';
                      str += '</div>';
                    str += '</div>';
                  }else{

                  }
              });
          }
          else{
              str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
          }
          $("#containEvent").html(str);
    }
  );
}
</script>