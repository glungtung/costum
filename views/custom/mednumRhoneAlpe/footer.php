<footer id="footer-rhone-alpe" style="background-color:#ffd400" class="text-center col-xs-12  col-lg-12 col-sm-12 no-padding">
    <div class="row" style="margin-top: 1%;">
        <div class="col-xs-12 col-sm-12 col-lg-12 text-center col-footer col-footer-step">
            <a class="col-lg-5 col-xs-4" style="color:black" href="https://lamednum.coop/" class="lbh-menu-app">
            	<center>
                    <img style="width:9vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/logo_principal_transp.png">
                </center>
            	</br>
            	MednumCoop
            </a>
            <a class="col-lg-5 col-xs-4" style="color: black;margin-left: 3vw" href="https://societenumerique.gouv.fr/" class="lbh-menu-app">
                <center>
            	   <img style="width:9vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/mednumRhoneAlpe/logomarianne_typo-sombre.png">
                </center>
            	</br>
            	Société du numérique
            </a>
        </div>
    </div>
</footer>
