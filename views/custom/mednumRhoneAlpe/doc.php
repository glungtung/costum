<?php 
$cssJS = array('/js/docs/docs.js');
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl());

/* TODO 

[ ] reload is empty on hope page
[ ] reload on any page 
[ ] use categories or types to organize poi.docs 
[ ] create poi.timeline as an action plan 
[ ] edit poi.doc
[ ] delete poi.doc

wishlist 
[ ] poi.doc templates
[ ] move doc position index
[ ] use poi.html as piece of a page

*/

$poiList = array();
  if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
      $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  }

?>

<style type="text/css">

	#menu-left{
		
    	bottom: 0;
	    
	    padding: 0;
	    overflow-y: scroll;
	    	background-color: white;
	}
	#header-doc{

		height: 60px;
		padding-top: 10px;
		background-color: white;
	}
	#header-doc h2{
		float: left;
	    color: #354C57;
	    font-size: 20px;
	    font-variant: small-caps;
	    line-height: 41px;
	    padding: 0px 10px;
	}
	#menu-left ul li{
		list-style: none;
	}
	#menu-left > ul > li > a{
		font-size: 20px;
	}
	ul.subMenu > li > a{
		font-size:16px;
	}
	#menu-left > ul > li > a, ul.subMenu > li > a{
		color: #354C57;
		width: 100%;
	    float: left;
	    padding: 5px 20px;
	    text-align: left;
	}
	#menu-left ul li .subMenu, #menu-left > ul > li > a{
		border-bottom: 1px solid #ccc;
	}
	#menu-left > ul > li > a.active, #menu-left > ul > li > a:hover{
		text-decoration: none;
		background-color:#65BA91;
		color: white;
		font-size: 22px;
	}
	ul.subMenu > li > a.active, ul.subMenu > li > a:hover{
		border-left: 4px solid #65BA91;
		color: #65BA91;
		font-size:18px;
		text-decoration: none;
	}
	#menu-left ul li a.active span.text-red, #menu-left ul li a:hover span.text-red{
		color:#354C57 !important;
	}
	.close-modal{
		top: 10px !important;
    	right: 10px !important;
     	z-index: 100000000000000 !important;
    	position: fixed !important;
	}
	.close-modal .lr, .close-modal .rl{
		height: 40px !important;
	}
	ul.subMenu{
		/*display:none;*/

	}
	ul.subMenu{
		padding-left: 30px
	}
#show-menu-xs, #close-docs{
	    padding: 7px 15px;
    font-size: 20px;
}
.keypan .panel-heading{
	margin-top: 20px;
    min-height: 70px;
}
.keypan{
	border: none;
    margin-bottom: 10px;
    box-shadow: none;
}
.keypan, .keypanList{
	box-shadow: none;	
}
.keypanList .panel-title i{
	margin-right: 10px;
}
.keypanList .panel-body ul{
	padding-left: 0px;
}
.keypanList .panel-title span{
	font-size: 24px !important;
}
.keypan .panel-body{
	min-height: 200px;
}
.keypan hr {
	width: 75%;
    margin: auto;
}
#header-docs .panel-title, .subtitleDocs .panel-title {
	font-size: 40px;
}
#header-docs .panel-title .sub-title, .subtitleDocs .panel-title .sub-title{
	font-size: 20px !important;
	font-style: italic;	
}
#container-docs{
	background-color: white;
	padding-left: 50px;
}

@media (max-width: 991px) {
  #menu-left{
    width: 56%;
    left: -56%;
	bottom: 0px;
	}
  
}

@media (min-width: 991px) {
  #menu-left {
    left:0 !important;
  }
}

</style>

<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding no-margin">
	<div id="header-doc" class="col-xs-12 col-md-12 col-lg-12 col-sm-12 shadow2">
		<a href='javascript:;' id="show-menu-xs" class=" pull-left" data-placement="bottom" data-title="Menu"><i class="fa fa-bars"></i></a>
		<h2 class="elipsis no-margin"><i class="fa fa-book hidden-xs"></i> DOCUMENTATION <span style="color:#00bcbe">HINAURA</span></h2>
		<a href='javascript:nextDoc();' id="nextDoc" class=" pull-right" data-placement="bottom" data-title="Next"><i class="fa fa-2x fa-arrow-right"></i></a>
		<a href='javascript:prevDoc() ;' id="prevDoc" style="margin-right: 10px;" class=" pull-right" data-placement="bottom" data-title="Prev"><i class="fa fa-2x fa-arrow-left"></i></a>
		 
	</div>

	<div style="display: initial" id="menu-left" class="col-md-3 shadow2" >
	  	<ul class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">
			<?php /* ?>
			<li class="col-xs-12 no-padding">
				<a href="javascript:;" class="link-docs-menu down-menu" data-type="kicker" data-dir="costum.views.custom.ctenat.docs">
					<i class="fa fa-angle-right"></i> Parcours
				</a>
				<ul class="subMenu col-xs-12 no-padding">
					<li class="col-xs-12 no-padding">
						<a href="javascript:;"  id="startDoc"  class="link-docs-menu" data-type="public" data-dir="costum.views.custom.ctenat.docs">
							Acteurs publics
						</a>
					</li>
					<li class="col-xs-12 no-padding">
						<a href="javascript:;" class="link-docs-menu" data-type="socioeco" data-dir="costum.views.custom.ctenat.docs">
							Acteurs socio-économiques
						</a>
					</li>
					
				</ul>
			</li>
				
				
			<?php 
*/
			$pois = PHDB::find( Poi::COLLECTION, array( 
								"parent.".$this->costum["contextId"] => array('$exists'=>1), 
	                            "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"], 
	                           	"type"=>"doc") );	
			$structField = "structags";
			$pois = Poi::hierarchy($pois,$structField);
			$docUrls = [];
			foreach ($pois["parentTree"] as $key => $value) 
			{
				if(isset($pois["orphans"][$key]))
				{
			 ?>
				<li class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">
					<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$pois["orphans"][$key]["_id"]; ?>" data-pos="<?php echo count($docUrls) ?>" >
						<?php $docUrls[] = (string)$pois["orphans"][$key]["_id"]; ?>
						<i class="fa fa-angle-right"></i> <?php echo $pois["orphans"][$key]["name"]; ?>
					</a>
					<ul class="subMenu col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">
						<?php
							
							//position basé sur les 2 premier chiffres
							foreach ( $value as $k => $v) { 
								$value[$k]["pos"] = substr( $v["name"], stripos( $v["name"], ">" ) + 1 , 3 ) ;
							}
							$pos = array_column( $value, 'pos' );
							array_multisort( $pos, SORT_ASC, $value );

							foreach ( $value as $k => $v ) 
							{ 

								?>
							<li class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">
								<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$v["_id"]; ?>" data-pos="<?php echo count($docUrls) ?>">
									<?php 
									$docUrls[] = (string)$v["_id"];
									echo $v["name"]; ?>
								</a>
							</li>
						<?php
							}
						?>
					</ul>
				</li>
				
			<?php
				}else {
					foreach ( $value as $k => $v) 
					{ 
						$pois["orphans"][$k]=$v;
					}
				}
			}
			
			foreach ($pois["orphans"] as $key => $value) 
			{ 
				if( isset($value[$structField]) && count($value[$structField]) == 1 && isset( $pois["parentTree"][ $value[$structField][0] ]) ){
					//on n'affiche pas le sparent en tant qu'orphelin
				}
				else if( isset($value[$structField]) && isset( $pois["parentTree"][ $value[$structField] ]) ){
					//on n'affiche pas le sparent en tant qu'orphelin
				}
				else
				{ ?>
				<li class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">
					<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$value["_id"]; ?>" >
						<?php 
							$docUrls[] = (string)$value["_id"];
							echo $value["name"]; ?>
					</a>
				</li>
			<?php } 
			} ?>

			
			<?php if( Authorisation::canEdit( Yii::app()->session["userId"] , $this->costum["contextId"], $this->costum["contextType"] ) )
			{ 
				
				?>
			<li class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">	
				<ul>
					<li class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding">
						<a href="javascript:;" class="createDocBtn text-red" >
							<i class="fa fa-plus-circle"></i> AJOUTER
						</a>
					</li>
				</ul>
			</li>
			<?php } ?>

		</ul>
	</div>

	<div id="container-docs" class="col-md-9 col-xs-9 col-lg-9 col-sm-9 text-center padding-20 margin-top-20"></div>
</div>

<script type="text/javascript">
var dynFormCostumDoc = {
		"beforeBuild":{
			    "removeProp" : {
			    	"image": 1,
                },
		        "properties" : {
		            "structags" : {
		                "inputType" : "tags",
		                "placeholder" : "Structurer le contenu",
		                "values" : null,
		                "label" : "Structure et Hierarchie (parent ou parent.enfant)"
		            },
		            "documentation" : {
                        "inputType" : "uploader",
                        "label" : "Document associé (5Mb max)",
                        "showUploadBtn" : false,
                        "docType" : "file",
                        "itemLimit" : 15,
                        "contentKey" : "file",
                        "order" : 8,
                        "domElement" : "documentationFile",
                        "placeholder" : "Le pdf",
                        "afterUploadComplete" : null,
                        "template" : "qq-template-manual-trigger",
                        "filetypes" : [
                        	"pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif"
                        	],
                    }
		        }
		    },
	    "onload" : {
	        "actions" : {
	            "setTitle" : "Documentation",
	            "html" : {
	                "nametext>label" : "Titre de la documentation",
	                "infocustom" : "<br/>Créer des sections et des sous chapitres"
	            },
	            "presetValue" : {
	                "type" : "doc"
	            },
	            "hide" : {

	            	"parentfinder":1,
		                "locationlocation" : 1,
		                "formLocalityformLocality" : 1,
	                "breadcrumbcustom" : 1,
	                "urlsarray" : 1,
	                "imageuploader":1
	            }
	        }
	    }
	};
function bindDocLinksPoi() { 
	
	$(".link-docs-menu-poi").off().on("click",function(){
		openDoc($(this).data("poi"),parseInt($(this).data("pos")));
	});
 }
jQuery(document).ready(function() {
	mylog.log("render","costum.views.mednumRhoneAlpe.doc");
	
	if ( location.hash.indexOf("docs.poi") >= 0 ) {
		paramT = location.hash.split(".");
		documentation.getBuildPoiDoc(paramT[2]);
	} else 	
		setTimeout(function() { navInDocs("docDetail", "costum.views.custom.mednumRhoneAlpe.docs"); }, 500)
	
	$("#close-docs").attr("href",urlBackHistory);

	bindDocLinksPoi();

	$("#show-menu-xs").off().on("click",function (){
		if (document.getElementById("menu-left").style.display == "initial"){
			document.getElementById("menu-left").style.display = "none";
			$('#container-docs').removeClass('col-md-9').addClass('col-md-12');
		}else{
			document.getElementById("menu-left").style.display = "initial";
			$('#container-docs').removeClass('col-md-12').addClass('col-md-9');
		}	
		navInDocs("docDetail", "costum.views.custom.mednumRhoneAlpe.docs");
	});	
	$(".createDocBtn").off().on("click",function (){
		mylog.log("createBtn");
		dyFObj.openForm('poi',null,null,null,dynFormCostumDoc);
	});

	contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

})
function openDoc(poiId,pos){
	
	var url = baseUrl+'/co2/element/get/type/poi/id/'+poiId+"/edit/true";
	hashdocs="#docs.poi."+poiId;
	currentDocPos=pos;
	//alert(".link-docs-menu-poi"+currentDocPos);
	//alert(hashdocs);
	//location.hash = hashdocs;
	ajaxPost(null ,url, null,function(data){
		descHtml = documentation.renderDocHTML(data);
			$('#container-docs').html(descHtml);
			// alert(data.map["_id"]["$id"]+data.map.name);
			// contextData = {};
			// contextData.type = "poi";
     	// contextData.id = data.map["_id"]["$id"];;
     	// contextData.name = data.map.name;

			// $(".uploadThisBtn").off().on("click",function (){
	    //     mylog.log("uploadThisBtn");
	    //     dyFObj.openForm('addFile');
	    // });

		$(".editThisBtn").off().on("click",function (){
			mylog.log("editThisBtn"); 
			var id = $(this).data("id");
		    var type = $(this).data("type");
			dyFObj.editElement(type,id, null,dynFormCostumDoc);
		});

		$(".deleteThisBtn").off().on("click",function (){
	        mylog.log("deleteThisBtn click");
	          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	          var btnClick = $(this);
	          var id = $(this).data("id");
	          var type = $(this).data("type");
	          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
	          
	          bootbox.confirm(trad.areyousuretodelete,
	            function(result) 
	            {
	                if (!result) {
	                  btnClick.empty().html('<i class="fa fa-trash"></i>');
	                  return;
	                } else {
                        ajaxPost(
                            null,
                            urlToSend,
                            null,
                            function(data){
                                if ( data && data.result ) {
                                    toastr.info("élément effacé");
                                    $("#"+type+id).remove();
                                    urlCtrl.loadByHash(location.hash)
                                } else {
                                    toastr.error("something went wrong!! please try again.");
                                }
                            },
                            null,
                            "json"
                        );
	                }
	            });

	    });
	},"html");

}

var docUrls = <?php echo json_encode($docUrls); ?>;
currentDocPos = -1;
function nextDoc () 
{ 
	$(".link-docs-menu-poi").removeClass("active");
	if(currentDocPos < 0){
		$('#container-docs').addClass('col-xs-9').removeClass('col-xs-12')
		$('#menu-left').removeClass("hide");
	}
	currentDocPos++;
	if(currentDocPos > docUrls.length)
		currentDocPos = -1;
	//alert("nextDoc"+currentDocPos+" : "+docUrls[currentDocPos]);
	openDoc(docUrls[currentDocPos],currentDocPos);
	$('.link-docs-menu-poi[data-pos="'+currentDocPos+'"]').addClass("active");
 }

function prevDoc () 
{ 
	$(".link-docs-menu-poi").removeClass("active");
	if(currentDocPos < 0){
		$('#container-docs').addClass('col-xs-9').removeClass('col-xs-12')
		$('#menu-left').removeClass("hide");
	}
	currentDocPos--;
	if( currentDocPos < 0 )
		currentDocPos = docUrls.length-1;
	//alert("prevDoc"+currentDocPos+" : "+docUrls[currentDocPos]);
	openDoc(docUrls[currentDocPos],currentDocPos);
	$('.link-docs-menu-poi[data-pos="'+currentDocPos+'"]').addClass("active");
 }

	
</script>
