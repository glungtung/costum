<?php 
    $cssAnsScriptFilesTheme = array(
    	'/css/filters.css',
	);

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, $this->module->getParentAssetsUrl());
?> 
<style type="text/css">

	#filters-nav #input-sec-search .input-global-search{
		border-radius: 0px 20px 20px 0px !important;
	}

	#filters-nav .dropdown-menu-complexe {
	    border-radius: 5px;
	    border: 1px solid #00bcbe;
	    padding: 10px;
	}
	
	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList{
    background: transparent;
    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub:hover{
	    background-color : #00bcbe;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList:hover{
	    background-color : #00bcbe;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe{
	    color: #00bcbe;
	}

	#filterContainer .dropdown .btn-menu, #filters-nav .complexeSelect .btn-menu ,#filters-nav .dropdown .btn-menu, #filters-nav .filters-btn {
	    padding: 10px 15px;
	    font-size: 16px;
	    border: 1px solid #00bcbe;
	    color: #00bcbe;
	    top: 0px;
	    position: relative;
	    border-radius: 20px;
	    text-decoration: none;
	    background: white;
	    line-height: 20px;
	}

	#filterContainer #input-sec-search .input-group-addon, #filters-nav #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
	    padding: 10px 0px 10px 10px;
	    font-size: 18px;
	    font-weight: 400;
	    line-height: 1;
	    color: #00bcbe;
	    margin-top: 5px;
	    text-align: center;
	    background-color: white !important;
	    border-right-width: 0px;
	    border: 1px solid #00bcbe;
	    border-right-width: 1px;
	    border-radius: 0px 20px 20px 0px;
	    border-right-width: 0px;
	    width: 30px;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne:hover{
	    background-color : #00bcbe;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo:hover{
	    background-color : #00bcbe;
	    color: white;
	}


	#filterContainer #input-sec-search .input-global-search, #filters-nav #input-sec-search .input-global-search, .searchBar-filters .search-bar {
	    border-left-width: 0px;
	    height: 40px;
	    margin-top: 5px;
	    border-radius: 20px 0px 0px 20px;
	    box-shadow: none;
	    width: inherit;
	    border-color: #00bcbe;
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;

	var paramsFilter = {
		container : "#filters-nav",
	 	header : {
	 		options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true,
						types : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						map : true
					}
				}
			}
	 	},
	 	inteface : {
			events : {
				scroll : true,
				scrollOne : true
			}
		},
		results : {
			renderView: "directory.elementPanelHtml",
		 	smartGrid : true
		}
	};

	if (pageApp == "search") {
		paramsFilter.defaults = {
	 		types : ["organizations"],
	 		fields : ["typePlace"],
		 	community : true,
		 	indexStep : 0
	 	};

	 	paramsFilter.filters = {
	 		scope : {
	 			name : "Code Postal",
	 			view : "scope",
	 			type : "scope",
	 			action : "scope"
 			},
	 		types : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "type",
	 			name : "types",
	 			event : "filters",
	 			list : {
					"NGO" : "Association", 
					"LocalBusiness": "Entreprise", 
					"Group" : "Groupe", 
					"GovernmentOrganization" : "Service public"
	 			}
	 		},
	 		typePlace : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "typePlace",
	 			name : "Type de place",
	 			event : "filters",
	 			list : {
	 				"Amicale Laïque": "Amicale Laïque",
		            "BIJ/PIJ" : "BIJ/PIJ",
		            "Centre Social" : "Centre Social",
		            "Cyberbase / Cybercentre" : "Cyberbase / Cybercentre",
		            "Fablab / Hackerspace" : "Fablab / Hackerspace",
		            "Maison de Quartier" : "Maison de Quartier",
		            "Maison des Services" : "Maison des Services",
		            "Médiathèque" : "Médiathèque"
	    		}
	 		}
	 	};
	}

	if (pageApp == "projects") {

		// paramsFilter.results = {
	 // 		renderView : "directory.classifiedPanelHtml",
	 // 		smartGrid : true
	 // 	};

		paramsFilter.defaults = {
			types : ["projects"],
			fields : [
				"categ", 
				"subcateg",
				"subsubcateg"
			]
		};

		paramsFilter.filters = {
			scope : true,
			categ : {
				view : "complexeSelect",
	 			name : "Services",
	 			event : "selectList", 
	 			list : costum.hinauraCategory,
	 			multiple:true,
	 			levelOptions : [
	 				{
	 					type : "categ",
	 					level : 1,
	 					separator : true,
	 					visible : true 
	 				},
	 				{
	 					type : "subcateg",
	 					level : 2,
	 					class : "col-xs-12",
	 					visible : true
	 				},
	 				{
	 					type : "subsubcateg",
	 					level : 3,
	 					visible : false 
	 				}
	 			]
			}
		};
	}

	// 	var paramsFilter= {
	//  		container : "#filters-nav",
	// 	 	interface : {
	//  			events : {
	// 	 			scroll : true,
	// 	 			scrollOne : true
	//  			}
	//  		},
	// 	 	filters : {
	// 	 		scope : true,	
	// 	 	// 	categ : {
	// 	 	// 		view : "complexeSelect",
	// 	 	// 		name : "Services", 
	// 	 	// 		event : "filters", 
	// 	 	// 		type : "filters" , 
	// 	 	// 		list : costum.hinauraCategory,
	// 	 	// 		levelOptions : [
	// 	 	// 			{
	// 	 	// 				field : "categ",
	// 	 	// 				level : 1,
	// 	 	// 				visible : true 
	// 	 	// 			},
	// 	 	// 			{
	// 	 	// 				field : "subcateg",
	// 	 	// 				level : 2,
	// 	 	// 				visible : true
	// 	 	// 			},
	// 	 	// 			{
	// 	 	// 				field : "subsubcateg",
	// 	 	// 				level : 3,
	// 	 	// 				visible : false 
	// 	 	// 			}
	// 	 	// 		]
	// 	 	// 	}
 // 			// }
 // 		}
var filterSearch={};

jQuery(document).ready(function() {
	filterSearch = searchObj.init(paramsFilter);
});

</script>