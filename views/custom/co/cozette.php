<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 
$poiList = array();
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                           "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                           "type"=>"cms") );
} else {?>
  
  <div class="col-xs-12 text-center margin-top-50">
    <h2>Ce COstum est un template <br/>doit etre associé à un élément pour avoir un context.</h2>
  </div>

<?php exit;}?>


<div>

<?php 
  
    echo $this->renderPartial("costum.views.tpls.tagBar",
                        $this->costum["htmlConstruct"]['tagBar']['top'] ,true); 

?>  
<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
    

    <div class="col-xs-12 text-center margin-bottom-50" style="padding:0px;">
    

  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#1F2532; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                
                <small style="text-align: left">
                <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
                  <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
  color: #2b2b2b;
  text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">



  « La COzette un journal local , territorial et connecté »</bloquote>

              </h3>
              <div  style="text-align: right; font-size: 1.4em;padding-right:30px;">
                <b>Mr. CO et Mme Zette</b>,<br/>
                Acteurs engagés dans le changement positif, respectant l'Homme, l'Environnement et la Finitude des ressources. 
                  </small>
              </div>
              <br/>



                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
              
              <div class="col-md-10 col-md-offset-1 col-xs-12">
                
                <span class="text-explain">
                  <br/>
                  <h2>Cahier des charges de la COzette <span class="text-red">CO</span>nnecté</h2>

                    <span class="text-red">CO</span>ntributif, <span class="text-red">CO</span>llaboratif, <span class="text-red">CO</span>smopolite,<span class="text-red">CO</span>loré, <span class="text-red">CO</span>opératif, <span class="text-red">CO</span>ol, <span class="text-red">CO</span>ordonnée....<br/><br/>
  
                    <?php 
                    echo $this->renderPartial("costum.views.tpls.multiblocks",array(
                        "poiList"   => $poiList,
                        "blockName" => "cc",
                        "titlecolor"=> "#e6344d",
                        "blockCt"   => 1,
                        "endHTML"   => '<br/><span class="bullet-point"></span><br/>'
                      ),true);
                     ?>

                </span>

                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">

            </a>
              </div>
            </div>
            <div class="text-center col-xs-12">
            <br/><br/>
            <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png"></div>
        </div>
          </div>

        </div>

      </div>
      






      <div class="col-xs-12 no-padding support-section">
        <h2><br/><i class="fa fa-calendar"></i> QUEL CALENDRIER ?</h2>
        <div class="col-xs-12  text-explain">
          
        <img style="margin:auto" class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/cal.png">
        
        
        
        </div>
        </div>



    </div>
  </div>
</div>




<?php 
/*
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>


<script type="text/javascript">
  jQuery(document).ready(function() {

    setTitle("COzette");
    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
    if(contextData.id == "")
      alert("Veuillez connecter ce costum à un élément!!")
    
});

</script>

