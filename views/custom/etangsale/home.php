<?php 

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );
  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  $poiList = array();
  if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
      $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  
      $poiList = PHDB::find(Poi::COLLECTION, 
                      array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                             "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                             "type"=>"cms") );
  }
?>
<style>
.header{
        background-image : url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/etangsale/header.jpg);
        background-size: cover;
        padding-bottom: 30%;
}
.bloc{
    margin-top: 2%;
    padding: 2%;
}
.para-bloc{
    padding: 5%;
    color: white;
}
.para-carousel{
    border: 1px solid black;
    padding: 2%;
}
.carousel-indicators li{
    border: 2px solid black;
}
.carousel-indicators .active{
    background-color: red;
}
#second-search-bar{
    border-radius: 20px 20px 20px 20px !important;
    width: 100%;
}
</style>
<script src="https://unpkg.com/scrollreveal"></script>


<div class="header">
<?php 
    $params = [  "tpl" => "etangsale","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
?>
</div>
<div class="bas-header no-padding col-xs-12">
    <div class="col-xs-4" style="padding: 0.5%; background:#99a5d5">
    </div>
    <div class="col-xs-4" style="padding: 0.5%; background:#5d5ba4">
    </div>
    <div class="col-xs-4" style="padding: 0.5%; background:#1d1a8d">
    </div>
</div>

<div class="text-center col-md-12" style="background: #8e8dca; color: white; padding: 1%;">
    <h3> <i class="fa fa-search"></i> QUE CE PASSE T-IL DANS MA <span style="color : #5d5ba4">VILLE</span> ? </h3> <br />
				<div class="hidden-xs col-xs-12" id="searchBar">
					<!-- <a data-type="filters" href="javascript:;">
            			<span id="second-search-bar-addon-alternatibaRe" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
                			<i id="fa-search" class="fa fa-search"></i>
            			</span>
        			</a> -->
        			<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Une recherche">
    			</div>

    			<!-- Dropdown -->
			    <div style="display: none; background: white none repeat scroll 0% 0%; border-radius: 20px;" id="dropdown" class="hidden-xs dropdown-result-global-search hidden-xs col-md-12 no-padding">
			    </div>
</div>

<!-- Carto --> 
<div  id="mapEtangSale" class="section-home col-xs-12 no-padding no-margin" style="height: 450px"></div>


<!-- Btn invitation --> 
<div class="bloc">
    <div class="text-center col-xs-12" style="padding : 3%; margin-top: -2%;">
    <h3> Vous êtes un(e) <span style="color : #5d5ba4">Étang-Saléen</span></h3>
        <?php if(@Yii::app()->session['userId']) : ?>
            <div class="col-md-6">
                <a href="#page.type.citoyens.id.<?= @Yii::app()->session['userId'] ?>" class="lbh btn btn-primary btn-lg" data-toggle="dropdown"><i class="fa fa-pencil"></i> Je souhaites accéder à ma page profil</a>
            </div>
            <div class="col-md-6">
            <a href="#element.invite.type.citoyens.id.<?= @Yii::app()->session['userId'] ?>" class="lbhp btn btn-success btn-lg" data-placement="bottom" data-original-title="Inviter des personnes "><i class="fa fa-user-plus "></i> Je souhaites inviter des personnes à la plateforme</a>
            </div>
        <?php else : ?>
        <div class="col-md-6">
                <a href="javascript:;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalRegister"><i class="fa fa-user"></i> Je souhaites rejoindre la plateforme</a>
            </div>
            <div class="col-md-6">
            <button class="btn btn-success btn-lg" disabled><i class="fa fa-user-plus "></i> Je souhaites inviter des personnes à la plateforme</a>
            </div>
        <?php endif ?>
    </div>
</div>

<!-- Separator --> 
<div class="bloc">
    <div class="container">
        <div class="bas-header no-padding col-xs-12 headline">
        <div class="col-xs-4" style="padding: 0.5%; background:#99a5d5">
        </div>
        <div class="col-xs-4" style="padding: 0.5%; background:#5d5ba4">
        </div>
        <div class="col-xs-4" style="padding: 0.5%; background:#1d1a8d">
        </div>
    </div>
</div>
</div>

<!-- Description -->
<div class="bloc">
        <div class="col-md-12 headline">
       
  <div class="text-center">
                <a href="javascript:;" data-hash="#article"  class="lbh-menu-app btn btn-danger btn-lg"><i class="fa fa-newspaper-o"></i> Voir tous les articles</a>
        </div>
    </div>

</div>

<div class="oui no-padding col-xs-12" style="margin-top: 5%;
margin-bottom: 2%;">
    <div class="text-center">
    <!-- j'ai besoin -->
            <div class="col-md-4 no-padding headline" style="background : #2bb0c6">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/economie/commun.jpg" class="img-responsive" style="height: 237px;
width: 100%;">
                <h2 style="margin-top: 2%; color:white;"><i class="fa fa-rocket"></i> J'ai besoin d'aide</h2>
                <p class="para-bloc">Vous avez besoin d'aide dans vos projets ? Demander la <br /> <br /> <br />
                <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'});" class="btn btn-success btn-lg">Proposer une demande</a><br><br>
                <a href="javascript:;" data-hash="#annonce"  class="lbh-menu-app btn btn-primary btn-lg">Voir les offres/demandes en cours</a> </p>
            </div>
    <!-- j'offre -->
            <div class="col-md-4 no-padding headline" style="background : #89403d">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/sante/MainHomme.jpg" class="img-responsive" style="height: 237px;
width: 100%;">
                <h2 style="margin-top: 2%; color:white; "><i class="fa fa-calendar"></i> J'offre mes services</h2>
                <p class="para-bloc">Vous souhaitez organiser un évènement et vous cherchez un moyen de le partager ? Partager la <br /> <br />
                <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'offer'});" class="btn btn-success btn-lg">Proposer une offre</a><br><br>
                <a href="javascript:;" data-hash="#annonce"  class="lbh-menu-app btn btn-primary btn-lg">Voir les évènements en cours</a></p> 
            </div>
    <!-- ressource -->
            <div class="col-md-4 no-padding headline" style="background : #20c06b">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/sante/produire.jpg" class="img-responsive" style="height: 237px;
width: 100%;">
            <h2 style="margin-top: 2%; color:white;"><i class="fa fa-calendar"></i> Ressources locales</h2>
                <p class="para-bloc">Découvrez les ressources en vraks disponible dans la commune<br /> <br />
                <a href="#" class="btn btn-success btn-lg">Proposer une nouvelle ressource</a><br><br>
                <a href="#" class="btn btn-primary btn-lg">Voir les organises</a> </p>
        </div>

    <!-- colonne de separation --> 
    <div class="col-md-4 hidden-xs headline" style="padding: 0.5%; background:#99a5d5">
    </div>
    <div class="col-md-4 hidden-xs headline" style="padding: 0.5%; background:#5d5ba4">
    </div>
    <div class="col-md-4 hidden-xs headline" style="padding: 0.5%; background:#1d1a8d">
    </div>

    <!-- projet -->
    <div class="col-md-4 no-padding headline" style="background : #8c5aa1">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/lapossession/Photo-Citoyen.jpg" class="img-responsive" style="height: 237px;
width: 100%;">
                <h2 style="margin-top: 2%; color:white;"><i class="fa fa-rocket"></i> Projet</h2>
                <p class="para-bloc">Vous avez un projet qui vous tient à coeur ? Partager la <br /> <br /> <br />
                <a href="javascript:;" onclick="dyFObj.openForm('project');"  class="btn btn-success btn-lg">Proposer un projet</a><br><br>
                <a href="javascript:;" data-hash="#project"  class="lbh-menu-app btn btn-primary btn-lg">Voir les projets en cours</a> </p>
            </div>
    <!-- évènement -->
            <div class="col-md-4 no-padding headline" style="background : #ffa200">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/cover.jpg" class="img-responsive" style="height: 237px;
width: 100%;">
                <h2 style="margin-top: 2%; color:white;"><i class="fa fa-calendar"></i> Évènement</h2>
                <p class="para-bloc">Vous souhaitez organiser un évènement et vous cherchez un moyen de le partager ? Partager la <br /> <br />
                <a href="javascript:;" onclick="dyFObj.openForm('event');"  class="btn btn-success btn-lg">Proposer un évènement</a><br><br>
                <a href="javascript:;" data-hash="#agenda"  class="lbh-menu-app btn btn-primary btn-lg">Voir les évènements en cours</a></p> 
            </div>
    <!-- organisation -->
            <div class="col-md-4 no-padding headline" style="background : #e33551">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/education/Outils.jpg" class="img-responsive" style="height: 237px;
width: 100%;">
            <h2 style="margin-top: 2%; color:white;"><i class="fa fa-calendar"></i> Organisation</h2>
                <p class="para-bloc">Vous êtes une organisation et vous souhaitez être répertorier ? Partager la <br /> <br />
                <a href="javascript:;" onclick="dyFObj.openForm('organization');" class="btn btn-success btn-lg">Proposer une organisation</a><br><br>
                <a href="javascript:;" data-hash="#organization"  class="lbh-menu-app btn btn-primary btn-lg">Voir les organisations</a> </p>
        </div>

    </div>
</div>

<div class="bloc">
    <div class="container">
    <div class="bas-header no-padding col-xs-12 headline">
    <div class="col-xs-4" style="padding: 0.5%; background:#99a5d5">
    </div>
    <div class="col-xs-4" style="padding: 0.5%; background:#5d5ba4">
    </div>
    <div class="col-xs-4" style="padding: 0.5%; background:#1d1a8d">
    </div>
</div>

    <div class="col-xs-12 headline" style="margin-top:2%;">
        <div class="col-xs-3">
            <a href="https://communecter.org" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-co.png" class="logo-footer img-responsive"></a>
        </div>
        <div class="col-xs-3">
            <a href="http://www.open-atlas.org/" target="_blank"> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/logo-openatlas.png" class="logo-footer img-responsive"></a>       
        </div>
    </div>
    </div>
</div>
<script>
var mapEtangSale = {};
var paramsMapEtangSale  = {};


var etangsale={
	initScopeObj : function(){
		$(".content-input-scope-etangsale").html(scopeObj);
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["RE"]
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.popup.default = function(data){
			mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = mapCustom.custom.getThumbProfil(data) ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup +=  "Accéder à la page";
					popup += '</div></a>';
				popup += '</div>';
            popup += '</div>';
			return popup;
		};
	}
}


function initEtangSaleMapView(){
	etangsale.initScopeObj();
	paramsMapEtangSale = {
		zoom : 1,
		container : "mapEtangSale",
		activePopUp : true,
		tile : "maptiler",
		menuRight : true,
		mapOpt:{
			latLon : ["-21.25", "55.3833"],
		}
	};

	mapEtangSaleHome = mapObj.init(paramsMapEtangSale);
	dataSearchEtangSale=searchInterface.constructObjectAndUrl();
	dataSearchEtangSale.searchType = ["projects","organizations"];
	dataSearchEtangSale.indexStep=0;
	etangsale.mapDefault();
	// dataSearchPossession.searchType = ["NGO","LocalBusiness","Group","GovernmentOrganization"];
	dataSearchEtangSale.private = true;
	dataSearchEtangSale.sourceKey = costum.contextSlug;
	var donnee = mapEtangSaleHome.data;
	ajaxPost(
		null,
		baseUrl+'/'+moduleId+'/search/globalautocomplete',
		dataSearchEtangSale,
      	function(data){ 
	       mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
			if(!data){ 
				toastr.error(data.content); 
			} 
			else{ 
				// $('#mapContent').html('');
				mapEtangSaleHome.addElts(data.results, true);
				// setTimeout(function(){
				// 	mapEtangSaleHome.map.panTo([-21.25,55.3833]);
				// 	mapEtangSaleHome.map.setZoom(3);
				// },2000);
			}
      	},
      	function(data){
            mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false; 
        }
    );
	
}

jQuery(document).ready(function(){
    mylog.log("render","/modules/costum/views/custom/filiereCostum/home.php");
    setTitle(costum.title);

    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

    ScrollReveal().reveal('.headline', { }, 5000);
    initEtangSaleMapView();

    $("#menuRightmapEtangSale").addClass("hidden");

    $("#second-search-bar").off().on("keyup",function(e){ 
                        $("#input-search-map").val($("#second-search-bar").val());
                        $("#second-search-xs-bar").val($("#second-search-bar").val());
                        if(e.keyCode == 13){
                                mylog.log("searchObject.text",searchObject.text);
                                searchObject.text=$(this).val();
                                searchObject.sourceKey=costum.contextSlug;
                                myScopes.type="open";
                                myScopes.open={};
                                startGlobalSearch(0, indexStepGS);
                                $("#dropdown").css('display','block');
                         }  
});

$("#second-search-xs-bar").off().on("keyup",function(e){ 
                        $("#input-search-map").val($("#second-search-xs-bar").val());
                        $("#second-search-bar").val($("#second-search-xs-bar").val());
                        if(e.keyCode == 13){
                                mylog.log("searchObject.text",searchObject.text);
                                searchObject.text=$(this).val();
                                searchObject.sourceKey=costum.contextSlug;
                                myScopes.type="open";
                                myScopes.open={};
                                startGlobalSearch(0, indexStepGS);
                                $("#dropdown").css('display','block');            
                        }
});
});
</script>