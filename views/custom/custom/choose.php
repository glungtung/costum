<?php

if(  Authorisation::canEditItem(Yii::app()->session["userId"],@$this->costum["contextType"], @$this->costum["contextId"])) 
{

//assets from ph base repo
$cssAnsScriptFilesTheme = array(
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

//gettting asstes from parent module repo
$cssAnsScriptFilesModule = array( 
	'/js/dataHelpers.js',
	'/css/md.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );

//if( $this->layout != "//layouts/empty"){
//	$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
//	$this->renderPartial($layoutPath.'header',array("page"=>"welcome","layoutPath"=>$layoutPath));
//}
?>
<style type="text/css">

header{
	background-color: #e6344d;
	color: white;
	margin-bottom: 30px;
}
</style>
<header class="col-xs-12">
<h1 style="text-align: center;padding:10px; font-size:24px; text-transform: inherit;">
	<i class="fa fa-mask"></i> 
		Personnalisez votre page :<br/>
		<?php echo CHtml::encode( (isset($this->module->pageTitle))?$this->module->pageTitle:""); ?>
</h1>
<nav class="col-xs-12 menu-doc-header text-center">
		Tester des examples de design existant
</nav>
</header>

<div class="col-xs-12">
    <?php 
    	$costums=PHDB::find("costum",["isTemplate"=>true]);

		$html="";
		foreach($costums as $key=> $v){
			$img=$this->module->getParentAssetsUrl()."/images/wave.jpg";
			if (!empty($v["shortDescription"])) {
				$shortDescription = $v["shortDescription"];
			}else{
				$shortDescription = "Aucune description";
			}

			if (@$v["metaImg"]) $img = Yii::app()->getModule("costum")->assetsUrl.$v["metaImg"];
		      	// $img = Yii::app()->createUrl(Yii::app()->getModule( $this->module->id )->getAssetsUrl().$v["metaImg"]);
			?>
			<div class='col-xs-12 col-sm-4 col-lg-4 shadow2 margin-bottom-10' style='min-height:300px;height:400px; overflow-y:hidden;'>
					<center>
						<div class='col-xs-12'>
							<img src='<?php echo $img?>' class='img-responsive'/>
						</div>
					</center>
					<div class='col-xs-12'>
						<a href='<?php echo Yii::app()->createUrl("/costum/co/index/slug/".@$_GET["slug"]."/test/".$v["slug"])?>' class='col-xs-12 elipsis' target='_blank'>
							<h3><?php echo @$v["name"]?></h3>
						</a>
						<span class='col-xs-12'><?php echo $shortDescription ?></span>
							<div class='col-xs-12 letter-red'>
						
					</div></div>
				</div>
	<?php }	?>

</div>
<?php } else {

	echo $this->renderPartial("co2.views.default.unTpl",array("msg"=>"Restricted Access","icon"=>"fa-hand-stop-o"));
	} ?>
