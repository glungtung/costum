<?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        //'/js/default/editInPlace.js',
        '/css/element/about.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@$this->appConfig["element"];
    if(isset($this->costum)){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["css"] == "about.css")
            array_push($cssJsCostum, '/css/'.$this->costum["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }

    $cssJsCostum = array(
		'/js/'.$this->costum["slug"].'/editInPlace.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
?>
<!--
<div class='col-md-12 margin-bottom-15 text-dark-blue'>
    <i class="fa fa-info-circle fa-2x"></i><span class='Montserrat' id='name-lbl-title'> <?php echo Yii::t("common","About") ?></span>
</div>
-->
<?php 
$edit = isset($_POST["canEdit"]) ? filter_var($_POST["canEdit"],FILTER_VALIDATE_BOOLEAN,FILTER_NULL_ON_FAILURE) : $edit ;
?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1">

    <div class="about-xs-completed visible-xs">
        <div class="about-avatar">
            <?php 
            $this->renderPartial('co2.views.pod.fileupload', 
                array(
                    "podId" => "modalDash",
                    "element"=>$element,
                    "edit" => $edit,
                    "openEdition" => false) 
                ); ?>   
        </div>
        <div class="well col-xs-12 net-well no-padding">
            <?php  //var_dump($params);exit; ?>
            <div id="menuCommunity" class="col-md-12 col-sm-12 col-xs-12 numberCommunity">
                
            </div>

            <div class="animated bounceInRight">
                <?php if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] != $invitedMe["invitorId"]) { ?>
                    <?php $this->renderPartial('co2.views.element.menus.answerInvite', 
                            array(  "invitedMe"      => $invitedMe,
                                    "element"   => $element
                                    ) 
                            ); 
                }else if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] == $invitedMe["invitorId"]) {?>
                    <div class="containInvitation">
                        <div class="statuInvitation">
                            <?php echo Yii::t("common", "Friend request sent") ?>
                            <?php 
                            $inviteCancel="Cancel";
                            $option=null;
                            $msgRefuse=Yii::t("common","Are you sure to cancel this invitation");

                            echo 
                            '<br>'.
                            '<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\''.$element["collection"].'\',\''.(string)$element["_id"].'\',\''.Yii::app()->session["userId"].'\',\''.Person::COLLECTION.'\',\''.Element::$connectTypes[$element["collection"]].'\',null,\''.$option.'\',\''.$msgRefuse.'\')" data-placement="bottom" data-original-title="'.Yii::t("common","Not interested by the invitation").'">'.
                                '<i class="fa fa-remove"></i> '.Yii::t("common",$inviteCancel).
                            '</a>';

                         ?>
                        </div>
                    </div>
                <?php }?>   
            </div>
           



            <div class="section-Community">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading no-padding" id="headingOne" role="tab">
                            <h4 class="panel-title">
                                <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Communauté<i class="pull-right fa fa-chevron-down"></i>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">

                            <div class="panel-body">
                                <?php if( $type == Person::COLLECTION){ ?>
                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["follows"])) {
                                                echo count($element["links"]["follows"]); 
                                            }else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Follows") ?> </span>
                                    </div>

                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["friends"])) {
                                                echo count($element["links"]["friends"]);
                                            }else{
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Friends") ?> </span>
                                    </div>

                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["memberOf"])) {
                                                echo count($element["links"]["memberOf"]);
                                            }else {
                                                echo 0;
                                            }?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Organizations") ?> </span>
                                    </div>
                                <?php } ?>

                                <?php if( $type == Organization::COLLECTION){ ?>
                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["members"])) {
                                                echo count($element["links"]["members"]); 
                                            }else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Members") ?> </span>
                                    </div>

                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["followers"])) {
                                                echo count($element["links"]["followers"]);
                                            }else{
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Followers") ?> </span>
                                    </div>

                                <?php } ?>

                                <?php if( $type == Project::COLLECTION){ ?>
                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["contributors"])) {
                                                echo count($element["links"]["contributors"]); 
                                            }else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Contributors") ?> </span>
                                    </div>

                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["followers"])) {
                                                echo count($element["links"]["followers"]);
                                            }else{
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Followers") ?> </span>
                                    </div>

                                <?php } ?>

                                <?php if( $type == Event::COLLECTION){ ?>
                                    <div class="col-xs-12 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["attendees"])) {
                                                echo count($element["links"]["attendees"]); 
                                            }else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Attendees") ?> </span>
                                    </div>

                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            
        </div>
    </div>

    
<div class="pod-info-Description"  id="pod-info-Description">
 <?php if  (Yii::app()->session["userId"] != null)  {?> 
    <span class="text-nv-3">
        <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
    </span>
    
    <?php
     if($edit==true ||( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
        <button class="btn-update-descriptions btn pad-2 pull-right tooltips"
                    data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update description") ?>" id="btn-descriptions">
        <?php if((!empty($element["shortDescription"])) || (!empty($element["description"])) ) {?>
            <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
        <?php }
                else{?>
            <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
            <?php } ?>
        </button>
   
    <?php } ?>

 <hr class="line-hr" >
  <?php } 

  else if((!empty($element["shortDescription"])) || (!empty($element["description"])) && (Yii::app()->session["userId"] == null )) { ?>  
        <span class="text-nv-3 letter-blue">
            <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
        </span> 
    <hr class="line-hr">
   <?php } ?> 

    <div id="contenuDesc">  
    <?php if  (!( (empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) {?> 
        <div class="contentInformation margin-10">
            <span id="shortDescriptionAbout" name="shortDescriptionAbout" style="font-size: 18px;" class="bold"><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
            <span id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit"  class="hidden" ><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></span>
        </div>
    <?php }
    if  (!( (empty($element["description"])) && Yii::app()->session["userId"] == null)) {?>
        
        <div class="contentInformation margin-10">
            <div class="more no-padding" id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
            </div>
        </div>   
    <?php }?>  

    </div>
</div>


    <div class="section light-bg pod-infoGeneral" id="pod-infoGeneral">
        <div class="row profil-title-informations">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <span class="text-nv-3">
                    <i class="fa fa-address-card-o"></i> <?php echo Yii::t("common","General information") ?>
                </span>
                <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
                <button class="btn-update-info btn btn-update-ig pad-2 pull-right  visible-xs tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>
                </button>
                <?php } ?>
            </div>
            <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
            <div class=" col-md-4 col-sm-4 col-xs-12 hidden-xs">
                <button class="btn-update-info btn btn-update-ig pad-2 pull-right tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                </button>
            </div>
            <?php } ?>

        </div>
        <hr class="line-hr">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
                <div class="col-md-6 col-sm-6 col-xs-12" >
                    <div class="card features">
                        <div class="card-body">

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-font gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="nameAbout"> <?php echo $element["name"]; ?> </p>
                                </div>
                            </div>

                            <?php if($type==Project::COLLECTION){ ?>
                            <!-- <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-spinner gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="avancementAbout"> <?php echo (@$element["properties"]["avancement"]) ? Yii::t("project",$element["properties"]["avancement"]) : '<i>'.Yii::t("common","Not specified").'</i>' ?> </p>
                                </div>
                            </div> -->
                            <?php $category=array("common"=>"Commun","research"=>"Recherche-action")?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-creative-commons gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="categoryAbout"> <?php echo (@$element["category"] && in_array($element["category"], array_keys(LabEnCommuns::$category))) ? LabEnCommuns::$category[$element["category"]] : '<i>'.Yii::t("common","Not specified").'</i>' ?> </p>
                                </div>
                            </div>
                                <?php if(isset($element["category"]) && $element["category"]=="research") { 
                                    $state=array("idea"=>"Idée","development"=>"En cours");
                                ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-tasks gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="categoryAbout"> <?php echo (@$element["properties"]["avancement"] && in_array($element["properties"]["avancement"], array_keys(LabEnCommuns::$avancement))) ? LabEnCommuns::$avancement[$element["properties"]["avancement"]]: '<i>'.Yii::t("common","Not specified").'</i>' ?> </p>
                                </div>
                            </div>        

                                <?php } ?>   
                            <?php } ?>

                            <?php if($type==Person::COLLECTION) { ?>
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-user-secret gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10"
                                           id="usernameAbout"><?php echo (@$element["username"]) ? $element["username"] : '<i>' . Yii::t("common", "Not specified") . '</i>' ?></p>
                                    </div>
                                </div>
                                <?php if (Preference::showPreference($element, $type, "birthDate", Yii::app()->session["userId"])) { ?>
                                    <div class="media contentInformation" >
                                        <span class="ti-2x mr-3"><i class="fa fa-birthday-cake gradient-fill"></i></span>
                                        <div class="media-body">
                                            <p class="padding-top-10" id="birthDateAbout"><?php echo (@$element["birthDate"]) ? date("d/m/Y", strtotime($element["birthDate"]))  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                        </div>
                                    </div>

                                <?php }
                            }

                            if($type==Organization::COLLECTION || $type==Event::COLLECTION){ ?>
                            <div class="media contentInformation" id="divTypeAbout">
                                <span class="ti-2x mr-3"><i class="fa fa-list-ul gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10"  id="typeAbout">
                                        <?php
                                        if(@$typesList && @$element["type"] && !empty($typesList[$element["type"]]))
                                            $showType=Yii::t( "category",$typesList[$element["type"]]);
                                        else if (@$element["type"])
                                            $showType=Yii::t( "category",$element["type"]);
                                        else
                                            $showType='<i>'.Yii::t("common","Not specified").'</i>';
                                        echo $showType; ?>
                                    </p>
                                </div>
                            </div>
                            <?php }

                            if( (   $type==Person::COLLECTION &&
                                Preference::showPreference($element, $type, "email", Yii::app()->session["userId"]) ) ||
                            in_array($type, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])) { ?>

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="emailAbout"><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card features">
                        <div class="card-body">
                            <?php if( $type != Person::COLLECTION /*&& $type != Organization::COLLECTION */ ){ ?>
                            <div class="media contentInformation" id="divParentAbout">
                                <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Carried by"); ?></span>
                                    <p class="padding-top-10" id="parentAbout">
                                        <?php
                                        if(!empty($element["parent"])){
                                            $count=count($element["parent"]);
                                            foreach($element['parent'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php   }
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type == Event::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Organized by"); ?></span>
                                    <p class="padding-top-10" id="organizerAbout">
                                        <?php
                                        if(!empty($element["organizer"])){
                                            $count=count($element["organizer"]);
                                            foreach($element['organizer'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php   }
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type!=Poi::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-desktop gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="webAbout">
                                        <?php
                                        if(@$element["url"]){
                                            //If there is no http:// in the url
                                            $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
                                            echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php  if($type==Organization::COLLECTION || $type==Person::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-phone gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="fixeAbout">
                                        <?php
                                        $fixe = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["fixe"]))
                                            $fixe = ArrayHelper::arrayToString($element["telephone"]["fixe"]);

                                        echo $fixe;
                                        ?>
                                    </p>
                                </div>
                            </div>


                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-mobile gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="mobileAbout">
                                        <?php
                                        $mobile = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["mobile"]))
                                            $mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);
                                        echo $mobile;
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>

                     <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="card features hidden-xs margin-bottom-20">

                         </div>

                        <?php if(!empty($element["tags"])) { ?>
                        <div class="card features margin-bottom-20">
                            <div class="card-body">

                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-tags gradient-fill"></i></span>
                                    <div class="media-body">
                                        <ul class="tag-list no-padding">
                                            <?php
                                            if(!empty($element["tags"])){
                                                foreach ($element["tags"]  as $key => $tag) {
                                                    echo '<li class="tag"><a href="#search?text=#'.$tag.'"> <i class="fa fa-tag icon-before"></i>&nbsp;'.$tag.'</a></li>';
                                                }
                                            }else{
                                                echo '<i>'.Yii::t("common","Not specified").'</i>';
                                            } ?>

                                            <!--<li><a href=""><i class="fa fa-tag"></i> tag</a></li>-->
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div >
                        <?php } ?>
                    </div>
                </div>
            </div>
    </div>
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-section2">
    <?php if(in_array($type, [Event::COLLECTION ,Project::COLLECTION ,Organization::COLLECTION])){ 
        $modeDate="recurrency";
        $title = Yii::t("common","When");
        $emptyval = Yii::t("common","No date");
        if($type == Organization::COLLECTION) {
            $title = Yii::t("common","Opening hours");
            $emptyval = Yii::t("common","No opening hours");
            $modeDate="openingHours";

        } else if($type==Project::COLLECTION){
                    $modeDate="date";
            }
                $this->renderPartial('co2.views.pod.dateOH', array("element" => $element, "title" => $title, "emptyval" => $emptyval, "edit" => $edit , "openEdition" => $openEdition));
        };?>
    
    <!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->
    <?php $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
    <?php $this->renderPartial('co2.views.pod.newSocialNetwork', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    
</div>
    
</div>
<?php $this->renderPartial('../pod/whycommunexion',array()); ?>

<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function () {
        controleAffiche();
    });

    function controleAffiche() {
        mylog.log(contextData.shortDescription, contextData.description);
        if((typeof contextData.shortDescription == "undefined") && (typeof contextData.description == "undefined")){
            $("#contenuDesc").addClass("hidden");
            //$("#btn-descriptions").html('<i class="fa fa-plus"> Ajouter');

            };
    };


    function afficheMap(){
            mylog.log("afficheMap");

            //Si contextData.geo est undefined caché la carte
            if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
            };

            var paramsMapContent = {
                container : "divMapContent",
                latLon : contextData.geo,
                activeCluster : false,
                zoom : 16,
                activePopUp : false
            };


            mapAbout = mapObj.init(paramsMapContent);

            var paramsPointeur = {
                elt : {
                    id : contextData.id,
                    collection : contextData.type,
                    geo : contextData.geo
                },
                center : true
            };
            mapAbout.addMarker(paramsPointeur);
            mapAbout.hideLoader();


    };


    //var paramsPointeur[contextId] = contextData;
    var formatDateView = "DD MMMM YYYY à HH:mm" ;
    var formatDatedynForm = "DD/MM/YYYY HH:mm" ;

    jQuery(document).ready(function() {
        bindDynFormEditable();
        initDate();
        inintDescs();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-update-organizer").click(function(){
            updateOrganizer();
        });
        $("#btn-add-organizer").click(function(){
            updateOrganizer();
        });

        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });

        coInterface.bindLBHLinks();

    });
    function inintDescs() {
        mylog.log("inintDescs");
        if($("#descriptionAbout").length > 0){
            if(canEdit == true || openEdition== true)
                descHtmlToMarkdown();
            mylog.log("after");
            mylog.log("inintDescs", $("#descriptionAbout").html());
            var descHtml = "<i>"+trad.notSpecified+"</i>";
            if($("#descriptionAbout").html().length > 0){
                descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
            }
            $("#descriptionAbout").html(descHtml);
            //$("#descProfilsocial").html(descHtml);
            mylog.log("descHtml", descHtml);
        }
    }

    function initDate() {//DD/mm/YYYY hh:mm

        formatDateView = "DD MMMM YYYY à HH:mm" ;
        formatDatedynForm = "DD/MM/YYYY HH:mm" ;
        
        mylog.log("formatDateView", formatDateView);
        //if($("#startDateAbout").html() != "")

            $("#startDateAbout").html(moment(contextData.startDateDB).local().locale(mainLanguage).format(formatDateView));

            $("#endDateAbout").html(moment(contextData.endDateDB).local().locale(mainLanguage).format(formatDateView));

        if($("#birthDate").html() != "")
            $("#birthDate").html(moment($("#birthDate").html()).local().locale(mainLanguage).format("DD/MM/YYYY"));
        $('#dateTimezone').attr('data-original-title', "Fuseau horaire : GMT " + moment().local().format("Z"));
    }

    function AddReadMore() {
    var showChar = 300;
    var ellipsestext = "...";
    var moretext = "<?php echo Yii::t("common","Read more")?>";
    var lesstext = "<?php echo Yii::t("common","Read less")?>";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);
        //var h = content.substr(showChar-1, content.length - showChar);

        var html = '<span class="container "><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

        $(this).html(html);
        $(this).after('<a href="" class="morelink">' + lesstext + '</a>');
      }

    });

    $(".morelink").click(function() {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.morecontent').fadeToggle(100, function(){
          $(this).prev().fadeToggle(100);
        });
       
      } else {
        $(this).addClass("less");
        $(this).html(moretext);
        $(this).prev().children('.container').fadeToggle(100, function(){
          $(this).next().fadeToggle(100);
        });
      }
      //$(this).prev().children().fadeToggle();
      //$(this).parent().prev().prev().fadeToggle(500);
      //$(this).parent().prev().delay(600).fadeToggle(500);
      
      return false;
    });
}
$(function() {
    //Calling function after Page Load
    AddReadMore();
});



</script>

