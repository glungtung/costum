<?php 

$cssAnsScriptFilesTheme = array(
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js',
  
  "/plugins/jquery-counterUp/waypoints.min.js",
  "/plugins/jquery-counterUp/jquery.counterup.min.js"
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  
$poiList = array();
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                           "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                           "type"=>"cms") );
} 

$bannerImg = @$el["profilRealBannerUrl"] ? Yii::app()->baseUrl.$el["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/filiereCostum/no-banner.jpg";
$descriptions = @$this->costum["descriptions"] ? $this->costum["descriptions"] : null;
$title = @$this->costum["title"] ? $this->costum["title"] : null;

?>
<style>
section{
    background-image : url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
}
.loader{
    display : none;
}
    .header{
        background-image : url(<?= $bannerImg; ?>);
        background-size: cover;
        padding-bottom: 45%;
    }
    .agenda{
        margin-top: 5%;
    }
    .card{
        margin-top: 7%;
    }
    .info-card{
        border-radius: 15px;
        background: #95c235;
        padding: 3%;
        font-size: 25px;
        color : white;
        box-shadow: 4px 3px 7px 3px #dadada;
        margin-left: 13%;
        margin-right: 13%;
    }
    .description{
        border: 1px;
    }
    .img-hexa{
        margin-top: -25%;
    }

    .hexagon {
    overflow: hidden;
    visibility: hidden;
    -webkit-transform: rotate(120deg);
       -moz-transform: rotate(120deg);
        -ms-transform: rotate(120deg);
         -o-transform: rotate(120deg);
            transform: rotate(120deg);
    cursor: pointer;
    }
    .hexagon1 {
    width: 400px;
    height: 200px;
    margin: 0 0 0 -80px;
    }

    .hexagon-in1 {
    overflow: hidden;
    width: 100%;
    height: 100%;
    -webkit-transform: rotate(-60deg);
       -moz-transform: rotate(-60deg);
        -ms-transform: rotate(-60deg);
         -o-transform: rotate(-60deg);
            transform: rotate(-60deg);
    }
.hexagon-in2 {
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: 50%;
    visibility: visible;
    -webkit-transform: rotate(-60deg);
       -moz-transform: rotate(-60deg);
        -ms-transform: rotate(-60deg);
         -o-transform: rotate(-60deg);
            transform: rotate(-60deg);
    }
    #second-search-bar-addon{
        background-color: #2cb522 !important;
    }
    #wizard{
        margin-top: 3%;
    }
    .markdown > p {
        font-style: italic;
        text-align: center;
        color:#878786;
        padding-bottom: 4%;
        padding-left: 5%;
        padding-right: 5%;
    }
    #second-search-bar{
        padding: 10px;
        font-size: 14px;
        height: 40px;
        font-weight: 200;
        border-radius: none !important;
        width: 90%;
        float: left;
        -webkit-box-shadow: none !important;
        -moz-box-shadow: 0px 0px 2px -1px rgba(0,0,0,0.5);
        box-shadow: none !important;
        border: none !important;
    }
</style>
<div class="header">

<?php

$params = [  "tpl" => "templateCostumDesTiersLieux","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el ];
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );
?>
        <!-- Edit Banniere -->
        <?php 
        $params =  array(
            "type"=> @$el["type"], 
            "id"=>(string) @$el["_id"], 
            "name"=> @$el["name"],
            "canEdit" => $canEdit,
            "profilBannerUrl"=> @$el["profilRealBannerUrl"]);

        echo $this->renderPartial("costum.views.tpls.modalHeader", $params); 
        ?>
    <div class="col-xs-12" style="padding-top: 3%;"><center><img src="<?= @$this->costum["logo"]; ?>" class="img-responsive" style="width:45%;"></center></div>
</div>
<div class="searchbar col-md-12">
<!-- DEBUT SVG -->
<div class=" text-center">
    <?xml version="1.0" encoding="utf-8"?>
    <!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 80 1006 95" style="enable-background:new 0 0 1006 217; margin-top: -2%; width: 50%;"" xml:space="preserve">
    <style type="text/css">
        .st0{fill:#FFFFFF;}
        .st1{fill:#9AC21E;}
    </style>
    <g>
        
            <image style="overflow:visible;opacity:0.29;" width="956" height="124" xlink:href="BD554E7F0366A2C6.png"  transform="matrix(1 0 0 1 20.4646 63.5639)">
        </image>
        <g style="filter: drop-shadow(0 0px 10px rgba(0, 0, 0, 0.5));">
            <polygon class="st0" points="943.5,117.4 922.2,80.5 908.1,80.5 879.6,80.4 77,80.4 77,80.5 58.9,80.5 37.5,117.4 58.9,154.4 
                77,154.4 77,154.5 737.5,154.5 879.6,154.4 922.2,154.4 		"/>
        </g>
    </g>
    <g>
    <a data-type="filters" href="javascript:;" id="second-search-bar-addon-smarterre">
        <!-- <div id="second-search-bar-addon-smarterre" class="main-search-bar-addon-smarterre"> -->
            <polygon class="st1" points="921.7,80.2 878.5,80.2 857.8,116.7 879.4,154 922.6,154 944.2,116.7 	"/>
            <path class="st0" d="M919,116.9l-2.3-2.3l0,0l-6.3-6.3l-2.3,2.3l4.5,4.4h-28v3.2h28.5l-5,5l2.3,2.3l6.3-6.3l0,0L919,116.9
            L919,116.9L919,116.9z M914.2,117.2v-0.5l0.3,0.3L914.2,117.2z"/>
        <!-- </div> -->
    </a>
    </g>
    <g>
	    <text x="125" y="130"></text>
    </g>
    <path class="st1" d="M110.7,138.8c-0.1-0.1-0.1-0.2-0.2-0.2c-3.3-3.3-6.6-6.6-9.9-9.9c-0.1-0.1-0.1-0.1-0.2-0.2
        c-4,2.8-8.3,3.7-13.1,2.5c-3.8-0.9-6.8-3.1-9.1-6.3c-4.4-6.3-3.4-15.1,2.4-20.4c5.7-5.2,14.6-5.4,20.5-0.5
        c6.1,5.1,7.7,14.3,2.7,21.3c0.1,0.1,0.1,0.1,0.2,0.2c3.3,3.3,6.5,6.5,9.8,9.8c0.1,0.1,0.2,0.1,0.3,0.2c0,0,0,0.1,0,0.1
        C113,136.5,111.9,137.7,110.7,138.8C110.7,138.8,110.7,138.8,110.7,138.8z M80.4,115.9c0,6,4.9,10.9,10.8,10.9
        c6,0,10.8-4.9,10.8-10.8c0-6-4.8-10.9-10.8-10.9C85.3,105,80.4,109.9,80.4,115.9z"/>
    </svg>
</div>

<div class="hidden-xs" style="margin-top: -4.5%; position: absolute; width: 450px; border: none; margin-left: 31%;">
				<input type="text" class="main-search-bar barS" id="second-search-bar" placeholder="Rechercher un territoire">
			
				<!-- <a data-type="filters" href="javascript:;">
					<span id="second-search-bar-addon-smarterre" style="position: absolute;z-index: 10000;cursor: pointer;background-color: #0ddff5;margin-left: 34vw;border-radius: 32px;width: 5vw;height: 40px;" class="text-white input-group-addon pull-left main-search-bar-addon-smarterre">

						<i style="margin-top: -6%;font-size: 34px" class="fa fa-search">
						</i>
					</span>
				</a> -->
				<div id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding" style="top: 4px;max-height: 100%;background-color:white;width: 110%;margin-left: 0%;z-index: 1000;">
				</div>
			</div>
<!-- FIN SVG -->
</div>

<!-- ESPACE DESCRIPTION -->
<div class="description container text-center">
    <?= $this->renderPartial("costum.views.tpls.text", 
    array(
        "poiList" => $poiList, 
        "tag" => "descriptionsfil1")); 
    ?>
</div>

<!-- Charge TPLS -->
<?php 
if(@$this->costum["tpls"]){
foreach (@$this->costum["tpls"] as $key => $value) : 
    $params = array("canEdit"   =>    $canEdit,
                    "poiList"   =>    $poiList);

    if($key === "communityCaroussel") $params["roles"] = @$value["roles"];

    $color = @$value["color"] ? $value["color"] : "black";
    ?>

<div class="actu text-center container">
    <h1 style="
    font-size: 30px; 
    background: #95c235; 
    padding: 2%;
    border-radius: 15px; 
    box-shadow: 4px 3px 7px 3px #dadada;
    color : <?= $color ?>
    ">
    <?php if(@$value["icon"] && !empty($value["icon"])) : ?>
            <i class="fa <?= @$value["icon"] ?>" aria-hidden="true"></i>
    <?php endif ?>
    <?= @$value["title"]; ?></h1>

    <?php
        $path = preg_split('/(?=[A-Z])/',$key);
        if(@$path[1]){
            $y = $path[0].$path[1];
            echo $this->renderPartial("costum.views.tpls.$path[0].$y", $params);
        }
        else{
            echo $this->renderPartial("costum.views.tpls.$path[0]", $params);
        }
    ?>
</div>
<?php endforeach; }?>


<?php if($canEdit) : ?> 
    <!-- ESPACE ADMIN --> 
    <hr>
    <div class="container">
        <a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent" data-id="<?= $this->costum["contextId"]; ?>" data-collection="<?= $this->costum["contextType"]; ?>"><i class="fa fa-plus"></i> Ajouter une section</a>
    </div>
<?php endif ?>

<script type="text/javascript">

var mapTemplateCostumDesTiersLieux = {};
var dataSearchTemplateCostumDesTiersLieux = {} ;

jQuery(document).ready(function() {
    setTitle(costum.title);

    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

    $("#donation-btn").hide();
    $(".loader").hide();

    $('.counter').counterUp({
  	  delay: 10,
  	  time: 2000
  	});
  
 	$('.counter').addClass('animated fadeInDownBig');
  	$('h3').addClass('animated fadeIn');
});

$("#second-search-bar").off().on("keyup",function(e){ mylog.log("keyup #second-search-bar");
            $("#input-search-map").val($("#second-search-bar").val());
            $("#second-search-xs-bar").val($("#second-search-bar").val());
            if(e.keyCode == 13){
                searchObject.text=$(this).val();
                searchObject.searchType = ["costum"];
                searchObject.tags = ["territoire","tco"];
                myScopes.type="open";
                myScopes.open={};
               startGlobalSearch(0, indexStepGS);
           		$("#dropdown").css('display','block');
             }	
});

    $("#second-search-xs-bar").off().on("keyup",function(e){ mylog.log("keyup #second-search-bar");
                $("#input-search-map").val($("#second-search-xs-bar").val());
                $("#second-search-bar").val($("#second-search-xs-bar").val());
                if(e.keyCode == 13){
                    searchObject.text=$(this).val();
                    searchObject.searchType = ["costum"];
                    searchObject.tags = ["territoire","tco"];
                    myScopes.type="open";
                    myScopes.open={};
                    startGlobalSearch(0, indexStepGS);
                    $("#dropdown").css('display','block');            
                }
    });

    $("#second-search-bar-addon-smarterre, #second-search-xs-bar-addon").off().on("click", function(){
                $("#input-search-map").val($("#second-search-bar").val());
                searchObject.text=$("#second-search-bar").val();
                searchObject.searchType = ["costum"];
                searchObject.tags = ["territoire","tco"];
                myScopes.type="open";
                myScopes.open={};
                startGlobalSearch(0, indexStepGS);
                $("#dropdown").css('display','block');
    });



</script>