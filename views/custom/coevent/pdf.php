<?php
$banniere = [
	'banner' => Yii::app()->getModule("costum")->getAssetsUrl() . '/images/coevent/no-banner.jpg',
	'logo' => Yii::app()->getModule("costum")->getAssetsUrl() . '/images/coevent/logo.png'
];

$currentEvent = PHDB::findOneById(Event::COLLECTION, new MongoId($_GET['event']));

$documents = Document::getListDocumentsWhere(
	[
		'source.key' => $currentEvent['slug'],
		'type' => 'cms',
	],
	'image'
);

foreach ($documents as $document) {
	switch ($document['subKey']) {
		case 'logo':
		case 'banner':
			$banniere[$document['subKey']] = $document['imagePath'];
			break;
	}
}


$fields = ["name", "shortDescription", "description", "startDate", "endDate", 'profilRealBannerUrl', 'timeZone', 'profilMediumImageUrl'];

$subEventsDatabase = PHDB::findAndSort(Event::COLLECTION, [
	'parent.' . $_GET['event'] => ['$exists' => true],
	'_id' => ['$ne' => new MongoId($_GET['event'])],
], ['startDate' => 1], 0, $fields);
$subEventsCode = [];
foreach ($subEventsDatabase as $subEventDatabase) {
	$tempSubEvent = [];
	$tempSubEvent["name"] = $subEventDatabase["name"];
	$tempSubEvent["shortDescription"] = isset($subEventDatabase["shortDescription"]) ? $subEventDatabase["shortDescription"] : "";
	$tempSubEvent["fullDescription"] = isset($subEventDatabase["description"]) ? $subEventDatabase["description"] : "";
	$tempSubEvent["startDate"] = $subEventDatabase["startDate"]->toDateTime();
	$tempSubEvent["endDate"] = $subEventDatabase["endDate"]->toDateTime();
	$tempSubEvent["background"] = $subEventDatabase["profilRealBannerUrl"] ?? '';
	if (!empty($subEventDatabase["timeZone"])) {
		$tempSubEvent["endDate"]->setTimeZone(new DateTimeZone($subEventDatabase["timeZone"]));
		$tempSubEvent["startDate"]->setTimeZone(new DateTimeZone($subEventDatabase["timeZone"]));
	}
	$tempSubEvent["logo"] = $subEventDatabase["profilMediumImageUrl"] ?? '';

	$subEventsCode[] = $tempSubEvent;
}
$parser = new Parsedown();
$structField = "structags";
$cssAnsScriptFilesModule = array(
	'/plugins/font-awesome/css/font-awesome.min.css',
	'/plugins/font-awesome-custom/css/font-awesome.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
?>
<img src="<?= $banniere['banner'] ?>" alt="banniere" style="width: 800px; height: 300px;display: block; position: absolute;">
<h1 style="font-family: sans-serif; position: fixed; top: 15px; left: 15px;"><img src="<?= $banniere['logo'] ?>" alt="logo" style="height: 50px;"> <?= $currentEvent["name"] ?></h1>
<?php if (!empty($currentEvent["shortDescription"])) { ?>
	<p style="font-size: 16px; font-family: sans-serif;"><span style="font-size: 16px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong><em><?= $currentEvent["shortDescription"] ?></em></strong></p>
<?php } ?>
<?php if (!empty($currentEvent["description"])) { ?>
	<p style="font-size: 16px; font-family: sans-serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $parser->text(@$currentEvent["description"]) ?></p>
<?php } ?>

<?php
foreach ($subEventsCode as $subEventCode) {
?>
	<img src="<?= $subEventCode['logo'] ?>" style="width: 100px; height: 100px;margin-top:20px;" />
	<span><b><?= $subEventCode["name"] ?></b></span>
	<?php
	$start_date = [
		'datetime' => $subEventCode['startDate'],
		'coded_date' => $subEventCode['startDate']->format('Y-m-d'),
		'formated_date' => $subEventCode['startDate']->format('d') . ' ' . Yii::t('translate', $subEventCode['startDate']->format('F')) . ' ' . $subEventCode['startDate']->format('Y'),
		'formated_hour' => $subEventCode['startDate']->format('H:i')
	];
	$end_date = [
		'datetime' => $subEventCode['endDate'],
		'coded_date' => $subEventCode['endDate']->format('Y-m-d'),
		'formated_date' => $subEventCode['endDate']->format('d') . ' ' . Yii::t('translate', $subEventCode['endDate']->format('F')) . ' ' . $subEventCode['endDate']->format('Y'),
		'formated_hour' => $subEventCode['endDate']->format('H:i')
	];
	if ($start_date['coded_date'] == $end_date['coded_date']) {
	?>
		<p style="font-family: sans-serif;">Le <?= $start_date['formated_date'] ?> de <span style="font-family: sans-serif;"><?= $start_date['formated_hour'] ?></span> à <span style="font-family: sans-serif;"><?= $end_date['formated_hour'] ?></span></p>
	<?php
	} else {
	?>
		<p style="font-family: sans-serif;">Le <?= $start_date['formated_date'] ?> à <span style="font-family: sans-serif;"><?= $start_date['formated_hour'] ?></span> jusqu'au <?= $end_date['formated_date'] ?> à <span style="font-family: sans-serif;"><?= $end_date['formated_hour'] ?></span></p>
	<?php
	}
	if (!empty($subEventCode["shortDescription"])) { ?>
		<p style="font-size: 16px; font-family: sans-serif;"><span style="font-size: 16px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong><em><?= $subEventCode["shortDescription"] ?></em></strong></p>
	<?php }
	if (!empty($subEventCode["background"])) { ?>
		<p style="text-align: center;"><img style="width: 500px; display: block;" src="<?= $subEventCode["background"] ?>"></p>
	<?php }
	if (!empty($subEventCode["fullDescription"])) { ?>
		<div style="font-family: sans-serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $parser->text($subEventCode["fullDescription"]) ?></div>
	<?php } ?>
	<hr />
<?php
}
?>