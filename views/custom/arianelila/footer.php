<style type="text/css">

.menu-line > .nav-tabs > li.active {
  border: 0;
  border-top: 4px solid #01dc94;
}
.nav-tabs {
  border: 0;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
  cursor: default;
  border: 0;
  color: #D0C5FB;
  background-color: #0b464f;

}

.nav-tabs>li >a:hover {
  border: 0;
}

.lila-footer-panel{
  background-color: #0b464f;
  color: #D0C5FB;
  min-height: 150px;
  font-family: FiraSans-SemiBold !important;
  font-size: 12px;

}
.lila-footer-panel a{
  color: #D0C5FB;

}

.menu-line > .nav-tabs > li:hover::before {
  width: 100%;
}

.menu-line > .nav-tabs > li::before {
  content: '';
  display: block;
  width: 0;
  height: 4px;
  background: #9a52fc;
  transition: width .3s;
  position: absolute;
}

#menu-lila a {
  font-size: 12px;  
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 20px;
  color: #cfc5fa ;
  font-family: FiraSans-SemiBold !important;
}

/************menu-top menu style************/
#mainNav #menuTopLeft a .active, .nav-tabs .active:hover, .nav-tabs .active:focus {
    border: 0;
    border-top: 4px solid #01dc94;
}

#menuTopLeft .active{
  border: 0;
  border-bottom: 4px solid #01dc94;
  position: relative;
}


#menuTopLeft .nav-tabs:hover::after {
  width: 10%;
}

#menuTopLeft .nav-tabs::after {
  content: '';
  display: block;
  width: 0;
  height: 4px;
  background: #9a52fc;
  margin-top: 10px;
  transition: width .3s;
  position: absolute;
}
/************End menu-top menu style************/

.btn-abonne {
  background-color: #9a52fc; 
  border-radius: 50%;
  margin-left: 10px;
  border: 0;
  height: 25px;
  width: 25px;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 20px;
  line-height: 0;
  border-radius: 15px;
  background-color: #9a52fc;
  color: #0b464f;
}
::-webkit-input-placeholder {
   font-style: italic;
}
:-moz-placeholder {
   font-style: italic;  
}
::-moz-placeholder {
   font-style: italic;  
}
:-ms-input-placeholder {  
   font-style: italic; 
}
.lila-logo {
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  background-image: url("<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/arianelila/rr.jpg");
  height: 60px;
  width: 100%;
}

.open-atlas-logo {
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  background-image: url("<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/arianelila/logo-openatlas.png");
  height: 60px;
  width: 100%;
}

.panel-form .widget-body .bg-active {
    background-color: #994aff !important;
}

.footer-arianelila{
  display:flex;
  flex-direction:row;
  flex-wrap: wrap;
  position:relative;
  flex-shrink: 0;
}
.footer-arianelila ::placeholder { 
  color: black;
  opacity: 1; 
}

.addPadding {
  padding-left: 20% !important;
  padding-right: 20% !important;
}
.addPadding2 {
  padding-left: 3% !important;
  padding-right: 3% !important;
}

.addPadding .col-md-6 {
  width: 100% !important;
}

@media (max-width: 900px) {
  .addPadding {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }
}

  

@media (max-width: 600px) {
  .addPadding {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  .lila-footer-panel div {
    width: 100%;
  }

  .lila-footer-panel .input-group-btn{
    display: list-item;
  }
   .footer-arianelila {
    /*flex-direction:column;*/
    left: 0 !important;
    tip: 5px !important;
    width: 100% !important;
  }
}
</style>
<div class="lila-footer-panel footer-arianelila" style="width: 100%;justify-content: center;">
  <div class="footer-arianelila padding-bottom-10" style="width: 22%;align-items: end;font-family: 'Arial'">
      <img class="img-responsive" style="max-height: 50px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/arianelila/LOGO.png">
    <span>Un cabinet d’accompagnement professionnel des individus, des organisations et d’innovation sociétale.</span>
      <a class="bold" target="_blank" href="#mention" style="width:100%">Mentions légales</a>
      <a class="bold" target="_blank" href="#condition" style="width:100%">Conditions générales de vente</a><br>
  
  </div>
  <div class="footer-arianelila" style="width: 55%;justify-content: center;">
    <div class="menu-line footer-arianelila" style="width: 100%;justify-content: center;">
      <div class="footer-arianelila" style="width: 100%;justify-content: center;">
        <a class="lbh-anchor padding-20" href="#luniversariane" data-toggle="tab">
        L'UNIVERS ARIANE </a>
        <a class="lbh-anchor padding-20" href="#nosaccompagnements" data-toggle="tab">
        NOS ACCOMPAGNEMENTS </a>
        <a class="lbh-anchor padding-20" href="#evenement" data-toggle="tab">
        ÉVÉNEMENTS</a>
        <a class="lbh padding-20" href="#blogs" data-toggle="tab">
        BLOGS </a>
        <a  href="#contact" class="lbh padding-20">
        CONTACT  </a>
        <a class="lbh padding-20" href="#communaute" data-toggle="tab">
        COMMUNAUTÉ  </a>
      </div>
    </div>


  </div>

  <div class="footer-arianelila padding-top-20 text-center" style="width:20%;justify-content: center;">
    <!-- <div class="input-group" style="width: 80%;">
      <input style="height: 25px;font-family: 'Arial';" class="form-control elem-control" type="text" placeholder="Je m'abonne à la newsletter"> 
      <div class="input-group-btn">                    
        <button class="tooltips btn-abonne" type="button" data-placement="bottom" title="" data-original-title="Valider">
          <i style="font-size: 21px;" class="fa fa-play" aria-hidden="true"></i><br>
        </button> 
      </div>
    </div> -->

    <div class="footer-arianelila padding-top-20" style="width: 100%;justify-content: center;">
      <div class="lila-logo"></div>
        <p class="padding-top-20" style="font-size:12px">Avec le soutien financier de la Région Réunion</p>
    </div>

  </div>
</div>
<div class="footer-arianelila bg-dark padding-10" style="justify-content: center;">
  <span>Copyright LILA-Tous droits réservés</span>
</div>
<script type="text/javascript">;
setTimeout(function(){
  $("#customHeader").addClass("addPadding");
  $(".questionList").css("padding","30px");
  $(".central-section").css("padding-top","0");
  $("#social-header").remove();
  $("#menu-top-profil-social").remove();
  $(".tools-dropdown").find($('a[href="#nosaccompagnements"]')).text("Accompagnement sur mesure");
}, 2000);
</script>