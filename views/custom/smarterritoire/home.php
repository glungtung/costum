<?php  
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
      // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'            
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 



if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  
  $poiList = PHDB::find(Poi::COLLECTION, 
      array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
         "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
         "type"=>"cms") );
} 

//   var_dump($el);exit;

$bannerImg = @$el["profilRealBannerUrl"] ? Yii::app()->baseUrl.$el["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/smarterritoireautre/phototerritoire.jpg";
$logo = @$this->costum["logo"] ? $this->costum["logo"] : null;
$title = @$this->costum["title"] ? $this->costum["title"] : null;
$sectionTitleColor = @$this->costum["cms"]["sectionTitleColor"];
$sectionColor = @$this->costum["cms"]["sectionColor"];
?>

<?php 
$params = [  "tpl" => "smarterritoire","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el ];
echo $this->renderPartial("costum.views.tpls.tplsEngine", $params,true ); 
?>
<style type="text/css">

    section {
        background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoire/backgroundlink.jpg);
    }
    .smarterritoire-header{
        /*background: #001146;*/
        background-image : url(<?= $bannerImg; ?>);
        width: 101%;
        height: auto;
        background-size: cover;
        padding-bottom: 5%;
    }
    .smarterritoire-carousel-control.left{
        background-image : none !important;
        background-repeat : none !important;
    }
    .smarterritoire-carousel-control.right{
        background-image : none !important;
        background-repeat : none !important;
    }
</style>

<div class="smarterritoire-header">
    <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 821.8 613" style="enable-background:new 0 0 821.8 613;" xml:space="preserve">
<a href="#citoyennete" class="lbh" title="Citoyenneté"><?php echo Yii::t("home","") ?>
<polygon id="dessus" points="501.7,208.5 501.7,101.8 409.3,48.5 317,101.8 317,208.5 409.3,261.8 "/>
<polygon id="2" class="hex" points="501.7,208.5 501.7,101.8 409.3,48.5 317,101.8 317,208.5 409.3,261.8 " fill="url(#img2)"/>

<defs>
  <pattern  id="img2" patternUnits="userSpaceOnUse" width="268" height="262">
    <image  xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/citoyennete.jpg" width="800" height="366" />
    </pattern>
</defs>
<text class="text2" font-size="20" x="413" y="168" text-anchor="middle">CITOYENNETE</text>
</a>

<a href="#sante" class="lbh" title="Alimentation et Santé"><?php echo Yii::t("home","") ?>
<polygon id="dessus"  points="693,208.5 693,101.8 600.6,48.5 508.3,101.8 508.3,208.5 600.6,261.8 "/>
<polygon id="3" class="hex" points="693,208.5 693,101.8 600.6,48.5 508.3,101.8 508.3,208.5 600.6,261.8 " fill="url(#img3)"/>

<defs>
  <pattern id="img3" patternUnits="userSpaceOnUse" width="229.5" height="265">
    <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/alimentation.png" width="335" height="306" />
    </pattern>
</defs>
<text class="text3" font-size="20" x="602" y="168" text-anchor="middle">ALIMENTATION</text>
<text class="text3" font-size="20" x="602" y="191" text-anchor="middle">SANTE</text>
</a>

<a href="#construction" class="lbh" title="Construction"><?php echo Yii::t("home","") ?>
<polygon id="dessus"  points="407.3,372.9 407.3,266.3 315,212.9 222.6,266.3 222.6,372.9 315,426.3 "/>
<polygon id="5" class="hex" points="407.3,372.9 407.3,266.3 315,212.9 222.6,266.3 222.6,372.9 315,426.3 " fill="url(#img5)"/>

<defs>
  <pattern id="img5" patternUnits="userSpaceOnUse" width="500" height="500">
    <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/construction.png" width="540" height="558" />
    </pattern>
</defs>
<text class="text5" font-size="18" x="318" y="352" text-anchor="middle">CONSTRUCTIONS</text>
<text class="text5" font-size="20" x="315" y="329" text-anchor="middle">AMENAGEMENT</text>
</a>

<a href="#education" class="lbh" title="Education"><?php echo Yii::t("home","") ?>
<polygon id="dessus" points="598.6,372.9 598.6,266.3 506.3,212.9 413.9,266.3 413.9,372.9 506.3,426.3"/>
<polygon id="6" class="hex" points="598.6,372.9 598.6,266.3 506.3,212.9 413.9,266.3 413.9,372.9 506.3,426.3" fill="url(#img6)"/>

<defs>
  <pattern id="img6" patternUnits="userSpaceOnUse" width="303" height="211">
    <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/education.png" width="361" height="220" />
    </pattern>
</defs>
<text class="text6" font-size="20" x="510" y="329" text-anchor="middle">EDUCATION</text>
</a>

<a href="#energie" class="lbh" title="Energie"><?php echo Yii::t("home","") ?>
<polygon id="dessus" points="312.4,538.6 312.4,431.9 220,378.6 127.7,431.9 127.7,538.6 220,591.9 "/>
<polygon id="8" class="hex" points="312.4,538.6 312.4,431.9 220,378.6 127.7,431.9 127.7,538.6 220,591.9 " fill="url(#img8)"/>

<defs>
  <pattern id="img8" patternUnits="userSpaceOnUse" width="312" height="372">
    <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/energie.png" width="456" height="227" />
    </pattern>
</defs>
<text class="text8" font-size="20" x="220" y="490" text-anchor="middle">TRANSPORT</text>
<text class="text8" font-size="20" x="220" y="510" text-anchor="middle">ENERGIE</text>
</a>

<a href="#dechets" class="lbh" title="Déchets"><?php echo Yii::t("home","") ?>
<polygon points="503.7,538.6 503.7,431.9 411.3,378.6 319,431.9 319,538.6 411.3,591.9 "/>
<polygon id="4" class="hex" points="503.7,538.6 503.7,431.9 411.3,378.6 319,431.9 319,538.6 411.3,591.9 " fill="url(#img4)"/>

<defs>
  <pattern id="img4" patternUnits="userSpaceOnUse" width="277" height="299">
    <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/dechets.png" width="269" height="293" />
    </image>
</pattern>
</defs>
<text class="text4" font-size="20" x="413" y="496" text-anchor="middle">DECHETS</text>
</a>

<a href="#commun" class="lbh" title="Les communs"><?php echo Yii::t("home","") ?>
<polygon id="dessus" points="695,538.6 695,431.9 602.6,378.6 510.3,431.9 510.3,538.6 602.6,591.9 "/>
<polygon id="7" class="hex" points="695,538.6 695,431.9 602.6,378.6 510.3,431.9 510.3,538.6 602.6,591.9 " fill="url(#img7)"/>

<defs>
  <pattern id="img7" patternUnits="userSpaceOnUse" width="233" height="376">
    <image xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/commun.png" x="-25" width="319" height="214" />
    </image>
</pattern>
</defs>
<text class="text7" font-size="20" x="606" y="493" text-anchor="middle">LES COMMUNS</text>
</a>

<a href="#commun" title="Les communs"><?php echo Yii::t("home","") ?>
<img id="im7" style="filter: invert(100%);position: absolute;width: 4%;margin-left: -28%;margin-top: 49%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/commun.svg">
</a>

<a href="#dechets" title="Déchets"><?php echo Yii::t("home","") ?>
<img id="im4" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 46.5%;margin-top: -22.7%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/Dechets.svg">
</a>

<a href="#energie" title="Energie"><?php echo Yii::t("home","") ?>
<img id="im8" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 23.4%;margin-top: -23%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/energie.svg">
</a>

<a href="#education" title="Education"><?php echo Yii::t("home","") ?>
<img id="im6" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 57%;margin-top: -41%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/education.svg">
</a>

<a href="#construction" title="Construction"><?php echo Yii::t("home","") ?>
<img id="im5" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 35%;margin-top: -41.2%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/Construction.svg">
</a>

<a href="#sante" title="Alimentation et Santé"><?php echo Yii::t("home","") ?>
<img id="im3" style="filter:invert(100%);position: absolute;width: 4%;margin-left: 68.2%;margin-top: -60%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/alimentation.svg">
</a>

<a href="#citoyennete" class="lbh" title="Citoyenneté"><?php echo Yii::t("home","") ?>
<img id="im2" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 46.5%;margin-top: -62%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/citoy.svg">
</a>

<a href="#economie" class="lbh" title="Economie"><?php echo Yii::t("home","") ?>
<img id="im1" style="filter: invert(100%);position: absolute;width: 4%;margin-left: 23.5%;margin-top: -61%;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/icones/economie.svg">
</a>
</svg>

<script type="text/javascript">
    $("polygon").each(function(){

        $(".text"+$(this).attr('id')).css("filter", "invert(100%)");
        // $(".text"+$(this).attr('id')).css("visibility", "hidden");
    });
</script>

</div>

<!-- SEARCHBARRE -->
<div class="hidden-xs searchbar col-md-12">
    <!-- DEBUT SVG -->
    <div class=" text-center">
        <!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
        <svg class="search" version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 80 1006 95" style="enable-background:new 0 0 1006 217; margin-top: -2%; width: 50%;" xml:space="preserve">
            <style type="text/css">
                .st0{fill:#FFFFFF;}
                .st1{fill:<?= $this->costum["css"]["menuTop"]["connectBtn"]["background"] ?>;}
            </style>
            <g>
            <!-- <image style="overflow:visible;opacity:0.29;" width="956" height="124" xlink:href="BD554E7F0366A2C6.png"  transform="matrix(1 0 0 1 20.4646 63.5639)">
            </image> -->
            <g style="filter: drop-shadow(0 0px 10px rgba(0, 0, 0, 0.5));">
                <polygon class="st0" points="943.5,117.4 922.2,80.5 908.1,80.5 879.6,80.4 77,80.4 77,80.5 58.9,80.5 37.5,117.4 58.9,154.4 
                77,154.4 77,154.5 737.5,154.5 879.6,154.4 922.2,154.4"/></g>
        </g>
        <g>
            <a data-type="filters" href="javascript:;" id="second-search-bar-addon-smarterre">
                <polygon class="st1" points="921.7,80.2 878.5,80.2 857.8,116.7 879.4,154 922.6,154 944.2,116.7  "/>
                <path class="st0" d="M919,116.9l-2.3-2.3l0,0l-6.3-6.3l-2.3,2.3l4.5,4.4h-28v3.2h28.5l-5,5l2.3,2.3l6.3-6.3l0,0L919,116.9
                L919,116.9L919,116.9z M914.2,117.2v-0.5l0.3,0.3L914.2,117.2z"/>
            </a>
        </g>
        <g>
            <text x="125" y="130"></text>
        </g>
        <path class="st1" d="M110.7,138.8c-0.1-0.1-0.1-0.2-0.2-0.2c-3.3-3.3-6.6-6.6-9.9-9.9c-0.1-0.1-0.1-0.1-0.2-0.2
        c-4,2.8-8.3,3.7-13.1,2.5c-3.8-0.9-6.8-3.1-9.1-6.3c-4.4-6.3-3.4-15.1,2.4-20.4c5.7-5.2,14.6-5.4,20.5-0.5
        c6.1,5.1,7.7,14.3,2.7,21.3c0.1,0.1,0.1,0.1,0.2,0.2c3.3,3.3,6.5,6.5,9.8,9.8c0.1,0.1,0.2,0.1,0.3,0.2c0,0,0,0.1,0,0.1
        C113,136.5,111.9,137.7,110.7,138.8C110.7,138.8,110.7,138.8,110.7,138.8z M80.4,115.9c0,6,4.9,10.9,10.8,10.9
        c6,0,10.8-4.9,10.8-10.8c0-6-4.8-10.9-10.8-10.9C85.3,105,80.4,109.9,80.4,115.9z"/>
    </svg>
</div>

<div class="hidden-xs" style="margin-top: -4.5%; position: absolute; width: 39%; border: none; margin-left: 31%;">
    <input type="text" class="main-search-bar barS" id="second-search-bar" placeholder="Rechercher un territoire">

    <div id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding" style="top: 16px;max-height: 100%;background-color:white;width: 110%;margin-left: 0%;z-index: 1000;">
    </div>
</div>
<!-- FIN SVG -->
</div>


<!-- Charge TPLS -->
<?php 

if(@$this->costum["tpls"]){
    foreach (@$this->costum["tpls"] as $key => $value) : $params = array("canEdit"   =>    $canEdit, "poiList"   =>    $poiList);

        if($key === "communityCaroussel") $params["roles"] = @$value["roles"];

        $color = @$value["color"] ? $value["color"] : "white";

        if($key != "communityCaroussel"):  ?>
            <div style="margin-top: 3vw;border-radius: 10px 10px 10px 10px;background-color:<?= @$value['background']; ?>;" class="explication-title col-xs-10 col-sm-12 col-lg-12">
                <div>
                    <h1 style="color:<?= @$color ?>;font-family: 'Covered By Your Grace', cursive !important;">  
                        <?php if(@$value["icon"] && !empty($value["icon"])) : ?>
                            <i class="fa <?= @$value["icon"] ?>" aria-hidden="true"></i>
                        <?php endif ?>
                        <?= @$value["title"]; ?>
                    </h1>
                </div>
            </div>

            <div class="col-xs-12 col-md-12">
                <!-- <div class="container" style="margin-top: 2%; margin-bottom : 2%;"> -->
                    <?php
                    $path = preg_split('/(?=[A-Z])/',$key);
                    if(@$path[1]){
                        $y = $path[0].$path[1];
                        echo $this->renderPartial("costum.views.tpls.$path[0].$y", $params);
                    }
                    else{
                        echo $this->renderPartial("costum.views.tpls.$path[0]", $params);
                    }
                    ?>
                    <!-- </div> -->
                </div>
                <?php else: ?>
                    <div class="col-xs-12 col-md-12">
                        <?php
                        $path = preg_split('/(?=[A-Z])/',$key);
                        if(@$path[1]){
                            $y = $path[0].$path[1];
                            echo $this->renderPartial("costum.views.tpls.$path[0].$y", $params);
                        }
                        else{
                            echo $this->renderPartial("costum.views.tpls.$path[0]", $params);
                        }
                        ?>
                    </div>
                    <?php
                endif; 
                ?>
                <?php 
            endforeach;
        }
        if($canEdit){ ?> 
            <!-- ESPACE ADMIN --> 
            <center>
                <hr>
                <div class="container">
                    <a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent" data-id="<?= $this->costum["contextId"]; ?>" data-collection="<?= $this->costum["contextType"]; ?>"><i class="fa fa-plus"></i> Ajouter une section</a>
                </div>
            </center>
        <?php } ?> 

        <div style="margin-top: 2vw;">
            <img style="width: 100%;position: absolute;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/smarterritoireautre/phototerritoire.jpg">
            <?php echo $this->renderPartial("costum.assets.images.smarterritoireautre.bloc_smarterre"); ?> 
        </div> 





        <script>
            jQuery(document).ready(function() {
                $.each($(".markdown"), function(k,v){
                    descHtml = dataHelper.markdownToHtml($(v).html()); 
                    $(v).html(descHtml);
                });

                logo = costum.logo;

                $("#hexa-haut").attr("src",logo);
                setTitle("<?= @$el["name"] ?>");

                contextData = {
                    id : "<?php echo $this->costum["contextId"] ?>",
                    type : "<?php echo $this->costum["contextType"] ?>",
                    name : '<?php echo htmlentities($el['name']) ?>',
                    profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
                };
                setTitle(costum.title);

            });

        </script>