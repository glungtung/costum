<?php
$default = "white";
$structField = "structags";
$bk = (@$this->costum["tpls"]["blockevent"]["background"] && !empty($this->costum["tpls"]["blockevent"]["background"])) ? $this->costum["tpls"]["blockevent"]["background"] : $this->costum["css"]["menuTop"]["connectBtn"]["background"];
?>
<style>
.btn-form {
    background : <?= $bk; ?>;
}
.btn-form-ct{
    margin-top : 5%;
    color : white;
}
.text-m1{
    font-size: 0.55rem;
}
</style>
<div class="btn-form row">
    <?php
    // On check si sa existe 
    if(count ( Poi::getPoiByStruct($poiList,"btnform0",$structField ) ) != 0){
        $result = Poi::getPoiByStruct($poiList,"btnform0",$structField)[0];
        // var_dump($result["_id"]); exit;
    // $img = (Document::getWhere(array("id" =>(String) $result["_id"])) ? Document::getWhere(array("id" =>(String) $result["_id"])) : null);
    $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $result["_id"]));
    ?>
    <div class="btn-form-ct col-md-6 text-center">
        <!-- Btn je rejoins -->
        <!-- Début news svg--> 
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.09 11.71" style="width: 60%"><defs>
	<style>.cls-1{fill:#fff;}.cls-2{font-size:5.11px;font-family:SharpSansNo1-Medium, Sharp Sans No1;font-weight:500;letter-spacing:-0.01em;}.cls-3{letter-spacing:-0.03em;}.cls-4{letter-spacing:-0.01em;}.cls-5{letter-spacing:0em;}.cls-6{letter-spacing:-0.04em;}.cls-7{letter-spacing:-0.01em;}.cls-8{letter-spacing:-0.02em;}</style>
</defs>
<title>
	je souhaite</title>
<polygon class="cls-1" points="88.09 5.86 84.72 11.69 3.37 11.69 0 5.86 3.37 0.02 84.72 0.02 88.09 5.86"/>
<text x="23.5" y="8" class="text-m1"><a href="javascript:;" onclick="dyFObj.openForm('organization'); " style="text-decoration : none;"> Je rejoins la filière</a></text>
</svg>

        <!-- Fin news svg --> 
<br>
        <!-- Btn je souhaite -->
                <!-- Début news svg--> 
                <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.09 11.71" style="width: 80%; margin-top: 3%;"><defs>
	<style>.cls-1{fill:#fff;}.cls-2{font-size:5.11px;font-family:SharpSansNo1-Medium, Sharp Sans No1;font-weight:500;letter-spacing:-0.01em;}.cls-3{letter-spacing:-0.03em;}.cls-4{letter-spacing:-0.01em;}.cls-5{letter-spacing:0em;}.cls-6{letter-spacing:-0.04em;}.cls-7{letter-spacing:-0.01em;}.cls-8{letter-spacing:-0.02em;}</style>
</defs>
<title>
	je souhaite</title>
<polygon class="cls-1" points="88.09 5.86 84.72 11.69 3.37 11.69 0 5.86 3.37 0.02 84.72 0.02 88.09 5.86"/>
<text x="10" y="8" class="text-m1"><a href="javascript:;" onclick="dyFObj.openForm('project'); " style="text-decoration : none;"> Je souhaite ajouter un projet</a></text>
</svg>

        <!-- Fin news svg --> 
    <br>
    <h1><span class="markdown"><?= @$result["description"]; ?></span></h1>
    </div>
    <div class="col-md-6">
    <?php if(isset($img)){
        ?>
        <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
    <?php } else {
        ?>
         <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostum/no-banner.jpg" class="img-responsive">
    <?php } ?>
    </div>

    </div>
<?php 
        $edit = "update";
    } else { ?>
        <div class="btn-form-ct col-md-6 text-center">
                <!-- Btn je rejoins -->
        <!-- début svg --> 
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 225.3 39.91" style="width:50%;">
        <defs><style>.cls-1{fill:#fff;}</style></defs>
        <title>je rejoins</title>
            <path class="cls-1" d="M192.66,268.72a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm190-19.2H181.14l-11.9,20,11.9,19.95H382.65l11.89-19.95ZM199.53,267.66a4.19,4.19,0,0,1-.16,1.07l8.46,4.07a4.36,4.36,0,0,1,3.38-1.59,4.23,4.23,0,1,1-4.14,3.07c-2.83-1.37-5.65-2.71-8.47-4.08a4.29,4.29,0,0,1-2.67,1.53c-.14,1.86-.27,3.71-.41,5.57a4,4,0,0,1,.74.3,2.9,2.9,0,0,1,1.09,4.06,3.16,3.16,0,0,1-4.23,1,2.91,2.91,0,0,1-1.09-4.06,3.12,3.12,0,0,1,2.26-1.42c.13-1.82.28-3.64.4-5.46a4.28,4.28,0,0,1-2.44-1.12l-4.55,2.82a2.9,2.9,0,0,1-.21,2.59,3.15,3.15,0,0,1-4.23,1,2.91,2.91,0,0,1-1.07-4.07,3.15,3.15,0,0,1,4.22-1,2.91,2.91,0,0,1,.62.49l4.46-2.77a4,4,0,0,1-.56-2,3.6,3.6,0,0,1,.26-1.4l-2.81-2a2.65,2.65,0,0,1-.55.37,2.59,2.59,0,0,1-3.41-1,2.41,2.41,0,0,1,1.1-3.29,2.6,2.6,0,0,1,3.42,1.05,2.41,2.41,0,0,1,0,2.14l2.66,1.88a4.28,4.28,0,0,1,3-1.81c0-.94,0-1.9,0-2.85a2.51,2.51,0,0,1-1.77-1.26,2.39,2.39,0,0,1,1.09-3.29,2.59,2.59,0,0,1,3.41,1.05,2.4,2.4,0,0,1-1.1,3.29,2,2,0,0,1-.62.2l0,2.83a4.38,4.38,0,0,1,3,1.58l8.47-4.08a4.12,4.12,0,0,1-.16-1.06,4.3,4.3,0,1,1,4.3,4.13,4.33,4.33,0,0,1-3.38-1.59l-8.46,4.07A4.19,4.19,0,0,1,199.53,267.66ZM192.7,269l0,0a2,2,0,0,1-.08-.22A.77.77,0,0,0,192.7,269Zm0-.25a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Z" transform="translate(-169.24 -249.52)"/>
            <g>
                <text x="59" y="26" class="text-m1"><a href="javascript:;" onclick="dyFObj.openForm('organization'); " style="text-decoration : none;"> Je rejoins la filière</a></text>
            </g>
        </svg>
        <!-- fin svg --> 
    <br>
        <!-- Btn je propose -->
        <!-- Debut svg --> 
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 220.44 29.05" style="width:75%;">
        <defs><style>.cls-1{fill:#fff;}</style></defs>
        <title>je souhaite</title>
        <path class="cls-1" d="M168.56,424.7a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm194.73-14H160.17l-8.66,14.53,8.66,14.52H363.29L372,425.26Zm-189.73,13.2a2.89,2.89,0,0,1-.12.78c2.05,1,4.11,2,6.16,3a3.19,3.19,0,0,1,2.46-1.16,3,3,0,1,1-3.13,3,2.88,2.88,0,0,1,.12-.77l-6.17-3a3.13,3.13,0,0,1-1.94,1.12c-.11,1.35-.2,2.7-.3,4.05a2.52,2.52,0,0,1,.54.21,2.12,2.12,0,0,1,.79,3,2.3,2.3,0,0,1-3.08.76,2.12,2.12,0,0,1-.79-3,2.29,2.29,0,0,1,1.64-1c.1-1.33.21-2.65.3-4a3.13,3.13,0,0,1-1.78-.82L165,428.16a2.1,2.1,0,0,1-.16,1.88,2.3,2.3,0,0,1-3.07.76,2.13,2.13,0,0,1-.79-3,2.29,2.29,0,0,1,3.07-.76,2,2,0,0,1,.46.36l3.25-2a2.8,2.8,0,0,1-.41-1.49,2.88,2.88,0,0,1,.18-1l-2-1.44a1.65,1.65,0,0,1-.4.27,1.88,1.88,0,0,1-2.48-.77,1.75,1.75,0,0,1,.8-2.39,1.88,1.88,0,0,1,2.49.77,1.76,1.76,0,0,1,0,1.55l1.94,1.37A3.1,3.1,0,0,1,170,421c0-.68,0-1.38,0-2.07a1.84,1.84,0,0,1-1.29-.92,1.74,1.74,0,0,1,.8-2.39,1.88,1.88,0,0,1,2.48.76,1.75,1.75,0,0,1-.8,2.4,1.39,1.39,0,0,1-.45.14c0,.69,0,1.38,0,2.06a3.17,3.17,0,0,1,2.16,1.15l6.17-3a2.76,2.76,0,0,1-.12-.77,3.13,3.13,0,1,1,3.13,3,3.19,3.19,0,0,1-2.46-1.16l-6.16,3A2.89,2.89,0,0,1,173.56,423.93Zm-5,.95,0,0a.75.75,0,0,1-.05-.16A.57.57,0,0,0,168.59,424.88Zm0-.18a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Z" transform="translate(-151.51 -410.73)"/>
        <g>
            <text x="43" y="19" class="text-m2"><a onclick="dyFObj.openForm('project');" style="text-decoration : none" href="javascript:;">Je souhaite ajouter un projet</a></text>
        </g>
    </svg>
        <!-- fin svg -->
        <br>
        <span class="markdown">En attente d'une description</span>
        </div>
        <div class="col-md-6">
            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostum/no-banner.jpg" class="img-responsive">
        </div>
            <?php 
            echo "</div>";
            $edit = "create"; 

        } 
        echo $this->renderPartial("costum.views.tpls.openFormBtn",
            array(
                'edit' => $edit,
                'tag' => 'btnform0',
                'id' => (string)@$result["_id"]),true); ?>
       <?php  
        // }  
    
        echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
        ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		console.log("---------- Render blockimg","costum.views.custom.smarterritoire.tpls.btn");
		$.each($(".markdown"), function(k,v){
			descHtml = dataHelper.markdownToHtml($(v).html());
			$(v).html(descHtml);
		});
});

	$(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'cms'},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });

    });

    
</script>