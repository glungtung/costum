<style>
	.mb-4, .my-4 {
	    margin-bottom: 1.5rem!important;
	}
	.card-img-top {
	    width: 100%;
	    border-top-left-radius: calc(.25rem - 1px);
	    border-top-right-radius: calc(.25rem - 1px);
	    height: 250px;
    	object-fit: cover;
	}
	.card-body {
	    -ms-flex: 1 1 auto;
	    flex: 1 1 auto;
	    padding: 1.25rem;
	}
	.card-title {
	    margin-bottom: .75rem;
	   	font-size: 20px;
	   	text-transform: none;
	}
	.card-text {
		font-size: 16px;
	}
	.btn-outline-dark {
	    color: #343a40;
	    background-color: transparent;
	    background-image: none;
	    border-color: #343a40;
	}
	on avais un imprevue la 
	nous avons encore une dette pour valider notre formation en master 2
	et l'emit vien de sortir n calendrier des examen qui auras lieux la semaine prochaine
</style>
<?php //var_dump($allCostum); ?>
<div class="row">
<?php foreach($allCostum as $key => $val){ ?>
	<?php 
	$images = (@$val["profilMediumImageUrl"]) ? $val["profilMediumImageUrl"] :  Yii::app()->getModule("costum")->assetsUrl."/images/costumDesCostums/costum.png";
	 ?>
      <div class="col-md-4">
         <div class="well mb-4 no-padding">
            <img class="card-img-top" src="<?php echo $images ?>" alt="Card image cap">
            <div class="card-body">
               <h5 class="card-title"><?php echo $val["name"] ?></h5>
               <p class="card-text"><?php echo (@$val["shortDescription"]) ? $val["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
               <a href="<?php Yii::app()->baseUrl; ?>/costum/co/index/slug/<?php echo $val["slug"] ?>" class="btn btn-outline-dark btn-sm" target="_blank">Visiter le site</a>
            </div>
         </div>
      </div>
<?php } ?>
</div>