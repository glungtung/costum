<style type="text/css">

    @import url("https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap");
    .container{margin-top:20px}
    .fa-spin-fast{-webkit-animation:fa-spin-fast .1s infinite linear;animation:fa-spin-fast 0.1s infinite linear}
    @-webkit-keyframes fa-spin-fast{0%{-webkit-transform:rotate(0);transform:rotate(0)}
        100%{-webkit-transform:rotate(180deg);transform:rotate(180deg)}}
    @keyframes fa-spin-fast{0%{-webkit-transform:rotate(0);transform:rotate(0)}
        100%{-webkit-transform:rotate(180deg);transform:rotate(180deg)}}
    .material-card{position:relative;height:0;padding-bottom:calc(250px - 16px);margin-bottom:6.6em}
    .material-card h2{position:absolute;top:calc(240px - 16px);left:0;width:100%;padding:10px 16px 10px 20px;height:82px;color:#fff;font-size:1.4em;line-height:1.6em;margin:0;z-index:2;-webkit-transition:all .3s;-moz-transition:all .3s;-ms-transition:all .3s;-o-transition:all .3s;transition:all .3s;box-shadow: 0 2px 20px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);text-transform: none;}
    .material-card h2 span{word-wrap: break-word;  display: -webkit-box;  -webkit-line-clamp: 3;  -webkit-box-orient: vertical;overflow:hidden;}
    .material-card h2 strong{font-weight:300;display:block;font-size:.76em}
    .material-card h2:after,.material-card h2:before{content:' ';position:absolute;left:0;top:-16px;width:0;border:8px solid;-webkit-transition:all .3s;-moz-transition:all .3s;-ms-transition:all .3s;-o-transition:all .3s;transition:all .3s}
    .material-card h2:after{top:auto;bottom:0}
    @media screen and (max-width:767px){.material-card.mc-active{padding-bottom:0;height:auto}
    }

    .material-card.mc-active h2{top:0;padding:10px 16px 10px 90px}
    .material-card.mc-active h2:before{top:0}
    .material-card.mc-active h2:after{bottom:-16px}
    .material-card .mc-content{position:absolute;right:0;top:0;height: 250px;bottom:16px;left:16px;-webkit-transition:all .3s;-moz-transition:all .3s;-ms-transition:all .3s;-o-transition:all .3s;transition:all .3s;box-shadow:0 2px 20px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);}
    .material-card .mc-btn-action{position:absolute;right:16px;top:15px;-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;border:5px solid;width:54px;height:54px;line-height:46px;text-align:center;color:#fff!important;cursor:pointer;z-index:3;-webkit-transition:all .3s;-moz-transition:all .3s;-ms-transition:all .3s;-o-transition:all .3s;transition:all .3s}
    .material-card .mc-btn-action:hover{box-shadow:0 0px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);transition:all 0.3s ease;-webkit-transition:all 0.3s ease;-moz-transition:all 0.3s ease;-ms-transition:all 0.3s ease;-o-transition:all 0.3s ease;}
    .material-card.mc-active .mc-btn-action{top:62px;box-shadow:0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);}
    .material-card .mc-description{position:absolute;top:100%;right:30px;left:30px;bottom:40px;overflow:hidden;opacity:0;-webkit-transition:all 0.4s;-moz-transition:all 0.4s;-ms-transition:all 0.4s;-o-transition:all 0.4s;transition:all 0.4s;word-wrap: break-word;  display: -webkit-box;  -webkit-line-clamp: 4;  -webkit-box-orient: vertical;}
    .material-card .mc-footer{height:0;overflow:hidden;-webkit-transition:all .3s;-moz-transition:all .3s;-ms-transition:all .3s;-o-transition:all .3s;transition:all .3s}
    .material-card .mc-footer h4{position:absolute;top:200px;left:30px;padding:0;margin:0;font-size:16px;font-weight:700;-webkit-transition:all .4s;-moz-transition:all 0.4s;-ms-transition:all 0.4s;-o-transition:all 0.4s;transition:all 0.4s}
    .material-card .mc-footer a{display:block;float:left;position:relative;height:40px;margin-left:10px;font-size:16px;color:#fff;line-height:25px;text-decoration:none;top:200px;border-radius:50px;}
    .material-card .mc-footer a:nth-child(1){-webkit-transition:all .1s;-moz-transition:all .1s;-ms-transition:all .1s;-o-transition:all .1s;transition:all .1s}
    .material-card .img-container{overflow:hidden;position:absolute;left:0;top:0;width:100%;height:250px;z-index:1;-webkit-transition:all .3s;-moz-transition:all .3s;-ms-transition:all .3s;-o-transition:all .3s;transition:all .3s;}
    .material-card.mc-active .img-container{-webkit-border-radius:50%;-moz-border-radius:50%;border-radius:50%;left:0;top:12px;width:55px;height:55px;z-index:3}
    .img-fluid{max-width:100%}
    .material-card .img-container img{width: 100%;  height: 250px;  object-fit: cover;}
    .material-card.mc-active .img-container img{width: 55px;  height: 55px;  object-fit: cover;}
    .material-card.mc-active .mc-content{padding-top:5.6em}
    @media screen and (max-width:767px){.material-card.mc-active .mc-content{position:relative;margin-right:16px}
    }
    .material-card.mc-active .mc-description{top:50px;padding-top:5.6em;opacity:1;text-align:left;}
    @media screen and (max-width:767px){.material-card.mc-active .mc-description{position:relative;top:auto;right:auto;left:auto;padding:50px 30px 70px 30px;bottom:0}
    }
    .material-card.mc-active .mc-footer{overflow:visible;position:absolute;top:calc(250px - 16px);left:16px;right:0;height:72px;display:flex;align-items:center;justify-content:center;    box-shadow:0 2px 20px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);}
    .material-card.mc-active .mc-footer a{top:0;border:2px solid #e8eaf6;}
    .material-card.mc-active .mc-footer h4{top:27px;left:20px;font-weight:400;}

    .material-card.Light-Green h2{background-color:#8bc34a}
    .material-card.Light-Green h2:after{border-top-color:#8bc34a;border-right-color:#8bc34a;border-bottom-color:transparent;border-left-color:transparent}
    .material-card.Light-Green h2:before{border-top-color:transparent;border-right-color:#33691e;border-bottom-color:#33691e;border-left-color:transparent}
    .material-card.Light-Green.mc-active h2:before{border-top-color:transparent;border-right-color: #8bc34a;border-bottom-color:#8bc34a;border-left-color:transparent}
    .material-card.Light-Green.mc-active h2:after{border-top-color:#33691e;border-right-color:#33691e;border-bottom-color:transparent;border-left-color:transparent}
    .material-card.Light-Green .mc-btn-action{background-color:#8bc34a}
    .material-card.Light-Green .mc-btn-action:hover{background-color:#33691e}
    .material-card.Light-Green .mc-footer h4{color:#33691e}
    .material-card.Light-Green .mc-footer a{background-color:#33691e}
    .material-card.Light-Green.mc-active .mc-content{background-color:#f1f8e9}
    .material-card.Light-Green.mc-active .mc-footer{background-color:#dcedc8}
    .material-card.Light-Green.mc-active .mc-btn-action{border-color:#f1f8e9}

    .material-card .div-img{background-color: #eee;text-align: center;height: 100%;display: flex;align-items: center;justify-content: center;}
    .material-card .div-img .fa-2x{font-size: 12em;}
    .material-card.mc-active .div-img .fa-2x{font-size: 2em;}

    #mapContent {
        z-index: 4!important;
    }
</style>

<script type="text/javascript">
	var paramsFilter= {
        results : {
            smartGrid : true,
            renderView : "directory.materialDesign"
        },
        interface : {
            events : {
                scroll : true,
                scrollOne : true
            }
        },
	 	container : "#filters-nav",
        defaults : {
            types : ["NGO","LocalBusiness","Group","GovernmentOrganization","Cooperative","projects"],
            filters : {
                $or : {
                    $and : [
                        {"costum.slug" :{'$not':{'$regex':"cocity"}}},
                        {"costum.slug" :{'$exists':true}}
                    ]
                },

            },
            notSourceKey : true

        },
	 	filters : {
            text : true
         }

        
     };



	jQuery(document).ready(function() {
        filterSearch = searchObj.init(paramsFilter);
        directory.materialDesign = function(params){
                mylog.log("materialDesign","Params",params);


                var dateStr="";
                if(typeof params.updated != "undefined" && notNull(params.updated))
                    dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
                else if(typeof params.created != "undefined" && notNull(params.created))
                    dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
                var str='';

                str +=	'<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-6 col-sm-6 col-12 mt-5 searchEntityContainer '+params.containerClass+'">'+
                            '<article class="material-card Light-Green">'+
                            '<h2>'+
                                '<span>'+params.name+'</span>'+
                            '</h2>'+
                            '<div class="mc-content">'+
                                '<div class="img-container">'+
                                    params.imageProfilHtml +
                                    '</div>'+
                                    '<div class="mc-description">';
                                    if (notNull(params.localityHtml)) {
                str +=				    '<strong>'+
                                             params.localityHtml+
                                        '</strong><br>';
                                    }
                str +=				params.descriptionStr+
                                 '</div>'+
                            '</div>'+
                            '<a class="mc-btn-action ripple">'+
                                '<i class="fa fa-bars"></i>'+
                                '</a>'+
                                '<div class="mc-footer">'+
                                '<a target="_blank" class="btn" href="'+baseUrl+'/costum/co/index/slug/'+params.slug+'"> Visiter le site <i class="fa fa-fw fa-arrow-right ripple"></i> </a>'+
                            '</div>'+
                            '</article>'+
                        '</div>';

                return str;
            };
        $("#search-content").on("click", ".material-card > .mc-btn-action", function(e){
            e.stopPropagation();
            var card = $(this).parent('.material-card');
            var icon = $(this).children('i');
            icon.addClass('fa-spin-fast');

            if (card.hasClass('mc-active')) {
                card.removeClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-arrow-left ripple')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-bars');

                }, 800);
            } else {
                card.addClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-bars')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-arrow-left ripple');

                }, 800);
            }
        })



    });





</script>