<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

    $cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
    );
    
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
    
    $poiList = array();
    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    
        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "source.key" => $this->costum["slug"],
                               "type"=>"cms") );
                               
    } 

    //  var_dump($poiList);
    // var_dump($this->costum["contextId"]);
    ?>
    <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace&display=swap" rel="stylesheet"> 
    <link href="//db.onlinewebfonts.com/c/cb18f0f01e1060a50f2a273343bb844e?family=Homestead" rel="stylesheet" type="text/css"/>
<style>
@font-face {
    font-family: "Homestead"; src: url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.eot"); src: url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.woff") format("woff"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/cb18f0f01e1060a50f2a273343bb844e.svg#Homestead") format("svg"); 
    font-family : "Helvetica"; src: url("<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/font/costumDesCostums/Helvetica.ttf") format("ttf");
} 
  .header{
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/photo.jpg);
    background-size: cover;
    padding-bottom: 49%;
}
.intro{
    background-color: #ededed;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: -3%;
    padding-bottom: 3%;
}
.intro-description{
    background: white;
    text-align: center;
    padding-top: 3%;
    padding-bottom: 3%;
    color : <?php echo $this->costum["css"]["color"]["blue"]; ?>;
    border: solid 1px silver;
}
.intro-description > h1,h2,h3,h4,h5,h6{
    font-family: Homestead !important;
}
.projet-title{
    text-align : center;
    border-top: dashed 1.5px <?php echo $this->costum["css"]["color"]["blue"]; ?>;
    border-bottom:dashed 1.5px <?php echo $this->costum["css"]["color"]["blue"]; ?>;
    color: <?php echo $this->costum["css"]["color"]["blue"]; ?>;
}
.projet{
    margin-top:3%;
    margin-left: 10%;  
    margin-right: 10%;
}
.projet-affiche{
    margin-top: 3%;
    background-color:#ededed;
    box-shadow: 0px 0px 20px -2px #878786;
    padding-bottom: 3%;
}
.explication{
    margin-top:3%;
    margin-left: 10%;  
    margin-right: 10%;
}
.explication-title{
    text-align : center;
    border-top: dashed 1.5px <?php echo $this->costum["css"]["color"]["bg-green"]; ?>;
    border-bottom:dashed 1.5px <?php echo $this->costum["css"]["color"]["bg-green"]; ?>;
    color : <?php echo $this->costum["css"]["color"]["bg-green"]; ?>
}

.explication-img {
    text-align: center;
}
.tarif{
    margin-top:3%;
    background-color:#f8f9f9;
    margin-bottom : 2%;
}
.tarif-title {
    text-align : center;
    border-top: dashed 1.5px <?php echo $this->costum["css"]["color"]["blue"]; ?>;
    border-bottom:dashed 1.5px <?php echo $this->costum["css"]["color"]["blue"]; ?>;
    margin-top: 5%;
    margin-bottom: 5%;
    color : <?php echo $this->costum["css"]["color"]["blue"]; ?>;
}
.tarif-border{
    background-color: #ededed;
    padding: 2%;
    box-shadow: 0px 0px 20px -2px #878786;
}
.tarif-c{
    border-right : dashed 1.5px black;
    text-align : center;
}
.tarif-cs{
    text-align : center;
}
.bouton{
    margin-top: 2%;
    margin-left: 10%;
    margin-right: 10%;
}
.bouton-img {
    text-align: center;
}
.tarif-c > p {
    margin-top: 5%;
    color : #878786;
    font-size: 2.25rem;
}
.tarif-cs > p {
    margin-top: 5%;
    color : #878786;
    font-size: 2.25rem;
}
.tarif-c > h2{
    color : <?php echo $this->costum["css"]["color"]["blue"]; ?>;
}
.tarif-cs > h2{
    color: <?php echo $this->costum["css"]["color"]["blue"]; ?>;
}
.bouton-img > span {
    background: white;
    border-radius: 20px 20px 20px 20px;
    padding: 2.3%;
    padding-left: 5%;
    padding-right: 5%;
    border: black 1px solid;
}
.tarif-description > p {
    font-style: italic;
    text-align: center;
    margin-left: 10%;
    margin-right: 10%;
    margin-top: 3%;
    margin-bottom: 3%;
    color: #878786;
}
.btn-plus-tarif{
    margin-top: -3%;
    padding: 2%;
}
.img-projet{
    width: 100%;
    height: 50%;
}
.modal-header{
    background-color: #94c138;
    color: white;
}
.center{
    margin-left : 10%;
    margin-right: 10%;
}
.img-expli{
    width: 60%;
}
.m-plus{
    width : 5%;
}
.costum-m{
    height:300px;
    overflow-y:auto; 
    background-color: white;
    border: solid 3px #ededed;
}
.costum-m > a {
    text-decoration : none;
}
@media (max-width:768px){
    .tarif-title > h1{
        font-size : 3rem;
    }
    .tarif-c > p {
        font-size: 2rem;
    }
    .tarif-cs > p {
        font-size: 2rem;
    }
    .tarif-c > h1{
        font-size: 3rem;
    }
    .tarif-cs > h1{
        font-size: 3rem;
    }
    .tarif-c{
        border-bottom : dashed 1.5px black;
        margin-top : 2%;
    }
    .bouton-img > span{
        font-size: 1.5rem;
        padding: 5%;
    }
    .img-projet {
        width: 100%;
        height: 75%;
    }
    .projet-mobile{
        font-size: 1rem;
        line-height: 9px;
    }
    .bouton-img{
        margin-top : 10%;
        font-size: 1rem;
    }
    .tarif-title > h1{
        font-size : 3rem;
    }
    .center{
        margin-left: 0%;
        margin-right:0%;
    }
    .explication{
        margin-left: 0%;
        margin-right:0%;
    }
    .explication-title > h1{
        font-size : 3rem;
    }
    .explication-img > h1 {
        font-size : 2.5rem;
    }
    .img-expli{
        width: 80%;
    }
    .m-plus{
        width : 15%;
    }
    .poi-m{
        margin-left: -4%;
    }
    .costum-m{
        height:200px;
        overflow-y:auto; 
        background-color: white;
        border: solid 3px #ededed;
    }
    .projet{
        margin-left: 0%;
        margin-right: 0%;
    }
    .projet-title > h1{
        font-size : 3rem;
    }
    .intro-description{
        font-size: 3rem;
    }

    .intro-description h1{
        font-size: 22px;
    }
    .intro-description h2{
        font-size: 18px;
    }

    .intro-description h4{
        font-size: 18px;
    }
}

.carousel-costum {
    margin-top: 30px;
    margin-bottom: 30px;
}


.our-costum-section {
  position: relative;
  padding-top: 40px;
  padding-bottom: 40px;
}
.our-costum-section:before {
  position: absolute;
  top: -0;
  left: 0;
  content: " ";
  background: url(img/service-section-bottom.png);
  background-size: 100% 100px;
  width: 100%;
  height: 100px;
  float: left;
  z-index: 99;
}
.our-costum {
  padding: 0 0 40px;
  background: #f9f9f9;
  text-align: center;
  overflow: hidden;
  position: relative;
  border-bottom: 5px solid #00325a;
}
.our-costum:hover {
  border-bottom: 5px solid #2f2f2f;
}

.our-costum .pic {
  display: inline-block;
  width: 130px;
  height: 130px;
  margin-bottom: 50px;
  z-index: 1;
  position: relative;
}
.our-costum .pic:before {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: #00325a;
  position: absolute;
  bottom: 135%;
  right: 0;
  left: 0;
  opacity: 1;
  transform: scale(3);
  transition: all 0.3s linear 0s;
}
.our-costum:hover .pic:before {
  height: 100%;
  background: #2f2f2f;
}
.our-costum .pic:after {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: #ffffff00;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;
  transition: all 0.3s linear 0s;
}
.our-costum:hover .pic:after {
  background: #7ab92d;
}
.our-costum .pic img {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  transform: scale(1);
  transition: all 0.9s ease 0s;
  box-shadow: 0 0 0 14px #f7f5ec;
  transform: scale(0.7);
  position: relative;
  z-index: 2;
}
.our-costum:hover .pic img {
  box-shadow: 0 0 0 14px #f7f5ec;
  transform: scale(0.7);
}
.our-costum .costum-content {
  margin-bottom: 30px;
}
.our-costum .title {
  font-size: 16px;
  font-weight: 700;
  color: #4e5052;
  letter-spacing: 1px;
  text-transform: capitalize;
  margin-bottom: 5px;
}
.our-costum .post {
  display: block;
  font-size: 15px;
  color: #4e5052;
  text-transform: capitalize;
}
.our-costum .social {
  width: 100%;
  padding-top: 10px;
  margin: 0;
  background: #2f2f2f;
  position: absolute;
  bottom: -100px;
  left: 0;
  transition: all 0.5s ease 0s;
  color: #fff;
}
.our-costum:hover .social {
  bottom: 0;
}
.our-costum .social li {
  display: inline-block;
}
.our-costum .social li a {
  display: block;
  padding-top: 6px;
  font-size: 15px;
  color: #fff;
  transition: all 0.3s ease 0s;
}
.our-costum .social li a:hover {
  color: #2f2f2f;
  background: #f7f5ec;
}
@media only screen and (max-width: 990px) {
  .our-costum {
    margin-bottom: 10px;
  }
}
.carousel-control          { width:  4%; }
.carousel-control.left {margin-left:15px;background-image:none;}
.carousel-control.right {margin-right:15px;background-image:none;}
@media (max-width: 767px) {
  .carousel-inner .active.left { left: -100%; }
  .carousel-inner .next        { left:  100%; }
  .carousel-inner .prev    { left: -100%; }
  .active > div { display:none; }
  .active > div:first-child { display:block; }

}
@media (min-width: 768px) and (max-width: 992px ) {
  .carousel-inner .active.left { left: -50%; }
  .carousel-inner .next        { left:  50%; }
  .carousel-inner .prev    { left: -50%; }
  .active > div { display:none; }
  .active > div:first-child { display:block; }
  .active > div:first-child + div { display:block; }
}
@media (min-width: 993px ) {
  .carousel-inner .active.left { left: -16.7%; }
  .carousel-inner .next        { left:  16.7%; }
  .carousel-inner .prev    { left: -16.7%; }  
}


/*experiment section*/
.row-1 .ser-col-4{border-right:solid 1px #ccc;border-bottom: solid 1px #ccc;}
.row-1 .ser-col-4-l{border-bottom: solid 1px #ccc;}



@media (max-width:768px){
    .ser-col-4-l{border-right:solid 1px #ccc;border-bottom: solid 1px #ccc;border-left:solid 1px #ccc;}
    .ser-col-4{border-left:solid 1px #ccc;border-bottom: solid 1px #ccc;}
}

.ser-col{width:100%;height:auto;text-align: center;padding:20px;position: relative;min-height: 330px;}
.icon-col{width:80px;height:80px;border-radius: 50%;text-align: center;margin-left: auto;margin-right: auto;}
.icon-col i{font-size: 35px;padding: 20px;}
.circle {
    position: absolute;
    bottom: -11px;
    right: -24px;
    z-index: 99;
}
.circle i{
    color:#ccc;
    font-size: 18px;
    background-color:ghostwhite;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 10px;
    margin-top: 10px;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    color: red;
    background-image: none;
    border: 1px solid red;
    border-radius: 0px;
}
.ser-col h2{
    color: #777;
    margin-bottom: 20px;
    text-transform: initial;
    font-size: 25px;
    margin-top: 20px;
}

.ser-col p{
    color:#3e3b3b;
    margin-bottom: 20px;
}

/*====== service 1 ====*/
.ser-1 .icon-col{border: solid 1px red;}
.ser-1 .icon-col i{color:red;}
.ser-1 .btn{color:red;border: 1px solid red;}

/*====== service 2 ====*/
.ser-2 .icon-col {border: solid 1px green;}
.ser-2 .icon-col i{color:green;}
.ser-2 .btn{color:green;border: 1px solid green;}

/*====== service 3 ====*/
.ser-3 .icon-col {border: solid 1px chocolate;}
.ser-3 .icon-col i{color:chocolate;}
.ser-3 .btn{color:chocolate;border: 1px solid chocolate;}


/* service block Tarif*/

.serviceBox{text-align: center;margin-top: 60px;position: relative;z-index: 1; margin-bottom: 21px;}
.serviceBox .service-icon{width: 78px;height: 78px;border-radius:3px;background: #fff;margin: 0 auto;position: absolute;top: -34px;left: 0;right: 0;z-index: 1;transition: all 0.3s ease-out 0s;}
.serviceBox:hover .service-icon{transform: rotate(45deg);}
.serviceBox .service-icon span{display: inline-block;width: 60px;height: 60px;line-height: 60px;border-radius:3px;background: #727cb6;font-size: 30px;color: #fff;margin: auto;position: absolute;top: 0;left: 0;bottom: 0;right: 0;transition: all 0.3s ease-out 0s;}
.serviceBox .service-icon span i{transition: all 0.3s ease-out 0s;}
.serviceBox:hover .service-icon span i{transform: rotate(-45deg);}
.serviceBox .service-content{background: #fff;border: 1px solid #e7e7e7;border-radius: 3px;padding: 55px 15px;position: relative;}
.serviceBox .service-content:before{content: "";display: block;width: 80px;height: 80px;border: 1px solid #e7e7e7;border-radius: 3px;margin: 0 auto;position: absolute;top: -37px;left: 0;right: 0;z-index: -1;transition: all 0.3s ease-out 0s;}
.serviceBox:hover .service-content:before{transform: rotate(45deg);}
.serviceBox .title{font-size: 26px;font-weight: 500;color: #324545;text-transform: uppercase;margin: 0 0 25px 0;position: relative;transition: all 0.3s ease-out 0s;}
.serviceBox:hover .title{color: #727cb6;}
.serviceBox .description{font-size: 18px;font-weight: 500;line-height: 24px;margin-bottom: 0;}
.serviceBox .read-more{
    display: block;
    width: 45px;
    height: 45px;
    line-height: 45px;
    border-radius: 50%;
    background: #fff;
    border: 1px solid #e7e7e7;
    font-size: 18px;
    color: #c4c2c2;
    margin: 0 auto;
    position: absolute;
    bottom: -17px;
    left: 0;
    right: 0;
    transition: all 0.3s ease-out 0s;
}
.serviceBox .read-more:hover{border: 1px solid #727cb6;color: #727cb6;text-decoration: none;}
.serviceBox.green .service-icon span{ background: #008b8b; }
.serviceBox.blue .service-icon span{ background: #3498db; }
.serviceBox.orange .service-icon span{ background: #e67e22; }
.serviceBox.green:hover .title{ color: #008b8b; }
.serviceBox.blue:hover .title{ color: #3498db; }
.serviceBox.orange:hover .title{ color: #e67e22; }

.serviceBox:hover .read-more{border: 1px solid #727cb6;color: #727cb6;}
.serviceBox.green:hover .read-more{border: 1px solid #008b8b;color: #008b8b;}
.serviceBox.blue:hover .read-more{border: 1px solid #3498db;color: #3498db;}
.serviceBox.orange:hover .read-more{border: 1px solid #e67e22;color: #e67e22;}
    
</style>
<div class="header">
<center class="col-xs-12 col-sm-12">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/header.png" class="img-responsive" style="margin-top: 3%; width:80%"  usemap="#headerimg">

        <map name="headerimg">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
        <!-- #$-:Please do not edit lines starting with "#$" -->
        <!-- #$VERSION:2.3 -->
        <!-- #$AUTHOR:Tony Emma -->
            <area shape="circle" coords="450,97,92" alt="Costum pour le pacte de la transition" href="https://pacte-transition.org" target="_blank" />
            <area shape="circle" coords="501,346,68" alt="Costum du site Open Altas" target="_blank" href="http://www.open-atlas.org/" />
            <area shape="circle" coords="736,131,75" alt="Site du costum de la Raffinerie" href="https://www.communecter.org/costum/co/index/id/laRaffinerie" target="_blank" />
            <area shape="circle" coords="770,442,90" alt="Costum de la filière Numérique" href="https://www.communecter.org/costum/co/index/id/coeurNumerique" target="_blank" />
        </map>

</center>
</div>

<div class="intro row" >
    <div style="margin-top: 55%; margin-bottom: 0%; margin-left: 3%; margin-right: 3%;">
        <div class="intro-description col-xs-12 col-sm-12" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
            <h1>Créer votre propre réseau sociale personnalisée !</h1>
            <h2>Votre design, vos données et innover </h2>
            <h4> Tout en contribuant aux communs</h4>
        </div>
    </div>
</div>

<div class="projet row" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
    <div class="projet-title col-xs-12 col-sm-12">
    <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Nos exemples de costum</h1>
    </div>
<!--
    <div class="projet-affiche col-xs-12 col-sm-12">
        <div class="col-lg-12 text-center containe-costum-list " id='content-results-persons' style="margin-top:2%;">--> 
				<!-- içi que s'affiche les persons --> 
<!--			</div>
    </div>

 -->   
</div>



<!--.Carousel-->

<?php 
    $where = array(
            "costum.slug" => array('$exists' => true)
        );
        
    $costumOrga = PHDB::find(Organization::COLLECTION,$where);
        //var_dump($costumOrga);//exit;

    $costumProject = PHDB::find(Project::COLLECTION,$where);
    //var_dump($costumProject);exit;

    $costumFus = array_merge($costumOrga,$costumProject);
    //var_dump($costumFus);exit;


 ?>


<div class="col-xs-12 carousel-costum" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
    <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
      <div class="carousel-inner">

              <?php 
              $i = 0;
              foreach ($costumFus  as $key => $val) { ?>

              <div class="item <?php echo $i==0 ? "active" : ""; ?>">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="our-costum">
                        <div class="pic">
                            <?php if (isset($val["profilImageUrl"]) && !empty($val["profilImageUrl"]) ) {?>
                               <img src="<?php echo @$val["profilImageUrl"]; ?>"> 
                            <?php } else { ?>
                                <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/costum.png"> 
                            <?php } ?>
                            
                            
                        </div>
                        <div class="costum-content">
                          <h3 class="title"><?php echo $val["name"]; ?></h3>
                          <span class="post"><b>Slug : </b><?php echo $val["slug"]; ?></span>
                        </div>
                        <a href="<?php echo Yii::app()->getRequest()->getBaseUrl(true);?>/costum/co/index/slug/<?php echo $val["slug"]; ?>" target="_blank">
                            <ul class="social">
                              <li>
                                <span class="fa fa-eye"></span>&nbsp;Visiter
                              </li>
                            </ul>
                        </a>
                      </div>
                    </div>
                </div>
            <?php $i++; } ?>
                  
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
            <!--.item-->

        </div>
        <!--.carousel-inner-->
    </div>
    <!--.Carousel-->

    <div class="col-xs-12 col-sm-12" style="text-align:center; margin-bottom: 2%;">
        <center>
            <a href="javascript:;" data-hash="#searchcostum" class="lbh-menu-app">
                <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/plus.svg" class="img-responsive m-plus">
            </a>
        </center>
    </div>

<div class="useCase row">
    <div class="explication-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Cas d'usage</h1>
    </div>
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/territoirefd.png" class="img-responsive">
</div>   


<div class="explication row" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
    <div class="explication-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Explication</h1>
    </div>
    <div class="col-xs-12 margin-top-20 poi-m">
                    <?php 
                    $params = array(
                        "poiList"=>$poiList,
                        "listSteps" => array("1","2","3","4"),
                        "el" => $el,
                        "color1" => $this->costum["css"]["color"]["bg-green"]
                    );
                    echo $this->renderPartial("survey.views.tpls.wizard",$params); ?>
    </div> 
    <div class="explication-img col-xs-12 col-sm-12 no-padding">
            <center> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/costumDesCostums/illu.svg" class="img-responsive img-expli"></center>
            <h1 style="color : <?php echo $this->costum["css"]["color"]["blue"]; ?>; font-family: 'Covered By Your Grace', cursive !important; margin-top: 4%;">
            « Contribuer aux communs avec un contexte personnalisé. »
            </h1>
    </div>
</div>

<!-- Expérimentée --> 


<div class="experimente row">
    <div class="center" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
        <div class="tarif-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Expérimenter</h1>
        </div>
    </div>
  
          <div class="center row-1">
            <div class="col-md-4 ser-col-4" data-aos="fade-up-left" data-aos-easing="linear" data-aos-duration="1500">
              <div class="ser-col ser-1">
                <div class="icon-col">
                  <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                </div>
                <h2>Votre idée</h2>
                <p>Descriver votre<br> costum de rêve </p>
                <a onclick="dyFObj.openForm('project',null,null,null,dynFormProject);"  class="btn" href="javascript:;">Vous avez une idée de costum</a>
                <span class="circle hidden-xs">
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                </span>
              </div>
            </div>

            <div class="col-md-4 ser-col-4" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
              <div class="ser-col ser-2">
                <div class="icon-col">
                  <i class="fa fa-mouse-pointer" aria-hidden="true"></i>
                </div>
                <h2> En un clic</h2>
                <p>Testez-les templates<br> existant à partir de communecter </p>
                <!-- <a href="#" class="btn"> Read More</a> -->
                <span class="circle hidden-xs">
                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                </span>
              </div>
            </div>

            <div class="col-md-4 ser-col-4-l" data-aos="fade-up-right" data-aos-easing="linear" data-aos-duration="1500">
              <div class="ser-col ser-3">
                <div class="icon-col">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </div>
                <h2>En autonomie</h2>
                <p>Débrouillard,<br> faites le vous-mêmes</p>
                <a href="https://doc.co.tools/books/2---utiliser-loutil/page/costum" target="_blank"  class="btn"> Documentation</a>
              </div>
            </div>
          </div>


</div>


<div class="tarif row" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
    <div class="center">
        <div class="tarif-title col-xs-12 col-sm-12">
        <h1 style="font-family: 'Covered By Your Grace', cursive !important;">Tarifs</h1>
        </div>
    </div>
    
    <div style="margin-left: 2%; margin-right: 2%;">
    <div class="col-md-3 col-sm-6 col-xsx-6">
            <div class="serviceBox">
                <div class="service-icon">
                    <span><i class="fa fa-users"></i></span>
                </div>
                <div class="service-content">
                    <h3 class="title">2000 €</h3>
                    <p class="description">Association - <br> de 50 <br> adhérents </p>
                    <a href="#" data-toggle="modal" data-target="#descriptionTarif" class="read-more fa fa-plus" data-toggle="tooltip" title=""></a>
                </div>
            </div>
        </div>
 
        <div class="col-md-3 col-sm-6 col-xsx-6">
            <div class="serviceBox green">
                <div class="service-icon">
                    <span><i class="fa fa-users"></i></span>
                </div>
                <div class="service-content">
                    <h3 class="title">2800 €</h3>
                    <p class="description"> Association + <br> de 50 <br> adhérents </p>
                    <a href="#" onclick="changeModal('first')" data-toggle="modal" data-target="#descriptionTarif" class="read-more fa fa-plus" data-toggle="tooltip" title="Read More"></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xsx-6">
            <div class="serviceBox orange">
                <div class="service-icon">
                    <span><i class="fa fa-industry bg-nightblue"></i></span>
                </div>
                <div class="service-content">
                    <h3 class="title">3200 €</h3>
                    <p class="description"> Coopératives <br> et entreprises <br> responsable </p>
                    <a href="#" data-toggle="modal" data-target="#descriptionTarif" class="read-more fa fa-plus" data-toggle="tooltip" title="Read More"></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xsx-6">
            <div class="serviceBox blue">
                <div class="service-icon">
                    <span><i class="fa fa-circle-o"></i></span>
                </div>
                <div class="service-content">
                    <h3 class="title">Devis</h3>
                    <p class="description"> Collectivé <br> et autres <br>entreprise </p>
                    <a href="#" data-toggle="modal" data-target="#descriptionTarif" class="read-more fa fa-plus" data-toggle="tooltip" title="Read More"></a>
                </div>
            </div>
        </div>
    </div>



    <div class="tarif-description col-xs-12 col-sm-12">
   <p style="font-style: italic; text-align: center; margin-left: 10%; margin-right: 10%; margin-top: 3%; margin-bottom: 3%; color: #878786;">
       Nous sommes soucieux de proposer un tarif juste et équitable.
       Nous le fixons en fonction de la taille de votre organisation et vos besoins de personnalisations. <br />
       Nous privilégions le système d’abonnement mensuel. Si ce n’est pas possible merci de nous contacter. <br />
       Le prix fixe correspond à la mise en place du site, et le coût mensuel à la participation au commun.

   </p>
    </div>
</div>

<!-- <div class="bouton row">
        <div class="col-xs-12 col-sm-12" style="margin-bottom: 2%;">
  
        </div>
</div> -->


<!-- Modal -->
<div class="modal fade" id="descriptionTarif" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.5);">
  <div class="modal-dialog modal-lg" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Les tarifs</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -4%">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p> La participation mensuelle permet d’améliorer le commun et donne droit à l’assistance mail et chat. </p>
       <p>
        Le prix fixe comprend : <br />
        <ul>
            <li>Personnalisation d’une de nos maquettes : ajout du logo, couleurs et textes</li>
            <li>Choix des fonctionnalités (<a href="https://doc.co.tools/books/2---utiliser-loutil/page/liste" target="_blank">voir la liste</a>)</li>
            <li>500 Mo d’espace de stockage</li>
            <li><b>Formation de 3 heures à la prise en main de l’outil</b></li>
        </ul>
        </p>
        <p>
            Il est bien sûr possible d’aller plus loin (devis sur simple demande) : <br />
            <ul>
                <li>Importer des données</li>
                <li>Organiser un événement convivial de référencement collectif (cartopartie)</li>
                <li>Créer une maquette personnalisée</li>
                <li>Intégrer votre maquette réalisée chez un autre prestataire</li>
                <li>Créer une nouvelle fonctionnalité</li>
                <li>Organiser un concours</li>
            </ul>
        </p>
      </div>
    </div>
  </div>
</div>

<?php echo $this->renderPartial("co2.views.admin.costumize");  ?>
<script>

  var dynFormProject = {};
  var rcObj = {};
  dynFormProject.afterSave = function(data){
      dyFObj.commonAfterSave(data,function(){
        mylog.log("dataCustomize", data);
        rcObj.postMsg({ "channel": "#costumDesCostums", "text": "Le costum qui porte le slug de"+data.map.slug+" vient d'etre créer"}).then(function(data) {
            // console.log('Created msg', data);
            console.log(data.success);
        });
        contextData.slug = data.map.slug;
        contextData.collection = data.map.collection;
        contextData.id = data.id;
        costumize();
        //window.location.href = baseUrl+"/#@"+data.map.slug;       

      });
    }
 jQuery(document).ready(function() {
        //init AOS
        AOS.init();

    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
    
    // autoCompleteSearchGS("", 0,3);
    globalSearchCostum();

    //carousel

    $('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});


});

// GLOBALSEARCHCOSTUM
function globalSearchCostum(){
    // autoCompleteSearchGS(search,1,3);
    mylog.log("---------------- moduleId", moduleId);

    var data = {"searchType" : "costum", "searchBy" : "ALL",
    "indexMin" : 1, "indexMax" : 3, "limit" : 3 };

    ajaxPost(
        null,
        baseUrl+"/" + moduleId + "/search/globalautocomplete",
        data,
        function(a){
            mylog.log("success autoglobalsearch --------------------", a);

            var i = data.indexMin;
            // Gestion affichage des costums
            var str = "<div class='costum-search'>";

            $.each(a.results, function(k, v) {
                if(i <= data.indexMax){
                    mylog.log("globalcostum res",v);
                    i++; // Permettra de count le nombre de projet
                    var shortDescription = (notEmpty(v.shortDescription)) ? v.shortDescription : "";
                    var images = (notEmpty(v.profilMediumImageUrl)) ? baseUrl+v.profilMediumImageUrl : "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.costum.defaultImg;

                    // Initilisation de certains choses
                    str += "<div class='col-md-4 costum-m'>";
                    str += "<a href='"+baseUrl+"/costum/co/index/slug/"+v.slug+"' target='_blank'>";
                    str += "<img src='"+images+"' class='img-responsive img-projet'>";
                    str += "<h4>"+v.name+"</h4>";
                    str += "<p>"+shortDescription+"</p>";

                    str += "<div class='letter-red'>";
                    if(notEmpty(v.tags)){
                        v.tags.forEach(value =>
                            str += "#"+value );
                    }
                    str += "</div>";

                    str += "</a></div>";
                }
                else
                    return false;
            });

            str += "</div>";
            mylog.log(str);
            $("#content-results-persons").html(str);
        },
        function (data){
            mylog.log("error"); mylog.dir(data);
        },
        "json"
    );
}



</script>