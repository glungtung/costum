
<div class="row">
	<div class="col-xs-12">
		<div id='filterContainer' class='searchObjCSS'></div>
	</div>
	<div class="content-answer-search col-xs-12 col-md-12">
		<div class="headerSearchContainer col-xs-12 no-padding"></div>
		<div class="col-xs-12 bodySearchContainer margin-top-10">
			<div class="no-padding col-xs-12" id="dropdown_search"></div>
			<div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
		</div>
	</div>
	<div id="mapOfResultsAnsw" class="col-md-6" style="height:500px;"></div>
</div>

<script type="text/javascript">
	searchObject.types=["organizations","projects"];
	searchObject.indexStep=30;

	//definition des paramètres pour le filtre
	var paramsFilter= {
		container : "#filterContainer",
		urlData : baseUrl+"/co2/search/globalautocomplete",
		interface : {
			events : {
				page : true
			}
		},
		filters : {
			"text" : true
		},
		defaults:{
			notSourceKey : true,
			types:["organizations","projects"],
			forced : {
                    "filters" : {
                        "costum.slug" : {
                            "$exists" : true
                        }
                    }
                }
		},

		header:{
			dom : ".headerSearchContainer",
			options:{
				left:{
					classes :"col-xs-12 no-padding",
					group:{
					count : true,
					map : false
				}
				}
			},
			views : {
				map : function(fObj,v){
				/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							'<i class="fa fa-map-marker"></i> '+trad.map+
						'</button>';*/
						return "";
			}
		},
		events : {
			map : function(fObj){
				$(".btn-show-map-search").off().on("click", function(){
					$("#mapOfResultsAnsw").css({"left":"0px", "right":"0px", "bottom":"50px"});
					$("#mapOfResultsAnsw").show(200);
					$(".footerSearchContainer").addClass("affix");
					mapAnsw.map.invalidateSize();
					$("body").css({"overflow":"hidden"});
					$("#mapOfResultsAnsw .btn-hide-map").off().on("click", function(){
						$("#mapOfResultsAnsw").hide(200);
						$("body").css({"overflow":"inherit"});
						$(".footerSearchContainer").removeClass("affix");	
					});

				});
			}
		}
		}
	};

	var filterSearch={};

	$(function(){
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.header.set=function(fObj){
			var resultsStr = (fObj.results.numberOfResults > 1) ? " dossiers" : "dossier";
			if(typeof costum.hasRoles != "undefined" && $.inArray("Opérateur", costum.hasRoles)>=0  && !costum.isCostumAdmin)
				resultsStr+= " en attente d'opérateur";
			$(fObj.header.dom+" .countResults").html('<i class=\'fa fa-angle-down\'></i> ' + fObj.results.numberOfResults + ' '+resultsStr+' ');
		};

		filterSearch.results.render=function(fObj, results, data){
			mylog.log("govva",results);
			if(Object.keys(results).length > 0){
					ajaxPost(fObj.results.dom, baseUrl+"/survey/answer/views/tpl/costum.views.custom.costumDesCostums.viewCard", 
					{allCostum:results},
					function(){
						fObj.results.events(fObj);
					});
			}else 
				$(fObj.results.dom).append(fObj.results.end(fObj));
			fObj.results.events(fObj);
			fObj.results.addToMap(fObj, results);
		};

		filterSearch.results.addToMap = function(fObj, results){
			mylog.log("resultss",results);
			var elts = [];

			$.each(results, function(k, result){
				if(result.answers){
					$.each(result.answers, function(k, answer){
						if(answer.adress && answer.adress.geo)
						elts.push(
							{
								geo: answer.adress.geo,
								name: answer.proposition,
							}
						)
					})
				}
			})
			mylog.log("eltss",elts);
		};

		filterSearch.search.init(filterSearch);
	})

</script>