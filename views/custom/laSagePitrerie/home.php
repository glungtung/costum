<div id="start" class="section-home section-home-video">
    <div class="col-xs-12 content-video-home no-padding">
      <div class="col-xs-12 no-padding container-video text-center no-padding" style="max-height: 450px;overflow-y: hidden;">
        <img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/laSagePitrerie/banner.jpg' style="margin:auto;">
      </div>
    </div>
</div>
<h2 class="col-xs-12 bg-red text-lightwhite padding-20">C'est quoi</h2>
<span class="text-dark">
	Salut la compagnie
</span>
<h2 class="col-xs-12 bg-red text-lightwhite padding-20">Les événements</h2>
<div class="col-xs-12">
	<?php $events=PHDB::find(Event::COLLECTION, array('$or'=>array(
				array("organizer.".$this->costum["contextId"] => array('$exists'=> true)),
				array("source.key"=>"laSagePitrerie")))); 
	foreach($events as $e => $v){ ?>
		<div class="col-xs-12 bg-dark no-padding" style="border-radius:5px;">
			<div class="col-xs-4">
				<img src="<?php echo  Yii::app()->createUrl('/'.@$v['profilImageUrl']); ?>" class="img-responsive"/>
			</div>
			<div class="col-xs-8"><h3 class="col-xs-12 text-red"><?php echo $v["name"] ?></h3>
			<span class="col-xs-12 text-lightwhite"><?php echo @$v["shortDescription"] ?></span></div>
		</div>
	<?php } ?>
</div>