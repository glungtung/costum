
	<header class="masthead">
	  <div class="container container-profil">
	  <div class="row row-from-right">
	    <div class="col-sm-8 text-center">
	      <h1 class="nom">RAMIANDRISON Jean Dieu Donné</h1>
	      <h3 class="dev-web">Spécialité: Modélisation et Ingénierie Informatique (M2I)</h3>	

	    </div>
	    <div class="col-md-4 text-center">    
	        <img style="margin-top:10%;margin-bottom: 10%" class="img-responsive img-thumbnail" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/profil.jpg" width="50%" height="200">
	    </div>
	  </div>
	  </div>
	</header>  

	<!-- Begin Navbar -->
	<div id="nav">
	  <div class="navbar navbar-default navbar-static transition-right">
	    <div class="container" style="border-color: #D9534F;border: 1px solid #D9534F">
	      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
	      <a class="navbar-toggle text-color" data-toggle="collapse" data-target=".navbar-collapse" style="">
	        <span class="glyphicon glyphicon-bar"></span>
	        <span class="glyphicon glyphicon-bar"></span>
	        <span class="glyphicon glyphicon-bar"></span>
	      </a>
	      <div class="navbar-collapse collapse">
	        <ul class="nav navbar-nav">
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-etat-civile">Contact</a></li>
	          <li class="divider"></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-apropos-moi">Information</a></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-form-dipl">Formations & Diplomes</a></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-stg-exp">Stages & Expériences professionnels</a></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-cpt-info">Développement</a></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-langue">Langues</a></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-centre-int">Centre d’intérêt</a></li>
	          <li><a href="javascript:;" data-toggle="collapse" data-target=".navbar-collapse" id="li-divers">Divers</a></li>
	        </ul>
	      </div>		
	    </div>
	  </div><!-- /.navbar -->
	</div>

	<!-- Begin Body -->
	<div class="container">
	      <div class="row">
	        <div class="col-md-12 row-from-left">
	          <div class="row">
	            <div class="col-md-4 transition-left" id="etat-civile">
	              <h2  class="text-color">Contacts</h2>
	              <ul class="li-content" style="list-style-type:none;">
	                <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/marker.png" width="20" height="20"> Lot 0205F/0043 Soatsihadino FIANARANTSOA</li>
	                <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/phone.png" width="20" height="20"> +261 34 84 821 84</li>
	                <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/gmail.png" width="20" height="20"> ramiandrison.jdd@gmail.com</li>
	              </ul>
	            </div>

	            <div class="col-md-8 transition-right" id="apropos-moi">
	              <h2 class="text-color">Informations</h2>
	              <ul class="li-content">
	                <li>RAMIANDRISON Jean Dieu Donné</li>
	                <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/madagascar.png" width="20" height="20"> Malagasy</li>
	                <li>c Né le 25 Mai 1995 (25 ans)</li>
	              </ul>
	            </div>
	          </div>

	          <div class="row">
	            <div class="col-md-12 transition-left" id="form-dipl">
	              <h2 class="text-color">Formations et Diplomes</h2>
	              <ul class="li-content" style="list-style-type:none;">
	                <li> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/diploma.png" width="20" height="20" alt=""> <strong>2017-2019:</strong> Formation Master en Modélisation et Ingénierie Informatique (M2I) à l’Ecole De Management et d’Innovation Technologique à l’Université de Fianarantsoa.</li>
	                <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/diploma.png" width="20" height="20" alt=""> <strong>2014-2017:</strong> Formation Licence en Développement d’Application Internet/Intranet (DA2I) à l’Ecole De Management et d’Innovation Technologique à l’Université de Fianarantsoa.</li>
	                <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/diploma.png" width="20" height="20" alt=""> <strong>2013:</strong> Baccalauréat de l’enseignement générale, série D, Lycée privée Rakoto Andrianarijaona Ivory Atsimo.</li>
	              </ul>
	            </div>
	          </div>

	          <div class="row transition-right">
	            <div class="col-md-8" id="stg-exp">
	              <h2 class="text-color" style="padding-left: 12px">Stages et expériences professionnels</h2>
	              <ul>
	                <li>
	                  <p><strong>Mars 2019:</strong> Projet de mini-mémoire en vue de passage en Master 2 au sein de l’EMIT. <br>
	              <strong>Thème:</strong> <i>« Tableau de Bord et Géolocalisation en temps réel des auditeurs du site web radio <a href="http://rejf.net">http://rejf.net</a> de la Radio Evangélique Jiro sy Fanasina Fianarantsoa ».</i><br>
	              <strong>Langage:</strong> PHP avec Framework CodeIgniter et Javascript.</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Janvier 2019:</strong> Projet réalisé au sein de l’EMIT. <br>
	              <strong>Thème:</strong> <i>« Développement d’application mobile pour le suivi en ligne de l’emploi du temps de l’EMIT ».</i><br>
	              <strong>Technologie:</strong>Framework IONIC</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Décembre 2018:</strong> Projet réalisé au sein de l’EMIT. <br>
	              <strong>Thème:</strong> <i>« Conception et réalisation d’un site web de vente de voiture ».</i><br>
	              <strong>Technologie:</strong>PHP avec Framework Symfony 3.</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Novembre 2018:</strong> Projet réalisé au sein de l’EMIT. <br>
	              <strong>Thème:</strong> <i>« Conception et réalisation d’une application web pour gérer la vente de voiture ».</i><br>
	              <strong>Technologie:</strong>JAVA JEE.</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Décembre 2017 - Mars 2018 :</strong> Stage en vue de l’obtention du diplôme licence au sein de l’Atelier Vatolahy Fianarantsoa. <br>
	              <strong>Thème:</strong> <i>« Mise en place d’une application web pour gérer le mouvement de stock des produits en béton ».</i><br>
	              <strong>Technologie:</strong>PHP avec Framework CodeIgniter.</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Octobre 2017 :</strong> Projet réalisé au sein de l’EMIT. <br>
	              <strong>Thème:</strong> <i>« Gestion de réparation de vélo ».</i><br>
	              <strong>Technologie:</strong>Java (JSP et SERVLET).</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Novembre 2016 - Février 2017 :</strong> Stage au sein de Tranoben’ny Tantsaha Région Haute Matsiatra.<br>
	              <strong>Thème:</strong> <i>« Mise en place d’une application pour les suivis des apports des bénéficiaires de la chambre d’agriculture Haute Matsiatra ».</i><br>
	              <strong>Technologie:</strong>Java Swing.</p>
	                </li>
	              </ul>

	              <ul>
	                <li>
	                  <p><strong>Novembre 2016 :</strong> Projet réalisé au sein de l’EMIT<br>
	              <strong>Thème:</strong> <i>« Gestion de cabinet médical.».</i><br>
	              <strong>Technologie:</strong>PHP sans Framework.</p>
	                </li>
	              </ul>

	               <ul>
	                <li>
	                  <p><strong>Septembre 2015 :</strong> Projet réalisé au sein de l’EMIT<br>
	              <strong>Thème:</strong> <i>« Conception et réalisation d’un logiciel pour la gestion d’un Cabinet Médical ».</i><br>
	              <strong>Technologie:</strong>Access</p>
	                </li>
	              </ul>
	             
	            </div>

	            <div class="col-md-4" id="cpt-info">
	              <h2 class="text-color" style="padding-left: 12px">Développements</h2>
	                <h5>Systèmes d’exploitation</h5>
	                <ul style="list-style-type:none;">
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/windows.png" width="20" height="20" alt="">  Windows</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/linux.png" width="20" height="20" alt="">  Linux</li>
	                </ul>
	         
	          
	                
	                <h5>Modélisations</h5>
	                <ul>
	                  <li>UML</li>
	                  <li>BPMN</li>
	                  <li>MERISE</li>
	                </ul>
	                <h5>Langages de programmation</h5>
	                <ul style="list-style-type:none;">
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/java.png" width="20" height="20" alt=""> Java</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/python.png" width="20" height="20" alt=""> Python</li>
	                </ul>
	            
	           
	                <h5>Technologies web</h5>
	                <ul style="list-style-type:none;">
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/html5.png" width="20" height="20" alt=""> HTML5</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/css3.png" width="20" height="20" alt=""> CSS3</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/javascript.png" width="20" height="20" alt=""> JavaScript</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/php.png" width="20" height="20" alt=""> PHP</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/symfony.png" width="20" height="20" style="background-color: white;border-radius: 100%"> Symfony</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/codeigniter.png" width="20" height="20" alt=""> CodeIgniter</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/jquery.png" width="20" height="20" alt="" style="background-color: white;border-radius: 100%"> JQuery</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/reactjs.png" width="20" height="20" alt=""> ReactJS</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/nodejs.png" width="20" height="20" alt="" style="background-color: white;border-radius: 100%"> Node JS</li>
	                  <li>Rest</li>
	                  <li>Graphql</li>  
	                </ul>
	            
	          
	                <h5>SGBD</h5>
	                <ul style="list-style-type:none;">
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/mysql.png" width="20" height="20" alt=""> Mysql</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/postgresql.png" width="20" height="20" alt=""> Postgresql</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/mongodb.png" width="20" height="20" alt="" style="background-color: white;border-radius: 100%"> MongoDB</li>
	                </ul> 

	                <h5>Application mobile</h5>
	                <ul style="list-style-type:none;">
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/ionic.png" width="20" height="20" alt=""> Ionic</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/reactjs.png" width="20" height="20" alt=""> React native</li> 
	                </ul>

	                <h5>Système de gestion de version</h5>
	                <ul style="list-style-type:none;">
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/github.png" width="20" height="20" style="background-color: white;border-radius: 100%"> Github</li>
	                  <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/gitlab.png" width="20" height="20"> GitlAb</li> 
	                </ul>         
	            </div>
	          </div>

	          <div class="row">
	          <div class="col-md-4 transition-left" id="langue">
	            <h2 class="text-color">Langues</h2>
	            <h5>Francais</h5>
	            <ul style="list-style-type:none;">
	              <li> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/france.png" width="20" height="20"> Lu, écrit, parlé</li>
	            </ul>
	            <h5>Anglais</h5>
	            <ul style="list-style-type:none;">
	              <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/english.png" width="20" height="20"> Connaissance de base</li>
	            </ul>
	          </div>

	          <div class="col-md-4 transition-right" id="centre-int">
	            <h2 class="text-color">Centre d’intérêt</h2>
	            <h5>Sport</h5>
	            <ul style="list-style-type:none;">
	              <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/football.png" width="20" height="20" alt=""> Football</li>
	            </ul>
	            <h5>Loisirs</h5>
	            <ul style="list-style-type:none;">
	              <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/internet.png" width="20" height="20" alt=""> Internet</li>
	              <li><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/ramiandrisonjdd/joystick.png" width="20" height="20" alt=""> Jeux vidéo</li>
	            </ul>
	          </div>
	          <div class="col-md-4 transition-left" id="divers">
	            <h2 class="text-color">Divers</h2>
	            <ul>
	              <li>Sens du travail d’équipe</li>
	              <li>Dynamique</li>
	              <li>Capacité d’adaptation aux situations nouvelles</li>
	            </ul>
	          </div>

	        </div>
	        </div>
	      </div>
	    </div>
	    <div class="container transition-right">
	      <div class="row footer text-center">
	        <div class="col-md-12">Copyright ramiandrison © 2020. All rights reserved</div>
	      </div>
	    </div>


		<div class="row">
	    	<div class="col-md-3">
	    		Entrer votre choix : <input type="text" id="choixko">
	    		<button id="cherche">Chercher</button>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-md-12" id="resultat">
	    		
	    	</div>
	    </div>

	    <script>
	    	jQuery(document).ready(function(){
console.log(baseUrl+"/costum/ramiandrison/test");
	    		ajaxPost(
		            null,
		            baseUrl+"/costum/ramiandrison/test",
		            {
		    	  		id: "5f2d061913c26711008b456d"
		    	  	},
		            function(data){ 
		            	console.log(data);
		                /*console.log(data["5f32863113c2670e008b4574"].answers.cobrainstorm182020_1637_0.cobrainstorm182020_1637_01[0].user);
		                var anarako = data["5f32863113c2670e008b4574"].answers.cobrainstorm182020_1637_0.cobrainstorm182020_1637_01[0].user;
		                jQuery('#gova').html(anarako);*/
		                
		            }
		        );


		    	})



		    </script>
