<?php
$cssAnsScriptFilesModule = array(
	'/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));

$cssAnsScriptFilesModule = array(
	'/css/ctenat/filters.css',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );


//$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
//$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
//$this->renderPartial( $layoutPath.'modals.'.Yii::app()->params["CO2DomainName"].'.mainMenu', array("me"=>$me) );

$memberId = Yii::app()->session["userId"];
$memberType = Person::COLLECTION;
$tags = array();
$scopes = array(
	"codeInsee"=>array(),
	"codePostal"=>array(),
	"region"=>array(),
);
?>
<style type="text/css">
.simple-pagination li a, .simple-pagination li span {
	border: none;
	box-shadow: none !important;
	background: none !important;
	color: #2C3E50 !important;
	font-size: 16px !important;
	font-weight: 500;
}
.simple-pagination li.active span{
	color: #d9534f !important;
	font-size: 24px !important;	
}

</style>
<div id="adminDirectory" class="col-xs-12 no-padding"></div>
<script type="text/javascript">
var results = <?php echo json_encode($results) ?>;
var initType = <?php echo json_encode($typeDirectory) ?>;
var panelAdmin = <?php echo json_encode($panelAdmin) ?>;
var adminGroup = {};
var filterGroup = {};
jQuery(document).ready(function() {
	//alert("HERE");
	var paramsAdmin = {
		results : results,
		initType : initType,
		panelAdmin : panelAdmin
	}
	adminGroup = adminDirectory.init(paramsAdmin);

	
});
</script>