<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

	$logo = $this->costum["logo"]; 
?>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="https://threejs.org/examples/js/libs/stats.min.js"></script>
<style type="text/css">
	@font-face{
        font-family: "Homestead-Display";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/pixelHumain/Homestead-Display.ttf")
  }
  @font-face{
    font-family: "Montserrat Bold";
        src : url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/pixelHumain/Montserrat/Montserrat-Bold.ttf")
  }
  @font-face{
    font-family: "Montserrat Regular";
        src : url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/pixelHumain/Montserrat-Regular.ttf")
  }


</style>
<!-- <div style="z-index:0;" id="particles-js"> -->
<div style="background-color:#2A353D">
	
	<div style="z-index: 10;" class="row text-center">
		<div class="col-lg-12 col-xs-12 col-md-12">
			<img style="margin-right: -0.5%" class="img-responsive pull-right" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/pixelHumain/pixel.png'>
			<img class="img-responsive logoPixel" src="<?php echo $logo; ?>">
			<p class="title col-xs-11 col-lg-5 col-sm-10 col-md-8">
				UNE ORGANISATION <br> 
				COMMUNAUTAIRE DU DIGITAL <br> 
				AU SERVICE DU BIEN COMMUN	
			</p>
		</div>

		<div style="position: relative;height: 200px;" class="col-lg-12 col-xs-12 col-md-12 hidden-xs">
			<div style="height: 200px !important;" id="particlesjs"></div>
		</div>

		<div style="background-color:#273337;" class="text-center col-lg-12 col-xs-12 col-md-12">
			<div class="text-center col-lg-7 col-md-7"> 
				<h1 class="titleSecond col-lg-10 col-xs-12 col-md-10">Qui sommes-nous ?</h1>
				<p class="col-lg-6 col-xs-12 col-md-10 description">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,quis nostrud exerci tation ullamcorper suscipit.
				</p>
			</div>
			<div style="padding: 40px;" class="col-lg-4 col-md-5 col-xs-12">
				<center>
					<img style="border: solid 20px #2A353D;" class="img-responsive pull-right" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/pixelHumain/student.jpg'>
				</center>
			</div>
		</div>

		<div style="margin-top: 1vw;color:white" class="col-lg-12 col-md-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-xs-12 text-center">
				<center class="col-lg-12 col-md-12 col-xs-12">
					<img class="img-responsive imgPixel" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/pixelHumain/petit_bloc_titre.svg'>
				</center>
				<h1 class="col-lg-12 col-xs-12 col-md-12 titleThird" style="">Les pixels</h1>
			</div>
		</div>

		<div>
			<?php echo $this->renderPartial("costum.views.custom.pixelHumain.test1"); ?>
			<div style="" id="filterContainer" class="col-xs-2 col-md-3 col-lg-2 col-md-offset-1 pull-left custom-select">
				    <span style="font-family: 'Montserrat Regular';" class="custom-dropdown">
				        <select class="custom-dropdown__select custom-dropdown__select--white">
				            <option>Les Pixels</option>
				            <option value="développeur">Développeur(s)</option>
				            <option value="graphiste">Graphiste(s)</option>
				        </select>
				    </span>
			</div>	
			<div id="Community" style="margin-top: 0vw;margin-bottom: 4vw;position:relative;" class="col-lg-12 col-xs-12 col-md-12">
				<div style="margin-top: 6vw" class="no-padding carousel-border" >
					<div style="margin-top: -1vw;" class="hidden-xs" id="particles-js"></div>
		        	<div id="docCarousel" class="carousel slide" data-ride="carousel">
		        		<ol class="carousel-indicators">
				      		
    					</ol>
		            	<div class="carousel-inner itemctr">
		            		
		                </div>		          
		            <div id="arrow-caroussel">
		                <a style="opacity: 1" class="carousel-control" href="#docCarousel" data-slide="prev">
		                    <?php echo $this->renderPartial("costum.assets.images.pixelHumain.backLeft"); ?>
		                    <span class="sr-only">Previous</span>
		                </a>
		                <a style="opacity:1;margin-left: 84vw;" class="carousel-control" href="#docCarousel" data-slide="next">
		                     <?php echo $this->renderPartial("costum.assets.images.pixelHumain.backRight"); ?>
		                    <span class="sr-only">Next</span>
		                </a>
		            </div>   
		        </div>
		    </div> 
		</div>

		<div style="margin-top: 1vw;" class="col-lg-12 col-xs-12 col-md-12">
			<?php echo $this->renderPartial("costum.views.custom.pixelHumain.test2") ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {

	/* ---- stats.js config ---- */
	var  stats, update;
	stats = new Stats;
	stats.setMode(0);
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.left = '0px';
	stats.domElement.style.top = '0px';
	document.body.appendChild(stats.domElement);
	// count_particles = document.querySelector('.js-count-particles');
	update = function() {
	  stats.begin();
	  stats.end();
	  if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
	    count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
	  }
	  requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
		$("#bg-homepage").removeClass("inline-block");
		$("#bg-homepage").removeClass("pull-left");

		setTitle("Pixel Humain");

		var tags = "";

		getMembers(tags);	
	});

	$(".custom-dropdown__select").children("option").click(function(){
		tags = $(this).attr("value");
		getMembers(tags);
	});

	function getMembers(tags){
		//Envoi du coontextId et du tags en paramètres si tags n'est pas vide
		if (tags == "") {
			var params = {
				id : costum.contextId
			};
		}else{
			var params = {
				id : costum.contextId,
				tags : [tags]
			};
		}

		//On va chercher l'action et éxécuter la requête
        ajaxPost(
            null,
            baseUrl+"/costum/pixelhumain/getcommunityaction",
            params,
            function(data){
                mylog.log("success : ",data);

                var ctr = "";
                var str = "";

                var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/pixelHumain/logo-PixelHumain.png";
                mylog.log("url",url);

                if (data.result == true) {

                    var i = 0;
                    var count = 0;


                    ctr += "<div class='item active'>";
                    str += '<li data-target="#myCarousel" data-slide-to="'+count+'" class="active">';
                    // ctr += "<div class='item'></div>";

                    $(data.elt).each(function(k,v){

                        var imgProf = (v.imgProfil != "none") ? baseUrl+v.imgProfil: url;

                        if (count >= 2) {
                            str += "</li>";
                            str += '<li data-target="#myCarousel" data-slide-to="'+count+'">';
                        }

                        if (i >= 3) {
                            ctr += '</div>';
                            ctr += '<div class="item">';
                            i = 0;
                            count++;
                        }

                        i++;

                        ctr += '<div class="afficheCard">';
                        ctr += '<div class="col-lg-3 col-xs-11 col-sm-3 text-center">';
                        ctr += '<div class="containCard text-center">';
                        ctr += "<img class='bloc_profil img-responsive' src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/pixelHumain/bloc_profil.svg'>";
                        ctr += '<a href="#page.type.citoyens.id.'+v.id+'" class="undefined entityName bold text-dark add2fav  lbh">';
                        ctr += '<center style="position:relative;z-index:1000;">';
                        ctr += "<img class='imgProfil img-responsive' style='' src='"+imgProf+"'>";
                        ctr += "<p class='text col-xs-9 col-lg-12 col-sm-12' style='font-family: 'Montserrat Regular';color:#2A353D'>"+v.name+"</p>";
                        ctr += "<p class='text col-xs-9 col-lg-12 col-sm-12' style='font-family: 'Montserrat Regular';color:#2A353D'>"+v.tags+"</p>";
                        ctr += "<img class='plus img-responsive' style='' src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/pixelHumain/btn_voir_plus.svg'>";
                        ctr += '</center>';
                        ctr += '</a>';
                        ctr += '</div><br>';
                        ctr += '</div>';
                        ctr += '</div>';



                        mylog.log("i",count);
                        // mylog.log("count",count);
                    });
                    $(".carousel-indicators").html(str);
                }
                else{
                    str += "Aucun membres avec ce tag";
                }
                $(".itemctr").html(ctr);
            },
            function(data){
                mylog.log("error : ",data);
            },
            "json",
            {async : false}
        );
	}
	particlesJS("particles-js", {
	  "particles": {
	    "number": {
	      "value": 200,
	      "density": {
	        "enable": true,
	        "value_area": 700
	      }
	    },
	    "color": {
	      "value": "#ffffff"
	    },
	    "shape": {
	      "type": "edge",
	      "stroke": {
	        "width": 0,
	        "color": "#000000"
	      },
	      "polygon": {
	        "nb_sides": 10
	      },
	      "image": {
	        "src": "img/github.svg",
	        "width": 100,
	        "height": 100
	      }
	    },
	    "opacity": {
	      "value": 0.5,
	      "random": false,
	      "anim": {
	        "enable": false,
	        "speed": 1,
	        "opacity_min": 0.1,
	        "sync": false
	      }
	    },
	    "size": {
	      "value": 3,
	      "random": true,
	      "anim": {
	        "enable": false,
	        "speed": 10,
	        "size_min": 0.1,
	        "sync": false
	      }
	    },
	    "line_linked": {
	      "enable": true,
	      "distance": 150,
	      "color": "#ffffff",
	      "opacity": 0.4,
	      "width": 1
	    },
	    "move": {
	      "enable": true,
	      "speed": 2,
	      "direction": "none",
	      "random": false,
	      "straight": false,
	      "out_mode": "out",
	      "bounce": false,
	      "attract": {
	        "enable": false,
	        "rotateX": 600,
	        "rotateY": 1200
	      }
	    }
	  },
	  "interactivity": {
	    "detect_on": "canvas",
	    "events": {
	      "onhover": {
	        "enable": true,
	        "mode": "grab"
	      },
	      "onclick": {
	        "enable": true,
	        "mode": "grab"
	      },
	      "resize": true
	    },
	    "modes": {
	      "grab": {
	        "distance": 140,
	        "line_linked": {
	          "opacity": 1
	        }
	      },
	      "bubble": {
	        "distance": 400,
	        "size": 40,
	        "duration": 2,
	        "opacity": 8,
	        "speed": 3
	      },
	      "repulse": {
	        "distance": 200,
	        "duration": 0.4
	      },
	      "push": {
	        "particles_nb": 4
	      },
	      "remove": {
	        "particles_nb": 2
	      }
	    }
	  },
	  "retina_detect": true
	});

	particlesJS("particlesjs", {
	  "particles": {
	    "number": {
	      "value": 200,
	      "density": {
	        "enable": true,
	        "value_area": 800
	      }
	    },
	    "color": {
	      "value": "#ffffff"
	    },
	    "shape": {
	      "type": "edge",
	      "stroke": {
	        "width": 0,
	        "color": "#000000"
	      },
	      "polygon": {
	        "nb_sides": 10
	      },
	      "image": {
	        "src": "img/github.svg",
	        "width": 100,
	        "height": 100
	      }
	    },
	    "opacity": {
	      "value": 0.5,
	      "random": false,
	      "anim": {
	        "enable": false,
	        "speed": 1,
	        "opacity_min": 0.1,
	        "sync": false
	      }
	    },
	    "size": {
	      "value": 3,
	      "random": true,
	      "anim": {
	        "enable": false,
	        "speed": 10,
	        "size_min": 0.1,
	        "sync": false
	      }
	    },
	    "line_linked": {
	      "enable": true,
	      "distance": 150,
	      "color": "#ffffff",
	      "opacity": 0.4,
	      "width": 1
	    },
	    "move": {
	      "enable": true,
	      "speed": 2,
	      "direction": "none",
	      "random": false,
	      "straight": false,
	      "out_mode": "out",
	      "bounce": false,
	      "attract": {
	        "enable": false,
	        "rotateX": 600,
	        "rotateY": 1200
	      }
	    }
	  },
	  "interactivity": {
	    "detect_on": "canvas",
	    "events": {
	      "onhover": {
	        "enable": true,
	        "mode": "grab"
	      },
	      "onclick": {
	        "enable": true,
	        "mode": "grab"
	      },
	      "resize": true
	    },
	    "modes": {
	      "grab": {
	        "distance": 140,
	        "line_linked": {
	          "opacity": 1
	        }
	      },
	      "bubble": {
	        "distance": 400,
	        "size": 40,
	        "duration": 2,
	        "opacity": 8,
	        "speed": 3
	      },
	      "repulse": {
	        "distance": 200,
	        "duration": 0.4
	      },
	      "push": {
	        "particles_nb": 4
	      },
	      "remove": {
	        "particles_nb": 2
	      }
	    }
	  },
	  "retina_detect": true
	});
</script>


