<?php
$cssAnsScriptFilesModule = array(
    '/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
 
    $where=array("id"=>@$id, "type"=>$type, "doctype"=>"image", "contentKey"=> "slider");
    $images = Document::getListDocumentsWhere($where, "image");
?>
<style type="text/css">
    .social-share-button img{
        margin-right: 10px;
    }
</style>
<div class="col-xs-12 padding-10 toolsMenu">
	<button class="btn btn-default pull-right btn-close-preview">
		<i class="fa fa-times"></i>
	</button>
	<a href="#@<?php echo $element["slug"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
</div>
<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
<?php 
	if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"])))
		$url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
	else
		$url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]); 
	?> 
    	
	<div class="col-xs-12 no-padding" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
        
		<?php 
			$imgHtml='<img style="width:16vw;" class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" 
				alt="'.Yii::t("common","Banner").'" 
				src="'.$url.'">';
			if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
				$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
							class="thumb-info"  
							data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
							data-lightbox="all">'.
							$imgHtml.
						'</a>';
			}
			echo $imgHtml;
		?>		
	</div>
	<div class="content-img-profil-preview col-xs-6 col-xs-offset-3 col-lg-4 col-lg-offset-4">
		<?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?> 
		<a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
		class="thumb-info"  
		data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
		data-lightbox="all">
			<img class="img-responsive" style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
		</a>
		<?php }else{ ?>
			<img class="img-responsive shadow2 thumbnail" style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/>
		<?php } ?>
	</div>
	<div class="col-xs-12 margin-bottom-20 no-padding">
		<h3 class="title text-gray col-xs-12 text-center margin-bottom-20"><?php echo $element["name"] ?></h3>
        <span class="col-xs-12 text-center blockFontPreview"> 
            <?php if($type==Project::COLLECTION){ 
                $label=(isset($element["category"]) && $element["category"]=="cteR") ? "Territoire en CTE" : "Action";
                $iconColor=(isset($element["category"]) && $element["category"]=="cteR") ? $iconColor : "purple";
                $icon=(isset($element["category"]) && $element["category"]=="cteR") ? "map-marker" : "lightbulb-o";
                ?> 
                
                <span class="text-<?php echo $iconColor; ?>"><i class="fa fa-<?php echo $icon ?>"></i> <?php echo $label; ?></span>
            <?php }else{ ?>
                <span class="text-<?php echo $iconColor; ?>"><?php echo strtoupper(Yii::t("common", Element::getControlerByCollection($type))); ?></span>
            <?php } ?>
            <?php if(($type==Organization::COLLECTION || $type==Event::COLLECTION) && @$element["type"]){ 
                if($type==Organization::COLLECTION)
                    $typesList=Organization::$types;
                else
                    $typesList=Event::$types;
                ?>
                    <i class="fa fa-x fa-angle-right margin-left-10"></i>
                    <span class="margin-left-10"><?php echo Yii::t("category", $typesList[$element["type"]]) ?></span>
            <?php } ?>
            
        </span>
        
        <?php 
            if(!empty($element["address"]["addressLocality"])){ ?>
                <div class="header-address col-xs-12 text-center blockFontPreview">
                    <?php
                        echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
                        echo !empty($element["address"]["postalCode"]) ? 
                                $element["address"]["postalCode"].", " : "";
                        echo $element["address"]["addressLocality"];
                    ?>
                </div>
            <?php } ?>
            <div class="header-tags col-xs-12 text-center blockFontPreview">
                <?php 
                if(@$element["tags"] && $type!=Project::COLLECTION){ 
                    foreach ($element["tags"] as $key => $tag) { ?>
                        <a  href="javascript:;"  class="letter-red" style="vertical-align: top;">#<?php echo $tag; ?></a>
                    <?php } 
                } ?>
                </div>
            </div>
        <?php if($type==Event::COLLECTION){ ?>
            <div class="event-infos-header text-center margin-top-10 col-xs-12 blockFontPreview"  style="font-size: 14px;font-weight: none;"></div>
        <?php } ?>
        <?php if(isset($element["filRouge"])){ 
            $descr= (strlen($element["filRouge"]) > 250) ? substr($element["filRouge"], 0, 250)." ..." : $element["filRouge"]; ?>
            <div class="col-xs-12 text-center margin-bottom-20 markdown-txt"><?php echo $descr ?></div>

        <?php } ?> 
        <div class="social-share-button col-xs-12 text-center margin-bottom-20"></div> 
		<?php if(isset($element["shortDescription"]) && !empty($element["shortDescription"]) && $element["shortDescription"] != "Nouveau Candidat au CTE") echo "<span class='col-xs-12 text-center'>".$element["shortDescription"]."</span>"; ?>
    	<?php if(@$element["category"]=="cteR"){ 
            $descr= (@$element["why"]) ? substr($element["why"], 0, 400)." ..." : ""; ?>
    		<div class="col-xs-12 text-center markdown-txt"><?php echo $descr ?></div>
	    <?php	}else{ 
                $descr= (@$element["description"]) ? substr($element["description"], 0, 400)." ..." : ""; ?>

				<div class="col-xs-12 text-center markdown-txt"><?php echo $descr ?></div>
		<?php	}?>
		<div class="col-xs-12">
			<?php 
				if(isset($element["links"]) && isset($element["links"]["contributors"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les contributeurs", "links"=>$element["links"]["contributors"], "connectType"=>"contributors", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
				} 
				if(isset($element["links"]) && isset($element["links"]["members"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les membres", "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
				} 
				if(isset($element["links"]) && isset($element["links"]["projects"])){ 
                    if($type==Project::COLLECTION){
                        $label=(isset($element["category"]) && $element["category"]=="cteR") ? "Les actions" : "Développé sur"; 
                    }
                    else{
                        $label="Contribue à";
                    }
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>$label, "links"=>$element["links"]["projects"], "connectType"=>"projects", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
				} 

				if(isset($element["links"]) && isset($element["links"]["memberOf"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Membre de", "links"=>$element["links"]["memberOf"], "connectType"=>"organizations", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10", "contextId"=>$id, "contextType"=>$type));
				} 
			?>
		</div>
		
	</div>
	

?>
</div>
<script type="text/javascript">
    var typePreview=<?php echo json_encode($type); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
	jQuery(document).ready(function() {
		coInterface.bindTooltips();
        $(".markdown-txt").each(function(){
            descHtml = dataHelper.markdownToHtml($(this).html()); 
            var tmp = document.createElement("DIV");
            tmp.innerHTML = descHtml;
            descText = tmp.textContent || tmp.innerText || "";
            $(this).html(descText);
        });
        
        $(".container-preview .social-share-button").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : [{"type":"facebook"}, {"type":"twitter"} ], "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
        resizeContainer();
	});
	
</script> 