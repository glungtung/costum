<?php 
  /*  $cssAnsScriptFilesModule = array(
    	'/css/filters.css',
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());*/
?> 
<div class="content-answer-search col-xs-12 col-md-6">
	<div class="headerSearchContainer col-xs-12 no-padding"></div>
	<div class="col-xs-12 bodySearchContainer margin-top-10 <?php echo $page ?>">
	    <div class="no-padding col-xs-12" id="dropdown_search">
	    </div>
	    </div>
	    <div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
	</div>
</div>
<div id="mapOfResultsAnsw">

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	searchObject.types=["answers"];
	searchObject.indexStep=30;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	urlData : baseUrl+"/survey/answer/directory/source/"+costum.slug,
	 	interface : {
	 		events : {
	 			page : true
	 		}
	 	},
	 	filters : {
	 		text : true,
	 		address : {
	 			view: "text",
	 			event:"text",
	 			placeholder : "Où (ville, cp) ?"
	 		}
	 	},
	 	defaults:{
	 		types:["answers"]
	 	},
	 	header:{
	 		options:{
	 			left:{
	 				classes :"col-xs-12 no-padding",
	 				group:{
						count : true,
						map : true
					}
	 			}
	 		},
	 		views : {
	 			map : function(fObj,v){
					return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
								'<i class="fa fa-map-marker"></i> '+trad.map+
							'</button>';
				}
			},
			events : {
				map : function(fObj){
					$(".btn-show-map-search").off().on("click", function(){
						$("#mapOfResultsAnsw").css({"left":"0px", "right":"0px", "bottom":"50px"});
						$("#mapOfResultsAnsw").show(200);
						$(".footerSearchContainer").addClass("affix");
						mapAnsw.map.invalidateSize();
						$("body").css({"overflow":"hidden"});
						$("#mapOfResultsAnsw .btn-hide-map").off().on("click", function(){
							$("#mapOfResultsAnsw").hide(200);
							$("body").css({"overflow":"inherit"});
							$(".footerSearchContainer").removeClass("affix");	
						});

					});
				}
			}
	 	}
	};

	if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) || (typeof costum.hasRoles != "undefined" && $.inArray("Financeur", costum.hasRoles)>=0 )){
		// || $.inArray("Opérateur", costum.hasRoles)>=0
		paramsFilter.filters.status = {
 			view : "dropdownList",
 			type : "filters",
 			field : "step",
 			name : "Statuts des dossiers",
 			event : "filters",
 			list : {
 			//	deal1 : "En attente d'opérateur",
 			//	deal12 : "En attente de validation",
    			deal2 : "Montage du dossier",
    			deal3 : "Budget prévisionnel",
    			deal4 : "En cours d'instruction financeurs",
    			deal5 : "Travaux en cours"
    		}
 		};
 	}else{
 		paramsFilter.defaults.fields={
 			step : "deal1"
 		};
 	}
	var filterSearch={};
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.header.set=function(fObj){
			//var countRes=fObj.results.numberOfResults(fObj);
			var resultsStr = (fObj.results.numberOfResults > 1) ? " dossiers" : "dossier";
			if(typeof costum.hasRoles != "undefined" && $.inArray("Opérateur", costum.hasRoles)>=0  && !costum.isCostumAdmin)
				resultsStr+= " en attente d'opérateur";
			$(fObj.header.dom+" .countResults").html('<i class=\'fa fa-angle-down\'></i> ' + fObj.results.numberOfResults + ' '+resultsStr+' ');
		};
		filterSearch.results.render=function(fObj, results, data){


			if(Object.keys(results).length > 0){
					ajaxPost(fObj.results.dom, baseUrl+"/survey/answer/views/tpl/costum.views.custom.deal.newAnswers", 
					{allAnswers:results,
						form:data.form,
						forms:data.forms,
						el: data.el,
						what: data.what,
						canEdit: data.canEdit
					},
					function(){
						fObj.results.events(fObj);
					});
			}else 
				$(fObj.results.dom).append(fObj.results.end(fObj));
			fObj.results.events(fObj);
			fObj.results.addToMap(fObj, results);
		};
		var paramsMapAns = {
	        container : "mapOfResultsAnsw",
	        hideContainer : ".pageContent",
	        activePopUp : true,
	        tile : "maptiler",
	        btnHide : true
	    };
	    mapAnsw = mapObj.init(paramsMapAns);
	    filterSearch.results.addToMap = function(fObj, results){
	    	mapAnsw.clearMap();
	    	addResToMap={};
	    	$.each(results,function(e,v){
	    		if(typeof v.mappingValues != "undefined" && typeof v.mappingValues.name != "undefined" && notNull(v.mappingValues.name)){
	    			addResToMap[e]=v;
	    			$.each(v.mappingValues, function(k, val){
	    				if(notNull(val))
    						addResToMap[e][k]=val;
	    			});
	    		}
	    	});
	    	mylog.log("azeee", addResToMap);
    		mapAnsw.addElts(addResToMap);
			mapAnsw.map.invalidateSize();
    	};
		filterSearch.search.init(filterSearch);
	});
</script>