
		<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">	
			<div class="col-xs-12 text-white pull-right">
		    	<div class="col-xs-12 col-sm-9 text-white pull-right" style="text-shadow: 2px 2px black;">
					<?php if (@$element["status"] == "deletePending") { ?> 
						<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
					<?php } ?>
					<h4 class="text-left padding-left-15 pull-left no-margin">
						<span id="nameHeader">
							<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">
								
							</div>
							<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i> 
							<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
						</span>
						<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
								$typesList= array(
									"workshop" => "Atelier",
									"tasting" => "Dégustation",
									"secondHandStoreJumbleSale" => "Dépôt-vente, Braderie",
									"radioShow" => "Emission de radio",
									"exhibition"=> "Exposition",
									"trainingAwareness" => "Formation, sensibilisation",
									"forumMeetingTradeFair" => "Forums, Recontres, salons",
									"openHouse" => "Portes-ouvertes",
									"filmProjection" => "Projection de film",
									"others"=> "Autres",
									"competition" => "Competition",
									"concert" => "Concert",
									"contest" => "Concours",
									"exhibition" => "Exposition",
									"festival" => "Festival",
									"getTogether" => "Rencontre",
									"market" => "Marché",
									"meeting" => "Réunion",
									"course"=>"Formation",
									"conference"=>"Conférence",
									"debate"=>"Débat",
									"film"=>"Film",
									"stand"=>"Stand",
									"crowdfunding"=>"Financement Participatif",
									"internship" => "Stage",
									"spectacle" =>  "Spectacle",
									"protest" => "Manifestation",
									"fair" => "Foire"
								);
						?>
							<span id="typeHeader" class="margin-left-10 pull-left">
								<i class="fa fa-x fa-angle-right pull-left"></i>
								<div class="type-header pull-left">
							 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
							 	</div>
							</span>
						<?php } ?>
					</h4>	

					
				</div>

			

			<?php 
				$classAddress = ( (@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden" ); ?>	
				<div class="header-address-tags col-xs-12 col-sm-9 pull-right margin-bottom-5 <?php echo $classAddress ; ?>">
					<?php if(!empty($element["address"]["addressLocality"])){ ?>
						<div class="header-address badge letter-white bg-red margin-left-5 pull-left">
							<?php
								echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? 
										$element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
						</div>
						<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden" ); ?>
							<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO ; ?>" style="font-size: 10px;line-height: 20px;">
								<i class="fa fa-circle-o"></i>
							</span>
						
					<?php } ?>
					<div class="header-tags pull-left">
					<?php 
					if(@$element["tags"]){ 
						foreach ($element["tags"] as $key => $tag) { ?>
							<a  href="#search?text=#<?php echo $tag; ?>"  class="badge letter-red bg-white lbh" style="vertical-align: top;">#<?php echo $tag; ?></a>
						<?php } 
					} ?>
					</div>
				</div>

			
			
				<div class="col-xs-12 col-sm-9 pull-right">
					<span class="pull-left text-white" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
					</span>	
				</div>
			
			<?php if( ( $edit || $openEdition ) && !empty(Yii::app()->session["userId"])){ 
				$href=(isset($bannerConfig["editButton"]) 
					&& isset($bannerConfig["editButton"]["dynform"]) 
					&& $bannerConfig["editButton"]["dynform"]) ? "javascript:dyFObj.editElement('".Element::getControlerByCollection($element["collection"])."', '".(string)$element["_id"]."');" : "javascript:;";
				?>
				<div class="col-xs-12 col-sm-9 pull-right">
					<a href="<?php echo $href ?>" class="pull-left btn letter-blue bg-white" id="btnHeaderEditInfos">
						<i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit information") ?>
					</a>	
				</div>

			<?php } ?>
		</div>
		
		<?php if(in_array($element["collection"], [Event::COLLECTION, Poi::COLLECTION, Project::COLLECTION])){ 
			if(@$element['parent'] || @$element['organizer'] ){ ?>
				<div class="section-date pull-right">
					<?php if($element["collection"]==Event::COLLECTION){ ?>
						<div class="event-infos-header"  style="font-size: 14px;font-weight: none;"></div>
					<?php } ?>
					<div style="font-size: 14px;font-weight: none;">
						<div id="parentHeader" >
							<?php if(@$element['parent']){
								$count=count($element["parent"]);
								$msg = ($element["collection"]==Event::COLLECTION) ? Yii::t("common","Planned on") : Yii::t("common","Carried by") ;
								echo $msg. " : ";
								foreach($element['parent'] as $key =>$v){
									$heightImg=($count>1) ? 35 : 25;
									$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
									<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
										class="lbh tooltips"
										<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
										<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
										<?php  if ($count==1) echo $v['name']; ?>
									</a>
									 
							<?php } ?> <br> <?php } ?>
						</div>
						<div id="organizerHeader" >
							<?php if(@$element['organizer']){
									$count=count($element["organizer"]);
									echo Yii::t("common","Organized by"). " : ";
									foreach($element['organizer'] as $key =>$v){
										$heightImg=($count>1) ? 35 : 25;
										$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
									<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
											class="lbh tooltips"
											<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
											<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
											<?php  if ($count==1) echo $v['name']; ?>
										</a>
										 
								<?php } } ?>
						</div>
					</div>
			    </div>
			<?php }
	 		} ?>
		


<script type="text/javascript">
var banner="<?php echo Yii::app()->getModule("costum")->assetsUrl .'/images/deal/bannerProfil.png'?>"; 
	jQuery(document).ready(function() {
		$("#contentBanner img").attr("src",banner);
	});
	
</script>