<style type="text/css">
	.notifications-dash .notifLi{
		background-color: rgba(208, 249, 208, 1);
	}
	.notifications-dash .notifLi.read{
		background-color: white !important;
	}
	.notifications-dash ul.notifListElement {
	    padding: 0px !important;
	    max-height: 350px;
	    overflow-y: scroll;
	    border-radius: 5px;
	}
	.notifications-dash a.notif{
		font-size : 11px;
		font-weight: bold;
	}
	.notifications-dash a.notif .content-icon{
		position: absolute;
	}
	.notifications-dash a.notif .content-icon span.label{
		background-color: #56c557b5 !important;
	}
	.notifications-dash a.notif .notif-text-content{
		width: 91.66666667% !important; 
		float: right;
	}
	.notifications-dash .pageslide-title{
		width: 100%;
    	padding-bottom: 5px;
    	text-transform: uppercase !important;
    	font-size: 18px !important;
	}
	.notifications-dash .pageslide-title i.fa-angle-down{
		display:none;
	}
	.notifications-dash .btn-notification-action{
		display: none;
	}

	@media (max-width: 792px){
		.menu-dashboard{
			display:none;
			width: 90%;
		}
	}
	.dashboard-modal-content h1, .dashboard-modal-content h2, .dashboard-modal-content h3{
		color: white;
	}
	.dashboard-modal-content .dash-answers{
		display: none;
	}
	.dashboard-modal-content .answerLi{    
	    color: white;
    	text-transform: initial;
    	box-shadow: 1px 2px 3px -1px #f4f4f44f;
    	font-weight: initial;
	}
	.dashboard-modal-content .answerLi .titleAnsw {
	    color: #67b04c;
	    font-size: 20px;
	}
	.dashboard-modal-content .answerLi b {
    	color: #a2ea87;
	}
	.dashboard-modal-content .answerLi:hover {
    	background-color: #3d4f3c;
	}
</style>
 <div class="col-md-3 col-sm-4 margin-bottom-20 menu-dashboard">
	<?php $imgPath=(isset(Yii::app()->session["user"]["profilMediumImageUrl"]) && !empty(Yii::app()->session["user"]["profilMediumImageUrl"])) ? Yii::app()->createUrl(Yii::app()->session["user"]["profilMediumImageUrl"]): $this->module->getParentAssetsUrl()."/images/thumbnail-default.jpg"; ?>  
	<img class="img-responsive profil-img" src="<?php echo $imgPath ?>">
	<?php if (Authorisation::isInterfaceAdmin()){ ?>
		<a href="#admin" class="lbh btn-dash-link col-xs-12">Administration</a>
	<?php	} ?>
	<a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="lbh btn-dash-link col-xs-12">Mon profil</a>
	<a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>.view.settings" class="lbh btn-dash-link col-xs-12">Mes paramètres</a>
	<!-- <a href="javascript:;" onclick="rcObj.loadChat("","citoyens", true, true)" class="btn-dash-link col-xs-12"> Ma messagerie</a> -->
	<div class="col-xs-12 no-padding notifications-dash"></div>
	<a href="javascript:;" class="btn-dash-link logoutBtn col-xs-12 bg-red" style="border-bottom: inherit;
    border-radius: 5px;"><i class="fa fa-sign-out"></i> Déconnexion</a>
</div>
<?php 
$tplSlug=isset($this->costum["slug"]) ? $this->costum["slug"] : $this->costum["assetsSlug"];
//var_dump("<pre>",$this->costum,"</pre>");
?>
<div class="col-md-9 col-sm-8 col-xs-12 dashboard-modal-content">
	<div class="col-xs-12 no-padding"><h2>Bonjour <?php echo Yii::app()->session["user"]["name"] ?></h2></div>
	<?php 
	
	$form=Form::getBySourceAndId($tplSlug);
		if(isset($this->costum["slug"]) && isset($this->costum["hasRoles"])){ ?>
			<div class="col-xs-12 no-padding">
			<?php if(in_array("Financeur", $this->costum["hasRoles"])){ ?>
				<span class="col-xs-12 no-padding text-white no-padding">Vous êtes administrateur de  
				<?php  
					$financor=$this->costum["financorOf"]; 
					$thumb=(isset($financor["profilThumbImageUrl"])) ? Yii::app()->createUrl($financor["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.png"; 
						?>
						<a href="#page.type.organizations.id.<?php echo (string)$financor["_id"] ?>" class="lbh-preview-element text-green"><img src="<?php echo $thumb ?>" width=40 height=40 style="margin-right: 5px;border-radius: 20px"/><?php echo $financor["name"] ?></a>
				<br/>  En qualité de partenaire financier de la DEAL, vous pouvez accéder à l'ensemble des dossiers afin de les consulter, valider les opérateurs, attribuer les financements et suivre les avancements de chaque dossier</span>
			<?php } else if(in_array("Opérateur", $this->costum["hasRoles"])){ ?> 
					<span class="col-xs-12 no-padding text-white">Vous êtes opérateur pour  
				<?php $operator=$this->costum["operatorOf"];
					$idOp=(string)$operator["_id"];
					$thumb=(isset($operator["profilThumbImageUrl"])) ? Yii::app()->createUrl($operator["profilThumbImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_organizations.png"; 
						?>
						<a href="#page.type.organizations.id.<?php echo $idOp ?>" class="lbh-preview-element text-green"><img src="<?php echo $thumb ?>" width=40 height=40 style="margin-right: 5px;border-radius: 20px"/><?php echo $operator["name"] ?></a>
				<?php 
				$answsOp=Answer::getListBy(null, $tplSlug ,null, array('links.operators.'.$idOp => array('$exists'=> true))); ?>
				<div class="col-xs-12 no-padding margin-bottom-20" style="border-bottom: 1px solid white;"><h3>Vos dossiers opérateurs (<?php echo count($answsOp) ?>)</h3></div>
				<?php  
					if(empty($answsOp)){
						echo "<span class='col-xs-12 text-white no-padding'>Vous n'êtes pas en charge de dossier en tant qu'opérateur. Retrouvez tous les dossiers en attente d'opérateur/maître d'ouvrage sur l'onglet <a href='#dossiers' class='lbh text-green'>dossiers</a></span>";
					}else{
						$params = [
				                      "el" => @$this->costum["el"],
				                      "color1" => "blue",
				                      "canEdit" => true,
				                      "allAnswers"=>@$answsOp,
				                      "what" => "dossiers",
				                      "form"=>$form["form"],
				                      "forms"=>$form["forms"],
				                      "viewDom"=>"dashboard"
				                  ];
				        echo $this->renderPartial("costum.views.custom.deal.newAnswers",$params);
					}
			} ?>
			</div>
		<?php } 
		$answs=Answer::getListBy(null, $this->costum["slug"],Yii::app()->session["userId"]);
	?>	
	<div class="col-xs-12 no-padding margin-bottom-20" style="border-bottom: 1px solid white;"><h3>Vos dossiers (<?php echo count($answs) ?>)</h3></div>
	<?php  
		if(empty($answs)){
			echo "<span class='col-xs-12 text-white no-padding'>Vous n'avez pas déposé de dossier pour effectuer des travaux.</span>".
			"<a href='#candidature' class='btn btn-success lbh col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0 col-md-4 margin-top-10'>Déposer une demande</a>";
		}else{
			$params = [
	                      "el" => @$this->costum["el"],
	                      "color1" => "blue",
	                      "canEdit" => true,
	                      "allAnswers"=>@$answs,
	                      "what" => "dossiers",
	                      "form"=>$form["form"],
	                      "forms"=>$form["forms"],
	                      "viewDom"=>"dashboard"
	                  ];
	        echo $this->renderPartial("costum.views.custom.deal.newAnswers",$params);
		 }
	 ?> 
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	mylog.log("pageProfil.views.notifications");
	$("#showAnswerBtndashboard").trigger("click");
	var url = "element/notifications/type/citoyens/id/"+userId;
	ajaxPost('.notifications-dash', baseUrl+'/'+moduleId+'/'+url, 
		null,
		function(){});
});
</script>