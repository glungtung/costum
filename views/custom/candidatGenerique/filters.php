
<script type="text/javascript">
    var isInterfaceAdmin = false
    <?php if(Authorisation::isInterfaceAdmin()){ ?>
        isInterfaceAdmin = true
    <?php } ?>
    var pageApp=<?php echo json_encode(@$page); ?>;
    var paramsFilter= {
        container: "#filters-nav",
        loadEvent : {
            default : "scroll"
        },
        results : {
            smartGrid: true,
            renderView : "directory.elementPanelHtml"
        },
        filters : {
            text : true,
            scope : true
        },
        header : {
            options : {
                left : {
                    classes : 'col-xs-8 elipsis no-padding',
                    group:{
                        count : true,
                        types : true
                    }
                },
                right : {
                    classes : 'col-xs-4 text-right no-padding',
                    group : {
                        map : true
                    }
                }
            }
        }
    }
    if(pageApp=="projects"){
        paramsFilter.defaults = {
            types : [ "projects" ]
        };
        if(isInterfaceAdmin){
            paramsFilter.header.options.right.group.add = true;
        }
    }else if(pageApp=="dda"){
        paramsFilter.results.renderView = "directory.coopPanelHtml";
        paramsFilter.defaults = {
            types : [ "proposals" ],
            fields : ["status", "producer", "amendementActivated", "amendementAndVote", "amendementDateEnd", "voteActivated", "voteDateEnd", "majority", "voteAnonymous", "answers", "votes", "voteCanChange"]
        };
        if(isInterfaceAdmin){
            paramsFilter.header.options.right.group.add = true;
        }
    }
    jQuery(document).ready(function() {
        filterSearch = searchObj.init(paramsFilter);
    });

</script>