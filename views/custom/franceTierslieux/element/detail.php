<?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        '/js/default/editInPlace.js',
        //'/css/element/about.css'

    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@$this->appConfig["element"];
    if(isset($this->costum)){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["js"] == "about.css")
            array_push($cssJsCostum, '/css/'.$this->costum["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }
?>

<?php if($edit){?>
    <div class="container-fluid  margin">
        <button class="themeBtn editElement"><i class="fa fa-pencil"></i> Editer les informations</button>
    </div>
<?php } ?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1 contain-char">
    <div class="col-xs-12 no-padding">
        <div class="why-edit-box">
            <h3><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>Description courte</i>'; ?></h3>
            <p class="activeMarkdown">
                <?php 
                $Parsedown = new Parsedown();
                echo (@$element["description"]) ? $Parsedown->text($element["description"]) : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
            </p>
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/book-icon.png" alt="Book Icon">
        </div>
    </div>
    <div class="col-xs-12 contain-char">
        <h2 class="title-section"><?php echo Yii::t("common","Caractérisques") ?></h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 bhoechie-tab-menu hidden-xs">
            <div class="list-group">

                 <?php $tabSpec=(isset($element["category"]) && $element["category"]=="network") ? array("typePlace"=>"Thématique(s)","manageModel"=>"Modèle de gestion", "state"=>"Avancement","certification"=>"Label","greeting"=>"Actions d'accueil","compagnon"=>"Compagnon France Tiers-lieux") : array("typePlace"=>"Type", "services"=>"Services", "manageModel"=>"Modèle de gestion", "state"=>"Avancement","spaceSize"=>"Superficie","certification"=>"Label","greeting"=>"Actions d'accueil","network"=>"Réseau associé","compagnon"=>"Compagnon France Tiers-lieux");
                 //var_dump($tabSpec);
                 $inin = 1;
                 foreach($tabSpec as $key => $val){

                 ?>
                        <a href="javascript:;" class="list-group-item  <?php echo $inin == 1 ? "active" : "" ?> text-center">
                            <?php echo $val; ?>
                        </a>
                    <?php $inin++;  } ?>

            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bhoechie-tab">
            <!-- flight section -->
            <?php
                //var_dump($tabSpec );
            $in = 1;
            foreach($tabSpec as $k => $v){
                $strTags="";
                $listCurrent=$this->costum["lists"][$k];
                if(!empty($element["tags"])){
                    foreach($element["tags"] as $tag){
                        if(in_array($tag, $listCurrent)){
                            if($tag=="Compagnon France Tiers-Lieux") {
                                $strTags="<i>Oui</i>";
                            }
                            else
                                $strTags.=(empty($strTags)) ? $tag : ", ".$tag;
                        }

                    }
                }
                if(empty($strTags)) $strTags="<i>Non spécifié</i>";
                ?>
                <div class="bhoechie-tab-content hidden-xs <?php echo $in == 1 ? "active" : "" ?> ">
                    <center>
                        <!--<h1 class="glyphicon glyphicon-map-marker" style="font-size:70px;color:#55518a"></h1>-->
                        <img class="icon-category" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/<?php echo $v; ?>.png" alt="<?php echo $v; ?>">
                        <h2 class="plan-title" ><?php echo $v; ?></h2>
                        <div class="plan-name" ><?php echo $strTags ?></div>
                    </center>
                </div>
                <div class="visible-xs col-xs-12 contentInformation padding-10">
                    <span class="title-char"><b><?php echo $v; ?></b></span><br/><span class="list-char"><?php echo $strTags ?></span>
                    </div>
            <?php  $in++;   }  ?>


        </div>
    </div>
    </div>

    <div class="col-xs-12 contain-char">
        <div class="item-tags">
            <div class="item-content-block">
                <!--<div class="block-title"><i class="fa fa-tags gradient-fill"></i> Tags</div>-->
                <h2 class="title-section">Tags</h2>
            </div>
            <div class="item-content-block tags">
             <?php
                if(@$element["tags"]){
                    $element["tags"] = array_unique($element["tags"]);
                    foreach ($element["tags"] as $key => $tag) { ?>
                        <a  href="#search?tags=<?php echo $tag; ?>"  class="btn-tag" ><?php echo $tag; ?></a>
                    <?php }
                } ?>

            </div>
        </div>
    </div>
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-section2 contain-char">
    <div class="col-xs-12 no-padding module come-in">
        <div class="con1">
            <h2 class="title-section">Contact</h2>
            <div class="tags-info">
                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-at gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>E-mail</span>
                        <p><?php echo (@$element["email"]) ? $element["email"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-phone gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>Contact</span>
                        <?php 
                          $contact = Yii::t("common","Not specified");
                          if(!empty($element["telephone"])){
                            if(is_array($element["telephone"])){
                                if(!empty($element["telephone"]["fixe"]) && is_array($element["telephone"]["fixe"])){
                                    $contact=$element["telephone"]["fixe"][0];
                                }else if(!empty($element["telephone"]["mobile"]) && is_array($element["telephone"]["mobile"])){
                                    $contact=$element["telephone"]["mobile"][0];
                                }
                            }else if(is_string($element["telephone"])){
                                $contact=$element["telephone"];
                            }
                          }else if(!empty($element["mobile"])){
                            $contact=$element["mobile"];
                          }
                          
                        ?>
                        <p><?php echo $contact; ?></p>

                    </div>
                    <div class="clear"></div>
                </div>

                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-globe gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>Site web</span>
                        <p>
                        <?php echo (isset($element["url"])) ? "<a href='".$element["url"]."' target='_blank' id='urlWebAbout' style='cursor:pointer;'>".$element["url"]."</a>" : "<i>".Yii::t('common','Not specified')."</i>" ?>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xs-12 no-padding">
        <!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->
        <?php $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    </div>
    <div class="col-xs-12 no-padding">
        <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
        <?php $this->renderPartial('co2.views.pod.newSocialNetwork', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    </div>

</div>
<?php $levelTer=isset($element["level"]) ? $element["level"] : ""; ?>
<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function () {
        controleAffiche();
    });


    function afficheMap(){
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
        };

        var paramsMapContent = {
            container : "divMapContent",
            latLon : contextData.geo,
            activeCluster : false,
            zoom : 16,
            activePopUp : false
        };


        mapAbout = mapObj.init(paramsMapContent);

        var paramsPointeur = {
            elt : {
                id : contextData.id,
                collection : contextData.type,
                geo : contextData.geo
            },
            center : true
        };
        mapAbout.addMarker(paramsPointeur);
        mapAbout.hideLoader();


    };

    jQuery(document).ready(function() {
        bindDynFormEditable();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });


        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });

        coInterface.bindLBHLinks();

        //edit element
        pageProfil.bindViewActionEvent();
        $(".editElement").click(function(){
            dyFObj.editMode=true;
            uploadObj.update = true;
            dyFObj.currentElement={type : contextData.type, id : contextData.id};

            var levelTer= <?php echo json_encode($levelTer) ?> ;
            if (levelTer !=="" && typeof costum.lists.level !="undefined" && exists(costum.lists.level[levelTer])){
                contextData.level=costum.lists.level[levelTer];
            }
            var dataElem = jQuery.extend(true, {},contextData);
            dataElem.type=contextData.typeElement;
            var formType= (typeof contextData.category!="undefined" && contextData.category=="network") ? "reseau" : typeObj[contextData.type].sameAs;
            // dyFObj.openForm(formType, "afterLoad", dataElem);
            var editDynF=jQuery.extend(true, {},costum.typeObj[type].dynFormCostum);
	        editDynF.onload.actions.hide.similarLinkcustom=1;
            dyFObj.editElement(contextData.type,contextData.id, null, editDynF, {}, "afterLoad");
        });
        $("#menuRight").find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
        $("#menuRight").find("a").toggleClass("changelabel");
        $(".BtnFiltersLieux").hide();
    });

    $(document).ready(function() {
        // var toastinfohtml='<div class="modal-body">'+
        //                         '<div class="row">'+
        //                             // '<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs left-Tlieux">'+
        //                             //     '<div class="contact-info">'+
        //                             //         '<div class="text-center cont-icon">'+
        //                             //             '<i class="fa fa-map-marker fa-4x mb-3 animated bounce"></i>'+
        //                             //             '<img src="'+assetPath+'/images/franceTierslieux/soulignement.svg" alt="image"/>'+
        //                             //         '</div>'+
        //                             //         '<img src="'+assetPath+'/images/franceTierslieux/logo.svg" alt="image"/>'+
        //                             //     '</div>'+
        //                             // '</div>'+
        //                             '<div class="col-xs-12">'+
        //                                 '<img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/Banniere-site-recensement.png" style="width:100%;text-align:center;display:inline-block;">'+ 
        //                                 '<div class="welcome-txt text-justify margin-top-20 section-paragraph">'+
        //                                     '<p>La fiche d\'identité détaillée est momentanément indisponible pendant la durée du <span style="font-weight:bold">Recensement des Tiers-Lieux 2023.</span> qui se déroule <span style="font-weight:bold;">du 22 mars au 10 mai</span>.</p>'+
        //                                     '<p>Si vous souhaitez modifier les informations de votre tiers-lieux nous vous invitons à le faire depuis le formulaire du recensement.</p>'+

        //                                 '</div>'+
        //                                 // '<div class="welcome-txt text-center section-paragraph">'+
        //                                 //     'En effet, , vous pouvez répondre au questionnaire Recensement des Tiers-Lieux auquel vous accéderer en cliquant sur "J\'accède au recensement"'+
        //                                 // '</div>'+
        //                             '</div>'+
        //                         '</div>'+
        //                     '</div>';
        // var boxRecElem=bootbox.dialog({
        //             message : toastinfohtml,
        //             closeButton: false,
        //             buttons : {
        //                 confirm: {
        //                     label: 'D\'accord',
        //                     className: 'btn-primary text-white',
        //                     callback : function(result){
        //                         urlCtrl.loadByHash("#recensement2023");
        //                     }
        //                 }
        //             }
        //         });
        // boxRecElem.css("background","blue");
        // mylog.log('boxRecElem',boxRecElem);
        // boxRecElem.next().addClass("display-block-important");
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });

</script>
