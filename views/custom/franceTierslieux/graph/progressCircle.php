<span id="arc"><span>De Coworking</span></span>

<script type="text/javascript">

    var progressCircle = {
        width: 500,
        height: 300,
        pi : 2 * Math.PI,
        data: {id:"colored", percent: 65},
        arc : null,
        parent:null,
        init: function(procir){
            procir.arc = d3.arc()
                .innerRadius(40)
                .outerRadius(60) // arc width
                .startAngle(0);

            procir.parent = d3.select("#arc").append("svg")
                .attr("width", procir.width)
                .attr("height", procir.height);

            procir.draw(procir, procir.data);
        },
        draw: function(procir, data){
            var d3Ring = procir.parent  
            .append("g")
            .attr("transform", "translate("+ procir.width/2 +"," +150+")")
            .attr("id",data.id);
        
            // background
            d3Ring
            .append("path")
            .datum({endAngle: procir.pi})
            .style("fill", "#ddd")
            .attr("d", procir.arc); 
            
            // foreground
            var foreground = d3Ring
            .append("path")
            .datum({endAngle: 0})
                .style("stroke", "none")
                .style("stroke-width", "0px")
                .style("opacity", 1)
            .attr("d", procir.arc)
            .attr("fill", "#F0506A");
            
            // text
            d3Ring
            .append("text")
            .attr("x", -20)
            .attr("y", 8) 
            .style("font-size", 25)
            .style("font-family", "Georgia, Arial, sans-serif")
            .text(data.percent + "%");
                
            var angle = procir.helpers.convertPercentToAngle(procir);
            
            // animation
            foreground
            .transition()
              .duration(1500)
                    .delay(500)
              .call(procir.arcTween, procir, angle);
        },
        arcTween: function(transition, procir, newAngle){
            transition.attrTween("d", function(d) {  
                var interpolate = d3.interpolate(d.endAngle, newAngle);
                return function(t) {
                    d.endAngle = interpolate(t);
                    return procir.arc(d);
                };
            });
        },
        helpers:{
            convertPercentToAngle: function(procir){
                return ( procir.data.percent / 100 ) * procir.pi
            }
        }
    }

    progressCircle.init(progressCircle);
</script>