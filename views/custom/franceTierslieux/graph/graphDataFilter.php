<style>
    .rounded-0{
        border-radius:1px;
    }
</style>

<div id="f<?= $kunik ?>">
    <div id="title<?= $kunik ?>"></div>
    <div class="row padding-left-15">
        <div class="btn-group margin-right-5" style="font-size:16pt">
            <button type="button" class="btn btn-lg btn-default rounded-0 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filtre par Region <span class="caret"></span>
            </button>
            <ul id="filterRegion<?= $kunik ?>" class="dropdown-menu">
                <li><a href='javascript:;' data-id='' class='filterRegions' >Tout</a></li>
            </ul>
        </div>
        <div class="btn-group" style="font-size:16pt">
            <button type="button" class="btn btn-lg btn-default rounded-0 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sections <span class="caret"></span>
            </button>
            <ul id="sections<?= $kunik ?>" class="dropdown-menu"></ul>
        </div>
    </div>
    <div id="loader<?= $kunik ?>"></div>
</div>

<script>
    jQuery(document).ready(function() {
	    var anchors = "";
    
        $(".pa-title").each(function(){
            anchors+="<li><a href='#"+$(this).attr("id")+"' data-typeurl='externalLink' class='lbh-anchor-btn' >"+$(this).text()+"</a></li>";
        })

       $("#64518f3cc7d57f54b4078228-1").css({
            position: "-webkit-sticky",
            position: "sticky",
            top: "50px",
            zIndex: 99999
        });

        var activeRegion = localStorage.getItem("activeRegion");
        if(activeRegion!=null && typeof activeRegion!="undefined" && activeRegion!="undefined" && activeRegion!=""){
            $("#title<?= $kunik ?>").append("<h4> La Données est focaliser sur la Région : "+activeRegion+"</h4>");
        }

        var regions = "";
        if(regionTL.lengh > 0){
            $.each(regionTL, function(index, r){
                regions+="<li><a href='javascript:;' data-id='"+index+"' data-name='"+r.name+"' class='filterRegions' >"+r.name+"</a></li>";
            })
            $("#filterRegion<?= $kunik ?>").append(regions);
        }else{
            ajaxPost(null,
                baseUrl + '/co2/search/getzone/', 
                {level:["3"], countryCode:"FR"}, 
                function(data){
                    regionTL = data;
                    $.each(regionTL, function(index, r){
                        regions+="<li><a href='javascript:;' data-id='"+index+"' data-name='"+r.name+"' class='filterRegions' >"+r.name+"</a></li>";
                    })
                    $("#filterRegion<?= $kunik ?>").append(regions);
                }
            )
        }
        setTimeout(() => {
            $("#sections<?= $kunik ?>").append(anchors);
        }, 0);
    });


    $(document).on("click", ".filterRegions", function(e){
        e.preventDefault();
        coInterface.showLoader("#all-block-container");
        localStorage.setItem("activeRegion", $(this).data("name"));
        setTimeout(() => {
            getDashboardData("", location.hash.replace("#", ""), function(){
                urlCtrl.loadByHash(location.hash);
            }, $(this).data("id"));
        }, 0);
    });

    $(document).off("click",".lbh-anchor-btn").on("click",".lbh-anchor-btn", function(e){
        e.stopPropagation();
        e.preventDefault();
        var idBtnLink = $(this).attr("href");
        $('html,body').animate({
                scrollTop: $(idBtnLink).offset().top-200},
            'slow');
    });
</script>
