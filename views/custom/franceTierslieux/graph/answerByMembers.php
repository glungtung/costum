<?php 

    $keyTpl = "answerByMembers";

    $paramsData = [
        "textLeft" => "TIERS-LIEUX RÉPONDANTS SOIT",
        "textRight" => "DES TIERS-LIEUX CARTOGRAPHIÉS",
        "coform" => ""
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
 ?>

<style type="text/css">
    .color-blue{
        color: #4E54C9;
    }

    .fs-24{
        font-size: 27pt;
        font-weight: bolder;
    }
    
</style>

<div class="color-blue fs-24">
    <span class="sp-text margin-top-10" data-id="<?= $blockKey ?>" data-field="textLeft"><?= $paramsData["textLeft"] ?></span>
    <span class="margin-top-10 textValue<?= $blockKey ?>" ></span>
    <span class="sp-text margin-top-10" data-id="<?= $blockKey ?>" data-field="textRight"><?= $paramsData["textRight"] ?></span>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $(".textValue<?= $blockKey ?>").text("XX");

        if(typeof costum["dashboardData"] != "undefined" && costum["dashboardData"]["<?= $blockKey ?>"]){
            $(".textValue<?= $blockKey ?>").text(costum["dashboardData"]["<?= $blockKey ?>"]["text_value"]+" %");

            $("a.super-href").each(function(i){
                $(this).text($(this).text().replace("XXXX", costum["dashboardData"]["<?= $blockKey ?>"]["countCommunity"]));
            });
        }

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                            $("#ajax-modal").modal('hide');
                            urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

