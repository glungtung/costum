<?php 

if($this->costum["contextType"] && $this->costum["contextId"]){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    if($this->costum["dashboard"]){   
        $el["costum"]["dashboard"] = $this->costum["dashboard"];
    }  
}

?>

<style>
    #graph-container-<?= $id ?>{
        margin-bottom: 0px !important;
        width: 100% !important;
        overflow: hidden !important;
    }

    #graph-container-<?= $id ?> svg:not(:root) {
        min-height: 340px !important;
    }
</style>

<?php
    if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
        $graphAssets = [
            '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
        ];
        
        HtmlHelper::registerCssAndScriptsFiles(
            $graphAssets,
            Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
        );
    }
?>

<?php 

    // formulaire
    $paramsData = [
        "title" => "",
        "coform" => "",
        "path" => "",
        "name" => "",
        "type" => ["circle"=>"Cercle D3", "mindmap"=>"Carte mentale D3", "network"=>"Réseau D3", "relation"=>"Relation D3"],
        "width" => ["4"=>4, "6"=>6, "8"=>8, "9"=>9, "10"=>10, "12"=>12],
        "typeValue" => "",
        "widthValue" => ""
    ];
    if(!isset($childForm) && !isset($formInputs)){
        // Get all Forms
        $childForm = PHDB::find("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true],"active"=>"true"));
        $formInputs = [];
        foreach ($childForm as $formKey => $formValue) {
            if(is_array($formValue["subForms"])){
               $subFormId =  array('$in' => $formValue["subForms"]);
            }

            if(is_string($formValue["subForms"]) && $formValue["subForms"] !=""){
                $subFormId =  $formValue["subForms"];
            }

            if(isset($subFormId) && is_array($subFormId) && $subFormId!=""){
                $subForms = PHDB::find(Form::COLLECTION, array(
                'id' => $subFormId));
                foreach ($subForms as $subFormKey => $subFormValue) {
                    if(isset($subFormValue["inputs"])){
                        $formInputs[$formKey][$subFormValue["id"]]=$subFormValue["inputs"];
                    }
                }
            }
        }
    }

    if(isset($el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["coform"])){
        $formId = $el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["coform"];
        $paramsData["coform"] = $formId;
    }

    if(isset($el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["name"])){
        $paramsData["name"] = $el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["name"];
    }

    if(isset($el["costum"]["dashboard"]["config"][$id])){
        $paramsData["title"] = $el["costum"]["dashboard"]["config"][$id]["title"];
        $paramsData["path"] = $el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["path"];
        $paramsData["widthValue"] = $el["costum"]["dashboard"]["config"][$id]["width"];
    }

    if(isset($el["costum"]["dashboard"]["config"][$id]["type"])){
        $paramsData["typeValue"] = $el["costum"]["dashboard"]["config"][$id]["type"];
    }

    // Default Form
    if(isset($formId)){
        $formTL = PHDB::findOneById(Form::COLLECTION, $formId);
    }else{
        $formTL = PHDB::findOne(Form::COLLECTION, array("parent.".$this->costum["contextId"] => array('$exists' => true)));
    }
    
    $formConfig = [];
    
    if(isset($path) && isset($formTL) && isset($formTL["params"][explode(".", $path)[1]])){
        $formConfig = $formTL["params"][explode(".", $path)[1]];
    }
 ?>

<?php if(Authorisation::isInterfaceAdmin()){

    echo '<span><button class="btn btn-sm btn-info dashboarConfigBtn edit'.$id.'Params" data-id="'.$id.'">
            <i class="fa fa-cogs"></i>
        </button> ';

    echo ' <button class="btn btn-sm btn-danger removeConfigBtn remove'.$id.'Params" data-id="'.$id.'">
            <i class="fa fa-trash"></i>
        </button></span>';
} ?>

<div>
    <div id="search-container-<?= $id ?>" class="searchObjCSS" style='background-color:white!important'></div>
    <div id="loader-container-<?= $id ?>"></div>
    <div id="graph-container-<?= $id ?>"></div>
</div>

<script>
    jQuery(document).ready(function() {
        contextData = <?php echo json_encode($el); ?>;
        var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
        var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
        var contextName = <?php echo json_encode($el["name"]); ?>;
        contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
    });
</script>

<script>
    var rawTags = "";
    var authorizedTags = []
    if(rawTags.trim() != ""){
        authorizedTags = rawTags.split(',');
    }
    var l<?= $id ?> = {
        //container: "#search-container-<?= $id ?>",
        loadEvent: {
            default: "graph"
        },
        defaults: {
            types: ["answers"],
            fields: ["collection", "user", "answers"],
            indexStep: 100,
            filters: {
                "form": "<?php echo isset($formId)?$formId:(string)$formTL['_id'] ?>",
                "draft": {'$exists':false},
                "answers": {'$exists':true}
            }
        },
        graph: {
            dom: "#graph-container-<?= $id ?>",
            authorizedTags: [],
            defaultGraph: "<?php echo $type ?>", //  mindmap, relation , network
        },
        header: {
            options : {}
        }
    };
    coInterface.showLoader("#loader-container-<?= $id ?>");
    let namePath = "<?php echo $paramsData["name"] ?>";
    if(namePath!=""){
        namePath = namePath.split(".");
    }
    var path<?= $id ?> = "";
    var root<?= $id ?> = "";
    var formConfig<?= $id ?> = <?php echo json_encode($formConfig) ?>;
    
    var p<?= $id ?> = {};

    <?php if(isset($path)){ ?>
        path<?= $id ?> = "<?php echo $path ?>";
    <?php } ?>

    <?php if(isset($title)){ ?>
        root<?= $id ?> = "<?php echo $title ?>";
    <?php } ?>


    extractLabelsFromAnswers = function(inputData){
        let extractedData;
        if(Array.isArray(inputData)){
            extractedData = [];
            for (let i = inputData.length - 1; i >= 0; i--) {
                if(typeof inputData[i] === 'string' && inputData[i]!==null){
                    extractedData.push(inputData[i]);
                }else{
                    extractedData.push(Object.keys(inputData[i])[0]);
                }
            }
        }else{
            if(inputData && inputData["value"]){
                extractedData = inputData["value"];
            }else{
                extractedData = "<?php echo $paramsData['title'] ?>";
            }
        }
        return extractedData;
    }

    
    
    setTimeout(() => {
        p<?= $id ?> = searchObj.init(l<?= $id ?>);

        <?php if($formConfig != []){ ?>

        <?php if(isset($type) && $type=="mindmap"){ ?>
        p<?= $id ?>.graph.successComplete = function(fObj, rawData){
            const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
            fObj.graph.graph.setAuthorizedTags(tags)
            this.lastResult = rawData;
            let answerPath = path<?= $id ?>.split(".");

            let structuredData = {
                id:"root<?= $id ?>",
                label:(root<?= $id ?>!="")?root<?= $id ?>:"<?php echo $paramsData['title'] ?>",
                children:[]
            };
            for (var i = formConfig<?= $id ?>["global"]["list"].length - 1; i >= 0; i--) {
                let child = {
                    id:"element"+i,
                    label:formConfig<?= $id ?>["global"]["list"][i],
                    children: []
                }
                Object.keys(rawData.results).forEach((key, index) => {
                    let extracted = null;
                    let identity = {};
                    if(rawData.results[key].answers && Array.isArray(namePath) && namePath.length==2){
                        identity["label"] = rawData.results[key].answers[namePath[0]][namePath[1]]
                    }else{
                        identity["label"] = "Non définit";
                    }
                    if(rawData.results[key].answers[answerPath[0]] && rawData.results[key].answers[answerPath[0]][answerPath[1]]){
                        extracted = extractLabelsFromAnswers(rawData.results[key].answers[answerPath[0]][answerPath[1]]);
                    }

                    if(extracted && extracted.includes(formConfig<?= $id ?>["global"]["list"][i])){
                        child.children.push({
                            id:key,
                            ...identity,
                            collection: rawData.results[key].collection
                        })
                    }
                });
                structuredData["children"].push(child)
            }

            p<?= $id ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
            p<?= $id ?>.graph.graph.initZoom();
        }
        <?php } ?>


        <?php if(isset($type) && $type=="network" ){ ?>
        p<?= $id ?>.graph.successComplete = function(fObj, rawData){
            const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
            fObj.graph.graph.setAuthorizedTags(tags)
            this.lastResult = rawData;
            let answerPath = path<?= $id ?>.split(".");
            
            let structuredData = [
                {
                    label: (root<?= $id ?>!="")?root<?= $id ?>:"Tiers Lieux",
                    type: "root",
                    group: "root"
                }
            ];

            for (var i = formConfig<?= $id ?>["global"]["list"].length - 1; i >= 0; i--) {
                Object.keys(rawData.results).forEach((key, index) => {
                    let extracted = null;
                    let identity = {};
                    if(rawData.results[key].answers && Array.isArray(namePath) && namePath.length == 2){
                        identity["label"] = rawData.results[key].answers[namePath[0]][namePath[1]]
                    }else{
                        identity["label"] = "Non définit";
                    }
                    if(rawData.results[key].answers[answerPath[0]] && rawData.results[key].answers[answerPath[0]][answerPath[1]]){
                        extracted = extractLabelsFromAnswers(rawData.results[key].answers[answerPath[0]][answerPath[1]]);
                    }
                    if(extracted && extracted.includes(formConfig<?= $id ?>["global"]["list"][i])){
                        structuredData.push({
                            id:key,
                            ...identity,
                            collection: rawData.results[key].collection,
                            type: formConfig<?= $id ?>["global"]["list"][i],
                            group: formConfig<?= $id ?>["global"]["list"][i],
                            img:""
                        })
                    }
                });
            }

            p<?= $id ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
            p<?= $id ?>.graph.graph.initZoom();
        }
        <?php } ?>

        <?php if(isset($type) && $type=="circle"){ ?>
        p<?= $id ?>.graph.successComplete = function(fObj, rawData){
            const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
            fObj.graph.graph.setAuthorizedTags(tags)
            this.lastResult = rawData;
            let answerPath = path<?= $id ?>.split(".");
            let structuredData = [];

            for (var i = formConfig<?= $id ?>["global"]["list"].length - 1; i >= 0; i--) {
                Object.keys(rawData.results).forEach((key, index) => {
                    let extracted = null;
                    if(rawData.results[key].answers[answerPath[0]] && rawData.results[key].answers[answerPath[0]][answerPath[1]]){
                        extracted = extractLabelsFromAnswers(rawData.results[key].answers[answerPath[0]][answerPath[1]]);
                    }
                    if(extracted && extracted.includes(formConfig<?= $id ?>["global"]["list"][i])){
                        let identity = {};
                        if(rawData.results[key].answers && Array.isArray(namePath) && namePath.length==2){
                            identity["label"] = rawData.results[key].answers[namePath[0]][namePath[1]]
                        }else{
                            identity["label"] = "Non définit";
                        }

                        structuredData.push({
                            id: key,
                            ...identity,
                            group: (formConfig<?= $id ?>["global"]["list"][i])?formConfig<?= $id ?>["global"]["list"][i]:"",
                            img: "",
                            collection:rawData.results[key].collection
                        })
                    }
                });
            }
            p<?= $id ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
            p<?= $id ?>.graph.graph.initZoom();
        }
        <?php } ?>
        <?php } ?>
        p<?= $id ?>.graph.init(p<?= $id ?>);
        p<?= $id ?>.search.init(p<?= $id ?>);
        
        setTimeout(() => {
            p<?= $id ?>.graph.graph.initZoom();
            $("#loader-container-<?= $id ?>").remove();
        },500);
    
    }, 200)

</script>

<script type="text/javascript">
    let sectionDyf = {};
    let tplCtx = {};


    jQuery(document).ready(function() {
        if(localStorage.getItem("previewMode") && localStorage.getItem("previewMode")=="v"){
            $(".dashboarConfigBtn").hide();
            $(".removeConfigBtn").hide();
        }else{
            $(".removeConfigBtn").show();
            $(".dashboarConfigBtn").show();
        }
        
        sectionDyf.<?php echo $id ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $id ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        "inputType" : "text",
                        "label" : "Titre du graph",
                        "value" :  sectionDyf.<?php echo $id ?>ParamsData.title
                    },
                    "url" : {
                        "inputType" : "select",
                        "label" : "Quelle type de graph",
                        "class" : "form-control <?php echo $id ?>",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $id ?>ParamsData.type,
                        "value":sectionDyf.<?php echo $id ?>ParamsData.typeValue
                    },
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $id ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {
                            <?php 
                            foreach($childForm as $key => $value) { 
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }    
                            ?>
                        },
                        "value": sectionDyf.<?php echo $id ?>ParamsData.coform
                    },
                    "path" : {
                        "inputType" : "select",
                        "class" : "<?php echo $id ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {}
                    },
                    "name" : {
                        "inputType" : "select",
                        "class" : "<?php echo $id ?>",
                        "label" : "Selectionner une champ dans pour représenter la réponse",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Nom de l'entité dans la réponse",
                        "options": {}
                    },
                    "width" : {
                        "inputType" : "select",
                        "class" : "form-control <?php echo $id ?>",
                        "label" : "Largeur d'espace à occuper",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $id ?>ParamsData.width,
                        "value":sectionDyf.<?php echo $id ?>ParamsData.widthValue
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $id ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $id ?>ParamsData.coform);
                    }
                    if($("#path.<?php echo $id ?> option[value='"+sectionDyf.<?php echo $id ?>ParamsData.path+"']").length > 0){
                        $("#path.new-graph").val(sectionDyf.<?php echo $id ?>ParamsData.path);
                    }
                    if($("#name.<?php echo $id ?> option[value='"+sectionDyf.<?php echo $id ?>ParamsData.name+"']").length > 0){
                        $("#name.<?php echo $id ?>").val(sectionDyf.<?php echo $id ?>ParamsData.name);
                    }
                    
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $id ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $id ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k != "title" && k != "width"){
                            if(k == "url"){
                                tplCtx.value["graph"][k] = "/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.d3Common/type/"+$("#"+k).val();
                                tplCtx.value["type"] = $("#"+k).val();
                            }else{
                                tplCtx.value["graph"]["data"][k] = $("#"+k).val();
                            }
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    tplCtx.value["counter"] = "";

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $id ?>Params").off().on("click",function() {  
            tplCtx["id"] = contextData.id;
            tplCtx["collection"] = contextData.collection;
            tplCtx["path"] = "costum.dashboard.config."+$(this).data("id");
            dyFObj.openForm( sectionDyf.<?php echo $id ?>Params,null, sectionDyf.<?php echo $id ?>ParamsData);
        });

        $(".remove<?php echo $id ?>Params").off().on("click",function() {  
            tplCtx["id"] = contextData.id;
            tplCtx["collection"] = contextData.collection;
            tplCtx["path"] = "costum.dashboard.config."+$(this).data("id");
            tplCtx["value"] = {};

            bootbox.dialog({message:`<div class="alert-white text-center"><br>
                <strong>Vous voulez vraiement supprimer cette section de graph</strong>
                <br><br>
                <button id="deleteGraphBtn" class="btn btn-danger bootbox-close-button">JE CONFIRME</button>
                <button type="button" class="btn btn-default bootbox-close-button" aria-hidden="true">NON, ANNULER</button></div>`});

                $("#deleteGraphBtn").on("click", function(){
                    dataHelper.path2Value( tplCtx, function(params) {
                        toastr.info("La suppression de graph <?php $paramsData['title'] ?>");
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                    });
                });
        });

        $(document).on("change", "#coform.<?php echo $id ?>", function(){
            updateInputList($(this).val());
        });

        let updateInputList = function(value){
            let childForm = <?php echo json_encode($formInputs) ?>;
            $("#path.<?php echo $id ?>").empty();

            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    if(input["type"].includes("multiCheckboxPlus")){
                        $("#path.<?php echo $id ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes("multiRadio")){
                        $("#path.<?php echo $id ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $id ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }
        }
    });
    
</script>