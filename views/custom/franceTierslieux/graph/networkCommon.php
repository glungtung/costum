<?php 

if($this->costum["contextType"] && $this->costum["contextId"]){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

//echo $size." -- ".$id." -- ";//.$graphtype;

?>

<style>
    #graph-container-<?= $id ?>{
        height: 100%;
        width: 100%;
        overflow: hidden;
    }
</style>

<?php
    if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
        $graphAssets = [
        '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
        ];
        
        HtmlHelper::registerCssAndScriptsFiles(
            $graphAssets,
            Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
        );
    }
?>

<div>
    <div id="search-container-<?= $id ?>" class="searchObjCSS" style='background-color:white!important'></div>
    <div id="graph-container-<?= $id ?>" class="graph-panel"></div>
</div>

<script>
    jQuery(document).ready(function() {
        contextData = <?php echo json_encode($el); ?>;
        var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
        var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
        var contextName = <?php echo json_encode($el["name"]); ?>;
        contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
    });
</script>

<script>
    var rawTags = "";
    var authorizedTags = []
    if(rawTags.trim() != ""){
        authorizedTags = rawTags.split(',');
    }
    var l<?= $id ?> = {
        container: "#search-container-<?= $id ?>",
        loadEvent: {
            default: "graph"
        },
        
        defaults: {
            types: ["organizations"],
            indexStep: 100
        },
    
        results: {
            dom: "#loader-container"
        },
    
        graph: {
            dom: "#graph-container-<?= $id ?>",
            authorizedTags: <?php echo $id ?>Data,
            defaultGraph: "network", // mindmap, relation , network
            //mindmapDepth: 1
        },
    
        filters: {
            text: true, 
        },
        
        header: {
            options : {}
        }
    };
  
  // l<?= $id ?>.defaults.forced = {
  //   filters: {
  //       "$or" : {}
  //   }
  //   }
  // var dataSource<?= $id ?> = <?= json_encode([]) ?>;
  // if(Object.keys(dataSource<?= $id ?>).length > 0){
  //   l<?= $id ?>.defaults["notSourceKey"] = true;
  //   for (const [id, value] of Object.entries(dataSource<?= $id ?>)) {
  //     l<?= $id ?>.defaults.forced.filters["$or"]["links.memberOf."+id] = {"$exists" : true};    
  //   }
  // }
  
    var p<?= $id ?> = {};
    
    setTimeout(() => {
        p<?= $id ?> = searchObj.init(l<?= $id ?>);
        p<?= $id ?>.graph.init(p<?= $id ?>);
        p<?= $id ?>.search.init(p<?= $id ?>);
        
        setTimeout(() => {
            p<?= $id ?>.graph.graph.initZoom();
        },200);
    
    }, 200)

</script>