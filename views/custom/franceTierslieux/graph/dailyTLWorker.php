<?php 
    $keyTpl     = "crossedDoubleProgressBar";
    $paramsData = [
        "coform" => "",
        "label" => "",
        "answerPath1" => "",
        "answerPath2" => "",
        "valueFontSize" => 35,
        "valueColor" => "#9B6FAC"
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

    $divider = 1;

    if(isset($paramsData["coform"])){
        $sum_query1 = array(
            '$group' => array(
                '_id'=> array("form"=>'$form'),
                "total" => array( 
                    '$sum'=> array(
                        '$multiply' => [
                            array('$toInt' => '$answers.'.$paramsData["answerPath1"]), 
                            array('$toInt' => '$answers.'.$paramsData["answerPath2"])
                        ]
                    )
                ),
                "count"=> array( '$sum' => 1 )
            )
        );


        $match_query1 = array(
                '$match' => array(
                    '$and' => array(
                        array("form" => $paramsData["coform"]),
                        array('answers.'.$paramsData["answerPath1"] => array('$regex' => new MongoRegex('/^(0|[1-9][0-9]*)$/'))),
                        array('answers.'.$paramsData["answerPath2"] => array('$regex' => new MongoRegex('/^(0|[1-9][0-9]*)$/')))
                    )
                )
            );
/*
        $match_query2 = array(
                '$match' => array(
                    '$and' => array(
                        array("form" => $paramsData["coform"]),
                        array('answers.'.$paramsData["answerPath2"] => array('$regex' => new MongoRegex('/^(0|[1-9][0-9]*)$/')))
                    )
                )
            );
*/
        try{
            $ca1 = PHDB::aggregate(Form::ANSWER_COLLECTION, array($match_query1, $sum_query1));
            
            if(isset($ca1["ok"]) && $ca1["ok"]==1.0 && isset($ca1["result"]) && isset($ca1["result"][0])){
                $value1 = $ca1["result"][0]["total"];
                $divider = $ca1["result"][0]["count"];
            }
        }catch(Exception $except){
            $message = var_dump($except);
        }
/*
        try{

            $ca2 = PHDB::aggregate(Form::ANSWER_COLLECTION, array($match_query2, $sum_query2));

            if(isset($ca2["ok"]) && $ca2["ok"]==1.0 && isset($ca2["result"])){
                    $value2 = $ca2["result"][0]["total"];

                }
        }catch(Exception $except){
            // var_dump($except);
        }*/
    }else{
        
    }

    $value = 0;

    if(isset($value1)){
        $value = $value1;
    }    
/*
    if(isset($value2)){
        $value*=$value2;
    }*/
?>

<style type="text/css">
     .textWithValue<?= $kunik; ?>{
        font-size: <?php echo $paramsData["valueFontSize"]; ?>pt;
        color: <?php echo $paramsData["valueColor"]; ?> !important;
        font-weight: bolder;
        margin-bottom: -20px;
        line-height: 1;
     }

 </style>

<span class="textWithValue<?= $kunik; ?> margin-top-10" ><?= round($value/$divider) ?></span>
                
<script type="text/javascript">
    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath1" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Question 1",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath1
                    },
                    "answerPath2" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Question 2",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath2
                    },
                    "valueFontSize": {
                        "inputType" : "text",
                        "rules":{"number":true},
                        "label" : "Taille du texte"
                    },
                    "valueColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du texte"
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath1.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath1+"']").length > 0){
                                $("#answerPath1.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath1);
                            }

                            if($("#answerPath2.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath2+"']").length > 0){
                                $("#answerPath2.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath2);
                            }
                        });
                    }
                    
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        let updateInputList = function(value, callback=null){
            let childForm = {};
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["coformInputs"]){
                childForm = costum["dashboardGlobalConfig"]["coformInputs"];
            }
            $("#answerPath1.<?php echo $kunik ?>").empty();
            $("#answerPath2.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(!input["type"].includes(".multiRadio") && !input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath1.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                        $("#answerPath2.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>
