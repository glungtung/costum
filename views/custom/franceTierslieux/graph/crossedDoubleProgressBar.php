<?php 
    $keyTpl     = "crossedDoubleProgressBar";
    $paramsData = [
        "coform" => "",
        "label" => "",
        "progressTitle" => "UN MODÈLE ÉCONOMIQUE HYBRIDE QUI REPOSE",
        "answerPath1" => "",
        "answerPath2" => "",
        "textOnProgressBarLeft" => "",
        "textOnProgressBarRight" => "",
        "progressBarHeight" => 35,
        "percentColor" => "white",
        "emptyColor" => "#D6CEF3",
        "completeColor" => "#9B6FAC",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<style type="text/css">
    .progress-contain<?= $kunik ?>{
        margin-top: <?= -3 + (35-$paramsData["progressBarHeight"]) ?>px;
    }
    .progress-contain<?= $kunik ?> .progress-bar {
        text-align: left;
        white-space: nowrap;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        cursor: pointer;
        height: <?= $paramsData["progressBarHeight"] ?>px;
        /*margin-top: <?= 35-$paramsData["progressBarHeight"] ?>px;*/
        margin-right: 6px;
        background-color: <?= $paramsData["emptyColor"] ?>;
    }
    .progress-contain<?= $kunik ?> .progress-in {
        font-size: 11pt;
        font-weight: bolder;
        padding-left: <?= (($paramsData["progressBarHeight"]<30)?0: 1) ?>em;
        margin-top: <?= - (31-$paramsData["progressBarHeight"]) ?>px;
        color: <?= $paramsData["percentColor"] ?>;
    }
    .progress-contain<?= $kunik ?> .progress-out {
        font-size: 11pt;
        font-weight: bolder;
        padding-left: <?= (($paramsData["progressBarHeight"]<30)?0: 1) ?>em;
        margin-top: <?= - (37-$paramsData["progressBarHeight"]) ?>px;
        color: #4E54C9;
    }

    .progress-contain<?= $kunik ?> .progress {
        background-color: <?= $paramsData["completeColor"]; ?>;
        height: <?= $paramsData["progressBarHeight"] ?>px;
        /*margin-top: <?= 35-$paramsData["progressBarHeight"] ?>px;*/
        border-radius: 2px;
    }

    .progress-contain<?= $kunik ?> .percentage-value{
        font-size: 18pt;
    }

    .btn-edit-delete-<?= $kunik ?>{
        position: absolute!important;
    }
</style>
<h2 class="sp-text" data-id="<?= $blockKey ?>" data-field="progressTitle"><?= $paramsData["progressTitle"] ?></h2>
<div class="progress-contain<?= $kunik ?> padding-0">
    <div class="mp-progress padding-0 mpprogress<?= $kunik ?>">
        <div class="progress progress<?= $kunik ?>">
            <div class="progress-out">
                <span class="percentage-value pvo<?= $kunik ?>"></span>
                <span>
                    <span class="sp-text" data-id="<?= $blockKey ?>" data-field="textOnProgressBarRight"><?= $paramsData["textOnProgressBarRight"] ?></span>
                </span>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {

        if(typeof costum["dashboardData"]!="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]){
            let {value1, value2} = costum["dashboardData"]["<?= $blockKey ?>"];
            if(typeof value1!="number" && typeof value1["$numberDecimal"]!="undefined"){
                value1 = parseFloat(value1["$numberDecimal"]);
            }

            if(typeof value2!="number" && typeof value2["$numberDecimal"]!="undefined"){
                value1 = parseFloat(value2["$numberDecimal"]);
            }

            $(".progress<?= $kunik ?>").prepend(`
                <div class="progress-bar" role="progressbar" aria-valuenow="${Math.round((value1*100)/(value1+value2))}" aria-valuemin="0" aria-valuemax="100" style="width: ${ Math.round((value1*100)/(value1+value2))}% !important;" data-toggle="tooltip" data-placement="top" title="${(value1)?Math.round((value1*100)/(value1+value2)):50 }%">
                <span class="sr-only">
                    ${ ((value1!=0)?''+Math.round((value1*100)/(value1+value2))+'% Complete':"") }
                </span>
                <div class="progress-in text-right padding-right-20">
                    <span>
                        <span class="sp-text" data-id="<?= $blockKey ?>" data-field="textOnProgressBarLeft"><?= $paramsData["textOnProgressBarLeft"] ?></span>
                    </span>
                    <span class="percentage-value">
                        ${ ((value1!=0)?Math.round((value1*100)/(value1+value2)):"0") } % 
                    </span>
                </div>
            </div>`);

            $(".pvo<?= $kunik ?>").text(Math.round((value2*100)/(value1+value2))+" %");

            <?php if($paramsData["withStaticTextBottom"]==true && $paramsData["withStaticTextBottom"]=="true"){ ?>

                $(".mpprogress<?= $kunik ?>").append(`
                    <div class="sp-text margin-top-10" style="margin-left:${((value1!=0)?(Math.round((value1*100)/(value1+value2))):"0")}% ;" data-id="<?= $blockKey ?>" data-field="label"><?= $paramsData["label"] ?></div>
                    `);
            <?php } ?>
        }

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath1" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Question 1",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath1
                    },
                    "answerPath2" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Question 2",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath2
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "emptyColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc rempli"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc vide"
                    },
                    "progressBarHeight": {
                        "inputType" : "text",
                        "rules":{"number":true},
                        "label" : "Couleur de l'arc vide"
                    },
                    "withStaticTextBottom": {
                        "inputType" : "checkboxSimple",
                        "label" : "Texte en bas",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath1.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath1+"']").length > 0){
                                $("#answerPath1.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath1);
                                $("#answerPath1.<?php echo $kunik ?>").change();
                            }

                            if($("#answerPath2.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath2+"']").length > 0){
                                $("#answerPath2.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath2);
                                $("#answerPath2.<?php echo $kunik ?>").change();
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath1.<?php echo $kunik ?>").empty();
            $("#answerPath2.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(!input["type"].includes(".multiRadio") && !input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath1.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                        $("#answerPath2.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
    
</script>
