<?php
	if (isset($name)) {
		$name = str_replace("#", "'", $name);
	}

	echo $this->renderPartial(
		"costum.views.custom.franceTierslieux.previewDataViz.previewSections", 
		array(
			"name" => $name, 
			"page" => $page, 
			"sections" => $sections, 
			"pathSectionTitle" => $pathSectionTitle
		)
	);

	if ($page == "tiersLieux") 
		echo $this->renderPartial(
			"costum.views.custom.franceTierslieux.previewDataViz.tiersLieuxPreview", 
			array(
				"name" => $name, 
				"image" => $image, 
				"orgaId" => $orgaId, 
				"id" => $id, 
				"network" => $network, 
			)
		);

	if ($page == "reseaux")
		echo $this->renderPartial(
			"costum.views.custom.franceTierslieux.previewDataViz.reseauPreview", 
			array(
				"name" => $name, 
				"markerData" => $markerData,
			)
		);
?>
