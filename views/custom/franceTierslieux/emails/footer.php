<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
		<tr style="padding: 0;vertical-align: top;text-align: left;">
			<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 15px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
				<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
					
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
						   <img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/footer-recensement.png" style="width:100%;text-align:center;display:inline-block;"/>
						</th>
					</tr>
				</table>

			</th>
		</tr>
		</table>
   <!-- End main email content -->

 <table class="container text-center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: center;background: #fefefe;width: 580px;margin: 0 auto;">
 	<tbody>
 		<tr style="padding: 0;vertical-align: top;text-align: left;">
 			<td style="word-wrap: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: left;color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 15px;border-collapse: collapse !important;"> <!-- Footer -->
			    <table class="row grey" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: #f0f0f0;width: 100%;position: relative;display: table;">
			    	<tbody>
			    		<tr style="padding: 0;vertical-align: top;text-align: left;">
			    			<th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
							<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
								<tr style="padding: 0;vertical-align: top;text-align: left;">
									<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
										<p class="text-center footercopy" style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 20px 0px;text-align: center;line-height: 19px;font-size: 12px;font-style: italic;"><?php echo Yii::t("mail","Mail send from") ?> <?php echo $urlRedirect; ?></p>
									</th>
									<th class="expander" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0 !important;margin: 0;text-align: left;line-height: 19px;font-size: 15px;visibility: hidden;width: 0;"></th>
								</tr>
							</table>
				        	</th>
				        </tr>
				    </tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>