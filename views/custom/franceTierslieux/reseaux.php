<?php 
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/js/franceTierslieux/answerDirectory.js"
    );
    $answerStep = "franceTierslieux2022023_753_10";
    $answerInput = "franceTierslieux2022023_753_10lemfa0njory1oxpszb";
    $getOptions = PHDB::distinct( Answer::COLLECTION,"answers.".$answerStep.".".$answerInput."",[ "form" => "63e0a8abeac0741b506fb4f7" ]) ?? [];
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( "costum" )->getAssetsUrl());
?>
<style>
	.ans-dir .bodySearchContainer, .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }
</style>

<div id="reseaux" class="container padding-bottom-20 padding-left-0 padding-right-0 ans-dir"></div>
<script>  
let initPagination = ""; 
$(function(){
    directory.PanelHtml = function(params){
        let organisationArrayId = [];
        let network = "";
        let networkName = "";
        $.each(params, function(e, v) {
            if ( !isNaN(e)) {
                networkName = networkName + v
            }
        })
        let image = (organisationArrayId.length != 0 && getNameOrUrl("organizations",organisationArrayId[0]) != "") ? getNameOrUrl("organizations",organisationArrayId[0]) : (exists(params?.answers?.aapStep1?.images) && exists(params.answers?.aapStep1?.images[0])) ? params.answers?.aapStep1?.images[0] : defaultImage
        var str = `
            <div class="team">
                <div class="card-team">
                    <div class="img-box">
                        <img class="img-responsive-action" alt="Responsive Team Profiles" src="${image}" />
                        <ul class="text-center">
                            <a href="#">
                                <li><i class="fa fa-facebook"></i></li>
                            </a>
                            <a href="#">
                                <li><i class="fa fa-twitter"></i></li>
                            </a>
                            <a href="#">
                                <li><i class="fa fa-linkedin"></i></li>
                            </a>
                        </ul>
                    </div>
                    <h1>
                        <a href="javascript:" onclick="showAllNetwork.loadPreview('','${image}','${networkName}','' ,'reseaux')" class="">
                            ${networkName}
                        </a>
                    </h1>
                    <h2>
                        <a href="javascript:" onclick='showAllNetwork.showModal("${networkName}")' style="text-decoration: none;" target="_blank">
                            Voir les tiers lieu
                        </a>
                    </h2>`;  
        
        str += `
                    </div>
            </div>
        `;
		// if (networkName == "Aucun")
        return str;
    }

    showAllNetwork.showModal = function (networkName) {
        const arrayToObject = (arr) => {
            return arr.reduce(function(obj, key) {
                obj[key] = key;
                return obj;
            }, {});
        };
        smallMenu.open(`<div id="tiers-modal" class="container padding-bottom-20 padding-left-0 padding-right-0 ans-dir"></div>`);
        $("#tiers-modal.ans-dir").html(showAllNetwork.initView(networkName,"searchModalFooter"));
        let modalParams = showAllNetwork.paramsFilters;
        initPagination = $(".reseauxFooter").html();
        modalParams.filters['networkFilter'] = {
            name : "filtre par réseau",
            view : "dropdownList",
            type : "filters",
            field : "answers.<?= $answerStep ?>.<?= $answerInput ?>",
            event : "filters",
            keyValue : false,
            list : arrayToObject(<?= json_encode($getOptions) ?>)
        }
        modalParams.results.renderView = "directory.modalPanel";
        modalParams['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7/filterOrga/true";
        modalParams.container = "#tiers-modal #filterContainercacs";
        modalParams.header.dom = "#tiers-modal .headerSearchIncommunity";
        modalParams.results.dom = "#tiers-modal .bodySearchContainer";
        searchObj.footerDom = "#tiers-modal .searchModalFooter";
        let modalFilter = searchObj.init(modalParams);
        modalFilter.search.init(modalFilter);
        setTimeout(() => { $('[data-key="'+networkName+'"]').trigger('click');}, 100);
    }
    showAllNetwork.paramsFilters.results.renderView = "directory.PanelHtml";
    showAllNetwork.paramsFilters.defaults.textPath = "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb";
    showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7/distinct/answers.<?= $answerStep ?>.<?= $answerInput ?>";
    searchObj.footerDom = ".reseauxFooter";
    $(".ans-dir").html(showAllNetwork.initView("","reseauxFooter"));
    let filter = searchObj.init(showAllNetwork.paramsFilters);
    filter.search.init(filter);
    $(".container-filters-menu").attr("style","height : 50px !important");
}) 
$('[data-dismiss="modal"]').click(function(){
    $(".reseauxFooter").html(initPagination);
})
</script>

<!-- <div class="row">
	<div class="col-xs-12">
		<div id='filterContainer' class='searchObjCSS'></div>
	</div>
	<div class="content-answer-search col-xs-12 col-md-12">
		<div class="headerSearchContainer col-xs-12 no-padding"></div>
		<div class="col-xs-12 bodySearchContainer margin-top-10">
			<div class="no-padding col-xs-12" id="dropdown_search"></div>
			<div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
		</div>
	</div>
	<div id="mapOfResultsAnsw" class="col-md-6" style="height:500px;"></div>
</div>

<script type="text/javascript">
	searchObject.types=["organizations","projects"];
	searchObject.indexStep=30;

	//definition des paramètres pour le filtre
	var paramsFilter= {
		container : "#filterContainer",
		urlData : baseUrl+"/costum/tierslieuxgenerique/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7",
		interface : {
			events : {
				page : true
			}
		},
		filters : {
			"text" : true
		},
		defaults:{
			notSourceKey : true,
			types:["answers"],
			// forced : {
            //         "filters" : {
            //             "costum.slug" : {
            //                 "$exists" : true
            //             }
            //         }
            //     }
		},

		header:{
			dom : ".headerSearchContainer",
			options:{
				left:{
					classes :"col-xs-12 no-padding",
					group:{
					count : true,
					map : true
				}
				}
			},
			views : {
				map : function(fObj,v){
				/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							'<i class="fa fa-map-marker"></i> '+trad.map+
						'</button>';*/
						return "";
			}
		},
		events : {
			map : function(fObj){
				$(".btn-show-map-search").off().on("click", function(){
					// $("#mapOfResultsAnsw").css({"left":"0px", "right":"0px", "bottom":"50px"});
					$("#mapOfResultsAnsw").show(200);
					// $(".footerSearchContainer").addClass("affix");
					// mapAnsw.map.invalidateSize();
					// $("body").css({"overflow":"hidden"});
					// $("#mapOfResultsAnsw .btn-hide-map").off().on("click", function(){
					// 	$("#mapOfResultsAnsw").hide(200);
					// 	$("body").css({"overflow":"inherit"});
					// 	$(".footerSearchContainer").removeClass("affix");	
					// });

				});
			}
		}
		}
	};

	var filterSearch={};

	$(function(){
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.header.set=function(fObj){
			var resultsStr = (fObj.results.numberOfResults > 1) ? " dossiers" : "dossier";
			if(typeof costum.hasRoles != "undefined" && $.inArray("Opérateur", costum.hasRoles)>=0  && !costum.isCostumAdmin)
				resultsStr+= " en attente d'opérateur";
			$(fObj.header.dom+" .countResults").html('<i class=\'fa fa-angle-down\'></i> ' + fObj.results.numberOfResults + ' '+resultsStr+' ');
		};

		filterSearch.results.render=function(fObj, results, data){
			mylog.log("govva",results);
			// if(Object.keys(results).length > 0){
			// 		ajaxPost(fObj.results.dom, baseUrl+"/survey/answer/views/tpl/costum.views.custom.costumDesCostums.viewCard", 
			// 		{allCostum:results},
			// 		function(){
			// 			fObj.results.events(fObj);
			// 		});
			// }else 
			// 	$(fObj.results.dom).append(fObj.results.end(fObj));
			// fObj.results.events(fObj);
			// fObj.results.addToMap(fObj, results);
		};

		filterSearch.results.addToMap = function(fObj, results){
			mylog.log("resultss",results);
			var elts = [];

			$.each(results, function(k, result){
				if(result.answers){
					$.each(result.answers, function(k, answer){
						if(answer.adress && answer.adress.geo)
						elts.push(
							{
								geo: answer.adress.geo,
								name: answer.proposition,
							}
						)
					})
				}
			})
			mylog.log("eltss",elts);
		};

		filterSearch.search.init(filterSearch);
	})

</script> -->


