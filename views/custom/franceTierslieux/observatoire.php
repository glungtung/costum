<style type="text/css">
    #banner {
        background: url(https://francetierslieux.fr/wp-content/uploads/2020/01/accueil-1920x1080-1.jpg) no-repeat fixed;
        background-size: cover;
        min-height: 50vh;
        position: relative;
    }

</style>
<div class="col-xs-12 no-padding">
    <div id="banner" class="banner no-padding">
        <div class="bg-color">
            <div class="row">
                <div class="banner-info">
                    <div class="banner-logo text-center">
                        <img src="http://communecter74-dev/upload/communecter/projects/5ecfad983fbbdf18008b4572/LOGOCommunecter.png" class="img-responsive">
                    </div>
                    <div class="banner-text text-center">
                        <h1 class="text-white">OBSERVATOIRE DES TIERS-LIEUX</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-xs-12 margin-top-50">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="cedarville-number">
                900
            </div>
            <div class="text-explain">
                TIERS-LIEUX
                RÉPONDANTS
                SOIT X% DES TIERS-LIEUX CARTOGRAPHIÉS
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 progress-contain">
            <div class="mp-progress">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">45% Complete</span>
                        <span class="progress-type">45%</span>
                    </div>
                </div>
                <div class="text-progress">
                    DES TIERS-LIEUX DANS LES 22 MÉTROPOLES
                </div>
            </div>

            <div class="mp-progress">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">55% Complete</span>
                        <span class="progress-type">55%</span>
                    </div>
                </div>
                <div class="text-progress">
                    DES TIERS-LIEUX HORS DES MÉTROPOLES
                </div>
            </div>

            <div class="mp-progress">
                <h4 class="red-ftl">1/3 DES QPV ONT 1 TIERS-LIEU</h4>
                <h5 class="blue-ftl">AU SEIN DU QUARTIER OU À MOINS D’1 KM
                    (531 QUARTIERS PRIORITAIRES DE LA POLITIQUE DE LA VILLE)
                </h5>
            </div>
            <div class="mp-progress">
                <div   class="col-xs-6">
                    <h4>
                        <span class="red-ftl">XX% </span>
                        <span class="blue-ftl">TIERS-LIEUX EN VILLES MOYENNES</span>
                    </h4>
                </div>
                <div   class="col-xs-6">
                    <h4>
                        <span class="red-ftl">XX% </span>
                        <span class="blue-ftl">TIERS-LIEUX EN MILIEU RURAL</span>
                    </h4>
                </div>

            </div>

        </div>
    </div>

    <div class="col-xs-12 no-padding bg-blue-ftl">
        <div class="activity-stat">
            <h4 class="text-white padding-bottom-20 ">
                LES GRANDS TYPES D’ACTIVITÉS
            </h4>
            <?php
            echo str_repeat(
            '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 progress-circle">
                        <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                            <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                            <div class="progress-value">90<span style="font-size: 15px">%</span> </div>
                        </div>
                        <div class="text-white text-center">
                            DE COWORKING
                        </div>
                    </div>
                    ', 6);
            ?>
        </div>


    </div>

    <div class="col-xs-12 padding-25">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="text-center number-header">150 000</div>
            <div class="text-explain2">
                PERSONNES Y TRAVAILLENT QUOTIDIENNEMENT
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="text-center number-header">2,2 M</div>
            <div class="text-explain2">
                DE PERSONNES sont venues dans un tiers-lieu pour Y RÉALISER DES PROJETS OU TRAVAILLER
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="text-center number-header">+ 4 M</div>
            <div class="text-explain2">
                DE PERSONNES ont assisté à UN ÉVÉNEMENT CULTUREL EN TIERS-LIEUX
            </div>
        </div>
    </div>

    <div class="col-xs-12 no-padding bg-blue-ftl">
        <div class="activity-stat">
            <h4 class="text-white padding-bottom-20 ">
                DIVERSITÉ DES TIERS-LIEUX SE RETROUVE DANS LES MODES DE GESTION
            </h4>
            <?php
            echo str_repeat(
                '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 progress-circle">
                        <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                            <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                            <div class="progress-value">90<span style="font-size: 15px">%</span> </div>
                        </div>
                        <div class="text-white text-center">
                            DE COWORKING
                        </div>
                    </div>
                    ', 4);
            ?>
        </div>

    </div>

    <div class="col-xs-12 no-padding">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="red-ftl padding-30">
                <span class="number-header red-ftl"> 248 </span>
                <span class="text-after">Millions d’€ de chiffre d’affaires cumulé (hors subvention)</span>
            </div>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            GRAPH DEV-CHRISTON
        </div>
    </div>

    <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-10  col-sm-12 col-xs-12 padding-20 text-center progress-contain">
        <div class="mp-progress">
            <h4 class=" blue-ftl ">
                UN MODÈLE ÉCONOMIQUE HYBRIDE QUI REPOSE
            </h4>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                    <span class="sr-only">45% Complete</span>
                    <span class="progress-type">45%</span>
                </div>
            </div>
        </div>
    </div>


</div>