<?php 
    $costumizeJSON = file_get_contents("../../modules/costum/data/costumize.json", FILE_USE_INCLUDE_PATH);
 ?>
<script>

	var costumizeJson = JSON.parse(<?php echo json_encode($costumizeJSON) ?>);
	var hasCostumJson = false;
	var defaultConfig = {
        slug : "costumize",
        htmlConstruct : {
            "header" : {
                "menuTop" : {
                    "left" : {
                        "buttonList" : {
                            "logo" : {
                                "height" : "50"
                            },
                            "xsMenu" : {
                                "buttonList" : {
                                    "app" : {
                                        "label" : true,
                                        "buttonList" : {}
                                    }
                                }
                            },
                            "app" : {
                                "label" : true,
                                "icon" : true,
                                "class" : "hidden-xs line-height-3 padding-left-10",
                                "labelClass" : "padding-left-5",
                                "buttonList" : {}
                            }
                        }
                    }
                }
            },
            "element" : {
	            "tplCss" : [ 
	                "v1.0_pageProfil.css"
	            ],
	            "banner" : "co2.views.element.bannerHorizontal",
	            "containerClass" : {
	                "centralSection" : "col-xs-12 col-lg-10 col-lg-offset-1"
	            },
	            "menuTop" : {
	                "class" : "col-xs-12",
	                "left" : {
	                    "class" : "col-lg-3 col-md-3 col-sm-4 hidden-xs no-padding",
	                    "buttonList" : {
	                        "imgUploader" : true
	                    }
	                },
	                "right" : {
	                    "class" : "col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center",
	                    "buttonList" : {
	                        "xsMenu" : {
	                            "class" : "visible-xs",
	                            "buttonList" : {
	                                "detail" : true,
	                                "community" : true,
	                                "gallery" : true,
	                                "events" : true,
	                                "classifieds" : true,
	                                "newspaper" : true
	                            }
	                        },
	                        "detail" : true,
	                        "community" : true,
	                        "gallery" : true,
	                        "events" : true,
	                        "classifieds" : true,
	                        "newspaper" : true
	                    }
	                }
	            }
	        }
        }
    };

	<?php 
		$resultQuery = Costum::getBySlug("costumize");
		if(!empty($resultQuery)){
	 ?>
	 hasCostumJson = true;
	<?php } ?>

	 $(".costumize").on("click",function() {
	 	var costumizeSlug = $(this).data("costumize-slug");
	 	costumize(costumizeSlug);
	 });

	function costumize(costumizeSlug=null,defaultCms=null){
	  if(exists(contextData) && exists(contextData.costum) && exists(contextData.costum.slug)){
	      bootbox.confirm({
	        title: "<h5 class='text-center letter-green-k'><i><u>"+contextData.name+"</u></i> a déja un costum </h5>",
	        message: "Vous allez être redirigé vers l'interface de COstumisation de <i><u>"+contextData.name+"</u></i> !",
	        buttons: {
	            cancel: {
	                label: '<i class="fa fa-times"></i> '+trad.cancel
	            },
	            confirm: {
	                label: '<i class="fa fa-check"></i> Ok'
	            }
	        },
	        callback: function (result) {
	            if(result)
	              window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug;
	        }
	    });    
	  }else{
	  	//alert(costumizeSlug);
	  		if(costumizeSlug){
		  		ajaxPost(
			        null,
			        baseUrl+"/costum/costumgenerique/getJson",
			        {slug:costumizeSlug},
			        function(data){ 
			          	mylog.log("jsonFile",data);
			          	costumizeJson=data;
			  		}

	  			);
	  		tplCtx = {};
			tplCtx.id = contextData.id;
			tplCtx.collection = contextData.collection;
			tplCtx.path = "costum.slug";
			tplCtx.value =costumizeSlug;	
					
	  		dataHelper.path2Value( tplCtx, function(params) { 
	                        customParams();
	                    } );
	  		
			

	  		}
	  		else{
 				customizeCostum(defaultConfig,defaultCms);
 			}	
	  	}
	}


	function customParams(){
		var sectionDyf={};
		sectionDyf.colorParams = {
	        "jsonSchema" : {    
	            "title" : "Couleurs principales",
	            "icon" : "fa-cog",
	            "properties" : {
	                "main1" : {
	                    label : "Couleur principale",
	                    inputType : "colorpicker"
	                },
	                "main2" : {
	                    label : "Couleur secondaire",
	                    inputType : "colorpicker"
	                }
	            },
	            save : function (data) {
	            	 mylog.log("color",data);
	            	 tplCtx.value={};
	            	 $.each( sectionDyf.colorParams.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        mylog.log("value",'.'+k+val.inputType,tplCtx.value[k]);
                    });
                    mylog.log("save color tplCtx",tplCtx);
	                if(typeof tplCtx.value == "undefined")
	                    toastr.error('value cannot be empty!');
	                else {
	                    dataHelper.path2Value( tplCtx, function(params) { 
	                        paramsCat();
	                    } );
	                }

	            }
	        }
    	};
		tplCtx = {};
		tplCtx.id = contextData.id;
		tplCtx.collection = contextData.collection;
		tplCtx.path = "costum.colors";
        dyFObj.openForm( sectionDyf.colorParams,null, {collection:"organizations"});
    }

    function paramsCat(){
		var sectionDyf={};
		sectionDyf.catParams = {
	        "jsonSchema" : {    
	            "title" : "Réseau thématique",
	            "icon" : "fa-cog",
	            "properties" : {
	            	"info" : {
						"inputType" : "custom",
						"html":"<p class='text-"+typeObj["reseau"].color+"'>"+
						//"Faire connaître votre Organisation n'a jamais été aussi simple !<br>" +
						"Si votre réseau de tiers-lieux regroupent des structures spécialisées dans une ou plusieures thématiques...<hr>" +
					 "</p>",
					},
	                "themes" : {
	                    "label" : "Thématiques de votre réseau",
	                    "inputType" : "select",
	                    "placeholder":"Choisir la(les) thématique(s)",
                        "list":"typePlace",
                        "groupOptions":false,
                        "groupSelected":false,
                        "optionsValueAsKey":true,
                        "select2":{
                           "multiple":true
                        }  
	                }
	            },
	            save : function (data) {
	            	 mylog.log("categorie",data);
						tplCtx.value={};
                        tplCtx.value = data.themes;
                        if(typeof tplCtx.value=="string"){
                        	tplCtx.value=tplCtx.value.split();
                        }
                        //var valLen=Object.keys(data.themes).length;
                        mylog.log("Themes to save",data.themes)
                        var urlTags=data.themes.toString();
                        mylog.log("tags url",urlTags);
	                if(typeof tplCtx.value == "undefined")
	                    toastr.error('value cannot be empty!');
	                else {
	                    tplCtx.updatePartial=true;
	                     //tplCtx.arrayForm = true;
	                    dataHelper.path2Value( tplCtx, function(params) { 
	                        $("#ajax-modal").modal('hide');
	                        toastr.success("Bien ajouté");
	                         window.location.href = baseUrl+'/costum/co/index/slug/'+data.slug+'#?tags='+urlTags;
	                    } );
	                }
	            }
	        }
    	};
		tplCtx = {};
		tplCtx.id = contextData.id;
		tplCtx.collection = contextData.collection;
		tplCtx.path = "tags";
		 if(typeof contextData.map.data[contextData.id].tags!="undefined"){
		 	var themeTags = [];
		 	$.each(contextData.map.data[contextData.id].tags,function(i,v){
		 		if(costum.lists.typePlace.indexOf(v)>-1){
		 			themeTags.push(v);
		 		}
		 	});
		 	mylog.log("themeTags",themeTags);
		}
        dyFObj.openForm(sectionDyf.catParams,null, {themes:themeTags});


    }		

	function customizeCostum(defaultConfig,defaultCms){
		bootbox.confirm({
	          title: "<h5 class='text-center letter-green-k'><i><u style='text-transform:capitalize'>'"+contextData.name+"'</u></i> n'a pas encore un costum </h5>",
	          message: "<h6 class='text-center'>Veuillez créer votre costum !</h6>",
	          buttons: {
	              cancel: {
	                  label: '<i class="fa fa-times"></i> '+trad.cancel
	              },
	              confirm: {
	                  label: '<i class="fa fa-check"></i> Créer'
	              }
	          },
	          callback: function (result) {
	              if(result){
	                var tplCtx = {};
                    var defaultCostumizeObj = {
                        costum: defaultConfig
                    };

                    mylog.log("defaultCostumizeObj",defaultCostumizeObj);
	                tplCtx.id = contextData.id;
	                tplCtx.collection = contextData.collection;
	                tplCtx.path ="allToRoot";
	                tplCtx.value = defaultCostumizeObj;
	                tplCtx.format = true;
	                dataHelper.path2Value(tplCtx,function(response){
	                    mylog.log("valiny",response);
	                    $("#ajax-modal").modal('hide');
	                    if(response.result){
							if(notNull(defaultCms))
								addDefaultCms(defaultCms);
	                     	else{
								toastr.success("Votre costum est créé");
	                      		window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug;
							}
	                    }else{

	                    }
	                })
	              }
	          }
	    });
	}

function getRandomString(length) {
    var chars = "ABCDEFabcdef0123456789";
    var randS = "";
    while(length > 0) {
        randS += chars.charAt(Math.floor(Math.random() * chars.length));
        length--;
    }
    return randS;
}

function addDefaultCms(params){
	ajaxPost(
		null,
		baseUrl+"/co2/cms/insert",
		params,
		function(data){
			if(data.result){
				toastr.success("bloc cms par défaut ajouté");
				toastr.success("Votre costum est créé");
				window.location.href = baseUrl+'/costum/co/index/slug/'+contextData.slug;
			}
				
			else
				toastr.error("bloc cms par défaut non ajouté")
		}
	)
}
</script>