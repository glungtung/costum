<style>

</style>

<div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-xs-12">
    <hr class="hr-ftl">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 banner-metier">
        <div class="red-ftl title-page">LES MÉTIERS DES TIERS-LIEUX</div>
        <div class=" dp-flex">
            <span class="number-header blue-ftl"> 69% </span>
            <span class="text-after blue-ftl">
                DES TIERS-LIEUX ONT UN FACILITATEUR, VÉRITABLE PILOTE DU PROJET ET ANIMATEUR DU LIEUX. <br>
                MÉTIER ESSENTIEL AU FONCTIONNEMENT DES TIERS-LIEUX, IL PEUT ÊTRE APPUYÉ PAR DIVERSES FONCTIONS POUR.
            </span>
        </div>
        <div class="row margin-left-5 margin-top-15">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 div-col blue-ftl">
                 <h1>43%</h1>
                <h5>PAR UN CHARGÉ DE COMMUNICATION </h5>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 div-col blue-ftl">
                <h1>40%</h1>
                <h5>PAR UN CHARGÉ D'ACCUEIL </h5>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 div-col blue-ftl">
                <h1>31%</h1>
                <h5>PAR UN CHARGÉ D'ANIMATION </h5>
            </div>
        </div>

    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="title-header red-ftl">
            6300
        </div>
        <div class="subtitle-header blue-ftl">
            PERSONNES ANIMENT ET GÈRENT LES TIERS-LIEUX
        </div>
    </div>
    <hr class="hr-ftl col-xs-12">

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="subtitle-text">
            <span class="red-ftl">
                COMMENT PARTICIPENT LES UTILISATEURS <br>
            </span>
            <span class="blue-ftl">
                AU PROJET DU TIERS-LIEU ?
            </span>
        </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 progress-contain">
        <div class="mp-progress">
            <div class="dp-flex">
                <div class="textbefore-progress">
                    PARTICIPATION A L'ANIMATION
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">77% Complete</span>
                        <span class="progress-type">45%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    GESTION DU LIEUX
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">45% Complete</span>
                        <span class="progress-type">45%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    PARTICIPATION A LA GOUVERNANCE
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">40% Complete</span>
                        <span class="progress-type">40%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    ACCUEIL DES AUTRES UTILISATEURS
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="52" aria-valuemin="0" aria-valuemax="100" style="width: 52%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">52% Complete</span>
                        <span class="progress-type">52%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    TUTORAT, COUP DE MAIN ETC ....
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">62% Complete</span>
                        <span class="progress-type">62%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
            <div class="textbefore-progress">
                AUCUN PARTICIPATION
            </div>
            <div class="progress bg-white prgress-flex">
                <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                    <span class="sr-only">62% Complete</span>
                    <span class="progress-type">62%</span>
                </div>
            </div>
        </div>

        </div>
    </div>

    <hr class="hr-ftl col-xs-12">

    <div class="col-xs-12 no-padding">
        <h3 class="blue-ftl">
            PRINCIPALEMENT LIEU DE TRAVAIL, <br>
            LES PERSONNE FRÉQUENTANT LES TIERS-LIEUX SONT EN MAJORITÉ :
        </h3>
        <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/profils.png">
    </div>

</div>
<div class="col-xs-12 no-padding" style="background-color: #9B6FAC;">
    <div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-xs-12 no-padding" >
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div style="padding-top: 50px;padding-bottom: 50px;">
                <span class="red-ftl subtitle-header">AU CŒUR D’ÉCOSYSTÈMES LOCAUX <br></span>
                <span class="text-white second-subtitle">
                    ACTIVATEURS DE DYNAMIQUES LOCALES, LES TIERS-LIEUX SONT EN LIEN AVEC UNE GRANDE DIVERSITÉ D’ACTEURS DE LEUR TERRITOIRE
                </span>
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 progress-contain bg-white margin-top-10">
        <?php
        echo str_repeat(
            '<div class="dp-flex">
                <div class="textbefore-progress">
                    ACTEUR DE LA TRANSITION ECOLOGIQUE
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">62% Complete</span>
                        <span class="progress-type">62%</span>
                    </div>
                </div>
            </div>
                    ', 10);
        ?>

        </div>
    </div>
</div>

<div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-xs-12 padding-top-30" >
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="blue-ftl text-foot">
            LES TIERS-LIEUX ACCOMPAGNENT À LA CRÉATION D'ACTIVITÉ
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stat-footer">
        <div class="dp-flex activity-stat">
            <div class="progress-circle">
                <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                    <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                    <div class="progress-value bg-white blue-ftl">90<span style="font-size: 15px">%</span> </div>
                </div>
            </div>
            <div class="blue-ftl" style="margin-top: auto; margin-bottom: auto; padding-left: 10px;">
                ONT DES SERVICE D'INCUBATION
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stat-footer">
        <div class="dp-flex activity-stat">
            <div class="progress-circle">
                <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                    <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                    <div class="progress-value bg-white blue-ftl">90<span style="font-size: 15px">%</span> </div>
                </div>
            </div>
            <div class="blue-ftl" style="margin-top: auto; margin-bottom: auto; padding-left: 10px;">
                ONT UNE PÉPINIÈRE D'ENTREPRISE
            </div>
        </div>
    </div>

</div>

