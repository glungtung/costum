<style>
    .progress-contain .actor-progress .textbefore-progress {
        color: #ffffff;
    }
    .progress-contain .actor-progress .val-progress {
        background-color: #ffffff;
    }
    .progress-contain .actor-progress .progress {
        background-color: #24577C;
    }
    .progress-contain .actor-progress .progress-bar {
        color: #4623C9;
    }
    .orange-ftl {
        color: #D62F2C;
    }
</style>
<div class="col-xs-12 no-padding">
    <div class="number-header" style="color: #783469">
        MOTEURS DE LA RELOCALISATION DE LA PRODUCTION
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="text-explain blue-ftl">
                49%<br>
                DES TIERS-LIEUX SONT ACTEURS DE LA FABRICATION LOCAL <br>
            </div>
            <div class="second-subtitle blue-ftl">
                (FABLABS ET ATELIERS ARTISANAUX PARTAGÉS)
            </div>
            <div class="col-xs-12 margin-top-20 activity-stat competence-stat">
                <h3 class="blue-ftl text-center padding-bottom-20">
                    LES 3 PRINCIPES ACTIVITÉS DE PRODUCTION
                </h3>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 progress-circle">
                    <div class="progress blue">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                        <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                        <div class="progress-value">90<span style="font-size: 15px">%</span> </div>
                    </div>
                    <div class="blue-ftl text-center">
                        L'IMPRESSION 3D
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 progress-circle">
                    <div class="progress blue">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                        <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                        <div class="progress-value">90<span style="font-size: 15px">%</span> </div>
                    </div>
                    <div class="blue-ftl text-center">
                        LA RÉPARATION ET LE RÉEMPLOI
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 progress-circle">
                    <div class="progress blue">
                    <span class="progress-left">
                        <span class="progress-bar"></span>
                    </span>
                        <span class="progress-right">
                        <span class="progress-bar"></span>
                    </span>
                        <div class="progress-value">90<span style="font-size: 15px">%</span> </div>
                    </div>
                    <div class="blue-ftl text-center">
                        MENUISIER ET L'ARTISANAT D'ART
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 progress-contain">

            <h4 class="text-center" style="color: #AC9B6F">
                QUELS TYPES DE PRODUCTION S'OPERENT AU SEIN TIERS-LIEUX
            </h4>
            <div class="mp-progress">
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        PARTICIPATION A L'ANIMATION
                    </div>
                    <div class="progress bg-white prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">77% Complete</span>
                            <span class="progress-type">45%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        GESTION DU LIEUX
                    </div>
                    <div class="progress bg-white prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">45% Complete</span>
                            <span class="progress-type">45%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        PARTICIPATION A LA GOUVERNANCE
                    </div>
                    <div class="progress bg-white prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">40% Complete</span>
                            <span class="progress-type">40%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        ACCUEIL DES AUTRES UTILISATEURS
                    </div>
                    <div class="progress bg-white prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="52" aria-valuemin="0" aria-valuemax="100" style="width: 52%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">52% Complete</span>
                            <span class="progress-type">52%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        TUTORAT, COUP DE MAIN ETC ....
                    </div>
                    <div class="progress bg-white prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">62% Complete</span>
                            <span class="progress-type">62%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        AUCUN PARTICIPATION
                    </div>
                    <div class="progress bg-white prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">62% Complete</span>
                            <span class="progress-type">62%</span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="col-xs-12" style="background-color:#24577C">
        <div class="text-explain text-white padding-10">
            ACTEURS DE LA CRÉATION ET DIFFUSION DES PRATIQUES ARTISTIQUES ET CULTURELLES
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <div class="text-white text-center">
                <span class="number-header red-ftl"> 1/3 </span>
                <span class="text-after">DES TIERS-LIEUX DÉVELOPPENT DES ACTIVITÉS ARTISTIQUES ET CULTURELLES</span>
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 progress-contain">
            <div class="second-subtitle text-white text-center">
                LES ACTIVITÉS ARTISTIQUES ET CULTURELLES QU’ILS ACCUEILLENT
            </div>


            <div class="mp-progress actor-progress">
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        PARTICIPATION A L'ANIMATION
                    </div>
                    <div class="progress prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">77% Complete</span>
                            <span class="progress-type">45%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        GESTION DU LIEUX
                    </div>
                    <div class="progress prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">45% Complete</span>
                            <span class="progress-type">45%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        PARTICIPATION A LA GOUVERNANCE
                    </div>
                    <div class="progress prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">40% Complete</span>
                            <span class="progress-type">40%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        ACCUEIL DES AUTRES UTILISATEURS
                    </div>
                    <div class="progress prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="52" aria-valuemin="0" aria-valuemax="100" style="width: 52%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">52% Complete</span>
                            <span class="progress-type">52%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        TUTORAT, COUP DE MAIN ETC ....
                    </div>
                    <div class="progress prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">62% Complete</span>
                            <span class="progress-type">62%</span>
                        </div>
                    </div>
                </div>
                <div class="dp-flex">
                    <div class="textbefore-progress">
                        AUCUN PARTICIPATION
                    </div>
                    <div class="progress prgress-flex">
                        <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                            <span class="sr-only">62% Complete</span>
                            <span class="progress-type">62%</span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 ">
        <div class="orange-ftl padding-30">
            <span class="subtitle-header"> DES ESPACE D'ACCUEIL SANS DISTINCTION DES PUBLICS<BR>
            <span class="second-subtitle"">OUVERTS A TOUS, INCLUSIFS, FAVORISANT LA MICTÉ SOCIALE ET L'ACCOMPAGNEMENT DE TOUS LES PUBLICS </span>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  stat-footer">
            <div class="dp-flex activity-stat">
                <div class="progress-circle">
                    <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value bg-white blue-ftl">90<span style="font-size: 15px">%</span> </div>
                    </div>
                </div>
                <div class="blue-ftl second-subtitle" style="margin-top: auto; margin-bottom: auto; padding-left: 10px;">
                    DES TIERS-LIEUX ONT TRAVAILLÉ SUR DES PROBLÉMATIQUES SOCIALES SPÉCIFIQUES
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <span class="number-header red-ftl"> 7,4% </span><br>
            <span class="second-subtitle blue-ftl">DES TIERS-LIEUX ONT UN AGREMENT CAF (CAISSE D’ALLOCATIONS FAMILIALES) EN TANT QUE CENTRE SOCIAL OU ESPACE DE VIE SOCIALE</span>
        </div>
    </div>



</div>