<?php
	$countMenu = 0;
	// echo "<pre>";
	// echo var_export( $sections, true) ;
	// echo "</pre>";
?>
<style>
    .vertical-menu {
		padding-right: 0px !important;
	}
	.vertical-menu ul li {
		margin-bottom: 15px;
	}
	.vertical-menu ul li a{
		text-decoration: none;
	}
	/* .listLeftBtn .active a {
		border-left: solid 4px ;
    	padding-left: 12px ;
		color: black !important;
	} */
	.listLeftBtn .active a {
		border-left: solid 4px ;
    	padding-left: 12px ;
		color: #fff !important;		
	}

	.listLeftBtn a[aria-expanded="false"] {
		text-decoration: none;
		/* color: #737f8b; */
		color: #b1b4b7;
	}
	.tab-content {
		overflow-y: scroll; 
		height: 90vh;
	}

	/* dark */
	.tab-content {
		/* background: #404040 !important; */
	}

	/* .content-img-profil-preview {
		margin-top: 61.5px;
	}
	.img-responsive {
		margin: auto;
		box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);
	} */
	.listLeftBtn {
		list-style: none; 
		padding-left: 0px;
		height: 85vh;
		overflow: auto;
		overflow-x: auto;
	}
	/* .toolsMenu {
		background: #e3e3e3;
	} */

	/* dark */
	.toolsMenu {
		/* background: #1C0C5B; */
		background: #4A6FA5;
		color: white;
	}

	.enlarge {
		top:0px !important; 
		left:0px !important
	}

	/* dark */
	.vertical-menu {
		/* background: #270082; */
		background: #394648;
		/* color: white !important; */
	}

	.goToItemPage {
		/* background: #4623C9 !important; */
		background: #8AC4C5;
	}
	/* .tab-pane {
		margin-bottom: 200px !important;
	} */
</style>

<div class="col-xs-12 padding-10 preview-answers toolsMenu">
	
		<!-- <a href="javascript:" class="btn btn-primary full-width"> Agrandir </a> -->
	
		<div class="col-md-6 title-tiers">
			<h4><?= ($name == "Aucun") ? "Les tiers-lieux qui n'ont pas de réseaux" : $name ?></h4>
		</div>
		<button class="btn btn-default close-preview-answers pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <button class="btn btn-default full-width margin-right-10 pull-right">
            <i class="fa fa-window-maximize"></i>
        </button>
		<?php 
			if ($page == "tiersLieux") 
				echo '<a href="javascript:" class="goToItemPage lbh btn btn-primary pull-right margin-right-10">Aller sur la page</a>';
		?>
</div>
<div class="form-answer-content" ></div>
<div class="col-xs-12 vertical-menu">
	<div class="col-xs-3 margin-top-20">
		<ul data-ref="" class="listLeftBtn">
			<?php foreach ($sections as $key => $value) {
				$aria = ($countMenu == 0) ? "true" : "false";
				if (gettype($value) != "array")
					echo '<li><a data-toggle="tab" id="section-'.$key.'" href="#menu'.$countMenu.'" class="section-button" aria-expanded="'.$aria.'">'.$value.'</a><li>';
				else {
					$stepPattern = '/^(?:.*?)(franceTierslieux[\d_]+)(?:.*)$/';
					preg_match($stepPattern, $key, $matches);
					$path = (isset($matches) && isset($matches[0]) && isset($matches[1])) ? "answers.".$matches[1].".".$matches[0] : "";
					$label = $pathSectionTitle[$path] ?? $key;
					// var_dump($value);
					echo '<li><a data-count="'.$countMenu.'" data-sub="sub-'.$countMenu.'" style="cursor: pointer;" class="margin-bottom-15 parent-tab " aria-expanded="false"> ' . $label . ' </a>';
					 // ' . (isset($pathSectionTitle))? $pathSectionTitle[$path] : $key . '
					echo '<ul style="list-style: none;" data-ref="sub-'.$countMenu.'" class="sub-menu-section sub-'.$countMenu.' '.str_replace(["'", " "], "_", $label).'" hidden>';
						foreach ($value as $subKey => $subValue) {
							if ($path != "") {
								if (isset($subValue) && $subValue > 0) {
									echo '<li><a data-toggle="tab" class="filter-map-section"  data-title="'. $label .'" data-key="'.$subKey.'" data-path="'.$path.'" href="#menu'.$countMenu.'" aria-expanded="false">'.$subKey.' ('.$subValue.') </a><li>';
								}			
							} else {
								echo '<li><a data-toggle="tab" href="#menu'.$countMenu.'" aria-expanded="false">'.$subValue.'</a><li>';
							}
							$countMenu ++;
						}
					echo '</ul>';
					echo '<li>';
				}
				$countMenu ++;
			}?>
		</ul>
	</div>

  <div class="col-xs-9 tab-content container" style="padding-left: 30px; padding-right: 0px !important;">
		<?php 
			$countMenu = 0;
			foreach ($sections as $key => $value) {
				$activeContent = ($countMenu == 0) ? "active in" : "";
				if (gettype($value) != "array") {
					echo '<div id="menu'.$countMenu.'" class="tab-pane fade '.$activeContent.'">';
					echo '<div id="'.$key.'" class="col-xs-10  no-padding margin-bottom-20 margin-top-20"></div>';
					echo '</div>';
				} else {
					foreach ($value as $subKey => $subValue) { 
						echo '<div id="menu'.$countMenu.'" class="tab-pane fade '.$activeContent.'">';
						echo '<div id="'.$subKey.'" class="col-xs-10  no-padding margin-bottom-20 margin-top-20"></div>';
						echo '</div>';
						$countMenu ++;
					}
				}
				
				$countMenu ++;
		}?>
  </div>
</div>

<script>
	
	$("a.parent-tab").click(function(){
		let ulClass = $(this).data("sub");
		let count = $(this).data("count");
		let delay = 150;
		if ($("." + ulClass).is(':hidden')) {
			$("." + ulClass).show();
		} else {
			$("." + ulClass).hide();
		}
		$(".listLeftBtn  .active a").attr("aria-expanded", "false");
		$(".listLeftBtn  .active ").attr("class", "");
		// $("[href='#menu"+count+"']").trigger("click");
		setTimeout(function() {
			$("[href='#menu"+count+"'], .filter-map-section [href='#menu"+count+"']").trigger('click');
		}, delay);
	})

	if($("#modal-preview-coop").attr("class").includes("enlarge")){
		$(".preview-answers").find(".full-width")
			.html('<i class="fa fa-window-restore"></i>')
			.addClass("initial-width")
			.removeClass("full-width");
	}	

	// $(".full-width").click(function () {
	$(".preview-answers").on('click','.full-width', function() { 
		$("#modal-preview-coop").addClass("enlarge")
		$(this)
			.html('<i class="fa fa-window-restore"></i>')
			.addClass("initial-width")
			.removeClass("full-width");
	})

	// $(".initial-width").on('click',function () {
	$(".preview-answers").on('click', ".initial-width", function() { 
		$("#modal-preview-coop").removeClass("enlarge")
		$(this)
			.addClass("full-width")
			.html('<i class="fa fa-window-maximize"></i>')
			.removeClass("initial-width")
	})

</script>