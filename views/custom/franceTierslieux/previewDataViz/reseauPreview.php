<?php 
    $cssAnsScriptFilesModuleMap = array(
        '/leaflet/leaflet.curve.js'
    );
    $cssAnsScriptFiles = array(
        // "/css/franceTierslieux/answersDirectory.css",
        "/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
        "/js/franceTierslieux/dataviz/radarChart.js",
		// "/js/franceTierslieux/dataviz/negativePositiveBar.js",
		"/css/franceTierslieux/radarChart.css",
    );
    $cssAnsScriptFilesTheme = array(
		'/plugins/jQCloud/dist/jqcloud.min.js',
		'/plugins/jQCloud/dist/jqcloud.min.css',
		"/plugins/Chart.js/Chart.v4.js",
        "/plugins/d3/d3.v3.min.js"
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl());

    $countTiers = PHDB::count(Answer::COLLECTION, array("form"=> "63e0a8abeac0741b506fb4f7", "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" => ($name == "") ? 0 : $name ));
    $allTiersLieux = PHDB::count(Answer::COLLECTION, array("form"=> "63e0a8abeac0741b506fb4f7"));
    $tiersLieuxPercent = ($countTiers * 100) / $allTiersLieux;
?>
<style>
    .show-map {
        height: 50vh;
    }
    .tab-content {
        overflow-y: inherit;
        padding-left: 0px !important;
        padding-right: 0px !important;
        background: #f9f7f7;
        height: 95vh;
    }
    .container-filters-menu {
        background-color: inherit;
    }
    #filterContainer .dropdown .btn-menu {
        border-radius: inherit;
    }

    .image-network-content {
        display: flex;
        justify-content: center;
        margin-left: 50px;
        margin-bottom: 10%;
    }

    .stat-content, .word-cloud-content {
        display: flex;
        justify-content: center;
    }

    #distValue {
        height: 85vh;
        overflow-y: auto;
        width: auto;
    }

    /* #wordCloud {
        margin-left: 15vh;
        margin-top: 50px;
    } */

    .stat-network {
        overflow-y: auto;
        height: 90vh;
    } 

    .ans-dir .bodySearchContainer, .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }

</style>
<script>
    $(function() {
        let name = "<?= $name ?>";
        // alert(Object.keys(orgaObject).length)
        let markersAndSuplForms = showAllNetwork.getAnswers ('/form/63e0a8abeac0741b506fb4f7/getMarkers/true', showAllNetwork.markerParams(name));
        let orgaObject = markersAndSuplForms['markers'];
        // ['markers'];
        // showAllNetwork.getAnswers ('/form/63e0a8abeac0741b506fb4f7/getMarkers/true', showAllNetwork.markerParams)['supplementAnswer'];
       
        let economicModelObject = {
            distributionValue : {
                id: "distValue",
                path : { 
                    Nombre_d__équipement : "answers.lesCommunsDesTierslieux1052023_949_0.multiCheckboxPluslesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw",
                    Pourcentage_de_production : "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3onic3xd6dvbjzxu",
                    Pourcentage_activité : "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq"
                },
                title : "Distribution de valeur"
            },
            valueCreation : {
                id: "createValue",
                path : { 
                   valueDistribution: "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl16ek1pd92s71wm8"
                },
                title : "Création de valeur"
            },
            captureValue : {
                id: "captValue",
                path : {
                    valuecapturePath: "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr"
                },
                title : "Capture de valeur"
            },
            resume : {
                id: "resume",
                path : {
                    benef : "answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu50gd93x5x1uva3xv",
                    amount : "answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu51awl25edd4zlmsp"
                },
                title : "Résumé"
            }

        }
        let preview = {
            init : function () {
                preview.views.load();
                preview.actions.load();
                preview.events.load();
            },
            events : {
                load : function () {
                    $(".filter-map-section").click(function() {
                        $("#activityLinkMap").show();
                        let sectionPath = $(this).data("path");
                        let sectionName = $(this).data("key");
                        let menuReference = $(this).attr("href");
                        let title = $(this).data("title");
                        let filterValue = {};
                        let renderHtml = `
                        <div class="margin-left-20 margin-top-20">
                            <h4>Voici la cartographie qui affiche les tiers-lieux du ${name} :</h4><br>
                            <h5 style="text-align: center; padding-bottom: 15px; margin-bottom: 10px;">${title} : ${sectionName}</h5>
                        </div>
                        `;
                        $(".tab-content " + menuReference).html(renderHtml);
                        preview.actions.filterMap({}, "", {[sectionPath] : {"$exists": sectionName}});
                    })

                    $("#section-reseaux").click(function () {
                        $("#activityLinkMap, .sub-menu-section").hide();
                        $("#barChartresume").hide();
                    })

                    $(".sub-menu-section li a").click(function () {
                        $(".canvasContent").hide();
                    })

                    $(".Modèle_économique li a").click(function () {
                        $("#activityLinkMap").hide();
                    })

                    $("a[href='#"+$("#resume").parent().attr("id")+"']").click(function() {
                        $(".canvasContent").fadeIn();
                    })

                    $("#distValue, #captValue, #createValue").removeClass("col-xs-10");
                }
            },
            views : {
                load : function () {
                    // preview.views.sections.showActivityLink();
                    // preview.views.sections.showValueLink();
                    // preview.views.sections.showProductionLink();
                    // preview.views.sections.showEquipment();
                    preview.views.sections.load();
                    preview.views.map();
                },
                emptyContent : function () {
					return `
                        <div class="col-md-offset-1">
                            <p>
                                Les tiers lieux n'ont pas remplient les formulaires supplementaires pour visualiser ces données.
                                En remplissant le formulaire, vous nous permettez de disposer d'une vision plus complète de la situation, ce qui nous aide à prendre des décisions éclairées pour l'amélioration de ces espaces. Votre participation contribue également à créer une communauté solidaire et à favoriser l'échange de connaissances entre les utilisateurs. <br>
                            </p>                        
                        </div>
					`;
				},
                map : function () {
                    let initMap = `
                        <div id="activityLinkMap" style="height: 100vh !important;" class="show-map"></div>
                    `;
                    $(".tab-content").append(initMap);
                },
                sections : {
                    load : function () {
                        preview.views.sections.showNetworkDescription();
                        
                    },
                    showNetworkDescription : function () {              
                        let paramsToGetAns = {
                            showInOrga : {
                                "address.level4" : 1,
                                "_id" : 0
                            }            
                            ,
                            show: [
                                "links.organizations", 
                                "_id",
                                "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q",
                                "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op",
                            ],    
                            where : {
                                "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" : name,
                            }
                        };
                        let descriptionHtml = `
                            <div class="margin-bottom-20 text-center">
                                <h3 class="margin-bottom-20">DESCRIPTION DU RESEAU</h3>
                            </div>
                            <hr class="margin-bottom-20" style=" width: 50%; border-top: 1px solid #000;">
                            <div class="col-md-12 stat-network"></div>
                        `;
                        $(".tab-pane #reseaux").removeClass("col-xs-10");
                        $(".tab-pane #reseaux").html(descriptionHtml);

                        let showTheseReferences = `
                                <div class="col-md-6 text-center tiers-number" style="margin-top: 67px"></div>
                                <div class="col-md-6 text-center percentage"></div>
                                <div class="col-md-6 text-center communes" style="margin-top: 57px"></div>
                                <div class="col-md-6 text-center activityDomain" style="margin-top: 57px"></div>
                                <div class="col-md-12 word-cloud-content">
                                    <div id="wordCloud" class="col-md-12 text-center" style="width: 600px; height: 400px;"></div>
                                </div>
                        `;


                        // TIERS-LIEUX NUMBER
                        $(".stat-network").append(showTheseReferences);
                        $(".tiers-number").append(preview.actions.generateCounterText(<?= $countTiers ?>, "Tiers-Lieux", "red", "50px"));

                        // DUGNUT PERCENTAGE
                        // $(".percentage").append(`
                        //     <div>
                        //         <canvas width="200" id="chartProgress"></canvas>
                        //     </div>
                        //     <div>
                        //         <span style="color:skyblue; font-size:50px;">De tous les Tiers-Lieux</span>
                        //     </div>
                        // `);
                        $(".percentage").append(preview.actions.generateCounterText(<?= $tiersLieuxPercent ?>, "De tous les Tiers-Lieux", "skyblue", "50px", true));
                        
                        // COMMUNES
                        let allAnswersOrg = showAllNetwork.getAnswers("/form/63e0a8abeac0741b506fb4f7/showOrga/true", paramsToGetAns).allAnswers;
                        // let allAnswersOrg = orgaObject;
                        $(".communes").append(preview.actions.generateCounterText(preview.actions.getLevel4Number(allAnswersOrg), "Communes impliquées", "orange", "50px"));


                        // ACTIVITY DOMAIN      
                        $(".activityDomain").append(preview.actions.generateCounterText(preview.actions.countActivityDomain(allAnswersOrg, "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q", "domain"), "Domaine d'activité", "purple", "50px"));

                        // WORD TEXT 
                        // mylog.log("adkhcadkiec") 
                        let data = preview.actions.countActivityDomain(allAnswersOrg, "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op", "value")
                        preview.actions.wordCloud(data)

                        preview.views.sections.economicModel(allAnswersOrg);

                    },
                    economicModel : function () {
                        let paramsToGetAns = {
                            // showInOrga : {
                            //     "address.level4" : 1,
                            //     "_id" : 0
                            // }            
                            // ,
                            show: [
                                "links", 
                                "_id",
                                economicModelObject.distributionValue.path["Nombre_d__équipement"],
                                economicModelObject.distributionValue.path["Pourcentage_de_production"],
                                economicModelObject.distributionValue.path["Pourcentage_activité"],
                                economicModelObject.valueCreation.path["valueDistribution"],
                                economicModelObject.captureValue.path["valuecapturePath"],
                                economicModelObject.resume.path["benef"],
                                economicModelObject.resume.path["amount"]
                            ],    
                            // where : {
                            //     "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" : name,
                            // }
                        };
                        // let allResults  = Object.assign(
                        //     {}, 
                        //     preview.actions.getAnswers("645b300b6d70bb35426fc0e3", paramsToGetAns), 
                        //     preview.actions.getAnswers("6486d24e9cad105cbf29a777", paramsToGetAns),
                        //     preview.actions.getAnswers("646498a5f2b3d6423d74b7f6", paramsToGetAns)
                        // ); 
                        let allResults = markersAndSuplForms['suplAnswers']

                        
                
                        $.each(economicModelObject, function(k,v){                 
                            
                            let initHtml = `
                                <div class="margin-left-20">
                                    <h3>${v.title}</h3>
                                </div>                   
                            `;   

                            $.each(v.path, function(key, value){
                                if (k == "resume") {
                                    
                                } else {
                                    if (k == "distributionValue") {
                                        let accent = key.replace("__","'");
                                        let word = accent.replace("_"," ");
                                        initHtml += `
                                            <div class="${key}">        
                                                <h4 style="margin-left: 82px;">${word}</h4>
                                            </div>  
                                        `;
                                    } else {
                                        initHtml += `
                                            <div class="${key}"> </div>  
                                        `;
                                    }
                                }
                            })
                            $("#" + v.id).html(initHtml);

                            // let negativeValue = 1000;
                            // let positiveValue = 2000;
                            let chartBarData = {
                                // positive: positiveValue,
                                // negative: negativeValue
                            }

                            $.each(v.path, function(key, value){ 
                                if (k == "resume") {
                                    if (key == "benef") {
                                        chartBarData["positive"] = Number(preview.actions.totalBenefOrAmount(allResults, value));
                                    } else {
                                        chartBarData["negative"] = - Number(preview.actions.totalBenefOrAmount(allResults, value));
                                    }
                                       
                                } else {
                                    mylog.log("sldeafvhdsaeil", preview.actions.transformObject(preview.actions.onlyAnswer(allResults, value)))
                                    if (preview.actions.transformObject(preview.actions.onlyAnswer(allResults, value)).length > 0) {
                                        preview.actions.showRadar('.' + key, preview.actions.transformObject(preview.actions.onlyAnswer(allResults, value))); 
                                    } else {
                                        $('.' + key).append(preview.views.emptyContent());
                                    }
                                     
                                }
                            })

                            if (k == "resume") {
                                mylog.log("sdaivgudai", chartBarData);
                                let initrendHtml = `
                                    <div class="canvasContent">
                                        <canvas id="BarChartNetworkResume"><canvas>
                                    </div>
                                `;
                                $(".tab-content").append(initrendHtml);
                                ctx = document.getElementById("BarChartNetworkResume").getContext("2d");
                                preview.actions.loadActivityChart(chartBarData, "bar", ctx);
                                if (chartBarData.positive == 0 && chartBarData.negative == -0) {
                                    $(".canvasContent").append(preview.views.emptyContent());
                                }
                            }

                        })
                    }
                },
                progressChart : function (ctx, percentage) {
                    let chart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                        labels: ['Progress', 'Remaining'],
                        datasets: [{
                            data: [percentage, 100 - percentage],
                            backgroundColor: ['#5283ff','#fff']
                        }]
                        },
                        plugins: [{
                        beforeDraw: (chart) => {
                            var width = chart.width,
                                height = chart.height,
                                ctx = chart.ctx;
                            
                            ctx.restore();
                            var fontSize = (height / 150).toFixed(2);
                            ctx.font = fontSize + "em sans-serif";
                            ctx.fillStyle = "#9b9b9b";
                            ctx.textBaseline = "middle";
                            var text = chart.data.datasets[0].data[0] + "%",
                                textX = Math.round((width - ctx.measureText(text).width) / 2),
                                textY = height / 2;
                            ctx.fillText(text, textX, textY);
                            ctx.save();
                        }
                        }],
                        options: {
                                maintainAspectRatio: false,
                                cutout: '85%',
                                rotation: Math.PI / 2,
                                plugins: {
                                legend: {
                                    display: false
                            },
                            tooltip: {
                                filter: (tooltipItem) => tooltipItem.dataIndex === 0
                            }
                            }
                        }
                    });
                }
            },
            actions : {
                load : function () {
                    preview.actions.clickInitial();
                    // preview.actions.filterMap();
                },
                clickInitial : function () {
					$("[href='#menu0']").trigger("click");
				},
                loadActivityChart : function (chartData={}, chartType="", ctx) {
                    // var ctx = document.getElementById(id).getContext("2d"); 
                    var labels = Object.keys(chartData);
                    var data = Object.values(chartData);
                    var backgroundColors = preview.actions.getRandomActivityColors(labels.length);
                    var chartConfig = {}

                    if (chartType == "pie") {
                        chartConfig = {
                            type: chartType,
                            data: {
                                labels: labels,
                                datasets: [{
                                    data: data,
                                    backgroundColor: backgroundColors
                                }]
                            },
                            options: {
                                responsive: true
                            }
                        }
                    }

                    if (chartType == "bar") { 	
                        chartConfig =  {
                            type: chartType,
                            data: {
                                labels: ['Data'],
                                datasets: [
                                    {
                                    label: 'Positive Gain',
                                    backgroundColor: 'rgb(0, 131, 126)',
                                    data: [chartData.positive],
                                    barThickness: 40
                                    },
                                    {
                                    label: 'Negative Loss',
                                    backgroundColor: 'rgb(116, 31, 24)',
                                    data: [-chartData.negative],
                                    barThickness: 40
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                scales: {
                                    x: {
                                        stacked: true
                                    },
                                    y: {
                                        stacked: true,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // if (chartType == 'radar') 
                    // 	jsonHelper.setValueByPath(chartConfig, 'options.legend.display', false)		
                    let chartJs = new Chart(ctx, chartConfig);	
                    mylog.log("dsajfvgik", chartConfig);
                },
                wordCloud : function (wordCdata) {
                    $('#wordCloud').jQCloud(wordCdata, {
                        delay: 100,
                        fontSize: {
                            from: 0.1,
                            to: 0.04
                        }
                    });
                },
                showMap : function (container, data={}) {
                    let customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
                    let appMap = new CoMap({
                        zoom : 5,
                        container : container,
                        activePopUp : true,
                        mapOpt:{
                            zoomControl:true
                        },
                        activePreview : true,
                        mapCustom:customMap,
                        elts : {}
                    })        
                    appMap.addElts(data);
                    // return appMap;
                    // 

                    // L.polyline(appMap.getOptions().arrayBounds, { color: 'blue', smoothFactor: 1 }).addTo(appMap.getMap())
                    // mylog.log("sdkfvjhdksa",appMap.getOptions().arrayBounds);
                    // https://codepen.io/haiiaaa/pen/KKpYmRz
                },
                filterMap : function(dataFilter={}, containerId="", pathFilter="") {
                    var paramsFilter= {
                            interface : {
                                events : {
                                    page : true,
                                    scroll : true,
                                    // scrollOne : true
                                }
                            },
                            container : containerId,
                            urlData : baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7",
                            defaults: {
                                notSourceKey : true,
                                types:["answers"],
                                indexStep : 9000,
                                forced : {
                                        filters : { }
                                    }
                            },
                    };
                    paramsFilter["filters"] = dataFilter;
                    paramsFilter.defaults.forced["filters"] = (pathFilter != "") ? pathFilter : {"answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" : name};

                    var filterSearch={};
                    
                    filterSearch = searchObj.init(paramsFilter);
                    filterSearch.results.render=function(fObj, results, data){
                        let orgaId = "";
                        let type = "";
                        let answerOrga = {};
                        let markerData = [];
                        $.each(results, function (k, v) {
                            markerData.push(orgaObject[k])
                        })
                        markerData = Object.assign({}, ...markerData);
                        if (!(Object.keys(markerData).length > 0)) {
                            let rectifiedFilter = paramsFilter.defaults.forced["filters"];
                            for (var key in rectifiedFilter) {
                                if (rectifiedFilter.hasOwnProperty(key)) {
                                    if (rectifiedFilter[key].hasOwnProperty("$exists")) {
                                        rectifiedFilter[key] = rectifiedFilter[key]["$exists"];
                                        delete rectifiedFilter[key]["$exists"];
                                    }
                                }
                            }
                            preview.actions.filterMap({}, "", rectifiedFilter);   
                        }
                        preview.actions.showMap('#activityLinkMap', markerData)
                    };
                    
                    filterSearch.container = "#activityLinkMap" 
                    filterSearch.search.init(filterSearch);
                },
                generateCounterText : function (number=0, text="", color="", size=0, isPercent=false) {
                
                    return `
                        <h4 style="margin: 0px !important; color:${color}; font-size:${size};" >${(isPercent == true) ? Math.ceil(number) + " % " : number}</h4>
                        <span style="color:${color}; font-size:${size};"> ${text}  </span>
                    `;
                    ;
                }, 
                getLevel4Number : function (dataOrga={}) {
                    if (Object.keys(dataOrga).length > 0) {
                        let allLev4 = {};
                        $.each(dataOrga, function(key,value) {
                            let levelValue = value?.address?.level4;
                            if (levelValue != undefined) {
                                allLev4[levelValue] = "level4";
                            }          
                        })
                       
                        return (Object.keys(allLev4).length > 0) ? Object.keys(allLev4).length + 1 : 0;
                    }
                },
                transformObjectToArray : function (obj) {
                    let entries = Object.entries(obj);
                    let sortedEntries = entries.sort((a, b) => b[1] - a[1]);
                    let result = sortedEntries.map(([text, weight]) => ({ text, weight }));
                    return result;
                },
                transformObject : function (obj) {
					return Object.entries(obj).map(([axis, value]) => ({ axis, value }));
				},
                countActivityDomain : function (activityDomainData = {}, answerPath=undefined, typeAnswer) {
                    let result = {};
                    if (Object.keys(activityDomainData).length > 0) {
                        result = 0;
                        let dataWord = {}
                        $.each(activityDomainData, function(keyAct, valueAct) {
                            if (answerPath != undefined) {
                                let answer = jsonHelper.getValueByPath(valueAct, answerPath) ?? {};
                                let allDomain = {};
                                if (typeAnswer == "domain") {
                                    if (Object.keys(answer).length > 0) {
                                        $.each(answer, function(k, v) {
                                            allDomain[Object.keys(v)[0]] = "domain";
                                        })
                                        result = Object.keys(allDomain).length; 
                                    }
                                }  
                                if (typeAnswer == "value") {      
                                    if (Object.keys(answer).length > 0) {
                                        $.each(answer, function(k, v) {
                                            if (typeof dataWord[Object.keys(v)[0]] == "undefined") {
                                                dataWord[Object.keys(v)[0]] = 0;
                                            }
                                            dataWord[Object.keys(v)[0]] = dataWord[Object.keys(v)[0]] + 1;
                                        })
                                    }
                                }     
                            }
                        }) 
                        if (Object.keys(dataWord).length > 0) {
                            return preview.actions.transformObjectToArray(dataWord);
                        }
                    }
                    return result ;
                },
                showRadar : function (container='', dataContent=[]) {
					radarChart.defaultConfig.color = function() {};
					radarChart.defaultConfig.radius = 3;
					radarChart.defaultConfig.w = 600;
					radarChart.defaultConfig.h = 600;

					var data = [
						{
						className: 'default', 
						axes: dataContent
						}
					];

					var chart = radarChart.chart();
					var cfg = chart.config(); 
					var svg = d3.select(container).append('svg')
						.attr('width', "900")
						.attr('height', cfg.h + cfg.h / 4);
					svg.append('g').classed('single', 1).attr("transform", "translate(170, 90)").datum(data).call(chart);
				},
                onlyAnswer : function (data={}, path="") {
                    let result = {}
                    mylog.log("sdakuvh", data);
                    if( Object.keys(data).length > 0 && path != "" ) {   
                        
                        $.each(data, function (k, v) {
                            if (v.answers != undefined && Object.keys(v.answers).length > 0) {
                                // answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu50gd93x5x1uva3xv
                                
                                let answerContent = jsonHelper.getValueByPath(v, path);
                                if (answerContent != undefined) {
                                    $.each(answerContent, function(ansKey, ansValue) {
                                       let valueKey =  Object.keys(ansValue)[0];
                                       if (typeof result [Object.keys(ansValue)[0]] == "undefined") {
                                            result [Object.keys(ansValue)[0]] = 0;
                                       }
                                       result [Object.keys(ansValue)[0]] = Number(result [Object.keys(ansValue)[0]]) + ((ansValue[valueKey].textsup != undefined) ? Number(ansValue[valueKey].textsup) : 0);
                                    })
                                }
                            }
                            // result[] = 
                        })
                        // mylog.log("sdakuvh", result);
                        return result;

                    }
    
                    return result;
                },
                getRandomActivityColors : function (count) {
					let colors = [];
					for (var i = 0; i < count; i++) {
						var color = '#' + Math.floor(Math.random() * 16777215).toString(16);
						colors.push(color);
					}
					return colors;
				},
                totalBenefOrAmount : function (data={}, path, benefOrAmountValue) {
                    // mylog.log("sdvhudso", data);
                    let total = 0;
                    if (Object.keys(data).length > 0) {
                        $.each(data, function (index,value) { 
                            if (jsonHelper.getValueByPath(value, path) != undefined && !isNaN(jsonHelper.getValueByPath(value, path))) {
                                total = total + Number(jsonHelper.getValueByPath(value, path));
                            }
                        })
                    }
                    return total;
                }
            }
        }     
        preview.init();

        $('.listLeftBtn li').on('click',function() {
            $('.filters-activate').trigger('click');
        })
    })
</script>