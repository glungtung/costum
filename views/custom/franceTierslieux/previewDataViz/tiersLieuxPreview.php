<?php 
    $cssAnsScriptFilesTheme = array(
		'/plugins/jQCloud/dist/jqcloud.min.js',
		'/plugins/jQCloud/dist/jqcloud.min.css',
		"/plugins/d3/d3.v3.min.js",
		"/plugins/Chart.js/Chart.v4.js"
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
	$cssAnsScriptFiles = array(
		"/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
		"/js/franceTierslieux/dataviz/radarChart.js",
		"/js/franceTierslieux/dataviz/negativePositiveBar.js",
		"/css/franceTierslieux/radarChart.css",
		"/css/franceTierslieux/negativePositiveBr.css"
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
?>
<style>
	.content-img-profil-preview {
		margin-top: 61.5px;
	}
	.img-responsive {
		margin: auto;
		box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);
	}
	.show-map {
        height: 50vh;
    }
	.listTools {
		list-style: none;
    	text-align: center;
	}
	.tab-content {
        background: #f9f7f7;
        height: 95vh;
    }

	.map-preview-tiers #filterContainerInside {
		background: inherit;
	}

	/* .gauge-content{
		display: flex;
    	justify-content: center;
	} */

	/* .canvas-chart {
		width: 50vh !important;
		height: auto !important;
	} */

	.tab-content>.active, .health-btn , .gauge-content, #wordsContent{
		display: flex !important;
		justify-content: center !important;
	}

	.title-btn {
		display: flex;
		justify-content: space-between;
	}

	.coFormbody {
		height: 80vh;
		overflow-y: scroll;
	}

	.btn-return-form:hover {
		color: black;
		background: #8AC4C5 !important;
	}
	.btn-return-form {
		background: #8AC4C5 !important;
	}

	#showRadarDistributionVal, 
	#showRadarCreateValue, 
	#showRadarCreateValue1,
	#showRadarCreateValue2,
	#showRadarCaptureValue,
	.buttons-content {
		display: flex;
   	 	justify-content: center;
	}

	.buttons-content button {
		border-radius: 0px !important;
	}
	
</style>
<script>
	$(function() {
		let network = "<?= $network ?>";
		let organizationId = "<?= $orgaId ?>";
		let id = "<?= $id ?>";
		let name = "<?= $name ?>";
		let initHtml = "";
		let orgaObject = showAllNetwork.getAnswers ('/form/63e0a8abeac0741b506fb4f7/getMarkers/true', showAllNetwork.markerParams(network))["markers"];
		let ecomomicModelAnswer = {};
		let getOrganizations = {};
		let economicModelSuplementId = '645b300b6d70bb35426fc0e3';
        let otherEconomicSupplement = '6486d24e9cad105cbf29a777';
		getAnswers = {
			...showAllNetwork.getElement("answers", id).answers,
			answerId : id
		};
		getOrganizations = showAllNetwork.getElement("organizations", organizationId);
		// // get economicModel supp form
		let results = {...showAllNetwork.getAnswers('/form/' + economicModelSuplementId, 
			{
				where: 
				{
					["answers.lesCommunsDesTierslieux1052023_949_0.finderlesCommunsDesTierslieux1052023_949_0lat3sl0xwwg06k856jc." + organizationId + ".name"] : name
				}
			}), ...showAllNetwork.getAnswers('/form/' + otherEconomicSupplement,  
				{
				where: 
					{
						["answers.lesCommunsDesTierslieux1262023_1215_0.finderlesCommunsDesTierslieux1262023_1215_0lix46h1uhnukfgtzj6s." + organizationId + ".name"] : name
					}
				})
			};
		
		if (Object.keys(results).length > 0) {
			economicModelAnswer = results;
		} 
		// FREQUENTATION
		let frequentationSuplPath = "answers.lesCommunsDesTierslieux1752023_1312_0.finderlesCommunsDesTierslieux1752023_1312_0lix4a7257iht8lp282r";
		let frequentationFormId = "646498a5f2b3d6423d74b7f6";
		//activity form
		let activityPath = "answers.lesCommunsDesTierslieux1262023_1215_0.finderlesCommunsDesTierslieux1262023_1215_0lix46h1uhnukfgtzj6s";
		let activityFormId = "6486d24e9cad105cbf29a777";
		let activityAnswerPath = ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq";
		let activityClass = "activityBtn";
		//LIST TOOL FINDER
		let listToolFinder = "answers.lesCommunsDesTierslieux10112022_1423_0.finderlesCommunsDesTierslieux10112022_1423_0ljb8xhl6zywb6rehocs";
		let listToolFormId = "636cd563e2439b7fc12cd680";
		// THIS ANSWERID
		let thisAnswerId = getAnswers?.answerId;
		let activityInput = getAnswers?.franceTierslieux1822023_1721_4?.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab;
		let principleAndValue = getAnswers?.franceTierslieux1522023_146_2?.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op;
		let gouvernance = getAnswers?.franceTierslieux1922023_1417_7?.multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes;
		let equipment = getAnswers?.franceTierslieux1822023_1721_4?.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4;
		let frequentation = getAnswers?.franceTierslieux1922023_1231_5?.franceTierslieux1922023_1231_5lebb61gowlj39f2a2e;
		let ageFrequentation = getAnswers?.franceTierslieux1922023_1231_5?.franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v;
		let employeeContract = getAnswers?.franceTierslieux1922023_1231_6?.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt;
		let memberShip = getAnswers?.franceTierslieux1922023_1231_5?.franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve;
		let generatedProductAns = getAnswers?.franceTierslieux2022023_732_9?.franceTierslieux2022023_732_9lecfwuxx9q1jro8u6xf;

		let preview = {
			init : function () {
				preview.actions.load();
				preview.views.load();
				preview.events.load();
			},
			events : {
				load : function () {
					preview.events.sectionButtons();
				},
				sectionButtons : function() {
					$('#resumeBarContent, .resumeBtnClass').hide();
				
					$('.sub-menu-section li a').on('click', function () {
						$('.map-preview-tiers').hide();
						$('#resumeBarContent, .resumeBtnClass').hide();
					}) 

					$('.listLeftBtn > li >  .section-button').on('click',function() {
						$('#resumeBarContent, .resumeBtnClass').hide();
						// alert($(this).attr('href'))
						$(".sub-menu-section").hide();
						$('.map-preview-tiers').hide();
						// })

						// $('.listLeftBtn  li  a').on('click',function() {
						let filterActivity = {};
						if ($(this).attr("id") == "section-reseaux") { 		
							// preview.actions.listCheckbox({}, "Section qui represente les liens de ce tiers-lieux par rapport aux autres", ".map-preview-tiers");		
							filterActivity =  { 
								activity : {
									view : "dropdownList",
									type : "filters",
									name : "Filtre par activites",
									event : "filters",
									typeList : "object",
									list : (activityInput != undefined) ? preview.actions.generateFilterObject(activityInput, "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab") : []
						
								},
								valueFilter : {
									view : "dropdownList",
									type : "filters",
									name : "Filtre par valeurs",
									event : "filters",
									typeList : "object",
									list : (principleAndValue != undefined) ? preview.actions.generateFilterObject(principleAndValue, "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op") : []
								},
								gouvernanceFilter : {
									view : "dropdownList",
									type : "filters",
									name : "Filtre par gouvernance",
									event : "filters",
									typeList : "object",
									list : (gouvernance != undefined) ? preview.actions.generateFilterObject(gouvernance, "answers.franceTierslieux1922023_1417_7.multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes") : []
								},
								equipementFilter : {
									view : "dropdownList",
									type : "filters",
									name : "Filtre par equipement",
									event : "filters",
									typeList : "object",
									list : (equipment != undefined) ? preview.actions.generateFilterObject(equipment, "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4") : []
								},
								FrequentationFilter : {
									view : "dropdownList",
									type : "filters",
									field : "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gowlj39f2a2e",
									name : "Filtre par frequentation par catégorie sociale",
									event : "inArray",
									list : (frequentation != undefined) ? frequentation : []
								},
								ageFrequentationFilter : {
									view : "dropdownList",
									type : "filters",
									field : "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v",
									name : "Filtre par frequentation par age",
									event : "inArray",
									list : (ageFrequentation != undefined) ? ageFrequentation : []
								}
							}
							$('.map-preview-tiers').fadeIn();
							let filterList = getAnswers?.franceTierslieux1922023_1231_5?.franceTierslieux1922023_1231_5lebb61gowlj39f2a2e;
							if (filterList != undefined) {
								filterActivity.FrequentationFilter.list = filterList;
							}
							// $('.map-preview-tiers').fadeIn();
							preview.actions.filterMap(filterActivity, "#filterContainer", network);
						}
										
					})

					$(preview.actions.clickSectionHref("#resume")).on("click", function() { 
						$('#resumeBarContent, .resumeBtnClass').fadeIn();
					})

					// $("#section-reseaux").click(function() {
					$("#reseaux").append(
						`<h3>Section qui represente les liens de ce tiers-lieux par rapport aux autres</h3>`
					)	
					$("#reseaux").attr("class","");
					// })

				},
				clickModalEvent : function (selector) {
					$(selector).off().on('click', function (e) {
						preview.actions.answerModal($(this).data("id"), $(this).data("elformid"),$(this).data("step"));
					});
				}
			},
			views : {
				load: function() {
					preview.views.answerInformation.loadAnswerInformation();
					// preview.views.hideNetworkMap();
					preview.views.showMapNetwork();
				},
				modalButtonHtml : function (modalClass="", answerId="", formId="", step="", btnName="Formulaire", color="") {
					return `
						<button 
							class="btn btn-small ${(color != "") ? "bg-" + color : "bg-red"} ${modalClass}" style="cursor: pointer; color: white; " 
							data-id="${answerId}" 
							data-step="${step}"
							data-elformid="${formId}"> <h5> <i class="fa fa-pencil"> </i> ${btnName} </h5>
						</button>						
					`;
				},
				hideNetworkMap : function () {
					// $('.map-preview-tiers').css('display:none');
				},
				showMapNetwork : function () {
					// <div id="filterContainer"></div>
					let networkHtml = `
						<div class="map-preview-tiers">	
							<div id="filterContainer"></div>
							<div id="mapContainerTiers" class="show-map"></div>
						</div>
					`;
					$(".tab-content").append(networkHtml); // reseauMap
				},
				showGauge : function (width="600", height="400", percentage=0) {
					let minValue = 135;
					let maxValue = 315;
					let percentageDegree = (percentage / 100) * (maxValue - minValue) + minValue;
					let gaugeHtml = `
						<div class="gauge-content">
							<svg width="${width}" height="${height}" viewBox="-50 0 400 200">
								<path  stroke="#E81123" stroke-width="20" fill-opacity="0" d="M 10, 170.00000000000003 A 160,160 0 0, 1 52.98329862125358, 60.88037940664711"></path>
								<path fill="#FFF" stroke="#FF8C00" stroke-width="20" fill-opacity="0" d="M 60.880262390000226, 52.98340774093273 A 160,160 0 0, 1 164.41592062507033, 10.0974732609441"></path>
								<path fill="#FFF" stroke="#FCD116" stroke-width="20" fill-opacity="0" d="M 175.5839194724002, 10.097467676944689 A 160,160 0 0, 1 279.1196205933529, 52.983298621253624"></path>
								<path fill="#FFF" stroke="#3BA316" stroke-width="20" fill-opacity="0" d="M 287.0165922590673, 60.88026239000028 A 160,160 0 0, 1 329.99999999991996, 169.99983999999995"></path>
								<path fill="#7f8c8d" d="M170 170l10.606601717798211 0 l95.4594154601839 106.06601717798212 l-106.06601717798212 -95.4594154601839l0 -10.606601717798211z" transform="rotate(${percentageDegree}, 170, 170)"></path>
							</svg>
						</div>
					`;
					return gaugeHtml;
				},
				emptyContent : function () {
					return `
						<p>
							Vous n'avez pas rempli le formulaire complémentaire pour visualiser ces données. <br>
							En remplissant le formulaire, vous nous permettez de disposer d'une vision plus complète de la situation, ce qui nous aide à prendre des décisions éclairées pour l'amélioration de ces espaces. Votre participation contribue également à créer une communauté solidaire et à favoriser l'échange de connaissances entre les utilisateurs. <br>
							vous pouvez y accéder en cliquant sur le bouton <b> FORMULAIRE </b>
						</p>
					`;
				},
				initialInformation : {
					loadInitalInformation : function (frequentationBtnAndSup, equipmentSuplement, activityFormButton) {
						preview.views.initialInformation.showAddress();
						preview.views.initialInformation.showSiteWeb();
						preview.views.initialInformation.showShortDescription(frequentationBtnAndSup, equipmentSuplement, activityFormButton);
						preview.views.initialInformation.showSocialLink();
						preview.views.initialInformation.showNetwork();
					},
					showAddress : function () {
						let address = (getOrganizations?.address?.streetAddress != undefined) ? getOrganizations?.address?.streetAddress : "";
						let postalCode = (getOrganizations?.address?.postalCode != undefined) ? ", "+getOrganizations?.address?.postalCode : "";
						let level4Name = (getOrganizations?.address?.level4Name != undefined) ? ", "+getOrganizations?.address?.level4Name : "";
						let level3Name = (getOrganizations?.address?.level3Name != undefined) ? ", "+getOrganizations?.address?.level3Name : "";
						let level2Name = (getOrganizations?.address?.level2Name != undefined) ? ", "+getOrganizations?.address?.level2Name : "";
						let level1Name = (getOrganizations?.address?.level1Name != undefined) ? ", "+getOrganizations?.address?.level1Name : "";
						address = address + postalCode + level3Name + level2Name + level1Name;
						let showAddressHtml = `
							<p style="color: red;">
								<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
								${address}
							</p>
						`;
						$(".elementAddress").html(showAddressHtml);
					}, 
					showSiteWeb : function () {
						let elementWebSite = (getOrganizations?.url != undefined) ? getOrganizations?.url : "";
						let urlHtml = `
							<a href="${elementWebSite}">${elementWebSite}</a>
						`;
						if (elementWebSite != "") {
							$(".elementUrl").html(urlHtml);
						}
					},
					showShortDescription : function (frequentationBtnAndSup, equipmentSuplement, activityFormButton) {
						// alert(frequentationBtnAndSup["html"])
						let descriptionValue = (getOrganizations?.shortDescription != undefined) ? getOrganizations.shortDescription : "";
						let descHtml = `
							<div class="description-content">
								<h4> Description courte : </h4>
								<p>${descriptionValue}</p>							
							</div>
							<div class="form-sup-content margin-top-20">
								<h4 class=""> Fomulaires supplémentaire: </h4>
								<p> Veuiller completer le(s) formulaire(s) en couleur rouge </p>
								<div class="buttons-content">
									${frequentationBtnAndSup}
									${equipmentSuplement}
									${activityFormButton}
								</div>							
							</div>
						`;
						if (descriptionValue != "") {
							$(".shortDescriptionHeader").html(descHtml);
						}
					},
					showSocialLink : function () {
						if (getOrganizations?.socialNetwork != undefined && getOrganizations.socialNetwork.length > 0) {
							//socialBar
							let socialNetworkHtml = "";
							$.each(getOrganizations.socialNetwork, function (index, socialValue) {
								if (socialValue.platform.includes("Linkedin")) {
									socialNetworkHtml += '<img class="co3BtnShare" width="40" src="' + parentModuleUrl + '/images/social/linkedin-icon.png" onclick="window.open(`'+ socialValue.url +'`,`_blank`)">';
								}
								if (socialValue.platform.includes("Instagram")) {
									socialNetworkHtml += '<img class="co3BtnShare" width="40" src="' + parentModuleUrl + '/images/social/instagram-icon.png" onclick="window.open(`'+ socialValue.url +'`,`_blank`)">';
								}
								if (socialValue.platform.includes("Facebook")) {
									socialNetworkHtml += '<img class="co3BtnShare" width="40" src="' + parentModuleUrl + '/images/social/facebook-icon-64.png" onclick="window.open(`'+ socialValue.url +'`,`_blank`)">';
								}
								if (socialValue.platform.includes("Twitter")) {
									socialNetworkHtml += '<img class="co3BtnShare" width="40" src="' + parentModuleUrl + '/images/social/twitter-icon.png" onclick="window.open(`'+ socialValue.url +'`,`_blank`)">';
								}
								// if (socialValue.platform.includes("Facebook")) {
								// 	socialNetworkHtml += '<img class="co3BtnShare" width="40" src="' + parentModuleUrl + '/images/social/facebook-icon-64.png" onclick="window.open(`'+ socialValue.url +'`,`_blank`)">';
								// }
							})
							$(".socialBar").html(socialNetworkHtml);
						}
					},
					showNetwork : function () { 
						let networkHtml = `
							<h4> RESEAUX : </h4>
							<p>${network}</p>
						`
						$(".showTheNetwork").html(networkHtml);
					}
				},
				answerInformation : {
					loadAnswerInformation : function () {
						// (suplForm="", btnPath="", btnClass="", formName="")
						let frequentationBtnAndSup = preview.actions.generateButton(
							frequentationFormId, 
							frequentationSuplPath, 
							"frequentationBtn",
							"LA FRÉQUENTATION"
						);
						
						let equipmentSuplement = preview.actions.generateButton(
							economicModelSuplementId, 
							"answers.lesCommunsDesTierslieux1052023_949_0.finderlesCommunsDesTierslieux1052023_949_0lat3sl0xwwg06k856jc", 
							"equipmentBtn",
							"LES ÉQUIPEMENTS"
						);

						let activityFormButton = preview.actions.generateButton(
							activityFormId, 
							activityPath, 
							activityClass,
							"MODÈLE ÉCONOMIQUE"
						);
						
						preview.views.answerInformation.showInfo(frequentationBtnAndSup["htmlWhithName"], equipmentSuplement["htmlWhithName"], activityFormButton["htmlWhithName"]);
						preview.views.answerInformation.showActivity(equipmentSuplement, activityFormButton);
						preview.views.answerInformation.showPrincipleAndValue();
						preview.views.answerInformation.showEquipment();
						preview.views.answerInformation.showDomain();
						preview.views.answerInformation.showFrequentation(frequentationBtnAndSup);
						preview.views.answerInformation.showAgeFrequentation(frequentationBtnAndSup);
						preview.views.answerInformation.showEmployeesContract();
						preview.views.answerInformation.showGouvernance();
						preview.views.answerInformation.listTools();
						preview.views.answerInformation.showHealth(frequentationBtnAndSup, equipmentSuplement, activityFormButton);
						preview.views.answerInformation.showCreateValue(activityFormButton, equipmentSuplement);
						preview.views.answerInformation.showDistributionValue(activityFormButton);
						preview.views.answerInformation.showCaptureValue(activityFormButton);
						preview.views.answerInformation.showResume(activityFormButton);
					},
					showInfo : function (frequentationBtnAndSup, equipmentSuplement, activityFormButton) {
						let infoHtml = `
							<div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
								<img class="img-responsive shadow2 thumbnail" src="<?= $image ?>">
							</div>
							<div class="preview-element-info col-xs-12">
								<h3 class="text-center"><?= $name ?></h3>
								<div class="col-xs-12 margin-top-20 text-center elementAddress" ></div>
								<div class="col-xs-12 text-center elementUrl" ></div>
								<div class="header-tags col-xs-12 text-center blockFontPreview margin-top-20"></div>
							</div>
							<div class="social-share-button-preview col-xs-12 text-center margin-top-20 margin-bottom-20">
								<div class="socialBar"></div>
							</div>
							<div class="col-xs-10 col-xs-offset-1 margin-top-20 showTheNetwork"></div>
							<div class="col-xs-10 col-xs-offset-1 margin-top-20 shortDescriptionHeader"></div>
						`;
						$("#info").html(infoHtml);
						preview.views.initialInformation.loadInitalInformation(frequentationBtnAndSup, equipmentSuplement, activityFormButton);
					},
					showActivity : function (equipmentSuplement, activityFormButton) {
						// step franceTierslieux1822023_1721_4
						// input multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab
						// #activity
						// tet answer 6452009060ad233f5761f025
						preview.actions.listCheckbox({}, "Activités proposées", "#activity", false, "Cette section montre les activité du Tiers-Lieux.", activityFormButton["html"]);	
						let initChartHtml = "";
						let activityChartData = jsonHelper.getValueByPath(activityFormButton["getSuplForm"], activityFormButton["answerId"] + activityAnswerPath);
						if (activityChartData == undefined) {
							initChartHtml += preview.views.emptyContent();
						}
						initChartHtml += `<canvas id="activityPie"></canvas>`;
						$("#activity").append(initChartHtml);
						if (activityFormButton["canShowContent"]) {
							let chartData = {
								...preview.actions.getKeyAndTextSup(activityChartData),
							};
							let ctx = document.getElementById("activityPie").getContext("2d");
							preview.actions.section.loadActivityChart(chartData, "pie", ctx);
						} 
						
					},
					showPrincipleAndValue : function () {
						// step franceTierslieux1522023_146_2
						// input multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op
						// #principleAndValue
						
						let dataPrinciple = [];
						preview.actions.listCheckbox({}, "Valeurs et principes", "#principleAndValue",false, "Cette section montre les valeurs et principes du Tiers-Lieux",(thisAnswerId != undefined) ? preview.views.modalButtonHtml("principleValue", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1522023_146_2") : "");
						let initWordCloudHtml = `
							${(principleAndValue == undefined) ? preview.views.emptyContent() : ""}
							<div id="wordsContent">	
								<div id="words" style="width: 600px; height: 400px;"></div> 
							</div> 
						`;
						$("#principleAndValue").append(initWordCloudHtml);
						preview.events.clickModalEvent(".principleValue");
						
						if (principleAndValue != undefined) {
							$.each(principleAndValue, function (dataKey, dataValue) {
								let name = Object.values(dataValue)[0].value;
								let importance = Object.values(dataValue)[0].rank;
								dataPrinciple.push({
									text: name,
									weight: importance
								})
							})
						}
						preview.actions.section.loadPrincipleWordC(dataPrinciple)
					},
					showEquipment : function () {
						// step franceTierslieux1822023_1721_4
						// input multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4
						// #equipment
						// assetPath	
						preview.actions.listCheckbox({}, "Types équipements", "#equipment", false, "", (thisAnswerId != undefined) ? preview.views.modalButtonHtml("showModalEquipment", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1822023_1721_4") : "");

						let initImagesHtml = `
							<div class="imagesEquipment">
								${preview.actions.section.listEquipmentImage(equipment)}
							</div>
						`;
						$("#equipment").append(initImagesHtml);
						if (equipment == undefined) {
							$(".imagesEquipment").prepend(preview.views.emptyContent());
						}
						preview.events.clickModalEvent(".showModalEquipment");
					},
					showDomain : function () {
						// step franceTierslieux1522023_146_2
						// input multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q
						// #domain
						let domain = getAnswers?.franceTierslieux1522023_146_2?.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q;
						preview.actions.listCheckbox({}, "Domaines", "#domain", false,"", (thisAnswerId != undefined) ? preview.views.modalButtonHtml("domainForm", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1522023_146_2") : "");
						let pictorialCharthtml = `
							<div class="domain-pictorial">
								${preview.actions.listPictorialChart(domain, ".domain-pictorial-chart")}
							</div>
						`;
						$("#domain").append(pictorialCharthtml);
						if (domain == undefined) {
							$(".domain-pictorial").prepend(preview.views.emptyContent())
						}
						preview.events.clickModalEvent(".domainForm");
					},
					showFrequentation : function (frequentationForm) {
						// step franceTierslieux1922023_1231_5
						// input franceTierslieux1922023_1231_5lebb61gowlj39f2a2e
						// #frequentation
						// array
						let frequentationSuplAnswer = {};
						let frequentationPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhrhuhp0xv1qqamf0i";
						preview.actions.listCheckbox({}, "Fréquentation", "#frequentation", true, "", frequentationForm["html"]);
						let percentageInitHtml = "";
						frequentationSuplAnswer = jsonHelper.getValueByPath(frequentationForm["getSuplForm"], frequentationForm["answerId"] + frequentationPath);
						if (frequentationSuplAnswer == undefined) {
							percentageInitHtml += preview.views.emptyContent();
						}
						if (frequentationForm["canShowContent"]) {
							// mylog.log("sdkvuhds", frequentationSuplAnswer);
							percentageInitHtml += `
								<div class="image-container">
									${preview.actions.section.listFrequentation(frequentationSuplAnswer)}
								</div>
							`;	
						}
						$("#frequentation").append(percentageInitHtml);
					},
					showAgeFrequentation : function (ageFrequentationForm) {
						// step franceTierslieux1922023_1231_5
						// input franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v
						// #ageFrequentation
						// 64649a64da37b44c5004e6e1 multiResultContainermultiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq

						let ageFrequentationSuplAnswer = {};
						let ageFrequentationPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq";

						preview.actions.listCheckbox({}, "Fréquentation par âge", "#ageFrequentation", true, "", ageFrequentationForm["html"]);
						let percentageInitHtml = "";					
						ageFrequentationSuplAnswer = jsonHelper.getValueByPath(ageFrequentationForm["getSuplForm"], ageFrequentationForm["answerId"] + ageFrequentationPath);
						if (ageFrequentationSuplAnswer == undefined) {
							percentageInitHtml += preview.views.emptyContent();
						}
						if (ageFrequentationForm["canShowContent"]) { 
							percentageInitHtml += `			
								<div class="image-container">
									${preview.actions.section.listFrequentation(ageFrequentationSuplAnswer, "age")}
								</div>
							`;		
						}
						$("#ageFrequentation").append(percentageInitHtml);

					},
					showEmployeesContract : function () {
						// step franceTierslieux1922023_1231_6
						// input multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt
						// #employeeContract
						// let employeeContract = getAnswers?.franceTierslieux1922023_1231_6?.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt
						preview.actions.listCheckbox({}, "Contrat des salariés", "#employeeContract", false, "", (thisAnswerId != undefined) ? preview.views.modalButtonHtml("contractEmployee", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1922023_1231_6") : "");
				
						let percentageInitHtml = `
							${(employeeContract == undefined) ? preview.views.emptyContent() : ""}
							<div class="image-container">
								${preview.actions.section.listContract(employeeContract)}
							</div>
						`;
						$("#employeeContract").append(percentageInitHtml);
						preview.events.clickModalEvent(".contractEmployee");
					},
					showGouvernance : function () {
						// step franceTierslieux1922023_1417_7
						// input multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes
						// #gouvernance		
						preview.actions.listCheckbox({}, "Gouvernance", "#gouvernance", false, "", (thisAnswerId != undefined) ? preview.views.modalButtonHtml("gouvernanceBtn", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1922023_1417_7") : "");
						
						let gouvCharthtml = `
							${(gouvernance == undefined) ? preview.views.emptyContent() : ""}
							<div>
								${preview.actions.listPictorialChart(gouvernance, ".domain-pictorial-chart", "gouv")}
							</div>
						`;
						$("#gouvernance").append(gouvCharthtml);
						preview.events.clickModalEvent(".gouvernanceBtn");
					},
					listTools: function () {
						//finderlesCommunsDesTierslieux10112022_1423_0ljb8xhl6zywb6rehocs
						//lesCommunsDesTierslieux10112022_1423_0
						let listToolsClass = "listToolsBtn";
						let listToolsForm = preview.actions.generateButton(listToolFormId, listToolFinder, listToolsClass);
						let listToolsSuplAnswer = {};
						let listToolsPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq";
						let listToolsHtml = ``;
						preview.actions.listCheckbox({}, "Liste des outils", "#numericUsage", false, "", listToolsForm["html"]);
						listToolSuplAnswer = jsonHelper.getValueByPath(listToolsForm["getSuplForm"], listToolsForm["answerId"] + ".answers");
						
						if (listToolsForm["canShowContent"]) {
							delete listToolSuplAnswer.lesCommunsDesTierslieux10112022_1423_0;
							listToolsHtml += `
								<div>
									${(Object.keys(listToolSuplAnswer).length == 0) ? preview.views.emptyContent() : ""}
									<ul class="listTools">
										<li class="divider"></li>
										${preview.actions.extractUniqueCriteria(listToolSuplAnswer)}
									</ul>
								</div>
							`;
						}
						$("#numericUsage").append(listToolsHtml);
					},
					showCreateValue: function (activityAndProduction, equipmentSuplmement) {
						preview.actions.listCheckbox({}, "Creation de valeur", "#createValue");
						let equipementCreateValue = equipmentSuplmement;
						let economicModelAnswer = jsonHelper.getValueByPath(equipementCreateValue["getSuplForm"], equipementCreateValue["answerId"] + ".answers.lesCommunsDesTierslieux1052023_949_0.multiCheckboxPluslesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw");
						let productionPercentage = jsonHelper.getValueByPath(activityAndProduction["getSuplForm"], activityAndProduction["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq");
						let ActivityPercentage = jsonHelper.getValueByPath(activityAndProduction["getSuplForm"], activityAndProduction["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3onic3xd6dvbjzxu");

						let createValueHtml = `
							<h4>Nombre équipement</h5>
							${(economicModelAnswer == undefined) ? preview.views.emptyContent() : ""}
							${equipementCreateValue["html"]}
							<div id="showRadarCreateValue" class='canvas-chart'></div>
							<h4>Pourcentage production</h5>
							${(ActivityPercentage == undefined) ? preview.views.emptyContent() : ""}
							${activityAndProduction["html"]}
							<div id="showRadarCreateValue1" class='canvas-chart'></div>
							<h4 margin-top-20" >Pourcentage acitité</h5>
							${(productionPercentage == undefined) ? preview.views.emptyContent() : ""}
							${activityAndProduction["html"]}
							<div id="showRadarCreateValue2" class='canvas-chart'></div>
						`;
						$("#createValue").append(createValueHtml);
						let equipmentNumber = 0;
						let chartData = {...preview.actions.getKeyAndTextSup(economicModelAnswer)};
						let chartData1 ={...preview.actions.getKeyAndTextSup(ActivityPercentage)}
						let chartData2 ={...preview.actions.getKeyAndTextSup(productionPercentage)}
						chartData = preview.actions.transformObjectToArray(chartData);
						chartData1 = preview.actions.transformObjectToArray(chartData1);
						chartData2 = preview.actions.transformObjectToArray(chartData2);
						preview.actions.showRadar('div #showRadarCreateValue', chartData);
						preview.actions.showRadar('div #showRadarCreateValue1', chartData1);
						preview.actions.showRadar('div #showRadarCreateValue2', chartData2);
					},
					showDistributionValue: function (amount) {
						preview.actions.listCheckbox({}, "Distribution de valeur", "#distValue", false, "", amount["html"]);
						let montant = jsonHelper.getValueByPath(amount["getSuplForm"], amount["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl16ek1pd92s71wm8");
						let createValueHtml = `
							${(montant == undefined && employeeContract == undefined) ? preview.views.emptyContent() : ""}
							<div id="showRadarDistributionVal" class=' radar-test canvas-chart'></div>
						`;
						$("#distValue").append(createValueHtml);
						chartData = { 
							...preview.actions.getKeyAndTextSup(montant),
							...preview.actions.getKeyAndTextSup(employeeContract)
						}
						chartData = preview.actions.transformObjectToArray(chartData)
						preview.actions.showRadar('div.radar-test', chartData);
					},
					showCaptureValue: function (valueCapture) {
						preview.actions.listCheckbox({}, "Capture de valeur", "#captValue", false, "", valueCapture["html"]);
						let capture = jsonHelper.getValueByPath(valueCapture["getSuplForm"], valueCapture["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr");
						let createValueHtml = `	
							${(capture == undefined) ? preview.views.emptyContent() : ""}
							<div id="showRadarCaptureValue"></div>
						`;
						$("#captValue").append(createValueHtml);
						let chartData = { 
							...preview.actions.getKeyAndTextSup(capture)
						}
						chartData = preview.actions.transformObjectToArray(chartData);
						preview.actions.showRadar('div #showRadarCaptureValue', chartData);
					},	
					showHealth : function (personFrequentation, equipementSatisfaction, activitySuplForm) {
						preview.actions.listCheckbox({}, "Sante du tiers lieux", "#tiersHealth");
						let personPercentage = preview.actions.personFrequentationPerc(
							personFrequentation["getSuplForm"], 
							"answers.evaluationlesCommunsDesTierslieux1752023_1312_0lhrhon89o6qyj512jp9",
							"answers.lesCommunsDesTierslieux1752023_1312_0.lesCommunsDesTierslieux1752023_1312_0liycoyb6rhah0gc8bt"
						);

						let satisfactionPercentage = preview.actions.equipmentSatisfaction(
							equipementSatisfaction["getSuplForm"], 
							"answers.yesOrNolesCommunsDesTierslieux1052023_949_0laazfcfrg2ze8nirm0f"
						);

						let totalCurrGenProdArr = [
							"Vente de produit manufacturés", 
							"Ateliers", 
							"Conseil, accompagnement, ingénierie de projet",
							"Formation professionnelle",
							"Location d’espaces",
							"Location de machines",
							"Offre d’hébergement"
						]
						
						let generateProductPrcnt =  preview.actions.generateProduct(
							activitySuplForm["getSuplForm"], 
							"answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr", 
							totalCurrGenProdArr,
							(generatedProductAns != undefined && !isNaN(generatedProductAns)) ? Number(generatedProductAns) : 0
						);

						let renderHtml = `
							<div class="row">
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Frequentation personnes</h5> 
									</div>
									${preview.views.showGauge("400","200", personPercentage)}
									<div class="health-btn">
										${personFrequentation['html']}
									</div>
								</div>
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Satisifaction des équipements</h5> 
									</div>
									${preview.views.showGauge("400","200", satisfactionPercentage)}
									<div class="health-btn">
										${equipementSatisfaction['html']}
									</div>
								</div>
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Adhérents</h5> 
									</div>
									${preview.views.showGauge("400","200", preview.actions.membershipPercentage(memberShip, 500))}
									<div class="health-btn">
										${(typeof thisAnswerId != "undefined") ? preview.views.modalButtonHtml("membershipBtn", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1922023_1231_5") : ""}
									</div>
								</div>
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Produits générés</h5> 
									</div>
									${preview.views.showGauge("400","200", generateProductPrcnt)}
									<div class="health-btn">
										${(typeof thisAnswerId != "undefined") ? preview.views.modalButtonHtml("generatedProduct", thisAnswerId, "63e0a8abeac0741b506fb4f7", "franceTierslieux1922023_1231_5") : ""}
									</div>
								</div>
							</div>
						`;
						$("#tiersHealth").append(renderHtml);
						preview.events.clickModalEvent(".membershipBtn");
						preview.events.clickModalEvent(".generatedProduct");
					},
					showResume : function (resumeShowForm) {
						let negativeValue = jsonHelper.getValueByPath(resumeShowForm["getSuplForm"], resumeShowForm["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu50gd93x5x1uva3xv");
						let positiveValue = jsonHelper.getValueByPath(resumeShowForm["getSuplForm"], resumeShowForm["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu51awl25edd4zlmsp");
						let chartBarData = {
							positive: positiveValue,
							negative: negativeValue
						}
						let initNegPosbar = `
							<div id="resumeBarContent">
								${(negativeValue == undefined && positiveValue == undefined) ? preview.views.emptyContent() : ""}
								<canvas id="resumeBar"></canvas>
							</div>						
						`;
						$("#resume").removeClass("col-xs-10").addClass("col-xs-12");
						preview.actions.listCheckbox({}, "Resume", "#resume", false, "", resumeShowForm["html"]);
						$('.tab-content').append(initNegPosbar)
						ctx = document.getElementById("resumeBar").getContext("2d");
						preview.actions.section.loadActivityChart(chartBarData, "bar", ctx);
					}
				}
			},
			actions : {
				load : function () {
					preview.actions.goToItemPage();
					preview.actions.clickInitial();
				},
				section : {
					loadActivityChart : function (chartData={}, chartType="", ctx) {
						var labels = Object.keys(chartData);
						var data = Object.values(chartData);
						var backgroundColors = preview.actions.getRandomActivityColors(labels.length);
						var chartConfig = {}
	
						if (chartType == "pie") {
							chartConfig = {
								type: chartType,
								data: {
									labels: labels,
									datasets: [{
										data: data,
										backgroundColor: backgroundColors
									}]
								},
								options: {
									responsive: true
								}
							}
						}

						if (chartType == "bar") { 	
							chartConfig =  {
								type: chartType,
								data: {
									labels: ['Data'],
									datasets: [
										{
										label: 'Positive Gain',
										backgroundColor: 'rgb(0, 131, 126)',
										data: [chartData.positive],
										barThickness: 40
										},
										{
										label: 'Negative Loss',
										backgroundColor: 'rgb(116, 31, 24)',
										data: [-chartData.negative],
										barThickness: 40
										}
									]
								},
								options: {
									responsive: true,
									scales: {
										x: {
											stacked: true
										},
										y: {
											stacked: true,
											ticks: {
												beginAtZero: true
											},
											gridLines: {
												display: false, 
											}
										}
									}
								}
							}
						}
						let chartJs = new Chart(ctx, chartConfig);	
					},
					loadPrincipleWordC : function (wordCdata) {
						$("#words").jQCloud(wordCdata, {
							delay: 100,
							fontSize: {
								from: 0.1,
								to: 0.04
							}
						});
					},
					domainPictorialChart : function (data, section="" ,rank=0) {
						if (typeof data.svgDivClass == "undefined") {
							data.svgDivClass = "";
						}
						if (data.percentage > 100) {
							data.percentage = 100;
						}
						if (data.percentage < 0) {
							data.percentage = 0;
						}
						let realPercent = (100 - data.percentage);
						let path = (typeof ftIconSvgPath[data.label] != "undefined") ? ftIconSvgPath[data.label] : ftIconSvgPath["bureauIcon"];
						let svgPictureHtml = `
							<div ${(data.svgDivClass == "") ? "" : "class='"+data.svgDivClass+"'"}> 
								<svg viewBox="0 0 220 200"  style="margin: ${(jsonHelper.pathExists("svgHeigth." + data.label + ".margin")) ? svgHeigth[data.label].margin : data.marginX}px ${data.marginY}px">
								<defs>
								<linearGradient height = "200" id="progress${section + rank}" x1="0" y1="1" x2="0" y2="0">
									<stop id="color1" offset="${data.percentage / 100}" stop-color="grey"/>
									<stop id="color2" offset="${data.percentage / 100}" stop-color="lightblue"/>
								</linearGradient>
								</defs>
									${data.percentage}%
								</text>
								<path fill="url(#progress${section + rank})" style="opacity:0.973" d="${path}"/>
								</svg>		
								<p class="text-center">${data.label} <b> (${data.percentage}%) </b> </p>
							</div>		
						`;
						return svgPictureHtml;
					},
					equipmentImage : function (label, image, Eqclass="") {
						let equipmentHtml = `
							<div ${(Eqclass == "") ? "class='col-md-4 text-center'": Eqclass}>
								<img src="${image}" alt="" >
								<p class="margin-top-10">${label}</p>
							</div>
						`;
						return equipmentHtml;
					},
					listEquipmentImage : function (data={}) {
						let equipmentImage = "";
						let resultHtml = "";

						if (Object.keys(data).length > 0) {
							$.each(data, function (equipmentKey, equipmentValue) {
								equipmentImage += preview.actions.section.equipmentImage(Object.keys(equipmentValue)[0], franceTiersLieuxImages[Object.keys(equipmentValue)[0]]);
							})
						}

						resultHtml += `
							<div class="row">
								${equipmentImage}
							</div>
						`;
						return resultHtml;
					},
					listFrequentation : function (data={}) {
						data = preview.actions.getKeyAndTextSup(data)
						let listHtml = "";
						if (Object.keys(data).length > 0) {
							$.each(data, function(datakey, dataValue) {
								listHtml+= preview.actions.genRepeatedImages(franceTiersLieuxImages[datakey], datakey, dataValue, dataValue + "%");
							})
						}	
						return listHtml;
					},
					listContract : function (data={}) {
						let totalTextSup = preview.actions.totalNumberContract(data);
						let listContractHtml = "";
						if (Object.keys(data).length > 0) {
							$.each(data, function(datakey, dataValue) {
								let percent = (isNaN(Object.values(dataValue)[0].textsup)) ? 0 : (Number(Object.values(dataValue)[0].textsup) / totalTextSup) * 100;
								listContractHtml += preview.actions.genRepeatedImages(franceTiersLieuxImages[Object.values(dataValue)[0].value], Object.values(dataValue)[0].value, percent, Object.values(dataValue)[0].textsup);
							})
						}

						return listContractHtml;
					}
				},
				goToItemPage : function () {
					$(".goToItemPage").attr("href","#@"+getOrganizations.slug);
				},
				listCheckbox : function (input={}, title="", dom="", isArray=false, descriptionText="", button="") {
					let listInputHtml = `
						<div class="title-btn" >
							<h3>${title}</h3>
							${button}
						</div>
						<p>${descriptionText}</p>
					`;
					if (Object.keys(input).length != 0) {
						listInputHtml += "<ul>"
						if (input != undefined) {
							$.each(input, function(key, value) {
								if (isArray) {
									listInputHtml += "<li>" + value + "</li>";
								} else {
									listInputHtml += "<li>" + Object.keys(value) + "</li>";
								}
							})
						} else {
							listInputHtml += "<li> Pas de résultat </li>";
						}
						listInputHtml += "</ul>"
					}
					$(dom).html(listInputHtml);
				},
				clickInitial : function () {
					$("[href='#menu0']").trigger("click");
				},
				getRandomActivityColors : function (count) {
					let colors = [];
					for (var i = 0; i < count; i++) {
						var color = '#' + Math.floor(Math.random() * 16777215).toString(16);
						colors.push(color);
					}
					return colors;
				},
				generateFilterObject : function (inputData={}, path="") {
					let objectValues = {};
					let objectResult = {};
					let filterChoice = "";
					if (Object.keys(inputData).length > 0) {
						objectValues = Object.values(inputData);
							$.each(objectValues, function (k, v) {
								filterChoice = Object.keys(v)[0];
									objectResult [filterChoice] = {
										label : filterChoice,
										field : path + "." + filterChoice + ".value"
									}
							})
					}
					return objectResult;
				},
				listPictorialChart : function (data={}, selector, section) {
					let pictorialData = {};
					let totalTextSup = (section == "gouv") ? preview.actions.totalTextSup(data) : 0;
					let percent = 0;
					pictorialData["width"] = 200;
					pictorialData["height"] = 200;
					pictorialData["marginY"] = 20;
					pictorialData["marginX"] = 10;
					pictorialData["svgDivClass"] = "col-md-4";

					// 
					let list = "";
					if (Object.keys(data).length > 0) {
						let domainResultLength = Object.keys(data).length;
						$.each(data, function(datakey, dataValue) {
							if (section == "gouv") {
								pictorialData["percentage"] = Number(Math.ceil((Number(Object.values(dataValue)[0].textsup) / totalTextSup) * 100));
							} else {
								// domainResultLength = ()
								
								pictorialData["percentage"] = (Number(Object.values(dataValue)[0].rank) / Number(domainResultLength)) * 100;
								pictorialData["percentage"] = 100 - Math.ceil(pictorialData["percentage"]) + 10;
								pictorialData["percentage"] = (pictorialData["percentage"] >= 100) ? 97 : (pictorialData["percentage"] <= 0) ? 7 : pictorialData["percentage"];
							}
							pictorialData["label"] = Object.keys(dataValue)[0];
							pictorialData["imageUrl"] = assetPath + "/images/franceTierslieux/icon_bureau.png";
							list += preview.actions.section.domainPictorialChart(pictorialData, section, Object.values(dataValue)[0].rank);
						})
					}

					let listHtml = `
						 <div class="row">
						 	${list}
						 </div>
					`;
					return listHtml;
				},
				genRepeatedImages : function (imageUrl, label, percent, textSup=0, isNumberUser=false) {
					var html = '<div class="image-container" style="display: flex; align-items: center; justify-content: space-between; margin-bottom: 30px">';
					html += '<div class=col-md-3>';
					html += `<span class="label margin-right-10" style="color: black; font-size: 15px;white-space: break-spaces;line-height: 1.5;"> ${label} ${(textSup != 0) ? " (<b>" + textSup + "</b>)" : ""} </span>`;
					html += '</div>';
					html += '<div class="image-repeat col-md-9">';
					var repetitions = (isNumberUser) ? textSup : Math.ceil((percent * 2) / 10);
					
					for (var i = 0; i < repetitions; i++) {
						html += '<img src="' + imageUrl + '" style="width: 6vh;">';
					}
					html += '</div>';
					html += '</div>';
					// if (textSup == 0 && percent == 0) 
					// 	return "";

					return html;
				},
				totalTextSup : function (data={}) {
					let totalResult = 0;
					if (Object.keys(data).length > 0) {
						$.each(data, function(datakey, dataValue) {
							totalResult = totalResult + Number(Object.values(dataValue)[0].textsup);
						})
					}
					return totalResult
				},
				getKeyAndTextSup : function (data={}, title="") {
					let finalObject = {};
					title = title.toUpperCase();
					let legendTitle = "";
					let numberValue = 0;
					if (Object.keys(data).length > 0) { 
						$.each(data, function(datakey, dataValue) {
							legendTitle = Object.values(dataValue)[0].value;
							if (!isNaN(Object.values(dataValue)[0].textsup)) {
								numberValue = Number(Object.values(dataValue)[0].textsup);
								finalObject[legendTitle] = numberValue;
							}
						})
					}
					return finalObject;
				},
				filterMap : function (dataFilter={},container="") {
					var paramsFilter= {
                            container : container,
                            urlData : baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7",
                            defaults:{
                                notSourceKey : true,
                                types:["answers"],
                                indexStep : 400,
                                forced : {
                                        "filters" : {
                                            "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" : "<?= $network ?>"
                                        }
                                    }
                            },
                    };
                    paramsFilter["filters"] = dataFilter;
					let filterSearch={};
                    filterSearch = searchObj.init(paramsFilter);
                    filterSearch.results.render = function(fObj, results, data) {
                        let markerData = [];
						if (Object.keys(results).length > 0) {
							$.each(results, function (k, v) {
								markerData.push(orgaObject[k])
							})
						}
						
						markerData = Object.assign({}, ...markerData);
                        preview.actions.showMap("#mapContainerTiers", markerData)
                    };
                    filterSearch.search.init(filterSearch);
				},
				showMap : function (container, data={}) {
                    let customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
                    let appMap = new CoMap({
                        zoom : 5,
                        container : container,
                        activePopUp : true,
                        mapOpt:{
                            zoomControl:true
                        },
                        activePreview : false,
                        mapCustom:customMap,
                    })        
					appMap.showLoader();
                    appMap.addElts(data);
                }, 
				showRadar : function (container='', dataContent=[]) {
					radarChart.defaultConfig.color = function() {};
					radarChart.defaultConfig.radius = 3;
					radarChart.defaultConfig.w = 600;
					radarChart.defaultConfig.h = 600;

					var data = [
						{
						className: 'default', 
						axes: dataContent
						}
					];

					var chart = radarChart.chart();
					var cfg = chart.config(); // retrieve default config
					var svg = d3.select(container).append('svg')
						.attr('width', "850")
						.attr('height', cfg.h + cfg.h / 4);
					svg.append('g').classed('single', 1).attr("transform", "translate(110, 90)").datum(data).call(chart);
				},
				transformObjectToArray : function (obj) {
					return Object.entries(obj).map(([axis, value]) => ({ axis, value }));
				},
				clickSectionHref: function(id) {
					let sectionId = '#' + $(id).parent().attr("id");
					return "[href='"+sectionId+"']";
				},
				answerModal : function (answerId="", formId="", step="") {
					localStorage.setItem("menuHref", $(".vertical-menu").find(".active a").attr("href"));
					localStorage.setItem("parentMenu", $(".vertical-menu").find(".active").parent().data("ref"));
					ajaxPost(null, baseUrl+'/survey/answer/index/id/'+answerId+'/form/'+formId+'/mode/w',
						{ url : window.location.href },
						function(data){
							<?php if (Authorisation::isElementAdmin($orgaId, Organization::COLLECTION, $_SESSION["userId"])) { ?> 
								// setTimeout(() => {	
								$(".form-answer-content").show()	
								$(".form-answer-content").html(data);
								$(".vertical-menu").hide();
								setTimeout(() => {	
									$("#wizardcontainer").prepend(`
										<div class="form-btn">
											<button style="color:white;" class="bg-main2 btn btn-return-form btn-default margin-top-20 margin-bottom-20" >
											<h6><i class="fa fa-arrow-circle-left"></i>  Retour</h6>
											</button>
										</div>
									`)
									
									$(".btn-return-form").click(function() {
										// $(".form-answer-content").html("");
										// $(".vertical-menu").fadeIn();
										showAllNetwork.loadPreview(
											getAnswers?.answerId, 
											"<?= $image ?>",
											name,
											organizationId,
											"tiersLieux",
											network,
											function () {
												setTimeout(() => {	
													if (localStorage.parentMenu != "") {
														$("a[data-sub='"+localStorage.parentMenu+"']").trigger('click');
														setTimeout(() => {	
															$("a[href='"+localStorage.menuHref+"']").trigger('click');
														}, 300);
													} else {
														$("a[href='"+localStorage.menuHref+"']").trigger('click');
													}
												}, 200);
											}
										)
										// alert();
									})
								}, 200);
								// $('.btn-close-preview').click()
								if (step != "") {
									showStepForm('#'+step);
								}	
							// }, 100);
							<?php } else { ?> 
								toastr.error("Vous n'avez pas l'accèss");
								// Mahefa
							<?php } ?>
						},"html");
				},
				getSuplementAnswers : function (formId="", params={}) {
					let results = {}
					if (formId != "") {
						ajaxPost(
							null,
							baseUrl + '/costum/francetierslieux/getallanswers/type/answers/form/' + formId + '/getForm/true',
							params,
							function (data) {   
								if (data.result) { 
									results = data;
								} 
							},
							null,
							"json",
							{ async: false }     
						);
					}
					return results;
				},
				getAnswerForm : function (path="", orgaId="", formId) {
					let paramAnswers = {
						show: [
							"answers"              
						],
						where : {
							[path] : 
								{
									"$exists" : true
								}
						}
					}
					let answerContent = preview.actions.getSuplementAnswers(formId, paramAnswers);
					return answerContent;
				},
				newAnswers : function (formId="", answerPath="") {
					let dataId = "";
					ajaxPost(
						null,
						baseUrl + `/survey/answer/newanswer/form/${formId}`,
						{
							"action": "new"
						},
						function (data) {
							// console.log(data)
							if (data['_id']) {
								let dataToSend = {
									id: data['_id']['$id'],
									collection: "answers",
									path: answerPath ,
									// arrayForm: false,
									value : {
										id: organizationId,
										name : "<?= $name ?>",
										type : "organizations"
									} 
								}
								dataId = data['_id']['$id'];
								dataHelper.path2Value(dataToSend, function (params) {});
							}
						},
						null,
						"json",
						{ async: false }  
					)	
					return dataId;				
				},
				generateButton : function (suplForm="", btnPath="", btnClass="", formName="") {
					let button = {};
					let getSuplForm = preview.actions.getAnswerForm (
						btnPath + "." + organizationId, 
						organizationId, 
						suplForm
					);
					let initSuplFormData = getSuplForm;
					getSuplForm = getSuplForm.allAnswers;
					if (Object.keys(getSuplForm).length > 0) {
						let resultAnswerId = Object.keys(getSuplForm)[0];
						let btnColor = preview.actions.setButtonColor(getSuplForm[resultAnswerId].answers,initSuplFormData.inputs, formName, initSuplFormData.form.subForms);
						button["html"] = preview.views.modalButtonHtml(btnClass, resultAnswerId, suplForm);
						button["htmlWhithName"] = preview.views.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", formName, btnColor);
						button["getSuplForm"] = getSuplForm;
						button["answerId"] = resultAnswerId;
						button["canShowContent"] = true;
					} else {
						let newResultAnswerId = preview.actions.newAnswers(
							suplForm,
							btnPath + "." + organizationId
						); 
						button["html"] = preview.views.modalButtonHtml(btnClass, newResultAnswerId, suplForm);
						button["htmlWhithName"] = preview.views.modalButtonHtml(btnClass, newResultAnswerId, suplForm, "", formName);	
						button["canShowContent"] = false;									
					}

					if (btnClass != "")
						setTimeout(() => {
							preview.events.clickModalEvent("." + btnClass);
						}, 100);
							
					return button;
				},
				extractUniqueCriteria : function (contentAnswer) {
					let criteriaArray = "";
					for (var key in contentAnswer) {
							if (contentAnswer.hasOwnProperty(key)) {
							var criteriaObj = contentAnswer[key];
							for (var criteriaKey in criteriaObj) {
								if (criteriaObj.hasOwnProperty(criteriaKey)) {
									var criteria = criteriaObj[criteriaKey].criteria;
									let defaultImage = parentModuleUrl+"/images/default_tool.png";
									let urlImageTool = (jsonHelper.pathExists("toolsImageUrl."+criteria.toLowerCase().replace(/\s/g, "")+".imageUrl")) ? (toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.imageUrl == "") ? defaultImage : toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.imageUrl : defaultImage;
									let hrefImageTool = (jsonHelper.pathExists("toolsImageUrl."+criteria.toLowerCase().replace(/\s/g, "")+".url")) ? toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.url : "";
									criteriaArray += '<li class="col-xs-4"><a href="'+hrefImageTool+'" target="_blank"><img src="'+urlImageTool+'" alt="" style="width: 15vh;"><p>'+criteria+'</p></a></li>'
								}
							}
						}
					}

					return criteriaArray;
				},
				personFrequentationPerc : function (data={}, path="", day="") {
					let result = 0;
					if (data != null && data != undefined && Object.keys(data).length > 0 && path != "" && day != "") {
						let evaluationContent = jsonHelper.getValueByPath(Object.values(data)[0], path);
						let days = jsonHelper.getValueByPath(Object.values(data)[0], day)
						let count = 0;
						if (evaluationContent != undefined && days != undefined && days.length > 0) {
							let totalScore = days.length * 4;
							$.each(days, function(k, v) {
								if (evaluationContent[v] != undefined) {
									count = count + Number(Object.keys(evaluationContent[v])[0]);
								}
							})
							result = (count / totalScore) * 100
						}
					}
					return result;
				},
				equipmentSatisfaction : function (data = {}, path="") {
					let result = 0;
					if (Object.keys(data).length > 0 && path != "") {
						let answerContent = jsonHelper.getValueByPath(Object.values(data)[0], path);
						let allRate = 0;
						if (answerContent != undefined) {
							let ratingLength = Object.keys(answerContent).length * 5;
							$.each(answerContent, function (k, v) {
								if (v.note != undefined) {
									allRate = allRate + Number(v.note);
								}
							})
							result = (allRate / ratingLength) * 100
						}
					}
					return result;
				},
				membershipPercentage : function (number=0, numberMax=0) {
					let result = 0;
					if (number != undefined && number > 0 && Number(number) < numberMax) {
						if (number >= 500) {
							return 100;
						} else {
							result = (Number(number) / 500) * 100
						}
					}
					return result;
				},
				generateProduct : function (data={}, path="", arrayctReference=[], numberRef=0) {
					let generateProductRes = 0;
					let totalCurrGenProd = 0;
					if (arrayctReference.length > 0  && Object.keys(data).length > 0 && path != "") {
						let generateProductContent = jsonHelper.getValueByPath(Object.values(data)[0], path)
						if (generateProductContent != undefined) {
							$.each(generateProductContent, function(k,v) {
								if (arrayctReference.includes(Object.keys(v)[0]) && Object.values(v)[0].textsup != undefined && !isNaN(Object.values(v)[0].textsup)) {
									totalCurrGenProd = totalCurrGenProd + Number(Object.values(v)[0].textsup);
									
								}
							})
						}
						
						generateProductRes = (totalCurrGenProd > numberRef) ? 100 : (totalCurrGenProd == 0 && numberRef == 0) ? 0 : (totalCurrGenProd / numberRef) * 100;
						generateProductRes = (generateProductRes < 0) ? 0 : (generateProductRes > 100) ? 100 : generateProductRes;
					}
					
					return generateProductRes;
				},
				setButtonColor : function (answers={}, subFormsArrays=[], name, subFrmArray=[]) {
					// mylog.log("sdivuhdsio", answers);
					let countFormParams = 0;
					let countAnswers = 0;
					if (Object.keys(answers).length > 0) {
						$.each(answers, function(k, v) {
							if (subFrmArray.includes(k)) {
								countAnswers = countAnswers + Object.keys(v).length;
							} else {
								if (k != "") {
									countAnswers = countAnswers + 1;
								}
							}
						})
					}
					if (subFormsArrays.length > 0) {
						$.each(subFormsArrays, function(k, v) {
							let eachInputLngth = Object.keys(v).length;
							countFormParams = countFormParams + eachInputLngth;
						})
					}
					return (countAnswers == countFormParams) ? "green" : "red";
				},
				totalNumberContract : function (data={}) {
					let totalCount = 0
					if (Object.keys(data).length > 0) {
						$.each(data, function (k, v) {
							if (typeof v && Object.values(v) != undefined && Object.values(v).length > 0 && Object.values(v)[0] != undefined) {
								if (!isNaN(Object.values(v)[0].textsup)) {
									totalCount = totalCount + Number(Object.values(v)[0].textsup);
								}
							}
						})
					}
					return totalCount;
				}
			}
		}
		preview.init();
	})
</script>