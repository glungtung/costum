<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	options : {
	 		tags : {
	 			verb : '$all'
	 		}
	 	},
	 	// loadEvent : {
	 	// 	default : "scroll"
	 	// },
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true,
            map : {
            	active : true 
            }
        },
	 	defaults : {
	 		indexStep : 0,
	 		types : ["organizations"]
	 	},
	 	filters : {
	 		scope :true,
	 		scopeList : {
	 			params : {
	 				countryCode : ["FR","RE","MQ","GP","GF","YT"], 
	 				level : ["3"]
	 			}
	 		},
	 		typePlace : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Type",
	 			event : "tags",
	 			list : costum.lists.typePlace
	 		},
	 		services:{
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Services",
	 			event : "tags",
	 			list : costum.lists.services
	 		},
	 		// greeting : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Accueil",
	 		// 	event : "tags",
	 		// 	list : costum.lists.greeting
	 		// },
	 		manageModel : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Portage",
	 			event : "tags",
	 			list : costum.lists.manageModel
	 		},
	 		// state : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Etat",
	 		// 	event : "tags",
	 		// 	list : costum.lists.state
	 		// },
	 		spaceSize : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Taille",
	 			event : "tags",
	 			list : costum.lists.spaceSize
	 		},
	 		
	 		compagnon : {
	 			view : "tags",
	 			type : "tags",
	 			name : "Compagnons",
	 			event : "tags",
	 			list : costum.lists.compagnon
	 		},
	 		certification : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Lauréats Fabriques",
	 			event : "tags",
	 			list : costum.lists.certification
	 		},
	 		network : {
	 			view : "tags",
	 			type : "category",
	 			name : "Réseaux",
	 			event : "selectList",
	 			field : "category"
	 		}
	 	}
	};
	 
	//function lazyFilters(time){
	  //if(typeof searchObj != "undefined" )
	    //filterGroup = searchObj.init(paramsFilter);
	  //else
	    //setTimeout(function(){
	      //lazyFilters(time+200)
	    //}, time);
	//}
var filterSearch={};
	jQuery(document).ready(function() {
		  filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		if(!($("#menuRight").find("a").hasClass("changelabel"))){
			$("#menuRight").find("a").addClass("changelabel")
		}
  
		$("#menuRightmapContent").hide();	
		$(".BtnFiltersLieux").show();
	//	lazyFilters(0);
		
	});

</script>



