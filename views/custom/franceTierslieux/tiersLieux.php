<?php 
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/js/franceTierslieux/answerDirectory.js",
		"/js/appelAProjet/aap.js"
    );
	$formId = "63e0a8abeac0741b506fb4f7";
    $answerStep = "franceTierslieux2022023_753_10";
    $answerInput = "franceTierslieux2022023_753_10lemfa0njory1oxpszb";
    $getOptions = PHDB::distinct( Answer::COLLECTION,"answers.".$answerStep.".".$answerInput."",[ "form" => $formId ]) ?? [];
	$formInputs = FranceTierslieux::getInputsOrElement(Form::COLLECTION,$formId);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( "costum" )->getAssetsUrl());
?>

<style>
	.ans-dir .bodySearchContainer, .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }
</style>

<div class="tiers-lieux-page">
	<div class="padding-bottom-20 padding-left-0 padding-right-0 ans-dir" style="padding: 0% 10%"></div>
</div>

<script>   
$(function(){
    const arrayToObject = (arr) => {
        return arr.reduce(function(obj, key) {
            obj[key] = key;
            return obj;
        }, {});
    };
    showAllNetwork.paramsFilters.filters['networkFilter'] = {
        name : "filtre par réseau",
        view : "dropdownList",
        type : "filters",
        field : "answers.<?= $answerStep ?>.<?= $answerInput ?>",
        event : "filters",
        keyValue : false,
        list : arrayToObject(<?= json_encode($getOptions) ?>)
    }
	ajaxPost(
		null,
		baseUrl+"/"+moduleId+"/element/get/type/forms/id/63e0a8abeac0741b506fb4f7",
		{},
		function (data) {   
			if (data.result) {
				aapObject["formConfig"] = data.map;
				aapObject["formParent"] = data.map;
			} 
		},
		null,
		"json",
		{ async: false }     
	);
	aapObject["formInputs"] = <?= json_encode($formInputs) ?>;

	let filterBuilded = {
	};
	if(aapObject.formParent.params) {
		if(aapObject.formParent.params.filterParams) {
			var filterParamsIter = 0;
			for(const [key, value] of Object.entries(aapObject.formParent.params.filterParams)) {
				if(key.toLocaleLowerCase().includes('text')) {
					filterBuilded[key] = {
						view : 'text',
						field : value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
						event : "text",
						placeholder : trad.search + ` ${trad.by} `+ value["labelFor"+ key.charAt(0).toUpperCase() + key.substring(1)]
					}
				}
				if(key.toLocaleLowerCase().includes('select')) {
					let listValue = [];
					if(aapObject.formParent.params[value["paramsKeyFor"+ key.charAt(0).toUpperCase() + key.substring(1)]]) {
						const keyUtil = key.charAt(0).toUpperCase() + key.substring(1);
						if(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["list"] || aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["options"]) 
							listValue = Object.values(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]])[0];
						if(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["global"]) {
							listValue = Object.values(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["global"])[0];
						}
					}
					const inputKeys = value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)].split(".");
					let inputType;
					if(aapObject.formInputs[inputKeys[1]]) {
						const inputData = Object.values(aapObject.formInputs[inputKeys[1]])[0]["inputs"];
						if(inputData[inputKeys[2]])
							inputType = inputData[inputKeys[2]].type
					}

					filterBuilded[key] = {
						view : 'accordionList',
						type : "filters",
						field : value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
						event : "filters",
						name : value["labelFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
						keyValue : inputType == "tpls.forms.select" ? false : true,
						list : listValue
					}
					// if(inputType == "tpls.forms.cplx.checkboxNew") filterBuilded[key]["multiple"] = true;
					mylog.log("input type", inputType)
					mylog.log("builded filter", filterBuilded)
				}
				if(filterParamsIter == 0) {
					if(Object.keys(filterBuilded)[0].includes("text")){
						delete filterBuilded.usersFilter
						filterBuilded.usersFilter = {}
					} else {
						filterBuilded = { usersFilter: {}, ...filterBuilded}
					}
				}
				filterParamsIter++
			}
		} else {
			filterBuilded.usersFilter = {}
		}
	}
	filterBuilded.sortBy = {
		view : "accordionList",
		type : "sortBy",
		name : trad.sortBy,
		event : "sortBy",
		list : {
			"Date de création croissant" : {
				"created" : 1,
			},
			"Date de création décroissant" : {
				"created" : -1,
			},
			"Date de mise à jour croissant" : {
				"updated" : 1,
			},
			"Date de mise à jour décroissant" : {
				"updated" : -1,
			},					
		}
	}
	filterBuilded['networkFilter'] = {
        name : "filtre par réseau",
        view : "dropdownList",
        type : "filters",
        field : "answers.<?= $answerStep ?>.<?= $answerInput ?>",
        event : "filters",
        keyValue : false,
		class: "dshfvb",
        list : arrayToObject(<?= json_encode($getOptions) ?>),
    }

	filterBuilded['text'] = true;

	showAllNetwork.paramsFilters.filters.networkFilter = {
		name : "filtre par réseau",
		view : "dropdownList",
		type : "filters",
		field : "answers.<?= $answerStep ?>.<?= $answerInput ?>",
		event : "filters",
		keyValue : false,
		list : arrayToObject(<?= json_encode($getOptions) ?>),
	};
	showAllNetwork.paramsFilters.filters = filterBuilded;

    showAllNetwork.paramsFilters.defaults.textPath = ["answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb"];
    showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7/filterOrga/true";
    showAllNetwork.paramsFilters.results.renderView = "directory.modalPanel";
    showAllNetwork.showElements();
	$('.tiers-lieux-page .tiersLieuxContent #filterContainerInside').removeClass('container-filters-menu');
	$('.tiers-lieux-page .tiersLieuxContent #filterContainerInside').addClass('tiers-lieux-filter');
	$('.tiers-lieux-page .tiersLieuxContent #filterContainercacs').detach().insertBefore('.tiersLieuxContent').attr("style","margin-right: 118px;");
	$('.tiers-lieux-page #filterContainercacs').addClass("col-md-2");
	$('.tiers-lieux-page .searchBar-filters').attr('style', 'margin: 10px auto;');

	$('.tiers-lieux-page .tiers-lieux-filter').prepend(`
		<div class="another-filters" style="display: flex; justify-content: space-between !important;">
			<h4>plus de filtre</h4>
			<a href="javascript:;" class="openFilterParams" style="height: fit-content; left: -16px; transform: scale(0.8);">
				<i class="fa fa-gear fa-2x"></i> 
			</a>
		</div>
	`)
	$('.tiers-lieux-page .networkFilter').parent().parent().parent().prepend(`
	 	<div class='dropdown-network-filter-title'>
	 		<h4 >Filtre par réseau </h4>
	 	</div>
	`);
	$('.tiers-lieux-page .networkFilter').parent().parent().parent().addClass('network-filter-content');

	$('.tiers-lieux-page [data-field="text"]').parent().before(`
	 	<div class='tiers-search-filter-content'>
	 		<h4 class="tiers-search-filter-title" >Rechercher </h4>
	 	</div>
	`);
	$('.tiers-lieux-page [data-field="text"]').parent().insertAfter(".tiers-search-filter-title")
	$('.tiers-lieux-page [data-field="text"]').parent().attr('style','');

	$('.tiers-lieux-page .network-filter-content').insertBefore('.another-filters');
	$('.tiers-lieux-page .tiers-search-filter-content').insertBefore('.network-filter-content');
	$('.tiers-lieux-page .tiers-search-filter-content').attr("style", "margin-top: 10px;")

	$(".tiers-lieux-page .openFilterParams").off().on("click", function () {
		var inputsText = {};
		var inputsSelect = {};
		let dataInputs = {};
		let dataParamsInputs = {};
		const inputSelectType = ["tpls.forms.cplx.radioNew", "tpls.forms.cplx.checkboxNew", "tpls.forms.select"];
		const paramsKeys = aapObject.formParent.params ? Object.keys(aapObject.formParent.params) : [];
		if (aapObject.formInputs) {
			for (const [index, elem] of Object.entries(aapObject.formInputs)) {
				const key = Object.keys(elem)[0];
				if (elem[key].inputs) {
					for (const [k, v] of Object.entries(elem[key].inputs)) {
						dataInputs[k] = index;
						if (v.type) {
							if (v.type == "text") {
								inputsText[k] = v.label;
							}
							if (inputSelectType.indexOf(v.type) > -1) {
								inputsSelect[k] = v.label;
								const indexFinded = paramsKeys.findIndex(e => e.includes(k));
								const inputParamsKey = indexFinded > -1 ? paramsKeys[indexFinded] : "";
								inputParamsKey != "" ? dataParamsInputs[k] = inputParamsKey : "";
							}
						} else {
							inputsText[k] = v.label;
						}
					}
				}
			}
		}
		mylog.log("dakvh",inputsText)
		sectionDyfFilter = {
			"jsonSchema": {
				"title": trad.configureFilter,
				"icon": "cog",
				"text": "Sélectionnez la réponse d'un champ text pour filtrer par titre",
				"properties": {
					labelForText: {
						inputType: "text",
						label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter,
						placeholder: trad.EnterText + " " + trad.for + " " + trad.textFilter,
						rules: {
							required: true
						},
						value: aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (
							aapObject.formParent.params.filterParams.text ? aapObject.formParent.params.filterParams.text.labelForText : null
						) : null) : null,
					},
					inputForText: {
						inputType: "select",
						label: trad.SelectField + " " + trad.for + " " + trad.textFilter,
						placeholder: trad.SelectField + " " + trad.for + " " + trad.textFilter,
						noOrder: true,
						options: inputsText,
						rules: {
							required: true
						},
						value: aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (
							aapObject.formParent.params.filterParams.text ? aapObject.formParent.params.filterParams.text.inputForText.split('.')[2] : null
						) : null) : null
					}
				},
				onLoads: {},
				save: function () {
					let dataToSend = {
						id: aapObject.formParent["_id"]["$id"],
						path: "params.filterParams",
						collection: 'forms',
						value: {
						},
						costumEditMode : true,
					}
					$.each(sectionDyfFilter.jsonSchema.properties, function (k, val) {
						if ($("#" + k).val()) {
							const keyForGroup = k.substring(k.indexOf('For') + 3).toLocaleLowerCase();
							if (!dataToSend.value[keyForGroup]) dataToSend.value[keyForGroup] = {};
							switch (k) {
								case "inputFor" + keyForGroup.charAt(0).toUpperCase() + keyForGroup.substring(1): {
									if (dataInputs[$("#" + k).val()])
										dataToSend.value[keyForGroup][k] = 'answers.' + dataInputs[$("#" + k).val()] + '.' + $("#" + k).val();
									if (dataParamsInputs[$("#" + k).val()])
										dataToSend.value[keyForGroup]["paramsKeyFor" + keyForGroup.charAt(0).toUpperCase() + keyForGroup.substring(1)] = dataParamsInputs[$("#" + k).val()]
								} break;

								default: dataToSend.value[keyForGroup][k] = $("#" + k).val();
									break;
							}
						}
					});

					mylog.log("save tplCtx", dataToSend);
					if (typeof dataToSend.value == "undefined") {
						toastr.error('value cannot be empty!');
					} else {
						dataHelper.path2Value(dataToSend, function (params) {
							urlCtrl.loadByHash(location.hash)
						});
					}

				}
			}
		};
		sectionDyfFilter.jsonSchema.properties = aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (Object.keys(aapObject.formParent.params.filterParams).length > 0 ? {} : sectionDyfFilter.jsonSchema.properties) : sectionDyfFilter.jsonSchema.properties) : sectionDyfFilter.jsonSchema.properties;
		if (aapObject.formParent.params) {
			if (aapObject.formParent.params.filterParams) {
				for (const [key, value] of Object.entries(aapObject.formParent.params.filterParams)) {
					for (const [k, v] of Object.entries(value)) {
						let label = trad.textFilter;
						!key.includes("text") ? label = trad.selectFilter : "";
						if (k.includes("label")) {
							sectionDyfFilter.jsonSchema.properties[k] = {
								inputType: "text",
								label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + label,
								placeholder: trad.EnterText + " " + trad.for + " " + label,
								value: v
							}
						} else if (k.includes("input")) {
							sectionDyfFilter.jsonSchema.properties[k] = {
								inputType: "select",
								label: trad.SelectField + " " + trad.for + " " + " " + label,
								placeholder: trad.SelectField + " " + trad.for + " " + label,
								options: key.includes("text") ? inputsText : inputsSelect,
								value: v.split(".")[2]
							}
						}
					}
				}
			}
		}
		sectionDyfFilter.jsonSchema.onLoads.onload = function () {
			let objectForArrayGroup = {};
			for (const [key, value] of Object.entries(sectionDyfFilter.jsonSchema.properties)) {
				if (key.includes('label') || key.includes('input')) {
					const keyForGroup = key.substring(key.indexOf('For') + 3);
					if (!objectForArrayGroup[keyForGroup]) {
						objectForArrayGroup[keyForGroup] = {
							groupType: keyForGroup.includes("ext") ? trad.text.charAt(0).toUpperCase() + trad.text.slice(1) : trad.selection.charAt(0).toUpperCase() + trad.selection.slice(1),
							inputToGroup: []
						};
					}
					objectForArrayGroup[keyForGroup].inputToGroup.push(key + value.inputType);
				}
			}
			for (const [key, value] of Object.entries(objectForArrayGroup)) {
				alignInput2(value.inputToGroup, key, 6, 12, null, null, value.groupType, "#3f4e58", "");
				$(".fieldset" + key).append(`<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-propertiesToRemove="labelFor${key},inputFor${key}" data-inputSelector=".fieldset${key}"><i class="fa fa-times"></i></button>`);
			}

			$(`
				<div class="form-group mb-0 d-flex justify-content-center flex-xs-column filter-actions-group">
					<button type="button" class="btn btn-xs btn-primary addTextFilter">${trad.Add + " " + trad.textFilter}</button>
					<button type="button" class="btn btn-xs btn-info addSelectFilter">${trad.Add + " " + trad.selectFilter}</button>
				</div>
			`).insertBefore(".form-actions");
			let inputsTextOptions = "";
			for (const [key, value] of Object.entries(inputsText)) {
				inputsTextOptions += `<option value="${key}" data-value="">${value}</option>`;
			}
			let inputsSelectOptions = "";
			for (const [key, value] of Object.entries(inputsSelect)) {
				inputsSelectOptions += `<option value="${key}" data-value="">${value}</option>`;
			}
			$(".addTextFilter").off().on("click", function () {
				const fieldSet = "Text" + Object.keys(sectionDyfFilter.jsonSchema.properties).length + Math.random().toString(36).substring(4);
				let fieldSets = $(this).parent().parent().find(".fieldset").toArray();
				let lastField = fieldSets.length > 0 ? fieldSets[fieldSets.length - 1] : "#ajaxFormModal .errorHandler";
				$(`
					<div class="col-xs-12 fieldset fieldset${fieldSet}">
						<div class="labelFor${fieldSet}text col-xs-12 col-md-6 col-md-offset-null">
							<label class="col-xs-12 text-left control-label no-padding" for="labelFor${fieldSet}">
								<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter}
							</label>
							<input type="text" class="form-control " name="labelFor${fieldSet}" id="labelFor${fieldSet}" placeholder="${trad.EnterText + " " + trad.for + " " + trad.textFilter}">
						</div>
						
						<div class="inputFor${fieldSet}select col-xs-12 col-md-6 col-md-offset-null">
							<label class="col-xs-12 text-left control-label no-padding" for="inputFor${fieldSet}">
								<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.SelectField + " " + trad.for + " " + trad.textFilter}
							</label>
							<select class="form-control   " placeholder="${trad.SelectField + " " + trad.for + " " + trad.textFilter}" name="inputFor${fieldSet}" id="inputFor${fieldSet}" style="width: 100%;height:auto;" data-placeholder="${trad.SelectField + " " + trad.for + " " + trad.textFilter}">
								<option class="text-red" style="font-weight:bold" disabled="" selected="">${trad.SelectField + " " + trad.for + " " + trad.textFilter}</option>
								${inputsTextOptions}
							</select>
						</div>
						<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-inputselector=".fieldset${fieldSet}">
							<i class="fa fa-times"></i>
						</button>
					</div>
				`).insertAfter(lastField).hide().fadeIn(300);
				styleWrapped = `
					.modal-content .fieldset${fieldSet}:before {
						content: "${trad.text.charAt(0).toUpperCase() + trad.text.slice(1)}";
						color: #3f4e58;
						position: absolute;
						top: -15px;left: 14px;
						background: white;
						font-size: medium;
						font-weight: bold;
						padding: 0 15px;
					}
				`;
				$("head style").last().append(styleWrapped);
				sectionDyfFilter.jsonSchema.properties["labelFor" + fieldSet] = {
					inputType: "text",
					label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter,
					placeholder: trad.EnterText + " " + trad.for + " " + trad.textFilter,
					value: ""
				};
				sectionDyfFilter.jsonSchema.properties["inputFor" + fieldSet] = {
					inputType: "select",
					label: trad.SelectField + " " + trad.for + " " + trad.textFilter,
					placeholder: trad.SelectField + " " + trad.for + " " + trad.textFilter,
					options: inputsText,
					value: ""
				}
			});
			$(".addSelectFilter").off().on("click", function () {
				const fieldSet = "Select" + Object.keys(sectionDyfFilter.jsonSchema.properties).length + Math.random().toString(36).substring(4);
				let fieldSets = $(this).parent().parent().find(".fieldset").toArray();
				let lastField = fieldSets.length > 0 ? fieldSets[fieldSets.length - 1] : "#ajaxFormModal .errorHandler";
				$(`
					<div class="col-xs-12 fieldset fieldset${fieldSet}">
						<div class="labelFor${fieldSet}text col-xs-12 col-md-6 col-md-offset-null">
							<label class="col-xs-12 text-left control-label no-padding" for="labelFor${fieldSet}">
								<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.selectFilter}
							</label>
							<input type="text" class="form-control " name="labelFor${fieldSet}" id="labelFor${fieldSet}" placeholder="${trad.EnterText + " " + trad.for + " " + trad.selectFilter}">
						</div>
						
						<div class="inputFor${fieldSet}select col-xs-12 col-md-6 col-md-offset-null">
							<label class="col-xs-12 text-left control-label no-padding" for="inputFor${fieldSet}">
								<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.SelectField + " " + trad.for + " " + trad.selectFilter}
							</label>
							<select class="form-control   " placeholder="${trad.SelectField + " " + trad.for + " " + trad.selectFilter}" name="inputFor${fieldSet}" id="inputFor${fieldSet}" style="width: 100%;height:auto;" data-placeholder="${trad.SelectField + " " + trad.for + " " + trad.selectFilter}">
								<option class="text-red" style="font-weight:bold" disabled="" selected="">${trad.SelectField + " " + trad.for + " " + trad.selectFilter}</option>
								${inputsSelectOptions}
							</select>
						</div>
						<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-inputselector=".fieldset${fieldSet}">
							<i class="fa fa-times"></i>
						</button>
					</div>
				`).insertAfter(lastField).hide().fadeIn(300);
				styleWrapped = `
					.modal-content .fieldset${fieldSet}:before {
						content: "${trad.selection}";
						color: #3f4e58;
						position: absolute;
						top: -15px;left: 14px;
						background: white;
						font-size: medium;
						font-weight: bold;
						padding: 0 15px;
					}
				`;
				$("head style").last().append(styleWrapped);
				sectionDyfFilter.jsonSchema.properties["labelFor" + fieldSet] = {
					inputType: "text",
					label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.selectFilter,
					placeholder: trad.EnterText + " " + trad.for + " " + trad.selectFilter,
					value: ""
				};
				sectionDyfFilter.jsonSchema.properties["inputFor" + fieldSet] = {
					inputType: "select",
					label: trad.SelectField + " " + trad.for + " " + trad.selectFilter,
					placeholder: trad.SelectField + " " + trad.for + " " + trad.selectFilter,
					options: inputsSelect,
					value: ""
				}
			});
			$(document).on("click", ".removeFilterOption", function (e) {
				e.stopImmediatePropagation();
				const propertiesToRemove = $(this).attr("data-propertiesToRemove") ? $(this).attr("data-propertiesToRemove").split(",") : [];
				propertiesToRemove.forEach(e => {
					if (sectionDyfFilter.jsonSchema.properties[e]) delete sectionDyfFilter.jsonSchema.properties[e]
				})
				$($(this).attr("data-inputSelector")).fadeOut(300, function () {
					$(this).remove()
				});
			});
		}
		dyFObj.openForm(sectionDyfFilter);
    });
}) 				

</script>