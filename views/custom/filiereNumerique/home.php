<?php

    function count_distinct(&$array, $label){
        $exist = false;
        foreach ($array as $k => &$v) {
            if($v["label"]==$label){
                $v["number"]++;
                $exist = true;
            }
        }

        if(!$exist){
            array_push($array, array("label"=>$label, "number"=>1));
        }
    }

    $acteursId = array();
    $acteurs = array();
    
    $answers = array();
    $entreprises = array();
    $independants = array();
    $projects = array();

    $graphe_data = array();
    $carto_data = array();

    $is_member = false;
    //echo "This is the context id ==== ".$this->costum['contextId'];
    //echo "    This is the name ==== ".$this->costum['name'];
    // Get form parent
    $form = PHDB::find("forms", array("parent.".$this->costum['contextId'].".name"=>$this->costum['name']));
    $formId = "";
    
    foreach($form as $key => $value) { $formId = $key; }

    echo "This is the form ".$formId;
    
    // Get answers
    $answers = PHDB::find("answers", array("context.".$this->costum['contextId'].".name"=>$this->costum['name']));
    $myAnswer = "";
    foreach ($answers as $key => $value) {

        array_push($acteursId, $value["user"]);

        // Test if current user is a member
        if(Yii::app()->session["userId"] == $value["user"]){
            $is_member = true;
            $myAnswer = $value['_id']->{'$id'};
        }
        
        if(!empty($value["answers"])){
            $eliste = array();
            $cliste = array();
            
            if(isset($value["answers"]["entreprises"])){
                foreach ($value["answers"]["entreprises"] as $ek => $ev) {
                    array_push($eliste, $ek);
                }
            }

            if(isset($value["answers"]["friends"])){
                foreach ($value["answers"]["friends"] as $ck => $cv) {
                    array_push($cliste, $ck);
                }
            }

            $me = PHDB::findOneById("citoyens", $value["user"]);

            // Parrainage friends
            $pf = array();
            if(count($cliste)!=0){
                $pf = PHDB::findByIds("citoyens", $cliste);
            }

            // Parrainage organizations
            $pe = array();
            if(count($eliste)!=0){
                $pe = PHDB::findByIds("organizations", $eliste);
            }

            $besoins = array();
            if(isset($value["answers"]["filiereNumerique27102020_837_3"]["question31"])){
                $besoins = $value["answers"]["filiereNumerique27102020_837_3"]["question31"];
            }

            $ressources = array();
            if(isset($value["answers"]["filiereNumerique27102020_840_4"]["question41"])){
                $ressources = $value["answers"]["filiereNumerique27102020_840_4"]["question41"];
            }

            $acteurs[$value['user']] = array(
                        "citoyen" => $me,
                        "entreprises" => $pe,
                        "friends" => $pf,
                        "besoins" => $besoins,
                        "ressources" => $ressources,
                        "answers" => array(
                            "date" => $value["source"]["date"],
                            "presentation" => $value["answers"]["filiereNumerique27102020_82_0"]["question01"]["0"]
                            
                        )
                    );

            // Collect entreprises 
            if($value["answers"]["filiereNumerique27102020_82_0"]["question01"]["0"]["entity"]=="Entreprise"){
                array_push($entreprises, $value);
            }

            // Collect independants
            if($value["answers"]["filiereNumerique27102020_82_0"]["question01"]["0"]["entity"]=="Indépendant"){
                array_push($independants, $value);
                count_distinct($graphe_data, $value["answers"]["filiereNumerique27102020_82_0"]["question01"]["0"]["speciality"]);
            }

            // Collect carto data
            if(isset($value["answers"]["filiereNumerique27102020_82_0"]["question01"]["0"]["address"])){
                array_push($carto_data, $value["answers"]["filiereNumerique27102020_82_0"]["question01"]["0"]["address"]);
            }
            
            // Collect project data and its owner
            if(isset($value["answers"]["filiereNumerique27102020_834_1"]["question11"])){
                foreach ($value["answers"]["filiereNumerique27102020_834_1"]["question11"] as $pk => $pv) {
                    array_push($projects, array("project"=>$pv, "owner"=>$value["user"]));
                }
            }
        }
    }

    /*if(count($acteursId)==0){
        $acteurs = PHDB::find("citoyens", $acteursId);
    }*/
?>

<?php
    $cssAndScriptFilesModule = array(
        '/js/default/profilSocial.js'
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

    if($this->costum["contextType"] && $this->costum["contextId"]){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    }
?>

<style>
    .btn-actor {
        /*animation: corner 3s linear infinite;*/
        margin-top: -1.8em;
        border-radius: 40px;
        float: right;
        padding: 1em;
        padding-top: 0.8em;
    }

    h1{
        font-weight: 900 !important;
    }

    #bg-homepage{
        width: 101% !important;
    }
    
    .header{
        background: #001146;
        background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/filiereNumerique/mada-background.jpg);
        width: 101%;
        background-size: cover;
        padding-bottom: 22%;
        background-position: center;
        position: initial !important;
    }
    .logofn{
        width: 48%;
        margin-top: 5%;
        position: absolute;
        margin-left: 3%;
    }

    #mapfilierenumerique {
        width: 100%;
        margin-top: 10px;
        height: 400px;
    }

    .actu{
        margin-top:3%;
    }

    #newsstream .loader{
        display: none;
    }

    @media (max-width:768px){
        .plus-m {
        width : 5%;
        }
        .logofn{
            width: 100%;
            margin-top: 13%;
        }
    }

    .tabbable-panel {
        border:1px solid #eee;
        padding: 10px;
    }

    /* Default mode */
    .tabbable-line > .nav-tabs {
        border: none;
        margin: 0px;
    }
    .tabbable-line > .nav-tabs > li {
        margin-right: 2px;
    }
    .tabbable-line > .nav-tabs > li > a {
        border: 0;
        margin-right: 0;
        color: #737373;
    }
    .tabbable-line > .nav-tabs > li > a > i {
        color: #a6a6a6;
    }

    .tabbable-line > .nav-tabs > li {
        display: inline-block;
        color: #000;
        text-decoration: none;
    }

    .tabbable-line > .nav-tabs > li::after {
        content: '';
        display: block;
        width: 0;
        height: 4px;
        background: #00ff00;
        margin-bottom: -4px;
        transition: width .3s;
    }

    .tabbable-line > .nav-tabs > li:hover::after {
        width: 100%;
    }

    .tabbable-line > .nav-tabs > li.open {
        border-bottom: 4px solid #00ff00;
    }
    .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
        border: 0;
        background: none !important;
        color: #333333;
    }
    .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
        color: #a6a6a6;
    }
    .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
        margin-top: 0px;
    }
    .tabbable-line > .nav-tabs > li.active {
        border-bottom: 4px solid #00cc00;
        position: relative;
    }
    .tabbable-line > .nav-tabs > li.active > a {
        border: 0;
        color: #333333;
    }
    .tabbable-line > .nav-tabs > li.active > a > i {
        color: #404040;
    }
    .tabbable-line > .tab-content {
        margin-top: -3px;
        background-color: #fff;
        border: 0;
        border-top: 1px solid #eee;
        padding: 15px 0;
    }

    .btn-actor{
        margin-top: -1.8em;
        border-radius: 40px;
        float: right;
        padding: 1em;
        padding-top: 0.8em;
    }

    .portlet .tabbable-line > .tab-content {
        padding-bottom: 0;
    }

	.afn-card, .pfn-card{
		border: 1px solid #ddd;
		padding: 0.5em 1.5em 1.8em 1.5em;
        margin-bottom: 1.5em;
	}

    .pfn-card p{
        margin-bottom: 1.2em;
    }
/*
	.afn-card div{
		padding-left: 3em;
	}

    .afn-card p{
        padding-left: 2.5em;
    }
*/
    .effectif h5{
        margin-left: 0;
        padding-left: 1em;
        border-left: 6px solid #ccc;
    }

    .effectif h1{
        margin-left: 0;
        padding-left: 0.5em;
    }

    .text-success{
        color: #00cc00;
    }

    .action, .action:hover{
        border: 2px solid #ccc;
        border-radius: 20px;
        padding: 6px 10px;
        text-decoration: none
    }

    .action:hover, .action:focus{
        border: 2px solid #00cc00;
        text-decoration: none
    }


    #searchInput {
        background-image: url('/images/search.png'); 
        background-position: 15px center;  
        background-repeat: no-repeat;
        background-color: #f1f1f1;
        border-radius: 30px;
        font-size: 16px; 
        padding: 5px 20px 5px 40px; 
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 12px;
    }

    .badge{
        padding: 5px 10px 5px 10px;
    }
</style>

<style>
    a:hover, a:focus{
        text-decoration: none;
        outline: none;
    }
    #accordion .panel{
        border: none;
        background: none;
        border-radius: 0;
        box-shadow: none;
    }
    #accordion .panel-heading{
        padding: 0;
    }
    #accordion .panel-title a{
        display: block;
        font-size: 16px;
        color: #00cc00;
        padding: 17px 40px 17px 65px;
        background: #fff;
        border: 1px solid #ddd;
        border-bottom: none;
        position: relative;
        transition: all 0.5s ease 0s;
    }
    #accordion .panel-title a.collapsed{
        background: #fafafa;
        color: #777;
    }
    #accordion .panel-title a.collapsed:hover{
        color: #00cc00;
    }
    #accordion .panel-title a:after,
    #accordion .panel-title a.collapsed:after{
        content: "\f068";
        font-family: "FontAwesome";
        font-weight: 900;
        font-size: 15px;
        color: #fff;
        width: 45px;
        height: 100%;
        line-height: 54px;
        text-align: center;
        position: absolute;
        top: 0px;
        left: -1px;
        background: #00cc00;
        transition: all .2s;
    }
    #accordion .panel-title a.collapsed:after{
        content: "\f067";
        color: #777;
        background: #f4f4f4;
        transition: all 0.5s ease 0s;
    }
    #accordion .panel-title a.collapsed:hover:after{
        background: #00cc00;
        color:#fff;
    }
    #accordion .panel-title a:before{
        content: "";
        position: absolute;
        bottom: -11px;
        left: -1px;
        border-bottom: 12px solid transparent;
        border-right: 12px solid #00cc00;
    }
    #accordion .panel-title a.collapsed:before{
        display: none;
    }
    #accordion .panel-body{
        font-size: 14px;
        color: #777;
        line-height: 20px;
        margin-left: 12px;
        background: #fff;
        border: 1px solid #ddd;
        border-top: none;
    }
</style>

<div class="header row">
    <div class="logofn col-md-8 text-center">
        <!--Début svg logo --> 
        <!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
        <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 320 595.28 200.89" style="enable-background:new 0 0 595.28 841.89; filter: drop-shadow( 0px 0px 8px rgb(41, 40, 40));" xml:space="preserve">
        <style type="text/css">
            .st0{fill:#FFFFFF;}
            .st1{font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: lighter !important;}
            .st2{font-size:43.3313px;}
            .st3{font-family:'customFont';}
            .st4{fill:#00FF00;}
            .st5{font-family:'customFont';}
            .st6{font-size:26.4145px;}
            .st7{fill:#00CC00;}
            .st8{fill:#FFFFFF;}
            .st9{fill:#FF0000;}
            .st10{fill:#FFFFFF;}
        </style>
        <g>
            <g>
                <g>
                    <text transform="matrix(1 0 0 1 271.2626 403.6614)"><tspan x="0" y="10" class="st0 st1 st2">FILIÈRE</tspan><tspan x="0" y="57.99" class="st0 st3 st2">NUMÉRIQUE</tspan></text>
                    <g><text transform="matrix(1 0 0 1 400.971 492.166)" class="st4 st5 st6 fadeIn animated">Madagascar</text></g>
                </g>
            </g>
            <g>
                <g>
                    <polygon class="st10" points="137.1,420.94 210.21,512.22 210.21,329.67          "/>
                    <polygon class="st7" points="52.11,420.94 104.81,512.22 210.21,512.22 137.1,420.94          "/>
                    <polygon class="st8" points="210.21,329.67 210.21,512.22 262.91,420.94          "/>
                    <polygon class="st9" points="210.21,329.67 104.81,329.67 52.11,420.94 140.14,420.94             "/>
                </g>
                <g>
                    <path id="share_1_" class="st0" d="M195.31,406.78c-4.78,0-8.99-2.28-11.74-5.77c-9.83,4.92-19.68,9.83-29.52,14.76
                        c0.34,1.24,0.57,2.52,0.57,3.86c0,1.35-0.23,2.63-0.57,3.87c9.83,4.92,19.67,9.83,29.51,14.75c2.75-3.48,6.96-5.76,11.74-5.76
                        c8.28,0,14.99,6.71,14.99,14.98c0,8.28-6.71,15-14.99,15c-8.27,0-14.98-6.72-14.98-15c0-1.34,0.23-2.62,0.57-3.86
                        c-9.83-4.93-19.67-9.83-29.52-14.76c-2.75,3.49-6.96,5.78-11.74,5.78c-8.27,0-14.99-6.7-14.99-14.99
                        c0-8.27,6.72-14.98,14.99-14.98c4.78,0,8.99,2.28,11.74,5.77c9.83-4.91,19.67-9.83,29.52-14.74c-0.34-1.25-0.57-2.53-0.57-3.88
                        c0-8.27,6.71-14.99,14.98-14.99c8.28,0,14.99,6.72,14.99,14.99C210.3,400.06,203.59,406.78,195.31,406.78z"/>
                    <path id="share_3_" class="st0" d="M128.53,459.39c1.74-2.95,4.69-4.71,7.85-5.13c0.55-7.86,1.12-15.72,1.68-23.59
                        c-0.89-0.24-1.77-0.57-2.59-1.06c-0.84-0.49-1.54-1.1-2.18-1.76c-6.62,4.27-13.25,8.54-19.87,12.81c1.15,2.97,1.01,6.4-0.73,9.34
                        c-3.02,5.11-9.61,6.79-14.71,3.78c-5.11-3.02-6.8-9.62-3.78-14.72c3.02-5.1,9.61-6.78,14.72-3.76c0.83,0.49,1.53,1.1,2.17,1.76
                        c6.63-4.27,13.25-8.54,19.88-12.81c-1.15-2.97-1.02-6.4,0.72-9.35c3.02-5.1,9.61-6.8,14.72-3.77c5.1,3.02,6.79,9.61,3.77,14.72
                        c-1.75,2.95-4.69,4.71-7.84,5.13c-0.56,7.86-1.12,15.72-1.69,23.59c0.89,0.25,1.77,0.57,2.6,1.07c5.1,3.02,6.8,9.61,3.78,14.71
                        c-3.02,5.11-9.62,6.79-14.72,3.77C127.2,471.09,125.51,464.5,128.53,459.39z"/>
                    <path id="share_2_" class="st0" d="M117.72,396.82c1.29,2.5,1.22,5.33,0.13,7.71c5.23,3.83,10.45,7.67,15.69,11.5
                        c0.56-0.51,1.17-0.98,1.87-1.34c0.71-0.36,1.44-0.59,2.18-0.75c-0.07-6.48-0.14-12.96-0.21-19.44c-2.57-0.5-4.9-2.1-6.18-4.6
                        c-2.23-4.34-0.52-9.67,3.82-11.89c4.34-2.23,9.67-0.52,11.9,3.82c2.23,4.34,0.51,9.66-3.83,11.89c-0.7,0.36-1.44,0.59-2.18,0.74
                        c0.07,6.48,0.14,12.96,0.21,19.45c2.57,0.5,4.9,2.09,6.19,4.6c2.23,4.34,0.52,9.66-3.82,11.9c-4.34,2.23-9.66,0.51-11.89-3.82
                        c-1.29-2.51-1.22-5.33-0.14-7.71c-5.22-3.83-10.45-7.67-15.68-11.51c-0.57,0.51-1.17,0.98-1.88,1.34
                        c-4.34,2.23-9.67,0.52-11.89-3.82c-2.23-4.34-0.51-9.67,3.82-11.9C110.16,390.77,115.49,392.48,117.72,396.82z"/>
                </g>
            </g>
        </g>
    </svg>
    </div>
</div>

<div class="container">
    <?php //if(!$is_member && $formId!=""){ ?>
    <?php if($myAnswer!=""){ ?>
        <a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/filierenumerique#answer.index.id.<?= $myAnswer; ?>.mode.w" target="_blank" data-id="<?= $formId; ?>" class="btn btn-lg btn-primary btn-actor bg-default myAnswers"> Mettre à jour mes données</a>
    <?php }else{ ?>
        <a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/filierenumerique#answer.index.id.new.form.<?= $formId; ?>" target="_blank" class="btn btn-lg btn-primary btn-actor bg-default"> Rejoindre le filière</a>
    <?php } ?>
</div>

<br><br><br>
<!-- Carto Acteurs -->
<div class="container">
<div class="container">
    <div class="carto-n row">
        <div class="col-md-12">
            <!--div class="container">
                <form class="form-inline" action="/action_page.php">
                    <div class="form-group">
                        <label for="email">Debut :</label>
                        <input type="date" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Fin :</label>
                        <input type="date" class="form-control" id="pwd">
                    </div>
                    <button type="submit" class="btn btn-default">Ok</button>
                </form>
            </div>
            <br><br-->
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12 effectif">
                        <h5>Acteurs</h5>
                        <h1 class="text-success"><?= count($answers) ?></h1>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 effectif">
                        <h5>Entreprise</h5>
                        <h1 class="text-success"><?= count($entreprises) ?></h1>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 effectif">
                        <h5>Indépendant</h5>
                        <h1 class="text-success"><?= count($independants) ?></h1>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 effectif">
                        <h5>Projets</h5>
                        <h1 class="text-success"><?= count($projects) ?></h1>
                    </div>
                    <!--div class="col-md-2 col-sm-6 col-xs-12 effectif">
                        <h5>Evenements</h5>
                        <h1 class="text-success"><?= count($projects) ?></h1>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
<?php  if(isset($_SESSION["userId"])){ ?>
    <div class="container">
        <div class="carto-n row">
            <div class="col-md-12">
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_default_1" data-toggle="tab">
                                Liste des acteurs </a>
                            </li>
                            <li>
                                <a href="#tab_default_2" data-toggle="tab">
                                Liste des projets </a>
                            </li>
                            <li>
                                <a href="#tab_default_3" data-toggle="tab">
                                Actualités </a>
                            </li>
                            <li>
                                <a href="#tab_default_4" data-toggle="tab">
                                Graphique </a>
                            </li>
                            <li>
                                <a href="#tab_default_5" data-toggle="tab">
                                Cartographique </a>
                            </li>
                        </ul>
                        
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_default_1">
                                <div class="container">
                                    <br>
                                    <input type="text" class="float-right" id="searchInput" onkeyup="search()" placeholder="Recherche ...">
                                    <br><br>
                                    <div class="row" id="actors-list">
                                        <?php foreach ($acteurs as $id => $acteur) { ?>
                                            <div id="answer-list" class="col-md-5">
                                            <div class="afn-card">
                                                <h5>
                                                    <a href="<?php echo Yii::app()->createUrl("/#@".$acteur["citoyen"]["slug"]) ?>" target="_blank">
                                                        <img class="img-circle" src="<?= (isset($acteur["citoyen"]["profilImageUrl"]))?$acteur["citoyen"]["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" alt="Avatar" style="width:8%">
                                                        <b> &nbsp; <?=$acteur["citoyen"]["name"]?></b>
                                                    </a>
                                                </h5>
                                                <?php if($acteur["answers"]["presentation"]["entity"]=="Entreprise"){ ?>
                                                    <p><?=$acteur["answers"]["presentation"]["entity"]?></p>
                                                <?php }else{ ?>
                                                    <p>
                                                    <small></small> <span class="text-success"><?=$acteur["answers"]["presentation"]["speciality"]?></span>
                                                    </p>
                                                <?php } ?>
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingOne<?= $id ?>">
                                                            <h6 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?= $id ?>" aria-expanded="false" aria-controls="collapseOne<?= $id ?>">
                                                                    Présentation
                                                                </a>
                                                            </h6>
                                                        </div>
                                                        <div id="collapseOne<?= $id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne<?= $id ?>">
                                                            <div class="panel-body">
                                                            <?php if($acteur["answers"]["presentation"]["entity"]=="Entreprise"){ ?>
                                                                <p><?=$acteur["answers"]["presentation"]["organizationPropos"]?></p>
                                                            <?php }else{ ?>
                                                                <p><?=$acteur["answers"]["presentation"]["situation"]?></small></p>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingTwo<?= $id ?>">
                                                            <h6 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?= $id ?>" aria-expanded="false" aria-controls="collapseTwo<?= $id ?>">
                                                                    Parrainage ( <?php echo count($acteur["entreprises"]) + count($acteur["friends"]) ?> )
                                                                </a>
                                                            </h6>
                                                        </div>
                                                        <div id="collapseTwo<?= $id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo<?= $id ?>">
                                                            <div class="panel-body">
                                                            <?php if (count($acteur["entreprises"])==0 && count($acteur["friends"])==0) { ?>
                                                                <p class="text-light"><b>[ Aucun ]</b></p>
                                                            <?php } ?>
                                                            <?php if (count($acteur["entreprises"])!=0) { ?>
                                                                <p><b>Entreprise(s):</b></p><br>
                                                                <?php foreach($acteur["entreprises"] as $key => $value) { ?>
                                                                    <a href="<?php echo Yii::app()->createUrl("/#@".$value["slug"]); ?>" target="_blank" class="list-group-item"><?php echo $value["name"]; ?></a>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <br>
                                                            <?php if (count($acteur["friends"])!=0) { ?>
                                                                <p><b>Indépendant(s):</b></p><br>
                                                                <div class="list-group">
                                                                    <?php foreach($acteur["friends"] as $key => $value) { ?>
                                                                        <a href="<?php echo Yii::app()->createUrl("/#@".$value["slug"]); ?>" target="_blank" class="list-group-item"><?php echo $value["name"]; ?></a>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingThree<?= $id ?>">
                                                            <h6 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree<?= $id ?>" aria-expanded="false" aria-controls="collapseThree<?= $id ?>">
                                                                    Besoins
                                                                </a>
                                                            </h6>
                                                        </div>
                                                        <div id="collapseThree<?= $id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree<?= $id ?>">
                                                            <div class="panel-body">
                                                                <p><br>
                                                                <?php foreach ($acteur["besoins"] as $key => $value) {
                                                                    echo "<span class='badge'>".$value['liste_row']."</span>";
                                                                } ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingFour<?= $id ?>">
                                                            <h6 class="panel-title">
                                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour<?= $id ?>" aria-expanded="false" aria-controls="collapseFour<?= $id ?>">
                                                                    Ressources
                                                                </a>
                                                            </h6>
                                                        </div>
                                                        <div id="collapseFour<?= $id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour<?= $id ?>">
                                                            <div class="panel-body">
                                                                <p><br>
                                                                <?php foreach ($acteur["ressources"] as $key => $value) {
                                                                    echo "<span class='badge badge-lg'>".$value['liste_row']."</span>";
                                                                } ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--div>
                                                    <a href="javascript:;" class="action">VOIR</a>
                                                    &nbsp;&nbsp;
                                                    <small></small>
                                                    <a href="#parrainage<?= $id ?>" class="action" data-toggle="collapse">Parrainage</a>
                                                </div-->
                                                <div>
                                                    <div id="parrainage<?= $id ?>" class="collapse">
                                                        <?php foreach ($acteur["entreprises"] as $key => $value) { ?>
                                                        <?php print_r($value["name"]); ?>
                                                        <?php } ?>
                                                        <hr>
                                                        <?php foreach ($acteur["friends"] as $key => $value) { ?>
                                                        <?php print_r($value["name"]); ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" id="loadMoreAnswers">Afficher plus</button>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_default_2">
                                <br><br>
                                <div class="container">
                                    <div class="row">
                                        <?php foreach($projects as $p => $project) { ?>
                                        <div id="project-list" class="col-md-5">
                                            <div class="pfn-card">
                                                <h5>
                                                    <b><?=$project["project"]["projectName"]?></b>
                                                </h5>
                                                <div title="Propriétaire du projet">
                                                    <a href="<?php echo Yii::app()->createUrl("/#@".$acteurs[$project["owner"]]["citoyen"]["slug"]) ?>" target="_blank" class="text-success">
                                                        <img class="img-circle" src="<?= (isset($acteurs[$project["owner"]]["citoyen"]["profilImageUrl"]))?$acteurs[$project["owner"]]["citoyen"]["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" alt="Avatar" style="width:8%">
                                                        <b> &nbsp; <?= $acteurs[$project["owner"]]["citoyen"]["name"] ?></b>
                                                    </a>
                                                </div>
                                                <p class="project-desc">
                                                    <small><?=$project["project"]["projectDescription"]?></small>
                                                </p>
                                                <div>
                                                    <a href="javascript:;" class="action read-more">Lire davantage</a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" id="loadMoreProjects">Afficher plus</button>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_default_3">
                                <!-- Actualité -->
                                <div class="actu row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="actucard col-xs-12 col-sm-12 col-md-12">
                                            <div id="newsstream" style="width: 90%; margin-left: 6%;"></div>
                                        </div>
                                        <!--div class="col-xs-12 col-sm-12" style="text-align : center;">
                                            <a href="javascript:;" data-hash="#live" class="lbh-menu-app " style="text-decoration : none; font-size:2rem;">
                                                <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive plus-m" style="margin: 1%;"><br>
                                                Voir plus <br>
                                                d'actualités
                                            </a>
                                        </div-->
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_default_4">
                                <br><br>
                                <canvas id="bar-chart" width="800" height="250"></canvas>
                            </div>
                            <div class="tab-pane" id="tab_default_5">
                                <div id="mapfilierenumerique"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SCRIPT /home/dev-christon/dev/communecter/pixelhumain/ph-->
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script-->
<script src="/plugins/Chart-2.6.0/Chart.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
        urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

        ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
        
        if(notNull(currentUser)) currentUser.addressCountry = "Madagascar";

        setTitle("Filière Numérique Mada");
        
        contextData = {
            id : "<?php echo $this->costum["contextId"] ?>",
            type : "<?php echo $this->costum["contextType"] ?>",
            name : "<?php echo $el['name'] ?>",
            slug : "<?php echo $el['slug'] ?>",
            profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
        };

        mapParams = {
            zoom : 6,
            container : "mapfilierenumerique",
            tile : "maptiler",
            mapOpt:{
                latLon : ["-21.4546", "47.0875"],
            }
        };

        var zone = [];

        var contextDataCit = {
            name : "cit",
            type: "citoyens",
            geo: { "type": "Point", "latitude" : "-21.4546", "longitude" : "47.0875"}
        };

        var contextDataOrg = {
            name: "orga",
            type: "organizations",
            geo: {"type": "GeoCoordinates", "latitude" : "-21.4547", "longitude" : "47.1875"}
        };

        zone.push(contextDataCit);
        zone.push(contextDataOrg);

        theMap = mapObj.init(mapParams);

        theMap.addElts(zone, true);

        setTimeout(function(){
                theMap.map.panTo([-21.4546, 47.0875]);
                theMap.map.setZoom(6);
        },2000);
    });


    var ctx = $("#bar-chart")

    new Chart(ctx, {
        type: 'pie',
        data: {
        labels: <?=json_encode(array_column($graphe_data, "label"))?>,
        datasets: [
            {
                label: "Acteurs",
                backgroundColor: ["#1bbc9b", "#00ff00", "#00aca9", "#e77e23","#27ae61", "#e84c3d", "#f1c40f"],
                data: <?=json_encode(array_column($graphe_data, "number"))?>
            }
        ]
        },
        options: {
            responsive: true,
            legend: { 
                display: true,
                position: "left",
                align: "center"
            },
            title: {
                display: true,
                text: 'Distribution des indépendants du filière numérique par groupe de spécialités \n \n'
            },
            cutoutPercentage: 50
        }
    });

    // Hide read more button
    $(".read-more").hide();

    // Read more section
    setTimeout(() => {
        var maxLength = 100;
        $(".project-desc small").each(function(index){
            var theText = $(this).text();
            if($.trim(theText).length > maxLength){
                var newText = theText.substring(0, maxLength);
                var removedStr = theText.substring(maxLength, $.trim(theText).length);
                $(this).empty().html(newText);
                $(".read-more").show();
                $(this).append('<span class="more-text'+index+'">' + removedStr + '</span>');
                $(".more-text").hide();
            }
        });
        
        $(".read-more").click(function(event){
            $(".project-desc small .more-text").contents().unwrap();
            $(this).remove();
        });
    }, 2000);

    setTimeout(() => {
            var x=10;
            var answer_size = $(".afn-card").length;

            (answer_size > x)? $("#loadMoreAnswers").show(): $("#loadMoreAnswers").hide();

            $(".afn-card").hide();
            $(".afn-card:lt("+x+")").show();

            $('#loadMoreAnswers').click(function () {
                x= (x+5 <= answer_size)? x+5: answer_size;
                $('.afn-card:lt('+x+')').show();
            });
    }, 2000);

    setTimeout(() => {
            var x=10;
            var answer_size = $(".pfn-card").length;

            (answer_size > x)? $("#loadMoreProjects").show(): $("#loadMoreProjects").hide();

            $(".pfn-card").hide();
            $(".pfn-card:lt("+x+")").show();

            $('#loadMoreProjects').click(function () {
                x= (x+5 <= answer_size)? x+5: answer_size;
                $('.pfn-card:lt('+x+')').show();
            });
    }, 2000);

    function search() {
        var input, filter, liste, item, i, txtValue;
        input = $('#searchInput');
        filter = input.val().toUpperCase();
        liste = $("#actors-list");
        item = $('.afn-card');

        for (i = 0; i < item.length; i++) {
            paragraph = item[i].getElementsByTagName("p")[0];
            txtValue = paragraph.textContent || paragraph.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                item[i].style.display = "";
            } else {
                item[i].style.display = "none";
            }
        }
    }

    $('.effectif h1').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 900,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
</script>
<?php }else{ ?>
    <div class="container">
        <hr>
    </div>
    <div class="text-center">
        <h4 class="text-light text-center">Veuillez vous connecter pour plus d'informations</h4>
        <br>
        <div>ou</div>
        <br>
        <button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalRegister">
    		<i class="fa fa-plus-circle"></i> Créer Un Compte <b>Citoyen</b>
		</button>
    </div>
</div>
<?php } ?>



