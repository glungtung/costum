<?php 
    $cssAnsScriptFilesModule = array(
    '/plugins/jquery-simplePagination/jquery.simplePagination.js',
    '/plugins/jquery-simplePagination/simplePagination.css',
    '/plugins/facemotion/faceMocion.css',
    '/plugins/facemotion/faceMocion.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));
    $cssAnsScriptFilesModule = array(
    '/assets/css/default/responsive-calendar.css',
    '/assets/css/default/search.css',
    '/assets/js/comments.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);

    $cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js'     
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);


//     $cssAnsScriptFilesModule = array(
//     '/js/default/responsive-calendar.js',
// //    '/js/default/search.js',
//  //   '/js/news/index.js',
//     );
//     HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
 
    

    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';

    $maxImg = 5;
    $page = (@$page && !empty($page)) ? $page : "search";
    if(!@$type){  $type = "all"; }

    if(@$type=="events")    { $page = "agenda"; $maxImg = 7; }
    if(@$type=="classifieds"){ $page = "annonces"; $maxImg = 1; }
    if(@$type=="ressources") { $page = "ressources"; }
    if(@$type=="proposals") { $page = "dda";}

    $filliaireCategories = CO2::getContextList("filliaireCategories");
    $directoryParams=Yii::app()->session["paramsConfig"]["directory"];
    if(@Yii::app()->session["paramsConfig"]["pages"]["#".$page]["directory"])
        $directoryParams=Yii::app()->session["paramsConfig"]["pages"]["#".$page]["directory"];
    if( isset($this->costum["app"]["#".$page]["tagsList"]) && $this->costum[ $this->costum["app"]["#".$page]["tagsList"] ] ){
        $tagsList = $this->costum[ $this->costum["app"]["#".$page]["tagsList"] ] ;
        $nameParamsTags = $this->costum["app"]["#".$page]["tagsList"];
    }else if( isset($this->costum["tags"]) ){
        $nameParamsTags = "tags" ;
        $tagsList = $this->costum["tags"];
    }

    


?>

<style>

    #dropdown_search{
        margin-top:0px;
    }

    .container{
        padding-bottom:0px !important;
    }
    .simple-pagination{
        padding: 5px 0px;
        border-bottom: 1px solid #e9e9e9;
      /*  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;*/
    }

    .simple-pagination li a, .simple-pagination li span {
        border: none;
        box-shadow: none !important;
        background: none !important;
        color: #707478 !important;
        font-size: 13px !important;
        font-weight: 500;
    }
    .simple-pagination li.active span{
        color: #d9534f !important;
        font-size: 24px !important; 
    }

    .simple-pagination li a.next,
    .simple-pagination li a.prev{
        color:#223f5c !important;
    }
    
</style>

<!-- <div id="mapContent" class="col-md-12 col-sm-12 col-xs-12" style="display: none; height: 600px"></div> -->
<div class="col-md-12 col-sm-12 col-xs-12 bg-white no-padding shadow" 
     id="search-content" style="min-height:700px;">
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding app-<?php echo $page ?>" id="page">
        <?php 
        //var_dump($page);
        if(isset(Yii::app()->session["paramsConfig"]["pages"]["#".$page]["header"])){
            echo $this->renderPartial(Yii::app()->session["paramsConfig"]["pages"]["#".$page]["header"], array("page"=>$page));
            //var_dump(Yii::app()->session["paramsConfig"]["pages"]["#".$page]["header"]);
        }

        if(   isset($this->costum) && 
                    isset($this->costum["htmlConstruct"]) && 
                    isset($this->costum["htmlConstruct"]["directory"]) && 
                    isset($this->costum["htmlConstruct"]["directory"]["filters"]) && 
                    isset($this->costum["htmlConstruct"]["directory"]["filters"]["tagsList"]) &&  
                    //isset($this->costum["tags"])
                    isset($tagsList)
                ){ 

                if( !empty($this->costum["paramsData"]) &&
                    !empty($this->costum["paramsData"][$nameParamsTags])  ){
                    $paramsTags = $this->costum["paramsData"][$nameParamsTags];
                }
                $tagsVertical = false ;
                if(!empty($this->costum["htmlConstruct"]["directory"]["filters"]["tagsList"]["rendering"]) && $this->costum["htmlConstruct"]["directory"]["filters"]["tagsList"]["rendering"] == "vertical"){
                    $tagsVertical = true ;
                }
                
        ?> 



            <div class="col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1 text-center margin-top-20 tags-cloud">
           
            <?php
                $colLi = "";
                if( $tagsVertical === true){
                    $colLi .= " col-xs-12 ";
                }

                $tagsHTML = " <a class='btn btn-link ".$colLi." bg-purple elipsis btn-tags-unik'
                    data-k='' href='javascript:;'>".Yii::t("common","See all")."</a>" ;
                foreach($tagsList as $v){    
                    if(!empty($paramsTags[$v]["color"])){
                        $color = (!empty($paramsTags[$v]["color"]) ? "background-color : ".$paramsTags[$v]["color"].";" : "");
                        $class = "";
                    }else{
                        $class = "bg-purple";
                        $color = "";
                    }

                    $class .=$colLi;
                    $tagsHTML .= "<a class='btn btn-link ".$class." elipsis  btn-tags-unik'
                        data-k='".$v."' href='javascript:;' style='".$color."' >".$v."</a>";

                    if( !empty($tagsVertical) && $tagsVertical === true){
                        $tagsHTML .= "<br/>";
                    }
                } 


                if( empty($tagsVertical)) {
                    echo $tagsHTML;
                }
            ?>
        </div> 
        
        <?php } 
        
        //top page Title 
        if( isset( Yii::app()->session[ "costum" ][ "app" ][ "#".$page ]["title"] ) ){
        ?>
            <h3 class="title-section col-sm-8"><?php echo Yii::app()->session[ "costum" ][ "app" ][ "#".$page ]["title"] ?></h3>
        <?php 
        }

        if(!empty($directoryParams["header"])){ 
            $classHeader = (!empty($directoryParams["header"]["class"]) ? $directoryParams["header"]["class"] : "no-padding col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1");
            ?>
            <div class="headerSearchContainer no-padding <?php echo $classHeader; ?>"></div>

        <?php } ?>
        <?php 

        $classBodySearch = "col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1 col-xs-12";
        if( !empty($tagsVertical) && $tagsVertical === true){
            $classBodySearch = "col-md-10 col-sm-10 col-xs-12";
            echo '<div class="tagsMenuSearch col-md-2 col-xs-12">';
                echo $tagsHTML ;
            echo '</div>' ;
        }

        if( !empty($directoryParams["body"]["class"]) )
            $classBodySearch = $directoryParams["body"]["class"];

        ?>


        
        <div class="<?php echo $classBodySearch ?> bodySearchContainer margin-top-10 <?php echo $page ?>">
            <div class="no-padding col-xs-12" id="dropdown_search">
              <div class='col-md-12 col-sm-12 text-center search-loader text-dark'>
                  <i class='fa fa-spin fa-circle-o-notch'></i> <?php echo Yii::t("common","Currently researching") ?> ...
              </div>
            </div>
            <div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
        </div>
    </div>
<?php 
//$this->renderPartial($layoutPath.'modals.'.Yii::app()->params["CO2DomainName"].'.pageCreate', array());

//$this->renderPartial($layoutPath.'footer', array(  "page" => $page)); ?>


<script type="text/javascript" >

var type = "<?php echo @$type ? $type : 'all'; ?>";
var typeInit = "<?php echo @$type ? $type : 'all'; ?>";
directory.appKeyParam="#<?php echo $page ?>";//(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;
var pageCount=false;
searchObject.count=true;
searchObject.initType=typeInit;
var scrollEnd = false;
if(searchObject.initType=="all"){
  var categoriesFilters={
    "persons" : { "key": "persons", "icon":"user", "label":"people","color":"yellow"}, 
    "NGO" : { "key": "NGO", "icon":"group", "label":"NGOs","color":"green-k"}, 
    "LocalBusiness" : { "key": "LocalBusiness", "icon":"industry", "label":"LocalBusiness","color":"azure"}, 
    "Group" : { "key": "Group", "icon":"circle-o", "label":"Groups","color":"turq"}, 
    "GovernmentOrganization" : { "key": "GovernmentOrganization", "icon":"university", "label":"services publics","color":"red"},
    "projects" : { "key": "projects", "icon":"lightbulb-o", "label":"projects","color":"purple"}, 
    "events" : { "key": "events", "icon":"calendar", "label":"events","color":"orange"}, 
    "poi" : { "key": "poi", "icon":"map-marker", "label":"points of interest","color":"green-poi"}, 
    //"place" : { "key": "place", "icon":"map-marker", "label":"points of interest","color":"brown"},
    //"places" : { "key": "places", "icon":"map-marker", "label":"Places","color":"brown"}, 
    "classifieds" : { "key": "classified", "icon":"bullhorn", "label":"classifieds","color":"azure"}, 
     
    //"ressources" : { "key": "ressources", "icon":"cubes", "label":"Ressource","color":"vine"} 
    //"services" : { "key": "services", "icon":"sun-o", "label":"services","color":"orange"}, "circuits" : { "key": "circuits", "icon":"ravelry", "label":"circuits","color":"orange"},
  };
}

var filliaireCategories = <?php echo json_encode($filliaireCategories); ?>;
var currentKFormType = "";

jQuery(document).ready(function() {
//    initKInterface();
    initCountType();
    mapCO.showLoader();
    showMap(true);
    
    loadingData = false; 
    searchObject.indexStep = 0 ;

    if(typeof costum != "undefined" && notNull(costum) && typeof costum.app != "undefined" && typeof costum.app[directory.appKeyParam] != "undefined" && typeof costum.app[directory.appKeyParam].searchObject != "undefined" && typeof costum.app[directory.appKeyParam].searchObject.sourceKey != "undefined"){
        searchObject.sourceKey=costum.app[directory.appKeyParam].searchObject.sourceKey;
    }

    searchInterface.init(type);

    paramsSearchMap=searchInterface.constructObjectAndUrl();
    paramsSearchMap.searchType = costum.app[directory.appKeyParam].filters.types;

    paramsSearchMap.indexStep = 0;

    searchMapCress(paramsSearchMap);
    
    $(".tooltips").tooltip();
});



</script>