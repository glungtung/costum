<?php 
	// $themeFilters = $this->renderPartial('costum.views.custom.cressReunion.themeFilter', array());
	// echo $themeFilters;

	//	$filliaireCategories = CO2::getContextList("filliaireCategories");
?>

<style>
	#filters-nav{
		padding-top: .6em !important;
	}

	.label{
		border-radius: 10px;
	}

	.btn{
		font-size: medium;
	}

	#infoBL{
		font-size: large;
	}

	.tooltips{
		font-size:18px;
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;

	var eventList = eventTypes;

//	var filliaireCategories=<?php echo json_encode(@$filliaireCategories); ?>;

	// var paramsFilter= {
	//  	container : "#filters-nav",
	//  	//filliaireCategories : filliaireCategories,
	//  	filters : {
	//  		themes : {
	//  			view : "themes",
	//  			type : "themes",
	//  			name : "Thématique",
	//  			action : "themes",
	//  			filliaireCategories : filliaireCategories
	//  		},
	// 	 	secteur : {
	//  			view : "dropdownList",
	//  			type : "filters",
	//  			field : "secteurEtablissement",
	//  			name : "Secteur d'activité",
	//  			action : "filters",
	//  			list : costum.lists.secteurEtablissement
	// 	 	},
	// 	 	arrondissement : {
	//  			view : "dropdownList",
	//  			type : "filters",
	//  			field : "arrondissement",
	//  			name : "Arrondissement",
	//  			action : "filters",
	//  			list : costum.lists.arrondissement
	// 	 	},
	// 	 	famille : {
	//  			view : "dropdownList",
	//  			type : "filters",
	//  			field : "famille",
	//  			name : "Famille",
	//  			action : "filters",
	// 			list : costum.lists.famille
	// 	 	}
	//  	}
	//  };

	let dropdownColumn = {};
	let index = 0;
    $.each(eventList, function(eventK, eventType) {
      if(!dropdownColumn["e-"+index%4]){
        dropdownColumn["e-"+index%4]={};
      }
      dropdownColumn["e-"+index%4][eventK]=eventType;
      index++;
    });

    console.log("event list column", dropdownColumn);

	var paramsFilter= {
	 	container : "#filters-nav",
	 	urlData : baseUrl+"/co2/search/agenda",
	 	calendar : true,
	 	loadEvent: {
	 		default : "agenda"
	 	},
	 	filters : {
	 		types : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			name : "Thématique",
	 			event : "selectList",
	 			keyValue : false,
	 			remove0:false,
	 			activeCounter:false,
	 			list : dropdownColumn
	 		},
	 		monthEss : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Mois de l'ESS",
	 			keyValue : false,
	 			event : "tags",
	 			list : {
	 				"MoisESS2020" : "MoisESS2020"
	 			}
	 		}		
	 	},
	 	defaults : {
	 		types : ["events"],
	 		sourceKey : "cressReunion"
	 	},
	 	results : {
 			renderView : "directory.eventPanelHtml"
 		}
	};
	var filterSearch={};
	// function lazyFilters(time){
	// 	mylog.log("lazyFilters TESTHERE paramsFilter", time);
	// 	if(typeof filterObj != "undefined" ){
	// 		mylog.log("lazyFilters TESTHERE paramsFilter", paramsFilter);
	// 		filterGroup = filterObj.init(paramsFilter);
	// 	} else if(time < 5000){
	// 		setTimeout(function(){
	// 		  lazyFilters(time+200)
	// 		}, time);
	// 	}
	// }

	jQuery(document).ready(function() {
		//alert("HERE");
		//lazyFilters(0);
		mylog.log("CRESSREUNION filterEvent.php");
		filterSearch = searchObj.init(paramsFilter);
		$(".dropdown-title").remove();

	});
	
</script>
