<style>
    body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span {
		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
	}
</style>
<?php if(isset($_SESSION["userId"])){ ?>
    
    <?php 
        $isAdmin = false;
        
        $theFormParent = PHDB::find("forms", array("parent.".$this->costum['contextId'].".name"=>$this->costum['name']));
        $formParentId = "";
        
        foreach ($theFormParent as $key => $value) { 
            $formParentId = $key; 
            if($value["creator"] == Yii::app()->session["userId"]){
                $isAdmin = true;
            }
        }
        
        $theAnswers = PHDB::find("answers", array("context.".$this->costum['contextId'].".name"=>$this->costum['name'], "user" => Yii::app()->session["userId"]));
        $myAnswer = "";
        
        foreach ($theAnswers as $mk => $value) {
            if(Yii::app()->session["userId"] == $value["user"]){
                $myAnswer = $value['_id']->{'$id'};
            }
        }
    ?>
    <script>
        let formParentId = "<?php echo $formParentId ?>";
        let myAnswer = "<?php echo $myAnswer ?>";

        setTitle("Evaluation");
    </script>
    <?php if($isAdmin==true){ ?>
        <script>
            document.location = "<?php echo Yii::app()->createUrl("/#@LILAEvaluation.view.forms.dir.observatory.") ?>"+formParentId;
        </script>
    <?php }else{ ?>
        <script>
            if(myAnswer!=""){
                document.location = "#answer.index.id."+myAnswer+".mode.w";
            }else{
                document.location = "#answer.index.id.new.form."+formParentId;
            }
        </script>
    <?php } ?>
<?php }else{ ?>
    <h4 class="text-center text-danger">
        Vous êtes déconnecté, Veuillez d'abord vous connecter.
    </h4>
<?php } ?>
