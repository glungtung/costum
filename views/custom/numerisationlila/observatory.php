 	
<?php if(Authorisation::isInterfaceAdmin()){ ?>
  <script type="text/javascript">
    isInterfaceAdmin = true;
    var sectDyf = {};
    var tplCtx = {};
    var DFdata = {
      'tpl'  : costum.contextSlug,
      "id"   : "<?php echo $this->costum["contextId"] ?>"
    };
    costum.col = "<?php echo $this->costum["contextType"]  ?>";
    costum.ctrl = "<?php echo Element::getControlerByCollection($this->costum["contextType"]) ?>";

    var configDynForm = <?php echo json_encode((isset($this->costum['dynForm'])) ? $this->costum['dynForm']:null); ?>;
    var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;
    var tplAuthorisation = <?php echo json_encode((isset($me["roles"]["superAdmin"])) ? $me["roles"]["superAdmin"]:false); ?>;

    pay = { 
      itIsPaid : function (answerId,isItPaid){  
        if (isItPaid == "false") {
          $("#statAns"+answerId).html(
            `&nbsp;&nbsp;<i class="fa fa-check fa-lg tooltips offer_warning"  data-placement="left" data-original-title="Problème de paiement" aria-hidden="true" data-toggle="collapse" href="#`+answerId+`"></i>`
            );
        }else{          
          $("#statAns"+answerId).html(
            ` &nbsp;&nbsp;<i class="fa fa-check fa-lg tooltips offer_paid"  data-placement="left" data-original-title="Déjà payé" aria-hidden="true" data-toggle="collapse"></i>`
            );
        }
        var params = {
          "answerId" : answerId,
          "paid" : isItPaid,
          "action" : "paid" 
        }
        ajaxPost(
          null,
          baseUrl+"/survey/answer/newanswer",
          params,
          function(data){
              urlCtrl.loadByHash(location.hash);
          },
          null,
          "json",
          {async : false}
        );

      }
    }
</script>
<style>

    a:hover, a:focus{
     text-decoration: none;
     outline: none;
   }

   .respondent-item{
     border-bottom: 1px solid #ebebeb;
   }

   #answerModal{
     z-index: 10000000 !important;
   }

   .text-green-theme{
     color: #7cb927;
   }

   .offer_warning{
    z-index: 1999;
    border-radius: 100px;
    width: 25px;
    height: 25px;
    padding-top: 7px;
    color: white;
    background-color: #f0ad4e;
    cursor: pointer;
  }
  .offer_paid{
    z-index: 1999;
    border-radius: 100px;
    width: 25px;
    height: 25px;
    padding-top: 7px;
    color: white;
    background-color: #93C020;
  }
  .offer_confirmed{
    z-index: 1999;
    width: 25px;
    height: 25px;
    padding-top: 7px;
    color: #93C020;
  }

  #searchInput {
    background-image: url('/images/search.png'); 
    background-position: 15px center;  
    background-repeat: no-repeat;
    background-color: #f1f1f1;
    border-radius: 30px;
    font-size: 16px; 
    padding: 5px 20px 5px 40px; 
    border: 1px solid #ddd;
    margin-bottom: 12px;
    margin-left: 12px;
  }

  .step-vld, #modeSwitch{
    display: none;
  }

  .i-obs {        
    float: left;
    width: 5%;
    padding-left: 15px;
    color: #7cb927;
  }
  .contact-obs {        
    float: left;
    padding-left: 15px;
  }
  .editFormBtn, .deleteFormBtn, .coFormbody h2{
    display: none;
  }

  .imgAnswer {
    width: 29px;
    height: 25px;
    border-radius: 50px;
    margin-right: 5px;
  }

  .mail-box {
    border-collapse: collapse;
    border-spacing: 0;
    display: table;
    table-layout: fixed;
    width: 100%;
  }
  .mail-box aside {
    display: table-cell;
    float: none;
    height: 100%;
    padding: 0;
    vertical-align: top;
  }
  .mail-box .sm-side {
    background: none repeat scroll 0 0 #e5e8ef;
    border-radius: 4px 0 0 4px;
    width: 25%;
  }
  .mail-box .lg-side {
    background: none repeat scroll 0 0 #fff;
    border-radius: 0 4px 4px 0;
    width: 75%;
  }
  .mail-box .sm-side .user-head {
    background: none repeat scroll 0 0 #00a8b3;
    border-radius: 4px 0 0;
    color: #fff;
    min-height: 80px;
    padding: 10px;
  }
  .user-head .inbox-avatar {
    float: left;
    width: 65px;
  }
  .user-head .inbox-avatar img {
    border-radius: 4px;
  }
  .user-head .user-name {
    display: inline-block;
    margin: 0 0 0 10px;
  }
  .user-head .user-name h5 {
    font-size: 14px;
    font-weight: 300;
    margin-bottom: 0;
    margin-top: 15px;
  }
  .user-head .user-name h5 a {
    color: #fff;
  }
  .user-head .user-name span a {
    color: #87e2e7;
    font-size: 12px;
  }
  a.mail-dropdown {
    background: none repeat scroll 0 0 #80d3d9;
    border-radius: 2px;
    color: #01a7b3;
    font-size: 10px;
    margin-top: 20px;
    padding: 3px 5px;
  }
  .inbox-body {
    padding: 20px;
  }

  ul.inbox-nav {
    display: inline-block;
    margin: 0;
    padding: 0;
    width: 100%;
  }
  .inbox-divider {
    border-bottom: 1px solid #d5d8df;
  }
  ul.inbox-nav li {
    display: inline-block;
    line-height: 45px;
    width: 100%;
  }
  ul.inbox-nav li a {
    color: #6a6a6a;
    display: inline-block;
    line-height: 45px;
    padding: 0 20px;
    width: 100%;
  }
  ul.inbox-nav li a:hover, ul.inbox-nav li.active a, ul.inbox-nav li a:focus {
    background: none repeat scroll 0 0 #d5d7de;
    color: #6a6a6a;
  }
  ul.inbox-nav li a i {
    color: #6a6a6a;
    font-size: 16px;
    padding-right: 10px;
  }
  ul.inbox-nav li a span.label {
    margin-top: 13px;
  }

  .inbox-head {
    background: none repeat scroll 0 0 #44546b;
    border-radius: 0 4px 0 0;
    color: #fff;
    min-height: 80px;
    padding: 14px;
  }
  .inbox-head h3 {
    display: inline-block;
    font-weight: 300;
    margin: 0;
    padding-top: 6px;
  }
  .inbox-head .sr-input {
    border: medium none;
    border-radius: 4px 0 0 4px;
    box-shadow: none;
    color: #8a8a8a;
    float: left;
    height: 40px;
    padding: 0 10px;
  }
  .inbox-head .sr-btn {
    background: none repeat scroll 0 0 #00a6b2;
    border: medium none;
    border-radius: 0 4px 4px 0;
    color: #fff;
    height: 40px;
    padding: 0 20px;
  }

  .inbox-body .modal .modal-body input, .inbox-body .modal .modal-body textarea {
    border: 1px solid #e6e6e6;
    box-shadow: none;
  }

  .modal-header h4.modal-title {
    font-family: "Open Sans",sans-serif;
    font-weight: 300;
  }
  .modal-body label {
    font-family: "Open Sans",sans-serif;
    font-weight: 400;
  }

  .files .progress {
    width: 200px;
  }
  .fileupload-processing .fileupload-loading {
    display: block;
  }

  @media (max-width: 767px) {
    .files .btn span {
      display: none;
    }
    .files .preview * {
      width: 40px;
    }
    .files .name * {
      display: inline-block;
      width: 80px;
      word-wrap: break-word;
    }
    .files .progress {
      width: 20px;
    }
    .files .delete {
      width: 60px;
    }
    .labelMenu{
      display: none;
    }
  }
  ul {
    list-style-type: none;
    padding: 0px;
    margin: 0px;
  }

  .contactSide{
    background: #44546b;
    padding: 20px;
  }
 
  .logoLoginRegister{
    background-color: #44546b !important;
    color: #ffffff;
    border-radius: 10px;
    padding: 20px;
  }

  @media (max-width: 767px){
    .name img {
      max-height: 70px;
    }
  }



</style>

    <?php

    $sumPaid = 0;
    $qttPaid = 0;
    $qttWrong = 0;
    $qttAll = 0;

     if($this->costum["contextType"] && $this->costum["contextId"]){
      $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    }

    $graphe_data = array();

   		# Get form parent
    $form = PHDB::findOne("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));
    $formId = (string)$form['_id'];
    $answers = PHDB::findAndSort("answers", array("context.".$this->costum['contextId']=>['$exists'=>true], "draft"=>['$exists' => false ]), array( 'created' => -1 ));
    $new_answers = PHDB::find("answers", array("context.".$this->costum['contextId']=>['$exists'=>true], "seen"=>"false", "draft"=>['$exists' => false ]));
    $display_nbr_newAnswer = "none";
    $nbr_newAnswer = count($new_answers);
    if ($nbr_newAnswer > 0) {
      $display_nbr_newAnswer = "inline-block"; 
      if ($nbr_newAnswer > 99) {
        $nbr_newAnswer = "+99";
      }
    } 
    $keyuni = "lilaobs".$formId;    
    $childForm = PHDB::find("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));

    $paramsData = [
      "object" => "",
      "msg" => "",
      "sign" => "",
      "formId" => ""
    ];

    if(isset($form["confirm"])){
     $paramsData["object"] = $form["confirm"]["object"];
     $paramsData["msg"] = $form["confirm"]["msg"];
     $paramsData["sign"] = $form["confirm"]["sign"];
     $paramsData["formId"] = $form["confirm"]["form"];
   }
    ?>

    <?php

    $cssAndScriptFilesModule = array(
     '/js/default/profilSocial.js'
   );
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

    function time_elapsed_string($ptime)
    {
      $etime = time() - $ptime;

      if ($etime < 1)
      {
        return '0 seconds';
      }

      $a = array( 365 * 24 * 60 * 60  =>  'an',
       30 * 24 * 60 * 60  =>  'mois',
       24 * 60 * 60  =>  'jour',
       60 * 60  =>  'heure',
       60  =>  'minute',
       1  =>  'second'
     );
      $a_plural = array( 'an'   => 'ans',
       'mois'  => 'mois',
       'jour'    => 'jours',
       'heure'   => 'heures',
       'minute' => 'minutes',
       'second' => 'seconds'
     );

      foreach ($a as $secs => $str)
      {
        $d = $etime / $secs;
        if ($d >= 1)
        {
          $r = round($d);
          if (($r > 1 ? $a_plural[$str] : $str) == "seconds") {
            return "À l'instant";
          }else{
           return 'Il y a '.$r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
         }         
       }
     }
   }

 //Getting the string after specified ctr
   function after($it, $inthat){
    if (!is_bool(strpos($inthat, $it)))
      return substr($inthat, strpos($inthat,$it)+strlen($it));
  };

 //Getting the string before specified ctr
  function before($it, $inthat){
    return substr($inthat, 0, strpos($inthat, $it));
  };
    ?>
  
   <?php  if(isset($_SESSION["userId"])){ ?>
    <div class="">
  <div class="mail-box">
    <aside class="sm-side">
      <div class="user-head">
        <div class="user-name">
          <h4>Réponses</h4>
        </div>
      </div>
      <ul class="inbox-nav inbox-divider">
        <li>
          <a href="#observatoire" class="new-ans"><i class="fa fa-inbox"></i> <font class="labelMenu">Nouveau</font> <span class="label label-danger pull-right new-ans-label" style="display: <?= $display_nbr_newAnswer ?>"></span></a>

        </li>
        <li>
          <a class="all-paid" href="#observatoire"><i class="fa fa-money text-green"></i> <font class="labelMenu">Payé</font>
            <span class="label pull-right qtt-label label-success"></span></a>
        </li>
        <li>
          <a class="all-wrong" href="#observatoire"><i class="fa fa-exclamation-circle text-red"></i> <font class="labelMenu">Erreur de paiement</font>
            <span class="label pull-right qtt-wrong-label label-warning"></span>
          </a>
        </li>
        <li>
          <a class="all-ans" href="#observatoire"><i class="fa fa-envelope-o"></i> <font class="labelMenu">Tout</font>
            <span class="label pull-right qtt-all-label label-default"></span></a>
        </li>
        <li>
          <a class="lbh" href="#@numerisationlila.view.forms"><i class="fa fa-cog"></i> <font class="labelMenu">Paramètres</font></a>
        </li>
        <li>
          <a class="lbh" href="#introduction"><i class="fa fa-info"></i> <font class="labelMenu">Page d'introduction</font></a>
        </li>
      </ul>

    </aside>
    <aside class="lg-side" style="height: 100vh;">
      <div class="inbox-head">
        <div class="row">
          <div class="col-md-8">
            <form action="#" class="text-right position">
              <div class="input-append">
                <input type="text" class="sr-input" id="searchInput" onkeyup="search()" placeholder="Recherche ...">
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="inbox-body">
       <div class="row" id="actors-list">
        <?php
          $payStat = array();
         foreach ($answers as $id => $answer_val) { ?>
          <?php $array_respondent = PHDB::find("citoyens", array('_id' => new MongoId($answer_val["user"])));
          $respondent = $array_respondent[$answer_val["user"]];  
          $seen_color = "";
          $statanswer = "";
          $confirmed = false;
          $paid = "unkown";
          $answerpayed = PHDB::find("answers", array("form" => $answer_val["form"], "user" => $answer_val["user"], "answers.isPaid"=>['$exists' => true ]));
          $payment = PHDB::find("payments", array('user' => $answer_val["user"], 'tracktrace' => $id));
          $amount = isset($answer_val["paid"]) ? $answer_val["paid"] : false;

          if (isset($answer_val["confirmed"])) {
            $confirmed = true;
          }

          if (is_numeric(before("_",$amount))) {
            $dateOfPay = (before("_",$amount));
            $paid = $dateOfPay;
            $statanswer = "this_is_paid";

          }elseif($amount) {
            $paid = 0;
            $statanswer = "this_is_wrong";

          }elseif (empty($answer_val["seen"])){
           $statanswer = "this_is_draft";

         }elseif(@$answer_val["seen"] == "false" ) {
           $seen_color = "#dff3ff "; 
           $qttAll ++;
           $statanswer = "this_is_new";
         }else{
          $statanswer = "this_is_seen"; 
          $qttAll ++;
        }
        $mobile = "<i>Non renseigné(e)</i>";
        $fixe ="<i>Non renseigné(e)</i>";
        if(!empty($respondent['telephone'])) {
          $mobile = isset($respondent['telephone']['mobile'][0])? $respondent['telephone']['mobile'][0] : "<i>Non renseigné(e)</i>";
          $fixe = isset($respondent['telephone']['fixe'][0])? $respondent['telephone']['fixe'][0] : "<i>Non renseigné(e)</i>";
        }
        ?>
        <div class="respondent-item <?= $statanswer; ?>" id="ansid<?php echo $id; ?>">
          <div class="media media<?php echo $id; ?>" style="background-color: <?php echo $seen_color; ?>">
            <div class="media-left pull-left">
              <a href="<?php echo '#page.type.citoyens.id.'.$answer_val["user"]; ?>" class="lbh-preview-element">
                <img src="<?= (isset($respondent['profilImageUrl']))?$respondent['profilImageUrl']:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" class="media-object" style="width:100px">
              </a>
            </div>
            <div class="media-body">
                <div class="row">
                  <div class="col-md-12 padding-top-20">
                     <a href="#observatoire" class="getanswer" data-paid="<?= $paid ?>" data-confirm="<?= $confirmed?>" data-mode="r" data-ansid="<?php echo $id; ?>" data-toggle="modal" data-target="#answerModal" data-name="<?= $respondent['name']; ?>" data-picture="<?= (isset($respondent['profilImageUrl']))?$respondent['profilImageUrl']:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>">
                    <div class="col-md-3">
                      <nobr><h4 style="text-transform: capitalize;font-weight: normal;"> <?= $respondent['name']; ?></h4></nobr>
                    </div>
                    <div class="col-md-3">
                        <i class="fa fa-at gradient-fill text-green-theme"></i>
                      <nobr> <?php echo $respondent['email']; ?></nobr>
                    </div>
                    <div class="col-md-2">
                      <div class=" i-obs">
                        <i class="fa fa-mobile gradient-fill "></i>
                      </div>
                      <div class="contact-obs">
                        <b>Mobile: </b>
                      </div>
                      <nobr> <?= $mobile ?></nobr>
                    </div>
                    <div class="col-md-2">
                      <div class="contact-obs">
                        <?php echo time_elapsed_string(@$answer_val["created"]) ?>
                      </div>
                    </div>
                  </a>
                    <div class="col-md-1 text-center">
                    <div id="statAns<?php echo $id; ?>">   
                      <?php 
                      // var_dump($paid == "" && $confirmed == true);
                      if ($paid === "unkown" && $confirmed == true) { ?>
                        &nbsp;&nbsp;<i class="fa fa-check fa-lg tooltips"  data-placement="left" data-original-title="Déjà confirmé" aria-hidden="true"></i>
                      <?php }
                      elseif ($paid === 0) { 
                        $qttWrong ++;
                        ?>
                        &nbsp;&nbsp;<i class="fa fa-check fa-lg tooltips offer_warning"  data-placement="left" data-original-title="Problème de paiement" aria-hidden="true" data-toggle="collapse" href="#<?php echo $id; ?>"></i>
                      <?php }
                      elseif (is_numeric($paid) === true) {?>
                        <?php 
                        echo after('_',$amount);
                        $sumPaid += after('_',$amount);
                        $qttPaid ++;
                        ?>€
                        &nbsp;&nbsp;<i class="fa fa-check fa-lg tooltips offer_paid"  data-placement="left" data-original-title="<?= after('_',$amount) ?>€ est payé!" aria-hidden="true" data-toggle="collapse" href="#<?php echo $id; ?>"></i>
                      <?php
                      } ?>
                      </div>                   
                    </div>
                    <div class="col-md-1">
                      <a style="color: #d9534f;" class="deleteAnswer tooltips" data-id="<?php echo $id; ?>" data-original-title="Supprimer" data-placement="left"><i class="fa fa-trash fa-lg"></i></a>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <?php if ($paid === 0 || $paid === "unkown" && $confirmed ==true) { ?>
          <div class="panel-group" style="margin-bottom: 0px !important;">
            <div class="panel panel-info">
              <div id="<?php echo $id; ?>" class="panel-collapse collapse">
                <ul class="list-group">
                  <?php foreach ($payment as $keyPai => $valuePai) { ?>
                    <li class="list-group-item border-blue">
                      <span class="text-info"><?= $valuePai["payment"]["description"]?></span>

                      <?php 
                      $stat = isset($valuePai["payment"]["status"]) ? $valuePai["payment"]["status"] : "";

                      if ($stat == "paid") { 
                        echo '<script type="text/javascript">',' pay.itIsPaid("'.$id.'","'.$valuePai['orderId'].'_'.$valuePai["payment"]["value"].'");','</script>';
                        ?>

                        <span class="label label-success">Payé</span>

                      <?php } elseif ($stat == "failed") { 
                        echo '<script type="text/javascript">',' pay.itIsPaid("'.$id.'","false");','</script>';
                        ?>

                        <span class="label label-danger">Echoué</span>

                      <?php } elseif ($stat == "canceled") { 
                        echo '<script type="text/javascript">',' pay.itIsPaid("'.$id.'","false");','</script>';
                        ?>

                        <span class="label label-warning">Annulé</span>

                      <?php } elseif ($stat == "open") { 
                        echo '<script type="text/javascript">',' pay.itIsPaid("'.$id.'","false");','</script>';
                        ?>

                        <span class="label label-info">Ouvert</span>

                      <?php } ?>

                      <small class="text-light pull-right"><?php echo gmdate("l jS \, F Y",$valuePai['orderId']); ?></small class="">
                        <br>
                        <a class="text-center text-red" target="_blank" href="https://www.mollie.com/dashboard/payments/<?= $valuePai["payment"]["id"]?>">
                        Voir plus de détails</a>

                      </li>
                    <?php } ?>

                  </ul>
                </div>
              </div>
            </div>
          <?php }?>          
        </div>
      <?php } ?>
      </div>
      </div>
      <div class="padding-top-20" style="">        
        <span style="font-size: 24px; padding-left: 0 !important;border-bottom: solid 1px #dadada;" class="paid-label text-dark"></span>
      </div>
    </aside>
  </div>
</div>

  <!-- Modal Answers-->
  <div id="answerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header modalD" style="border-bottom: none; background-color: #eaebef;">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Réponse dans <?php echo $form["name"] ?></h4>
           <div class="row">
            <div class="contact-obs">
             <p id="respondentName"></p>
             <p id="respondentMail"><b>Email: </b> <i class="fa fa-spinner fa-spin"></i></p>
            </div>
          </div>
         </div>
         <div id="answerContent" style="margin-top: -15px;">
            
         </div>
         <div class="modal-footer"  style="border-top: none;padding-right: 45px;">
          <p class="text-center confirm-lbl"></p>
           <button type="button" class="btn btn-default text-red" data-dismiss="modal"><i class="fa fa-times "></i> Fermer</button>
           <button type="button" class="btn btn-default text-bold letter-green confirmLila" data-dismiss="modal" data-toggle="modal" data-target="#confirmModal">Confirmer <i class="fa fa-arrow-circle-right"></i></button>
         </div>
       </div>
     </div>
   </div>

     <!-- Modal confirm-->
  <div id="confirmModal" class="modal fade" role="dialog" style="z-index: 100000;">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="row" style="margin: 0px !important;">
          <div class=""> 
          <div class="contactSide">
            <div class="contact-inf">
              <i class="fa fa-envelope" style="font-size: 45px; color: white"><h4>Confirmation par email</h4></i>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
          </div> 
            <div class="contact-form">
              <div class="form-group">
                <label class="control-label col-sm-2" for="email">A envoyer à:</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="<?= $keyuni ?>emailDestin" placeholder="Email de destinataire" name="email">
                  <p class="text-center" id="result<?= $keyuni ?>"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="lname">Objet:</label>
                <div class="col-sm-10">          
                  <input type="text" class="form-control" id="<?= $keyuni ?>obj" placeholder="Objet" name="lname" value='<?=$paramsData["object"]?>'>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="comment">Message:</label>
                <div class="col-sm-10">
                  <textarea class="form-control" rows="5" id="<?= $keyuni ?>msg"><?=$paramsData['msg']?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="comment">Lien:</label>
                <div class="col-sm-10">
                  <select id="<?= $keyuni ?>form" class="form-control">                    
                      <?php 
                      foreach($childForm as $key => $value) { 
                        echo  '<option value="'.$key.'">'.$value["name"].'</option>';
                      }    
                      ?> 
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="fname">Signature:</label>
                <div class="col-sm-10">          
                  <textarea type="text" class="form-control" id="<?= $keyuni ?>sign" placeholder="Enter First Name" name="fname"><?=$paramsData["sign"]?></textarea>
                </div>
              </div>             
              <div class="modal-footer"  style="border-top: none;padding-right: 45px;">
               <button type="button" class="btn btn-default text-red" data-dismiss="modal"><i class="fa fa-times "></i> Fermer</button>
               <button type="button" class="btn btn-default text-bold letter-green getMail" data-dismiss="">Envoyer <i class="fa fa-send"></i></button>
             </div>
            </div>
          </div>
        </div>
       </div>
     </div>
   </div>
	<script type="text/javascript">


    sectDyf.<?php echo $keyuni ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var ansNotif = <?= $nbr_newAnswer ?>;
     $(".this_is_draft").remove();
     $(".new-ans-label").html(ansNotif);
     $("#<?= $keyuni ?>form").val("<?=$paramsData["formId"]?>");
    var obs = {
      itIsSeen : function (answerId){  
        var params = {
          "answerId" : answerId,
          "seen" : true,
          "action" : "newanswer" 
        }
        ajaxPost(
          null,
          baseUrl+"/survey/answer/newanswer",
          params,
          function(data){
          },
          null,
          "json",
          {async : false}
        );
      },
      itIsConfirmed : function (answerId){  
        var params = {
          "answerId" : answerId,
          "confirmed" : true,
          "action" : "confirm" 
        }
        ajaxPost(
          null,
          baseUrl+"/survey/answer/newanswer",
          params,
          function(data){
          },
          null,
          "json",
          {async : false}
        );

      },

        itIsPaid : function (answerId){  
        var params = {
          "answerId" : answerId,
          "confirmed" : true,
          "action" : "paid" 
        }
        ajaxPost(
            null,
            baseUrl+"/survey/answer/newanswer",
            params,
            function(data){
            },
            null,
            "json",
            {async : false}
        );
      },

      validateEmail : function(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      },

      validate : function() {
        const $result = $("#result<?= $keyuni ?>");
        const email = $("#<?= $keyuni ?>emailDestin").val();
        $result.text("");
         if (!obs.validateEmail(email)) {
          $result.text("Adresse email invalide :(");
          $result.css("color", "red");
          return false; 
        }else{
          return true; 
        }   
    }
  }

    $(document).ready(function(){
      setTimeout(function(){ $("#menuTopRight a[href$='#observatoire']").html("Observatoire <span class='bg-green badge' style='display: <?= $display_nbr_newAnswer ?>'>"+ansNotif+"</span>");; }, 600);

     $('.deleteAnswer').off().click( function(){
      id = $(this).data("id");
      bootbox.dialog({
        title: trad.confirmdelete,
        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
        buttons: [
        {
          label: "Ok",
          className: "btn btn-primary pull-left",
          callback: function() {
            getAjax("", baseUrl+"/survey/co/delete/id/"+id , function(){
                  urlCtrl.loadByHash(location.hash);                  
                  $("#ansid"+id).remove();
                },"html");
          }
        },
        {
          label: "Annuler",
          className: "btn btn-default pull-left",
          callback: function() {}
        }
        ]
      });
    });
     sumPaid  = "<?= $sumPaid ?>";
     qttPaid  = "<?= $qttPaid ?>";
     qttWrong = "<?= $qttWrong ?>";
     qttAll   = "<?= $qttAll ?>";
     if (sumPaid > 0) {      
       $(".qtt-label").html(qttPaid);
       $(".paid-label").html("Montant total payé: "+sumPaid+'€');
     }
     if (qttWrong > 0) {      
       $(".qtt-wrong-label").html(qttWrong);
     }
     if (qttAll > 0) {      
       $(".qtt-all-label").html(qttAll);
     }

     $(".new-ans").on('click', function() {
      $(".this_is_seen").hide();
      $(".this_is_new").show();
      $(".this_is_paid").hide();
      $(".new-ans-label").remove();
      $(".this_is_wrong").hide();
      $(".paid-label").hide();
    });

      $(".all-ans").on('click', function() {
        $(".this_is_new").show();
        $(".this_is_seen").show();
        $(".this_is_paid").show();
        $(".this_is_wrong").show();
        $(".paid-label").show();
      });

      $(".all-paid").on('click', function() {
        $(".this_is_paid").show();
        $(".this_is_seen").hide();
        $(".this_is_new").hide();
        $(".this_is_wrong").hide();
        $(".paid-label").show();
      });  

      $(".all-wrong").on('click', function() {
        $(".this_is_wrong").show();
        $(".this_is_paid").hide();
        $(".this_is_seen").hide();
        $(".this_is_new").hide();
        $(".paid-label").hide();
      });    

      $(".getanswer").on('click', function() {
         $(".confirmLila").show();
        $(".confirm-lbl").html("");
        $(".modalD").css("color","#333");
        $(".modalD").css("background-color","#eaebef");
        $(".confirmLila").html(`Confirmer <i class="fa fa-arrow-circle-right"></i>`);
        if ($(this).data("paid") > 2) {
          let date = new Date($(this).data("paid") * 1000);
         $(".confirm-lbl").html(`<h2><span class="label label-success" style="border-radius: 0 !important"></i>&nbsp;Payé le `+date.toLocaleDateString()+`</span></h2>`);
         $(".confirmLila").hide();
         $(".modalD").css("background-color","#5cb85c");
         $(".modalD").css("color","white");
        }else if ($(this).data("confirm") == 1) {
          $(".confirm-lbl").html(`<i style="z-index: 1999" class="fa fa-check fa-lg text-green-theme"></i>&nbsp;Déjà confirmé!`);
          $(".confirmLila").html(`Reconfirmer <i class="fa fa-repeat"></i>`);
          $(".modalD").css("background-color","#00a8b3");
          $(".modalD").css("color","white");
        }
        $("#answerContent").empty();
        coInterface.showLoader("#answerContent");
        var answid = $(this).data("ansid");
        var getdatatype = "";
        var contextId = "<?php echo $this->costum['contextId'] ?>";
        var contextType = "<?php echo $this->costum['contextType'] ?>";
        if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
            ajaxPost("#answerContent", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                  null,
                  function(){
                      if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid+".contextId."+contextId+".contextType."+contextType);
                      }
                  },"html");
        }else{
          ajaxPost("#answerContent", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                 null,
                  function(){
                    
                      if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid);
                      }
                  },"html");
        } 
        str_html_respond = "<b>Répondu par Mme/Mr</b>: <img class='imgAnswer' src='"+$(this).data("picture")+"'>"+$(this).data("name");

        checkEmail = 0;
        <?=$keyuni?>respondentEmail = "";
        var lilaEmailTimeOut = setInterval(function(){
          checkEmail += 1
          if (typeof $('.ans_val_by_i').html() != "undefined") {
            clearInterval(lilaEmailTimeOut);
          $("#respondentMail").html(`<p><b>Email: </b>` +$('.ans_val_by_i').html()+`</p>`);
          <?=$keyuni?>respondentEmail = $('.ans_val_by_i').html();
          $(".questionList").removeClass("questionList");

          $(".sectionDescri").parent().remove();
          }
        }, 30);

        setTimeout(function(){
          clearInterval(lilaEmailTimeOut);
        }, 6000);

          $("#respondentName").html(str_html_respond);

          $('.getMail').off().click( function(){
            if (obs.validate() != false) {
              mailData = {};
              mailData.collection = "forms";
              mailData.id = "<?= $formId ?>";
              mailData.path = "confirm";
              mailData.value = {};
              mailData.value.object = $("#<?= $keyuni ?>obj").val();
              mailData.value.msg = $("#<?= $keyuni ?>msg").val();
              mailData.value.form = $("#<?= $keyuni ?>form").val();
              mailData.value.sign = $("#<?= $keyuni ?>sign").val();
              mylog.log("Loza",mailData);

              if(typeof mailData.value == "undefined"){
                toastr.error('value cannot be empty!');
              }
              else {
                dataHelper.path2Value( mailData, function(params) {
                  urlCtrl.loadByHash(location.hash);
                } );
              }
              var paramsmailLilaConfirm = {
                tpl : "custom",
                sign : mailData.value.sign,
                logo :"banner",
                tplObject : mailData.value.object,
                tplMail : [$("#<?= $keyuni ?>emailDestin").val()],
                html: mailData.value.msg,
                community : <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" ) ?>,
                btnRedirect : {
                  hash : location.pathname+"#answer.index.id.new.form." + mailData.value.form,
                  label : "Procéder au paiement "
                }
              };
              ajaxPost(
                null,
                baseUrl+"/co2/mailmanagement/mailfeedback",
                paramsmailLilaConfirm,
                function(data){ 
                  toastr.success("<?php echo ("Mail de confirmation envoyée") ?>");
                  obs.itIsConfirmed(answid);
                }
                ); 
            };
            
          });
        
        obs.itIsSeen(answid);
        $(".media"+answid).css("background-color", "white");
      });


		
			var x=10;
			var answer_size = $(".afn-card").length;

			(answer_size > x)? $("#loadMoreAnswers").show(): $("#loadMoreAnswers").hide();

			$(".afn-card").hide();
			$(".afn-card:lt("+x+")").show();

			$('#loadMoreAnswers').click(function () {
				x= (x+5 <= answer_size)? x+5: answer_size;
				$('.afn-card:lt('+x+')').show();
			});


			var x=10;
			var answer_size = $(".pfn-card").length;

			(answer_size > x)? $("#loadMoreProjects").show(): $("#loadMoreProjects").hide();

			$(".pfn-card").hide();
			$(".pfn-card:lt("+x+")").show();

			$('#loadMoreProjects').click(function () {
				x= (x+5 <= answer_size)? x+5: answer_size;
				$('.pfn-card:lt('+x+')').show();
			});

    sectDyf.<?=$keyuni?>Params = {
      "jsonSchema" : {
        "title" : "Envoi d'email de confirmation",
        "description" : "",
        "icon" : "fa-cog",
        "properties" : {
          contact : {
            inputType : "text",
            label : "Contact ",
            value : "",
            rules:{
              "required":true
            }
          },
          object : {
            inputType : "text",
            label : "Objet"
          },
          msg : {
            inputType : "textarea",
            label : "Message"
          },
          sign : {
            inputType : "textarea",
            label : "Signature"
          }

        },
        save : function () {
          tplCtx.value = {};
          $.each( sectDyf.<?php echo $keyuni ?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
          });
          delete tplCtx.value.contact
          mylog.log("Loza",tplCtx);
          if(typeof tplCtx.value == "undefined"){
            toastr.error('value cannot be empty!');
          }
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.closeForm();
              urlCtrl.loadByHash(location.hash);
            } );
          }

        }
      }
    };

    $(".confirmLila").off().on("click",function() {
     $("#<?= $keyuni ?>emailDestin").val(<?=$keyuni?>respondentEmail)
    });
	});

   function search() {
      var input, filter, liste, item, i, txtValue;
      input = $('#searchInput');
      filter = input.val().toUpperCase();
      liste = $("#respondents-list");
      item = $('.respondent-item');
      ti = item.length;
      for (i = 0; i < item.length; i++) {
         paragraph = item[i].getElementsByTagName("h4")[0];
         txtValue = paragraph.textContent || paragraph.innerText;
         if (txtValue.toUpperCase().indexOf(filter) > -1) {
            item[i].style.display = "";
         } else {
          //$('#itemNbr').html(i);
            item[i].style.display = "none";
         }
      }
   }

  
</script>
<?php }else{ ?>
	<div class="container">
		<hr>
	</div>
	<div class="text-center">
		<h4 class="text-light text-center">Veuillez vous connecter pour plus d'informations</h4>
		<br>
		<div>ou</div>
		<br>
		<button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalRegister">
			<i class="fa fa-plus-circle"></i> Créer Un Compte <b>Citoyen</b>
		</button>
	</div>
<?php } ?>
<?php }else{ ?>
  <div class="container">
    <div class="text-center">
      <div class="col-xs-12 col-lg-10 col-md-9 col-sm-9 central-section" style="padding-top: initial;">
        <div class="col-xs-12 ">
          <div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">
            <div class="padding-20 text-center text-red" style="border: 1px solid red"> 
              <i class="fa fa-lock fa-4x "></i><br>
              <h1 class="">Sorry, You Are Not Allowed to Access This Page! </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>