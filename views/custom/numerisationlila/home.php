<?php 
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

  $answers = PHDB::find("answers", array("source.key"=>$this->costum['slug'], "seen"=>"false", "draft"=>['$exists' => false ]));
  $display_nbr_newAnswer = "none";
  $nbr_newAnswer = count($answers);
  if ($nbr_newAnswer > 0) {
    $display_nbr_newAnswer = "inline-block"; 
    if ($nbr_newAnswer > 99) {
        $nbr_newAnswer = "+99";
    }
} 
  $cmsList = PHDB::find(Cms::COLLECTION,array( "parent.".$this->costum["contextId"] => array('$exists'=>1)));
}
$param=["canEdit"=>true];
$params = [

  "tpl" => $el["slug"],
  "slug"=>$this->costum["slug"],
  "canEdit"=>true,
  "blockCms" => $cmsList,
  "el"=>"" ]; 
  echo $this->renderPartial("costum.views.tpls.tplsEngine", $params,true );
  //echo $this->renderPartial("costum.views.tpls.blockCms.index",$param);
?>
<style type="text/css">
    .gCoForm{
        /*display: none !important;*/
    }

    .new-ans-badge {
        display: <?= $display_nbr_newAnswer?>;
        min-width: 10px;
        min-width: 10px;
        padding: 3px 7px;
        font-size: 15px;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        background-color: #f9576d;
        border-radius: 10px;
        margin-top: -10px;
        position: absolute;
    }
    .logoLoginRegister{
      background-color: #44546b !important;
      color: #ffffff;
      border-radius: 10px;
      padding: 20px;
    }

    @media (max-width: 767px){
      .name img {
        max-height: 70px;
      }
    }
</style>
<?php if($canEdit && Authorisation::isInterfaceAdmin() ){ ?>
    <div class="text-center content-btn-action btn-edit-delete">
        
          <ul class="list-inline" id="options">
            <li class="option">
              <button class="material-button option1 bg-red deleteLine tooltips" type="button" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Supprimer block">
                <i class="fa fa-trash" aria-hidden="true"></i>
              </button>
            </li>
            <li class="option">
              <button class="material-button option2 hide" type="button">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
              </button>
            </li>
             <li class="option">
              <button class="material-button option3 editSectionBtns tooltips" type="button" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Modifier block">
                <i class="fa fa-edit" aria-hidden="true"></i>
              </button>
            </li>
          </ul>
          <button class="material-button material-button-toggle material-button-toggle" type="button">
            <i class="fa fa-pencil" aria-hidden="true"></i>
          </button>
        

    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.material-button-toggle-<?= @$kunik?>').on("click", function () {
                $(this).toggleClass('open');
                $('.option-<?= @$kunik?>').toggleClass('scale-on');
            });
        });
    </script>

<?php }?>

<style type="text/css">
	.vcenter {
		display: inline-block;
		vertical-align: middle;
		float: none;
	}

 

/*	button {width:6em;transition: transform .2s; }
	button:hover span {display:none}
	button:hover:before {
		content:"Ajouter section"
	}
	button {
		padding: 50px;
		background-color: green;
		transition: transform .2s; /* Animation 
		width: 20px;
		height: 20px;
		margin: 0 auto;
	}

	button:hover {
		transform: scale(1.5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) 
	}*/
/
</style>
<script type="text/javascript">
  <?php if(!Authorisation::isInterfaceAdmin()){ ?>
   $(".menu-xs-cplx").remove();
   $(".dropdown-menu-top").remove();
  <?php } ?>
  var ansNotif = <?= $nbr_newAnswer?>;
	jQuery(document).ready(function() {
		mylog.log("render","/home/mirana/pixelhumain-docker/code/modules/costum/views/custom/numerisationlila/home.php");
		setTitle(costum.title);
		contextData = <?php echo json_encode($el); ?>;
		var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
		var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
		var contextName=<?php echo json_encode($el["name"]); ?>;
		contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;
   setTimeout(function(){ $("#menuTopRight a[href$='#observatoire']").html("Observatoire <span class='bg-green badge' style='display: <?= $display_nbr_newAnswer ?>'>"+ansNotif+"</span>");; }, 600);
	});
</script>


