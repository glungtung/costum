<?php
    $cssAndScriptFilesModule = array(
        '/js/default/profilSocial.js',
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

    if($this->costum["contextType"] && $this->costum["contextId"]){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    }

    # ----------------------------------
    $form = PHDB::find("forms", array("parent.".$this->costum['contextId'].".name" => $this->costum['name']));
    $formId = "";
    //print_r(array("parent.".$this->costum['contextId'].".name"=>$this->costum['name']));
    foreach($form as $key => $value) { $formId = $key; }

    # ----------------------------------
    $myAnswer = "";
?>
<style>
.header{
    background: #001146;
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/photo/header.png);
    width: 101%;
    height: auto;
    background-size: cover;
    padding-bottom: 58%;
    position: initial !important;
}
.logofn{
    width: 50%;
margin-top: 5%;
position: absolute;
margin-left: 3%;
}
.carto{
    margin-top : -7.9%;
}
.carto-h1 {
    color : white;
}
.carto-d {
    box-shadow: 0px 0px 20px -2px black;
    width: 80%;
    left: 10%;
    background : white;
    padding: 2%;
    font-size : 1.5vw;
}
.c-description{
    width: 80%;
    left: 10%;
    font-size: 2vw;
    margin-top: 3%;
}
.join {
    background : #55586b;
    margin-top : 2%;
}
.join-btn{
    text-align : center;
    color : white;
}
.actu{
    margin-top:3%;

}
.agenda{
    /* background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/fond/fond.png); */
    color : white; 
    margin-top :3%;
    background : #3b495a;
}
.agenda-h1{
    margin-left :12%;
    left: 4%;
    margin-top: 2%;
}

.carousel-border{
      box-shadow: 0px 0px 20px -2px black;
      background: #e2e2e2;
      left: 10%;
      margin-bottom : 7%;
}
.card{
/* border-width: 1px;
border-style : solid; */
    margin-left: 10%;
    margin-right: 5%;
    color:white;
    font-size: 1.5vw;
    box-shadow: 0px 0px 20px -2pxblack;
}
.card-info{
    background : #c7d407;
    padding : 2%;
    font-size : 1vw;
}
.card-c{
    left: 11%;
    margin-top: 25%;
    z-index: 10;
    position : absolute;
}
.ctr-img{
    width : 100%; 
    height:auto;
}
.carousel-inner{
    position: relative;
    width: 100%;
    overflow: hidden;
    height: 40vw;
}
.ctr-img {
    z-index : 1;
}
.event{
    margin-top: 3%;
}
.agenda-carousel{
    margin-top : -3%;
    background: white;
    width: 85%;
}
.event-lien{
    text-align: center;
    padding-bottom : 10%;
    margin-top: 3%;
}

#caroussel{
    /* background-color: rgba(0,0,0,0.5); */
    opacity: 1;
    width: 31%;
    z-index: 1;
    margin-left: 5%;
}
.resume {
    text-align: left;
    z-index: 10;
    position: absolute;
    padding-top: 3%;
    padding-left: 2%;
    background-color:
    rgba(0,0,0,0.7);
    width: 40%;
    padding-bottom: 100%;
}
.resume > h2{
    font-size : 2vw;
    color : white;
}
.resume > p{
   font-size : 1.3vw;
   color : white;
}
#arrow-caroussel{
    margin-top: -9%;
    font-size: 3vw;
    color:white;
    position: absolute;
    width: 40%;
    text-align: center;
}
#arrow-caroussel > a {
    color : white;
    text-decoration : none;
    margin: 3%;
}
.carousel {
    position: revert !important;
}
.join-btn{
    margin-top:3%;
}
.join-btn > svg {
    margin : 2%;
}
.plus-m {
    width : 2%;
}
.text-m1{
    font-size: 1.2em;
}
.text-m2{
    font-size: 0.86em;        
}
.img-event{
    width: 50vw;
}
.carto-p-m{
    font-size: 2.85vw;
    margin-top: 6%;
    line-height: 42px;
}
.h2-day{
    font-weight: bold !important;
    font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
}
.description{
    padding : 5%; 
    top: 5%;
}
.dropmenu{
    margin-top: -10%;
    position: relative;
    margin-left: -5%;
}
.dropdown-menu{
    padding : 0px 0px !important;
}
.dropdown-menu > li >  a {
    padding: 1px 10px !important;
}
.title-carto{
    font-size : 2.81em;
    text-shadow: 0px 0px 7px black;
}
button {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
}
#newsstream .loader{
    display: none;
}
@media (max-width:768px){
    .carousel-inner{
        position: relative;
        width: 100%;
        overflow: hidden;
        height: 55vw;
    }
    #arrow-caroussel{
        font-size: 3vw;
        color:white;
    }
    .plus-m {
    width : 5%;
    }
    .carto-description{
        font-size: 2.5vw !important;
    }
    .text-m1{
        font-size: 4.95vw;
    }
    .text-m2{
        font-size : 3.3vw;
    }
    .img-event {
        height: 75px;
        width: 250px;
    }
    .carto-p-m{
        font-size: 2.5rem;
        margin-top: 2%;
        line-height: 19px;
    }
    .p-mobile-description{
        font-size : 1.5rem;
    }
    .description{
        padding : 5%; 
        margin-top: -3%;
    }
    .dropmenu{
        margin-top: -7%;
        margin-left: -6%;
        position: relative;
    }
    .title-carto{
        font-size : 2.81vw;
        text-shadow: 0px 0px 7px black;
    }
    .card-info{
        background: #c7d407;
        padding: 2%;
        font-size: 2.3vw;
        margin-left: 0%;
        margin-top: -20%;
        position: absolute;
    }
    .resume{
        padding-top: 5%;
    }
    .resume > p {
    font-size: 2vw;
    }
    .btn {
        padding: 3px 5px;
        font-size: 7px;
    }
    .card-mobile{
        margin-top : 3%;
    }
    .logofn{
        width: 100%;
        margin-top: 13%;
    }
}
</style>
<div class="header row">
    <div class="logofn col-md-9">
        <!--Début svg logo --> 
        <!--?xml version="1.0" encoding="utf-8"?-->
<!-- Generator: Adobe Illustrator 23.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 320 595.28 200.89" style="enable-background:new 0 0 595.28 841.89; filter: drop-shadow( 0px 0px 8px rgb(41, 40, 40));" xml:space="preserve">
<style type="text/css">
    .st0{fill:#FFFFFF;}
    .st1{font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: lighter !important;}
    .st2{font-size:43.3313px;}
    .st3{font-family:'customFont';}
    .st4{fill:#36B4A0;}
    .st5{font-family:'customFont';}
    .st6{font-size:26.4145px;}
    .st7{fill:#6BBFA3;}
    .st8{fill:#AFCA0B;}
    .st9{fill:#00ACA9;}
    .st10{fill:#95C11F;}
</style>
<g>
    <g>
        <g>
            <text transform="matrix(1 0 0 1 271.2626 403.6614)"><tspan x="0" y="10" class="st0 st1 st2">FILIÈRE</tspan><tspan x="0" y="57.99" class="st0 st3 st2">NUMÉRIQUE</tspan></text>
            <g>
                <text transform="matrix(1 0 0 1 400.971 492.166)" class="st4 st5 st6">RÉUNION</text>
            </g>
        </g>
    </g>
    <g>
        <g>
            <polygon class="st7" points="52.11,420.94 104.81,512.22 210.21,512.22 137.1,420.94          "/>
            <polygon class="st8" points="210.21,329.67 210.21,512.22 262.91,420.94          "/>
            <polygon class="st9" points="210.21,329.67 104.81,329.67 52.11,420.94 140.14,420.94             "/>
            <polygon class="st10" points="137.1,420.94 210.21,512.22 210.21,329.67          "/>
        </g>
        <g>
            <path id="share_1_" class="st0" d="M195.31,406.78c-4.78,0-8.99-2.28-11.74-5.77c-9.83,4.92-19.68,9.83-29.52,14.76
                c0.34,1.24,0.57,2.52,0.57,3.86c0,1.35-0.23,2.63-0.57,3.87c9.83,4.92,19.67,9.83,29.51,14.75c2.75-3.48,6.96-5.76,11.74-5.76
                c8.28,0,14.99,6.71,14.99,14.98c0,8.28-6.71,15-14.99,15c-8.27,0-14.98-6.72-14.98-15c0-1.34,0.23-2.62,0.57-3.86
                c-9.83-4.93-19.67-9.83-29.52-14.76c-2.75,3.49-6.96,5.78-11.74,5.78c-8.27,0-14.99-6.7-14.99-14.99
                c0-8.27,6.72-14.98,14.99-14.98c4.78,0,8.99,2.28,11.74,5.77c9.83-4.91,19.67-9.83,29.52-14.74c-0.34-1.25-0.57-2.53-0.57-3.88
                c0-8.27,6.71-14.99,14.98-14.99c8.28,0,14.99,6.72,14.99,14.99C210.3,400.06,203.59,406.78,195.31,406.78z"/>
            <path id="share_3_" class="st0" d="M128.53,459.39c1.74-2.95,4.69-4.71,7.85-5.13c0.55-7.86,1.12-15.72,1.68-23.59
                c-0.89-0.24-1.77-0.57-2.59-1.06c-0.84-0.49-1.54-1.1-2.18-1.76c-6.62,4.27-13.25,8.54-19.87,12.81c1.15,2.97,1.01,6.4-0.73,9.34
                c-3.02,5.11-9.61,6.79-14.71,3.78c-5.11-3.02-6.8-9.62-3.78-14.72c3.02-5.1,9.61-6.78,14.72-3.76c0.83,0.49,1.53,1.1,2.17,1.76
                c6.63-4.27,13.25-8.54,19.88-12.81c-1.15-2.97-1.02-6.4,0.72-9.35c3.02-5.1,9.61-6.8,14.72-3.77c5.1,3.02,6.79,9.61,3.77,14.72
                c-1.75,2.95-4.69,4.71-7.84,5.13c-0.56,7.86-1.12,15.72-1.69,23.59c0.89,0.25,1.77,0.57,2.6,1.07c5.1,3.02,6.8,9.61,3.78,14.71
                c-3.02,5.11-9.62,6.79-14.72,3.77C127.2,471.09,125.51,464.5,128.53,459.39z"/>
            <path id="share_2_" class="st0" d="M117.72,396.82c1.29,2.5,1.22,5.33,0.13,7.71c5.23,3.83,10.45,7.67,15.69,11.5
                c0.56-0.51,1.17-0.98,1.87-1.34c0.71-0.36,1.44-0.59,2.18-0.75c-0.07-6.48-0.14-12.96-0.21-19.44c-2.57-0.5-4.9-2.1-6.18-4.6
                c-2.23-4.34-0.52-9.67,3.82-11.89c4.34-2.23,9.67-0.52,11.9,3.82c2.23,4.34,0.51,9.66-3.83,11.89c-0.7,0.36-1.44,0.59-2.18,0.74
                c0.07,6.48,0.14,12.96,0.21,19.45c2.57,0.5,4.9,2.09,6.19,4.6c2.23,4.34,0.52,9.66-3.82,11.9c-4.34,2.23-9.66,0.51-11.89-3.82
                c-1.29-2.51-1.22-5.33-0.14-7.71c-5.22-3.83-10.45-7.67-15.68-11.51c-0.57,0.51-1.17,0.98-1.88,1.34
                c-4.34,2.23-9.67,0.52-11.89-3.82c-2.23-4.34-0.51-9.67,3.82-11.9C110.16,390.77,115.49,392.48,117.72,396.82z"/>
        </g>
    </g>
</g>
</svg>

    <!-- Fin svg -->
    </div>
</div>

<!-- Description -->

<div class="carto row">
    <div class="carto-h1 col-xs-6 col-sm-6 no-padding">
        <p class="title-carto" style="margin-left: 19.5%; font-size: 20pt;"><i>La Réunion 100% numérique...</i></p>
    </div>
    <div class="carto-img col-xs-6 col-sm-6" >
       <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="margin-top: -60%; margin-left:3%; position: absolute; opacity: 0.7;">
    </div>
    <div class="carto-d col-xs-12 col-sm-12">
        <h1>La communauté Winum</h1>
                <p class="p-mobile-description">Bienvenue sur la plateforme collaborative <b>Winum</b>. </p>
        <p class="p-mobile-description">Cet outil interactif a été conçu pour mutualiser nos ressources et participer ainsi à l'évolution de la filière numérique.  </p>
        <p class="p-mobile-description"><b>Winum</b> s'inscrit dans une démarche engagée par La Région Réunion pour accompagner les acteurs de cette filière dynamique. </p>
        <p class="p-mobile-description">Partageons nos documents, animons cette plateforme et collaborons pour une Réunion 100% numérique. </p>
        </div>
    </div>

<!-- Carto NEWS -->

<div class="carto-n row">
    <div class="c-description col-sm-12 col-xs-12">
        <div class="description col-md-6">
                   <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration : none;" class="dropdown-a">
        <h1 style="color: <?php echo $this->costum["css"]["color"]["blue"]; ?>;"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/carto.svg" class="img-responsive"  style="width:6.5%; margin-right: 2%;"> CARTO/ANNUAIRE <i class="fa fa-sort-down"></i> <br> <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait.svg" class="img-responsive img-dropdown" style="margin-top: -4%; margin-right: 42%;"><br> </h1></a>
    <!-- Debut dropdown --> 
    <div class="dropdown-menu col-xs-12 col-sm-12 col-md-12 dropmenu" style="background-color: transparent !important; width: 86%;">
        <!-- Début svg --> 
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 278.07 154.84">
    <defs>
        <style>.clls-1{fill:#41435b;}.cls-2{fill:#fff;}.cls-2{font-size:1.25em;font-family:customFont, customFont;font-weight:600;}</style></defs><title>acteurscarto</title>

        <polygon class="clls-1" points="278.07 25.61 263.39 51.21 14.67 51.21 0 25.61 14.67 0 263.39 0 278.07 25.61"/>
        <text class="cls-2" x="80" y="30"><a href="javascript:;" data-hash="#Producteur" class="lbh-menu-app " style="text-decoration : none;">Acteurs Producteurs</a></text>

        <polygon class="clls-1" points="278.07 76.82 263.39 102.42 14.67 102.42 0 76.82 14.67 51.21 263.39 51.21 278.07 76.82"/>
        <text class="cls-2" x="80" y="80"><a href="javascript:;" data-hash="#Strategie" class="lbh-menu-app " style="text-decoration : none;">Acteurs Stratégiques</a></text>
        <polygon class="clls-1" points="278.07 128.03 263.39 153.63 14.67 153.63 0 128.03 14.67 102.42 263.39 102.42 278.07 128.03"/>
        <text class="cls-2" x="80" y="130"><a href="javascript:;" data-hash="#Formateur" class="lbh-menu-app " style="text-decoration : none;">Acteurs Formateurs</a></text>

        <path class="cls-2" d="M174.19,244h-6.11v-1.18a.58.58,0,0,0-.59-.59.59.59,0,0,0-.59.59V244h-6.11a1.58,1.58,0,0,0-1.58,1.58v2.08l-.82-.27a15.51,15.51,0,0,0-3.89-.83l-.23.47h0l.66,5.17-.79,1.38-.79-1.38L154,247h0l-.23-.47a15.65,15.65,0,0,0-3.91.84l-.08.05-.18.11-.13.12-.11.15a1.42,1.42,0,0,0-.08.18s0,.06,0,.1l-1.58,7.1a1,1,0,0,0,.75,1.18.75.75,0,0,0,.21,0,1,1,0,0,0,1-.77l1.46-6.55.34-.1v6.73l-.78,9.76a1.18,1.18,0,0,0,1.08,1.27h.1a1.19,1.19,0,0,0,1.18-1.09l.71-8.82a1.2,1.2,0,0,0,1,0l.71,8.82a1.19,1.19,0,0,0,1.18,1.09h.1a1.17,1.17,0,0,0,1.08-1.27l-.78-9.76V249a8.89,8.89,0,0,1,.85.27h0l1.45.48v5.31a1.58,1.58,0,0,0,1.58,1.58h2.87l-2.52,9.4a.58.58,0,0,0,.42.72l.15,0a.59.59,0,0,0,.57-.44l2.6-9.71h2v6.88a.59.59,0,0,0,1.18,0v-6.88h2l2.6,9.71a.6.6,0,0,0,.57.44l.15,0a.6.6,0,0,0,.42-.72l-2.52-9.4h2.87a1.58,1.58,0,0,0,1.58-1.58v-9.46A1.63,1.63,0,0,0,174.19,244Zm.79,11a.79.79,0,0,1-.79.79h-13.4A.79.79,0,0,1,160,255V250l2.5.83a1.12,1.12,0,0,0,.31.05,1,1,0,0,0,.93-.67,1.52,1.52,0,0,0,0-.21l5.93-2.74a.2.2,0,0,0-.16-.36l-5.8,2.68a.94.94,0,0,0-.61-.62l-3.13-1v-2.34a.79.79,0,0,1,.79-.79H174.2a.79.79,0,0,1,.79.79V255Z" transform="translate(-128.57 -125.82)"/>
        <path class="cls-2" d="M154.13,246.33c1.37,0,2.49-1.6,2.49-3a2.49,2.49,0,0,0-5,0C151.64,244.73,152.76,246.33,154.13,246.33Z" transform="translate(-128.57 -125.82)"/>
        <path class="cls-2" d="M168.66,251l.41-.25-.4-.64-.41.25a1.93,1.93,0,0,0-.65-.46l.11-.47-.74-.17-.11.47a2,2,0,0,0-.79.13l-.26-.41-.64.4.26.41a1.93,1.93,0,0,0-.46.65l-.47-.11-.17.74.47.11a2,2,0,0,0,.13.79l-.41.25.4.64.41-.26a1.84,1.84,0,0,0,.65.46l-.09.47.73.17.11-.47a2,2,0,0,0,.79-.13l.25.41.64-.4-.25-.41a1.93,1.93,0,0,0,.46-.65l.47.11.17-.74-.47-.11A2.11,2.11,0,0,0,168.66,251Zm-1.13,1.86A1.36,1.36,0,1,1,168,251,1.36,1.36,0,0,1,167.53,252.86Z" transform="translate(-128.57 -125.82)"/>
        <path class="cls-2" d="M172.75,249.8l-.36.12a1.62,1.62,0,0,0-.42-.48l.17-.34-.54-.27-.17.34a1.49,1.49,0,0,0-.63,0l-.12-.36-.57.19.12.36a1.62,1.62,0,0,0-.48.42l-.34-.17-.27.54.34.17a1.49,1.49,0,0,0,0,.63l-.36.12.19.57.36-.12a1.62,1.62,0,0,0,.42.48l-.17.34.54.27.17-.34a1.49,1.49,0,0,0,.63,0l.12.36.57-.19-.12-.36a1.62,1.62,0,0,0,.48-.42l.34.17.27-.54-.34-.17a1.49,1.49,0,0,0,0-.63l.36-.12Zm-1.39,1.93a1.08,1.08,0,1,1,.68-1.37A1.09,1.09,0,0,1,171.36,251.73Z" transform="translate(-128.57 -125.82)"/>
        <path class="cls-2" d="M155.88,147.5a1.52,1.52,0,0,0-.34.23C155.68,147.79,156,147.5,155.88,147.5Zm0-.05h0Zm9.75-10.12A2.64,2.64,0,1,1,163,140,2.64,2.64,0,0,1,165.67,137.33Zm-15.88,11.33h.71v-.34h-.71a.54.54,0,0,1-.38-.1.15.15,0,0,1,0-.12V146h-.34v2a.48.48,0,0,0,.08.39A.73.73,0,0,0,149.79,148.66Zm22.4,4.83-.1-.78c-.38-3-.39-3.16-.25-8.53a.34.34,0,0,0-.68,0c-.15,5.42-.13,5.54.25,8.63l.1.78c.1.75,0,1.27-.24,1.47s-.81,0-.85,0c-2.5-.77-5.62.82-5.76.89a.35.35,0,0,0-.15.46.33.33,0,0,0,.3.18.32.32,0,0,0,.16,0,10.83,10.83,0,0,1,3-.95v5.3h-1.41a.52.52,0,0,0,0,1h4a.52.52,0,0,0,0-1h-1.3v-5.36a4.13,4.13,0,0,1,.92.16,1.64,1.64,0,0,0,1.49-.11C172.15,155.22,172.32,154.52,172.19,153.49ZM170.57,152c-.06-2.24-.79-4.39-.85-6.62-.09-3.25-5.06-3.32-5.1-.15a26.93,26.93,0,0,1-5.74.41c-1.76-.08-1.76,2.64,0,2.72a28.67,28.67,0,0,0,6.06-.43,33.59,33.59,0,0,1,.53,3.58c-4.14,1.45-6.44,5-7,9.41,0,0,0,.06,0,.1V151.1H161v-2.33h-9.37a.34.34,0,0,0,0-.11v-.09c0-.2.05-.26.24-.3h.17v.16h2.52l.21,0a.27.27,0,0,0,.25-.1.22.22,0,0,0,0-.14.25.25,0,0,1,0-.13.35.35,0,0,1,.16-.08.73.73,0,0,0-.09.32H158c0-.55-.64-1-1.42-1a2,2,0,0,0-.67.12c0,.47-.39.42-.57.39a.35.35,0,0,0-.05.08h0a.32.32,0,0,0-.24.11.28.28,0,0,0,0,.18.2.2,0,0,1,0,.09.24.24,0,0,1-.16.06V148h-2.76v.21l-.19,0c-.24,0-.29.16-.31.38v.09a.34.34,0,0,1,0,.11h-5.18v2.33h1.42v11.32h10.6v-1.13c.17,1.74,3.15,1.58,3.41-.41a7.16,7.16,0,0,1,5.85-6.51A2.37,2.37,0,0,0,170.57,152Zm-20.35-10.87.43,6.11-1.13.08-.43-6.11Z" transform="translate(-128.57 -125.82)"/>
        <path class="cls-2" d="M151.73,205.74l.59-.59h4.33l-2.76-5-2,.42a1.43,1.43,0,0,0-1.1,1l-2.43,7.92a1.43,1.43,0,0,0,1.39,1.8h2v-5.6Z" transform="translate(-128.57 -125.82)"/>
        <path class="cls-2" d="M165.75,209.53l-2.43-7.92a1.45,1.45,0,0,0-1.12-1.05l-2-.39-2.75,5h4.66l.59.59v5.59h1.7a1.44,1.44,0,0,0,1.14-.56A1.43,1.43,0,0,0,165.75,209.53Z" transform="translate(-128.57 -125.82)"/>
        <polygon class="cls-2" points="24.35 85.51 24.18 85.51 24.18 85.51 32.75 85.51 32.75 85.51 32.91 85.51 32.91 80.51 24.35 80.51 24.35 85.51"/>
        <rect class="cls-2" x="21.98" y="86.05" width="12.97" height="1.07"/>
        <path class="cls-2" d="M152.42,196.66l.08.19a2.05,2.05,0,0,0,.37.66,4.71,4.71,0,0,0,4.16,3.63c1.8,0,3.47-1.51,4.11-3.66a1.89,1.89,0,0,0,.36-.66l.08-.2a1.07,1.07,0,0,0-.18-1.31v-.05c0-2.66-.36-5.51-4.41-5.49s-4.41,2.74-4.4,5.52v.05A1.07,1.07,0,0,0,152.42,196.66Zm.57-.86h0a.24.24,0,0,0,.15-.13l.71.11a.15.15,0,0,0,.16-.09l.45-1a19.74,19.74,0,0,0,3.92.55,4.56,4.56,0,0,0,1.15-.13l.28.68a.14.14,0,0,0,.14.09h0l.82-.23a.3.3,0,0,0,.17.16h0c.12.06.16.33,0,.59,0,.06-.06.15-.1.23a2.1,2.1,0,0,1-.22.45.28.28,0,0,0-.14.18c-.54,1.95-2,3.31-3.56,3.32s-3-1.35-3.61-3.29a.28.28,0,0,0-.14-.18,2.36,2.36,0,0,1-.22-.45c0-.08-.07-.16-.1-.23C152.83,196.13,152.86,195.86,153,195.8Z" transform="translate(-128.57 -125.82)"/>
    </svg>

       <!-- Fin svg -->  
    </div>
    <!-- Fin dropdown -->   
    
        <p class="p-mobile-description">Retrouvez ci-contre tous les acteurs de la filière numérique à La Réunion. </p>
                <div style="text-align: center;">
        <a href="javascript:;" data-hash="#search" class="lbh-menu-app" style="text-decoration : none;">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive" style="width:7%;"></a></div>
        </div>

        <div class="carto-img col-md-6">
            <a href="javascript:;" data-hash="#search" class="lbh-menu-app" style="text-decoration : none;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/element/carto.png" class="img-responsive">
            </a>
        </div>

    </div>
</div>


<!-- Join -->

<div class="join row">
    <div class="join-btn col-md-6">
        <!-- Btn je rejoins -->
        
        <?php if($myAnswer!=""){ ?>
            <svg data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 225.3 39.91" style="width:50%;">
                    <!-- début svg -->
                    <defs><style>.cls-1{fill:#fff;}</style></defs>
                    <title>je rejoins</title>
                    <path class="cls-1" d="M192.66,268.72a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm190-19.2H181.14l-11.9,20,11.9,19.95H382.65l11.89-19.95ZM199.53,267.66a4.19,4.19,0,0,1-.16,1.07l8.46,4.07a4.36,4.36,0,0,1,3.38-1.59,4.23,4.23,0,1,1-4.14,3.07c-2.83-1.37-5.65-2.71-8.47-4.08a4.29,4.29,0,0,1-2.67,1.53c-.14,1.86-.27,3.71-.41,5.57a4,4,0,0,1,.74.3,2.9,2.9,0,0,1,1.09,4.06,3.16,3.16,0,0,1-4.23,1,2.91,2.91,0,0,1-1.09-4.06,3.12,3.12,0,0,1,2.26-1.42c.13-1.82.28-3.64.4-5.46a4.28,4.28,0,0,1-2.44-1.12l-4.55,2.82a2.9,2.9,0,0,1-.21,2.59,3.15,3.15,0,0,1-4.23,1,2.91,2.91,0,0,1-1.07-4.07,3.15,3.15,0,0,1,4.22-1,2.91,2.91,0,0,1,.62.49l4.46-2.77a4,4,0,0,1-.56-2,3.6,3.6,0,0,1,.26-1.4l-2.81-2a2.65,2.65,0,0,1-.55.37,2.59,2.59,0,0,1-3.41-1,2.41,2.41,0,0,1,1.1-3.29,2.6,2.6,0,0,1,3.42,1.05,2.41,2.41,0,0,1,0,2.14l2.66,1.88a4.28,4.28,0,0,1,3-1.81c0-.94,0-1.9,0-2.85a2.51,2.51,0,0,1-1.77-1.26,2.39,2.39,0,0,1,1.09-3.29,2.59,2.59,0,0,1,3.41,1.05,2.4,2.4,0,0,1-1.1,3.29,2,2,0,0,1-.62.2l0,2.83a4.38,4.38,0,0,1,3,1.58l8.47-4.08a4.12,4.12,0,0,1-.16-1.06,4.3,4.3,0,1,1,4.3,4.13,4.33,4.33,0,0,1-3.38-1.59l-8.46,4.07A4.19,4.19,0,0,1,199.53,267.66ZM192.7,269l0,0a2,2,0,0,1-.08-.22A.77.77,0,0,0,192.7,269Zm0-.25a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Z" transform="translate(-169.24 -249.52)"/>
                    <g>
                        <text x="59" y="26" class="text-m1">
                            <a href="javascript:;" target="_blank" data-id="<?= $formId; ?>"> Accéder à mes données</a>
                            <!--a href="javascript:;" onclick="dyFObj.openForm('organization'); " style="text-decoration : none;"> Accéder à mes données</a-->
                        </text>
                    </g>
                
            </svg>
        <?php }else{ ?>
            <!-- début svg --> 
            <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 225.3 39.91" style="width:50%;">
                    <defs><style>.cls-1{fill:#fff;}</style></defs>
                    <title>je rejoins</title>
                    <path class="cls-1" d="M192.66,268.72a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm190-19.2H181.14l-11.9,20,11.9,19.95H382.65l11.89-19.95ZM199.53,267.66a4.19,4.19,0,0,1-.16,1.07l8.46,4.07a4.36,4.36,0,0,1,3.38-1.59,4.23,4.23,0,1,1-4.14,3.07c-2.83-1.37-5.65-2.71-8.47-4.08a4.29,4.29,0,0,1-2.67,1.53c-.14,1.86-.27,3.71-.41,5.57a4,4,0,0,1,.74.3,2.9,2.9,0,0,1,1.09,4.06,3.16,3.16,0,0,1-4.23,1,2.91,2.91,0,0,1-1.09-4.06,3.12,3.12,0,0,1,2.26-1.42c.13-1.82.28-3.64.4-5.46a4.28,4.28,0,0,1-2.44-1.12l-4.55,2.82a2.9,2.9,0,0,1-.21,2.59,3.15,3.15,0,0,1-4.23,1,2.91,2.91,0,0,1-1.07-4.07,3.15,3.15,0,0,1,4.22-1,2.91,2.91,0,0,1,.62.49l4.46-2.77a4,4,0,0,1-.56-2,3.6,3.6,0,0,1,.26-1.4l-2.81-2a2.65,2.65,0,0,1-.55.37,2.59,2.59,0,0,1-3.41-1,2.41,2.41,0,0,1,1.1-3.29,2.6,2.6,0,0,1,3.42,1.05,2.41,2.41,0,0,1,0,2.14l2.66,1.88a4.28,4.28,0,0,1,3-1.81c0-.94,0-1.9,0-2.85a2.51,2.51,0,0,1-1.77-1.26,2.39,2.39,0,0,1,1.09-3.29,2.59,2.59,0,0,1,3.41,1.05,2.4,2.4,0,0,1-1.1,3.29,2,2,0,0,1-.62.2l0,2.83a4.38,4.38,0,0,1,3,1.58l8.47-4.08a4.12,4.12,0,0,1-.16-1.06,4.3,4.3,0,1,1,4.3,4.13,4.33,4.33,0,0,1-3.38-1.59l-8.46,4.07A4.19,4.19,0,0,1,199.53,267.66ZM192.7,269l0,0a2,2,0,0,1-.08-.22A.77.77,0,0,0,192.7,269Zm0-.25a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Zm0,0a.77.77,0,0,0,0,.25l0,0A2,2,0,0,1,192.66,268.72Z" transform="translate(-169.24 -249.52)"/>
                    <g>
                        <text x="59" y="26" class="text-m1">
                        <a href="javascript:;" onclick="dyFObj.openForm('organization'); " style="text-decoration : none;"> Je rejoins la filière</a>
                        </text>
                    </g>
            </svg>
        <?php } ?>
        <!-- fin svg --> 
        <br>
        <!-- Btn je propose -->
        <!-- Debut svg --> 
        <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 220.44 29.05" style="width:75%;">
            <defs><style>.cls-1{fill:#fff;}</style></defs>
            <title>je souhaite</title>
            <path class="cls-1" d="M168.56,424.7a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm194.73-14H160.17l-8.66,14.53,8.66,14.52H363.29L372,425.26Zm-189.73,13.2a2.89,2.89,0,0,1-.12.78c2.05,1,4.11,2,6.16,3a3.19,3.19,0,0,1,2.46-1.16,3,3,0,1,1-3.13,3,2.88,2.88,0,0,1,.12-.77l-6.17-3a3.13,3.13,0,0,1-1.94,1.12c-.11,1.35-.2,2.7-.3,4.05a2.52,2.52,0,0,1,.54.21,2.12,2.12,0,0,1,.79,3,2.3,2.3,0,0,1-3.08.76,2.12,2.12,0,0,1-.79-3,2.29,2.29,0,0,1,1.64-1c.1-1.33.21-2.65.3-4a3.13,3.13,0,0,1-1.78-.82L165,428.16a2.1,2.1,0,0,1-.16,1.88,2.3,2.3,0,0,1-3.07.76,2.13,2.13,0,0,1-.79-3,2.29,2.29,0,0,1,3.07-.76,2,2,0,0,1,.46.36l3.25-2a2.8,2.8,0,0,1-.41-1.49,2.88,2.88,0,0,1,.18-1l-2-1.44a1.65,1.65,0,0,1-.4.27,1.88,1.88,0,0,1-2.48-.77,1.75,1.75,0,0,1,.8-2.39,1.88,1.88,0,0,1,2.49.77,1.76,1.76,0,0,1,0,1.55l1.94,1.37A3.1,3.1,0,0,1,170,421c0-.68,0-1.38,0-2.07a1.84,1.84,0,0,1-1.29-.92,1.74,1.74,0,0,1,.8-2.39,1.88,1.88,0,0,1,2.48.76,1.75,1.75,0,0,1-.8,2.4,1.39,1.39,0,0,1-.45.14c0,.69,0,1.38,0,2.06a3.17,3.17,0,0,1,2.16,1.15l6.17-3a2.76,2.76,0,0,1-.12-.77,3.13,3.13,0,1,1,3.13,3,3.19,3.19,0,0,1-2.46-1.16l-6.16,3A2.89,2.89,0,0,1,173.56,423.93Zm-5,.95,0,0a.75.75,0,0,1-.05-.16A.57.57,0,0,0,168.59,424.88Zm0-.18a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Zm0,0a.57.57,0,0,0,0,.18l0,0A.75.75,0,0,1,168.56,424.7Z" transform="translate(-151.51 -410.73)"/>
            <g>
                <text x="43" y="19" class="text-m2"><a onclick="dyFObj.openForm('project');" style="text-decoration : none" href="javascript:;">Je souhaite ajouter un projet</a></text>
            </g>
        </svg>
        <!-- fin svg --> 
        <br>

        <p class="carto-p-m"><i>Restons connectés <br> pour faire avancer <br> notre filière numérique</i></p>
    </div>
    <div class="join-img col-md-6">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/photo/photo-deux.jpg" class="img-responsive">
    </div>
</div>

<!-- Actualité -->
<div class="actu row">
    <div class="col-xs-12 col-sm-12">
    <div class="actu-titre col-xs-6 col-sm-6" style="padding-left: 16%;">
        <h1 style="color: <?php echo $this->costum["css"]["color"]["blue"]; ?>"><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/actus.svg" class="img-responsive" style="width: 8%; margin-right: 2%;"> Actualités</h1>
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait.svg" class="img-responsive" style=" margin-top: -4%; margin-right: 45.5%;">
    
    </div>
<div class="actucard col-xs-12 col-sm-12 col-md-12">
       <div id="newsstream" style="width: 90%; margin-left: 6%;">
       </div>
    </div>

    <div class="col-xs-12 col-sm-12" style="text-align : center;">
    <a href="javascript:;" data-hash="#live" class="lbh-menu-app " style="text-decoration : none; font-size:2rem;">
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive plus-m" style="margin: 1%;"><br>
    Voir plus <br>
    d'actualités
    </a>
    </div>
    </div>
</div>

<!-- Agenda --> 

<div class="agenda row">
    <div class="agenda-h1 col-xs-6 col-sm-6">
    <h1><img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/agenda.svg" class="img-responsive" style="width:5%; margin-right: 2%;"> Agenda <br></h1>
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/trait-blanc.svg" class="img-responsive" style="margin-top: -3%; padding-right: 64%;">
    <p style="margin-top: -1.5%;">Évènement à venir</p>
    </div>
    <div class="event col-xs-12 col-sm-12">
        <div id="event-affiche"></div>

    </div>
    <div class="event-lien col-xs-12 col-sm-12">
            <p>
            <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app " style="text-decoration : none; font-size:2rem; color:white;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus-blanc.svg" class="img-responsive plus-m" style="margin: 1%;"><br>            Voir plus<br>
            d'évènement.</a></p>
        </div>
</div>

<!-- Carousel --> 
<div class="agenda-carousel">

<div class="col-xs-12 col-sm-12 col-md-12 no-padding carousel-border" style="margin-top : -7.5%;">
        <div id="docCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner itemctr">
            </div>
            <div id="arrow-caroussel">
               <!-- Début svg --> 
               <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 152.1 54.9" style="width:35%;">
    <defs><style>.cls-3{fill:none;}.cls-2{fill:#fff;}</style></defs>
    <title>fleche</title>
    <a href="#docCarousel" data-slide="next"> <rect class="cls-3" x="73.74" width="78.36" height="54.9"/><polygon class="cls-2" points="87.57 43.75 119.64 43.75 119.64 54.9 147.08 27.45 119.64 0 119.64 11.14 87.57 11.14 87.57 43.75"/></a>
    <a href="#docCarousel" data-slide="prev"><rect class="cls-3" x="131.87" y="340.95" width="78.36" height="54.9" transform="translate(210.23 395.84) rotate(180)"/><polygon class="cls-2" points="64.53 11.14 32.46 11.14 32.46 0 5.01 27.45 32.46 54.9 32.46 43.75 64.53 43.75 64.53 11.14"/></a>
</svg>
            <!-- <div class="background-carousel">
            .
            </div> -->
            <div id="caroussel" class="carousel-control col-xs-6 col-sm-6 col-md-6">

               <!-- Fin svg --> 
                </div>
            </div>
                <!-- <a class="left carousel-control" href="#docCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#docCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a> -->
        </div>
    </div> 

</div>

<!-- SCRIPT --> 
<script type="text/javascript">

$(document).ready(function(){
    afficheEvent();
    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
    if(notNull(currentUser)) currentUser.addressCountry = "RE";

    setTitle("Coeur Numérique");
    
    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
});


function afficheEvent(){
    ajaxPost(
        null,
        baseUrl+"/costum/coeurnumerique/geteventaction",
        {},
        function(data){ 
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var url_event = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>" + "/images/coeurNumerique/default_event.jpg";
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";
            
            var i = 1;
            if(data.result == true){
                $(data.element).each(function(key,value){

                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;


                    var imgMedium = (value.imgMedium != "none") ?  baseUrl+value.imgMedium : url_event;
                    //var imgMedium = (value.imgMedium != "none") ?  baseUrl+value.imgMedium : url;
                    var img = (value.img != "none") ? baseUrl+value.img : url;
                    

                    str += "<div class='col-md-4 card-mobile'>";
                    str += "<div class='card'>";
                    str += "<button class='lbh-preview-element' href='#@"+value.slug+"' data-hash='#page.type.events.id."+value.id+"' style='text-decoration : none; color:white;'>";
                    str += "<div class='col-xs-12 col-sm-12 col-md-12 card-c'>";
                    str += "<div class='col-xs-6 col-sm-6 col-md-6 card-info'>"+startDate+"<br>"+value.type;
                    str += "</div></div>";
                    str += "<img src='"+imgMedium+"' class='img-responsive img-event'>";
                    str += "</button></div></div>";

                    if(i == 1) ctr += "<div class='item active'>";
                    else ctr += "<div class='item'>";

                    ctr += "<div class='resume col-xs-3 col-sm-3 col-md-3'>";
                    ctr += "<h2 class='h2-day'>"+startDate+"</h2><br>";
                    ctr += "<h2>"+value.name+"</h2><br>";
                    ctr += "<p>"+value.resume+"<p>";
                    ctr += "<button class='btn btn-info lbh-preview-element' href='#@"+value.slug+"' data-hash='#page.type.events.id."+value.id+"'><i class='fa fa-info-circle'></i> Plus d'information</button>";
                    ctr += "</div>";

                    ctr += "<img src='"+img+"' class='ctr-img img-responsive'>";
                    ctr += "</div>";

                    i++;
                });
                
                $(".itemctr").html(ctr);
                // $("#tclass").html(ctr);
                // $(".resume").html(resume);
                
                
              
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
                $(".agenda-carousel").hide();
            }

            if(i != 2) $("#arrow-caroussel").show();
            else $("#arrow-caroussel").hide();
            
            $("#event-affiche").html(str);
        }
    ); 
}

//Dropdown
$('.dropdown-a').mouseover(function() {
    $(this).attr("aria-expanded",true);
    $(".description").addClass("open");
});

// $('.img-dropdown').mouseover(function() {
//     $(".description").addClass("open");
// });

$(".dropdown-menu").mouseover(function(){
    $(".description").addClass("open");
});

$(".dropdown-a").mouseleave(function(){
    if($('.description').hasClass('open')){
        $(".dropdown-a").attr("aria-expended",false);
        $(".description").removeClass("open");
    }
});

$(".dropdown-menu").mouseleave(function(){
    $(".dropdown-a").attr("aria-expended",false);
    $(".description").removeClass("open");
});
</script>