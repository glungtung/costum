<style type="text/css">
	.createBlockBtn{
		margin-top: 10px;
	}

	.simpleTextAndImg-txt{
		text-align: left;
	}
</style>

<?php
	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

		$cmsList = PHDB::find(Cms::COLLECTION,array( "source.key" => $this->costum["slug"]));
	}

	if (Authorisation::isElementAdmin($this->costum["contextId"],$this->costum["contextType"],Yii::app()->session["userId"])) 
		$canEdit = true;
	else 
		$canEdit = false;

	echo '<div class="col-xs-12 col-sm-12 col-lg-12">';
		$params = array( 
          	"cmsList" => $cmsList,
          	"canEdit" => $canEdit,
          	"tag"=> "other");

		echo $this->renderPartial("costum.views.tpls.blockCms.textImg.simpleTextAndImg" ,$params,true);
	echo '</div>'; 
?>