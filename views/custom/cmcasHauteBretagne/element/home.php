<?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        '/js/default/editInPlace.js',
        '/css/element/about.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@$this->appConfig["element"];
    if(isset($this->costum)){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["js"] == "about.css")
            array_push($cssJsCostum, '/css/'.$this->costum["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }

    if (Authorisation::isCostumAdmin()) 
        $edit =true;
    else
        $edit =false;
?>
<!--
<div class='col-md-12 margin-bottom-15 text-dark-blue'>
    <i class="fa fa-info-circle fa-2x"></i><span class='Montserrat' id='name-lbl-title'> <?php echo Yii::t("common","About") ?></span>
</div>
-->
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    
<div class="pod-info-Description"  id="pod-info-Description">
 <?php if  (Yii::app()->session["userId"] != null)  {?> 
    <span class="text-nv-3 text-orange">
        <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
    </span>
    
    <?php if($edit==true ||( $element["preferences"]["isOpenEdition"]==true && Yii::app()->session["userId"] != null ) ){?>
        <button class="btn-update-descriptions btn btn-update-orange pad-2 pull-right tooltips"
                    data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update description") ?>" id="btn-descriptions">
        <?php if((!empty($element["shortDescription"])) || (!empty($element["description"])) ) {?>
            <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
        <?php }
                else{?>
            <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
            <?php } ?>
        </button>
   
    <?php } ?>

 <hr class="line-hr text-orange" >
  <?php } 

  else if((!empty($element["shortDescription"])) || (!empty($element["description"])) && (Yii::app()->session["userId"] == null )) { ?>  
        <span class="text-nv-3 letter-blue">
            <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
        </span> 
    <hr class="line-hr text-orange">
   <?php } ?> 

    <div id="contenuDesc">  
    <?php if  (!( (empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) {?> 
        <div class="contentInformation margin-10">
            <span id="shortDescriptionAbout" name="shortDescriptionAbout" style="font-size: 18px;" class="bold"><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
            <span id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit"  class="hidden" ><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></span>
        </div>
    <?php }
    if  (!( (empty($element["description"])) && Yii::app()->session["userId"] == null)) {?>
        
        <div class="contentInformation margin-10">
            <div class="more no-padding" id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
            </div>
        </div>   
    <?php }?>  

    </div>
</div>


    <div class="section light-bg pod-infoGeneral" id="pod-infoGeneral">
        <div class="row profil-title-informations">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <span class="text-nv-3 text-green">
                    <i class="fa fa-address-card-o"></i> <?php echo Yii::t("common","General information") ?>
                </span>
                <?php if($edit==true || ( $element["preferences"]["isOpenEdition"]==true && Yii::app()->session["userId"] != null ) ){?>
                <button class="btn-update-info btn btn-update-red pad-2 pull-right  visible-xs tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>
                </button>
                <?php } ?>
            </div>
            <?php if($edit==true || ( $element["preferences"]["isOpenEdition"]==true && Yii::app()->session["userId"] != null ) ){?>
            <div class=" col-md-4 col-sm-4 col-xs-12 hidden-xs">
                <button class="btn-update-info btn btn-update-red pad-2 pull-right tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                </button>
            </div>
            <?php } ?>

        </div>
        <hr class="line-hr text-green">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
                <div class="col-md-6 col-sm-6 col-xs-12" >
                    <div class="card features">
                        <div class="card-body">

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-font gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="nameAbout"> <?php echo $element["name"]; ?> </p>
                                </div>
                            </div>

                            <?php if($element["collection"]==Project::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-spinner gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="avancementAbout"> <?php echo (@$element["properties"]["avancement"]) ? Yii::t("project",$element["properties"]["avancement"]) : '<i>'.Yii::t("common","Not specified").'</i>' ?> </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($element["collection"]==Person::COLLECTION) { ?>
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-user-secret gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10"
                                           id="usernameAbout"><?php echo (@$element["username"]) ? $element["username"] : '<i>' . Yii::t("common", "Not specified") . '</i>' ?></p>
                                    </div>
                                </div>
                                <?php if (Preference::showPreference($element, $element["collection"], "birthDate", Yii::app()->session["userId"])) { ?>
                                    <div class="media contentInformation" >
                                        <span class="ti-2x mr-3"><i class="fa fa-birthday-cake gradient-fill"></i></span>
                                        <div class="media-body">
                                            <p class="padding-top-10" id="birthDateAbout"><?php echo (@$element["birthDate"]) ? date("d/m/Y", strtotime($element["birthDate"]))  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                        </div>
                                    </div>

                                <?php }
                            }

                            if($element["collection"]==Organization::COLLECTION || $element["collection"]==Event::COLLECTION){ ?>
                            <div class="media contentInformation" id="divTypeAbout">
                                <span class="ti-2x mr-3"><i class="fa fa-list-ul gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10"  id="typeAbout">
                                        <?php
                                        if(@$typesList && @$element["type"] && !empty($typesList[$element["type"]]))
                                            $showType=Yii::t( "category",$typesList[$element["type"]]);
                                        else if (@$element["type"])
                                            $showType=Yii::t( "category",$element["type"]);
                                        else
                                            $showType='<i>'.Yii::t("common","Not specified").'</i>';
                                        echo $showType; ?>
                                    </p>
                                </div>
                            </div>
                            <?php }

                            if( (   $element["collection"]==Person::COLLECTION &&
                                Preference::showPreference($element, $element["collection"], "email", Yii::app()->session["userId"]) ) ||
                            in_array($element["collection"], [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])) { ?>

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="emailAbout"><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="media contentInformation">
                                <div class="media-body">
                                    <p class="padding-top-10" id="thematiqueAbout"><?php echo (@$element["thematique"]) ? "<span style='color:#7bba2e;'>Thématique : </span>".$element["thematique"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                </div>
                            </div>

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i style="color:#7bba2e;" class="fa fa-phone"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="phoneAbout"><?php echo (@$element["phone"]) ? $element["phone"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card features">
                        <div class="card-body">
                            <?php if( $element["collection"] != Person::COLLECTION /*&& $type != Organization::COLLECTION */ ){ ?>
                            <div class="media contentInformation" id="divParentAbout">
                                <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Carried by"); ?></span>
                                    <p class="padding-top-10" id="parentAbout">
                                        <?php
                                        if(!empty($element["parent"])){
                                            $count=count($element["parent"]);
                                            foreach($element['parent'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php   }
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($element["collection"] == Event::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Organized by"); ?></span>
                                    <p class="padding-top-10" id="organizerAbout">
                                        <?php
                                        if(!empty($element["organizer"])){
                                            $count=count($element["organizer"]);
                                            foreach($element['organizer'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php   }
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($element["collection"] !=Poi::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-desktop gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="webAbout">
                                        <?php
                                        if(@$element["url"]){
                                            //If there is no http:// in the url
                                            $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
                                            echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php  if($element["collection"]==Organization::COLLECTION || $element["collection"]==Person::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-phone gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="fixeAbout">
                                        <?php
                                        $fixe = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["fixe"]))
                                            $fixe = ArrayHelper::arrayToString($element["telephone"]["fixe"]);

                                        echo $fixe;
                                        ?>
                                    </p>
                                </div>
                            </div>


                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-mobile gradient-fill"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="mobileAbout">
                                        <?php
                                        $mobile = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["mobile"]))
                                            $mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);
                                        echo $mobile;
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>

                     <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="card features hidden-xs margin-bottom-20">

                         </div>

                        <?php if(!empty($element["tags"])) { ?>
                        <div class="card features margin-bottom-20">
                            <div class="card-body">

                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-tags gradient-fill"></i></span>
                                    <div class="media-body">
                                        <ul class="tag-list no-padding">
                                            <?php
                                            if(!empty($element["tags"])){
                                                foreach ($element["tags"]  as $key => $tag) {
                                                    echo '<li class="tag"><a href="#search?text=#'.$tag.'"> <i class="fa fa-tag text-green"></i>&nbsp;'.$tag.'</a></li>';
                                                }
                                            }else{
                                                echo '<i>'.Yii::t("common","Not specified").'</i>';
                                            } ?>

                                            <!--<li><a href=""><i class="fa fa-tag"></i> tag</a></li>-->
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div >
                        <?php } ?>

                    </div>
                </div>
            </div>
    </div>
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <?php if(in_array($element["collection"], [Event::COLLECTION ,Project::COLLECTION ,Organization::COLLECTION])){ 
        $modeDate="recurrency";
        $title = Yii::t("common","When");
        $emptyval = ' Pas de date';
        if($element["collection"] == Organization::COLLECTION) {
            $title = 'Heure d\'ouverture';
            $emptyval = ' Pas d\'horaire d\'ouverture';
            $modeDate="openingHours";

        } else if($element["collection"]==Project::COLLECTION){
                    $modeDate="date";
            }
                echo $this->renderPartial('co2.views.pod.dateOH', array("element" => $element, "title" => $title, "emptyval" => $emptyval, "edit" => $edit , "openEdition" => $element["preferences"]["isOpenEdition"]));
        };?>
    
    <!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->
    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $element["collection"], "edit" => $edit , "openEdition" => $element["preferences"]["isOpenEdition"])); ?>
    <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
    <?php echo $this->renderPartial('co2.views.pod.socialNetwork', array("element" => $element, "type" => $element["collection"], "edit" => $edit , "openEdition" => $element["preferences"]["isOpenEdition"])); ?>
    
</div>
    
</div>

<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function () {
        controleAffiche();
    });

    function controleAffiche() {
        mylog.log(contextData.shortDescription, contextData.description);
        if((typeof contextData.shortDescription == "undefined") && (typeof contextData.description == "undefined")){
            $("#contenuDesc").addClass("hidden");
            //$("#btn-descriptions").html('<i class="fa fa-plus"> Ajouter');

            };
    };


    function afficheMap(){
            mylog.log("afficheMap");

            //Si contextData.geo est undefined caché la carte
            if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
            };

            var paramsMapContent = {
                container : "divMapContent",
                latLon : contextData.geo,
                activeCluster : false,
                zoom : 16,
                activePopUp : false
            };


            mapAbout = mapObj.init(paramsMapContent);

            var paramsPointeur = {
                elt : {
                    id : contextData.id,
                    type : contextData.type,
                    geo : contextData.geo
                },
                center : true
            };
            mapAbout.addMarker(paramsPointeur);
            mapAbout.hideLoader();


    };


    //var paramsPointeur[contextId] = contextData;
    var formatDateView = "DD MMMM YYYY à HH:mm" ;
    var formatDatedynForm = "DD/MM/YYYY HH:mm" ;

    jQuery(document).ready(function() {
        bindDynFormEditable();
        initDate();
        inintDescs();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-update-organizer").click(function(){
            updateOrganizer();
        });
        $("#btn-add-organizer").click(function(){
            updateOrganizer();
        });

        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });


    });

     $(".btn-update-info").off().on( "click", function(){

         var form = {
             saveUrl : baseUrl+"/costum/cmcashautebretagne/updateblock/",
             dynForm : {
                 jsonSchema : {
                    title : "Modifier les informations générales",
                     afterSave : function(){
                        $("#ajax-modal").fadeOut("slow");
                        location.reload();
                     },
                     properties : {
                        block : dyFInputs.inputHidden(),
                        typeElement : dyFInputs.inputHidden(),
                        isUpdate : dyFInputs.inputHidden(true),
                        name : dyFInputs.name("event"),
                        type : dyFInputs.inputSelect(tradDynForm.eventTypes,null,eventTypes, { required : true }),
                        tags : dyFInputs.tags(),
                        email : dyFInputs.text(),
                        url : dyFInputs.inputUrl(),
                        parent : {
                            inputType : "finder",
                            label : tradDynForm.whoiscarrypoint,
                            multiple : true,
                            rules : { lengthMin:[1, "parent"]}, 
                            initType: ["organizations", "projects", "events"],
                            openSearch :true
                        },
                        organizer : {
                            inputType : "finder",
                            id : "organizer",
                            label : tradDynForm.whoorganizedevent,
                            initType: ["projects", "organizations", "persons"],
                            multiple : true,
                            initMe : true,
                            openSearch :true,
                        },
                        phone : {
                            inputType : "text",
                            label : "Téléphone"
                        },
                        thematique : {
                            inputType : "select",
                            label : "Thématique",
                            placeholder : "Selectionner une thématique",
                            options : {
                                "Café littéraire" : "Café littéraire",
                                "Fait des bulles" : "Fait des bulles",
                                "Blablabla" : "Blablabla",
                                "Bien-être en famille" : "Bien-être en famille",
                                "Carte Blanche" : "Carte Blanche",
                                "En lien avec Trébeurden" : "En lien avec Trébeurden"
                            },
                            groupOptions : false,
                            groupSelected : false,
                            optionsValueAsKey:true,
                            rules : {"required": true}
                        }
                     }
                 }
             }
         };

         var dataUpdate = {
             block : "info",
             typeElement : contextData.type,
             id : contextData.id,
             name : contextData.name,
             url : contextData.url,
             tags : contextData.tags,
             phone : contextData.phone,
             type : contextData.typeEvent,
             thematique : contextData.thematique,
             email : contextData.email
         };

         dyFObj.openForm(form, "markdown", dataUpdate);
     });

    function inintDescs() {
        mylog.log("inintDescs");
        if($("#descriptionAbout").length > 0){
            if(canEdit == true || openEdition== true)
                descHtmlToMarkdown();
            mylog.log("after");
            mylog.log("inintDescs", $("#descriptionAbout").html());
            var descHtml = "<i>"+trad.notSpecified+"</i>";
            if($("#descriptionAbout").html().length > 0){
                descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
            }
            $("#descriptionAbout").html(descHtml);
            //$("#descProfilsocial").html(descHtml);
            mylog.log("descHtml", descHtml);
        }
    }

    function initDate() {//DD/mm/YYYY hh:mm

        formatDateView = "DD MMMM YYYY à HH:mm" ;
        formatDatedynForm = "DD/MM/YYYY HH:mm" ;
        
        mylog.log("formatDateView", formatDateView);
        //if($("#startDateAbout").html() != "")

            $("#startDateAbout").html(moment(contextData.startDateDB).local().locale(mainLanguage).format(formatDateView));

            $("#endDateAbout").html(moment(contextData.endDateDB).local().locale(mainLanguage).format(formatDateView));

        if($("#birthDate").html() != "")
            $("#birthDate").html(moment($("#birthDate").html()).local().locale(mainLanguage).format("DD/MM/YYYY"));
        $('#dateTimezone').attr('data-original-title', "Fuseau horaire : GMT " + moment().local().format("Z"));
    }

    function AddReadMore() {
    var showChar = 300;
    var ellipsestext = "...";
    var moretext = "Lire la suite";
    var lesstext = "Lire moins";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);
        //var h = content.substr(showChar-1, content.length - showChar);

        var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

        $(this).html(html);
        $(this).after('<a href="" class="morelink">' + moretext + '</a>');
      }

    });

    $(".morelink").click(function() {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
        $(this).prev().children('.morecontent').fadeToggle(500, function(){
          $(this).prev().fadeToggle(500);
        });
       
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.container').fadeToggle(500, function(){
          $(this).next().fadeToggle(500);
        });
      }
      //$(this).prev().children().fadeToggle();
      //$(this).parent().prev().prev().fadeToggle(500);
      //$(this).parent().prev().delay(600).fadeToggle(500);
      
      return false;
    });
}
$(function() {
    //Calling function after Page Load
    AddReadMore();
});



</script>

