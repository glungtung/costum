<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
$poiList = array();

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                           "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                           "type"=>"cms") );
} 
?>

<style type="text/css">
  @font-face{
      font-family: "montserrat";
       src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
       url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
  }.mst{font-family: 'montserrat'!important;}

  @font-face{
      font-family: "CoveredByYourGrace";
      src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
  }.cbyg{font-family: 'CoveredByYourGrace'!important;}
    
    
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: <?php echo $this->costum["colors"]["pink"]; ?>;
    border-radius: 10px;
    padding: 10px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    font-size: 1.5em
    /*min-height:100px;*/
  }
  .btn-main-menuW{
    background: white;
    color: <?php echo $this->costum["colors"]["pink"]; ?>;
    border:none;
    cursor:text ;
  }
  .btn-main-menu:hover{
    border:2px solid <?php echo $this->costum["colors"]["pink"]; ?>;
    background-color: white;
    color: <?php echo $this->costum["colors"]["pink"]; ?>;
  }
  .btn-main-menuW:hover{
    border:none;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){
    h1, h2, h3, h4, h5, h6 {
        display: block;
        font-size: 1.9em;
    }
  }
  #customHeader #newsstream .loader{
    display: none;
  }

</style>

<?php
//var_dump($el);exit;
	$params = [  
    "tpl" => $this->costum["slug"],
		"slug"=>$el["slug"],
		"canEdit"=>$canEdit,
		"el"=>$el  ];
	echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params, true ); 
?>  

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">

<?php
$color1 = "#E63458";
if(isset($this->costum["cms"]["color1"]))
  $color1 = $this->costum["cms"]["color1"];
?>

  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:<?php echo $this->costum["colors"]["grey"]; ?>; max-width:100%;">

    <div class="col-xs-12 no-padding" style=""> 
      
      <?php if( !isset(Yii::app()->session["userId"])){ ?>
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style=" padding-left:100px;background-color: <?php echo $this->costum["colors"]["grey"]; ?>; ">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="font-family: montserrat; background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12  ourvalues" style="text-align:center;">
              <h2 class="mst col-xs-12 text-center">
                <br>
                <?php 
                  if(isset($this->costum["cms"]["title1"])){
                    echo htmlentities($this->costum["cms"]["title1"]);
                  } else { ?> Open Form Wizard (With steps)<?php } 
                  if($canEdit)
                    echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='title1' data-path='costum.cms.title1'><i class='fa fa-pencil'></i></a>";
                  ?>
                </h2>

                <p class="mst" style="color:<?php echo $this->costum["colors"]["dark"]; ?>">
                  <?php 
                  if(isset($this->costum["cms"]["subtitle1"])){
                    echo htmlentities($this->costum["cms"]["subtitle1"]);
                  } else { ?>
                  Votre questionnaire contient des sections ou des étapes
                  <br/>Vous etes au bon endroit.<br/>
                  <?php } 
                  if($canEdit)
                    echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='subtitle1' data-path='costum.cms.subtitle1'><i class='fa fa-pencil'></i></a>";
                  ?>
                 <br/>
                </p>

                <h2 class="mst" style="color:<?php echo $this->costum["colors"]["pink"]; ?>" >JE PARTICIPE</h2>
                  
              
              <br/>
                <div class="col-xs-12" style="margin-bottom:40px;">
                  <div class="col-xs-12 col-sm-4">
                        <?php if( Person::logguedAndValid() ){ ?>
                    <span class="col-xs-12 text-center btn-main-menu btn-main-menuW ">Je suis Connecté !! <br>et je veux l'utopie</span>
                        <?php } else { ?>
                        <button  data-toggle="modal" data-target="#modalLogin" class="col-xs-12 btn-main-menu text-center" styl >JE PARTICIPE</button>
                        <?php } ?>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <a href="alert('open share it panel')" class="col-xs-12 btn-main-menu btn-main-menuW text-red" style="font-size: 1.5em; padding-top: 20px;"  >En partageant <br>ce sondage</a>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <a href="#results" class="col-xs-12 btn-main-menu">Voir les résultats</a>
                </div>
              </div>

            </div>


          </div>

          <div class="col-xs-12 text-center " style="font-size: 1.5em; padding-top: 20px;" >
          <?php 
                if(isset($this->costum["cms"]["textIntro"])){
                  echo htmlentities($this->costum["cms"]["textIntro"]);
                } else { ?> <b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

                <?php } 
                if($canEdit)
                  echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='textIntro' data-type='textarea' data-markdown='1'  data-path='costum.cms.textIntro' data-label='Expliquez les objectifs de ce formulaire ? '><i class='fa fa-pencil'></i></a>";
                ?>
          </div>
          
        </div><br/>

      </div>
      <?php } else { ?>

		
        <style type="text/css">
        	.monTitle{
        		border-top: 1px dashed <?php echo $this->costum["colors"]["pink"]; ?>; 
        		border-bottom: 1px dashed <?php echo $this->costum["colors"]["pink"]; ?>;
                margin-top: -20px;
        	}
        </style>
        <?php $formSmallSize =  12; ?>
    	<div class="col-md-12 col-lg-<?php echo $formSmallSize?> no-padding "><br/>
    		<div class="col-md-12 col-sm-12 col-xs-12 " style="background-color: white; ">
    		  <h1 class="margin-top-20 monTitle  padding-20 text-center cbyg" style="color:<?php echo $color1  ?>">
            <?php 
            if(isset($this->costum["cms"]["sec1Title"])){
              echo htmlentities($this->costum["cms"]["sec1Title"]);
            } else { ?> Sondage <?php } 
            if($canEdit)
              echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='sec1Title' data-type='text'  data-path='costum.cms.sec1Title' data-label='Titre '><i class='fa fa-pencil'></i></a>";
            ?>
          </h1>
          <div class="text-center">
          <?php if(isset($this->costum["form"]["startDate"])){ ?>
            Démarrage : <?php echo $this->costum["form"]["startDate"]; ?>
            <?php } 

            if(isset($this->costum["form"]["endDate"])){ ?>
            et se termine : <?php echo $this->costum["form"]["endDate"]; ?>
            <?php } ?>
            </div>
    		</div>

          <div class="row margin-top-20   padding-20" style="background-color: white; ">

            <div class="col-xs-12">

              <div class="padding-20 btnParticiper margin-top-20 Montserrat text-center" style="font-size:1.2em ">
                  <?php 
                if(isset($this->costum["cms"]["textIntro"])){
                  echo htmlentities($this->costum["cms"]["textIntro"]);
                } else { ?> Quelle est votre objectif avec ce formulaire <?php } 
                if($canEdit)
                  echo "<a class='btn btn-xs btn-danger editBtn' href='javascript:;' data-key='textIntro' data-type='textarea' data-markdown='1'  data-path='costum.cms.textIntro' data-label='Expliquez les objectifs de ce formulaire ? '><i class='fa fa-pencil'></i></a>";
                ?>
              </div> 


<script type="text/javascript">
var formInputs = {};
var answerObj = <?php echo (!empty($answer)) ? json_encode( $answer ) : "null"; ?>;
</script>
            
              <?php 
                $wizardUid = "wizardInfo";
                $params = [
                    "el"        => $el,
                    "color1"    => $color1,
                    "canEdit"   => $canEdit,
                    "allAnswers"=> $allAnswers,
                    "formList"  => $formList,
                    "what"      => "producteur" ,
                    "wizid"     => $wizardUid
                ];
                //var_dump($allAnswers);
                echo $this->renderPartial("costum.views.custom.kiltirvrac.offres",$params); 
               //var_dump($answer);
              
              if($showForm){
                $params = [
                    "formList"  => $formList,
                    "el"        => $el,
                    "active"    => "all",
                    "color1"    => $color1,
                    "canEdit"   => $canEdit,
                    "answer"    => $answer,
                    "showForm"  => $showForm,
                    "mode" => "w",
                    "wizid"     => $wizardUid
                ];
                echo $this->renderPartial("survey.views.tpls.forms.wizard",$params); 

              } else {
                  echo "<h4 class='text-center' style='color:".$color1."'><i class='fa fa-warning'></i> Une seul réponse n'est possible.</h4>";
                  echo "<a class='btn btn-primary' href='/costum/co/index/slug/".$el["slug"]."/answer/".$myAnswers[0]."'>Votre réponse</a>";
              }
                ?>

	        </div>
        </div>

		</div>

	<?php }  ?>
		
    </div>
  </div>


</div>

<script type="text/javascript">
//to edit costum page pieces 
 var configDynForm = <?php echo json_encode(Yii::app()->session['costum']['dynForm']); ?>;
//information and structure of the form in this page  
var tplCtx = {};

jQuery(document).ready(function() {

  mylog.log("render","/modules/costum/views/custom/co/process.php");
  contextData = {
      id : "<?php echo $this->costum["contextId"] ?>",
      type : "<?php echo $this->costum["contextType"] ?>",
      name : '<?php echo htmlentities($el['name']) ?>',
      profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
  };


});


</script>

