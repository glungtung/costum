<style type="text/css">
	#allAnswersList,.communityList ul,.tasksList ul{ list-style: none }
	#allAnswersList li{ padding:5px; border-bottom: 1px solid #ccc;  }
</style>

<div class="col-xs-12 text-center">
     <a href="javascript:;" class='btn btn- btn-default' id="showAnswerBtn"><i class="fa fa-bars"></i> Les <?php echo $what ?>s</a>
     <a href="#dashboard" class='lbh btn  btn-default'><i class="fa  fa-area-chart"></i> Observatoire Global</a>
     <a href="javascript:;" id="globalCommunityBtn" class=' btn  btn-default'><i class="fa  fa-group"></i> Communauté</a>
     <a href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo $el["slug"] ?>/answer/new" class='btn  btn-primary' id="showAnswerBtn"><i class="fa fa-plus"></i>  Nouveau <?php echo $what ?></a>
</div>

<div id="allAnswersContainer" class="hide col-xs-12 col-lg-offset-2 col-lg-8 margin-top-20">
	<ul id="allAnswersList">
	<?php 
	$lbl = $what." ";
	$ct = 0;
	$globalLinks = [];
	$gUids = [];
	if(!empty($allAnswers)){
		foreach ($allAnswers as $k => $ans) {
			$ct++;
			$lbl = $what." ".$ct;
			if(isset($this->costum["form"]["title"]))
			{
				$titlePath = explode( ".", $this->costum["form"]["title"] );
				if(isset($ans["answers"][$titlePath[0]][$titlePath[1]]))
				$lbl = $ans["answers"][$titlePath[0]][$titlePath[1]];
			}

			$lblp = "";
			$percol = "danger";
			$step = "ouvert";
			$icon = "folder-open-o";
			$localLinks = [];
			$uids = [];
			$todo = 0;
			$done = 0;
			$tasksPerson = [];
			if(!isset($ans["answers"])) {
				$lblp = "Commandez" ;
				$percent = 0;
			} else {
				$totalInputs = 0;
				$answeredInputs = 0;
				//var_dump(Yii::app()->session["forms"]);
				foreach (Yii::app()->session["forms"] as $fid => $f) 
				{
					$totalInputs += count($f["inputs"]);
					//echo "|".$f['id']."-fi=".count($f["inputs"]);
					if( isset( $ans["answers"][$fid] ) ){
						$answeredInputs += count( $ans["answers"][$fid] );
						$step = $f['name'];
					
					}
				}
				if(isset($ans["links"])) 
				{
					foreach ( $ans["links"] as $type => $ls ) 
					{
						if(!isset($localLinks[$type]))
							$localLinks[$type] = [];
						if(!isset($globalLinks[$type]))
							$globalLinks[$type] = [];

						foreach ($ls as $uid => $time) {
							if(is_string($uid) && strlen($uid) == 24 && ctype_xdigit($uid)){
								if(!in_array($uid, $localLinks[$type] ))
									$localLinks[$type][] = $uid;	
								if(!in_array($uid, $uids ))
									$uids[] = new MongoId( $uid );
								if(!in_array($uid, $globalLinks[$type] ))
									$globalLinks[$type][] = $uid;
								if(!in_array($uid, $gUids ))
									$gUids[] = new MongoId( $uid );
							}
						}
						
					}
				}

				//echo "tot".$totalInputs."-ans".$answeredInputs;
				$percent = floor( $answeredInputs*100 / $totalInputs );
				$percol = "primary";
				$lblp = "Livré";
			}

			$orderState = (isset($ans["state"])) ? $ans["state"] : "order"; // get || done
			$state = "danger";
			$lblp = "COMMANDER";
			if($orderState == "get"){
				$state = "warning";
				$lblp = "VENEZ RÉCUPÉRER";
			}
			else if($orderState == "done"){
				$state = "success";
				$lblp = "TERMINÉ";	
			}
				
			$liBg = ($todo>0) ? "style='background-color:lightGreen'" : "";
		?>

		<li class="answerLi col-xs-12" <?php echo $liBg ?>>
				<div class="col-xs-3 text-center">
					<a href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo $el["slug"] ?>/answer/<?php echo (String) $ans["_id"] ?>"> <?php echo $lbl; ?></a> 
				</div>
				
				<div class="col-xs-8">
					<span class="margin-5" style="font-size:0.8em"> <i class="fa fa-calendar"></i> <?php echo date("d/m/y H:i",$ans["created"]); ?></span>
					<?php if ( isset($ans["updated"] )) {?>
					<span class="margin-5" style="font-size:0.8em"> <i class="fa  fa-edit "></i> <?php echo date("d/m/y H:i",$ans["updated"]); ?></span>
					<?php } ?>

					
					<span class="margin-5 label label-<?php echo $state ?>"> <i class="fa fa-download"></i> <?php echo $lblp ?> </span>

					
					<br/>
					<?php if ( $percent!= 0 && count($uids)) {?>
					<a href="javascript:;" data-id='<?php echo $ans["_id"] ?>' class='answerCommunityBtn margin-5 btn btn-xs btn-default '> <i class="fa  fa-group "></i>Communauté <span class="margin-5  label label-primary"> <?php echo count($uids) ?> </span></a>
					<?php if ( count($tasksPerson)) {?>
					<a href="javascript:;" data-id='<?php echo $ans["_id"] ?>' class='answerTasksBtn margin-5 btn btn-xs btn-default '> <i class="fa  fa-cogs "></i>Tasks <span class="margin-5  label label-primary"> <i class="fa fa-square-o"></i> <?php echo $todo ?> </span> <span class="margin-5  label label-success"> <i class="fa   fa-check-square-o"></i> <?php echo $done ?> </span></a>
					<?php } ?>

					<a href="#dashboard.answer.<?php echo $ans["_id"] ?>" class='margin-5  lbh btn btn-xs btn-default '> <i class="fa  fa-pie-chart "></i> Observatoire Local</a>
					<?php } ?>
				</div>

				<div class="col-xs-1">
					<?php if($canEdit) { ?>
						<?php if(Yii::app()->session["userId"] == $ans["user"]) { ?>
					<a class='#' data-id="<?php echo $ans["_id"] ?>" href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo $el["slug"] ?>/answer/<?php echo $ans["_id"] ?>"> <i class="fa  fa-pensil"></i> </a> 
						<?php } ?>
					<a class='text-red pull-right deleteAnswer' data-id="<?php echo $ans["_id"] ?>" href="javascript:;"> <i class="fa  fa-trash"></i> </a> 
						
					<?php } ?>
				</div>
				
				<div id="community<?php echo $ans["_id"] ?>" class="communityList col-xs-12 col-lg-offset-2 col-lg-8  hide">
				<?php 
				if( count($uids) ){
					$people = PHDB::find(Person::COLLECTION,["_id" => array( '$in'=>$uids )], ["name","username"]);
					echo "<h5>Created by : ".$people[$ans['user']]["name"]."</h5><hr>";
					foreach ($localLinks as $type => $ls) {
						echo "<h5>".$type."</h5><ul>";
						foreach ($ls as $ix => $uid) {
							echo "<li><a class='lbh' href='#@".$people[$uid]["username"]."' >".$people[$uid]["name"]."</a></li>";
						}
						echo "</ul><hr>";
					} 
				}?>
				</div>
				<?php if ( count($tasksPerson)) {?>
				<div id="tasks<?php echo $ans["_id"] ?>" class="tasksList col-xs-12 col-lg-offset-2 col-lg-8  hide">
				<?php 
				
				foreach ($tasksPerson as $person => $ls) {
					echo "<h5>".$person."</h5><ul>";
					foreach ($ls as $ix => $t) {
						echo "<li>".$t."</li>";
					}
					echo "</ul><hr>";
				}?>
				</div>
				<?php } ?>
		</li>
		
	<?php } 
	} ?>
		<li class="text-center"><a href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo $el["slug"] ?>/answer/new" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i>  Ajouter</a></li>
	</ul>
</div>

<div id="globalCommunity" class="col-xs-12 col-lg-offset-2 col-lg-8  hide">
<?php 
if( count($gUids) ){
	$people = PHDB::find(Person::COLLECTION,["_id" => array( '$in'=>$gUids )], ["name","username"]);
	foreach ($globalLinks as $type => $ls) {
		echo "<h5>".$type."</h5><ul>";
		foreach ($ls as $ix => $uid) {
			echo "<li><a class='lbh' href='#@".$people[$uid]["username"]."' >".$people[$uid]["name"]."</a></li>";
		}
		echo "</ul><hr>";
	} 
}?>
</div>

<script type="text/javascript">

jQuery(document).ready(function() {

  mylog.log("render","/modules/costum/views/tpls/forms/cplx/answers.php");
 
<?php if($canEdit) { ?>
	

	  $('.deleteAnswer').off().click( function(){
      id = $(this).data("id");
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
                	//urlCtrl.loadByHash(location.hash);
                	$("#ansline"+id).remove();
                },"html");
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });


<?php } ?>
  $('#showAnswerBtn').off().on("click",function() { 
    $("#allAnswersContainer").toggleClass("hide");
    $('#<?php echo @$wizid ?>').toggleClass("hide");
    $('#globalCommunity').addClass("hide");
   });

  $('#globalCommunityBtn').off().on("click",function() { 
    $("#allAnswersContainer").addClass("hide");
    $('#<?php echo @$wizid ?>').addClass("hide");
    $('#globalCommunity').removeClass("hide");
   });

  $('.answerCommunityBtn').off().on("click",function() { 
    $( "#community"+$(this).data("id") ).toggleClass("hide");
   });
  $('.answerTasksBtn').off().on("click",function() { 
    $( "#tasks"+$(this).data("id") ).toggleClass("hide");
   });

  

});


</script>
