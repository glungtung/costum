<style>
	#modal-preview-coop{
		overflow: auto;
	}
	#poi .title{
		padding-bottom: 15px !important;
    	border-bottom: 1px solid rgba(150,150,150, 0.3);
	}
	.short-description{
		font-size:20px;
		text-align: justify;
	}
	.description-preview{
		text-align: justify;
	}
	.description-preview.activeMarkdown p, .description-preview.activeMarkdown li{
		font-size: 14px !important;
	}
	.link-files{
		border-radius: 3px;
		padding: 5px 10px;
	}
</style>
<div class="margin-top-25 margin-bottom-50 col-xs-12">
	<div class="col-xs-12 no-padding">
		<button class="btn btn-default pull-right btn-close-preview" style="margin-top:-15px;">
			<i class="fa fa-times"></i>
		</button>
		<?php if( $element["creator"] == Yii::app()->session["userId"] || 
				  Authorisation::canEditItem( Yii::app()->session["userId"], "poi", $id, @$element["parentType"], @$element["parentId"] ) ){ ?>
			
			<button class="btn btn-default pull-right margin-right-10 text-red deleteThisBtn" 
					data-type="poi" data-id="<?php echo $id ?>" style="margin-top:-15px;">
				<i class=" fa fa-trash"></i>
			</button>
			<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="poi" data-id="<?php echo $id ?>" 
			data-subtype="<?php echo $element["type"] ?>" style="margin-top:-15px;">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>
		<div id="poi" class="<?php echo @$element["type"]; ?>">
			<h2 class="col-xs-12 text-purple no-padding"><?php echo $element["name"] ?></h2>
			<div class="col-xs-12 auhtor-poi no-padding margin-bottom-10">
				<?php if(@$element["parent"]["name"]){ ?>
					<span class="font-montserrat col-xs-12">
						<i class="fa fa-angle-down"></i> <i class="fa fa-address-card"></i> 
						<?php echo Yii::t("common", "{what} published by {who}", 
							array("{what}"=>Yii::t("common",Element::getControlerByCollection(@$element["typeClassified"])),
								"{who}"=>"<a href='#page.type.".@$element["parentType"].".id.".@$element["parentId"]."' class='lbh'>".
											@$element["parent"]["name"].
										"</a>")
							);
						 ?> 
					</span>
				<?php }else if(@$element["parent"]){ ?>
					<span class="font-montserrat letter-blue">
						<?php echo Yii::t("common", "{what} published by",array("{what}"=>ucfirst(Yii::t("category", @$element["type"]))));  ?> :
						</span>	
					<?php foreach($element["parent"] as $key => $v){ 
							$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
							<a href='#page.type.<?php echo $v["type"] ?>.id.<?php echo $key ?>' class='lbh'>
								<img src='<?php echo $imgPath ?>' class='img-circle padding-right-10' width='25' height='25'/>
								<?php echo $v["name"] ?>
							</a> 
					
				<?php	}
				} ?>
			</div>
			<?php if(@$element["shortDescription"]){ ?>
			<span class="col-xs-12 short-description margin-bottom-15 no-padding"><?php echo $element["shortDescription"] ?></span>
			<?php } 
				if(@$element["type"]=="measure"){
					//var_dump($params);
				if(!empty($element["files"])){ ?> 
				<?php foreach($element["files"] as $k => $v){ ?>
						<div class='col-xs-12 padding-5 margin-top-30 margin-bottom-35'>
							<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files col-xs-6 col-xs-offset-3 text-center bg-orange">Voir le pdf</a>
						</div>
				<?php } } 
				}
				if(@$element["type"]=="doc"){
					//var_dump($params);
				if(!empty($element["files"])){ ?> 
				<?php foreach($element["files"] as $k => $v){ ?>
						<div class='col-xs-12 padding-5 margin-top-30 margin-bottom-35'>
							<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files col-xs-6 col-xs-offset-3 text-center bg-orange"><?php echo $v["name"] ?></a>
						</div>
				<?php } } 
				}
				if(@$element["type"]=="article"){
					echo $this->renderPartial('../pod/sliderMedia', 
								array(
									  "medias"=>@$element["medias"],
									  "images" => @$element["images"],
									  ) ); 
					
					echo $this->renderPartial('../poi/preview/article', 
								array("params"=>$element) );
				}
				else if(@$element["type"]=="measure" || @$element["type"]=="doc" || @$element["type"]=="faq"){
					echo $this->renderPartial('../poi/preview/measure', 
								array("params"=>$element) );
				}else if(@$element["type"]=="forum"){ ?>
					<div id="commentElement-preview" class="col-xs-12 no-padding"></div>
			<?php	}
			?>
		</div>
		
        <!--<a href="javascript:;" onclick="dySObj.openSurvey('octosource','json')" class="btn btn-primary col-xs-12"  style="width:100%">C'est parti <i class="fa fa-arrow-circle-right fa-2x "></i></a>-->
	</div>
</div>

<script type="text/javascript">

	var poiAlone=<?php echo json_encode($element); ?>;

	jQuery(document).ready(function() {	
		setTitle("", "", poiAlone.name);
		poiAlone["typePoi"] = poiAlone.type;
		poiAlone["type"] = "poi";
		poiAlone["typeSig"] = "poi";
		mylog.log("preview poiAlone", poiAlone);
		poiAlone["id"] = poiAlone['_id']['$id'];
		directory.bindBtnElement();
	  	if($("#poi #container-element-accordeon").length > 0){
	  		params={
	  			"images": [],
	  			"medias": []
	  		};
	  		if(typeof poiAlone.images != "undefined")
	  			params.images=poiAlone.images;
	  		if(typeof poiAlone.medias != "undefined")
	  			params.medias=poiAlone.medias;
	  		ajaxPost("#poi #container-element-accordeon", baseUrl+'/'+moduleId+'/pod/slidermedia', params, function(){},"html");
	  	}
	  	if($(".description-preview").hasClass("activeMarkdown")){
	  		descHtml = dataHelper.markdownToHtml($(".description-preview").html());
	  		$(".description-preview").html(descHtml);
	  	}
	  	if($("#commentElement-preview").length>0){
	  	 	getAjax("#commentElement-preview",baseUrl+"/"+moduleId+"/comment/index/type/poi/id/"+poiAlone['_id']['$id'],
				function(){},"html");
	  	}
	  	$("#modal-preview-coop .btn-close-preview, .deleteThisBtn").click(function(){
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
		});		
	});
</script>