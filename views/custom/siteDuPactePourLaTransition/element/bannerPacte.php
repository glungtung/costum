<style type="text/css">
	.name-header {
	    line-height: 30px;
	    font-size: 30px!important;
	    text-transform: none;
	}
	@media (min-width: 768px) {
		.contentHeaderInformation {
		    background-color: #592749;
		    bottom: 21%;
		    padding-top: 4px;
		    padding-bottom: 4px;
		    padding-left: 4px;
		    z-index: 10;
		    right: 0px;
		    /*border-top: 1px solid #592749;
		    border-bottom: 1px solid #592749;*/
		}
		.contentHeaderInformation:before {
		    content: " ";
		    height: 100%;
		    width: 0;
		    position: absolute;
		    right: 100%;
		    top: 0;
		    margin: 0;
		    pointer-events: none;
		    border-top: 32px solid transparent;
		    border-bottom: 32px solid transparent;
		    border-right: 32px solid #592749;
		    z-index: 10;
		}
	}
	
	@media (max-width: 767px) {
		.contentHeaderInformation {
		    background-color: #fff;
		    bottom: 0px;
		    padding-top: 20px;
		    padding-bottom: 15px;
		    z-index: 10;
		}
	}
	.user-image-buttons{
		display: none;
	}
	
</style>

<?php $bannerConfig=$this->appConfig["element"]["banner"]; ?>
<?php 
$thumbAuthor =  @$element['profilImageUrl'] ? 
Yii::app()->createUrl('/'.@$element['profilImageUrl']) : "";?>


<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 text-left no-padding" id="col-banner">
	<?php echo $this->renderPartial("co2.views.element.modalBanner", array(
			"edit" => true,
			"openEdition" => $openEdition,
			"profilBannerUrl"=> @$element["profilBannerUrl"],
			"title"=>"Ajustez / Recadrez",
			"element"=> $element,
			"forcedUnloggued"=>true)); 
	?>
	<div id="contentBanner" class="col-md-12 col-sm-12 col-xs-12 no-padding">
		<?php if (@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])){	
			$imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
				src="'.Yii::app()->createUrl('/'.$element["profilBannerUrl"]).'">';
			if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
				$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
							class="thumb-info"  
							data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
							data-lightbox="all">'.
							$imgHtml.
						'</a>';
			}
			echo $imgHtml;
			}else{
			if(isset($pageConfig["banner"]["img"]) && !empty($pageConfig["banner"]["img"]))
				$url=Yii::app()->getModule( "costum" )->assetsUrl.$pageConfig["banner"]["img"];
			else		
				$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';	
	
			echo '<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="'.Yii::t("common","Banner").'"
				src="'.$url.'">';
		} ?>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-5 hidden-xs contentHeaderInformation pull-right">	<div class="col-xs-12 text-center" style="background-color: #fff;padding: 15px;"> 
			<h2 class="header-aligne">
				<span id="nameHeader">
					<div class="name-header"><?php echo @$element["name"]; ?></div>
				</span>
			</h2>
		</div>
	</div>
	<div class="col-xs-12 visible-xs contentHeaderInformation">
		<div class="col-xs-12 text-center">
			<h2 class="header-aligne">
				<span id="nameHeader">
					<div class="name-header"><?php echo @$element["name"]; ?></div>
				</span>
			</h2>
		</div>
	</div>
</div>