<style type="text/css">
	.editCollectif, .editContract, .addContrat, .seeUnvalidatedContract{
		display: none;
	}
	.empty-data {
		font-size: 18px!important;
    	text-align: center;
	}
	.txt-comment {
		display: flex;
    	justify-content: center;
	}
</style>
<?php 
	$where = array(
			"source.key" => "siteDuPactePourLaTransition",
            "parent.".(string)$element["_id"] => array('$exists' => true)
        );
        
    $contrat = PHDB::find(Poi::COLLECTION,$where);
        //var_dump($contrat);exit;

    $where = array(
			"source.key" => "siteDuPactePourLaTransition",
            "type"=> "measure"
        );
    $att = array("name" => 1);
        
    $mesurePacte = PHDB::findAndSort(Poi::COLLECTION,$where,$att);

    function mySort($a, $b){ 
        if(isset($a["name"]) && isset($b["name"])){
	   		return ( ($b["name"]) < $a["name"] );
	    } else{
			return false;
		}
    }


 ?>	

<div class="col-xs-12 col-lg-3 col-md-3 col-sm-3">
	<ul class="nav nav-pills nav-stacked">
	<!-- Collectif ---------------------------------------------->
	  <li class="active">
	  	<a href="javascript:;" class="contrat-list" data-contrat="dataHead1">
	  		<i class="fa fa-chevron-down"></i> Collectif actif sur :
	  	</a>
	  </li>
	  <?php if(!empty($element["scope"])){
		  	$inin = 1;
		  	$ifArrondissementCol = [];
			foreach ($element["scope"]  as $key => $valScope) { ?>
	  			<?php 
  					$temp = explode(' ', $valScope["cityName"] ); 
  					if($temp[sizeof($temp)-1] == "Arrondissement"){
  						if(!in_array($temp[0],$ifArrondissementCol))
  							$ifArrondissementCol[] = $temp[0];
  					}else{ ?>
  						<li>
	  						<a href="javascript:;" class="contrat-list" data-contrat="dataCol<?php echo $inin; ?>">
	  							<i class="fa fa-chevron-right"></i> <?php echo ucwords(strtolower($valScope["cityName"])); ?>
	  						</a>
  						</li>
	  			<?php $inin++; 
	  				}
	  		}  
	  		?>
		  		
	<?php 
		foreach($ifArrondissementCol as $key => $valScope ){ ?>
			<li>
	  			<a href="javascript:;" class="contrat-list" data-contrat="dataCol<?php echo $inin; ?>">
	  				<i class="fa fa-chevron-right"></i> <?php echo ucwords(strtolower($valScope)); ?>
	  			</a>
  			</li>
		<?php $inin++; } ?>
	  <?php }?>
	<!--end Collectif ---------------------------------------------->

	<!-- Contrat ---------------------------------------------->
	  <li class="active">
	  	<a href="javascript:;" class="contrat-list" data-contrat="dataHead">
	  		<i class="fa fa-chevron-down"></i> Contrat
	  	</a>
	  </li>

	  <?php
		if(!empty($contrat)){
			$inc = 1;
			$ifArrondissementContrat = [];
		   	foreach ($contrat  as $key => $value) {  
		   		if(isset($value["scope"])){
		   			$valueScope = array_keys($value["scope"])[0];
		   			$tmp = explode(' ', $value["scope"][$valueScope]["cityName"] );  
			   			if($tmp[sizeof($tmp)-1] == "Arrondissement"){
		  					if(!in_array($tmp[0],$ifArrondissementContrat)){
		  						$ifArrondissementContrat[] = $tmp[0];
		  					}
		  				}else{ ?>	
					  		<li>
					  			<a href="javascript:;" class="contrat-list" data-contrat="data<?php echo $inc; ?>">
					  				<i class="fa fa-chevron-right"></i> 
					  				<?php if(isset($value["scope"])){
					  					$valueScope = array_keys($value["scope"])[0];
										//$value["ownerList"]; 
										echo ucwords(strtolower( $value["scope"][$valueScope]["cityName"])); 
									}?> 
					  			</a> 
					  		</li>
		  		<?php $inc++;
		  			} 
		  		}
			} 
	  ?>
	  <?php 
		foreach($ifArrondissementContrat as $key => $valScope ){ ?>
			<li>
	  			<a href="javascript:;" class="contrat-list" data-contrat="data<?php echo $inc; ?>">
	  				<i class="fa fa-chevron-right"></i> <?php echo ucwords(strtolower($valScope)); ?>
	  			</a>
  			</li>
		<?php $inc++; } ?>
	  <?php }?>
	  <!-- end Contrat ---------------------------------------------->
	</ul>
</div>
<div class="col-xs-12 col-lg-9 col-md-9 col-sm-9">
	<div class="col-xs-12 no-padding margin-bottom-30">
		<div class="col-md-8 col-sm-8 col-xs-12 margin-b-5 elue-data" data-contrat="dataHead1">
	        <span class="head-title-pact">
	            <i class="fa fa-file-text-o"></i>&nbsp;LE COLLECTIF   
	        </span>
	    </div>
	    <?php //if($edit){?>
		    <div class=" col-md-4 col-sm-4 col-xs-12 margin-b-5 editCollectif">
		        <button class="btn-add-pacte btn pull-right tooltips editElement" onclick="" data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="Editer le collectif">
		            <i class="fa fa-edit"></i>&nbsp;Editer                
		        </button>
		    </div>
		<?php //} ?> 
	</div>   
	<!--
    <hr class="hr-pacte">
	-->
	<div class="services-list">
        <div class="row">

            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="service-block" style="visibility: visible;">
                    <div class="ico fa fa-at highlight"></div>
                    <div class="text-block">
                        <div class="name">Adresse(s) de contact</div>
                        <div class="info">
                        	<?php if(isset($element["publicMail"])){
                        			echo "<span>".$element["publicMail"]."</span><br/>";
                        		} ?>
                        	<span><?php if(isset($element["email"])) echo $element["email"]; ?></span>
                        </div>
                    </div>
                </div>
            </div>

        
            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="service-block" style="visibility: visible;">
                    <div class="ico fa fa-desktop highlight"></div>
                    <div class="text-block">
                        <div class="name">Site web</div>
                        <div class="info"><?php echo (isset($element["url"])) ? $element["url"] : '<i>Non renseigné</i>'; ?>
                        </div>
                    </div>
                </div>
            </div>





        	<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <div class="service-block" style="visibility: visible;">
                    <div class="ico fa fa-map highlight"></div>
                    <div class="text-block">
                        <div class="name">Actif sur :</div>
                        <div class="info">
							<?php if(!empty($element["scope"])){
								$in = 1;
								$ifArrondissementColList = [];
	   							foreach ($element["scope"]  as $key => $valScope) { 
							?>
								<?php 
			  					$temp = explode(' ', $valScope["cityName"] ); 
			  					if($temp[sizeof($temp)-1] == "Arrondissement"){
			  						if(!in_array($temp[0],$ifArrondissementColList))
			  							$ifArrondissementColList[] = $temp[0];
			  					}else{ ?>
		                        	<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6 elue-data content-elue" data-contrat="dataCol<?php echo $in; ?>"
		                        	>
										<div class="our-team-main">
										
											<div class="team-front">
												<span class="fluid-circle">
													<i class="fa fa-map-marker"></i>
												</span>
												<h3><?php echo $valScope["cityName"]; ?></h3>
												<!--<p>Web Designer</p>-->
											</div>
											
											<div class="team-back">
												<label>Nom du commune : </label>&nbsp; <?php echo $valScope["cityName"]; ?><br>
												<label>Code Postal : </label>&nbsp; <?php echo $valScope["postalCode"]; ?><br>
											</div>

										
										</div>
									</div>
							<?php $in++;
									}
								}; ?>

							<?php 
							foreach($ifArrondissementColList as $key => $valScope ){ ?>
								<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6 elue-data content-elue" data-contrat="dataCol<?php echo $in; ?>"
		                        	>
										<div class="our-team-main">
										
											<div class="team-front">
												<span class="fluid-circle">
													<i class="fa fa-map-marker"></i>
												</span>
												<h3><?php echo ucwords(strtolower($valScope)); ?></h3>
												<!--<p>Web Designer</p>-->
											</div>
											
											<div class="team-back">
												<label>Nom du commune : </label>&nbsp; <?php echo ucwords(strtolower($valScope)); ?><br>
												<label>Code Postal : </label>&nbsp; 
												<?php if (ucwords(strtolower($valScope)) == "Lyon") 
													echo "69000";
													else if (ucwords(strtolower($valScope)) == "Paris") 
														echo "75000";
													else if (ucwords(strtolower($valScope)) == "Marseille") 
														echo "13000";
												 ?>
												 <br>
											</div>

										
										</div>
									</div>
							<?php $in++; } 
							}; 
							?>
							
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <div class="service-block" style="visibility: visible;">
                    <div class="ico fa fa-quote-right highlight"></div>
                    <div class="text-block">
                        <div class="name">Description</div>
                        <div class="infoDesc activeMarkdown"><?php echo (isset($element["description"])) ? $element["description"] : '<i>Non renseignée</i>'; ?>
                        </div>
                        <!--<div class="text"> </div>-->
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="service-block" style="visibility: visible;">
                    <div class="ico fa fa-calendar highlight"></div>
                    <div class="text-block">
                        <div class="name">Prochains événements</div>
                        <div class="info"><?php echo (isset($element["nextEvents"])) ? $element["nextEvents"] : '<i>Non renseignés</i>'; ?></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                <div class="service-block" style="visibility: visible;">
                    <div class="ico fa fa-users highlight"></div>
                    <div class="text-block">
                        <div class="name">Associations membres</div>

                        <div class="info"><?php echo (isset($element["localMembers"])) ? $element["localMembers"] : '<i>Non renseignées</i>'; ?>
                    	</div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>


	<div class="pod-info-Description"  id="pod-info-Description">
		<div class="col-xs-12 no-padding margin-bottom-30">
			<div class="col-md-9 col-sm-8 col-xs-12 margin-b-5  elue-data" data-contrat="dataHead">
		        <span class="head-title-pact">
		            <i class="fa fa-file-text-o"></i>&nbsp;Les engagements des Elu·es   
		        </span>
		    </div>
		    <div class=" col-md-3 col-sm-4 col-xs-12 margin-b-5 addContrat">
		    	<?php //if($edit){?>
			        <button class="btn-add-pacte btn pull-right tooltips" onclick="dyFObj.openForm('contract');" data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="Ajouter Contrat">
			            <i class="fa fa-plus"></i>&nbsp;Ajouter Contrat                
			        </button>
		        <?php //} ?>
		    </div>
		</div>
		<!--
	    <hr class="hr-pacte">
		-->
		<!-- a partir-->
		<?php
			if(!empty($contrat)){
				$incr = 1;
	   			foreach ($contrat  as $key => $value) { 
					$classSee="";
					$spanStatus="";
					$valueScope = array_keys($value["scope"])[0];
					if(isset($value["source"]["toBeValidated"])){ 
	   					$classSee= " seeUnvalidatedContract";
	   					$spanStatus="<span class='text-orange'>En attente de validation</span>";
	   				}
	   	?>
			<div  class="elue-data content-elue col-xs-12 no-padding bg-white <?php echo $classSee ?>" data-contrat="data<?php echo $incr; ?>">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 contract-txt  padding-top-20">
					<?php echo $spanStatus ?>
					<p class="p-detail">
						 Les élu·es majoritaires de <b>
						 	
						 	<?php 
			  					$temp = explode(' ', $value["scope"][$valueScope]["cityName"] ); 
			  					if($temp[sizeof($temp)-1] == "Arrondissement"){
			  						echo $temp[0];
			  					}else{ ?>

						 <?php echo (isset($value["scope"][$valueScope])) ? $value["scope"][$valueScope]["cityName"]: '(...)'; }; ?> 
						 </b> se sont engagé·es <b>
						 sur <b>
						 <?php echo (isset($value["links"]["measures"])) ? count($value["links"]["measures"]) : '(...)'; ?>
						  mesures le 
						 <?php echo (isset($value["signedOn"])) ? $value["signedOn"] : '(...)'; ?> </b>
		
						   
						</b>
		
					</p>
				</div>

				<?php //if($edit){?>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 editContract" style="padding-bottom: 20px;">
						<a href="javascript:;" onclick="dyFObj.editElement('poi', '<?php echo $key ?>','contract')" class="btn bg-purple text-white">
							<i class="fa fa-edit"></i>&nbsp;Editer
						</a>
						<!--<a href="javascript:;" data-type="poi" data-id="<?php echo $key ?>" class="deleteThisBtn btn bg-red letter-white">
							<i class="fa fa-trash"></i>
						</a>-->
					</div>
				<?php //} ?>

				<div class="second-title">
				<!--	<span class="section-second-title">
						<i class="fa fa-map-marker"></i>
						<?php if(isset($value["scope"])){
							$valueScope = array_keys($value["scope"])[0];
							//$value["ownerList"]; 
							echo $value["scope"][$valueScope]["cityName"]; 
						}?> 
					</span>
				-->
					<span class="section-second-title">
						 <i class="fa fa-user"></i> Maireᐧsse : 
						<?php echo $value["name"]; ?> 
					</span>
					<span class="section-second-title">
						 <i class="fa fa-hand-o-up "></i>
						<?php echo (isset($value["shortDescription"])) ? $value["shortDescription"] : 'Orientation politique non renseignée'; ?> 
					</span>

				</div>


				<div class="col-md-4 col-sm-4 col-xs-12 col-lg-4  text-center">
			    	<a href="javascript:;" data-id="<?php echo $key; ?>" data-show="true" class="link-pacte showMeasures"><i class='fa fa-arrow-down'></i>
			    		Voir les mesures
			    	</a>
			    </div>

			    <?php 
					$where = array("id" => (string)$value["_id"], "subKey"=>"contractPdf") ;
				        
				    $document = PHDB::findOne(document::COLLECTION,$where);

				       // var_dump($document["folder"].$document["name"]);exit;
				        //var_dump(Yii::app()->createUrl);exit;
				 ?>
			    	
			    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4 text-center">
			    	<?php if (isset($document) && !empty($document)) { ?>
			    		<a href="<?php echo Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$document["moduleId"].'/'.$document["folder"].'/'.$document["name"]; ?>" target='_blank' class="link-pacte" data-toggle="collapse" data-target="#show-contrat">
			    		<i class='fa fa-file-pdf-o'></i>
			    		Voir le contrat
			    		</a>
			    	<?php }else if(isset($value["urlContract"]) && !empty($value["urlContract"])){ ?>
			    		<a href="<?php echo $value["urlContract"]; ?>" target='_blank' class="link-pacte" data-toggle="collapse" data-target="#show-contrat">
			    		<i class='fa fa-file-pdf-o'></i>
			    		Voir le contrat
			    		</a>
			    	<?php } ?>
			    	
			    </div>
			    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4 text-center">
			    	<a href="javascript:;" data-id="<?php echo $key; ?>" data-show="true" class="link-pacte showSuivi" ><i class='fa fa-arrow-down'></i>
			    		Voir le suivi
			    	</a>
			    </div>

				<?php //var_dump($value["measure"]) ;
				if(!empty($value["links"]["measures"])){ ?>
						<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 contentListMesure<?php echo $key;?> no-padding mesureList">
							<div class="second-title title-mesure text-center">
			                	<i class="fa fa-list-ul"></i>&nbsp;
			                	<span class="underline underline--double">Liste des mesures</span>
			                </div>
							<ul class="timeline">
							<?php 
								//asort($value["links"]["measures"]);
								
						       	//usort($value["links"]["measures"],"mySort");
						       	

								foreach($value["links"]["measures"] as $k => $val){
									$value["links"]["measures"][$k]["id"] = $k;
									$nameMes = isset($mes["name"]) ? $mes["name"] : '';
									if (empty($val["name"]) && isset($mesurePacte[$k]["name"]) ) {
											$value["links"]["measures"][$k]["name"] = $mesurePacte[$k]["name"];
									}
									
								}
								usort($value["links"]["measures"], "mySort");
								$measures = [];
								foreach($value["links"]["measures"] as $val){
								$measures[$val["id"]] = $val;
								}
								
								$value["links"]["measures"] =  $measures;
						       	//var_dump($measures);exit;
								foreach ($value["links"]["measures"]  as $k => $mes){ 
									?>
									<li>
										<div class="col-xs-12 desc-network">
											<a href="#page.type.poi.id.<?php echo $k; ?>" class="col-xs-12 no-padding lbh-preview-element" style='font-size:18px;'><?php echo $mes["name"]; ?></a>
												<span>Niveau d'ambition <?php echo (isset($mes["level"])) ? $mes["level"] : '(...)'; ?></span>
										</div>
									</li>
							<?php } 
								$i = 1;
								if (isset($value["localMeasures"])){
									foreach ($value["localMeasures"]  as $k => $mes){ ?>
										<?php if (isset($mes["description"]) && $mes["description"] != "") { ?>
										<li>
											<div class="col-xs-12 desc-network">
												<span class="col-xs-12 no-padding bold" style="color: #2C3E50;font-size:18px;"><?php echo (isset($mes["name"]))  ? $mes["name"] : "Mesure locale ".$i; ?></span> 
												<span><?php echo (isset($mes["description"])) ? $mes["description"] : '
												(...)'; ?></span>
											</div>
										</li>

										<?php
									} 
								?>

									<?php
									$i++;
									} 
								}?>
							</ul>
						</div>
				<?php } ?>			

			    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 contentListSuivi<?php echo $key;?> no-padding suiviList">
					 <div class="second-title title-suivi text-center">
	                	<i class="fa fa-refresh"></i>&nbsp;
	                	<span class="underline underline--double">Suivi des engagements</span>
			         </div>
					 <div class="media comment-box">
			            <div class="media-left">
			                <a href="#">
			                    <span><i class="fa fa-comments"></i></span>
			                </a>
			            </div>
			            <div class="media-body">
			                <div class="media-heading second-title">
			                	<span class="underline underline--double">Commentaire du collectif</span>
			                </div>
			                <p><?php echo (isset($value["commentEngagement"])) ? $value["commentEngagement"] : '<span class="txt-comment">Plus d\'informations sur le suivi prochainement par ici !</span>'; ?></p> 
			              
			            </div>
			        </div>
			        <?php $where = array("id" => (string)$value["_id"], "subKey"=>"compteRendu") ;
				        
				    $cr = PHDB::findOne(document::COLLECTION,$where); ?>

			        <div class="compte-rendu text-center">
			        	<?php if(isset($cr) && !empty($cr)){ ?>
			        	<a href="<?php echo Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$cr["moduleId"].'/'.$cr["folder"].'/'.$cr["name"]; ?>" target="_blank" class="btn-compte-rendu">
			        		<span style="width: 45px;">
			        			<i class="fa fa-file-pdf-o"></i>
			        		</span>&nbsp;
							<span class="underline underline--double">
								Voir le compte-rendu du collectif
							</span>
			        	</a>
			        	<?php }else { ?> 
			        		<span class="italic">Aucun compte-rendu</span>
			        	<?php } ?>
			        </div>

			    </div>
				
				
			</div>	
			<div class="col-xs-12" style="padding: 20px 0px 30px 0px;">
				<hr class="end-contract">
			</div>
				<?php $incr++;
			}
		}else{
			echo "<p class='empty-data'>Aucun engagement n'a encore été signé par ce collectif avec des élu·es de son territoire</p>";
		}; ?>
	</div>
</div>

<script type="text/javascript">
		
	jQuery(document).ready(function() {
		$('h3#ajax-modal-modal-title').html('<i class="fa fa-crop"></i> Ajustez / Recadrez votre image pour avoir une jolie bannière');
		coInterface.bindLBHLinks();	

		if(location.hash.indexOf('edit.'+contextData.id) > 0) {
			$(".editCollectif, .addContrat, .editContract, .user-image-buttons, .seeUnvalidatedContract").show();
			directory.bindBtnElement();
		}
		//else 
		//	$(".editElement, #pod-info-Description .addContrat").hide();

		/****uppercase C in coller une image de votre navigateur ici****/
		/*$(".editElement, #pod-info-Description .addContrat").click(function(){
			setTimeout(function(){
				var input = $('input[placeholder="coller une image de votre navigateur ici"]');
				var placeholderValue = input.attr('placeholder'); 
				input.attr('placeholder',placeholderValue[0].toUpperCase()+placeholderValue.slice(1));
			},1500)
		});*/
		
		$('.suiviList,.mesureList').hide();
		var contrat = <?php echo json_encode($contrat) ?>;
		//mylog.log("milay",contrat);
		if($(".infoDesc").hasClass("activeMarkdown")){
            descHtml = dataHelper.markdownToHtml($(".infoDesc").html());
            $(".infoDesc").html(descHtml);
        }

		$(".showMeasures").off().on("click", function(){
 			idContact=$(this).data("id");
			$(".contentListSuivi"+idContact).hide(300);
			$(".showSuivi").html("<i class='fa fa-arrow-down'></i> Voir le suivi");
 			if($(".contentListMesure"+idContact).is(":visible")){
 				$(".contentListMesure"+idContact).hide(300);
 				$(this).html("<i class='fa fa-arrow-down'></i> Voir les mesures");
 			}else{
 				$(".contentListMesure"+idContact).show(300);
 				$(this).html("<i class='fa fa-arrow-up'></i> Cacher les mesures");

 			}
 		});

 		$(".title-mesure").off().on("click", function(){
 			$('.mesureList').hide(300);
 			$('.showMeasures').html("<i class='fa fa-arrow-down'></i> Voir les mesures");
 		});
 		$(".title-suivi").off().on("click", function(){
 			$('.suiviList').hide(300);
 			$(".showSuivi").html("<i class='fa fa-arrow-down'></i> Voir le suivi");
 		});

 		$(".showSuivi").off().on("click", function(){
 			idContact=$(this).data("id");
 			$(".contentListMesure"+idContact).hide(300);
 			$(".showMeasures").html("<i class='fa fa-arrow-down'></i> Voir les mesures");
 			if($(".contentListSuivi"+idContact).is(":visible")){
 				$(".contentListSuivi"+idContact).hide(300);
 				$(this).html("<i class='fa fa-arrow-down'></i> Voir le suivi");
 			}else{
 				$(".contentListSuivi"+idContact).show(300);
 				$(this).html("<i class='fa fa-arrow-up'></i> Cacher le suivi");

 			}
 		});

	 	$(".editElement").click(function(){
			dyFObj.editMode=true;
			//uploadObj.set(type, id);
			uploadObj.update = true;
			dyFObj.currentElement={type : contextData.type, id : contextData.id};
			dataEdit=jQuery.extend(true, {},contextData);
			if(typeof contextData.typeOrga != "undefined")
				dataEdit.type=contextData.typeOrga;
			dyFObj.openForm('collectif', null, dataEdit);
		});
	});

	$('.contrat-list').click(function (ev) {
        ev.preventDefault();
        var thisAttr = $(this).attr("data-contrat");
        var scrollTo = $('.elue-data[data-contrat="' + thisAttr + '"]').offset().top;

        $(this).addClass("active2").siblings().removeClass("active2");

        //$(".elue-data").removeClass("active");
        //$('.elue-data[data-contrat="' + thisAttr + '"]').addClass("active");

        $('html, body').animate({
            scrollTop: scrollTo - 120
        }, 500);
    });
</script>





                    