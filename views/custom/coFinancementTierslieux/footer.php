<style type="text/css">
    .footer-link{
        font-size:16px;
        text-decoration:none;
    }

    .footer-link:hover{
        text-decoration:underline;
    }



</style>

<footer style="min-height:100px;background-color:#FFB77C" class="text-center col-xs-12 pull-left">
        <div class="col-xs-12">
                <a style="position:absolute;bottom:40%;right:10px"class="btn btn-primary tooltips lbh-anchor" href="#adopteUnCommun" data-toggle="tooltip" data-placement="left" data-original-title="Adopte un commun"><span><i class="fa fa-chevron-up"></i></span></a>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 no-padding">
                    
                    <img style="width: inherit;" src='<?php echo Yii::app()->getModule( "costum" )->assetsUrl."/images/coFinancementTierslieux/logo_LCTL03.png" ?>'/>
                
                    <p class="footer-link">Contact : <a style="text-decoration:none;color:white" href="mailto:bonjour@lescommunsdestierslieux.org">bonjour@lescommunsdestierslieux.org</a></p>
                    <p class="footer-link"><a target="_blank" style="text-decoration:none;color:white" href="https://tiers-lieux.us20.list-manage.com/subscribe?u=948f679787c39d6f8f150463c&id=02edb70a3c">S'inscrire à la liste de diffusion (mailing list)</a></p>

                    <!-- <p style="font-size:16px;text-decoration:none;">Téléphone : 06.00.00.00.00</p> -->
                        <div class="col-xs-12 margin-top-20">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                <p style="margin:0px;font-size: 10px">Powered by</p>
                                <a href="https://communecter.org/#@openAtlas" target="_blank">
                                    <div style="min-height: 40px;display: inline-grid;" class="col-xs-12">
                                        <img class="img-responsive" style="max-height: 80%;max-width:50%;vertical-align: middle;    margin: auto;" src="<?php echo Yii::app()->getModule( "costum" )->assetsUrl."/images/OpenAtlas/logo-openatlas.png" ?>">
                                    </div>
                                </a>

                            </div>
                            <!-- <div class="col-xs-12 col-sm-6">
                                <p style="margin:0px;font-size: 10px">Based on</p>
                                <a href="https://gitlab.adullact.net/pixelhumain" target="_blank">
                                    <div style="min-height: 40px;display: inline-grid;" class="col-xs-12">
                                        <img class="img-responsive" style="max-height: 80%;max-width:50%;vertical-align: middle;    margin: auto;" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/logo-communecter.png">
                                    </div>    
                                </a>
                            </div> -->
                        </div>
                </div>
        </div>
</footer>

<script>
jQuery(document).ready(function() {
    //urlCtrl.loadByAnchor();
});    
</script>
