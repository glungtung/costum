<style>
    hr
    {
        border: 1px solid #858585;
    }

    .team
    {
        margin-top: 25px;
    }

    .team h1
    {
        font-weight: normal;
        font-size: 22px;
        margin: 10px 0 0 0;
    }

    .team h2
    {
        font-size: 16px !important;
        font-weight: lighter;
        margin-top: 5px;
    }

    .team .img-box
    {
        opacity: 1;
        display: block;
        position: relative;
    }

    .team .img-box:after
    {
        content: "";
        opacity: 0;
        background-color: rgba(0, 0, 0, 0.75);
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
    }

    .img-box ul
    {
        position: absolute;
        z-index: 2;
        bottom: 50px;
        text-align: center;
        width: 100%;
        padding-left: 0px;
        height: 0px;
        margin: 0px;
        opacity: 0;
    }

    .team .img-box:after, .img-box ul, .img-box ul li
    {
        -webkit-transition: all 0.5s ease-in-out 0s;
        -moz-transition: all 0.5s ease-in-out 0s;
        transition: all 0.5s ease-in-out 0s;
    }

    .img-box ul i
    {
        font-size: 20px;
        letter-spacing: 10px;
    }

    .img-box ul li
    {
        width: 30px;
        height: 30px;
        text-align: center;
        border: 1px solid #fff;
        margin: 2px;
        padding: 5px;
        display: inline-block;
    }

    .img-box a
    {
        color: #fff;
    }

    .img-box:hover:after
    {
        /* opacity: 1; */
    }

    .img-box:hover ul
    {
        /* opacity: 1; */
    }

    .img-box ul a
    {
        -webkit-transition: all 0.3s ease-in-out 0s;
        -moz-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }

    .img-box a:hover li
    {
        border-color: #FFEA05;
        color: #FFEA05;
    }

    .img-box a
    {
        color: #FFEA05;
    }

    .img-box a:hover
    {
        text-decoration: none;
        color: #519548;
    }
    .card-team{
        position: relative;
        /* height: 614px; */
        width: 100%;
        padding-top: 15px;
    }
    .team{
        box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
        flex: 0 0 calc(98%/3);
        padding: 5px 10px;
        margin-right : 7px
    }
    .bodySearchContainer{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: normal;
        align-items: stretch;
    }
    .divEndOfresults{
        display: none;
    }
    .team .desc{
        word-break: break-word;
        line-height:120%;
        text-align: left !important;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp:3;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .img-responsive-action{
        width: 100%;
        height: 253px;
        object-fit: cover;
    }
    @media only screen and (min-width : 320px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (min-width : 480px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 49%;
            padding: 5px 10px;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/2);
            padding: 5px 10px;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/3);
            padding: 5px 10px;
        }
    }
    #filterContainerproposalInside {
        overflow-y: unset !important;
        max-height: 800px;
    }
    .aap-footer,.aap-options-btn,.rater-sheet,.openAnswersComment,.votant-modal,.view-contributors,.rebtnFinancer{
        display: none !important;
    }

    .portfolio-modal.modal#modal-action .modal-content{
        background: rgb(0 0 0 / 72%);
    }
    .portfolio-modal.modal#modal-action .modal-content .container{
        background: #fff;
        padding-top: 10px;
        border-radius:5px;
    }
    .portfolio-modal.modal#modal-action .modal-content .container .row h2{
        padding-left:15px;
    }
    .portfolio-modal.modal#modal-action .close-modal .lr .rl,
    .portfolio-modal.modal#modal-action .close-modal .lr {
        background-color: #fff;
    }
    .container-filters-menu .dropdown .btn-menu, .container-filters-menu .filters-btn {
        border: 1px solid #7da53d !important;
    }
    .searchBar-filters .main-search-bar-addon, .searchBar-filters .main-search-bar {
        border: 1px solid #9fbd38 !important;
    }
    #filterContainerproposal #filterContainerInside {
        overflow-y: unset !important;
        max-height: 800px;
    }
    .team .btn-tag {
        font-weight: 300;
        padding: 2px 7px 4px;
        background: #777;
    }
    #aapViewFormModal{
        z-index: 1000000;
    }
    #modeSwitch{
        display: none;
    }
</style>
<?php 
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/aap.css"], Yii::app()->getModule('costum')->assetsUrl);
if(empty($inProject))
    $inProject = false;


?>
<div class="portfolio-modal modal fade" id="aapViewFormModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class=" container ">
            <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">

    </div>
</div>

<div class="container padding-bottom-20 padding-left-0 padding-right-0 ">
    <div class="col-md-12 no-padding">
        <div id='filterContainerproposal' class='searchObjCSS'></div>
        <div class='headerSearchIncommunity no-padding col-xs-12'></div>
        <div class='bodySearchContainer margin-top-30'>
            <div class='no-padding col-xs-12' id='dropdown_search'>
            </div>
        </div>
        <div class='padding-top-20 col-xs-12 text-left footerSearchContainer'></div>
    </div>
</div>

<script>
    $(function(){
        var inProject = <?= json_encode($inProject) ?>;
        let proposalActionObj = {
            data : {
                forms : [],
            },
            getform : function(obj){
                var formsParsed = {};
                ajaxPost(
                    null,
                    baseUrl + "/costum/aap/proposalbycostumslug/action/getparentforms2/slug/"+costum.contextSlug,
                    {
                        searchType: ["forms"],
                        count: true,
                        countType : ["forms"],
                        indexStep: 0  ,
                        notSourceKey: true
                    },
                    function(data){ 
                        obj.data.forms = data;
                    },null,null,{async:false}
                );
                return formsParsed;
            },
            events : function(cobj){
                $('.like-project').on('click', function () {
                    $btn = $(this);
                    $val = $btn.data('value').toString();
                    var tplCtx = {
                        id: $btn.data("ans"),
                        collection: "answers",
                        format: true,
                        path: "publicRating." + userId,
                    }
                    if ($val == "false") {
                        $btn.data('value', "true");
                        $btn.find('i').removeClass("fa-heart-o").addClass("fa-heart");
                        var count = $btn.find('.counter').text();
                        $btn.find('.counter').text(parseInt(count) + 1);
                        tplCtx.value = $btn.data('value').toString();
                        ajaxPost(null, baseUrl + '/costum/aap/publicrate',
                            tplCtx,
                            function (res) {
                                // if(res.result)
                                //     toastr.success(res.msg)
                                // else
                                //     toastr.error(res.msg)
                            }, null);
                    } else if ($val == "true") {
                        $btn.data('value', "false");
                        $btn.find('i').removeClass("fa-heart").addClass("fa-heart-o");
                        var count = $btn.find('.counter').text();
                        $btn.find('.counter').text(parseInt(count) - 1);
                        tplCtx.value = $btn.data('value').toString();
                        ajaxPost(null, baseUrl + '/costum/aap/publicrate',
                            tplCtx,
                            function (res) {
                                //    if(res.result)
                                //         toastr.success(res.msg)
                                //     else
                                //         toastr.error(res.msg)
                            }, null);
                    }
                })
                $('.aapgetaapview3').off().on('click', function (e) {
                    e.stopPropagation();
                    coInterface.showLoader(".aapinputcontent");
                    var ansurl = $(this).data('url');
                    //alert(baseUrl+'/survey/form/getaapview/urll'+ansurl);
                    if (typeof formStandalone != "undefined" && formStandalone) {
                        ansurl += "/slugConfig/" + contextDataAap.slug
                    }
                    smallMenu.openAjaxHTML(baseUrl + '/survey/form/getaapview/urll/' + ansurl);
                    $(".aapstatus-container").parent().parent().remove();
                    setTimeout(() => {
                        $(".aap-footer,.aap-options-btn,.rater-sheet,.openAnswersComment,.votant-modal,.view-contributors,.rebtnFinancer").remove();
                        $(".aapstatus-container").parent().parent().remove();
                    }, 1000);
                });
            }
        }
        proposalActionObj.getform(proposalActionObj);

        directory.proposalPanelHtml = function(params){
            params.contextId = Object.keys(params.context)[0];
            params.tags = exists(params?.answers?.aapStep1?.tags) ? params.answers.aapStep1.tags : [];
            params.tagsView = "";
            
            $.each(params.tags,function(k,v){
                params.tagsView+= `
                <a href="javascript:;" class="btn-tag-panel template-tag-panel" data-tag="${v}">
                    <span class="badge  btn-tag tag" data-tag-value="cte" data-tag-label="cte">#${v}</span>
                </a>
                `;
            })

            var str = `
                <div class="team">
                    <div class="card-team">
                        <div class="img-box">
                            <img class="img-responsive-action" alt="Responsive Team Profiles" src="${exists(params.answers?.aapStep1?.images) && exists(params.answers?.aapStep1?.images[0]) ? params.answers?.aapStep1?.images[0] : defaultImage}" />
                            <ul class="text-center">
                                <a href="#">
                                    <li><i class="fa fa-facebook"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-twitter"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-linkedin"></i></li>
                                </a>
                            </ul>
                        </div>
                        <h1>
                            <a href="javascript:;" class="aapgetaapview3" data-url="slug.${costum.slug}.formid.${params.form}.aappage.sheet.answer.${params._id.$id}">
                                ${params.answers.aapStep1.titre}
                            </a>
                        </h1>
                        <h2>
                            <a href="${location.origin}${location.pathname}/#oceco.slug.${params.context[params.contextId]?.slug}.formid.${params.form}.aappage.list" target="_blank">
                                ${exists(params.context[params.contextId]?.name) ? params.context[params.contextId]?.name : ""}
                            </a>
                        </h2>
                        <ul class="tag-list margin-top-5">
                            ${params.tagsView}
                        </ul>
                        <hr />
                        <p class="text-left desc">${exists(params.answers.aapStep1?.description) ? params.answers.aapStep1?.description : ""}</p>
                        <div class="btn-group btn-group-xs pull-right like-project-container">
                            <a href="javascript:;" class="like-project letter-red tooltips margin-right-10" data-value="${(exists(params?.vote) && exists(params?.vote[userId]) && params?.vote[userId]) ? "true" : "false"}" data-ans="${params._id.$id}" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J&#039;adore">
                                <i class="fa ${(exists(params?.vote) && exists(params?.vote[userId]) && params?.vote[userId]) ? 'fa-heart' : 'fa-heart-o'}"></i>
                                <small class="counter" style="font-size: 12px">${notEmpty(params?.vote) ? Object.keys(params?.vote).length : 0}</small>
                            </a>
                            <a href="javascript:;" class="pull-right votant-modal-heart"  data-answerid="${params._id.$id}"></a>
                        </div>
                    </div>
                </div>
            `;
            return str;
        }
        mylog.log(proposalActionObj?.data?.forms?.params?.tags?.list,"lilili")
        var paramsFiltersAction= {
            urlData : baseUrl + "/costum/aap/proposalbycostumslug/action/globalautocompleteaction/slug/"+costum.slug,
            container : "#filterContainerproposal",
            interface : {
                events : {
                    //page : true,
                    scroll : true,
                    //scrollOne : true
                }
                },
            header : {
                dom : ".headerSearchIncommunity",
                options : {
                    left : {
                        classes : 'col-xs-8 elipsis no-padding',
                        group:{
                            count : true,
                            types : true
                        }
                    }
		        },
            },
            defaults : {
                notSourceKey : true,
                textPath : "answers.aapStep1.titre",
                types : ["answers"],
                indexStep: 10,
                forced: {
                    filters: {
                    }
                },
                fields:[],
                sortBy:{updated : -1},
                tagsPath:"answers.aapStep1.tags",
                filters : {
                    'project.id' : {'$exists': (inProject ? "true" : "false")}
                }
            },
            results : {
                dom:".bodySearchContainer",
                smartGrid : true,
                renderView :"directory.proposalPanelHtml",
                map : {
            	    active : false
                },
                smartGrid: true,
                events : function(fObj){
                    $(fObj.results.dom+" .processingLoader").remove();
                    proposalActionObj.events(proposalActionObj);
                    coInterface.bindLBHLinks();
                    directory.bindBtnElement();
                    coInterface.bindButtonOpenForm();
                },
            },
            filters : {
                text : true,
                theme : {
                    view : "dropdownList",
                    type : "filters",
                    field : "answers.aapStep1.tags",
                    /*remove0: true,
                    countResults: true,
                    activateCounter:true,*/
                    countFieldPath: "answers.aapStep1.tags",
                    remove0: false,
                    name : "<?php echo Yii::t("common", "search by theme")?>",
                    event : "filters",
                    list : exists(proposalActionObj?.data?.forms?.params?.tags?.list) ? proposalActionObj.data.forms.params.tags.list : []
                }
            }
        };
        var filterActions    = searchObj.init(paramsFiltersAction);
        filterActions   .search.init(filterActions );
    })
</script>