

<script>
    
        var paramsFilterBadge = {
            urlData: baseUrl + "/co2/search/globalautocomplete",
            container: "#filterContainer",
            loadEvent:{
                default:"scroll"
            },
            header: {
                dom: ".headerSearchIncommunity",
                options: {
                    left: {
                        classes: 'col-xs-8 elipsis no-padding',
                        group: {
                            count: true,
                            types: true
                        }
                    }
                }
            },
            defaults: {
                indexStep: 10,
                types: ["badges"],
                notSourceKey:true,
                tags :["FabLab","Fabrication"]
            },
            filters: {
                text: true,
                isParcours : {
                    name : "Parcours ?",
                    view : "dropdownList",
                    type : "filters",
                    event : "filters",
                    action : "filters",
                    keyValue: false,
                    field: "isParcours",
                    list : {
                        "true" : trad.yes,
                        "false" : trad.no
                    }
	 		    },
            }
        };
        filterGroupbadge = searchObj.init(paramsFilterBadge);
       

        coInterface.bindLBHLinks();
  
</script>