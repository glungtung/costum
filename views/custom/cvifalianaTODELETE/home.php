
<style type="text/css">
        .nom{
            color: #106BA4;
        }
        .img{
           height: 65px;
        }
        .titre-img {
            text-transform: inherit!important;
            color: #5b2649;
            font-size: 22px!important;
        }
    </style>
<div onclick="topFunction()" id="myBtn" title="Go to top" class="scroll"><span class="glyphicon glyphicon-chevron-up"> </span></div>

	<div id="home" class="banner">
		<div  id="index_banner_wrapper"  class="banner-info">
			<div id="index_banner" class="container">
				<div class="col-md-4 header-left" style="max-width: 300px ">
					<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cvifaliana/Picture.JPG">
				</div>
				<div class="col-md-8 header-right">
					<h1>IFALIAN'ARIMANANA Louis Oméga</h1>
					<h6>Web Designer et Dévéloppeur</h6>
					<ul class="address">
						<li>
							<ul class="address-text">
								<li><b>Né le </b></li>
								<li>08-02-1995</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>TEL </b></li>
								<li>+261 34 67 937 56</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>ADRESSE </b></li>
								<li>Ampopoka Golf, Fianarantsoa</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>E-MAIL </b></li>
								<li><a href="mailto:ifaomega@gmail.com"> ifaomega@gmail.com</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//banner-->
	<!--top-nav-->
	<div class="top-nav " id="navbar" style="z-index:1">
		<div class="container">
			<div class="navbar-header logo">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					Menu
				</button>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<div class="menu">
					<ul class="nav navbar">
						<li><a href="#work" class="scroll">Experiences</a></li>
						<li><a href="#education" class="scroll">Formations et diplôme</a></li>
						<li><a href="#skills" class="scroll">Connaissances en informatiques</a></li>
						<li><a href="#linguistiques" class="scroll">Connaissances linguistiques</a></li>
					</ul>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>	

	<div id="work" class="work">
		<div class="container">
			<h3 class="title">Experiences</h3>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Mars 2019 </h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-briefcase"> </span>Mini-mémoire pour le passage en Master 2</h5>
					<p>- Thème : Génération de code QR infalsifiable cas de permis de conduire</p>
					<p>- Outils : Python, Tensorflow.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>Nov 2017 - Mars 2018 </h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5>Stage professionnel au sein de l’ITDC MADA <span class="glyphicon glyphicon-briefcase"> </span></h5>
					<p>- Thème : Conception et réalisation d’une application pour la facturation.</p>
					<p>- Outils : PHP, Framework Codeigniter HMVC, Javascript, MySql</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Aout 2017 </h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-briefcase"> </span> Projet d’études en L3</h5>
					<p>- Thème : Conception et réalisation de gestion de vente de matériels informatique. </p>
					<p>- Thème : Conception et réalisation de gestion de vente de matériels informatique.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>Déc 2016 - Fév 2017 </h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5> Stage professionnel au sein de l’INSTAT <span class="glyphicon glyphicon-briefcase"> </span> </h5>
					<p>- Thème : Conception et réalisation d’un application de gestion de matériels</p>
					<p>- Outils : JAVA, MySql.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>Oct - Nov 2016 </h4>
				</div>
				<div class="col-md-6 work-right"> 
					<h5><span class="glyphicon glyphicon-briefcase"> </span>Projet d’études en L2</h5>
					<p>- Thème 2 : Conception et réalisation d’un application gestion de vente de vaisselle.</p>
					<p>- Outils : PHP, MySql.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="work-info"> 
				<div class="col-md-6 work-right work-right2"> 
					<h4>Novembre 2015 </h4>
				</div>
				<div class="col-md-6 work-left work-left2"> 
					<h5> Stage professionnel au sein de l’INSTAT <span class="glyphicon glyphicon-briefcase"> </span></h5>
					<p>- Voyage d’étude à Antsirabe</p>
					<p>- Conception et réalisation d’un logiciel pour la « Gestion de vente de vente de vaisselle » en Access</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//work-experience-->
	<!---728x90--->

	<!--education-->
	<div id="education" class="education">
		<div class="container">
			<h3 class="title">Formations et diplôme</h3>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>2017 – Maintenant : </h4>
				</div>
				<div class="col-md-6 work-right"> 
					<p>- Master en Informatique à </p> <div class="ecole"></div>
					<p>- Parcours M2I (Modélisation et Ingénierie Informatique). </p>
					 
				</div>
				<div class="clearfix"> </div>
			</div><hr>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>2014 – 2017 :</h4>
				</div>
				<div class="col-md-6 work-right"> 
					<p>- Licence en Informatique à </p><div class="ecole"></div>
					<p>- Parcours DA2I (Développement d’Application Internet Intranet). </p>
				</div>
				<div class="clearfix"> </div>
			</div><hr>
			<div class="work-info"> 
				<div class="col-md-6 work-left"> 
					<h4>2014 : </h4>
				</div>
				<div class="col-md-6 work-right"> 
					<p>Baccalauréat Technique à ECAT Taratra</p>
					<p>Gestion</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--//education-->
	<!--skills-->
	<div id="skills" class="skills">
		<div class="container">
			<h3 class="title">Connaissances en informatiques</h3>
			<div class="skills-info">
				<div class="col-md-6 bar-grids">
					<div class="col-md-8 about-left">
						<b><p>Outils bureautiques</p></b>
						<p>Microsoft World, Microsoft Excel, Power Point, Access.</p><hr>
						<b><p>SIG/Télédétection</p></b>
						<p>Quantum GIS, MapServer, GeoServer.</p><hr>
						<b><p>Langages de programmation</p></b>
						<p>PHP, JAVA, C, C++, Python, Visual Basic, Prolog.</p><hr>
						<b><p>Technologie web</p></b>
						<p>HTML5, JSP, JavaScript, CSS3, Ajax, JQuery.</p>
					</div>
				</div>
				<div class="col-md-6 bar-grids">
					
						<b><p>Frameworks:</p></b>
						<p>Codeigniter, React JS, Bootstrap, Bulma, Cordova.</p><hr>
						<b><p>SGBD</p></b>
						<p>Oracle, Ms Access, MySQL, PostgreSQL, PostGIS.</p><hr>
						<b><p>Méthode et langage de conception</p></b>
						<p>MERISE II, GAMA et UML.</p><hr>
						<b><p>Outils</p></b>
						<p>NetBeans, Eclipse, NodeJS, CodeBlocks, Sublime Text, QGIS, Monteverdi.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--Connaissances linguistiques-->
	<div id="linguistiques" class="education">
		<div class="container">
			<h3 class="title">Connaissances linguistiques</h3>
			<div class="skills-info">
				<div class="col-md-6 bar-grids">
					<h6>Francais  <span> 60% </span></h6>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" style="width: 60%">
						</div>
					</div>
				</div>
				<div class="col-md-6 bar-grids">
					<h6>Anglais <span> 40% </span></h6>
					<div class="progress">
						<div class="progress-bar progress-bar-striped active" style="width: 40%">
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
  scrollFunction();
}

var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


  function sortProperties(obj)
  {
  // convert object into array
    var sortable=[];
    for(var key in obj)
      if(obj.hasOwnProperty(key))
        sortable.push([key, obj[key].name]); // each item is an array in format [key, value]

    // sort items by value
    sortable.sort(function(a, b)
    {
      var x=a[1].toLowerCase(),
        y=b[1].toLowerCase();
      return x<y ? -1 : x>y ? 1 : 0;
    });
    newObj={};
    $.each(sortable,function(a,b){
      newObj[b[0]]=obj[b[0]];
    });
    return newObj; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
  }
  jQuery(document).ready(function() {
      setTitle("Ifaliana");
      getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/citoyens/id/"+userId+"/dataName/organizations",

          function (data) {
          
              var str="";
              data=sortProperties(data);
              $.each(data, function(i,v){
                  if(v.slug == 'emit' ){
                    str+="<a href='#page.type.organizations.id."+i+"' class='lbh-preview-element'><p class='nom'>"+v.name+"</p></a>";
                  }
              });
              $(".ecole").html(str);
              coInterface.bindLBHLinks();
          },
      "html");
  });

</script>