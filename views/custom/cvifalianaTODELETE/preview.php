<style>
    }

    .description-preview{
        text-align: justify;
    }
    .description-preview.activeMarkdown p, .description-preview.activeMarkdown li{
        font-size: 14px !important;
    }

    blockquote {
        padding: 25px 20px;
        margin: 0 0 20px;
        border-left: 1px solid #bdb8b8;
    }

</style>
<div class="margin-top-25 margin-bottom-50 col-xs-12">
	<div class="col-xs-12 no-padding">
		<button class="btn btn-default pull-right btn-close-preview" style="margin-top:-15px;">
			<i class="fa fa-times"></i>
		</button>

        <div class="row">
                <div class="col-xs-4" style="margin-block-start: 3em; text-align: center!important;">
                        <img src="<?php echo $element["profilImageUrl"] ?> " class="img-oval">
                </div>
                <div class="col-xs-8" >
                    <h3 class="titre-img"><?php echo $element["name"] ?></h3>
                    <blockquote style="margin-block-start:3em;font-weight: normal;">
                        <span class="description-preview activeMarkdown"><?php echo  $element["shortDescription"]  ?></span>
                    </blockquote>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	jQuery(document).ready(function() {

        if($(".description-preview").hasClass("activeMarkdown")){
            descHtml = dataHelper.markdownToHtml($(".description-preview").html());
            $(".description-preview").html(descHtml);
        }
	  	$(".btn-close-preview").click(function(){
            $("#modal-preview-coop").hide(300);
            $("#modal-preview-coop").html("");
		});
	});
</script>




