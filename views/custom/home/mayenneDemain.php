<div class="">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #1b7baf;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #1b7baf;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #450e33;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #fbae55;
  }
  .text-explain{
    color: #555;
    font-size: 18px;
  }
  .blue-bg {
  background-color: white;
  color: #5b2549;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #fea621;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #ea4335;
    border: inset 3px #ea4335;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #450e33;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #0091c6;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline div {
  padding: 0;
  height: 40px;
}
.timeline hr {
  border-top: 3px solid #0091c6;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline .corner {
  border: 3px solid #0091c6;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>Mayenne Demain<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/banner.jpg'> 
  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#450e33; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                <span class="main-title">Construisez la commune de demain</span><br>
                <small>
                  <b>Dès aujourd’hui, mobilisez-vous à Mayenne pour impulser le changement en partageant vos idées, pour mener Mayenne vers une transition plus sociale, plus démocratique, plus
écologique.<br>
                </small>
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-xs-12">
                <a href="#porterPacte" class="btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("home","JE CONTRIBUE") ?>
                            </h4>
                        </div>
                    </div>
                </a>
              </div>
              <h3 class="col-xs-12 text-center">
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-md-10 col-md-offset-1 col-xs-12">
                <span class="text-explain">
                En mars 2020, Jean-Claude Lavandier et son équipe se présenteront aux élections municipales. Aidez-nous à
                identifier et mettre en œuvre, une fois élu⋅e⋅s, des mesures concrètes pour encourager la transition
                écologique, sociale et démocratique de Mayenne.<br/><span class="bullet-point"></span><br/>
                Vous, citoyennes et citoyens, êtes les mieux placé⋅e⋅s pour définir ces priorités et construire la ville de demain.
                </span>
              </div>
            </div>
          </div>

        </div>

      </div>
      <div class="timeline-ctc col-xs-12 no-padding" style="font-weight: 300;height: 100%;text-align: inherit;">
             
              <div class="container-fluid blue-bg no-padding">
                <div class="container col-xs-12 no-margin no-padding">
                  <h2 class="pb-3 pt-2">
                    <i class="fa fa-calendar"></i> L'agenda
                  </h2>
                  <!--first section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center bottom">
                      <div class="circle">1</div>
                    </div>
                    <div class="col-6">
                      <h5>Octobre à Décembre 2019</h5>
                      <p>Consultation des citoyens et des citoyennes</p>
                    </div>
                  </div>
                  <!--path between 1-2-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--second section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Du 25 janvier au 28 février 2020</h5>
                      <p>Construction du programme</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">2</div>
                    </div>
                  </div>
                  <!--path between 2-3-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <!--third section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle active">3</div>
                    </div>
                    <div class="col-6">
                      <h5>Mars 2019</h5>
                      <p>Elections municipales</p>
                    </div>
                  </div>
                   <!--path between 3-4-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--4th section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>Septembre 2019 à Mars 2020</h5>
                      <p>Dialogue des citoyen⋅ne⋅s avec Jean-Claude Lavandier et son équipe, candidat⋅e⋅s aux élections municipales de 2020</p>
                    </div>
                    <div class="col-2 text-center full">
                      <div class="circle">4</div>
                    </div>
                  </div>
                  <!--path between 4-5-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>
                  <!--5th section-->
                  <div class="row align-items-center how-it-works">
                    <div class="col-2 text-center full">
                      <div class="circle">5</div>
                    </div>
                    <div class="col-6">
                      <h5>Mars 2020</h5>
                      <p>Elections municipales</p>
                    </div>
                  </div>
                   <!--path between 3-4-->
                  <div class="row timeline">
                    <div class="col-2">
                      <div class="corner top-right"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner left-bottom"></div>
                    </div>
                  </div>
                  <!--6th section-->
                  <div class="row align-items-center justify-content-end how-it-works">
                    <div class="col-6 text-right">
                      <h5>A partir d'avril 2020</h5>
                      <p>Suivi de la mise en place des mesures avec les élus</p>
                    </div>
                    <div class="col-2 text-center top">
                      <div class="circle">6</div>
                    </div>
                  </div>
                  <!--path between 6-7-->
                  <!--<div class="row timeline">
                    <div class="col-2">
                      <div class="corner right-bottom"></div>
                    </div>
                    <div class="col-8">
                      <hr/>
                    </div>
                    <div class="col-2">
                      <div class="corner top-left"></div>
                    </div>
                  </div>-->
                  <!--7th section-->
                  <!--<div class="row align-items-center how-it-works">
                    <div class="col-2 text-center top">
                      <div class="circle">7</div>
                    </div>
                    <div class="col-6">
                      <h5>À partir de Mars 2020</h5>
                      <p>Suivi de la mise en œuvre</p>
                    </div>
                  </div>-->
                </div>
                
              </div>

      </div>
      <div class="col-xs-12 no-padding support-section">
        <h2 id="porterPacte"><i class="fa fa-connectdevelop"></i> Je veux apporter une idée</h2>
        <div class="col-xs-12 padding-20 text-center" >
          <b>Bonne nouvelle !</b> Alors voilà des pistes pour procéder : <br/><br/>
          <ul  class="padding-10">
            <li>Ce site proposera très bientôt un espace de discussion.</li>
            <li>Jean-Claude Lavandier et son équipe viendront dans votre quartier pour discuter avec vous.</li>
            <li>D'ici là, vous pouvez toujours :</li>
          </ul>
          <br/>
          <a href="mailto:contact@mayenne-demain.fr" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 col-sm-offset-2  margin-top-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Nous écrire") ?>
                      </h4>
                  </div>
              </div>
          </a>
          <a href="#" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 col-sm-offset-2 col-md-2 col-md-offset-1 margin-top-20"  >
              <div class="text-center">
                  <div class="col-md-12 no-padding text-center">
                      <h4 class="no-margin uppercase">
                        <i class="fa fa-hand-point-right faa-pulse"></i>
                        <?php echo Yii::t("home","Faire un don") ?>
                      </h4>
                  </div>
              </div>
          </a>

          
          <div class="text-center col-xs-12"><br/> <br/><b>...Et en parler tout autour de vous !!</b></div>
        </div>
      </div>
      <div class="col-xs-12 no-padding support-section">
        <h2><i class="fa fa-connectdevelop"></i> A propos des idées</h2>
        <div class="col-xs-12 padding-20 text-explain">
       
        <a href="javascript:;" data-hash="#dda" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin uppercase">
                      <i class="fa fa-hand-point-right faa-pulse"></i>
                      <?php echo Yii::t("home","Consultez les idées déjà partagées") ?>
                    </h4>
                </div>
            </div>
        </a>
        </div>
      </div>
     </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("Mayenne Demain");
  });
</script>

