
<?php
$cssAnsScriptFilesModule = array( 
  //'/leaflet/leaflet.css',
  //'/leaflet/leaflet.js',
  //'/css/map.css',
  //'/markercluster/MarkerCluster.css',
  //'/markercluster/MarkerCluster.Default.css',
  //'/markercluster/leaflet.markercluster.js',
  //'/js/map.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );
?>
<div class="">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #3595a8;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #3595a8;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #3595a8;
  }

  .ourvalues h3{
    font-size: 25px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }

  .participate {
background-color: white;
  }
 
.participate i{
  float: left;
  margin-right: 20px;
  color: #a34a08;

}

.participate-content{
	font-size: 17px;
}



.btn-redirect-home{
  background-color: #a34a08;
  color: white;
}
.btn-redirect-home:hover{
  background-color: white;
  color: #4797cc;
  border: 2px solid #4797cc; 
}
.mapBackground{
    /*background-image: url(/ph/assets/449afa38/images/city/cityDefaultHead_BW.jpg);*/
    background-color: #f6f6f6;
    background-repeat: no-repeat;
    background-position: 0px 0px;
    background-size: 100% auto;
    height: 400px;
}
 

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

    .carousel-media .carousel-inner{
      max-height: 400px !important;
    }
  }


  .section-separtor div{
        height:5px;
        padding:0px;
    }
    element.style {
}

.carousel-indicators li, .carousel-media > ol > li {
    display: inline-block;
    width: 15px !important;
    height: 15px !important;
    margin: 1px !important;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000\9;
    background-color: rgba(0,0,0,0);
    border: 1px solid #fff;
    border-radius: 10px !important;
  }
.carousel-media .carousel-inner{
  max-height: 500px !important;
}
.title-main-home{
      z-index: 100;
    position: absolute;
    bottom: 60px;
    color: white;
    font-weight: bolder;
    font-size: 35px;
    text-shadow: 2px 2px 5px #000000;
    left: 0px;
    right: 0px;
}
.carousel-media {margin-bottom: 0px !important}
.carousel-media > ol{
  bottom: 20px !important;
}
.carousel-media > ol > li > img{
  height: 0px !important;
  width: 0px !important;
  display:none;
}
.textIcoEX{
  color: white;margin-bottom:30px;padding-right:30px;
}
.textIcoEX i {
  font-size: 40px;
}
.textIcoEX span{
  font-size: 25px;
  font-weight: 800;
}
.section-home-content{
  margin-top:0px;
  background-color: #4797cc;
  padding-top: 50px;
  padding-bottom: 50px;
}
.content-all-cities{
  padding: 10px;
}
.citiesFiltering{
  padding: 0px 5px;
    color: #4797cc;
    font-size: 20px;
}
.section-home-content .btn-redirect-home{
  background-color: white;
  color: #a34a08;
  border: 2px solid !important;
}
.section-home-content .btn-redirect-home:hover, .section-home-content .btn-redirect-home:hover i{
  color: #4797cc !important;
}
</style>
<?php 
  if($this->costum["contextType"] && $this->costum["contextId"])
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  $lat=50.4911522;
  $long=5.0964543;
  foreach(@$this->costum["scopeSelector"] as $v){
    if(@$v["active"] && $v["active"]){
      $res=City::getById($v["id"],array("geo"));
      $lat=$res["geo"]["latitude"];
      $lon=$res["geo"]["longitude"];
    }
  }

?>
<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <?php 
    $banner = Yii::app()->getModule("costum")->assetsUrl."/images/meuseCampagnes/Andenne/banner.png";
    $packImage=array("Andenne"=>array(), "Fernelmont"=>array(), "Wasseiges"=>array());
    foreach( $packImage as $city => $arr){
      $files=glob(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR."assets/images/meuseCampagnes/".$city."/caroussel*");
      foreach($files as $filename) {
        array_push($packImage[$city],Yii::app()->getModule("costum")->assetsUrl.str_replace(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR."assets", "", $filename));
      }
      //$packImage[$city]
    }
    /*if(@$this->costum["metaImg"]){
      if(substr_count($this->costum["metaImg"], 'this.') > 0 && isset($el)){
        $field = explode(".", $this->costum["metaImg"]);
        if( isset( $el[ $field[1] ] ) )
          $banner = Yii::app()->getRequest()->getBaseUrl(true).$el[$field[1]] ;
      }
      else if(strrpos($this->costum["metaImg"], "http" ) === false && strrpos($this->costum["metaImg"], "/upload/" ) === false )
        $banner = Yii::app()->getModule("costum")->getAssetsUrl().$this->costum["metaImg"] ;
      else 
        $banner = $this->costum["metaImg"];
    }*/
    ?>
    <div id="carrousel-media"></div>
    <!--<img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo $banner ?>'/>--> 
    <div class="main-title text-center title-main-home">
        Initiatives associatives et citoyennes à <span class="home-scope-name">Andenne</span></span>
    </div>
  
      
      <!-- <div id=carousel class="col-md-12 no-padding carousel carousel-generic slide" data-ride="carousel">
	 -->
	<!-- Wrapper for slides -->
		<!-- <div class="carousel-inner">
			<img class="img-responsive active" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/meuseCampagnes/Andenne/default_directory.png'>
			<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/meuseCampagnes/Andenne/andenne-carte.jpg'>

		</div> -->

	<!-- Indicators -->
	<!-- 	<ol class="carousel-indicators">
			 <li data-target="#carousel ?>" data-slide-to="0" class="active"></li>
			 <li data-target="#carousel ?>" data-slide-to="1"></li>
			
		</ol>

</div> -->
  </div>
  <div class="col-xs-12 section-separtor no-padding">
    <div class="col-xs-4 bg-brown"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-2 bg-blue"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-4 bg-brown"></div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 section-home-content">
				<div class="home-info-villages">
					<div  class="col-md-3 col-sm-6 col-xs-12 text-center textIcoEX">
						<i class="fa fa-2x fa-university" ></i><br/><br/>
						<span class="statTerritorial">1 centre ville <br/>&<br/> 9 villages</span>
					</div>
					<div  class="col-md-3 col-sm-6 col-xs-12 text-center textIcoEX">
						<i class="fa fa-2x fa-users"></i><br/><br/>
						<span class="statsAssoc">Près de<br/>200 associations<br/>en activité</span>
					</div>
				
						<div  class="col-md-3 col-sm-6 col-xs-12 text-center textIcoEX">
							<i class="fa fa-2x fa-lightbulb-o" ></i><br/><br/>
							<span class="statsInitatives">Des 100taines d'initiatives<br/>informelles ou structurées<br/>en cours</span>
						</div>
						<div  class="col-md-3 col-sm-6 col-xs-12 text-center textIcoEX">
							<i class="fa fa-2x fa-calendar" ></i><br/><br/>
							<span class="statsEvents">Plusieurs 100taines<br/>d'activités proposées<br/>chaque année</span>
						</div>
				</div>			
        <a href="javascript:;" data-hash="#search" class="btn btn-redirect-home lbh-menu-app col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 text-center" style="border: solid 1px;font-size: 22px !important;">
            <i class="fa fa-info-circle" style="color:#a34a08"></i> Consulter tous les acteurs et leurs initiatives
        </a>
  </div>
  <div class="col-xs-12 section-separtor no-padding">
    <div class="col-xs-4 bg-brown"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-2 bg-blue"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-4 bg-brown"></div>
  </div>
  <div class="col-xs-12 mapBackground no-padding" id="mapMeuse">
    <!-- <img class="img-responsive col-xs-12 col-sm-8 col-sm-offset-2 no-padding" src=""/> -->
  </div>
  <div class="col-xs-12 content-all-cities text-center"></div>
 <div class="col-xs-12 section-separtor no-padding">
    <div class="col-xs-4 bg-brown"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-2 bg-blue"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-4 bg-brown"></div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px; margin-top:0px;background-color: #4797cc;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 text-justify" style="margin-top: -19px;margin-bottom: -18px;font-size: 14px;z-index: 5;color:#fff">
	        <h3 class="col-xs-12" style="/*font-family:'Pacifico', Helvetica, sans-serif;*/color:#fff;   font-size: 30px">
	             Un outil numérique par et pour tous   
	        </h3>    
            <div class="col-md-4 col-sm-12 col-xs-12 padding-20"">
            	<h4 style="text-align:left;">Rassembler</h4><br/>
            	Sport, musique, théâtre, concert, tourisme, patrimoine, artisanat, cours artistiques, fêtes villageoises, mouvements de jeunesse, fablab, comités de quartier, espace public numérique, service d’échange local, transition, jardins partagés,  …<br/>
            	Il y en a pour tous les goûts, tous les âges, toutes les bonnes volontés, toute l’année.
            </div> 
            <div class="col-md-4 col-sm-12 col-xs-12 padding-20"">
            	<h4 style="text-align:left;">Interagir, mutualiser, coopérer</h4>
            	L’ambition de cette plateforme est de rassembler, en un seul lieu numérique, toutes ces initiatives associatives & citoyennes de la commune, de faciliter leurs interactions et de susciter le contact entre tous les acteurs du territoire : les citoyen.ne.s, les associations, les services publics, les entreprises.
            </div>  
            <div class="col-md-4 col-sm-12 col-xs-12 padding-20"">
            	<h4 style="text-align:left;">Repérer et agir localement</h4><br/>
            	Son originalité tient dans une présentation cartographique de ces initiatives. Grâce à leur géolocalisation et à un moteur de recherche territorial, il est facile pour l’internaute de repérer les initiatives actives autour de chez lui, dans un rayon donné, de prendre connaissance de l’agenda des activités, des offres et demandes en services ou matériels, de dialoguer, de partager de l’information, de créer son propre groupe ou projet, …
            </div>    
		     
    	</div>
    </div>
    <div class="col-xs-12 section-separtor no-padding">
    <div class="col-xs-4 bg-brown"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-2 bg-blue"></div>
    <div class="col-xs-1 bg-white"></div>
    <div class="col-xs-4 bg-brown"></div>
  </div>
    <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate" style="padding-left:100px; margin-top:0px;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 text-justify" style="margin-top: -19px;margin-bottom: -18px;font-size: 14px;z-index: 5;">
	        <h3 class="col-xs-12" style="color: #a34a08;   font-size: 30px">
	             A votre niveau, participez !  
	       	<br/>
		        <small style="color: #333;">
		        	Vous, citoyen.ne.s de <span class="home-scope-name">ANDENNE</span>, êtes au centre de la dynamique proposée par la plateforme qui vous propose de vous « communecter », de vous connecter à votre commune, à votre territoire.<br/><br/>
		        	<b style="color:#4797cc;">Vous pouvez alors être :</b>
		        </small>    
	    	</h3>
            <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
            	<i class="fa fa-3x fa-info-circle"></i>
            	informés en quasi temps réel de la dynamique de votre territoire : activités, événements, débats, actions, projets…
            </div> 
            <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
            	<i class="fa fa-3x fa-exchange"></i>
            	actifs en coopérant à la dynamique locale; partage d’information, dialogue interactif facilité avec les collectivités, regroupement de citoyens par affinités pour réfléchir ou débattre, échanger, agir concrètement   sur le terrain au travers d’événements, d’actions, de projets. 
            		
            </div>  
            <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
            	<i class="fa fa-3x fa-bullhorn"></i>
            	connectés​ avec d’autres utilisateurs du même territoire pour partager des centres d’intérêts communs, avec des associations locales qui proposent des initiatives sociales et solidaires, avec des entreprises et producteurs qui fabriquent localement des produits,... 
            </div>
             <div class="col-md-12 col-sm-12 col-xs-12 padding-20 participate-content">
            	<i class="fa fa-3x fa-newspaper-o"></i>
            	<span class="pull-left"> promoteurs ​de votre activité propre, associative, entrepreneuriale...</span> 
            </div>    
		     
    	</div>
    </div>
   




  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
    <!--<div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="padding-top:0 !important;background-color: #fff;font-size: 14px;z-index: 5;">  
      <div class="col-xs-12 font-montserrat ourvalues text-center" style="">
        <h3 class="col-xs-4"> <a href="<?php echo Yii::app()->createUrl("/costum/co/index/slug/andenneMeuse") ?>">Andenne</a></h3>
        <h3 class="col-xs-4"> <a href="<?php echo Yii::app()->createUrl("/costum/co/index/slug/fernelmontMeuse") ?>">Fernelmont</a></h3>
        <h3 class="col-xs-4"> <a href="<?php echo Yii::app()->createUrl("/costum/co/index/slug/wasseigesMeuse") ?>">Wasseiges</a></h3>
      </div>
    </div>

  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="margin-top:40px;max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style=""> 

      <div class="col-xs-12 no-padding">

        <div class="col-md-12 col-sm-12 col-xs-12 " style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="padding-top:0 !important;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">  
            
            <div class="col-xs-12 font-montserrat ourvalues text-justify" style="">
              <h3 class="col-xs-12" style="margin-top:0;">
                <span style="display:inline-block;width:100%;" class="main-title text-center margin-5">1 <small>centre-ville <br>+ </small><br>9 <small>villages</small></span>
                <span style="display:inline-block;width:100%;" class="main-title text-center margin-5"> <small>Près de</small><br> 200 associations<br> <small>en activité</small></span>
                <span style="display:inline-block;width:100%;" class="main-title text-center margin-5"><smalldes 100taines d'initiativeS <small>informelles ou structurées en cours</small></span>
                <span style="display:inline-block;width:100%;" class="main-title text-center margin-5">plusieurs 100taines d'activités <small>proposées chaque année. </span><br>
                <br>
                <small>
                  <b>L’Agence de l’Aventure et de l’Inattendu oeuvre à la production, au développement et à la préservation de biens communs.
                  Cette plate-forme se propose de référencer les communs et d'informer sur les activités (projets/événements) menées et organisées par l'Agence.<br>
                </small>
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-xs-12">
                <a href="javascript:;" data-hash="#ressources" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("Consult resources","Consulter les ressources") ?>
                            </h4>
                        </div>
                    </div>
                </a>
              </div>         
              <div class="col-xs-12" style="margin-top: 15px;">
                <a href="javascript:;" data-form-type="poi" class="btn-open-form btn-main-menu col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3"  >
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                            <h4 class="no-margin uppercase">
                              <i class="fa fa-hand-point-right faa-pulse"></i>
                              <?php echo Yii::t("Add resources","Ajouter une ressource") ?>
                            </h4>
                        </div>
                    </div>
                </a>
              </div>
              <h3 class="col-xs-12 text-center">
                <hr style="width:40%; margin:20px auto; border: 4px solid #fbae55;">
              </h3>
              <div class="col-md-10 col-md-offset-1 col-xs-12">
                <span class="text-explain">Nous sommes de plus en plus nombreux à vouloir agir face au dérèglement climatique, à la croissance des inégalités, à la crise de la démocratie.<br/><span class="bullet-point"></span><br/>
                Pour relever ces défis majeurs, changer nos pratiques quotidiennes de consommation ne suffit plus.
                Il nous faut également œuvrer à la transformation de nos politiques publiques.<br/><span class="bullet-point"></span><br/>
                Si la transition doit avoir lieu à toutes les échelles, les communes peuvent être le fer de lance de ce
                mouvement.<br/>
                En mars 2020, des candidat.e.s se présenteront aux élections municipales. Nous voulons les aider à
                identifier et mettre en œuvre, une fois élu.e.s, des mesures concrètes pour encourager la transition
                écologique, sociale et démocratique de leur commune.<br/><span class="bullet-point"></span><br/>
                Nous, citoyennes et citoyens, sommes les mieux placés pour définir ces priorités et construire les
                communes de demain. Ensemble, nous sommes la transition.
                </span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>-->
</div>

<script type="text/javascript">
var stats_village={
  "Fernelmont":{
    "statTerritorial" :"10<br/> villages",
    "statsAssoc" : "Une multitude<br/> d’organisations",
    "statsInitatives": "des 100taines d’initiatives<br/> informelles ou structurées<br/> en cours",
    "statsEvents":"Plusieurs 100taines<br/>d'activités proposées<br/>chaque année",
    "cities":["bierwart", "cortil-wodon","forville","franc-waret","hemptinne","hingeon","marchovelette","noville-les-bois","pontillas","tillier"]
  },
  "Andenne":{
    "statTerritorial" :"1 centre ville <br/>&<br/> 9 villages",
    "statsAssoc" : "Une multitude<br/> d’organisations",
    "statsInitatives": "Des 100taines d'initiatives<br/>informelles ou structurées<br/>en cours",
    "statsEvents":"Plusieurs 100taines<br/>d'activités proposées<br/>chaque année",
    "cities":["andenne", "bonneville","coutisse","landenne","maizeret","namêche","sclayn","seilles","thon-samson","vezin"]
  },
  "Wasseiges":{
    "statTerritorial" :"4<br/> villages",
    "statsAssoc" : "Une multitude<br/> d’organisations",
    "statsInitatives": "des 100taines d’initiatives<br/> informelles ou structurées<br/> en cours",
    "statsEvents":"Plusieurs 100taines<br/>d'activités proposées<br/>chaque année",
    "cities":["acosse", "ambresin","meeffe","wasseiges"]
  }
};
var imageUrlCities= <?php echo json_encode($packImage); ?>;
var baseImgReplace=<?php echo json_encode(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.".."); ?>;
var mapMeuseHome = {};
var dataSearchMeuse = {} ;
var initLat=<?php echo json_encode($lat); ?>;
var initLong=<?php echo json_encode($long); ?>;
	jQuery(document).ready(function() {
      setTitle("Meuse Campagnes");
      if(typeof costum.scopeActivated != "undefined"){
        nameCity=costum.scopeActivated.name;
        $(".home-scope-name").text(nameCity);
        $(".mapBackground img").attr('src', assetPath+"/images/"+costum.slug+"/"+nameCity+"/map.jpg"); 
        $.each(["statTerritorial", "statsAssoc", "statsInitatives", "statsEvents"], function(e,v){
            renderBox=stats_village[nameCity][v];
            if(v=="statsAssoc")
                renderBox="<a href='#search?types=organizations' class='lbh text-white'>"+stats_village[nameCity][v]+"</a>";
            else if(v=="statsInitatives")
                renderBox="<a href='#search?types=projects' class='lbh text-white'>"+stats_village[nameCity][v]+"</a>";
            else if(v=="statsEvents")
                renderBox="<a href='#agenda' class='lbh text-white'>"+stats_village[nameCity][v]+"</a>";
              
            $(".home-info-villages ."+v).html(renderBox);
        });
        listCities="";
        $.each(stats_village[nameCity]["cities"], function(e, v){
          listCities+="<a href='#search?tags="+v+"' class='lbh citiesFiltering'>"+v+"</a>";
        })
        $(".content-all-cities").html(listCities);
        //if($("#poi #container-element-accordeon").length > 0){
        params={
          "images": [],
          "medias": [],
          "simplePuce":true
        };
        inc=1;
       $.each(imageUrlCities[nameCity], function(e, v){
          params.images.push({"imagePath": v.replace(baseImgReplace, ""), "name":"images "+nameCity+" "+inc, "imageThumbPath":""});
          inc++;
        });
        ajaxPost("#carrousel-media", baseUrl+'/'+moduleId+'/pod/slidermedia', params, function(){},"html");
      }


          dataSearchMeuse=searchInterface.constructObjectAndUrl();
          dataSearchMeuse.searchType = ["projects", "organizations"];
          if(typeof dataSearchMeuse.locality != "undefined"){
            ajaxPost(
              null,
              baseUrl+"/" + moduleId + "/search/globalautocomplete",
              dataSearchMeuse,
              function(data){ 
                var paramsMapMeuse = {
                  container : "mapMeuse",
                  activePopUp : true,
                  tile : "mapbox2"
                };
                mapMeuseHome = mapObj.init(paramsMapMeuse);
                allMaps.maps[paramsMapMeuse.container]=mapMeuseHome;
         
                if(!data){ 
                  toastr.error(data.content); 
                } 
                else{ 
                  mapMeuseHome.addElts(data.results);
                  setTimeout(function(){
                    mapMeuseHome.map.panTo([initLat,initLong]);
                    mapMeuseHome.map.setZoom(10);
                  },1000);
                }
              },
              function(data){
                mylog.log(">>> error autocomplete search"); 
                mylog.dir(data);   
                var paramsMapMeuse = {
                  container : "mapMeuse",
                  activePopUp : true,
                  tile : "mapbox2"
                };
                mapMeuseHome = mapObj.init(paramsMapMeuse);
                allMaps.maps[paramsMapMeuse.container]=mapMeuseHome;
         
                 $("#dropdown_search").html(data.responseText);  
                 //signal que le chargement est terminé
                loadingData = false;    
              }
            );
        }
  });
</script>


