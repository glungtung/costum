<?php
$cs = Yii::app()->getClientScript();
$cssAnsScriptFilesModule = array(
		'/plugins/jsonview/jquery.jsonview.js',
		'/plugins/jsonview/jquery.jsonview.css',
		'/plugins/JSzip/jszip.min.js',
		'/plugins/FileSaver.js/FileSaver.min.js',
		//'/assets/js/sig/geoloc.js',
		/*'/assets/js/dataHelpers.js',
		'/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js'*/
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js',
		//'/plugins/PapaParse/papaparse.min.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule,Yii::app()->request->baseUrl);

$cssAnsScriptFilesModule = array(
	'/js/import.js',
	//'/js/PapaParse/papaparse.min.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl() );

$cssAnsScriptFilesModule = array(
	'/assets/vendor/papaparse/papaparse.min.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);

$userId = Yii::app()->session["userId"] ;

// $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
// //header + menu
// $this->renderPartial($layoutPath.'header', 
// 		    array(  "layoutPath"=>$layoutPath , 
// 						    "page" => "admin") ); 
?>

<style>
	.bg-azure-light-1{
		background-color: rgba(43, 176, 198, 0.3) !important;
	}
	.bg-azure-light-2{
		background-color: rgba(43, 176, 198, 0.7) !important;
	}
	.bg-azure-light-3{
		background-color: rgba(42, 135, 155, 0.8) !important;
	}

	.menu-step-tsr div{
		margin-left: 20px;
	    font-size: 18px;
	    width: 15%;
	    text-align: center;
	    display: inline-block;
	    margin-top:15px;
	    margin-bottom:5px;
	}
	.menu-step-tsr div.homestead{
		font-size:12px;
	}
	.menu-step-tsr div.selected {
	    border-bottom: 7px solid white;
	}

	.block-step-tsr div{
		font-size: 18px;
	    text-align: center;
	    display: inline-block;
	    margin-top:15px;
	    margin-bottom:15px;
	}

	.mapping-step-tsr{
	    display: inline-block;
	    margin-top:15px;
	    margin-bottom:15px;
	}

	.nbFile{
	    font-size: 18px;
	}

	.divJsonClass{
		height: 300px;
	}
	.modal-title-delete{
		color : red;
	}
</style>

<div id="adminContainer" class="no-padding col-xs-12"></div>
<div class="col-xs-12 no-padding bg-white">
	<div class="panel panel-white col-lg-offset-1 col-lg-10 col-xs-12 no-padding">


		<!-- MAPPING STEP2 -->



<!-- Modal UPDATE MAPPING -->
<div id="modal-update-element" class="modal fade" role="dialog" style="z-index: 100000;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-dark"><?php echo Yii::t("import", "update my mapping"); ?></h4>
      </div>
      <div class="modal-body text-dark">
        <p>
		<h2></h2><!--Résous un problème de maj-->
			<i><?php echo Yii::t("import", "Do you want to change name of your mapping ?"); ?></i>
        </p>
			<strong><?php echo Yii::t("import", "Name your mapping : "); ?> </strong><div id="divSaisirNameUpdate"></div>
      </div>
      <div class="modal-footer">
				<!-- Utilisation du bouton confirmDeleteElement -->
       <a href="javascript:;" id="btnconfirmUpdateMapping" type="button" class="btn btn-success margin-top-15"><?php echo Yii::t("import", "Update my mapping"); ?></a>
        <button type="button" class="btn btn-danger margin-top-15" data-dismiss="modal"><?php echo Yii::t('common','No');?></button>
      </div>
    </div>

  </div>
</div>

<!-- Modal SUPPRIME MAPPING -->
<div id="modal-delete-element" class="modal fade" role="dialog" style="z-index: 100000;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title-delete text-dark" ><?php echo Yii::t("import", "delete my mapping"); ?></h4>
      </div>
      <div class="modal-body text-dark">
        <p>
		<center><h4 class="modal-title-delete"><?php echo Yii::t("import","Are you sure of delete your mapping ?"); ?></h4> </center>
			<i><?php echo Yii::t("import","You will not be able to use this mapping"); ?></i>
        </p>
      </div>
      <div class="modal-footer">
				<!-- Utilisation du bouton confirmDeleteElement -->
       <a href="javascript:;" id="btnconfirmDeleteMapping" type="button" class="btn btn-warning"><?php echo Yii::t("import","Yes, I confirm the delete my mapping"); ?></a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('common','No');?></button>
      </div>
    </div>

  </div>
</div>

<!-- VIEW MAPPING -->

		<div class="col-md-12 mapping-step-tsr section-tsr" id="menu-step-mapping">
			<input type="hidden" id="nbLigneMapping" value="0"/>
			<div class="col-md-12 nbFile text-dark" >
				<?php echo Yii::t("import","There is "); ?><span id="nbFileMapping" class="text-red"> <span> 
			</div>
			<div id="divInputHidden"></div>
			<table id="tabcreatemapping" class="table table-striped table-bordered table-hover">
	    		<thead>
		    		<tr>
		    			<th class="col-sm-5"><?php echo Yii::t("common", "Source"); ?></th>
		    			<th class="col-sm-5"><?php echo Yii::t("common", "Communecter"); ?> <a href="https://hackmd.co.tools/KwUwTAbADMDMDGBaA7BCBDRAWAZuqi6EYAnIgCblgBGAjBABzIMli1A"  target="_blank" title="<?php echo Yii::t("import", "Data sheet referenced"); ?>" class="homestead text-red"><i class="fa fa-info-circle"></i></a></th>
		    			<th class="col-sm-2"><?php echo Yii::t("common", "Add")." / ".Yii::t("common", "Remove"); ?></th>
		    		</tr>
	    		</thead>
		    	<tbody class="directoryLines" id="bodyCreateMapping">
			    	<tr id="LineAddMapping">
		    			<td>
		    				<input type="hidden" name="hiddenSwitch" id="hiddenSwitch" value="noHidden">
		    			<!--<input type="checkbox" id="checkSwitch" onclick="isCheckSwitch()" title="Passé à une saisir de type select/manuelle" checked></input>-->
		    				<input type="text" id="selectSourceTxt" class="col-sm-12" placeholder="<?php echo Yii::t("import","Grap manually my mapping"); ?>" maxlength="40" title="Maximum 40 caractères">
							 <select id="selectSource" class="col-sm-12">
							</select>
					
		    			</td>
		    			<td>
		    				<select id="selectAttributesElt" class="col-sm-12"></select>
		    			</td>
		    			<td>
		    				<input type="submit" id="addMapping" class="btn btn-primary col-sm-12" value="<?php echo Yii::t("import","Add"); ?>"/>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="col-sm-12 col-xs-12">
				<div class="col-sm-6 col-xs-12">
				<i><?php echo Yii::t("import","Fields mandatory"); ?> (*)</i><br />
					<label for="inputKey"><?php echo Yii::t("import","Key : "); ?></label>
					<input class="" placeholder="<?php echo Yii::t("import","Key assigned to all data import"); ?>" id="inputKey" name="inputKey" value="">
				</div>
				<!--<div class="col-sm-6 col-xs-12" id="divCheckboxWarnings">
					<label>
						Warnings : <input type="checkbox" value="" id="checkboxWarnings" name="checkboxWarnings">
					</label>
				</div>-->
			</div>
			<div class="col-sm-12 col-xs-12">
				<div class="col-sm-6 col-xs-12">
					<label>

					Test : <!--<input class="hide" id="isTest" name="isTest" ></input>
					<input id="checkboxTest" name="checkboxTest" type="checkbox" data-on-text="<?php echo Yii::t("common","Yes") ?>" data-off-text="<?php echo Yii::t("common","No") ?>" name="my-checkbox"  onclick="isCheckTest()"  checked></input>-->
							<input type="hidden" id="isTest" name="isTest"/>
			<input id="checkboxTest" name="checkboxTest" type="checkbox" data-on-text="<?php echo Yii::t("common","Yes") ?>" data-off-text="<?php echo Yii::t("common","No") ?>" checked/></input></label>
				</div>
				<div class="col-sm-6 col-xs-12" id="divNbTest">
					<div id="divNbTestAff"><label for="inputNbTest"><?php echo Yii::t("import","Number of entites to test (max 900) : "); ?> </label>
					<input class="" placeholder="" id="inputNbTest" name="inputNbTest" value="5"></div>
					<center>
					<div id="divAjout">
						<a id="btnAjoutMapping" class="btn btn-primary col-sm-12" data-toggle="modal" data-target="#modal-ajout-element"><?php echo Yii::t("import", "Add my mapping") ?></a>
					</div>
					<div id="divUpdate">
						<a id="btnUpdateMapping" class="btn btn-warning" data-toggle="modal" data-target="#modal-update-element"><strong><?php echo Yii::t("import", "Update my mapping"); ?></strong></a>
						<a id="btnDeleteMapping" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-element"><strong><?php echo Yii::t("import", "Delete my mapping"); ?></strong></a>
					</div>
					</center>
				</div>
			</div>
			<div class="col-sm-2 col-xs-12"  id="divInvite">
				<div class="col-sm-12 col-xs-12" id="divAuthor">
					<label for="nameInvitor"><?php echo Yii::t("import", "Author Invite: "); ?></label>
					<input class="" placeholder="" id="nameInvitor" name="nameInvitor" value="">
				</div>
				<div class="col-sm-12 col-xs-12" id="divMessage">
					<textarea id="msgInvite" class="" rows="3"><?php echo Yii::t("import", "Message Invite"); ?></textarea>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12">
				<a href="javascript:;" id="btnPreviousStep" class="btn btn-danger margin-top-15"><?php echo Yii::t("common", "Previous step"); ?></a>
				<a href="javascript:;" id="btnNextStep2" class="btn btn-success margin-top-15"><?php echo Yii::t("common", "Next step"); ?></a>
			</div>
			<div class="testAffichageXML"></div>
		</div>

		<!-- VISUALISATION STEP3 -->
		<div class="col-md-12 mapping-step-tsr section-tsr" id="menu-step-visualisation">
			<div class="panel-scroll row-fluid height-300">
				<label class="nbFile text-dark"><?php echo Yii::t("import", "List of elements :"); ?></label>
				<table id="representation" class="table table-striped table-hover"></table>
			</div>
			<br/>
			<div class="panel-scroll row-fluid height-300">
				<label class="nbFile text-dark"><?php echo Yii::t("import","List of cities has add :"); ?></label>
				<table id="saveCitiesTab" class="table table-striped table-hover"></table>
				<input type="hidden" id="jsonCities" value="">
			</div>
			<br/>	
			<div class="col-xs-12 col-sm-6">
				<label class="nbFile text-dark">
					<?php echo Yii::t("import","Imported data : "); ?><span id="nbFileImport" class="text-red"> <span> 
				</label>
				<div class="panel panel-default">
					<div class="panel-body">
							<input type="hidden" id="jsonImport" value="">
						    <div class="col-sm-12" style="max-height : 300px ;overflow-y: auto" id="divJsonImportView"></div>
					</div>
				</div>
				<div class="col-sm-12 center">
			    	<!-- <a href="javascript:;" class="btn btn-primary col-sm-2 col-md-offset-2" type="submit" id="btnImport">Save</a> -->
			    </div>

			</div>
			<div class="col-xs-12 col-xs-12 col-sm-6">
				<label class="nbFile text-dark">
					<?php echo Yii::t("import", "Data rejected : "); ?><span id="nbFileError" class="text-red"> <span> 
				</label>
				<div class="panel panel-default">
					<div class="panel-body">
						<input type="hidden" id="jsonError" value="">
						   <div class="col-sm-12" id="divJsonErrorView" style="max-height : 300px ;overflow-y: auto"></div>
						
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 center">
			    <!--	<a href="javascript:;" class="btn btn-primary col-sm-2" type="submit" id="btnError">Save</a> -->
			    </div>
			</div>
			<div class="col-xs-12 col-sm-12 margin-top-15">
				<button class="btn btn-danger col-sm-2 col-md-offset-4 " onclick="returnStep2()"><?php echo Yii::t("import", "Return"); ?> 
					<i class="fa fa-reply"></i>

				</button>
				<a href="javascript:;" class="btn btn-success col-sm-3 col-md-offset-2 lbh" onclick="location.hash='#admin.view.adddata';loadAdddata();" type="submit" id="btnBDD"><?php echo Yii::t("import", "Page add of datas"); ?></a>
				<a href="javascript:;" class="btn btn-primary col-sm-3 col-md-offset-2" type="submit" id="btnImport"><?php echo Yii::t("import","Save"); ?></a>
				<a href="javascript:;" class="btn btn-primary col-sm-3 col-md-offset-2" type="submit" id="btnError"><?php echo Yii::t("import","Save"); ?></a>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
var file = [] ;
var csvFile = "" ;
var extensions = ["csv", "json", "js", "geojson","xml"];
var nameFile = "";
var typeFile = "";
var typeElement = "";
var nbFinal = 0 ;
var mappingPrevious = $("#chooseMapping").html();
var ifMappingDelete = false;
var listSource = [];
var listArbre = [];
var listeObligatoire = {
	name : "name",
	type : "type",
	postalCode : "postalCode",
	addressLocality : "addressLocality",
	streetAddress : "streetAddress",
	addressCountry : "addressCountry",
	username : "username"
}
var importMeuse = {};



jQuery(document).ready(function() {

	setTitle("CreateFile","circle");
	$("#divFile").hide();
	$("#divCsv").hide();
	$("#divUrl").hide();
	$("#divPathElement").hide();
	$("#menu-step-mapping").hide();
	$("#menu-step-visualisation").hide();
	$("#btnBDD").hide();
	$("#btnImport").hide();
	$("#btnError").hide();
	$("#selectSourceTxt").hide();
	bindCreateFile();
	bindUpdate();

	var paramsImport= {
		urlStep3 : baseUrl+'/costum/meuseCampagnes/saveorga/',
		step1 : {
			typeElt : "organizations",
			typeSource : "file",
			hide : {
				typeElt : true,
				typeSource : true,
				chooseMapping : true
			}
		},
		step3 : {
			nextUrl : baseUrl+'/costum/meuseCampagnes#search',
		},
		step2 : {
			inputKey : "meuseCampagnes",
			isTest : false,
			mapping : {
				"Titre" : "name",
		        "Type : ( NGO, Group, LocalBusiness ou GovernmentOrganization )" : "type",
		        "Rue" : "address.streetAddress",
		        "Code Postal" : "address.postalCode",
		        "Ville" : "address.addressLocality",
		        "Code Pays" : "address.addressCountry",
		        "Email" : "email",
		        "Lien" : "url",
		        "Tags":"tags",
		        "Description courte" : "shortDescription",
		        "Description longue" : "description"
			},
			hide : {
				inputKey : true,
				isTest : true
			}
		}
	};
	importMeuse = importObj.init(paramsImport);
	
});



function bindCreateFile(){

	$("#btnPreviousStep").off().on('click', function(e){
		returnStep1();
  	});

//	Bouton Insert
  	$("#btnconfirmInsertMapping").off().on('click', function(e){
  		var params = {
  			names : $("#nameMapping").val(),
  			idMapping : $("#chooseMapping").val(),
  			typeElement : $("#chooseElement").val()
  		}

  		if(params.names != "")
		{
			if(ifMappingDelete == true)
			{
				params.idMapping = "-1";
				ifMappingDelete = false;
			}
  			setMappings(params);
  		}
  		else
  			toastr.error("<?php echo Yii::t("import","You will need to enter the name for your mapping"); ?>");
  	});

//	Boutton Update
  	$("#btnconfirmUpdateMapping").off().on('click',function(e)
  	{
  		  	var params = {
  			names : $("#nameMappingUpdate").val(),
  			idMapping : $("#chooseMapping").val(),
  			typeElement : $("#chooseElement").val()
  		}

  		if(params.names != "")
  			setMappings(params);
  		else
  			toastr.error("<?php echo Yii::t("import","You will need to enter the name for your mapping"); ?>");
  	});

//	BOUTON DELETE
	$("#btnconfirmDeleteMapping").off().on('click', function(e)
	{
		mylog.log("deleteMapping");
		var params = {
			idMapping : $("#chooseMapping").val() //Les liens 
		}
		ajaxPost(
			null,
			baseUrl+"/" + moduleId + "/adminpublic/deletemapping/",
			params,
			function(data){ 
				toastr.success("<?php echo Yii::t("import", "Your mapping has been delete"); ?>");
				$("#modal-delete-element").modal('toggle');
				$("#divUpdate").hide();
				$("#divAjout").show();
				ifMappingDelete = true;
			}
		);
	});
//BOUTON NEXT/PREVIOUS
	


	

	$(".deleteLineMapping").off().on('click', function(){
  		$(this).parent().parent().remove();
  	});

  	$("#btnNextStep2").off().on('click', function(){
  		themeObj.blockUi.show();
		setTimeout(function(){ preStep2(); }, 2000);
  		return false;
  	});


	$("#btnImport").off().on('click', function(){
		$("#btnImport").hide();
		$("#btnBDD").show();
		if(notEmpty($('#jsonCities').val())){
			var zip = new JSZip();
			zip.file(nameFile+"_StandardForCommunecter.json", $('#jsonImport').val());
			zip.file("SaveCities.json", $('#jsonCities').val());
			zip.generateAsync({type:"blob"})
				.then(function(content) {
				    // see FileSaver.js
				    saveAs(content, nameFile+"_Import.zip");
				});
		}else{
			saveAs($('#jsonImport').val(), nameFile+"_StandardForCommunecter.json");
		}
  		// $("<a />", {
		  //   "download": nameFile+"_StandardForCommunecter.json",
		  //   "href" : "data:application/json," + encodeURIComponent($('#jsonImport').val())
		  // }).appendTo("body")
		  // .click(function() {
		  //    $(this).remove()
		  // })[0].click() ;
  	});

  	$("#btnError").off().on('click', function(){
  		$("#btnError").hide();
  		$("<a />", {
		    "download": nameFile+"_NotStandardForCommunecter.json",
		    "href" : "data:application/json," + encodeURIComponent($('#jsonError').val())
		  }).appendTo("body")
		  .click(function() {
		     $(this).remove()
		  })[0].click() ;
  	});
}

function preStep2(){
	cleanVisualisation();
		var nbLigneMapping = $("#nbLigneMapping").val();
		var inputKey = $("#inputKey").val().trim();
		var infoCreateData = [] ;

		//Je sais pas à quoi sa cela correspond
		if(nbLigneMapping == 0){
			toastr.error("<?php echo Yii::t("import","You must make at least one data assignment"); ?>"); 
			$.unblockUI();
  			return false ;
		}else if(inputKey.length == 0){ //Il est n'a pas de clé
			toastr.error("<?php echo Yii::t("import","You will need to add the Key"); ?>");
			$.unblockUI();
  			return false ;
		}
		else{
			//Pour i allant de 0 à nbLigneMapping
			for (i = 0; i <= nbLigneMapping; i++){
				//si lineMapping.lenght+i
	  			if($('#lineMapping'+i).length){
					  // création d'un tableau vide
	  				var valuesCreateData = {};
					valuesCreateData['valueAttributeElt'] = $("#valueAttributeElt"+i).text(); //Récupère les informations du tableau Etape "Lien" côté communecter
					//mylog.log("valyesCreateData ",valuesCreateData['valueAttributeElt']); mon test pour savoir ce que sa fait
					//mylog.log(typeof $("#idHeadCSV"+i).val());
					valuesCreateData['idHeadCSV'] = $("#idHeadCSV"+i).val(); //Récupère les informations du tableau Etape "Lien" partie "Source"
					//mylog.log("valuesCreateData['idHeadCSV']",valuesCreateData['idHeadCSV']);
					infoCreateData.push(valuesCreateData);
	  			}	
	  		}
	  		if(infoCreateData != []){	
	  			
				  //Renseigne les informations importants.
	  			var params = {
	        		infoCreateData : infoCreateData, 
	        		typeElement : typeElement,
	        		nameFile : nameFile,
	        		typeFile : typeFile,
	        		pathObject : $('#pathObject').val(),
			        key : inputKey,
			        warnings : $("#checkboxWarnings").is(':checked')
			    }

				//Si le typeElement concerne les personnes
			    if(typeElement == "<?php echo Person::COLLECTION;?>"){
			    	params["msgInvite"] = $("#msgInvite").val();
					params["nameInvitor"] = $("#nameInvitor").val();
				}

				//Si on a coché la partie "test"
	  			if($("#checkboxTest").is(':checked')){
					//Si c'est un fichier de type "csv"
	  				if(typeFile == "csv"){
	  					//mylog.log("inputNbTest", $("#inputNbTest").val());
	  					var subFile = file.slice(0,parseInt($("#inputNbTest").val())+1);  // Je sais pas à quoi sert cette ligne.
	  					params["file"] = subFile;
	  				}
					// Si c'est un fichier de type JSON
			  		else if(typeFile == "json" || typeFile == "js" || typeFile == "geojson"){
			  			params["file"] = file;
			  			params["nbTest"] = $("#inputNbTest").val();
			  		}
			  		else if(typeFile == "xml"){
			  			params["pathObject"] = "elements";
			  			params["file"] = file;
			  			params["nbTest"] = $("#inputNbTest").val();	
			  		}
	  				//mylog.log("params ",params);
		  			stepThree(params);
		  			showStep3();

	  			}else{
	  				//mylog.log("Here");
					  //Si c'est un fichier csv
	  				if(typeFile == "csv"){
	  					var fin = false ;
				  		var indexStart = 1 ;
				  		var limit = 10 ; //On ne charge pas le fichier d'un block, mais peu par peu
				  		var indexEnd = limit;
				  		var head = file.slice(0,1);
				  		// params["file"] = file;
				  		// mylog.log("hello there", file, params);
				  		// stepThree(params);
				  		//alert("hello there");

				  		while(fin == false){
				  			subFile = head.concat(file.slice(indexStart,indexEnd));
				  			mylog.log("subFile", subFile.length);
				  			params["file"] = subFile;

				  			stepThree(params);

							indexStart = indexEnd ;
				  			indexEnd = indexEnd + limit;
				  			if(indexStart > file.length)
				  				fin = true ;
				  		}
				  		showStep3();
	  				}
					//Si c'est un fichier JSON
			  		else if(typeFile == "json" || typeFile == "js" || typeFile== "geojson"){
			  			params["file"] = file;
				  		stepThree(params);
				  		showStep3();
			  		}
			  		else if(typeFile == "xml"){
			  			params["pathObject"] = "elements";
			  			params["file"] = file;
			  			stepThree(params);
			  			showStep3();
			  		}
	  			}
	  		}
			  //S'il n'y a rien dans le lien
	  		else{
				$.unblockUI();
				toastr.error("<?php echo Yii::t("import","You will need to add the elements in the mapping"); ?>");
			}
		}
}

function stepTwo(){
	//Renvoi dans la console
	mylog.log("stepTwo", typeFile, typeElement);
	var params = {
		typeElement : typeElement, // Organisation, personne...
		typeFile : typeFile, //Si JSON or CSV
		idMapping : $("#chooseMapping").val(), //Les liens 
		path : $("#pathElement").val()
	};

	mylog.log("params", params);

	if(typeFile == "json" || typeFile == "js" || typeFile == "geojson" || typeFile == "xml")
		params["file"] = file ;
	else if(typeFile == "csv")
		file = dataHelper.csvToArray(csvFile, $("#selectSeparateur").val(), $("#selectSeparateurText").val());
	ajaxPost(
		null,
		baseUrl+"/" + moduleId + "/adminpublic/assigndata/",
		params,
		function(data){ 
			mylog.log("stepTwo data",data);
        	if(data.result == true){
        		createStepTwo(data);
        	}else{
				toastr.error(data.msg);
			}
		}
	);
}
function bindUpdate(data){
	//Supprime la ligne sur le tableau "Lien"
	$(".deleteLineMapping").off().on('click', function(){
  		$(this).parent().parent().remove();
  	});

//On prend en charge le fichier de l'utilisateurs
  	$("#fileImport").change(function(e) {
    	var fileSplit = $("#fileImport").val().split("."); 
		/*if(file.length == 0 && csvFile.length == 0){
	  		toastr.error("Vous devez sélectionner un fichier.");
	  		return false ;
		}*/
		//Si l'extension n'est pas un CSV n'y JSON fait apparait un msg dans les notifications
		if(extensions.indexOf(fileSplit[fileSplit.length-1]) == -1){
  			toastr.error("<?php echo Yii::t("import", "You will need to select a file CSV, JSON or XML"); ?>");
  			return false ;
  		}
		
		//Affiche les fichiers qu'on compte upload sur le serveur
  		nameFileSplit = fileSplit[0].split('\\');
  		mylog.log("nameFileSplit", nameFileSplit);
  		nameFile = nameFileSplit[nameFileSplit.length-1];
		typeFile = fileSplit[fileSplit.length-1];

		//Si le format ne correspond pas
		if(extensions.indexOf(typeFile) == -1) {
			alert('<?php echo Yii::t("import","Upload CSV, JSON or XML"); ?>');
			return false;
		}
		file = [];		//Tableau vide
		if (e.target.files != undefined) {
			var reader = new FileReader();
			reader.onload = function(e) {
				//SI CSV
				if(typeFile == "csv"){
					//var csvval=e.target.result.split("\n");
					csvFile = e.target.result;
					//mylog.log("csv : ", csvval );
					/*$.each(csvval, function(key, value){
						var ligne = value.split(";");
						var newLigne = [];
						$.each(ligne, function(keyLigne, valueLigne){
							//mylog.log("valueLigne", valueLigne);
							if(valueLigne.charAt(0) == '"' && valueLigne.charAt(valueLigne.length-1) == '"'){
								var elt = valueLigne.substr(1,valueLigne.length-2);
								newLigne.push(elt);
							}else{
								newLigne.push(valueLigne);
							}
						});
		  				file.push(newLigne);
		  			});*/
		  			$("#divCsv").show();
				}
				//Si JSON
				else if(typeFile == "json" || typeFile == "js" || typeFile == "geojson") {
					$("#divCsv").hide();
					$("#divPathElement").show();
					file.push(e.target.result);
	  			}
	  			else if(typeFile == "xml"){
	  				file.push(e.target.result);
	  				$("#divCsv").hide();
	  				$("#divPathElement").hide();
	  			}	
			};
			reader.readAsText(e.target.files.item(0));
		}
		return false;
	});
}

function createStepTwo(data){

//Gestion du select côté source
	var chaineSelectCSVHidden = "" ;
	var i = 0;
	if(data.typeFile == "csv"){ //Cas CSV
		$("#nbFileMapping").html(file.length - 1 + "<?php echo Yii::t("import"," element(s)"); ?>"); //Compte le nb d'élèment
		$.each(file[0], function(key, value){
			// chaineSelectCSVHidden += (key == data.arrayMapping[i]?'<option value="'+value+'" disabled>'+value+'</option>':'<option value="'+value+'">'+value+'</option>'); //Pour l'utilisateur puisse rajouté un élèment en cas s'il lui manque
			chaineSelectCSVHidden += '<option value="'+value+'">'+value+'</option>';
			// i++;
		});
	}
	else if(data.typeFile == "json" || data.typeFile == "geojson" || data.typeFile == "js"){ //Cas JSON
		$("#nbFileMapping").html(data.nbElement  + "<?php echo Yii::t("import"," element(s)"); ?>"); //Compte le nb d'élèment
		$.each(data.arbre, function(key, value){
			chaineSelectCSVHidden += '<option value="'+value+'">'+value+'</option>'; //Pour l'utilisateur puisse rajouté un élèment en cas s'il lui manque
		});
	}

	else if(data.typeFile == "xml"){
		$("#nbFileMapping").html(data.nbElement + "<?php echo Yii::t("import"," element(s)"); ?>");
		$.each(data.arbre, function(key, value){
			chaineSelectCSVHidden += '<option value="'+value+'">'+value+'</option>';
		});

		file[0] = data.json;
		mylog.log("file", file);
	}	

	//chaineSelectCSVHidden += '<option onClick="switchChamp()" value="Autre" ><?php echo Yii::t("import","Other"); ?></option>';	
	
	$("#selectSource").html(chaineSelectCSVHidden); //Le select de la partie "Lien" côté Source
	

//On fait de même pour le select côté communecter
	chaineAttributesElt = "" ;
	$.each(data.attributesElt, function(key, value){
		var valueadd = value.replace("address.","");
		if(value == listeObligatoire[value] || valueadd == listeObligatoire[valueadd]){
			chaineAttributesElt += '<option name="optionAttributesElt" value="'+value+'">'+value+' (*) </option>';
		}
		else{
			chaineAttributesElt += '<option name="optionAttributesElt" value="'+value+'">'+value+'</option>';
		}
	});

	$("#selectAttributesElt").html(chaineAttributesElt); //Le select de la partie "Lien" côté Communecter

//Affiche information de data
	mylog.log("createStepTwo", data);
	//mylog.log("ListSource", listSource);
//Partie HTML a était mise en commentaire
	if(typeElement != "<?php echo Organization::COLLECTION;?>")
		$("#divCheckboxWarnings").hide();

	if(typeElement != "<?php echo Person::COLLECTION;?>")
		$("#divInvite").hide();
	
	//Si la chaîne renvoyé est différent de "undefined"
	if(typeof data.arrayMapping != "undefined"){
		var nbLigneMapping = $("#nbLigneMapping").val();
		var i = 0 ;
		$.each(data.arrayMapping, function(key, value){
			ligne = '<tr id="lineMapping'+nbLigneMapping+'" class="lineMapping"> ';
	  		ligne =	 ligne + '<td id="valueSource'+nbLigneMapping+'">' + key + '</td>';

			ligne =	 ligne + '<td id="valueAttributeElt'+nbLigneMapping+'">' + value + '</td>';
			ligne =	 ligne + '<td><input type="hidden" id="idHeadCSV'+nbLigneMapping+'" value="'+ key +'"/><a href="javascript:;" class="deleteLineMapping btn btn-danger">X</a></td></tr>';
	  		nbLigneMapping++;

	  		$("#LineAddMapping").before(ligne);
	  		i++;
		});
		$("#nbLigneMapping").val(nbLigneMapping);
	}

	
	if(typeof data.idMapping == "undefined" || data.idMapping == "-1" ){
		$("#divAjout").show();
		$("#divUpdate").hide();
	}
	else if(data.idMapping != ""){
		$("#divAjout").hide();
		$("#divUpdate").show();
	}
	else{
		$("#divAjout").hide();
		$("#divUpdate").hide();
	}

	if(typeof data.mapping != "undefined" && typeof data.mapping.name != "undefined"){
		var nameUpdate = '<input type="text" name="nameMappingUpdate" id="nameMappingUpdate" value='+data.mapping.name+'>';
		$("#divSaisirNameUpdate").html(nameUpdate);
	}
	
	
	//mylog.log("listElt",listElt);

	//disabledChamp();
	bindUpdate();
	displayStepTwo();
}

function verifNameSelected(arrayName){
	var find = false ; 
	$.each(arrayName, function(key, value){
		var beInt = parseInt(value);
		if(!isNaN(beInt)){
			find = true ;
		}
	});
	return find ;
}

//Désactive les élèments de la partie 2
function displayStepTwo(){
	mylog.log("showStep2")
	$('#menu-step-2 i.fa').removeClass("fa-circle-o").addClass("fa-circle");
	$('#menu-step-1 i.fa').removeClass("fa-circle").addClass("fa-check-circle");
	$('#menu-step-1').removeClass("selected");
	$('#menu-step-2').addClass("selected");
	$("#menu-step-mapping").show(400);
	$("#menu-step-source").hide(400);
	$("#menu-step-visualisation").hide(400);
}

//Préparation de la partie 3
function showStep3(){
	mylog.log("showStep3");
	$('#menu-step-3 i.fa').removeClass("fa-circle-o").addClass("fa-circle");
	$('#menu-step-2 i.fa').removeClass("fa-circle").addClass("fa-check-circle");
	$('#menu-step-2').removeClass("selected");
	$('#menu-step-3').addClass("selected");
	$("#menu-step-mapping").hide(400);
	$("#menu-step-source").hide(400);
	$("#menu-step-visualisation").show(400);
	//alert("hello");
	$.unblockUI();
}

//Retourne dans l'étape 2 (l'interface)
function returnStep2(){
	mylog.log("returnStep2");
	$('#menu-step-3 i.fa').removeClass("fa-circle").addClass("fa-circle-o");
	$('#menu-step-2 i.fa').removeClass("fa-check-circle").addClass("fa-circle");
	$('#menu-step-3').removeClass("selected");
	$('#menu-step-2').addClass("selected");
	$("#menu-step-mapping").show(400);
	$("#menu-step-source").hide(400);
	$("#menu-step-visualisation").hide(400);
	$("#btnImport").hide();
	$("#btnError").hide();
	$("#btnBDD").hide();
	nbFinal=0;
}

//Retourne dans l'étape 1 (l'interface & init des fichiers).
function returnStep1(){
	mylog.log("returnStep1");
	file = [] ;
	nameFile = "";
	typeFile = "";
	typeElement = "";
	nbFinal=0;
	$('#divInputFile').html('<input type="file" id="fileImport" name="fileImport" accept=".csv,.json,.js,.geojson,.xml">')
	$('#menu-step-1 i.fa').removeClass("fa-circle-o").addClass("fa-circle");
	$('#menu-step-2 i.fa').removeClass("fa-circle").addClass("fa-circle-o");
	$('#menu-step-2').removeClass("selected");
	$('#menu-step-1').addClass("selected");
	$("#menu-step-mapping").hide(400);
	$("#menu-step-source").show(400);
	$("#menu-step-visualisation").hide(400);
	$(".lineMapping").remove();
	bindUpdate();
}


function addNewMappingForSelecte(arrayMap, subArray){
	var firstElt = arrayMap[0] ;
	arrayMap.shift(); //Supprime le premier élèment du tableau.
	var beInt = parseInt(firstElt);
	var newSelect = {} ;

	if(!isNaN(beInt)){
		beInt++;
		if(subArray){	
			if(arrayMap.length >= 1){
				var newArrayMap = jQuery.extend([], arrayMap);
				newSelect[firstElt] = addNewMappingForSelecte(arrayMap, subArray);
				newSelect[beInt.toString()] = addNewMappingForSelecte(newArrayMap, subArray);
			}
			else{
				newSelect[firstElt] = "";
				newSelect[beInt.toString()] = "";
			}
		}
		else{
			if(arrayMap.length >= 1){
				subArray = true ;
				newSelect[beInt.toString()] = addNewMappingForSelecte(arrayMap, subArray);
			}
			else{
				newSelect[beInt.toString()] = "";
			}
		}
	}
	else{
		if(arrayMap.length >=1){
			newSelect[firstElt] = addNewMappingForSelecte(arrayMap, true);
		}
		else{
			newSelect[firstElt] = "";
		}
	}
	return newSelect ;
}

function getOptionHTML(arrayOption, objectOption, father)
{
	if(!jQuery.isPlainObject(objectOption)){
		arrayOption.push(father);
	}
	else{
		$.each(objectOption, function(key, values){
			if(father != "")
				var newfather = father +"."+ key
			else
				var newfather = key
			getOptionHTML(arrayOption, values, newfather);
		});
	}
}

function verifBeforeAddSelect(arrayMap)
{
	$('[name=optionAttributesElt]').each(function() {
	  	var option = $(this).val() ;
	  	var position = jQuery.inArray( option, arrayMap);
	  	if(position != -1)
	  		arrayMap.splice(position, 1);
		//mylog.log("option", option);
	});
}
//Reset les informations contenant dans l'étape 3
function cleanVisualisation(){
	$("#representation").html("");
	$("#jsonImport").val("");
    $("#jsonError").val("");
    $("#jsonCities").val("");
}

function createInpu(nameFile, typeFile, typeElement){
	var chaineInputHidden = '<input type="hidden" id="typeElement" value="' + typeElement + '"/>';
	chaineInputHidden += '<input type="hidden" id="nameFile" value="'+nameFile+'"/>';
	chaineInputHidden += '<input type="hidden" id="typeFile" value="'+typeFile+'"/>';
	$("#divInputHidden").html(chaineInputHidden);
}

//Troisième étape
function stepThree(params){
	
}

function setMappings(params)
{
		var nbLigneMappings = $("#nbLigneMapping").val();
		var mappingCommunecter = [];
		var mappingSource = [];
		//Parcours les liens que l'user a mise et on les stock dans un tableau
		for(j = 0; j <= nbLigneMappings; j++){
			if($('#lineMapping'+j).length){
				//Mapping côté communecter
				var mappingCommunecterInsert = {};
				mappingCommunecterInsert = $("#valueAttributeElt"+j).text();
				mappingCommunecter.push(mappingCommunecterInsert);
				//Mapping côté source
				var mappingSourceInsert = {};
				mappingSourceInsert = $("#valueSource"+j).text();
				mappingSource.push(mappingSourceInsert);
			}
		}

		var param = {
			name : params.names,
			attributeSource : mappingSource,
			attributeElt: mappingCommunecter,
			idMapping : params.idMapping,
			typeElement : params.typeElement
		}

		mylog.log("setMapping", param);
		if(param.attributeElt != "" && param.attributeSource != "")
		{
			if(param.idMapping == "-1" ||  param.idMapping  == "5b0d1b379eaf44ea598b4580" || param.idMapping == "5b0d1b379eaf44ea598b4581" || param.idMapping ==  "5b0d1b379eaf44ea598b4582" || param.idMapping ==  "5b0d1b379eaf44ea598b4583"|| param.idMapping == "5b1654d39eaf4427171cd718")
			{
				ajaxPost(
					null,
					baseUrl+"/" + moduleId + "/adminpublic/setmapping/",
					param,
					function(data){ 
						mylog.log("sucess", data);
						toastr.success("<?php echo Yii::t("import","Your mapping have been added"); ?>");
						$("#modal-ajout-element").modal('toggle');
						$("#divAjout").hide();
						mappingPrevious += '<option value="'+data._id+'">'+data.name+'</option>';
					}
				);
			}
			else
			{
				ajaxPost(
					null,
					baseUrl+"/" + moduleId + "/adminpublic/setmapping/",
					param,
					function(data){ 
						mylog.log("sucess", data);
						toastr.success("<?php echo Yii::t("import","Your mapping have been updated"); ?>");
						$("#modal-update-element").modal('toggle');
					}
				);
			}
			//mylog.log(mappingPrevious);
			$("#chooseMapping").html(mappingPrevious);	
		}
		else 
		{	
			toastr.error("<?php echo Yii::t("import","You will need to complete at least one field"); ?>");
		}

}

function switchChamp(){
	selectSource = $("#selectSource option:selected").val();
	hiddenSwitch = $("#hiddenSwitch").val();
	if(hiddenSwitch === "noHidden" && selectSource === "Autre")
	{
		mylog.log("switchChamp");
		$("#selectSource").hide();
		$("#selectSourceTxt").show();
		$("#hiddenSwitch").val('yesHidden');
		return true;
	}
	else
	{
		$("#selectSource").show();
		$("#selectSourceTxt").hide();
		return false;
	}
}
/*function disabledChamp(){

var chaineSelectSource = "";
var i = 0;
if(listSource != [])
	$.each(listSource,function(key,value){
		chaineSelectSource += (key == listArbre[i] ? "<option value="+value+" disabled>"+value+"</option>" : "<option value="+value+" >"+value+"</option>");
		i++;
	});
	$("#selectSource").html(chaineSelectSource);
	mylog.log(chaineSelectSource);

}*/
</script>