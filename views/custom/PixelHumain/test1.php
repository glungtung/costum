<?xml version="1.0" encoding="utf-8"?>
			<!-- Generator: Adobe Illustrator 24.0.3, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
			<svg class="pull-left filtres" version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 476 185" style="enable-background:new 0 0 476 185;" xml:space="preserve">
				<style type="text/css">
					.st0{fill:none;}
					.st1{fill:#3E3C3F;}
					.st2{fill:#F7E200;}
					.st3{fill:#263A46;}
					.st4{fill:#FFFFFF;}
					.st5{fill:url(#SVGID_1_);}
					.st6{fill:url(#SVGID_2_);}
					.st7{fill:url(#New_Pattern_3);}
				</style>
				<pattern  y="-182" width="113.39" height="170.08" patternUnits="userSpaceOnUse" id="New_Pattern_3" viewBox="41.44 -218.56 113.39 170.08" style="overflow:visible;">
					<g>
						<rect x="41.44" y="-218.56" class="st0" width="113.39" height="170.08"/>
						<g>
							<rect x="85.61" y="-85.47" class="st1" width="57.7" height="57.7"/>
							<rect x="113.39" y="-57.7" class="st1" width="57.7" height="57.7"/>
							<rect x="95.79" y="-122.43" class="st2" width="57.7" height="57.7"/>
							<rect x="123.57" y="-94.66" class="st2" width="57.7" height="57.7"/>
							<rect x="150.34" y="-67.88" class="st2" width="57.7" height="57.7"/>
							<rect x="146.83" y="-100.42" class="st1" width="36.39" height="36.39"/>
							<rect x="153.25" y="-123.73" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="0" y="-57.7" class="st1" width="57.7" height="57.7"/>
							<rect x="10.18" y="-94.66" class="st2" width="57.7" height="57.7"/>
							<rect x="36.96" y="-67.88" class="st2" width="57.7" height="57.7"/>
							<rect x="33.44" y="-100.42" class="st1" width="36.39" height="36.39"/>
							<rect x="50.96" y="-82.91" class="st1" width="36.39" height="36.39"/>
							<rect x="69.15" y="-66.02" class="st1" width="36.39" height="36.39"/>
							<rect x="39.86" y="-123.73" class="st2" width="36.39" height="36.39"/>
							<rect x="57.38" y="-106.22" class="st2" width="36.39" height="36.39"/>
							<rect x="74.27" y="-89.33" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="142.31" y="-170.51" class="st1" width="57.7" height="57.7"/>
							<rect x="152.49" y="-207.47" class="st2" width="57.7" height="57.7"/>
						</g>
						<g>
							<rect x="28.92" y="-170.51" class="st1" width="57.7" height="57.7"/>
							<rect x="56.69" y="-142.74" class="st1" width="57.7" height="57.7"/>
							<rect x="85.54" y="-115.96" class="st1" width="57.7" height="57.7"/>
							<rect x="39.1" y="-207.47" class="st2" width="57.7" height="57.7"/>
							<rect x="66.87" y="-179.7" class="st2" width="57.7" height="57.7"/>
							<rect x="93.65" y="-152.92" class="st2" width="57.7" height="57.7"/>
							<rect x="90.14" y="-185.46" class="st1" width="36.39" height="36.39"/>
							<rect x="107.65" y="-167.95" class="st1" width="36.39" height="36.39"/>
							<rect x="125.85" y="-151.06" class="st1" width="36.39" height="36.39"/>
							<rect x="96.56" y="-208.77" class="st2" width="36.39" height="36.39"/>
							<rect x="114.07" y="-191.26" class="st2" width="36.39" height="36.39"/>
							<rect x="130.96" y="-174.37" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="12.46" y="-151.06" class="st1" width="36.39" height="36.39"/>
							<rect x="17.57" y="-174.37" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="85.61" y="-255.55" class="st1" width="57.7" height="57.7"/>
							<rect x="113.39" y="-227.78" class="st1" width="57.7" height="57.7"/>
							<rect x="142.24" y="-201" class="st1" width="57.7" height="57.7"/>
							<rect x="123.57" y="-264.74" class="st2" width="57.7" height="57.7"/>
							<rect x="150.34" y="-237.96" class="st2" width="57.7" height="57.7"/>
						</g>
						<g>
							<rect x="0" y="-227.78" class="st1" width="57.7" height="57.7"/>
							<rect x="28.85" y="-201" class="st1" width="57.7" height="57.7"/>
							<rect x="10.18" y="-264.74" class="st2" width="57.7" height="57.7"/>
							<rect x="36.96" y="-237.96" class="st2" width="57.7" height="57.7"/>
							<rect x="50.96" y="-252.99" class="st1" width="36.39" height="36.39"/>
							<rect x="69.15" y="-236.1" class="st1" width="36.39" height="36.39"/>
						</g>
					</g>
				</pattern>
				<pattern  y="-182" width="113.39" height="170.08" patternUnits="userSpaceOnUse" id="New_Pattern_4" viewBox="40.36 -218.56 113.39 170.08" style="overflow:visible;">
					<g>
						<rect x="40.36" y="-218.56" class="st0" width="113.39" height="170.08"/>
						<g>
							<rect x="84.54" y="-85.47" class="st3" width="57.7" height="57.7"/>
							<rect x="113.39" y="-57.7" class="st3" width="57.7" height="57.7"/>
							<rect x="94.71" y="-122.43" class="st2" width="57.7" height="57.7"/>
							<rect x="122.49" y="-94.66" class="st2" width="57.7" height="57.7"/>
							<rect x="149.27" y="-67.88" class="st2" width="57.7" height="57.7"/>
							<rect x="145.75" y="-100.42" class="st3" width="36.39" height="36.39"/>
							<rect x="152.17" y="-123.73" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="0" y="-57.7" class="st3" width="57.7" height="57.7"/>
							<rect x="9.1" y="-94.66" class="st2" width="57.7" height="57.7"/>
							<rect x="35.88" y="-67.88" class="st2" width="57.7" height="57.7"/>
							<rect x="32.37" y="-100.42" class="st3" width="36.39" height="36.39"/>
							<rect x="49.88" y="-82.91" class="st3" width="36.39" height="36.39"/>
							<rect x="68.07" y="-66.02" class="st3" width="36.39" height="36.39"/>
							<rect x="38.79" y="-123.73" class="st2" width="36.39" height="36.39"/>
							<rect x="56.3" y="-106.22" class="st2" width="36.39" height="36.39"/>
							<rect x="73.19" y="-89.33" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="141.23" y="-170.51" class="st3" width="57.7" height="57.7"/>
							<rect x="151.41" y="-207.47" class="st2" width="57.7" height="57.7"/>
						</g>
						<g>
							<rect x="27.84" y="-170.51" class="st3" width="57.7" height="57.7"/>
							<rect x="56.69" y="-142.74" class="st3" width="57.7" height="57.7"/>
							<rect x="84.46" y="-115.96" class="st3" width="57.7" height="57.7"/>
							<rect x="38.02" y="-207.47" class="st2" width="57.7" height="57.7"/>
							<rect x="65.79" y="-179.7" class="st2" width="57.7" height="57.7"/>
							<rect x="92.57" y="-152.92" class="st2" width="57.7" height="57.7"/>
							<rect x="89.06" y="-185.46" class="st3" width="36.39" height="36.39"/>
							<rect x="106.57" y="-167.95" class="st3" width="36.39" height="36.39"/>
							<rect x="124.77" y="-151.06" class="st3" width="36.39" height="36.39"/>
							<rect x="95.48" y="-208.77" class="st2" width="36.39" height="36.39"/>
							<rect x="112.99" y="-191.26" class="st2" width="36.39" height="36.39"/>
							<rect x="129.88" y="-174.37" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="11.38" y="-151.06" class="st3" width="36.39" height="36.39"/>
							<rect x="16.5" y="-174.37" class="st2" width="36.39" height="36.39"/>
						</g>
						<g>
							<rect x="84.54" y="-255.55" class="st3" width="57.7" height="57.7"/>
							<rect x="113.39" y="-227.78" class="st3" width="57.7" height="57.7"/>
							<rect x="141.16" y="-201" class="st3" width="57.7" height="57.7"/>
							<rect x="122.49" y="-264.74" class="st2" width="57.7" height="57.7"/>
							<rect x="149.27" y="-237.96" class="st2" width="57.7" height="57.7"/>
						</g>
						<g>
							<rect x="0" y="-227.78" class="st3" width="57.7" height="57.7"/>
							<rect x="27.77" y="-201" class="st3" width="57.7" height="57.7"/>
							<rect x="9.1" y="-264.74" class="st2" width="57.7" height="57.7"/>
							<rect x="35.88" y="-237.96" class="st2" width="57.7" height="57.7"/>
							<rect x="49.88" y="-252.99" class="st3" width="36.39" height="36.39"/>
							<rect x="68.07" y="-236.1" class="st3" width="36.39" height="36.39"/>
						</g>
					</g>
				</pattern>
				<polygon class="st2" points="332.97,100.99 332.97,84.99 30.97,84.99 30.97,163.99 352.97,163.99 352.97,100.99 "/>
				<rect x="374.97" y="86.99" transform="matrix(0.866 0.5 -0.5 0.866 98.8007 -178.7586)" class="st4" width="16" height="16"/>
				<pattern  id="SVGID_1_" xlink:href="#New_Pattern_4" patternTransform="matrix(1.7974 0 0 1.7974 5503.7778 5679.0088)">
				</pattern>
				<polygon class="st5" points="406.97,57.99 374.97,57.99 393.97,46.99 406.97,25.99 "/>
				<pattern  id="SVGID_2_" xlink:href="#New_Pattern_4" patternTransform="matrix(1.7974 0 0 1.7974 5503.7778 5679.0088)">
				</pattern>
				<polygon class="st6" points="455.97,41.99 423.97,41.99 431.97,16.99 455.97,9.99 "/>
				<polygon class="st7" points="420.94,101.74 412.21,70.95 438.45,71.84 439.97,88.99 "/>
				<polygon class="st4" points="437.86,15.27 439.97,41.99 423.97,41.99 431.97,16.99 "/>
			</svg>