    <style type="text/css">
    	.text-explain{
    		font-size: 22px;
    	}
    </style>
<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h2 class="title-section col-sm-8">Mentions légales</h2>
        	<hr>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Editeur du site</h3>
        	<span class="col-xs-12 text-left text-explain">

                Le GCSMS RelYance Terramies (Groupement de Coopération Sociale et Médico-sociale), identifiée au répertoire Sirène sous le n° 879 135 028 00022, représenté par Monsieur Pierre-Antoine LE PAGE, Directeur, dûment habilité, conformément aux termes de la Convention Constitutive du groupement Relyance Terramies du 10 octobre 2019.
                <br/><br/>
                Adresse du siège social : Le Mozart - Bâtiment D - 17, avenue Condorcet 69100 Villeurbanne <br/>
                Siret : 879 135 028 00022<br/>
                APE : 8790A (Hébergement social pour enfants en difficultés)<br/><br/>
                <b>Informations générales</b><br/>
                E-mail : info@terramies.fr<br/>
                Questions juridiques et vie privée: siege@terramies.fr <br/>
                Téléphone : 04.87.65.32.32<br/>   
                Horaires : Du lundi au jeudi de 09h à 12h et de 14h 18h<br/>      
    		</span>
        </div>
        <div class="col-xs-12">
            <h3 class="sub-header-section col-sm-12">Responsable de la publication et de la collecte des données à caractère personnel</h3>
            <span class="col-xs-12 text-left text-explain">
                M. Pierre-Antoine Le Page pour le compte du GCSMS RelYance Terramies.
            </span>
        </div>  
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Hébergement :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Le site est hébergé par la société OVH, SAS au capital de 10 000 000€ ayant son siège social au 2 rue Kellermann - 59100 Roubaix - France, immatriculée au R.C.S. de Roubaix sous le numéro 424 761 419.<br>Contact : <a href="https://www.ovh.com/fr/support/" target="_blank">https://www.ovh.com/fr/support/</a>
    		</span>
        </div>
       <!--  <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Protection des données personnelles :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Les informations collectées nous permettront de mieux vous connaître. Elles seront utilisées pour vous informer sur la campagne de la liste #MayenneDemain et la construction de son programme.<br/><br/> Conformément aux dispositions de la loi Informatique et Libertés du 6 Janvier 1978, vous disposez d’un droit d’accès, de modification, de rectification et de suppression des données qui vous concernent. Pour exercer ce droit, adressez-vous à : Jean-Claude Lavandier, place Cheverus, 53100 MAYENNE
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Propriété intellectuelle :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Les contenus de ce site sont soumis à la licence Creative Commons BY-SA-NC. Pour améliorer l’accès à la culture et à la connaissance libres, #MayenneDemain autorise la redistribution et réutilisation libre de ses contenus pour n’importe quel usage, sauf à des fins commerciales. Une telle utilisation n’est autorisée que si l’auteur est précisé, et la liberté de réutilisation et de redistribution s’applique également à tout travail dérivé de ces contributions.
    		</span>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12"> Responsabilité quant au contenu :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Alors même que #MayenneDemain s’efforce que le contenu de son site soit le plus à jour et le plus fiable possible, il ne peut donner aucune garantie quant à l’ensemble des informations présentes sur le site, qu’elles soient fournies par ses soins, par ses partenaires ou par tout tiers, par l’envoi d’e-mails ou de toute autre forme de communication.<br/><br/>
        		Compte tenu des caractéristiques du réseau Internet que sont la libre captation des informations diffusées et la difficulté, voire l’impossibilité, de contrôler l’utilisation qui pourrait en être faite par des tiers, #MayenneDemain n’apporte aucune garantie quant à l’utilisation des dites informations.<br/><br/>
        		#MayenneDemain ne saurait en conséquence être tenue pour responsable du préjudice ou dommage pouvant résulter de l’utilisation des informations présentes sur le site, ni d’erreurs ou omissions dans celles-ci.<br/><br/>
        		#MayenneDemain décline toute responsabilité en ce qui concerne les contenus des sites web édités par des tiers et accessibles depuis <a href="https://www.mayenne-demain.fr">www.mayenne-demain.fr</a> par des liens hypertextes ou répertoriés dans la cartographie du site.
    		</span>
        </div>  -->
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12"> Réalisation du site :</h3>
        	<span class="col-xs-12 text-left text-explain">
        		Le site <a href="https://www.terramies.fr">https://www.terramies.fr</a> a été développé par le collectif Pixel Humain sur la base du réseau <a href="https://www.communecter.org">communecter.org</a>.
    		</span>
    		<span class="col-xs-12 text-left text-explain">Le site est hébergé par <a href="https://gitlab.adullact.net/pixelhumain" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGO_PIXEL_HUMAIN.png" height=50></a></span>
        </div>
        <div class="col-xs-12">
            <h3 class="sub-header-section col-sm-12"> Crédits :</h3>
            <span class="col-xs-12 text-left text-explain">
                Les photos illustrant le site ont été soit réalisées directement par l’éditeur, soit utilisées légalement via le site <a href="https://stock.adobe.com/fr/" target="_blank">https://stock.adobe.com/fr/</a> par l'achat d'une licence par le pretataire ayant réalisé le design, soit téléchargées sur la plateforme libre de droit  <a href="https://unsplash.com/" target="blank">« Unsplash »</a> (voir sources sur le site). 
                <br/><br/>
                Les textes sont intégralement écrits en interne par le GCSMC RelYance Terramies, et sont disponibles sous licence Creative Commons.
            </span>
        </div>  
    </div>
</div>

<script>
jQuery(document).ready(function() {
    setTitle("Mentions légales"); 
});
</script>