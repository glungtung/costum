<script type="text/javascript">

    var pageApp=<?php echo json_encode(@$page); ?>;            
	var paramsFilter= {
	 	container : "#filters-nav",
        results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true
        },
	 	filters : {
                      
            actor : {
                view : "dropdownList",
                type : "filters",
                field : "category",
                name : "Acteurs",
                event : "filters",
                keyValue : false,
                list : {
                    "entreprise":"Entreprises",
                    "loisir": "Sport et loisirs",
                    "annuaireMedical":"Etablissements de santé"
                }
            },
            
            types : {
                lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization"]
            } 
        }
        
     };
  
    var filterSearch={};
    jQuery(document).ready(function() {
        mylog.log('filterSearch paramsFilter', paramsFilter);
        filterSearch = searchObj.init(paramsFilter);

 
    });

</script>