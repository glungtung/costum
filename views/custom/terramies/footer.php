<footer class="col-xs-12 col-lg-12 no-padding c-footer o-wrapper" style="background-color:rgba(0,0,0,.6);color: #14489d;">
      <div class="o-boxer">
        <div class="col-xs-11 col-xs-offset-1" style="font-size:40px;">
          <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/logo.png" class="col-xs-11 col-xs-offset-1 margin-bottom-30">
          <span id="divFacebook" class="col-xs-offset-3 col-xs-4">
              <a class="contentInformation btn-network tooltips" href="https://www.facebook.com/Terramies-107600451173189/" target="_blank" id="facebookAbout" data-toggle="tooltip" data-placement="left" title="" data-original-title="Facebook">
                <i class="fa fa-facebook" style="color:#1877f2;"></i>
              </a>
          </span>
          <span id="divTwitter" class="col-xs-5" >
              <a class="contentInformation btn-network tooltips" href=" https://twitter.com/terramies" target="_blank" id="twitterAbout" data-toggle="tooltip" data-placement="left" title="" data-original-title="Twitter">
                <i class="fa fa-twitter" style="color:#1da1f2;"></i>
              </a>
          </span>
          <span id="divInstagram" class="col-xs-offset-3 col-xs-4" >
              <a class="contentInformation btn-network tooltips" href="https://instagram.com/terramies?igshid=118vmncbvpn5c" target="_blank" id="instagramAbout" data-toggle="tooltip" data-placement="left" title="" data-original-title="Instagram">
                <i class="fa fa-instagram" style="color: #c94d95;"></i>
              </a>
          </span>
          <span id="divLinkedin" class="col-xs-5 col-xs-offset-0" >
              <a class="contentInformation btn-network tooltips" href="https://www.linkedin.com/company/groupement-de-coop%C3%A9ration-sociale-et-m%C3%A9dico-sociale-relyance-terramies/" target="_blank" id="linkedinAbout" data-toggle="tooltip" data-placement="left" title="" data-original-title="LinkedIn">
                <i class="fa fa-linkedin" style="color:#0a66c2;"></i>
              </a>
          </span>
        </div>  
        <div>
          <a style="text-decoration: none;">
            <span class="c-footer--title">GCSMS RelYance Terramies</span>
            <span class="c-footer--address">Le Mozart - Bâtiment D<br/>17, avenue Condorcet<br>69100 Villeurbanne<br><br>
              Ouvert du lundi au vendredi de 09h à 12h et de 14h à 18h</</span>
          </a>
          <a class="c-footer--mail" href="mailto:">info@terramies.fr</a>
          <a class="c-footer--tel" href="tel:">04.87.65.32.32</a>
        </div>
        <div>  
          <a href="#mentions" class="c-footer--title lbh"><?php echo Yii::t("home","Mentions légales") ?></a>
          <a  href="#cgu" class="c-footer--title lbh" href="">Conditions générales d'utilisation</a>
          <a  href="#confidentialite" class="c-footer--title lbh" href="">Politique de confidentialité</a>
        </div>
      </div>
      <div class="col-xs-12 text-center">
        <span><i class="fa fa-creative-commons"></i> Le contenu du site est sous licence Creative Commons</span>
      </div>
</footer>