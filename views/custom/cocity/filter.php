<?php 
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}
$filliaireCategories = CO2::getContextList("filliaireCategories"); 
?>
<script type="text/javascript">

var thematic = null;

	
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.theme != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.theme);
	}
	mylog.log("thematic",thematic);
	

	var filliaireCategories = <?php echo json_encode(@$filliaireCategories); ?>;
	var pageApp = "<?= @$page?>";
	var typeClassified=<?php echo json_encode(@$_GET["type"]); ?>;
	var sectionClassified=<?php echo json_encode(@$_GET["section"]); ?>;
	if(notNull(costum) && typeof costum["app"]["#annonces"] != "undefined" && typeof costum["app"]["#annonces"]["filters"] != "undefined" &&  typeof costum["app"]["#annonces"]["filters"]["type"] != "undefined"){
		typeClassified=costum["app"]["#annonces"]["filters"]["type"];
	}
	sectionsClassified={};
	categoriesList={};
	if(notEmpty(typeClassified)){
		$.each(modules[typeClassified].categories.sections, function(k,v){
			sectionsClassified[k]=(typeof tradCategory[v.labelFront] != "undefined") ? tradCategory[v.labelFront] : v.label; 
		});

		$.each(modules[typeClassified].categories.filters, function(k,v){
			labelCat=(typeof tradCategory[k] != "undefined") ? tradCategory[k] : v.label;
			categoriesList[k]={label : labelCat, icon : v.icon};
			if(typeof v.subcat != "undefined"){
				categoriesList[k]["subList"]={};
				$.each(v.subcat, function(e,val){
					labelCat=(typeof tradCategory[e] != "undefined") ? tradCategory[e] : val.label;
					categoriesList[k].subList[e]={label : labelCat, icon : val.icon};
				});			
			}
		});
	}
	var paramsFilter = {
		container : "#filters-nav",
		defaults : {
			notSourceKey:true,
			indexStep : 0,
			filters : {
				$or :{
					"source.keys" : "<?=$el["slug"]?>",
				}				
			},
		},
		results : {
			smartGrid : true,
			renderView : "directory.elementPanelHtml"
		},
		filters : {
			
		}
	}
	<?php if(!isset($el["ville"])){?>
		<?php if ($el["address"]["postalCode"] == "")  {?>
			paramsFilter.defaults.filters["$or"] = {
				"source.keys" : "<?=$el["slug"]?>",
				"$and":[
					{"address.addressLocality" :"<?=$el["address"]["addressLocality"]?>"},
					{"address.level1Name" :"<?=$el["address"]["level1Name"]?>"}
				],
				"links.memberOf.<?=(String)$el["_id"]?>" : {$exists:true}
			}
		<?php }else{ ?>
			paramsFilter.defaults.filters["$or"] = {
				"source.keys" : "<?=$el["slug"]?>",
				"$and":[
					{"address.postalCode":"<?=$el["address"]["postalCode"]?>"},
					{"address.level1Name" :"<?=$el["address"]["level1Name"]?>"},
					{"address.addressLocality" :"<?=$el["address"]["addressLocality"]?>"}
				],
				"links.memberOf.<?=(String)$el["_id"]?>" : {$exists:true}
			}
		<?php } ?>
		
	<?php } ?>

	<?php if(!isset($el["ville"])){?>
		paramsFilter.filters.theme ={
			view : "megaMenuDropdown",
			type : "tags",
			remove0: false,
			name : "<?php echo Yii::t("common", "search by theme")?>",
			event : "tags",
			keyValue: true,
			list : thematic
		
		}
	<?php } ?>
	if(pageApp=="search"){
		paramsFilter.defaults.types =["events",  "organizations", "poi", "projects","classifieds","citoyens","ressources"];
		paramsFilter.filters.text =true;
		paramsFilter.filters.scope =true;
		paramsFilter.filters.types ={
				lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi","classifieds","ressources"]
		}
		
	}else if(pageApp=="organization"){
		paramsFilter.defaults.types =["organizations"];
		paramsFilter.filters.text =true;
		paramsFilter.filters.scope =true;
		paramsFilter.filters.types ={
			lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi","classifieds","ressources"]
		}
		
	} else if(pageApp=="project"){
		paramsFilter.defaults.types =["projects"];
		paramsFilter.filters.text =true;
		paramsFilter.filters.scope =true;
		
		
	} else if(pageApp=="annonce"){
		paramsFilter.defaults.types =["classifieds"];
		paramsFilter.filters.text =true;
		paramsFilter.filters.scope =true;
	}
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		mylog.log("filterSearchfilterSearch",paramsFilter)
	});
</script>