<?php 
$cssAnsScriptFilesTheme = array(

	'/plugins/jQCloud/dist/jqcloud.min.js',
	'/plugins/jQCloud/dist/jqcloud.min.css',

);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
$tagsPoiList = array();
if(!empty($allTags)){
	shuffle($allTags);
	foreach ($allTags as $key => $elem){
		$found = false;
		foreach ($tagsPoiList as $ix => $value) {
			$value["text"]; 
			if(	$value["text"] == $elem)
				$found = $ix;
		}
		if ( !$found )
			array_push($tagsPoiList,array(
				"text"=>$elem,
				"weight"=>1,
				"link"=>array(
					"href" => "javascript:;",
					"class" => "favElBtn ".InflectorHelper::slugify2($elem)."Btn",
					"data-tag" => InflectorHelper::slugify2($elem)
				)
			));
		else
			$tagsPoiList[$found]["weight"]++;
	}
}
?>
<style type="text/css">
	.observacity {
		background: #e2e2e2;
	}
	.stat-card {
		background: #fff; 
		box-shadow: 0 0 35px 0 #656565 ; 
		margin-bottom: 15px;
		border-radius: 10px;
	}
	.obsIcon {
		font-size: 34px;
		border-radius: 50%;
		padding: 15px;
		margin-top: 19px;
		margin-bottom: auto;
		color: #005E6F;
		background: #ddd;
	}
	.observacity h4 {
		font-size: 14px ; 
		color: #005E6F; 
		font-weight: bold ;
	}
	.counter {
		color: #8ABF32; 
		font-weight: bold ; 
	}
	.card-box {
		background-color: #fff ; 
		padding: 1.25rem ; 
		-webkit-box-shadow: 0 0 35px 0 rgba(73,80,87,.15) ; 
		box-shadow: 0 0 35px 0 rgba(73,80,87,.15) ; 
		margin-bottom: 24px ; 
		border-radius: 0.5rem; 
		margin: 1rem ;
	}
	.card-box h6{
		font-size: 14px ; 
		color: #3ea3b4; 
		font-weight: bold ;
		padding: 10px;
		text-align: center;
	}
	.observacityContainer {
		padding-left: 5%;
		padding-right: 5%;
	}
	
</style>
<div class="observacity">
	
	<div class="observacityContainer ">
		<div class="">
			<div class="">
				<article class="">
					<div class="col-md-12" style="margin-bottom: 40px; ">
						<div class="col-md-offset-2 col-md-8">
							<h1 class="" style="text-transform: none; text-align: center">OBSERVACITY </h1>
						</div>
					</div>
				</article>
			</div>
		</div>
		<div class="col-md-12 text-center">

			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<div class="col-md-5">
							<i class="fa fa-rocket obsIcon " style="">
							</i>
						</div>
						<div class="col-md-7">
							<div class=" card tilebox-one" >
								<h4 class=""  >Projets</h4>
								<h3 class="counter" data-plugin="counterup" ><?php echo count($projects) ?></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<div class="col-md-5">
							<i class="fa fa-users  obsIcon" >
							</i>
						</div>
						<div class="col-md-7">
							<div class=" card tilebox-one" >
								<h4 class=""  >Organisations</h4>
								<h3 class="counter" data-plugin="counterup" ><?php echo count($organization) ?></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<div class="col-md-5">
							<i class="fa fa-user obsIcon " >
							</i>
						</div>
						<div class="col-md-7">
							<div class=" card tilebox-one" >
								<h4 class=""  >Personnes</h4>
								<h3 class="counter" data-plugin="counterup" > <?php  echo count($citoyens) ?></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<div class="col-md-5">
							<i class="fa fa-calendar obsIcon" >
							</i>
						</div>
						<div class="col-md-7">
							<div class=" card tilebox-one" >
								<h4 class=""  >Evènement</h4>
								<h3 class="counter" data-plugin="counterup"><?php echo count($event) ?></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="col-lg-6 col-sm-12 col-md-12 col-xs-12 ">
			<div class="card-box card tilebox-one">
				<?php foreach ($blocks as $key => $value) {
					if(isset($value["graph"])){
						if($value["graph"]["key"] == "pieManyactivityFiliere"){
							?>
							<div>
								<h6 class=""  ><?php echo $value["title"]?> 
								</h6>
							</div>
							<div class="counterBlock" id="<?php echo $key?>" > </div> 
						<?php }
					}

				}; ?>
			</div>
		</div>
		<div class=" col-lg-6 col-sm-12 col-md-12 col-xs-12 ">
			<div class="card-box card tilebox-one">
				<?php foreach ($blocks as $key => $value) {
					if(isset($value["graph"])){
						if($value["graph"]["key"] == "pieManynombrecocity"){
							?>
							<div>
								<h6 class="" ><?php echo $value["title"]?> 
							</h6>
						</div>
						<div class="counterBlock" id="<?php echo $key?>" > </div> 
					<?php }
				}       
			}; ?>
		</div>
	</div>
	<div class="col-sm-12 canvasCocity"></div>
</div>

<script type="text/javascript">
	<?php  
	foreach ($blocks as $id => $d) {
		if( isset($d["graph"]) ) {
			?>
			var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
			mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
		<?php }
	} ?>
	var poiListTags = <?= json_encode($tagsPoiList)?>;
	var newListP =  [];

	$.each(poiListTags,function(kT, vT){
		var nElt = {
			text : vT.text,
			weight : vT.weight,
			link : vT.link,
			handlers: {
				click: function(e) {
					location.hash = "#search?tags="+e.target.dataset.tag;
					urlCtrl.loadByHash(location.hash);
				}
			},
		};
		newListP.push(nElt);
	});
	jQuery(document).ready( function() { 
		<?php  foreach ($blocks as $id => $d) {
			if( isset($d["graph"]) ) { ?>
				mylog.log('url graphs1',baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>");
				ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
			<?php }
		} ?>
		mylog.log(" poiListTags", poiListTags);

		mylog.log(" newListP", newListP);
		$('.canvasCocity').jQCloud(newListP, {
			height: 700,
			autoResize: true,
			shape: 'rectangular',
			colors: ["#8ABF32", "#1aa5b7", "#005E6F"],
			fontSize: { 
				from: 0.04,
				to: 0.01
			}
		})
	});

</script>