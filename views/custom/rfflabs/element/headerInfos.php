<style>
.section-badges{
	/*position : unset !important;
	margin-top:0px !important;
	margin-left: 10px !important;*/
	background-color: unset !important;
}

.menu-btn-follow{
	display:none !important;
}

.boxBtnLink{
	background-color: #FF286B;
	border:solid 1px #fff;
}

/*.boxBtnLink:hover{
	opacity:0.75;
}*/
.menu-linksBtn:hover{
	background-color: unset;
	opacity:unset;
}
.header-address-tags .badge {
    margin: 5px;
    width: fit-content;
    height: 20px;
    background: #ea4335;
    color: #fff;
}
.header-tags .badge {
    margin: 5px;
    width: fit-content;
    height: 20px;
    background: #fff;
}
@media (min-width: 768px) {
    .header-tags .badge {
        margin-left: 3px;
        font-size: 13px;
    }
}

.name-header{
	text-shadow: 1px 1px 1px black;
	color: var(--main2);
}
/*.social-main-container{
	color:#f8f8f8
}*/

</style>
<div class="btn-group section-badges pull-left no-padding" >


    <!-- Btn Link -->
    <div class="pull-left no-padding" style="">
        <?php
        if(@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]){ ?>
            <div class="pull-left boxBtnLink no-padding bg-dark-green" style="border-radius: 30px; display: flex;">
                <?php  $this->renderPartial('co2.views.element.menus.links',
                    array(  "linksBtn" => $linksBtn,
                        "element"   => $element,
                        "openEdition" => $openEdition )
                );
                ?>
            </div>
        <?php } ?>

    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">	
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 text-white pull-right margin-bottom-5">
			<?php if (@$element["status"] == "deletePending") { ?> 
				<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
			<?php } ?>
			<h4 class="text-left padding-left-15 pull-left no-margin">
				<span id="nameHeader">
					<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">
						
					</div>
					<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i> 
					<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
				</span>
				<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
						$typesList=Event::$types;
				?>
					<span id="typeHeader" class="margin-left-10 pull-left">
						<i class="fa fa-x fa-angle-right pull-left"></i>
						<div class="type-header pull-left">
					 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
					 	</div>
					</span>
				<?php } ?>
			</h4>	

			<!-- <?php if($edit){ ?>
				<button class="btn-danger text-white pull-right" style="border: none;padding: 5px 10px;border-radius: 5px;" onclick="directory.deleteElement('<?php echo $element["collection"] ?>', '<?php echo (string)$element["_id"] ?>');"><i class="fa fa-trash"></i> Supprimer</button>
			<?php } ?>	 -->			
		
	</div>



<?php 
	$classAddress = ( (@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden" );

	if(!empty($cteRParent))
		$classAddress = "";
//if(@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]){ ?>
	<div class="header-address-tags col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right margin-bottom-5 <?php echo $classAddress ; ?>">
		<?php
		 if(!empty($element["address"]["addressLocality"])){ ?>
			<div class="header-address badge letter-white bg-red margin-left-5 pull-left col-xl-12">
				<?php
					echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
					echo !empty($element["address"]["postalCode"]) ? 
							$element["address"]["postalCode"].", " : "";
					echo $element["address"]["addressLocality"];
				?>
			</div>
		<?php
		} ?>	

		<div class="header-tags pull-left  col-xs-12" style="margin-top: 10px;" >
		<?php 
		/*if(@$element["tags"]){
			foreach ($element["tags"] as $key => $tag) { */?><!--
				<a  href="#search?text=#<?php /*echo $tag; */?>"  class="badge letter-red bg-white lbh" style="vertical-align: top;">#<?php /*echo $tag; */?></a>
			--><?php /*}
			
		}*/ ?>

            <?php

            if(!empty($element["tags"])){
                $countTags=count($element["tags"]);
                $nbTags=1;
                foreach ($element["tags"]  as $key => $tag) {
                    if($nbTags < 5){
                        echo '<span class="badge tag-banner-element letter-red bg-white " style="margin-top: 3px;"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</span>';
                        $nbTags++;
                    }else{
                        if(($countTags-$nbTags) > 0) echo "<span class='badge indicate-more-tags letter-red bg-white'>+".($countTags-$nbTags)."</span>";
                        break;
                    }
                }
            } ?>
		</div>
	</div>

<?php 
	echo $this->renderPartial('co2.views.element.menus.answerInvite', 
    			array(  "invitedMe"      => $invitedMe,
    					"element"   => $element
    					) 
    			); 
 	?>
</div>

