<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	filters : {
	 		scope : {
	 			view : "scope",
	 			type : "scope",
	 			action : "scope"
	 		},
	 		typeEmplacement : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Type d'emplacement",
	 			action : "tags",
	 			list : costum.lists.typeEmplacement
	 		},
	 		accessibilite:{
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Accessibilité",
	 			action : "tags",
	 			list : costum.lists.accessibilite
	 		},
	 		visibilite : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Visibilité",
	 			action : "tags",
	 			list : costum.lists.visibilite
	 		},
	 		state : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Etat de l'affiche",
	 			action : "tags",
	 			list : costum.lists.state
	 		}
	 	}
	};
	 
	function lazyFilters(time){
	  if(typeof filterObj != "undefined" )
	    filterGroup = filterObj.init(paramsFilter);
	  else
	    setTimeout(function(){
	      lazyFilters(time+200)
	    }, time);
	}

	jQuery(document).ready(function() {
		lazyFilters(0);
	});
</script>