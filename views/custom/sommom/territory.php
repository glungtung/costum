<?php 
foreach ($answers as $key => $value) {
  $answers = $value;
}

$photoRes =  PHDB::find(Document::COLLECTION, array("id"=>(string)$answers["_id"]),array("name"));

foreach ($photoRes as $kp => $vp) {
    if(isset($vp['name'])){
      $photoName = Yii::app()->request->baseUrl."/upload/communecter/answers/".(string)$answers["_id"]."/".$vp['name'];
    }
}

if(count($photoRes) == 0){
    $photoName = Yii::app()->getModule('costum')->assetsUrl."/images/sommom/bandeau1.jpg";
}

// $data = {};

if(isset($answers["answers"]["sommomForm1"]["sommomForm12"])){
    foreach ($answers["answers"]["sommomForm1"]["sommomForm12"] as $s12k => $s12v) {


    }
}

$territoryName = '';
if(isset($answers["answers"]["sommomForm1"]["sommomForm120"])){
    $territoryName = $answers["answers"]["sommomForm1"]["sommomForm120"];
}

// var_dump($answers["answers"]["sommomForm1"]);
?>

<style type="text/css">
    :root {
     --primary: #7b56ff;
     --secondary: #ff6fb7;
     --tertiary: #a9e61c;
     --other: #00c9ec;
     --other2: #ffcd44;
     --white: #fff;
     --greyLight-1: #ebf2fc;
     --greyLight-2: #d5ddf3;
     --greyLight-3: #bec8e4;
     --greyDark: #5b657a;
  }
   *, *::before, *::after {
     margin: 0;
     padding: 0;
     box-sizing: inherit;
  }
   html {
     box-sizing: border-box;
     font-size: 62.5%;
     overflow-y: scroll;
     font-family: 'Poppins', sans-serif;
  }
   html h2 {
     font-weight: 300;
  }

  .message:after {
        content: '';
        position: absolute;
        margin: auto;
        right: 0;
        bottom: 0;
        left: 0;
        width: 50%;
        height: 2px;
        background-color: rgb(142, 191, 39);
  }

   #app {
     /*display: flex;*/
     /*justify-content: center;
     align-items: center;*/
     background: var(--greyLight-1);
  }
   .container-t {
     display: grid;
     /*grid-template-rows: 40% 60%;*/
     flex-direction: column;
     height: 59.7rem;
     /*width: 30rem;*/
     margin: 5rem 2rem;
     box-shadow: 0px 20px 40px rgba(91, 101, 122, 0.15);
     border-radius: 2rem;
     background: var(--white);
     color: var(--greyDark);
     letter-spacing: 0.04rem;
  }

   .container-r {
     /*display: grid;*/
     /*grid-template-rows: 40% 60%;*/
     flex-direction: column;
     height: 59.7rem;
     /*width: 30rem;*/
     margin: 5rem 2rem;
     border-radius: 2rem;
     color: var(--greyDark);
     letter-spacing: 0.04rem;
  }

 #mapterritoire {
    width: 100%;
    margin-top: 10px;
    height: 250px;
}

.slider {
  margin-top: 5rem;
  width: 100%;
  text-align: center;
  overflow: hidden;
}

.slides {
  display: flex;
  
  overflow-x: auto;
  scroll-snap-type: x mandatory;
  
  
  
  scroll-behavior: smooth;
  -webkit-overflow-scrolling: touch;
  
  /*
  scroll-snap-points-x: repeat(300px);
  scroll-snap-type: mandatory;
  */
}
.slides::-webkit-scrollbar {
  width: 10px;
  height: 10px;
}
.slides::-webkit-scrollbar-thumb {
  background: black;
  border-radius: 10px;
}
.slides::-webkit-scrollbar-track {
  background: transparent;
}
.slides > div {
  scroll-snap-align: start;
  flex-shrink: 0;

  margin-right: 50px;
  border-radius: 10px;
  background: #eee;
  transform-origin: center center;
  transform: scale(1);
  transition: transform 0.5s;
  position: relative;
  height: 59.7rem;
  width: 100%;
  box-shadow: 0px 20px 40px rgba(91, 101, 122, 0.15);
  border-radius: 2rem;
  background: var(--white);
  color: var(--greyDark);
  letter-spacing: 0.04rem;
/*  display: flex;
  justify-content: center;
  align-items: center;*/
}
.slides > div:target {
/*   transform: scale(0.8); */
}
.author-info {
  background: rgba(0, 0, 0, 0.75);
  color: white;
  padding: 0.75rem;
  text-align: center;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  margin: 0;
}
.author-info a {
  color: white;
}


.slider > a {
  display: inline-flex;
  width: 1.5rem;
  height: 1.5rem;
  background: white;
  text-decoration: none;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  margin: 0 0 0.5rem 0;
  position: relative;
}
.slider > a:active {
  top: 1px;
}
.slider > a:focus {
  background: #000;
}

/* Don't need button navigation */
@supports (scroll-snap-type) {
  .slider > a {
    display: none;
  }
}


.wrapper{
  margin: 10% auto;
  width: 400px;
}

.wrapper ul{
  list-style: none;
  margin: 0;
  padding: 0;
}

.wrapper label{
  display: block;
  cursor: pointer;
  padding: 10px;
  border: 1px solid #fff;
  border-bottom: none;
}

.wrapper label:hover{
  background: #26C281;
}

.wrapper label.last{
  border-bottom: 1px solid #fff;
}

.wrapper ul ul li{
  padding: 10px;
  background: #59ABE3;
}


.wrapper input[type="checkbox"]{
  position: absolute;
  left: -9999px;
}

.wrapper input[type="checkbox"] ~ ul{
  height: 0;
  transform: scaleY(0);
}

.wrapper input[type="checkbox"]:checked ~ ul{
  height: 100%;
  transform-origin: top;
  transition: transform .2s ease-out;
  transform: scaleY(1); 
}

.wrapper input[type="checkbox"]:checked + label{
  background: #26C281;
  border-bottom: 1px solid #fff;
}


</style>

<div class="container-fluid" id="app">
  <div class="row">
      <div class="container-r container col-md-3 col-sm-12">
          <div class="">
              <span> <h3> <?php echo $territoryName ?> </h3> </span>
          </div>
          <div class="">
                  <img style="width: 100%;" src="<?php echo $photoName; ?>">
          </div>
          <div id="mapterritoire">
            
          </div>
      </div>

      <div class=" container col-md-8 col-sm-12">
          <div class="slider">
              <!-- <a href="#slide-1">1</a>
              <a href="#slide-2">2</a>
              <a href="#slide-3">3</a>
              <a href="#slide-4">4</a>
              <a href="#slide-5">5</a> -->
              <div class="slides">
<?php
if(isset($answers["answers"]["sommomForm1"]["sommomForm12"])){
    foreach ($answers["answers"]["sommomForm1"]["sommomForm12"] as $s12k => $s12v) {
?>              
                <div id="slide-<?php echo $s12k?>">
                  <div class="col-md-12">
                      <h4 style="color: black">Zone d'observation</h4>
                  </div>
                  <div class="col-md-12">
                      <h3><?php echo $s12v["localisation"];?></h3>
                  </div>
                  <div class="col-md-12">
                  </div>
                  <div class="col-md-6 ">
                    <div class="col-md-12 ">
                      <h4 class = "message" style="color: black">Les espèces</h4>
                    </div>
                      <div class="wrapper">
                        <ul>
                          <?php 
                    if(isset($answers["answers"]["sommomForm1"]["sommomForm13"])){
                        foreach ($answers["answers"]["sommomForm1"]["sommomForm13"] as $s13k => $s13v) {
                          ?>
                          <li>
                            <input type="checkbox" id="list-item-<?php echo $s13k ?>">
                            <label for="list-item-<?php echo $s13k ?>" class="first"><?php echo $s13v["especeRec"] ?></label>
                            <ul>
                              <li></li>
                            </ul>
                          </li>
                    <?php
                        }
                      }
                    ?>
                        </ul>
                        </div>
                  </div>


                    


                </div>
<?php
    }
}
?>                
              </div>
        </div>
        
      </div>
  </div>
</div>

<script type="text/javascript">
  setTitle("SOMMOM : Territoires");

    paramsMapAlternatiba = {
      zoom : 4,
      container : "mapterritoire",
      tile : "maptiler",
      mapOpt:{
        latLon : ["-21.115141", "55.536384"],
      }
    };

    var zone = [];

    var contextDataobszone = {
          name : "fdhfdb",
          type: "poi",
          geo: { "type": "GeoCoordinates", "latitude" : "-21.115141", "longitude" : "55.536384"}

    };

    zone.push(contextDataobszone);

    mapAlternatibaHome = mapObj.init(paramsMapAlternatiba);

    mapAlternatibaHome.addElts(zone, true);

    setTimeout(function(){
          mapAlternatibaHome.map.panTo([-21.115141,55.536384]);
          mapAlternatibaHome.map.setZoom(10);
    },2000);

    $("#btn-nextcloud").remove();
    <?php if(isset($_SESSION["costum"][$this->costum["contextSlug"]]["isMember"]) && $_SESSION["costum"][$this->costum["contextSlug"]]["isMember"]==true){ ?>
        $("#menuLeft a:eq(3)").after(`
            <a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank"> 
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                <span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
            </a>
        `);
    <?php } ?>
</script>
