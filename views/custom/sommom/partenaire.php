
<style type="text/css">


.content {
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  max-width: 500px;
  /*padding: 35vh 20px 20px 20px*/
}

.logo {
  max-width: 300px;
}

p {
  /*font-family: sans-serif;*/
  text-align: center;
}

.message {
  margin-top: 40px;
  font-size: 30px;
  position: relative;
}

.message:after {
        content: '';
        position: absolute;
        margin: auto;
        right: 0;
        bottom: 0;
        left: 0;
        width: 50%;
        height: 2px;
        background-color: rgb(142, 191, 39);
      }

.message-text {
    margin: 10px;
    padding: 10px;
}

.message-text .text {
    font-weight: bold;
    text-align: center;
    font-size: 20px;
}

.btn-start {
    background-color: transparent;
    border: 2px solid #7d98c7;
    color: #1e56a2;
    border-bottom-left-radius: 10px;
    font-weight: bold;
}

.btn-start:hover {
    background-color: #7d98c7;
    border: 2px solid #7d98c7;
    color: #fff;
    border-bottom-left-radius: 10px;
    font-weight: bold;
}

.s-center {
    margin: 0px;
    text-align: center;
    width: 100%;
    font-size: 40px;
    margin-bottom: 10px;
}


</style>

<?php
    $adminStatus = false;
    $membre = false;
    $title = "Devenir partenaire";

    if (isset($this->costum["admins"])) {
        if(is_array($this->costum["admins"])){
            foreach ($this->costum["admins"] as $key => $value) {
                if($key == Yii::app()->session["userId"]){
                    $adminStatus = true;
                }

            }
        }
    }

    if(isset(Yii::app()->session["userId"]) and !$adminStatus and !$partenaire){
        $membre = true;
    }

    $message = "Devenir partenaire";
    $submessage = "";
    $btnAction = '<button class="btn btn-start b-partenaire">participer au projet</button>';

    if ($adminStatus or $partenaire) {
        $message = "Vous êtes déjà un partenaire";
        $submessage = "";
        $btnAction = '<button class="btn btn-start b-partenaire">participer au projet</button>';
        $btnAction = "";
    } elseif ($membre and !$adminStatus and !$partenaire) {
        $message = "Devenir partenaire";
        $submessage = "";
    } elseif (!$membre and !$adminStatus and !$partenaire) {
        $submessage = "Vous devez d'abord être membre";
        $btnAction = '<button class="btn btn-start lbh" href="#membre" >s\'inscrire</button>';
        // $btnAction = '<button class="btn btn-start b-partenaire">participer au projet</button>';
    }


?>

<!-- <div class="object">
    <div class="object-rope"></div>
    <div class="object-shape">
    	Bientôt <span class="soon">disponible</span>
    </div>
</div> -->

<div class="content">
  <img class="logo" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/cdv-cedtm-blanc.png"/>
  
  <p class="message"><?php echo $message ?></p>

  <p class="submessage" style="color : grey;"><?php echo $submessage ?></p>

  <?php echo $btnAction ?>
  
</div>

<div class="col-md-6">
    <div class="message-text">
      <div class="col-md-12"><i class="fa fa-file-text s-center" style="color: burlywood"></i></div>
      <div class="text col-md-12">Devenir Partenaire vous permet de participer au projet « SOMMOM », dont l’objet est la mise en réseau des acteurs de l’activité d’observation des cétacés des différents territoires français.</div>
    </div>
</div>

<div class="col-md-6">
    <div class="message-text">
      <div class="col-md-12"><i class="fa fa-envelope-o s-center" style="color: darkgrey"></i></div>
      <div class="text col-md-12"> Vous pourrez préciser votre demande via le formulaire. Cette dernière sera envoyée aux administrateurs de la plateforme qui pourront y donner suite.</div>
    </div>
</div>

<script type="text/javascript">
  setTitle("SOMMOM : Partenaires");

    tplCtx = {};

    territoireList = <?php echo json_encode($territoire); ?>;

sectD = {
  "jsonSchema":{
      "title":"Devenir partenaire",
      "icon":"fa-globe",
      "text":"",
      "properties":{
          "nom":{
              "label":"Nom",
              "placeholder":"Nom",
              "inputType":"text",
              "rules":{"required":true}
          },
          "prenom":{
              "label":"Prénom",
              "placeholder":"Prénom",
              "inputType":"text",
              "rules":{"required":true}
          },
          "localite":{
              "label":"Localité",
              "placeholder":"Localité",
              "inputType":"text",
              "rules":{"required":true}
          },
          "structure":{
              "label":"Structure",
              "placeholder":"Structure",
              "inputType":"text",
              "rules":{"required":true}
          },
          "contact":{
              "label":"Contact email",
              "placeholder":"Contact email",
              "inputType":"text",
              "rules":{"required":true}
          },
          "title":{
              "inputType":"custom",
              "html" : "<h5> Objet de la demande </h5>"
          },
          "objet1":{
                "placeholder":"Intéressé(e) pour contribuer au projet « SOMMOM »",
                "inputType":"checkbox",
                "rules":{"required":false}
          },
          "objet2":{
                "placeholder":"Intéressé(e) pour contribuer au renseignement d’informations sur un territoire",
                "inputType":"checkbox",
                "rules":{"required":false}
          },
          "for":{
              "inputType":"custom",
              "html" : "<p> pour le territoire : </p>"
          },
          "territoire":{
                "inputType":"selectMultiple",
                "rules":{"required":false},
                "options" : territoireList
          },
          "nb":{
              "inputType":"custom",
              "html" : "<span style = 'color : #797777' > Maintenir la touche <span style = 'background-color : #cdcdcd; color : black;' > Ctrl </span> pour sélectionner plusieurs territoires </b>"
          },
          "autre":{
              "placeholder":"Autre objet",
              "inputType":"text",
              "rules":{"required":false}
          }
      },
      save : function () {

          var objectChecked = "";

          if($("#objet1").is(":checked")){
              objectChecked += "Intéressé(e) pour contribuer au projet « SOMMOM » <br>";
          };
          if ($("#objet2").is(":checked")){
              objectChecked += "Intéressé(e) pour contribuer au renseignement d’informations sur un territoire <br>";
          }; 
          if ($("#autre").val() != ""){
              objectChecked += $("#autre").val() + "<br>";
          };


          
          
          var msg='<div class="content">'
                      +'<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">'
                          +'<tbody>'
                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Nom'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">'
                                      +$("#nom").val()
                                  +'</th>'
                              +'</tr>'


                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'

                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Prénom'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">'
                                      +$("#prenom").val()
                                  +'</th>'
                              +'</tr>'


                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Localité'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">'
                                      +$("#localite").val()
                                  +'</th>'
                              +'</tr>'

                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Structure'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">'
                                      +$("#structure").val()
                                  +'</th>'
                              +'</tr>'

                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Contact'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">'
                                      +$("#contact").val()
                                  +'</th>'
                              +'</tr>'

                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Objet'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px; color: #e33551">'
                                      +objectChecked
                                  +'</th>'
                              +'</tr>'

                              +'<tr style="padding: 0;vertical-align: top;text-align: left;">'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: right;line-height: 19px;font-size: 15px;font-weight: bold; width: 100px;">'
                                      +'Territoire'
                                  +'</th>'
                                  +'<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding : 10px;margin: 0;text-align: left;line-height: 19px;font-size: 15px; color: #3ea3b4">'
                                      +$("#territoire").val().toString()
                                  +'</th>'
                              +'</tr>'

                          +'</tbody>'
                      +'</table>'
                    +'</div>';

          var params={
            tpl : "basic",
            tplObject : "Nouveau partenaire : "+$("#nom").val()+" "+$("#prenom").val(),
            html: msg
          };
          ajaxPost(
                null,
                baseUrl+"/co2/mailmanagement/createandsend",
                params,
                function(data){ 
                    toastr.success("Votre demande a été envoyée aux administrateurs. Vous recevrez une réponse sous peu.");
                }
          ); 
          dyFObj.closeForm();           
      }
  }
}

jQuery(document).ready(function() {
  $(".b-partenaire").off().on("click",function() {
      dyFObj.openForm( sectD );
  });

<?php 

  if(isset($_SESSION["costum"][$this->costum["contextSlug"]]["isMember"]) || $_SESSION["costum"][$this->costum["contextSlug"]]["isAdmin"]){
 ?>
    if($("#btn-nextcloud").length == 0) {
      $("#menuLeft a:eq(3)").after(`
          <a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank"> 
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
              <span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
          </a>
      `);
    }

<?php } ?>
});
</script>