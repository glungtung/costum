<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/jquery-counterUp/waypoints.min.js",
		"/plugins/jquery-counterUp/jquery.counterup.min.js",
		); 
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style type="text/css">

h6 {
	font-size: 1rem !important;
	font-weight: 500 !important;

}

h3 {
	font-size: 2.5rem !important;
	font-weight: 600;
}

h1 {
	font-size: 2em;
}

.card-box {
	background-color: #fff !important;
	padding: 1.25rem !important;;
	-webkit-box-shadow: 0 0 35px 0 rgba(73,80,87,.15) !important;;
	box-shadow: 0 0 35px 0 rgba(73,80,87,.15) !important;;
	margin-bottom: 24px !important;;
	border-radius: 0.5rem !important;;
	margin: 1rem !important;;
}

.card-box > .icon {
	float: right;
}

.card-box h3, h6 {
	margin: 0;
}

h6 {
	font-size: 1rem;
	font-weight: 500;
}
h3 {
	font-size: 2.5rem;
	font-weight: 600;
}

.header > h1, p {
	margin: 0;
}

.header > h1 {
	font-weight: 600;
}

@media (max-width:1000px)  {
	.stat-card {
		width: 100% !important;
	}

	header {
		height: 150px;
		clear: both;
	}

	.select2-selection__choice__remove {
		display: none !important;
	}

	.mybtngroup {
		display: none;
	}

	.contbntngroup:hover .mybtngroup {
		display: inline-block;
	}
}
</style>

<?php 

	$ters = ""; // Territory string list (@christon)

	$nombreport = 0;
	$nombrenavire = 0;

	$zone = 0;
	$accord = 0;
	$plan = 0;
	$equipe = 0;

	$reglementation = 0;
	$code = 0;
	$label = 0;

	$espece = 0;
	$acteur = 0;

	$obmaritime = 0;
	$obterrestre = 0;
	$obaerienne = 0;
	$adminStatus = false;


if (isset($this->costum["admins"])) {
	if(is_array($this->costum["admins"])){
		foreach ($this->costum["admins"] as $key => $value) {
			if($key == Yii::app()->session["userId"]){
				$adminStatus = true;
			}
		}
	}
}

if(isset($gr[4])){
	$nombreport = count($gr[4]);
	$zone = count($gr[4]);
}

if(isset($gr[5])){
	$plan = count($gr[5]);
}

if(isset($gr[6])){
	$reglementation = count($gr[6]);
}

if(isset($gr[7])){
	$label = count($gr[7]);
}

if(isset($gr[16])){
	$accord = count($gr[16]);
}

if(isset($gr[17])){
	$equipe = count($gr[17]);
}

if(isset($gr[18])){
	$code = count($gr[18]);
}

if(isset($gr[8])){
	$espece = count($gr[8]);
}

if(isset($gr[9])){
	foreach ($gr[9] as $key => $value) {
		if(isset($value["nbrNav"])){
			if (ctype_digit($value["nbrNav"])) {
				$nombrenavire = $nombrenavire + intval($value["nbrNav"]);
			}
		}
	}
}

if(isset($gr[3])){
	$acteur = count($gr[3]);
}

foreach ($blocks as $key => $value) {
	if(isset($value["id"])){
		if($value["id"] == 1){
			if(isset($value["data"][6]["data"])){
				$nombreport = $value["data"][6]["data"];
			}
		}
	}
}; 

// make teritoires on headers
if(isset($gr[15])){
	for($i = 0; $i < count($gr[15]); $i++){ 
		if($gr[15][$i]!=""){
			$ters = $ters."".$gr[15][$i].", ";
		}
	}
}

?>

<?php if($aj == "false") { ?>

<div class="">
	<div class="">
		<article class="">
			<div class="col-md-12" style="margin-bottom: 40px; ">
				<div class="col-md-offset-2 col-md-8">
					<h1 class="" style="text-transform: none; text-align: center">L’activité d’observation des cétacés en France et dans les territoires français d’outre-mer </h1>
				</div>
					
				<div class="col-md-6"></div>
			</div>
			
			<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
			
			<div class="col-md-12" style="margin-bottom: 10px; " >
				<div class="container contbntngroup" >
					<div class="btn-group mybtngroup" role="group" aria-label="...">
						<button id="refresh" class="btn "><i class="fa fa-refresh"></i></button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret">
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="javascript:" id="selectall">Sélectionner tout</a></li>
								<li class="divider"></li>
								<li><a href="javascript:" id="unselectall">Déselectionner tout</a></li>
							</ul>
						</div>
					</div>
					<select id="allterri" multiple>
						<option disabled></option>
					<?php for($i = 0; $i < count($gr[15]); $i++){ ?>
						<option value="<?php echo $i ?>">
						<?php if(isset($gr[15][$i])){
								if($gr[15][$i] == ""){
									echo "non défini"; 
								}else{ 
									echo $gr[15][$i];
								}
							} ?>	
						</option>
					<?php } ?>
					</select>
				</div>
			</div>

			<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

			<div id="ajaxgraphcontent" >
<?php } 

if($aj == "true"){ ?>
				<div class="col-md-12">
					<div class="col-md-6">
						<div class=" col-md-12 stat-card">
							<div class="card-box card tilebox-one" >
								<span class="pull-right">
									<i class="fa fa-anchor" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px; "></i>
								</span>
								<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important; " >Ports de départ</h6>
								<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; color: #74b976"><?php echo $nombreport ?></h3>
								<div class="">
									<span class=""></span>
									<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
								</div>
							</div>
						</div>
						<div class=" col-md-12 stat-card">
							<div class="card-box card tilebox-one" >
								<span class="pull-right">
									<i class="fa fa-ship" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px;"></i>
								</span>
								<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" >	Navires pratiquant l’observation</h6>
								<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; color: #74b976"><?php echo $nombrenavire; ?></h3>
								<div class="">
									<span class=""></span>
									<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class=" col-md-12 stat-card">
							<div class="card-box card tilebox-one">
							<?php foreach ($blocks as $key => $value) {
								if(isset($value["graph"])){
									if($value["graph"]["key"] == "pieManyperiodeactivite"){
											?>
										<div><h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6>
										</div>
										<div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
							<?php //if( isset($d["html"]) ) 
									//echo $d["html"];?>
							</div> 
							<?php
								}
							}
						}; ?>
							<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
						</div>
					</div>
				</div>	
			</div>

			<div class="col-md-12">
				<div class=" col-md-6">
					<div class=" col-md-12 stat-card">
						<div class="card-box card tilebox-one">
							<?php foreach ($blocks as $key => $value) {
								if(isset($value["graph"])){
									if($value["graph"]["key"] == "pieManyespeceobserve"){
													?>
										<div><h6 class="text-center" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6></div>
										 
										<h3 class="text-center" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important;"><?php echo $espece; ?></h3>
																	

										<div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
							<?php //if( isset($d["html"]) ) 
								//echo $d["html"];?>
													
						</div> 
						<?php	}
							}
						}; ?>
						<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
					</div>
				</div>									
			</div>
							
			<div class=" col-md-6">
				<div class=" col-md-12 stat-card">
					<div class="card-box card tilebox-one">
						<?php foreach ($blocks as $key => $value) {
							if(isset($value["graph"])){
								if($value["graph"]["key"] == "pieManyacteurimpl"){
								?>
							<div><h6 class="text-center" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6></div>
										 
							<h3 class="text-center" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important;"><?php echo $acteur; ?></h3>
							<div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
						<?php //if( isset($d["html"]) ) 
							//echo $d["html"];?>
													
							</div> 
						<?php							 
							}
						}
					}; ?>

					<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class=" col-md-6">
			<div class=" col-md-12 stat-card">
				<div class="card-box card tilebox-one">
					<?php foreach ($blocks as $key => $value) {
						if(isset($value["graph"])){
							if($value["graph"]["key"] == "pieManyopm"){
						?>
		
						<div><h6 class="text-center" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6></div>
										

						<div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
									<?php //if( isset($d["html"]) ) 
											//echo $d["html"];?>
						
						</div> 
		<?php
				 
					}
				}
					
				}; ?>
</div>
</div>
				
</div>

<div class="col-md-6">
		<div class=" col-md-12 stat-card">
				<div class="card-box card tilebox-one" style="min-height: 120px">

						<div class="col-md-3 col-xs-3 col-sm-3 text-center">
							<div>
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" >Zone(s) protégée(s)</h6><br>
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $zone ?></h3>
						 
						</div>
					</div>

						<div class="col-md-3 col-xs-3 col-sm-3 text-center">
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" >Accord(s) / réseau(x)</h6><br>
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $accord ?></h3>
						<div></div>
						</div>

						<div class="col-md-3 col-xs-3 col-sm-3 text-center">
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" >Plan(s) de conservation</h6><br>
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $plan ?></h3>
						<div></div>
						</div>

						<div class="col-md-3 col-xs-3 col-sm-3 text-center">
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" >Equipe(s) dédiée en mer</h6>
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $equipe ?></h3>
						<div></div>
						</div>
						

				</div>
				<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
		</div>

		 <div class=" col-md-12 stat-card">
				<div class="card-box card tilebox-one" style="min-height: 120px"> 
						
						<div class="col-md-4 text-center">
						
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" > Réglementation(s) de la pratique</h6>
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $reglementation ?></h3>
						
							
						</div><div class="col-md-4 text-center">
						
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" > Code de conduite</h6> 
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $code ?></h3>
						
							
						</div><div class="col-md-4 text-center">
						
						<h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" > Label / certification</h6>
						<h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; bottom: 20px; color: #74b976"><?php echo $label ?></h3>
						
							
						</div>
				</div>
				<span class="" style="color: rgba(0,0,0,.4);">Données sur <?php echo $nbgraph ?> territoire(s)</span>
		</div>

</div>
</div>

<div class="col-md-12">
<div class=" col-md-6">
		<div class=" col-md-12 stat-card">
				<div class="card-box card tilebox-one">
						<?php foreach ($blocks as $key => $value) {
									if(isset($value["graph"])){
											if($value["graph"]["key"] == "pieManyembarqueopm"){
						?>
		
				<div><h6 class="text-center" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6></div>
								

				<div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
							<?php //if( isset($d["html"]) ) 
									//echo $d["html"];?>
				
				</div> 
<?php
		 
			}
		}
			
		}; ?>
</div>
</div>
		
</div>

<div class=" col-md-6">
<div class=" col-md-12 stat-card">
		<div class="card-box card tilebox-one">
				<?php foreach ($blocks as $key => $value) {
							if(isset($value["graph"])){
									if($value["graph"]["key"] == "pieManynageavecopm"){
				?>

				<div><h6 class="text-center" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;" ><?php echo $value["title"]?> </h6></div>
								

				<div class="counterBlock <?php //echo $borderClass; ?>" id="<?php echo $key?>" >
							<?php //if( isset($d["html"]) ) 
									//echo $d["html"];?>
				
				</div> 
<?php
		 
			}
		}
			
		}; ?>
</div>
</div>
		
</div>
</div>





<!-- <div class="col-md-12">
<div class="col-md-6">
<div class=" col-md-12 stat-card">
			<div class="card-box card tilebox-one">
					<span class="pull-right">
						<i class="fa fa-anchor" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px;"></i>
					</span>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $zone ?></span> Zone(s) protégée(s)</h6>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $accord ?></span> Accord(s) / réseau(x)</h6>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $plan ?></span> Plan(s) de conservation</h6>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $equipe ?></span> Equipe(s) dédiée en mer</h6>
						
			</div>
	</div>
</div>

<div class="col-md-6">
<div class=" col-md-12 stat-card">
			<div class="card-box card tilebox-one">
					<span class="pull-right">
						<i class="fa fa-anchor" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px;"></i>
					</span>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $reglementation ?></span> Réglementation(s) de la pratique</h6>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $code ?></span> Code de conduite</h6>
					<h6 class="" style="font-size: 14px !important;" ><span style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important;"><?php echo $label ?></span> Label / certification</h6>
					
						
			</div>
</div>
</div>

</div> -->
<?php } 
if($aj == "false"){

?>
</div>
			</article>

			
	</div>
</div>

<?php } ?>

<script type="text/javascript">

//mapCO.activePreview = true;
//alert("/modules/costum/views/custom/ctenat/dashboard.php")
//prepare global graph variable needed to build generic graphs
<?php  
foreach ($blocks as $id => $d) {
		if( isset($d["graph"]) ) {
				?>
				var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
				mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
<?php }
} ?>


jQuery(document).ready(function() {

	$(function () {
		setTitle("SOMMOM - Activité de suivi et amélioration des conditions d'observation des dauphins et baleines  à "+"<?php echo $ters; ?>");

		//$('meta[name=description]').attr('content', "Activité de suivi et amélioration des conditions d'observation des cétacés (dauphins et baleines) et des tortues marines au <?php echo $ters; ?>");
		$("header").append("<meta name='robots' content='index, follow'>");
	});

<?php if($aj == "false"){ 
		if ($ter == "") { ?>
		ajaxPost("ajaxgraphcontent", baseUrl+"/costum/sommom/graph",
			{
				"tpl":"costum.views.custom.sommom.graph",
				"aj":"true",
				"territoires": "[]",
				"firstkey":"<?php echo $firstkey; ?>"
			},
			function(data){
				$('#ajaxgraphcontent').html(data);
			},"html");
	<?php 
		} else {
	?>
	
		ajaxPost(null,baseUrl+'/costum/sommom/graph',
			{
				"tpl":"costum.views.custom.sommom.graph",
				"aj":"true",
				"territoires": '["<?php echo $ter ?>"]',
				"firstkey":"<?php echo $firstkey; ?>"
			},
			function(data){
				$('#ajaxgraphcontent').html(data);
				$('#allterri').val(<?php echo $ter ?>).trigger('change');
				
				var selectedTers = "";
				if(Array.isArray($('#allterri').val())){
					$("#allterri option:selected").each(function () {
						var $this = $(this);
						if ($this.length) {
							selectedTers = selectedTers+$this.text();
						}
					});
				}else{
					selectedTers = $("#allterri option:selected").text();
				}
				
				setTitle("SOMMOM - Activité de suivi et amélioration des conditions d'observation des dauphins et baleines  à "+selectedTers);
			},"html");

			//$('meta[name=description]').attr('content', "Activité de suivi et amélioration des conditions d'observation des cétacés (dauphins et baleines) et des tortues marines "+selectedTers);
			
			<?php 
				} 
			?>
	 	<?php } ?>

		$('#refresh').off().click(function(){
			ajaxPost("ajaxgraphcontent",baseUrl+"/costum/sommom/graph",
				{
				"tpl":"costum.views.custom.sommom.graph",
				"aj":"true",
				"territoires": JSON.stringify($("#allterri").val()),
				"firstkey":"<?php echo $firstkey; ?>"
				}, 
				function(data){
				$('#ajaxgraphcontent').html(data);
			},"html");
		});
		
					
		mylog.log("render graph","/modules/costum/views/custom/ctenat/dashboard.php");

		<?php  foreach ($blocks as $id => $d) {
				if( isset($d["graph"]) ) { ?>
					mylog.log('url graphs',' <?php echo $d["graph"]["url"]?>');
					ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
		<?php }
		} ?>
	});

	$(document).ready(function() {
		let branch_all = [];
		
		function formatResult(state) {
				if(!state.id) {
					var btn = $('<div class="text-right"><button id="all-branch" style="margin-right: 10px;" class="btn btn-default">Select All</button><button id="clear-branch" class="btn btn-default">Clear All</button></div>')
						return btn;
				}
				
				branch_all.push(state.id);
				var id = 'state' + state.id;
				var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
				return checkbox;   
		}
		
		function arr_diff(a1, a2) {
			var a = [], diff = [];
			for (var i = 0; i < a1.length; i++) {
				a[a1[i]] = true;
			}
			for (var i = 0; i < a2.length; i++) {
				if (a[a2[i]]) {
					delete a[a2[i]];
				} else {
					a[a2[i]] = true;
				}
			}
			for (var k in a) {
				diff.push(k);
			}
			return diff;
		}
		
		let optionSelect2 = {
				templateResult: formatResult,
				closeOnSelect: false,
				width: '80%',
				placeholder: 'Sélectionner un ou plusieurs territoires'
		};
		
		let $select2 = $("#allterri").select2(optionSelect2);
		
		var scrollTop;
		
		$select2.on("select2:selecting", function( event ){
				var $pr = $( '#'+event.params.args.data._resultId ).parent();
				scrollTop = $pr.prop('scrollTop');
		});
		
		$select2.on("select2:select", function( event ){
				$(window).scroll();
				
				var $pr = $( '#'+event.params.data._resultId ).parent();
				$pr.prop('scrollTop', scrollTop );
				
				$(this).val().map(function(index) {
						$("#state"+index).prop('checked', true);
				});
		});
		
		$select2.on("select2:unselecting", function ( event ) {
				var $pr = $( '#'+event.params.args.data._resultId ).parent();
				scrollTop = $pr.prop('scrollTop');
		});
		
		$select2.on("select2:unselect", function ( event ) {
				$(window).scroll();
				
				var $pr = $( '#'+event.params.data._resultId ).parent();
				$pr.prop('scrollTop', scrollTop );
				
				var branch  = $(this).val() ? $(this).val() : [];
				var branch_diff = arr_diff(branch_all, branch);
				branch_diff.map(function(index) {
						$("#state"+index).prop('checked', false);
				});
		});
		
		$(document).on("click", "#all-branch",function(){
				$("#allterri > option").not(':first').prop("selected", true);// Select All Options
				$("#allterri").trigger("change")
				$(".select2-results__option").not(':first').attr("aria-selected", true);
				$("[id^=state]").prop("checked", true);
				$(window).scroll();
		});
		
		$(document).on("click", "#clear-branch", function(){
			$("#allterri > option").not(':first').prop("selected", false);
			$("#allterri").trigger("change");
				$(".select2-results__option").not(':first').attr("aria-selected", false);
				$("[id^=state]").prop("checked", false);
				$(window).scroll();
		});

});

</script>

 <script type="text/javascript">

	$('#selectall').click(function() {
		$("#allterri > option").prop("selected","selected");
				$("#allterri").trigger("change");
	});

	$('#unselectall').click(function() {
		 $("#allterri > option").removeAttr("selected");
			$("#allterri").trigger("change");
	});
		
<?php 
	if(isset($_SESSION["costum"][$this->costum["contextSlug"]]["isMember"]) || $_SESSION["costum"][$this->costum["contextSlug"]]["isAdmin"]){
	
?>
	if($("#btn-nextcloud").length == 0) {
		$("#menuLeft a:eq(3)").after(`
			<a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank"> 
					<i class="fa fa-file-text-o" aria-hidden="true"></i>
					<span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
			</a>
		`);
	}

<?php } ?>
</script>
