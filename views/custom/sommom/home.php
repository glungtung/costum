<div id="homeSommom" class="col-xs-12">
        <div style="width: 100%; position: fixed;" id="loaderSommom">
            
        </div>
</div>
<?php 

$adminStatus = false;

if (isset($this->costum["admins"])) {
    if(is_array($this->costum["admins"])){
        foreach ($this->costum["admins"] as $key => $value) {
            if($key == Yii::app()->session["userId"]){
                $adminStatus = true;
            }
        }
    }
}

?>

<script type="text/javascript">
    //to edit costum page pieces
    jQuery(document).ready(function() {
        coInterface.showLoader("#loaderSommom");
    });

    <?php if($adminStatus){ ?>
        if($("#btn-nextcloud").length == 0) {
            $("#menuLeft a:eq(3)").after(`
                <a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank"> 
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    <span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
                </a>
            `);
        }
    <?php } ?>


    if ( location.hash == "#" || location.hash == "#welcome" || location.hash == "" || location.hash == "#email-feedback") {
    	// window.location.replace(baseUrl+"/costum/co/index/slug/sommom#dashboard");
        <?php if(isset($_SESSION["userId"])){ ?>
            if(location.hash == "#email-feedback"){
                bootbox.dialog({message:'<div class="alert-white text-center"><br><strong><?php echo Yii::t("common","Vous confirmer de ne plus vouloir recevoir de mail venant de sommom ? En acceptant, vous ne recevrez plus de mail adressé au membre.") ?></strong><br><br><button id="denyEmail" class="btn btn-info" data-id="<?php echo $_SESSION["userId"]; ?>" data-collection="citoyens">JE CONFIRME</button></div>'});

                $("#denyEmail").on("click", function(){
                    tplCtx.id = $(this).data("id");
                    tplCtx.collection = $(this).data("collection");
                    tplCtx.path = "links.projects.<?= $this->costum["contextId"] ?>.receiveMail";
                    tplCtx.value = {"status": false};

                    dataHelper.path2Value( tplCtx, function(params){
                        toastr.success("Votre préférence a été enregistré, Merci !");
                        $(".bootbox-close-button").click();
                    });
                });
            }
        <?php }else{ ?>
            if(location.hash == "#email-feedback"){
                bootbox.dialog({message:'<div class="alert-white text-center"><br><strong>Veuillez vous connecter et puis reéssayer la désactivation de la reception de mail.</div>'});
            }
        <?php } ?>

        urlCtrl.loadByHash("#dashboard");
    }
</script>
