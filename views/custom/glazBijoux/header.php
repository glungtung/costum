<style type="text/css">
  @font-face {
    font-family: "ReenieBeanie";
    src: url("<?php  echo $assetsUrl?>/font/glazBijoux/ReenieBeanie-Regular.ttf");
  }
  @font-face {
    font-family: "Oswald";
    src: url("<?php  echo $assetsUrl?>/font/glazBijoux/Oswald-VariableFont_wght.ttf");
  }
  @font-face {
    font-family: "JosefinSansRegular";
    src: url("<?php  echo $assetsUrl?>/font/glazBijoux/JosefinSans-Regular.ttf");
  }
  @font-face {
    font-family: "JosefinSansLight";
    src: url("<?php  echo $assetsUrl?>/font/glazBijoux/JosefinSans-Light.ttf");
  }
  .ReenieBeanie{
    font-family: "ReenieBeanie";
  }
  .Oswald{
    font-family: Oswald;
  }
  .JosefinSansRegular{
    font-family: JosefinSansRegular;
  }
  .JosefinSansLight{
    font-family: JosefinSansLight;
  }
  
.fc-center h2{
	font-family: "Oswald" !important;
	color: #565656 !important;
}
.searchEntity .letter-green{
	color: #008037 !important;
}


</style>