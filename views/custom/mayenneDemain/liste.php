<style type="text/css">
        .text-explain{
            font-size: 22px;
        }
        .img-oval {
    border-radius: 35%;
        height: 165px;
        }
        .titre-img {
            text-transform: inherit!important;
            color: #5b2649;
            font-size: 22px!important;
        }
        /*.espace-profile {
          padding: 2em 3em;
            min-width: fit-content;
        }*/
    </style>

<?php
$cssAnsScriptFilesModule = array(
      '/js/default/profilSocial.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
?>


<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
            <h3 class="title-section col-sm-8">La liste Mayenne Demain</h3>
            <hr>
        </div>
        <div class="col-xs-12">
        
            <span class="col-xs-12 text-left text-explain">
              La liste #MayenneDemain est une liste hors partis politiques, donc <strong>sans étiquette</strong>, avec des personnes de sensibilité de gauche et de droite modérées,
              réunies avant tout au service de Mayenne autour de valeurs fortes <strong>républicaines</strong>, <strong>écologistes</strong> et <strong>solidaires</strong>.
                 <!--<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/liste-mayenne-demain.jpg'> -->
               <br/>
            </span>
        </div> 

        <div class="col-xs-12">
                    <div class="text-center">
                        <div class="col-md-12 no-padding text-center">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/imk73G8GrIM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <hr style="width:40%; margin:20px auto; border: 4px solid #71b62c;">
                        </div>
                    </div>
              </div>
              
        <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
          <div class="row text-center" id="community">

          </div>

        </div>



    </div>
</div>




<script type="text/javascript">

  var hashUrlPage="#community";
  var connectTypeElement="<?php echo Element::$connectTypes[Organization::COLLECTION] ?>";
  var contextData = {type:costum.contextType,id:costum.contextId,name:"La liste Mayenne Demain"};

  var openEdition = false;
  var canEdit = false; 
  function sortProperties(obj)
  {
  // convert object into array
    var sortable=[];
    for(var key in obj)
      if(obj.hasOwnProperty(key))
        sortable.push([key, obj[key].name]); // each item is an array in format [key, value]

    // sort items by value
    sortable.sort(function(a, b)
    {
      var x=a[1].toLowerCase(),
        y=b[1].toLowerCase();
      return x<y ? -1 : x>y ? 1 : 0;
    });
    newObj={};
    $.each(sortable,function(a,b){
      newObj[b[0]]=obj[b[0]];
    });
    return newObj; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
  }
  jQuery(document).ready(function() {
      setTitle("La liste");
      getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/members",

          function (data) {
          
              var str="";
              data=sortProperties(data);//.sort(dynamicSort("name"));
              /*data.sort(function(a, b) {
                var textA = a.name.toUpperCase();
                var textB = b.name.toUpperCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
              });*/
              $.each(data, function(i,v){
                  if(v.username != "mayennedemain"){
                    str+="<a href='#page.type.citoyens.id."+i+"' class='lbh-preview-element'><div class=\"col-md-4 col-sm-6 col-xs-12 text-center espace-profile  margin-top-20 center-block\">\n"+
                        "<span id=\"avatar\"></span>\n"+
                        "<img src=\""+baseUrl+v.profilImageUrl+"\" class=\"img-oval\">\n"+
                        "<h3 class=\"titre-img\">"+v.name+"</h3>\n"+
                        "<span class='elipsis'>"+v.shortDescription+"</span>\n"+
                        "</div></a>";
                  }
              });
              $("#community").html(str);
              coInterface.bindLBHLinks();
          },
      "html");
  });



</script>