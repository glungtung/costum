<style type="text/css">
        .text-explain{
            font-size: 22px;
        }
    </style>

<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
            <h3 class="title-section col-sm-8">Le programme de Mayenne Demain</h3>
            <hr>
        </div>
        <div class="col-xs-12">
        
            <span class="col-xs-12 text-left text-explain">
              Voici les grandes lignes du programme de Mayenne Demain, écrites en concertation avec tous les membres de la liste, et en tenant compte de vos propositions dans la rubrique <a href="#dda"><?php echo Yii::t("home","Je contribue") ?></a>.
              <br/><br/>Pour feuilleter le programme complet, cliquez ici : <a href="http://programme.mayenne-demain.fr" target="_blank"><strong>programme.mayenne-demain.fr</strong></a>
                <br/><br/> 
        </div>

        <div class="col-xs-12">
        
        	<span class="col-xs-12 text-center">        
             <iframe width="560" height="315" src="https://www.youtube.com/embed/dPEC_68dV84" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
             <hr style="width:40%; margin:20px auto; border: 4px solid #71b62c;">
    		</span>
         </div> 

        <div class="col-xs-12">                 
                 <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/programme.png'> 
               <br/>
            </span>
        </div> 

    </div>
</div>

<script>
jQuery(document).ready(function() {
    setTitle("Le Programme"); 
});
</script>