 <style type="text/css">
    	.text-explain{
    		font-size: 22px;
    	}
    </style>
<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-8">Faire un don</h3>
        	<hr>
        </div>
        <div class="col-xs-12">
        
        	<span class="col-xs-12 text-left text-explain">
        	   Pour faire un don afin de soutenir la liste Mayenne Demain, vous pouvez réaliser un virement bancaire sur le compte ci-dessous : <br/><br/>
        	    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mayenneDemain/rib-mayenne-demain.png'>
        	   <br/>
    		</span>
        </div> 
 <div class="col-xs-12">
        
        	<span class="col-xs-12 text-left text-explain">
        	   Vous pouvez également envoyer un chèque libellé à l'ordre de :<br/>
        	   "<strong>Jérôme Beaugé Mandataire financier de Jean-Claude Lavandier</strong>" à l'adresse suivante :<br/><br/>
        	   Mayenne Demain<br/>
        	   20 rue Aristide Briand<br/>
        	 53100 MAYENNE<br/>
        	 <br/><br/><strong>Attention</strong> : quel que soit le mode de paiement choisi, merci de faire parvenir un courrier à l'adresse ci-dessus mentionnant vos nom, prénom, nationalité et adresse postale afin que nous puissions vous faire parvenir un reçu.
<br/><br/>
Les  dons  effectués  par  chèque ou  virement ouvrent  droit, pour les donateurs, à <strong>réduction  d’impôt sur  le revenu égale à 66 %</strong> du montant du don dans la limite de 20 % du revenu imposable (article 200 du code général des impôts). Pour bénéficier de cette réduction d'impôt, le reçu doit être fourni à l'administration fiscale.
<br/><br/>
Tout don supérieur à 150 euros doit être versé par chèque ou virement.
<br/><br/>
Les dons consentis par une personne physique sont limités à 4 600 euros pour une même élection, tous candidats confondus.
    		</span>
        </div>        
 
        
    </div>
</div>


<script>
jQuery(document).ready(function() {
    setTitle("Faire un don"); 
});
</script>