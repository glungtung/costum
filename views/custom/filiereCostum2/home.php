<?php 
$bannerImg = @$this->costum["banner"] ? Yii::app()->baseUrl.$this->costum["banner"] : Yii::app()->getModule("costum")->assetsUrl."/images/filiereCostum/no-banner.jpg";
$logo = @$this->costum["logo"] ? $this->costum["logo"] : null;

$cssAnsScriptFilesTheme = array(
    // SHOWDOWN
    '/plugins/showdown/showdown.min.js',
    // MARKDOWN
    '/plugins/to-markdown/to-markdown.js'            
  );
  HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 

  $poiList = array();
  if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
      $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  
      $poiList = PHDB::find(Poi::COLLECTION, 
                      array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                             "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                             "type"=>"cms") );
  } 
?>
<style>
.header{
    background-image: url(<?= $bannerImg; ?>);
    width: 100%;
    height: auto;
    background-size: cover;
    padding-bottom: 55%;
}
.bloc-section{
    margin-top: 7%;
    font-size: 3rem;
}
.rejoins{
    background: #32a2a4;
    margin-bottom: 2%;
}
.logofn{
    padding-top: 5%;
}
.projet{
    background-color : #e7e6e6;
    padding : 3%;
    font-size: 1.5rem;
    text-transform: none;
}
.bloc-projet {
    background: white;
    width: 25%;
    margin-left: 6%;
    padding: 3%;
    font-size: 1rem;
    border-radius: 3%;
}
.bloc-projet h2{
    text-transform: none;
}
#content-projet {
    margin-top: 7%;
}
</style>

<div class="header">
<?php 
    $params = [  "tpl" => "filiereCostum2","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
?>
    <div class="logofn col-md-9">
        <img src="<?= $bannerImg; ?>" class="img-responsive" width="75%">
    </div>
</div>

<div class="filigram">
    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="
    margin-top: -40.5%;
    position: absolute;
    opacity: 0.7;
    width: 51%;
    left: 53%;
    ">
</div>

<?php if(isset($this->costum["tpls"]["ressource"]))
        echo $this->renderPartial("costum.views.tpls.ressource", array("canEdit" => $canEdit));
 ?>

<div class="projet text-center bloc-section">
    <div class="container">
    <?= $this->renderPartial("costum.views.tpls.text", array("poiList" => $poiList, "tag" => "descriptionsfil1"));
    ?>
    </div>
</div>

<br /> <br />
<!-- join -->
<?= $this->renderPartial("costum.views.custom.filiereCostum.tpls.btn", array("poiList" => $poiList)); ?>


<?php if($canEdit){ ?> 
<!-- ESPACE ADMIN --> 

<hr>
<div class="container">
<a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent" data-id="<?= $this->costum["contextId"]; ?>" data-collection="<?= $this->costum["contextType"]; ?>"><i class="fa fa-plus"></i> Ajouter une section</a>
</div>
<?php } ?>


<script>
jQuery(document).ready(function(){
    mylog.log("render","/modules/costum/views/custom/filiereCostum/home.php");
    setTitle(costum.title);

    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };
});
</script>