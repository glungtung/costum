
<div>

<style type="text/css">
.row_title {
  margin-top:10px;
}

#bg-homepage {
  width:100%;
}

.numberCircle {
  font-size: 50px;
  font-weight: bold;
  margin-right: 15px;
}

p.step {
  font-size:30px;
  font-weight:bold;
  display : flex;
  align-items : center;
}

ul.mocica {
    list-style: none;
    margin-left: 0;
    padding-left: 1.2em;
    text-indent: -1.2em;
}

ul.mocica li:before {
    content: "►";
    display: block;
    float: left;
    width: 1.2em;
    color: #00B1E4;
}

ul.mocica li {
  font-size:1.5em;
  margin-bottom:10px;
}

div.cadre {
	border-radius:4px;
	padding:10px;
	font-size:1.3em;
}

div.cadre_title {
	font-size:1.6em;
	text-transform: uppercase;
	margin-bottom:7px;
}

span.number{
	font-size:0.8em;
	color:white;
	padding:3px 7px;
	margin-left:10px;
}

div.tool_header {
	font-size:1.4em;
	text-transform: uppercase;
	background-color:#54A8B3;
	color:white;
	padding:10px;
	border-radius:4px 4px 0px 0px;
}

div.tool_content {
	border:2px solid #54A8B3;
	border-radius:0px 0px 4px 4px;
}

ul.tool {
	list-style: none;
}

ul.tool li:before {
	content: "-";
    display: block;
    float: left;
    width: 1.2em;
}

ul.tool li{
	margin-top:10px;
	font-size:1.3em;
}

</style>

<!--<div class="row row_title">
		<p class="text-center">
			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/title_mocica3.png' alt="MOCICA" class="mocica" style="width:450px;margin-left:40px;"/>
			<img src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/title_logo.png' alt="MOCICA" class="logo" style="width:170px;"/>
		</p>
		<h1 class="phrase text-center">Unis et Libres</h1>
	</div>-->
	
	<div class="container">
	    <p style="margin-top:15px;font-weight:bold;">
		  L’objectif du projet Mocica est le passage à une société sans argent ni dirigeant suivant 3 étapes :
		</p>
	</div>
	<br/>
	
	<div class="container">
		<div class="col-md-4">
			<div class="cadre_title" style="color:#F44336;"><span class="number" style="background-color:#F44336;">1</span> Le rassemblement</div>
			<div class="cadre" style="border:2px solid #F44336;">
			Nous nous retrouvons dans nos assemblées de quartier et mettons en place notre gouvernance autogérée ODG (Organisation Démocratique Globale).
			</div>
		</div>
		<div class="col-md-4">
			<div class="cadre_title" style="color:#448AFF;"><span class="number" style="background-color:#448AFF;">2</span> La transition</div>
			<div class="cadre" style="border:2px solid #448AFF;">
			Nous continuons nos activités habituelles sans utiliser d’argent.
			</div>
		</div>
		<div class="col-md-4">
			<div class="cadre_title" style="color:#4CAF50;"><span class="number" style="background-color:#4CAF50;">3</span> L’organisation</div>
			<div class="cadre" style="border:2px solid #4CAF50;">
			Nous mettons en place notre société non marchande suivant les modalités choisies ensemble.
			</div>
		</div>
	</div>
	<br/>
	
	<div class="container">
		<p>Retrouvez tous les détails sur <a href='https://mocica.org'>mocica.org</a>.</p>
	</div>
	<br/>
	
	<div class="container">
		<div class="col-md-8">
			<div class="tool_header">
				<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/mocica/outils.svg" width="32" />
				<span style="margin-left:10px;">
				Vous trouverez ici les outils nécessaires pour concrétiser le projet :
				</span>
			</div>
			<div class="tool_content">
				<ul class='tool'>
					<li>Cartographie des membres et des assemblées</li>
					<li>Compte-rendu des assemblées</li>
					<li>Espace de discussion</li>
					<li>Réseaux de dons de biens et services </li>
					<li>Création d’évènements</li>
					<li>Etc.</li>
				</ul>
			</div>
		</div>
			
		<div class="col-md-4">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/mocica/together.jpg" class="img-responsive" style="border-radius:4px; margin-top:30px;"/>
		</div>
	</div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {
    setTitle("Projet MOCICA Unis et Libre");
  });
</script>

