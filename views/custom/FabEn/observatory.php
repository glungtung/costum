  <style>
    a:hover, a:focus{
      text-decoration: none;
      outline: none;
    }

    .project-item{
       min-height: 10em;
    }

    .list-item{
       margin-left: 0.5em;
       padding-top: 0.4em;
       padding-bottom: 0.3em;
    }

    .acteur-item{
       margin-bottom: 1.5em;
    }

    #answerModal{
       z-index: 10000000 !important;
    }

    .text-green-theme{
      color: #F3652D;
    }

    #searchInput {
      background-image: url('/images/search.png'); 
      background-position: 15px center;  
      background-repeat: no-repeat;
      background-color: #f1f1f1;
      border-radius: 30px;
      font-size: 16px; 
      padding: 5px 20px 5px 40px; 
      border: 1px solid #ddd;
      margin-bottom: 12px;
      margin-left: 12px;
    }

    .effectif{
      padding: 0.2em;
      background: #eee;
      border-radius: 0.3em;
    }

    .dash-icon{
      font-size: 2em;
      border-radius: 50%;
      padding: 0.8em;
      margin-top: 0.3em;
      margin-bottom: auto;
      background: #ddd;
    }

    .nav-tabs > li {
        float:none;
        display:inline-block;
        zoom:1;
    }

    .nav-tabs {
        text-align:center;
        text-transform: uppercase;
    }

    #obs-content .tabbable-obs-panel {
      border:1px solid #eee;
      padding: 10px;
    }

     /* Default mode */
     #obs-content .tabbable-obs-line > .nav-tabs {
         border: none;
         margin: 0px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li {
         margin-right: 2px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li > a {
         border: 0;
         margin-right: 0;
         color: #737373;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li > a > i {
         color: #a6a6a6;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li {
         display: inline-block;
         color: #000;
         text-decoration: none;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li::after {
         content: '';
         display: block;
         width: 0;
         height: 4px;
         background: #F3652D;
         margin-bottom: -4px;
         transition: width .3s;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li:hover::after {
         width: 100%;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li.open {
         border-bottom: 3px solid #F3652D;
     }
     
     #obs-content .tabbable-obs-line > .nav-tabs > li.open > a, #obs-content .tabbable-obs-line > .nav-tabs > li:hover > a {
         border-bottom: 2px solid #F3652D;
         background: none !important;
         color: #333333;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.open > a > i, #obs-content .tabbable-obs-line > .nav-tabs > li:hover > a > i {
         color: #a6a6a6;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.open .dropdown-menu, #obs-content .tabbable-obs-line > .nav-tabs > li:hover .dropdown-menu {
         margin-top: 0px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active {
         border-bottom: 3px solid #F3652D;
         position: relative;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active > a {
         border: 0;
         color: #333333;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active > a > i {
         color: #404040;
     }
     .media{
       border: 1px solid #eee;
       padding: 0.5em;
     }
     #obs-content .tabbable-obs-line > .tab-content {
      margin-top: -3px;
        background-color: #fff;
         border: 0;
         border-top: 1px solid #eee;
         padding: 13px 0;
     }
     #obs-content .tab-pane{
       overflow: hidden;
     }

     .graph-container{
       height: 70vh;
     }
     div[id$="-container"]{
       height: 100%;
       width: 100%;
     }
</style>
    
    <?php
      if($this->costum["contextType"] && $this->costum["contextId"]){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
      }

      if(!function_exists("count_distinct")){
        function count_distinct(&$array, $label){
          $exist = false;
          foreach ($array as $k => &$v) {
            if($v["label"]==$label){
              $v["number"]++;
              $exist = true;
            }
          }

          if(!$exist){
            array_push($array, array("label"=>$label, "number"=>1));
          }
        }
      }

      $acteurs = array();

      $orgaMembers = array();

      $answers = array();
      $projects = array();

      $graphe_data = array();
      $is_member = false;

      # Get Events
      $events = PHDB::find("events", array('source.keys' => $this->costum["slug"]));

      # Get members (organization) 
      $orgaMembers = PHDB::find("organizations", array('source.keys'=>$this->costum["slug"]));

      # Get form parent
      $form = PHDB::findOne("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));
      
      if(isset($form['id'])){
        $formId = $form['id'];
      }else{
        $formId = '';
      }

      if(count($orgaMembers)>0){
        foreach ($orgaMembers as $km => $aMember) {
          # Collect statistic data
          if(array_key_exists("type", $aMember)){
            count_distinct($graphe_data, Yii::t("common", $aMember["type"]));
          }else if(array_key_exists("category", $aMember)){
            count_distinct($graphe_data, Yii::t("common", $aMember["category"]));
          }

          # Get the anwers of a member
          $answerKey = "";

          if(isset($aMember["creator"])){
            $myAnswers = PHDB::find("answers", array("source.key"=>$this->costum['slug'], "user" => $aMember["creator"], "draft"=>['$exists' => false ]));
            foreach($myAnswers as $makey => $answer) {
              $answerKey = $makey;
            }
          }

          if(isset($aMember["name"])){
            $aMember["id"]=$km;

            $acteurs[$aMember['name']] = array(
              "member" => $aMember,
              "form" => $answerKey
            );
          }
        }
      }
      
      # Get answers
      $projects = PHDB::find("projects", array("source.keys"=>$this->costum['slug']));
        
    ?>

    <?php
      $cssAndScriptFilesModule = array(
        '/js/default/profilSocial.js'
      );
      HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
      $graphAssets = [
        '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
      ];
      HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
      );
    ?>

        <div class="container">
          <br><br>
          <div class="row text-center">
            <div class="col-md-4 col-sm-6 col-xs-12">
              <a class="lbh" href="#search?searchType=organization">
              <div class="container-fluid effectif">
                <div class="row">
                <div class="col-md-5">
                  <i class="fa text-green-theme fa-group dash-icon"></i>
                </div>
                <div class="col-md-7">
                  <h5><?php echo Yii::t("common", "Community")?></h5>
                  <h1 class="text-green-theme"><?= count($orgaMembers) ?></h1>
                </div>
              </div>
              </div>
            </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <a class="lbh" href="#search?searchType=project">
              <div class="container-fluid  effectif">
                <div class="row">
                  <div class="col-md-5">
                    <i class="fa text-green-theme fa-lightbulb-o dash-icon" style="padding-right: 1em; padding-left: 1em;"></i>
                  </div>
                  <div class="col-md-7">
                    <h5><?php echo Yii::t("common", "Projects")?></h5>
                    <h1 class="text-green-theme"><?= count($projects) ?></h1>
                  </div>
                </div>
              </div>
              </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <a class="lbh" href="#search?searchType=events">
              <div class="container-fluid effectif">
                <div class="row">
                  <div class="col-md-5">
                    <i class="fa text-green-theme fa-calendar dash-icon"></i>
                  </div>
                  <div class="col-md-7">
                    <h5><?php echo Yii::t("common", "Events")?></h5>
                    <h1 class="text-green-theme"><?= count($events) ?></h1>
                  </div>
                </div>
              </div>
            </a>
            </div>
          </div>
        </div>
   <br><br>
   <?php  if(isset($_SESSION["userId"])){ ?>
    <div id="obs-content" class="container-fluid" style="margin-bottom: 50px;">
      <div class="carto-n row">
        <div class="col-md-12">
          <div class="tabbable-obs-panel">
            <div class="tabbable-obs-line">
              <ul class="nav nav-tabs text-center">
                <!--li class="active">
                  <a href="#tab_default_1" data-toggle="tab">
                  <?php echo Yii::t("common", "List")?> </a>
                </li>
                <li>
                  <a href="#tab_default_2" data-toggle="tab">
                  <?php echo Yii::t("common", "Projects")?> </a>
                </li-->
                <li class="active">
                  <a href="#tab_default_4" data-toggle="tab">
                  <?= Yii::t("graph", "Doughnut") ?> </a>
                </li>
                <li>
                  <a href="#tab-graph-mindmap" data-graph="mindmap" data-toggle="tab">
                  <?= Yii::t("graph", "Mindmap") ?>  </a>
                </li>
                <li>
                  <a href="#tab-graph-relation" data-graph="relation" data-toggle="tab">
                  <?= Yii::t("graph", "Relation") ?>  </a>
                </li>
                <li>
                  <a href="#tab-graph-circle" data-graph="circle" data-toggle="tab">
                  <?= Yii::t("graph", "Circle") ?>  </a>
                </li>
                <li>
                  <a href="#tab-graph-network" data-graph="network" data-toggle="tab">
                  <?= Yii::t("graph", "Network") ?>  </a>
                </li>
                <!--li>
                  <a href="#tab_default_3" data-toggle="tab">
                  <?php echo Yii::t("common", "Events")?> </a>
                </li-->
              </ul>
              
              <div class="tab-content">
                <!--div class="tab-pane active" id="tab_default_1">
                  <br>
                  <div class="text-center">
                    <input type="text" id="searchInput" onkeyup="search()" placeholder="Recherche ...">
                  </div>
                  <br>

                  <div class="container">
                    <div class="row" id="actors-list">
                      <?php foreach ($acteurs as $id => $acteur) { ?>
                        <div class="col-12 col-md-6 col-lg-4 acteur-item">
                          <div class="media">
                            <div class="media-left pull-left">
                              <a href="<?php echo '#page.type.organizations.id.'.$acteur["member"]["id"]; ?>" class="lbh-preview-element">
                                <img src="<?= (isset($acteur["member"]["profilImageUrl"]))?$acteur["member"]["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" class="media-object" style="width:100px; height:70px; object-fit:cover">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="<?php echo '#page.type.organizations.id.'.$acteur["member"]["id"]; ?>" class="lbh-preview-element member-name">
                                <h4 class="media-heading"><?php echo $acteur["member"]["name"]; ?></h4>
                                <?php 
                                  if(isset($acteur["member"]["category"])){
                                    if(is_array($acteur["member"]["category"])){
                                      echo "<p>".$acteur["member"]["category"][0]."</p>";
                                    }else{
                                      echo "<p>".$acteur["member"]["category"]."</p>";
                                    }
                                  }else if(isset($acteur["member"]["type"])){
                                    echo "<p>".$acteur["member"]["type"]."</p>";
                                  }
                                ?>
                              </a>
                              <?php if($acteur['form']!=""){ ?>
                                <button type="button" class="btn btn-default getanswer" datat-mode="r" data-ansid="<?php echo "".$acteur['form'].""; ?>" data-toggle="modal" data-target="#answerModal">Voir réponse</button>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="text-center">
                    <button class="btn btn-default" id="loadMoreAnswers">Afficher plus</button>
                  </div>
                </div-->
                <!--div class="tab-pane" id="tab_default_2">
                  <br><br>
                  <div class="container grid-container">
                    <div class="row">
                      <?php foreach($projects as $p => $project) { ?>
                        <div class="col-12 col-md-6 col-lg-4 project-item">
                          <div class="media">
                            <div class="media-left pull-left">
                              <a href="<?php echo '#page.type.projects.id.'.$p; ?>"  class="lbh-preview-element">
                                <img src="<?= (isset($project["profilImageUrl"]))?$project["profilImageUrl"]:Yii::app()->getModule("co2")->assetsUrl.'/images/thumbnail-default.jpg' ?>" class="media-object" style="w; height:70px; object-fit:coveridth:100px">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="<?php echo '#page.type.projects.id.'.$p; ?>"  class="lbh-preview-element">
                                <h4 class="media-heading"><?php echo $project["name"]; ?></h4>
                              </a>
                              <p style="font-size: 11pt">
                                <i>Porté par : 
                                <?php foreach($project["parent"] as $kpr => $parent) {
                                   echo str_replace("(Moi)", "", $parent["name"]);
                                } ?>
                                </i>
                              </p>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="text-center">
                   <button class="btn btn-default" id="loadMoreProjects">Afficher plus</button>
                  </div>
                </div-->
                <div class="tab-pane active" id="tab_default_4">
                  <div class="col-md-12">
                    <canvas id="bar-chart" width="800" height="250"></canvas>
                  </div>
                </div>
                <div class="tab-pane" id="tab-graph-circle">
                <div class="col-md-12 graph-container">
                    <div id="circle-container"></div>
                 </div>
          
                </div>
                <div class="tab-pane" id="tab-graph-network">
                <div class="col-md-12 graph-container">
                    <div id="network-container"></div>
                 </div>
                 
                </div>
                <div class="tab-pane" id="tab-graph-mindmap">
                <div class="col-md-12 graph-container">
                    <div id="mindmap-container"></div>
                 </div>
                 
                </div>
                <div class="tab-pane" id="tab-graph-relation">
                <div class="col-md-12 graph-container">
                 <div id="relation-container"></div>
                 </div>
                </div>
                
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- Modal Answers-->
  <div id="answerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header" style="border-bottom: none;">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Réponse sur <?php echo isset($form["name"])?$form["name"]:"" ?> </h4>
         </div>
         <div id="answerContent" class="modal-body">
            
         </div>
         <div class="modal-footer"  style="border-top: none;">
           <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
         </div>
       </div>
     </div>
   </div>

  <!--script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script-->
  <script src="/plugins/Chart-2.6.0/Chart.js"></script>
  <script type="text/javascript">
    var mindmap;
    var relation;
    var circle;
    var network;
    
    $(document).ready(function(){

      const orgaMembers = <?= json_encode($orgaMembers); ?>;
      var authorizedTags = [];

      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
        var currentGraph = $(e.target).data("graph");

        if(currentGraph == "mindmap" && typeof mindmap == "undefined"){
          // Graphs
          mindmap = new MindmapGraph([], 1, {id: "root", label: "<?= Yii::t("common", "Community")?>"});
          if(costum.lists.theme){
            mindmap.setTheme(costum.lists.theme)
          }
          
          mindmap.setLabelFunc((d,i,n) => {
            if(d.data.id == "root"){
              return "<?= Yii::t("common", "Community")?>";
            }
          
            if(Object.keys(trad).includes(d.data.label)){
              return trad[d.data.label];
            }
            return d.data.label;
          })
        }

        if(currentGraph == "circle" && typeof circle == "undefined"){
          circle = new CircleGraph([],d => d.group, authorizedTags);
        }

        if(currentGraph == "relation" && typeof relation == "undefined"){
          relation = new RelationGraph([],authorizedTags)
        }

        if(currentGraph == "network" && typeof network == "undefined"){
          network = new NetworkGraph([], d => {return d.data.type});
            network.setLabelFunc((d,i,n) => {
              if("group" == d.data.group){

                if(Object.keys(trad).includes(d.data.label)){
                  return trad[d.data.label];
                }
              }
              if("root" == d.data.group){
                return "<?= Yii::t("common", "Community")?>";
              }
              return d.data.label;
            })
        }


        var observer = new ResizeObserver((entries) => {
            for(const entry of entries){
                // console.log(entry.contentRect);
                const {width, height} = (entry.contentRect);
                console.log(width, height)
                const graph = ($(entry.target).attr("id").replace("-container", ""));
                if(width > 0 && height > 0){
                  window[graph].adaptViewBoxByRatio(width / height);
                }
            }
        });
        const observed = new Set();

        if(currentGraph){
          let currentContainer = "#" + currentGraph + "-container";
          if(!window[currentGraph].isDrawed){
            window[currentGraph].draw(currentContainer);
            window[currentGraph].updateData(window[currentGraph].preprocessResults(orgaMembers));
            setTimeout(() => window[currentGraph].initZoom(), 100)
          }
          if(!observed.has(currentGraph)){
            observed.add(currentGraph);
            observer.observe(document.querySelector(currentContainer));
          }
        }
      })
      // FIN GRAPHS 

      $(".getanswer").on('click', function() {
        $("#answerContent").empty();
        coInterface.showLoader("#answerContent");
        var answid = $(this).data("ansid");
        var getdatatype = "";
        var contextId = "<?php echo $this->costum['contextId'] ?>";
        var contextType = "<?php echo $this->costum['contextType'] ?>";
        if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
            ajaxPost("#answerContent", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                  null,
                  function(){
                      if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid+".contextId."+contextId+".contextType."+contextType);
                      }
                  },"html");
        }else{
          ajaxPost("#answerContent", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                 null,
                  function(){
                      if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid);
                      }
                  },"html");
        }
      });

         // Hide read more button
         $(".read-more").hide();

        var ctx = $("#bar-chart");

    new Chart(ctx, {
      type: 'pie',
      data: {
        labels: <?=json_encode(array_column($graphe_data, "label"))?>,
        datasets: [
          {
            label: "Acteurs",
            backgroundColor: ["#CCE428", "#FEEF33", "#FBCBA9", "#F69854","#CCE428", "#e84c3d", "#f1c40f", "#F69854"],
            data: <?=json_encode(array_column($graphe_data, "number"))?>
          }
        ]
      },
      options: {
        responsive: true,
        legend: { 
          display: true,
          position: "bottom",
          align: "center"
        },
        title: {
          display: true,
          text: 'Type des acteurs \n'
        },
        cutoutPercentage: 50
      }
    });

    
    
      var x=10;
      var answer_size = $(".afn-card").length;

      (answer_size > x)? $("#loadMoreAnswers").show(): $("#loadMoreAnswers").hide();

      $(".afn-card").hide();
      $(".afn-card:lt("+x+")").show();

      $('#loadMoreAnswers').click(function () {
        x= (x+5 <= answer_size)? x+5: answer_size;
        $('.afn-card:lt('+x+')').show();
      });


      var x=10;
      var answer_size = $(".pfn-card").length;

      (answer_size > x)? $("#loadMoreProjects").show(): $("#loadMoreProjects").hide();

      $(".pfn-card").hide();
      $(".pfn-card:lt("+x+")").show();

      $('#loadMoreProjects').click(function () {
        x= (x+5 <= answer_size)? x+5: answer_size;
        $('.pfn-card:lt('+x+')').show();
      });



    $('.effectif h1').each(function () {
      $(this).prop('Counter',0).animate({
        Counter: $(this).text()
      }, {
        duration: 900,
        easing: 'swing',
        step: function (now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  });

   function search() {
      var input, filter, liste, item, i, txtValue;
      input = $('#searchInput');
      filter = input.val().toUpperCase();
      liste = $("#acteurs-list");
      item = $('.acteur-item');

      for (i = 0; i < item.length; i++) {
         paragraph = item[i].getElementsByTagName("h4")[0];
         txtValue = paragraph.textContent || paragraph.innerText;
         if (txtValue.toUpperCase().indexOf(filter) > -1) {
            item[i].style.display = "";
         } else {
            item[i].style.display = "none";
         }
      }
   }

   
</script>
<?php }else{ ?>
  <div class="container">
    <hr>
  </div>
  <div class="text-center">
    <h4 class="text-light text-center">Veuillez vous connecter pour plus d'informations</h4>
    <br>
    <div>ou</div>
    <br>
    <button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalRegister">
      <i class="fa fa-plus-circle"></i> Créer Un Compte <b>Citoyen</b>
    </button>
  </div>
<?php } ?>
