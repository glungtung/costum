<div class="container py-4" id="container-about">
    <div class="row">
        <div class="col-12 col-md-6 py-3 text-center">
            <img class="img-thumbnail w-75  wow slideInLeft" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/jaona/me2.jpg" alt="">
        </div>
        <div class="col-12 col-md-6 py-3 ">
            <div class="wow slideInRight">
                <h3 class="text-center"> À propos </h3>
                <br>
                <blockquote class="blockquote">
                    L'informatique n'est pas seulement un emploi, c'est d'abord une passion, un art. Confiez-moi vos
                    projets, je les réaliserai avec soin, en suivant toutes les étapes nécessaires de la conception
                    au
                    déploiement.
                </blockquote>
                <br>
                <ul id="contacts">
                    <li class="pb-2">
                        <i class="fa fab fa-2x fa-facebook mr-2"></i>
                        <a href="http://web.facebook.com/rajaomariajaona" target="_blank" rel="noopener noreferrer">
                            Rajaomaria Jaona </a>
                    </li>
                    <li class="pb-2">
                        <i class="fa fa-phone fa-2x mr-2"></i>
                        +261 34 48 220 17
                    </li>
                    <li class="pb-2"><i class="fa fa-envelope fa-2x mr-2"></i>
                        <a href="mailto:rajaomariajaona11@gmail.com">rajaomariajaona11@gmail.com</a>
                    </li>
                    <li class="pb-2">
                        <i class="fa fa-map-marker fa-2x mr-2"></i>
                        IB 90 Andraivato Ambatofotsy Antananarivo 102 Madagascar
                    </li>
                </ul>
                <div class="text-md-left text-center">
                    <a class="btn color-primary border-primary mx-3" href="assets/files/cv.pdf" target="_blank" rel="noopener noreferrer">Télécharger mon CV</a>
                </div>
            </div>

        </div>
    </div>
</div>