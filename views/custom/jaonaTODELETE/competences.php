<div id="container-skill">
    <div class="container py-5 ">
        <div class="row">
            <div class="col-md-6 col-sm-12 wow slideInUp">
                <h3 class="level-1">Technologies</h3>
                <h4 class="level-2">Langages</h4>
                <ul class="level-3 p-0">
                    <li>JAVA
                        <div class="level-1 my-2">
                            <div class="progress">
                                <div class="progress-bar" style="width: 70%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                    <li>JavaScript / TypeScript
                        <div class="level-1 my-2">

                            <div class="progress">
                                <div class="progress-bar" style="width: 90%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                    <li>Python
                        <div class="level-1 my-2">

                            <div class="progress">
                                <div class="progress-bar" style="width: 60%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                </ul>
                <h4 class="level-2">Framework et Librairies</h4>
                <ul class="level-3 p-0">
                    <li> Express
                        <div class="level-1 my-2">
                            <div class="progress">
                                <div class="progress-bar" style="width: 90%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                    <li> TypeORM
                        <div class="level-1 my-2">
                            <div class="progress">
                                <div class="progress-bar" style="width: 85%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                    <li> NestJS
                        <div class="level-1 my-2">
                            <div class="progress">
                                <div class="progress-bar" style="width: 60%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                    <li> Angular
                        <div class="level-1 my-2">

                            <div class="progress">
                                <div class="progress-bar" style="width: 75%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                    <li> Flutter
                        <div class="level-1 my-2">

                            <div class="progress">
                                <div class="progress-bar" style="width: 90%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12 wow slideInUp">
                <h3 class="level-1">Outils</h3>
                <h4 class="level-2">IDE</h4>
                <span class="level-3 pb-3">
                    Visual Studio Code, Netbeans, Android Studio
                </span>
                <h4 class="level-2">Gestionnaire de Subversion</h4>
                <span class="level-3 pb-3">
                    Git, GitHub
                </span>
                <h4 class="level-2">Technologie de virtualisation</h4>
                <span class="level-3 pb-3">
                    VMWare, Virtual Box, Docker
                </span>
                <h4 class="level-2">Système d'exploitation</h4>
                <span class="level-3 pb-3">
                    Windows, Linux(Ubuntu, Debian, Arch), Android
                </span>
                <h4 class="level-2">Bureautique</h4>
                <span class="level-3 pb-3">
                    Word, Excel, PowerPoint
                </span>
            </div>
        </div>
    </div>
</div>