<?php

Yii::import("parsedown.Parsedown", true);
$Parsedown = new Parsedown();

?>
<style type="text/css">
	
h1 {
	font-size: 24px;
}

.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

table.first {
        color: #003300;
        font-family: helvetica;
        border: 3px solid black;
    }

table.first td {
        border: 1px solid black;
        font-size: 8pt;
    }

table.first td .entete {
         font-size: 10pt;
         font-weight: bold;
    }

</style>
<?php

foreach ($answers as $key => $elt) {

	?>
<div class="body">
<?php
	
	$cter = PHDB::findOne(Project::COLLECTION, array("slug" => $elt["formId"], "category" => "cteR"), array("name"));

	if( !empty($elt["answers"]) &&
					!empty($elt["formId"]) &&
					!empty($elt["answers"][$elt["formId"]]) &&
					!empty($elt["answers"][$elt["formId"]]["answers"]) ) {
					$answers = $elt["answers"][$elt["formId"]]["answers"];
				
		if(!empty($answers["organization"]) && !empty($answers["organization"]["id"]) ){
			$organization = PHDB::findOneById(Project::COLLECTION, $answers["organization"]["id"], array("name"));
		}
		if(!empty($answers["project"]) && !empty($answers["project"]["id"])  ){
			$project = PHDB::findOneById(Project::COLLECTION, $answers["project"]["id"], array("name"));
		}
	}

?>
	<span style="text-align: center;">
		<h1 class="blue"> </h1>
		<span class="darkgreen"><?php echo $cter["name"]; ?></span>
		<span class="lightgreen"></span>
		<br/>
		<?php
			if(!empty($project)) { ?>
				<span class="">Action N° <?php echo (String)$project["_id"]." : ".$project["name"]; ?> </span>
		<?php	} ?>
		
	</span>
	<br/><br/>
	<span style="text-align: left; font-size: 14px;">
		Rattachée à l’orientation : <?php 
			if(isset($answer["project"]["orientations"]) && !empty($answer["project"]["orientations"])){
				foreach ($answer["project"]["orientations"] as $key => $value) {
					echo $orientations[$key]["name"];
				}
			}
			
			if(!empty($answers["caracter"])){
				if(gettype($answers["caracter"]["cibleDDPrincipal"]) == "array")
					echo $answers["caracter"]["cibleDDPrincipal"][0];
				else
					echo $answers["caracter"]["cibleDDPrincipal"];
			}
		?>
		<br/>
		Dernière date de mise à jour : Date
	</span>
	<div class='col-xs-12'>
		<?php
		$str = "";
		if(!empty($elt["shortDescription"])){
			$str .= '<h4 class="padding-20 blue" style="">'."shortDescription".'</h4>';
			$str .= '<span>'.$elt["shortDescription"].'</span>';
		}

		if(!empty($elt["description"])){
			$str .= '<h4 class="padding-20 blue" style="">'."description".'</h4>';
			$str .= '<span>'.$elt["Description"].'</span>';
		}

		if(!empty($elt["tags"])){
			$str .= '<h4 class="padding-20 blue" style="">'."tags".'</h4>';
			$str .= '<span>';

			foreach ($elt["tags"] as $keyT => $valT) {
				$str.= '<span style="color :red">#'.$valT.'</span> ';
			}

			'</span>';
		}


		?>
	</div>
</div>
<?php
	}

?>