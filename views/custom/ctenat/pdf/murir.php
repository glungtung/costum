<?php

$formid = "murir";

$parentForm = PHDB::findOneById( Form::COLLECTION , $answer["form"]);
$murirforms = PHDB::findOne( Form::COLLECTION , array("id"=>$formid));

// echo "<ul class='questionList col-xs-12 '>";

foreach ( $murirforms["inputs"] as $key => $input) { 
//     var_dump($key);
//notaBene, calendar, partner, budget, planFinancement, results

    // $listInput = array("calendar");
    // if(in_array($key, $listInput)){

    	$tpl = $input["type"];
            if(in_array($tpl, ["textarea","markdown","wysiwyg"]))
                $tpl = "tpls.forms.textarea";
            else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags"]))
                $tpl = "tpls.forms.text";
            else if(in_array($tpl, ["sectionTitle"]))
                $tpl = "tpls.forms.".$tpl;

        $answers = null;
        if( isset($answer["answers"][$formid]) && 
            isset($answer["answers"][$formid][$key]) && 
            count($answer["answers"][$formid][$key])>0 )
            $answers = $answer["answers"][$formid][$key];

        $kunikT = explode( ".", $input["type"]);
        $keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
        $kunik = $keyTpl.$key;

    	$p = [ 
                "parentForm"=> $parentForm,
                "form"      => $form, 
                "key"       => $key, 
                "kunik"     => $kunik,
                "input"     => $input,
                "type"      => $input["type"], 
                "answerPath" => "answers.".$key.".",
                "answer"    => $answer,
                "answers"   => $answers ,//sub answers for this input
                "label"     => $input["label"] ,//$ct." - ".$input["label"] ,
                "titleColor"=> (isset($this->costum["colors"]["pink"])) ? $this->costum["colors"]["pink"] : "#000",
                "info"      => isset($input["info"]) ? $input["info"] : "" ,
                "placeholder" => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
                "mode"      => "pdf",
                "canEdit"   => false,
                "canEditForm" => false,
                "canAdminAnswer" => false,
                "canAnswer" => false,
                "editQuestionBtn" => "",
                "saveOneByOne" => false,
                "wizard"    => true  ];

        echo $this->renderPartial( "survey.views.".$tpl , $p );

        if(isset($answer["answers"]["murir"][$key]) && isset($dateComment)){
            foreach($answer["answers"]["murir"][$key] as $e=>$v ){
                //var_dump($e);var_dump($key);var_dump();exit;
                $titleComment="";
                if(isset($v["step"])) $titleComment=$v["step"];
                else if(isset($v["title"])) $titleComment=$v["title"];
                else if(isset($v["qui"])) $titleComment=$v["qui"];
                else if(isset($v["poste"])) $titleComment=$v["poste"];
                else if($key=="results") $titleComment="Indicateur ".($e+1);
               
                echo Ctenat::buildLineCopilComment((string)$answer["_id"], $titleComment, $dateComment,  (string)$answer["_id"], $key, $e);
            }
        }

    //}
}
// echo "</ul>";