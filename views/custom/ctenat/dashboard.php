<?php 

    $cssAnsScriptFilesTheme = array(
        "/plugins/jquery-counterUp/waypoints.min.js",
        "/plugins/jquery-counterUp/jquery.counterup.min.js",
    ); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style type="text/css">
    <?php if( !isset($css["border"]) || $css["border"]==true ){ ?>
    .counterBlock{min-height:340px;}
    .wborder{border : 2px solid #002861; border-radius : 20px;}
    .title2{font-size : 24px; font-weight : bold;}
    <?php }?>
    
</style>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-20 dashboard app-<?php echo @$page ?>">
    <div class="col-xs-12 text-center" style=" color:#24284D">
        <h1 style="margin:0px"><?php echo $title ?></h1>
    </div>
   <div classs="col-xs-12" style="">
        <?php //echo $this->renderPartial("graph.views.co.menuSwitcher",array()); ?> 
        <div class="col-xs-12" id="graphSection">
                <?php                           
                $blocksize = floor(12/count($blocks));
                $colCount = 0;
                $fontSize = "24px;font-weight:bold;";
                
                 foreach ($blocks as $id => $d) 
                 { 
                    $borderClass = (isset($d["noborder"])) ? "" : "wborder";
                    if( isset($d["graph"]) ){
                        $borderClass =  "wborder";     
                        $fontSize = "24px;font-weight:bold;";
                    }
                    if( isset($d["colSize"]) ){
                        $blocksize = $d["colSize"];
                    }
                    else if( isset($d["graph"]) && count($blocks) > 3){
                        $blocksize = 4;
                        $borderClass =  "wborder";     
                        $fontSize = "24px;font-weight:bold;";
                    }
                    else if( isset($d["html"]) ){
                        $blocksize = 2;
                        $fontSize = "18px;";
                    }
                    else if( isset($d["offset"]) ){
                        $blocksize = 1;
                    } 
                    else if( isset($d["col12"]) ){
                        $blocksize = 12;
                    }
// $blocksize = 12;
 //var_dump($blocksize);
// var_dump(@$d["graph"]["data"]["datasets"]);
// var_dump(@$d["graph"]["data"]["labels"]);

                    ?>

                    
                    
                    <?php
                    if($id == "co2"){

                        echo "<div class='col-xs-12 text-center'><h4 class='text-red'>Les chiffres présentés ci-dessous correspondent aux engagements des territoires</h4></div><div class='col-xs-12'></div>";
                    }
                     if( isset($d["col12"]) )  { ?> 
                    <div class="col-md-<?php echo $blocksize?> text-center padding-10 "  >
                        <?php echo $d["col12"]?>
                    </div>
                    <?php } else if( isset($d["map"]) )  { ?> 
                        <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <div id='map<?php echo $id?>' class='col-xs-12' style='height: 420px;'></div>
                            <script type="text/javascript">
                                mylog.log("map","id:map<?php echo $id?>","var js :mapDash<?php echo $id?>");
                                var mapDash<?php echo $id?> = {};
                                jQuery(document).ready(function() {
                                    mapDash<?php echo $id?> =  mapObj.init({
                                        container : "map<?php echo $id?>",
                                        latLon : [ 39.74621, -104.98404],
                                        activeCluster : true,
                                        zoom : 16,
                                        activePopUp : true,
                                        menuRight : false
                                    });
                                    mapDash<?php echo $id?>.addElts(<?php echo json_encode( $elements) ?>, true);
                                });
                                //.clearMap
                            </script>
                        </div>

                    <?php } elseif( isset($d["indicator"])  )  { 
                        ?>
                        <div class="col-md-12 text-center padding-10 "  >
                        <h1><?php echo $d["title"] ?></h1>
                        </div>
                        <?php
                        foreach ($d["indicator"] as $ix => $ind) {
                        ?>
                        <div class=" col-md-4 col-xs-12  text-center padding-10"  >
                            <div class="<?php echo $borderClass; ?>">
                                <h4 style="color:#000091"><?php echo $ind["title"];?></h4>
                                
                                <h5>Engagement : <?php echo $ind["obj"];?></h5>
                                <h5>Réalisé : <?php echo $ind["done"];?></h5>
                            </div>    
                        </div>
                        
                        <?php }

                     
                     } elseif( isset($d["graph"]) || isset($d["html"])  )  { 
                        ?>     
                        <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <h1><span class="<?php echo (!empty($d["counter"]))? "counter" : ""?>"><?php echo $d["counter"]?></span></h1>
                            <div style='font-size:<?php echo $fontSize?>'><?php echo $d["title"]?></div>
                            <div class="counterBlock <?php echo $borderClass; ?>" id="<?php echo $id?>" >
                                <?php if( isset($d["html"]) ) 
                                    echo $d["html"];?>
                            </div>
                        </div>
                     <?php 
                     
                     } ?>
                <?php 
                     $colCount = $colCount + $blocksize;
                     //LINE BREAK
                     if($colCount == 12){
                        echo '<div class="col-xs-12"></div>';
                        $colCount = 0;
                     } ?>
             <?php }?>
        </div>
   </div>

</div>



<script type="text/javascript">
//alert("/modules/costum/views/custom/ctenat/dashboard.php")
//prepare global graph variable needed to build generic graphs

<?php  
foreach ($blocks as $id => $d) {
    if( isset($d["graph"]) ) {
        ?>
        var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
        mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
<?php }
} ?>

jQuery(document).ready(function() {
    setTitle("CTE : <?php echo strip_tags($title) ?>");
    mylog.log("render graph","/modules/costum/views/custom/ctenat/dashboard.php");

    <?php  foreach ($blocks as $id => $d) {
        if( isset($d["graph"]) ) { ?>
            mylog.log('render ajaxPost url graphs',' <?php echo $d["graph"]["url"]?>','#<?php echo $id?>');
            ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
    <?php }
    } ?>

    $('.counter').counterUp({
      delay: 10,
      time: 2000
    });

    $('.counter').addClass('animated fadeInDownBig');
    $('h3').addClass('animated fadeIn');

});
</script>


<?php 
if(isset($elements)){
?>
<style type="text/css">
    .dashElem{
        /*height:275px;*/
        overflow:hidden;
        /*border: 1px solid #bbb;*/
    }
    .grayimg{
        opacity:0.2;
    }
    .openDashModal{
        font-size: 10px;
    }
</style>

<?php 
    $badgeColor = [];
    $colorCt = 0;
    $colorList = array_slice(Ctenat::$COLORS, 3);
    foreach ($this->costum["lists"]["domainAction"] as $key => $value) {
        $badgeColor[$key] = $colorList[$colorCt];
        $colorCt++;
    }
    
    function getBadgeFamily($tag, $badgeColor,$domainAction ){
        foreach ( $domainAction as $key => $childBadges) {
            foreach ($childBadges as $ic => $cb) {
                if($tag == $cb)
                    return array( 
                        "color"  => $badgeColor[$key],
                        "parent" => $key );
            }
            
        }
    }

    echo "<div class='col-xs-12'>"; 
        $slugData = (isset($slug)) ? " data-slug='".$slug."'" : "";
        echo "<div class='col-xs-12 margin-bottom-20'>"; 
            foreach ($badgeColor as $badge => $color) {
                
                echo "<a href='javascript:;' data-badge='".$badge."' ".$slugData." class='openDashModal btn btn-xs' style='margin-left:5px; background-color:".$color."'>#".$badge."</a>";
            }
        echo "</div>"; 
        echo "<h1 class='text-center' id='dashElemTitle'>Projets ".$blocks[ array_keys( $blocks)[0] ]["title"]." (".count($elements).")</h1>";
        $elCount = 0;
        foreach ($elements as $id => $p) {
            // echo $tag;
            //  echo $el["type"];
            if( ($elCount % 4) == 0)
                echo "<div class='col-xs-12 margin-top-20'></div>"; 
            $tagClasses = "";
            if(isset($p["tags"])){
                foreach ( $p["tags"] as $i => $t ) {
                   $tagClasses .= InflectorHelper::slugifyLikeJS($t)." ";
                }
            }
            echo "<div class='col-xs-12 col-sm-3 dashElem ".$tagClasses."'>"; 
                echo "<div style='height:150px;overflow:hidden;'>";   
                if(isset($p["profilMediumImageUrl"])){
                    echo "<img style='margin:auto; width:100%;' src='".Yii::app()->createUrl($p["profilMediumImageUrl"])."' class='img-responsive'/>";
                } else {
                    echo '<img class="img-responsive grayimg"  style="margin:auto; width:100%;" src="'.Yii::app()->getModule("costum")->assetsUrl.'/images/ctenat/action.png">';
                }
                echo "</div>";
                // les badges
                echo "<div>";
                   echo "<a href='#@".$p["slug"]."' class='lbh-preview-element text-dark' data-id='".$id."' data-type='projects'> ".$p["name"]." </a>";
                    if(isset($p["tags"])){
                        foreach ( $p["tags"] as $i => $t ) {
                           $badgeParent = (getBadgeFamily($t, $badgeColor,$this->costum["lists"]["domainAction"])) ? getBadgeFamily($t, $badgeColor,$this->costum["lists"]["domainAction"]) : array("parent"=>"","color"=>"#ccc");

                           if( !empty($badgeParent["parent"] ))
                                echo "<br/><a href='javascript:;' data-badge='".$p["domaineAction"]["family"]."'  ".$slugData." class='btn btn-xs openDashModal' style='background-color:".$badgeColor[$p["domaineAction"]["family"]]."'>#".$t."</a>";
                            // CibleDD :  else echo "<br/><span class='openDashModal' style='color:red;padding:3px; background-color:".$badgeParent["color"]."'>#".$t."</span>";
                        }
                    }
                echo "</div>";
                //Cterr
                if(isset($p["links"]["projects"])){
                    $cterId = array_keys($p["links"]["projects"]);
                    
                    if(isset($cterId[0])){
                        try {
                            $cter = PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId( $cterId[0] )),array("name","slug"));
                            echo "<div>";  
                                
                                echo "<a href='#@".$cter["slug"]."' class='lbh-preview-element text-dark' data-id='".$cterId[0]."' data-type='project'> <i class='fa fa-map-marker'></i> ".$cter["name"]." </a>";
                            echo "</div>";
                        } catch (Exception $e) {

                            //echo "Exception:", $p["_id"],$e->getMessage();
                        } 
                    }
                }
            
        //var_dump($p["domaineAction"]);
        echo "</div>";  
        $elCount++;
    }
    echo "</div>"; 
}

?>
<script type="text/javascript">

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/ctenat/dashboard.php",<?php echo json_encode( $elements) ?>);
        coInterface.bindLBHLinks();
        $('.openDashModal').off().click(function() { 
            tag = $(this).data("badge");
            slug = ($(this).data("slug")) ? "/slug/"+$(this).data("slug") : "";
            smallMenu.openAjaxHTML( baseUrl+'/costum/ctenat/dashboard/tag/'+$(this).data("badge")+slug );
        });
                
 
        //    });

    });
</script>
