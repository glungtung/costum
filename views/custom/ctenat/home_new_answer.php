<div class="col-xs-12 text-center">
	<div class="col-xs-12 text-center">
<!--		Vous souhaitez proposer une action pour le CTE de votre territoire.-->
		<br/><br/>
		<h3>ETAPE 1 :</h3>
		Dans le formulaire suivant vous devrez renseigner l’organisme porteur, l’intitulé de l’action et une brève description. On vous demandera également de caractériser brièvement votre action.
	</div>

	<div class="col-xs-12 text-center">
		<img class='img-responsive' style='margin:30px auto 0px auto;' src='<?php echo Yii::app()->getModule("costum")->assetsUrl?>/images/ctenat/stepper.png'>
	</div>

	<div class="col-xs-12 text-center">
		Cette première étape réalisée, <b>votre page action sera automatiquement créée</b> sur la plateforme et vous pourrez y accéder en tant qu’administrateur pour la modifier et la compléter avec d’autres informations (financements, partenaires, indicateurs de suivi…), en vous rendant sur <b>l’onglet “candidature”</b> de votre page action.
	</div>

	<div class="col-xs-12 text-center">
		<img class='img-responsive' style='margin:30px auto 0px auto;' src='<?php echo Yii::app()->getModule("costum")->assetsUrl?>/images/ctenat/menuTabs.png'>
	</div>

	<div class="col-xs-12 text-center">
		A ce stade, votre page action ne sera visible que de vous et des référents du CTE du territoire (la collectivité, les services de l’Etat et leurs partenaires locaux).
	</div>


	<br/>
	<?php
		if(!empty($forms)){
			foreach ($forms as $key => $value) {
				if(!empty(Yii::app()->session['userId'])){ 
					$url = "/survey/answer/index/id/new/form/".$key;
					if(!empty($contextId) && !empty($contextType))
						$url .= "/contextId/".$contextId."/contextType/".$contextType;
					?>
					<a class="btn btn-success newAnswerCter" href="javascript:;" data-form="<?php echo $key; ?>" data-contexttype="<?php echo $contextType; ?>" data-contextid="<?php echo $contextId; ?>" style="font-size: 17px;"> Déposer une candidature</a>
		       <?php } else { ?>
					<button class="btn btn-default bg-white letter-green bold" data-toggle="modal" data-target="#modalLogin" style="font-size: 17px;">
						<i class="fa fa-sign-in"></i> 
						<span class="hidden-xs"><?php echo Yii::t("login", "Login") ?></span>
					</button>
					<button class="btn btn-default bg-white letter-blue bold" data-toggle="modal" data-target="#modalRegister" style="font-size: 17px;">
	                        <i class="fa fa-plus-circle bold"></i> 
	                        <span class="hidden-xs"><?php echo Yii::t("login", "I create my account") ?></span>
	                </button>
		       <?php }
			}
		} ?>
</div>

<script type="text/javascript">
	//var formProfil = {} ;
	jQuery(document).ready(function() {

		pageProfil.form = formObj.init({});
		$(".newAnswerCter").unbind("click").on("click",function(e){
			// ajaxPost('#central-container', baseUrl+$(this).data("url"),
			// null,
			// function(){},"html");
			pageProfil.form.urls.answer(pageProfil.form, "new", $(this).data("form"), null, $(this).data("contextid"), $(this).data("contexttype"));
		});
	});
</script>