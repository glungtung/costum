<style type="text/css">
    footer.footer-cte{
        border-top: 2px solid #000091;
        border-bottom: 1px solid #cecece;
        color: #383838;
    }
    .row-footer {
        padding-top: 40px;
        padding-left: 0px;
        padding-right: 0px;
    }
    footer.footer-cte .img-section, footer.footer-cte .text-section {
        display: flex;
        flex-wrap: wrap;
    }
    footer.footer-cte .img-section img {
        height: 120px;
        width: auto;
        min-height: 100px;
    }
    footer.footer-cte .text-section p {
        font-size: 16px;
    }
    footer.footer-cte .footer-content-list {
        list-style-type: none;
        display: flex;
        flex-direction: row;
        align-self: center;
        flex-wrap: wrap;
        padding-inline-start: 0px;
    }
    footer.footer-cte .footer-content-link {
        margin-right: 16px;
        margin-top: 8px;
        margin-bottom: 8px;
    }
    @media (min-width: 768px) {
      footer.footer-cte .footer-content-link {
            margin-right: 1.5rem;
        }  
    }
    
    footer.footer-cte .footer-link {
        display: inline-flex;
        font-weight: bold;
        font-size: 16px;
        line-height: 24px;
        color: inherit;
        text-decoration: none;
    }
    footer.footer-cte .footer-bottom {
        border-top: 1px solid #cecece;
        margin-top: 16px;
        display: flex;
        flex-direction: row;
        align-items: center;
        flex-wrap: wrap;
    }
    footer.footer-cte .footer-bottom .footer-bottom-list{
        flex-wrap: wrap;
        align-items: center;
        padding: 8px 0 16px;
        width: 100%;
        margin: 0;
        list-style-type: none;
    }
    footer.footer-cte .footer-bottom .footer-bottom-link:first-child {
        margin: 8px 0 0;
    }
    footer.footer-cte .footer-bottom .footer-bottom-link:first-child:before {
        background-color: transparent;
        display: inline-block;
        margin-right: 0px;
    }
    footer.footer-cte .footer-bottom .footer-bottom-link {
        position: relative;
        margin: 8px 0 0 4px;
        display: inline;
    }
    footer.footer-cte .footer-bottom .footer-bottom-link .bottom-link {
        color: #6a6a6a;
        background-color: transparent;
        font-size: 12px;
        line-height: 20px;
    }
    footer.footer-cte .footer-bottom .footer-bottom-link:before {
        background-color: #cecece;
        display: inline-block;
        content: "";
        vertical-align: middle;
        position: relative;
        width: 1px;
        height: 16px;
        margin-right: 4px;
        margin-bottom: 8px;
        margin-top: 8px;
    }
    footer.footer-cte .footer-bottom .footer-bottom-link {
        position: relative;
        margin: 8px 0 0 4px;
        display: inline;
    }
    .footer-bottom-copy {
        color: #6a6a6a;
    }
    .footer-bottom-copy * {
        margin-bottom: 16px;
        margin-top: 5px;
        font-size: 12px;
        line-height: 20px;
    }
    .footer-bottom-copy p>a, footer.footer-cte .img-section, footer.footer-cte .text-section p>a {
        text-decoration: underline;   
    }

    @media (min-width: 768px) {
        footer.footer-cte .footer-bottom .footer-bottom-link:before {
            margin-right: 12px;
            margin-bottom: 4px;
            margin-top: 4px;
        }
        footer.footer-cte .footer-bottom .footer-bottom-link {
            margin: 8px 0 0 4px;
        }
    }
    @media (max-width: 991) {
        footer.footer-cte .text-section {
            margin-left: 0px;
            margin-right: 0px;
        }
    }
    @media (max-width: 767px) {
        footer.footer-cte .img-section, footer.footer-cte .text-section {
            min-height: 100px;
            padding-right: 0px;
            padding-left: 0px;
        }
        .row-footer {
            padding-top: 10px;
            padding-right: 0px;
            padding-left: 0px;
        }
        footer.footer-cte .img-section img {
            height: 100px;
        }

    }
</style>
<footer class="text-center col-xs-12 pull-left padding-15 footer-cte">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 row-footer">
        <div class="col-xs-12 col-md-12 col-md-6 col-lg-6 img-section">
            <a href="#welcome" class="lbh" >
                <img class="img-responsive"  src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Gouvernement_RVB.png">
            </a>
            <!-- <a href="territoires-transition@developpement-durable.gouv.fr" class="lbh-menu-app pull-left" target="_blanc">
                Nous contacter
            </a> -->
        </div>
        <div class="col-xs-12 col-md-12 col-md-6 col-lg-6 text-section text-left">
            <p>
                Ce site est géré par le <a href="https://www.ecologie.gouv.fr/" target="_blank" rel="noopener" title="Ministère de la transition écologique - nouvelle fenêtre">Ministère de la Transition écologique et de la Cohésion des territoires.</a> 
            </p>
            <ul class="footer-content-list">
                
                <li class="footer-content-link">
                    <a class="footer-link" href="https://legifrance.gouv.fr" target="_blank" rel="noopener" title="legifrance.gouv.fr - nouvelle fenêtre">legifrance.gouv.fr</a>
                </li>
                <li class="footer-content-link">
                    <a class="footer-link" href="https://gouvernement.fr" target="_blank" rel="noopener" title="gouvernement.fr - nouvelle fenêtre">gouvernement.fr</a>
                </li>
                <li class="footer-content-link">
                    <a class="footer-link" href="https://service-public.fr" target="_blank" rel="noopener" title="service-public.fr - nouvelle fenêtre">service-public.fr</a>
                </li>
                <li class="footer-content-link">
                    <a class="footer-link" href="https://data.gouv.fr" target="_blank" rel="noopener" title="data.gouv.fr - nouvelle fenêtre">data.gouv.fr</a>
                </li>

            </ul>
        </div>
        <div class="col-xs-12 no-padding footer-bottom text-left">
            <ul class="footer-bottom-list">
                <li class="footer-bottom-link">
                    <a class="bottom-link lbh-menu-app" href="#mention">Mentions légales</a>
                </li>
                <li class="footer-bottom-link">
                    <a class="bottom-link lbh-menu-app" href="#accessibilite"  rel="noopener">Accessibilité</a>
                </li>
                <!-- <li class="footer-bottom-link">
                    <a class="bottom-link" href="javascript:void(0)" onclick="javascript:tarteaucitron.userInterface.openPanel();return tag.click.send({elem:this, name:'personnaliser', level2:'8', chapter1:'footer', chapter2:'gestion_des_cookies', type:'navigation'});" data-smarttag-click="%7B%22name%22:%22personnaliser%22,%22chapter1%22:%22footer%22,%22chapter2%22:%22gestion_des_cookies%22,%22type%22:%22action%22%7D">Gestion des cookies</a>
                </li> -->
                <li class="footer-bottom-link">
                    <a class="bottom-link" href="mailto:territoires-transition@developpement-durable.gouv.fr" data-rawhref="mailto:territoires-transition@developpement-durable.gouv.fr" target="_blank">Contactez nous
                    </a>
                </li>
            </ul>
            <div class="footer-bottom-copy">
                <p>Sauf mention contraire, tous les textes de ce site sont sous licence <a class="rf-link-licence" title="Licence etalab-2.0 sur le site etalab - nouvelle fenêtre" target="_blank" rel="noopener" href="https://www.etalab.gouv.fr/licence-ouverte-open-licence">etalab-2.0</a>
                </p>
            </div>
        </div>
    </div>
</footer>
