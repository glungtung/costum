<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<div id="container" style="margin:20px auto;width:100%">
	<canvas id="canvas-bar"></canvas>
</div>
		
<script>
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};
	jQuery(document).ready(function() {
		mylog.log("render","/modules/graph/views/co/bar.php");
		// var barChartData = {
		// 	labels: ["Services aux territoires et citoyens",
		// 				"Economie verte et circulaire",	
		// 				"Eau, nature et biodiversité",
		// 				"Aménagement",	
		// 				"Mobilités",	
		// 				"Agriculture et alimentation",	
		// 				"Energie"],
		// 	datasets: [{
		// 		backgroundColor: COLORS,
		// 		borderWidth: 1,
		// 		data: [78,43,40,29,40,59,103]
		// 	}]

		// };
			var ctxContainer = document.getElementById('canvas-bar');
			var ctx = ctxContainer.getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barActionsCTErrData,
				options: {
					responsive: true,
					legend : {display:false}
				}
			});

			ctxContainer.onclick = function(evt) {
		      var activePoints = myBar.getElementsAtEvent(evt);
		      if (activePoints[0]) {
		        var chartData = activePoints[0]['_chart'].config.data;
		        var idx = activePoints[0]['_index'];

		        var label = chartData.labels[idx];
		        var value = chartData.datasets[0].data[idx];

		        //alert("todo sub graphs"+url);
		        smallMenu.openAjaxHTML( baseUrl+'/costum/ctenat/dashboard/tag/'+label+"/slug/"+contextData.slug);
		      }
		    };

});
	</script>
