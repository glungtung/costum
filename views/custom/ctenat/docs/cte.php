<?php 
/* TODO 
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>

<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #1b7baf;
    border-radius: 20px;
    padding: 20px !important;
    color: white;
    cursor: pointer;
    border:3px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #1b7baf;
    background-color: white;
    color: #1b7baf;
  }
  .ourvalues img{
    height:70px;
  }
  .main-title{
    color: #1F2532;
  }

  .ourvalues h3{
    font-size: 36px;
  }
  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  .bullet-point{
      width: 5px;
    height: 5px;
    display: -webkit-inline-box;
    border-radius: 100%;
    background-color: #6BB3C1;
  }
  .text-explain{
    color: #555;
    font-size: 18px;
  }
  .blue-bg {
  background-color: white;
  color: #5b2549;
  height: 100%;
  padding-bottom: 20px !important;
}

.circle {
  font-weight: bold;
  padding: 15px 20px;
  border-radius: 50%;
  background-color: #fea621;
  color: white;
  max-height: 50px;
  z-index: 2;
}
.circle.active{
      background: #ea4335;
    border: inset 3px #ea4335;
    max-height: 70px;
    height: 70px;
    font-size: 25px;
    width: 70px;
}
.support-section{
  background-color: white;
}
.support-section h2{
  text-align: center;
    padding: 60px 0px !important;
    background: #1F2532;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.timeline-ctc h2{
 text-align: center;
    padding: 105px 0px 60px 0px !important;
    background: #1F2532;
    font-size: 40px;
    color: white;
    margin-bottom: 20px;
}
.how-it-works.row {
  display: flex;
}
.row.timeline{
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -15px;
  margin-left: -15px;
}
.how-it-works.row .col-2 {
  display: inline-flex;
  align-self: stretch;
  position: relative;
  align-items: center;
  justify-content: center;
}
.how-it-works.row .col-2::after {
  content: "";
  position: absolute;
  border-left: 3px solid #0091c6;
  z-index: 1;
}
.pb-3, .py-3 {
    padding-bottom: 1rem !important;
}
.pt-2, .py-2 {
    padding-top: 0.5rem !important;
}
.how-it-works.row .col-2.bottom::after {
  height: 50%;
  left: 50%;
  top: 50%;
}
.how-it-works.row.justify-content-end .col-2.full::after {
  height: 100%;
  left: calc(50% - 3px);
}
.how-it-works.row .col-2.full::after {
    height: 100%;
    left: calc(50% - 0px);
}
.how-it-works.row .col-2.top::after {
  height: 50%;
  left: 50%;
  top: 0;
}

.timeline div {
  padding: 0;
  height: 40px;
}
.timeline hr {
  border-top: 3px solid #0091c6;
  margin: 0;
  top: 17px;
  position: relative;
}
.timeline .col-2 {
  display: flex;
  overflow: hidden;
  flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.align-items-center {
    -ms-flex-align: center !important;
    align-items: center !important;
}
.justify-content-end {
    -ms-flex-pack: end !important;
    justify-content: flex-end !important;
}
.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}
.how-it-works.row .col-6 p{
  color: #444;
}
.how-it-works.row .col-6 h5{
font-size: 17px;
    text-transform: inherit;
}
.col-2 {
    -ms-flex: 0 0 16.666667%;
    flex: 0 0 16.666667%;
    max-width: 16.666667%;
}
.col-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
}
.timeline .col-8 {  
    flex: 0 0 66.666667%;
    max-width: 66.666667%;
}
.timeline .corner {
  border: 3px solid #0091c6;
  width: 100%;
  position: relative;
  border-radius: 15px;
}
.timeline .top-right {
  left: 50%;
  top: -50%;
}
.timeline .left-bottom {
  left: -50%;
  top: calc(50% - 3px);
}
.timeline .top-left {
  left: -50%;
  top: -50%;
}
.timeline .right-bottom {
  left: 50%;
  top: calc(50% - 3px);
}

  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
  

</style>


<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  
    <div class="col-xs-12 text-center margin-bottom-50" style="padding:0px;">
    <img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/banner.jpg'> 
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#1F2532; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                
                <small style="text-align: left">
                <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
                  <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
  color: #2b2b2b;
  text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">


<!--
<span class="main-title" style="font-family:'Pacifico', Helvetica, sans-serif;color:#65BA91;font-size: 50px; font-style: italic;">#iciOnAccélère</span><br>
<br/>
-->

  « Le contrat de transition écologique illustre la méthode souhaitée par le
Gouvernement pour accompagner les territoires : une coconstruction avec les
élus, les entreprises et les citoyens qui font le pari d’une transition écologique
génératrice d’activités économiques et d’opportunités sociales. »</bloquote>

              </h3>
              <div  style="text-align: right; font-size: 1.4em;padding-right:30px;">
                <b>Emmanuelle Wargon</b>,<br/>
                secrétaire d’État auprès du ministre d’État, ministre de la Transition écologique et solidaire
                  </small>
              </div>
              <br/>




                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
              





              <div class="col-md-10 col-md-offset-1 col-xs-12">
                
                <span class="text-explain">
                  <br/>
                  <h2 style="color: #1A5D98">UN CONTRAT ADAPTE AU TERRITOIRE</h2>


                  Lancés en 2018, les <b>contrats de transition écologique</b>
                  (CTE) traduisent les engagements environnementaux
                  pris par la France (Plan climat, COP21, One Planet
                  Summit) au niveau local. Ce sont des outils au
                  service de la transformation écologique de territoires
                  volontaires, autour de projets durables et concrets.

                  <br/><span class="bullet-point"></span><br/>

                  Mis en place par une ou plusieurs intercommunalités,
                  le CTE est coconstruit à partir de projets locaux, entre
                  les collectivités locales, l’État, les entreprises, les
                  associations... Les territoires sont accompagnés aux
                  niveaux technique, financier et administratif, par les
                  services de l’État, les établissements publics et les
                  collectivités. Signé après six mois de travail, le CTE fixe
                  un programme d’actions avec des engagements précis
                  et des objectifs de résultats.

                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
                <br/>
                <h2 style="color: #1A5D98">TROIS OBJECTIFS</h2>
                <br/>
                <b>1) Démontrer  par  l’action  que  l’écologie  est  un  moteur  de  l’économie,  et  développer  l’emploi  local  par  la  transition  écologique</b>  (structuration  de filières, création de formations).

                <br/><span class="bullet-point"></span><br/>

                <b>2) Agir avec tous les acteurs du territoire, publics comme privés</b> pour traduire concrètement la transition écologique.

                <br/><span class="bullet-point"></span><br/>

                <b>3) Accompagner de manière opérationnelle les
                situations de reconversion industrielle d’un ter-
                ritoire</b> (formation professionnelle, reconversion de
                sites).



                </span>

                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">

                Pour préparer votre candidature, veillez à télécharger la présentation du contenu attendu. <a href="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/Formulaire candidature.pdf" style="background-color: #65BA91" class="btn" target="_blank"><i class="fa fa-download"></i></a><br/><br/>

              </div>
            </div>
            <div class="text-center col-xs-12">
            <br/><br/>
            <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/city.png"></div>
        </div>
          </div>

        </div>

      </div>
      

    </div>
  </div>


