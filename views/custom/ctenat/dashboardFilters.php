<div id="filters-dash" class="col-xs-12 searchObjCSS">
    
</div>

<div id="dashboardContainer">

</div>

<script type="text/javascript">
    //alert("dashboardFilters");
    var validStates = <?php echo json_encode( Ctenat::$validActionStates) ?>;
    var paramsSearch = {
        container : "#filters-dash",
        urlData : baseUrl+"/costum/ctenat/dashgraph",
        loadEvent: {
            default : "dashboard",
            eventType: "scroll",
            eventTarget: null,  
        },
        defaults : {
            notSourceKey : true,
            forced : {
                filters : { "priorisation" : { "$in" : validStates } }
            }
        },
        dashboard : {
            options:{}
        }, 
        results : {
            dom : "#dashboardContainer"
        },
        filters : {
            //text : true,
            ////scope : true,
            // scopeList : {
            //     name : "Region",
            //     params : {
            //         countryCode : ["FR", "RE"],
            //         level : ["3"]
            //     }
            // }
                    
        }
    };

    var filterSearch={};

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/ctenat/dashboardFilters.php");
        filterSearch = searchObj.init(paramsSearch);
        filterSearch.search.init(filterSearch);
    });

    
</script>