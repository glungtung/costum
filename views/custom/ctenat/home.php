<?php 
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  $cmsList = PHDB::find(Cms::COLLECTION,array( "parent.".$this->costum["contextId"] => array('$exists'=>1)));
}
$params = [
  "tpl" => $el["slug"],
  "slug"=>$this->costum["slug"],
  "canEdit"=>true,
  "blockCms" => $cmsList,
  "el"=>$el ]; 
  echo $this->renderPartial("costum.views.tpls.tplsEngine", $params,true );
  ?>
<script type="text/javascript">

  jQuery(document).ready(function() {
    mylog.log("render","/modules/costum/views/custom/ctenat/home.php");
    
    setTitle("COMETE : la Communauté Ecologie et Territoires ");

    $.each($(".markdown"), function(k,v){
      descHtml = dataHelper.markdownToHtml($(v).html()); 
      $(v).html(descHtml);
    });
    contextData = {
      id : "<?php echo $this->costum["contextId"] ?>",
      type : "<?php echo $this->costum["contextType"] ?>",
      name : "<?php echo $el['name'] ?>",
      slug : "<?php echo $el['slug'] ?>",
      profilThumbImageUrl : "<?php
      if (isset($el['profilThumbImageUrl'])) {
        echo $el['profilThumbImageUrl'];
      } ?>"
    };
   
  });

</script>
