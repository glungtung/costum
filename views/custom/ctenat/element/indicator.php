<?php 
$id=(string)$element["_id"]; ?>
<style>
	#modal-preview-coop{
		overflow: auto;
	}
	#poi .title{
		padding-bottom: 15px !important;
    	border-bottom: 1px solid rgba(150,150,150, 0.3);
	}
	.short-description{
		font-size:20px;
		text-align: justify;
	}
	.description-preview{
		text-align: justify;
	}
	.description-preview.activeMarkdown p{
		font-size: 14px !important;
	}
</style>

<div class="margin-top-25 margin-bottom-50 col-xs-12">
	<div class="col-xs-12 no-padding">
		<?php if( $element["creator"] == Yii::app()->session["userId"] || 
				  Authorisation::canEditItem( Yii::app()->session["userId"], "poi", $id, @$element["parentType"], @$element["parentId"] ) ){ ?>
			
			<button class="btn btn-default pull-right margin-right-10 text-red deleteThisBtn" 
					data-type="poi" data-id="<?php echo $id ?>" style="margin-top:-15px;">
				<i class=" fa fa-trash"></i>
			</button>
			<button data-subtype="indicator" data-type="poi" data-id="<?php echo $id ?>" class="btn btn-default pull-right margin-right-10 btn-edit-preview" style="margin-top:-15px;">
				<i class="fa fa-pencil"></i>
			</button>
			
		<?php } ?>
		<!-- <h3 class="text-center letter-green"><i class="fa fa-map-marker"></i> Point d'intéret</h3> -->
		<div id="poi">
			<h2 class="col-xs-12 title text-dark no-padding"><?php echo $element["name"] ?></h2>

			<?php
				if(!empty( $element["nameLong"]))
					echo '<h5 class="col-xs-12 title text-dark no-padding">'.$element["nameLong"].'</h5>';

				if(!empty( $element["typectenat"]))
					echo '<div class="col-xs-12 margin-10"><span>Type :</span> '.$element["typectenat"].'</div>';
				if(!empty( $element["definition"]))
					echo '<div class="col-xs-12 margin-10"><span>Définition :</span> '.$element["definition"].'</div>';
				if(!empty( $element["methodeCalcul"]))
					echo '<div class="col-xs-12 margin-10"><span>Méthode de calcule :</span> '.$element["methodeCalcul"].'</div>';
				if(!empty( $element["unity1"]))
					echo '<div class="col-xs-12 margin-10"><span>Unité 1 :</span> '.$element["unity1"].'</div>';
				if(!empty( $element["unity2"]))
					echo '<div class="col-xs-12 margin-10"><span>Unité 2 :</span> '.$element["unity2"].'</div>';

				if(!empty( $element["sourceData1"]))
					echo '<div class="col-xs-12 margin-10"><span>Source de la donnée :</span> '.$element["sourceData1"].'</div>';

				if(!empty($element["echelleApplication"])){

					echo '<div class="col-xs-12 margin-10"><span>Echelle d\'application :</span> ' ;

					if(is_string($element["echelleApplication"]))
						echo $element["echelleApplication"];
					else {
						foreach ($element["echelleApplication"] as $key => $val) {
							if(!empty($val)){
								if($key != 0 && $key != (count($element["echelleApplication"])-1) )
									echo ", ";
								echo $val ;
							}
						}
					}
					echo '</div>';
				}

				if(!empty( $element["periode"])){

					echo '<div class="col-xs-12 margin-10"><span>Source de la donnée :</span> ' ;
					foreach ($element["periode"] as $key => $val) {
						if(!empty($val)){
							if($key != 0 && $key != (count($element["periode"])-1) )
								echo ", ";
							echo $val ;
						}
						
					}
					echo '</div>';
				}

				if(!empty( $element["perimetreGeo"])){

					echo '<div class="col-xs-12 margin-10"><span>Perimètre Geo :</span> ' ;
					foreach ($element["perimetreGeo"] as $key => $val) {
						if(!empty($val)){
							if($key != 0 && $key != (count($element["perimetreGeo"])-1) )
								echo ", ";
							echo $val ;
						}
					}
					echo '</div>';
				}


				if(!empty( $element["origin"])){

					echo '<div class="col-xs-12 margin-10"><span>Origin :</span> ' ;
					foreach ($element["origin"] as $key => $val) {
						if(!empty($val)){
							if($key != 0 && $key != (count($element["origin"])-1) )
								echo ", ";
							echo $val ;
						}
					}
					echo '</div>';
				}

				if(!empty( $element["domainAction"])){

					echo '<div class="col-xs-12 margin-10"><span>Domaine d\'action :</span> ' ;
					if(is_string($element["domainAction"]))
						echo $element["domainAction"];
					else {
						foreach ($element["domainAction"] as $key => $val) {
							if(!empty($val)){
								if($key != 0 && $key != (count($element["domainAction"])-1) )
									echo ", ";
								echo $val ;
							}
						}
					}
					echo '</div>';
				}


				if(!empty( $element["objectifDD"])){

					echo '<div class="col-xs-12 margin-10"><span>Objectif DD :</span> ' ;
					if(is_string($element["objectifDD"]))
						echo $element["objectifDD"];
					else {
						foreach ($element["objectifDD"] as $key => $val) {
							if(!empty($val)){
								if($key != 0 && $key != (count($element["objectifDD"])-1) )
									echo ", ";
								echo $val ;
							}
						}
					}
					echo '</div>';
				}
			?>
			
			<?php if(@$element["shortDescription"]){ ?>
			<span class="col-xs-12 short-description margin-bottom-15 no-padding"><?php echo $element["shortDescription"] ?></span>
			<?php } ?>
			
		</div>
		
        <!--<a href="javascript:;" onclick="dySObj.openSurvey('octosource','json')" class="btn btn-primary col-xs-12"  style="width:100%">C'est parti <i class="fa fa-arrow-circle-right fa-2x "></i></a>-->
	</div>
</div>

<script type="text/javascript">

	var poiAlone=<?php echo json_encode($element); ?>;

	jQuery(document).ready(function() {	
		setTitle("", "", poiAlone.name);
		poiAlone["typePoi"] = poiAlone.type;
		poiAlone["type"] = "poi";
		poiAlone["typeSig"] = "poi";
		mylog.log("preview poiAlone", poiAlone);
		poiAlone["id"] = poiAlone['_id']['$id'];
		// if($.inArray(poiAlone.typePoi, ["article", "measure"])<0){
		// 	var html = directory.preview(poiAlone);
		//   	$("#poi").html(html);
		// }
	  	// if($("#poi #container-element-accordeon").length > 0){
	  	// 	params={
	  	// 		"images": [],
	  	// 		"medias": []
	  	// 	};
	  	// 	if(typeof poiAlone.images != "undefined")
	  	// 		params.images=poiAlone.images;
	  	// 	if(typeof poiAlone.medias != "undefined")
	  	// 		params.medias=poiAlone.medias;
	  	// 	ajaxPost("#poi #container-element-accordeon", baseUrl+'/'+moduleId+'/pod/slidermedia', params, function(){},"html");
	  	// }
	  	directory.bindBtnElement();
	  	if($(".description-preview").hasClass("activeMarkdown")){
	  		descHtml = dataHelper.markdownToHtml($(".description-preview").html());
	  		$(".description-preview").html(descHtml);
	  	}

	  	$("#modal-preview-coop .btn-close-preview, .deleteThisBtn").click(function(){
			
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
		});

		$(".btn-edit-preview").click(function(){
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
			//key=(typeof costum != "undefined" && typeof costum.typeObj != "undefined"
				//&& typeof costum.typeObj[poiAlone.typePoi] != "undefined") ? poiAlone.typePoi : "poi";
			dyFObj.editElement("poi", poiAlone.id, poiAlone.typePoi );
		});
		
	  	mapCO.addElts(new Array(poiAlone));
	});
</script>