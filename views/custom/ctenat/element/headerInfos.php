<?php if(@$element["category"] && $element["category"]=="ficheAction" && $element["collection"]!=Organization::COLLECTION){ 
	if(isset($element["links"]) && isset($element["links"]["projects"])) { 
		$count=1;
		$countCter=count($element["links"]["projects"]);
        foreach($element["links"]["projects"] as $k => $v){ 
            $ter=Element::getElementById( $k, Project::COLLECTION, null, array("name", "slug","profilThumbImageUrl", "profilMediumImageUrl"));
            $ter["id"]=$k;
            $cteRParent=array_merge($element["links"]["projects"][$k], $ter); 
       
		    if(isset($cteRParent)){ 
				$imgPath = (@$cteRParent["profilMediumImageUrl"] && !empty($cteRParent["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$cteRParent["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_'.$v["type"].'.png'; 
				if($count==1){ ?> 
				<div class="section-date pull-right" style="padding: 0px;background: none;top: 10px;position: absolute;right: 10px;margin: 0px;float:right;">
				<?php } ?>
					<a href="#page.type.<?php echo Project::COLLECTION ?>.id.<?php echo $cteRParent["id"] ?>" class="lbh-preview-element text-white pull-right margin-right-5" style="height:70px;">
						<img class="img-circle tooltips" height="50" width="50" src="<?php echo $imgPath ?>" data-toggle="tooltip" data-placement="left" title="Déposée sur <?php echo $cteRParent["name"] ?>">
					</a>
					<?php if($countCter==$count){ ?> 
					<?php if($element["collection"]==Project::COLLECTION && $element["category"]=="ficheAction" 
						&& isset($element["links"]) && isset($element["links"]["answers"]) && !empty($element["links"]["answers"])){
							foreach($element["links"]["answers"] as $k => $v){
								$idanswer=$k;
								break;
							}
							$status=Form::getAnswerById($idanswer,array("priorisation"));
							if(!empty($status["priorisation"])){ ?>
							<div class="section-date pull-right" style="padding: 5px 10px;position: relative;padding: 5px 10px;top: inherit;margin-top: 8px;">
								<span  style="color: #5fad88;font-variant: small-caps;font-size: 18px;font-weight: 800 !important"><?php echo $status["priorisation"] ?></span>
							</div>
							<?php }

						} 
					} ?>
				<?php if($countCter==$count){ ?>
					</div>
				<?php } ?>
<?php 		} 
			$count++;
 		} 
 	}
}
// echo "ZFZFZFZFEZGREZGr ez";
// 		var_dump($cteRParent);
 ?>
<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">	
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 text-white pull-right">
		<?php if (@$element["status"] == "deletePending") { ?> 
			<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
		<?php } ?>
		<h4 class="text-left padding-left-15 pull-left no-margin">
			<span id="nameHeader">
				<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">
					
				</div>
				<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i> 
				<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
			</span>
			<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
					$typesList=Event::$types;
			?>
				<span id="typeHeader" class="margin-left-10 pull-left">
					<i class="fa fa-x fa-angle-right pull-left"></i>
					<div class="type-header pull-left">
				 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
				 	</div>
				</span>
			<?php }  ?>
		</h4>	

		<?php 
		if($edit){ 
			if (@$element["category"] == "cteR") {?>
			 <button class="btn-danger text-white pull-right" style="border: none;padding: 5px 10px;border-radius: 5px;" onclick="directory.deleteElement('<?php echo $element["collection"] ?>', '<?php echo (string)$element["_id"] ?>');"><i class="fa fa-trash"></i> Supprimer</button> 
		<?php } 
		} 
		?>				
	</div>

<?php 
	$classAddress = ( (@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden" );

	if(!empty($cteRParent))
		$classAddress = "";
//if(@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]){ ?>
	<div class="header-address-tags col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right margin-bottom-5 <?php echo $classAddress ; ?>">
		<?php
		 if(!empty($element["address"]["addressLocality"])){ ?>
			<div class="header-address badge letter-white bg-red margin-left-5 pull-left">
				<?php
					echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
					echo !empty($element["address"]["postalCode"]) ? 
							$element["address"]["postalCode"].", " : "";
					echo $element["address"]["addressLocality"];
				?>
			</div>
			<?php $classCircleO = (!empty($element["tags"]) ? "" : "hidden" ); ?>
				<span id="separateurTag" class="margin-right-10 margin-left-10 text-white pull-left <?php echo $classCircleO ; ?>" style="font-size: 10px;line-height: 20px;">
					<i class="fa fa-circle-o"></i>
				</span>
			
		<?php } ?>
		<div class="header-tags pull-left">
		<?php 
		//var_dump($element["category"]);
		if (@$element["category"] == "ficheAction") {
	
			$countdport = count($element["links"]["projects"]);
			if (@$countdport > 1) {  ?>
				<div class="dropdown">
				  <button class="btn dropdown-toggle btn-dp" type="button" data-toggle="dropdown" >Dispositif Porteur
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				  <?php 
					  foreach($element["links"]["projects"] as $k => $v){ 
					  	$dport =Element::getElementById( $k, Project::COLLECTION, null, array("name", "slug"));
					    echo '<li><a class="lbh" href="#page.type.'.Project::COLLECTION.'.id.'.$k.'">'.$dport ["name"].'</a></li>';
				  } ?>
				  </ul>
				</div>
			<?php } 
			else if ($countdport == 1) {
				
				foreach($element["links"]["projects"] as $key => $val){ 
					$dport =Element::getElementById( $key, Project::COLLECTION, null, array("name", "slug"));
					echo '<a class="btn lbh btn-dp" href="#page.type.'.Project::COLLECTION.'.id.'.$key.'">'.$dport ["name"].'</a>';
				}
			}
		}
		/*if(@$cteRParent){
			echo '<a href="#page.type.'.Project::COLLECTION.'.id.'.$cteRParent["id"].'" style="color: white !important;font-size: 16px;font-weight: 800;" class="lbh text-white">'.$cteRParent["name"].'</a>';
		}  */
		if(@$element["tags"]){ 
			foreach ($element["tags"] as $key => $tag) { 
				if(!in_array($tag,["cte", "candidat"]) && !empty($tag)){ ?>
				<!--<a  href="#search?text=#<?php echo $tag; ?>"  class="badge letter-red bg-white lbh" style="vertical-align: top;">#<?php echo $tag; ?></a>-->
			<?php } 
			}
		} ?>
		</div>
	</div>

<!--<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right">
	<span class="pull-left text-white" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
	</span>	
</div>-->
<?php if(@$element["source"] && @$element["source"]["status"] 
	&& @$element["source"]["status"][$this->costum["slug"]] &&
	is_string($element["source"]["status"][$this->costum["slug"]]) && isset($element["category"]) && $element["category"]=="cteR"){ ?>
		<div class="section-date pull-right" style="padding: 5px 10px;">
			<span  style="color: #5fad88;font-variant: small-caps;font-size: 18px;font-weight: 800 !important"><?php echo $element["source"]["status"][$this->costum["slug"]] ?></span>
		</div>
<?php } ?>
<?php if( ( $edit || $openEdition ) && !empty(Yii::app()->session["userId"])){ 
	$href=(isset($bannerConfig["editButton"]) 
		&& isset($bannerConfig["editButton"]["dynform"]) 
		&& $bannerConfig["editButton"]["dynform"]) ? "javascript:dyFObj.editElement('".Element::getControlerByCollection($element["collection"])."', '".@$elementId."');" : "javascript:;";
	?>
	<!--<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right">
		<a href="<?php echo $href ?>" class="pull-left btn letter-blue bg-white" id="btnHeaderEditInfos">
			<i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit information") ?>
		</a>	
	</div>-->
<?php }
if(in_array($element["collection"], [Event::COLLECTION])){ 
	if(@$element['parent'] || @$element['organizer'] ){ ?>
		<div class="section-date pull-right">
			<div class="event-infos-header"  style="font-size: 14px;font-weight: none;"></div>
			<div style="font-size: 14px;font-weight: none;">
				<div id="parentHeader" >
					<?php if(@$element['parent']){
						$count=count($element["parent"]);
						$msg = ($element["collection"]==Event::COLLECTION) ? Yii::t("common","Planned on") : Yii::t("common","Carried by") ;
						echo $msg. " : ";
						foreach($element['parent'] as $key =>$v){
							$heightImg=($count>1) ? 35 : 25;
							$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
							<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
								class="lbh tooltips"
								<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
								<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
								<?php  if ($count==1) echo $v['name']; ?>
							</a>
							 
					<?php } ?> <br> <?php } ?>
				</div>
				<div id="organizerHeader" >
					<?php if(@$element['organizer']){
							$count=count($element["organizer"]);
							echo Yii::t("common","Organized by"). " : ";
							foreach($element['organizer'] as $key =>$v){
								$heightImg=($count>1) ? 35 : 25;
								$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
							<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
									class="lbh tooltips"
									<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
									<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
									<?php  if ($count==1) echo $v['name']; ?>
								</a>
								 
						<?php } } ?>
				</div>
			</div>
	    </div>
	<?php }
	} 
	echo $this->renderPartial('co2.views.element.menus.answerInvite', 
    			array(  "invitedMe"      => $invitedMe,
    					"element"   => $element
    					) 
    			); 
 	if(@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]){ ?>
		<div class="pull-right col-xs-12 col-sm-9 col-lg-10 margin-bottom-5 margin-top-5 shadow2 boxBtnLink">
	        	<?php echo $this->renderPartial('co2.views.element.menus.links', 
	    			array(  "linksBtn"      => $linksBtn,
	    					"element"   => $element,
	    					"openEdition" => $openEdition,
	    					"options" => array("invite"=>false) ) 
	    			); 
	    			if(Authorisation::canEditItem(Yii::app()->session["userId"], $element["collection"] ,(string)$element["_id"])){
	    				echo $this->renderPartial('co2.views.element.menus.inviteBtn', 
					    			array(  
					    				"contextType"      => $element["collection"],
				    					"contextId"   => (string)$element["_id"],
				    					"class"=> "menu-linksBtn",
										"tooltip"=>false,
										"separator"=> false
					    			) 
					    	);
	    			}
	    		?>

		</div>
	<?php } ?>
</div>
<?php if(isset($element["category"]) && $element["category"]=="cteR") {
	$badges=Badge::getByWhere(array("parent.".(string)$element["_id"] => array('$exists'=>true)));
}?>

<script type="text/javascript">var cterOrientations=<?php echo json_encode(@$badges); ?>;</script>
