<style type="text/css">
	.sliderMedia #editSliderAlbum{
		background-color: #000091;
	    color: white;
	    border-radius: 3px;
	    margin-right: 2px;
	    position: absolute;
	    top: 5px;
	    left: 5px;
	    padding: 3px 10px;
	    border: none;
	}
	.description-section .title{
		margin-top: 0px;
		text-transform: initial;
	}
	.divCommunityHome{
		margin-top: 25px;
	}
	.divCommunityHome .labelPod{
		font-size: 22px;
		padding-bottom: 10px;
	}
	#partnersCTE .labelPod{
		font-size: 18px;
	}
	
	.filRougeTitle{
		text-transform: initial;
		font-size: 20px;
	}
	.contentBadgeHome .badge{
		padding: 15px 65px;
		font-size: 18px;
		margin-right: 10px;
		border-radius: 50px;
	}
	.contentFilRouge hr{
		border-top-width: 3px;
	}
	.btn-calling-projects{
		 padding: 10px 35px;
	    background-color: #000091;
	    color: white;
	    border-radius: 20px;
	    font-size: 20px;
	    text-transform: uppercase;
	}
</style>
<?php 
  $el["type"]=Project::COLLECTION;

if($el["category"]=="ficheAction"){ ?>
	<div class="col-xs-12">
	<?php if(isset($cteRParent)) { ?>
			<div class="col-xs-12 contentBadgeHome no-padding margin-bottom-15 margin-top-10">
				<a href="#page.type.<?php echo Project::COLLECTION ?>.id.<?php echo $cteRParent["id"] ?>" class="lbh badge" style="padding: 5px 15px"><i class="fa fa-sign-out"></i> Retour au territoire</a>
			</div>
	<?php } ?>
	</div> 
<?php } ?> 
<div class="col-xs-12 col-sm-6 col-md-4">
	<div class="sliderMedia col-xs-12 no-padding">
		<?php if(isset($images) && !empty($images)){
			$label="Modifier les images";
			echo $this->renderPartial('co2.views.pod.sliderMedia', 
								array(
									  "medias"=>[],
									  "images" => @$images,
									  "simplePuce"=>true
									  ) ); 
		 }else{
		 	$label="Ajouter les images"; 
		  ?>
		  <img class="img-responsive" src="<?php echo $this->module->getParentAssetsUrl().'/images/thumbnail-default.jpg' ?>"/>
		   <?php } ?>
		<?php if(Authorisation::canEditItem(Yii::app()->session["userId"], $el["type"] ,(string)$el["_id"])){ ?>
			<button id="editSliderAlbum"><?php echo $label ?></button>
		<?php } ?>
	</div>
	
	<?php if($el["category"]=="cteR") { ?>
		<div id="porteurCTE" class="divCommunityHome col-xs-12 no-padding">
			
			<span class="labelPod col-xs-12 text-center text-gray"><i class="fa fa-user"></i> Porteur(s) du dispositif</span><br/>
			<?php if(isset($porteurCTE)){
				$count=count($porteurCTE); ?>
				<div class="col-xs-12 text-center">
					<?php foreach($porteurCTE as $key =>$v){
						$heightImg=($count>1) ? 35 : 25;
						$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_'.$v["type"].'.png' ?> 
						<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
							class="lbh-preview-element tooltips"
							<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.@$v["name"].'"' ?>> 
							<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
							<?php  if ($count==1) echo @$v['name']; ?>
						</a>
				 	<?php } ?> 
				</div>
			<?php } ?>
		</div>
		<div id="partnersCTE" class="divCommunityHome col-xs-12 no-padding">
			<span class="labelPod col-xs-12 text-center text-gray">Partenaires</span><br/>
		<?php if(@$partners){
				$count=count($partners); ?>
				<div class="col-xs-12 text-center">
					<?php foreach($partners as $key =>$v){
						$heightImg=50;
						$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_organizations.png' ?> 
					<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
						class="lbh-preview-element tooltips"
						<?php if($count>1) echo ' data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>> 
						<img src="<?php echo $imgPath ?>" class="" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
					</a>
					<?php } ?> 
				</div>

		<?php } ?>
		</div> 
	<?php }
	if($el["category"]=="ficheAction") { ?>
		<div id="porteurAction" class="divCommunityHome col-xs-12 no-padding">
			<span class="labelPod col-xs-12 text-center text-gray"><i class="fa fa-user"></i> Porteur(s) de l'action</span><br/>
			<?php if(isset($porteurAction)){
				$count=count($porteurAction); ?>
			<div class="col-xs-12 text-center">
					<?php foreach($porteurAction as $key =>$v){
						$heightImg=($count>1) ? 35 : 25;
						$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_'.$v["type"].'.png' ?> 
						<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
							class="lbh-preview-element tooltips"
							<?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.@$v["name"].'"' ?>> 
							<img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
							<?php  if ($count==1) echo @$v['name']; ?>
						</a>
				 	<?php } ?> 
				</div>
			<?php } ?>
		</div>
		<div id="partnersCTE" class="divCommunityHome col-xs-12 no-padding">
			<span class="labelPod col-xs-12 text-center text-gray">Financeurs</span><br/>
		<?php if(@$financers){
			$count=count($financers); ?>
			<div class="col-xs-12 text-center">
				<?php foreach($financers as $key =>$v){
					$heightImg=50;
					$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_organizations.png' ?> 
				<a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>" 
					class="lbh-preview-element tooltips"
					data-toggle="tooltip" data-placement="left" title="<?php echo $v["name"] ?>"> 
					<img src="<?php echo $imgPath ?>" class="" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
				</a>
				<?php } ?> 
			</div>
		<?php } ?>
		</div>
	<?php } ?>
</div>
<div  class="col-xs-12 col-sm-6 col-md-8 no-padding description-section">
	
<h3 class="title text-gray"> Bienvenue sur <?php echo $el["name"] ?></h3>

<?php 
if($el["category"]=="ficheAction"){
	 $dynFormSource = PHDB::findOne( Costum::COLLECTION , array("slug"=>"ctenat") );
	  $costumProperties = $dynFormSource["typeObj"]["projects"]["dynFormCostum"]["beforeBuild"]["properties"];
	  //echo $this->renderPartial("costum.views.tpls.elemCmsScriptBefore",null,true); 
	?>

	<script type="text/javascript">

	mylog.log("render","/modules/costum/views/custom/ctenat/element/home.php");
	dyFObj.dynFormSource = <?php echo json_encode($dynFormSource) ?>; 
	mylog.log("editProperty","home dyFObj.dynFormSource ",dyFObj.dynFormSource);
	dyFObj.dynFormCostumCMS = jQuery.extend(true, {}, dyFObj.dynFormSource.typeObj.projects.dynFormCostum); 
	mylog.log("editProperty","home dyFObj.dynFormCostum ",dyFObj.dynFormCostum);
	dyFObj.elementData = <?php echo json_encode($el) ?>; 
	mylog.log("editProperty","home dyFObj.elementData ",dyFObj.elementData);

	</script>

	<?php
	$field = ($el["category"]=="cteR") ? "why" : "description";
	$title=($el["category"]=="cteR") ? "Contexte et motivations": "";
	$params = array(
	  "value"           => @$el[$field],
	  "title"           => $title,
	  "collection"      => $el["type"],
	  "id"              =>(string)$el["_id"],
	  "field"           => $field,
	  "dynFormProperty" => ($el["category"]=="cteR") ? $costumProperties[$field] : null,
	  "color"           => "#9ea2a8"
	);
	echo $this->renderPartial("costum.views.tpls.elemText",$params,true); 
	//echo $this->renderPartial("costum.views.tpls.elemCmsScript",$params,true); 
}else{ ?>
	<div class="description-markdown-territory markdown"><?php echo @$el["why"] ?></div>
	<?php if($el["category"]=="cteR" && Authorisation::canEditItem(Yii::app()->session["userId"], $el["type"] ,(string)$el["_id"])) { ?>
		<a href="javascript:;" class="edit-territory btn-edit-element margin-top-10 margin-bottom-10 col-xs-12 no-padding" data-id="<?php echo (string)$el["_id"] ?>" data-type="<?php echo Project::COLLECTION ?>"><i class="fa fa-pencil"></i> Editer les informations du territoire</a>
	<?php }
} ?>
	
</div>
<?php if(isset($el["showAAP"]) && $el["showAAP"]){ ?>
	<div class="col-xs-12  margin-top-20" style="background-color: #000091;height:3px;"></div>
	<div class="col-xs-12 text-center margin-top-20 margin-bottom-20">
  		<button class="ssmla btn col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 margin-top-10" data-view="ficheAction" style="background: #000091;color: white;font-size: 19px;font-weight: 800;">
		 	Déposer une action
		</button>
  		<span class="col-xs-12 padding-top-20" style="text-align:left; font-size: 16px;">
  			En tant que porteur du projet, vous pouvez déposer ici une proposition d'action qui sera travaillée avec la/les collectivités, les services et opérateurs de l'Etat. La sélection des actions s'effectuera sur leur opérationnalité, la qualification de leur impact au regard du projet du territoire et en particulier leur dimension de transition écologique (porteur et partenaires identifiés, financements acquis, calendrier de mise en oeuvre, indicateur de suivi). Si l’action n’est pas suffisamment mâture, elle pourra être travaillée pour intégrer par la suite le dispositif.


  		</span>
		
	</div>
	<!--<div class="col-xs-12  text-center">-->
	<?php 
	/* $answers = PHDB::find( Form::ANSWER_COLLECTION , array("formId"=>$el["slug"], "user"=>Yii::app()->session["userId"]) );
	if(!empty($answers)){
		echo "<br/><h5>Vous avez candidaté sur ce territoire:</h5><br/><table class='table table-striped table-bordered table-hover' >";
		echo "<tr>";
			echo "<th class='text-center'></th>";
			echo "<th class='text-center'>Le projet</th>";
			echo "<th class='text-center'>Porté par</th>";
			//echo "<th class='text-center'>Publier par</th>";
			echo "<th class='text-center'>Actions</th>";
			//echo "<th class='text-center'>Actions</th>";
		echo "</tr>";
		$i=1;
		foreach ($answers as $id => $a) {
			if(isset($a["answers"][$el["slug"]]["answers"]['project']['id']))
			{
				$proj = PHDB::findOne( Project::COLLECTION , array("_id"=>new MongoId($a["answers"][$el["slug"]]["answers"]['project']['id'])),['slug'] );
				echo "<tr>";

					echo "<td>".$i.".</td>";
					echo "<td><a class='lbh-preview-element' href='#page.type.projects.id.".@$a["answers"][$el["slug"]]["answers"]['project']['id']."'>".@$a["answers"][$el["slug"]]["answers"]['project']['name']."</a></td>";
					echo "<td><a class='lbh-preview-element' href='#page.type.organizations.id.".@$a["answers"][$el["slug"]]["answers"]['organization']['id']."'>".@$a["answers"][$el["slug"]]["answers"]['organization']['name']."</a></td>";
					//echo "<td><a href='/costum/co/index/id/ctenat#@".$a["user"]."'>".Yii::app()->session["user"]["name"]."</a></td>";
					echo "<td>".
							"<a href='#@".$proj['slug'].".view.answers'  class='lbh btn btn-default'><i class='fa text-red fa-pencil'></i> Reprendre</a>".
							"<a href='javascript:;' data-id='".$a['_id']."' class='deleteAnswer pull-right btn btn-default'><i class='fa text-red fa-trash'></i> Supprimer</a>".
						"</td>";
				echo "</tr>";
			} else 
				PHDB::remove( Form::ANSWER_COLLECTION , array("_id"=>new MongoId($a['_id'])) );
			$i++;
		}
		echo "</table>";
	} */
 ?>
<!--</div>-->
<?php } ?>
<div class="col-xs-12 margin-top-20" style="background-color: #8a8c8e;height:3px;"></div>
<?php if($el["category"]=="cteR"){ ?>
<div class="col-xs-10  col-sm-offset-1 text-center contentBadgeHome margin-top-20" >
	<h3>En Chiffres</h3>
	<?php 
	 

	$projects = PHDB::find( Project::COLLECTION, [
								  'category' => 'ficheAction',
								  'source.key' => 'ctenat',
								  'links.projects.'.(string)$el["_id"] => [ '$exists' => 1 ],
								  '$or' => [
								    [ 'preferences.private' => false],
								    [ 'preferences.private' => ['$exists' => 0 ] ],
								  ] ],["_id"] );
	
	$projIdList = array_keys($projects);
	// var_dump(count($projIdList));
	// var_dump($projIdList);exit;
	$answersList = PHDB::find(Form::ANSWER_COLLECTION,[
	 					"cterSlug"=>$el["slug"],
	 					"context.".(string)$el["_id"]=>['$exists'=>true],
	 					"priorisation" => ['$in'=>[ Ctenat::STATUT_ACTION_VALID,
	 												Ctenat::STATUT_ACTION_COMPLETED,
	 												Ctenat::STATUT_ACTION_CONTRACT ]],
	 					"answers.action.project.0.id"=>['$in'=>$projIdList]],["_id","answers.murir.planFinancement"]);
	$finance = Ctenat::chiffreFinancement($answersList,$el["slug"]);
	
	$answersReaCount = PHDB::count(Form::ANSWER_COLLECTION,[
	 					"cterSlug"=>$el["slug"],
	 					"priorisation" => Ctenat::STATUT_ACTION_COMPLETED,
	 					"answers.action.project.0.id"=>['$in'=>$projIdList]],["_id"]);
	
	$dashParams = [
		"population" => isset($el["nbHabitant"]) ? $el["nbHabitant"] : 0,
		"actions" => count($answersList),
		"actionsRea" => $answersReaCount,
		//"contributeurs" => "1,550",
		"financeLbl"=>$finance["lbl"],
		"finance" => $finance["total"],
		"public" => $finance["public"],
		"private" => $finance["private"]
	]; 
	echo $this->renderPartial("costum.views.custom.ctenat.cterrHPChiffres",$dashParams,true);  ?>
</div>
<?php } ?>

<?php if($el["category"]=="cteR"){ ?>

<?php }else if($el["category"]=="ficheAction" && !empty($badges)){ 
	?>
	<script type="text/javascript">var badges=<?php echo json_encode($badges); ?>;</script>
	<?php $DA=array();
	$DD=array();
	foreach($badges as $e => $v){
		if($v["category"]=="domainAction")
			$DA[$e]=$v;
		else if($v["category"]=="cibleDD")
			$DD[$e]=$v;
	} 
 	if(!empty($DA)){ ?>
		<div class="col-xs-12 text-center contentFilRouge no-padding badgesDA">
			<hr/>
			<h3 class="filRougeTitle col-xs-12 text-gray">Domaines d’action concernés</h3>
			<?php echo $this->renderPartial("costum.views.custom.ctenat.element.badges",array("el"=>$el, "badges"=>@$DA, "htmlContainer"=>".badgesDA"),true); ?>
		</div>
	<?php } 
	if(!empty($DD)){ ?>
	<div class="col-xs-12 text-center contentFilRouge no-padding badgesCibleDD">
		<hr/>
		<h3 class="filRougeTitle col-xs-12 text-gray">Cibles de développement durable retenues</h3>
		<?php echo $this->renderPartial("costum.views.custom.ctenat.element.badges",array("el"=>$el, "badges"=>@$DD,  "htmlContainer"=>".badgesCibleDD"),true); ?>
	</div>
<?php } 
} ?>

<script type="text/javascript">
var imagesAlbum = <?php echo json_encode(@$images) ?>; 
var surveyCter = <?php echo json_encode(@$survey) ?>;

jQuery(document).ready(function() {
	mylog.log("render","/modules/costum/views/custom/ctenat/element/home.php");



  $("#editSliderAlbum").click(function(){
  	dyFObj.openForm('addPhoto', null, {images : imagesAlbum},null,costum.typeObj['addPhoto']["dynFormCostum"]);
  });
  /*$("#myCarousel .carousel-inner .item").each(function(){
  	heightImg=$(this).find("img").height();alert(heightImg);
  	if(heightImg < 300){
  		paddingDif=(300-heightImg)/2;alert(paddingDif);
  		$(this).css({"padding-top": paddingDif+"px"})
  	}
  });*/
descHtml = dataHelper.markdownToHtml($(".description-markdown-territory").html());
$(".description-markdown-territory").html(descHtml);
descHtml = dataHelper.markdownToHtml($(".description-markdown-filRouge").html());
$(".description-markdown-filRouge").html(descHtml);
//dataHelper.activateMarkdown(".description-markdown-filRouge");
descHtml = dataHelper.markdownToHtml($("#elemTextdescription").html());
$("#elemTextdescription").html(descHtml);

  pageProfil.bindViewActionEvent();
 // coInterface.bindLBHLinks();
  directory.bindBtnElement();
  $('.deleteAnswer').off().click( function(){
      id = $(this).data("id");
      bootbox.dialog({
          title: "Confirmez la suppression de la fiche action",
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
                	//urlCtrl.loadByHash(location.hash);
                	pageProfil.views.home();
                },"html");
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });
});

</script>

