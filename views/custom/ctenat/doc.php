
<script type="text/javascript">
     var url = window.location.href;
     var newUrl;
     if(url.indexOf("test") > -1)
        newUrl = url.substring( 0, url.indexOf("test"));
    else
        newUrl = url;

    var sectionDyf = {};
    var tplCtx = {};
    var DFdata = {
      'tpl'  : costum.contextSlug,
      "id"   : "<?php echo $this->costum["contextId"] ?>"
    };
    costum.col = "<?php echo $this->costum["contextType"]  ?>";
    costum.ctrl = "<?php echo Element::getControlerByCollection($this->costum["contextType"]) ?>";

    var configDynForm = <?php echo json_encode((isset($this->costum['dynForm'])) ? $this->costum['dynForm']:null); ?>;
    var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;
</script>
<?php 
$cssJS = array('/js/docs/docs.js');
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl());

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"]))
    $el = Element::getByTypeAndId( $this->costum["contextType"], $this->costum["contextId"] );


if ( Authorisation::isCostumAdmin() )
	$canEdit = true;
else 
	$canEdit = false;
/* TODO 

[ ] reload is empty on hope page
[ ] reload on any page 
[ ] use categories or types to organize poi.docs 
[ ] create poi.timeline as an action plan 
[ ] edit poi.doc
[ ] delete poi.doc

wishlist 
[ ] poi.doc templates
[ ] move doc position index
[ ] use poi.html as piece of a page

*/

?>

<style type="text/css">

	/*#menu-left{
		
    	bottom: 0;
	    
	    padding: 0;
	    overflow-y: scroll;
	    	background-color: white;
	}*/
	#header-doc{

		height: 60px;
		padding-top: 10px;
		background-color: white;
	}
	#header-doc h2{
		float: left;
	    color: #354C57;
	    font-size: 20px;
	    font-variant: small-caps;
	    line-height: 41px;
	    padding: 0px 10px;
	}
	
	#menu-left > ul > li > a.active, #menu-left > ul > li > a:hover, #menu-left > ul > li > a:focus,
	#menu-left .navbar-default .navbar-nav>li>a:hover, #menu-left .navbar-default .navbar-nav>li>a:focus {
		text-decoration: none;
		background-color:#65BA91;
		color: #65BA91;
		font-size: 20px;
		border-right: 30px solid #65BA91;
		color: #fff;
	}
	ul.subMenu > li > a.active, ul.subMenu > li > a:hover, ul.subMenu > li > a:focus{
		/*border-right: 30px solid #65BA91;
		color: #fff;*/
		font-size:18px;
		text-decoration: none;
	}
	/*
	#menu-left ul li{
		list-style: none;
	}
	#menu-left > ul > li > a{
		font-size: 20px;
	}
	ul.subMenu > li > a{
		font-size:16px;
	}
	#menu-left > ul > li > a, ul.subMenu > li > a{
		color: #354C57;
		width: 100%;
	    float: left;
	    padding: 5px 20px;
	    text-align: left;
	}
	#menu-left ul li .subMenu, #menu-left > ul > li > a{
		border-bottom: 1px solid #ccc;
	}
	#menu-left ul li a.active span.text-red, #menu-left ul li a:hover span.text-red{
		color:#354C57 !important;
	}
	.close-modal{
		top: 10px !important;
    	right: 10px !important;
     	z-index: 100000000000000 !important;
    	position: fixed !important;
	}
	.close-modal .lr, .close-modal .rl{
		height: 40px !important;
	}
	*/
	ul.subMenu{
		/*display:none;*/

	}
	/*ul.subMenu{
		padding-left: 30px
	}*/
#show-menu-xs, #close-docs{
	    padding: 7px 15px;
    font-size: 20px;
}
.keypan .panel-heading{
	margin-top: 20px;
    min-height: 70px;
}
.keypan{
	border: none;
    margin-bottom: 10px;
    box-shadow: none;
}
.keypan, .keypanList{
	box-shadow: none;	
}
.keypanList .panel-title i{
	margin-right: 10px;
}
.keypanList .panel-body ul{
	padding-left: 0px;
}
.keypanList .panel-title span{
	font-size: 24px !important;
}
.keypan .panel-body{
	min-height: 200px;
}
.keypan hr {
	width: 75%;
    margin: auto;
}
#header-docs .panel-title, .subtitleDocs .panel-title {
	font-size: 40px;
}
#header-docs .panel-title .sub-title, .subtitleDocs .panel-title .sub-title{
	font-size: 20px !important;
	font-style: italic;	
}
#container-docs{
	/*background-color: white;*/
	/*padding-left: 50px;*/
}

/*@media (max-width: 991px) {
  #menu-left{
    width: 56%;
    left: -56%;
	bottom: 0px;
	}
  
}

@media (min-width: 991px) {
  #menu-left {
    left:0 !important;
  }
}*/

/*css by nicoss */
.btn-glyphicon { padding:8px; background:#ffffff; margin-right:4px;margin-top: 1px; }
.icon-btn { padding: 1px 15px 3px 2px; border-radius:50px;font-weight: 700;margin: 5px;}

/*Doc file*/
.job-box .img-holder {
  height: 65px;
  width: 65px;
  background-color: #4e63d7;
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
  background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
  color: #fff;
  font-size: 22px;
  font-weight: 700;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border-radius: 65px;
}
.job-box .img-holder i{
	font-size: 30px;
}

.job-box {
  -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
          box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
  border-radius: 10px;
  padding: 10px 35px;
}
.mb-30 {
    margin-bottom: 30px;
}
.mb-4, .my-4 {
    margin-bottom: 1.5rem!important;
}

.mt-4, .my-4 {
    margin-top: 1.5rem!important;
}
.ml-auto, .mx-auto {
    margin-left: auto!important;
}
.mr-auto, .mx-auto {
    margin-right: auto!important;
}
.job-content h5 {
  font-size: 15px;
}

/**/
.align-items-center {
    -ms-flex-align: center!important;
    align-items: center!important;
}
.justify-content-between {
    -ms-flex-pack: justify!important;
    justify-content: space-between!important;
}
.flex-wrap {
    -ms-flex-wrap: wrap!important;
    flex-wrap: wrap!important;
}
.mb-4, .my-4 {
    margin-bottom: 1.5rem!important;
}
.mt-4, .my-4 {
    margin-top: 1.5rem!important;
}
.flex-shrink-0 {
    -ms-flex-negative: 0!important;
    flex-shrink: 0!important;
}
@media (min-width: 768px){
  .d-md-flex {
    display: -ms-flexbox!important;
    display: flex!important;
  }
  .mr-md-4, .mx-md-4 {
      margin-right: 1.5rem!important;
  }
  .ml-md-0, .mx-md-0 {
    margin-left: 0!important;
  }
  .mb-md-0, .my-md-0 {
    margin-bottom: 0!important;
  }
  .d-md-none {
      display: none!important;
  }
  .text-md-left {
      text-align: left!important;
  }
}

@media (min-width: 992px){
  .d-lg-flex {
      display: -ms-flexbox!important;
      display: flex!important;
  }
}
@media (min-width: 576px){
  .d-sm-inline-block {
      display: inline-block!important;
  }
}

.w-100 {
    width: 100%!important;
}
.ml-310 {
   margin-left: 310px;
}
.pl-310 {
	padding-left: 310px;
}
/*************************  MENU LEFT NICOSS  *****************************/

.side-menu {
  	position: fixed;
    width: 300px;
    height: 100%;
    overflow-y: auto;
    background-color: #f8f8f8;
    border-right: 1px solid #e7e7e7;
    z-index: 1;
}
.side-menu .navbar {
  border: none;
}
.side-menu .navbar-header {
  width: 100%;
  border-bottom: 1px solid #e7e7e7;
}
.side-menu .navbar-nav a.active {
  	background-color: #65BA91;
    margin-right: -1px;
    border-right: 30px solid #65BA91;
}
.side-menu .navbar-nav li {
  display: block;
  width: 100%;
  border-bottom: 1px solid #e7e7e7;
}
.side-menu .navbar-nav li a {
  padding: 15px;
}
.side-menu .navbar-nav li a .glyphicon {
  padding-right: 10px;
}
.side-menu #dropdown {
  border: 0;
  margin-bottom: 0;
  border-radius: 0;
  background-color: transparent;
  box-shadow: none;
}
.side-menu #dropdown .caret {
  float: right;
  margin: 9px 5px 0;
}
.side-menu #dropdown .indicator {
  float: right;
}
.side-menu #dropdown > a {
  border-bottom: 1px solid #e7e7e7;
}
.side-menu #dropdown .panel-body {
  padding: 0;
  background-color: #f3f3f3;
}
.side-menu #dropdown .panel-body .navbar-nav {
  width: 100%;
}
.side-menu #dropdown .panel-body .navbar-nav li {
  padding-left: 15px;
  border-bottom: 1px solid #e7e7e7;
}
.side-menu #dropdown .panel-body .navbar-nav li:last-child {
  border-bottom: none;
}
.side-menu #dropdown .panel-body .panel > a {
  margin-left: -20px;
  padding-left: 35px;
}
.side-menu #dropdown .panel-body .panel-body {
  margin-left: -15px;
}
.side-menu #dropdown .panel-body .panel-body li {
  padding-left: 30px;
}
.side-menu #dropdown .panel-body .panel-body li:last-child {
  border-bottom: 1px solid #e7e7e7;
}
.side-menu #search-trigger {
  background-color: #f3f3f3;
  border: 0;
  border-radius: 0;
  position: absolute;
  top: 0;
  right: 0;
  padding: 15px 18px;
}
.side-menu .brand-name-wrapper {
  min-height: 50px;
}
.side-menu .brand-name-wrapper .navbar-brand {
  display: block;
  padding: 15px;
  COLOR: #354C57;
  font-weight: 700;
}
.side-menu #search {
  position: relative;
  z-index: 1000;
}
.side-menu #search .panel-body {
  padding: 0;
}
.side-menu #search .panel-body .navbar-form {
  padding: 0;
  padding-right: 50px;
  width: 100%;
  margin: 0;
  position: relative;
  border-top: 1px solid #e7e7e7;
}
.side-menu #search .panel-body .navbar-form .form-group {
  width: 100%;
  position: relative;
}
.side-menu #search .panel-body .navbar-form input {
  border: 0;
  border-radius: 0;
  box-shadow: none;
  width: 100%;
  height: 50px;
}
.side-menu #search .panel-body .navbar-form .btn {
  position: absolute;
  right: 0;
  top: 0;
  border: 0;
  border-radius: 0;
  background-color: #f3f3f3;
  padding: 15px 18px;
}
/* Main body section */
.side-body {
  /*margin-left: 310px;*/
}
@media (min-width: 768px) {
	#header-doc {
		/*padding-left: 310px;*/
	}
	.side-menu {
	    padding-bottom: 60px;
	}
	.section-desc {
    	margin-top: 80px;
    }
    .side-body {
      margin-top: 30px;
    }
  /*  .blockcms-section {
    	margin-top: -20px;
	}*/
}
/* small screen */
@media (max-width: 768px) {
  .side-menu {
    position: relative;
    width: 100%;
    height: 0;
    border-right: 0;
    border-bottom: 1px solid #e7e7e7;
  }
  ul.subMenu > li > a.active, ul.subMenu > li > a:hover{
		border-right: 5px solid #65BA91;
	}
  .side-menu .brand-name-wrapper .navbar-brand {
    display: inline-block;
  }
  /* Slide in animation */
  @-moz-keyframes slidein {
    0% {
      left: -300px;
    }
    100% {
      left: 10px;
    }
  }
  @-webkit-keyframes slidein {
    0% {
      left: -300px;
    }
    100% {
      left: 10px;
    }
  }
  @keyframes slidein {
    0% {
      left: -300px;
    }
    100% {
      left: 10px;
    }
  }
  @-moz-keyframes slideout {
    0% {
      left: 0;
    }
    100% {
      left: -300px;
    }
  }
  @-webkit-keyframes slideout {
    0% {
      left: 0;
    }
    100% {
      left: -300px;
    }
  }
  @keyframes slideout {
    0% {
      left: 0;
    }
    100% {
      left: -300px;
    }
  }
  /* Slide side menu*/
  /* Add .absolute-wrapper.slide-in for scrollable menu -> see top comment */
  .side-menu-container > .navbar-nav.slide-in {
    -moz-animation: slidein 300ms forwards;
    -o-animation: slidein 300ms forwards;
    -webkit-animation: slidein 300ms forwards;
    animation: slidein 300ms forwards;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    overflow-y: auto;
    padding-bottom: 95px;
  }
  .side-menu-container > .navbar-nav {
    /* Add position:absolute for scrollable menu -> see top comment */
    position: fixed;
    left: -300px;
    width: 300px;
    top: 43px;
    height: 100%;
    border-right: 1px solid #e7e7e7;
    background-color: #f8f8f8;
    -moz-animation: slideout 300ms forwards;
    -o-animation: slideout 300ms forwards;
    -webkit-animation: slideout 300ms forwards;
    animation: slideout 300ms forwards;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
  }


  @-moz-keyframes bodyslidein {
    0% {
      left: 0;
    }
    100% {
      left: 300px;
    }
  }
  @-webkit-keyframes bodyslidein {
    0% {
      left: 0;
    }
    100% {
      left: 300px;
    }
  }
  @keyframes bodyslidein {
    0% {
      left: 0;
    }
    100% {
      left: 300px;
    }
  }
  @-moz-keyframes bodyslideout {
    0% {
      left: 300px;
    }
    100% {
      left: 0;
    }
  }
  @-webkit-keyframes bodyslideout {
    0% {
      left: 300px;
    }
    100% {
      left: 0;
    }
  }
  @keyframes bodyslideout {
    0% {
      left: 300px;
    }
    100% {
      left: 0;
    }
  }
  /* Slide side body*/
  .side-body, .blockcms-section,.section-desc {
    margin-left: 5px;
    /*margin-top: 55px;*/
    position: relative;
    -moz-animation: bodyslideout 300ms forwards;
    -o-animation: bodyslideout 300ms forwards;
    -webkit-animation: bodyslideout 300ms forwards;
    animation: bodyslideout 300ms forwards;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
  }
  .side-body {
  	margin-top: 55px;
  }
  /*.blockcms-section {
  	margin-top: -20px;
  }*/
  .body-slide-in {
    -moz-animation: bodyslidein 300ms forwards;
    -o-animation: bodyslidein 300ms forwards;
    -webkit-animation: bodyslidein 300ms forwards;
    animation: bodyslidein 300ms forwards;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
  }
  /* Hamburger */
  .navbar-toggle {
    border: 0;
    float: left;
    padding: 18px;
    margin: 0;
    border-radius: 0;
    background-color: #354C57;
  }
  /* Search */
  #search .panel-body .navbar-form {
    border-bottom: 0;
  }
  #search .panel-body .navbar-form .form-group {
    margin: 0;
  }
  .navbar-header {
    /* this is probably redundant */
    position: fixed;
    z-index: 3;
    background-color: #f8f8f8;
  }
  /* Dropdown tweek */
  #dropdown .panel-body .navbar-nav {
    margin: 0;
  }
  .navbar-default .brand-wrapper .navbar-toggle .icon-bar {
	    background-color: #fff;
	}
  .navbar-default .brand-wrapper .navbar-toggle:hover, .navbar-default .brand-wrapper .navbar-toggle:focus {
    background-color: #65BA91;
  }

}

.button-wrap-doc{
    padding: 14px 46px;
    color: #fff;
    background-color: #65BA91;
    border: 1px solid transparent;
    box-shadow: 0 3px 3px 0 rgba(1,1,1,.55);
    -webkit-transition: .5s cubic-bezier(.22,.61,.36,1);
    -moz-transition: .5s cubic-bezier(.22,.61,.36,1);
    transition: .5s cubic-bezier(.22,.61,.36,1);
    position: relative;
    display: inline-block;
    width: 270px;
    left: -25px;
    text-transform: uppercase;
    font-size: 15px;
    text-align: center;
}

.button-wrap-doc:hover {
    -webkit-filter: drop-shadow(3px 3px 20px #65BA91);
    filter: drop-shadow(3px 3px 20px #65BA91);
    color: #fff;
    text-decoration: none;
}

.button-wrap-doc:before{
    content:"";
    position: absolute;
    top: -1px;
    left: -15px;
    width: 0;
    height: 0;
    border-top: 54px solid #65BA91;
    border-left: 15px solid transparent;
    -webkit-transition: border-color .2s ease-in-out;
    -moz-transition: border-color .2s ease-in-out;
    transition: border-color .2s ease-in-out;
}

.button-wrap-doc:after{
    content:"";
    position: absolute;
    top: -1px;
    right: -15px;
    width: 0;
    height: 0;
    border-top: 54px solid #65BA91;
    border-right: 15px solid transparent;
    -webkit-transition: border-color .2s ease-in-out;
    -moz-transition: border-color .2s ease-in-out;
    transition: border-color .2s ease-in-out;
}
</style>

<div class="col-xs-12 no-padding no-margin">


	<!-- ******************************* -->
	<!-- TOP NAVIGATION INDICATOR BAR -->
	<!-- ******************************* -->

	<!-- <div id="header-doc" class=" col-xs-12 shadow2 hidden-xs" data-archi="page d'entrée de la navigation de la documentation">
		<a href='javascript:;' id="show-menu-xs" class=" pull-left" data-placement="bottom" data-title="Menu"><i class="fa fa-bars"></i></a>
		<h2 class="elipsis no-margin"><i class="fa fa-book hidden-xs"></i> DOCUMENTATION <span style="color:#65BA91"></span></h2>
		<a href='javascript:nextDoc();' id="nextDoc" class=" pull-right" data-placement="bottom" data-title="Next"><i class="fa fa-2x fa-arrow-right"></i></a>
		<a href='javascript:prevDoc() ;' id="prevDoc" style="margin-right: 10px;" class=" pull-right" data-placement="bottom" data-title="Prev"><i class="fa fa-2x fa-arrow-left"></i></a>
		 
	</div> -->


	<!-- ******************************* -->
	<!-- LEFT MENU SECTION -->
	<!-- ******************************* -->
	<!-- <div id="menu-left" class="side-menu shadow2 hide" data-archi="menu de navigation à gauche à 2 niveaux, le lien de parenté se faisant grace à l'attribut structags ex : axe1 est un parent de 1er niveau, axe1.ss1 sera un enfant de axe1 ">
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
	            <div class="brand-wrapper">
	                
	                <button type="button" class="navbar-toggle">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>

	                
	                <div class="brand-name-wrapper">
	                    <a class="navbar-brand" href="#">
	                        Liste document
	                    </a>

	                    <a href='javascript:nextDoc();' id="nextDoc" class=" pull-right navbar-brand visible-xs" data-placement="bottom" data-title="Next"><i class="fa fa-arrow-right"></i></a>
						<a href='javascript:prevDoc() ;' id="prevDoc" style="margin-right: 10px;" class="navbar-brand pull-right visible-xs" data-placement="bottom" data-title="Prev"><i class="fa fa-arrow-left"></i></a>
	                </div>
	            </div>

	        </div>

	        <div class="side-menu-container">
			  	<ul class="nav navbar-nav">
					<?php
					$i = 1;
					$cms = PHDB::find( Cms::COLLECTION, array( 
										"parent.".$this->costum["contextId"] => array('$exists'=>1),
                    "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"], 
			              "type"=>"doc", "source.key"=>$this->costum["slug"],
                    "page" => array('$nin'=>array("public","socioeco","welcome","mention","infos-events","accessibilite"))
                  ));	


					$structField = "structags";
           //var_dump($cms);
					$cms = Cms::hierarchy($cms,$structField);
					$docUrls = [];
					foreach ($cms["parentTree"] as $key => $value) 
					{
						if(isset($cms["orphans"][$key]))
						{
					 ?>


						<li class="panel panel-default" id="dropdown">
							<a href="javascript:" data-toggle="collapse" class="link-docs-menu-cms" data-cms="<?= (string)$cms["orphans"][$key]["_id"]; ?>" data-id="<?= (string)$cms["orphans"][$key]["_id"]; ?>" data-pos="<?php echo count($docUrls) ?>" style="color: <?php echo (isset($cms["orphans"][$key]["color"])) ? $cms["orphans"][$key]["color"] :''; ?>">
								<?php $docUrls[] = (string)$cms["orphans"][$key]["_id"]; ?>
								<i class="fa fa-angle-right"></i> <?php echo $cms["orphans"][$key]["name"]; ?>
								<span class="caret"></span>
							</a>
							<div id="dropdown-lvl1" class="panel-collapse collapse in">
		                        <div class="panel-body">
									<ul class="subMenu nav navbar-nav">
										<?php
											//position basé sur les 2 premier chiffres
											foreach ( $value as $k => $v) {
												$value[$k]["pos"] = substr( $v["name"], stripos( $v["name"], ">" ) + 1 , 3 ) ;
											}
											$pos = array_column( $value, 'pos' );
											array_multisort( $pos, SORT_ASC, $value );

											foreach ( $value as $k => $v ) 
											{ 
												?>
											<li>
												<a href="javascript:" class="link-docs-menu-cms numero2" data-cms="<?php echo (string)$v["_id"]; ?>" data-id="<?php echo (string)$v["_id"]; ?>" data-pos="<?php echo count($docUrls) ?>" style="color: <?php echo (isset($v["color"])) ? $v["color"] :''; ?>">&nbsp;&nbsp;&nbsp;&nbsp;
													<?php 
													$docUrls[] = (string)$v["_id"];
													echo $v["name"]; ?>
												</a>
											</li>
										<?php
											}
										?>
									</ul>
								</div>
							</div>
						</li>
					<?php
						}else {
							foreach ( $value as $k => $v) 
							{ 
								$cms["orphans"][$k]=$v;
							}
						}
					}
					
					foreach ($cms["orphans"] as $ky => $valu) 
					{ 
						$i++;
						if( isset($valu[$structField]) && count($valu[$structField]) == 1 && isset( $cms["parentTree"][ $valu[$structField][0] ]) ){
							//on n'affiche pas le sparent en tant qu'orphelin
						}
						else if( isset($valu[$structField]) && isset( $cms["parentTree"][ $valu[$structField] ]) ){
							//on n'affiche pas le sparent en tant qu'orphelin
						}
						else
						{ ?>
						
						<li >
							<a href="javascript:" class="link-docs-menu-cms" data-id="<?php echo (string)$valu["_id"]; ?>" data-cms="<?php echo (string)$valu["_id"]; ?>" style="color: <?php echo (isset($valu["color"])) ? $valu["color"] :'' ; ?>">
								<?php 
									$docUrls[] = (string)$valu["_id"];
									echo $valu["name"]; ?>
							</a>
						</li>
					<?php } 
					} 
					?>

					
					<?php if( Authorisation::canEdit( Yii::app()->session["userId"] , $this->costum["contextId"], $this->costum["contextType"] ) )
					{ 
						
						?>
					<li class="col-xs-12 no-padding" style="border-bottom: 1px solid #f8f8f8;">	
						<ul>
							<li class="col-xs-12 no-padding" style="border-bottom: 1px solid #f8f8f8;">
								<a href="javascript:;" data-struct="axe<?= $i ?>" class="createDocBtn button-wrap-doc" >
									<i class="fa ffa fa-book"></i>&nbsp; Ajouter Documentation
								</a>
							</li>
						</ul>
					</li>
					<?php } ?>

				</ul>
			</div>
		</nav>
	</div> -->


	<!-- ******************************* -->
	<!-- END MENU SECTION -->
	<!-- ******************************* -->


	


	<!-- ******************************* -->
	<!-- CONTENT SECTION -->
	<!-- ******************************* -->
  
	
  <div class="container-fluid">
    <?php 
    /*$cmsBlock = PHDB::find( Cms::COLLECTION, array( 
                    "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                                  "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"], 
                                  "path"=>"tpls.blockCms.text.cte_desc"));*/
       /*foreach ($cmsBlock as $keys => $valiny) 
          {
            if (isset($valiny["path"]) && isset($valiny["_id"])){
                $params = [
                        "keyTpl" => (string)$valiny["_id"],
                        "desc"  =>  isset($valiny["description"]) ? $valiny["description"] : ""
                      ];
                  echo $this->renderPartial("costum.views.".$valiny["path"],$params);
             }

          }*/
    ?>
      <!-- ******************************* -->
  <!-- BLOCK CMS SECTION -->
  <!-- ******************************* -->
  
  <!--<div id="section-cms" class="section-cms"> -->
       <?php 
           /* $cms1 = PHDB::find( Cms::COLLECTION, array( 
                                "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                       "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"], 
                        "page" => array('$exists'=>1,'$nin'=>array("public","socioeco","welcome","mention","infos-events","accessibilite"))) ); 
             $structField = "page";
            $listPage = Cms::listPage($cms1,$structField);
            foreach ($listPage as $keyy => $val) {
              $cmsList[$val] = PHDB::find( Cms::COLLECTION, array( 
                "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"], "page" => $val,
                "path"=> array('$exists'=>true,'$nin'=> array("tpls.blockCms.text.cte_desc")))
                
              );*/
              ?>
                
                  <?php 
                    /*foreach ($cmsList[$val] as $k => $v) {*/ ?>

                      <!-- <div class="<?//= $val?> <?//= (string)$v["_id"]?> blockcms-section "  > -->
                  <?php
                     /*if(isset($v["path"])) {
                      $params = [
                        "blockCms"  =>  $v,
                        "page"      =>  $v["page"],
                        "canEdit"   =>  $canEdit,
                        "blockKey"  => (string)$v["_id"]
                      ];
                      echo $this->renderPartial("costum.views.".$v["path"],$params);
                     }*/
                      ?>
                    <!-- </div> -->
                   <?php   /*}*/
             /*}*/ ?>
    <!--</div> -->

    <!-- ******************************* -->
  <!-- END BLOCK CMS SECTION -->
  <!-- ******************************* -->
		<div id="container-docs" class=" side-body" data-archi="div affichant le contenu des docs">
    
      

		</div>
	</div>
	<!-- ******************************* -->
	<!-- END CONTENT SECTION -->
	<!-- ******************************* -->


</div>

<script type="text/javascript">
// $.each($(".markdown"), function(k,v){
//   descHtml = dataHelper.markdownToHtml($(v).html());
//   $(v).html(descHtml);
// });

$(function () {
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('.blockcms-section').toggleClass('body-slide-in');
        $('.section-desc').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');
        
    });
   
   // Remove menu for searching
   $('#search-trigger').click(function () {
        $('.navbar-nav').removeClass('slide-in');
        $('.side-body').removeClass('body-slide-in');

        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').removeClass('slide-in');

    });
});

var allCms = <?php echo json_encode($cms) ?> ;

var dynFormCostumDoc = {
	"beforeBuild":{
	    "removeProp" : {
	    	"image": 1,
        },
        "properties" : {
            "structags" : {
                "inputType" : "tags",
                "placeholder" : "Structurer le contenu",
                "values" : null,
                "label" : "Structure et Hierarchie (parent ou parent.enfant)"
            },
            "documentation" : {
                "inputType" : "uploader",
                "label" : "Document associé (5Mb max)",
                "showUploadBtn" : true,
                "docType" : "file",
                "itemLimit" : 5,
                "contentKey" : "file",
                "domElement" : "documentationFile",
                "placeholder" : "Le pdf",
                "afterUploadComplete" : null,
                "template" : "qq-template-manual-trigger",
                "filetypes" : [
                    "pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif","eps"
                ]
            },
            "color" : {
                "inputType" : "colorpicker",
                "label" : "couleur du menu",
                "order" : 3
            },
            "path" : {
                "inputType" : "hidden",
                "value" : "tpls.blockCms.text.cte_desc"
            }
        }
    },
	"onload" : {
        "actions" : {
            "setTitle" : "Documentation",
            "html" : {
                "nametext>label" : "Titre de la documentation",
                "infocustom" : "<br/>Créer des sections et des sous chapitres"
            },
            "presetValue" : {
                "type" : "doc"
            },
            "hide" : {
            	"structagstags":1,
	            "locationlocation" : 1,
	            "formLocalityformLocality" : 1,
                "breadcrumbcustom" : 1,
                "urlsarray" : 1,
                "imguploader" : 1
            }
    	}
	}
};
dynFormCostumDoc.afterSave = function(data){
	//alert("govalos");
			//mylog.log("afterSave cms", data, typeof callB);
			dyFObj.commonAfterSave(data,function(){
				mylog.log("data", data);
				 location.hash = "#documentation."+data.id;
				 location.reload();

			});
		},





jQuery(document).ready(function() {
	mylog.log("render","costum.views.ctenat.doc");
	if(typeof location.hash.split(".")[1] != "undefined"){
		location.hash = "#documentation."+location.hash.split(".")[1];
		$('#header-doc').addClass('pl-310');
		$('#menu-left').removeClass("hide");
		$('#container-docs').addClass('ml-310');
    $('.section-desc').addClass('ml-310');
    $('.section-desc').hide();
		$('.blockcms-section').addClass('ml-310');
		$(".blockcms-section").hide();
    $("#"+location.hash.split(".")[1]).show();
		$("."+location.hash.split(".")[1]).show();
		$("."+location.hash.split(".")[1]).removeClass("hide");
		//navInDocs("docDetail", "costum.views.custom.ctenat.docs");
		openDoc(location.hash.split(".")[1]);
	}else{
		//alert("vide");
		$(".blockcms-section").hide();
    $('.section-desc').hide();
		setTimeout(function() { navInDocs("kicker", "costum.views.custom.ctenat.docs"); }, 500)
	}

	
	/*
	setTimeout(function() {
		$("a[href='#documentation']").unbind("click").on("click",function(){
		//alert("hahahaha");
		 navInDocs("kicker", "costum.views.custom.ctenat.docs");
		 $('.blockcms').addClass("hide");
		});
	}, 500)
	*/
	
	
	// if ( location.hash.indexOf("docs.cms") >= 0 ) {
	// 	paramT = location.hash.split(".");
	// 	documentation.getBuildPoiDoc(paramT[2]);
	// }else 	
	// 	setTimeout(function() { navInDocs("kicker", "costum.views.custom.ctenat.docs"); }, 500)
	
	
	$("#close-docs").attr("href",urlBackHistory);

	function bindDocLinksCms() { 
		$(".link-docs-menu-cms").off().on("click",function(){
		      $(".blockcms-section").hide();
          $(".section-desc").hide();
		      $("."+$(this).data("cms")).show();
		      $("."+$(this).data("cms")).removeClass("hide");
          $("#"+$(this).data("id")).show();
          $("#"+$(this).data("id")).removeClass("hide");
    			$(".blockTpls").css("display","none");
    			$('.tpls'+$(this).data("cms")).css("display","block");
    			$('.navbar-nav').toggleClass('slide-in');
      		$('.side-body').toggleClass('body-slide-in');
          $('.section-desc').toggleClass('body-slide-in');
      		$('.blockcms-section').toggleClass('body-slide-in');
          if(parseInt($(this).data("pos")) != "NaN")
			       openDoc($(this).data("cms"),parseInt($(this).data("pos")));
           else
            openDoc($(this).data("cms"));
    //alert("cms : "+$(this).data("cms")+"<br> position : "+parseInt($(this).data("pos")));
			var urlDoc = location.url;
			var urlHistoric = "";
			if (location.hash.indexOf("#") == -1) 
				urlHistoric = location.hash+"."+$(this).data("cms");
			else 
				urlHistoric = "#documentation."+$(this).data("cms");

			history.replaceState({}, null, urlHistoric);
			

		});
	}

	bindDocLinksCms();

	$("#show-menu-xs").off().on("click",function (){
		//$('#container-docs').removeClass('col-xs-9').removeClass('col-xs-12') 

		$('.blockcms-section').addClass("hide");
		$('#container-docs').removeClass('ml-310');
    $('.section-desc').removeClass('ml-310');
    $('.section-desc').addClass("hide");
		$('#header-doc').removeClass('pl-310');
		$('#menu-left').addClass("hide");
		navInDocs("kicker", "costum.views.custom.ctenat.docs");
	});

	$(".createDocBtn").off().on("click",function (){
		mylog.log("createBtn");

		dataObj = {parent:{}};
		dataObj.parent[costum.contextId] = {collection : costum.contextType, name : contextData.name } ;
		dataObj.structags = $(this).data("struct");

		dyFObj.openForm('cms',null,dataObj,null,dynFormCostumDoc);
	});

	var navItems = $('#menu-left li > a');
    var navListItems = $('#menu-left li > a');
    navItems.click(function(e)
    {
        e.preventDefault();
        navListItems.removeClass('active');
        $(this).closest('a').addClass('active');

    });
});



function openDoc(cmsId,pos=null){
	
	var url = baseUrl+'/co2/element/get/type/cms/id/'+cmsId+'/edit/true';
	currentDocPos=pos;
	var z = 1;
	ajaxPost(null ,url, null,function(data){
		console.log("openDoc",data);
		
		if(typeof data.map.structags != "undefined") {
      structag = data.map.structags.split('.');
			hashdocs="#documentation."+data.map.structags;
    }
  //alert(data.map._id.$id);
		dyFObj.elementData = {page:data.map._id.$id};
		descHtml = documentation.renderDocHTML(data);
    //descHtml = "";
    mylog.log("descHtml",descHtml);
		//for top level cms only
		if (structag.length <= 1) {
			$.each(allCms.parentTree , function(e,v){
				if (typeof allCms["orphans"][e] != "undefined") {
					$.each(v , function(key,val){
						valueStructag = val.structags.split('.');
						if ( structag[0] == valueStructag[0] ) {
							mylog.log("allcms.parentTree strutags",structag,valueStructag);
							z = v.length;
						}else{
							z = 1;
						}
					});
				}
			});

			descHtml += "</div class='col-md-12 col-sm-12 col-lg-12'>";
          		descHtml += "<a href='javascript:;' data-struct='"+structag[0]+".ss"+ z +"' class='createSubMenu btn icon-btn btn-info' > <span class='fa fa-list-ul btn-glyphicon img-circle text-info' aria-hidden='true'></span> Nouveau sous-menu </a>";
          	descHtml += "</div>";
    	}

    // 	descHtml += <?php //echo $this->renderPartial("costum.views.".$path,$params); ?>;
	   //  	$param=[
				// 	  "page"=>$page,
				// 	  "canEdit"=>true
				// 	]; 
				// echo $this->renderPartial("costum.views.tpls.blockCms.cmsEngine",$param); 
    	descHtml += "</div class='col-md-12 col-sm-12 col-lg-12'>";
      		descHtml += "<a href='javascript:;' data-key='blockevent' class='addBlockCms btn icon-btn btn-success' > <span class='fa fa-plus btn-glyphicon img-circle text-success' aria-hidden='true'></span> Ajouter une section </a>";
      	descHtml += "</div>";


		$('#container-docs').html(descHtml);
		//alert();
		

		$(".editThisDoc").off().on("click",function (){
			mylog.log("editThisDoc"); 
			var id = $(this).data("id");
		    var type = $(this).data("type");
			dyFObj.editElement(type,id, null,dynFormCostumDoc);
		});

		$(".addBlockCms").off().on("click",function (){
			var optionsJs = <?= json_encode(array(
				//"tpls.blockCms.textImg.blockText" => "Nouveau Paragraphe",
				"tpls.blockCms.textImg.cte_blockwithimg" => "Un bloc avec un texte et une image",
				"tpls.blockCms.text.cte_bloctextwithtitle" => "Un bloc avec un Titre et description"
		    )); ?>;
			var page = dyFObj.elementData.page;
			console.log("sylvany",dyFObj.elementData);
			var tplCtx = { value : { tpls : {} } };
			//alert(page);
			var addBlockCms = {
	            "jsonSchema" : {
	                "title" : "Ajouter une section",
	                "type" : "object",
	                onLoads : {
				    	//pour creer un subevnt depuis un event existant
				    	onload : function(){
				    		$(".parentfinder").css("display","none");
				    	}
				    },
	                "properties" : {
	                    type : { 
	                        label : "Choisir le template",
	                        inputType : "select",
	                        options : optionsJs,
	                        value : "text" 
	                    },
	                    page : {
	                        inputType : "hidden",
	                        value : page
	                    }
	                }
	            }
        	}; 

        	//tplCtx.id = $(this).data("id");
        	//tplCtx.key = $(this).data("key");
        	
        	
	        addBlockCms.jsonSchema.save = function () {
	        	tplCtx.collection = "cms";
	        	tplCtx.path = "allToRoot";
	        	tplCtx.value = {
	        		"path" : $("#type").val(),
				    "page" : $("#page").val(),
				    "haveTpl" : "false"
	        	};
	        	tplCtx.value.parent = {};
	        	tplCtx.value.parent[contextData.id] = {
	        		"type" : costum.contextType,
				    "name" : costum.contextSlug
				};


	            if(typeof tplCtx.value == "undefined")
	                toastr.error('value cannot be empty!');
	            else {
	                mylog.log("addBlockCms save tplCtx",tplCtx);
	                dataHelper.path2Value( tplCtx, function(params) {
	                	location.reload();
	                    toastr.success("Section ajouté");
	                    $("#ajax-modal").modal('hide');
	                } );
	            }
	            // urlCtrl.loadByHash(location.hash);
	        }
	        dyFObj.openForm(addBlockCms);
		});

		$(".createSubMenu").off().on('click',function (){
			dataObj = {parent:{}};
			dataObj.parent[costum.contextId] = { collection : costum.contextType, name : '<?php echo $this->costum["slug"] ?>' } ;
			dataObj.structags = $(this).data("struct");
			dyFObj.openForm('cms',null,dataObj,null,dynFormCostumDoc);
		});

		$(".deleteThisDoc").off().on("click",function (){
	       	mylog.log("deleteThisDoc click");
	        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	        var btnClick = $(this);
	        var id = $(this).data("id");
	        var type = $(this).data("type");
	        var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
	          
        	bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                	ajaxPost(
				        null,
				      	urlToSend,
				        {},
				        function(data){ 
							if ( data && data.result ) {
	                          toastr.info("élément effacé");
	                          $("#"+type+id).remove();
	                          urlCtrl.loadByHash(location.hash);
	                        } else {
	                           toastr.error("something went wrong!! please try again.");
	                        }
				        }
				    ); 
                }
        	});
	    });

	    $(".deleteLine").off().on("click",function (){
	    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	    var id = $(this).data("id");
	    var type = $(this).data("collection");
	    var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
	    bootbox.confirm(trad.areyousuretodelete,
	      function(result){
	        if (!result) {
	          return;
	        } 
	        else {
                ajaxPost(
                    null,
                    urlToSend,
                    null,
                    function(data){
                        if ( data && data.result ) {
                            toastr.success("Element effacé");
                            $("#"+type+id).remove();
                        } else {
                            toastr.error("something went wrong!! please try again.");
                        }
                        location.reload();
                    },
                    null,
                    "json"
                );
	        }
	      }
	      );
	  });
	},"html");
}

var docUrls = <?php echo json_encode($docUrls); ?>;
currentDocPos = -1;

function nextDoc () { 
	$(".link-docs-menu-cms").removeClass("active");
	$(".blockcms-section").hide();
  $(".section-desc").hide();
	if(currentDocPos < 0){
		//$('#container-docs').addClass('col-xs-9').removeClass('col-xs-12')
		$('#container-docs').addClass('ml-310');
    $('.section-desc').addClass('ml-310');
		$('.blockcms-section').addClass('ml-310');
		$('#menu-left').removeClass("hide");
	}
	currentDocPos++;
	if(currentDocPos > docUrls.length)
		currentDocPos = -1;
	mylog.log("nextDoc",docUrls[currentDocPos],currentDocPos);
	openDoc(docUrls[currentDocPos],currentDocPos);
	//alert(docUrls[currentDocPos]);
	$("."+docUrls[currentDocPos]).show();
  $("#"+docUrls[currentDocPos]).show();
	$(".blockTpls").css("display","none");
	$('.tpls'+docUrls[currentDocPos]).css("display","block");
	$('.link-docs-menu-cms[data-pos="'+currentDocPos+'"]').addClass("active");
	var urlHistoric = "";
	if (location.hash.indexOf("#") == -1) 
		urlHistoric = location.hash+"."+docUrls[currentDocPos];
	else 
		urlHistoric = "#documentation."+docUrls[currentDocPos];

	history.replaceState({}, null, urlHistoric);
}

function prevDoc () { 
	$(".link-docs-menu-cms").removeClass("active");
	$(".blockcms-section").hide();
   $('.section-desc').hide();
	if(currentDocPos < 0){
		//$('#container-docs').addClass('col-xs-9').removeClass('col-xs-12')
		$('#container-docs').addClass('ml-310');
    $('.section-desc').addClass('ml-310');
		$('.blockcms-section').addClass('ml-310');
		$('#menu-left').removeClass("hide");
	}
	currentDocPos--;
	if( currentDocPos < 0 )
		currentDocPos = docUrls.length-1;
	openDoc(docUrls[currentDocPos],currentDocPos);
	$("."+docUrls[currentDocPos]).show();
  $("#"+docUrls[currentDocPos]).show();
	$(".blockTpls").css("display","none");
	$('.tpls'+docUrls[currentDocPos]).css("display","block");
	$('.link-docs-menu-cms[data-pos="'+currentDocPos+'"]').addClass("active");
	var urlHistoric = "";
	if (location.hash.indexOf("#") == -1) 
		urlHistoric = location.hash+"."+docUrls[currentDocPos];
	else 
		urlHistoric = "#documentation."+docUrls[currentDocPos];

	history.replaceState({}, null, urlHistoric);
}



contextData = <?php echo json_encode($el); ?>;
var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
var contextName=<?php echo json_encode($el["name"]); ?>;
contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;
</script>