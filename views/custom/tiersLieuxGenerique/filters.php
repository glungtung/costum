<?php
$filliaireCategories = CO2::getContextList("filliaireCategories");
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>
<style type="text/css">
	#filters-nav{
		display: flex !important;
  		justify-content: center !important;
	}
</style>
<script type="text/javascript">

    var pageApp= window.location.href.split("#")[1];
	var appConfig=<?php echo json_encode(@$appConfig); ?>;

	var typologyOfActors = null;
	var defaultType = {};

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.types != "undefined"){
		defaultType = Object.keys(costum.lists.types);
		//defaultType.push("organizations");
		//Object.assign(defaultType, costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"]["options"]);
	}else{
		defaultType = Object.keys({"NGO" : trad.ong,
						"Cooperative" : trad.servicepublic,
						"Group":trad.group,
						"LocalBusiness":trad.LocalBusiness,
						"GovernmentOrganization":trad.GovernmentOrganization
					});
		//defaultType = Object.keys(costum.lists.types);
	}

	if(costum && typeof costum.lists == "undefined"){
		costum["lists"] = {}
	}

	if(typeof costum.lists.typologyOfActors == "undefined"){
		costum.lists["typologyOfActors"] = {};
	}

	typologyOfActors = {};
	Object.assign(typologyOfActors, costum.lists.typologyOfActors);

	var defaultScopeList = ["FR", "RE"];

	var paramsFilter= {
		container : "#filters-nav",
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			indexStep : 0,
		 	types : (typeof appConfig.filters !="undefined" && exists(appConfig.filters.types))?appConfig.filters.types:"organizations",
			filters:{},
		},
		filters : {
		 	category : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			remove0: false,
	 			countResults: true,
	 			name : "<?php echo Yii::t("common", "search by theme")?>",
	 			event : "tags",
	 			keyValue: true,
	 			list : costum.lists.category
	 		},
	 		/*typology : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "<?php echo Yii::t("common", "search by typology of actors")?>",
	 			event : "tags",
	 			keyValue: true,
	 			list : typologyOfActors
	 		},*/
 			/*scopeList : {
	 			name : "<?php echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList,
	 				level : ["3"]
	 			}
	 		},*/
	 		text : {
				placeholder: "Recherche par #tag ou texte"
			}
	 	},
	 	results :{
            renderView : "directory.elementPanelHtml",
            map : {
            	active : true
            }
        }
	}

	if(pageApp == "projects"){
		paramsFilter.defaults.types = ["projects"];
		delete paramsFilter.filters["types"];
	}

	if(typologyOfActors==null || pageApp=="projects"){
		delete paramsFilter.filters["theme"];
	}

	/*if(costum["dataSource"]){
		paramsFilter["defaults"]["sourceKey"] = costum["dataSource"];
	}*/

	jQuery(document).ready(function() {
		  filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		if(!($("#menuRight").find("a").hasClass("changelabel"))){
			$("#menuRight").find("a").addClass("changelabel")
		}
  
		$("#menuRightmapContent").hide();	
		$(".BtnFiltersLieux").show();
	//	lazyFilters(0);
		
	});

</script>
