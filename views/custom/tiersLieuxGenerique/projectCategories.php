<?php 
$baseUrl=Yii::app()->getRequest()->getBaseUrl(true);
$costumSlug = $this->costum["slug"]; 
?>

<table>
	<thead>
	    <tr>
		    <th>Les projets</th>
		    <th>Les catégories</th>
	    </tr>
	</thead>
	<tbody>
		<?php 
		$backColor="";
        foreach($projects as $id => $value){
        	$backColor=empty($backColor) ? "bg-blue" : "" ;
        ?>	
        	<tr class="<?php echo $backColor ?>">
        		<td><a href="<?php echo $baseUrl ?>/costum/co/index/slug/<?php echo $costumSlug ?>/#@<?php echo $value["slug"] ?>" target="_blank"><?php echo $value["name"]?></td>    
        		<td>
        			<select class="categories select2Select form-control" multiple='multiple' placeholder='ok' name='<?php echo $value["name"]?>' id='<?php echo $value["name"]?>' style='width: 100%;height:auto;' data-placeholder='ok'>
        				

        			</select>	
        		</td>  
        		<td><button disabled data-id="<?php echo $value["_id"]?>" data-type="<?php echo $value["collection"]?>" data-name="<?php echo $value["name"]?>" data-tags='<?php echo json_encode(@$value["tags"])?>' class="margin-right-5 setProjectCategories btn bg-green text-white"><i class="fa fa-plus"></i>Associer la(es) catégorie(s)</button></td> 
        	</tr>

        <?php
        }
		?>
		
	</tbody>    

	

</table>

<script type="text/javascript">
    var options='<option class="text-red" style="font-weight:bold" disabled>Choisissez une ou plusieurs catégories (maintenez la touche Ctrl pour en sélectionner plusieurs)</option>';

		bindSetCategory = function(){   
		   $(".setProjectCategories").off().on("click",function(){
		   	 var $this=$(this);
	          var categories=$(this).parent().prev().find('select').val();
	          mylog.log("categories table",categories);
	          var id=$(this).data("id");
	          var type = $(this).data("type");
	          var name = $(this).data("name");
	          var tags=$(this).data("tags");
	          var allTags=tags.concat(categories);
	          mylog.log("allTags",allTags);
	          var dataProject = {
	          	id : id,
	          	collection : type,
	          	path : "tags",
	          	value : allTags
	          };
	          
	          var textCat = (categories.length>1) ? "Les catégories" : "La catégorie" ;
	          	var addedText = (categories.length>1) ? " ont été ajoutées" : " a été ajoutée" ;
	          	var catText=categories.toString();
	          	$(this).parent().prev().html("<span>"+textCat+" "+catText+addedText+" au projet</span>");

	          //add tags categories project
	          dataHelper.path2Value(dataProject, function (response) {
	          	$this.parent().html('<button class="margin-right-5 modifyProjectCategories btn bg-green text-white"><i class="fa fa-crog"></i>Modifier</button>');
	          	bindModify(id,type,name,tags,categories);          	
	          });

	          // add reference project
	          dataProject.path="reference.costum";
	          dataProject.value=costum.slug;
	          dataProject.arrayform=true;
	          dataHelper.path2Value(dataProject, function (response) {
	          });
	         

	           
		   });

		   $(".chosenOption").off().on("click",function(){
               $(this).parent().parent().next().find('button').removeAttr("disabled");
	       });
		}   

		bindModify = function(id,type,name,tags,categories){   
		   $(".modifyProjectCategories").off().on("click",function(){
		   	   var $th=$(this);

	           $(this).parent().prev().html("<select class='categories' multiple='multiple'>"+options+"</select>");
	           $.each(categories,function(ind,cat){
	               $(this).parent().prev().find('option[value="'+cat+'"]').attr("selected","selected");
	           });
	           
	           $(this).html('<button disabled data-id="'+id+'" data-type="'+type+'" data-name="'+name+'" data-tags="'+JSON.stringify(tags).replaceAll(`'`,`\'`).replaceAll(`"`,`'`)+'" class="margin-right-5 setProjectCategories btn bg-green text-white"><i class="fa fa-plus"></i>Associer la(es) catégorie(s)</button>')

	           bindSetCategory();

		   });
		} 



    jQuery(document).ready(function() {
    	
	   $.each(Object.keys(costum[costum.contextSlug].categoriesTL), function(ind, cat){
            options +="<option class='chosenOption' value='"+cat+"'>"+cat+"</option>";

	   });

	   $(".categories").append(options);
	   bindSetCategory();


	   


	});    

</script>

