<?php 

if(!isset($noRender)){
	echo '<h4 style="text-align:left; color:'.@$color.'">'.( (isset($title)) ? $title : @$field).'</h4>';
	echo "<div class='text-left markdown' id='elemText".$field."'>".@$value."</div>";
}
echo $this->renderPartial("costum.views.tpls.elemOpenFormBtn",
                                array(
                                    'collection' => $collection,
                                    'id' => $id,
                                    'field' => $field,
                                    'dynFormProperty'=>(is_array($dynFormProperty))? "" : $dynFormProperty ,
                                 ),true);

?>


<script type="text/javascript">

jQuery(document).ready(function() {
    dyFInputs.defaultProperties = {
        description : dyFInputs.textarea( tradDynForm.longDescription, "...",null,true),
        location : dyFInputs.location,
        tags :dyFInputs.tags(),
        urls : dyFInputs.urls,
        image : dyFInputs.image(),
        shortDescription : dyFInputs.textarea(tradDynForm.shortDescription, "...",{ maxlength: 140 }),
        public : dyFInputs.checkboxSimple("true", "public", 
                { "onText" : trad.yes,
                  "offText": trad.no,
                  "onLabel" : tradDynForm.public,
                  "offLabel": tradDynForm.private,
                  //"inputId" : ".amendementDateEnddatetime",
                  "labelText": tradDynForm.makeeventvisible+" ?",
                  //"labelInInput": "Activer les amendements",
                  "labelInformation": tradDynForm.explainvisibleevent
            }),
        organizer : {
                inputType : "finder",
                id : "organizer",
                label : tradDynForm.whoorganizedevent,
                initType: ["projects", "organizations"],
                multiple : true,
                openSearch :true,
            },
        allDay : dyFInputs.allDay(),
        startDate : dyFInputs.startDateInput("datetime"),
        endDate : dyFInputs.endDateInput("datetime"),
        scope : dyFInputs.scope()
    };
    mylog.log("render","/modules/costum/views/tpls/elemText.php",'field : <?php echo $field ?>');
    
    var dynFormProperty = <?php echo (!$dynFormProperty) ? "null" : json_encode($dynFormProperty) ?>;
    mylog.log("editProperty","elemText ",dynFormProperty);
    var thisField = "<?php echo $field ?>";

    if(dyFObj.formProperties == null)
        dyFObj.formProperties = {};

    //if no dynFormProperty means we are using a predefined property
    if(dynFormProperty != null){
        if(dyFObj.dynFormSource != null){
        	mylog.log("editProperty","add dyFObj.formProperties.",thisField,<?php echo json_encode($dynFormProperty) ?>);
        	dyFObj.formProperties[thisField] = dynFormProperty;
        }
    } else  {
        mylog.log("editProperty","dyFInputs.defaultProperties",dyFInputs.defaultProperties);
        dyFObj.formProperties[thisField] = dyFInputs.defaultProperties[thisField];
    }

    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout

    
    // /alert("costum.views.tpls.text");
    $(".editThisElemDataBtn").off().on("click",function (){
        var id = $(this).data("id");
        var col = $(this).data("type");
        var field = $(this).data("field");
        var property = $(this).data("property");
        mylog.log( "editThisElemDataBtn", col, id, field, property );
        if(typeof dyFObj.dynFormCostumCMS != "undefined")
            dyFObj.dynFormCostum = dyFObj.dynFormCostumCMS;
        
        dyFObj.editProperty(col,id,field,property);
    });

    $(".openAllAtOnce").off().on("click",function (){
        var id = $(this).data("id");
        var col = $(this).data("type");
        mylog.log( "openAllAtOnce", col, id );
        dyFObj.editProperty(col,id,null,null,true);
    });
    
    
    
});
</script>