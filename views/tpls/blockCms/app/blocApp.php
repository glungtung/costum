<?php 
$keyTpl = "blocApp";
$paramsData = [
  "title" => "Les acteurs"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
if(isset($costum["contextType"]) && isset($costum["contextId"])){
  $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
}
?>
<style type="text/css">
  
  .bloc-app-cocity_<?= $kunik?> .title-1 {
    margin-top: 5%;
    margin-bottom: 0;
  }
  .bloc-app-cocity_<?= $kunik?> h2{
    margin-top: 5%;
    margin-bottom: 5%;
  }
  .bloc-app-cocity_<?= $kunik?> p{
    margin-top: 5%;
    color: white;
    font-size: 16px;
    margin-left: 5%;
    margin-right: 9%;
  }
  .bloc-app-cocity_<?= $kunik?> .bouton{
    margin-top: 2%; 
    width: 85%;
    left: 8%;
    margin-bottom: 8%;
  }
  
  .colonne1 {
    background: #0A96B5;
    margin-bottom: 2%;
    max-height: 600px; 
    min-height: 520px;
  }
  .colonne2{
    background: #8ABF32;
    max-height: 600px; 
    min-height: 520px;
  }

  .bloc-app-cocity_<?= $kunik?> {
    padding-left: 100px;
    padding-right: 100px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne {
    padding-top: 1% ;
  }
  
  .bloc-app-cocity_<?= $kunik?> .bg1 {
    height: 370px;
    background: #d8dcd7;
    opacity: 0.2;
  }
  .bloc-app-cocity_<?= $kunik?> .bg2 {
    height: 370px;
    background: black;
    opacity: 0.7;
    padding-top: 15%
  }
  .bloc-app-cocity_<?= $kunik?> .btn-bloc{
    width : 215px;
    background: transparent;
    font-weight: bold;
    color: white !important;
    margin-top: 10px;
    text-transform:uppercase;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne1ligne1 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe18.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 370px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne2ligne1 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe19.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 370px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne3ligne1 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe20.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 370px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne1ligne2 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe23.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 370px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne2ligne2 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe22.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 370px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne3ligne2 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe21.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 370px;
  }
  @media (max-width: 978px) {
   
    .bloc-app-cocity_<?= $kunik?> h2{
      margin-top: 3%;
      font-size: 30px !important;
      margin-bottom: 5%;
    }
    .bloc-app-cocity_<?= $kunik?> p{
      font-size: 16px;
      margin-bottom: 4%;
    }
    .bloc-app-cocity_<?= $kunik?> {
      padding-left: 20px !important;
      padding-right: 20px !important;
    }
  }


</style>
<script type="text/javascript">
  var dyfOrga = {};
  var element =<?= json_encode($el)?>;
  var cocityCoordonate = {};
  cocityCoordonate["address"] = element.address;
  cocityCoordonate["geo"] = element.geo;
  cocityCoordonate["geoPosition"] = element.geoPosition;
</script>
<div class="bloc-app-cocity_<?= $kunik?>">
  <h2 class="title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?= $paramsData["title"]?></h2>
  <div class="col-md-4 col-sm-6 col-xs-12   text-center colonne " >
    <div class=" colonne1ligne1">
      <div class="" >
        <div class="bg2" >
          <div class="teste">
            <h2 class="h2-bloc title-2 ">
              <?php echo Yii::t("cms", "I need help")?>
            </h2>
            <p class="para-bloc title-6 ">
                <?php echo Yii::t("cms", "need help with your projects? call us!")?>
            </p>
          </div>
          <div class="">
            <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'},null,dyfOrga);" class="btn btn-lg btn-default">
                <?php echo Yii::t("cms", "Propose a request")?>
            </a>
          </div>
          <div class=" " >
            <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                <?php echo Yii::t("cms", "See more")?>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12   text-center colonne " >
    <div class=" colonne2ligne1">
      <div class="" >
        <div class="bg2" >
          <div class="teste">
            <h2 class="h2-bloc title-2 ">
                <?php echo Yii::t("cms", "I offer my services")?>
           </h2>
           <p class="para-bloc title-6 ">
               <?php echo Yii::t("cms", "need help with your projects? call us!")?>
          </p>
        </div>
        <div class="">
          <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'offer'},null,dyfOrga);" class="btn btn-lg btn-default">
              <?php echo Yii::t("cms", "Make an offer")?>
          </a>
        </div>
        <div class=" " >
          <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
              <?php echo Yii::t("cms", "See more")?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12   text-center colonne " >
  <div class=" colonne3ligne1">
    <div class="" >
      <div class="bg2" >
        <div class="teste">
          <h2 class="h2-bloc title-2 ">
              <?php echo Yii::t("cms", "Local resources")?>
          </h2>
          <p class="para-bloc title-6 ">
              <?php echo Yii::t("cms", "Discover the vraks resources available in the commune")?>
          </p>
        </div>
        <div class="">
          <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'},null,dyfOrga);" class="btn btn-lg btn-default"><?php echo Yii::t("cms", "Make an offer")?></a>
        </div>
        <div class=" " >
          <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
              <?php echo Yii::t("cms", "See more")?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="col-md-4 col-sm-6 col-xs-12   text-center colonne " >
  <div class=" colonne1ligne2">
    <div class="" >
      <div class="bg2" >
        <div class="teste">
          <h2 class="h2-bloc title-2 ">
              <?php echo Yii::t("common", "Project")?>
          </h2>
          <p class="para-bloc title-6 ">
              <?php echo Yii::t("cms", "Do you have a project that is close to your heart? Share it")?>
          </p>
        </div>
        <div class="">
          <a href="javascript:;" onclick="dyFObj.openForm('project',null,cocityCoordonate,null,dyfOrga);"  class="btn btn-lg btn-default"><?php echo Yii::t("cms", "Propose a project")?></a>
        </div>
        <div class=" " >
          <a href="javascript:;" data-hash="#project"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
              <?php echo Yii::t("cms", "See more")?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="col-md-4 col-sm-6 col-xs-12   text-center colonne " >
  <div class=" colonne2ligne2">
    <div class="" >
      <div class="bg2" >
        <div class="teste">
          <h2 class="h2-bloc title-2 ">
              <?php echo Yii::t("common", "Event")?>
          </h2>
          <p class="para-bloc title-6 ">
              <?php echo Yii::t("cms", "You want to organize an event and are looking for a way to share it?")?>
          </p>
        </div>
        <div class="">
          <a href="javascript:;" onclick="dyFObj.openForm('event',null,cocityCoordonate,null,dyfOrga);"  class="btn btn-lg btn-default"><?php echo Yii::t("common", "Propose an event")?></a>
        </div>
        <div class=" " >
          <a href="javascript:;" data-hash="#agenda"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
              <?php echo Yii::t("cms", "See more")?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-4 col-sm-6 col-xs-12   text-center colonne " >
  <div class=" colonne3ligne2">
    <div class="" >
      <div class="bg2" >
        <div class="teste">
          <h2 class="h2-bloc title-2 ">
              <?php echo Yii::t("common", "Organization")?>
          </h2>
          <p class="para-bloc title-6 ">
              <?php echo Yii::t("cms", "Are you an organization and want to be listed? Share it.")?>
          </p>
        </div>
        <div class="">
          <a href="javascript:;" onclick="dyFObj.openForm('organization',null,cocityCoordonate,null,dyfOrga);" class="btn btn-lg btn-default"><?php echo Yii::t("cms", "Propose an organization")?>
          </a>
        </div>
        <div class=" " >
          <a href="javascript:;" data-hash="#organization"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
              <?php echo Yii::t("cms", "See more")?>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script type="text/javascript">
  
  dyfOrga.afterSave = function(data){
    dyFObj.commonAfterSave(data, function(){
      urlCtrl.loadByHash(location.hash);  
    });
  }
  
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
   var data = 

    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",

        "properties" : {
          
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    

  });
</script>