<?php
$keyTpl ="timeline";
$paramsData = [
    "titleBlock" => "TIMELINE",
    "panel"=> "transparent",
    "iconColor" => "#49cbe3",
    "iconBgColor" => "#1A2660",
    "timelineData"       => null
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<style type="text/css">

    .timeline-block {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
    }

    .timeline-block:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: <?= $paramsData["iconColor"] ?>;
        left: 0%;
        margin-left: -1.5px;
        border-left: dashed;
    }

    .timeline-block > li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline-block > li:before,
    .timeline-block > li:after {
        content: " ";
        display: table;
    }

    .timeline-block > li:after {
        clear: both;
    }

    .timeline-block > li:before,
    .timeline-block > li:after {
        content: " ";
        display: table;
    }

    .timeline-block > li:after {
        clear: both;
    }

    .timeline-block > li > .timeline-panel {
        width: 94%;
        float: left;
        border: 1px solid <?= $paramsData["panel"] ?>;
        background-color: <?= $paramsData["panel"] ?>;
        border-radius: 2px;
        padding: 20px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline-block > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid <?= $paramsData["panel"] ?>;
        border-right: 0 solid <?= $paramsData["panel"] ?>;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline-block > li > .timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid <?= $paramsData["panel"] ?>;
        border-right: 0 solid <?= $paramsData["panel"] ?>;
        border-bottom: 14px solid transparent;
        content: " ";
    }
    .timeline-block > li.timeline-inverted, .timeline-container-offset {
        float: none;
    }



    .timeline-block > li > .timeline-badge {
        color: <?= $paramsData["iconColor"] ?>;
        width: 90px;
        height: 90px;
        line-height: 50px;
        font-size: 40px;
        padding: 20px;
        text-align: center;
        position: absolute;
        top: 0px;
        left: 0%;
        margin-left: -35px;
        background-color: <?= $paramsData["iconBgColor"] ?>!important;
        border: 2px solid <?= $paramsData["iconColor"] ?>;
        z-index: 100;
        border-radius: 50%;
    }

    .timeline-block > li.timeline-inverted > .timeline-panel {
        float: right;
    }
    .timeline-block > li.timeline-inverted > .timeline-panel:before {
        display: none;
    }


    .timeline-block > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }


    .timeline-block .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-block .timeline-body > p,
    .timeline-block .timeline-body > ul {
        margin-bottom: 0;
    }

    .timeline-block .timeline-body {
        min-height: 70px;
    }


    .timeline-block .timeline-body > p  {
        font-size: 20px;
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .timeline-block > li > .timeline-badge {
            color: #49cbe3;
            width: 70px;
            height: 70px;
            font-size: 32px;
            padding: 8px;
            margin-left: -24px;
        }
    }


    @media (max-width: 767px) {
        ul.timeline-block:before {
            left: 25px;
        }
        .timeline-block > li.timeline-inverted > .timeline-panel:before {
            display: block;
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }
        .timeline-block > li > .timeline-badge {
            width: 50px;
            height: 50px;
            display: block;
            line-height: 35px;
            font-size: 1.4em;
            margin-left: 12px!important;
            padding: 4px;
        }

        ul.timeline-block > li > .timeline-panel {
            width: calc(100% - 60px);
            width: -moz-calc(100% - 60px);
            width: -webkit-calc(100% - 60px);
        }

        ul.timeline-block > li > .timeline-badge {
            left: 0px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline-block > li > .timeline-panel {
            float: right;
        }

        ul.timeline-block > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline-block > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }

    .timeline-block .page-header h1 {
        color : <?= $paramsData["iconColor"] ?>;
    }

    .timeline-block .timeline-heading h4.timeline-title, .timeline-block .timeline-body p  {
        color: #ffffff;
    }
    #particle-js {
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: 50% 50%;
        position: absolute;
        top: 0px;
        z-index: 0;
    }
</style>

<div class="container<?php echo $kunik ?> col-xl-12 no-padding">
    <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 timeline-container-offset z-index-10 ">
        <h3 class="title<?php echo $kunik ?> sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock">
            <?php echo $paramsData["titleBlock"] ?>
        </h3>
    </div>



        <?php
        if (isset($paramsData["timelineData"])) { ?>
            <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 timeline-container-offset">
            <ul class="timeline timeline-block z-index-10 ">
                <?php
                foreach ($paramsData["timelineData"] as $key => $value) {?>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="fa fa-<?php echo $value['icon'] ?>"></i></div>
                        <div class="timeline-panel">
                            <!--<div class="timeline-heading">
                                <h4 class="timeline-title"><?php /*echo $value['nom'] */?></h4>
                            </div>-->
                            <div class="timeline-body">
                                <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="timelineData.<?= $key?>.nom"><?php echo $value['nom'] ?></p>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            </div>
        <?php } ?>
    <div id="particle-js"></div>
</div>



<script type="text/javascript">
    callParticle("particle-js");
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms','Set up your section') ?>",
                "description" : "<?php echo Yii::t('cms','Customize your section') ?>",
                "icon" : "fa-cog",

                "properties" : {
                    "panel":{
                        label : "<?php echo Yii::t('cms','Color of the panel body') ?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.panel
                    },
                    "iconColor":{
                        label : "<?php echo Yii::t('cms','Color of the icon') ?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.iconColor
                    },
                    "iconBgColor":{
                        label : "<?php echo Yii::t('cms','Background color of the icon') ?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.iconBgColor
                    },
                    "timelineData" : {
                        "label" : "<?php echo Yii::t('cms','Timeline') ?>",
                        "inputType" : "lists",
                        "entries":{
                            "key":{
                                "type":"hidden",
                                "class":""
                            },
                            "icon":{
                                "label":"<?php echo Yii::t('cms','Icon') ?>",
                                "type":"select",
                                "class":"col-md-4 col-sm-4 col-xs-11"
                            },
                            "nom":{
                                "label":"<?php echo Yii::t('cms','Description') ?>",
                                "type":"textarea",
                                "class":"col-md-7 col-sm-7 col-xs-11"
                            }
                        }
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent")
                            tplCtx.value[k] = formData.parent;

                        if(k == "timelineData")
                            tplCtx.value[k] = data.timelineData;
                    });
                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            createFontAwesomeSelect<?php echo $kunik ?>();
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });

    function createFontAwesomeSelect<?php echo $kunik ?>(){
        dyFObj.init.buildEntryList = function(num, entry,field, value){
            mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
            countEntry=Object.keys(entry).length;
            defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";

            str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
            $.each(entry, function(e, v){
                name=field+e+num;
                classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : "";
                str+="<div class='"+classEntry+"'>";
                if(typeof v.label != "undefined"){
                    str+='<div class="padding-5 col-xs-12">'+
                        '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                        '</div>'+
                        '<div class="space5"></div>';
                }
                valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                if(v.type=="hidden")
                    str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="text")
                    str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="textarea")
                    str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                else if(v.type=="select"){
                    str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                    //This is the select Options-----------------//
                    str+= createSelectOptions(valueEntry,fontAwesome);
                    str+='</select>';
                }
                str+="</div>";
            });
            str+='<div class="col-sm-1">'+
                '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                '</div>'+
                '</div>';
            return str;
        };
    }

    function createSelectOptions(current,Obj){
        var html = "";
        $.each(Obj,function(k,v){
            html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
        });
        return html;
    }

    //particle



</script>