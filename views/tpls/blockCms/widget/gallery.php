<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $container = "content-".$kunik." .swiper-container";
  $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
?>

<style id="css-<?= $kunik ?>">
  .content-<?php echo $kunik ?> .swiper-container {
    width: 100%;
    height: 350px;
    position: relative;
  }
  .swiper-slide img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .dark .input-section .panel {
    background-color: #3a3a3a00 !important ;
  }
</style>

<div class="gallery content-<?= $kunik ?>"></div>

<script type="text/javascript">
  var swiperConstruction = {
    init: function() {
      swiperConstruction.views.init();
      swiperObj.initSwiper('.content-<?= $kunik ?>', <?= json_encode($blockCms["swiper"]) ?>);
    },
    views: {
      init: function() {
        swiperObj.htmlSwiper('.content-<?= $kunik ?>');
        swiperConstruction.views.appendImage();
      },
      appendImage: function() {
        var html = "", allImage = [];
        if (notEmpty(<?= json_encode($blockCms["images"]) ?>)) {
          allImage = <?= json_encode($blockCms["images"]) ?>;
          allImage.forEach(function(image) {
            html += "<div class='swiper-slide'><img src='"+image+"'></div>";
          })
          swiperConstruction.actions.allImageLength = allImage.length;
          swiperConstruction.actions.optionsOfSlide = swiperObj.incrementeNumberOfOptions(allImage.length);
          $('.content-<?= $kunik ?> .swiper-wrapper').append(html);
        }
      }
    },
    actions: {
      allImageLength: 0,
      optionsOfSlide: []
    }
  }

  jQuery(document).ready(function() { 
    swiperConstruction.init();

    var styleCss="";
    styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(styleCss);

    if (costum.editMode) {
      var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [] ) ?>;
      swiperObj.inputsOnEdit(paramsDataSwiper);
      cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
      var galleryInput = {
        configTabs : {
          general : {
            inputsConfig: [
              {
                type: 'inputFileImage',
                options: {
                  name : "images",
                  canSelectMultipleImage: true,
                  maxImageSelected: 6,
                  label : tradCms.uploadeImage,
                }
              },
              "width",
              "height"
            ]
          },
          swiper : {
            key: "swiper",
            keyPath : "swiper",
            icon: "image",
            label: "Swiper",
            content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"swiper"),
            inputsConfig : [
              {
                type: "selectGroup",
                options: {
                    label: tradCms.numberSlidetoShow,
                    name: "slidesPerView",
                    inputs:[
                        {
                            name: "md",
                            icon : "desktop",
                            options: notEmpty(swiperConstruction.actions.optionsOfSlide) ? swiperConstruction.actions.optionsOfSlide : ["0"],
                            
                        },
                        {
                            name: "sm",
                            icon : "tablet",
                            options: notEmpty(swiperConstruction.actions.optionsOfSlide) ? swiperConstruction.actions.optionsOfSlide : ["0"]
                            
                        },
                        {
                            name: "xs",
                            icon : "mobile",
                            options: notEmpty(swiperConstruction.actions.optionsOfSlide) ? swiperConstruction.actions.optionsOfSlide : ["0"]
                            
                        }
                    ],
                    defaultValue: (notEmpty(paramsDataSwiper) && typeof paramsDataSwiper["breakpoints"] != "undefined" && typeof paramsDataSwiper["breakpoints"]["slidesPerView"] != "undefined") ? paramsDataSwiper["breakpoints"]["slidesPerView"] : ""
                },
                payload: {
                    path: "swiper.breakpoints.slidesPerView"
                }
              },
              {
                  type: "select",
                  options: {
                      name: "initialSlide",
                      label: tradCms.firstSlide,
                      options: notEmpty(swiperConstruction.actions.optionsOfSlide) ? swiperConstruction.actions.optionsOfSlide : ["0"],
                  }
              },
              "addCommonConfig"
            ]
          },
          style: {
            inputsConfig: [
              "background",
              "addCommonConfig",
              "boxShadow"
            ]
          },
          hover: {
            inputsConfig : [
                "addCommonConfig",
            ]
          },
          advanced: {
              inputsConfig : [
                  "addCommonConfig",
              ]
          }
        },
        afterSave: function(path,valueToSet,name,payload,value) {
          cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
        }
      }
      cmsConstructor.blocks.gallery =  galleryInput;
    }
  }); 
</script>
