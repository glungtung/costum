<?php 
  $keyTpl ="e_community_caroussel";
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [ $kunik => $blockCms["css"] ?? [] ];
?>  

<style id="communityCaroussel<?= $kunik ?>" >
</style>

<div class="community  <?= $kunik ?> ">
</div>

<script type="text/javascript">

//*************** CAROUSSEL ***********************************/
data = <?= json_encode($dataResult) ?? [] ?>;
var swiperConstruction = {
    init: function() {
      swiperConstruction.views.init();
      swiperObj.initSwiper('.<?= $kunik?>', <?= json_encode($blockCms["swiper"]) ?>);
    },
    views: {
      init: function() {
        swiperObj.htmlSwiper('.<?= $kunik ?>');
        swiperConstruction.views.appendCommunity(data);
      },
      appendCommunity: function(data) {
        var html = "";
        if (notEmpty(data)) {
          $.each(data,function(k,v){
                html += 
                `<div class="swiper-slide">
                    <div class="inner">
                      <div class="img-container" style="background-image:url(${v.profilMediumImageUrl})"></div>
                    </div>
                    <div class="info">
                      <h3 class="text-center">
                        <a class="subtitle lbh" href="#page.type.${v.collection}.id.${k}"> ${v.name}</a>
                      </h3>
                      <h5 class="other">${exists(v.rolesLink)? v.rolesLink.join(",") : ""}</h5>
                      <p class="description">${exists(v.shortDescription) ? v.shortDescription : ""}</p>
                    </div>
                </div>`;
          })
          
        } else {
          html = `<div><h1>Aucun membre</></div>`;
        }
        $('.<?= $kunik?> .swiper-wrapper').append(html);
      }
    },
  }
      
  coInterface.bindLBHLinks();
  //***************END CAROUSSEL***********************************/
  swiperConstruction.init();
  str="";
  str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#communityCaroussel<?= $kunik ?>").append(str);
  if (costum.editMode){
    var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [] ) ?>;
    var optionsOfSlide = swiperObj.incrementeNumberOfOptions(<?= count($dataResult) ?>); 
    swiperObj.inputsOnEdit(paramsDataSwiper);
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ; 
		communityInput = {
      configTabs : {
                general : {
                  inputsConfig : [
                            "width",
                            "height",
                            "background"
                  ]
                },
                swiper : {
                    key: "swiper",
                    keyPath : "swiper",
                    icon: "image",
                    label: "Swiper",
                    content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"swiper"),
                    inputsConfig : [
                      {
                        type: "selectGroup",
                        options: {
                            label: tradCms.numberSlidetoShow,
                            name: "slidesPerView",
                            inputs:[
                                {
                                    name: "md",
                                    icon : "desktop",
                                    options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"],
                                    
                                },
                                {
                                    name: "sm",
                                    icon : "tablet",
                                    options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"]
                                    
                                },
                                {
                                    name: "xs",
                                    icon : "mobile",
                                    options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"]
                                    
                                }
                            ],
                            defaultValue: (notEmpty(paramsDataSwiper) && typeof paramsDataSwiper["breakpoints"] != "undefined" && typeof paramsDataSwiper["breakpoints"]["slidesPerView"] != "undefined") ? paramsDataSwiper["breakpoints"]["slidesPerView"] : ""
                        },
                        payload: {
                            path: "swiper.breakpoints.slidesPerView"
                        }
                      },
                      {
                          type: "select",
                          options: {
                              name: "initialSlide",
                              label: tradCms.firstSlide,
                              options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"],
                          }
                      },
                      "addCommonConfig"
                    ]
                },
                style: {
                  inputsConfig : [
                    "addCommonConfig",
                    {
                    type: "section",
                      options: {
                        name: "swiper-slide",
                        label: tradCms.circle,
                        showInDefault: true,
                        inputs: [
                          {
                            type: "section",
                              options: {
                                name: "inner",
                                label: tradCms.external,
                                showInDefault: true,
                                inputs: [
                                  "background",
                                  "border",
                                ]
                              }
                          },
                          {
                            type: "section",
                              options: {
                                name: "img-container",
                                label: tradCms.internal,
                                showInDefault: true,
                                inputs: [
                                  "background",
                                  "border",
                                ]
                              }
                          },
                        ]
                      }
                    },
                  ],
                },
                // query : {
                //   key: "query",
                //   keyPath : "query",
                //   icon: "gears",
                //   label: "query",
                //   content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"query"),
                //   inputsConfig : [
                //     {
                //           type : "select",
                //           options: {
                //               name : "orderBy",
                //               label : "Order by",
                //               options : ["role", "badges"]
                //           }
                //     },
                //   ],
                // } 
      },
      afterSave: function(path,valueToSet,name,payload,value) {
          cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
      }
    };
		cmsConstructor.blocks.community =  communityInput;
  }
</script>