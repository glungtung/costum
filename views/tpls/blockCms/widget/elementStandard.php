<?php



$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [ $kunik => $blockCms["css"] ?? [] ];

$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);

?>

<style id="elementStandard<?= $kunik ?>">
    .elem-stdr-content-<?= $kunik?> {
        min-height: 100px;
    }
</style>
<div class="content-element<?= $kunik?> <?= $kunik ?>">
    <div class="elem-stdr-content-<?= $kunik?>"></div>
</div>


<script>

   str="";
   str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
   $("#elementStandard<?= $kunik ?>").append(str);

    <?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    mylog.log("ParamsData",<?php echo $kunik ?>ParamsData);
    var dataResult = <?= json_encode($dataResult) ?>;
    var optionsOfSlide = swiperObj.incrementeNumberOfOptions(<?= count($dataResult) ?>);

        var design = {
            "1" : "Carousel multi-carte de communecter",
            "3" : "Non carousel multi-carte configurable",
            "2" : "Carousel à une carte horizontal",
            "4" : "Carousel multi-carte configurable",
            "6" : "Non Carousel à une carte horizontal",
            "5" : "Non Carousel multi-carte de communecter",
            "7" : "Carte rotatif Carousel",
            "8" : "Carte rotatif Non Carousel",
        };
        var orderBy = {
            "name" : "Nom",
            "created" : "Date de création",
            "updated" : "Date de modification",
            "startDate" : "Date de début"
        }
        var orderType = {
            "1" : "Croissant",
            "-1" : "Décroissant",
        }


    if (costum.editMode){
        var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [])?>;
        swiperObj.inputsOnEdit(paramsDataSwiper);
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
            elementInput = {
                configTabs : {
                    query : {
                        key : "query",
                        keyPath : "query",
                        icon: "sliders",
                        label: "Général",
                        content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"query"),
                        inputsConfig : [
                            {
                                type : "select",
                                options : {
                                    name : "designType",
                                    label : "Type de design",
                                    options : $.map( design, function( key, val ) {
                                      return {value: val,label: key}
                                    })
                                },
                                payload: {
                                    path: "displayConfig.designType",
                                }
                            },
                            {
                                type : "selectMultiple",
                                options : {
                                    name : "searchType",
                                    label : "Type d'element",
                                    options : $.map( elTypeOptions, function( key, val ) {
                                        return {value: val,label: key}
                                    })
                                },
                            },
                            {
                                type: "inputSimple",
                                options: {
                                    name: "indexStep",
                                    label: "nombre de résultat afficher",
                                },
                            },
                            {
                                type : "select",
                                options : {
                                    name : "orderBy",
                                    label : "Trier par",
                                    options : $.map( orderBy, function( key, val ) {
                                        return {value: val,label: key}
                                    })
                                },
                            },
                            {
                                type : "select",
                                options : {
                                    name : "orderType",
                                    label : "Type d'ordre",
                                    options : $.map( orderType, function( key, val ) {
                                        return {value: val,label: key}
                                    })
                                },
                            },
                        ]
                    },
                    swiper : {
                        key: "swiper",
                        keyPath : "swiper",
                        icon: "image",
                        label: "swiper",
                        content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"swiper"),
                        inputsConfig : [
                            {
                                type: "selectGroup",
                                options: {
                                    label: tradCms.numberSlidetoShow,
                                    name: "slidesPerView",
                                    inputs:[
                                        {
                                            name: "md",
                                            icon : "desktop",
                                            options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"],
                                            
                                        },
                                        {
                                            name: "sm",
                                            icon : "tablet",
                                            options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"]
                                            
                                        },
                                        {
                                            name: "xs",
                                            icon : "mobile",
                                            options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"]
                                            
                                        }
                                    ],
                                    defaultValue: (notEmpty(paramsDataSwiper) && typeof paramsDataSwiper["breakpoints"] != "undefined" && typeof paramsDataSwiper["breakpoints"]["slidesPerView"] != "undefined") ? paramsDataSwiper["breakpoints"]["slidesPerView"] : ""
                                },
                                payload: {
                                    path: "swiper.breakpoints.slidesPerView"
                                }
                            },
                            {
                                type: "select",
                                options: {
                                    name: "initialSlide",
                                    label: tradCms.firstSlide,
                                    options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"],
                                }
                            },
                            "addCommonConfig"
                        ]
                    },
                    style: {
                        inputsConfig : [
                            "addCommonConfig",
                            "textShadow",
                            "fontSize"
                        ]
                    },
                    hover: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    },
                    advanced: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
        };
        cmsConstructor.blocks.elementStandard =  elementInput;
    }





    var elementStandardObj = {
        bindEvents: function(paramsData, kunik) {
            // ajouter ici les evennement des edition et suppression
            $('.elem-stdr-content-' + kunik + ' .smartgrid-slide-element').removeClass('col-lg-4 col-md-4 col-sm-6');
            $('.elem-stdr-content-' + kunik + ' .swiper-slide .lbh').removeClass('lbh').addClass('lbh-preview-element');
            $('.elem-stdr-content-' + kunik + ' .swiper-slide .see-more').removeClass('lbh-preview-element').addClass('lbh');
            document.querySelectorAll('img.lzy_img').forEach((v) => {
                imageObserver.observe(v);
            });
            coInterface.bindLBHLinks();
            directory.bindBtnElement();
        },
       directoryCart : {
           init: function(paramsData, kunik) {
               var $this = this;
               $this.cssCarousel(paramsData, kunik);
               elementStandardObj.directoryCart.fetchData(paramsData,kunik);
               swiperObj.initSwiper('.elem-stdr-content-<?= $kunik?>',paramsData["swiper"]);
               elementStandardObj.bindEvents(paramsData, kunik);
           },
           fetchData: function(paramsData, kunik) {
               var html = "";
               var dataEl = {} ;
               $.each(dataResult, function(k, v) {
                   if (exists(paramsData["displayConfig"]["designType"]) && (paramsData["displayConfig"]["designType"] == 1 || paramsData["displayConfig"]["designType"] == 5)) {
                       v = directory.prepParamsHtml(v);
                       html += `<div class="swiper-slide">`;
                       if (paramsData["query"]["searchType"] == "events" ) {
                           html += directory.eventPanelHtml(v);
                       }
                       else if (paramsData["query"]["searchType"] == "classifieds")
                           html += directory.classifiedPanelHtml(v);
                       else
                           html += directory.elementPanelHtml(v);
                       html += `</div>`;
                   }else if (exists(paramsData["displayConfig"]["designType"]) && (paramsData["displayConfig"]["designType"] == 7 || paramsData["displayConfig"]["designType"] == 8)) {
                       v = directory.prepParamsHtml(v);
                       html += `<div class="swiper-slide">`;
                       html += directory.rotatingCard(v);
                       html += `</div>`;
                   }
               });
               mylog.log("swiperObj",swiperObj);
               swiperObj.htmlSwiper('.elem-stdr-content-<?= $kunik?>');
               $('.elem-stdr-content-<?= $kunik?> .swiper-wrapper').html(html);

           },
           cssCarousel: function(paramsData, kunik) {
               var imgDisplay = (exists(paramsData["displayConfig"]) && exists(paramsData["displayConfig"]["imgShow"])) ? paramsData["displayConfig"]["imgShow"] : paramsData["imgShow"];
               var containerKunik = ".container-" + kunik;
               var css = `<style>`;
               if (exists(paramsData["displayConfig"]["designType"]) && (paramsData["displayConfig"]["designType"] == 5,paramsData["displayConfig"]["designType"] == 8)) {
                   css += `${containerKunik} .swiper-slide {
                            display: block;
                       }`
               }else {
                   css += `${containerKunik} .swiper-slide {
                          display: -webkit-box;
                          display: -ms-flexbox;
                          display: -webkit-flex;
                          display: flex;
                          }`
               }

               if (exists(paramsData["displayConfig"]["designType"]) && (paramsData["displayConfig"]["designType"] == 3 || paramsData["displayConfig"]["designType"] == 5 || paramsData["displayConfig"]["designType"] == 8)) {
                   css += `.elem-stdr-content-${kunik} .swiper-pagination,
                          .elem-stdr-content-${kunik} .swiper-button-next,
                          .elem-stdr-content-${kunik} .swiper-button-prev{
                              display:none
                          }

                          .elem-stdr-content-${kunik} .swiper-wrapper{
                            flex-wrap : wrap;
                            justify-content: space-evenly;
                          }
                          .elem-stdr-content-${kunik} .swiper-slide{
                            margin-right: 0px !important;
                          }
                          @media (max-width:400px){
                            .elem-stdr-content-${kunik} .swiper-slide{
                                flex-basis : calc(95%/ ${exists(paramsData["swiper"]["breakpoints"]["slidesPerView"]["xs"]) ? paramsData["swiper"]["breakpoints"]["slidesPerView"]["xs"] : 1}) !important;
                            }
                          }
                          @media (max-width:768px){
                            .elem-stdr-content-${kunik} .swiper-slide{
                                flex-basis : calc(95%/ ${exists(paramsData["swiper"]["breakpoints"]["slidesPerView"]["sm"]) ? paramsData["swiper"]["breakpoints"]["slidesPerView"]["sm"] : 2}) !important;
                            }
                          }
                          @media (min-width:769px){
                            .elem-stdr-content-${kunik} .swiper-slide{
                                flex-basis : calc(95%/ ${exists(paramsData["swiper"]["breakpoints"]["slidesPerView"]["md"]) ? paramsData["swiper"]["breakpoints"]["slidesPerView"]["md"] : 3}) !important;
                            }
                          }
                  </style>`;

                   $('head').append(css);
               }
           }
       }
    }

    $(function(){
        <?php if(isset($paramsData["displayConfig"]) && isset($paramsData["displayConfig"]["designType"])){
        $designCard = $paramsData["displayConfig"]["designType"];
        if($designCard==1 || $designCard==5 || $designCard==7 || $designCard==8){ ?>
        elementStandardObj.directoryCart.init(<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
        <?php }
        } ?>
    })



</script>
