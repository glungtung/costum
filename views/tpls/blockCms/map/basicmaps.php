<?php
  $keyTpl ="basicmaps";

  $paramsData = [
    "activeFilter" => true,
    "mapHeight" => 550,
    "elementsType" => ["projects","organizations","events", "citoyens","poi"],
    "filterHeaderColor" =>  "#AFCB21",
    "btnAnnuaireText" =>  "Voir dans l'annuaire",
    "btnAddText" =>  "",
    "btnBackground" =>  "#AFCB21",
    "btnBorderColor" =>  "#AFCB21",
    "btnBorderRadius" =>  2,
    "btnAnnuaireTextColor" =>  "white",
    "btnLink" =>  "search",
    "isBtnLbh" =>  true,
    "legendShow" =>  false,
    "legendConfig" => [],
    "legendCible" => "",
    "activeContour" => false,
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  if($paramsData["elementsType"]==""){
    $paramsData["elementsType"] = ["projects","organizations","events", "citoyens","poi"];
  }

 ?>
<style>
    .basicmaps-container<?= $kunik ?>{
        padding-top : 0px !important;
    }

    .basicmaps-container<?= $kunik ?> #menuRightmap<?php echo $kunik ?>{
        position: absolute !important;
    }
    .basicmaps-container<?= $kunik ?> a.title-4:focus,.basicmaps-container<?= $kunik ?> a.title-4:hover{
        font-weight: bold;
        text-decoration:none !important;
        border-bottom: 3px solid <?= @$blockCms["blockCmsColorTitle4"]; ?>;
    }
    .basicmaps-container<?= $kunik ?> a.title-4{
        padding-right: 0px;
        padding-left: 0px;
        margin: 0 15px;
    }
    .basicmaps-container<?= $kunik ?> .title-1{
        margin-top:-100px;
    }
    .menuRight .menuRight_header, .menuRight .panel_map, .menuRight .btn-panel {
        background-color: <?php echo $paramsData['filterHeaderColor']; ?> !important;
        box-shadow: none !important;
    }

    .leaflet-popup-tip-container {
        bottom: -29px;
    }

    .m-1{
        margin: 0.3em;
    }

    .rounded<?= $kunik ?>{
        border-radius: 40px;
        text-transform: uppercase;
    }

    #thematic<?= $kunik ?>{
        background: #eee;
        /*padding: 0.3em;*/
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: white;
        font-weight: 900 !important;
    }

    #annuaireButton<?= $kunik ?> {
        background: <?= $paramsData["btnBackground"]?>;
        color: <?= $paramsData["btnAnnuaireTextColor"]?>;
        border-radius : <?= $paramsData["btnBorderRadius"]?>px;
        border-color : <?= $paramsData["btnBorderColor"]?>px;
        border-width: 1px;
        text-align: center;
    }

    .buttons<?= $kunik ?>{
        position: absolute;
        top: auto;
        right: 20px;
        z-index: 400;
        margin: 2%;
    }

    .buttons<?= $kunik ?> *{
        padding: 10px 15px;
        display: block;
        font-size: 20px;
    }

    .addButton<?= $kunik ?>{
        font-family: inherit !important;
        background-color: #76A5AF;
        color: white;
    }

    .legende-container<?= $kunik?>{
        position: absolute;
        left: auto;
        top: 50px;
        display: inline-block;
        z-index: 2;
        margin: 4px 0 2% 4%;
        border-radius: 10px;
        background: white;
        border:  2px solid #ddd;
   }

   .legende-container<?= $kunik?> ul {
        list-style-type: none;
        padding: 2px 3px 2px 8px;

    }
    #map<?= $kunik?> .leaflet-container{
        position: inherit !important;
        height: inherit !important;
        /*margin: 0 10px 0 10px;*/
    }
</style>
<!--div id="filters<?= $kunik ?>" class="searchObjCSS text-center menuFilters menuFilters-vertical col-xs-12 bg-light text-center"></div>
<br><br><br><br-->
<div class="basicmaps-container<?= $kunik ?>">
  <div id="thematic<?= $kunik ?>"></div>
  <?php if($paramsData["legendShow"]=="true"){ ?>
    <div class="legende-container<?= $kunik?>">
        <div class="legend-header<?= $kunik?> active padding-10">
            <p style="display:flex">LEGENDE <!--i class="fa fa-angle-down"></i--></p>
        </div>
        <div class="legend-body<?= $kunik?>">
            <div id="legende<?= $kunik?>" >
                <div class="legende-content<?= $kunik?>"></div>
            </div>

        </div>
    </div>
  <?php } ?>  
    <div class="buttons<?= $kunik ?>">
        <a id="annuaireButton<?= $kunik ?>" href="#<?=$paramsData["btnLink"]?>" class="<?=($paramsData["isBtnLbh"]!=0)?'btn lbh':'lbh-anchor'?>" data-filters=""><?php echo $paramsData["btnAnnuaireText"] ?></a>
        
        <?php if($paramsData["btnAddText"]!=""){?>
        <button class="btn margin-top-5 addButton<?= $kunik ?>" onclick="dyFObj.openForm('organization')"><?php echo $paramsData["btnAddText"] ?></button>
        <?php } ?>
    </div>
 
  
  <div id="map<?= $kunik ?>" style="position: relative; height: <?php echo $paramsData["mapHeight"]; ?>px; overflow:show"></div>
</div>


<script>
    var contextData = <?php echo json_encode($el); ?>;
    var filteredTheme = "";
    var filteredKeyValue = {};

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    $(function(){

        var customIcon={tile : "maptiler"};
        if(typeof paramsMapCO.mapCustom!="undefined"){
            paramsMapCO.mapCustom.mapLegends = sectionDyf.<?= $kunik ?>ParamsData.legendConfig;
            customIcon = paramsMapCO.mapCustom;
        }else{
            paramsMapCO = $.extend(true, {}, paramsMapCO, {
                mapCustom: {
                    mapLegends: [],
                    icon: {
                        getIcon: function (params) {
                            var elt = params.elt;

                            var myCustomColour = '#F88518';

                            if (typeof elt.tags != "undefined") {
                                $.each(elt.tags, function (index, tag) {
                                    let sameKey = paramsMapCO.mapCustom.mapLegends.find(element => (element.label == tradCategory[tag] || element.label == tag));
                                    if (typeof sameKey != "undefined") {
                                        myCustomColour = sameKey.color;
                                    }
                                });
                            }

                            var markerHtmlStyles = `
                                background-color: ${myCustomColour};
                                width: 3.5rem;
                                height: 3.5rem;
                                display: block;
                                left: -1.5rem;
                                top: -1.5rem;
                                position: relative;
                                border-radius: 3rem 3rem 0;
                                transform: rotate(45deg);
                                border: 1px solid #FFFFFF`;

                            var myIcon = L.divIcon({
                                className: "my-custom-pin",
                                iconAnchor: [0, 24],
                                labelAnchor: [-6, 0],
                                popupAnchor: [0, -36],
                                html: `<span style="${markerHtmlStyles}" />`
                            });
                            return myIcon;
                        }
                    },
                    getClusterIcon: function (cluster) {
                        var childCount = cluster.getChildCount();
                        var c = ' marker-cluster-';
                        if (childCount < 100) {
                            c += 'small-'+costum.contextSlug;
                        } else if (childCount < 1000) {
                            c += 'medium-'+costum.contextSlug;
                        } else {
                            c += 'large-'+costum.contextSlug;
                        }
                        return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
                    }
                }
            });
        }

        var map<?= $kunik ?> = new CoMap({
            container : "#map<?= $kunik ?>",
            activePopUp : true,
            mapOpt:{
                <?= ($paramsData["activeFilter"]!="" && $paramsData["activeFilter"]=="true")?'menuRight:true,':'' ?>
                btnHide : false,
                doubleClick : true,
                scrollWheelZoom: false,
                zoom : 2,
            },
            mapCustom:customIcon,
            elts : []
        });


        mylog.log("basic map map<?= $kunik ?>", map<?= $kunik ?>);

        // map<?= $kunik ?> = $.extend(true, {}, paramsMapCO, map<?= $kunik ?>);

        /*map<?= $kunik ?>.once('focus', function() {
            map<?= $kunik ?>.scrollWheelZoom.enable();
        });*/
/**
        $("#mapContent").remove();

        try{
            createFilters("#filters<?= $kunik ?>", false, "#map<?= $kunik ?>");
            filterSearch = searchObj.init(paramsFilter);
        }catch(e){

        }
*/
        let defaultFilters<?= $kunik ?> = {'$or':{}};
        defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
        defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
        /*if(costum.contextType!="projects"){
            defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
        }*/
        defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
        defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};

        function mapLegende<?= $kunik ?>(params){
            var legendHTML = $('<ul></ul>');
            var legendColors = [];
            var legendLabels = {};


            if(typeof costum.lists !="undefined" && costum.lists[sectionDyf.<?= $kunik ?>ParamsData.legendCible]){
                legendLabels = costum.lists[sectionDyf.<?= $kunik ?>ParamsData.legendCible];
            }

            if (sectionDyf.<?= $kunik ?>ParamsData.legendConfig.length!=0) {
                legendColors = sectionDyf.<?= $kunik ?>ParamsData.legendConfig;
            } else {
                for(let key in legendLabels){
                    let legColor = "#F88518";
                    let sameKey = params.find(element => (element.label == tradCategory[key] || element.label == key) );

                    if(typeof sameKey != "undefined"){
                        legColor = sameKey.color;
                    }
                    
                    //var labelValue = legendLabels[key];
                    legendColors.push({
                        color: legColor,
                        label: (tradCategory[key] || legendLabels[key] || key),
                        key:key
                    });
                }
            }

            legendColors.map(item => {
                legendHTML.append(`
                <li>
                    <a class="btn rounded<?= $kunik ?> thematic<?= $kunik ?>" type="button" data-field="<?= $paramsData["legendCible"] ?>" data-filters="${item.key||item.label}">
                        <i class="fa fa-map-marker" style="color:${item.color}; font-size:16pt"></i>
                        ${ item.label}
                    </a>
                </li>`)
            });

            if(sectionDyf.<?= $kunik ?>ParamsData.legendConfig.length==0){
                sectionDyf.<?= $kunik ?>ParamsData.legendConfig = legendColors;
                paramsMapCO.mapCustom.mapLegends = legendColors;
            }

            $('#legende<?= $kunik?> .legende-content<?= $kunik?>').html(legendHTML);
        }

        function getElements<?= $kunik ?>(criteriaParams = null, keyValue=null){
            var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
            params = {
                notSourceKey: true,
                searchType : <?= json_encode($paramsData["elementsType"]) ?>,
                fields : mapSearchFields,
                activeContour: <?= json_encode($paramsData["activeContour"]) ?>,
                indexStep: "0",
                filters: defaultFilters<?= $kunik ?>
            };

            if(criteriaParams!=null && criteriaParams!=""){
                params["searchTags"] = criteriaParams.split(",");
            }

            if(keyValue!=null){
                params["filters"] = {...(params.filters),  ...keyValue};
            }

            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    if(typeof data != "undefined" && typeof data.zones != "undefined"){
                        Object.assign(data.results, data.zones);
                    }
                    map<?= $kunik ?>.clearMap();
                    map<?= $kunik ?>.addElts(data.results);
                        
                }
            )
        }

        mapLegende<?= $kunik ?>(sectionDyf.<?= $kunik ?>ParamsData.legendConfig);
        
        getElements<?= $kunik ?>();

        /********* Filter thematics *******************/
        var thematic = [];
        if(costum && costum.lists && sectionDyf.<?= $kunik ?>ParamsData.legendShow!="true"){
            var inList = (costum.lists.theme || costum.lists.typologyOfActors || {});
            for (const [keyT, valueT] of Object.entries(inList)) {
                var asTags = [];
                let labelT = "";

                if(typeof valueT == "string"){
                    asTags.push(valueT);
                    labelT = valueT;
                }else{
                    labelT = keyT;
                    for (const [keyChild, valueChild] of Object.entries(valueT)){
                        if(!thematic.includes(valueChild)){
                            thematic.push(valueChild);
                        }
                        asTags.push(valueChild);
                    }
                }
                $("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
            }
        }else{
            $("#thematic<?= $kunik ?>").remove();
        }

        $(".thematic<?= $kunik ?>").off().on("click",function(){
            if($(this).data("field")!="" && $(this).data("filters")!=""){
                if(filteredKeyValue[$(this).data("field")]){
                    delete filteredKeyValue[$(this).data("field")];
                    $(this).removeClass("btn-primary");
                    $(this).addClass("btn-white");
                }else{
                    filteredKeyValue[$(this).data("field")] = $(this).data("filters");
                    $(this).removeClass("btn-white");
                    $(this).addClass("btn-primary");
                }
            }else{
                if(filteredTheme.indexOf($(this).data("filters")) !== -1){
                    filteredTheme = filteredTheme.replace($(this).data("filters"), "");
                }else{
                    if(filteredTheme==""){
                        filteredTheme = $(this).data("filters");
                        $(this).removeClass("btn-primary");
                        $(this).addClass("btn-white");
                    }else{
                        filteredTheme+=","+$(this).data("filters");
                        $(this).removeClass("btn-white");
                        $(this).addClass("btn-primary");
                    }
                    $("#annuaireButton<?= $kunik ?>").attr("href", "#<?= $paramsData['btnLink']; ?>?tags="+filteredTheme);
                }

                if(filteredTheme.charAt(0)==","){
                    filteredTheme = filteredTheme.substring(1);
                }

                if(filteredTheme.length==0){
                    $("#annuaireButton<?= $kunik ?>").attr("href", "#<?= $paramsData['btnLink']; ?>");
                }
            }

            getElements<?= $kunik ?>(filteredTheme, filteredKeyValue);
        });

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {
                "activeFilter" :{
                      "inputType" : "select",
                      "label" : "Filtre",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.activeFilter,
                      "options" : {
                        "true" : "Map avec filtre",
                        "false" : "Map sans filtre"
                    }
                },
                "mapHeight" :{
                      "inputType" : "text",
                      "rules" : {
                        "number" : true
                      },
                      "label" : "Hauteur du map",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.mapHeight,
                },
                "elementsType" :{
                    "inputType" : "selectMultiple",
                    "label" : "Elément du map",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.elementsType,
                    "class" : "multi-select select2 form-control",
                    "options" : {
                        "organizations" : trad.organizations,
                        "citoyens" : tradCategory.citizen,
                        "events" : trad.events,
                        "projects" : trad.projects,
                        "poi" : trad.poi
                    }
                },
                "btnAnnuaireText" :{
                      "inputType" : "text",
                      "label" : "Text sur bouton annuaire",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.btnAnnuaireText
                },
                "btnAddText" :{
                      "inputType" : "text",
                      "label" : "Text sur bouton nouvelle organizations",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.btnAddText
                },
                "btnBackground" : {
                    "label" : "Couleur de fond du bouton",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBackground
                },
                "btnAnnuaireTextColor" : {
                    "label" : "Couleur du label du bouton",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnAnnuaireTextColor
                },
                "btnBorderRadius" : {
                    "label" : "Rond du bordure",
                    inputType : "text",
                    "rules" : {
                        "number":true
                    },
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBorderRadius
                },
                "btnBorderColor" : {
                    "label" : "Couleur du bordure",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBorderColor
                },
                "btnLink" : {
                    "label" : "Lien cible du bouton annuaire",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnLink
                },
                "filterHeaderColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur de l'entête du filtre",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.filterHeaderColor
                },
                "legendShow" : {
                    "inputType" : "checkboxSimple",
                    "label" : "Afficher de legende",
                    "rules" : {
                        "required" : true
                    },
                    "params" : {
                        "onText" : trad.yes,
                        "offText" : trad.no,
                        "onLabel" : trad.yes+" <a href='#privacy' target='_blank'><b>("+trad.readmore+")</b></a>",
                        "offLabel" : trad.no+" <a href='#privacy' target='_blank'><b>("+trad.readmore+")</b></a>"
                    },
                    "checked" : false
                },
                "legendCible" : {
                    "inputType" : "select",
                    "label" : "Légende sur : ",
                    "options" : Object.keys((costum.lists||{})).reduce((a,b)=> {
                        return (a[b]=b, a);
                    },{})
                },
                "legendConfig":{
                    "label" : "Préférence sur le légende",
                    "inputType" : "lists",
                    "entries" : {
                        "color" : {
                            "type" : "colorpicker",
                            "label": "Couleur",
                            "class": "col-md-5",
                            "placeholder":"Couleur"
                        },
                        "label" : {
                            "type" : "text",
                            "label": "label",
                            "class": "col-md-5",
                            "placeholder":"Libellé"
                        }
                    }
                },
                "activeContour" : {
                    "inputType" : "checkboxSimple",
                    "label" : "Afficher la contour de la zone",
                    "rules" : {
                        "required" : true
                    },
                    "params" : {
                        "onText" : trad.yes,
                        "offText" : trad.no,
                        "onLabel" : trad.yes,
                        "offLabel" : trad.no
                    },
                    "checked" : false
                },
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            afterBuild : function(){
                if(sectionDyf.<?php echo $kunik?>ParamsData.legendShow!="true" && sectionDyf.<?php echo $kunik?>ParamsData.legendShow!=true){
                    $(".legendConfiglists").hide();
                    $(".legendCibleselect").hide();
                }
                $("#legendCible").on("change", function(){
                    sectionDyf.<?php echo $kunik ?>ParamsData.legendCible = $(this).val();

                    if($(this).val()!=""){
                        sectionDyf.<?= $kunik ?>ParamsData.legendConfig = [];
                    }
                    
                    mapLegende<?= $kunik ?>(sectionDyf.<?= $kunik ?>ParamsData.legendConfig);
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
                    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
                    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"legend",6,12,null,null,"Préférence sur le légende","green","");
                });
            },
            save : function (data) {
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
                if (k == "legendConfig"){
                    tplCtx.value[k] = [];
                    $.each(data.legendConfig, function(index, va){
                        tplCtx.value[k].push(va);
                    })
                }
              });
              if(typeof tplCtx.value == "undefined"){
                toastr.error('value cannot be empty!');
              }else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        dyFObj.closeForm();
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }
            }
          }
        }

        costum.checkboxSimpleEvent = {
            true : function(id){
                if(id=="legendShow"){
                    $("#ajaxFormModal .legendConfiglists").show();
                    $("#ajaxFormModal .legendCibleselect").show();
                }
            },
            false : function(id){
                if(id=="legendShow"){
                    $("#ajaxFormModal .legendConfiglists").hide();
                    $("#ajaxFormModal .legendCibleselect").hide();
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btnAnnuaire",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"legend",6,12,null,null,"Préférence sur le légende","green","");
        });

        <?php if($paramsData["isBtnLbh"]!="true" || !$paramsData["isBtnLbh"]){ ?>
            $("#annuaireButton<?= $kunik ?>").on("click", function() {
                var elementtoScrollToID = $(this).attr("href");
                $([document.documentElement, document.body]).animate({
                        scrollTop: $(elementtoScrollToID).offset().top
                }, 2000);
            });
        <?php } ?>
    });
</script>
