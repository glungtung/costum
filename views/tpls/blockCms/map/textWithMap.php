<?php 
$keyTpl = "textWithMap";
$paramsData = [ 
    "title"=>"Lorem Ipsum",
    "content" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",   
    "tags"           => "acteurs kosasa",
    "lienButton"=>"",
    "labelbutton"=>"Boutton",
    "colorLabelButton"=>"",
    "colorbutton"=>"",
    "colorBorderButton"=>"",
    "class"=> "lbh",
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<style type="text/css">
    .textBack_<?= $kunik ?>{
        width: 100%;         
    }
    .textBack_<?= $kunik ?> h2{
        margin-top: 2%;
        
    }
    .textBack_<?= $kunik ?> p{
        margin-top: 2%;
        text-transform: none;
        font-size: 20px;
    }
    .button_<?= $kunik ?> {
        background-color: <?= $paramsData["colorbutton"]?>; 
        border:1px solid <?= $paramsData["colorBorderButton"]?>;
        color: <?= $paramsData["colorLabelButton"]?>;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        padding: 8px 26px;
        margin-bottom: 6%;      
        margin-top: 4%;
        cursor: pointer;
        border-radius: 20px ;
    }
     #mapWithLegende<?= $kunik?>{
        margin-top: 2%;
        position: relative;
        height: auto;
    }
    .legende-container<?= $kunik?>{
        position: absolute;
        left: 0;
        top: 100px;
        display: inline-block;
        z-index: 2;
        margin: 2%;
        border-radius: 10px;
        background: #e2e2e2;
   }
   .legende-container<?= $kunik?> .legend-body<?= $kunik?> {
        display: flex;
        text-align: left;
        padding: 10px;
    }
      
      .legende-container<?= $kunik?> .legend-body<?= $kunik?> {
        display: flex;
        text-align: left;
        padding: 10px;
    }
    .legende-container<?= $kunik?> ul {
        margin-left: -34px;
    }
    .legende-container<?= $kunik?> ul li{
        list-style: none;
        font-size: 15px;
        font-weight: bold;
    }
    .legende-container<?= $kunik?> ul  li i{
        padding: 4px;
        font-size: 20px;
    }
    .legende-container<?= $kunik?> p{
        font-size: 20px;
        font-weight: bold;
        text-align: center;
        color: #f5833c;
    }
    .legende-container<?= $kunik?> hr{
        margin-bottom: 0px;
        margin-top: 0px;
    }
    #mapCommunity<?= $kunik?> .leaflet-marker-pane img{
        width: 20px;
        height: 25px;
        transform: translate3d(423px, -290px, 0px);
        z-index: -290;
        opacity: 1;
    }
    @media (max-width: 414px) {
        #mapCommunity<?= $kunik?>{
            z-index: 1;
            height: 320px;
        }
        .legende-container<?= $kunik?> {
            position: absolute;
            z-index: 2;
            border-radius: 10px;
            background: #e2e2e2;
        }
        .legende-container<?= $kunik?> ul li {
            list-style: none;
            font-size: 11px;
            font-weight: bold;
        }
        .legende-container<?= $kunik?> ul li i {
            padding: 4px;
            font-size: 13px;
        }
        .legende-container<?= $kunik?> p {
            font-size: 18px;
            font-weight: bold;
            text-align: center;
            color: #f5833c;
            margin-bottom: 0px;
        }
        .textBack_<?= $kunik ?> h2{
            font-size: 20px;
        }
        .textBack_<?= $kunik ?> p{
            font-size: 13px;
        }
        .button_<?= $kunik ?> {
            padding: 8px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 11px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 20px;
        }
    }
    
    .textBack_<?= $kunik ?> .communaute h2 {
        margin-top: 0%;
    }
    .textBack_<?= $kunik ?> .communaute .btn-edit-delete-<?= $kunik?>{
        display: none
    }
    .communaute:hover .btn-edit-delete-<?= $kunik?>{
        display: none
    }
    .textBack_<?= $kunik ?> #mapCommunity{
        z-index: 1;
        height: 300px;
    }

    @media screen and (min-width: 1500px){
        .button_<?=$kunik?> {
            padding: 10px 20px;
            margin-bottom: 5%;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 25px;
            cursor: pointer;
            border-radius: 35px;
        }
        .textBack_<?=$kunik?> h2{
            margin: 0 0 10px 0;
            font-size: 40px;
            line-height: 35px;
            text-transform: none;
            margin-top: 5%;
        }
        .textBack_<?=$kunik?> p{
            line-height: 35px;
            font-size: 25px;
            text-transform: none;
            padding: 1% 0% 10% 0%;
            margin-bottom: -85px;
            margin-top: 0;
        }
        .legende-container<?= $kunik?> p {
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            color: #f5833c;
            margin-bottom: 0px;
        }
    }
     #mapCommunity<?= $kunik?>{
        z-index: 1;
        height: 500px;
    }
</style>
<section  class="textBack_<?= $kunik ?>">
    
    <div class="sp-cms-container text-center" >  
        <h2 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?= $paramsData["title"]?></h2>
        <p class=" sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content" ><?= $paramsData["content"]?></p>
        
         <div id="mapWithLegende<?= $kunik?>">
            <div class="legende-container<?= $kunik?>">
                <div class="legend-header<?= $kunik?> active">
                    <p>Legende <i class="fa fa-angle-down"></i></p>
                </div>
                <hr>
                <div class="legend-body<?= $kunik?>">
                    <div id="legende<?= $kunik?>" >
                        <div class="legende-content<?= $kunik?>"></div>
                    </div>
                </div>
            </div>
        <div class="col-md-12 mapBackground no-padding" id="mapCommunity<?= $kunik?>">
        </div>
    </div>
    <div>
        
        <a href="<?= $paramsData["lienButton"]?>" class="<?= $paramsData["class"]?>  button_<?= $kunik ?>" target="_blank">
            <?= $paramsData["labelbutton"]?>
        </a>
    </div>,
    
</section>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section1",
                "description" : "Personnaliser votre section1",
                "icon" : "fa-cog",
                "properties" : {
                    
                    "tags" : {
                        "label" : "tags à afficher",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.tags
                    },
                    "labelbutton" : {
                        "label" : "Label du Boutton",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.labelbutton
                    },
                    "class" : {
                        "label" : "Classe du Boutton",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.class
                    },
                    "lienButton" : {
                        "label" : "lien du boutton",
                        "inputType" :"textarea",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.lienButton
                    },
                    "colorLabelButton":{
                        label : "Couleur du label de boutton",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorLabelButton
                    },
                    "colorbutton":{
                        label : "Couleur du boutton",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
                    },
                    "colorBorderButton":{
                        label : "Couleur du bordure de boutton",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorBorderButton
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                      });
                    } );
                  }

                }
            }
        };
        mylog.log("sectiondyfff",sectionDyf);
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });
        paramsmapCommunity<?= $kunik?> = new CoMap({
            zoom : 3,
            container : "#mapCommunity<?= $kunik?>",
            activePopUp : true,
            tile : "maptiler",
            menuRight : true,
            mapOpt:{
                latLon : ["-21.115141", "55.536384"],
            },
            elts : [],
            mapCustom:{
            markers: {
                getMarker : function(data){
                    mylog.log("dataaaaamap", data);
                    var imgM = "";
                    var imgM = modules.map.assets + '/images/markers/citizen-marker-default.png';
                    if (typeof data.category != "undefined"){
                  if ( data.category =="Artistes")
                      imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_artiste.png";
                  else if ( data.category == "Agriculteurs" || data.category == "Maraîchers")
                       imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_agri.png";
                  else if ( data.category == "Pêcheurs")
                      imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_peche.png";
                  else if(data.category == "Artisans")
                      imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_artiasnt.png";
                  else if (data.category =="Bien-être")
                      imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_bienetre.png";
                  else if  (data.category =="Associations" ||data.category =="Tiers Lieux" )
                      imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_association.png";                  
              }
                    return imgM;
                }
            }
        }
});


afficheCommunity();
    });
function afficheCommunity(){
        mylog.log("----------------- Affichage community");
        var params = {
            fields : ["socialNetwork","email","geo","address"],               
            "searchType" : ["organizations"], 
            filters : {
                tags : ["<?= $paramsData["tags"]?>"]
            }        
            };

        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                mylog.log("success : elt ",data.results);

                paramsmapCommunity<?= $kunik?>.addElts(data.results);

            }
        );
    }
    var WIND_COLORS =[
    {
        name: "Agriculteurs, Maraîchers",
        bgColor:"green",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Pêcheurs",
        bgColor:"blue",
        bgColorShade:"red",
        contentColor:"light"
    },

    {
        name: "Artisans",
        bgColor:"yellow",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Artistes",
        bgColor:"orange",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Bien-être",
        bgColor:"purple",
        bgColorShade:"red",
        contentColor:"light"
    },
    {
        name: "Associations, Tiers Lieux",
        bgColor:"red",
        bgColorShade:"red",
        contentColor:"light"
    },


    ];
    function kosasaLegende(){
        var legende = $('<ul></ul>');
        WIND_COLORS.reverse().map(color => {
            legende.append(`
                <li>
                <i class="fa fa-map-marker" style="color:${color.bgColor};"></i>
                ${ color.name}
                </li>
                `)
        })
        $('#legende<?= $kunik?> .legende-content<?= $kunik?>').html(legende);
    }
    $(function(){
        kosasaLegende();
        $('.legende-container<?= $kunik?> .legend-header<?= $kunik?>').click(function(){
            $(this).toggleClass('active')
            $('.legende-container<?= $kunik?> .legend-body<?= $kunik?>').toggle('speed')
        })
    });
</script>