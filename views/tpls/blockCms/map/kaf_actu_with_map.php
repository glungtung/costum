 
 <?php 
$keyTpl = "kaf_actu_with_map";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
  "title" => "Lorem Ipsum", 
  "subtitle"=> "simply text",
  "bgCard"=> "#000000",
  "colorDecor" => "#f0ad16"

];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<?php 
  $latestLogo = [];

  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"logo"
        ),"image"
    );

    foreach ($initImage as $key => $value) {
         $latestLogo[]= $value["imagePath"];
    }

?>
 <style type="text/css">
        
      .card-actu {
        background: <?= $paramsData["bgCard"]?>;
        padding: 20px;
        line-height: 1.5em; }

      @media (max-width: 767px) {
        .card-actu {
          padding: 5px;
        }
      }

      @media screen and (min-width: 997px) {
        .wrapper {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display: flex; } }

      .details {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -webkit-flex-direction: column;
            -ms-flex-direction: column;
                flex-direction: column; }


      .card-actu .btn-in-block {
        background: <?= $paramsData["colorDecor"]?>;
        padding: 10px;
        border: none;
        font-weight: bold;
        color: #fff;
        -webkit-transition: background .3s ease;
        transition: background .3s ease; 
      }
      .card-actu .panel-body .btn-in-block {
        text-align: center;
      }
      .card-actu .btn-in-block:hover, .like:hover {
          background: #b36800;
          color: #fff; }

      .card-actu .template_faq {
          background: #edf3fe none repeat scroll 0 0;
      }

      .panel-group {
          background: #fff none repeat scroll 0 0;
          border-radius: 3px;
          box-shadow: 0 5px 30px 0 rgba(0, 0, 0, 0.04);
          margin-bottom: 0;
          padding: 30px;
      }
      .card-actu #accordion .panel {
          border: medium none;
          border-radius: 0;
          box-shadow: none;
          margin: 0 0 15px 10px;
          background-color: transparent;
      }
      .card-actu #accordion .panel-heading {
          border-radius: 30px;
          padding: 0;
      }
      .card-actu #accordion .panel-title a {
          background: <?= $paramsData["colorDecor"]?> none repeat scroll 0 0;
          border: 1px solid transparent;
          border-radius: 30px;
          color: #fff;
          display: block;
          font-size: 18px;
          font-weight: 600;
          padding: 12px 20px 12px 50px;
          position: relative;
          transition: all 0.3s ease 0s;
      }
      .card-actu #accordion .panel-title a.collapsed {
          background: #fff none repeat scroll 0 0;
          border: 1px solid #ddd;
          color: #333;
      }
      .card-actu #accordion .panel-title a::after, #accordion .panel-title a.collapsed::after {
          background: <?= $paramsData["colorDecor"]?> none repeat scroll 0 0;
          border: 1px solid transparent;
          border-radius: 50%;
          box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
          color: #fff;
          content: "";
          font-family: fontawesome;
          font-size: 25px;
          height: 55px;
          left: -20px;
          line-height: 55px;
          position: absolute;
          text-align: center;
          top: -5px;
          transition: all 0.3s ease 0s;
          width: 55px;
      }
      .card-actu #accordion .panel-title a.collapsed::after {
          background: #fff none repeat scroll 0 0;
          border: 1px solid #ddd;
          box-shadow: none;
          color: #333;
          content: "";
      }
      .card-actu #accordion .panel-body {
          background: transparent none repeat scroll 0 0;
          border-top: medium none;
          padding: 20px 25px 10px 9px;
          position: relative;
      }
      .card-actu #accordion .panel-body .contain-body {
          border-left: 1px dashed #8c8c8c;
          padding-left: 25px;
      }
      .card-actu #accordion .panel-body .contain-body h3, .card-actu #accordion .panel-body .contain-body h4, .card-actu #accordion .panel-body .contain-body h5{
          font-weight: 500;
      }
</style>

    <div class="card-actu">
      <div class="container-fliud">
        <div class="wrapper row">
          <div class="col-md-6">
              <img class="img-responsive" src="<?php echo !empty($latestLogo) ? $latestLogo[0] : ""; ?>" />
          </div>
          <div class="details col-md-6">
            <h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h3>
             <?php 
              if (isset($content)) { ?>  
                <div  id="accordion" role="tablist" aria-multiselectable="true">
              <?php 
                  foreach ($content as $key => $value) {?>  
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="heading<?= $key ?>">
                        <h4 class="panel-title title-2">
                          <a class="<?php echo $key == 1 ? "" : "collapsed" ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $key ?>" aria-expanded="true" aria-controls="collapse<?= $key ?>">
                            <?= $value["namelocality"] ?>
                          </a>
                        </h4>
                      </div>
                      <div id="collapse<?= $key ?>" class="panel-collapse collapse <?php echo $key == 1 ? "in" : "" ?> " role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <div class="contain-body">
                            <h3 class="title-3"><?= !empty($value["service"] ) ? $value["service"] : "";?></h3>
                            <?php 
                              if(!empty($value["address"]["addressLocality"])){
                                echo "<div class='about-identity title-4 text-white'>";
                                echo "<i class='fa fa-map-marker fs-18'></i> ";
                                echo !empty($value["address"]["streetAddress"]) ? $value["address"]["streetAddress"].", " : "";
                                echo !empty($value["address"]["postalCode"]) ? $value["address"]["postalCode"].", " : "";
                                echo $value["address"]["addressLocality"];
                                echo "</div>";
                              }
                            ?>
                            <h5 class="title-5"><?= !empty($value["contact"] ) ? $value["contact"] : "";?></h5>
                          </div>
                          <div class="col-xs-12 text-center">
                          <?php if(Authorisation::isInterfaceAdmin()){ ?>
                            <button class="btn-in-block btn btn-default editElement<?= $blockCms['_id'] ?> editSectionBtns"
                              data-key="<?= $key ?>" 
                              data-namelocality="<?= $value["namelocality"] ?>" 
                              data-service="<?= $value["service"] ?>" 
                              data-contact="<?= $value["contact"] ?>" 
                              data-value='<?= json_encode($value)?>'>
                              <i class="fa fa-pencil"></i>
                            </button>

                            <button class="btn-in-block btn btn-danger deletePlan<?= $blockKey ?> "
                                            data-key="<?= $key ?>" 
                                            data-id ="<?= $blockKey ?>"
                                            data-path="content.<?= $key ?>"
                                            data-collection = "cms">
                              <i class="fa fa-trash"></i>
                            </button>
                          <?php }  ?>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                    <?php }  ?>
                </div>
              <?php }  ?>
              
                <div class="action text-center">
                  <?php if(Authorisation::isInterfaceAdmin()){?>
                    <button class="btn-in-block btn btn-default  addElement<?= $blockCms['_id'] ?>" type="button">Ajouter Adresse</button>
                  <?php } ?>

                    <button class="btn-in-block btn btn-default" id="btn-map-show">Afficher map</button>
                </div>
              
          </div>
        </div>
      </div>

           <div class="section-home col-xs-12 no-padding margin-top-20" id="divMap" style="display: none;height: 450px;"></div>
    </div>
<script type="text/javascript">

  // $("#divMap").show(function () {
  //       showMap();

  //   });
    var mapAbout = {};

  function showMap(){
      
        var contextDataMap = <?php echo json_encode( $content ); ?>;
        // if(typeof contextDataMap == "undefined"){
        //   $("#divMap").addClass("hidden");
        // };

        mylog.log("contextDataMap",contextDataMap);

        var paramsMapContent = {
            container : "divMap",
            activeCluster : false,
            zoom : 9,
            activePopUp : false
        };

        
        mapAbout = mapObj.init(paramsMapContent);
        $.each(contextDataMap,function(k,v){
          mapAbout.addMarker({
            elt:{
                geo:v.geo
              }
          })
        });
        
        mapAbout.hideLoader();


};
  
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    $("#btn-map-show").click(function(){
      if ($(this).text() == "Afficher map") {
          $("#divMap").css('display', 'block');
          showMap();
          $(this).text('Masquer map');
      }else{
          $("#divMap").css('display', 'none');
          $(this).text('Afficher map');
      }
    });
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre bloc",
        "description" : "Personnaliser votre bloc",
        "icon" : "fa-cog",
        "properties" : {          
         
         
          "bgCard":{
            label : "Couleur du cadre",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.bgCard
          },
          "colorDecor":{
            label : "Couleur secondaire",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.colorDecor
          },
          "logo" : {
            "inputType" : "uploader",
            "label" : "Image",
            "showUploadBtn" : false,
            "docType" : "image",
            "itemLimit" : 1,
            "contentKey" : "slider",
            "domElement" : "logo",
            "placeholder" : "image logo",
            "afterUploadComplete" : null,
            "endPoint" : "/subKey/logo",
            "filetypes" : [
            "png","jpg","jpeg","gif"
            ],
            initList : <?php echo json_encode($initImage) ?>
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};

          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouté");
                          $("#ajax-modal").modal('hide');
                          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                          // urlCtrl.loadByHash(location.hash);
                        });
                      } );
          }
        }
      }
    };
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });

    $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
            var deleteObj ={};
            deleteObj.id = $(this).data("id");
            deleteObj.path = $(this).data("path");          
            deleteObj.collection = $(this).data("collection");
            deleteObj.value = null;
            bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",
            function(result){
              if (!result) {
                return;
              }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("Element effacé");

                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
              }
            }); 
        });

    $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
    //var contentLength = Object.keys(<?php //echo json_encode($content); ?>).length;
    var key = $(this).data("key");
    var val = $(this).data("value");
    var tplCtx = {};
    tplCtx.id = "<?= $blockCms['_id'] ?>"
    tplCtx.collection = "cms";
    tplCtx.path = "content."+(key);
    var obj = {
        //namelocality :         $(this).data("namelocality")

    };
    var activeForm = {
        "jsonSchema" : {
            "title" : "Ajouter nouveau bloc CMS",
            "type" : "object",
            onLoads : {
                onload : function(data){
                    $(".parentfinder").css("display","none");
                }
            },
            "properties" : getProperties(obj,key),
            beforeBuild : function(){
                uploadObj.set("cms","<?= $blockCms['_id'] ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( activeForm.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
              });
              if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                  tplCtx.value["coord"] = formData.geo;
                  tplCtx.value["address"] = formData.address;
                  tplCtx.value["geo"] = formData.geo;
                  tplCtx.value["geoPosition"] = formData.geoPosition;

                  if(typeof formData.addresses != "undefined")
                      tplCtx.value["addresses"] = formData.addresses;
              }

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) { 
                       dyFObj.commonAfterSave(null, function(){
                            if(dyFObj.closeForm()){
                              $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");

                              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                              // urlCtrl.loadByHash(location.hash);
                            }else{

                              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            }
                        });
                  } );
              }

            }
        }
        };          
        dyFObj.openForm( activeForm, null, val);
    });

    $(".addElement<?= $blockCms['_id'] ?>").click(function() { 
      var keys = Object.keys(<?php echo json_encode($content); ?>);
      var lastContentK = 0; 
      if (keys.length!=0) 
          lastContentK = parseInt((keys[(keys.length)-1]), 10);
      var tplCtx = {};
      tplCtx.id = "<?= $blockCms['_id'] ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "content."+(lastContentK+1);
      var obj = {
          namelocality :         $(this).data("namelocality"),
          service :         $(this).data("service"),
          contact :         $(this).data("contact")     
      };

      var activeForm = {
          "jsonSchema" : {
              "title" : "Ajouter nouveau bloc CMS",
              "type" : "object",
              onLoads : {
                  onload : function(data){
                      $(".parentfinder").css("display","none");
                  }
              },
              "properties" : getProperties(obj,lastContentK+1),
              beforeBuild : function(){
                  uploadObj.set("cms","<?= $blockCms['_id'] ?>");
              },
              save : function (data) {  
                tplCtx.value = {};
                $.each( activeForm.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                    tplCtx.value["coord"] = formData.geo;
                    tplCtx.value["address"] = formData.address;
                    tplCtx.value["geo"] = formData.geo;
                    tplCtx.value["geoPosition"] = formData.geoPosition;

                    if(typeof formData.addresses != "undefined")
                        tplCtx.value["addresses"] = formData.addresses;
                }

                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                         dyFObj.commonAfterSave(null, function(){
                              if(dyFObj.closeForm()){
                                $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                // urlCtrl.loadByHash(location.hash);

                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                              }else{
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                              // urlCtrl.loadByHash(location.hash);
                              }
                          });
                    } );
                }

              }
          }
          };          
          dyFObj.openForm( activeForm, null, sectionDyf.<?php echo $kunik ?>ParamsData);
      });

    function getProperties(obj={},subKey){
            var props = {
                namelocality : {
                    label : "Nom du localité",
                    "inputType" : "text",
                value : obj["namelocality"]
                },
                service : {
                    label : "Service",
                    "inputType" : "text",
                value : obj["service"]
                },
                contact : {
                    label : "Contact",
                    "inputType" : "text",
                value : obj["contact"]
                },
                formLocality : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality),
                 location : dyFInputs.location
                
            };
            return props;
        }

  });
  </script>