<?php 
  $keyTpl ="filterByContinent";
  $paramsData = [ 
    "title" => "Ou nous-trouver",
    "subTitle" =>"",
    "description" => ""
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
<style>
	.basicmaps-container<?= $kunik ?> #menuRightmap<?php echo $kunik ?>{
		position: absolute !important;
	}
	.basicmaps-container<?= $kunik ?> a.title-4:focus,.basicmaps-container<?= $kunik ?> a.title-4:hover{
		font-weight: bold;
		text-decoration:none !important;
    border-bottom: 3px solid <?= @$blockCms["blockCmsColorTitle4"]; ?>;
	}
  .basicmaps-container<?= $kunik ?> a.title-4{
    padding-right: 0px;
    padding-left: 0px;
    margin: 0 15px;
  }
  .basicmaps-container<?= $kunik ?> .title-1{
    margin-top:-100px;
  }
</style>

  <p class=" title<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></p>
  <p class=" subTitle<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subTitle"><?php echo $paramsData["subTitle"] ?></p>
  <div class="item<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description"><?php echo $paramsData["description"]?></div>

<nav class="navbar navbar-blank" class="title-4">
  <ul class="nav navbar-nav">
  	<li><a href="javascript:;" id="panToNomade" class="title-4">Eywa nomade</a></li>
    <li><a href="javascript:;" id="panToOceanIndien" class="title-4">Océan Indien</a></li>
    <li><a href="javascript:;" id="panToAfrique" class="title-4">Afrique</a></li>
    <li><a href="javascript:;" id="panToEurope" class="title-4">Europe</a></li>
    <li><a href="javascript:;" id="panToAsie" class="title-4">Asie</a></li>
    <li><a href="javascript:;" id="panToAmerique" class="title-4">Amérique</li>
  </ul>
</nav>

<div id="map<?= $kunik ?>" style="position: relative;height: 400px" class="basicmaps-container<?= $kunik ?>"></div>


<script>
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	$(function(){
var africaCountryCode = ["DZ","AO","BW","BI","CM","CV","CF","TD","KM","YT","CG","CD","BJ","GQ","ET","ER","DJ","GA","GM","GH","GN","CI","KE","LS","LR","LY","MG","MW","ML","MR","MU","MA","MZ","NA","NE","NG","GW","RW","SH","ST","SN","SC","SL","SO","ZA","ZW","SS","EH","SD","SZ","TG","TN","UG","EG","TZ","BF","ZM"];
var europaCountryCode = ["AL","AD","AZ","AT","AM","BE","BA","BG","BY","HR","CY","CZ","DK","EE","FO","FI","AX","FR","GE","DE","GI","GR","VA","HU","IS","IE","IT","KZ","LV","LI","LT","LU","MT","MC","MD","ME","NL","NO","PL","PT","RE","RO","RU","SM","RS","SK","SI","ES","SJ","SE","CH","TR","UA","MK","GB","GG","JE","IM"];
var asiaCountryCode = ["AF","AZ","BH","BD","AM","BT","IO","BN","MM","KH","LK","CN","TW","CX","CC","CY","GE","PS","HK","IN","ID","IR","IQ","IL","JP","KZ","JO","KP","KR","KW","KG","LA","LB","MO","MY","MV","MN","OM","NP","PK","PH","TL","QA","RU","SA","SG","VN","SY","TJ","TH","AE","TR","TM","UZ","YE","XE","XD","XS"];
var northAmericaCountryCode = ["AG","BS","BB","BM","BZ","VG","CA","KY","CR","CU","DM","DO","SV","GL","GD","GP","GT","HT","HN","JM","MQ","MX","MS","AN","CW","AW","SX","BQ","NI","UM","PA","PR","BL","KN","AI","LC","MF","PM","VC","TT","TC","US","VI"];
var southAmericaCountryCode = ["AR","BO","BR","CL","CO","EC","FK","GF","GY","PY","PE","SR","UY","VE"];
var americaCountryCode = northAmericaCountryCode.concat(southAmericaCountryCode);

		var mapCO<?= $kunik ?> = new CoMap({
	    	container : "#map<?= $kunik ?>",
	        activePopUp : true,
	        mapOpt:{
	            menuRight : true,
	            btnHide : false,
	            doubleClick : true,
	            zoom : 0,
				/*center : ["47.482649", "2.431357"]*/
	        },
	        mapCustom:{
	            tile : "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
	        },
	        elts : []
	    });

        setTimeout(function(){
        	$('#panToNomade').click(function(){
        		filterByContinent<?= $kunik ?>(); 
        	})
			$('#panToOceanIndien').click(function(){
				//mapCO<?= $kunik ?>.centerOne(-19.709874,55.834223);
				filterByContinent<?= $kunik ?>(africaCountryCode); 
			})
			$('#panToAfrique').click(function(){
				filterByContinent<?= $kunik ?>(africaCountryCode); 
				//mapCO<?= $kunik ?>.centerOne(3.003335,24.281489);
			})
			$('#panToEurope').click(function(){
				filterByContinent<?= $kunik ?>(europaCountryCode); 
				//mapCO<?= $kunik ?>.centerOne(52.385629,23.05102);
			})
			$('#panToAsie').click(function(){
				filterByContinent<?= $kunik ?>(asiaCountryCode); 
				//mapCO<?= $kunik ?>.centerOne(52.385629,23.05102);
			})
			$('#panToAmerique').click(function(){
				filterByContinent<?= $kunik ?>(americaCountryCode); 
				//mapCO<?= $kunik ?>.centerOne(17.156490,-93.75562);
			})
        },500)

        function filterByContinent<?= $kunik ?>(countryCodeArray = null){
	        params = {
		 		//"type" : "video",        
		    	searchType : ["project","organizations","events"],
		    	fields : ["urls","address","geo","geoPosition"]          
	        };
	    	  ajaxPost(
             null,
             baseUrl+"/" + moduleId + "/search/globalautocomplete",
             params,
             function(data){
	          		mylog.log("govalos",data.results);
	          		mapCO<?= $kunik ?>.clearMap();
	          		if(countryCodeArray == null)
	            		mapCO<?= $kunik ?>.addElts(data.results);
	            	else{
	            		$.each(data.results,function(k,v){
	            			if(!exists(data.results[k].address))
	            				delete data.results[k];
	            			else if(exists(data.results[k]) && exists(data.results[k].address) && exists(data.results[k].address.addressCountry)){
	            				if(countryCodeArray.indexOf(data.results[k].address.addressCountry) == -1)
	            					delete data.results[k];
	            			}
	            		});
	            		mapCO<?= $kunik ?>.addElts(data.results);
	            	}
	          	}
	    	)
        }
        filterByContinent<?= $kunik ?>(); 

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                  
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
	})
</script>	