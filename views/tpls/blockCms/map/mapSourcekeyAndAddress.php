<?php
$keyTpl     = "mapSourcekeyAndAddress";
$paramsData = [
	"title"      => " <font color=\"#005e6f\">Titre</font><div><br></div>"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if ( isset($blockCms[$e])) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
if(isset($costum["contextType"]) && isset($costum["contextId"])){
	$el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
}

?>
<style type="text/css">
	.listCostum<?= $kunik?> {
		margin-bottom: 20px;
		margin-bottom: 20px;
	}
	.listCostum<?= $kunik?> .leaflet-popup-content-wrapper {
		border: 3px solid #005e6f;
	}
	.listCostum<?= $kunik?> .popup-address{
		margin-top: 7px;
		font-size: 16px;
	}
	.listCostum<?= $kunik?> .btn-more {
		border-color: #005e6f !important;
		color: #005e6f !important;
	}
	.listCostum<?= $kunik?> .btn-more:hover {
		background-color: #005e6f !important;
		color: white !important;
	}

	.listCostum<?= $kunik?> .leaflet-container a.leaflet-popup-close-button {
		color: #005e6f !important;
	}
	.rounded<?= $kunik ?>{
        border-radius: 40px;
        text-transform: uppercase;
    }

    #thematic<?= $kunik ?>{
        background: #eee;
        padding: 0.3em;
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: white;
        font-weight: 900 !important;
    }
    #thematic<?= $kunik ?> .m-1{
        margin: 0.3em;
    }
    #thematic<?= $kunik ?> .btn-active {
    	color: white;
    	background-color: #052434;
    	border-color: #052434;
    	font-weight: 700;
    }
    #annuaireButton<?= $kunik ?> {
    	position: absolute;
    	top: 20px;
    	right: 20px;
    	padding: 8px 10px;
    	z-index: 400;
    	font-size: 20px;
    	border-radius: 10px;
    	color: white;
    	text-decoration: none;
    	background-color: #052434;
    }

</style>
<div class=" costumActif_<?= $kunik?> text-center">
	<h1  class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?= $paramsData["title"]?></h1>
	<div class="">
		<div class="row">
			<div class="main_<?= $kunik?> col-md-12 col-sm-12 col-xs-12 vertical" >
				<div class="col-xs-12 bodySearchContainer  ">
					<div id="listCostum" class="no-padding col-xs-12 listCostum<?= $kunik?>"  style="height: 500px; position: relative;" >

					</div>
					<a id="annuaireButton<?= $kunik ?>" href="#search" class="btn  lbh" data-filters="">Voir dans l'annuaire</a>
					<div id="thematic<?= $kunik ?>"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	 var contextData = <?php echo json_encode($el); ?>;
    var filteredTheme = "";
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	var map<?= $kunik ?> = new CoMap({
		container : ".listCostum<?= $kunik?>",
		activePopUp : true,
		mapOpt:{
			menuRight : false,
			btnHide : false,
			doubleClick : true,
			zoom : 2,
        	scrollWheelZoom: false
		},
		
		elts : []
	});
	var blocCostumObj<?= $kunik?> ={
		costumList:function(criteriaParams = null){
			var params = {
				searchType : ["organizations", "poi", "projects","classifieds","citoyens"],
				notSourceKey:true,
				filters : {
					$or :{
						"source.keys" : ["<?=$el["slug"]?>"]
					}
					
				}
			};
			<?php if ($el["address"]["postalCode"] == "")  {?>
				params.filters["$or"] = {
					"source.keys" : "<?=$el["slug"]?>",
					"$and":[
						{"address.addressLocality" :"<?=$el["address"]["addressLocality"]?>"},
						{"address.level1Name" :"<?=$el["address"]["level1Name"]?>"}
					],
					"links.memberOf.<?=(String)$el["_id"]?>" : {$exists:true}
				}
			<?php }else{ ?> 
				params.filters["$or"] = {
					"source.keys" : "<?=$el["slug"]?>",
					"$and":[
						{"address.postalCode":"<?=$el["address"]["postalCode"]?>"},
						{"address.level1Name" :"<?=$el["address"]["level1Name"]?>"},
						{"address.addressLocality" :"<?=$el["address"]["addressLocality"]?>"}  
					],
					"links.memberOf.<?=(String)$el["_id"]?>" : {$exists:true}
				}
			<?php } ?>
			if(criteriaParams!=null && criteriaParams!=""){
                params["searchTags"] = criteriaParams.split(",");
            }
			ajaxPost(
				null,
				baseUrl + "/" + moduleId + "/search/globalautocomplete",
				params,
				function(data){
					map<?= $kunik ?>.clearMap();
					mylog.log("dataaaa",data.results);
					var src = '';
					map<?= $kunik ?>.addElts(data.results);

				}
				);
		}
	}    
	jQuery(document).ready(function() {	
		blocCostumObj<?= $kunik?>.costumList();

		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section1",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?= $blockCms['_id'] ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Modification enregistrer");
								$("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}

				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
		/********* Filter thematics *******************/
        var thematic = [];

        if(costum && costum.lists && costum.lists.theme){
            for (const [keyT, valueT] of Object.entries(costum.lists.theme)) {
                var asTags = [];
                let labelT = "";

                if(typeof valueT == "string"){
                    asTags.push(valueT);
                    labelT = valueT;
                }else{
                    labelT = keyT;
                    for (const [keyChild, valueChild] of Object.entries(valueT)){
                        if(!thematic.includes(valueChild)){
                            thematic.push(valueChild);
                        }
                        asTags.push(valueChild);
                    }
                }
                $("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
            }   
        }else{
            $("#thematic<?= $kunik ?>").remove();
        }
        $(".thematic<?= $kunik ?>").off().on("click",function(){
            if(filteredTheme.indexOf($(this).data("filters")) !== -1){
                filteredTheme = filteredTheme.replace($(this).data("filters"), "");
                $(this).removeClass("btn-active");
                $(this).addClass("btn-white");
            }else{
                $(this).removeClass("btn-white");
                $(this).addClass("btn-active");
                if(filteredTheme==""){
                    filteredTheme = $(this).data("filters");
                }else{
                    filteredTheme+=","+$(this).data("filters");
                }
                $("#annuaireButton<?= $kunik ?>").attr("href", "#search?tags="+filteredTheme);
            }

            if(filteredTheme.charAt(0)==","){
                filteredTheme = filteredTheme.substring(1);
            }

            if(filteredTheme.length==0){
                $("#annuaireButton<?= $kunik ?>").attr("href", "#search");
            }

            blocCostumObj<?= $kunik?>.costumList(filteredTheme);
        });
	})	
</script>