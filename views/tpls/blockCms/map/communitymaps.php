<?php
$cssAnsScriptFilesModuleMap = array( 
    '/leaflet/leaflet.css',
    '/leaflet/leaflet.js',
    '/css/map.css',
    '/markercluster/MarkerCluster.css',
    '/markercluster/MarkerCluster.Default.css',
    '/markercluster/leaflet.markercluster.js',
    '/js/map.js',
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

$keyTpl = "communitymaps";

$paramsData = [ 
    "title" => "Carte de la communautée",
    "sousTitle" =>"",
    "content"=>""
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<style type="text/css">
    #menuRightmapCommunity{
        position: absolute !important;
    }
</style>

<div class="communaute ">
    <p class=" sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title" >
     
     <?= $paramsData["title"]; ?> 
</p>
 <h2 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="sousTitle">
     <?= $paramsData["sousTitle"]; ?> 
 </h2>
 <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"> 
    <?= $paramsData["content"]; ?> 
</p>
<div style="z-index: 1;height: 500px;" class="col-md-12 mapBackground no-padding" id="mapCommunity<?= $kunik?>"></div>

</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var map<?= $kunik ?> =  new CoMap({
		container : "#mapCommunity<?= $kunik?>",
		activePopUp : true,
		mapOpt:{
			menuRight : false,
			btnHide : false,
			doubleClick : true,
			zoom : 3,
        	scrollWheelZoom: false
		},
		
		elts : []
	});


    function afficheCommunity(){
        mylog.log("----------------- Affichage community");

        var params = {
            "id" : costum.contextId,
            "type" : costum.contextType
        };


        ajaxPost(
            null,
            baseUrl + "/costum/costumgenerique/getcommunity",
            params,
            function(data){
                mylog.log("success : elt ",data.elt);
                map<?= $kunik ?>.clearMap();
                map<?= $kunik ?>.addElts(data.elt);
            }
        );
    }
    jQuery(document).ready(function(){

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section de la carte de la communautée",
                "description" : "Personnaliser votre section e la carte de la communautée",
                "icon" : "fa-cog",
                "properties" : {
                    title : {
                        label : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    "sizeTitle" : {
                        "label" : "taille du titre",
                        "inputType":"number",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeTitle
                    },
                    color : {
                        label : "Couleur du titre",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                    },
                    sousTitle : {
                        label : "Sous titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.sousTitle
                    },
                    "sizeSousTitle" : {
                        "label" : "taille du sous titre",
                        "inputType":"number",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeSousTitle
                    },
                    colorSousTitle : {
                        label : "Couleur du  sous titre",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorSousTitle
                    },
                    content : {
                        label : "Contenu",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.content
                    },
                    sizeContent : {
                        "label" : "taille du contenu",
                        "inputType":"number",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.sizeContent
                    },
                    colorContent : {
                        label : "Couleur du contenu",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorContent
                    },

                    icon : { 
                        label : "Icone",
                        inputType : "select",
                        options : {
                            "fa-newspaper-o"    : "Newspapper",
                            "fa-calendar " : "Calendar",
                            "fa-lightbulb-o "  :"Lightbulb"
                        },
                        value : "text" 
                    },
            
                    background : {
                        label : "Couleur du fond du titre",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                    }
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("élement mis à jour"); 
                            $("#ajax-modal").modal('hide');

                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        } );
                    }

                }
            }   
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        afficheCommunity();
    });


</script>