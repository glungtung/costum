<?php 
    $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
    $paramsData = [ 
        "title"=>"Art",
        //"view"=>"appGenerator",
        "view"=>"main",
        "description" =>$loremIpsum,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    } 
?>

<script>
    var properties<?= $kunik ?>={
        "title" : {
            inputType : "text",
            label : "Titre",
        },
        "description" : {
            inputType : "textarea",
            label : "Description",
            markdown : true
        },
        "view" : {
            inputType : "select",
            label : "Affiche",
            options : {
                aapGenerator : "Générateur de formulaire",
                proposition : "Proposition",
            }
        }
    };
</script>

<?php
   echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.main",
        array(
            "paramsData" => $paramsData,
            "kunik" => $kunik,
            "blockCms"=>$blockCms,
            "blockKey" => $blockKey,
            "clienturi" => $clienturi,
            )
    );
?>