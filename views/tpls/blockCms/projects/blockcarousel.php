<?php 
/**
 * TPLS QUI PERMET AFFICHAGE DES PROJETS
 */
$keyTpl = "blockcarousel";

$paramsData = [
    "title"         => "Block caroussel d'éléments",
    "description"   => "",
    "elementType"   => "projects",
    "background"    => "#FFFFFF",
    "icon"          => " ",
    "color"         => "",
    "limit"         => 3,
    "iconActionColor" =>  "#ABB76B",
    "imageForm" => "circle",
    "textPreview" => "En savoir plus",
    "category"      => null
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if(isset($costum["contextType"]) && isset($costum["contextId"])){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
}

?>
<style type="text/css">

    .mx-2{
        margin-left: 1em;
        margin-right: 1em;
    }

    .truncate {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    li.indicator_<?=$kunik?>{
        background-color: #dddddd !important;
        width: 15px !important;
        height: 15px !important;
        margin-right: .3em !important;
        margin-left: .3em !important;
    }

    .projet_<?= $kunik ?> .color-h2{
        margin-top: 5%;
    }
    .projet_<?= $kunik ?> .a-caroussel{
        color: <?= $paramsData["iconActionColor"] ?>;
        text-decoration : none;
    }
    .projet_<?= $kunik ?> .modal-footer{
        border : none !important;
    }
    .projet_<?= $kunik ?> .carousel-indicators{
        bottom: 0px;
        position: relative;
    }
    .projet_<?= $kunik ?> .carousel-indicators li{
        border: 1px solid #8fcd52;
    }

    .projet_<?= $kunik ?> .arrow{
        margin-top: 2%; 
        font-size: 3rem; 
        color: #abb76b;
    }
    .projet_<?= $kunik?> .btn-edit-delete{
        display: none;
        z-index: 1000;
    }
    .projet_<?= $kunik?>:hover  .btn-edit-delete {
        display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        left: 50%;
        transform: translate(-50%,-50%);
    }
    @media (max-width:768px){
        
        .projet_<?= $kunik ?>  .color-h2{
            margin-top: 5%;
        }
        .projet_<?= $kunik ?>  .a-caroussel{
            color:<?= $paramsData["iconActionColor"] ?>;;
            text-decoration : none;
        }
        .projet_<?= $kunik ?>  .modal-footer{
            border : none !important;
        }
        .projet_<?= $kunik ?>  .carousel-indicators{
            bottom: 0px;
            position: relative;
        }
        .projet_<?= $kunik ?>  .carousel-indicators li{
            border: 1px solid #8fcd52;
        }
        .projet_<?= $kunik ?>  .carousel-indicators .active{
            background-color: #8fcd52;
        }
    }
    
</style>
<section class="projet_<?= $kunik ?> text-center">
    <h1 id="title<?= $kunik ?>" class="text-center title  sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <i class="fa <?= $paramsData["icon"] ?>"></i>
        <span><?= $paramsData["title"]; ?></span>
    </h1>
    <div class="text-center btn-edit-delete ">
        <?php if(Authorisation::isInterfaceAdmin()){ ?>
            <?php if($paramsData["elementType"]=="projects"){ ?>
                <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('project')">
                    Ajouter un élément
                </button>
            <?php } ?>

            <?php if($paramsData["elementType"]=="citoyens"){ ?>
                <a class="btn lbh btn-primary btn-xs" href="#element.invite.type.citoyens.id.<?= $_SESSION['userId'] ?>">Ajouter un élément</a>
            <?php } ?>
        <?php } ?>
    </div>
    <p class="description sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description">
        <?= $paramsData["description"]; ?>
    </p>
    <div id="elementsCarousel<?= $kunik ?>" class="carousel slide text-center" data-ride="carousel" data-type="multi">
        <div id="slider<?= $kunik ?>" class="text-center arrow hidden" style="">
            <a class="left a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="prev">  
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
            <a class="right a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="next">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
        <br>

        <div id="content-results-project<?= $kunik ?>" class="carousel-inner"></div>
       
        <div class="col-md-12" style="margin-top: 1%;">
           <ol class="carousel-indicators" id="indactors-elementsCarousel<?= $kunik ?>"></ol>
       </div>
   </div>
    
</section>

<script type="text/javascript">
    contextData = {
        id : "<?php echo $costum["contextId"] ?>",
        type : "<?php echo $costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };

    let nbItem<?= $kunik ?> = <?= $paramsData["limit"] ?>;
    let elementType<?= $kunik ?> = "<?= $paramsData["elementType"] ?>";

    tplCtx = {};
    
    sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    $(document).ready(function(){
    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section des projets sous forme carousel",
            "description" : "Personnaliser votre section sur des projets sous forme carousel",
            "icon" : "fa-cog",
            "properties" : {
                elementType : {
                    label : "Element",
                    inputType : "select",
                    options : {
                        "projects":"Liste projets de la communauté",
                        "citoyens":"Les personnes de la communauté"
                    },
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.elementType
                },
                imageForm : {
                    label : "Forme des images",
                    class : "form-control",
                    inputType : "select",
                    options : {'circle' : "Image rond", 'rounded': 'Image carré'},
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.imageForm
                },
                icon : { 
                    label : "Icone",
                    class : "form-control",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                },
                category : { 
                    label : "Catégorie des projets affichés",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.category
                },
                limit : {
                    label : "Nombre de project afficher",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.limit
                },
                textPreview : {
                    label : "Text du bouton en savoir plus",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.textPreview
                },
                iconActionColor : { 
                    label : "Couleur des boutons 'en savoir plus'",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconActionColor
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value[k] = formData.parent;
                    }
                });
                mylog.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                       dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouté");
                          $("#ajax-modal").modal('hide');
                          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                      });
                    } );
                }
            }
        }
    }

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        //alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"icon",2,6,null,null,"Paramètre d'icons","blue","");
        
    });
    // Affichage
    var params<?= $kunik ?> = {
        source : costum.contextSlug,
        element : elementType<?= $kunik ?>,
        id : contextData.id
    };

    if (notNull(sectionDyf.<?php echo $kunik ?>ParamsData.category)){
        params<?= $kunik ?>.category = sectionDyf.<?php echo $kunik ?>ParamsData.category;
    }


    ajaxPost(
        null,
        baseUrl+"/costum/filiere/get-related-elements",
        params<?= $kunik ?>,
        function(data){
            mylog.log("success", data);
            var html = "";
            var li = "";
            var y= 0;

            if(data.result == true){
                i = 0;
                url1 = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
                url2 = "<?= Yii::app()->getModule('co2')->assetsUrl; ?>/images/thumb/default_citoyens.png";
                html += "<div class='item active'>";
                li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='active indicator_<?=$kunik?>'></li>";

                if(Object.entries(data.elt).length <= nbItem<?= $kunik ?>){
                    $("#slider<?= $kunik ?>").remove();
                    $("#title<?= $kunik ?>").css("margin-bottom","1em");
                }

                for (const [k, v] of Object.entries(data.elt)) {
                    
                    let imgThumb = url1;
                    
                    if(elementType<?= $kunik ?>=="projects"){
                        imgThumb = url1;
                    }else{
                        imgThumb = url2;
                    }
                    
                    if(typeof v.profilImageUrl != "undefined" && v.profilImageUrl != null && v.profilImageUrl != "none"){
                        imgThumb = v.profilImageUrl; 
                    }

                    let descript = "";

                    if(i >= nbItem<?= $kunik ?>){
                        html += "</div>";
                        html += "<div class='item'>";
                        y++;
                        li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='indicator_<?=$kunik?>'></li>";
                        i = 0;
                        $(".arrow").removeClass("hidden");
                    }

                    i++;
                                
                    html += "<div class='card-info col-sm-12 col-md-"+(12/nbItem<?= $kunik ?>)+"'>";
  
                    html += "<center><img src='"+imgThumb+"' class='img-responsive img-rounded bg-light' style='<?=($paramsData["imageForm"]=="circle")?"border-radius : 50%; width: 150px; max-width: 150px;":"width: 200px; max-width: 200px;";?> height: 150px; max-height: 150px; object-fit: cover; background-color:#eeeeee'>";
                    
                    html += "<h2 class='color-h2 mx-2 mb-1 title-2 text-center truncate'>"+v.name+"</h2></center>";
                            
                    if(v.shortDescription){
                        descript = v.shortDescription;
                    }else if(v.email){
                        descript = v.email
                    }

                    html += "<p class='description mx-2 truncate'>"+descript.substring(0, 90)+"</p>";
                    
                    html += "<a href='#@"+v.slug+"' class='lbh-preview-element a-caroussel' data-hash='#page.type."+elementType<?= $kunik ?>+".id."+k+"'><?= $paramsData["textPreview"] ?></a>";
                    html += "</div>";
                    
                }
            }else{
                html += "Pas d'Elements à Afficher";
                li += "";
            }
            $("#content-results-project<?= $kunik ?>").html(html);
            $("#indactors-elementsCarousel<?= $kunik ?>").html(li);
        
        }
    );

    $('.carousel[data-type="multi"] .item').each(function() {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < nbItem; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
});
</script>