<?php 
$keyTpl = "project_List";
//$content = $content;
$paramsData = [
    "title" => "Nos projets"  
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if(isset($costum["contextType"]) && isset($costum["contextId"])){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
}
?>


<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>

 <style>
 	.projectSection_<?= $kunik?> {
 		padding: 20px 0 0 0;
 	}
	.plus-m {
	    width: 5vw;
	}
 </style>
<section class="projectSection_<?= $kunik?>">
	<div class="text-center">
		<h1 class="title-project title col-xs-12 col-lg-12 padding-20 text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title" > 
			<?= $paramsData["title"]?>
		</h1>
	</div>

	<div id="Projet" class="col-xs-12 col-lg-12 text-center margin-bottom-20" >
		<p class="text-center" style="min-height: 30px;">
	        <button class="btn btn-primary addProjects"> <i class="fa fa-plus-circle"></i> Ajouter un projet</button>
	    </p>

		<div class="col-xs-12 col-lg-12 text-center bodySearchContainer" id='content-results-project'>
			<!-- içi que s'affiche les projets --> 
		</div>
		
		
	</div>
</section>

<script type="text/javascript">
	contextData = {
        id : "<?php echo $costum["contextId"] ?>",
        type : "<?php echo $costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };

    
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$.each($(".markdown"), function(k,v){
			descHtml = dataHelper.markdownToHtml($(v).html()); 
			$(v).html(descHtml);
		});

		getProjects();

		$(".addProjects").click(function(){
			addListProject();
		})

		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre bloc",
				"description" : "Personnaliser votre bloc",
				"icon" : "fa-cog",
				"properties" : {	
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouté");
		                      $("#ajax-modal").modal('hide');
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});

	function addListProject(){
    	var dyfProject={};
		dyfProject.afterSave = function(data){
			dyFObj.commonAfterSave(data,function(){
				mylog.log("data", data);
				 //location.hash = "#documentation."+data.id;
				 urlCtrl.loadByHash(location.hash);

			});
		};
		dyFObj.openForm('project',null, null,null,dyfProject);
    }


	function getProjects () {
		mylog.log("getProjects >>>>>>>");

	    var str = "";

	    var params = {
	    	"searchType" : ["projects"]
	    };

	    ajaxPost(
	    	null,
	    	baseUrl+"/" + moduleId + "/search/globalautocomplete",
	    	params,
	    	function(data) {
	    		mylog.log("success projects: ",data);
	    		if(typeof data != "undefined"){
	                $.each(data,function(k,v){
	                   str += directory.showResultsDirectoryHtml(v);
	                });
	                mylog.log("str here",str);
	                $("#content-results-project").html(str);
	                $.unblockUI();
	            }
	    	},
	    	function(){
	    		mylog.log("error");
	    	}
	    );
	};

</script>