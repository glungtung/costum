<style>
    hr
    {
        border: 1px solid #858585;
    }

    .team
    {
        margin-top: 25px;
    }

    .team h1
    {
        font-weight: normal;
        font-size: 22px;
        margin: 10px 0 0 0;
    }

    .team h2
    {
        font-size: 16px !important;
        font-weight: lighter;
        margin-top: 5px;
    }

    .team .img-box
    {
        opacity: 1;
        display: block;
        position: relative;
    }

    .team .img-box:after
    {
        content: "";
        opacity: 0;
        background-color: rgba(0, 0, 0, 0.75);
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
    }

    .img-box ul
    {
        position: absolute;
        z-index: 2;
        bottom: 50px;
        text-align: center;
        width: 100%;
        padding-left: 0px;
        height: 0px;
        margin: 0px;
        opacity: 0;
    }

    .team .img-box:after, .img-box ul, .img-box ul li
    {
        -webkit-transition: all 0.5s ease-in-out 0s;
        -moz-transition: all 0.5s ease-in-out 0s;
        transition: all 0.5s ease-in-out 0s;
    }

    .img-box ul i
    {
        font-size: 20px;
        letter-spacing: 10px;
    }

    .img-box ul li
    {
        width: 30px;
        height: 30px;
        text-align: center;
        border: 1px solid #fff;
        margin: 2px;
        padding: 5px;
        display: inline-block;
    }

    .img-box a
    {
        color: #fff;
    }

    .img-box:hover:after
    {
        /* opacity: 1; */
    }

    .img-box:hover ul
    {
        /* opacity: 1; */
    }

    .img-box ul a
    {
        -webkit-transition: all 0.3s ease-in-out 0s;
        -moz-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }

    .img-box a:hover li
    {
        border-color: #FFEA05;
        color: #FFEA05;
    }

    .img-box a
    {
        color: #FFEA05;
    }

    .img-box a:hover
    {
        text-decoration: none;
        color: #519548;
    }
    .card-team{
        position: relative;
        /* height: 614px; */
        width: 100%;
        padding-top: 15px;
    }
    .team{
        box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
        flex: 0 0 calc(98%/3);
        padding: 5px 10px;
    }
    .bodySearchContainerCacs{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .divEndOfresults{
        display: none;
    }
    .team .desc{
        word-break: break-word;
        line-height:120%;
        text-align: left !important;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp:3;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .img-responsive-cacs{
        width: 100%;
        height: 253px;
        object-fit: cover;
    }
    @media only screen and (min-width : 320px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (min-width : 480px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 49%;
            padding: 5px 10px;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/2);
            padding: 5px 10px;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/3);
            padding: 5px 10px;
        }
    }
</style>
<style>
    .portfolio-modal.modal#modal-cacs .modal-content{
        background: rgb(0 0 0 / 72%);
    }
    .portfolio-modal.modal#modal-cacs .modal-content .container{
        background: #fff;
        padding-top: 10px;
        border-radius:5px;
    }
    .portfolio-modal.modal#modal-cacs .modal-content .container .row h2{
        padding-left:15px;
    }
    .portfolio-modal.modal#modal-cacs .close-modal .lr .rl,
    .portfolio-modal.modal#modal-cacs .close-modal .lr {
        background-color: #fff;
    }
</style>
<div class="portfolio-modal modal fade in" id="modal-action" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        

        <div class="row">
            <div id="modal-cacs-action" class="row">

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <h2 class="text-left">Liste des CACS <?= !empty($quartiername) ? ("du quartier <span class='text-green'>".$quartiername."</span>") : ""  ?></h2>
        <hr />
    </div>
</div>

<div class="container padding-bottom-20">
    <div class="col-md-12 no-padding">
        <div id='filterContainerCacs' class='searchObjCSS'></div>
        <div class='headerSearchIncommunityCacs no-padding col-xs-12'></div>
        <div class='bodySearchContainerCacs margin-top-30'>
            <div class='no-padding col-xs-12' id='dropdown_search'>
            </div>
        </div>
        <div class='padding-top-20 col-xs-12 text-left footerSearchContainer'></div>
    </div>
</div>

<script>
    $(function(){
        var cacsObj = {
            idquartier : <?= isset($idquartier) ? json_encode($idquartier) : "null" ?>,
            idcacs : <?= isset($idcacs) ? json_encode($idcacs) : "null" ?>,
            events : function(cacsObj){
                $('.see-action-from-cacs').off().on('click',function(){
                    /*var idquartier = $(this).data("id");
                    var quartiername = $(this).text();*/
                    var idcacs = $(this).data("cacs");
                    var cacsname = $(this).find( ".see-action-from-cacs-name" ).text();
                    ajaxPost(
                        null,
                        baseUrl + "/costum/aap/cacs/action/openactionlistbycacs",
                        {
                            /*idquartier : idquartier,
                            quartiername : quartiername,*/
                            idcacs : idcacs,
                            cacsname : cacsname
                        },
                        function(data){ 
                            //smallMenu.open(data);
                            $('#modal-action').modal('show');
                            $("#modal-cacs-action").html(data);
                        },null,null,{async:false}
                    );
                })
            }
        }
        directory.cacPanelHtml = function(params){
            mylog.log(params,"paramsnaka")
            var str = `
                <div class="team">
                    <div class="card-team">
                        <div class="img-box">
                            <img class="img-responsive-cacs" alt="Responsive Team Profiles" src="${exists(params.profilImageUrl) ? params.profilImageUrl : defaultImage}" />
                            <ul class="text-center">
                                <a href="#">
                                    <li><i class="fa fa-facebook"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-twitter"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-linkedin"></i></li>
                                </a>
                            </ul>
                        </div>
                        <h1>
                            <a href="${baseUrl}/costum/co/index/slug/coSinDni/#oceco.slug.${params.slug}.formid.${params.aapForm}.aappage.list" target="_blank">
                                ${params.name}
                            </a>
                        </h1>
                        <h2>
                            <a href="javascript:;" class="see-action-from-cacs" data-form="${params.aapForm}" data-cacs="${params._id.$id} ">
                                <span class="see-action-from-cacs-name hidden">${params.name}</span>
                                Voir les actions
                            </a>
                        </h2>
                        <hr />
                        <p class="text-left desc">${exists(params.shortDescription) ? params.shortDescription : ""}</p>
                    </div>
                </div>
            `;
            return str;
        }

        var paramsFilterCacs = {
            urlData : baseUrl + "/costum/aap/cacs/action/listcacs/slug/"+costum.contextSlug,
            container : "#filterContainerCacs",
            interface : {
                events : {
                    //page : true,
                    scroll : true,
                    //scrollOne : true
                }
                },
            header : {
                dom : ".headerSearchIncommunityCacs",
                options : {
                    left : {
                        classes : 'col-xs-8 elipsis no-padding',
                        group:{
                            count : true,
                            types : true
                        }
                    }
		        },
            },
            defaults : {
                notSourceKey : true,
                textPath : "answers.aapStep1.titre",
                types : ["organizations"],
                indexStep: 10,
                forced: {
                    filters: {
                    }
                },
                filters:{
                },
            },
            results : {
                dom:".bodySearchContainerCacs",
                smartGrid : true,
                renderView :"directory.cacPanelHtml",
                map : {
            	    active : false
                },
                events : function(fObj){
                    $(fObj.results.dom+" .processingLoader").remove();
                    cacsObj.events(cacsObj);
                    coInterface.bindLBHLinks();
                    directory.bindBtnElement();
                    coInterface.bindButtonOpenForm();
                },
            },
            filters : {
                text : true
            }
        };

        if(cacsObj.idquartier != null){
            paramsFilterCacs.defaults.filters = {
                idquartier : cacsObj.idquartier,
                idcacs : cacsObj.idcacs,
            }
        }

        var filterCacs = searchObj.init(paramsFilterCacs);
        filterCacs.search.init(filterCacs);
    })
</script>