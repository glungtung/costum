<style>
    .container-cacs-quartier hr
    {
        border: 1px solid #858585;
    }

    .container-cacs-quartier .team
    {
        margin-top: 25px;
    }

    .container-cacs-quartier .team h1
    {
        font-weight: normal;
        font-size: 22px;
        margin: 10px 0 0 0;
    }

    .container-cacs-quartier .team h2
    {
        font-size: 16px;
        font-weight: lighter;
        margin-top: 5px;
    }

    .container-cacs-quartier .team .img-box
    {
        opacity: 1;
        display: block;
        position: relative;
    }

    .container-cacs-quartier .team .img-box:after
    {
        content: "";
        opacity: 0;
        background-color: rgba(0, 0, 0, 0.75);
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
    }

    .container-cacs-quartier .img-box ul
    {
        position: absolute;
        z-index: 2;
        bottom: 50px;
        text-align: center;
        width: 100%;
        padding-left: 0px;
        height: 0px;
        margin: 0px;
        opacity: 0;
    }

    .container-cacs-quartier .team .img-box:after,.container-cacs-quartier .img-box ul, .img-box ul li
    {
        -webkit-transition: all 0.5s ease-in-out 0s;
        -moz-transition: all 0.5s ease-in-out 0s;
        transition: all 0.5s ease-in-out 0s;
    }

    .container-cacs-quartier .img-box ul i
    {
        font-size: 20px;
        letter-spacing: 10px;
    }

    .container-cacs-quartier .img-box ul li
    {
        width: 30px;
        height: 30px;
        text-align: center;
        border: 1px solid #fff;
        margin: 2px;
        padding: 5px;
        display: inline-block;
    }

    .container-cacs-quartier .img-box a
    {
        color: #fff;
    }

    .container-cacs-quartier .img-box:hover:after
    {
        /* opacity: 1; */
    }

    .container-cacs-quartier .img-box:hover ul
    {
        /* opacity: 1; */
    }

    .container-cacs-quartier .img-box ul a
    {
        -webkit-transition: all 0.3s ease-in-out 0s;
        -moz-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }

    .container-cacs-quartier .img-box a:hover li
    {
        border-color: #FFEA05;
        color: #FFEA05;
    }

    .container-cacs-quartier .img-box a
    {
        color: #FFEA05;
    }

    .container-cacs-quartier .img-box a:hover
    {
        text-decoration: none;
        color: #519548;
    }
    .container-cacs-quartier .card-team{
        position: relative;
        /* height: 614px; */
        width: 100%;
        padding-top: 15px;
    }
    .container-cacs-quartier .team{
        box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
        flex: 0 0 calc(98%/3);
        padding: 5px 10px;
    }
    .container-cacs-quartier .bodySearchContainerQuartier{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .container-cacs-quartier .divEndOfresults{
        display: none;
    }
    .container-cacs-quartier .team .desc{
        word-break: break-word;
        line-height:120%;
        text-align: left !important;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp:3;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .container-cacs-quartier .img-responsive-cacs{
        width: 100%;
        height: 253px;
        object-fit: cover;
    }
    @media only screen and (min-width : 320px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (min-width : 480px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 49%;
            padding: 5px 10px;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/2);
            padding: 5px 10px;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/3);
            padding: 5px 10px;
        }
    }
</style>
<div class="portfolio-modal modal fade in" id="modal-cacs" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        

        <div class="row">
            <div id="modal-cacs-content" style="height: 100%; width: 100%">

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <h2>Liste des quartiers</h2>
        <hr />
    </div>
</div>

<div class="container padding-bottom-20 container-cacs-quartier">
    <div class="col-md-12 no-padding">
        <div id='filterContainerQuartier' class='searchObjCSS'></div>
        <div class='headerSearchQuartier no-padding col-xs-12'></div>
        <div class='bodySearchContainerQuartier margin-top-30'>
            <div class='no-padding col-xs-12' id='dropdown_search'>
            </div>
        </div>
        <div class='padding-top-20 col-xs-12 text-left footerSearchContainer'></div>
    </div>
</div>

<script>
    $(function(){
        let cacQuartierObj = {
            data : {
                cacs : [],
                quarties : [],
            },
            events : function(cobj){
                $(".see-cacs-quartier").on('click',function(){
                    var idquartier = $(this).data("id");
                    var quartiername = $(this).text();
                    var idcacs = $(this).data("cacs");
                    ajaxPost(
                        null,
                        baseUrl + "/costum/aap/cacs/action/opencacslistbyquartier",
                        {
                            idquartier : idquartier,
                            quartiername : quartiername,
                            idcacs : idcacs
                        },
                        function(data){ 
                            //smallMenu.open(data);
                            $('#modal-cacs').modal('show');
                            $("#modal-cacs-content").html(data);
                        },null,null,{async:false}
                    );
                })
            }
        }
        directory.quartierPanelHtml = function(params){
            var str = `
                <div class="team">
                    <div class="card-team">
                        <div class="img-box hidden">
                            <img class="img-responsive-cacs" alt="Responsive Team Profiles" src="${exists(params.profilImageUrl) ? params.profilImageUrl : defaultImage}" />
                            <ul class="text-center">
                                <a href="#">
                                    <li><i class="fa fa-facebook"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-twitter"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-linkedin"></i></li>
                                </a>
                            </ul>
                        </div>
                        <h1>
                            <a href="javascript:;" data-id="${params._id.$id}" data-cacs="${exists(params?.cacs) ? params.cacs.join(",") : "" }" class="tooltips see-cacs-quartier" data-toggle="tooltip" data-placement="bottom" data-original-title="Voir les cacs">
                                ${params.name}
                            </a>
                        </h1>
                        <h2>${Object.keys(params.actions).length} Actions, ${Object.keys(params.cacs).length} Cacs</h2>
                        <hr />
                        <p class="text-left desc">${exists(params.shortDescription) ? params.shortDescription : ""}</p>
                    </div>
                </div>
            `;
            return str;
        }
        var paramsFilterQuartier= {
            urlData : baseUrl + "/costum/aap/cacs/action/globalautocompletequartier/slug/"+costum.contextSlug,
            container : "#filterContainerQuartier",
            //footerDom : ".footerSearchContainerQuartier",
            interface : {
                events : {
                    //page : true,
                    scroll : true,
                    //scrollOne : true
                }
                },
            header : {
                dom : ".headerSearchQuartier",
                options : {
                    left : {
                        classes : 'col-xs-8 elipsis no-padding',
                        group:{
                            count : true,
                            types : true
                        }
                    }
		        },
            },
            defaults : {
                notSourceKey : true,
                types : ["answers"],
                indexStep: 10,
            },
            results : {
                dom:".bodySearchContainerQuartier",
                smartGrid : true,
                renderView :"directory.quartierPanelHtml",
                map : {
            	    active : false
                },
                events : function(fObj){
                    $(fObj.results.dom+" .processingLoader").remove();
                    cacQuartierObj.events(cacQuartierObj);
                    coInterface.bindLBHLinks();
                    directory.bindBtnElement();
                    coInterface.bindButtonOpenForm();
                },
            },
            filters : {
                text : true
            }
        };
        var filterQuartier = searchObj.init(paramsFilterQuartier);
        filterQuartier.search.init(filterQuartier);
    })
</script>