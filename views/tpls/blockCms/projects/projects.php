<?php 
$keyTpl = "projects";

$paramsData = [
    "title"         => "Projets",
    "background"    => "#e84d4c",
    "icon"          => "",
    "color"         => "#FFFFFF",
    "limit"         => 3,
    "cardColor"     => "#FFFFFF",
    "textCardColor" =>  "#000000"
];

if (isset($blockCms)) {
    $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
    foreach ($paramsData as $e => $v) {
        if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
            $paramsData[$e] = $tplsCms[$e];
        }
    }
}
?>
<style>
    .card-info{
        background: <?= $paramsData["cardColor"] ?>;
        padding : 3%;
        margin-left: 1%;
        width: 32%;
        overflow-y: auto;
        height: 350px;
    }
    .color-h2{
        color: <?= $paramsData["textCardColor"]; ?>;
        margin-top: 5%;
    }

    .carousel-control { 
      width:  4%; 
      position: absolute !important;
  }
  .carousel-control.right { 
      right: -40px;
  }
  .carousel-control.left,.carousel-control.right {
      margin-left:-40px;
      background-image:none;
  }
  @media (max-width: 767px) {
      .carousel-inner .active.left { left: -100%; }
      .carousel-inner .next        { left:  100%; }
      .carousel-inner .prev    { left: -100%; }
      .active > div { display:none; }
      .active > div:first-child { display:block; }

  }
  @media (min-width: 767px) and (max-width: 992px ) {
      .carousel-inner .active.left { left: -50%; }
      .carousel-inner .next        { left:  50%; }
      .carousel-inner .prev    { left: -50%; }
      .active > div { display:none; }
      .active > div:first-child { display:block; }
      .active > div:first-child + div { display:block; }
  }
  @media (min-width: 992px ) {
      .carousel-inner .active.left { left: -16.7%; }
      .carousel-inner .next        { left:  16.7%; }
      .carousel-inner .prev    { left: -16.7%; }
      .items {width: 20%;}  
  }
  @media (max-width:768px){
    .card-info{
        background: <?= $paramsData["background"] ?>;
        padding : 3%;
        margin-left: 1%;
        width: 32%;
        overflow-y: auto;
        height: 350px;
    }
    .color-h2{
        color: <?= $paramsData["color"]; ?>;
        margin-top: 5%;
    }

    @media (max-width:768px){
        .card-info{
            background: white;
            overflow-y: auto;
            width: 100%;
            margin-left: 0%;
        }
    }


/*
    #owl-project .owl-wrapper-outer .owl-wrapper .owl-item {
        width: 100% !important;
        background-color: red;
    }*/
</style>

<style>
.project_<?= $kunik?> a {
  color: currentColor;
  text-decoration: none;
}

.project_<?= $kunik?> a:hover .card-outmore_<?= $kunik?> {
  background: #2C3E50;
  color: #fff;
}

.project_<?= $kunik?> a:hover .thecard_<?= $kunik?> {
  box-shadow: 0 10px 50px rgba(0,0,0,.6);
}
.thecard_<?= $kunik?> {
  /*width: 300px;*/
  margin: 5% auto;
  box-shadow: 0 1px 3px 0px rgba(0,0,0,.4);
  display: block;
  background-color: #fff;
  border-radius: 4px;
  transition: 400ms ease;
}
.card-img_<?= $kunik?> {
  height: 225px;
}
.card-img_<?= $kunik?> img {
  width:100%;
  height: 240px;
  border-radius: 4px 4px 0px 0px;
}
.card-caption_<?= $kunik?> {
  position: relative;
  background: #ffffff;
  padding: 15px 25px 5px 25px;
  border-radius: 0px 0px 4px 4px;
}
.card-outmore_<?= $kunik?> {
  padding: 10px 25px 10px 25px;
  border-radius: 0px 0px 4px 4px;
  border-top: 1px solid #e0e0e0;
  background: #efefef;
  color: #222;
  display: inline-table;
  width: 100%;
  box-sizing: border-box;
  transition: 400ms ease;
}
.card-outmore_<?= $kunik?> h5 {
  float: left;
}
.card-outmore_<?= $kunik?> i {
  float: right;
}


    #owl-project .item{
      display: block;
      padding: 30px 0px;
      margin: 5px;
      /*color: #FFF;*/
      -webkit-border-radius: 3px;
      -moz-border-radius: 3px;
      border-radius: 3px;
      text-align: center;
  }
  .owl-theme .owl-controls .owl-buttons div {
      padding: 5px 9px;
  }

  .owl-theme .owl-buttons i{
      margin-top: 2px;
  }

  /*To move navigation buttons outside use these settings:*/

  .owl-theme .owl-controls .owl-buttons div {
      position: absolute;
  }

  .owl-theme .owl-controls .owl-buttons .owl-prev{
      left: -45px;
      top: 55px; 
  }

  .owl-theme .owl-controls .owl-buttons .owl-next{
      right: -45px;
      top: 55px;
  }    
  #project-date .dateUpdated{
    position: inherit;
    right: 15px;
    top: 50px;
    background: #ffffff;
    padding: 2px 8px;
    border-radius: 0px 0px 0px 7px;
    font-size: 12px;
    color: #a2a2a2;
    font-weight: 100;
}    
    #outmore-icon {
      border:1px solid ;
      padding: 1px 6px;
      border-radius: 50em;
  }
</style>
<h1 class="padding-5" style="color:<?= $paramsData["color"] ?>;background-color: <?= $paramsData["background"] ?>;text-transform: initial;">
    <i class="fa <?= @$paramsData["icon"] ?>"></i>
    <?= $paramsData["title"]; ?> 
</h1>
<div class="col-md-10">
    <div class="project_<?= $kunik?>">
        <div class="row">
            <div class="span12">
                <div id="owl-project" class="owl-carousel">
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
tplCtx = {};
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
page = <?= json_encode(@$page); ?>;
type = 'tpls.blockCms.projects.<?= $keyTpl ?>';
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
$(document).ready(function(){

//     sectionDyf.<?php echo $keyTpl ?>Params = {
//         "jsonSchema" : {    
//             "title" : "Configurer la section des projets sous forme carousel",
//             "description" : "Personnaliser votre section sur des projets sous forme carousel",
//             "icon" : "fa-cog",
//             onLoads : {
//                 onload : function(){
//                     $(".parentfinder").css("display","none");
//                 }
//             },
//             "properties" : {
//                 title : {
//                     label : "Titre",
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
//                 },
//                 icon : { 
//                     label : "Icone",
//                     inputType : "select",
//                     options : <?= json_encode(Cms::$icones); ?>,
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.icon
//                 },
//                 color : {
//                     label : "Couleur du titre",
//                     inputType : "colorpicker",
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
//                 },
//                 background : {
//                     label : "Couleur de fond du titre",
//                     inputType : "colorpicker",
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
//                 },
//                 cardColor : {
//                     label : "Couleur de fond de la carte",
//                     inputType : "colorpicker",
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.cardColor
//                 },
//                 textCardColor : {
//                     label : "Couleur du texte dans la carte",
//                     inputType : "colorpicker",
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.textCardColor
//                 },
//                 limit : {
//                     label : "Nombre de project afficher",
//                     values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.limit
//                 },
//                 parent : {
//                     inputType : "finder",
//                     label : tradDynForm.whoiscarrytheproject,
//                     multiple : true,
//                     rules : { required : true, lengthMin:[1, "parent"]}, 
//                     initType: ["organizations"],
//                     openSearch :true
//                 }
//             },
//             save : function () {  
//                 tplCtx.value = {};

//                 if (typeof idToPath != "undefined") {
//                     tplCtx.value["id"] = idToPath;
//                 }

//                 $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
//                     tplCtx.value[k] = $("#"+k).val();
//                     if (k == "parent") {
//                         tplCtx.value["parent"] = formData.parent ;
//                     }
//                 });

//                 tplCtx.value["page"] = '<?= $page ?>';
//                 tplCtx.value["type"] = 'tpls.blockCms.projects.<?= $keyTpl ?>';

//                 mylog.log("save tplCtx",tplCtx);
                
//                 if(typeof tplCtx.value == "undefined")
//                     toastr.error('value cannot be empty!');
//                 else {
//                     dataHelper.path2Value( tplCtx, function(params) { 
//                         toastr.success("élément mis à jour");
//                         $("#ajax-modal").modal('hide');
//                         urlCtrl.loadByHash(location.hash);
//                     } );
//                 }

//             }
//         }

//     $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
//         tplCtx.id = $(this).data("id");
//         tplCtx.collection = $(this).data("collection");
//         tplCtx.path = (typeof $(this).data("path") != "undefined" && $(this).data("path") != "") ? $(this).data("path") : "allToRoot";
//         idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

//         dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
//     });
// }

    // Affichage
    var params = {
        source : contextSlug,
        limit : <?= $paramsData["limit"] ?>
    };

    ajaxPost(
        null,
        url : baseUrl+"/costum/costumgenerique/getProjects",
        params,
         function(data){
            var html = " ";
            var li = "";
            var y= 0;
            if(data.result == true){
              var i = 0;
              var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
              $(data.elt).each(function(key,value){
                  var img = (typeof value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;
                  var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "Aucune description";
                  var address = typeof (value.address) != "undefined" && value.address.addressLocality != null ? value.address.addressLocality : "";
                  mylog.log("Pro", value);
                  html +=
                  `<div class="item" >
                    <a href="#page.type.poi.id.`+value.id+`" class="lbh-preview-element">
                      <div class="thecard_<?= $kunik?> text-left">
                        <div class="card-img_<?= $kunik?>">
                          <img src="`+img+`">
                        </div>
                        <div class="card-caption_<?= $kunik?>">
                          <h4 style="text-transform: capitalize">`+value.name+`</h4>
                          <span class="date">`+address+`</span>
                          <p>`+description+`</p>
                        </div>
                    <div id="project-date" class="text-right">`+directory.showDatetimePost(value.collection, value.id, value.updated)+`</div>
                        <div class="card-outmore_<?= $kunik?>">
                          <h5>Voir plus</h5>
                          <i id="outmore-icon" class="fa fa-angle-right"></i>
                        </div>
                      </div>
                    </a>
                  </div>
                  `;
          });
          }

          else{
            html += "Pas de projet en cours";
            li += "";
        }
        $("#owl-project").html(html);
    }
);

     function random(owlSelector){
        owlSelector.children().sort(function(){
            return Math.round(Math.random()) - 0.5;
        }).each(function(){
          $(this).appendTo(owlSelector);
        });
      }

      $("#owl-project").owlCarousel({
        navigation: true,
        navigationText: [
        "<i class='icon-chevron-left icon-white'><</i>",
        "<i class='icon-chevron-right icon-white'>></i>"
        ],
        //Call beforeInit callback, elem parameter point to $("#owl-project")
        beforeInit : function(elem){
          random(elem);
        }

      });

});
</script>