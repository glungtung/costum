<?php
$keyTpl = "formsList";
$paramsData=[
	"title" => "Liste des formulaires",
  "titleColor" => "#8fcf1b",
  "colorTheme"  => "#8fcf1b",
	"btnEditText" => "Modifier",
	"btnNewFormText" => "Nouvelle formaulaire",
	"btnTemplateText" => "Choisir un template",
	"btnAnswerText" => "Répondre",
	"btnListAnswersText" => "Réponses",
	"btnConfigText" => "Configurer",
	"coformSlug" => $costum["contextSlug"],
	"coformDisplay" => [],
	"activeOnly" => false,
  "sourceKey" => "",
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>

<style>
.card<?= $kunik ?>{
  margin-top: 1.5em;
	margin-bottom: 1.5em;
  padding:.6em 1.2em;
  border-radius: 5px;
  background-color: #eee;
  font-size: 12pt !important;
}

.card<?= $kunik ?> p{
  font-size: 15pt !important;
  font-weight: bold;
  color: #777;
  margin-left: 1.3em;
}

.title<?= $kunik ?>{
  color: <?= $paramsData["colorTheme"] ?>;
	text-transform: uppercase;
}

.calendar-icon<?= $kunik ?>{
  border-radius: 50%;
  padding: 0.5em 0.6em;
  border-left: 4px solid <?= $paramsData["colorTheme"] ?>;
  border-bottom: 4px solid <?= $paramsData["colorTheme"] ?>;
  border-top: 4px solid <?= $paramsData["colorTheme"] ?>;
}

.btn-answer<?= $kunik ?>{
  background-color: <?= $paramsData["colorTheme"] ?>;
  border-radius: 40px;
  padding: .7em 1.5em;
  color:white;
}

.btn-answers<?= $kunik ?>{
  background-color: white;
  border-radius: 40px;
  padding: .7em 1.5em;
  color: <?= $paramsData["colorTheme"] ?>;
  margin-left: 1em;
}

.btn-config<?= $kunik ?>{
  background-color: white;
  border-radius: 40px;
  padding: .7em 1.5em;
  margin-left: 1em
}

.btn-<?= $kunik ?>{
  font-weight: bold;
	text-transform: uppercase;
  transition-property: all;
  transition-duration: .5s;
}

.btn-createForm<?= $kunik ?>{
  background-color: #eee;
  border-radius: 0px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  padding: .6em 1.4em;
  text-transform: uppercase;
}

.btn-templateList<?= $kunik ?>{
  background-color: <?= $paramsData["colorTheme"] ?>;
  border-radius: 0px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  padding: .6em 1.4em;
  text-transform: uppercase;
  color: white;
}

.btn-container<?= $kunik ?>{
  margin: 1.5em 0em .5em 0em;
  justify-content: space-between !important;
}

.context-icon<?= $kunik ?>{
  font-weight: bolder;
}

#central-container{
  background-color:white;
  min-height: auto !important;
}

.calendar<?= $kunik ?>{
  margin-right: 1em;
}

a .fa{
  margin-right: .6em;
}

h1, .h1, h2, .h2, h3, .h3 {
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}
</style>

<div class="container margin-top-10 col-xs-12">
  <a id="backtoinitial" class="btn btn-default btn-<?= $kunik ?>" style="border-radius: 40px" href="javascrit:;"><i class="fa fa-arrow-left"></i>Retour</a>
</div>
<div id="central-container"></div>
<?php
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
  $formAssets = [
    '/js/form.js', '/css/form.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $formAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("survey")->getAssetsUrl()
  );
}

?>
<?php 
$adminForm = [];
if(!empty($paramsData["coformDisplay"])){
  foreach($paramsData["coformDisplay"] as $kf => $vf){
    if(Form::isFormAdmin($vf)){
      $adminForm[$vf] = true;
    }else{
      $adminForm[$vf] = false;
    }
  }
} 
?> 
<script type="text/javascript">
  var allCoform = {}
  var slugParent = {}
  var strOptions = "";
  function getAllCoform(slug,html=false){
    strOptions = "";
    ajaxPost(
      null, 
      baseUrl+'/survey/form/get/slug/'+slug+'/tpl/json', 
      null,
      function(data){
        var str = "";
        mylog.log("pageProfil.views.forms ajaxPost data", data);
        if(typeof data != "undefined" && data != null &&
          data.forms != "undefined" && data.forms != null){
          $.each( data.forms , function(kf,valf) {
            if(html){
              strOptions += `<option value="${kf}" data-value="">${valf.name}</option>`
            }else{
              allCoform[kf] = valf.name;
            }
            
          }); 
        }        
      },null,null,{async:false}
    );
    if(!html)
      return allCoform;
    else 
      return strOptions
      
  }
  function getAllParent(){
    slugParent = {}
    ajaxPost(
      null, 
      baseUrl+'/survey/form/getallparent/userId/'+userId, 
      null,
      function(data){
        slugParent = data;
      },null,null,{async:false}
    );
    return slugParent;
      
  }
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  adminForm = <?php echo json_encode($adminForm); ?>;
  jQuery(document).ready(function() {
    slugParent = getAllParent() ;
    allCoform = getAllCoform(sectionDyf.<?php echo $kunik ?>ParamsData.coformSlug);
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "title": {
              "label" : "Titre :",
              "inputType" : "text",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.title
          },
          "colorTheme": {
              "label" : "Choisir une themes couleur :",
              "inputType" : "colorpicker",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.colorTheme
          },
          "btnNewFormText": {
              "label" : "Texte pour bouton d'ajout de nouvelle formaulaire:",
              "inputType" : "text",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.btnNewFormText
          },
          "btnTemplateText": {
              "label" : "Texte pour le bouton pour choisir une modèle :",
              "inputType" : "text",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.btnTemplateText
          },
          "btnAnswerText": {
              "label" : "Texte pour le bouton pour répondre au formaulaire :",
              "inputType" : "text",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.btnAnswerText
          },
          "btnListAnswersText": {
              "label" : "Texte pour le bouton pour afficher la liste des réponses :",
              "inputType" : "text",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.btnListAnswersText
          },
          "btnConfigText": {
              "label" : "Texte pour le bouton pour paramétrer le formulaire :",
              "inputType" : "text",
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.btnConfigText
          },
          "coformSlug": {
              "label" : "Les sources à afficher",
              "inputType" : "select",
              "options":slugParent,
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.coformSlug
          },
          "coformDisplay": {
              "label" : "Les coform à afficher", 
              "placeholder" : "Les coform à afficher", 
              "class": "multi-select", 
              "isSelect2": true, 
              "inputType" : "selectMultiple",
              "options":allCoform,
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.coformDisplay
          },
          "activeOnly": {
            "label" : "Afficher que les formulaires actives",
            "inputType" : "checkboxSimple",
            "params" : {
              "onText" : trad.yes,
              "offText" : trad.no,
              "onLabel" : trad.yes+"",
              "offLabel" : trad.no+""
            },
            "checked" : false
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              });
          }
        }
      }
    }

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      $('#coformSlug').change(function(){ 
          allCoform = getAllCoform($('#coformSlug').val(),true)
          $("#coformDisplay").val("") 
          $(".select2-search-choice").hide()
          $("#coformDisplay").html(allCoform) 
        });
      setTimeout(() => {
        onload();
        $("select[name='graphType']").change(onload);
      },1200)
    })
  });

  contextData = costum;
  if(window.location.hash==""){
    window.location.hash="#welcome";
  }else{
    window.location.hash=window.location.hash.split(".")[0];
  }

  var coFormObj = formObj;

  coFormObj.views.forms = function(fObj, forms){
    var str = '';
    
    str += '<div id="formList">'+
        '<div class="col-xs-12 col-lg-10 col-lg-offset-1">'+
          '<div class="page__title">'+
            '<h2 class="title<?=$kunik?> sp-text pull-left"  id="sp-<?= $blockKey ?>"  data-id="<?= $blockKey ?>" data-field="title"> <?= $paramsData["title"]; ?> </h2>';
    <?php if (Authorisation::isInterfaceAdmin()) {?>
      /*str +=  `<div class="btn-group pull-right margin-top-15">
                <button type="button" class="btn btn-templateList<?= $kunik ?> btn-<?= $kunik ?> chooseAapForm"><?= $paramsData["btnTemplateText"] ?></button>
              </div>`;
      str +=  `<div class="btn-group pull-right margin-top-15">
                <button type="button" class="btn btn-createForm<?= $kunik ?> btn-<?= $kunik ?> createForm"><?= $paramsData["btnNewFormText"] ?></button>
              </div>`;*/
    <?php } ?>
    str +=		' </div>';
    str += 		'<div class="col-xs-12 no-padding" role="row">';
    if(notEmpty(sectionDyf.<?php echo $kunik ?>ParamsData.coformDisplay)){
      $.each(sectionDyf.<?php echo $kunik ?>ParamsData.coformDisplay,function(kcf,vcf){
        if(forms[vcf]){
          str += fObj.views.newform(vcf, forms[vcf], fObj);
        }
      })

    }else{
      $.each(forms, function(idF, valForm){
            str += fObj.views.newform(idF, valForm, fObj);
      });
    }
    
    str += ' 	</div>'
        '</div>'+
    '</div>';

    $(fObj.container).html(str);
    fObj.events.form(fObj);
    fObj.events.add(fObj);
    fObj.events.share(fObj);
    fObj.events.answers(fObj);
  }

  coFormObj.urls.forms = function(fObj){
    ajaxPost('#central-container', baseUrl+'/survey/form/get/slug/'+sectionDyf.<?php echo $kunik ?>ParamsData.coformSlug+'/tpl/json', 
      null,
      function(data){
        var str = "";
        mylog.log("pageProfil.views.forms ajaxPost data", data);
        if(typeof data != "undefined" && data != null &&
          data.forms != "undefined" && data.forms != null){
          fObj.el = data.el;
          fObj.openFormList = fObj.openForm.list();
          fObj.views.forms(fObj, data.forms);
        }
        //history.replaceState(location.hash, "", hashUrlPage+".view.forms");
        
      },"html");
  }

  coFormObj.views.newform = function(id, form, fObj){
    mylog.log("formProfil.views.form", id, form);
    var panelColor = "panel-primary";
    var formCostum = false;
    var urlCostum = "";
    var datatypeform = "";
    var elconfigslug;

    if (typeof form.type != "undefined" && (form.type === "aap" || form.type === "templatechild") && typeof form.config != "undefined"){
      ajaxPost(
        null,
        baseUrl+"/survey/form/getaapconfig/formid/"+form.config,
        null,
        function(data){
          elconfigslug = data;
        },
        null,
        "",
        {async: false}
        );
    }

    if( typeof form.source != "undefined" &&
      typeof form.source.insertOrign != "undefined" &&
      form.source.insertOrign == "costum"){
      panelColor = "panel-success";
      formCostum = true;
      urlCostum = baseUrl+'/costum/co/index/id/'+form.source.key;
    }
    form.active = (form.active === true || form.active === "true");
    form.hasObservatory = true;
    if( typeof form.openForm != "undefined" && form.openForm == true){
      datatypeform = 'data-type="openform"';
    }

    if( typeof form.description == "undefined" || form.description == null ){
      form.description = "<b> ("+trad.nodescription+") </b>";
    }

    if( typeof form.startDate == "undefined" || form.startDate == null ){
      form.startDate = " ("+trad.notSpecified+") ";
    }

    if( typeof form.endDate == "undefined" || form.endDate == null ){
      form.endDate = " ("+trad.notSpecified+") ";
    }

    var str = "";
    if(typeof form.type != "undefined" && (form.type == "aapConfig"))
    {
      //Do not show aapConfig
    }
    else
    {
      var sbsl = [];
      if(typeof form != "undefined" && typeof form.subForms != "undefined") {
        $.each(form.subForms, function (ind, val) {
          if (typeof form[val] != "undefined" && form[val] != null && typeof form[val].name != "undefined") {
            sbsl.push(form[val].name);
          }
        });
      }
      <?php if($paramsData["activeOnly"]=="true" ){  echo "if(form.active){";  } ?>
        str += `
          <div class="card card<?= $kunik ?>">
            <h3><i class="fa fa-wpforms context-icon<?= $kunik ?>"></i> ${form.name}</h3>
            <p>${form.description}</p>
            <span class="calendar<?= $kunik ?>">
              <i class="fa fa-calendar calendar-icon<?= $kunik ?>"></i> <b>${trad.start}</b> : ${form.startDate}
            </span>
            <span class="calendar<?= $kunik ?>">
              <i class="fa fa-calendar calendar-icon<?= $kunik ?>"></i> <b>${trad.end}</b> : ${form.endDate}
            </span>
            <div class="btn-container<?= $kunik ?>">`;
            
              if(form.type != "aap" || typeof form.type == "undefined"){
                /*
                    str +=isUserConnected == "logged" ?	' <a href="'+urlCostum+'#form.edit.id.'+id+'" target="_blanc" class="btneditform btn btn-config<?= $kunik ?> btn-<?= $kunik ?> no-margin" data-toggle="tooltip" data-placement="bottom" data-original-title="Modifier les champs et les étapes du formulaire"><i class="fa fa-pencil"></i> <?= $paramsData["btnEditText"] ?></a>':'';
                    */
                    str += (form.active && isUserConnected == "logged") ?	'<a href="javascript:;" data-id="'+id+'" data-parentformid="'+form._id.$id+'" '+datatypeform+' class="addAnswer btn btn-answer<?= $kunik ?> btn-<?= $kunik ?> margin-left-10" data-toggle="tooltip" data-placement="bottom" data-original-title="Répondre au formulaire"><i class="fa fa-plus"></i> <?= $paramsData["btnAnswerText"] ?></a>':'';
                    /*
                    <?php if (Authorisation::isInterfaceAdmin()) {?>
                      str +=	'<a href="javascript:;" data-id="'+id+'" '+datatypeform+' class="observatory btn btn-answers<?= $kunik ?> btn-<?= $kunik ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Liste des réponses" ><i class="fa fa-envelope"></i> <?= $paramsData["btnListAnswersText"] ?></a>';
                    <?php } ?>
                    */
              }
              if(adminForm[id]) {  
                str += `<a href="javascript:;" data-id="${id}" data-form="${escapeHtml(JSON.stringify(form))}" data-subselect="${escapeHtml(JSON.stringify(sbsl))}" class="btnConfigForm btn btn-config<?= $kunik ?> btn-<?= $kunik ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Modifier les details et la description du formulaire"><i class="fa fa-cogs"></i> <?= $paramsData["btnConfigText"] ?></a>`;
              }  
        str += `</div> </div>`;
      <?php if($paramsData["activeOnly"]=="true"){ echo "}"; } ?>
    }
    return str;
  }

  function init(){ 
    if(typeof hashUrlPage == "undefined"){
      hashUrlPage = "#@"+costum.slug;
    }
    //coFormObj.urls.forms(coFormObj);
    $("#central-container").empty();
    $("#central-container").append(formObj.urls.forms(formObj));
    
    $("#backtoinitial").hide();
  }

  init();

  $("#backtoinitial").on("click", function(){
    init();
  })

  $(document).on("click",".btn-answer<?= $kunik ?>", function(e){
    $("#backtoinitial").show();
  })

      

</script>
