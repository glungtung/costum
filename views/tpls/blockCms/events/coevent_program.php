<?php
    $costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
    $paramsData = [
        'primaryColor' => '#EE302C'
    ];
    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e])) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
    $connected_user = Yii::app()->session['userId'] ?? '';
    $base_url = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
?>
<link title="timeline-styles" rel="stylesheet" href="https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css">
<style>
    #program-container<?= $blockKey ?> {
        text-align: left;
    }

    #journee-caroussel<?= $blockKey ?> {
        position: relative;
    }

    .slide {
        list-style: none;
        flex: 0 0 auto;
        width: 100%;
    }

    .slider {
        width: 100%;
        overflow: hidden;
        margin: 0 auto;
        position: relative;
    }

    .slides {
        display: flex;
        flex-direction: row;
        height: 100%;
        width: 100%;
        margin: 0;
        padding: 0;
    }

    #items<?= "$blockKey " ?> {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    #items<?= "$blockKey " ?>.item {
        cursor: pointer;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    #items<?= "$blockKey " ?>.item .date {
        border: <?= $paramsData['primaryColor'] ?> 1px solid;
        background: <?= $paramsData['primaryColor'] ?>;
        color: white;
        padding: 1.2rem 1.5rem .8rem;
        border-radius: 10px;
        display: block;
        font-weight: bold;
        font-size: 1.9rem;
        width: fit-content;
        width: -moz-fit-content;
    }

    #items<?= "$blockKey " ?>.item.active .date {
        border-color: black;
        background-color: white;
        color: black;
    }

    #items<?= "$blockKey " ?>.item .jour,
    .month {
        text-align: center;
        font-weight: bold;
        display: block;
        margin: auto 10px;
    }

    #journee-navigation<?= "$blockKey " ?>.journee-control {
        position: absolute;
        top: 5px;
        padding: 1.9rem 1rem;
        border-radius: 10px;
        border: transparent 1px solid;
        background-color: transparent;
        color: <?= $paramsData['primaryColor'] ?>;
    }

    #journee-navigation<?= "$blockKey " ?>.journee-control:hover {
        border-color: <?= $paramsData['primaryColor'] ?>;
        background-color: white;
        cursor: pointer;
    }

    .journee-control.right {
        right: 40px;
    }

    .journee-control.left {
        left: 40px;
    }


    #event-content<?= $blockKey ?> {
        border-left: <?= $paramsData['primaryColor'] ?> solid 5px;
        padding: 2rem;
        margin-left: 5rem !important;
    }

    #event-content<?= "$blockKey " ?>.date {
        font-size: 2rem;
        font-weight: bold;
        display: block;
    }

    #event-content<?= "$blockKey " ?>ul {
        margin: none;
    }

    #event-content<?= "$blockKey " ?>.event-detail {
        position: relative;
        font-weight: bold;
    }

    #event-content<?= "$blockKey " ?>.event-detail li {
        list-style: none;
        margin-top: 2rem;
    }

    #event-content<?= "$blockKey " ?>.event-detail li::before {
        content: "\2022";
        color: <?= $paramsData['primaryColor'] ?>;
        font-weight: bold;
        font-size: 1.5em;
        display: inline-block;
        width: .8em;
        margin-left: -1em;
    }

    #event-content<?= "$blockKey " ?>.event-detail-detail li {
        margin-top: 0;
        line-height: 1;
    }

    #event-content<?= "$blockKey " ?>.event-detail-detail li::before {
        color: black;
    }

    #event-content<?= "$blockKey " ?>.event-hour {
        font-weight: bold;
        position: absolute;
        left: -7rem;
        top: .7rem;
        color: <?= $paramsData['primaryColor'] ?>;
    }

    #active_filter_container<?= $blockKey ?> {
        background-color: <?= $paramsData['primaryColor'] ?>;
        color: white;
        position: fixed;
        left: 50px;
        padding: 10px;
        border-radius: 0 0 10px 10px;
        box-shadow: 0 0 10px rgba(0, 0, 0, .3);
        z-index: 9;
        border-left: 1px solid<?= $paramsData['primaryColor'] ?>;
        border-bottom: 1px solid<?= $paramsData['primaryColor'] ?>;
        border-right: 1px solid<?= $paramsData['primaryColor'] ?>;
        cursor: pointer;
    }

    .coevent-button<?= $blockKey ?> {
        border: none;
        border-radius: 20px;
        padding: 1rem 2rem;
        background-color: <?= $paramsData['primaryColor'] ?>;
        color: white;
    }

    .coevent-button<?= "$blockKey " ?>:hover {
        text-decoration: none;
    }

    .coevent-button<?= "$blockKey " ?>.inverted {
        color: <?= $paramsData['primaryColor'] ?>;
        background-color: white;
    }

    @media only screen and (max-width: 425px) {
        .journee-control.left {
            left: 0;
        }

        .journee-control.right {
            right: 0;
        }
    }

    @media only screen and (max-width: 992px) {

        #program-container<?= $blockKey ?> {
            text-align: left;
            max-width: 100%;
        }
    }
</style>
<div id="active_filter_container<?= $blockKey ?>" class="tooltips active_filter_container" data-toggle="tooltip" data-placement="bottom"
     data-original-title="Cliquez ici pour désactiver le filtre" area-describedby="tooltipeventfilter<?= $blockKey ?>" style="display: none;">
    Evénements filtrés sur la catégorie : <span id="active_filter_text<?= $blockKey ?>" class="active_filter_text" data-type=""></span>
    <div id="tooltipeventfilter<?= $blockKey ?>" class="tooltip">Cliquez ici pour désactiver le filtre</div>
</div>
<div class="container">
    <div id="leprogramme<?= $blockKey ?>" data-anchor-target="leprogramme" class="row">
        <div class="col-sm-12 program-container" id="program-container<?= $blockKey ?>">
            <h1 class="dinalternate" style="color: black;">
                Le programme
            </h1>
            <div style="border: solid <?= $paramsData['primaryColor'] ?> 4px;border-radius: 11px;width: 17%; margin-bottom: 15px;"></div>
            <div class="btn-group journee_views" style="margin-bottom: 1em;">
                <button type="button" class="btn btn-default program-view">Calendrier</button>
                <button type="button" class="btn btn-default program-view">Filtre par journée</button>
                <button type="button" class="btn btn-default program-view">Chronologie</button>
            </div>

            <div id="journee-caroussel<?= $blockKey ?>">
                <div class="slider">
                    <div class="slides" id="journee-carousel<?= $blockKey ?>"></div>
                </div>
                <div id="journee-navigation<?= $blockKey ?>">
                    <a class="journee-control left" data-slide="prev">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="journee-control right" data-slide="next">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div id="event-content<?= $blockKey ?>"></div>
        </div>
    </div>
    <!-- vue calendrier des events -->
    <div id="event_calendar_container<?= $blockKey ?>">
        <div id="event_calendar<?= $blockKey ?>"></div>
    </div>
    <!-- vue calendrier des events -->
    <div id="chronology_container<?= $blockKey ?>">
        <div id='timeline-embed<?= $blockKey ?>' style="width: 100%; height: 55vh; margin-top: 50px;"></div>
    </div>
    <br>
    <div class="text-center">
        <?php if (!empty($connected_user)) { ?>
            <button class="coevent-button<?= $blockKey ?>" type="button" id="event-proposition<?= $blockKey ?>">
                <i class="fa fa-calendar"></i> Proposer un évènement
            </button>
        <?php } ?>
        <button class="coevent-button<?= $blockKey ?>" type="button" id="event-download<?= $blockKey ?>">
            <i class="fa fa-file-pdf-o"></i> Télécharger le programme
        </button>
    </div>
</div>
<br>
<script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
<script type="text/javascript" src="<?= $base_url . Yii::app()->getModule('costum')->getAssetsUrl() ?>/js/coevent/carousel.js"></script>
<script type="text/javascript">
    (function ($, W) {
        var _journee_carousel = null;

        function reload_program_event() {
            $('#program-container<?= $blockKey ?>').off('click').on('click', '#journee-carousel<?= $blockKey ?> .item', function (__e) {
                __e.preventDefault();
                load_programs($(this).data('target'));
            });
        }

        function get_event_context() {
            return new Promise(function (__resolve) {
                ajaxPost(null, `${baseUrl}/costum/coevent/get_events/request/event/event/${costum['contextId']}`, null, function (__event) {
                    __resolve(__event);
                }, null, 'json')
            });
        }

        function get_subevents(__object = {}) {
            var url = __object.type ? `${baseUrl}/costum/coevent/get_events/request/subevents/event/${costum['contextId']}/filter/${__object.type}` : `${baseUrl}/costum/coevent/get_events/request/subevents/event/${costum['contextId']}`;
            return new Promise(function (__resolve) {
                ajaxPost(null, url, __object, function (__events) {
                    __resolve(__events)
                }, null, 'json')
            });
        }

        function get_subevent_dates(__type) {
            var url = __type ? `${baseUrl}/costum/coevent/get_events/request/dates/event/${costum['contextId']}/filter/${__type}` : `${baseUrl}/costum/coevent/get_events/request/dates/event/${costum['contextId']}`;
            return new Promise(function (__resolve) {
                ajaxPost(null, url, null, function (__events) {
                    __resolve(__events)
                }, null, 'json')
            });
        }

        function load_timeline(__option = {}) {
            return new Promise(function (resolve) {
                var timeline_knightlab = null;
                var tl_interval = setInterval(function () {
                    if (typeof TL !== 'undefined') {
                        clearInterval(tl_interval);
                        var sub_event_arg = {
                            type: __option.type ? __option.type : ''
                        };
                        get_subevents(sub_event_arg).then(function (__subevents) {
                            let start_at_slide = -1;
                            var data = {
                                events: __subevents.map(function (__event, __index) {
                                    var moment_start = moment(__event['start_date']);
                                    var moment_end = moment(__event['end_date']);
                                    if (typeof __option.goto !== 'undefined' && moment_start.isSame(__option.goto, 'day') && start_at_slide < 0) start_at_slide = __index;
                                    var map = {
                                        start_date: {
                                            year  : moment_start.year(),
                                            month : moment_start.month() + 1,
                                            day   : moment_start.date(),
                                            hour  : moment_start.hour(),
                                            minute: moment_start.minute()
                                        },
                                        end_date  : {
                                            year  : moment_end.year(),
                                            month : moment_end.month() + 1,
                                            day   : moment_end.date(),
                                            hour  : moment_end.hour(),
                                            minute: moment_end.minute()
                                        },
                                        text      : {
                                            headline: __event['name'],
                                            text    : `<blockquote>${__event['short_description']}</blockquote> ${dataHelper.markdownToHtml(__event['full_description'])}`
                                        },
                                        media     : {
                                            url      : __event['profile'],
                                            thumbnail: __event['profile']
                                        },
                                        background: {}
                                    };
                                    if (__event['banner'].length) map['background']['url'] = __event['banner'];
                                    else map['background']['color'] = `<?= $paramsData['primaryColor'] ?>`;

                                    return map;
                                })
                            };
                            var options = {
                                script_path     : `${baseUrl}/plugins/knightlab_timeline3/js/`,
                                language        : 'fr',
                                font            : 'bevan-pontanosans',
                                theme           : 'contrast',
                                scale_factor    : 0.5,
                                timenav_position: 'top'
                            }

                            if (start_at_slide >= 0) options.start_at_slide = start_at_slide;

                            timeline_knightlab = new TL.Timeline('timeline-embed<?= $blockKey ?>', data, options);
                            resolve();
                        });
                    }
                });
            });
        }

        function load_fullcalendar(__option = {}) {
            return new Promise(function (resolve) {
                get_event_context().then(function (__parent_event) {
                    var start_moment_parent = moment(__parent_event['start_date']);
                    var start_moment_parent = moment(__parent_event['start_date']);
                    var end_moment_parent = moment(__parent_event['end_date']);
                    var default_view = '';
                    switch (true) {
                        case start_moment_parent.format('YYYY-MM-DD') == end_moment_parent.format('YYYY-MM-DD'):
                            default_view = 'agendaDay';
                            break;
                        case end_moment_parent.diff(start_moment_parent, 'days') <= 6 && start_moment_parent.day() == 1:
                            default_view = 'agendaWeek';
                            break;
                        default:
                            default_view = 'month';
                            break
                    }
                    var request_arg = {
                        type: __option.type ? __option.type : ''
                    };
                    get_subevents(request_arg).then(function (__events) {
                        var calendar = $('#event_calendar<?= $blockKey ?>');
                        var events = __events.map(function (__event) {
                            return {
                                id             : __event['id'],
                                title          : __event['name'],
                                start          : moment(__event['start_date']).format('YYYY-MM-DD HH:mm'),
                                end            : moment(__event['end_date']).format('YYYY-MM-DD HH:mm'),
                                className      : ['lbh-preview-element'],
                                url            : `#page.type.events.id.${__event['id']}`,
                                backgroundColor: `<?= $paramsData['primaryColor'] ?> !important`
                            }
                        });
                        if (calendar.children().length > 0) {
                            calendar.fullCalendar('removeEvents');
                            calendar.fullCalendar('addEventSource', events);
                            calendar.fullCalendar('changeView', default_view);
                        } else {
                            calendar.fullCalendar({
                                lang          : 'fr',
                                timeFormat    : 'H(:mm)',
                                defaultView   : default_view,
                                header        : {
                                    left  : 'title',
                                    center: '',
                                    right : 'prev,next'
                                },
                                events,
                                eventMouseover: function () {
                                    coInterface.bindLBHLinks();
                                }
                            });
                        }
                        if (typeof __option.goto !== 'undefined') calendar.fullCalendar('gotoDate', __option.goto);
                        else calendar.fullCalendar('gotoDate', start_moment_parent.format('YYYY-MM-DD HH:mm'));
                        resolve();
                    });
                });
            });
        }

        function reset__journee_carousel() {
            return carousel({
                container : $('#journee-carousel<?= $blockKey ?>'),
                autoplay  : false,
                navigation: {
                    indicator: false,
                    button   : {
                        show: false,
                        next: '<span class="fa fa-chevron-right"></span>',
                        prev: '<span class="fa fa-chevron-left"></span>'
                    }
                },
                delay     : 2000
            });
        }

        function coevent_create_event(__start = null, __end = null) {
            dyFObj.unloggedMode = true;
            get_event_context().then(function (__event) {
                var today = moment();
                var moment_start = __start != null ? (today.format('YYYY-MM-DD') === start_date.format('YYYY-MM-DD') ? today : __start) : moment(__event['start_date']);
                var moment_end = __end != null ? __end.clone().subtract(60, 'minutes') : moment(__event['end_date']);
                var eventDynformDefaultParams = {
                    beforeBuild: {
                        "properties": {
                            "type": {
                                "options": eventTypes
                            }
                        }
                    },
                    afterBuild : function () {
                        $(".parentfinder").hide();
                        $(".publiccheckboxSimple").hide();
                        $(".recurrencycheckbox").hide();
                        $(".organizerNametext").hide();
                        $(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");
                    },
                    afterSave  : function (data) {
                        var callbackParams = {};
                        callbackParams['links'] = {};
                        callbackParams['links']['attendees'] = {};
                        callbackParams['links']['attendees'][data['map']['creator']] = {
                            type   : 'citoyens',
                            isAdmin: true
                        };

                        callbackParams['parent'] = {};
                        callbackParams['parent'][costum.contextId] = {
                            "type": costum.contextType,
                            "name": costum.title
                        };

                        var params = {
                            id        : data['id'],
                            collection: data['map']['collection'],
                            path      : 'allToRoot',
                            value     : callbackParams
                        };

                        dataHelper.path2Value(params, (callback) => {
                            dyFObj.commonAfterSave(params, function () {
                                const filter_active = $('.active_filter_container:first').is(':visible');
                                var type = filter_active ? $('.active_filter_text:first').data('type') : '';
                                load_views(type);
                                dyFObj.closeForm();
                            });
                        });
                    }
                };
                var eventDynformParams = (costum != null && typeof costum.typeObj != "undefined" && typeof costum.typeObj.events != "undefined" && typeof costum.typeObj.events.dynFormCostum != "undefined") ? null : eventDynformDefaultParams;
                var presentValuesEvent = {
                    startDate: moment_start.format('DD/MM/YYYY HH:mm'),
                    endDate  : moment_end.format('DD/MM/YYYY HH:mm')
                };
                
                if($('#event-content<?= $blockKey ?>').is(':visible')) {
                    const selected_date = moment($('#event-content<?= $blockKey ?> .dinalternate.date').text(), 'DD.MM.YYYY');
                    selected_date.hour(parseInt($('.event-hour:last').data('hour')));
                    presentValuesEvent.startDate = selected_date.format('DD/MM/YYYY HH:mm');
                    presentValuesEvent.endDate = selected_date.clone().add(1, 'hours').format('DD/MM/YYYY HH:mm');
                }
                dyFObj.openForm('event', null, presentValuesEvent, null, eventDynformParams);
                return false;
            });
        }

        function download_pdf() {
            document.location.href = `${baseUrl}/costum/coevent/getpdfaction?event=<?= $costum['contextId'] ?>`;
            return false;
        }

        function get_actors(__types = ['organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName']) {
            var url = `${baseUrl}/costum/coevent/get_events/request/actors/event/${costum['contextId']}`;
            var parameter = {
                types: __types
            }
            return new Promise(function (__resolve) {
                ajaxPost(null, url, parameter, function (__events) {
                    __resolve(__events)
                }, null, 'json')
            });
        }

        function load_programs(__date = '', __element = null) {
            return new Promise(function (__resolve) {
                var criteria = ['links.organizer', 'organizer', 'organizerName'];
                get_actors(criteria).then(function (__communities) {
                    var subevent_criteria = {
                        date: __date || 'begining',
                        type: $('#active_filter_container<?= $blockKey ?>').is(':hidden') ? '' : $('#active_filter_text<?= $blockKey ?>').data('type')
                    };

                    get_subevents(subevent_criteria).then(function (__subevents) {
                        var container = $('#event-content<?= $blockKey ?>');
                        var html = '';
                        var last_hour = '';
                        $.each(__subevents, function (__index, __subevent) {
                            var organizers = __communities.filter(__community => typeof __community['roles'] !== 'undefined' && typeof __community['roles'][__subevent['id']] !== 'undefined').map(__community => {
                                return {
                                    name : __community['name'],
                                    image: __community['image']
                                }
                            });

                            var moment_start = moment(__subevent['start_date']);
                            var moment_end = moment(__subevent['end_date']);

                            if (__index === 0) html += `<span class="dinalternate date">${moment_start.format('DD.MM.YYYY')}</span>`;
                            if (moment_start.format('HH') !== last_hour) {
                                last_hour = moment_start.format('HH');
                                if (__index !== 0) html += '</ul>';
                                html += `<ul class="event-detail"><span class="event-hour" data-hour="${moment_start.format('H')}">${last_hour}H</span>`;
                            }
                            var actors_with_id = [];
                            if (userConnected) actors_with_id = __subevent['communities'].filter(__community => __community === userConnected['_id']['$id']);

                            var isFollowed = false;
                            if (userId != "" && typeof __subevent.links.attendees != 'undefined' && typeof __subevent.links.attendees[userId] != "undefined") isFollowed = true;
                            var isShared = false;
                            var tip = trad['interested'];
                            var actionConnect = 'follow';
                            var icon = 'chain';
                            var classBtn = '';
                            if (isFollowed) {
                                actionConnect = 'unfollow';
                                icon = 'unlink';
                                classBtn = 'text-green';
                                tip = "Je ne veux plus participer";
                            }

                            var btnLabel = (actionConnect == "follow") ? "PARTICIPER" : "DÉJÀ PARTICIPANT(E)";

                            html += `<li>
                                    <a href="#page.type.events.id.${__subevent['id']}" class="lbh-preview-element" style="color: <?= $paramsData['primaryColor'] ?>">${__subevent['name']}</a> 
                                    <button class="btn btn-default btn-sm tooltips" onclick="event_participation(this)"
                                    data-toggle="tooltip" data-placement="left" data-original-title="${tip}"
                                    data-ownerlink="${actionConnect}" data-id="${__subevent['id']}" data-type="events" data-name="${__subevent['name']}"
                                    data-isFollowed="${isFollowed}"><i class="fa fa-${icon}"></i> ${btnLabel}</button><br>
                                    ${organizers.length == 0 ? `Aucun organisateur<br>` : `Organisateur${organizers.length > 1 ? 's' : ''}:<br>${organizers.map(function (map) {
                                return `<span style="padding-left: 20px;"> ${map['image'] ? ` * <img src="${baseUrl}/${map['image']}" height="30" width="30" class="" style="display: inline; vertical-align: middle; border-radius:100%;">` : ' * '} ${map['name']}</span><br>`;
                            }).join('')}`}
                                    ${__subevent['address'] ? `Lieu : ${__subevent['address']}<br>` : ''}
                                    ${typeof __subevent['short_description'] !== 'undefined' ? __subevent['short_description'] : ''}<br>
                                </li>`;
                        });

                        var items = $('#journee-caroussel<?= $blockKey ?> #items<?= $blockKey ?> .item');
                        items.removeClass('active');

                        if (items.length) {
                            date = moment(__subevents[0]['start_date']).format('DD.MM.YYYY');
                            if (__element) {
                                __element.classList.add('active');
                            } else items.each(function () {
                                if ($(this).data('target') === date) $(this).addClass('active');
                            });
                        }
                        html += '';
                        container.html(html);
                        coInterface.bindLBHLinks();
                        __resolve();
                    });
                });
            })
        }

        function load_journees(__option = {}) {
            return new Promise(function (__resolve) {
                const window_size = $(W).width();
                var container = $('#journee-carousel<?= $blockKey ?>');
                var date_view_limit = __option.view_limit ? __option.view_limit : (window_size <= 320 ? 1 : (window_size <= 425 ? 3 : (window_size <= 768 ? 4 : 5)));
                var type = $('#active_filter_container<?= $blockKey ?>').is(':hidden') ? '' : $('#active_filter_text<?= $blockKey ?>').data('type');
                get_subevent_dates(type).then(function (__dates) {
                    var html = '<div class="slide"><div id="items<?= $blockKey ?>">';
                    var last_group = '';
                    var change = 0;

                    __dates.forEach(function (__date) {
                        if (change % date_view_limit === 0 && change !== 0) {
                            html += '</div></div><div class="slide"><div id="items<?= $blockKey ?>">';
                        }
                        if (last_group !== __date['date_group']) {
                            change++;
                            last_group = __date['date_group'];
                            mylog.log('Rinelfi dates', __date);
                            html += `<div class="item" data-target="${last_group}">
                                    <span class="month">${__date['year']}<br>${trad[__date['month'].toLowerCase()].toUpperCase()}</span>
                                    <span class="date">${__date['date']}</span>
                                    <span class="jour">${trad[__date['day'].toLowerCase()].toUpperCase()}</span>
                                </div>`;
                        }
                    });
                    html += '</div></div>';
                    container.html(html);
                    _journee_carousel = reset__journee_carousel();
                    _journee_carousel.init();
                    _journee_carousel.onResize = load_journees;
                    if (change > date_view_limit) {
                        $('#journee-navigation<?= $blockKey ?> .journee-control').css({
                            display: 'block'
                        });
                    } else $('#journee-navigation<?= $blockKey ?> .journee-control').css({
                        display: 'none'
                    });
                    reload_program_event();
                    __resolve();
                });
            });
        }

        function change_views(__view, __element) {
            $('#program-container<?= $blockKey ?> .journee_views button').removeClass('active');
            $(__element).addClass('active');
            switch (__view) {
                case 0:
                    $('#event_calendar_container<?= $blockKey ?>').css('display', '');
                    $('#journee-caroussel<?= $blockKey ?>').css('display', 'none');
                    $('#chronology_container<?= $blockKey ?>').css('display', 'none');
                    $('#program-container<?= $blockKey ?>').next('div').css('display', 'none');
                    break;
                case 1:
                    $('#event_calendar_container<?= $blockKey ?>').css('display', 'none');
                    $('#journee-caroussel<?= $blockKey ?>').css('display', '');
                    $('#chronology_container<?= $blockKey ?>').css('display', 'none');
                    $('#program-container<?= $blockKey ?>').next('div').css('display', '');
                    const filter_active = $('.active_filter_container:first').is(':visible');
                    var type = filter_active ? $('.active_filter_text:first').data('type') : '';
                    load_views(type);
                    break;
                case 2:
                    $('#event_calendar_container<?= $blockKey ?>').css('display', 'none');
                    $('#journee-caroussel<?= $blockKey ?>').css('display', 'none');
                    $('#chronology_container<?= $blockKey ?>').css('display', '');
                    $('#program-container<?= $blockKey ?>').next('div').css('display', 'none');
                    break;
            }
        }

        function load_views(__type = '') {
            return new Promise(function (__resolve) {
                load_journees().then(function () {
                    const journees_dom = $('#journee-carousel<?= $blockKey ?> .item');
                    let nearest_incoming_event;
                    const today = moment();
                    const format_string = 'DD.MM.YYYY';
                    journees_dom.each(function () {
                        const self = $(this);
                        const target_date = self.data('target');
                        const this_moment = moment(target_date, format_string);
                        if (this_moment.isAfter(today) && (typeof nearest_incoming_event === 'undefined' || moment(nearest_incoming_event, format_string).isAfter(this_moment))) {
                            nearest_incoming_event = target_date;
                        }
                    });
                    load_programs(nearest_incoming_event).then(function () {
                        const params = {
                            type: __type,
                        };
                        if (typeof nearest_incoming_event !== 'undefined') params.goto = moment(nearest_incoming_event, 'DD.MM.YYYY');
                        load_timeline(params).then(function () {
                            load_fullcalendar(params).then(function () {
                                coInterface.bindLBHLinks();
                                __resolve();
                            })
                        })
                    })
                });
            });
        }

        $(function () {
            $('#program-container<?= $blockKey ?>').on('coeventfilter', function () {
                const filter_active = $('.active_filter_container:first').is(':visible');
                var type = filter_active ? $('.active_filter_text:first').data('type') : '';
                load_views(type);
            });
            $('#program-container<?= $blockKey ?> .program-view').each(function (__index, __element) {
                $(this).on('click', function () {
                    change_views(__index, __element);
                })
            });
            $('#journee-navigation<?= $blockKey ?> .journee-control.right').on('click', function () {
                if (_journee_carousel) _journee_carousel.gotoNext(function () {
                    reload_program_event();
                    var active = $('#event-content<?= $blockKey ?> .dinalternate.date').text();
                    $('[data-target="' + active + '"]').addClass('active');
                });
            });
            $('#journee-navigation<?= $blockKey ?> .journee-control.left').on('click', function () {
                if (_journee_carousel) _journee_carousel.gotoPrevious(function () {
                    reload_program_event();
                    var active = $('#event-content<?= $blockKey ?> .dinalternate.date').text();
                    $('[data-target="' + active + '"]').addClass('active');
                });
            });
            $('#event-proposition<?= $blockKey ?>').on('click', function (__e) {
                __e.preventDefault();
                coevent_create_event();
            });
            $('#event-download<?= $blockKey ?>').on('click', function (__e) {
                __e.preventDefault();
                download_pdf();
            });
            get_event_context().then(function (event) {
                load_views().then(function () {
                    var start_moment_parent = moment(event['start_date']);
                    var end_moment_parent = moment(event['end_date']);
                    if (end_moment_parent.diff(start_moment_parent, 'days') > 6) change_views(1, document.querySelector('#program-container<?= $blockKey ?> .program-view:nth-child(2)'));
                    else change_views(0, document.querySelector('#program-container<?= $blockKey ?> .program-view:first-child'));
                });
            });
        });
    })(jQuery, window);
</script>