<?php
$paramsData = [
    'primaryColor' => '#EE302C'
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$connected_user = Yii::app()->session['userId'] ?? '';
$base_url = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
?>
<style>
    .slide {
        list-style: none;
        flex: 0 0 auto;
        width: 100%;
    }

    .slider {
        width: 100%;
        overflow: hidden;
        margin: 0 auto;
        position: relative;
    }

    .slides {
        display: flex;
        flex-direction: row;
        height: 100%;
        width: 100%;
        margin: 0;
        padding: 0;
    }

    .card-acteurs {
        width: 100%;
        margin-left: auto;
        margin-right: auto;
        padding: 1em 1em 4em;
        display: flex;
        justify-content: space-evenly;
    }

    .card-acteur {
        border-radius: 10px;
        max-width: 300px;
        flex: 1;
        background-color: white;
        box-shadow: 0 0 10px rgba(0, 0, 0, .5);
        overflow: hidden;
    }

    .card-acteur .card-header {
        height: 200px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center;
    }

    .card-acteur .card-content {
        position: relative;
        padding: 1em;
        font-weight: bold;
    }

    .card-acteur .card-content .badget {
        font-size: 1.5em;
        display: block;
        color: white;
        border-radius: 5px;
        position: absolute;
        left: 15px;
        top: -25px;
        padding: .1em .2em;
    }

    #contributors<?= "$blockKey " ?>.card-acteur .card-content .badget {
        background-color: <?= $paramsData['primaryColor'] ?>;
    }

    .card-acteur .card-content .acteur-nom {
        display: block;
    }

    #contributors<?= "$blockKey " ?>.card-acteur .card-content .acteur-nom {
        color: <?= $paramsData['primaryColor'] ?>;
    }

    .slide-indicators {
        margin: 0;
        padding: 5px;
        position: absolute;
        display: flex;
        flex-direction: row;
        bottom: 12px;
        left: 50%;
        transform: translateX(-50%);
        background-color: rgba(0, 0, 0, .3);
        border-radius: 20px;
        line-height: 0;
    }

    .slide-navigator {
        list-style: none;
        display: block;
        padding: 15px 10px;
        background-color: transparent;
        margin-top: -5px;
        margin-bottom: -5px;
        color: white;
        cursor: pointer;
        align-self: center;
    }

    .slide-navigator:hover {
        background-color: white;
        color: black;
    }

    .slide-navigator.prev {
        margin-left: -6px;
        border-right: 1px solid rgba(255, 255, 255, .5);
        border-radius: 20px 0 0 20px;
    }

    .slide-navigator.next {
        margin-right: -6px;
        border-left: 1px solid rgba(255, 255, 255, .5);
        border-radius: 0 20px 20px 0;
    }

    .slide-indicator {
        display: block;
        list-style: none;
        margin-left: 5px;
        margin-right: 5px;
        align-self: center;
        width: 15px;
        height: 15px;
        border-radius: 50%;
        background-color: transparent;
        border: white 1px solid;
    }

    .slide-indicator {
        cursor: pointer;
    }

    .slide-indicator.active {
        background-color: white;
    }
</style>
<div class="row footer-section">
    <div id="acteursetparticipants<?= $blockKey ?>" data-anchor-target="acteursetparticipants">
        <div class="col-md-12">
            <h1 class="dinalternate text-center" style="color: black;">
                Acteurs et participants
            </h1>
        </div>
        <div class="col-xs-12">
            <div class="container" style="position: relative;">
                <img src="<?= Yii::app()->getModule('costum')->getAssetsUrl() ?>/images/coevent/points_block_membre.png" alt="block membre" style="width: 100px; position: absolute; top: 20px; left: 0;">
                <div class="row">
                    <div class="slider">
                        <ul class="slides" id="contributors<?= $blockKey ?>">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $base_url . Yii::app()->getModule('costum')->getAssetsUrl() ?>/js/coevent/carousel.js"></script>
<script type="text/javascript">
    (function($, W) {
        function get_actors(__types = ['organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName']) {
            var url = baseUrl + '/costum/coevent/get_events/request/actors/event/' + costum['contextId'];
            var parameter = {
                types: __types
            }
            return new Promise(function(__resolve) {
                ajaxPost(null, url, parameter, function(__events) {
                    __resolve(__events)
                }, null, 'json')
            });
        }

        function construct_contributor_html(__index, __contributors = {}) {
            var role_traduction = {
                creator: 'Organisateur',
                attendee: 'Participant',
                speaker: 'Intervenant',
                organizer: `Organisateur d'événement`
            }
            var other_defaults = {
                laSemaineDesTierslieux: baseUrl + '/upload/communecter/organizations/5d85cd7240bb4eec7d496a75/Logo-RTL.gif'
            }

            var html = '<li class="slide"><div class="card-acteurs">';
            var contributor_index = 0;
            for (var contributor_key in __contributors) {
                var contributor = __contributors[contributor_key];
                var roles = contributor.roles;

                var role_array = [];
                for (var role_key in roles) {
                    var role = roles[role_key];
                    if (!role_array.includes(role_traduction[role['role']])) {
                        role_array.push(role_traduction[role['role']]);
                    }
                }


                var tag_description_li = [];
                var description_html = '<small><b>Statut : </b> ' + role_array.map(function(__map) {
                    return __map;
                }).join(', ') + '</small>';

                if (contributor_index % __index === 0 && contributor_index !== 0) {
                    html += '</div></li><li class="slide"><div class="card-acteurs">';
                }

                var default_image = other_defaults[costum['slug']] ? other_defaults[costum['slug']] : costum['assetsUrl'] + '/images/thumbnail-default.jpg';

                html += '<div class="card-acteur">';
                html += '<div class="card-header" style="background-image: url(\'' + (contributor['image'] ? contributor['image'] : default_image) + '\');"></div>';
                html += '<div class="card-content">';
                html += '<!-- <span class="badget">' + trad[contributor['type']] + '</span> -->';
                html += '<span class="acteur-nom">' + contributor['name'] + '</span>';
                html += '<span class="acteur-description">' + description_html + '</span>';
                html += '</div>';
                html += '</div>';
                contributor_index++;
            }
            html += '</div></li>';
            return html;
        }

        function load_contributor_carousel() {
            var container = $('#contributors<?= $blockKey ?>');
            get_actors(['organizer', 'links.orgnaizer', 'organizerName']).then(function(__contributors) {
                var win = $(W);
                if (win.width() <= 700) container.html(construct_contributor_html(1, __contributors));
                else if (win.width() > 700 && win.width() <= 992) container.html(construct_contributor_html(2, __contributors));
                else if (win.width() > 992) container.html(construct_contributor_html(3, __contributors));

                const contributor_carousel = carousel({
                    container,
                    autoplay: false,
                    navigation: {
                        indicator: true,
                        button: {
                            show: true,
                            next: '<span class="fa fa-chevron-right"></span>',
                            prev: '<span class="fa fa-chevron-left"></span>'
                        }
                    },
                    delay: 2000
                });
                contributor_carousel.init();
                contributor_carousel.start();
                contributor_carousel.onResize = function() {
                    if (win.width() <= 700) container.html(construct_contributor_html(1, __contributors));
                    else if (win.width() > 700 && win.width() <= 992) container.html(construct_contributor_html(2, __contributors));
                    else if (win.width() > 992) container.html(construct_contributor_html(3, __contributors));
                    this.recreate(container);
                };
            });
        }

        $(function() {
            load_contributor_carousel();
        });
    })(jQuery, window)
</script>