<?php
/**
 * TPLS QUI PERMET AFFICHAGE DES 3 DERNIERS NOUVEAUTÉS
 * MODÈLE INSPIRER DU COSTUM FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMS LA COULEUR DE INFO
 */
$keyTpl = "blockeventcommunity";

$paramsData = [ 
    "titre" => "Événements de la communautés",
    "icon"  =>  "",
    "color" => "#000000",
    "txtcolor" => "#000000",
    "colorBorder" => "#268e42",
    "colorLabelButton" =>"#ffffff"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

?>
<style>
    @media (max-width: 414px) {
        #containEventCommunity_<?= $kunik?> .name{
            font-size: 18px !important;
            color: <?= $paramsData["txtcolor"] ?>;
        }
        #containEventCommunity_<?= $kunik?> p{
        font-size: 14px;
        }
        .event_<?= $kunik?> h1{
            font-size: 20px;
        }
         .event_<?= $kunik ?> .img-event{
            height: 200px;
            width: 320px;
        }
    }

    .event_<?= $kunik ?> .img-event{
        height: 200px;
        width: 360px;
        border: 7px solid <?= $paramsData["colorBorder"]?>;
        border-radius: 10px;
        margin-bottom: 4%;

    }
    .event_<?= $kunik ?> .plus-img {
        width: 39%;
        height: auto;
    }
    #containEventCommunity_<?= $kunik?> .name{
        font-size: 25px;
        color: <?= $paramsData["txtcolor"] ?>;
    }

    #containEventCommunity_<?= $kunik?> a{
        border-radius: 53px;
        color: <?= $paramsData["colorLabelButton"] ?>;
        background-color: <?= $paramsData["colorBorder"]?>;
        font-size: 17px;
        text-align: center;
        padding: 2% 4% 2% 4%;
        text-decoration: none;
    }
     #containEventCommunity_<?= $kunik?> p{
        color : <?= $paramsData["txtcolor"] ?>
     }
     
    
    .event_<?= $kunik?> h1{
        color:<?= $paramsData["color"] ?>;
    }
    #containEventCommunity_<?= $kunik ?>  #event-affiche{
        margin-bottom: 4%;
    }
</style>

<div class="event_<?= $kunik?>">
    <h1 class="text-center title"> 
        <i class="fa <?= $paramsData['icon'] ?>"></i> 
        <?= $paramsData["titre"] ?>
    </h1>   
    <div id="containEventCommunity_<?= $kunik?>" class="col-xs-12"> </div>   
</div>

<script type="text/javascript">
tplCtx = {};
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
$(document).ready(function(){
    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section bloc d'évènement",
            "description" : "Personnaliser votre section sur les blocs d'évènement",
            "icon" : "fa-cog",
            "properties" : {
                titre : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.titre
                },
                icon : { 
                    label : "<?php echo Yii::t('cms', 'Icon')?>",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                },
                color : {
                    label : "<?php echo Yii::t('cms', 'Title color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                },colorBorder : {
                    label : "<?php echo Yii::t('cms', 'Color of the image border and button')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorder
                },
                txtcolor : {
                    label : "<?php echo Yii::t('cms', 'Text color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.txtcolor
                },
                 colorLabelButton : {
                    label : "<?php echo Yii::t('cms', 'Button label color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorLabelButton
                }
            },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value[k] = formData.parent;
                    }
                });
                mylog.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                 else {
                      mylog.log("activeForm save tplCtx",tplCtx);
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }
            }
        }
    };
    
     $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


        var params = {
            "contextId" : contextId,
            "contextType" : contextType
        };

        ajaxPost(
            null,
            baseUrl+"/costum/costumgenerique/geteventcommunity",
            params,
            function(data){
                mylog.log("success", data);

                var str = "";

                if(data.result == true)
                {
                    var i = 0;
                    var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

                    $(data.element).each(function(key,value){
                        if (i <= 2) {
                            i++;
                            var img = (typeof value.profilMediumImageUr  != "undefined" && value.profilMediumImageUr  != "") ?value.profilMediumImageUr : url;

                            var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != "" ? value.shortDescription : "<?php echo Yii::t('cms', 'No description')?>";

                            str += '<div class="card text-center">';
                            str += '<div id="event-affiche" class="card-color col-md-4 text-center">';
                            str += '<img class="img-event" src="'+img+'">';
                            str += '<p class="name">'+value.name+'</p>';
                            str += '<p >'+description+'</p>';
                            str += '<a id="plus" class="undefined entityName bold  add2fav  lbh-preview-element" href="#page.type.events.id.'+value.id+'"> <?php echo Yii::t("cms", "More information")?>';
                            str += '</a>';
                            str += '</div>';
                            str += '</div>';
                        }else{

                        }                    
                    });
                    mylog.log("str event description",str);
                }
                else
                {
                    str += "<center><?php echo Yii::t('cms', 'There are no events')?></center>";
                }
                $("#containEventCommunity_<?= $kunik?>").html(str);
            }
        );
    });
</script>