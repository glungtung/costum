<?php
$keyTpl     = "modal_with_agenda_calendar";
$paramsData = [
    "labelButton"       => "Boutton",
    "colorlabelButton"  => "#000000",
    "colorBorderButton" => "#000000",
    "colorButton"       => "#ffffff",
    "colorButtonHover" => "#000",
    "colorlabelButtonHover" => "#fff",
    "eventBackgroundColor" => "#2d328d",
    "eventToday" => "#e16d38",
    "showType" => "calendar"
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<?php
$blockKey = (string)$blockCms["_id"];
$latestLogo = [];
$initImage = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"logo"
    ),"image"
);
foreach ($initImage as $key => $value) {
    $latestLogo[]= $value["imagePath"];
}
?>
<style type="text/css">
    .textBack_<?=$kunik?> h2{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    div#calendar-event .fc-event {
        background-color: <?=$paramsData["eventBackgroundColor"]?> !important;
    }
    div#agenda-calendar td.fc-today {
        background: <?=$paramsData["eventToday"]?>!important;
    }
    /*div#calendar-event .fc th, div#calendar-event .fc td {
        border-color: <?=$paramsData["eventToday"]?>!important;
    }*/
    .button_<?=$kunik?> {
        background-color: <?=$paramsData["colorButton"]?>;
        border:1px solid <?=$paramsData["colorBorderButton"]?>;
        color: <?=$paramsData["colorlabelButton"]?>;
        margin-bottom: 4%;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        padding: 10px;
        font-size: 16px;
        cursor: pointer;
        /*border-radius: 20px ;*/
        padding: 8px 8px;
    }

    .button_<?=$kunik?>:hover {
        background-color: <?=$paramsData["colorButtonHover"]?>;
        color: <?=$paramsData["colorlabelButtonHover"]?>;
        font-weight:700;
    }
/*    .block-container-<?=$kunik?> {
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
*/

    @media (max-width: 414px) {


        .button_<?=$kunik?> {
            padding: 8px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 11px;
            cursor: pointer;
            border-radius: 20px;
        }
    }

    @media screen and (min-width: 1500px){

        .button_<?=$kunik?> {
            padding: 10px 20px;
            margin-bottom: 2%;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 25px;
            cursor: pointer;
            border-radius: 35px;

        }
    }

    #modal_<?=$kunik?> .modal-dialog,
    #modal_<?=$kunik?> .modal-content {
        position: fixed;
        top: 10%;
        right: 5%;
        left: 5%;
        bottom: 10%;
        box-shadow: none;
        height: auto;
        padding-top: 10px !important;
        width: auto !important;
    }
    #modal_<?=$kunik?>.modal {
        margin: 0;
        padding: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.5);
        border-left: 3px solid #333;
        z-index: 200000;
    }

    #modal_<?=$kunik?> .modal-body {
        height: 100%;
    }
    <?php if(isset($latestLogo) && !empty($latestLogo)){ ?>
        .contain_image-bg {
            background-image: url(<?php echo $latestLogo[0] ?>);
            width: 100%;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center;
            /* Firefox */
            height: -moz-calc(100% - 70px);
            /* WebKit */
            height: -webkit-calc(100% - 70px);
            /* Opera */
            height: -o-calc(100% - 70px);
            /* Standard */
            height: calc(100% - 70px);
        }
    <?php } ?>
</style>

<?php $idBtn = ($paramsData["showType"] == "calendar") ? "modal_agenda_".$kunik : "" ?>
<div class="col-xs-12 textBack_<?=$kunik?> text-center container"  >

    <button  id="<?=$idBtn?>"  class=" button_<?=$kunik?> btn btn-lg btn-primary"  data-toggle="modal" data-target="#modal_<?=$kunik?>">
        <?=$paramsData["labelButton"]?>
    </button>
    <div class="modal fade" id="modal_<?=$kunik?>" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-default btn-sm text-dark pull-right close-modal margin-left-10" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>

                <h4><i class="fa fa-calendar"></i> <?=$paramsData["labelButton"]?></h4>
            </div>
            <div class="modal-body">
                <?php if ($paramsData["showType"] == "calendar") { ?>
                    <div id="calendar-event" class="calendar-event">
                        <div id="agenda-calendar" class="no-padding"></div>
                    </div>
                <?php } else { ?>
                    <div class="col-xs-12 contain_image-bg no-padding">
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "showType":{
                        "label" : "<?php echo Yii::t('cms', 'Display type')?>",
                        inputType : "select",
                        options : {
                            "calendar" : "<?php echo Yii::t('cms', 'Calendar')?>",
                            "image" : "<?php echo Yii::t('cms', 'Image')?>"
                        },
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.showType
                    },
                    "eventBackgroundColor":{
                        label : "<?php echo Yii::t('cms', 'Event background color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.eventBackgroundColor
                    },
                    "eventToday":{
                        label : "<?php echo Yii::t('cms', 'Background color of the day')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.eventToday
                    },
                    "logo" :{
                        "inputType" : "uploader",
                        "docType": "image",
                        "contentKey":"slider",
                        "endPoint": "/subKey/logo",
                        "domElement" : "logo",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        "label": "<?php echo Yii::t('cms', 'Image')?>",
                        "itemLimit" : 1,
                        "showUploadBtn": false,
                        initList : <?php echo json_encode($initImage) ?>
                    },
                    "labelButton" : {
                        "label" : "<?php echo Yii::t('cms', 'Button label')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelButton
                    },
                    "colorlabelButton":{
                        label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButton
                    },
                    "colorButton":{
                        label : "<?php echo Yii::t('cms', 'Button color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButton
                    },
                    "colorButtonHover":{
                        label : "<?php echo Yii::t('cms', 'Background color on hover')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButtonHover
                    },
                    "colorlabelButtonHover" :{
                        label : "<?php echo Yii::t('cms', 'Text color on hover')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButtonHover
                    },
                    "colorBorderButton":{
                        label : "<?php echo Yii::t('cms', 'Button border color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorderButton
                    }
                },

                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        mylog.log("sectiondyfff",sectionDyf);
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"event",6,6,null,null,"Propriété du calendar","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"color",6,6,null,null,"Propriété du bouton","blue","");
        });

        var domCal = {
            container : "#agenda-calendar",
            options :{
                header: {
                    left: 'prev,next',
                    center: 'title',
                    right: 'today'
                },
            },
            eventBackgroundColor: '#2D328D',
        };

        $('#modal_agenda_<?=$kunik?>').on('click', function(e) {
            setTimeout(function() {
                var eventCalendarModal={};
                eventCalendarModal = calendarObj.init(domCal);
                eventCalendarModal.options.personalAgenda = true
                eventCalendarModal.results.add(eventCalendarModal,myContacts.events);
            }, 2000);
        });
    });


</script>