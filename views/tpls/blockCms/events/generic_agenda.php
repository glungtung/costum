<?php
    
    use Liliumdev\ICalendar\ZCiCal;
    use PixelHumain\PixelHumain\modules\co2\controllers\actions\agenda\CalendarAction;
    
    $keyTpl = 'generic_agenda';
    $kunik = $kunik ?? sha1(date('c'));
    $blockKey = $blockKey ?? '';
    $connectedUser = Yii::app()->session['userId'] ?? '';
    $this->costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
    $context = PHDB::findOneById($this->costum['contextType'], $this->costum['contextId'], ['links']);
    
    $readonly_content = $readonly_content ?? false;
    $is_admin = false;
    if (!empty($connectedUser) && !empty($context['links'])) {
        if (!empty($context['links']['contributors'][$connectedUser]['isAdmin']) && filter_var($context['links']['contributors'][$connectedUser]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) $is_admin = true;
        else if (!empty($context['links']['attendees'][$connectedUser]['isAdmin']) && filter_var($context['links']['attendees'][$connectedUser]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) $is_admin = true;
        else if (!empty($context['links']['creator'][$connectedUser]['isAdmin']) && filter_var($context['links']['creator'][$connectedUser]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) $is_admin = true;
        else if (!empty($context['links']['organizer'][$connectedUser]['isAdmin']) && filter_var($context['links']['organizer'][$connectedUser]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) $is_admin = true;
        else if (!empty($context['links']['members'][$connectedUser]['isAdmin']) && filter_var($context['links']['members'][$connectedUser]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) $is_admin = true;
    }
    $view_table = [
        'calendar'            => 'Calendrier',
        'vertical_timeline'   => 'Timeline vertical',
        'horizontal_timeline' => 'Timeline horizontal',
        'gantt'               => 'Gantt'
    ];
    
    $show_el = [
        'participateBtn'     => 'Bouton pour participer',
        'allParticipantList' => 'Bouton qui affiche la liste des participants',
    ];
    
    $cms_data = [
        'content_type'           => $content_type ?? 'events',
        'allow_external_sources' => false,
        'width'                  => true,
        'available_views'        => [
            'calendar',
            'vertical_timeline',
            'horizontal_timeline',
            'gantt'
        ],
        'show'                   => []
    ];
    
    if (isset($blockCms)) {
        foreach ($cms_data as $e => $v) {
            if (isset($blockCms[$e])) {
                $cms_data[$e] = $blockCms[$e];
            }
        }
    }
    
    $openagendas = [];
    $icalendars = [];
    
    if ($cms_data['allow_external_sources'] && $cms_data['content_type'] === 'events') {
        // traitement de l'openagenda
        $urls = $context['links']['agendas']['openagenda']['urls'] ?? [];
        $api_key = $context['links']['agendas']['openagenda']['api_key'] ?? '';
        if (!empty($api_key)) {
            $response = CacheHelper::get("openagenda$api_key");
            if (!$response) {
                $request = curl_init("https://api.openagenda.com/v2/me/agendas?key=$api_key");
                // Tester l'url distant
                curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                $response = json_decode(curl_exec($request), true);
                curl_close($request);
                CacheHelper::set("openagenda$api_key", $response);
            }
            
            // Url valide
            if (!empty($response['success'])) {
                if (!empty($urls)) {
                    $slugs = [];
                    foreach ($urls as $url) {
                        $exploded = explode('/', $url);
                        $domain_index = array_search('openagenda.com', $exploded);
                        $slug = $domain_index === false ? $exploded[0] : $exploded[$domain_index + 1];
                        
                        $public_agenda = CacheHelper::get("openagenda$api_key$slug");
                        if (!$public_agenda) $slugs[] = $slug;
                        else $response['items'][] = $public_agenda;
                    }
                    $slugs = array_unique($slugs);
                    if (count($slugs) > 0) {
                        $url = 'https://api.openagenda.com/v2/agendas?key=' . $api_key . '&slug[]=' . implode('&slug[]=', $slugs);
                        $public = curl_init($url);
                        curl_setopt($public, CURLOPT_RETURNTRANSFER, true);
                        $public_agendas = json_decode(curl_exec($public), true);
                        curl_close($public);
                        $response['items'] = array_merge($response['items'], $public_agendas['agendas']);
                        foreach ($public_agendas['agendas'] as $public_agenda) {
                            CacheHelper::set("openagenda$api_key" . $public_agenda['slug'], $public_agenda);
                        }
                    }
                }
                foreach ($response['items'] as $item) {
                    $color = CalendarAction::get_random_color();
                    $openagenda = [
                        'value'  => 'openagenda' . $item['uid'],
                        'name'   => $item['title'],
                        'color'  => $color,
                        'count'  => 0,
                        'info'   => '',
                        'events' => []
                    ];
                    $url = 'https://api.openagenda.com/v2/agendas/' . $item['uid'] . '/events?key=' . $api_key . '&longDescriptionFormat=HTML&includeFields[]=description&includeFields[]=title&includeFields[]=timings.0&includeFields[]=slug&includeFields[]=status&includeFields[]=longDescription&includeFields[]=uid&includeFields[]=image&size=';
                    // recevoir la liste des events de l'agenda en question
                    $request = curl_init($url . '0');
                    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                    $events = json_decode(curl_exec($request), true);
                    curl_close($request);
                    
                    if (!empty($events['success'])) {
                        $total = $events['total'];
                        $request = curl_init($url . $total);
                        $events = CacheHelper::get("events$url$total");
                        if (!$events) {
                            curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                            $events = json_decode(curl_exec($request), true);
                            curl_close($request);
                            CacheHelper::set("events$url$total", $events);
                        }
                        
                        $openagenda['count'] = count($events['events']);
                        foreach ($events['events'] as $event) {
                            $openagenda['events'][] = [
                                'id'               => $event['uid'],
                                'parent'           => 'openagenda' . $item['uid'],
                                'group'            => $item['title'],
                                'slug'             => $event['slug'],
                                'name'             => $event['title'][Yii::app()->language],
                                'shortDescription' => $event['description'][Yii::app()->language] ?? '',
                                'description'      => $event['longDescription'][Yii::app()->language] ?? '',
                                'startDate'        => $event['lastTiming']['begin'],
                                'endDate'          => $event['lastTiming']['end'],
                                'profilImageUrl'   => !empty($event['image']['base']) && !empty($event['image']['filename']) ? $event['image']['base'] . $event['image']['filename'] : '',
                                'url'              => 'https://openagenda.com/' . $item['slug'] . '/events/' . $event['slug'] . '?lang=' . Yii::app()->language,
                                'color'            => $color
                            ];
                        }
                    }
                    $openagendas[] = $openagenda;
                }
            }
        }
        
        // traitement de l'ical
        if (!empty($context['links']['agendas']['icalendars'])) {
            $db_agendas = $context['links']['agendas']['icalendars'];
            foreach ($db_agendas as $db_agenda) {
                $icalendar = $db_agenda;
                $color = CalendarAction::get_random_color();
                $icalendar['color'] = $color;
                $icalendar['value'] = 'ical' . strtolower(str_replace(' ', '-', $icalendar['name']));
                $icalendar['events'] = [];
                
                if (!empty($icalendar['url'])) {
                    $response = file_get_contents($icalendar['url']);
                    
                    if ($response !== FALSE) {
                        $ical_object = new ZCiCal($response);
                        $timezone = date_default_timezone_get();
                        foreach ($ical_object->tree->child as $node) {
                            if (empty($node)) continue;
                            if ($node->getName() === 'VTIMEZONE') {
                                foreach ($node->data as $key => $value) {
                                    if ($key == 'TZID') {
                                        $timezone = $value->getValues();
                                        break;
                                    }
                                }
                                break;
                            } else if ($node->getName() === 'VEVENT') {
                                foreach ($node->data as $key => $value) {
                                    if ($key == 'TZID') {
                                        $timezone = $value->getValues();
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        foreach ($ical_object->tree->child as $node) {
                            if (empty($node)) continue;
                            if ($node->getName() === 'VEVENT') {
                                $event = [
                                    'parent'         => $icalendar['value'],
                                    'group'          => $icalendar['name'],
                                    'description'    => '',
                                    'profilImageUrl' => '',
                                    'color'          => $color,
                                ];
                                foreach ($node->data as $key => $value) {
                                    switch ($key) {
                                        case 'SUMMARY':
                                            $event['name'] = $value->getValues();
                                            $event['slug'] = str_replace(' ', '', ucwords($event['name']));
                                            break;
                                        case 'URL':
                                            $event['url'] = $value->getValues();
                                            break;
                                        case 'UID':
                                            $event['id'] = $value->getValues();
                                            break;
                                        case 'DESCRIPTION':
                                            $event['shortDescription'] = nl2br($value->getValues());
                                            break;
                                        case 'DTEND':
                                            $end_datetime = new DateTime();
                                            if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $end_datetime->setTimeZone(new DateTimeZone('UTC'));
                                            else $end_datetime->setTimezone(new DateTimeZone($timezone));
                                            $end_datetime->setTimestamp(strtotime($value->getValues()));
                                            $event['endDate'] = $end_datetime->format('c');
                                            break;
                                        case 'DTSTART':
                                            $start_datetime = new DateTime();
                                            if (!empty($value->parameter['value']) && $value->parameter['value'] === 'DATE') $start_datetime->setTimeZone(new DateTimeZone('UTC'));
                                            else $start_datetime->setTimezone(new DateTimeZone($timezone));
                                            $start_datetime->setTimestamp(strtotime($value->getValues()));
                                            $event['startDate'] = $start_datetime->format('c');
                                            break;
                                    }
                                }
                                $icalendar['events'][] = $event;
                            }
                        }
                    }
                }
                
                $icalendar['count'] = count($icalendar['events']);
                $icalendars[] = $icalendar;
            }
        }
    }
    
    $costum = $this->costum;
    $show_filter = isset($show_filter) ? $show_filter : $cms_data['content_type'] === 'events' || $costum['contextType'] === 'organizations' && $cms_data['content_type'] === 'projects';
    
    $scripts_and_css = [
        '/plugins/jquery-timeline/jquery.timeline.min.css',
        '/plugins/dhtmlxgantt/dhtmlxgantt.css',
        '/plugins/jquery-timeline/jquery.timeline.min.js',
        '/plugins/jquery-confirm/jquery-confirm.min.css',
        '/plugins/jquery-confirm/jquery-confirm.min.js',
        '/plugins/select2/select2.css',
        '/plugins/select2/select2.min.js',
        '/plugins/dhtmlxgantt/dhtmlxgantt.js',
        '/plugins/vertical-timeline/css/vertical-timeline.css',
        '/plugins/vertical-timeline/js/vertical-timeline.js',
        '/css/rinelfi/modal.css',
        '/js/rinelfi/modal.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles($scripts_and_css, Yii::app()->request->baseUrl);
    $scripts_and_css = ['/js/forms/conditional-forms.js'];
    HtmlHelper::registerCssAndScriptsFiles($scripts_and_css, Yii::app()->getModule('costum')->getAssetsUrl());
?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Oxygen:wght@300;400;700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
      rel="stylesheet">
<style>
    .timeline::before {
        all: initial;
        all: unset;
    }

    #gantt-title {
        margin-top: 25px;
    }

    #cpl<?= $blockKey ?> {
        display: flex;
        flex-direction: column;
        max-height: 388px;
        overflow-y: auto;
        gap: 10px;
    }

    .cpl,
    #cpl-select<?= $blockKey ?> {
        padding: 9px 10px;
        display: flex;
        align-items: center;
        gap: 5px;
        font-family: Inter, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue";
    }

    .cpl {
        border-radius: 4px;
        border: hsla(0, 0%, 82%, .9) 1px solid;
    }

    #cpl-search<?= $blockKey ?> {
        position: relative;
    }

    #cpl-search<?= "$blockKey " ?> > input,
    #cpl-search<?= "$blockKey " ?> > input:focus {
        border: 1px solid #cacaca;
        border-radius: 20px;
        line-height: 1.8rem;
        padding: 10px 10px 10px 35px;
        width: 100%;
        outline: none;
    }

    #cpl-search<?= "$blockKey " ?> > .fa {
        position: absolute;
        top: 50%;
        left: 10px;
        transform: translateY(-50%);
        color: #cacaca;
        font-size: 2rem;
    }

    .cpl-text {
        flex: 1;
        -moz-user-select: none;
        -webkit-user-select: none;
        user-select: none;
        cursor: default;
        display: block;
        margin-bottom: 0;
        font-weight: 500;
        font-style: normal;
    }

    .card {
        display: flex;
        flex-direction: column;
        border: 1px solid #cfcfcf;
        border-radius: 10px;
    }

    .card > div:first-child {
        border-radius: 10px 10px 0 0;
    }

    .card > div:last-child {
        border-radius: 0 0 10px 10px;
    }

    .card-header {
        padding: 10px;
        margin-bottom: 0;
        background-color: #f7f7f7;
        border-bottom: 1px solid rgba(0, 0, 0, .125);
    }

    .card-body {
        padding: 10px;
        background-color: #fff;
    }

    .gantt_tree_content,
    .cpl-text {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .cpl-actions {
        display: flex;
        flex-direction: row;
        position: relative;
    }

    .checkmark {
        display: block;
        height: 25px;
        width: 25px;
        background-color: #fff;
        margin-bottom: 0;
    }

    .checkmark:after,
    .checkmark:before {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    input:checked ~ .checkmark:after {
        display: block;
    }

    input:indeterminate ~ .checkmark:before {
        display: block;
    }

    /* Style the checkmark/indicator */
    .checkmark:after {
        left: 10px;
        top: 7px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .checkmark:before {
        left: 50%;
        top: 50%;
        width: 15px;
        height: 1px;
        border: solid white;
        border-width: 0 0 3px 0;
        -webkit-transform: translateX(-50%) translateY(50%);
        -ms-transform: translateX(-50%) translateY(50%);
        transform: translateX(-50%) translateY(50%);
    }

    .checkmark.default:after,
    .checkmark.default:before {
        border-color: #000;
    }

    .cpl-check {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    .cpl-check ~ .checkmark {
        border: 3px solid #000;
    }

    .cpl-check:checked ~ .checkmark {
        background-color: #fff;
    }

    #custom_tooltip<?= $blockKey ?> {
        display: none;
        position: fixed;
        border: 1px solid #2c2c2c;
        background-color: #2c2c2c;
        padding: 5px 15px;
        border-radius: 10px;
        color: white;
        font-size: 1.2rem;
        font-weight: bold;
        z-index: 11;
    }

    #custom_tooltip<?= "$blockKey " ?>.show {
        display: block;
    }

    .modal {
        z-index: 999999;
        font-family: Raleway, Fallback, sans-serif;
    }

    /* vertical timeline */

    #vertical_timeline {
        flex: 1;
    }

    #detail_contribution_indicator {
        color: #ffc900;
        display: block;
        text-align: center;
        gap: 5px;
    }

    .new_task_form_container {
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 10px;
    }

    .new_task_form_container > input {
        border: none;
        outline: none;
        flex: 1;
        font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
        font-weight: normal;
    }

    .new_task_form_container > button {
        border: none;
        outline: none;
        background: transparent;
    }

    .vertical_timeline_content .title {
        text-transform: inherit !important;
        font-weight: initial !important;
        font-size: medium !important;
        line-height: 1.9rem !important;
    }

    #generic_agenda<?= $blockKey ?> {
        margin: 20px auto;
        padding-left: 30px;
        padding-right: 30px;
        position: relative;
    }

    #generic_agenda<?= "$blockKey " ?>.nav-tabs > li.active > a:focus {
        color: #555;
        cursor: default;
        background-color: #fff;
        border: 1px solid #ddd;
        border-bottom-color: transparent;
    }

    #generic_agenda<?= "$blockKey " ?>.tab-content {
        border-left: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
        border-right: 1px solid #ddd;
        border-radius: 0 0 3px 3px;
        padding: 10px;
    }

    #horizontal_timeline_container<?= $blockKey ?> {
        width: 100%;
        height: 600px;
    }

    .agenda-viewer {
        display: flex;
        flex-direction: row;
        gap: 20px;
        align-items: flex-start;
    }

    .agenda-viewer > .filter {
        min-width: 300px;
        width: 300px;
    }

    .agenda-viewer > .content {
        width: 100%;
    }

    #gantt_container<?= $blockKey ?> {
        min-height: 600px;
    }

    .fc-scroller.fc-day-grid-container {
        height: auto !important;
    }

    #filter-xs<?= $blockKey ?> {
        display: none;
        position: absolute;
        z-index: 50;
    }

    #cpl_container<?= $blockKey ?> {
        background-color: white;
    }

    #cpl_container<?= "$blockKey " ?> .card-header {
        display: flex;
        align-items: center;
        gap: 10px;
    }

    #cpl_container<?= "$blockKey " ?> .card-header .card-title {
        flex: 1;
    }

    #cpl_container<?= "$blockKey " ?> .card-header .card-action {
        display: none;
    }

    #card-overlay-modal<?= $blockKey ?> {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100vw;
        height: 100vh;
        z-index: 999990;
        background-color: rgba(0, 0, 0, .5);
    }

    @media screen and (max-width: 1024px) {
        #cpl_container<?= $blockKey ?> {
            display: none;
            overflow-y: auto;
            z-index: 999990;
        }

        #cpl_container<?= "$blockKey " ?> .card-header .card-action {
            display: block;
        }

        #filter-xs<?= $blockKey ?> {
            display: block;
        }

        .agenda-viewer > .content {
            margin-top: 50px;
        }
    }

    .source-management {
        list-style: none;
        border-bottom: 1px solid #cfcfcf;
    }

    .source-management-item {
        padding: 5px 0;
    }

    .source-management-item:not(.source-management-item:last-child) {
        border-bottom: 1px solid #cfcfcf;
    }

    .source-management-item a:hover {
        text-decoration: none;
    }
</style>
<div id="action-preview<?= $blockKey ?>"></div>
<div class="rin-modal rin-closeable" id="event-detail">
    <div class="rin-modal-dialog">
        <div class="rin-modal-body">
            <div style="display: flex; flex-direction: row; align-items: flex-start; margin: 5px 5px 0; gap: 5px;">
                <span style="width: 30px; height: 30px;border-radius: 3px;" id="event-color<?= $blockKey ?>"></span>
                <span style="flex: 1;">
                    <span style="font-size: 30px;line-height: 1;" id="event-name<?= $blockKey ?>"></span><br>
                    <span id="event-date<?= $blockKey ?>"></span>
                </span>
            </div>
            <div style="display: flex; flex-direction: row; align-items: flex-start; margin: 5px 5px 0; gap: 5px;">
                <span style="font-size: 30px;" class="fa fa-calendar"></span>
                <span style="flex: 1;">
                    <span style="font-size: 20px;" id="event-group<?= $blockKey ?>"></span><br>
                    <span id="event-author<?= $blockKey ?>"></span>
                </span>
            </div>
            <!-- Mahefa -->
            <!-- <a class="btn btn-default btn-link followBtn active" data-id="<?= $blockKey ?>" data-type=""> Participer </a> -->
            <div class="text-center all-btn-content">
                <?php
                    if (in_array('participateBtn', $cms_data["show"])) { ?>
                        <a href="javascript:;" class="btn btn-default btn-link participateUser" data-toggle="tooltip" data-placement="left"
                           data-original-title="Participer" data-ownerlink="participate" data-id="64506e2c833ddd4564099c24" data-type="event"
                           data-name="test-event"><i class="fa fa-link fa-rotate-270"></i> Participer</a>
                        <?php
                    }
                    if (in_array('allParticipantList', $cms_data["show"])) { ?>
                        <a class="btn btn-default btn-link attendeeList"><i class="fa fa-list"></i> Liste des participants </a>
                        <?php
                    } ?>
            </div>
        </div>
    </div>
</div>
<div id="generic_agenda<?= $blockKey ?>"
     class="<?= filter_var($cms_data['width'], FILTER_VALIDATE_BOOLEAN) === true ? 'container-fluid' : 'container' ?>">
    <div id="filter-xs<?= $blockKey ?>">
        <button type="button" class="btn btn-default">
            <span class="fa fa-filter"></span>
        </button>
    </div>
    <?php if ($show_filter): ?>
    <div class="agenda-viewer">
        <div id="card-overlay-modal<?= $blockKey ?>"></div>
        <div id="cpl_container<?= $blockKey ?>" class="card filter">
            <div class="card-header">
                <span class="card-title">
                <?php
                    switch ($cms_data['content_type']) {
                        case 'events':
                            echo "Types d'événements";
                            break;
                        case 'projects':
                            echo 'Liste des projets';
                            break;
                    }
                ?>
                </span>
                <?php if ($cms_data['allow_external_sources']): ?>
                    <button class="btn btn-default btn-sm" data-target="event_sources">
                        <span class="fa fa-plus"></span>
                    </button>
                <?php endif ?>
                <div class="card-action">
                    <div class="fa fa-times"></div>
                </div>
            </div>
            <?php
                if ($cms_data['allow_external_sources']): ?>
                    <ul class="card-body source-management" style="display: none">
                        <li class="source-management-item"><a href="#openagenda"><span class="fa fa-calendar" aria-hidden="true"></span>
                                OpenAgenda</a></li>
                        <li class="source-management-item"><a href="#icalendar"><span class="fa fa-calendar" aria-hidden="true"></span> iCalendar</a>
                        </li>
                    </ul>
                <?php
                endif; ?>
            <div class="card-body co-scroll" id="cpl<?= $blockKey ?>">
                <div id="cpl-search<?= $blockKey ?>">
                    <i class="fa fa-search"></i>
                    <input type="search" placeholder="Rechercher">
                </div>
                <div id="cpl-select<?= $blockKey ?>">
                    <div class="cpl-actions">
                        <input class="cpl-check" type="checkbox" id="cpl_filter_select_all<?= $blockKey ?>" checked>
                        <label for="cpl_filter_select_all<?= $blockKey ?>" class="checkmark default"></label>
                    </div>
                    <label for="cpl_filter_select_all<?= $blockKey ?>" class="cpl-text">Sélectionner tout</label>
                </div>
            </div>
        </div>
        <div class="content">
            <?php endif ?>
            <ul class="nav nav-tabs" role="tablist" id="agenda_view_tabs<?= $blockKey ?>">
                <?php
                    foreach ($cms_data['available_views'] as $view): ?>
                        <?php if ($cms_data['content_type'] === 'events' && $view !== 'gantt' || $cms_data['content_type'] === 'projects'): ?>
                            <li><a href="#<?= $view ?><?= $blockKey ?>" role="tab"><?= $view_table[$view] ?></a></li>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php if ($is_admin && !$readonly_content): ?>
                    <li><a href="#setting<?= $blockKey ?>" role="tab"><i class="fa fa-cog"></i> Paramètre</a></li>
                <?php endif ?>
            </ul>

            <div class="tab-content">
                <?php foreach ($cms_data['available_views'] as $view): ?>
                    <?php if ($cms_data['content_type'] === 'events' && $view !== 'gantt' || $cms_data['content_type'] === 'projects'): ?>
                        <div class="tab-pane load" id="<?= $view ?><?= $blockKey ?>">
                            <div id="<?= $view ?>_container<?= $blockKey ?>"></div>
                            <?php if ($view === 'gantt'): ?>
                                <small style="display: inline-block; margin-top: 5px;">Si vous avez du mal à naviguer horizontalement essayez
                                    la combinaison
                                    <button class="btn btn-primary">Shift</button>
                                    puis défiler avec la molette de votre souris.
                                </small>
                            <?php endif ?>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
                <?php if ($is_admin && !$readonly_content): ?>
                    <div class="tab-pane" id="setting<?= $blockKey ?>">
                        <form role="form">
                            <div class="form-group">
                                <label>Largeur du bloc</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="width" id="width_true<?= $blockKey ?>"
                                               value="true" <?= $cms_data['width'] === true ? 'checked' : '' ?>>
                                        Large
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="width" id="width_false<?= $blockKey ?>"
                                               value="false" <?= $cms_data['width'] === false ? 'checked' : '' ?>>
                                        Retrécie
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Type de contenu</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="content_type" data-conditional-target="#source-manager-container"
                                               id="content_type_events<?= $blockKey ?>"
                                               value="events" <?= $cms_data['content_type'] === 'events' ? 'checked' : '' ?>>
                                        Evénement
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="content_type" data-conditional-target="#source-manager-container"
                                               id="content_type_projects<?= $blockKey ?>"
                                               value="projects" <?= $cms_data['content_type'] === 'projects' ? 'checked' : '' ?>>
                                        Projet
                                    </label>
                                </div>
                                <div class="checkbox" <?= $cms_data['content_type'] === 'projects' ? 'style="display: none;"' : '' ?>
                                     id="source-manager-container" data-show-condition="events">
                                    <label>
                                        <input type="checkbox" name="allow_external_sources"
                                               id="allow_external_sources<?= $blockKey ?>" <?= $cms_data['allow_external_sources'] ? 'checked' : '' ?>>
                                        Prendre en charge les sources extérieurs
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Liste des vues</label>
                                <?php
                                    foreach ($view_table as $key => $view) { ?>
                                        <div class="checkbox" <?= $key === 'gantt' && $cms_data['content_type'] === 'events' ? 'style="display: none;"' : 'style' ?>>
                                            <label>
                                                <input type="checkbox" name="available_views" id="available_views_<?= $key ?><?= $blockKey ?>"
                                                       value="<?= $key ?>" <?= !empty($cms_data['available_views']) && in_array($key, $cms_data['available_views']) ? ($key !== 'gantt' || $cms_data['content_type'] !== 'events' ? 'checked' : '') : '' ?>>
                                                <?= $view ?>
                                            </label>
                                        </div>
                                        <?php
                                    } ?>
                            </div>
                            <div class="form-group">
                                <label>Afficher</label>
                                <?php
                                    foreach ($show_el as $key => $view) { ?>
                                        <div class="checkbox" <?= $key === 'gantt' && $cms_data['content_type'] === 'events' ? 'style="display: none;"' : 'style' ?>>
                                            <label>
                                                <input type="checkbox" name="show" id="show_<?= $key ?><?= $blockKey ?>"
                                                       value="<?= $key ?>" <?= !empty($cms_data['show']) && in_array($key, $cms_data['show']) ? ($key !== 'gantt' || $cms_data['content_type'] !== 'events' ? 'checked' : '') : '' ?>>
                                                <?= $view ?>
                                            </label>
                                        </div>
                                        <?php
                                    } ?>
                            </div>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                        </form>
                    </div>
                <?php endif ?>
            </div>
            <?php if ($show_filter): ?>
        </div>
    </div>
<?php endif ?>
</div>
<div id="custom_tooltip<?= $blockKey ?>"></div>
<script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
<script>
    (function (gantt_component, $, W) {
        /**
         * @typedef {Object} FilterView
         * @property {string} color Code hexadécimal de la couleur associée
         * @property {string} [name] Nom à afficher ; Si c'est un event, c'est la traduction du type d'event
         * @property {string} [info] Information à affichier au survol du filtre
         * @property {number} count Nombre d'élément contenu dans la catégorie
         * @property {string} value Si c'est un projet: identifian du projet
         */
        /**
         * @typedef {Object} Task
         * @property {boolean} checked Status de la sous-tâche si elle est terminée ou pas
         * @property {string} color Code hexadécimal de la couleur associée
         * @property {string} endDate Date de fin
         * @property {string} id Identifiant de la sous-tâche
         * @property {string} name Titre ou nom
         * @property {string} startDate Date de début
         */
        /**
         * @typedef {Object} Action
         * @property {string} id Identifiant de l'action
         * @property {string} color Code hexadécimal de la couleur associée
         * @property {string} description Description longue (complète)
         * @property {string} endDate Date de fin
         * @property {string} group Nom du projet qui la porte
         * @property {string} name Titre ou nom
         * @property {string} profilImageUrl Image de présentation
         * @property {string} shortDescription Une description courte
         * @property {string} slug Slug
         * @property {string} startDate Date de début
         * @property {Task[]} subs Liste des sous-tâches
         */
        /**
         * @typedef {Object} ViewElement
         * @property {string} id Identifiant
         * @property {string} name Titre ou nom de l'élément
         * @property {string} color Code hexadécimal de la couleur associée
         * @property {string} description Description longue (complète)
         * @property {string} endDate Date de fin
         * @property {string} profilImageUrl Image de présentation
         * @property {string} shortDescription Une description courte
         * @property {string} slug Slug
         * @property {string} startDate Date de début
         * @property {number} original_created Timestamp de la date de création
         * @property {Action[]} [subs] Si c'est un projet il contient toutes les actions
         * @property {string} [group] Si c'est un event: le group est le nom traduit du type d'évènement
         * @property {string} [type] Si c'est un event: c'est la représentation du type de l'event depuis la base de données (pas traduit)
         */

        let search_last_value = '';
        gantt_component.plugins({
            tooltip: true
        });
        gantt_component.config.date_format = "%Y-%m-%d %H:%i";
        gantt_component.templates.tooltip_text = function (start, end, task) {
            return `<b>Tache:</b> ${task['text']}<br /><b>Durée:</b> ${task['duration']} ${task['duration'] === 1 ? 'Jour' : 'Jours'}<br><b>Progression : </b> ${Math.floor(task['progress'] * 100)}%`;
        };
        const _costum_is_aap = typeof costum !== 'undefined' && typeof costum.type !== 'undefined' && costum.type === 'aap';
        const storage_version = 'v1';
        const cms_data = JSON.parse('<?= json_encode($cms_data) ?>');
        const view_loadings = {};
        view_loadings['calendar<?= $blockKey ?>'] = function (__params) {
            const calendar = '#calendar_container<?= $blockKey ?>';

            if (!$(calendar).parents('.tab-pane').hasClass('load')) return;
            $(calendar).parents('.tab-pane').removeClass('load');

            let check_filters;
            let params;
            __params = __params ? __params : {reload: false};
            __params.reload = typeof __params.reload !== 'undefined' ? __params.reload : false;
            if ($(calendar).length > 0) {
                const search_field_changed = $('#cpl-search<?= $blockKey ?> input[type=search]').val() !== search_last_value;
                if (search_field_changed || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) {
                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                    $('#cpl<?= $blockKey ?>').append(show_loader());
                }
                if (__params.reload) {
                    $(calendar).fullCalendar('destroy');
                    $(calendar).html(show_loader());
                }
                // dans un costum classic et non dans un appel à projet ou seulement events
                if (cms_data['content_type'] === 'events') {
                    params = {
                        filter: {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        }
                    };
                    check_filters = load_storage();
                    for (const key in check_filters) {
                        if (!check_filters[key]) {
                            params.filter.exclude.push(key);
                        }
                    }

                    get_events(params).then(function (__events) {
                        if (__params.reload) $(calendar).empty().html('');
                        const groups = __events.groups.map(function (__group) {
                            __group.name = tradCategory[__group.value];
                            return __group;
                        });
                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) load_filter_view(groups);
                        load_calendar(__events.data, 'event');
                    })
                } else if (cms_data['content_type'] === 'projects') {
                    params = {
                        reorder: 1
                    };

                    if (costum.contextType === 'organizations') {
                        params['request'] = 'all';
                        params['filter'] = {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        };
                        check_filters = load_storage();

                        if (typeof costum.type !== 'undefined' && costum.type === 'aap') {
                            get_projects_ids().then(function (__ids) {
                                for (const key in check_filters) {
                                    if (!check_filters[key] && __ids.includes(key)) {
                                        params.filter.exclude.push(key);
                                    }
                                }
                                params['filter']['ids'] = __ids;

                                if (__ids.length) {
                                    get_actions(params).then(function (__actions) {
                                        if (__params.reload) $(calendar).empty();
                                        let data = [];

                                        for (const project of __actions.data) {
                                            data = data.concat(project.subs);
                                        }

                                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) load_filter_view(__actions.groups);
                                        load_calendar(data, 'action');
                                    });
                                } else {
                                    if (__params.reload) $(calendar).empty();
                                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                                    load_calendar([], 'action');
                                }
                            });
                        } else {
                            for (const key in check_filters) {
                                if (!check_filters[key]) {
                                    params.filter.exclude.push(key);
                                }
                            }
                            get_actions(params).then(function (__actions) {
                                if (__params.reload) $(calendar).empty();
                                let data = [];

                                for (const project of __actions.data) {
                                    data = data.concat(project.subs);
                                }

                                if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) load_filter_view(__actions.groups);
                                load_calendar(data, 'action');
                            });
                        }
                    } else {
                        get_actions(params).then(function (__actions) {
                            if (__params.reload) $(calendar).empty().html('');
                            load_calendar(__actions, 'action');
                        })
                    }
                }
            }
        };
        view_loadings['vertical_timeline<?= $blockKey ?>'] = function () {
            const timeline = '#vertical_timeline_container<?= $blockKey ?>';

            if (!$(timeline).parents('.tab-pane').hasClass('load')) return;
            $(timeline).parents('.tab-pane').removeClass('load');

            if ($(timeline).length > 0) {
                const search_field_changed = $('#cpl-search<?= $blockKey ?> input[type=search]').val() !== search_last_value;
                if (search_field_changed) {
                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                    $('#cpl<?= $blockKey ?>').append(show_loader());
                }
                // dans un costum classic et non dans un appel à projet ou seulement events
                if (cms_data['content_type'] === 'events') {
                    const params = {
                        reorder: 1,
                        filter : {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        }
                    };
                    const check_filters = load_storage();
                    for (const key in check_filters) {
                        if (!check_filters[key]) {
                            params.filter.exclude.push(key);
                        }
                    }

                    get_events(params).then(function (__events) {
                        const groups = __events.groups.map(function (__group) {
                            __group.name = tradCategory[__group.value];
                            return __group;
                        });

                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(groups);
                        load_vertical_timeline(__events.data, 'event');
                    })
                } else if (cms_data['content_type'] === 'projects') {
                    const params = {
                        reorder: 1
                    };

                    if (costum.contextType === 'organizations') {
                        params['request'] = 'all';
                        params['filter'] = {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        };
                        const check_filters = load_storage();

                        if (typeof costum.type !== 'undefined' && costum.type === 'aap') {
                            get_projects_ids().then(function (__ids) {
                                for (let key in check_filters) {
                                    if (!check_filters[key] && __ids.includes(key)) {
                                        params.filter.exclude.push(key);
                                    }
                                }
                                params['filter']['ids'] = __ids;

                                if (__ids.length) {
                                    get_actions(params).then(function (__actions) {
                                        let data = [];
                                        for (let project of __actions.data) {
                                            data = data.concat(project.subs);
                                        }

                                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(__actions.groups);
                                        load_vertical_timeline(data, 'action');
                                    });
                                } else {
                                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                                    load_vertical_timeline([], 'action');
                                }
                            });
                        } else {
                            for (const key in check_filters) {
                                if (!check_filters[key]) {
                                    params.filter.exclude.push(key);
                                }
                            }
                            get_actions(params).then(function (__actions) {
                                let data = [];
                                for (const project of __actions.data) {
                                    data = data.concat(project.subs);
                                }

                                if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(__actions.groups);
                                load_vertical_timeline(data, 'action');
                            });
                        }
                    } else {
                        get_actions(params).then(function (__events) {
                            load_vertical_timeline(__events, 'action');
                        })
                    }
                }
            }
        };
        view_loadings['horizontal_timeline<?= $blockKey ?>'] = function () {
            const timeline = '#horizontal_timeline_container<?= $blockKey ?>';

            if (!$(timeline).parents('.tab-pane').hasClass('load')) return;
            $(timeline).parents('.tab-pane').removeClass('load');

            if ($(timeline).length > 0) {
                const search_field_changed = $('#cpl-search<?= $blockKey ?> input[type=search]').val() !== search_last_value;
                if (search_field_changed) {
                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                    $('#cpl<?= $blockKey ?>').append(show_loader());
                }
                $(timeline).html(show_loader());
                // dans un costum classic et non dans un appel à projet ou seulement events
                if (cms_data['content_type'] === 'events') {
                    var params = {
                        reorder: 1,
                        filter : {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        }
                    };
                    const check_filters = load_storage();
                    for (const key in check_filters) {
                        if (!check_filters[key]) {
                            params.filter.exclude.push(key);
                        }
                    }

                    get_events(params).then(function (__events) {
                        $(timeline).empty();
                        const groups = __events.groups.map(function (__group) {
                            __group.name = tradCategory[__group.value];
                            return __group;
                        });
                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(groups);
                        load_horizontal_timeline(__events.data);
                    })
                } else if (cms_data['content_type'] === 'projects') {
                    const params = {
                        reorder: 1
                    };
                    if (costum.contextType === 'organizations') {
                        params['request'] = 'all';
                        params['filter'] = {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        };
                        const check_filters = load_storage();

                        if (typeof costum.type !== 'undefined' && costum.type === 'aap') {
                            get_projects_ids().then(function (__ids) {
                                for (var key in check_filters) {
                                    if (!check_filters[key] && __ids.includes(key)) {
                                        params.filter.exclude.push(key);
                                    }
                                }
                                params['filter']['ids'] = __ids;

                                if (__ids.length) {
                                    get_actions(params).then(function (__actions) {
                                        $(timeline).empty();
                                        let data = [];
                                        for (const project of __actions.data) {
                                            data = data.concat(project.subs);
                                        }
                                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(__actions.groups);
                                        load_horizontal_timeline(data);
                                    });
                                } else {
                                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                                    $(timeline).empty();
                                    load_horizontal_timeline([]);
                                }
                            });
                        } else {
                            for (const key in check_filters) {
                                if (!check_filters[key]) {
                                    params.filter.exclude.push(key);
                                }
                            }
                            get_actions(params).then(function (__actions) {
                                $(timeline).empty();
                                let data = [];
                                for (const project of __actions.data) {
                                    data = data.concat(project.subs);
                                }
                                if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(__actions.groups);
                                load_horizontal_timeline(data);
                            });
                        }
                    } else {
                        get_actions(params).then(function (__actions) {
                            $(timeline).empty().html('');
                            load_horizontal_timeline(__actions);
                        })
                    }
                }
            }
        };
        view_loadings['gantt<?= $blockKey ?>'] = function (__params) {
            __params = __params ? __params : {reload: false}
            const gantt = '#gantt_container<?= $blockKey ?>';

            if (!$(gantt).parents('.tab-pane').hasClass('load')) return;
            $(gantt).parents('.tab-pane').removeClass('load');

            if ($(gantt).length > 0) {
                const search_field_changed = $('#cpl-search<?= $blockKey ?> input[type=search]').val() !== search_last_value;
                if (search_field_changed) {
                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                    $('#cpl<?= $blockKey ?>').append(show_loader());
                }
                if (__params.reload) $(gantt).empty().html(show_loader());
                if (cms_data['content_type'] === 'projects') {
                    var params = {
                        reorder: 1,
                        request: 'actions_tasks'
                    };
                    if (costum.contextType === 'organizations') {
                        params['request'] = 'all';
                        params['filter'] = {
                            text   : $('#cpl-search<?= $blockKey ?> input').val(),
                            exclude: []
                        };
                        const check_filters = load_storage();

                        // Pour les appels à projets
                        if (typeof costum.type !== 'undefined' && costum.type === 'aap') {
                            get_projects_ids().then(function (__ids) {
                                for (const key in check_filters) {
                                    if (!check_filters[key] && __ids.includes(key)) {
                                        params.filter.exclude.push(key);
                                    }
                                }
                                params['filter']['ids'] = __ids;

                                if (__ids.length) {
                                    get_actions(params).then(function (__actions) {
                                        if (__params.reload) $(gantt).empty().html('');
                                        if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(__actions.groups);
                                        load_gantt(__actions.data);
                                    });
                                } else {
                                    $('#cpl<?= $blockKey ?>').find('.cpl').remove();
                                    if (__params.reload) $(gantt).empty().html('');
                                    load_gantt([]);
                                }
                            });
                        } else {
                            for (const key in check_filters) {
                                if (!check_filters[key]) {
                                    params.filter.exclude.push(key);
                                }
                            }
                            get_actions(params).then(function (__actions) {
                                if (__params.reload) $(gantt).empty().html('');
                                if (search_field_changed || $('#cpl<?= $blockKey ?> .cpl').length === 0) load_filter_view(__actions.groups);
                                load_gantt(__actions.data);
                            });
                        }
                    } else {
                        if (__params.reload) $(gantt).empty().html('');
                        get_actions(params).then(load_gantt);
                    }
                }
            }
        };
        const _openagendas = JSON.parse(JSON.stringify(<?= json_encode($openagendas) ?>));
        const _icalendars = JSON.parse(JSON.stringify(<?= json_encode($icalendars) ?>));
        const _forms = {
            openagenda: {
                jsonSchema: {
                    title      : 'Open Agenda',
                    description: 'une description',
                    icon       : 'fa-question',
                    properties : {
                        api_key        : {
                            inputType: 'text',
                            label    : 'Clef d\'API',
                            value    : JSON.parse(JSON.stringify(<?= json_encode($context['links']['agendas']['openagenda']['api_key'] ?? '') ?>)),
                            rules    : {
                                required: true
                            }
                        },
                        public_agenda  : {
                            inputType: 'custom',
                            html     : '<label class="col-xs-12 text-left control-label no-padding"><i class="fa fa-chevron-circle-right"></i> Autre agenda public</label>'
                        },
                        openagenda_urls: {
                            label    : 'Slugs ou urls',
                            inputType: 'array',
                            value    : JSON.parse(JSON.stringify(<?= json_encode($context['links']['agendas']['openagenda']['urls'] ?? []) ?>))
                        }
                    },
                    save       : function (__form_value) {
                        delete __form_value['collection'];
                        delete __form_value['scope'];
                        var agenda = {
                            api_key: __form_value['api_key'],
                            urls   : getArray('.openagenda_urlsarray')
                        }
                        var parameters = {
                            id        : costum.contextId,
                            collection: costum.contextType,
                            path      : 'links.agendas.openagenda',
                            value     : agenda

                        };
                        dataHelper.path2Value(parameters, function (__response) {
                            if (__response['result']) document.location.reload();
                        })
                    }
                }
            },
            icalendar : {
                jsonSchema: {
                    title      : 'iCalendar',
                    description: 'une description',
                    icon       : 'fa-question',
                    properties : {
                        ical: {
                            inputType: 'formArray',
                            template : `
                                <div class="form-group">
                                    <label>Nom du calendrier</label>
                                    <input type="text" class="form-control" data-form-control="name" placeholder="Calendar">
                                </div>
                                <div class="form-group">
                                    <label>Lien</label>
                                    <input type="text" class="form-control" data-form-control="url" placeholder="https://domaine.net/fichier.ics">
                                </div>
                            `,
                            mapping  : {
                                name: 'text',
                                url : 'text'
                            },
                            value    : _icalendars
                        }
                    },
                    save       : function (__form_value) {
                        delete __form_value['collection'];
                        delete __form_value['scope'];

                        var parameters = {
                            id        : costum.contextId,
                            collection: costum.contextType,
                            path      : 'links.agendas.icalendars',
                            value     : __form_value.ical
                        };
                        dataHelper.path2Value(parameters, function () {
                            document.location.reload();
                        })
                    }
                }
            }
        };
        const _default_filters = JSON.parse(JSON.stringify(<?= json_encode($default_filter_keys ?? []) ?>));
        const _color = JSON.parse(JSON.stringify(<?= json_encode($color ?? null) ?>));

        function show_loader() {
            return `<div class="view_loader" style="width: 100%; text-align: center"><img src="${baseUrl}/plugins/lightbox2/img/loading.gif" alt="loader" style="width: 32px; height: 32px;" /></div>`;
        }

        /**
         * Récupérer tous les identifiants des projets
         * relatifs au formulaire
         * @return {Promise.<string[]>}
         */
        function get_projects_ids() {
            var url = baseUrl + '/costum/project/project/request/aap_project_ids';
            var post = {
                costumSlug: costum.slug,
                costumType: costum.contextType,
                costumId  : costum.contextId,
                form      : typeof aapObj !== 'undefined' ? aapObj.form._id.$id : aapObject.formParent._id.$id
            };
            return new Promise(function (__resolve) {
                if (_default_filters.length) __resolve(_default_filters);
                else ajaxPost(null, url, post, __resolve);
            });
        }

        /**
         * @param {Event} __group Element
         * @param {Object} __option Options
         * @param {bool} __option.checked L'élément est-il sélectionné ou pas
         * @param {bool} __option.is_external C'est un élément externe ou pas
         * @return {HTMLDivElement}
         */
        function cpl_template(__group, __option = {checked: true, is_external: false}) {
            const cpl_dom = document.createElement('div'),
                div_actions_dom = document.createElement('div'),
                cpl_check_dom = document.createElement('input'),
                checkmark_dom = document.createElement('label'),
                cpl_text_dom = document.createElement('label'),
                counter_dom = document.createElement('span');

            cpl_check_dom.classList.add('cpl-check');
            cpl_check_dom.setAttribute('id', `${__group['value']}<?= $blockKey ?>`);
            cpl_check_dom.setAttribute('type', 'checkbox');
            cpl_check_dom.value = __group['value'];
            cpl_check_dom.checked = __option.checked;

            checkmark_dom.classList.add('checkmark');
            checkmark_dom.style.border = `3px solid ${(_color ? _color['foreground'] : __group['color'])}`;
            checkmark_dom.style.backgroundColor = __option.checked ? (_color ? _color['background'] : __group['color']) : '#fff';
            checkmark_dom.setAttribute('for', `${__group['value']}<?= $blockKey ?>`);

            div_actions_dom.classList.add('cpl-actions');

            cpl_text_dom.classList.add('cpl-text');
            cpl_text_dom.setAttribute('for', `${__group['value']}<?= $blockKey ?>`);
            cpl_text_dom.textContent = __group['name'];
            if (notEmpty(__group.info)) {
                cpl_text_dom.classList.add('tooltips');
                cpl_text_dom.dataset.toggle = 'tooltip';
                cpl_text_dom.dataset.placement = 'bottom';
                cpl_text_dom.dataset.originalTitle = __group.info;
            }

            counter_dom.classList.add('badge');
            counter_dom.textContent = __group['count'];

            cpl_dom.classList.add('cpl');
            if (__option.is_external) cpl_dom.classList.add('external-filter');

            div_actions_dom.appendChild(cpl_check_dom);
            div_actions_dom.appendChild(checkmark_dom);
            cpl_dom.appendChild(div_actions_dom);
            cpl_dom.appendChild(cpl_text_dom);
            cpl_dom.appendChild(counter_dom);

            $(cpl_check_dom).on('change', function ($e) {
                const self = $(this);
                const select_all = document.getElementById('cpl_filter_select_all<?= $blockKey ?>');
                select_all.checked = $e.target.checked;
                select_all.indeterminate = false;
                $('#cpl<?= $blockKey ?>').find('.cpl-check').each(function (i, element) {
                    if (element.checked !== $e.target.checked) {
                        select_all.indeterminate = true;
                        select_all.checked = false;
                    }
                });
                store_preference($e.target.value, $e.target.checked);
                $('#agenda_view_tabs<?= $blockKey ?>+.tab-content>.tab-pane').each(function () {
                    $(this).addClass('load');
                });
                reload_current_view();
            });

            return cpl_dom;
        }

        function store_preference($key, $value) {
            var user_connected = userConnected && userConnected._id && userConnected._id.$id ? userConnected._id.$id : '';
            var key = `${user_connected}${storage_version}<?= $blockKey ?>${cms_data.content_type}filter`;
            var data = load_storage();
            data[$key] = $value;
            if (user_connected) localStorage.setItem(key, JSON.stringify(data));
            else sessionStorage.setItem(key, JSON.stringify(data));
        }

        function load_storage() {
            var user_connected = userConnected && userConnected._id && userConnected._id.$id ? userConnected._id.$id : '';
            var key = `${user_connected}${storage_version}<?= $blockKey ?>${cms_data.content_type}filter`;
            var data = user_connected ? localStorage.getItem(key) : sessionStorage.getItem(key);
            if (data === null) data = {};
            else data = JSON.parse(data);
            return data;
        }

        /**
         * Vue sur le filtre de l'agenda
         * @param {FilterView[]} __groups Elements à afficher
         */
        function load_filter_view(__groups = []) {
            $('#cpl<?= $blockKey ?>').find('.view_loader').remove();
            const storage = load_storage();
            const select_all = document.getElementById('cpl_filter_select_all<?= $blockKey ?>');
            const filtered_openagendas = _openagendas.filter(filter_external);
            const filtered_icalendars = _icalendars.filter(filter_external);

            if (select_all !== null) {
                select_all.checked = true;
                select_all.indeterminate = false;
            }

            if (filtered_openagendas.length > 0 && select_all !== null) {
                select_all.checked = typeof storage[filtered_openagendas[0]['value']] !== 'undefined' ? storage[filtered_openagendas[0]['value']] : true;
            } else if (filtered_icalendars.length > 0 && select_all !== null) {
                select_all.checked = typeof storage[filtered_icalendars[0]['value']] !== 'undefined' ? storage[filtered_icalendars[0]['value']] : true;
            } else if (__groups.length > 0 && select_all !== null) {
                select_all.checked = typeof storage[__groups[0]['value']] !== 'undefined' ? storage[__groups[0]['value']] : true;
            }

            for (let i = 0; i < filtered_openagendas.length && select_all !== null; i++) {
                const openagenda = filtered_openagendas[i];
                const checked = typeof storage[openagenda['value']] !== 'undefined' ? storage[openagenda['value']] : true;
                if (checked !== select_all.checked) {
                    select_all.indeterminate = true;
                    select_all.checked = false;
                }
                $('#cpl<?= $blockKey ?>').append(cpl_template(openagenda, {checked, is_external: true}));
            }

            for (let i = 0; i < filtered_icalendars.length && select_all !== null; i++) {
                const icalendar = filtered_icalendars[i];
                const checked = typeof storage[icalendar['value']] !== 'undefined' ? storage[icalendar['value']] : true;
                if (checked !== select_all.checked) {
                    select_all.indeterminate = true;
                    select_all.checked = false;
                }
                $('#cpl<?= $blockKey ?>').append(cpl_template(icalendar, {checked, is_external: true}));
            }

            for (let i = 0; i < __groups.length && select_all !== null; i++) {
                const group = __groups[i];
                const checked = typeof storage[group['value']] !== 'undefined' ? storage[group['value']] : true;
                if (checked !== select_all.checked) {
                    select_all.indeterminate = true;
                    select_all.checked = false;
                }
                $('#cpl<?= $blockKey ?>').append(cpl_template(group, {checked}));
            }

            coInterface.bindTooltips();
        }

        /**
         * Formatter les liens dans le text pour qu'ils soient cliquables
         * @param {string} __text Texte d'entrée
         * @returns {string}
         */
        function enable_links(__text) {
            const regex = new RegExp(/(https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_+.~#?&/=]*))|([-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&//=]*))/, 'g');
            return __text.replace(regex, function (__match) {
                return `<a style="color: inherit; text-decoration: underline;" target="_blank" href="${__match}">${__match}</a>`;
            });
        }

        function reload_current_view() {
            var tabs = $('#agenda_view_tabs<?= $blockKey ?>');
            var target = tabs.find('li.active a').attr('href');
            view_loadings[target.substring(1)]();
            search_last_value = $('#cpl-search<?= $blockKey ?> input[type=search]').val();
        }

        /**
         * Filtre pour les agenda externes
         * @param {Object} __input
         * @return {boolean}
         */
        function filter_external(__input) {
            const search_input = $('#cpl-search<?= $blockKey ?> input[type=search]').val();
            if (search_input) {
                const regex = new RegExp(search_input, 'ig');
                return __input.name.match(regex);
            }
            return true;
        }

        function load_vertical_timeline(__events = [], __content_type) {
            let data = [];

            for (const openagenda of _openagendas.filter(filter_external)) {
                if ($(`.cpl-check[value=${openagenda.value}]`).is(':checked')) data = data.concat(openagenda.events);
            }
            for (const icalendar of _icalendars.filter(filter_external)) {
                if ($(`.cpl-check[value=${icalendar.value}]`).is(':checked')) data = data.concat(icalendar.events);
            }
            data = data.concat(__events);
            data = data.map(function (__event) {
                return {
                    id               : __event.id,
                    title            : __event.name,
                    start            : __event.startDate,
                    end              : __event.endDate,
                    short_description: __event.shortDescription,
                    full_description : dataHelper.markdownToHtml(__event.description),
                    backgroundColor  : _color ? _color['background'] : __event.color,
                    textColor        : _color ? _color['foreground'] : invert_color(__event.color),
                    group            : __event['group'],
                    type             : __content_type
                }
            });

            vertical_timeline('#vertical_timeline_container<?= $blockKey ?>', {
                events  : data,
                on_click: element_detail
            });
        }

        function load_horizontal_timeline(__events) {
            const tl_interval = setInterval(function () {
                if (typeof TL !== 'undefined') {
                    clearInterval(tl_interval);
                    let data = [];
                    for (const openagenda of _openagendas.filter(filter_external)) {
                        if ($(`.cpl-check[value=${openagenda.value}]`).is(':checked')) data = data.concat(openagenda.events);
                    }
                    for (const icalendar of _icalendars.filter(filter_external)) {
                        if ($(`.cpl-check[value=${icalendar.value}]`).is(':checked')) data = data.concat(icalendar.events);
                    }
                    data = data.concat(__events);
                    const option = {
                        events: data.map(function (event) {
                            const moment_start = moment(event['startDate']);
                            const moment_end = moment(event['endDate']);
                            const map = {
                                start_date: {
                                    year : moment_start.year(),
                                    month: moment_start.month() + 1,
                                    day  : moment_start.date()
                                },
                                end_date  : {
                                    year : moment_end.year(),
                                    month: moment_end.month() + 1,
                                    day  : moment_end.date()
                                },
                                text      : {
                                    headline: event['name'],
                                    text    : `<blockquote>${event['shortDescription']}</blockquote> ${dataHelper.markdownToHtml(event['description'])}`
                                }
                            };
                            if (_color) {
                                map['background'] = {
                                    color: _color['background']
                                };
                            } else if (typeof event['color'] !== 'undefined') {
                                map['background'] = {
                                    color: event['color']
                                };
                            }
                            if (event['profilImageUrl'] !== '') {
                                map['media'] = {
                                    url      : event['profilImageUrl'],
                                    thumbnail: event['profilImageUrl']
                                }
                            }
                            return map;
                        })
                    };
                    const options = {
                        script_path: `${baseUrl}/plugins/knightlab_timeline3/js/`,
                        language   : 'fr',
                        font       : 'bevan-pontanosans',
                        theme      : 'contrast',
                    }
                    new TL.Timeline('horizontal_timeline_container<?= $blockKey ?>', option, options);
                }
            });
        }

        function element_detail(__element) {
            if (__element.type === 'action') {
                var url = baseUrl + '/costum/project/action/request/action_detail_html';
                var id = __element.id;
                id = id.indexOf('act') >= 0 ? id.substring(3) : id;
                var post = {
                    id
                };
                ajaxPost(null, url, post, function (__html) {
                    $('#action-preview<?= $blockKey ?>').empty().html(__html);
                }, null, 'text');
            } else {
                $('#event-color<?= $blockKey ?>').css('background-color', __element['backgroundColor']);
                $('#event-name<?= $blockKey ?>').text(__element['title']);
                var start = __element['start'];
                var end = __element['end'];
                if (start.format('LL') === end.format('LL')) $('#event-date<?= $blockKey ?>').text(start.format('LL') + ' ' + start.format('HH:mm') + ' - ' + end.format('HH:mm'));
                else $('#event-date<?= $blockKey ?>').text(start.format('LLL') + ' - ' + end.format('LLL'));

                $('#event-group<?= $blockKey ?>').text(__element['group']);
                $('#event-detail').rinModal('show');
            }
        }

        /**
         * Charger la vue calendrier
         * @param {Action[]} [__events=[]] Contenu à insérer
         * @param {string} __content_type Type de contenu (projects/events)
         */
        function load_calendar(__events = [], __content_type) {
            const calendar_node = $('#calendar_container<?= $blockKey ?>');
            let data = [];
            for (const openagenda of _openagendas.filter(filter_external)) {
                if ($(`.cpl-check[value=${openagenda.value}]`).is(':checked')) data = data.concat(openagenda.events);
            }
            for (const icalendar of _icalendars.filter(filter_external)) {
                if ($(`.cpl-check[value=${icalendar.value}]`).is(':checked')) data = data.concat(icalendar.events);
            }
            data = data.concat(__events);

            if (calendar_node.children().length > 0) {
                calendar_node.fullCalendar('removeEvents');
                calendar_node.fullCalendar('addEventSource', data.map(function (event) {
                    const map = {
                        id   : event['id'],
                        title: event['name'],
                        start: moment(event['startDate']).format(),
                        end  : moment(event['endDate']).format(),
                        type : __content_type
                    }
                    if (_color) {
                        map['backgroundColor'] = _color['background'] + ' !important';
                        map['textColor'] = _color['foreground'] + ' !important';
                    } else if (typeof event['color'] !== 'undefined') {
                        map['backgroundColor'] = event['color'] + ' !important';
                        map['textColor'] = invert_color(event['color']) + ' !important';
                    }
                    return map;
                }));
            } else {
                calendar_node.fullCalendar({
                    selectable         : true,
                    events             : data.map(function (event) {
                        var map = {
                            id   : event['id'],
                            title: event['name'],
                            group: event['group'],
                            start: moment(event['startDate']).format(),
                            end  : moment(event['endDate']).format(),
                            type : __content_type
                        }
                        if (_color) {
                            map['backgroundColor'] = _color['background'] + ' !important';
                            map['textColor'] = _color['foreground'] + ' !important';
                        } else if (typeof event['color'] !== 'undefined') {
                            map['backgroundColor'] = event['color'] + ' !important';
                            map['textColor'] = invert_color(event['color']) + ' !important';
                        }
                        return map;
                    }),
                    eventAfterAllRender: function (__view) {
                        check_window_scroll();
                        if (__view.name === 'month') calendar_node.find('.fc-scroller.fc-day-grid-container').css('overflow', 'inherit');
                    },
                    eventClick         : function ($event, $js_event, $view) {
                        $js_event.preventDefault();
                        element_detail($event);
                    },
                    select             : function (__start, __end) {
                        var is_admin = JSON.parse(<?= json_encode($is_admin) ?>);
                        if (is_admin || _costum_is_aap && notEmpty(userConnected)) {
                            if (cms_data['content_type'] === 'events') {
                                var params = {
                                    startDate: __start.format('DD/MM/YYYY HH:mm'),
                                    endDate  : __end.format('DD/MM/YYYY HH:mm')
                                }
                                dyFObj.openForm('event', null, params, null, null, {
                                    type: 'bootbox'
                                });
                            } else {
                                var default_finder = {
                                    [userConnected['_id']['$id']]: {
                                        type: 'citoyens',
                                        name: userConnected['name']
                                    }
                                };
                                dyFObj.openForm({
                                    jsonSchema: {
                                        title      : 'Ajouter une action',
                                        properties : {
                                            generic_agenda_name        : dyFInputs.name('action'),
                                            generic_agenda_parentType  : dyFInputs.inputHidden('projects'),
                                            generic_agenda_parentId    : dyFInputs.inputHidden(''),
                                            generic_agenda_creator     : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                            generic_agenda_credits     : {
                                                inputType: 'text',
                                                label    : 'Crédits',
                                                rules    : {
                                                    number: true
                                                },
                                                value    : '1'
                                            },
                                            generic_agenda_idUserAuthor: dyFInputs.inputHidden(userConnected['_id']['$id']),
                                            generic_agenda_idParentRoom: dyFInputs.inputHidden(''),
                                            generic_agenda_tags        : {
                                                inputType: 'tags',
                                                label    : trad.tags
                                            },
                                            generic_agenda_contributors: {
                                                inputType   : 'finder',
                                                label       : 'Assigné à qui ?',
                                                multiple    : true,
                                                rules       : {
                                                    lengthMin: [1, 'contributors'],
                                                    required : true
                                                },
                                                initType    : ['citoyens'],
                                                values      : default_finder,
                                                initBySearch: true,
                                                initMe      : true,
                                                initContext : false,
                                                initContacts: false,
                                                openSearch  : true
                                            },
                                            generic_agenda_status      : dyFInputs.inputHidden('todo'),
                                            generic_agenda_startDate   : {
                                                label    : 'Date de début',
                                                inputType: 'datetime',
                                                value    : __start.format('DD/MM/YYYY HH:mm'),
                                                rules    : {
                                                    required: true
                                                }
                                            },
                                            generic_agenda_endDate     : {
                                                label    : 'Date de fin',
                                                inputType: 'datetime',
                                                value    : __end.format('DD/MM/YYYY HH:mm'),
                                                rules    : {
                                                    required: true
                                                }
                                            }
                                        },
                                        beforeBuild: function () {
                                            if (typeof costum !== 'undefined' && costum.contextType === 'projects') {
                                                this.properties['generic_agenda_parentId'] = dyFInputs.inputHidden(costum.contextId);
                                            } else {
                                                let filters;
                                                if (_costum_is_aap) {
                                                    filters = {
                                                        '_id': {
                                                            '$in': []
                                                        }
                                                    }
                                                    $('#cpl<?= $blockKey ?> .cpl .cpl-check').each(function () {
                                                        filters._id.$in.push(this.value);
                                                    });
                                                } else {
                                                    filters = {
                                                        '$or': {
                                                            creator                                        : userConnected._id.$id,
                                                            idUserAuthor                                   : userConnected._id.$id,
                                                            ['links.contributors.' + userConnected._id.$id]: {
                                                                $exists: true
                                                            },
                                                            ['links.members.' + userConnected._id.$id]     : {
                                                                $exists: true
                                                            },
                                                            ['links.organizer.' + userConnected._id.$id]   : {
                                                                $exists: true
                                                            }
                                                        }
                                                    }
                                                }
                                                this.properties['generic_agenda_parentId'] = {
                                                    inputType   : 'finder',
                                                    label       : 'Projet',
                                                    initType    : ['projects'],
                                                    search      : {
                                                        filters
                                                    },
                                                    rules       : {
                                                        required: true
                                                    },
                                                    multiple    : false,
                                                    initBySearch: true,
                                                    initMe      : false,
                                                    initContext : false,
                                                    initContacts: false,
                                                    openSearch  : true
                                                }
                                            }
                                        },
                                        save       : function (__form) {
                                            var value = {};
                                            var contributors = {};

                                            // Nettoyer les champs
                                            $.each(Object.keys(__form['generic_agenda_contributors']), function (__, __key) {
                                                contributors[__key] = {
                                                    type: 'citoyens'
                                                };
                                            });
                                            $.each(['scope', 'collection', 'generic_agenda_contributors'], function (__, __key) {
                                                delete __form[__key];
                                            });


                                            $.each(Object.keys(__form), function (__, __name) {
                                                var name = __name.substring('generic_agenda_'.length);
                                                if (name === 'tags') value[name] = __form[__name].split(',');
                                                else if (name === 'parentId' && typeof __form[__name] === 'object') value[name] = Object.keys(__form[__name])[0];
                                                else if (name === 'startDate' || name === 'endDate') value[name] = moment(__form[__name], 'DD/MM/YYYY HH:mm').format();
                                                else value[name] = __form[__name];
                                            });
                                            var params = {
                                                collection: 'actions',
                                                value
                                            };

                                            // Création de room
                                            get_project_room(value['parentId']).then(function (__room) {
                                                value['idParentRoom'] = __room;

                                                // Sauvegarder les données
                                                dataHelper.path2Value(params, function (__response) {
                                                    if (__response['result'] && __response['result'] === true) {
                                                        var action_id = __response['saved']['id'];

                                                        // Envoyer une notification rocket chat pour la nouvelle action
                                                        var url = baseUrl + '/costum/project/action/request/new_action_rocker_chat';
                                                        var post = {
                                                            project    : value['parentId'],
                                                            action_name: value['name']
                                                        };
                                                        ajaxPost(null, url, post);

                                                        // Enregistrer les contributeurs
                                                        dataHelper.path2Value({
                                                            id        : action_id,
                                                            collection: 'actions',
                                                            path      : 'links.contributors',
                                                            value     : contributors
                                                        }, function (__response) {
                                                            if (__response['result'] && __response['result'] === true) {
                                                                toastr.success(__response.msg);
                                                                const view_tabs = [
                                                                    'calendar<?= $blockKey ?>',
                                                                    'vertical_timeline<?= $blockKey ?>',
                                                                    'horizontal_timeline<?= $blockKey ?>',
                                                                    'gantt<?= $blockKey ?>'
                                                                ];
                                                                if (!_costum_is_aap) {
                                                                    params = {
                                                                        id        : value['parentId'],
                                                                        collection: 'projects',
                                                                        path      : 'parent.' + costum.contextId,
                                                                        value     : {
                                                                            type: costum.contextType,
                                                                            name: costum.title
                                                                        }
                                                                    }
                                                                    dataHelper.path2Value(params, function () {
                                                                        dyFObj.closeForm();

                                                                        let view_param;
                                                                        if ($('.cpl input[value=' + value['parentId'] + ']').length === 0) {
                                                                            view_param = {
                                                                                filter_reload: true
                                                                            };
                                                                        }
                                                                        $.each(view_tabs, function (__, __view) {
                                                                            $(`#${__view}`).addClass('load');
                                                                        });
                                                                        view_loadings['calendar<?= $blockKey ?>'](view_param);
                                                                    });
                                                                } else {
                                                                    dyFObj.closeForm();
                                                                    let view_param;
                                                                    if ($('.cpl input[value=' + value['parentId'] + ']').length === 0) {
                                                                        view_param = {
                                                                            filter_reload: true
                                                                        };
                                                                    }
                                                                    $.each(view_tabs, function (__, __view) {
                                                                        $(`#${__view}`).addClass('load');
                                                                    });
                                                                    view_loadings['calendar<?= $blockKey ?>'](view_param);
                                                                }
                                                            }
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    }
                                }, null, null, null, null, {
                                    type: 'bootbox'
                                });
                            }
                        }
                    }
                });
            }
        }

        /**
         * Lire charger la vue GANTT
         * @param {ViewElement[]} __elements L'élément qu'on va charger dans le gantt
         */
        function load_gantt(__elements = []) {
            const e_length = __elements.length;
            const data = [];
            const links = [];

            for (let i = 0; i < e_length; i++) {
                const project = __elements[i];
                const element_moment_start = moment(project['startDate']);
                const element_moment_end = moment(project['endDate']);
                const element_duration = element_moment_end.diff(element_moment_start, 'days');

                const t_project = {
                    id        : project['id'],
                    text      : project['name'],
                    start_date: element_moment_start.format('YYYY-MM-DD HH:mm'),
                    duration  : element_duration,
                    progress  : 0,
                    color     : _color ? _color['background'] : project['color'],
                    textColor : _color ? _color['foreground'] : invert_color(project['color'])
                };
                data.push(t_project);

                const progress_ratio = [0, 0];
                const actions_length = project.subs.length;
                for (let j = 0; j < actions_length; j++) {
                    const action = project.subs[j];
                    const action_start_date = moment(action['startDate']);
                    const action_end_date = moment(action['endDate']);
                    const action_duration = action_end_date.diff(action_start_date, 'days');
                    const checked_tasks_length = action.subs.filter(function (__task) {
                        return __task['checked']
                    }).length;

                    progress_ratio[0] += action.subs.length ? checked_tasks_length : 0;
                    progress_ratio[1] += action.subs.length ? action.subs.length : 1;

                    if (j === 0) {
                        links.push({
                            id    : links.length,
                            source: project.id,
                            target: action.id,
                            type  : '1'
                        })
                    } else {
                        const action_before = project.subs[j - 1];
                        links.push({
                            id    : links.length,
                            source: action_before.id,
                            target: action.id,
                            type  : '0'
                        });
                    }

                    data.push({
                        id        : action['id'],
                        text      : action['name'],
                        start_date: action_start_date.format('YYYY-MM-DD HH:mm'),
                        duration  : action_duration,
                        parent    : project['id'],
                        progress  : action.subs.length ? checked_tasks_length / action.subs.length : 0,
                        color     : _color ? _color['background'] : project['color'],
                        textColor : _color ? _color['foreground'] : invert_color(project['color'])
                    });

                    if (typeof action.subs !== 'undefined') {
                        const ss_length = action.subs.length;
                        for (let k = 0; k < ss_length; k++) {
                            const task = action.subs[k];
                            const task_start_date = moment(task['startDate']);
                            const task_end_date = moment(task['endDate']);
                            const task_duration = task_end_date.diff(task_start_date, 'days');

                            if (k === 0) {
                                links.push({
                                    id    : links.length,
                                    source: action.id,
                                    target: task.id,
                                    type  : '1'
                                })
                            } else {
                                const last_task = action.subs[k - 1];
                                links.push({
                                    id    : links.length,
                                    source: last_task.id,
                                    target: task.id,
                                    type  : '0'
                                });
                            }

                            data.push({
                                id        : task['id'],
                                text      : task['name'],
                                start_date: task_start_date.format('YYYY-MM-DD HH:mm'),
                                duration  : task_duration,
                                parent    : action['id'],
                                progress  : task['checked'] ? 1 : 0,
                                color     : _color ? _color['background'] : project['color'],
                                textColor : _color ? _color['foreground'] : invert_color(project['color'])
                            });
                        }
                    }

                    t_project.progress = progress_ratio[1] ? progress_ratio[0] / progress_ratio[1] : 0;
                }
            }
            const gantt_dom = document.getElementById('gantt_container<?= $blockKey ?>');
            if (gantt_dom.childElementCount) gantt_component.clearAll();
            else gantt_component.init(gantt_dom);

            gantt_component.parse({
                data,
                links
            });
        }

        /**
         * Récupérer le room du projet sélectionné
         * @param {string} __id Identifiant du projet associé
         * @return {Promise.<string>}
         */
        function get_project_room(__id) {
            var url = baseUrl + '/costum/project/project/request/room';
            var post = {
                project: __id
            };
            return new Promise(function (__resolve) {
                ajaxPost(null, url, post, __resolve);
            });
        }

        /**
         * Récupérer les évènements ainsi que les sous events
         * depuis la base de données
         * @param {Object} __params
         * @param {Object} [__params.filter] Filtres de sélection qu'on envoie
         * @param {string} [__params.request] Type de requête pour le contrôleur généralement 'all'
         * @param {number} [__params.reorder] Reordonner la réponse ou pas
         * @returns {Promise.<{data: ViewElement[], groups: FilterView[]}}>}
         */
        function get_events(__params = {}) {
            let url = baseUrl + '/costum/agenda/events/request/all';
            let post = {
                costumType: costum.contextType,
                costumId  : costum.contextId
            };

            if (typeof __params.filter !== 'undefined') post = $.extend(true, {}, post, __params.filter);

            if (typeof __params.reorder !== 'undefined') url += '/reorder/' + __params.reorder;
            return new Promise(function (__resolve) {
                ajaxPost(null, url, post, __resolve);
            });
        }

        /**
         * Récupérer les actions ainsi que la liste des projets
         * depuis la base de données
         * @param {Object} __params
         * @param {Object} [__params.filter] Filtres de sélection qu'on envoie
         * @param {string} [__params.request] Type de requête pour le contrôleur généralement 'all'
         * @param {number} [__params.reorder] Reordonner la réponse ou pas
         * @returns {Promise<{data: ViewElement[], groups: FilterView[]}}>}
         */
        function get_actions(__params = {}) {
            let url = baseUrl + '/costum/agenda/projects/request';
            let post = {
                costumType: costum.contextType,
                costumId  : costum.contextId
            };

            if (typeof __params.filter !== 'undefined') post = $.extend(true, {}, post, __params.filter);

            if (typeof __params.request !== 'undefined') url += '/' + __params.request;
            else url += '/actions';
            if (typeof __params.reorder !== 'undefined') url += '/reorder/' + __params.reorder;
            return new Promise(function (__resolve) {
                ajaxPost(null, url, post, __resolve);
            });
        }

        function invert_color(hex) {
            if (hex.indexOf('#') === 0) {
                hex = hex.slice(1);
            }
            // convert 3-digit hex to 6-digits.
            if (hex.length === 3) {
                hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
            }
            if (hex.length !== 6) {
                throw new Error('Invalid HEX color.');
            }
            // invert color components
            var r = parseInt(hex.slice(0, 2), 16),
                g = parseInt(hex.slice(2, 4), 16),
                b = parseInt(hex.slice(4, 6), 16);
            // pad each with zeros and return
            return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? '#000000' : '#ffffff';
        }

        /**
         * Action exécutée lors du scroll de l'écran ou aussi après actualisation des filtres
         */
        function check_window_scroll(__event) {
            const main_nav = $(__event && __event === 'costumizer' ? '#menu-top-costumizer' : '#mainNav');

            const filter_container = $('#cpl_container<?= $blockKey ?>');
            const content_container = filter_container.next('.content');
            const agenda_view = filter_container.parent('.agenda-viewer');
            const filter_button = $('#filter-xs<?= $blockKey ?>');
            const overlay_dom = $('#card-overlay-modal<?= $blockKey ?>')

            if (content_container.length === 0) return;

            const content_container_on_screen = content_container.get(0).getBoundingClientRect();
            const agenda_view_on_screen = agenda_view.get(0).getBoundingClientRect();
            const offset = 20;

            const view_is_not_mobile = W.innerWidth > 1024;
            const filter_exceed = (view_is_not_mobile ? filter_container.outerHeight() : filter_button.outerHeight()) < content_container.outerHeight() && content_container_on_screen.top + content_container.outerHeight() <= main_nav.outerHeight() + offset + (view_is_not_mobile ? filter_container.outerHeight() : filter_button.outerHeight());

            if (view_is_not_mobile) {
                filter_button.css('display', '');
                overlay_dom.css('display', '');
                if (typeof __event !== 'undefined' && __event === 'resize') {
                    filter_container.removeAttr('style');
                    filter_container.find('.card-body').css('max-height', '');
                }
            }

            if (main_nav.outerHeight() + offset >= agenda_view_on_screen.top) {
                let top;

                if (filter_exceed) top = content_container_on_screen.top + content_container.outerHeight() - (view_is_not_mobile ? filter_container.outerHeight() : filter_button.outerHeight());
                else top = main_nav.outerHeight() + offset;

                if (view_is_not_mobile) {
                    content_container.css({
                        'margin-left': $(filter_container).outerWidth() + offset
                    });
                    filter_container.css({
                        position: 'fixed',
                        top,
                        left    : agenda_view_on_screen.left
                    });
                } else {
                    content_container.css('margin-left', '');
                }

                filter_button.css({
                    position: 'fixed',
                    top,
                    left    : agenda_view_on_screen.left
                });

            } else {
                filter_button.css('position', '')
                             .css('left', '')
                             .css('top', '');
                if (view_is_not_mobile) {
                    filter_container.css('position', '')
                                    .css('left', '')
                                    .css('top', '');

                    content_container.css('margin-left', '');
                }
            }
        }

        $(function () {
            let timeout = null

            $.each(Object.keys(view_loadings), function (__, __tab) {
                const element = $('[href="#' + __tab + '"]');
                if (element.length > 0) {
                    element.on('shown.bs.tab', function () {
                        view_loadings[__tab]({
                            reload: true
                        });
                    });
                }
            });

            $('#agenda_view_tabs<?= $blockKey ?>').on('click', '[role=tab]', function ($e) {
                $e.preventDefault();
                $(this).tab('show');
            });

            $('#agenda_view_tabs<?= $blockKey ?> [role=tab]:first').tab('show');

            $('#cpl-search<?= $blockKey ?> input').on('input', function () {
                if (timeout !== null) clearTimeout(timeout);
                timeout = setTimeout(function () {
                    $('#agenda_view_tabs<?= $blockKey ?>+.tab-content>.tab-pane').each(function () {
                        $(this).addClass('load');
                    });
                    reload_current_view();
                }, 800);
            });
            $('#cpl_filter_select_all<?= $blockKey ?>').on('change', function (__e) {
                const checked = __e.target.checked;
                $('#cpl<?= $blockKey ?>').find('.cpl .cpl-check').each(function (__, __element) {
                    __element.checked = checked;
                    const checkmark = $(__element).next('.checkmark');
                    checkmark.css('background-color', checked ? checkmark.css('border-color') : '#fff');
                    store_preference(__element.value, checked);
                });

                $('#agenda_view_tabs<?= $blockKey ?>+.tab-content>.tab-pane').each(function () {
                    $(this).addClass('load');
                });
                reload_current_view();
            });
            $('#cpl<?= $blockKey ?>').on('change', '.cpl .cpl-check', function () {
                const checked = this.checked;
                const checkmark = $(this).next('.checkmark');
                checkmark.css('background-color', checked ? checkmark.css('border-color') : '#fff');
            });
            $('#setting<?= $blockKey ?> form').on('submit', function ($e) {
                $e.preventDefault();
                const form = {
                    content_type          : $(this).find('[name=content_type]:checked').val(),
                    allow_external_sources: $(this).find('[name=allow_external_sources]').is(':checked'),
                    width                 : $(this).find('[name=width]:checked').val() === 'true',
                    available_views       : []
                }
                $(this).find('[name=available_views]:checked').each(function () {
                    form.available_views.push($(this).val());
                });
                const path2value = {
                    collection: 'cms',
                    id        : '<?= $blockKey ?>',
                    path      : 'allToRoot',
                    value     : form,
                    setType   : [{
                        path: 'width',
                        type: 'boolean'
                    }]
                }
                dataHelper.path2Value(path2value, function () {
                    const id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    const path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    const kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    if (form.show.length == 0) {
                        dataHelper.unsetPath({
                                id        : path2value.id,
                                collection: path2value.collection,
                                path      : "show"
                            }, function (params) {
                                if (params.result) {
                                    $("input[name='show']").prop('checked', false);
                                    $(".all-btn-content").hide();
                                }
                            }
                        );
                    }
                });
            });

            $('#setting<?= $blockKey ?> [name=content_type]').on('change', function () {
                const checked_element = $('#setting<?= $blockKey ?> [name=content_type]:checked');
                const gantt_view_element = $('#available_views_gantt<?= $blockKey ?>');

                if (checked_element.val() === 'events') {
                    gantt_view_element.parents('.checkbox').css('display', 'none');
                    gantt_view_element.prop('checked', false);
                } else {
                    gantt_view_element.parents('.checkbox').css('display', '');
                    gantt_view_element.prop('checked', true);
                }
            });

            $('#card-overlay-modal<?= $blockKey ?>, #cpl_container<?= $blockKey ?> .card-action .fa-times').on('click', function () {
                const cpl_container = $('#cpl_container<?= $blockKey ?>');
                const close = $('#cpl_container<?= $blockKey ?> .card-action .fa-times');
                const overlay = $('#card-overlay-modal<?= $blockKey ?>');
                $('#filter-xs<?= $blockKey ?>').fadeIn();
                overlay.fadeOut();
                cpl_container.animate({
                    opacity: 0,
                    top    : 0
                }, {
                    complete: function () {
                        close.css({
                            position : '',
                            display  : '',
                            opacity  : '',
                            top      : '',
                            left     : '',
                            transform: ''
                        });
                        cpl_container.find('.card-body').css({
                            maxHeight: ''
                        });
                    }
                });
            });

            $('#filter-xs<?= $blockKey ?> .btn').on('click', function () {
                const cpl_container = $('#cpl_container<?= $blockKey ?>');
                const main_nav = $('#mainNav');

                cpl_container.find('.card-body').css({
                    maxHeight: `calc(100vh - ${cpl_container.find('.card-header').outerHeight() + main_nav.outerHeight() + 20 * 2}px)`
                });
                cpl_container.css({
                    position : 'fixed',
                    display  : 'block',
                    opacity  : 0,
                    top      : 0,
                    left     : '50%',
                    transform: 'translate(-50%, -50%)'
                }).animate({
                    opacity: 1,
                    top    : '50%'
                });
                $('#card-overlay-modal<?= $blockKey ?>').fadeIn();
                $(this).parent('#filter-xs<?= $blockKey ?>').fadeOut();
            });

            $('.cmsbuilder-center-content').on('scroll', function () {
                check_window_scroll('costumizer');
            });

            $('[data-target=event_sources]').on('click', function () {
                $('.source-management').slideToggle();
            });

            $('.source-management .source-management-item a').on('click', function (__e) {
                __e.preventDefault();
                const self = $(this);
                const target = self.attr('href').substring(1);
                dyFObj.openForm(_forms[target], null, null, null, null, {
                    type             : "bootbox",
                    notCloseOpenModal: true,
                });
                $('.source-management').slideToggle();
            });

            W.addEventListener('scroll', function () {
                check_window_scroll();
            });

            W.addEventListener('resize', function () {
                check_window_scroll('resize');
            });
        });
    })
    (gantt, jQuery, window);
</script>