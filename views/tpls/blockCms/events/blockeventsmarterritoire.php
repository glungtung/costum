<?php
/**
 * TPLS QUI PERMET AFFICHAGE DES 3 DERNIERS NOUVEAUTÉS
 * MODÈLE INSPIRER DU COSTUM FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMS LA COULEUR DE INFO
 */
$keyTpl = "blockeventsmarterritoire";

$paramsData = [ 
    "title" => "Block événements smarterritoire",
    "icon"  =>  "",
    "color" => "#000000",
    "txtcolor" => "#000000",
    "colorcard" => "#FFFFFF",
    "background" => "#FFFFFF"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style>
    .name{
        color: <?= $paramsData["txtcolor"] ?>;
    }

    .hexagon1 {
        width: 542px !important;
    }

    .info-card{
        background-color: <?= $paramsData["colorcard"] ?>;
    }
</style>
<div>
    <h1 class="text-center" style="color:<?= $paramsData["color"] ?>;background-color: <?= $paramsData["background"] ?>">
        <?= @$params["icon"]." ".$paramsData["title"]; ?>      
    </h1>

    <div style="margin-top: 7%;" id="containEventSmarterritoire" class="">

    </div>
    <div style="border-radius: 10px 10px 10px 10px;" class="text-center container ">

        <!-- Generator: Adobe Illustrator 24.0.3, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
        <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app" style="text-decoration : none;">
            <svg style="width:6%;margin-top: 2%;"  version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 102 97" style="enable-background:new 0 0 102 97;" xml:space="preserve">
            <style type="text/css">
                .st0plusevent{fill:#007A5E;}
            </style>
            <path class="st0plusevent" d="M73.73,8.43H28.42L5.76,47.67l22.66,39.24h45.31l22.65-39.24L73.73,8.43z M64.49,52.05h-9.52v9.52
            c0,2.05-1.66,3.71-3.71,3.71c-2.05,0-3.71-1.66-3.71-3.71v-9.52h-9.52c-2.05,0-3.71-1.66-3.71-3.71c0-2.05,1.66-3.71,3.71-3.71h9.52
            V35.1c0-2.05,1.66-3.71,3.71-3.71c2.05,0,3.71,1.66,3.71,3.71v9.52h9.52c2.05,0,3.71,1.66,3.71,3.71
            C68.2,50.39,66.54,52.05,64.49,52.05z"/>
        </svg>
    </a>
</div>


<script type="text/javascript">
filter = <?= json_encode(@$filters); ?>;
tplCtx = {};
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;

sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
$(document).ready(function(){
    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {
            "title" : "<?php echo Yii::t('cms', 'Configure the event block section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section on event blocks')?>",
            "icon" : "fa-cog",
            onLoads : {
                onload : function(){
                    $(".parentfinder").css("display","none");
                }
            },
            "properties" : {
                title : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                icon : { 
                    label : "<?php echo Yii::t('cms', 'Icon')?>",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                },
                color : {
                    label : "<?php echo Yii::t('cms', 'Title color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                },
                background : {
                    label : "<?php echo Yii::t('cms', 'Background color of the title')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                },
                colorcard : {
                    label : "<?php echo Yii::t('cms', 'Color of the card')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorcard
                },
                txtcolor : {
                    label : "<?php echo Yii::t('cms', 'Text color')?>",
                    inputType : "colorpicker",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.txtcolor
                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                     else {
                      mylog.log("activeForm save tplCtx",tplCtx);
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }
                }
            }
        };
    
     $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
    var params = {};
    filters = <?php echo json_encode(@$thematique); ?> ;

    if (typeof filters != "undefined" && filters != "") {
        params.filters  = filters;
    }

        if (typeof filters != "undefined" && filters != "") {
            params.filters  = filters;
        }

        var params = {
            source : contextSlug
        };

        $.ajax({
            type : 'POST',
            data : params,
            url : baseUrl+"/costum/costumgenerique/geteventaction",
            dataType : "json",
            async : false,
            success : function(data){

                var str = "";

                if(data.result == true) {
                    var i = 0;
                    var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

                    $(data.element).each(function(key,value){
                        if (i <= 2) {
                            i++;
                            var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null; 

                            var imgMedium = (value.imgMedium != "none") ? baseUrl+value.imgMedium : url;

                            var fleches = baseUrl+"/images/smarterritoireautre/fleche-plus-noire.svg";

                            str += '<div class="card">';
                            str += '<div id="event-affiche" class="card-color col-md-4">';
                            str += '<div id="affichedate" class="info-card text-center">';
                            str += '<div id="afficheImg" class="img-hexa">';
                            str += '<a class="entityName bold text-dark add2fav  lbh-preview-element" href="#page.type.events.id.'+value.id+'>';
                            str += '<div class="hexagon hexagon1"><div class="hexagon-in1"><div class="hexagon-in2" style="background-image: url('+imgMedium+');"><div style=";display:none;color:black;position: absolute;margin-left: -113px;margin-top: 6vw;" class="text-in1">'+value.name+'<br><img width="5vw;" src="'+fleches+'"></div></div></div></div>';
                            str += '</a>';
                            str += '</div>';
                            str += ''+startDate+'</br>'+value.type;
                            str += '</div>';
                            str += '</div>';
                            str += '</div>';
                        }else{

                        }                    
                    });
                }
                else{
                    str += "<center><?php echo Yii::t('cms', 'There are no events')?></center>";
                }
                $("#containEventSmarterritoire").html(str);
            }
        });
    });
</script>