<?php
    $keyTpl = 'coevent_timeline';
    $costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
    $paramsData = [
        'logo'             => $ownerEvent['profilImageUrl'] ?? '/assets/6b85ffc2/images/coevent/thumbnail-default.jpg',
        'banniere'         => $ownerEvent['profilRealBannerUrl'] ?? '/assets/6b85ffc2/images/coevent/thumbnail-default.jpg',
        'aboutBanniere1'   => $costum['assetsUrl'] . '/images/coevent/points_block_membre.png',
        'aboutBanniere2'   => $costum['assetsUrl'] . '/images/coevent/img4.jpg',
        'aboutBanniere3'   => $costum['assetsUrl'] . '/images/coevent/img3.png',
        'sectionSeparator' => $costum['assetsUrl'] . '/images/coevent/trait.png',
        'mainTitle'        => 'Qui sommes-nous ?',
        'aboutTitle'       => 'titre',
        'aboutContent'     => 'Contenu',
        'primary_color'    => '#EE302C',
        'programContent'   => 'Contenu du programme',
        'show_map'         => true
    ];
    
    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e])) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>
    <link title="timeline-styles" rel="stylesheet" href="https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css">
    <script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
    <style>
        .slider {
            width: 100%;
            overflow: hidden;
            margin: 0 auto;
            position: relative;
        }

        .slides {
            display: flex;
            flex-direction: row;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
        }

        .slide {
            list-style: none;
            flex: 0 0 auto;
            width: 100%;
        }

        .event-detail > li {
            list-style: none;
        }

        .slide-element {
            width: 100%;
            height: 100%;
        }

        .slide-indicators {
            margin: 0;
            padding: 5px;
            position: absolute;
            display: flex;
            flex-direction: row;
            bottom: 12px;
            left: 50%;
            transform: translateX(-50%);
            background-color: rgba(0, 0, 0, .3);
            border-radius: 20px;
            line-height: 0;
        }

        .slide-navigator {
            list-style: none;
            display: block;
            padding: 15px 10px;
            background-color: transparent;
            margin-top: -5px;
            margin-bottom: -5px;
            color: white;
            cursor: pointer;
            align-self: center;
        }

        .slide-navigator:hover {
            background-color: white;
            color: black;
        }

        .slide-navigator.prev {
            margin-left: -6px;
            border-right: 1px solid rgba(255, 255, 255, .5);
            border-radius: 20px 0 0 20px;
        }

        .slide-navigator.next {
            margin-right: -6px;
            border-left: 1px solid rgba(255, 255, 255, .5);
            border-radius: 0 20px 20px 0;
        }

        .slide-indicator {
            display: block;
            list-style: none;
            margin-left: 5px;
            margin-right: 5px;
            align-self: center;
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background-color: transparent;
            border: white 1px solid;
        }

        .slide-indicator {
            cursor: pointer;
        }

        .slide-indicator.active {
            background-color: white;
        }

        #second-search-bar {
            padding-left: 45px;
            width: calc(100% - 50px);
            text-align: left;
        }

        #search-button {
            background: <?= $paramsData['primary_color'] ?>;
            width: 50px;
            border-radius: 0 30px 30px 0;
        }

        #search-dropdown {
            margin-left: 60px;
        }

        #show_hide_map_container {
            position: absolute;
            bottom: 10px;
            left: 50%;
            transform: translateX(-50%);
        }

        .tl-headline {
            padding: 2px;
        }

        .tl-slidenav-title {
            opacity: 1 !important;
        }

        .tl-timeline .tl-attribution {
            display: none;
        }
    </style>
    <!-- Header -->
    <div class="container-fluid">
        <div class="container" style="margin-top: 3%;">
            <div class="row">
                <div class="col-sm-12 program-container">
                    <h1 id="chronologie" class="tooltips dinalternate margin-top-20" data-toggle="tooltip" data-placement="bottom"
                        data-original-title="Naviguez dans la frise chronologique" area-describedby="tooltipchronologie">Chronologie
                        de <?= $costum['title'] ?>
                        <div id="tooltipchronologie" class="tooltip">Naviguez dans la frise chronologique</div>
                    </h1>
                    <div style="border: solid <?= $paramsData['primary_color'] ?> 4px;border-radius: 11px;width: 17%; margin-bottom: 15px;"></div>
                    <div id='timeline-embed' style="width: 100%; height: 55vh; margin-top: 50px;"></div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?= $costum['assetsUrl'] ?>/js/coevent/carousel.js"></script>
    <script type="text/javascript">
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
        jQuery(document).ready(function () {
            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema": {
                    "title"      : "<?php echo Yii::t('cms', 'Set up your section') ?>",
                    "description": "<?php echo Yii::t('cms', 'Customize your section') ?>",
                    "icon"       : "fa-cog",
                    "properties" : {
                        primary_color   : {
                            inputType: 'colorpicker',
                            label    : 'Couleur primaire',
                            values   : '<?= $paramsData['primary_color'] ?>'
                        },
                        timenav_position: {
                            inputType: 'select',
                            label    : 'Position du slide',
                            options  : {}
                        }
                    },
                    beforeBuild  : function () {
                        uploadObj.set("cms", "<?php echo $blockKey ?>");
                    },
                    save         : function (data) {
                        tplCtx.value = {};
                        $.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, function (k, val) {
                            tplCtx.value[k] = $("#" + k).val();
                            if (k == "parent")
                                tplCtx.value[k] = formData.parent;

                            if (k == "items")
                                tplCtx.value[k] = data.items;
                        });

                        if (typeof tplCtx.value == "undefined") toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value(tplCtx, function (params) {
                                dyFObj.commonAfterSave(params, function () {
                                    toastr.success("<?php echo Yii::t('cms', 'Element well added') ?>");
                                    $("#ajax-modal").modal('hide');
                                    urlCtrl.loadByHash(location.hash);
                                });
                            });
                        }

                    }
                }
            };
            load_timeline();
        });

        function load_timeline(type = '') {
            var events_timeline = null;
            var tl_interval = setInterval(function () {
                if (typeof TL !== 'undefined') {
                    clearInterval(tl_interval);
                    var params = {
                        //notSourceKey: true,
                        searchType: ["events"],
                        indexStep : "0",
                        fields    : ["banner"]
                        //filters: {}
                    };
                    ajaxPost(
                        null,
                        baseUrl + "/" + moduleId + "/search/globalautocomplete",
                        params,
                        function (response) {
                            var main_events = [];
                            if (response.results) {
                                $.each(response.results, function (i, event) {
                                    var moment_start = moment(event.startDate, "YYYY-MM-DD");
                                    var moment_end = moment(event.endDate, "YYYY-MM-DD");
                                    var map = {
                                        start_date: {
                                            year  : moment_start.year(),
                                            month : moment_start.month() + 1,
                                            day   : moment_start.date(),
                                            hour  : moment_start.hour(),
                                            minute: moment_start.minute()
                                        },
                                        end_date  : {
                                            year  : moment_end.year(),
                                            month : moment_end.month() + 1,
                                            day   : moment_end.date(),
                                            hour  : moment_end.hour(),
                                            minute: moment_end.minute()
                                        },
                                        text      : {
                                            headline: event.name,
                                            text    : `<p>${event.shortDescription}</p>`
                                        },
                                        background: {}
                                    };

                                    if (event['banner'] && event['banner'].length) {
                                        map.background['url'] = event['banner'];
                                    } else {
                                        map.background['color'] = `<?= $paramsData['primary_color'] ?>`;
                                    }

                                    if (event['profile']) {
                                        map["media"] = {};
                                        map.media['url'] = event['profile'];
                                        map.media['thumbnail'] = event['profile'];
                                    }

                                    main_events.push(map);
                                });
                            }

                            var data = {
                                events: main_events
                            };

                            var options = {
                                script_path     : `${baseUrl}/plugins/knightlab_timeline3/js/`,
                                language        : 'fr',
                                layout          : 'portrait', //portrait or landscape
                                scale_factor    : 1,
                                timenav_position: 'top',
                                initial_zoom    : 3
                            }

                            events_timeline = new TL.Timeline('timeline-embed', data, options);
                        });
                }
            });
        }

        function subscribeToEvent(eventSource, theUserId = null) {
            var labelLink = "";
            var parentId = eventSource.data("id");
            var parentType = eventSource.data("type");
            var childId = (theUserId) ? theUserId : userId;
            var childType = "citoyens";
            var name = eventSource.data("name");
            var id = eventSource.data("id");
            //traduction du type pour le floopDrawer
            eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
            var connectType = (parentType == "events") ? "connect" : "follow";

            if (eventSource.data("ownerlink") == "follow") {
                callback = function () {
                    labelLink = (parentType == "events") ? "DÉJÀ PARTICIPANT(E)" : trad.alreadyFollow;
                    if (eventSource.hasClass("btn-add-to-directory"))
                        labelLink = "";
                    eventSource.html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
                    eventSource.addClass("active");
                    eventSource.data("ownerlink", "unfollow");
                    eventSource.data("original-title", labelLink);
                }
                if (parentType == "events")
                    links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
                else
                    links.follow(parentType, parentId, childId, childType, callback);
            } else {
                //eventSource.data("ownerlink")=="unfollow"
                connectType = (parentType == "events") ? "attendees" : "followers";
                callback = function () {
                    labelLink = (parentType == "events") ? "PARTICIPER" : "DÉJÀ PARTICIPANT(E)";
                    if (eventSource.hasClass("btn-add-to-directory"))
                        labelLink = "";
                    $(eventSource).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
                    $(eventSource).data("ownerlink", "follow");
                    $(eventSource).data("original-title", labelLink);
                    $(eventSource).removeClass("text-white");
                };
                links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
            }
        }


        $(document).on("mouseup", "#annuaireButton<?= $kunik ?>", function () {
            $('.journee_views button')[0].click();
        });

        $(document).on("click", ".customFollowBtn", function (event) {
            let eventData = $(this).data("event-id");
            thisElement = $(this);
            let isExpAttFull = (typeof eventData != "undefined" && typeof eventData.links.attendees != "undefined" && typeof eventData.expectedAttendees != "undefined" && Object.keys(eventData.links.attendees).length == eventData.expectedAttendees);

            if (isExpAttFull == false) {
                if (userId) {
                    subscribeToEvent(thisElement);
                } else {
                    $("#modalLogin").on('show.bs.modal', function (e) {
                        if ($("#infoBL").length == 0) {
                            $("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                                Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                                <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                            </div>`);
                        }
                    });
                    $("#modalLogin").on('hide.bs.modal', function (e) {
                        $("#infoBL").remove();
                    });
                    toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
                    Login.openLogin();
                }
            } else {
                if (userId) {
                    subscribeToEvent(thisElement);
                }
            }

            if (typeof eventData != "undefined" && isExpAttFull && thisElement.data("ownerlink") == "follow") {
                bootbox.dialog({
                    message: `<div class="alert-white text-center"><br>
            <strong>Désolé ! Il n'y a plus de place. Le nombre maximal de participants a été atteint</strong>
            <br><br>
            <button class="btn btn-default bootbox-close-button">OK</button>
            </div>`
                });
            }
        });
    </script>
<?php // } else { ?>
    <!--div class="jumbotron mt-3">
        <h1>Oops, halte là!</h1>
        <p>Ce block est conçu uniquement pour les évènements</p>
    </div-->
<? php// } ?>