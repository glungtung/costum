<?php
/**
 * TPLS QUI PERMET AFFICHAGE DES 3 DERNIERS NOUVEAUTÉS
 * MODÈLE INSPIRER DU COSTUM FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMS LA COULEUR DE INFO
 */
$keyTpl = "blockevent";

$paramsData = [
    "titre"          => "Bloc événements",
    "icon"           => "",
    "color"          => "#000000",
    "txtCard"        => "#000000",
    "backgroundCard" => "#FFFFFF",
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style>
    #event-block-<?=$kunik?> h1{

        color:<?=$paramsData["color"]?>;
    }
    #event-block-<?=$kunik?> .img-event{
        height : 200px;
        width: 500px;
    }
    #event-block-<?=$kunik?> .card-event{
        margin-top: -30%;
        background: <?=$paramsData["backgroundCard"];?>;
        color : <?=$paramsData["txtCard"];?>;
    }
    #event-block-<?=$kunik?> .btn-edit-delete{
        display: none;
        top: 50%;
        left: 50%;
        z-index: 1000;
    }
    #event-block-<?=$kunik?>:hover  .btn-edit-delete {
         display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        top:50%;
        left: 50%;
        transform: translate(-50%,-50%);
    }
</style>


<?php
// A crée dans un modèle a part ce code
date_default_timezone_set('UTC');

//On récupère sur ce mois-ci les évènements
$date_array = getdate();
$numdays    = $date_array["wday"];

$endDate   = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24 * 60 * 60)));
$startDate = strtotime(date("Y-m-d H:i"));

// Préparation de la requête
$where = array("source.key" => $costum["contextSlug"]);
// "startDate"      => array('$gte' => new MongoDate($startDate)),
// "endDate"        => array('$lt' => new MongoDate($endDate)));

if (!empty($filters)) {
    $where["thematique"] = $filters;
}
?>
<div id="event-block-<?=$kunik?>" class="sp-cms-container ">
    <h1 class="text-center title" >
        <i class="fa <?=$paramsData['icon']?>"></i>
        <?=$paramsData["titre"]?>
    </h1>
    <div class="text-center btn-edit-delete">
        <?php

if (Authorisation::isInterfaceAdmin()) {?>
                <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('event')">
                    <?php echo Yii::t('cms', 'Add an event')?>
                </button>
        <?php }?>
    </div>
<?php
$allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where, 3);
if ($allEvent) {
    foreach ($allEvent as $key => $value) {
        $img  = (isset($value["profilImageUr"])) ? $value["profilImageUrl"] : "/images/templateCostum/no-banner.jpg";
        $date = date(DateTime::ISO8601, $value["startDate"]->sec);?>
        <div class="col-md-4">
            <div class="col-xs-12">
                <img src="<?=Yii::app()->getModule("costum")->assetsUrl . $img;?>" class="img-event img-responsive">
                <div class="col-xs-6 card-event">
                    <?=$value["name"];?> <br>
                    <?=date("d/m/Y", strtotime($date));?> <br>
                    <?=Yii::t("common", $value["type"]);?>
                </div>
            </div>
        </div>
        <?php
}
} else {?>
    <div class="text-center"> <?php echo "<?php echo Yii::t('cms', 'No events are planned')?>"; ?> </div>

        <?php }?>

</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "titre" : {
                        label : "Titre",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.titre !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.titre:""

                    },
                    icon : {
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        inputType : "select",
                        options : {
                            "fa-newspaper-o"    : "Newspapper",
                            "fa-calendar " : "Calendar",
                            "fa-lightbulb-o "  :"Lightbulb"
                        },
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.icon !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.icon:""
                    },
                    color : {
                        label : "<?php echo Yii::t('cms', 'Title color')?>",
                        inputType : "colorpicker",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.color !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.color:""
                    },
                    txtCard : {
                        label : "<?php echo Yii::t('cms', 'Text color in the card')?>",
                        inputType : "colorpicker",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.txtCard !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.txtCard:""
                        },
                    backgroundCard : {
                        label : "<?php echo Yii::t('cms', 'Color of the card')?>",
                        inputType : "colorpicker",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.backgroundCard !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.backgroundCard:""
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                     else {
                      mylog.log("activeForm save tplCtx",tplCtx);
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }

                }
            }
        };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

});
</script>