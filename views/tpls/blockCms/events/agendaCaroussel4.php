<?php 
  $keyTpl ="agendaCarousel4";
  $paramsData = [ 
    "title" => "Titre",
    "imgBorderColor" => "#008037"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style>

  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
    z-index: 9999;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
  }
  .container<?php echo $kunik ?> .owl-item img{
    vertical-align: middle;
    height: 100%;
    border: 2px solid grey;
  }
  .container<?php echo $kunik ?> .owl-item .item{
    height: 327px;
  }
  .container<?php echo $kunik ?> .owl-item {
    transition: all .2s ease-in-out;
    height: 500px !important
  }
  .container<?php echo $kunik ?> .owl-item:hover {
    transform: scale(1.1);
  }
 .container<?php echo $kunik ?> .owl-stage-outer {
    padding: 35px 0px 0px 0px;
  }
  .container<?php echo $kunik ?> .event-place{
      /*padding: 20px 0px;*/
  }
.container<?php echo $kunik ?> .event-name{
    text-transform: none !important;
}
.container<?php echo $kunik ?> .letter-orange{
  color:#000000 !important;
  text-transform: capitalize !important;
  font-weight: normal !important;
}
.container<?php echo $kunik ?> .info{
  padding: 0px 46px !important;
}

  .container<?php echo $kunik ?> .swiper-container {
    width: 100%;
    height: 460px;
    margin-left: auto;
    margin-right: auto;
  }

  .container<?php echo $kunik ?> .swiper-slide {
    background-size: cover;
    background-position: center;
  }
  .container<?php echo $kunik ?> .swiper-slide-active{
    width: 100% !important;
  }
  .container<?php echo $kunik ?> .swiper-slide:hover .fa-eye{
    display: inline-block;
  }
  .container<?php echo $kunik ?> .swiper-slide .fa-eye{
    display: none;
    padding: 0 5px 0 5px;
    background: rgb(0,0,0,0.6);
    color: white;
    border-radius: 0 0 16px 0;
    font-size: 24px;
  }

  .container<?php echo $kunik ?> .info-events{
    background-color: rgb(8 8 8 / 60%);
    position: absolute;
    width: 95%;
    height: auto;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -14%);
  }
  @media (max-width: 450px){
    .container<?php echo $kunik ?> .info-events{
      width: 100%;
    }
  }
  .container<?php echo $kunik ?> .description{
      padding:10px 5px 0 5px;
     overflow: hidden;
     text-overflow: ellipsis;
     display: -webkit-box;
     -webkit-line-clamp: 5; /* number of lines to show */
     -webkit-box-orient: vertical;
     /*font-size: 13px;*/
     line-height : 1.6;
  }

  .container<?php echo $kunik ?> .add-event-caroussel{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .add-event-caroussel{
    display: block;
  }
  .container<?php echo $kunik ?> .event-img{
    position: relative;
    height: 425px;
    width: 425px;
    background-size: cover;
    background-position: center;
    margin: 0px auto;
    background-repeat: no-repeat;
    border-radius: 50%;
    border: 2px solid <?php echo @$paramsData["imgBorderColor"] ?> !important;
  }
  .container<?php echo $kunik ?> .swiper-button-prev,.container<?php echo $kunik ?> .swiper-button-next{
    display: none;
  }
  .block-container-<?= $kunik ?>:hover .swiper-button-prev,.block-container-<?= $kunik ?>:hover .swiper-button-next{
    display: initial;
  }
  @media (max-width: 993px){
    .container<?php echo $kunik ?> .event-img{
      position: relative;
      height: 353px;
      width: 100%;
      background-size: cover;
      background-position: center;
      margin: 0px auto;
      background-repeat: no-repeat;
      border-radius: 0;
      border: 2px solid <?php echo @$paramsData["imgBorderColor"] ?> !important;
    }
    .container<?php echo $kunik ?> .swiper-container {
      height: 640px;
    }
  }
</style>
<div class="container<?php echo $kunik ?> col-md-12">
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
    <p class="text-center add-event-caroussel hiddenPreview">
        <button class="btn btn-sm btn-primary hiddenPreview">Ajouter un évènement</button>
    </p>
  <?php } ?>
  <div class="swiper-container">
    <div class="swiper-wrapper"></div>
     <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your agenda.')?>",
              "description" : "<?php echo Yii::t('cms', 'Personalize your agenda')?>",
              "icon" : "fa-cog",
            
            "properties" : {
                "imgBorderColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Border of the photo')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.imgBorderColor
                }           
            },
              beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              	else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      dyFObj.closeForm();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              	}

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        //add-event*****************
        $(".container<?php echo $kunik ?> .add-event-caroussel").on('click',function(){
          var eventsObj ={
            afterSave : function(data){
              dyFObj.commonAfterSave(data, function(){
                urlCtrl.loadByHash(location.hash);
              });
            }
          };
          dyFObj.openForm('event',null,null,null,eventsObj)
        })
	});

</script>

<script>
$(function ($) {
var htmlContent = ""
  getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+
            '/id/'+costum.contextId+'/dataName/events',
          function(data){ 
              mylog.log("andrana events",data);
              $.each(data,function(k,v){
                      htmlContent+= 
                      `<div class="swiper-slide">
                        <div class="col-md-6 col-xs-12">
                          <div class="all-info" style='position:relative;padding-top:51px'>
                              <h2 class=" title title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
                                <?php echo $paramsData["title"] ?>
                              </h2>
                              ${getDateFormated<?php echo $kunik ?>(v)}
                              <h4 class="">
                                <a href="#page.type.${v.collection}.id.${v.id}" class="subtitle lbh event-place">
                                ${v.name}
                                </a>
                              </h4>
                              <h5 class="event-place">
                                  <a href="#page.type.${v.collection}.id.${v.id}" class=" other lbh"><i class="fa fa-map-marker"></i>
                                  ${((typeof v.address.level4Name !="undefined") ? v.address.level4Name : "<?php echo Yii::t('cms', 'Address not given')?>")}
                                  ${((typeof v.address.streetAddress !="undefined") ? v.address.streetAddress : "<?php echo Yii::t('cms', 'Address not given')?>")}
                                  </a>
                              </h5>
                                <p class="description markdown" style="word-break: break-word;">${v.shortDescription != null ? v.shortDescription : ""}</p>
                                <?php if(Authorisation::isInterfaceAdmin()){ ?>
                                  <button class="btn btn-sm btn-danger delete-events-<?= $kunik ?> hiddenPreview" data-id="${v.id}">
                                    Supprimer cet évènement
                                  </button>
                                  <button class="btn btn-sm btn-success edit-events-<?= $kunik ?> hiddenPreview" data-id="${v.id}">
                                    <?php echo Yii::t('cms', 'Edit this event')?>
                                  </button>
                                 <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                          <div class="event-img" style="background-image:url(${v.profilImageUrl})"></div>
                        </div>
                      </div>`;
                
              });
              $('.container<?php echo $kunik ?> .swiper-wrapper').html(htmlContent);
              $('.delete-events-<?= $kunik ?>').on('click',function(){
                    var idEvent = $(this).data('id');
                    bootbox.confirm(trad.areyousuretodelete, function(result){ 
                        if(result){
                          var url = baseUrl+"/"+moduleId+"/element/delete/id/"+idEvent+"/type/events";
                          var param = new Object;
                          ajaxPost(null,url,param,function(data){ 
                                  if(data.result){
                                    toastr.success(data.msg);
                                    urlCtrl.loadByHash(location.hash);
                                  }else{
                                    toastr.error(data.msg); 
                                }
                          })
                        }
                    })
              });
              $('.edit-events-<?= $kunik ?>').on('click',function(){
                dyFObj.editElement('events',$(this).data('id'));
              });
              var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                slidesPerView: 1,
                spaceBetween: 0,
                // init: false,
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                keyboard: {
                  enabled: true,
                },
                /*utoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },*/
                breakpoints: {
                  640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                  },
                  768: {
                    slidesPerView: 1,
                    spaceBetween: 40,
                  },
                  1024: {
                    slidesPerView: 1,
                    spaceBetween: 50,
                  },
                }
              });
              coInterface.bindLBHLinks();
          }
  ,"");

    function getDateFormated<?php echo $kunik ?>(params, onlyStr, allInfos){
        if(typeof params.recurrency != 'undefined' && params.recurrency){
          return directory.initOpeningHours(params, allInfos);
        }else{
          params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
          params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
          params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
          params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
          params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
          params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
            
          params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
          params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
          params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
          params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
          params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
          params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
          params.startDayNum = directory.getWeekDayName(params.startDayNum);
          params.endDayNum = directory.getWeekDayName(params.endDayNum);

          params.startMonth = directory.getMonthName(params.startMonth);
          params.endMonth = directory.getMonthName(params.endMonth);
          params.color='orange';
            

          var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
          var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
          mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
           
            
          var str = '';
          var dStart = params.startDay + params.startMonth + params.startYear;
          var dEnd = params.endDay + params.endMonth + params.endYear;
          mylog.log('DATEE', dStart, dEnd);

          if(params.startDate != null){
            if(notNull(onlyStr)){
              str+='<h5 class="" style="margin:0 !important">';
              if(params.endDate != null && dStart != dEnd)
                str +=  trad.fromdate;
              str += '<span class="">'+params.startDay+'</span>';
              if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
                str += ' <span class="">'+ params.startMonth+'</span>';
              if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
                str += ' <span class="">'+ params.startYear+'</span>';
              if(params.endDate != null && dStart != dEnd)
                str += ' <span class="">'+trad.todate+'</span> <span class="">'+params.endDay +'</span> <span class="">'+ params.endMonth +' '+ params.endYear+'</span>';
              str+='</h5>';

              str +=  '<span class="margin-top-5"><b><i class="fa fa-clock-o"></i> '+
                                    params.startTime+endTime+'</b></span>';
            
            }else{ 
              str += '<h5 class="title-5" style="margin:0 !important">'+
                        '<span>'+startLbl+' </span>'+
                        params.startDayNum+' '+params.startDay + ' ' + params.startMonth + 
                        ' <span class="">' + params.startYear + '</span>';
              if(params.collection == "events"){
              str +=  ' <span class="pull-right"><b><i class="fa fa-clock-o margin-left-10"></i> '+
                                    params.startTime+'</span>';
                  }
              str +=  '</h5>';

            }
          }    
            
          if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
            str += '<h5 class="title-5" style="margin:0 !important">'+
                            '<span>'+trad['todate']+' </span>'+
                            params.endDayNum+' '+params.endDay + ' ' + params.endMonth + 
                            ' <span class="">' + params.endYear + '</span>';
            if(params.collection == "events"){
            str += ' <span class="pull-right"><b><i class="fa fa-clock-o margin-left-10"></i> ' + 
                                      params.endTime+'</span>';
            }
            str +=  '</h5>';
          }   
          return str;
        }
  }
});
</script>

