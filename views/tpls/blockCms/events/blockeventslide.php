<?php
/**
 * TPLS QUI PERMET AFFICHAGE DES 3 DERNIERS NOUVEAUTÉS
 * MODÈLE INSPIRER DU COSTUM FILIÈRE NUMÉRIQUE
 * POSSIBILITÉ DE PARAMS LA COULEUR DE INFO
 */
$keytpl = "blockeventslide";

$paramsData = [ 
    "title" => "Block événements slide",
    "icon"  =>  "",
    "color" => "#000000",
    "txtcolor" => "#000000",
    "cardColor" => "#FFFFFF",
    "colorLabelButton" => "#ffffff",
    "colorBorder" => "#000"

];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style>
    .event_<?= $kunik?> img{
        height: 200px;
        width: 340px;
        border: 2px solid <?= $paramsData["colorBorder"]?>;
        border-radius: 10px;
        margin-bottom: 4%;
    }

    .event_<?= $kunik?> .plus {
        width: 39%;
        height: auto;
    }

    .event_<?= $kunik?> .ArrowLeft{
      width: 3vw;
      margin-top: 7vw;
      margin-left: -11vw;
    }

    .event_<?= $kunik?> .ArrowRight{
        transform: rotate(180deg);
        width: 3vw;
        margin-top: 7vw;
        margin-left: 8vw;
    }

    .event_<?= $kunik?> #Community{
        margin-top: 2%;
    }
    .event_<?= $kunik?> h1{
        color:<?= $paramsData["color"] ?>;
    }
    .itemctr_<?= $kunik?>  a{
        border-radius: 53px;
        color: <?= $paramsData["colorLabelButton"] ?>;
        background-color: <?= $paramsData["colorBorder"]?>;
        font-size: 17px;
        text-align: center;
        padding: 2% 4% 2% 4%;
        text-decoration: none;
    }
    .itemctr_<?= $kunik?>  .card-color{
        margin-bottom: 4%;
    }
     .event_<?= $kunik?> .btn-edit-delete{
        display: none;
        top: 20%;
        z-index: 1000;
    }
    .event_<?= $kunik?>:hover  .btn-edit-delete {
        display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        top:50%;
        left: 50%;
        transform: translate(-50%,-50%);
       
    }
</style>
<div class="event_<?= $kunik?>">
    <h1 class="text-center " > 
        <i class="fa <?= $paramsData['icon'] ?>"></i> 
        <?= $paramsData["title"] ?>
    </h1>
    <div class=" text-center btn-edit-delete">
        <?php 
           
            if(Authorisation::isInterfaceAdmin()){ ?>
            <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('event')">
                Ajouter un événement
            </button>            
    <?php } ?>
    </div>
    
    <div id="containEvent" class="col-xs-12">
        <div id="Community" class="col-lg-12 col-xs-12 col-md-12">
            <div class="no-padding carousel-border" >
                <div id="docCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner itemctr_<?= $kunik?>">

                    </div>
                    <div id="arrow-caroussel">
                        <a style="opacity: 1" class="carousel-control" href="#docCarousel" data-slide="prev">
                            <?php echo $this->renderPartial("costum.assets.images.templates.ArrowLeft"); ?>
                            <span class="sr-only"><?php echo Yii::t('cms', 'Previous')?></span>
                        </a>
                        <a style="opacity:1;margin-left: 82vw;" class="carousel-control" href="#docCarousel" data-slide="next">
                           <?php echo $this->renderPartial("costum.assets.images.templates.ArrowRight"); ?>
                           <span class="sr-only"><?php echo Yii::t('cms', 'Next')?></span>
                       </a>
                   </div>
               </div> 
           </div>
       </div>
   </div>
</div>
   
<script type="text/javascript">
tplCtx = {};
sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
$(document).ready(function(){
    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {
            "title" : "<?php echo Yii::t('cms', 'Configure the event block section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section on event blocks')?>",
            "icon" : "fa-cog",
            "properties" : {
                "title" : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                icon : { 
                    label : "<?php echo Yii::t('cms', 'Icon')?>",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                },
                color : {
                    label : "<?php echo Yii::t('cms', 'Title color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                }, 
                txtcolor : {
                    label : "<?php echo Yii::t('cms', 'Text color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.txtcolor
                },
                cardColor : {
                    label : "<?php echo Yii::t('cms', 'Color of the card')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.cardColor
                },

                colorBorder : {
                    label : "<?php echo Yii::t('cms', 'Color of the image border and button')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorder
                },
                 colorLabelButton : {
                    label : "<?php echo Yii::t('cms', 'Button label color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorLabelButton
                }
            },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
             save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                     else {
                      mylog.log("activeForm save tplCtx",tplCtx);
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }
                }
        }
    };
    
  $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

            var params = {};

    filters = <?php echo json_encode(@$thematique); ?> ;

    if (typeof filters != "undefined" && filters != "") {
        params.filters  = filters;
    }

            var params ={
                "source" : contextSlug
            };

            ajaxPost(
                null,
                baseUrl+"/costum/costumgenerique/geteventslide",
                params,
                 function(data){
                    mylog.log("success", data);
                    var str = "";
                    if(data.result == true)
                    {
                        var i = 0;
                        var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

                        str += "<div class='item active'>";

                        $(data.element).each(function(key,value){

                            var img = (typeof value.imageProfil  != "undefined" && value.imageProfil  != "") ? baseUrl+value.imageProfil : url;

                            var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != "" ? value.shortDescription : "<?php echo Yii::t('cms', 'No description')?>";

                            if (i >= 3) {
                                str += '</div>';
                                str += '<div class="item">';
                                i = 0;
                            }

                            i++;

                            str +='<div class="card text-center">';
                                str +='<div  class="card-color col-md-4">';
                                    str +='<img class="img-responsive" src="'+img+'">';
                                    str +='<p style="color : <?= $paramsData["txtcolor"]; ?>" class="name-event">'+value.name+'</p>';
                                    str +='<p style="color : <?= $paramsData["txtcolor"]; ?>" class="text-center">'+description+'</p>';
                                    str +='<a class="undefined entityName bold  add2fav  lbh-preview-element" href="#page.type.events.id.'+value.id+'"> <?php echo Yii::t("cms", "More information")?>';
                                        
                                    str +='</a>';
                                str +='</div>';
                            str +='</div>';                     
                        });
                    }
                    else
                    {
                        str += "<?php echo Yii::t('cms', 'There are no events')?>";
                    }
                    $(".itemctr_<?= $kunik?>").html(str);
                }
            );
        });
    </script>