<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
  <?php 
    $keyTpl ="agendaCarousel2";
    $paramsData = [ 

    	"title" => "AGENDA",
    	"titleColor" => "#f5833c",
    	"contenu" => " Nous pouvons ensemble créer un monde plus éthique.
    	Une Réunion plus forte en matière d’alimentation et de production
    	​pour notre Terre et nos Proches.",
    	"contenuColor"=>"#327753"
    ];

    if (isset($blockCms)) {
    	foreach ($paramsData as $e => $v) {
    		if (  isset($blockCms[$e]) ) {
    			$paramsData[$e] = $blockCms[$e];
    		}
    	}
    } 
    ?>
    <style>

    	.container<?php echo $kunik ?> .btn-edit-delete{
    		display: none;
    		z-index: 9999;
    	}
    	.container<?php echo $kunik ?>:hover .btn-edit-delete{
    		display: block;
    		position: absolute;
    		top:40%;
    		left: 50%;
    		transform: translate(-50%,-50%);
    	}
    	.title<?php echo $kunik ?>{
    		text-transform: none !important;
    		text-align: center;
    		color:<?php echo $paramsData["titleColor"] ?> !important;
    	}
    	.content_<?= $kunik?> p {
    		font-size: 20px;
    		color: <?= $paramsData["contenuColor"]?>;
    		margin-top: 2%;
    		margin-bottom: -1%;
    	}
    
    	.container<?php echo $kunik ?> .swiper-slide img{
    		vertical-align: middle;
    		width: 100%;
    		height: 300px;
    	}

    	.container<?php echo $kunik ?> .swiper-slide {
    		transition: all .2s ease-in-out;
    		height: 400px !important
    	}
    	.container<?php echo $kunik ?> .swiper-slide:hover {
    		transform: scale(1.1);
    	}
    	.container<?php echo $kunik ?> .owl-stage-outer {
    		padding: 35px 0px 0px 0px;
    		/*border: 1px solid;*/
    	}
    	.container<?php echo $kunik ?> .event-place{
    		color:#bca87d;
    		text-transform: none !important;
    	}
    	.container<?php echo $kunik ?> .event-name{
    		color: #000000;
    		text-transform: none !important;
    	}
    	.container<?php echo $kunik ?> .letter-orange{
    		color:#000000 !important;
    		/*text-align: center !important;*/
    		text-transform: capitalize !important;
    		font-weight: normal !important;
    	}

    	.blog_style1 {
    		margin-bottom: 40px;
    	}
    	.blog_style1 .blog_text {
    		padding-left: 40px;
    		padding-right: 40px;
    		padding-top: 0px;
    		padding-bottom: 0px;
    	}
    	.blog_text {
    		background: #fff;
    		padding: 115px 65px 65px 65px;
    	}

    	.blog_style1 .blog_text .blog_text_inner {
    		background: #fff;
    		margin-top: -100px;
    		position: relative;
    		-webkit-box-shadow: 0px 10px 20px 0px rgba(153, 153, 153, 0.1);
    		box-shadow: 0px 10px 20px 0px rgba(153, 153, 153, 0.1);
    		padding: 20px;
    	}
    	.blog_style1 .blog_text .blog_text_inner .cat {
    		margin-bottom: 25px;
    	}
    	.blog_style1.small .blog_text .blog_text_inner .cat .cat_btn {
    		display: block;
    		width: 86px;
    		margin-bottom: 12px;
    	}

    	.blog_style1 .blog_text .blog_text_inner .cat .cat_btn {
    		background: #f9f9ff;
    		color: #000;
    		border: 1px solid #e9e9e9;
    	}
    	.blog_text .cat .cat_btn {
    		background: #000000;
    		line-height: 30px;
    		color: #fff;
    		padding: 0px 20px;
    		border-radius: 15px;
    		display: inline-block;
    		font-size: 12px;
    		font-family: "Inconsolata", monospace;
    	}
    	.blog_text .cat a {
    		margin-right: 15px;
    	}
    	.blog_text .cat .title_<?= $kunik?>{
            margin-top: 5%;
    		color: #327753;
    		font-size: 20px;
    	}
    	.blog_text .cat .locality_<?= $kunik?> i{
    		font-size: 28px;
    		padding: 2px;
    	}
    	.blog_text .cat  i{
    		color: #f5833c;
    	}

    	.blog-img img{
    		width: 100%;
    	}
    	@media (max-width: 978px) {
    		.title<?php echo $kunik ?>{
    			font-size: 23px;
    			margin-top: 5%; 
    		}
    		.content_<?= $kunik?> p {
    			font-size: 14px;
    			margin-top: 2%;
    			margin-bottom: -1%;
    		}
    		.container<?php echo $kunik ?> .swiper-slide img{
    			vertical-align: middle;
    			width: 100%;
    			height: 180px;
    		}
    		.blog_text .cat .title_<?= $kunik?>{
    			font-size: 18px;
    		}
            .container<?php echo $kunik ?>  .blog_style1 {
                margin-bottom: 0px;
            }

        }
        .container<?php echo $kunik ?> .swiper-container {
            margin-top: 50px !important;
        }
    </style>
    <div class="container<?php echo $kunik ?> col-md-12">
    	<div class="btn-edit-delete text-center">
    		
    		<?php if(Authorisation::isInterfaceAdmin()){ ?>
    			<button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('event')"><?php echo Yii::t('cms', 'Add an event')?></button>
    		
    	<?php } ?>
        </div>
    	<h2 class="title title<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
    		<i class="fa fa-calendar"></i>
    		<?php echo $paramsData["title"] ?>
    	</h2>

    	<p class="content_<?= $kunik?> text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="contenu"> <?= $paramsData["contenu"]?></p>
     <div class="swiper-container">
        <div class="swiper-wrapper"></div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    </div>

    <script type="text/javascript">
    	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    	jQuery(document).ready(function() {
    		sectionDyf.<?php echo $kunik ?>Params = {
    			"jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Set up your agenda.')?>",
                    "description" : "<?php echo Yii::t('cms', 'Personalize your agenda')?>",
    				"icon" : "fa-cog",

    				"properties" : {
    					"titleColor" : {
    						"inputType" : "colorpicker",
    						"label" : "<?php echo Yii::t('cms', 'Title color')?>",
    						values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
    					} 
    				},
                      beforeBuild : function(){
                            uploadObj.set("cms","<?php echo $blockKey ?>");
                        },
    				save : function (data) {  
    					tplCtx.value = {};
    					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
    						tplCtx.value[k] = $("#"+k).val();
    						if (k == "parent")
    							tplCtx.value[k] = formData.parent;

    						if(k == "description")
    							tplCtx.value[k] = data.description;
    					});
    					console.log("save tplCtx",tplCtx);

    					if(typeof tplCtx.value == "undefined")
    						toastr.error('value cannot be empty!');
    					else {
                          dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
								toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            //   urlCtrl.loadByHash(location.hash);
                            });
                          } );
    					}

    				}
    			}
    		};

    		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
    			tplCtx.id = $(this).data("id");
    			tplCtx.collection = $(this).data("collection");
    			tplCtx.path = "allToRoot";
    			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    		});
    	});

    </script>

    <script>
    	$(function ($) {
    		
    		var htmlContent= "";

    		getAjax('',baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+
    			'/id/'+costum.contextId+'/dataName/events',
    			function(data){ 
    				mylog.log("andrana events",data);
    				$.each(data,function(k,v){
                         img = (typeof v.profilImageUrl != "undefined" || v.profilImageUrl != "null") ? v.profilImageUrl : "";
    					htmlContent += 
    					'<div class="swiper-slide blog_style1 small">'+
    					'<div class="blog_img text-center">'+
    					'<img class="img-fluid" src="'+img +'" alt="">'+
    					'</div>'+
    					'<div class="blog_text">'+
    					'<div class="blog_text_inner">'+
    					'<div class="cat">'+
    					'<h4>'+
    					'<a href="#page.type.'+v.collection+'.id.'+v.id+'" class="lbh-preview-element text-center event-name title_<?= $kunik?>" ">'+
    					v.name+
    					'</a>'+
    					'</h4>'+
    					'<h4>'+
    					'<a href="#page.type.'+v.collection+'.id.'+v.id+'" class=" locality_<?= $kunik?> lbh-preview-element">'+
    					'<i class="fa fa-map-marker "></i>'+'   '+
    					v.address.postalCode+' '+  v.address.addressLocality +' '+' '+
    					((typeof v.address.level1Name !="undefined") ? v.address.level1Name : "Addresse non renséigné")+
    					'</a> '+
    					'</h4>'+
    					directory.getDateFormated(v)+
    					'</div>'+
    					'</div>'+
    					'</div>'+
    					'</div>';
    				});
    				$('.container<?php echo $kunik ?> .swiper-wrapper').html(htmlContent);
                      var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        // init: false,
                        pagination: {
                          el: '.swiper-pagination',
                          clickable: true,
                        },
                        navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                        keyboard: {
                          enabled: true,
                        },
                       /* autoplay: {
                          delay: 2500,
                          disableOnInteraction: false,
                        },*/
                        breakpoints: {
                          640: {
                            slidesPerView: 1,
                            spaceBetween: 20,
                          },
                          768: {
                            slidesPerView: 2,
                            spaceBetween: 40,
                          },
                          1024: {
                            slidesPerView: 3,
                            spaceBetween: 50,
                          },
                        }
                      });
    				coInterface.bindLBHLinks();
    			}
    			,"");
    	});
    </script>




