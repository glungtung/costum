<?php 
    $keyTpl = "meteolamer-features";
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    $features = [
        [
            "img" =>"img1.png",
            "title"=>"Plus de spots sur l'ile de la Réunion",
            "description"=>"Meteolamer offre maintenant un plus grand nombre de pages spots repartis autour de l'ile. Comparez les prévisions pour planifiez vos journées a la plage ou votre sortie en mer.",
            "link"=>"#"
        ],
        [
            "img" =>"img2.png",
            "title"=>"Previsions marines sur l'Océan Indien",
            "description"=>"Comment sont les vagues a l'ile Maurice, a Mayotte, en Afrique du Sud ?... Meteolamer propose maintenant les previsions sur l'ensemble de bassin sud ouest de l'Ocean Indien.",
            "link"=>"#"
        ],
        [
            "img" =>"img3.png",
            "title"=>"Prévisions Vent et Météo a 7 jours",
            "description"=>"Une nouvelle page fournit les prévisions météorologiques a 7 jours sur l'Océan Indien: Vitesse et direction du vent, pression atmospherique, temperature de l'air et couverture nuageuse.",
            "link"=>"#"
        ],
        [
            "img" =>"img4.png",
            "title"=>"Marées et Calendrier Lunaire",
            "description"=>"Obtenez avec précision les horaires et l'amplitude des marées autour de l'ile, ainsi que les phases lunaires sur la semaine.",
            "link"=>"#"
        ],
        [
            "img" =>"img5.png",
            "title"=>"Un nouvel accès sur les réseaux sociaux",
            "description"=>"Partagez votre expérience et vos connaissances du milieu marin, les photos de votre session ou simplement de votre journée en mer avec les autres internautes.",
            "link"=>"#"
        ]
    ]
?>

<style>
    .<?= $keyTpl ?>-title{
        color: #708c8c;
        margin:100px 0px 30px 0px;
    }

    .<?= $keyTpl ?>-item{
        display:flex;
        border-bottom: 1px solid #dadada;
        margin-bottom: 30px;
    }

    .<?= $keyTpl ?>-item img{
        padding: 20px 80px 20px 0px;
    }
    .<?= $keyTpl ?>-item h5 a{
        font-size: 16px;
        text-transform: none;
        text-decoration:none;
        font-weight:bold;
    }

    .<?= $keyTpl ?>-item p{
        font-size: 14px;
        color: #708c8c;
    }

    @media only screen and (max-width: 1300px){
        .<?= $keyTpl ?>-title{
            font-size: 20px;
            margin:30px 0px;
        }

        .<?= $keyTpl ?>-item img {
            padding: 20px;
        }
    }
</style>

<div class="sp-cms-container <?= $keyTpl ?>-container">
    <h1 class="<?= $keyTpl ?>-title">Découvrez la nouvelle version de meteolamer.re</h1>
    <div>
        <?php foreach($features as $feature){ ?>
        <div class="<?= $keyTpl ?>-item">
            <div>
                <img src="<?= $assetsUrl ?>/images/meteolamer/features/<?= $feature["img"] ?>" alt="">
            </div>
            <div>
                <h5><a href="<?= $feature["link"] ?>"><?= $feature["title"] ?></a></h5>
                <p><?= $feature["description"] ?></p>
            </div>
        </div>
        <?php } ?>
    </div>
</div>