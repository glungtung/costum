<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $spots = [
        [
            "name"=>"stdenis",
            "label"=>"St Denis"
        ],
        [
            "name"=>"stleu",
            "label"=>"St Leu"
        ],
        [
            "name"=>"stpierre",
            "label"=>"St Pierre"
        ],
        [
            "name"=>"stbenoit",
            "label"=>"St Benoit"
        ]
    ];

    //active dates for the next 7 days
    $start_date = date("Y-m-d");
    $end_date = date("Y-m-d", strtotime($start_date." +6 day"));

    $spotsData = Meteolamer::getSpotsDataBetweenDates($spots, $start_date, $end_date);
    $dataReunion = Meteolamer::getDataBetweenDates("reunion", $start_date, $end_date);

    $current_date = $start_date;
?>

<style>
    .meteolamer-tide-header h1{
        font-size: 34px;
        color:#708c8c;
    }
    .meteolamer-tide-container{
        display:flex;
        margin-bottom: 50px;
    }
    .meteolamer-tide-map-container{
        width:400px;
    }
    .meteolamer-tide-map-container img{
        width:100%;
    }
    .meteolamer-tide-table-container{
        width: calc(100% - 350px);
        margin-left:50px;
    }
    .meteolamer-tide-table-container table{
        width: 100%;
        display: table;
        border-collapse: separate;
        box-sizing: border-box;
        text-indent: initial;
        border-spacing: 2px;
        border-color: grey;
    }
    .meteolamer-tide-table-container table th{
        background:#c1d7d7;
        padding: 15px 10px;
        color:#708c8c;
        white-space:nowrap;
    }
    .meteolamer-tide-table-container table td{
        padding: 10px;
        background:#f4f8f8;
        text-align:center;
    }
    .meteolamer-tide-table-container table tr td:first-child{
        text-align: left;
    }

    @media only screen and (max-width: 1000px){
        .meteolamer-tide-container{
            flex-direction:column;
        }
        .meteolamer-tide-table-container{
            margin-left:0;

        }

        .meteolamer-tide-map-container{
            width:100%;
        }
        .meteolamer-tide-table-container{
            width:100%;
            margin-left:0;
            overflow: auto;
            margin-top:20px;
        }

        .meteolamer-tide-header h1{
            text-align: left !important;
            font-size:22px;
            padding-bottom:10px;
        }
    }
</style>

<div class="container">
    <?php  while(strtotime($current_date) <= strtotime($end_date)){ ?>
        <div class="text-right meteolamer-tide-header">
            <h1>
                <?php
                    setlocale(LC_TIME,'fr_FR.utf8','fra');
                    echo strftime('%A %d %B', strtotime($current_date)); 
                ?>
            </h1>
        </div>
        <div class="meteolamer-tide-container">
            <div class="meteolamer-tide-map-container">
                <?php if(isset($dataReunion[$current_date]["data"]["h00"]["tide"]["map"])){ ?>
                <img src="<?= Meteolamer::$BASE_URL.str_replace("./data","",$dataReunion[$current_date]["data"]["h00"]["tide"]["map"]) ?>" alt="">
                <?php } ?>
            </div>
            <div class="meteolamer-tide-table-container">
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Marée Haute</th>
                            <th>Amplitude</th>
                            <th>Marée Basse</th>
                            <th>Amplitude</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($spots as $spot){
                            if(isset($spotsData[$spot["name"]][$current_date]["data"]["h06"]["tide"])){
                                $data = $spotsData[$spot["name"]][$current_date]["data"]["h06"]["tide"];
                        ?>
                        <tr>
                            <td><?= $spot["label"] ?></td>
                            <td><?= $data["high"][0]["time"] ?> <?= $data["high"][1]["time"] ?></td>
                            <td><?= $data["high"][0]["level"] ?>m</td>
                            <td><?= $data["low"][0]["time"] ?> <?= $data["low"][1]["time"] ?></td>
                            <td><?= $data["low"][0]["level"] ?>m</td>
                        </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php 
        $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
    }?>
</div>