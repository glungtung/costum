<?php
    $ASSETS_URL = Yii::app()->getModule('costum')->assetsUrl;
    //Constants
    $DEFAULT_SPOT = "home";
    $PARTIALS = [
        "wave.histogram", 
        "wave.height", 
        "wave.direction", 
        "wave.period", 
        "wave.windDirection",
        ["name"=>"wave.tide", "params" => ["type"=>"high"]],
        ["name"=>"wave.tide", "params" => ["type"=>"low"]],
    ];

    //retrieve current spot from reguest
    $spotName = Yii::app()->getRequest()->getQuery('spot');
    $spotName = $spotName ? $spotName : $DEFAULT_SPOT;

    //get all information of the spot
    $spot = Meteolamer::getSpotByName($spotName);

    //active dates for the next 7 days
    $start_date = date("Y-m-d");
    $end_date = date("Y-m-d", strtotime($start_date." +6 day"));

    //retrieve days's data
    $weekly_data = Meteolamer::getDataBetweenDates($spotName, $start_date, $end_date);

    //preload all map baseline
    for($i=0; $i<=10; $i++){
        echo '<link rel="preload" href="'.$ASSETS_URL.'/images/meteolamer/baseline/baseline_m'.$i.'.png" as="image">';
    }
?>

<style>
    button{
        outline: none !important;
    }

    .meteolamer-wave-header{
        width:100%;
        display:flex;
        justify-content:space-between;
        align-items:center;
    }
    .meteolamer-wave-header h1{
        font-size: 34px;
        color:#708c8c;
    }
    .meteolamer-wave-container{
        display:flex;
    }
    .wave-map-container{
        width:420px;
        height:420px;
        position:relative;
        background-size:cover;
        transition: .4s;
        border-radius:6px;
        border: 1px solid #eaeaea;
    }
    .wave-map-container img{
        width:100%;
        height:100%;
        position:absolute;
        border-radius:6px;
    }
    .wave-map-container img:last-child{
        z-index:2;
    }
    .wave-map-baseline-actions{
        width:100%;
        height:100%;
        position:absolute;
        z-index:3;
        border-radius:6px;
    }
    .wave-table-container{
        width:calc(100% - 420px);
    }
    .wave-table-container table{
        width: 100%;
        display: table;
        border-collapse: separate;
        box-sizing: border-box;
        text-indent: initial;
        border-spacing: 2px;
        border-color: grey;
    }
    .wave-table-container table th{
        text-align: center;
        padding: 2px;
        font-size: 11px;
        color: #707070;
    }
    .wave-table-container table th span{
        display: inline-block;
        width: 100%;
        padding: 4px 1px;
        color: #344848;
        border: 1px solid #eaeaea;
        border-radius: 4px;
        background-color: #c1d7d7;
        white-space:nowrap;
    }
    .wave-table-container table tbody{
        border-top: 15px solid transparent;
        background: #f4f8f8;
    }
    .wave-table-container table tr td:first-child{
        background:white;
    }

    .wave-table-container table tbody tr{
        height:30.75px;
    }

    .wave-table-container table tbody tr:nth-child(6), .wave-table-container table tbody tr:nth-child(7){
        height: 56px;
    }

    .wave-tr-heght-bar ul{
        height: 120px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        text-align:right;
    }
    .wave-tr-heght-bar ul li{
        font-size: 12px;
        list-style: none;
        color: #707070;
        position: relative;
        line-height: 0;
    }

    .wave-td-height-bar{
        vertical-align: bottom;
    }

    .wave-bar-wrapper{
        position:relative;
        overflow-y:hidden;
        max-height: 120px;
        bottom: 0;
    }
    .wave-bar-wrapper div{
        position:absolute;
        bottom:0;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        width: calc(100% - 4px);
        margin: 0px 2px;
    }
    .wave-bar-wrapper .wave-bar-height{
        background: #ed7322;
    }
    .wave-bar-wrapper .wave-bar-swellheight{
        background:#f7ce3b;
    }
    .wave-bar-tooltip{
        background-color: black;
        width: fit-content;
        color: white;
        font-size: 14px;
        border-radius: 4px;
    }
    .wave-bar-tooltip ul{
        margin: 0;
        padding: 0;
        height: fit-content !important;
        display: block !important;
    }
    .wave-bar-tooltip ul li{
        list-style: none;
        color: white !important;
        line-height: 22px !important;
        text-align: left !important;
        width: max-content;
    }
    .wave-bar-tooltip ul li span{
        display: inline-block;
        width: 10px;
        height: 10px;
    }
    .wave-bar-tooltip ul li:first-child span{
        background-color: #ff6700;
    }
    .wave-bar-tooltip ul li:last-child span{
        background-color: #ffcc00;
    }

    .wave-tr-title span{
        display:block;
        padding: 4px;
        font-size:12px;
        color:#666666;
    }
    .wave-tr-hsig span, .wave-tr-period span{
        display:block;
        font-size:14px;
        padding: 2px 4px;
        text-align:center;
    }

    .wave-tr-tide p{
        padding:0;
        margin:0;
        font-size:14px;
        padding: 2px 4px;
        text-align:center;
    }

    .wave-td-data{
        cursor:pointer;
        transition: .3s;
    }

    .wave-td-data.active{
        background: #e4e6e6;
    }

    .data-title{
        text-align:right;
        width:40px;
    }

    .data-title span{
        text-align:right;
        font-size: 12px;
        white-space: wrape;
        color:#707070;
    }

    /* map baseline actions styles =============> */
    .btn-map-zoom{
        position: absolute;
        border: none;
        width: 45px;
        height: 45px;
        border-radius: 100%;
        font-size: 1.8em;
        text-align: center;
        line-height: 45px;
        left: 7px;
        top: 5px;
        transition: .3s;
    }
    .btn-map-zoom:hover{
        background-color: #0093d0;
        color: white;
    }
    .btn-map-spot-m2{
        position: absolute;
        border: none;
        width: 88px;
        height: 88px;
        text-align: center;
        line-height: 88px;
        opacity: 0;
        cursor:pointer;
        transition: .3s;
        background-color: #0093d0;
        color: white;
        display: block;
    }
    .btn-map-spot-m2:hover{
        opacity: .95;
        text-decoration: none;
        color: white;
    }
    .btn-map-spot-m0{
        font-weight: bold;
        position: absolute;
        border: none;
        text-align: center;
        transition: .3s;
        font-size: 12px;
        border-radius: 50px;
        background-color: #0093d0;
        color: white;
        opacity:0;
        display: block;
        text-decoration: none;
        padding: 2px 5px;
        cursor: pointer;
    }
    .btn-map-spot-m0:hover{
        opacity: 1;
        text-decoration: none;
        color: white;
    }
    .btn-map-spot-navigation {
        position: absolute;
        width: 38px;
        border: none;
        height: 28px;
        transition:.3s;
    }
    .btn-map-spot-navigation-up{
        border-radius: 0px 0px 10px 10px;
    }
    .btn-map-spot-navigation-down{
        border-radius: 10px 10px 0px 0px;
    }
    .btn-map-spot-navigation-top{
        border-radius: 0px 0px 10px 10px;
    }
    .btn-map-spot-navigation-right{
        width: 30px;
        height: 36px;
        border-radius: 10px 0px 0px 10px;
    }
    .btn-map-spot-navigation-left{
        width: 30px;
        height: 36px;
        border-radius: 0px 10px 10px 0px;
    }
    .btn-map-spot-navigation:hover{
        background-color: #0093d0;
        color: white;
    }
    /* <============== map baseline actions styles */

    @media only screen and (max-width: 1300px){
        .meteolamer-wave-container{
            flex-direction:column;
        }
        .wave-table-container{
            width:100%;
            overflow-x:auto;
        }

        .wave-map-container{
            position: relative;
            width: 100%;
            height: 0;
            padding-bottom: 100%;
            margin-bottom:20px;
        }

        .meteolamer-wave-header{
            display:flex;
            flex-direction:column;
            align-items: flex-start;
            margin-bottom: 15px;
        }

        .meteolamer-wave-header h1:first-child{
            font-size:20px;
            font-weight: bold;
        }

        .meteolamer-wave-header h1{
            font-size:16px;
            font-weight: 500;
            margin: 5px;
        }
    }

    .wave-height-popup{
        width: 120px;
        position: absolute;
        white-space: nowrap;
        left: -60px!important;
        z-index: 99999;
        padding: 4px;
        background: black;
        color: white;
        visibility: hidden;
    }

    .wave-height-popup:first-child{
        visibility: visible;
    }

    .animate-height{
        animation-name: animationHeight;
        animation-duration: 1s;
        animation-fill-mode: both;
        animation-timing-function: ease-in;
        max-height: 0;
    }

    @keyframes animationHeight{
        from{
            max-height: 0;
        }
        to{
            max-height: 120px;
        }
    }

    .animate-rotate{
        animation-name: animationRotate;
        animation-duration: 1s;
    }
    @keyframes animationRotate{
        from{
            transform: rotate(90deg);
        }
        to{
            transform: rotate(0deg);
        }
    }

    .wave-map-loader{
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: white;
        border-radius: 6px;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .wave-map-loader img{
        width: 70px;
        height: 70px;
    }
</style>

<div class="container">
    <div class="meteolamer-wave-header">
        <h1 id="meteolamer-active-spot-name<?= $kunik ?>" class="meteolamer-active-spot-name"><?= isset($spot["label"])?$spot["label"]:"" ?></h1>
        <h1 class="meteolamer-active-time"></h1>
    </div>
    <div class="meteolamer-wave-container">
        <div class="wave-map-container" id="wap-map-container-<?= $kunik ?>">
            <div class="wave-map-loader" style="display: none;">
                <img src="<?= $ASSETS_URL ?>/images/meteolamer/spinner.svg"/>
            </div>
            <img src="" class="wave-map">
            <img src="" class="wave-map-baseline">
            <div class="wave-map-baseline-actions"></div>
        </div>
        <div class="wave-table-container" id="wave-table-container<?= $kunik ?>">
            <?= $this->renderPartial("costum.views.tpls.blockCms.meteolamer.tables.wave.index", [
                "weekly_data"=>$weekly_data,
                "start_date"=>$start_date,
                "end_date"=>$end_date
            ]); ?>
        </div>
    </div>
</div>

<script>
    var BASE_URL_RESSOURCE = "<?= Meteolamer::$BASE_URL ?>",
        ASSETS_URL = "<?=  $ASSETS_URL ?>",
        COSTUM_BASE_URL = ((new URL(baseUrl)).host == costum.host)?baseUrl:baseUrl+"/costum/co/index/slug/meteolamer";

    var spots = <?= json_encode(Meteolamer::getSpots()) ?>;
        
    var mapBaselineActions<?= $kunik ?> = {
        actions: {
            m0:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Seychelles",
                    "spot": "seychelles",
                    "position": {
                        "top": 0.093,
                        "left": 0.5
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Mayotte",
                    "spot": "mayotte",
                    "position": {
                        "top": 0.198,
                        "left": 0.255
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Toamasina",
                    "spot": "toamasina",
                    "position": {
                        "top": 0.333,
                        "left": 0.386
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Maurice",
                    "spot": "maurice",
                    "position": {
                        "top": 0.371,
                        "left": 0.552
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Reunion",
                    "spot": "reunion",
                    "position": {
                        "top": 0.438,
                        "left": 0.488
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Toliara",
                    "spot": "toliara",
                    "position": {
                        "top": 0.443,
                        "left": 0.152
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Durban",
                    "spot": "durban",
                    "position": {
                        "top": 0.519,
                        "left": 0.06
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Kerguelen",
                    "spot": "kerguelen",
                    "position": {
                        "top": 0.95,
                        "left": 0.721
                    }
                }
            ],
            m1:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Seychelles",
                    "spot": "seychelles",
                    "position": {
                        "top": 0.093,
                        "left": 0.495
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Mayotte",
                    "spot": "mayotte",
                    "position": {
                        "top": 0.195,
                        "left": 0.252
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Toamasina",
                    "spot": "toamasina",
                    "position": {
                        "top": 0.336,
                        "left": 0.388
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Maurice",
                    "spot": "maurice",
                    "position": {
                        "top": 0.374,
                        "left": 0.552
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Reunion",
                    "spot": "reunion",
                    "position": {
                        "top": 0.438,
                        "left": 0.495
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Toliara",
                    "spot": "toliara",
                    "position": {
                        "top": 0.445,
                        "left": 0.152
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Durban",
                    "spot": "durban",
                    "position": {
                        "top": 0.519,
                        "left": 0.057
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Kerguelen",
                    "spot": "kerguelen",
                    "position": {
                        "top": 0.948,
                        "left": 0.729
                    }
                }
            ],
            m2:[
                {
                    className:"btn-map-spot-m2",
                    content:"St Denis",
                    spot:"stdenis",
                    position:{ top:0.075, left: 0.3},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Paul",
                    spot:"stpaul",
                    position:{ top:0.17, left: 0.071},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Leu",
                    spot:"stleu",
                    position:{ top:0.392, left: 0.071},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Pierre",
                    spot:"stpierre",
                    position:{ top:0.564, left: 0.297},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"Manapany",
                    spot:"manapany",
                    position:{ top:0.676, left: 0.523},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Benoit",
                    spot:"stbenoit",
                    position:{ top:0.328, left: 0.647},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"Ste Marie",
                    spot:"stemarie",
                    position:{ top:0.102, left: 0.528},
                    size:{ width:0.22, height:0.22 }
                },
                {
                    className:"btn-map-spot-m2",
                    content:"St Philippe",
                    spot:"stphil",
                    position:{ top:0.554, left: 0.754},
                    size:{ width:0.22, height:0.22 }
                }
            ],
            m3:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Le Port Ouest",
                    "spot": "portoues",
                    "position": {
                        "top": 0.486,
                        "left": 0.786
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "St Paul",
                    "spot": "stpaul",
                    "position": {
                        "top": 0.788,
                        "left": 0.843
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Boucan-Canot",
                    "spot": "boucan",
                    "position": {
                        "top": 0.914,
                        "left": 0.617
                    }
                }
            ],
            m4:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Boucan Canot",
                    "spot": "boucan",
                    "position": {
                        "top": 0.026,
                        "left": 0.629
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Roches Noires",
                    "spot": "roches",
                    "position": {
                        "top": 0.124,
                        "left": 0.624
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "L'Hermitage",
                    "spot": "lhermit",
                    "position": {
                        "top": 0.295,
                        "left": 0.624
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Trois Bassins",
                    "spot": "troisbas",
                    "position": {
                        "top": 0.429,
                        "left": 0.729
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "St Leu",
                    "spot": "stleu",
                    "position": {
                        "top": 0.721,
                        "left": 0.89
                    }
                }
            ],
            m5:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Etang Salé",
                    "spot": "etangsal",
                    "position": {
                        "top": 0.076,
                        "left": 0.179
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "St Pierre",
                    "spot": "stpierre",
                    "position": {
                        "top": 0.388,
                        "left": 0.821
                    }
                }
            ],
            m6:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Manapany",
                    "spot": "manapany",
                    "position": {
                        "top": 0.321,
                        "left": 0.419
                    }
                }
            ],
            m7:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "St Benoit",
                    "spot": "stbenoit",
                    "position": {
                        "top": 0.412,
                        "left": 0.195
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Ste Rose",
                    "spot": "sterose",
                    "position": {
                        "top": 0.893,
                        "left": 0.619
                    }
                }
            ],
            m8:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Le Port",
                    "spot": "leport",
                    "position": {
                        "top": 0.929,
                        "left": 0.081
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "St Denis",
                    "spot": "stdenis",
                    "position": {
                        "top": 0.688,
                        "left": 0.814
                    }
                }
            ],
            m9:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "Ste Marie",
                    "spot": "stemarie",
                    "position": {
                        "top": 0.75,
                        "left": 0.186
                    }
                },
                {
                    "className": "btn-map-spot-m0",
                    "content": "Ste Suzanne",
                    "spot": "stesuzanne",
                    "position": {
                        "top": 0.824,
                        "left": 0.505
                    }
                }
            ],
            m10:[
                {
                    "className": "btn-map-spot-m0",
                    "content": "St Philippe",
                    "spot": "stphil",
                    "position": {
                        "top": 0.49,
                        "left": 0.248
                    }
                }
            ]
        },
        render: function(container, imap){
            var $container = $(container).html("");
            var actions = mapBaselineActions<?= $kunik ?>.actions["m"+imap];
            
            actions.forEach(function(action){
                var buttonStyle = `
                    top:${ action.position.top * $container.width() }px;
                    left:${ action.position.left * $container.width() }px; 
                `
                if(action.size){
                    buttonStyle += `
                        width:${action.size.width * $container.width()}px;
                        height:${action.size.height * $container.width()}px;
                        line-height:${action.size.height * $container.width()}px;
                    `
                }
                var button = $(`<a href="${COSTUM_BASE_URL}#vague?spot=${action.spot}" class="${action.className}" style="${buttonStyle}">${action.content}</a>`)
                button.click(function(){
                    getTable<?= $kunik ?>(action.spot)
                })
                $container.append(button);
            })
        }
    }

    function renderMap<?= $kunik ?>(map, imap){
        if(map){
            $(".wave-map-baseline").css({visibility:"visible"})
            $(".wave-map-baseline-actions").css({visibility:"visible"})

            $(".wave-map").attr("src", `${BASE_URL_RESSOURCE}${map.replace("./data","")}`)
            $(".wave-map-baseline").attr("src", `${ASSETS_URL}/images/meteolamer/baseline/baseline_m${imap}.png`)
            mapBaselineActions<?= $kunik ?>.render(".wave-map-baseline-actions", imap);
        }else{
            $(".wave-map-baseline").css({visibility:"hidden"})
            $(".wave-map-baseline-actions").css({visibility:"hidden"})

            $(".wave-map").attr("src", `${ASSETS_URL}/images/meteolamer/img_not_found.jpg`)
        }
    }

    function init<?= $kunik ?>(){
        $(".wave-td-data").hover(function(){
            var time = $(this).data("time"),
                day = $(this).data("day"),
                map = $(this).data("map"),
                imap = $(this).data("imap"),
                date = $(this).data("date");

            $(".wave-td-data").removeClass("active")
            $(`.wave-td-data-day${day}-${time}`).addClass("active")
            $(".meteolamer-active-time").text(moment.unix(date).format("dddd DD MMMM"))
            renderMap<?= $kunik ?>(map, imap)
        })

        setTimeout(() => {
            $(".wave-td-data").first().trigger("mouseenter")
        }, 500);

        $('[data-toggle="tooltip"]').tooltip({ 
            html:true,
            trigger: "manuel"
        }).click(function(){
            $(this).tooltip("show")
        }).mouseleave(function(){
            $(this).tooltip("hide")
        })
    }

    $(function(){
        init<?= $kunik ?>()
    })

    var startDate = "<?= $start_date ?>",
        endDate = "<?= $end_date ?>",
        spotName = "<?= $spotName ?>",
        assetsUrl = "<?= $ASSETS_URL ?>";

    $(".meteolamer-spot-nav").click(function(e){
        var spotName = $(this).data("spot")
        if(spotName){
            getTable<?= $kunik ?>(spotName)
        }
    })

    $("#spot-select-navigation").change(function(){
        getTable<?= $kunik ?>($(this).val())
        window.location.replace(`${location.pathname}#vague?spot=${$(this).val()}`)
    })

    function getTable<?= $kunik ?>(spotName){
        var $waveMapContainer = $("#wap-map-container-<?= $kunik ?>");
        $waveMapContainer.find(".wave-map, .wave-map-baseline, .wave-map-baseline-actions").hide()
        $waveMapContainer.find(".wave-map-loader").show()
        $(`#meteolamer-active-spot-name<?= $kunik ?>`).text(spots[spotName]?spots[spotName].label:"")
        $.get(`/costum/meteolamer/getwavetable/spot/${spotName}/start_date/${startDate}/end_date/${endDate}`, function(content){
            $("#wave-table-container<?= $kunik ?>").html(content)
            init<?= $kunik ?>()

            $waveMapContainer.find(".wave-map").on("load", function(){
                setTimeout(function(){
                    $waveMapContainer.find(".wave-map, .wave-map-baseline, .wave-map-baseline-actions").show()
                    $waveMapContainer.find(".wave-map-loader").hide()
                }, 300)
            })
        })
    }
</script>