<script>
    var ASSETS_URL = "<?= Yii::app()->getModule('costum')->assetsUrl ?>";
    var DIR_DEGREE = {
		e:90,
		n:0,
		ne:45,
		nw:315,
		s:180,
		se:135,
		sw:225,
		w:270,
		getWindDegree: (dir) => {
			var degree = DIR_DEGREE[dir];
			return (degree > 180)?(degree - 180):(degree + 180);
		} 
	}
</script>

<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    $scrips = array(
        "/js/meteolamer/jquery-meteolamer-card.js",
		"/js/meteolamer/jquery-meteolamer-map.js",
		"/js/meteolamer/meteolamer-map.js",
    );
    $styles = array(
        "/css/meteolamer/meteolamer-daily-forecast.css",
        "/css/meteolamer/meteolamer-weekly-forecast.css",
        "/css/meteolamer/meteolamer-map.css"
    );
    HtmlHelper::registerCssAndScriptsFiles($scrips, $assetsUrl);
    HtmlHelper::registerCssAndScriptsFiles($styles, $assetsUrl);
?>

<div class="row">
    <div class="col-sm-12 meteolamer-map-header-nav-md" style="padding:20px;">
        <div>
            <div class="meteolamer-map-search-bar">
                <input type="text" placeholder="Rechercher un spot" class="meteolamer-map-search-input">
                <i class="fa fa-search" aria-hidden="true"></i>
            </div>
            <div class="daily-forecast-header">
                <ul class="day-navigation">
                    <li class="btn-day-navigation" data-action="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
                    <li class="date-forecast" style="text-transform: capitalize; text-align:center;"></li>
                    <li class="btn-day-navigation" data-action="next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="wind-wave-forecast-container" style="padding-bottom: 10px;display:flex; justify-content:center; align-items:center">
                <ul class="time-navigation" id="day-forecast-times">
                    <li data-time="00" class="active">00:00</li>
                    <li data-time="06">06:00</li>
                    <li data-time="12">12:00</li>
                    <li data-time="18">18:00</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div style="position:relative;width:100%;">
            <div class="meteolamer-legende-container">
                <div class="legend-header active">
                    <p>Legende <i class="fa fa-angle-down"></i></p>
                </div>
                <div class="legend-body">
                    <div id="meteolamer-wave-legende" style="margin-right:10px">
                        <div class="legende-title">
                            <p>Vague</p>
                            <p>Hauteur en metre</p>
                        </div>
                        <div class="ledende-content"></div>
                    </div>
                    <div id="meteolamer-wind-legende" style="margin-left:10px">
                        <div class="legende-title">
                            <p>Vent</p>
                            <p>Vitesse en knot</p>
                        </div>
                        <div class="ledende-content"></div>
                    </div>
                </div>
            </div>
            <div id="meteolamerMapContainer">
            </div>
        </div>
    </div>
    <div class="col-md-4" style="background-color:white;">
        <div class="row">
            <div class="col-sm-12 meteolamer-map-header-nav-lg" style="padding:20px;">
                <div>
                    <div class="meteolamer-map-search-bar">
                        <input type="text" placeholder="Rechercher un spot" class="meteolamer-map-search-input">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                    <div class="daily-forecast-header">
                        <ul class="day-navigation">
                            <li class="btn-day-navigation" data-action="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
                            <li class="date-forecast" style="text-transform: capitalize; text-align:center;"></li>
                            <li class="btn-day-navigation" data-action="next"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="wind-wave-forecast-container" style="padding-bottom: 10px;display:flex; justify-content:center; align-items:center">
                        <ul class="time-navigation" id="day-forecast-times">
                            <li data-time="00" class="active">00:00</li>
                            <li data-time="06">06:00</li>
                            <li data-time="12">12:00</li>
                            <li data-time="18">18:00</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 meteolamer-map-cards-container">
                <div id="meteolamer-map-wave-container"></div>
                <div id="meteolamer-map-wind-container"></div>
                <div style="padding: 15px 5px 5px 5px;">
                    <div id="meteolamer-map-tide-container"></div>
                    <div id="meteolamer-map-weather-container"></div>
                </div>
            </div>
        </div>
    </div>
</div>