<?php
    $current_date = $startDate;
    $dayCount = 0;

    if(!function_exists("getWaveBarTooltip")){
        function getWaveBarTooltip($waveHeight, $swellHeight){
            return "
                <div class='wave-bar-tooltip'>
                    <ul>
                        <li>
                            <span></span> Hauteur significative: ".number_format(round($waveHeight, 1), 1)."m
                        </li>
                        <li>
                            <span></span> Hauteur de houle propre: ".number_format(round($swellHeight, 1), 1)."m
                        </li>
                    </ul>
                </div>
            ";
        }
    }
?>

<tr class="wave-tr-heght-bar">
    <td colspan="4">
        <ul>
            <li>4 -</li>
            <li>3 -</li>
            <li>2 -</li>
            <li>1 -</li>
            <li>0 -</li>
        </ul>
    </td>
    <?php  
        while(strtotime($current_date) <= strtotime($endDate)){ 
            foreach(Meteolamer::$TIMES as $time){
                $waveData = isset($data[$current_date]["data"]["h".$time]["wave"])?
                        $data[$current_date]["data"]["h".$time]["wave"]:NULL;
    ?>
        <td
            class="wave-td-height-bar wave-td-data wave-td-data-day<?= $dayCount ?>-h<?= $time ?>"
            data-day="<?= $dayCount ?>"
            data-time="h<?= $time ?>"
            data-map="<?= ($waveData ? $waveData["map"]:"") ?>"
            data-imap="<?= ($waveData ? $waveData["imap"]: "") ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
            <?php if($waveData){ ?>
                <div class="wave-bar-wrapper" style="height:<?= ($waveData["height"]*30) ?>px" data-toggle="tooltip" title="<?= getWaveBarTooltip($waveData["height"], $waveData["swellHeight"]) ?>">
                        <div class="animate-height" style="height:<?= ($waveData["height"]*30) ?>px; background: #ff6700"></div>
                        <div class="animate-height" style="height:<?= ($waveData["swellHeight"]*30) ?>px; background: #ffcc00"></div>
                    </div>
                </td>
            <?php } ?>
        </td>
    <?php 
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        } 
    ?>
</tr>