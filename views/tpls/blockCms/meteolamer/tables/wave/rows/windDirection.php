<?php
    $current_date = $startDate;
    $dayCount = 0;
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $ACTIVE_TIMES = ["h06", "h18"];
?>

<tr>
    <td colspan="4" class="data-title"><span>Vent</span></td>
    <?php
        while(strtotime($current_date) <= strtotime($endDate)){
            foreach($ACTIVE_TIMES as $time){
                $waveData = isset($data[$current_date]["data"][$time]["wave"])?
                            $data[$current_date]["data"][$time]["wave"]:NULL;
                $className = "wave-td-data-day".$dayCount."-".$time;
                $className .= " wave-td-data-day".$dayCount."-".(($time=="h06")?"h00":"h12");
    ?>
        <td
            colspan="2" 
            class="text-center wave-td-data <?= $className ?>"
            data-day="<?= $dayCount ?>" 
            data-time="<?= $time ?>"
            data-map="<?= ($waveData?$waveData["map"]:"") ?>"
            data-imap="<?= ($waveData?$waveData["imap"]:"") ?>"
            data-date="<?= (strtotime($current_date)) ?>"    
        >
        <?php if(isset($waveData["windDirection"])){ ?>
        <img class="animate-rotate" src="<?= $assetsUrl.'/images/meteolamer/dirwind/'.$waveData["windDirection"] ?>.png" alt="">
        <?php } ?>
        </td>
    <?php
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>