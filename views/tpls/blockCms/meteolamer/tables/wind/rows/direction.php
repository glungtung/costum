<?php
    $current_date = $startDate;
    $dayCount = 0;
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $ACTIVE_TIMES = ["h06","h18"];
?>
<tr>
    <td colspan="4" class="data-title"><span>Direction</span></td>
    <?php
        while(strtotime($current_date) <= strtotime($endDate)){ 
            foreach($ACTIVE_TIMES as $time){
                $windData = isset($data[$current_date]["data"][$time]["wind"])?
                        $data[$current_date]["data"][$time]["wind"]:NULL;
                $className = "text-center wind-td-data wind-td-data-day".$dayCount."-".$time;
                $className .= " wind-td-data-day".$dayCount."-".(($time=="h06")?"h00":"h12");
    ?>
        <td
            colspan="2"
            class="<?= $className ?>"
            data-day="<?=$dayCount?>" 
            data-time="<?= $time ?>"
            data-map="<?= $windData?str_replace("./data","",$windData["map"]):"" ?>"
            data-imap="<?= $windData?$windData["imap"]:"" ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
            <?php if($windData){ ?>
                <img class="animate-rotate" src='<?= $assetsUrl ?>/images/meteolamer/dirwind/<?= $windData["direction"] ?>.png'/>
            <?php } ?>
        </td>
    <?php
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>