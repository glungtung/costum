<?php
    $current_date = $startDate;
    $dayCount = 0;
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $ACTIVE_TIME = "h06";

    if(!function_exists("pascalToHectopascal")){
        function pascalToHectopascal($value){
            return $value / 100;
        }
    }

    if(!function_exists("kelvinToDegreeCelsius")){
        function kelvinToDegreeCelsius($value){
            return $value - 273.15;
        }
    }
?>
<tr class="wind-tr-meteo">
    <td colspan="4" class="data-title"><span>Méteo</span></td>
    <?php
        while(strtotime($current_date) <= strtotime($endDate)){ 
            $windData = isset($data[$current_date]["data"][$ACTIVE_TIME]["wind"]) ? $data[$current_date]["data"][$ACTIVE_TIME]["wind"]:NULL;
            $classNames = "";
            foreach(Meteolamer::$TIMES as $time)
                $classNames .= " wind-td-data-day".$dayCount."-h".$time;
    ?>
        <td
            colspan="4" 
            class="wind-td-data <?= $classNames ?>" 
            data-day="<?=$dayCount?>" 
            data-time="<?= $ACTIVE_TIME ?>"
            data-map="<?= $windData?str_replace("./data","",$windData["map"]):"" ?>"
            data-imap="<?= $windData?$windData["imap"]:"" ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
            <?php if($windData){ ?>
                <p><?= round(pascalToHectopascal($windData["atmosphericPressure"])) ?> hPa</p>
                <img src="<?= $assetsUrl ?>/images/meteolamer/meteo/<?= strtolower($windData["cloud"]) ?>.png" alt="">
                <p><?= round(kelvinToDegreeCelsius($windData["temperature"])) ?> °C</p>
            <?php } ?>
        </td>
    <?php 
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>