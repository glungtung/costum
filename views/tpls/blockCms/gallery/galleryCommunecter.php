<?php 
  $keyTpl ="galleryCommunecter";
  $paramsData = [ 
    "title" => "LA GALERIE",
    "subTitle" => "La gallerie de nous",
    "galleryFilters" => null,
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>
 <style>
   .container-<?= $kunik?> .panel{
    background-color: transparent;
   }
 </style>

<h2 class="title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h2>
<h4 class="subtitle sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subTitle"><?php echo $paramsData["subTitle"] ?></h4>
<div class="col-md-12 containerr container-<?= $kunik?>" id="containerr">
  
</div>
<script>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  	var hashUrlPage = "#@"+costum.contextSlug;
  	var url = "gallery/index/type/"+costum.contextType+"/id/"+costum.contextId;
  	url+="/docType/image";
  	url+="/contentKey/slider";
    var addPhotos = {
        "beforeBuild":{
            "properties":{
                "info":{
                    "html":"<br/>Ajouter des images dans votre gallery<hr>"
                }
            }
         },
        "onload" : {
            "setTitle" : "Ajout de photos",
            "actions" : {
                "hide" : {
                    /*"folderfolder" : 1,*/
                    "newsCreationcheckboxSimple" : 1
                }
            }
        }
    };
    ajaxPost('#containerr', baseUrl+'/'+moduleId+'/'+url, 
    null,
    function(data){
    });
    function updateDocument(id, title) {
      mylog.log("updateDocument", id, name);
      dataUpdate = { docId : id } ;
      if(title != "undefined")
        dataUpdate.title = title;
      mylog.log("dataUpdate", dataUpdate);
      dyFObj.openForm ('document','sub', dataUpdate);
    }

//this function remove #hash in url
function lazyWelcomeChangeGalleryUrl(time){
    if(typeof changeGalleryUrl == "function"){
        delete window.changeGalleryUrl;
        window.changeGalleryUrl = function() {return ''};
    }
    else
      setTimeout(function(){
        lazyWelcomeChangeGalleryUrl(time+200)
      }, time);
}

$(document).ready(function(){
    var btnAdd = `<li>
                    <a class="text-left" href="javascript:dyFObj.openForm('addPhoto',null,null,null,addPhotos)">
                      <i class="fa fa-upload"></i><?php echo Yii::t("common", "Add images"); ?>
                    </a>
                  </li>
                  <li>
                    <a class="show-nav-add text-left" href="javascript:dyFObj.openForm('addFile')">
                      <i class="fa fa-file-text-o"></i> <?php echo Yii::t("common", "Upload files"); ?>
                    </a>
                  </li>
                  <li class="text-left">
                    <a class="show-nav-add text-left" href="javascript:dyFObj.openForm('bookmark','sub', {category: 'link'})">
                      <i class="fa fa-bookmark"></i> <?php echo Yii::t("common", "Add bookmark") ?>
                    </a>
                  </li>`;
    setTimeout(function(){
        $(".dropdown-menu.dropdown-menu-file.arrow_box").html(btnAdd);
        location.hash = page;
    },1200)

    dyFObj.init.folderUploadEvent = function( type, id, docT, contentK, foldk){
        if(foldk=="")
          name=(docT=="image") ? trad.noalbumselected : trad.nofolderselected;
        else
          name=navInFolders[foldk].name;
        $(".nameFolder-form").text(name);
          endPoint=uploadObj.get(type, id, docT, contentK, foldk);
          $("#imageUploader").fineUploader('setEndpoint',endPoint);
    }

    lazyWelcomeChangeGalleryUrl(0);
    $('#containerr,body').on('click',function(){
      setTimeout(function(){
              lazyWelcomeChangeGalleryUrl(0);
      },500)
    })
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre gallerie",
        "icon" : "fa-cog",
        
        "properties" : {     
        },
        beforeBuild : function(){
            uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');

                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
            }

        }
      }
    };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    setTimeout(function(){
      folder.galleryGuide('#breadcrumGallery', false,0, '0','', 'slider', '');
      $('#breadcrumGallery a').on('click',function(){
        location.hash = page;
      })
    },1000);

    
});

</script>