<?php 
    $keyTpl ="simpleGallery";
    $paramsData = [ 
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
        }
        }
    } 
    $initeImages = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'block',
        ), "image"
    );  
    $allImages = [];
    foreach ($initeImages as $key => $value) {
        array_push($allImages, $value["imagePath"]);
    }
 ?>
 <style>
    .gallery<?= $kunik?> {
        width: 80%;
        margin: 0 auto;
        padding: 5px;
        background: #fff;
        box-shadow: 0 1px 1px rgba(0,0,0,.1);
    }

    .gallery<?= $kunik?> > div {
        position: relative;
        float: left;
        padding: 5px;
    }

    .gallery<?= $kunik?> > div > img {   
        width: 350px;
        object-fit: contain;
        transition: .1s transform;
        transform: translateZ(0); 
    }

    .gallery<?= $kunik?> > div:hover {
        z-index: 1;
    }

    .gallery<?= $kunik?> > div:hover > img {
        transform: scale(1.8,1.8);
        transition: .3s transform;
    }

    .cf<?= $kunik?>:before, .cf<?= $kunik?>:after {
        display: table;
        content: "";
        line-height: 0;
    }

    .cf<?= $kunik?>:after {
        clear: both;
    }

    @media (max-width: 767px) {
        .gallery<?= $kunik?> {
            width: 100%;
        }
        .gallery<?= $kunik?> > div > img {   
            width: 170px;
        }
    }
 </style>

<div class="gallery<?= $kunik?> cf<?= $kunik?>">

    <?php if(count($allImages)==0){ ?>
        <div class="brick<?= $kunik?>">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?> 
            " />
        </div>
        <div class="brick<?= $kunik?>">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?> 
            " />
        </div>
        <div class="brick<?= $kunik?>">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?> 
            " />
        </div>
        <div class="brick<?= $kunik?>">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?> 
            " />
        </div>
        <div class="brick<?= $kunik?>">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?> 
            " />
        </div>
        <div class="brick<?= $kunik?>">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?> 
            " />
        </div>
    <?php }else{ ?>
        <?php foreach ($allImages as $key => $value){ ?>
            <div class="brick<?= $kunik?>">
                <img src="<?php echo Yii::app()->createUrl("/").$value; ?>?>"/>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<script>
     sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
     
    jQuery(document).ready(function() {
        $('.gallery<?= $kunik?>').masonry({
            itemSelector: '.brick<?= $kunik?>'
        });
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		    "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
                "images" :{
                   "inputType" : "uploader",
                   "label" : "<?php echo Yii::t('cms', 'Images')?>",
                   "docType": "image",
                    "contentKey" : "slider",
                   "endPoint": "/subKey/block",
                   "domElement" : "image",
                   "filetypes": ["jpeg", "jpg", "gif", "png", "svg"],
                   "label": "Images :",
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initeImages) ?> 
                },
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
          },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>