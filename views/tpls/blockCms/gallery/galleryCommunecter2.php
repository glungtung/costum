<!-- Created by Ifaliana Février 2021-->
<!-- You can use communnecter gallery as BlockCms -->
<?php 
$keyTpl = "galleryCommunnecter";
$paramsData = [ 
  "title"=>"Gallery",
  "textColor"=>"#000000"
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>
<style type="text/css"> 
  #containerr #galleryPad .panel-white {
    backdrop-filter: blur(5px);
    background-color: #dddddd69;
  }
  #containerr #galleryPad .panel-white #breadcrumGallery{
   color: <?= $paramsData["textColor"] ?> !important
  }

  #containerr #galleryPad .panel-white a{
   color: <?= $paramsData["textColor"] ?> !important
  }
  #containerr #galleryPad .panel-white hr{
   border-top: 1px solid #b0b0b069 !important
  }

   #containerr #galleryPad .panel-white span{
   color: <?= $paramsData["textColor"] ?> !important
  }
  #containerr #galleryPad .panel-white ul .text-dark{
   color: <?= $paramsData["textColor"] ?> !important
 }
   #containerr #gallery-container hr{
   width: 98% !important
 }
</style>
<h2 class="title text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"] ?></h2>
<div class="col-md-12 containerr" id="containerr">
	
</div>
<script>
  	var hashUrlPage = "#@"+costum.contextSlug;
  	var url = "gallery/index/type/"+costum.contextType+"/id/"+costum.contextId;
  	url+="/docType/image";
  	url+="/contentKey/slider";
    var addPhotos = {
        "beforeBuild":{
            "properties":{
                "info":{
                    "html":"<br/>Ajouter des images dans votre gallery<hr>"
                }
            }
         },
        "onload" : {
            "setTitle" : "Ajout de photos",
            "actions" : {
                "hide" : {
                    /*"folderfolder" : 1,*/
                    "newsCreationcheckboxSimple" : 1
                }
            }
        }
    };
  	ajaxPost('#containerr', baseUrl+'/'+moduleId+'/'+url, 
  	null,
  	function(data){
  	});
  	function updateDocument(id, title) {
  		mylog.log("updateDocument", id, name);
  		dataUpdate = { docId : id } ;
  		if(title != "undefined")
  			dataUpdate.title = title;
  		mylog.log("dataUpdate", dataUpdate);
  		dyFObj.openForm ('document','sub', dataUpdate);
  	}
$(document).ready(function(){
    var btnAdd = `<li>
                    <a class="text-left" href="javascript:dyFObj.openForm('addPhoto',null,null,null,addPhotos)">
                      <i class="fa fa-upload"></i><?php echo Yii::t("common", "Add images"); ?>
                    </a>
                  </li>
                  <li>
                    <a class="show-nav-add text-left" href="javascript:dyFObj.openForm('addFile')">
                      <i class="fa fa-file-text-o"></i> <?php echo Yii::t("common", "Upload files"); ?>
                    </a>
                  </li>
                  <li class="text-left">
                    <a class="show-nav-add text-left" href="javascript:dyFObj.openForm('bookmark','sub', {category: 'link'})">
                      <i class="fa fa-bookmark"></i> <?php echo Yii::t("common", "Add bookmark") ?>
                    </a>
                  </li>`;
    setTimeout(function(){
        $(".dropdown-menu.dropdown-menu-file.arrow_box").html(btnAdd);
    },1200)

    addPhotos.afterSave = function() { 
          window.urlCtrl.loadByHash(location.hash);
    };

    lazyWelcomeChangeGalleryUrl(0);
    $('#containerr').on('click',function(){
      lazyWelcomeChangeGalleryUrl(0);
    })
sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer la section1",
        "description" : "Personnaliser votre section1",
        "icon" : "fa-cog",
        "properties" : {
          "textColor":{
            label : "Couleur du texte",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.textColor
          },
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');

                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
})

//this function remove #hash in url
function lazyWelcomeChangeGalleryUrl(time){
    if(typeof changeGalleryUrl == "function"){
        delete window.changeGalleryUrl;
        window.changeGalleryUrl = function() {return ''};
    }
    else
      setTimeout(function(){
        lazyWelcomeChangeGalleryUrl(time+200)
      }, time);
}

</script>