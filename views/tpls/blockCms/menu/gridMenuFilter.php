<?php 

$keyTpl = "gridMenuFilter";
$paramsData=[
    "titleBlock" => "OBJECTIVE ODD",
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
	#grid-menu-container<?= $kunik ?> .col-md-2{
		height: 14em;
		max-height: 14em;
	}

	#grid-menu-container<?= $kunik ?> a{
		text-decoration: none;
	}

	.counter<?=$kunik?>{
      position: absolute;
      bottom: 10px;
      right: 30px;
      font-size: 40px;
    }

    .titleBlock<?= $kunik ?>{

    }
</style>
<div class="container-fluid">
	<h3 class="sp-text text-center titleBlock<?= $kunik ?>" data-id="<?= $blockKey ?>" data-field="titleBlock"><?php echo $paramsData["titleBlock"] ?></h3>
	<div id="grid-menu-container<?= $kunik ?>" class="row"></div>
</div>
<script type="text/javascript">
	
	if(typeof costum !="undefined" && typeof costum.lists != "undefined" && typeof costum.lists.objectiveOdd !="undefined" && typeof costum.lists.objectiveOddImg != "undefined"){
		let contextId = costum.contextId;
		
		if(contextData && contextData.id){
			contextId = contextData.id;
		}

		if(costum && costum._id && costum._id.$id){
			contextId = costum._id.$id;
		}

		ajaxPost(null, baseUrl+"/costum/meir/countobjectiveodd/id/"+costum.contextId+"/slug/"+costum.slug, {"odd": Object.keys(costum.lists.objectiveOdd)}, function(data){
			$("#grid-menu-container<?= $kunik ?>").empty();
			const keysArray = Object.keys(costum.lists.objectiveOdd);
			for (var index in costum.lists.objectiveOdd) {
				const indexOfValue = keysArray.indexOf(index);
				const oddImg = (typeof costum.lists != "undefined" && costum.lists.objectiveOddImg && costum.lists.objectiveOddImg[index])?"#"+costum.lists.objectiveOddImg[index].replace(/\.[^/.]+$/, ""):"transparent";
				$("#grid-menu-container<?= $kunik ?>").append(`
					<div class="col-md-2 col-sm-4 col-xs-12 padding-5">
						<div style="background:${oddImg}; height:100%" class="padding-right-10 padding-left-10">
							<a class="btn-filters-select" href="javascript:;" data-type="filters" data-field="objectiveOdd" data-key="${index}" data-value="${index}" data-event="filters">
								<h5 style="display:flex; color:white"><b class="margin-right-5" style="font-size:24pt; font-weight:bolder">${indexOfValue+1} </b> <span> ${index}</span></h5>
								<div class="text-center">
									<img height="65em" src="${assetPath+"/images/"+costum.slug+"/odd/"+costum.lists.objectiveOddImg[index]}">
								</div>
								<h3 class="text-right counter<?=$kunik?> text-white">${data[indexOfValue]}</h3>
							</a>
						<div>	
					</div>
				`);
			}

			$("#grid-menu-container<?= $kunik ?>").append(`
				<div class="col-md-2 col-sm-4 col-xs-12 padding-5">
					<div style="background:white; height:100%" class="padding-10">
						<div class="text-center" style="margin-top:1em !important">
							<img height="70em" src="${assetPath+"/images/"+costum.slug+"/odd/ffffff.png"}">
						</div>
					<div>
				</div>
			`);

			$(".btn-filters-select").unbind().on("click", function(){
				//searchObj.search.obj.tags = [];
				//searchObj.search.obj.filters["objectiveOdd"] = $(this).data("key");
				//searchObj.search.obj.filters["category"] = $(this).data("startup");
				//alert("Here") // #search?objectiveOdd=${index}&category=startup
				urlCtrl.loadByHash("#search?category=startup&objectiveOdd="+$(this).data("key"));
			})

			coInterface.bindLBHLinks();
		})
	}
</script>