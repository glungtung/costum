<?php 

  $keyTpl ="menuWithSearch";
  $paramsData = [ 
   
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
  $parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere"]);

 ?>
<style>
  .container<?= $kunik?> {
    transition-property: transform;
    box-sizing: content-box;
    margin-top : 2%;
  }
  .container<?= $kunik?> .card {
    font-family : Please_write_me_a_song;
    color : #3b9ca3 !important;
    font-size : 25px;
    cursor: pointer;
    text-decoration : none;
  }
  
  .container<?= $kunik?> .card img{
    margin-bottom: 1%;
    height: 60px;
    object-fit: contain;

  }
  
  .container<?= $kunik?> h1{
    text-transform:none;
    font-family : Please_write_me_a_song;
    marging-bottom : 2%;
    color : white;
    font-size : 40px;
  }
  .search<?= $kunik?>{
    background: #3b9ca3;
    padding-top: 2%;
    padding-bottom: 4%;
  }
  .search<?= $kunik?> h1::after{
    content:'';
    height:2px;
    width:20%;
    background:white;
    position:absolute;
    left:calc(50% - 10%);
    bottom:-5px;    
  }
  .search<?= $kunik?> p{
    marging-bottom : 3%;
    color : white;
    font-size : 25px;
    font-family : Please_write_me_a_song;
  }
  .search<?= $kunik?> .inputSearch{
    margin-bottom: 5%;
    padding-left : 20%;
    text-align : left;
  }

  .search<?= $kunik?> form {
    width: 75%;
  }

  .search<?= $kunik?> form .inner-form {
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
    box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
    border-radius: 20px;
    overflow: hidden;
    margin-bottom: 30px;
    height: 70px;
  }

  .search<?= $kunik?> form .inner-form .input-field {
    height: 68px;
  }
  .search<?= $kunik?> form .inner-form .input-field input {
    height: 100%;
    border: 0;
    display: block;
    width: 100%;
    padding: 10px 0;
    font-size: 16px;
    color: #000;
  }

  .search<?= $kunik?> form .inner-form .input-field input.placeholder {
    color: #222;
    font-size: 14px;
  }

  .search<?= $kunik?> form .inner-form .input-field input:-moz-placeholder {
    color: #222;
    font-size: 14px;
  }

  .search<?= $kunik?> form .inner-form .input-field input::-webkit-input-placeholder {
    color: #222;
    font-size: 14px;
  }

  .search<?= $kunik?> form .inner-form .input-field input:hover, .search<?= $kunik?> form .inner-form .input-field input:focus {
    box-shadow: none;
    outline: 0;
  }

  .search<?= $kunik?> form .inner-form .input-field.first-wrap {
    padding: 10px 7px 10px 7px;
    -ms-flex-positive: 1;
    flex-grow: 1;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    background: #fff;
  }

  .search<?= $kunik?> form .inner-form .input-field.first-wrap input {
    -ms-flex-positive: 1;
    flex-grow: 1;
    background: #b9e4e4;
    padding: 1%;
    border-radius: 12px 0 0 12px;
  }


  .search<?= $kunik?> form .inner-form .input-field.first-wrap svg {
    width: 36px;
    height: 36px;
    fill: #222;
  }

  .search<?= $kunik?> form .inner-form .input-field.second-wrap {
    min-width: 70px;
  }

  .search<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search {
    height: 100%;
    width: 100%;
    white-space: nowrap;
    font-size: 35px;
    color: #ea631f;
    border: 0;
    cursor: pointer;
    position: relative;
    z-index: 0;
    background: white;
    transition: all .2s ease-out, color .2s ease-out;
    font-weight: 300;
  }

  .search<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:hover {
    background: white;
  }

  .search<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:focus {
    outline: 0;
    box-shadow: none;
  }
  .container<?= $kunik?> .dropdown {
    width : 100%;
  }
  .container<?= $kunik?> .dropdown button{
    background-color : #3b9ca3;
    color: white;
    font-family: 'Please_write_me_a_song';
    font-size: 27px;    
    border : none;
    height: 40px;
    width : 100%;
  }
  .container<?= $kunik?>  a:focus,.container<?= $kunik?>  a:hover{
    text-decoration: none;
  }
  
  .container<?= $kunik?> .dropdown .dropdown-menu, .container<?= $kunik?> .dropdown .dropdown-menu {
    position: absolute;
    overflow-y: visible !important;
    top: 35px;
    left: 5px;
    width: 100%;
    z-index: 2;
    border-radius: 2px;
    padding: 0px;
  }
  .container<?= $kunik?> .nav-tabs>li.active>a:after{
    bottom: -2px;
    border-bottom: 35px solid #3b9ca3;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    content: " ";
    border-width: 15px;
    z-index: 1;
    left: 40%;
    position: absolute;
  }
  .container<?= $kunik?> .nav-tabs>li.active>a,
  .container<?= $kunik?> .nav-tabs>li.active>a:hover,
  .container<?= $kunik?> .nav-tabs>li>a:hover,
  .container<?= $kunik?> .nav-tabs>li.active>a:focus {
    color: #555;
    cursor: default;
    background-color: white;
    border: white;
    border-bottom-color: transparent;
  }
  .container<?= $kunik?> .arrow_box:before {
    border-bottom-color: transparent;
    border: none;
  }
  .container<?= $kunik?> .arrow_box {
    overflow-y: visible !important;
    position: relative;
    border: 1px solid transparent;
    top: 50%;
  }
  .container<?= $kunik?> .nav-tabs {
    border-bottom: none;
    padding: 0 5% 0 5%;
  }
  a.openCostum<?= $kunik ?>{
    padding: 10px;
    font-size: 30px;
    border-radius: 30px;
    font-family: Please_write_me_a_song;
    background: transparent;
    color: white !important;
    border: 3px solid #F0FCFF;
	}
 
  @media (min-width: 768px){
    .container<?= $kunik?> .contain {
      padding: 0 10% 0 10%;
    }
  }
  @media (max-width: 992px){
    .<?= $kunik?> .card img {
        margin-bottom: 1%;
        height: 30px;
    }
    .<?= $kunik?> .card{
        font-size: 17px;
    }
    .container<?= $kunik?>{
      min-height: 50px !important;
    }
  }
  @media (max-width: 414px){
    .<?= $kunik?> .card img {
        margin-bottom: 1%;
        height: 30px;
    }
    .<?= $kunik?> .card{
        font-size: 17px;
    }
  }
  .element<?= $kunik?>{
    margin-top: 3%;
    margin-bottom: 2%;
    font-family: 'Please_write_me_a_song';
  }
  .element<?= $kunik?>.main-section {
    width: 80%;
    margin: 0 auto;
    text-align: center;
    padding: 0px 5px;
  }
    
  .element<?= $kunik?> .dashbord {
    border-radius: 15px;
    border: 2px solid white;
    width: 13%;
    margin-left: 1%;
    margin-right: 1%;
    display: inline-block;
    background-color: #005E6F;
    color: #fff;
  }
    
  .element<?= $kunik?> .icon-section i {
    height: 60px;
    width: 60px;
    font-size: 30px;
    padding: 11px;
    border: 1px solid #fff;
    border-radius: 50%;
    margin-top: -25px;
    margin-bottom: 10px;
    background-color: #005E6F;
  }
  
  .element<?= $kunik?> .icon-section p {
      margin: 0px;
      font-size: 20px;
      padding-bottom: 10px;
  }
  .element<?= $kunik?> .icon-section small {
      font-size: 25px;
  }
  
  .element<?= $kunik?> .detail-section {
      background-color: #2F4254;
      padding: 5px 0px;
  }
  
  .element<?= $kunik?> .dashbord .detail-section:hover {
      background-color: #5a5a5a;
      cursor: pointer;
  }
  
  .element<?= $kunik?> .detail-section a {
      color: #fff;
      text-decoration: none;
  }
  .container<?= $kunik?> .initialise<?= $blockCms['_id'] ?> .card {
    color : black !important;
  }
</style>
<div class="container<?= $kunik?> text-center">
  <div id="listFil_<?= $kunik?>">
  <div class="dropdown pull-right  hidden-md hidden-lg">
      <button class="  dropdown-toggle" type="button" id="dropdownMenuList2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Filière <i class="fa fa-angle-double-down  fa-x2"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuList2">
        
      </ul>
    </div>
    <ul class="nav nav-tabs col-md-12 text-center hidden-sm hidden-xs"> </ul>
    <div class="tab-content hidden-sm hidden-xs "></div>
    
  </div>
</div>

<script type="text/javascript">
  
  var src = "";
  var srcTab = "";
  var menuXs = "";
  var i = 0;
  var listSlugFilier = [];
  var menu<?= $kunik?> = {
    listeFiliere:function(){
      var params = {
        "contextId" : costum.contextId
      }
      ajaxPost(
        null,
        baseUrl+"/costum/cocity/getfiliere",
        params,
        function(data){
          $.each(data, function( index, value ) {
            var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
            tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
            : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
            nameFil1 = nameTheme.replace('É', 'E');
            nameFil= nameFil1.replace("é", "e");
            nameOrga = ("<?= $parent["name"]?>").toLowerCase()+nameFil;
            menu<?= $kunik?>.existOrgaFiliere(nameOrga,value);
          })      
        },
        function(error){},
        "json",
        {async : false}
      );
    },  
    existOrgaFiliere:function(nameOrga,value){
      var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
      tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
      : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
      var params = {
        "cocity" : costum.contextId,
        "thematic" :nameTheme
      };
      ajaxPost(
        null,
        baseUrl+"/costum/cocity/getorgafiliere",
        params,
        function(data){
        //alert("sucess");
        if (data.length!=0){
          $.each(data, function( index, valueFil ) {
            listSlugFilier.push(valueFil.slug);
            if(typeof valueFil.costum !="undefined" && typeof valueFil.costum.slug != "undefined" && (valueFil.costum.slug == "cocity" || valueFil.costum.slug == "filiereGenerique") && typeof valueFil.cocity !="undefined" && valueFil.cocity == costum.contextId) {               
              i++;
              if(i == 1)
                src += '<li class="col-md-1 active">';
              else 
                src += '<li class="col-md-1 ">';
              src += '<a data-toggle="tab" href="#'+valueFil.slug+'">'+
                      '<div class="card  4 swiper-slide ">'+
                      '<i class="fa '+value.icon+'"></i>'+
                        ' </br> '+nameTheme+    
                      '</div> '+                                                                  
                    '</a>'+
                ' </li>';

              if(i == 1)
                srcTab += '<div id="'+ valueFil.slug+'" class="tab-pane fade in active ">';
              else 
                srcTab += '<div id="'+ valueFil.slug+'" class="tab-pane fade">';
                srcTab += '<div class=" -menu arrow_box" style="overflow-y: auto;" aria-labelledby=" Types">'+
                  '<div class=" search<?= $kunik?> col-md-12">'+
                  '<h1>Où nous trouvé</h1>'+ 
                  '<div class="underline menuWithSearh616d8c368bc3675ac30c624d" style="position:relative;background:white;width:100px;height:3px;left:48%;transform:translateX(-50%);margin-bottom:30px;margin-top:10px"></div>'+  
                 
                  "<div class='element<?=$kunik?> element"+valueFil.slug+"' > "+getNumberElement(valueFil.slug,"unnotremonde")+"</div>"+
                  '<div class="inputSearch">'+
                      '<p>Faire une recherche dans le filière '+nameTheme+' </p>'+
                      '<form id="'+valueFil.slug +'">'+
                        '<div class="inner-form">'+
                          '<div class="input-field first-wrap">'+
                            '<input type="text" id="name'+valueFil.slug +'" placeholder="Filière '+nameTheme+' " value="" onkeypress="" />'+
                        ' </div>'+
                          '<div class="input-field second-wrap">'+
                          ' <button onclick="menu<?=$kunik?>.search(\''+valueFil.slug+'\') " class="btn-search" type="button">'+
                              '<i class="fa fa-long-arrow-right"></i>'+
                            '</button>'+
                          '</div>'+
                      '</div>'+
                      '</form>'+
                    '</div>'+
                    '<a  href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ valueFil.slug+'" class=" openCostum<?= $kunik ?>" > Voir le filière '+nameTheme+'</a>'+
                  '</div>'+
                '</div>'+
              '</div>'
            } 
            menuXs += '<li class="text-left">'+
            '<a  href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ valueFil.slug+'">'+
                      '<div class="card  4 swiper-slide ">'+nameTheme+    
                      '</div> '+                                                                  
                    '</a>'+
                    '</li>';
          });
        }else {
          src += '<li class="col-md-1 ">'+
              '<a  href="javascript:;" class=" initialise<?= $blockCms['_id'] ?>"  data-tag = "'+value.icon +'"data-name ="'+ value.name+'">'+
                '<div class="card  4 swiper-slide ">'+
                '<i class="fa '+value.icon+'"></i>'+
                  ' </br> '+nameTheme+    
                '</div> '+                                                                  
              '</a>'+
          '</li>';
        menuXs += '<li class="text-left">'+
        '<a  href="javascript:;" class=" initialise<?= $blockCms['_id'] ?>"  data-tag = "'+value.icon +'"data-name ="'+ value.name+'">'+
                '<div class="card  4 swiper-slide ">'+nameTheme+    
                '</div> '+                                                                  
              '</a>'+
          '</li>';
        }
      },
      function(error){},
      "json",
      {async : false}
      );
      $("#listFil_<?= $kunik?> .nav-tabs").html(src);
      $("#listFil_<?= $kunik?> .tab-content").html(srcTab);
      $("#listFil_<?= $kunik?> .dropdown-menu").html(menuXs);
      
      
    },
    search:function(slug){
      var name = document.getElementById("name"+slug).value;
      window.location="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/"+slug+"#search?text="+name;
    },
    test:function(){
      $.each(listSlugFilier,function(k,val){
        $("#"+val).on('submit', function(event){ 
          event.preventDefault();
          var data = $(this).serialize();
          menu<?= $kunik?>.search(val);
          return false;      
        });
      })
    }
  }
 
  
 
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    menu<?= $kunik?>.listeFiliere();
    menu<?= $kunik?>.test();
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre bloc",
        "description" : "Personnaliser votre bloc",
        "icon" : "fa-cog",
        
        "properties" : {
        },
        beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;

            if(k == "items")
              tplCtx.value[k] = data.items;
              mylog.log("andrana",data.items)
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                    toastr.success("Élément bien ajouté");
                    $("#ajax-modal").modal('hide');
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      tplCtx.format = true;
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"image",6,6,null,null,"Images","green","");
      alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"button",4,6,null,null,"Propriété du bouton","green","");
    });
    $(".initialise<?= $blockCms['_id'] ?>").click(function() { 
      nameOrga = "<?= $parent["name"]?>"+" "+$(this).data("name");
      nameFiliere = $(this).data("name");
        // alert(nameOrga);
      bootbox.confirm("Cet filière n'existe pas, vous voulez le créez ?",
        function(result){
          if (!result) {
            return;
          }else {
            localStorage.setItem("paramsCocity",costum.contextId+".<?= $parent["name"]?>."+nameFiliere);
            window.location="<?php  echo Yii::app()->createUrl('/costum')?>/co/index/slug/filierePrez"; 
          }
        }); 
    });
  });
</script>