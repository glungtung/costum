<?php 
$keyTpl = "menu";
$paramsData = [
  "filiere" =>[],
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere"]);

$listFielere = isset($parent["filiere"])?$parent["filiere"]:[];
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style type="text/css">
  .menuThemeCocity_<?= $kunik?>{
    width: 100%;
    height: 100px;
    padding: 2%;
    margin-left: 2%;

  }
  .menuThemeCocity_<?= $kunik?>  .titlemenu{
    margin-left: 2%;
  }

  .menuThemeCocity_<?= $kunik?> .initialise<?= $blockCms['_id'] ?>{
    color: aqua;
  }
  .menuThemeCocity_<?= $kunik?> .initialisexs<?= $blockCms['_id'] ?>{
    color: black;
  }
  .menuThemeCocity_<?= $kunik?> .openCostum<?= $blockCms['_id'] ?>{
    color: white;
  }
  .container-<?= $kunik?>{
    min-height: 50px !important; 
  }
  .addElement<?= $blockCms['_id'] ?> {
    display: none;
  }
  .menuThemeCocity_<?= $kunik?>:hover .addElement<?= $blockCms['_id'] ?>{
    display: block;
    position: absolute;
    top:20%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  .menuThemeCocity_<?= $kunik?> .card {
    margin: 10% auto;
    height: 150px;
    padding-top: 1%;
    border-radius: 10px;
    box-shadow:  0px 0px 6px 0px rgb(0 0 0 / 22%);
    cursor: pointer;
    background: #092434;
    transition: 0.4s;
  }

  .menuThemeCocity_<?= $kunik?> .cards-list {
    z-index: 0;
    width: 100%;
    justify-content: space-around;
    flex-wrap: wrap;
  }

  @media (max-width: 978px) {
    .menuThemeCocity_<?= $kunik?> .card {
      margin: 10% auto;
      height: 80px;
      padding-top: 5%;
      border-radius: 10px;
      box-shadow:  0px 0px 6px 0px rgb(0 0 0 / 22%);
      cursor: pointer;
      background: #092434;
      transition: 0.4s;
    }
    .menuThemeCocity_<?= $kunik?>{
      height: auto;
      padding: 2%;
      margin-left: 2%;
    }
    .menuThemeCocity_<?= $kunik?> .swiper-button-next, .menuThemeCocity_<?= $kunik?> .swiper-button-prev {
      top: 15% !important;
    }
    
   .menuThemeCocity_<?= $kunik?> .swiper-slide {
      margin-bottom: 2% !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-prev, .menuThemeCocity_<?= $kunik?> .swiper-container-rtl .swiper-button-next {
      left: auto  !important;
      right: 15%  !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-next,.menuThemeCocity_<?= $kunik?> .swiper-container-rtl .swiper-button-prev {
      right: 12%;
      left: auto;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-prev:after, .swiper-container-rtl .swiper-button-next:after {
      font-weight: bold !important;
      font-size: 20px !important;
    }
    .menuThemeCocity_<?= $kunik?> .swiper-button-next:after, .menuThemeCocity_<?= $kunik?> .swiper-container-rtl .swiper-button-prev:after {
      font-weight: bold !important;
      font-size: 20px !important;
    }
}
  @media(min-width:  978px){
    .menuThemeCocity_<?= $kunik?> i {
      font-size: 25px;
      padding: 5px;
    }
    .menuThemeCocity_<?= $kunik?> a {
      font-size: 15px;
    }
    .menuThemeCocity_<?= $kunik?> .swiper-slide {
      margin-right: 20px !important;
/*      width: 9% !important;*/
    }
    .menuThemeCocity_<?= $kunik?> .swiper-wrapper {
      margin-top: -7%;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-next, .menuThemeCocity_<?= $kunik?> .swiper-button-prev {
      top: 18% !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-prev, .menuThemeCocity_<?= $kunik?> .swiper-container-rtl .swiper-button-next {
      left: auto  !important;
      right: 15%  !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-next, .menuThemeCocity_<?= $kunik?> .swiper-container-rtl .swiper-button-prev {
      right: 12% !important;
      left: auto !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-prev:after,.menuThemeCocity_<?= $kunik?>  .swiper-container-rtl .swiper-button-next:after {
      font-weight: bold !important;
      font-size: 20px !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-next:after,.menuThemeCocity_<?= $kunik?>  .swiper-container-rtl .swiper-button-prev:after {
      font-weight: bold !important;
      font-size: 20px !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-next.swiper-button-disabled, .menuThemeCocity_<?= $kunik?> .swiper-button-prev.swiper-button-disabled {
      background-color: #f0fcff !important;
      color: black !important;
    }
     .menuThemeCocity_<?= $kunik?> .swiper-button-next, .menuThemeCocity_<?= $kunik?> .swiper-button-prev{
      height: 30px  !important;
      width: 30px !important;
      border-radius: 50% !important;
      background-color:#63aabc ;
      color: white !important;
    }
  }
  .element<?=$kunik?> {
    margin-top :10px;
    font-size : 15px;
    color : white;
  }
  .element<?=$kunik?> i{
    font-size : 15px;
    color : white;
  }
  .menuThemeCocity_<?= $kunik?>  .dropdown-menu, .menuThemeCocity_<?= $kunik?>  .dropdown-menu {
    position: absolute;
    overflow-y: visible !important;
    top: 35px;
    width: 100%;
    z-index: 2;
    border-radius: 2px;
    padding: 0px;
  }
  .menuThemeCocity_<?= $kunik?> .titlemenu button{
    border: none;
    background: white;
  }
  .openCostumxs<?=$kunik?>{
    color : #3b9ca3 !important;
  }
</style>

<div class="menuThemeCocity_<?= $kunik?> container-<?= $kunik?>">

  <div class="titlemenu">
   <h2 class="title-1">Catégorie
   <div class="dropdown pull-right  hidden-md hidden-lg ">
      <button class="  dropdown-toggle" type="button" id="dropdownMenuList2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-angle-double-down  fa-x2"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuList2">
    
      </ul>
    </div>


   </h2>
   
 </div>
 <div class="swiper-button-next hidden-xs"></div>
    <div class="swiper-button-prev hidden-xs "></div>
      <div class="text-center  " >
        <button class="btn btn-primary addElement<?= $blockCms['_id'] ?>">Ajouter un nouvel thèmatique</button>  
        <div id="listFil_<?= $kunik?>" class="  cards-list col-md-12">
          <div class="  swiper-container hidden-xs">           
            <div class=" swiper-wrapper">
              <div style="position: relative;width:100%;height: 100%">
                <p class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></p>
              </div>
            </div>
          </div>
        </div>    
      </div>
    </div>

</div>     

  <script type="text/javascript">
  

    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var src ="";
    var menuXs = "";
    var menu<?= $kunik?> = {
      listeFiliere:function(){
        var params = {
          "contextId" : costum.contextId
        }
        ajaxPost(
          null,
          baseUrl+"/costum/cocity/getfiliere",
          params,
          function(data){
            $.each(data, function( index, value ) {
              var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
              tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
              : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
              nameFil1 = nameTheme.replace('É', 'E');
              nameFil= nameFil1.replace("é", "e");
              nameOrga = ("<?= $parent["name"]?>").toLowerCase()+nameFil;
              menu<?= $kunik?>.existOrgaFiliere(nameOrga,value);
            })      
          },
          function(error){},
          "json",
          {async : false}
        );
      },  
      existOrgaFiliere:function(nameOrga,value){
        var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
        tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
        : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
        var params = {
          "cocity" : costum.contextId,
          "thematic" :nameTheme
        };
        ajaxPost(
          null,
          baseUrl+"/costum/cocity/getorgafiliere",
          params,
          function(data){
          //alert("sucess");
          if (data.length!=0){
            $.each(data, function( index, valueFil ) {
              if(typeof valueFil.costum !="undefined" && typeof valueFil.costum.slug != "undefined" && (valueFil.costum.slug == "cocity" || valueFil.costum.slug == "filiereGenerique") && typeof valueFil.cocity !="undefined" && valueFil.cocity == costum.contextId) {               
                src += 
                "<div class='card 4 swiper-slide '>"+
                "<a  href='<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/"+ valueFil.slug+"' class=' openCostum<?= $blockCms['_id'] ?>' >"+
                "<i class='fa "+value.icon+" '></i></br>"+nameTheme+               
                "</a>"+
                "<div class ='element<?=$kunik?> hidden-xs element"+valueFil.slug+"'>"+getNumberElement(valueFil.slug,"cocity")+"</div>"+
                "</div>";
                menuXs += '<li class="text-left">'+
                '<a class="openCostumxs<?=$kunik?>"  href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ valueFil.slug+'">'+
                      '<div class=" swiper-slide ">'+nameTheme+    
                      '</div> '+                                                                  
                    '</a>'+
                '</li>';
              } 
            });
          }else {
            src += 
                "<div class='card 4 swiper-slide '>"+
                "<a  href='javascript:;' class=' initialise<?= $blockCms['_id'] ?>' data-id='' data-tag = '"+value.icon +"'data-name ='"+ value.name+"'>"+
                "<i class='fa "+value.icon+" '></i></br>"+nameTheme+               
                "</a>"+
                "</div>";
            menuXs += '<li class="text-left">'+
            '<a href="javascript:;" data-name ="'+ value.name+'" class="initialisexs<?= $blockCms['_id'] ?> initialise<?= $blockCms['_id'] ?>"'+
                  '<div class=" swiper-slide ">'+nameTheme+    
                  '</div> '+                                                                  
                '</a>'+
            '</li>';
          }
        },
        function(error){},
        "json",
        {async : false}
        );
        $("#listFil_<?= $kunik?> .swiper-wrapper").html(src);
        $(".menuThemeCocity_<?= $kunik?> .dropdown-menu").html(menuXs);
      },
      swiperCarousel:function() {
        var options = {
          onlyExternal: true,
          slidesPerView: 1,
          spaceBetween: 10,
          keyboard: {
            enabled: true,
          },/*
          autoplay : {
            delay: 6000,
            disableOnInteraction: true,
          },*/
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
          breakpoints: {
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView:  4,
              spaceBetween: 20,
            },
            1024: {
              slidesPerView: 4,
              spaceBetween: 100,
            },
          }
        };
        var swiper = new Swiper(" .swiper-container", options);
      }
 }
  jQuery(document).ready(function() {
    menu<?= $kunik?>.listeFiliere();
    menu<?= $kunik?>.swiperCarousel();
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section1",
        "icon" : "fa-cog",
        "properties" : {

        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) { 
              $("#ajax-modal").modal('hide');
              toastr.success("élement mis à jour");
              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
              // urlCtrl.loadByHash(location.hash);
            } );
          }
        }

      }
    }
    mylog.log("paramsData",sectionDyf);
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
    $(".addElement<?= $blockCms['_id'] ?>").click(function() {  
      var tplCtx = {};
      var activeForm = {
        "jsonSchema" : {
          "title" : "Ajouter nouvelle filière",
          "type" : "object",
          "properties" : {
            name : {
              label : "Nom de la filière",
              "inputType" : "text",
              values : "",
              rules:{
                "required":true
              }
            },
            icon :{
              label : "icone de la filière",
              "inputType" : "select",
              options : fontAwesome,
              rules:{
                "required":true
              }
            },
            tags: {
             label : "Mot clé",
             "inputType" : "tags",
             rules:{
              "required":true
            }
          }
        }
      }
    };          

    activeForm.jsonSchema.save = function () {
      tplCtx.id = costum.contextId;
      tplCtx.collection = contextType;
      tplCtx.path="allToRoot";
      tplCtx.value = {};

      $.each(activeForm.jsonSchema.properties , function(k,val) {
        filiere = "filiere."+$("#name").val()+"."+k;
        tplCtx.value[filiere] = $("#"+k).val();
        if (k=="icon")
          tplCtx.value[filiere] = "fa-"+$("#"+k).val();
      });
      mylog.log("ediit addElement", tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          mylog.log("ediit addElement11", tplCtx);
          toastr.success('Modification enregistrer');
          $("#ajax-modal").modal('hide');
          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
          // urlCtrl.loadByHash(location.hash);
        } );
      } 
    }
    dyFObj.openForm( activeForm );
  });
    $(".initialise<?= $blockCms['_id'] ?>").click(function() { 
      nameOrga = "<?= $parent["name"]?>"+" "+$(this).data("name");
      nameFiliere = $(this).data("name");
          // alert(nameOrga);
          bootbox.confirm("Cet filière n'existe pas, vous voulez le créez ?",
            function(result){
              if (!result) {
                return;
              }else {
                localStorage.setItem("paramsCocity",costum.contextId+".<?= $parent["name"]?>."+nameFiliere);
               window.location="<?php  echo Yii::app()->createUrl('/costum')?>/co/index/slug/filierePrez"; 
              }
            }); 
        });
  });
  



  
  </script>