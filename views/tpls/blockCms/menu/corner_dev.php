<?php
    $blockKey = $blockKey ?? sha1(date('Y-m-d'));
    $params_data = [];
    
    if (!empty($blockCms)) {
        $keys = array_keys($params_data);
        foreach ($keys as $key) {
            if (isset($blockCms[$key])) {
                $params_data[$key] = $blockCms[$key];
            }
        }
    }
    $connected_user = Yii::app()->session['userId'];

// vérifier s'il est développeur
    $is_costum_admin = false;
    $can_see = false;
    $admin_only = false;
    if (!empty($connected_user) && $this->costum !== null) {
        $costum = $this->costum;
        $db_costum = PHDB::findOneById($costum['contextType'], $costum['contextId'], ['links', 'costum']);
        $admin_only = $db_costum['costum']['cd_project']['admin_only'] ?? false;
        $links = ['members', 'contributors', 'attendees', 'organizer'];
        $user_scopes = [
            Organization::COLLECTION => ['members', 'organizer'],
            Project::COLLECTION      => ['contributors'],
            Event::COLLECTION        => ['attendees']
        ];
        $where = array_map(function ($__link) use ($connected_user) {
            return ['$and' => [["links.$__link.$connected_user.isAdmin" => ['$exists' => true]], ["links.$__link.$connected_user.isAdmin" => true]]];
        }, $links);
        
        if (!empty($user_scopes[$costum['contextType']])) {
            foreach ($user_scopes[$costum['contextType']] as $link_type) {
                if (!empty($db_costum['links'][$link_type][$connected_user])) {
                    $can_see = true;
                    break;
                }
            }
        }
        foreach ($links as $link) {
            if (!empty($db_costum['links'][$link][$connected_user]['isAdmin']) && filter_var($db_costum['links'][$link][$connected_user]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) {
                $is_costum_admin = true;
                break;
            }
        }
    }
    
    $is_developper = false;
    if (!empty($connected_user)) {
        $db_my_roles = PHDB::findOne(Organization::COLLECTION, ['slug' => 'openAtlas'], ["links.members.$connected_user.roles"]);
        if (!empty($db_my_roles['links']) && !empty($db_my_roles['links']['members']) && !empty($db_my_roles['links']['members']) && !empty($db_my_roles['links']['members'][$connected_user]) && !empty($db_my_roles['links']['members'][$connected_user]['roles'])) {
            $roles = array_map(function ($__role) {
                return str_replace(' ', '', trim($__role));
            }, $db_my_roles['links']['members'][$connected_user]['roles']);
            if (in_array('Développeur', $roles)) $is_developper = true;
        }
    }
    
    if (!$admin_only && $can_see || $is_costum_admin || $is_developper) {
        $script_css = [
            '/plugins/jquery-confirm/jquery-confirm.min.css',
            '/plugins/jquery-confirm/jquery-confirm.min.js'
        ];
        HtmlHelper::registerCssAndScriptsFiles($script_css, Yii::app()->request->baseUrl);
        ?>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link
            href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Oxygen:wght@300;400;700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
            rel="stylesheet">
        <style>
            #corner_dev<?= "$blockKey " ?>,
            #corner_dev<?= "$blockKey " ?>h1,
            #corner_dev<?= "$blockKey " ?>h2,
            #corner_dev<?= "$blockKey " ?>h3,
            #corner_dev<?= "$blockKey " ?>h4,
            #corner_dev<?= "$blockKey " ?>h5,
            #corner_dev<?= "$blockKey " ?>h6,
            #corner_dev<?= "$blockKey " ?>button,
            #corner_dev<?= "$blockKey " ?>input,
            #corner_dev<?= "$blockKey " ?>select,
            #corner_dev<?= "$blockKey " ?>textarea,
            #corner_dev<?= "$blockKey " ?>a,
            #corner_dev<?= "$blockKey " ?>p,
            #corner_dev<?= "$blockKey " ?>span {
                font-family: Inter, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue" !important;
                user-select: none;
                -webkit-user-select: none;
                -moz-user-select: none;
            }

            #corner_dev<?= "$blockKey " ?>.fa {
                font-family: FontAwesome !important;
            }

            #corner-button-container<?= $blockKey ?> {
                position: fixed;
                bottom: 20px;
                right: 20px;
                z-index: 9999;
            }

            #corner-button<?= $blockKey ?> {
                display: block;
                width: 50px;
                height: 50px;
                border-radius: 50%;
                border: none;
                outline: none;
                box-shadow: 0 0 10px rgba(0, 0, 0, .5);
                transition: box-shadow .2s ease;
                background-color: white;
                overflow: hidden;
            }

            #corner-button<?= $blockKey ?>:hover {
                box-shadow: 0 0 25px rgba(0, 0, 0, .5);
            }

            #corner-card<?= $blockKey ?> {
                display: none;
                font-size: 1.3rem;
                position: fixed;
                bottom: 20px;
                right: 20px;
                padding: 5px;
                border-radius: 10px;
                box-shadow: 0 0 20px rgba(0, 0, 0, .3);
                background-color: white;
                width: 400px;
                max-width: calc(100vw - 40px);
                z-index: 100000;
            }

            #corner-header<?= $blockKey ?> {
                display: grid;
                grid-template-columns: 1fr 10fr 1fr;
                grid-column-gap: 2px;
                padding: 2px;
                border-radius: 7px;
                align-items: center;
                margin-bottom: 5px;
                box-shadow: 0 0px 3px rgba(0, 0, 0, .3);
            }

            #corner-header<?= "$blockKey " ?>.corner-title {
                grid-column-start: 2;
                font-weight: bold;
                font-size: 2em;
            }


            #corner-header<?= "$blockKey " ?>.corner-actions > *,
            #corner-header<?= "$blockKey " ?>.turn-back {
                font-size: 1.8em;
                color: #000;
                cursor: pointer;
                border-radius: 50%;
                width: 40px;
                height: 40px;
                text-align: center;
                transition: background-color .15s ease;
                line-height: 0;
                display: flex;
                flex-direction: column;
                justify-content: center;
            }

            #corner-header<?= "$blockKey " ?>.corner-actions > *:hover,
            #corner-header<?= "$blockKey " ?>.turn-back:hover {
                background-color: #f2f2f2;
            }

            #corner-header<?= "$blockKey " ?>.turn-back {
                display: none;
            }

            #corner-body<?= $blockKey ?> {
                overflow: hidden;
                display: flex;
                flex-direction: row;
                align-items: flex-start;
            }

            #corner-body<?= "$blockKey " ?> > div {
                min-width: 100%;
            }

            #context-container<?= $blockKey ?> {
                overflow-y: auto;
            }

            .corner-page-link,
            .corner-page-button {
                display: grid;
                font-size: 1.2em;
                padding: 10px;
                border-radius: 5px;
                margin-bottom: 5px;
                grid-template-columns: 1fr 60px;
                align-items: center;
                cursor: pointer;
                transition: background-color .15s ease;
            }

            .corner-page-link.disabled,
            .corner-page-button.disabled {
                color: rgba(44, 43, 43, .5);
                cursor: default;
            }

            .corner-page-link:last-child,
            .corner-page-button:last-child {
                margin-bottom: 0;
            }

            .corner-page-link:not(.disabled):hover,
            .corner-page-button:not(.disabled):hover {
                background: #f2f2f2;
            }

            .corner-element {
                display: flex;
                overflow: hidden;
                flex-direction: column;
                font-size: 1.2em;
                padding: 10px;
                border-radius: 5px;
                transition: background-color .15s ease;
                margin: 0 5px 10px 5px;
                box-shadow: 0 0 5px #828282;
            }

            .corner-element-footer {
                display: flex;
                flex-direction: row;
                margin: 0 -10px -10px -10px;
                overflow: hidden;
                border-top: 1px solid rgba(0, 0, 0, .1);
            }

            .corner-element-footer > * {
                flex: 1;
            }

            .corner-element:last-child {
                margin-bottom: 5px;
            }

            .corner-filter-container {
                margin-top: 5px;
                position: relative;
            }

            .corner-filter-input,
            .corner-filter-input:focus {
                border: 1px solid #cacaca;
                border-radius: 20px;
                line-height: 1.8rem;
                padding: 10px 10px 10px 35px;
                width: 100%;
                outline: none;
                margin-bottom: 10px;
            }

            .corner-filter-container .fa {
                position: absolute;
                top: calc(50% - 5px);
                left: 10px;
                transform: translateY(-50%);
                color: #cacaca;
                font-size: 2rem;
            }

            .action-participation,
            .action-image-uploader,
            .action-add-link {
                padding: 5px 10px;
                transition: background-color .2s ease;
                transition: color .2s ease;
                cursor: pointer;
            }

            .action-participation.green:hover {
                color: white;
                background-color: #e4514e;
            }

            .action-participation.red:hover {
                color: white;
                background-color: #9fbd38;
            }

            .action-image-uploader:hover,
            .action-add-link:hover {
                background-color: rgb(51, 51, 51);
                color: white;
            }

            #corner_dev<?= "$blockKey " ?>.dropdown {
                display: inline;
            }

            .screenshot-container {
                display: none;
                margin-top: 10px;
            }

            .screenshot-container > div {
                margin-bottom: 10px;
            }

            .screenshot-container > .image-container {
                border: 1px solid rgb(227, 227, 227);
                overflow: hidden;
            }

            .screenshot-container img {
                transition: transform .2s ease;
            }

            .screenshot-container img:hover {
                transform: scale(1.2);
            }

            #corner-screenshot-preview<?= $blockKey ?> {
                position: fixed;
                display: none;
                top: 0;
                left: 0;
                width: 100vw;
                height: 100vh;
                background: rgba(0, 0, 0, .5);
                z-index: 9999999;
            }

            .controlls<?= $blockKey ?> {
                position: inherit;
                top: 0;
                left: 0;
                background-color: transparent;
                display: flex;
                flex-direction: row;
                color: white;
                padding: 20px;
            }

            .controlls<?= "$blockKey " ?>[data-target=close] {
                padding: 10px 14px;
                border-radius: 50%;
                background: rgba(233, 233, 233, .5);
                cursor: pointer;
            }

            .image-container<?= $blockKey ?> {
                display: grid;
                grid-template-columns: repeat(3, 1fr);
                grid-template-rows: repeat(3, 1fr);
                align-items: center;
                width: 100vw;
                height: 100vh;
            }

            #screenshot-preview<?= $blockKey ?> {
                -webkit-user-drag: none;
                grid-column-start: 2;
                grid-row-start: 2;
            }

            .action-text:hover {
                text-decoration: underline;
                cursor: pointer;
            }

            .action-tracking {
                cursor: pointer;
                font-size: 2rem;
            }

            .action-tracking .fa {
                transition: color .2s ease;
            }

            .action-tracking:hover .fa-star-o,
            .action-tracking.active .fa {
                color: #ffc900;
            }

            .tags-container {
                margin-bottom: 5px;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                gap: 5px;
            }

            .label {
                line-height: 1.4;
            }

            .label.label-disabled {
                background-color: grey;
            }

            @media screen and (max-width: 768px) {
                #corner-card<?= $blockKey ?> {
                }
            }

            .action-tag {
                cursor: default;
            }

            #action-list-types<?= $blockKey ?> {
                background-color: rgba(0, 0, 0, .1);
                border-radius: 5px;
            }

            .corner-page-button .fa-chevron-down {
                transition: transform .2s ease-in;
            }

            .corner-page-button[aria-expanded=true] .fa-chevron-down {
                transform: rotate(180deg);
            }

            .status-filter {
                display: flex;
                gap: 5px;
                flex-wrap: wrap;
                margin: 0 5px 10px 5px;
            }

            .corner-page-link.disabled > span:nth-child(2) {
                display: none !important;
            }

            .corner-page-link > .corner-page-actions,
            .corner-page-button > .corner-page-actions {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: end;
                gap: 5px;
            }

            .corner-actions {
                display: flex;
                flex-direction: row;
            }

            .corner-actions > *:hover, .corner-actions > *:active, .corner-actions > *:focus {
                text-decoration: none;
            }

            [data-target=costum_setting] {
                display: none;
            }
        </style>
        <div id="corner-screenshot-preview<?= $blockKey ?>">
            <div class="controlls<?= $blockKey ?>">
                <span data-target="close"><span class="fa fa-times"></span></span>
            </div>
            <div class="image-container<?= $blockKey ?>">
                <img id="screenshot-preview<?= $blockKey ?>" src=""/>
            </div>
        </div>
        <div id="action-preview<?= $blockKey ?>"></div>
        <div id="corner_dev<?= $blockKey ?>">
            <div id="corner-card<?= $blockKey ?>">
                <div id="corner-header<?= $blockKey ?>">
                <span class="turn-back">
                    <span class="fa fa-arrow-left"></span>
                </span>
                    <div class="corner-title">
                        Corner dev
                    </div>
                    <div class="corner-actions">
                        <a href="" target="_blank" class="disabled" data-target="project-detail">
                            <span class="fa fa-info"></span>
                        </a>
                        <?php
                            if ($is_costum_admin || $is_developper) { ?>
                                <span data-target="setting">
                                    <span class="fa fa-cog"></span>
                                </span>
                                <?php
                            } ?>
                        <span class="corner-close">
                        <span class="fa fa-times"></span>
                    </span>
                    </div>
                </div>
                <div id="corner-body<?= $blockKey ?>" class="co-scroll">
                    <div id="link-container<?= $blockKey ?>">
                        <?php
                            if ($is_costum_admin) { ?>
                                <span class="corner-page-link" data-target="costum_setting">
                            <span>Reglage pour le costum</span>
                            <span class="corner-page-actions">
                                <span class="fa fa-chevron-right right"></span>
                            </span>
                        </span>
                                <span class="corner-page-button" data-target="create_project">Créer un projet</span>
                                <?php
                            }
                            if ($is_costum_admin || $is_developper) { ?>
                                <span class="corner-page-link" data-target="project_list">
                            <span>Projet : <span id="active-project<?= $blockKey ?>">(Aucun projet sélectionné)</span></span>
                            <span class="corner-page-actions">
                                <span class="badge" id="project-list-badge<?= $blockKey ?>">0</span>
                                <span class="fa fa-chevron-right right"></span>
                            </span>
                        </span>
                                <?php
                            } ?>
                        <span class="corner-page-button" style="grid-template-columns: 1fr 150px;" data-toggle="collapse"
                              data-target="#action-list-types<?= $blockKey ?>">
                        <span>Liste des actions</span>
                        <span class="corner-page-actions">
                            <span class="label label-danger all-action-badge<?= $blockKey ?>">0</span>
                            <span class="label label-warning mine-action-badge<?= $blockKey ?>">0</span>
                            <span class="label label-default context-linked-action-badge<?= $blockKey ?>">0</span>
                            <span class="fa fa-chevron-down right"></span>
                        </span>
                    </span>
                        <div id="action-list-types<?= $blockKey ?>" class="collapse out">
                        <span class="corner-page-link" data-target="action_list" data-scope="all">
                            <span>Toutes les actions</span>
                            <span class="corner-page-actions">
                                <span class="label label-danger all-action-badge<?= $blockKey ?>">0</span>
                                <span class="fa fa-chevron-right right"></span>
                            </span>
                        </span>
                            <span class="corner-page-link" data-target="action_list" data-scope="mine">
                            <span>Mes actions</span>
                            <span class="corner-page-actions">
                                <span class="label label-warning mine-action-badge<?= $blockKey ?>">0</span>
                                <span class="fa fa-chevron-right right"></span>
                            </span>
                        </span>
                            <span class="corner-page-link" data-target="action_list" data-scope="">
                            <span>Liées à cet url</span>
                            <span class="corner-page-actions">
                                <span class="label label-default context-linked-action-badge<?= $blockKey ?>">0</span>
                                <span class="fa fa-chevron-right right"></span>
                            </span>
                        </span>
                        </div>
                        <span class="corner-page-button" id="new-action<?= $blockKey ?>">Ajouter une action</span>
                        <span class="corner-page-button" id="declare-bug<?= $blockKey ?>">Déclarer un bug</span>
                    </div>
                    <div id="context-container<?= $blockKey ?>">
                    </div>
                </div>
            </div>
            <div id="corner-button-container<?= $blockKey ?>">
                <button id="corner-button<?= $blockKey ?>">
                    <img style="width: 100%; height: 100%" src="<?= Yii::app()->getModule('costum')->getAssetsUrl() ?>/images/corner_dev/oceco.png"
                         alt="oceco.png">
                </button>
            </div>
        </div>
        <script>
            (function ($, W) {
                // Début section des variables globales
                trad['tracking'] = 'En cours';
                trad['todiscuss'] = 'À valider';
                trad['totest'] = 'À tester';
                trad['everyone'] = 'Tout le monde';

                const _storage_version = 'v4';
                const _animate_duration = 180;
                let _last_input_value = '';
                let _current_location = W.location.href;
                const _is_costum = typeof costum === 'object' && costum !== null;
                const _is_costum_admin = JSON.parse(JSON.stringify(<?= json_encode($is_costum_admin) ?>));
                const _is_developper = JSON.parse(JSON.stringify(<?= json_encode($is_developper) ?>));

                const _open_context_container = {
                    costum_setting: function () {
                        const corner_header = $('#corner-header<?= $blockKey ?> .corner-title');
                        const context_container_dom = $('#context-container<?= $blockKey ?>');

                        const storage = get_storage();
                        mylog.log('Rinelfi storage', storage);

                        corner_header.fadeIn(_animate_duration).text('Réglage du costum');
                        context_container_dom.empty().html(`
                    <div class="form-group">
                        <label>Le bouton est visible par</label>
                        ${['everyone', 'administrator'].map(function (__user) {
                            mylog.log('Rinelfi user', __user, trad[__user]);
                            return `
                            <div class="radio">
                                <label>
                                    <input type="radio" name="cd_visibility" value="${trad[__user]}" ${storage.cs_visibility === trad[__user] ? 'checked' : ''}>
                                    ${trad[__user]}
                                </label>
                            </div>
                            `;
                        }).join('')}
                    </div>
                    `);
                        adjust_view();
                    },
                    project_list  : function () {
                        const corner_header_dom = $('#corner-header<?= $blockKey ?> .corner-title');
                        const context_container_dom = $('#context-container<?= $blockKey ?>');

                        corner_header_dom.fadeIn(_animate_duration).text('Liste des projets');

                        function load_view() {
                            var filter_value = $('.corner-filter-input').length > 0 ? $('.corner-filter-input').val() : '';
                            context_container_dom.css('align-self', 'center').empty().html('<div class="view_loader" style="width: 100%; text-align: center"><img src="' + baseUrl + '/plugins/lightbox2/img/loading.gif" alt="loader" style="width: 32px; height: 32px;" /></div>');
                            let promise;
                            if (filter_value) promise = get_project_names(filter_value);
                            else promise = get_project_names();
                            promise.then(function (__projects) {
                                var html = '<div class="corner-filter-container"><i class="fa fa-search"></i><input type="search" placeholder="Recherche" class="corner-filter-input" value="' + filter_value + '"></div>';

                                if (Object.keys(__projects).length) {
                                    var storage = get_storage();
                                    html += '<form id="project-selection<?= $blockKey ?>">';
                                    $.each(Object.keys(__projects), function (__, __key) {
                                        var project = __projects[__key];
                                        var checked = storage['active_project'] && storage['active_project']['id'] === __key ? 'checked="cheched"' : '';
                                        var container_style = project['background'] ? 'style="background: url(\'' + project['background'] + '\') no-repeat; background-size: cover;"' : '';
                                        html += '<div class="corner-element" ' + container_style + '><div class="checkbox" style="background-color: rgba(255, 255, 255, .7); border-radius: 5px"><label style="padding: 0; padding: 0 7px;"><input type="radio" value="' + __key + '" ' + checked + ' name="project-selection<?= $blockKey ?>" data-name="' + project['name'] + '" data-slug="' + project['slug'] + '"> ' + project['name'] + '</label></div></div>';
                                    });
                                    html += '</form>';
                                } else {
                                    if (_is_costum_admin) html += '<div class="corner-element corner-element" data-target="create_project">Aucun résultat, Créer un projet</div>';
                                    else html += '<div class="corner-element center">Aucun résultat à afficher</div>';
                                }

                                context_container_dom.empty().html(html).css('align-self', '');

                                if ($('.corner-filter-input').val()) {
                                    $('.corner-filter-input').each(function () {
                                        this.setSelectionRange(this.value.length, this.value.length);
                                    });
                                    $('.corner-filter-input').focus();
                                }

                                var timeout = null;
                                $('.corner-filter-input').on('input', function (__e) {
                                    var self = this;
                                    if (timeout != null) {
                                        clearTimeout(timeout);
                                        timeout = null;
                                    }
                                    timeout = setTimeout(function () {
                                        if (_last_input_value !== self.value) {
                                            load_view();
                                            _last_input_value = self.value;
                                        }
                                    }, 1000);
                                });

                                $('[name="project-selection<?= $blockKey ?>"]').on('change', function (__e) {
                                    const self = $(this);
                                    const name = self.data('name');
                                    const slug = self.data('slug');
                                    $('#active-project<?= $blockKey ?>').text(name);
                                    $('[data-target=project-detail]').attr('href', `${baseUrl}/#@${slug}`);
                                    $('.corner-page-link, .corner-page-button').removeClass('disabled');
                                    // Revenir à l'écran d'ouverture
                                    $('#corner_dev<?= $blockKey ?> .turn-back').trigger('click');

                                    set_storage({
                                        active_project: {
                                            id: self.val(),
                                            name,
                                            slug
                                        }
                                    }).then(function (__storage) {
                                        if (_is_costum_admin) save_costum_setting();
                                    });
                                });
                                // adaptation de l'affichage
                                adjust_view();
                            });
                        }

                        load_view();
                    },
                    action_list   : function (__scope) {
                        // définition des variables à utiliser notament des éléments du DOM
                        const corner_header = $('#corner-header<?= $blockKey ?> .corner-title');
                        const context_container = $('#context-container<?= $blockKey ?>');

                        // afficher le titre du corner
                        corner_header.fadeIn(_animate_duration).text('Liste des actions');

                        /**
                         * Charger la vue et initialiser l'événement de recherche
                         * Il ne nécessite pas de paramètre
                         * @return void
                         */
                        function load_view() {
                            // stocker la valeur de l'input de recherche
                            var filter_value = $('.corner-filter-input').length > 0 ? $('.corner-filter-input').val() : '';
                            // nettoyer le contenu et afficher à la place un loader
                            context_container.css('align-self', 'center').empty().html('<div class="view_loader" style="width: 100%; text-align: center"><img src="' + baseUrl + '/plugins/lightbox2/img/loading.gif" alt="loader" style="width: 32px; height: 32px;" /></div>');

                            // créer une variable de promise pour préparer la requête back end
                            var promise = null;
                            // vérifier si une recherche est faite
                            if (filter_value) promise = get_filtered_actions(filter_value, __scope);
                            else promise = get_filtered_actions('', __scope);

                            // executer la requête
                            promise.then(function (__actions) {
                                // initier le HTML par le nouveau input à utiliser pour la recherche
                                let html = '<div class="corner-filter-container"><i class="fa fa-search"></i><input type="search" placeholder="Recherche" class="corner-filter-input" value="' + filter_value + '"></div>';
                                const status = ['todiscuss', 'todo', 'tracking', 'totest', 'done'];
                                html += `
                                <div class="status-filter">
                                    ${status.map(function (__status) {
                                    return `<span class="btn btn-primary btn-xs" data-target="${__status}">${trad[__status]}</span>`
                                }).join('')}
                                </div>
                                `;

                                // construire le squelette de la liste des actions
                                if (__actions.length) {
                                    $.each(__actions, function (__, __action) {
                                        html += action_template(__action);
                                    });
                                } else {
                                    html += '<div class="corner-element center">Aucun résultat à afficher</div>';
                                }
                                context_container.empty().html(html).css('align-self', '');

                                // afficher le détail d'une action
                                $('.action-text').on('click', function (__e) {
                                    show_action_detail_modal($(this).data('target')).then(function (__html) {
                                        $('#action-preview<?= $blockKey ?>').empty().html(__html);
                                    });
                                });

                                // evenement de participation
                                $('.action-participation').on('click', function () {
                                    // décider de ne plus participer
                                    const action_id = $(this).data('target');
                                    if ($(this).hasClass('green')) {
                                        const self = this;
                                        const params = {
                                            id        : action_id,
                                            collection: 'actions',
                                            path      : 'links.contributors.' + userConnected['_id']['$id'],
                                            value     : null
                                        };
                                        dataHelper.path2Value(params, function (__data) {
                                            if (__data['result']) {
                                                toastr.success('Vous ne participez plus à cette action');
                                                $(self).removeClass('green').addClass('red').empty().html('<span class="fa fa-link"></span> Participer');
                                            }
                                        });
                                    } else
                                        // décider de participer
                                    if ($(this).hasClass('red')) {
                                        const self = this;
                                        const params = {
                                            id        : action_id,
                                            collection: 'actions',
                                            path      : 'links.contributors.' + userConnected['_id']['$id'],
                                            value     : {
                                                type: 'citoyens'
                                            }
                                        };
                                        dataHelper.path2Value(params, function (__data) {
                                            if (__data['result']) {
                                                rocket_chat_contribution(action_id, [userConnected['_id']['$id']]);
                                                toastr.success('Vous êtes maintenant un participant de cette action');
                                                $(self).addClass('green').removeClass('red').empty().html('<span class="fa fa-unlink"></span> Se retirer');
                                            }
                                        });
                                    }
                                });

                                // evenement sur les screenshots
                                $('.action-image-uploader').on('click', function () {
                                    const action_id = $(this).data('target');
                                    const screenshot_container = $(this).parents('.corner-element').find('.screenshot-container');
                                    if (screenshot_container.is(':hidden')) {
                                        // fermer les fenêtres ouvrantes
                                        $('.screenshot-container').slideUp(_animate_duration);

                                        get_filtered_screenshots(action_id).then(function (__screenshots) {
                                            var screenshots = __screenshots.map(function (__map) {
                                                return baseUrl + '/upload/communecter/' + __map;
                                            });
                                            var html = '';
                                            $.each(screenshots, function (__, __screenshot) {
                                                html += '<div class="image-container"><img src="' + __screenshot + '" alt="' + __ + '" style="border-radius: 0; width: 100%;" /></div>';
                                            });
                                            html += '<div style="text-align: center;"><button class="btn btn-primary add-screenshot" data-target="' + action_id + '"><span class="fa fa-plus"></span> Ajouter</button></div>'
                                            screenshot_container.empty().html(html).slideDown(_animate_duration, function () {
                                                $('.image-container img').each(function () {
                                                    const image = new Image();
                                                    image.onload = adjust_view;
                                                    image.src = $(this).attr('src');
                                                });
                                                adjust_view();
                                            });

                                            // affichage aperçu d'une image
                                            $('.image-container img').on('click', function () {
                                                $('#screenshot-preview<?= $blockKey ?>').attr('src', $(this).attr('src'));
                                                $('#corner-screenshot-preview<?= $blockKey ?>').fadeIn(_animate_duration);
                                            });

                                            $('.add-screenshot').on('click', function (__e) {
                                                __e.preventDefault();
                                                screenshot_container.slideUp(_animate_duration, adjust_view);
                                                var self = this;
                                                dyFObj.openForm({
                                                        jsonSchema: {
                                                            title      : 'Ajouter des captures',
                                                            description: '',
                                                            properties : {
                                                                action_media: {
                                                                    inputType    : 'uploader',
                                                                    label        : 'Capture d\'écran',
                                                                    docType      : 'image',
                                                                    itemLimit    : 0,
                                                                    showUploadBtn: false,
                                                                    filetypes    : [
                                                                        'png', 'jpg', 'jpeg', 'gif'
                                                                    ],
                                                                },
                                                            },
                                                            beforeBuild: function () {
                                                                uploadObj.set('actions', action_id);
                                                            },
                                                            afterBuild : function () {
                                                                setTimeout(function () {
                                                                    $('.qq-upload-button-selector.btn.btn-primary').css({
                                                                        position : 'absolute',
                                                                        left     : '50%',
                                                                        transform: 'translateX(-50%)'
                                                                    });
                                                                }, 200);
                                                            },
                                                            save       : function () {
                                                                dyFObj.commonAfterSave(null, function () {
                                                                    var params = {
                                                                        searchType  : ["documents"],
                                                                        fields      : ["id"],
                                                                        filters     : {
                                                                            'id': action_id,
                                                                        },
                                                                        notSourceKey: true,
                                                                    };
                                                                    var url = baseUrl + '/' + moduleId + '/search/globalautocomplete';
                                                                    ajaxPost(null, url, params, function (__data) {
                                                                        dataHelper.path2Value({
                                                                            id        : action_id,
                                                                            collection: 'actions',
                                                                            path      : 'media.images',
                                                                            value     : Object.keys(__data.results)
                                                                        }, function (__data) {
                                                                            if (__data['result'] && __data['result'] === true) {
                                                                                toastr.success(__data['msg']);
                                                                                dyFObj.closeForm();
                                                                            }
                                                                        })
                                                                    })
                                                                });
                                                            }
                                                        }
                                                    },
                                                    null,
                                                    null,
                                                    null,
                                                    null, {
                                                        type: 'bootbox'
                                                    });
                                            });
                                        });
                                    } else {
                                        screenshot_container.slideUp(_animate_duration, adjust_view);
                                    }
                                });

                                // ajout de liens
                                $('.action-add-link').on('click', function () {
                                    const action_id = $(this).data('target');
                                    get_selected_action(action_id).then(function (__action) {
                                        dyFObj.openForm({
                                            jsonSchema: {
                                                title      : 'Modifier les URLS',
                                                description: 'Ajouter des nouveaux URLS ou en supprimer d\'autres pour l\'action ' + __action['name'],
                                                properties : {
                                                    action_update_links: {
                                                        label    : 'Urls',
                                                        inputType: 'array',
                                                        value    : __action['urls'] ? __action['urls'] : []
                                                    }
                                                },
                                                save       : function (__form) {
                                                    const path2value = {
                                                        id        : action_id,
                                                        collection: 'actions',
                                                        path      : 'urls',
                                                        value     : getArray('.action_update_linksarray')
                                                    };
                                                    dataHelper.path2Value(path2value, function (__data) {
                                                        if (__data['result'] && __data['result'] === true) {
                                                            toastr.success(__data['msg']);
                                                            dyFObj.closeForm();
                                                        }
                                                    });
                                                }
                                            }
                                        }, null, null, null, null, {
                                            type: 'bootbox'
                                        });
                                    });
                                });

                                // reassigner les événements liés au DOM de l'input de recherche
                                if ($('.corner-filter-input').val()) {
                                    // mettre le curseur à la fin
                                    $('.corner-filter-input').each(function () {
                                        this.setSelectionRange(this.value.length, this.value.length);
                                    });
                                    // mettre le focus sur l'input s'il contient du texte
                                    $('.corner-filter-input').focus();
                                }

                                // temps d'attende pour exécuter la recherche si on tape du texte dans l'input de recherche
                                var timeout = null;
                                $('.corner-filter-input').on('input', function (__e) {
                                    var self = this;
                                    if (timeout != null) {
                                        clearTimeout(timeout);
                                        timeout = null;
                                    }
                                    timeout = setTimeout(function () {
                                        if (_last_input_value !== self.value) {
                                            load_view();
                                            _last_input_value = self.value;
                                        }
                                    }, 1000);
                                });

                                // Animation et adaptation de l'affichage
                                adjust_view();
                            });
                        }

                        // charger la vue une fois
                        load_view();
                    }
                }
                // Fin section des variables globales

                // Début section des fonctions
                /**
                 * Vérifier si la variable n'existe pas, n'est pas définie ou est vide
                 * @param {any} __input Variable à évaluer
                 * @returns {boolean}
                 */
                function is_empty(__input) {
                    return typeof __input === 'undefined' || typeof __input === 'object' && (__input === null || __input instanceof Array && __input.length === 0) || typeof __input === 'string' && __input === '';
                }

                /**
                 * Sauvegarder dans la base de données les paramètres liés au costum
                 *
                 * @return {Promise.<boolean}
                 */
                function save_costum_setting() {
                    storage = get_storage();
                    value = {};
                    if (!is_empty(storage.active_project)) value = {
                        id  : storage.active_project.id,
                        name: storage.active_project.name,
                        slug: storage.active_project.slug,
                    };
                    value['admin_only'] = storage.cs_visibility === trad['administrator'];

                    const path2value = {
                        id        : costum.contextId,
                        collection: costum.contextType,
                        path      : 'costum.cd_project',
                        value
                    };

                    return new Promise(function (__resolve) {
                        dataHelper.path2Value(path2value, function (__response) {
                            __resolve(__response['result']);
                        });
                    });
                }

                /**
                 * Générer un template pour une action
                 * @param {Object} __action
                 * @param {boolean} __action.is_contributor L'utilisateur courant fait-il parti de cette action ou pas
                 * @param {string} __action.status Status de l'action
                 * @returns {string}
                 */
                function action_template(__action) {
                    // Object regroupant les différents status d'une action
                    const action_status_map = {
                        todiscuss: {
                            classname: 'warning'
                        },
                        todo     : {
                            classname: 'info'
                        },
                        tracking : {
                            classname: 'warning'
                        },
                        totest   : {
                            classname: 'info'
                        },
                        done     : {
                            classname: 'success'
                        },
                        closed     : {
                            classname: 'danger'
                        }
                    };

                    const action_participation = '<span class="action-participation ' + (__action['is_contributor'] ? 'green' : 'red') + '" data-target="' + __action['id'] + '"><span class="fa fa-' + (__action['is_contributor'] ? 'unlink' : 'link') + '"></span> ' + (__action['is_contributor'] ? 'Se retirer' : 'Participer') + '</span>';
                    const action_image_uploader = '<span class="action-image-uploader" data-target="' + __action['id'] + '"><span class="fa fa-picture-o"></span> Captures</span>';
                    const action_add_link = '<span class="action-add-link" data-target="' + __action['id'] + '"><span class="fa fa-external-link"></span> URLS</span>';
                    let tags_html = '';
                    $.each(__action['tags'], function (__, __tag) {
                        tags_html += '<span class="badge action-tag" data-content="#' + __tag + '"><span class="fa fa-tag"></span> ' + __tag + '</span> ';
                    });

                    const real_status = __action['tracking'] ? 'tracking' : __action.status;
                    return `
                        <div class="corner-element" data-status="${real_status}">
                            <div class="corner-element-body">
                                <div>
                                    <span class="label label-${action_status_map[real_status]['classname']} pull-right">${trad[real_status]}</span>
                                    <span class="action-tracking ${__action['tracking'] ? 'active' : ''}" data-id="${__action['id']}">
                                        <span class="fa fa-${__action['tracking'] ? 'star' : 'star-o'}"></span>
                                    </span>
                                    <span class="action-text" data-target="${__action['id']}">${__action['name']}</span>
                                    <div class="tags-container">${tags_html}</div>
                                </div>
                                <div class="screenshot-container"></div>
                            </div>
                            <div class="corner-element-footer">${action_participation + action_image_uploader + action_add_link}</div>
                        </div>`;
                }

                /**
                 * Ajuster la vue courante
                 */
                function adjust_view() {
                    const link_container_dom = $('#link-container<?= $blockKey ?>');
                    const context_container_dom = ['0px', '0%'].includes(link_container_dom.css('margin-left')) ? link_container_dom : $('#context-container<?= $blockKey ?>');
                    let overflow = false;

                    let current_height = $('#corner-body<?= $blockKey ?>').height();
                    let auto_height = context_container_dom.css('height', 'auto').height();
                    const top_offset = 60 + $('#corner-header<?= $blockKey ?> .corner-title').outerHeight();

                    if (auto_height > $(W).height() - top_offset) {
                        auto_height = $(W).height() - top_offset;
                        overflow = true;
                    }

                    if ($('#corner-card<?= $blockKey ?>').is(':visible')) {
                        $('#corner-body<?= $blockKey ?>').height(current_height).animate({
                            height: auto_height,
                        }, {
                            duration: _animate_duration,
                            complete: function () {
                                if (overflow) $(this).css('overflow-y', 'auto');
                                else $(this).css('overflow-y', '');
                            }
                        });
                    }
                }

                /**
                 * Générer une clé pour le stockage des valeurs
                 * @returns {string}
                 */
                function generate_storage_key() {
                    const user_connected = userConnected && userConnected['_id'] && userConnected['_id']['$id'] ? userConnected['_id']['$id'].substring(-12) : '';
                    const costum_key = costum && costum['_id'] && costum['_id']['$id'] ? costum['_id']['$id'].substring(-12) : '';
                    return user_connected + costum_key + _storage_version;
                }

                /**
                 * Lecture du stockage du navigateur pour corner_dev
                 * @returns {active_project: {id: string, name: string, slug: string}, cs_visibility: string}
                 */
                function get_storage() {
                    const key = generate_storage_key();
                    let output = {
                        active_project: null,
                        cs_visibility : trad['everyone']
                    };
                    const storage = localStorage.getItem(key + 'corner_dev');
                    if (storage !== null) {
                        output = JSON.parse(storage);
                    }
                    return output;
                }

                /**
                 * Enregistrer une valeur dans le storage
                 * @return {Promise.<{active_project: {id: string, name: string, slug: string}, cs_visibility: string}>}
                 */
                function set_storage(__object = {}) {
                    const key = generate_storage_key();
                    return new Promise(function (__resolve) {
                        if (!is_empty(__object['active_project'])) {
                            const url = baseUrl + '/costum/project/project/request/room';
                            const post = {
                                project: __object['active_project']['id']
                            };
                            ajaxPost(null, url, post, function (__room) {
                                __object['active_project']['room'] = __room;
                                const storage = get_storage();
                                const output = $.extend(true, {}, storage, __object);
                                localStorage.setItem(key + 'corner_dev', JSON.stringify(output));
                                __resolve(output);
                            });
                        } else {
                            const storage = get_storage();
                            const output = $.extend(true, {}, storage, __object);
                            localStorage.setItem(key + 'corner_dev', JSON.stringify(output));
                            __resolve(output);
                        }
                    });
                }

                /**
                 * Permet le tracking d'une action
                 * @param {boolean} __track - Status de l'action si elle est tracké ou pas
                 * @param {string} __action - Identifiant de l'action
                 */
                function track_action(__track, __action) {
                    const params = {
                        id        : __action,
                        collection: 'actions',
                        path      : 'tracking',
                        value     : __track
                    };
                    dataHelper.path2Value(params, function (__response) {
                        if (!__response['result']) toastr.error(__response['msg']);
                    });
                }

                /**
                 * Affiche un modal contenant les détails d'une action.
                 * Possibilité d'ajouter des sous tâches et inviter des participants.
                 * @param {string} __id Identifiant de l'action à récupérer
                 * @return {Promise.<string>}
                 */
                function show_action_detail_modal(__id) {
                    return new Promise(function (__resolve) {
                        const url = baseUrl + '/costum/project/action/request/action_detail_html';
                        const post = {
                            id: __id
                        };
                        ajaxPost(null, url, post, __resolve, null, 'text');
                    });
                }

                /**
                 * Récupère les données d'une action sélectionnée
                 * @param {string} __action identifiant de l'action à récupérer
                 * @return {Promise}
                 */
                function get_selected_action(__action) {
                    return new Promise(function (__resolve) {
                        const url = baseUrl + '/costum/project/action/request/action';
                        const post = {
                            action: __action
                        };
                        ajaxPost(null, url, post, __resolve);
                    });
                }

                /**
                 * Récupère les tags d'une action sélectionnée
                 * @param {string} __action identifiant de l'action à récupérer
                 * @return {Promise.<string[]>}
                 */
                function get_tags(__action) {
                    return new Promise(function (__resolve) {
                        const url = baseUrl + '/costum/project/action/request/tags';
                        const post = {
                            id: __action
                        };
                        ajaxPost(null, url, post, __resolve);
                    });
                }

                /**
                 * Récupère la liste de tous les screenshots associé à l'action
                 * @param {String} __action identifiant de l'action
                 * @return {Promise}
                 */
                function get_filtered_screenshots(__action) {
                    return new Promise(function (__resolve) {
                        const url = baseUrl + '/costum/project/action/request/media';
                        const post = {
                            action: __action
                        };
                        ajaxPost(null, url, post, __resolve);
                    });
                }

                /**
                 * Récupère les actions filtrés sur l'URL courant.
                 * Récupère seulement les valeurs de retour au cas où la requête est un succès.
                 * Ignore les erreurs s'il y en a.
                 * @param {string} [__text] Terme à rechercher
                 * @param {string} [__scope] Encapsulation de la requête (all, mine, vide)
                 * @returns {Promise}
                 */
                function get_filtered_actions(__text, __scope) {
                    return new Promise(function (__resolve) {
                        // endpoint de la requête
                        const url = `${baseUrl}/costum/project/action/request/actions${__scope ? `/scope/${__scope}` : ''}`;
                        // construire la requête
                        const post = {
                            filters: {
                                urls: {
                                    '$in': [_current_location]
                                }
                            }
                        };
                        /**
                         * Vérifier si un projet est sélectionné
                         * Si oui on filtre la liste des actions en fonction du projet sélectionné
                         * Sinon on sélectionne tout
                         */
                        const storage = get_storage();
                        if (storage && storage['active_project']) {
                            post['filters']['parentId'] = storage['active_project']['id'];
                        }
                        if (__text) post['text'] = __text;
                        // lance la requête
                        ajaxPost(null, url, post, __resolve);
                    });
                }

                /**
                 * Fonction servant à recevoir la liste de tous les projets
                 * @param {string} [__text=''] Contient le terme à chercher s'il s'agit d'une recherche
                 * @param {Object} [__params={}] Paramètres facultatifs
                 * @return {Promise.<Object.<string, {name: string, slug: string, background: string}>>}
                 */
                function get_project_names(__text = '', __params = {}) {
                    const url = baseUrl + '/costum/project/project/request/project_names';
                    const post = {
                        params: {}
                    };
                    if (__text) post['text'] = __text;

                    for (let key in __params) post.params[key] = __params[key];

                    return new Promise(function (__resolve) {
                        ajaxPost(null, url, post, __resolve);
                    });
                }

                /**
                 * Envoyer une notification RocketChat sur la participations des citoyens à une action créée
                 * @param {String} __action identifiant de l'action auquel le contributeur participe
                 * @param {Array} __contributors liste des identifiants des participants
                 * @return {Promise.<{status: boolean, msg: string}[]>}
                 */
                function rocket_chat_contribution(__action, __contributors) {
                    const url = baseUrl + '/costum/project/action/request/contribution_rocker_chat';
                    const storage = get_storage();
                    const post = {
                        channel     : storage['active_project']['slug'],
                        action      : __action,
                        project_name: storage['active_project']['name'],
                        contributors: __contributors
                    };
                    return new Promise(function (__resolve) {
                        ajaxPost(null, url, post, __resolve);
                    });
                }

                /**
                 * Récupérer le paramètre de corner-dev par défaut du costum
                 * @return {Promise.<{id: string, slug: string, name: string}>}
                 */
                function get_costum_corner_dev() {
                    const url = `${baseUrl}/costum/project/project/request/cd_from_costum`;
                    const post = {
                        collection: costum.contextType,
                        id        : costum.contextId
                    };
                    return new Promise(function (__resolve, __reject) {
                        ajaxPost(null, url, post, function (__response) {
                            mylog.log('Rinelfi corner dev', __response);
                            if (!is_empty(__response['id'])) __resolve(__response);
                            else __reject(null);
                        }, __reject);
                    })
                }

                /**
                 * Fonction pour l'initialisation de la vue sur le projet ainsi que le chargement des rooms
                 *
                 * @return {Promise}
                 */
                function init_view() {
                    const project_list_dom = $('[data-target=project_list]');
                    const project_detail_dom = $('[data-target=project-detail]');
                    const setting_dom = $('.corner-actions [data-target=setting]');
                    project_list_dom.removeClass('disabled');
                    $('.corner-page-link, .corner-page-button').removeClass('disabled');
                    setting_dom.hide();
                    project_detail_dom.hide();

                    return new Promise(function (__resolve) {
                        const storage = get_storage();
                        if (!is_empty(costum)) {
                            setting_dom.show();
                            if (!_is_developper) project_list_dom.addClass('disabled');
                            get_costum_corner_dev().then(function (__project) {
                                set_storage({
                                    active_project: {
                                        id  : __project.id,
                                        name: __project.name,
                                        slug: __project.slug
                                    },
                                    cs_visibility : __project['admin_only'] ? trad['administrator'] : trad['everyone']
                                });
                                $('#active-project<?= $blockKey ?>').text(__project.name);
                                project_detail_dom.attr('href', `${baseUrl}/#@${__project.slug}`).show();
                                if (_is_costum_admin) project_list_dom.removeClass('disabled');
                            }).catch(function (__reason) {
                                const project = _is_costum && costum.contextType === 'projects' ? costum.contextId : '';
                                if (!is_empty(project)) {
                                    set_storage({
                                        active_project: {
                                            id  : project,
                                            name: costum.title,
                                            slug: costum.slug
                                        }
                                    });
                                    $('#active-project<?= $blockKey ?>').text(costum.title);
                                    project_detail_dom.attr('href', `${baseUrl}/#@${costum.slug}`).show();
                                    __resolve();
                                } else if (_is_costum_admin) {
                                    project_list_dom.removeClass('disabled');
                                    const post = {};
                                    post[`parent.${costum.contextId}`] = {
                                        '$exists': 1
                                    };
                                    get_project_names('', post).then(function (__projects) {
                                        if (is_empty(Object.keys(__projects))) {
                                            const exclusions = [
                                                '.corner-page-button[data-target=create_project]',
                                                '.corner-page-link[data-target=project_list]',
                                                '.corner-page-link[data-target=costum_setting]'
                                            ];

                                            $('.corner-page-link, .corner-page-button').each(function () {
                                                const self = $(this);
                                                const filtered = exclusions.filter(function (__exclusion) {
                                                    return $(__exclusion).is(self)
                                                });
                                                if (is_empty(filtered)) self.addClass('disabled');
                                            });
                                        } else {
                                            const first = Object.keys(__projects)[0];
                                            set_storage({
                                                active_project: {
                                                    id  : first,
                                                    name: __projects[first]['name'],
                                                    slug: __projects[first]['slug']
                                                }
                                            });
                                            $('#active-project<?= $blockKey ?>').text(__projects[first]['name']);
                                            project_detail_dom.attr('href', `${baseUrl}/#@${__projects[first].slug}`).show();
                                        }
                                        __resolve();
                                    });
                                }
                            });
                        } else if (storage['active_project']) {
                            $('#active-project<?= $blockKey ?>').text(storage['active_project']['name']);
                            project_detail_dom.attr('href', `${baseUrl}/#@${storage.active_project.slug}`).show();
                            __resolve();
                        } else {
                            get_project_names().then(function (__projects) {
                                if (Object.keys(__projects).length) {
                                    const first = Object.keys(__projects)[0];
                                    set_storage({
                                        active_project: {
                                            id  : first,
                                            name: __projects[first]['name'],
                                            slug: __projects[first]['slug']
                                        }
                                    });
                                    $('#active-project<?= $blockKey ?>').text(__projects[first]['name']);
                                    project_detail_dom.attr('href', `${baseUrl}/#@${__projects[first].slug}`).show();
                                    __resolve();
                                }
                            });
                        }
                    });
                }

                // Fin section des fonctions

                /**
                 * DOM initialisé
                 */
                $(function () {
                    /**
                     * Variables
                     */
                    const corner_button_dom = $('#corner-button-container<?= $blockKey ?>');
                    const corner_card_dom = $('#corner-card<?= $blockKey ?>');
                    const corner_body_dom = $('#corner-body<?= $blockKey ?>');
                    const corner_header_dom = $('#corner-header<?= $blockKey ?>');
                    const link_container_dom = $('#link-container<?= $blockKey ?>');
                    const context_container_dom = $('#context-container<?= $blockKey ?>');
                    const turn_back_dom = $('#corner-header<?= "$blockKey " ?>.turn-back');

                    function load_badgets() {
                        const storage = get_storage();
                        if (!is_empty(storage) && !is_empty(storage.active_project)) {
                            get_filtered_actions(':all').then(function (__actions) {
                                const badge_dom = $('.all-action-badge<?= $blockKey ?>');
                                badge_dom.each(function () {
                                    const self = $(this);
                                    const initial = self.text() ? parseInt(self.text(), 10) : 0;
                                    self.prop('Counter', initial).animate({
                                        Counter: __actions['length']
                                    }, {
                                        duration: 1000,
                                        easing  : 'swing',
                                        step    : function (__now) {
                                            self.text(Math.ceil(__now));
                                        }
                                    });
                                });
                            });
                            get_filtered_actions('', 'mine').then(function (__actions) {
                                const badge_dom = $('.mine-action-badge<?= $blockKey ?>');
                                badge_dom.each(function () {
                                    const self = $(this);
                                    const initial = self.text() ? parseInt(self.text(), 10) : 0;
                                    self.prop('Counter', initial).animate({
                                        Counter: __actions['length']
                                    }, {
                                        duration: 1000,
                                        easing  : 'swing',
                                        step    : function (__now) {
                                            self.text(Math.ceil(__now));
                                        }
                                    });
                                });
                            });
                            get_filtered_actions().then(function (__actions) {
                                const badge_dom = $('.context-linked-action-badge<?= $blockKey ?>');
                                badge_dom.each(function () {
                                    const self = $(this);
                                    const initial = self.text() ? parseInt(self.text(), 10) : 0;
                                    self.prop('Counter', initial).animate({
                                        Counter: __actions['length']
                                    }, {
                                        duration: 1000,
                                        easing  : 'swing',
                                        step    : function (__now) {
                                            self.text(Math.ceil(__now));
                                        }
                                    });
                                });
                            });
                            get_project_names().then(function (__projects) {
                                const badge = $('#project-list-badge<?= $blockKey ?>');
                                const initial = badge.text() ? parseInt(badge.text(), 10) : 0;
                                badge.prop('Counter', initial).animate({
                                    Counter: Object.keys(__projects)['length']
                                }, {
                                    duration: 1000,
                                    easing  : 'swing',
                                    step    : function (__now) {
                                        $(this).text(Math.ceil(__now));
                                    }
                                });
                            });
                        }
                    }

                    // Quand on appuye sur le bouton de corner dev
                    corner_button_dom.on('click', function (__e) {
                        __e.preventDefault();
                        $(corner_button_dom).hide();
                        corner_card_dom.show();
                        load_badgets();
                    });

                    // Quand on ferme le menu du corner dev
                    corner_card_dom.find('.corner-close').on('click', function (__e) {
                        __e.preventDefault();
                        corner_button_dom.fadeIn(_animate_duration);
                        corner_card_dom.animate({
                            width : 0,
                            height: 0
                        }, _animate_duration, function () {
                            corner_card_dom.css({
                                display: '',
                                width  : '',
                                height : ''
                            });
                        });
                    });

                    // Quand on clique sur l'un des liens pour changer de page dans le menu
                    corner_body_dom.on('click', '.corner-page-link:not(.disabled)', function (__e) {
                        const self = this;
                        corner_header_dom.find('.corner-title').fadeOut(_animate_duration);
                        link_container_dom.animate({
                            marginLeft: '-=100%'
                        }, _animate_duration, 'easeInQuad', function () {
                            corner_header_dom.find('.turn-back').css({
                                'display': 'flex'
                            });
                            _open_context_container[$(self).data('target')]($(self).data('scope'));
                        });
                    });

                    turn_back_dom.on('click', function () {
                        const link_contents = link_container_dom.html();
                        corner_header_dom.find('.corner-title').fadeOut(_animate_duration);
                        context_container_dom.empty();
                        _last_input_value = '';

                        link_container_dom.animate({
                            marginLeft: '+=100%',
                        }, _animate_duration, 'easeOutQuart', function () {
                            corner_header_dom.find('.corner-title').fadeIn(_animate_duration);
                            corner_header_dom.find('.corner-title').text('Corner dev');
                            corner_header_dom.find('.turn-back').css({
                                display: ''
                            });
                            $('[data-target=setting]').fadeIn(_animate_duration);

                            const current_height = corner_body_dom.height(),
                                auto_height = link_container_dom.css('height', 'auto').outerHeight();
                            corner_body_dom.height(current_height).animate({
                                height: auto_height
                            }, _animate_duration, function () {
                                corner_body_dom.css('overflow-y', 'hidden');
                                link_container_dom.empty().html(link_contents);
                                load_badgets();
                            });
                        });
                    });

                    link_container_dom.on('click', '#new-action<?= $blockKey ?>:not(.disabled)', function (__e) {
                        __e.preventDefault();
                        corner_card_dom.find('.corner-close').trigger('click');
                        const default_finder = {};
                        const storage = get_storage();

                        default_finder[userConnected['_id']['$id']] = {
                            type: 'citoyens',
                            name: userConnected['name']
                        };

                        if (storage.active_project) {
                            dyFObj.openForm({
                                jsonSchema: {
                                    title     : 'Ajouter une action',
                                    properties: {
                                        action_name        : dyFInputs.name('action'),
                                        action_parentType  : dyFInputs.inputHidden("projects"),
                                        action_parentId    : dyFInputs.inputHidden(storage['active_project']['id']),
                                        action_creator     : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                        action_credits     : dyFInputs.inputHidden(1),
                                        action_idUserAuthor: dyFInputs.inputHidden(userConnected['_id']['$id']),
                                        action_idParentRoom: dyFInputs.inputHidden(storage['active_project']['room']),
                                        action_tags        : {
                                            inputType: 'tags',
                                            label    : trad.tags
                                        },
                                        action_contributors: {
                                            inputType   : 'finder',
                                            label       : 'Assigné à qui ?',
                                            multiple    : true,
                                            rules       : {
                                                lengthMin: [1, 'contributors'],
                                                required : true
                                            },
                                            initType    : ['citoyens'],
                                            values      : default_finder,
                                            initBySearch: true,
                                            initMe      : true,
                                            initContext : false,
                                            initContacts: false,
                                            openSearch  : true
                                        },
                                        action_urls        : {
                                            label    : 'Urls',
                                            inputType: 'array',
                                            value    : [
                                                W.location.toString()
                                            ]
                                        },
                                        action_status      : dyFInputs.inputHidden('todo')
                                    },
                                    save      : function (__form) {
                                        var value = {
                                            urls: getArray('.action_urlsarray')
                                        };
                                        var contributors = {};
                                        $.each(Object.keys(__form['action_contributors']), function (__, __key) {
                                            contributors[__key] = {
                                                type: 'citoyens'
                                            };
                                        });
                                        $.each(['scope', 'collection', 'action_contributors', 'action_urls[]'], function (__, __key) {
                                            delete __form[__key];
                                        });

                                        $.each(Object.keys(__form), function (__, __name) {
                                            var name = __name.substring(7);
                                            if (name === 'tags') value[name] = __form[__name].split(',');
                                            else value[name] = __form[__name];
                                        });
                                        var params = {
                                            collection: 'actions',
                                            value
                                        };

                                        dataHelper.path2Value(params, function (__data) {
                                            if (__data['result'] && __data['result'] === true) {
                                                var action_id = __data['saved']['id'];

                                                // Envoyer une notification rocket chat pour la nouvelle tâche
                                                var url = baseUrl + '/survey/answer/rcnotification/action/newaction';
                                                var post = {
                                                    projectname: storage['active_project']['name'],
                                                    actname    : value['name'],
                                                    channelChat: storage['active_project']['slug'],
                                                };
                                                ajaxPost(null, url, post);

                                                // Enregistrer les contributeurs
                                                dataHelper.path2Value({
                                                    id        : action_id,
                                                    collection: 'actions',
                                                    path      : 'links.contributors',
                                                    value     : contributors
                                                }, function (__data) {
                                                    if (__data['result'] && __data['result'] === true) {
                                                        rocket_chat_contribution(action_id, Object.keys(contributors));
                                                        toastr.success(__data.msg);
                                                        dyFObj.closeForm();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }, null, null, null, null, {
                                type: 'bootbox'
                            });
                        } else {
                            toastr.error('Vous devez sélectionner un projet pour ajouter une action');
                        }
                    });

                    link_container_dom.on('click', '#declare-bug<?= $blockKey ?>:not(.disabled)', function (__e) {
                        __e.preventDefault();
                        corner_card_dom.find('.corner-close').trigger('click');
                        const default_finder = {};
                        const storage = get_storage();

                        default_finder[userConnected['_id']['$id']] = {
                            type: 'citoyens',
                            name: userConnected['name']
                        };

                        if (storage.active_project) {
                            dyFObj.openForm({
                                jsonSchema: {
                                    title     : 'Déclared un bug',
                                    properties: {
                                        action_name        : dyFInputs.name('action'),
                                        action_parentType  : dyFInputs.inputHidden("projects"),
                                        action_parentId    : dyFInputs.inputHidden(storage['active_project']['id']),
                                        action_credits     : dyFInputs.inputHidden(0),
                                        action_creator     : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                        action_idUserAuthor: dyFInputs.inputHidden(userConnected['_id']['$id']),
                                        action_idParentRoom: dyFInputs.inputHidden(storage['active_project']['room']),
                                        action_tags        : dyFInputs.inputHidden(['bug']),
                                        action_contributors: {
                                            inputType   : 'finder',
                                            label       : 'Assigné à qui ?',
                                            multiple    : true,
                                            rules       : {
                                                lengthMin: [1, 'contributors'],
                                                required : true
                                            },
                                            initType    : ['citoyens'],
                                            values      : default_finder,
                                            initBySearch: true,
                                            initMe      : true,
                                            initContext : false,
                                            initContacts: false,
                                            openSearch  : true
                                        },
                                        action_urls        : {
                                            label    : 'Urls',
                                            inputType: 'array',
                                            value    : [
                                                W.location.toString()
                                            ]
                                        },
                                        action_status      : dyFInputs.inputHidden('todo')
                                    },
                                    save      : function (__form) {
                                        const value = {
                                            urls: getArray('.action_urlsarray')
                                        };
                                        const contributors = {};
                                        $.each(Object.keys(__form['action_contributors']), function (__, __key) {
                                            contributors[__key] = {
                                                type: 'citoyens'
                                            };
                                        });
                                        $.each(['scope', 'collection', 'action_contributors', 'action_urls[]'], function (__, __key) {
                                            delete __form[__key];
                                        });

                                        $.each(Object.keys(__form), function (__, __name) {
                                            const name = __name.substring(7);
                                            if (name === 'tags') value[name] = __form[__name].split(',');
                                            else value[name] = __form[__name];
                                        });
                                        const params = {
                                            collection: 'actions',
                                            value
                                        };

                                        dataHelper.path2Value(params, function (__data) {
                                            if (__data['result'] && __data['result'] === true) {
                                                var action_id = __data['saved']['id'];

                                                // Envoyer une notification rocket chat pour la nouvelle tâche
                                                var url = baseUrl + '/survey/answer/rcnotification/action/newbug';
                                                var post = {
                                                    projectname: storage['active_project']['name'],
                                                    actname    : value['name'],
                                                    channelChat: storage['active_project']['slug'],
                                                };
                                                ajaxPost(null, url, post);

                                                // Enregistrer les contributeurs
                                                dataHelper.path2Value({
                                                    id        : action_id,
                                                    collection: 'actions',
                                                    path      : 'links.contributors',
                                                    value     : contributors
                                                }, function (__data) {
                                                    if (__data['result'] && __data['result'] === true) {
                                                        toastr.success(__data.msg);
                                                        dyFObj.closeForm();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }, null, null, null, null, {
                                type: 'bootbox'
                            });
                        } else {
                            toastr.error('Vous devez sélectionner un projet pour ajouter une action');
                        }
                    });

                    $.each([context_container_dom, link_container_dom], function (__, __dom) {
                        __dom.on('click', '[data-target=create_project]:not(.disabled)', function (__e) {
                            dyFObj.openForm('project', null, null, null, {
                                afterSave: function (__data) {
                                    if (is_empty(__data['map']['parent'][costum.contextId])) {
                                        dataHelper.path2Value({
                                            id        : __data['id'],
                                            collection: __data['map']['collection'],
                                            path      : `parent.${costum.contextId}`,
                                            value     : {
                                                name: costum.title,
                                                type: costum.contextType
                                            }
                                        }, function (__response) {
                                            if (__response['result']) {
                                                dyFObj.closeForm();
                                                init_view();
                                            }
                                        })
                                    }
                                }
                            }, {
                                type: 'bootbox'
                            });
                        });
                    });

                    $(document).on('keydown', function (__e) {
                        // Combinaison de la touche ctrl + shit + q
                        const original_event = __e.originalEvent;
                        if (original_event.ctrlKey && original_event.shiftKey && original_event.code === 'KeyA') {
                            $('#corner_dev<?= $blockKey ?>').fadeToggle(_animate_duration);
                        }
                    });

                    $('.controlls<?= "$blockKey " ?> [data-target=close], .image-container<?= $blockKey ?>').on('click', function (__e) {
                        if (this === __e.target) $('#corner-screenshot-preview<?= $blockKey ?>').fadeOut(_animate_duration);
                    });

                    context_container_dom.on('click', '.action-tag', function (__e) {
                        const input = $('.corner-filter-input');
                        const value = input.val();

                        if (value.match(/^:[a-zA-Z0-9]*[a-zA-Z0-9\-\s]*[a-zA-Z0-9](,(\s)?:[a-zA-Z0-9]*[a-zA-Z0-9\-\s]*[a-zA-Z0-9])*$/)) input.val(value + ',' + $(this).data('content'));
                        else input.val($(this).data('content')).trigger('input');
                    });

                    context_container_dom.on('click', '.status-filter .btn', function () {
                        const self = $(this);
                        const target = self.data('target');
                        const action_target_dom = $(`[data-status=${target}]`);

                        if (self.hasClass(`btn-primary`)) {
                            self.removeClass(`btn-primary`).addClass('btn-default');
                            action_target_dom.each(function (__index) {
                                if (__index === action_target_dom.length - 1) $(this).slideUp({
                                    duration: 800,
                                    complete: adjust_view
                                });
                                else $(this).slideUp(800);
                            });
                        } else {
                            self.removeClass('btn-default').addClass(`btn-primary`);
                            action_target_dom.each(function (__index) {
                                if (__index === action_target_dom.length - 1) $(this).slideDown({
                                    duration: 800,
                                    complete: adjust_view
                                });
                                else $(this).slideDown(800);
                            });
                        }
                    });

                    context_container_dom.on('change', '[name=cd_visibility]', function () {
                        set_storage({
                            cs_visibility: this.value
                        }).then(save_costum_setting);
                    });

                    // tracking des actions
                    context_container_dom.on('click', '.action-tracking', function (__e) {
                        const id = $(this).data('id');
                        const tracked = $(this).hasClass('active');
                        if (tracked) {
                            $(this).removeClass('active').find('.fa').removeClass('fa-star').addClass('fa-star-o');
                        } else {
                            $(this).addClass('active').find('.fa').addClass('fa-star').removeClass('fa-star-o');
                        }

                        track_action(!tracked, id);
                    });

                    corner_header_dom.on('click', '[data-target=setting]', function () {
                        $('[data-target=costum_setting]').trigger('click');
                        $(this).fadeOut(_animate_duration);
                    });

                    let adjust_view_on_resize_timeout = null;
                    W.addEventListener('resize', function () {
                        if (adjust_view_on_resize_timeout !== null) clearTimeout(adjust_view_on_resize_timeout);
                        adjust_view_on_resize_timeout = setTimeout(adjust_view, 100);
                    });

                    // Evenements bootstraps
                    const bootstrap_events = [
                        {
                            'hidden.bs.collapse': '#action-list-types<?= $blockKey ?>'
                        },
                        {'shown.bs.collapse': '#action-list-types<?= $blockKey ?>'}
                    ];
                    $.each(bootstrap_events, function (__, __element) {
                        const event = Object.keys(__element)[0];
                        $.each([corner_body_dom, context_container_dom], function (__, __dom) {
                            __dom.on(event, __element[event], function () {
                                adjust_view();
                            });
                        });
                    });

                    // Détecter le changement d'URL
                    setInterval(function () {
                        if (_current_location !== W.location.href) {
                            _current_location = W.location.href;
                            if (turn_back_dom.is(':visible')) turn_back_dom.trigger('click');
                            else init_view().then(load_badgets);
                        }
                    });

                    init_view();
                });
            })(jQuery, window);
        </script>
        <?php
    } ?>