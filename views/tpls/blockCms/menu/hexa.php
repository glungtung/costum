<?php 
$keyTpl ="hexa";

$paramsData = [ 
    "maxLine"=>3,
    "textColor"=>"#ffffff",
    "filieres" => array(),
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if (isset($costum["app"])){
    foreach ($costum["app"] as $key => $value) {
        if(isset($value["nameMenuTop"])){
            array_push($paramsData["filieres"], ["title"=>$value["nameMenuTop"], "hash"=>$key, "icon"=>$value["img"], "image"=>$value["icon"]]);
        }
    }
}else{
    $paramsData["filieres"] = array(
        ["title"=>"Lorem", "hash"=>"lorem", "image"=>"economie.png"],
        ["title"=>"Ipsum", "hash"=>"ipsum", "image"=>"citoyennete.jpg"],
        ["title"=>"Dolor", "hash"=>"dolor", "image"=>"alimentation.png"]
    );
}

?>
 
<?php
    $hexa_width = 200+(100)*($paramsData["maxLine"]-3);
    $hexa_height = 200+(100)*($paramsData["maxLine"]-3);
    $xd = 10;
    $yd = 50;

    $row = 0;
    if((count($paramsData["filieres"]))%$paramsData["maxLine"]==0){
        $row = count($paramsData["filieres"])/$paramsData["maxLine"]+1;
    }else{
        $row = floor(count($paramsData["filieres"])/$paramsData["maxLine"])+1;
    }
?>

<!-- ****************get image uploaded************** -->
<?php 
    $blockKey = (string)$blockCms["_id"];
    
    $initFiles = Document::getListDocumentsWhere(
        array(
            "id"=> $blockKey,
            "type"=>'cms',
        ), 
        "image"
    );

    $arrayImg = [];

    foreach ($initFiles as $key => $value) {
        $arrayImg[]= $value["imagePath"];
    }

?>
<!-- ****************end get image uploaded************** -->
<style>
    .hex-<?= $kunik?>{
        opacity: 50%;
    }

    .hexa-<?= $kunik?>{
        width: 100%;
        margin-top:-5%;
    }

    

    text{
        font-family: arial !important;
        text-transform: uppercase;
        font-weight: bold;
        word-wrap: break-word !important;
    }

    .text{
        text-align: center;
        margin-top: 20px;
    }

    #dessus, .hex-<?= $kunik?>{
        stroke: #ffffff;
        stroke-width:9;
    }
</style>

<div style="margin-top: -8%;" class="hexa-<?= $kunik?> sp-cms-container col-xs-12">
    <div class="row">
        <div class="<?= "col-xs-12 col-sm-12 col-md-".(4-($paramsData["maxLine"]-3)) ?>"></div>
        <div class="<?= "col-xs-12 col-sm-12 col-md-".(4+2*($paramsData["maxLine"]-3)) ?>">
            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 <?php echo $hexa_width*$paramsData["maxLine"] + $xd*2 ?> <?php echo (3*$row+2)*$hexa_height/4 ?>" style="enable-background:new 0 0 <?php echo $hexa_width*$paramsData["maxLine"] ?> <?php echo (3*$row+2)*$hexa_height/4 ?>;" xml:space="preserve">
                <?php 
                $y = 0;
                $m = 0;
                $k = $paramsData["maxLine"];
                foreach ($paramsData["filieres"] as $key => $value) {
                    if($k+$m==$key){
                        $y++;
                        $k=$key;
                        if($m==($paramsData["maxLine"]-1)){
                            $m = $paramsData["maxLine"];
                        }else{
                            $m = $paramsData["maxLine"]-1;
                        }
                    }
                    
                    if(($y)%2==0){
                        $x = $key%($paramsData["maxLine"]);
                    }else{
                        $x = $key%($m)+(1/2);
                    }
                    
                    ?>
                    <g>
                        <a href="<?php echo $value["hash"] ?>" class="lbh" title="<?php echo $value["title"] ?>">
                            <?php echo Yii::t("home","") ?>
                            <defs>
                                <pattern id="img<?php echo $key+1 ?>" height="<?=$hexa_height?>" width="200%" patternContentUnits="objectBoundingBox">
                                    <image  xlink:href="<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/".$value["image"]; ?>" x="-0.5" height="1" width="2" preserveAspectRatio="none"/>
                                </pattern>
                            </defs>
                            <polygon id="dessus" points="<?php echo (($x*$hexa_width)+($hexa_width + $xd)).','.(($y*(3*$hexa_height/4))+3*$hexa_height/4+$yd).' '.(($x*$hexa_width)+($hexa_width + $xd)).','.(($y*(3*$hexa_height/4))+$hexa_height/4+$yd).' '.(($x*$hexa_width)+($xd+$hexa_width/2)).','.(($y*(3*$hexa_height/4))+$yd).' '.(($x*$hexa_width)+$xd).','.(($y*(3*$hexa_height/4))+$hexa_height/4+$yd).' '.(($x*$hexa_width)+($xd)).','.(($y*(3*$hexa_height/4))+3*$hexa_height/4+$yd).' '.(($x*$hexa_width)+($xd+$hexa_width/2)).','.(($y*(3*$hexa_height/4))+$hexa_height+$yd).' '; ?>" />
                            <polygon id="<?php echo $key+1 ?>" class="hex-<?= $kunik?>" points="<?php echo (($x*$hexa_width)+($hexa_width + $xd)).','.(($y*(3*$hexa_height/4))+3*$hexa_height/4+$yd).' '.(($x*$hexa_width)+($hexa_width + $xd)).','.(($y*(3*$hexa_height/4))+$hexa_height/4+$yd).' '.(($x*$hexa_width)+($xd+$hexa_width/2)).','.(($y*(3*$hexa_height/4))+$yd).' '.(($x*$hexa_width)+$xd).','.(($y*(3*$hexa_height/4))+$hexa_height/4+$yd).' '.(($x*$hexa_width)+($xd)).','.(($y*(3*$hexa_height/4))+3*$hexa_height/4+$yd).' '.(($x*$hexa_width)+($xd+$hexa_width/2)).','.(($y*(3*$hexa_height/4))+$hexa_height+$yd).' '; ?>" fill="url(#img<?php echo $key+1 ?>)"/>
                            <text class="text<?php echo $key+1 ?>" fill="<?=$paramsData["textColor"]?>" font-size="<?=20+10*($paramsData["maxLine"]-3)?>" x="<?php echo (($x*$hexa_width)+($hexa_width/2)) ?>" y="<?php echo (($y*(3*$hexa_height/4))+$yd+$hexa_height/2) ?>" dominant-baseline="middle" text-anchor="middle">
                                <tspan><?php echo $value["title"] ?></tspan>
                            </text>
                        </a>
                    </g>
                <?php } ?>
            </svg>
        </div>
        <div class="<?= "col-xs-12 col-sm-12 col-md-".(4-($paramsData["maxLine"]-3)) ?>"></div>
    </div>
</div>

<script type="text/javascript">
    $("polygon").each(function(){
        //$(".text"+$(this).attr('id')).css("filter", "invert(100%)");
        // $(".text"+$(this).attr('id')).css("visibility", "hidden");
    });
</script>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                "maxLine" : {
                    "inputType" : "select",
                    "label" : "Nombre max d'hexagone d'une ligne",
                    "options":{
                        "3":"3",
                        "4":"4",
                        "5":"5",
                        "6":"6"
                    },
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.maxLine
                },
                "textColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du texte",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
                },
                "pages" : {
                    "inputType" : "none",
                    "label":"Pour ajouter un bouton hexagone, veuillez ajouter une page statique dans ''app''",
                    "type":"file",
                    "class":"col-xs-12 no-padding hidden"
                    //values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.pages
                }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent")
                        tplCtx.value[k] = formData.parent;

                    if(k == "items")
                        tplCtx.value[k] = data.items;

                    if(k=="filieres"){ 
                       // pages = getPairs('.'+k+val.inputType);
                        
                        //console.log(pages,"fuckkkk");

                        $.each(data.filieres,function(inc,va){
                            nameKeyPage=(typeof va.key != "undefined" && notEmpty(va.key)) ? va.key : "#"+va.name.replace(/[^\w]/gi, '').toLowerCase();
                            tplCtx.value[va.hash] = {
                                key:va.key,
                                title: va.title,
                                hash : va.hash,
                                image : va.image
                            }
                        });
                       // mylog.log("------------ array", pages);
                  
                    }

                    mylog.log("andrana",data.items)
                });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
