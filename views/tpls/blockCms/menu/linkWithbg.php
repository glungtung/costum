<?php 
$keyTpl = "linkWithbg";
$paramsData = [
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
 if(isset($costum["contextType"]) && isset($costum["contextId"])){
  $orga = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
 }
?>
<style type="text/css">
  .mainCocity_<?= $kunik?>{
    padding: 3%;
    background-size: 100% 100%;    
    background-position: bottom;
    background-repeat: no-repeat;    
    padding-bottom: 2%;
  }

  .bloc-img_<?= $kunik?>{
    padding-top: 10%;
    padding-bottom: 10%;  
    height: 300px;
    width: 40%;
    background-size: cover;
    font-size: 19px;
    box-shadow: 0px 0px 6px silver;
    
  }


  .bloc-img_<?= $kunik?> a{
    position: absolute;
    top: 35%;
    left: 0;
    right: 0;
    text-align: center;
    z-index: 1;
    font-size: 20px;
  }
  .mainCocity_<?= $kunik?> .menu1:after, .mainCocity_<?= $kunik?> .menu2:after {
    content: " ";
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0,0,0,.7);
  }

  .icon<?= $kunik?> {
    height: 150px;
    width: 150px;
    border-radius: 50%;
    padding: 24%;
    background-color: white;
    box-shadow: 1px 1px 2px 2px #e7f3f7;
  }

  .col<?= $kunik?> .col{
    padding-right: 25%;
    padding-left: 30%;
    margin-bottom: 7%;
  }
  .col<?= $kunik?> .col i{
    font-size: 60px;
    color: #63aabc;
  } 
  .icone<?= $kunik?> {
    font-size: 20px;
    color: #63aabc;
  }
  .mainCocity_<?= $kunik?> a{
    cursor: pointer;
  }
  
  @media (max-width: 978px) {
    .col<?= $kunik?> .icon<?= $kunik?> i{
      font-size: 30px !important;
    }
    .col<?= $kunik?> .icone<?= $kunik?>{
      font-size: 18px !important;
    }
    .mainCocity_<?= $kunik?> .col<?= $kunik?> h4{
      font-size: 20px !important;
    }
    .mainCocity_<?= $kunik?> .col<?= $kunik?> p{
      font-size: 16px !important;
      margin-top: 5%;
      margin-left: 5%;
      margin-right: 5%;
    }
    .bloc-img_<?= $kunik?>{
      margin-top: 3%;
      padding-top: 10%;
      height: 200px;
      padding-bottom: 10%;
      width: 100%;
      margin-left: 0;
      background-size: cover;
      font-size: 17px;
    }
    .icon<?= $kunik?> {
      padding: 20%;
      height: 80px;
      width: 80px
    }
    .col<?= $kunik?> .col{
      padding-left: 37%;
    }
    .col<?= $kunik?> {
      margin-bottom: 2%;
    }
  }
</style>
<div class="mainCocity_<?= $kunik?> row">
  <div class="col-xs-12 col-md-12 text-center  ">
    <div class=" col-md-3 hidden-xs" >
    </div>
    <div class=" col-md-3  col-xs-12 text-center col<?= $kunik?>" >  
      <div class="col">
        <div class="icon<?= $kunik?>">
          <i class="fa fa-user-circle-o"></i>
        </div>
      </div>  
      <a  class="  profil<?= $kunik?>"  data-toggle="dropdown">
        <h4 class="img-text-bloc description">
          Accéder à mon profil
        </h4>
      </a>
      <p class="title-6">Gérer mes notifications,<br> recherches, actions, ...</p>
     <a  class=" icone<?= $kunik?> profil<?= $kunik?>"  data-toggle="dropdown"><i class="fa fa-plus-circle"></i></a>
       
    </div>
    <div class=" col-md-3  col-xs-12 text-center col<?= $kunik?>" >  
      <div class="col">
        <div class="icon<?= $kunik?>">
          <i class="fa fa-heart-o"></i>
        </div>
      </div>  
      <a  href="#element.invite.type.organizations.id.<?= $costum["contextId"]?>" onClick="invite()" class="lbh invite<?= $kunik?>" data-placement="bottom" data-original-title="Inviter des personnes ">
        <h4 class="img-text-bloc description">
          Inviter des amis
        </h4>
      </a>
      <p class="title-6">Je souhaites inviter des amis à,<br> rejoindre la plateforme</p>
    
      <a href="#element.invite.type.organizations.id.<?= $costum["contextId"]?>" onClick="invite()" class="lbh icone<?= $kunik?>" data-placement="bottom" data-original-title="Inviter des personnes "><i class="fa fa-plus-circle"></i></a>
     
    </div>
    <div class=" col-md-3 hidden-xs" >
    </div>
  </div>
</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  
  
  function invite(){
    if(!userId){
      toastr.error("Vous devez être connecté");
      $(".close-modal").hide();      
			  $('#modalLogin').modal("show");
      }
    }
  jQuery(document).ready(function() {
    $('.profil<?= $kunik?>').on("click",function(){
      if(!userId){
        toastr.error("Vous devez être connecté");
        $(".close-modal").hide();
        console.log("Login.runLoginValidator",Login.runLoginValidator);
        $('#modalLogin').modal("show");

      }else{
        document.location.href =  "#page.type.citoyens.id.<?= @Yii::app()->session['userId'] ?>";
        urlCtrl.loadByHash(location.hash);
      }
    });  
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section1",
        "icon" : "fa-cog",
        "properties" : {
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                dyFObj.closeForm();
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };
    mylog.log("paramsData",sectionDyf);
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
  })
</script>