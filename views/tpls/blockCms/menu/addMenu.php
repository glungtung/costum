<?php 
$keyTpl = "addMenu";
$paramsData = [
  "filiere" =>[],
  "color" =>"#000000"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 
?>
<style type="text/css">
  .menuThemeCocity_<?= $kunik?>{
    padding: 2%;
    text-transform: uppercase;
  }
  .menuThemeCocity_<?= $kunik?> a{
    color: <?= $paramsData["color"]?>;
    text-align: center;
    padding: 5px;
    height: 15%;
  }
  .menuThemeCocity_<?= $kunik?> img{

    font-size: 50px;
    height: 40px;
    width: 40px;
  } 
  .menuThemeCocity_<?= $kunik?> .filiere{
    padding: 0px;
    margin-left: 3%;
    color:#005E6F;
  }
</style>
<div class="menuThemeCocity_<?= $kunik?> ">
  <div class="text-center container" style="display: flex;">
    <?php 
    if (isset($blockCms["filiere"]) && !empty($blockCms["filiere"])) {
      foreach ($blockCms["filiere"] as $key => $value) {
        //var_dump($value);
        $icone = isset($value["icone"])?$value["icone"]:"fa-heart";
        ?> 
        <div class="filiere">
          <a href='<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/filierenumerique' target="_blank"> 
            <i class="fa <?= $icone ?>"> </i>
            <span>
              <?php echo $value["name"] ?>              
            </span>
          </a>
        </div> 
        <?php 
      }
    } 
    ?>
  </div>
</div> 

<div class="text-center ">
  <button class="btn btn-primary addElement<?= $blockCms['_id'] ?>">Ajouter une filière</button>
</div>     

<script type="text/javascript">
  
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section1",
        "icon" : "fa-cog",
        "properties" : {
          "color":{
            label : "Couleur du text",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.color
          }
        },
        beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }
        }
      }

    };
    mylog.log("paramsData",sectionDyf);
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
    $(".addElement<?= $blockCms['_id'] ?>").click(function() {  
      var tplCtx = {};
      var activeForm = {
        "jsonSchema" : {
          "title" : "Ajouter nouveau bloc CMS",
          "type" : "object",
          onLoads : {
            onload : function(data){
              $(".parentfinder").css("display","none");
            }
          },
          "properties" : {
           icone : {
            label : "icone",
            "inputType" : "text",
            values : ""
          },
          name : {
            label : "Nom de la filière",
            "inputType" : "text",
            values : ""
          }
        }
      }
    };          

    activeForm.jsonSchema.save = function () {
      tplCtx.id = "<?= $blockCms['_id'] ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "filiere."+$("#name").val();
      tplCtx.value = {};
      tplCtx.value = {
        icone : $("#icone").val(),
        name : $("#name").val()
      };
      mylog.log("ediit addElement", tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          $("#ajax-modal").modal('hide');

          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
          // urlCtrl.loadByHash(location.hash);
        } );
      } 
    }
    dyFObj.openForm( activeForm );
  });


  });
</script>