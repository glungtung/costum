<?php
$keyTpl = "menuFilters";

$paramsData=[
    "titleBlock" => "AFFICHE",
    "cardBackground" => "White",
    "imgRadius" => "10"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>

<!-- ****************get image uploaded************** -->
<?php
$blockKey = (string)$blockCms["_id"];
$initFiles = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"notBackground"
    ), "image"
);
$arrayImg = [];
foreach ($initFiles as $key => $value) {
    $arrayImg[]= $value["imagePath"];
}
?>
<!-- ****************end get image uploaded************** -->

<style type="text/css">

    /*Card*/
    .carousel-card-gallery<?=$kunik?> {
        margin-top: 50px;
        width: 90%;
        margin: 50px auto;
        max-width: 1100px;
    }

    .active-filters<?=$kunik?>{
      position: absolute;
      bottom: 10px;
      left: 20px;
      border: 3px solid #49CBE3;
      border-radius: 50%;
      padding: 1px;
      font-size: 14px;
    }

    .counter<?=$kunik?>{
      font-size: 16px;
    }

    .carousel-card<?=$kunik?> {
        font-family: 'NotoSans-Regular' !important;
        font-weight: bolder !important;
        font-size: 25px;
        position: relative;
        display: block;
        width: 100%;
        height: 90px;
        margin: 1rem auto 2rem 1rem;
        border-radius: <?php echo $paramsData["imgRadius"]; ?>px;
        box-shadow: 0px 0px 10px #1a2660;
        overflow: hidden;
        transition: all .4s ease;
        background-color: <?php echo $paramsData["cardBackground"] ?>;
    }

    @media screen and (min-width: 768px) {
        .carousel-card<?=$kunik?> {
            height: 90px;
        }
    }

    @media screen and (min-width: 768px) {
        .carousel-card<?=$kunik?> {
            display: inline-block;
        }
    }

    .carousel-card-overlay<?=$kunik?> {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        z-index: 0;
        text-align: center;
        overflow-y: auto;
        overflow-x: hidden;
        background-color: <?php echo $paramsData["cardBackground"] ?>;
    }
    .carousel-card-overlay<?=$kunik?>:after {
        content: '';
        width: 100%;
        height: 100%;
        background-color: transparent;
        position: absolute;
        top: 0;
        z-index: -10;
        left: 0;
        transition: all .3s ease;
    }

    .carousel-card-title<?=$kunik?> {
        margin-bottom: 5px;
        margin-top: 2px;
        font-size: 16px;
        font-weight: bolder;
        color: #1a2660;
        text-align: center;
        letter-spacing: 2px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        padding-left: 5px;
        padding-right: 5px;
    }

    .carousel-card-logo<?=$kunik?> {
        max-height: 48px !important;
        position: relative;
        margin-top: 30px;
        border-radius: 10px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
    }

    .carousel-card-icon<?=$kunik?> {
        max-height: 48px !important;
        position: relative;
        margin-top: 30px;
        border-radius: 10px;
        font-size: 42pt;
        color: #1a2660;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
    }

    .text-blue<?=$kunik?>{
        color: #1a2660;
        font-size: 15pt;
        font-weight: bolder;
        background-color: transparent !important;
    }

    .swiper-pagination-bullet {
        margin-top: 2rem;
        background-color: transparent;
        display: inline-block;
        width: 2rem;
        height: 2rem;
        border: 3px solid white;
        opacity: .9;
    }

    .swiper-pagination-bullet-active {
      border: 3px solid #1a2660;
      opacity: 1;
    }

    .d-inline{
        display: inline;
    }
</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 container<?php echo $kunik ?> ">
<?php if($paramsData["titleBlock"]!=""){ ?>
    <h3 class="sp-text d-inline img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock"><?php echo $paramsData["titleBlock"]; ?></h3>
<?php } ?>
    <button class="btn btn-primary btn-refresh-filter<?= $kunik ?> d-inline margin-left-20">Reinitialiser le filtre</button>
    <div class="swiper-container">
        <div class="swiper-wrapper"></div>
        <br><br>
        <div class="swiper-pagination"></div>
        <!--div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div-->
    </div>
</div>

<script type="text/javascript">
    var defaultFilters = {'$or':{}};
    defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["source.keys"] = costum.slug;
    defaultFilters['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
</script>

<script type="text/javascript">
    var dyf<?=$kunik?>={
        "onload" : {
            "actions" : {
                "setTitle" : "Ajouter une <?=$kunik?>",
                "html" : {
                    "infocustom" : "<br/>Remplir le formulaire"
                },
                "presetValue" : {
                    "type" : "<?=$kunik?>"
                },
                "hide" : {
                    "breadcrumbcustom" : 1,
                    "parentfinder" : 1,
                }
            }
        }
    }

    dyf<?=$kunik?>.afterSave = function(data){
        dyFObj.commonAfterSave(data,function(){
            mylog.log("data", data);
            urlCtrl.loadByHash(location.hash);
        });
    }

    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    /********** list *************/
    getTagsFilters<?= $kunik ?>(()=>{
            if($(".badge-theme-count").length!=0){
                /*alert("is this OK ? all : "+$(".badge-theme-count").length);
                $(".badge-theme-count").each(function(){
                    alert($(this).data("countlock"));
                    console.log("themes is", searchObj.filters.actions.themes);
                })*/
                if(searchObj && searchObj.search.obj.types.length==0){
                    searchObj.search.obj.types = ["organizations"];
                }
                //searchObj.search.init(searchObj);
                searchObj.filters.actions.themes.setThemesCounts(searchObj);
            }
    });

    function getTagsFilters<?= $kunik ?>(callBack){
        var html = "";
        $(".btn-refresh-filter<?= $kunik ?>").hide()
        $.each(costum.lists.family, function(index, value) {
            html += '<div class="swiper-slide" >' +
                        '<a href="javascript:;" class="carousel-card<?=$kunik?> filterTag"  data-value="'+value+'" data-field="tags" data-type="filters">'+
                        '<div class="carousel-card-overlay<?=$kunik?> text-center co-scroll">'+
                        '<span class="badge-theme-count counter<?=$kunik?> text-blue<?=$kunik?>" data-countkey="'+value+'" data-countvalue="'+value+'" data-countlock="false">0</span>';
            if (typeof(costum.lists)!="undefined" && typeof(costum.lists.familyImg)!="undefined" && !costum.lists.familyImg[value].includes("/")){
                html +='<i class="carousel-card-icon<?=$kunik?> fa fa-'+costum.lists.familyImg[value]+'"></i>';
            }else if(typeof(costum.lists)!="undefined" && typeof(costum.lists.familyImg)!="undefined"){
                html +='<img class="carousel-card-logo<?=$kunik?>" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>'+costum.lists.familyImg[value]+'"> ';
            }

            html +='<div class="carousel-card-title<?=$kunik?>">'+value+'</div>';
            html +='<div class="col-xl-12">';
            html +='</div>' +
                   '</div>';

            html +='</a>' +
                '</div>';
        });

        $('.container<?php echo $kunik ?> .swiper-wrapper').empty();
        $('.container<?php echo $kunik ?> .swiper-wrapper').append(html);

        var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            /*navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },*/
            keyboard: {
                enabled: true,
            },
            breakpoints: {
                400: {
                    slidesPerView: 2,
                    spaceBetween: 10,
                },
                560: {
                    slidesPerView: 3,
                    spaceBetween: 10,
                },
                900: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                },
                1024: {
                    slidesPerView: 6,
                    spaceBetween: 15,
                },
                1200: {
                    slidesPerView: 6,
                    spaceBetween: 15,
                },
                1400: {
                    slidesPerView: 9,
                    spaceBetween: 15,
                }
            }
        });

        coInterface.bindLBHLinks();

        if(typeof callBack == "function"){
            callBack();
        }
    }

    function refreshAllGraph<?= $kunik ?>(){
        for (var variable in window){
            if (window[variable]!=null && typeof window[variable].pInit !="undefined" && exists(window[variable].pInit)){
                try {
                    window[variable].search.obj.filters = {
                        ...window[variable].search.obj.filters,
                        ...searchObj.search.obj.filters
                    }
                    if(Object.keys(searchObj.search.obj.filters).length==0){
                        window[variable].search.obj.filters=defaultFilters
                    }
                    coInterface.showLoader(window[variable].results.dom, trad.currentlyresearching);

                    if(window[variable].search.loadEvent.active!="graph" && window[variable].graph.circlerelations){
                        window[variable].search.init(window[variable]);
                    }
                }catch(e){}
            }
        }
        setTimeout(() => {
            $.each(window.initedGraph, function(i, graph){
                console.log("", window["l"+graph]);
                window["initGraph"+graph]();
                $('html, body').animate(
                    { scrollTop: $(".sortable-"+graph).offset().top - $("#mainNav").height(), queue:false },
                    500
                )
            })
        },500);

        if(Object.keys(searchObj.search.obj.filters).length>1){
            $(".btn-refresh-filter<?= $kunik ?>").show();
        }else{
            $(".btn-refresh-filter<?= $kunik ?>").hide();
        }
    }

    jQuery(document).ready(function() {

        $.each(costum.lists.family, function(index, value){
            if(searchObj.search.obj.filters[value]){
                $("[data-value='"+value+"'] .carousel-card-overlay<?=$kunik?>").append('<i class="fa fa-check active-filters<?=$kunik?>"></i>');
                $(".btn-refresh-filter<?= $kunik ?>").show();
            }
        })

        $(".filterTag").click(function(value) {
            let originalValue = $(this).data("value");
            let thisValue = originalValue.split("&");
            thisValue = thisValue.map(v=>v.trim());

            if(thisValue.length>1){
                thisValue.push(originalValue);
            }

            if(typeof searchObj.search.obj.filters[$(this).data("field")]!="undefined" && searchObj.search.obj.filters[$(this).data("field")].includes(...thisValue)){
                let pseudoElement = $(this);
                $.each(thisValue, (i, v)=>{
                    pseudoElement.data("value", v);
                    searchObj.filters.manage.removeActive(searchObj, pseudoElement, true);
                })
                $("[data-value='"+originalValue+"'] .carousel-card-overlay<?=$kunik?> .active-filters<?=$kunik?>").remove()
            }else{
                if(typeof $(this).data("field") != "undefined"){
  					if(typeof searchObj.search.obj.filters == "undefined" )
                    {
                        searchObj.search.obj.filters = {};
                    }
  					if(typeof searchObj.search.obj.filters[$(this).data("field")] == "undefined" )
                    {
  						searchObj.search.obj.filters[$(this).data("field")] = [];
                    }
                    searchObj.search.obj.filters[$(this).data("field")].push(...thisValue);
                    //searchObj.search.obj.tags.push(...thisValue);
                }

                $("[data-value='"+originalValue+"'] .carousel-card-overlay<?=$kunik?>").append('<i class="fa fa-check active-filters<?=$kunik?>"></i>')

                searchObj.filters.manage.addActive(searchObj, $(this), true);
            }

            refreshAllGraph<?= $kunik ?>();
        });

        $(".btn-refresh-filter<?= $kunik ?>").click(function(){
            searchObj.search.obj.filters = {};
            $(this).hide();
            $(".active-filters<?=$kunik?>").remove();
            refreshAllGraph<?= $kunik ?>();
        })
    });

</script>
