<?php 
$keyTpl ="menuInHexa";

$paramsData = [ 
    "maxLine"=>4,
    "textColor"=>"#ffffff",
    "filieres" => array(),
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style>
    .hex<?= $kunik?> img {
        width: 50px;
        height: 50px;
    }

    .hex<?= $kunik?>agon-gallery {
        margin: auto;
        margin-top: 50px;
        max-width: 1000px;
        display: grid;
        grid-template-columns: repeat(8, 1fr);
        grid-auto-rows: 200px;
        grid-gap: 14px;
        padding-bottom: 50px;
    }
    .hex<?= $kunik?> i{ 
        font-size : 30px
    }
    .hex<?= $kunik?> p{ 
        font-size : 25px
    }

    .hex<?= $kunik?> { 
        position: relative;
        text-align: center;
        width: 240px;
        color: white;
        background-size: cover;
        padding-top :40%;
        height: 265px;
        background-color: #424242;
        -webkit-clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
    }

    .hex<?= $kunik?>:first-child {
        grid-row-start: 1;
        grid-column: 2 / span 2;
    }

    .hex<?= $kunik?>:nth-child(2) {
        grid-row-start: 1;
        grid-column: 4 / span 2;
    }

    .hex<?= $kunik?>:nth-child(3) {
        grid-row-start: 1;
        grid-column: 6 / span 2;
    }

    .hex<?= $kunik?>:nth-child(4) {
        grid-row-start: 2;
        grid-column: 1 / span 2;
    }

    .hex<?= $kunik?>:nth-child(5) {
        grid-row-start: 2;
        grid-column: 3 / span 2;
    }

    .hex<?= $kunik?>:nth-child(6) {
        grid-row-start: 2;
        grid-column: 5 / span 2;
    }

    .hex<?= $kunik?>:nth-child(7) {
        grid-row-start: 2;
        grid-column: 7 / span 2;
    }

    .hex<?= $kunik?>:nth-child(8) {
        grid-row-start: 3;
        grid-column: 2 / span 2;
    }

    .hex<?= $kunik?>:nth-child(9) {
        grid-row-start: 3;
        grid-column: 4 / span 2;
    }

    .hex<?= $kunik?>:nth-child(10) {
        grid-row-start: 3;
        grid-column: 6 / span 2;
    } 
    .hex<?= $kunik?> .content{
        display: none;
    }
    
    .hex<?= $kunik?>:hover .content{
        display: block;
	    opacity: 0.7;
        position: relative;
        text-align: center;
        width: 240px;
        color: white;
        height: 265px;
        padding-top :40%;
        background-color: black;
        -webkit-clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
    }
    .hex<?= $kunik?>:hover{
        padding-top: 0;
    }
</style>

<section class="hex<?= $kunik?>agon-gallery">
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/citoyennete/citoyennete.jpg"?>')
    ">
    <a href="#citoyennete" class="lbh">
        <div class="content ">
            <p>Citoyennete </p>
            <i class="fa fa fa-user-circle-o "></i>
        </div>
    </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/economie/cover_economie.jpg"?>')
    ">
    <a href="#economie" class="lbh">
        <div class="content ">
            <p>Economie</p>        
            <i class="fa fa fa-money "></i>
        </div>
    </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/transport_energie/moins_de_transport.jpg"?>')
    ">
        <a href="#transport" class="lbh">
            <div class="content ">
                <p> Transport</p>
                <i class="fa fa-bus"></i>
            </div>
        </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/transport_energie/cover_transport.jpg"?>')
    ">
        <a href="#energie" class="lbh">
            <div class="content ">
                <p>Energie</p> 
                <i class="fa fa-sun-o "></i>
            </div>  
        </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/dechets/action_perso.jpeg"?>')
    ">    
        <a href="#dechets" class="lbh">
            <div class="content ">
                <p>Déchets</p>
                <i class="fa fa-trash-o  "></i>
            </div> 
        </a> 
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/education/education.jpg"?>')
    " >

        <a href="#education" class="lbh">    
            <div class="content ">
                <p>Education</p>
                <i class="fa fa-book "></i>
            </div> 
        </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/commun/cover.jpg"?>')
    ">
        <a href="#commun" class="lbh">
            <div class="content ">
                <p>Commun</p>
                <i class="fa fa-circle-o "></i>
            </div> 
        </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/construction/cover_construction.jpg"?>')
    ">
        <a href="#construction" class="lbh">
            <div class="content ">
                <p>Construction</p>
                <i class="fa fa-building "></i>
            </div> 
        </a>        
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/sante/yoga.jpg"?>')
    ">
        <a href="#sante" class="lbh">
            <div class="content ">
                <p>Santé</p>
                <i class="fa fa-heart-o "></i>
            </div> 
        </a>
    </div>
    <div class="hex<?= $kunik?>" style="background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl."/images/smarterre/sante/alimentation.jpg"?>')
    ">
        <a href="#alimentation" class="lbh">
            <div class="content ">
                <p>Alimentation</p>
                <i class="fa fa-cutlery "></i>
            </div> 
        </a>
    </div>
</section>
<!-- <div>
    <a href="javascript:;" class="addApp<?= $kunik?>"> Ajouter menu</a>
</div> -->
<script>    
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent")
                        tplCtx.value[k] = formData.parent;

                    if(k == "items")
                        tplCtx.value[k] = data.items;

                    if(k=="filieres"){ 

                        $.each(data.filieres,function(inc,va){
                            nameKeyPage=(typeof va.key != "undefined" && notEmpty(va.key)) ? va.key : "#"+va.name.replace(/[^\w]/gi, '').toLowerCase();
                            tplCtx.value[va.hash] = {
                                key:va.key,
                                title: va.title,
                                hash : va.hash,
                                image : va.image
                            }
                        });
                  
                    }

                    mylog.log("andrana",data.items)
                });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        $('.addApp<?php echo $kunik ?>').click(function(){
            <?php echo $kunik ?>modalAddMenu();
         })
         function <?php echo $kunik ?>modalAddMenu(){
            bootbox.dialog({
                title: "<h5 class='text-success text-center'><?php echo Yii::t('cms', 'The existing menus')?></h5>"+
                       "<h6 class='text-danger text-bold text-center'>"+existApp+"</h6>",
                message: sectionDyf.<?php echo $kunik ?>addForm,
                buttons: {
                    cancle: {
                       label: "<?php echo Yii::t('common', 'Cancel')?>",
                       className: 'btn-danger',
                    },
                    ok: {
                        label: "<?php echo Yii::t('cms', 'Create new')?>",
                        className: 'bg-green-k',
                        callback: function(){
                            
                            var appName = $('#appName').val();
                            var subdomainName = appName;
                            appName = sectionDyf.<?php echo $kunik ?>toCamelCase(appName);
                            var appType = $('input[name=appType]:checked', '#appForm').val();
                            if(appName != '' && appType != ''){
                                if(exists(sectionDyf.<?php echo $kunik ?>ParamsData[""]))
                                    delete sectionDyf.<?php echo $kunik ?>ParamsData[""];
                                else if(exists(sectionDyf.<?php echo $kunik ?>ParamsData["home"]))
                                    delete sectionDyf.<?php echo $kunik ?>ParamsData["home"];
                                //alert("Le chargemement peux prendre quelque seconde . . .");
                                sectionDyf.<?php echo $kunik ?>ParamsData[appName] = {};
                                if(appType == "staticPage")
                                    sectionDyf.<?php echo $kunik ?>ParamsData[appName] = {
                                        isTemplate : true,
                                        staticPage : true,
                                        useFilter : false,
                                        hash: "#app.view",
                                        icon : "",
                                        urlExtra : "/page/"+appName+"/url/costum.views.tpls.staticPage",
                                        subdomainName : subdomainName
                                    };

                                sectionDyf.<?php echo $kunik ?>AddProps(appName,subdomainName,appType,function(){
                                        sectionDyf.<?php echo $kunik ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $kunik ?>ParamsData,appName);
                                        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
                                        sectionDyf.<?php echo $kunik ?>PushToClassArray();
                                        sectionDyf.<?php echo $kunik ?>WrapAppToDiv(sectionDyf.<?php echo $kunik ?>ParamsData);
                                        $('body').append(`<style>.<?php echo $kunik ?>.${appName}-app:before {content : "${subdomainName.replace('"',"'")}"}</style>`);
                                        <?php echo $kunik ?>addButtonAddOnBottom();

                                });
                            }
                        }
                    }
                }
            });
            <?php echo $kunik ?>eventInBootbox();
        }

        sectionDyf.<?php echo $kunik ?>toCamelCase = function(str) {
            return str.normalize("NFD").replace(/([\u0300-\u036f])/g, "").replace(/["']/g, "").replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
                if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
                //return index == 0 ? match.toLowerCase() : match.toUpperCase();
                return index = match.toLowerCase() ;
            });
        }
        sectionDyf.<?php echo $kunik ?>CreateArrayForClassAndAddNewProps = function(paramsData,appName=null){
            mylog.log("lionel",paramsData);
            $.each(paramsData,function(k,v){
                var appType = null;
                sectionDyf[k+"<?php echo $kunik ?>"]=[];
                //alert(appName+" : "+k+" : "+JSON.stringify(v));
                if(appName != k){
                    if(typeof v["staticPage"] != "undefined"){
                        appType = "staticPage";
                        sectionDyf.<?php echo $kunik ?>AddProps(k,"",appType);
                    }else
                        sectionDyf.<?php echo $kunik ?>AddProps(k);
                    
                }
            });
        }
    });
</script>