<?php
$keyTpl = "docBasic";

$paramsData = 	[ "color" => "black" ];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

foreach ($blockCms["parentTree"] as $key => $value) {
	for ($i=0; $i < $value[$i] ; $i++) { 
		$test = $value[$i];
	}
}
?>

<style type="text/css">
	#container-docs h1{
		color : <?= $paramsData["color"] ?>;
	}
</style>

<div id="container-docs" class="col-xs-12 padding-20 margin-top-20"></div>

<script type="text/javascript">
paramsData = <?php echo json_encode( $paramsData ); ?>;
var dynFormCostumDoc = {
	"beforeBuild":{
	    "removeProp" : {
	    	"image": 1,
        },
        "properties" : {
            "structags" : {
                "inputType" : "tags",
                "placeholder" : "<?php echo Yii::t('cms', 'Structuring the content')?>",
                "values" : null,
                "label" : "<?php echo Yii::t('cms', 'Structure and Hierarchy (parent or parent.child)')?>"
            },
            "documentation" : {
                "inputType" : "uploader",
                "label" : "<?php echo Yii::t('cms', 'Associated document (5Mb max)')?>",
                "showUploadBtn" : false,
                "docType" : "file",
                "itemLimit" : 15,
                "contentKey" : "file",
                "order" : 8,
                "domElement" : "documentationFile",
                "placeholder" : "PDF",
                "afterUploadComplete" : null,
                "template" : "qq-template-manual-trigger",
                "filetypes" : [
           			"pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif"
                ]
            }
        }
    },
	"onload" : {
        "actions" : {
            "setTitle" : "<?php echo Yii::t('cms', 'Documentation')?>",
            "html" : {
                "nametext>label" : "<?php echo Yii::t('cms', 'Title of the documentation')?>",
                "infocustom" : "<br/><?php echo Yii::t('cms', 'Create sections and sub-chapters')?>"
            },
            "presetValue" : {
                "type" : "doc"
            },
            "hide" : {
            	"parentfinder":1,
	            "locationlocation" : 1,
	            "formLocalityformLocality" : 1,
                "breadcrumbcustom" : 1,
                "urlsarray" : 1,
                "imageuploader":1
            }
    	}
	}
};

var dynFormCostumTplDoc = {
	"jsonSchema" : {
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
            "color" : {
                label : "<?php echo Yii::t('cms', 'Title color')?>",
                inputType : "colorpicker",
                values :  paramsData.color
            }
        },
        save : function () {  
            tplCtx.value = {};

            $.each( dynFormCostumTplDoc.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
             });
            
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#ajax-modal").modal('hide');
                    toastr.success("<?php echo Yii::t('cms', 'updated item')?>");
					var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                } );
            }

        }
    }
}

function bindDocLinksCms() { 
	$(".link-docs-menu-cms").off().on("click",function(){
		openDoc($(this).data("cms"),parseInt($(this).data("pos")));
	});

	$(".link-docs-menu-cms").off().on("click",function(){
		openDoc($(this).data("cms"),parseInt($(this).data("pos")));
	});
}

jQuery(document).ready(function() {
	mylog.log("render","costum.views."+contextSlug+".doc");
	
	if ( location.hash.indexOf("docs.cms") >= 0 ) {
		paramT = location.hash.split(".");
		documentation.getBuildPoiDoc(paramT[2]);
	}else 	
		setTimeout(function() { navInDocs("kicker", "costum.views.custom."+contextSlug+".docs"); }, 500)
	
	$("#close-docs").attr("href",urlBackHistory);

	bindDocLinksCms();

	$("#show-menu-xs").off().on("click",function (){
		$('#container-docs').removeClass('col-xs-9').removeClass('col-xs-12')
		$('#menu-left').addClass("hide");
		navInDocs("kicker", "costum.views.custom."+contextSlug+".docs");
	});

	$(".createDocBtn").off().on("click",function (){
		mylog.log("createBtn");
		dataObj = {parent:{}};
		dataObj.parent[contextId] = {collection : contextType, name : '<?php echo $costum["title"] ?>' } ;
		dyFObj.openForm('cms',null,dataObj,null,dynFormCostumDoc);
	});
});

function openDoc(cmsId,pos){
	
	var url = baseUrl+'/co2/element/get/type/cms/id/'+cmsId+"/edit/true";
	hashdocs="#docs.cms."+cmsId;
	currentDocPos=pos;
	
	ajaxPost(null ,url, null,function(data){
		descHtml = documentation.renderDocHTML(data);
			$('#container-docs').html(descHtml);

		$(".editThisTpl").off().on("click",function() { 
			tplCtx = {}; 
		    tplCtx.id = $(this).data("id");
		    tplCtx.collection = $(this).data("type");
		    tplCtx.path = "allToRoot";

		    dyFObj.openForm( dynFormCostumTplDoc,null, paramsData);
		});

		$(".editThisBtn").off().on("click",function (){
			mylog.log("editThisBtn"); 
			var id = $(this).data("id");
		    var type = $(this).data("type");
			dyFObj.editElement(type,id, null,dynFormCostumDoc);
		});

		$(".deleteThisBtn").off().on("click",function (){
	       	mylog.log("deleteThisBtn click");
	        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	        var btnClick = $(this);
	        var id = $(this).data("id");
	        var type = $(this).data("type");
	        var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
	          
        	bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                	ajaxPost(
				        null,
				      	urlToSend,
				        {},
				        function(data){ 
							if ( data && data.result ) {
	                          toastr.info("élément effacé");
	                          $("#"+type+id).remove();
	                          urlCtrl.loadByHash(location.hash)
	                        } else {
	                           toastr.error("something went wrong!! please try again.");
	                        }
				        }
				    ); 
                }
        	});
	    });
	},"html");
}

var docUrls = <?php echo json_encode($docUrls); ?>;
currentDocPos = -1;

function nextDoc () { 
	$(".link-docs-menu-cms").removeClass("active");
	if(currentDocPos < 0){
		$('#container-docs').addClass('col-xs-9').removeClass('col-xs-12')
		$('#menu-left').removeClass("hide");
	}
	currentDocPos++;
	if(currentDocPos > docUrls.length)
		currentDocPos = -1;
	openDoc(docUrls[currentDocPos],currentDocPos);
	$('.link-docs-menu-cms[data-pos="'+currentDocPos+'"]').addClass("active");
}

function prevDoc () { 
	$(".link-docs-menu-cms").removeClass("active");
	if(currentDocPos < 0){
		$('#container-docs').addClass('col-xs-9').removeClass('col-xs-12')
		$('#menu-left').removeClass("hide");
	}
	currentDocPos--;
	if( currentDocPos < 0 )
		currentDocPos = docUrls.length-1;
	openDoc(docUrls[currentDocPos],currentDocPos);
	$('.link-docs-menu-cms[data-pos="'+currentDocPos+'"]').addClass("active");
}
</script>