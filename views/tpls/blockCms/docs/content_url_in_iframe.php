<?php
$keyTpl     = "content_url_in_iframe";
$paramsData = [
    "iframeWidth"             => "100%",
    "iframeHeight"        => "750px",
    "iframeLink"       => "https://www.communecter.org/costum/co/index/slug/mouvoutremer#membres" 
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style>
    .content_<?= $kunik?> iframe{
        height: <?= $paramsData["iframeHeight"]?>;
        width: <?= $paramsData["iframeWidth"]?>;
    }
</style>
<div class="content_<?= $kunik?>">
    <iframe src="<?= $paramsData["iframeLink"]?>" frameBorder="0"></iframe>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "iframeWidth" : {
                        "label" : "<?php echo Yii::t('cms', 'Width')?>",
                        "inputType" : "text",
                        "values" :  sectionDyf.<?php echo $kunik ?>ParamsData.iframeWidth
                    },
                    "iframeHeight" : {
                        "label" : "<?php echo Yii::t('cms', 'Height')?>",
                        "inputType" : "text",
                        "values" :  sectionDyf.<?php echo $kunik ?>ParamsData.iframeHeight
                    },
                    "iframeLink" : {
                        "label" : "<?php echo Yii::t('cms', 'Link')?>",
                        "inputType" : "text",
                        "values" :  sectionDyf.<?php echo $kunik ?>ParamsData.iframeLink
                    }
                },

                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        mylog.log("sectiondyfff",sectionDyf);
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"iframe",6,6,null,null,"Propriété du contenu","green","");
        });
    }); 
</script>