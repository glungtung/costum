<?php
$keyTpl     = "modal_with_iframe_content";
$paramsData = [
    "titleModal"             => "Title Modal Header",
    "lienButton"        => "https://codimd.communecter.org",
    "labelButton"       => "Boutton",
    "colorlabelButton"  => "#000000",
    "colorBorderButton" => "#000000",
    "colorButton"       => "#ffffff",
    "colorButtonHover" => "#000",
    "colorlabelButtonHover" => "#fff",
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">
    .textBack_<?=$kunik?> h2{
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .button_<?=$kunik?> {
        background-color: <?=$paramsData["colorButton"]?>;
        border:1px solid <?=$paramsData["colorBorderButton"]?>;
        color: <?=$paramsData["colorlabelButton"]?>;
        margin-bottom: 4%;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        padding: 10px;
        font-size: 16px;
        cursor: pointer;
        /*border-radius: 20px ;*/
        padding: 8px 8px;
    }

    .button_<?=$kunik?>:hover {
        background-color: <?=$paramsData["colorButtonHover"]?>;
        color: <?=$paramsData["colorlabelButtonHover"]?>;
        font-weight:700;
    }
    .parallax_<?=$kunik?> {
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }


    @media (max-width: 414px) {
        .textBack_<?=$kunik?> h2{
            font-size: 20px;
        }
        .textBack_<?=$kunik?> p{
            font-size: 14px
        }

        .button_<?=$kunik?> {
            padding: 8px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 11px;
            cursor: pointer;
            border-radius: 20px;
        }
    }

    @media screen and (min-width: 1500px){
        .textBack_<?=$kunik?> h2{
            font-size: 40px;
        }
        .textBack_<?=$kunik?> p{
            line-height: 35px;
            font-size: 25px;
        }
        .button_<?=$kunik?> {
            padding: 10px 20px;
            margin-bottom: 2%;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 25px;
            cursor: pointer;
            border-radius: 35px;
        }
    }

    #modal_<?=$kunik?> .modal-dialog,
    #modal_<?=$kunik?> .modal-content {
        position: fixed;
        top: 10%;
        right: 5%;
        left: 5%;
        bottom: 10%;
        border-radius: 0;
        box-shadow: none;
        height: auto;
        padding-top: 10px !important;
        width: auto !important;
    }
    #modal_<?=$kunik?>.modal {
        margin: 0;
        padding: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.5);
        border-left: 3px solid #333;
        z-index: 200000;
    }

    #modal_<?=$kunik?> .modal-body {
        height: 100%;
    }
</style>

<section  class="parallax_<?=$kunik?>" >
    <div class="col-xs-12 textBack_<?=$kunik?> text-center container"  >

        <a href="<?=$paramsData["lienButton"]?>" id="modal_button_<?=$kunik?>"  class=" button_<?=$kunik?> btn btn-lg btn-primary"  data-toggle="modal" data-target="#modal_<?=$kunik?>">
            <?=$paramsData["labelButton"]?>
        </a>

        <div class="modal fade" id="modal_<?=$kunik?>" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="btn btn-default btn-sm text-dark pull-right close-modal margin-left-10" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                    </button>
                    <a href="<?=$paramsData["lienButton"]?>" target="_blank" class="btn btn-default btn-sm  pull-right ">
                        <i class=" fa fa-external-link"></i> <span>Ouvrir dans un autre onglet</span>
                    </a>
                    <h4 class="modal-title text-left"><?=$paramsData["titleModal"]?></h4>
                </div>
                <div class="modal-body" style="padding:0px;">
                    <p>This is a large modal.</p>
                </div>
            </div>

        </div>
    </div>

</section>


<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "labelButton" : {
                        "label" : "<?php echo Yii::t('cms', 'Button label')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelButton
                    },
                    "lienButton" : {
                        "label" : "<?php echo Yii::t('cms', 'Button link')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.lienButton
                    },
                    "titleModal" : {
                        "label" : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleModal
                    },
                    "colorlabelButton":{
                        label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButton
                    },
                    "colorButton":{
                        label : "<?php echo Yii::t('cms', 'Button color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButton
                    },
                    "colorButtonHover":{
                        label : "<?php echo Yii::t('cms', 'Background color on hover')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButtonHover
                    },
                    "colorlabelButtonHover" :{
                        label : "<?php echo Yii::t('cms', 'Text color on hover')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButtonHover
                    },
                    "colorBorderButton":{
                        label : "<?php echo Yii::t('cms', 'Button border color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorderButton
                    }
                },

                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        mylog.log("sectiondyfff",sectionDyf);
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });

    $('#modal_button_<?=$kunik?>').on('click', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $(".modal-body").html('<iframe width="100%" height="100%" frameborder="0" allowtransparency="true" src="'+url+'"></iframe>');
    });
</script>