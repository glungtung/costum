<?php 
$keyTpl = "showDynform";
$paramsData = [
	"title" => "BIENTOT LE MAPPING DYNAMIQUE..."
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<style>	
	<?php if(!Authorisation::isInterfaceAdmin()){?>
		#menuTopLeft a {
			pointer-events: none;
		}
	<?php } ?>
    .<?= $kunik?>  .bouton:hover {
		background: transparent;
		color: white !important;
		border: 1px solid #F0FCFF;
	}
    .<?= $kunik?>{
        padding-top : 3%;
    }
	.<?= $kunik?>  .bouton{
        font-family: 'NotoSans-Regular' !important;
		background-color: #fff;
		padding: 5px 8px;
		border-radius: 30px;
		font-size: 15px;
        margin-top: 10px;
        margin-bottom: 20px;
		font-weight: bold;
        line-height: 28px;
	}
	.title<?=$kunik?>{
		font-size: 30px;
		color: white;
		text-transform: none;
		text-decoration: none;
		font-weight: 700;
	}
</style>
<div class="<?= $kunik?> text-center">
	<div class="sp-text img-text-bloc title<?=$kunik?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?= $paramsData["title"]?></div>
    <a href="javascript:;" id="joinBtn<?= $kunik ?>" class="btn bouton btnTwo" style="text-decoration : none;">Je souhaite rejoindre l'écosystème </a>
</div>
<script>
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"});
        $("#joinBtn<?= $kunik ?>").off().click(function(){            
            dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"});
		});
        sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		     	"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"icon" : "fa-cog",
				"properties" : {
				},
				beforeBuild : function(){
		            uploadObj.set("cms","<?php echo $blockKey ?>");
		        },
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                      $("#ajax-modal").modal('hide');
	                      urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	              	}

				}
			}
		};
    })
</script>