
<?php
$keyTpl     = "showing_pdf_in_popup_modal";
$paramsData = [
    "title"             => "Lorem Ipsum",
    "content"           => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "buttonlabel"       => "Boutton",
    "buttoncolorlabel"  => "#2b7849",
    "buttoncolor"       => "#e7e4e4",
    "buttoncolorBorder" => "#b8eeb4",
    "showType" => "textbtn"
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php 
$blockKey = (string)$blockCms["_id"];
$initFile = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'pdf1',
    ), "file"
  );

$latestLogo = [];
$initImage = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"logo"
    ),"image"
);
foreach ($initImage as $key => $value) {
    $latestLogo[]= $value["imagePath"];
}
?>

<style type="text/css">
    .iframe-container {    
	    padding-bottom: 60%;
	    padding-top: 30px; height: 0; overflow: hidden;
	}
	 
	.iframe-container iframe,
	.iframe-container object,
	.iframe-container embed {
	    position: absolute;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	}
	@media (min-width: 768px) {
		#myModalDoc .modal-header {
		    min-height: 80px;
		}
	}

	.textView_<?=$kunik?> h2{
    margin-bottom: 10px;
    margin-top: 10px;
  }


  .button_<?=$kunik?> {
    background-color: <?=$paramsData["buttoncolor"]?>;
    border:1px solid <?=$paramsData["buttoncolorBorder"]?>;
    color: <?=$paramsData["buttoncolorlabel"]?>;
    padding: 8px 26px;
    margin-bottom: 4%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    cursor: pointer;
    border-radius: 20px ;
  }
  .document_<?=$kunik?> {
  	padding: 20px 0 20px 0!important;
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .document_<?=$kunik?> h1 {
    margin: 0 0 20px 0;
    font-size: 48px;
    font-weight: 700;
    line-height: 56px;
    color: #fff;
  }
  .document_<?=$kunik?> h2{
   margin: 0 0 10px 0;
   font-size: 35px;
   line-height: 35px;
   text-transform: none;
   /*margin-bottom: -180px;
   margin-top: 4%;*/
 }
 .document_<?=$kunik?> h3{
  font-size: 25px;
  line-height: 32px;
  text-transform: none;
  margin-bottom: -30px;
  }
  .document_<?=$kunik?> .description{
    line-height: 26px;
    text-transform: none;
    /*padding: 10% 5% 10% 5%;
    margin-bottom: -90px;*/
  }
@media (max-width: 414px) {
  .document_<?=$kunik?> h2{
    margin: 0 0 20px 0;
    font-size: 20px;
    line-height: 27px;
    text-transform: none;
    margin-bottom: -78px;
    margin-top: 17px;
  }
  .document_<?=$kunik?> .description{
    line-height: 15px;
    text-transform: none;
    padding: 82px 0px 19px 15px;
    margin-bottom: -40px;
    font-size: 13px;
  }
  .document_<?=$kunik?> {
    width: 100%;
    height: 100%;
    padding: 0 15px;
  }
  .button_<?=$kunik?> {
    padding: 8px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 11px;
    margin-top: 25px;
    /*margin: 22% 3% 4% 2%;*/
    cursor: pointer;
    border-radius: 20px;
  }
}

@media screen and (min-width: 1500px){
  .document_<?=$kunik?> h2{
    margin: 0 0 20px 0;
    font-size: 40px;
    line-height: 35px;
    text-transform: none;
    margin-top: 2%;
  }
  .document_<?=$kunik?> .description{
    line-height: 35px;
    font-size: 25px;
    text-transform: none;
    padding: 1% 0% 8% 0%;
    margin-bottom: -90px;
  }
  .button_<?=$kunik?> {
    padding: 10px 20px;
    margin-bottom: 2%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 25px;
    cursor: pointer;
    border-radius: 35px;
  }
}
	
</style>
  <section  class="document_<?=$kunik?>" >
    <div class="textView_<?=$kunik?> text-center" >
        <?php if ($paramsData["showType"] == "textbtn") { ?>
          <h2 class="sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?=$paramsData["title"]?></h2>
          <div class="sp-text img-text-bloc description" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content" ><?=$paramsData["content"]?></div>

          <a href="<?php echo Document::getLastImageByKey($blockKey, "cms", "pdf","pdf1"); ?>" class="view-pdf button_<?=$kunik?>" >
           <?=$paramsData["buttonlabel"]?>
         </a>
        <?php } else { ?>
            <a href="<?php echo Document::getLastImageByKey($blockKey, "cms", "pdf","pdf1"); ?>" class="view-pdf" >
                <?php if(isset($latestLogo) && !empty($latestLogo)){ ?>
                <img class="logo<?=$kunik ?> img-responsive" src="<?php echo $latestLogo[0] ?>" style="width: 100%;">
                <?php } else {?>
                    <img class="logo<?=$kunik ?> img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg" style="width: 100%;">
                <?php } ?>
            </a>
        <?php } ?>
   </div>
   
 </section>

<script type="text/javascript">
/*
* This is the plugin
*/
(function(a){
	a.createModal=function(b){
		defaults={
			title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false
		};
		var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";
			html='<div class="portfolio-modal modal fade" id="myModalDoc" tabindex="-1" role="dialog" aria-hidden="true">';
        
			html+='<div class="modal-content padding-top-15">';
			html+='<div class="close-modal" data-dismiss="modal"><div class="lr"><div class="rl"></div></div></div>';
			html+='<div class="modal-header">';
			if(b.title.length>0){
				html+='<h4 class="modal-title">'+b.title+"</h4>"
			}
			html+="</div>";
			html+='<div class="modal-body" '+c+">";
			html+=b.message;
			html+="</div>";
			// html+='<div class="modal-footer">';
			// if(b.closeButton===true){
			// 	html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'
			// }
			// html+="</div>";
			html+="</div>";
			html+="</div>";
			a("body").prepend(html);
			a("#myModalDoc").modal().on("hidden.bs.modal",function(){
				a(this).remove()
			})
	}
})(jQuery);

/*
* Here is how you use it
*/
$(function(){    
    $('.view-pdf').on('click',function(){
        var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        $.createModal({
        title:'<?=$paramsData["title"]?>',
        message: iframe,
        closeButton:true,
        scrollable:false
        });
        return false;        
    });    
})

sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
	          "pdf1" : {
	            "inputType" : "uploader",
	            "label" : "<?php echo Yii::t('cms', 'Your pdf file')?>",
	            "showUploadBtn" : false,
	            "docType" : "file",
	            "itemLimit" : 1,
	            "contentKey" : "file",
	            "domElement" : "pdf1",
	            "placeholder" : "PDF",
	            "endPoint" : "/subKey/pdf1",
	            "filetypes" : [
	                "pdf"
	            ],
	            initList : <?php echo json_encode($initFile) ?>
	          },
            "showType":{
                "label" : "<?php echo Yii::t('cms', 'Display type')?>",
                inputType : "select",
                options : {
                    "textbtn" : "<?php echo Yii::t('cms', 'Text and button')?>",
                    "imgbtn" : "<?php echo Yii::t('cms', 'Image')?>"
                },
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.showType
            },
	          "buttonlabel" : {
	            "label" : "<?php echo Yii::t('cms', 'Button label')?>",
	            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonlabel
	          },
	          "buttoncolorlabel":{
	            label : "<?php echo Yii::t('cms', 'Button label color')?>",
	            inputType : "colorpicker",
	            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttoncolorlabel
	          },
	          "buttoncolor":{
	            label : "<?php echo Yii::t('cms', 'Button color')?>",
	            inputType : "colorpicker",
	            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttoncolor
	          },
	          "buttoncolorBorder":{
	            label : "<?php echo Yii::t('cms', 'Button border color')?>",
	            inputType : "colorpicker",
	            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttoncolorBorder
	          },
            "logo" :{
                "inputType" : "uploader",
                "docType": "image",
                "contentKey":"slider",
                "endPoint": "/subKey/logo",
                "domElement" : "logo",
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "label": "<?php echo Yii::t('cms', 'Image')?>",
                "itemLimit" : 1,
                "showUploadBtn": false,
                initList : <?php echo json_encode($initImage) ?>
            },
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                	//alert("bien ajouter");
                  toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"button",6,6,null,null,"Propriété du bouton","green","");
    });


  });
</script>