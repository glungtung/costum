<?php
$keyTpl     = "download_pdf";
$paramsData = [
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);

$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'pdf1',
    ), "file"
  );
?>
<style>
  .contain<?= $kunik?>{
    padding-top: 10%;
  }
  .col<?= $kunik?> h4{
    font-family: 'NotoSans-Bold';
    text-transform: none;
    font-size: 17px;
  }
  .col<?= $kunik?> .markdown p{
    font-family: 'NotoSans-Regular' !important;
    font-size: 16px !important;
  }
  .col<?= $kunik?> img{
    margin-bottom : 5px;
  }
  .col<?= $kunik?> { 
    margin-top : 10px;
    margin-left: 1px;
    border-radius: 20px;
    box-shadow: 0px 0px 6px silver;
    color : white;
    padding: 20px 15px 20px 15px;
    display: -webkit-box;
    word-wrap: break-word;
    -webkit-box-orient: vertical;
  }
  .float<?= $kunik?>{
    margin-bottom: 20px;
    display: inline-block;
  }
  .col<?= $kunik?> .btn<?= $kunik?> a{
    font-family:'Poppins_Medium' !important;
    font-size :12px;
    margin-left: 4px;
    border-radius: 20px;
    color: white;
    padding: 5px 20px 5px 20px;
    text-decoration : none;
    border: 3px solid white;
  }

  .btn<?= $kunik?> {
    margin-top : 5px;
  }
  .text-<?= $kunik?> {
    height : 120px;
  }
  .edit<?= $kunik?>.editSectionBtns{
    position: absolute;
    top: 15%;
    z-index: 2;
    left: 50%;
  }
 
  .text-blue<?=$kunik?>{
    color: #49CBE3;
    font-size: 13px;
    font-weight: bolder;
    background-color: transparent !important;
  }
  .contain<?= $kunik?> .swiper-pagination-bullet{
    margin-top: 2rem;
    background-color: transparent;
    display: inline-block;
    width: 1rem;
    height: 1rem;
    border: 1px solid white;
    opacity: .9;
  } 
  
  .contain<?= $kunik?> .swiper-pagination-bullet-active {
    background-color: white;
    opacity: 1;
  }
  @media screen and (min-width : 1186px) and (max-width : 1403px){
    .col<?= $kunik?> .btn<?= $kunik?> a{
      margin-bottom : 10px;
    }
  }
  @media screen and (min-width : 1404px) and (max-width : 1567px){
    .col<?= $kunik?> .btn<?= $kunik?> a{      
      padding: 5px 10px 5px 10px;
    }
    .col<?= $kunik?> { 
      padding: 30px 0px 20px 0px;
    }
  }
  @media screen and (min-width : 1180px) and (max-width : 1390px){
    .text-<?= $kunik?> {
      min-height : 170px !important;
    }
  }
  @media (min-width : 1403px){
    .btn<?= $kunik?> {
      display: inline-flex;
    }
  }
  .contain<?= $kunik?> .box-title {
      min-height: 58px;
  }
  .contain<?= $kunik?> a:hover .box-title {
      color: #49cbe3;
      text-decoration: none;
      text-decoration: ;
  }
  .contain<?= $kunik?> .modal-img {
      margin-top: -80px;
      width: 90px;
      height: 90px;
      border: 5px solid white;
      border-radius: 50%;
      padding: 5px;
      background: #131c46;
      object-fit: contain;
  }
  .contain<?= $kunik?> .link-modal {
    color: white;
    text-decoration: none;
  }
  .contain<?= $kunik?> .link-modal img {
    height: 50px;
    width: auto;
  }
  .contain<?= $kunik?> .modal-dialog {
    border: 2px solid white;
    border-radius: 10px;
  }
  .contain<?= $kunik?> .modal .modal-header {
      border-bottom: none;
      background-color: #0E163C;
      color: white;
  }
  .contain<?= $kunik?> .modal .modal-header .close {
    color: white;
    opacity: 1;
    margin-top: -6px;
  }
  .contain<?= $kunik?> .btn-editSectionpdf {
    width: 35px;
    height: 35px;
    line-height: 35px;
    border-radius: 50%;
    font-size: 18px;
    border: 1px solid #ffffff;
    margin-right: 5px;
    padding-left: 5px;
    transition: all .5s ease 0s;
    padding: 0px;
  }
  .edit<?= $kunik?> .btn-editSectionpdf:hover {transform:rotate(360deg)}
  .contain<?= $kunik?> .text-secondary {
    text-transform: none;
    color: #131c46;
  }

  .contain<?= $kunik?> .modal .markdown{
    font-size: 16px;
    color: black;
    font-family: 'NotoSans-Regular';
    text-align: left;
  }
  .contain<?= $kunik?> .modal h3{
    margin-bottom : 10px;
  }
  .amodal<?= $kunik?> {
    background: #0E163C;
    padding: 10px;
    text-decoration: none;
    border-radius: 20px;
    margin : 5%;
    font-size: 16px;
    color: white;
    font-family: 'NotoSans-Bold' !important;
  }
  .amodal<?= $kunik?>:hover {
    background: #0E163C;
    padding: 10px;
    text-decoration: none;
    border-radius: 20px;
    font-size: 16px;
    color: white;
    font-family: 'NotoSans-Bold' !important;
  }
  .contain<?= $kunik?> .modal{
    top: 10%; 
  }
  #pdfparticle-js {
      width: 100%;
      height: 100%;
      background-size: cover;
      background-position: 50% 50%;
      position: absolute;
      top: 0px;
      z-index: 0;
  }
  .contain<?= $kunik?> .modal .markdown{
    margin-bottom : 4%;
  }
  .contain<?= $kunik?> .modal .markdown ul{
    margin-top: -3%;
    margin-bottom: -3%;
    line-height: 1.2;
  }
</style>
<div class="col-xs-12 no-padding">
    <div class="text-center edit<?= $kunik?> editSectionBtns">
      <div class="" style="width: 100%; display: inline-table; padding: 10px;">
        <?php if($costum["editMode"] == "true"){?>
          <div class="text-center addElement<?= $kunik ?>">
            <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add content')?></button>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 contain<?= $kunik?> text-center ">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php if (isset($blockCms["content"])) {
            $content = $blockCms["content"];
            $interv = 1;
            foreach ($content as $key => $value) {
              ${'initFilesIcone' . $key}= Document::getListDocumentsWhere(
                array(
                  "id"=> $blockKey,
                  "type"=>'cms',
                  "subKey"=> (string)$key
                ), "image"
              );
              ${'arrFileIcone' . $key}= [];
              foreach (${'initFilesIcone' . $key} as $k => $v) {
                ${'arrFileIcone' . $key}[] =$v['imagePath'];
              }
              ?>
              <div class="col<?= $kunik?> swiper-slide ">
                <a href="#myModalShow<?= @$interv ?>"  class="link-modal" role="button" data-toggle="modal" >
                <?php if (count(${'arrFileIcone' . $key})!=0) {?>
                  <img src="<?php echo ${'arrFileIcone' . $key}[0] ?>" alt="">
                <?php } ?>
                  <div class="box-title">
                      <h4 ><?=  @$value["title"]?></h4>
                  </div>
                </a>
                <div class="text-<?= $kunik?>">
                    <p class="markdown"><?= @$value["description"]?></p>
                  </div>

                  <?php if($costum["editMode"] == "true"){ ?>
                      <div
                              class="text-center editSectionBtns hiddenPreview padding-top-5" >
                          <a  href="javascript:;"
                              class="btn btn-editSectionpdf  btn-primary editElement<?= $blockCms['_id'] ?>"
                              data-key="<?= $key ?>"
                              data-title='<?= @$value["title"] ?>'
                              data-description="<?= @$value["description"] ?>"
                              data-textdesc="<?= @$value["textDesc"] ?>"
                              data-tags="<?= @$value["tags"] ?>"
                              data-icon='<?php echo json_encode(${"initFilesIcone" . $key}) ?>' >
                              <i class="fa fa-edit"></i>
                          </a>

                          <a  href="javascript:;"
                              class="btn  bg-red btn-editSectionpdf text-center deleteElement<?= $kunik?> "
                              data-id ="<?= $blockKey ?>"
                              data-path="content.<?= $key ?>"
                              data-collection = "cms"
                          >
                              <i class="fa fa-trash"></i>
                          </a>
                      </div>
                  <?php } ?>
              </div>
            <?php $interv++; }
          }?>
        </div>
        <br><br>
        <div class="hidden-lg swiper-pagination"></div>
      </div>
      <?php
      $int = 1;
      foreach ($content as $k => $v) {
        ${'initFilesIcone' . $k}= Document::getListDocumentsWhere(
          array(
            "id"=> $blockKey,
            "type"=>'cms',
            "subKey"=> (string)$k
          ), "image"
        );
        ${'arrFileIcone' . $k}= [];
        foreach (${'initFilesIcone' . $k} as $ki => $vi) {
          ${'arrFileIcone' . $k}[] =$vi['imagePath'];
        }
      ?>
      <div class="modal fade text-center py-5"  id="myModalShow<?= @$int ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">✕</button>
            </div>
            <div class="modal-body">
                <?php if (count(${'arrFileIcone' . $k })!=0) {?>
                    <img src="<?php echo ${'arrFileIcone' . $k }[0] ?>" alt=""  class="modal-img">
                <?php } ?>
                <h3 class="text-secondary"><?=  @$v["title"]?></h3>
                <p class="pb-3 text-muted markdown text-left"><small><?= @$v["textDesc"]?></small></p>
                <a class="amodal<?= $kunik?>" href="javascript:;" data-tags="<?= @$v["tags"]?>">Découvrez dans l'annuaire</a>  
            </div>
          </div>
        </div>
      </div>


      <?php $int++;
     } ?>
    </div>
    <div id="pdfparticle-js"></div>
</div>
<script>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                	//alert("bien ajouter");
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
    $(".addElement<?= $kunik ?>").click(function() {        
      var keys<?= $blockCms['_id'] ?> = Object.keys(<?php echo json_encode($content); ?>);
			var	lastContentK = 0; 
			if (keys<?= $blockCms['_id'] ?>.length!=0) 
				lastContentK = parseInt((keys<?= $blockCms['_id'] ?>[(keys<?= $blockCms['_id'] ?>.length)-1]), 10);
      var tplCtx = {};
      tplCtx.id = "<?= $blockKey ?>";
      tplCtx.collection = "cms";

      tplCtx.path = "content."+(lastContentK+1);
      var obj = {
        subKey : (lastContentK+1),
        icon :     $(this).data("icon"),
        title :     $(this).data("title"),
        description:    $(this).data("description"),
          
      };
      var activeForm = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Add a file')?>",
          "type" : "object",
          onLoads : {
            onload : function(data){
              $(".parentfinder").css("display","none");
            }
          },
          "properties" : getProperties(obj),
          beforeBuild : function(){
            uploadObj.set("cms","<?= $blockCms['_id'] ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( activeForm.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            mylog.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                  dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                  });
                } );
            }
          }
        }
      };
      dyFObj.openForm( activeForm );
    });
    $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>" 
			tplCtx.collection = "cms";
			tplCtx.path = "content."+(key);
			var obj = {
        subKey : key,
        icon :     $(this).data("icon"),
        title :     $(this).data("title"),
        description:    $(this).data("description"),
        textdesc:    $(this).data("textdesc"),
        tags:      		$(this).data("tags")
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "<?php echo Yii::t('cms', 'File modification')?> "+$(this).data("title"),
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj,key),
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
          save : function (data) {  
            tplCtx.value = {};
            $.each( activeForm.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
            }
          }
				}
				};          
				dyFObj.openForm( activeForm );				 
    });

    $(".deleteElement<?= $kunik?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?> ?",

      function(result){
        if (!result) {
          return;
        }else {
          dataHelper.path2Value( deleteObj, function(params) {
            mylog.log("deleteObj",params);
            toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
            // urlCtrl.loadByHash(location.hash);
          });
        }
      }); 
			
		});
    function getProperties(obj={},newContent){
      mylog.log("objjjjj",obj);
      var props = {
        title : {
          label : tradCms.title,
          inputType : "text",
          value : obj["title"]
        },
        description : {
          inputType : "textarea",
          label : trad.description,
          markdown : true,
          value :  obj["description"]
        },
        textDesc :{
          inputType : "textarea",
          label : "<?php echo Yii::t('cms', 'Text description')?>",
          markdown : true,
          value :  obj["textdesc"]
        },
        tags :{
          inputType: "select", 
          "placeholder": "",
          label : "Famille",          
          options : costum.lists.family,
          value :  obj["tags"]
        },
				logo : {
					inputType : "uploader",	
					label : "<?php echo Yii::t('cms', 'Logo')?>",
          docType: "image",
					contentKey : "slider",
					domElement : obj["subKey"],		
          filetype : ["jpeg", "jpg", "gif", "png"],
          showUploadBtn: false,
          endPoint :"/subKey/"+obj["subKey"],
          initList : obj["icon"]
				},
      };
      return props;
    }
     
    if($(".badge-theme-count").length!=0){
      if(searchObj && searchObj.search.obj.types.length==0){
          searchObj.search.obj.types = ["organizations"];
      }searchObj.filters.actions.themes.setThemesCounts(searchObj);
    }
    
    $(".amodal<?= $kunik?>").click(function(){
			var tags = $(this).data("tags");         
      if(tags == ""){
        searchObj.search.obj.tags = [];
        urlCtrl.loadByHash("#search");
      }
      else{
        tags = tags.split(",");        
        urlCtrl.loadByHash("#search?tags="+tags);
      }            
		})
    var swiper = new Swiper(".contain<?= $kunik?> .swiper-container", {
      slidesPerView: 1,
      spaceBetween: 1,
      pagination: {
          el: '.swiper-pagination',
          clickable: true,
      },
      autoplay: {
          delay: 1500,
      },
      /*navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },*/
      keyboard: {
          enabled: true,
      },
      breakpoints: {
          486: {
              slidesPerView: 1,
          },
          768: {
              slidesPerView: 2,
              spaceBetween: 10,
          },
          992: {
              slidesPerView: 2,
              spaceBetween: 10,
          },
          1024: {
              slidesPerView: 3,
              spaceBetween: 10,
          },
          1200: {
            slidesPerView: 7,
            spaceBetween: 10,
          }
      }
  });

  coInterface.bindLBHLinks();
  });
  callParticle("pdfparticle-js");
</script>