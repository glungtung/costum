<?php 
    $keyTpl ="consultEducation";
    $paramsData = [
        "buttonRejoindre"=>"Je rejoint le filière",
        "buttonProjet"=>"Je souhaite ajouter un projet",
        "backgroundColor"=>"#97bf1e",
        "formId"=>"",
        "image"=> Yii::app()->getModule('costum')->assetsUrl."/images/smarterre/phototerritoire.jpg",
        "content" => "_Restons connectés pour faire avancer notre filière énérgetique_"
    ];

    $childForm = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<!-- ****************get image uploaded************** -->
<?php 

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
    $image = [];

    foreach ($initImage as $key => $value) {
        $image[]= $value["imagePath"];
    }

    if($image!=[]){
        $paramsData["image"] = $image[0];
    }
    
 ?>
<!-- ****************end get image uploaded************** -->

<?php 

    # If user is admin
    #$is_admin = false;
    #$admins = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
    #if(count($admins)!=0){
    #    $is_admin = true;
    #}

    # If user has already joined
    $members = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"]=>['$exists' => true ], "costumId"=>$costum['contextId']));
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    if($paramsData["formId"]!=""){
        $formId = $paramsData["formId"];
    }else{
        # Get the form id
        $form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
        $formId = "";
        foreach($form as $key => $value) { $formId = $key; }
    }

    # If user has submited the coform
    $answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));
    $has_answered = false;
    
    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if(Yii::app()->session["userId"] == $value["user"]){
            $myAnswer = $value['_id']->{'$id'};
        }
    }
    
    if(isset(Yii::app()->session["userId"])){
        
        $data = PHDB::findByIds("citoyens", [Yii::app()->session["userId"]] ,["links.follows", "links.memberOf"]);
        
        $memberOf_ids = array();

        if(isset($data[Yii::app()->session["userId"]]["links"]["memberOf"])){
            $links2 = $data[Yii::app()->session["userId"]]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }
?>

<style>
    .join-<?= $kunik?> {
        background : <?php echo $paramsData["backgroundColor"] ?>;
		margin: 0 !important;
        margin-top : 2%;
		padding-right: 0px;
		
    }
	.join-<?= $kunik?> .col-md-6{
		padding-right: 0;
	}
    .join-btn-<?= $kunik?>{
        text-align : center;
        color : white;
		margin-top:3%;
    }

	.carto-p-m-<?= $kunik?>{
		font-size: 2.85vw;
		margin-top: 6%;
		line-height: 3vw;
	}
	.carto-p-m-<?= $kunik?> p{
        font-size: 2.85vw;
        margin-top: 6%;
        line-height: 3vw;
    }
	

    .join-btn-<?= $kunik?> a{
        border-radius: 40px;
        font-size: 1.3em;
    }
</style>

<div class="join-<?= $kunik?> carto-n-<?= $kunik?> row content-<?= $kunik?>">
	<div class="join-btn-<?= $kunik?> col-md-6">
        <br><br>
		<!-- Btn je rejoins -->
        <?php if(!$is_member){ ?>
            <a class="btn btn-primary btn-lg" href="javascript:;" onclick="dyFObj.openForm('organization');" style="text-decoration : none;"> 
                <?php echo $paramsData["buttonRejoindre"] ?>
            </a>
        <?php }else{ ?>
            <?php if(!$has_answered){ ?>
                <a class="btn btn-primary btn-lg lbh" href="#answer.index.id.new.form.<?= $formId; ?>" style="text-decoration : none;"> 
                    Je rempli le formulaire membre
                </a>
            <?php }else{ ?>
                <a class="btn btn-primary btn-lg lbh" href="#answer.index.id.<?= $myAnswer; ?>.mode.w" data-id="<?= $formId; ?>" style="text-decoration : none;"> 
                    Je met à jour mes données
                </a>
            <?php } ?>
        <?php } ?>
		<br> <br>

		<!-- Btn je propose -->
        <a class="btn btn-primary btn-lg" onclick="dyFObj.openForm('project');" style="text-decoration : none" href="javascript:;">
            <?php echo $paramsData["buttonProjet"] ?>
        </a>

		<div class="carto-p-m-<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"><?php echo $paramsData["content"] ?></div>	
	</div>

	<div class="join-img col-md-6">
		<img src="<?php echo $paramsData["image"] ?>" width="100%" class="img-responsive">
	</div>
    
</div>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                
                "buttonRejoindre" : {
                    "inputType" : "text",
                    "label" : "Texte du bouton pour rejoindre le filière",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonRejoindre
                },
                "buttonProjet" : {
                    "inputType" : "text",
                    "label" : "Texte du bouton pour ajouter un projet",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonProjet
                },
                "formId": {
                    "label" : "Formulaire pour membre",
                    "class" : "form-control",
                    "inputType" : "select",
                    "options": {
                        <?php 
                            foreach($childForm as $key => $value) { 
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }    
                        ?>
                    }
                },
                "backgroundColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur d'arrière plan",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.backgroundColor
                },
                "image" :{
                    "inputType" : "uploader",
                    "label" : "image",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    "domElement" : "image",
                    "endPoint" :"/subKey/image",
                    initList : <?php echo json_encode($initImage) ?>
                }
            },
           beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                   dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }
            }
        }
    }

        $("#btnOk").click(function(){
            var test = new Array();
            $("input[name='organization']:checked").each(function() {
                test.push($(this).val());
            });

            alert(JSON.stringify(test));
        });

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        costum.searchExist = function (type,id,name,slug,email) { 

            var data = {
                collection : type,
                id : id,
                type: type,
                path : "allToRoot",
                value : {
                    costumId: "<?php echo $costum["contextId"]; ?>",
                    category: $("#category").val(),
                    "source": {
                        "keys" : ["<?php echo $costum["slug"]; ?>"],
                    },
                    reference : {
                        costum : [ 
                            "<?php echo $costum["slug"]; ?>"
                        ]
                    }
                }
            }

            
            if(type == "projects"){
                //costumInputs.elementproject.afterSave(data);
                delete data.value.reference;
                
                data.typeStruct = $("#ajaxFormModal #typeStruct").val();

                dataHelper.path2Value( data, function(params) { 
                    if(dyFObj.closeForm()){
                        $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                    }else{
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    }
                });
            }else{
                if($("#category").val()!=null){
                    data.typeStruct = $("#ajaxFormModal #typeStruct").val();

                    dataHelper.path2Value( data, function(params) { 
                        if(dyFObj.closeForm()){
                            $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                        }else{
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        }
                    });
                }else{
                    toastr.error("Séléctionner d'abord votre catégorie, En quelle catégories est-il votre organisation");
                }
            }
        }
        
    });

    
</script>
