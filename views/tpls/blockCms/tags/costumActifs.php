
<?php
$keyTpl     = "costumActifs";
$paramsData = [
	"title"      => "Les cocitys actifs",
	"colorTitle" =>"#005E6F",
	"activeFilter" => true,
	"costumSlug" => "cocity"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if ( isset($blockCms[$e])) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}

?>
<style type="text/css">
	.costumActif_<?= $kunik?> h1 {
		color: <?= $paramsData["colorTitle"]?>;
	}
	.listCostum<?= $kunik?> {
		margin-bottom: 20px;
		margin-bottom: 20px;
	}
	.listCostum<?= $kunik?> .leaflet-popup-content-wrapper {
		border: 3px solid #005e6f;
	}
	.listCostum<?= $kunik?> .popup-address{
		margin-top: 7px;
		font-size: 16px;
	}
	.listCostum<?= $kunik?> .btn-more {
		border-color: #005e6f !important;
		color: #005e6f !important;
	}
	.listCostum<?= $kunik?> .btn-more:hover {
		background-color: #005e6f !important;
		color: white !important;
	}

	.listCostum<?= $kunik?> .leaflet-container a.leaflet-popup-close-button {
		color: #005e6f !important;
	}
	#thematic<?= $kunik ?>{
        background: #eee;
        padding: 0.3em;
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: white;
        font-weight: 900 !important;
    }
    #thematic<?= $kunik ?> .m-1{
        margin: 0.3em;
    }
    #thematic<?= $kunik ?> .btn-active {
    	color: white;
    	background-color: #052434;
    	border-color: #052434;
    	font-weight: 700;
    }

</style>
<div class=" costumActif_<?= $kunik?> text-center">
	<h1 > 
		<i class="fa fa-users"> </i> 
		<font class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> 
			<?= $paramsData["title"]?>
		</font>
	</h1>
	<div class="">
		<div class="row">
			<div class="main_<?= $kunik?> col-md-12 col-sm-12 col-xs-12 vertical" >
				<div class="col-xs-12 bodySearchContainer  ">
					<div id="listCostum" class="no-padding col-xs-12 listCostum<?= $kunik?>"  style="height: 500px; position: relative;" >

					</div>
					<div id="thematic<?= $kunik ?>"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function removeParams(sParam){
		var url = window.location.href.split('?')[0]+'?';
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;
		
		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] != sParam) {
				url = url + sParameterName[0] + '=' + sParameterName[1] + '&'
			}
		}
		return url.substring(0,url.length-1);
	}

	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	var map<?= $kunik ?> = new CoMap({
		container : ".listCostum<?= $kunik?>",
		activePopUp : true,
		mapOpt:{
			menuRight : <?= ($paramsData["activeFilter"]!="")?$paramsData["activeFilter"]:"true" ?>,
			btnHide : false,
			doubleClick : true,
			zoom : 2,
		},
		mapCustom:{
			getPopup: function(data){
				var id = data._id ? data._id.$id:data.id;
				var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
				if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
					imgProfil = baseUrl + data.profilThumbImageUrl;
				else
					imgProfil = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
				//var imgProfil = map<?= $kunik ?>.mapCustom.getThumbProfil(data);

				var eltName = data.title ? data.title:data.name;
				var popup = "";
				popup += "<div class='padding-5' id='popup" + id + "'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

				if(data.tags && data.tags.length > 0){
					popup += "<div style='margin-top : 5px;'>";
					var totalTags = 0;
					$.each(data.tags, function(index, value){
						totalTags++;
						if (totalTags < 3) {
							popup += "<div class='popup-tags'>#" + value + " </div>";
						}
					})
					popup += "</div>";
				}
				if(data.address){
					var addressStr="";
					if(data.address.streetAddress) 
						addressStr += data.address.streetAddress;
					if(data.address.postalCode)
						addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
					if(data.address.addressLocality)
						addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
					popup += "<div class='popup-address text-dark'>";
					popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
					popup += "</div>";
				}
				if(data.shortDescription && data.shortDescription != ""){
					popup += "<div class='popup-section'>";
					popup += "<div class='popup-subtitle'>Description</div>";
					popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
					popup += "</div>";
				}
				if((data.url && typeof data.url == "string") || data.email || data.telephone){
					popup += "<div id='pop-contacts' class='popup-section'>";
					popup += "<div class='popup-subtitle'>Contacts</div>";

					if(data.url && typeof data.url === "string"){
						popup += "<div class='popup-info-profil'>";
						popup += "<i class='fa fa fa-desktop fa_url'></i> ";
						popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
						popup += "</div>";
					}

					if(data.email){
						popup += "<div class='popup-info-profil'>";
						popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
						popup += "</div>";
					}

					if(data.telephone){
						popup += "<div class='popup-info-profil'>";
						popup += "<i class='fa fa-phone fa_phone'></i> ";
						var tel = ["fixe", "mobile"];
						var iT = 0;
						$.each(tel, function(keyT, valT){
							if(data.telephone[valT]){
								$.each(data.telephone[valT], function(keyN, valN){
									if(iT > 0)
										popup += ", ";
									popup += valN;
									iT++; 
								})
							}
						})
						popup += "</div>";
					}

					popup += "</div>";
					popup += "</div>";
				}
				var url = baseUrl+'/costum/co/index/slug/'+data.slug;
				popup += "<div class='popup-section'>";
				popup += "<a href='" + url + "' target='_blank' class=' item_map_list popup-marker' id='popup" + id + "'>";
				popup += '<div class="btn btn-sm btn-more col-md-12">';
				popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
				popup += '</div></a>';
				popup += '</div>';
				popup += '</div>';

				return popup;
			},
			markers: {
				getMarker : function(data){
					var imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/smarterritoire.png";	
					return imgM;
				}
			}
		},
		
		elts : []
	});
	var blocCostumObj<?= $kunik?> ={
		costumList:function(criteriaParams = null){
			var params = {
				slug : "<?= $paramsData["costumSlug"]?>"
			};
			if(criteriaParams!=null && criteriaParams!=""){
                params["tags"] = criteriaParams.split(",");
            }
			ajaxPost(
				null,
				baseUrl+"/costum/cocity/getlistcocityaction",
				params,
				function(data){
					map<?= $kunik ?>.clearMap();
					mylog.log("nomcity",data);
					var src = '';
					map<?= $kunik ?>.addElts(data);

				}
			);
		}
	}    
	var filteredTheme = "";
	jQuery(document).ready(function() {	
		if (getUrlParameter('map')=="scroll") {
			$('html, body').animate(
				{ scrollTop: $("#cocityactif").offset().top - $("#mainNav").height() },
				800,
				() => {
				},
			);
			window.history.pushState('',document.title,removeParams("map"));
		}
		blocCostumObj<?= $kunik?>.costumList();
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section1",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
					"title" : {
						"inputType" : "text",
						"label" : "Titre",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
					},
					"colorTitle":{
						label : "Couleur du titre",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
					"costumSlug" : {
						"inputType" : "text",
						"label" : "Costum.key",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.costumSlug
					},
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?= $blockCms['_id'] ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Modification enregistrer");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}

				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
		/********* Filter thematics *******************/
        var thematic = [];

        if(costum && costum.lists && costum.lists.theme){
            for (const [keyT, valueT] of Object.entries(costum.lists.theme)) {
                var asTags = [];
                let labelT = "";

                if(typeof valueT == "string"){
                    asTags.push(valueT);
                    labelT = valueT;
                }else{
                    labelT = keyT;
                    for (const [keyChild, valueChild] of Object.entries(valueT)){
                        if(!thematic.includes(valueChild)){
                            thematic.push(valueChild);
                        }
                        asTags.push(valueChild);
                    }
                }
                $("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
            }   
        }else{
            $("#thematic<?= $kunik ?>").remove();
        }
		$(".thematic<?= $kunik ?>").off().on("click",function(){
            if(filteredTheme.indexOf($(this).data("filters")) !== -1){
                filteredTheme = filteredTheme.replace($(this).data("filters"), "");
                $(this).removeClass("btn-active");
                $(this).addClass("btn-white");
            }else{
                $(this).removeClass("btn-white");
                $(this).addClass("btn-active");
                if(filteredTheme==""){
                    filteredTheme = $(this).data("filters");
                }else{
                    filteredTheme+=","+$(this).data("filters");
                }
                
            }

			if(filteredTheme.charAt(0)==","){
                filteredTheme = filteredTheme.substring(1);
            }


            blocCostumObj<?= $kunik?>.costumList(filteredTheme);
        });
	})	
</script>