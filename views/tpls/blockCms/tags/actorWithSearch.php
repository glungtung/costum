<?php 
$keyTpl = "actorWithSearch";
$paramsData = [
  "tags"       => "acteurs kosasa",    
  "nameAlbum"  =>"exposition"
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>
<style type="text/css">
  .<?= $kunik?>{
    margin-top: 3%;
    padding-left: 5%;
    padding-right: 5%;
  }

  .addActeur<?= $kunik?> {
    margin-top: 5px;
    margin-left: 31px;
    font-size: 18px;
    border-radius: 35px;
    color: #fff;
    border: 1px solid #f5853c;
    background: #f5853c;

  }
  .addActeur<?= $kunik?>:hover, .addActeur<?= $kunik?>:focus {
    color: #fff;
    background: #f5853c;
  }
  
 
  .<?= $kunik?> .searchVideo {
    margin-left: 15%; 
    margin-bottom:5%
  }
  .<?= $kunik?> .searchBar-filters .search-bar{
    margin-top: 5px;
    border-radius: 20px 0px 0px 20px !important;
    box-shadow: none;
    width: 180px;
    max-width: 700px;
    border-color: #f5833c;
  }
  .<?= $kunik?> .searchBar-filters .main-search-bar-addon {
    width: 15%;
    background-color: #f5833c!important;
    border: 1px solid #f5833c;
    font-size: 14px;
    padding: 9px 7px;
  }
  .<?= $kunik?> #filterActor<?= $kunik?> li {
    list-style: none;
  }
  .<?= $kunik?> #filterActor<?= $kunik?> .dropdown .btn-menu  {
    padding: 9px 10px;
    font-size: 16px;
    border: 1px solid #f5833c;
    color: #6b6b6b;
    top: 0;
    position: relative;
    border-radius: 20px;
    text-decoration: none;
    background: #fff;
    line-height: 20px;
    display: inline-block;
    margin-left: 5px;
  }
  .<?= $kunik?> #filterActor<?= $kunik?> .dropdown   {
    float: left;
    display: grid;
    margin-bottom: 5px;
    margin-top: 5px;
    margin-right: 10px;
    padding-top: 0px;
    padding-bottom: 0px;
  }
  .<?= $kunik?> #filterActor<?= $kunik?> .dropdown .dropdown-menu  {
    border: 1px solid #f5833c;
    padding: 0px;
    position: absolute;
    overflow-y: visible!important;
    top: 50px;
    left: 5px;
    width: 250px;
    border-radius: 2px;
  }
  .<?= $kunik?> #filterActor<?= $kunik?> .dropdown .dropdown-menu:before  {

    left: 27px;
  }
  .<?= $kunik?> .searchBar-filters .main-search-bar-addon i{
    font-size: 20px
  }
  .<?= $kunik?> .btn-group .btnHeader {
    font-size: 25px;
    width: 100%;
    height: 50px;
    border-radius: 20px;
    margin-top: 4px;
    background: #f5833c;
    color: white;
  }

  .<?= $kunik?> .main-search-bar-addon {
    width: 15%;
    height: 40px;
    background-color: #f5833c!important;
    font-size: 14px;
    border: 1px solid #f5833c;
    padding: 12px 7px;
  }
  .<?= $kunik?> .searchBar-filters .main-search-bar-addon i{
    font-size: 20px
  }
  .<?= $kunik?> .searchBar-filters .nameActeur{
    height: 50px;
    margin-top: 5px;
    border-radius: 20px 0px 0px 20px;
    box-shadow: none;  
    width: 400px;
    max-width: 600px;
    border-color: #f5833c;
  }
  .<?= $kunik?> .searchBar-filters .motClef{
    height: 50px;
    margin-top: 5px;
    border-radius: 20px 20px 20px 20px;
    box-shadow: none;  
    width: 300px;
    max-width: 450px;
    border-color: #f5833c;
  }
  .<?= $kunik?> .btn-group .dropdown-item {
   width: 100%;
   text-align: left;
   font-size: 18px;

  }
  .<?= $kunik?> #activeFilters .filters-activate{
    background-color: #327753;
  }

  @media (max-width: 978px) {
    .<?= $kunik?> iframe{
      width: 100%;
      height: 24vh;
    }
    .addActeur<?= $kunik?> {
      padding: 6px;
      font-size: 14px;
    }
  }

  .<?= $kunik?>  #input-sec-search  {
    display: grid;
    margin-bottom: 5px;
    float: left;

  }
  .<?= $kunik?> #input-sec-search .shadow-input-header{
    margin-top: 5px;
  }
  .<?= $kunik?> .input-global-search{
    height: 40px;
    border-radius: 0px 20px 20px 0px !important;
    box-shadow: none;
    width: 180px;
    max-width: 700px;
    border-color: #f5833c;
  }
  .<?= $kunik?> #input-sec-search .input-group-addon {
    color: white;
    width: 15%;
    height: 40px;
    background-color: #f5833c!important;
    border: 1px solid #f5833c;
    padding: 7px 2px;
    border-radius: 20px 0px 0px 20px;
  }
  .<?= $kunik?> #input-sec-search .input-group-addon i{
    font-size: 20px;
  }
  @media (max-width: 767px){
    .<?= $kunik?>{
      margin-top: 0;
    }
    .<?= $kunik?> .searchBar-filters {
      display: none;
    }
    .<?= $kunik?>  .dropdown{
      width: 100% !important;
    }
    .<?= $kunik?> .searchBarInMenu #main-search-bar{
      border: 1px solid #f5833c !important;
    }
    .<?= $kunik?> .searchBarInMenu #main-search-bar-addon {
      padding: 10px 10px;
      font-size: 14px;
      height: 40px;
      background-color: #f5833c !important;
      border: 1px solid #f5833c !important;
    }
    .<?= $kunik?> .searchBarInMenu #main-search-bar-addon i{
      font-size: 20px;
    }
    .<?= $kunik?> #show-filters-xs{
      border: 1px solid #f5833c !important;
      background-color: #e2e2e2;
    }
    .<?= $kunik?> #filterActor<?= $kunik?> .dropdown .btn-menu  {
      border-radius: 0;
    }
    .<?= $kunik?> .input-global-search{
      height: 40px;
      border-radius: 0px !important;
      width: 288px;
      max-width: 700px;
      border-color: #f5833c;
    }
    .<?= $kunik?> #input-sec-search .input-group-addon {
      color: white;
      width: 15%;
      height: 40px;
      background-color: #f5833c!important;
      border: 1px solid #f5833c;
      padding: 7px 2px;
      border-radius: 0;
    }
  }
  .edit<?= $kunik?> .material-button{
    border-radius: 0 0 50% 0 !important;
  }
  .edit<?= $kunik?>{
    position: absolute;
    display: none;
    top: 6px;
    left: 6px;
    z-index: 9999;
  }
  .ficheActeur_<?= $kunik?>:hover .edit<?= $kunik?>{
    display: block;
  }
</style>

<?php
$acteurs = PHDB::find(Organization::COLLECTION, ["source.key" => $costum["contextSlug"], "tags" => $paramsData["tags"]]);
$allTags = array();
foreach ($acteurs as $keya => $valuea) {
  foreach ($valuea["tags"] as $k => $v) {
    $allTags[] = $v;
  }

  ?>
  <style type="text/css">
    .ficheActeur_<?=$kunik?>{
      margin-top: 20px;
      padding: 30px;
      margin-bottom: 20px;
      position: relative;
      background: #e2e2e2;
    }

    .profil_<?=$kunik?> img{
      width:100%;
    }
    .ficheActeur_<?=$kunik?> p{
      margin-top: 30px;
      font-size: 17px;
      text-align: left;
    }
    .profil_<?=$kunik?> .primary-btn{
      display: inline-block;
      font-size: 13px;
      padding: 8px 25px 6px;
      color: #fff;
      text-transform: uppercase;
      font-weight: 700;
      border-radius: 20px;
      border: 1px solid #f5833c;
      margin-bottom: 20px;
      background: #f5833c;
    }
    .profil_<?=$kunik?> .primary-btn:hover{
      display: inline-block;
      font-size: 14px;
      padding: 8px 25px 6px;
      color: #327753;
      text-transform: uppercase;
      font-weight: 700;
      border: 1px solid #327753;
      background: #fff;
      margin-bottom: 20px;
      text-align: left;
    }
    .acteurVideo_<?=$kunik?> iframe{
      width:100%;
      height : 400px;
      margin-bottom: 15px;
    }
    .hide-bullets_<?=$valuea["_id"]?> {
      list-style:none;
      margin-left: -40px;
      margin-top:20px;
    }

    .thumbnail_<?=$kunik?> {
      display: block;
      padding: 0;
      margin-bottom: 20px;
      line-height: 1.42857143;
      background-color: #fff;
      border: 1px solid #ddd;
      border-radius: 4px;
      -webkit-transition: all .2s ease-in-out;
      -o-transition: all .2s ease-in-out;
      transition: all .2s ease-in-out;

    }
    .thumbnail_<?=$kunik?> img{
      width: 100%;
      height: 50px;
      object-fit:cover;
    }
    .carousel-inner>.item>img, .carousel-inner>.item>a>img {
      width: 100%;
      height: 400px;
      object-fit:contain;
    }

    .follow_<?=$kunik?>  {
      display: inline-block;
      cursor: default;
      padding: 0;
      margin: 0 .5em;
      position: relative;
      text-align: center;
    }
    .follow_<?=$kunik?>:hover .label_<?=$kunik?>{
      opacity: 0;
      transition: opacity .5s .125s ease-out;
    }

    .follow_<?=$kunik?>:hover .icon_<?=$kunik?> {
      border-radius: 1em;
      margin: 0 0;
    }


    .icon_<?=$kunik?>, .label_<?=$kunik?> {
      background-color: #f5833c;
      color: #fff;
      line-height: 2rem;
    }
    .label_<?=$kunik?> {
      display: inline-block;
      font-size: 14px;
      padding: 8px 25px 6px;
      text-transform: uppercase;
      font-weight: 700;
      border-radius: 20px;
      margin-bottom: 20px;
      position: absolute;
      letter-spacing: .0816em;
      top: 0;
      left: -10px;
      right: 0;
      opacity: 1;
      pointer-events: none;
      transition: opacity .5s .75s ease-out;
      min-width: 150px;
    }

    .icon_<?=$kunik?> {
      border-radius: 0;
      cursor: pointer;
      display: inline-block;
      height: 2em;
      margin: 0 -.5em;
      transition:
      background-color .5s ease-out,
      border-radius .5s .25s ease-out,
      margin .5s .25s ease-out;
      width: 2em;
    }
    .icon_<?=$kunik?>.first {
      border-bottom-left-radius: 1em;
      border-top-left-radius: 1em;
      margin-left: 0;
    }

    .icon_<?=$kunik?>.last {
      border-bottom-right-radius: 1em;
      border-top-right-radius: 1em;
      margin-right: 0;
    }

    .icon_<?=$kunik?>:hover {
      background-color: #fff;
      color: #327753;
      border: 1px solid #327753;

    }
    .container_<?=$kunik?> {
      display: inline-block;
      color: #ccc;
      padding: 1em;
      top:15px;
      left: 0;
      text-align: right;
      transform: translateY(-50%);
      width: 10%;
      white-space: nowrap;;

    }
    .anim-left_<?=$kunik?>{
      position: absolute;
      top: 0px;
      left: 0px;
      width: 6px;
      height: 30%;
      background-color: green;
      transition: all 1s ease-in-out 0s;
      -webkit-transition: all 1s ease-in-out 0s;
      -moz-transition: all 1s ease-in-out 0s;
    }
    .anim-top_<?=$kunik?>{
      position: absolute;
      top: 0px;
      left: 0px;
      width: 50%;
      height: 6px;
      background-color: green;
      transition: all 1s ease-in-out 0s ;
      -webkit-transition: all 1s ease-in-out 0s;
      -moz-transition: all 1s ease-in-out 0s;
    }
    .anim-rigth_<?=$kunik?>{
      position: absolute;
      bottom:  0px;
      right:  0px;
      width: 6px;
      height: 30%;
      background-color: green;
      transition: all 1s ease-in-out 0s;
      -webkit-transition: all 1s ease-in-out 0s;
      -moz-transition: all 1s ease-in-out 0s;
    }
    .anim-bottom_<?=$kunik?>{
      position: absolute;
      bottom:  0px;
      right:  0px;
      width: 50%;
      height: 6px;
      background-color: green;
      transition: all 1s ease-in-out 0s ;
      -webkit-transition: all 1s ease-in-out 0s;
      -moz-transition: all 1s ease-in-out 0s;
    }
    .ficheActeur_<?=$kunik?>:hover .anim-rigth_<?=$kunik?> {
      transition: all 1s ease 0s;
      height: 100%
    }
    .ficheActeur_<?=$kunik?>:hover .anim-bottom_<?=$kunik?> {
      transition: all 1s ease 0s;
      width: 100%
    }
    .ficheActeur_<?=$kunik?>:hover .anim-left_<?=$kunik?> {
      transition: all 1s ease 0s;
      height: 100%
    }
    .ficheActeur_<?=$kunik?>:hover .anim-top_<?=$kunik?> {
      transition: all 1s ease 0s;
      width: 100%
    }

    @media (max-width: 414px) {
      .acteurVideo_<?=$kunik?> iframe{
        width:100%;
        height : 200px;
        margin-bottom: 10px;
      }
      .acteurVideo_<?=$kunik?> p{
        font-size: 13px;
        line-height: 15px;
      }

      .thumbnail_<?=$kunik?> img{
        width: 100%;
        height: 20px;
      }
      .carousel-inner>.item>img, .carousel-inner>.item>a>img {
        width: 100%;
        height: 200px;
      }
      .hide-bullets_<?=$valuea["_id"]?> {
        list-style:none;
        margin-left: -10px;
        margin-top:10px;
      }
      .hide-bullets_<?=$valuea["_id"]?> li {
        padding-left: 2px;
        padding-right: 2px;
      }
      .thumbnail_<?=$kunik?> {
        margin-bottom: 5px;
      }
      #carousel-bounding-box{
        padding-left: 2px;
        padding-right: 2px;
        margin-left: -25px;
      }
      .profil_<?=$kunik?> p {
        font-size: 13px;
        line-height: 15px;
      }
      .profil_<?=$kunik?> .primary-btn {
        font-size: 10px;
      }
      .label_<?=$kunik?> {
        display: none;
      }
      .container_<?=$kunik?> {
        top: 25px;
      }
      .icon_<?=$kunik?>{
        border-radius: 1em;
        margin-left: 10px;
      }
    }
    @media screen and (min-width: 1500px){
      .ficheActeur_<?=$kunik?> p{
        margin-top: 10px;
        font-size: 20px;
        text-align: left;
      }
      .label_<?=$kunik?> {
        font-size: 17px;
        padding: 13px 15px 8px;
      }
      .profil_<?=$kunik?> .primary-btn{
        font-size: 16px;
      }
    }
    #mapContainer<?= $kunik?> {
      width: 100%;
      background-color: black;
      height: 500px;
    }
    #mapWithLegende<?= $kunik?>{
      margin-top: 2%;
      position: relative;
      height: auto;
    }
    .legende-container<?= $kunik?>{
      position: absolute;
      left: 0;
      bottom: 0;
      display: inline-block;
      z-index: 2;
      margin: 2%;
      border-radius: 10px;
      background: #e2e2e2;
    }
    .legende-container<?= $kunik?> .legend-body<?= $kunik?> {
      display: flex;
      text-align: left;
      padding: 10px;
    }
    .legende-container<?= $kunik?> ul {
      margin-left: -34px;
    }
    .legende-container<?= $kunik?> ul li{
      list-style: none;
      font-size: 15px;
      font-weight: bold;
    }
    .legende-container<?= $kunik?> ul  li i{
      padding: 4px;
      font-size: 20px;
    }
    .legende-container<?= $kunik?> p{
      font-size: 20px;
      font-weight: bold;
      text-align: center;
      color: #f5833c;
    }
    .legende-container<?= $kunik?> hr{
      margin-bottom: 0px;
      margin-top: 0px;
    }

    @media (max-width: 767px) {
      .legende-container<?= $kunik?> {
        position: absolute;
        bottom: 7%;
        left: 5%;
        z-index: 2;
        border-radius: 10px;
        background: #e2e2e2;
      }
      .legende-container<?= $kunik?> ul li {
        list-style: none;
        font-size: 11px;
        font-weight: bold;
      }
      .legende-container<?= $kunik?> ul li i {
        padding: 4px;
        font-size: 13px;
      }
      .legende-container<?= $kunik?> p {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        color: #f5833c;
        margin-bottom: 0px;
      }
    }
    @media (max-width: 1500px) {
     .legende-container<?= $kunik?> p {
      font-size: 20px;
      font-weight: bold;
      text-align: center;
      color: #f5833c;
      margin-bottom: 0px;
    }
}

</style>

<?php }?>
<div class=" <?= $kunik?>">

  <div id="filterActor<?= $kunik?>"></div>
  <div id="mapWithLegende<?= $kunik?>">
    <div class="legende-container<?= $kunik?>">
      <div class="legend-header active">
        <p>Legende <i class="fa fa-angle-down"></i></p>
      </div>
      <hr>
      <div class="legend-body<?= $kunik?>">
        <div id="legende<?= $kunik?>" >
          <div class="legende-content<?= $kunik?>"></div>
        </div>
      </div>
    </div>
    <div id="mapContainer<?= $kunik?>"></div>
  </div>
  <div id="actorResult"></div>
</div>
<script type="text/javascript">
  var WIND_COLORS =[
    {
      name: "Agriculteurs, Maraîchers",
      bgColor:"green",
      bgColorShade:"red",
      contentColor:"light"
    },
    {
      name: "Pêcheurs",
      bgColor:"blue",
      bgColorShade:"red",
      contentColor:"light"
    },
    {
      name: "Artisans",
      bgColor:"yellow",
      bgColorShade:"red",
      contentColor:"light"
    },
    {
      name: "Artistes",
      bgColor:"orange",
      bgColorShade:"red",
      contentColor:"light"
    },
    {
      name: "Bien-être",
      bgColor:"purple",
      bgColorShade:"red",
      contentColor:"light"
    },
    {
      name: "Associations, Tiers Lieux",
      bgColor:"red",
      bgColorShade:"red",
      contentColor:"light"
    }
  ];
  /*Supprimer un acteur */

  var actorObj = {
    /* Ajouter nouvel acteur*/
    addActor<?= $kunik?>:function(){
      var dyfOrga={
        "beforeBuild":{
          "properties" : {
            "name" : {
              "label" : "Nom de l'acteur",
              "placeholder" : "Nom de l'acteur",
              "order" : 1
            },
            "category" : {
              "placeholder" : "Activités",
              "inputType" : "select",
              "label" : "Activités de l'acteur",
              "order" : 2,
              "options" : {
                "Associations" : "Associations",
                "Artistes" : "Artistes",
                "Agriculteurs" : "Agriculteurs",
                "Maraîchers" : "Maraîchers",
                "Pêcheurs" : "Pêcheurs",
                "Bien-être" : "Bien-être",
                "Tiers Lieux" : "Tiers Lieux",
                "Artisans" :"Artisans"
              },
              "rules":{
                "required":true
              }     
            },
            "tags" : {
              "label" : "Mot Clef ",
              "inputType" : "tags",
              "value" :["<?= $paramsData["tags"]?>"],
              "rules" : {
                "required" : true
              },
              "order" : 3
            },
            "shortDescription":{
              "rules" : {
                "required" : true
              },
              "order" : 4
            },
            "description":{
              "label" : "Description de l'acteur",
              "rules" : {
                "required" : true
              },
              "order" : 5
            },
            "formLocality" : {
              "label" : "Adresse de l'acteur",
              "rules" : {
                "required" : true
              }
            },
            "image":{
              "rules" : {
                "required" : true
              },
              "order" : 7
            }
          }
        },
        "onload" : {
          "actions" : {
            "setTitle" : "Ajouter une acteur",
            "src" : {
              "infocustom" : "Remplir le champ"
            }
          },
          "hide" : {
            "typeselect" : 1,
            "roleselect":1
          } 
        }
      };
      dyfOrga.afterSave = function(data){
        dyFObj.commonAfterSave(data, function(){
          mylog.log("dataaaa", data);
          urlCtrl.loadByHash(location.hash);
        });
      }
      dyFObj.openForm('organization',null, null,null,dyfOrga);
    },

    /* Enregistrer l'activité de l'acteur dans le tags*/
    saveTags<?= $kunik?>:function(idOrga,category,tags){
      tags.push(category) ;
      mylog.log("tagsss",tags);
      var tplCtx = {};
      tplCtx.id = idOrga;
      tplCtx.collection = "organizations";
      tplCtx.path = "tags";
      tplCtx.value = tags;
      mylog.log("tplCtx0000", tplCtx);
      dataHelper.path2Value( tplCtx, function(params) {            
       toastr.success("tags enregistrer");          

     })
    },

    /** affichage fiche acteur*/
    ficheActeur<?= $kunik?>:function (params){
      mylog.log("praaaaa",params._id.$id );

      profil = (typeof params.profilMediumImageUrl != "undefined" || params.profilMediumImageUrl != "null") ? params.profilMediumImageUrl : "";
      shortDescription =(typeof params.shortDescription != "undefined" || params.shortDescription != "null") ? params.shortDescription : "";
      description = (typeof params.description != "undefined" || params.description != "null") ? params.description : "";
      socialNetwork = (typeof params.socialNetwork != "undefined" || params.socialNetwork != "null") ? params.socialNetwork : "";
      var str ='';

      str +=
      "<div class='ficheActeur_<?=$kunik?> '>"+
      "<div class='anim-left_<?=$kunik?>'></div>"+
      "<div class='anim-top_<?=$kunik?>'></div>"+
      "<div class='anim-rigth_<?=$kunik?>'></div>"+
      "<div class='anim-bottom_<?=$kunik?>'></div>";

      <?php if(Authorisation::isInterfaceAdmin()){ ?>
        str +='<div class = "edit<?= $kunik?>">'+
        '<a href="javascript:;" onclick="actorObj.deleteActor<?= $kunik?>('+'\''+params._id.$id+'\''+')" class="btn material-button bg-red  "> <i class="fa fa-trash"></i>'+
        '</a>'+
        '</div>';
      <?php }?>

      str += "<div class='text-center title-2 name_<?=$kunik?>''>"+
      params.name +
      "</div>"+
      "<div class='col-lg-8 col-md-8'>"+
      "<div class='acteurVideo_<?=$kunik?>'>"+
      "<p>"+shortDescription + "</p>"+
      "<div id='resultVideo"+params._id.$id+"'>"+actorObj.videoActeur<?= $kunik?>(params._id.$id) +"</div>"+
      
      "</div>"+
      "</div>"+
      "<div class='col-lg-4 col-md-4 profil_<?=$kunik?>'>"+
      "<img src='"+profil +"'>"+
      "<p>"+description+"</p>"+
      "<a href='#@"+ params.slug +"' class=' col-lg-6 primary-btn' target='_blank'>En savoir plus</a>"+
      "<div class='container_<?=$kunik?> col-lg-6'>"+
      "<div class='follow_<?=$kunik?>'>";
      if (typeof socialNetwork != "undefined" && typeof socialNetwork.facebook != "undefined") {
        str+=
        "<a href='"+socialNetwork.facebook +"' target='_blank'>"+
        "<i class='icon_<?=$kunik?> first fa fa-facebook' ></i>"+
        "</a>";
      }
      if (typeof socialNetwork != "undefined" && typeof socialNetwork.twitter != "undefined") {
        str+=
        "<a href='"+socialNetwork.twitter +"' target='_blank'>"+
        "<i class='icon_<?=$kunik?> first fa fa-twitter' ></i>"+
        "</a>";
      }
      if (typeof socialNetwork != "undefined" && typeof socialNetwork.youtube != "undefined") {
        str+=
        "<a href='"+socialNetwork.youtube +"' target='_blank'>"+
        "<i class='icon_<?=$kunik?> first fa fa-youtube-play' ></i>"+
        "</a>";
      }
      if (typeof socialNetwork != "undefined" && typeof socialNetwork.instagram != "undefined") {
        str+=
        "<a href='"+socialNetwork.instagram +"' target='_blank'>"+
        "<i class='icon_<?=$kunik?> first fa fa-instagram' ></i>"+
        "</a>";
      }
      if (typeof params.email != "undefined") {
        str+=
        "<a href='mailto:"+params.email +"' target='_blank'>"+
        "<i class='icon_<?=$kunik?> first fa fa-envelope-o' ></i>"+
        "</a>";
      }
      str +=
      "<div class='label_<?=$kunik?>'>Suivez-nous </div>"+
      "</div>"+
      "</div>"+
      "</div>"+
      "<div id='resultImag"+params._id.$id+"'>"+
      +actorObj.imageExpo<?= $kunik?>(params._id.$id)+
      "</div>"+
      "</div>"+
      "</div>";
      return str;
    },
    /* Afficher le poi type video de chaque acteur*/
    videoActeur<?= $kunik?>:function(acteurId){
      var params = {
        "type" : "video",        
        "searchType" : ["poi"],
        fields : ["urls","parent"]          
      };
      var strVideo = '';
      ajaxPost(
        null,
        baseUrl+"/" + moduleId + "/search/globalautocomplete",
        params,
        function(data){
          mylog.log("video",data.results);            
          actorObj.filtersByIdParent<?= $kunik?>(data.results,acteurId);
        }
        )
    },
    /* Récuperer le poi de même parent*/
    filtersByIdParent<?= $kunik?>:function(data,acteurId){
      var videoObj = [];
      var strVideo ='';
      $.each(data, function(key, value){         
        if (Object.keys(value.parent)[0] == acteurId) {
          videoObj.push(value);
        }
      });
      mylog.log("videoObj",videoObj);
      $.each(videoObj, function(keyv, valuev){ 
        var url = valuev.urls[0];
        var youtubeId = ((url.split("/")).reverse())[0];
        strVideo+=
        "<iframe  src='https://www.youtube.com/embed/"+ youtubeId+"'></iframe>";
        $("#resultVideo"+acteurId).html(strVideo);
      })

      return strVideo;
    },
    /* Imange  expostion de chaque acteur*/
    imageExpo<?= $kunik?>:function(acteurId){
      $('#myCarousel_'+acteurId).carousel({
        interval: 5000
      });
      $('[id^=carousel-selector-]').click(function () {
        var id_selector = $(this).attr("id");
        try {
          var id = /-(\d+)$/.exec(id_selector)[1];
          mylog.log(id_selector, id);
          jQuery('#myCarousel_'+acteurId).carousel(parseInt(id));
        } catch (e) {
          console.log('Regex failed!', e);
        }
      });
      $('#myCarousel_'+acteurId).on('slid.bs.carousel', function (e) {
        var id = $('.item.active').data('slide-number');
        $('#carousel-text').html($('#slide-content-'+id).html());
      });
      params = {
        "acteurId" :acteurId,
        "nameAlbum" : "<?= $paramsData["nameAlbum"]?>"
      };
      var strImage = '';
      ajaxPost( 
        null,
        baseUrl+"/costum/assokosasa/getimagesaction",
        params,
        function(data){
          mylog.log("nomActeur",data);
          strImage+=
          "<div class='row'>"+
          "<div class='col-sm-8 col-xs-6'>"+
          "<div class='col-xs-12' id='slider'>"+
          "<div class='row'>"+
          "<div class='col-sm-12' id='carousel-bounding-box'>"+
          "<div class='carousel slide' id='myCarousel_"+acteurId+"'>"+
          "<div class='carousel-inner'>";
          i = 0;
          $.each(data, function( index, valueImage ) {
            mylog.log("valueImage", valueImage);
            if (i==0) {
              strImage+=
              "<div class='active item' data-slide-number='"+i+"'>"+
              "<img src='"+valueImage.docPath+ " '>"+
              "</div>";
            } else {
              strImage+=
              "<div class='item' data-slide-number='"+i+"'>"+
              "<img src='"+valueImage.docPath+ " '>"+
              "</div>";
            }
            i += 1;
          });
          strImage+=
          "</div>"+
          "<a class='left carousel-control' href='#myCarousel_"+acteurId+"' role='button' data-slide='prev'>"+
          "<span class='glyphicon glyphicon-chevron-left'></span>"+
          "</a>"+
          "<a class='right carousel-control' href='#myCarousel_"+acteurId+"' role='button' data-slide='next'>"+
          "<span class='glyphicon glyphicon-chevron-right'></span>"+
          "</a>"+
          "</div>"+
          "</div>"+
          "</div>"+
          "</div>"+
          "</div>"+
          "<div class='col-sm-4 col-xs-6' id='slider-thumbs_"+acteurId+"'>"+
          "<ul class='hide-bullets_"+acteurId+"'>";
          j = 0;
          $.each(data, function( index, valueImages ) {
            strImage+=
            "<li class='col-sm-3 col-xs-4'>"+
            "<a class='thumbnail_<?=$kunik?>' id='carousel-selector-"+j+"'>"+
            "<img src='"+valueImages.docPath+"'> "+
            "</a>"+
            "</li>";
            j += 1;
          });
          strImage+=
          "</ul>"+
          "</div>"+
          "</div>";
          $("#resultImag"+acteurId).html(strImage);
        }
        )
    },
    mapLegende<?= $kunik?>: function(){
      var legende<?= $kunik?> = $('<ul></ul>');
      WIND_COLORS.reverse().map(color => {
        legende<?= $kunik?>.append(`
          <li>
          <i class="fa fa-map-marker" style="color:${color.bgColor};"></i>
          ${ color.name}
          </li>
          `)
      })
      $('#legende<?= $kunik?> .legende-content<?= $kunik?>').html(legende<?= $kunik?>);
    },
    deleteActor<?= $kunik?>: function(id){ 
      var type = "organizations"
      var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
      bootbox.confirm("Etes-vous sûre de vouloir supprimer cette acteur ?",
        function(result){
          if (!result) {
            return;
          } 
          else {
            $.ajax({
              type: "POST",
              url: urlToSend,
              dataType : "json"
            })
            .done(function (data) {
              if ( data && data.result ) {
                toastr.success("Element effacé");
                $("#"+type+id).remove();
              } else {
               toastr.error("something went wrong!! please try again.");
             }
             urlCtrl.loadByHash(location.hash);
           });
          }
        }
      );
    }
  };
  var activity<?= $kunik?> = [
    "Associations",
    "Artistes",
    "Agriculteurs",
    "Maraîchers",
    "Pêcheurs",
    "Bien-être",
    "Tiers Lieux",
    "Artisans"
  ];
  var tags = <?= json_encode(array_unique($allTags))?> ;
  /* params initilisation map */
  var map<?= $kunik?> = new CoMap({
    container : "#mapContainer<?= $kunik?>",
    activePopUp : true,
    elts : {},
    mapOpt:{
      scrollWheelZoom: false
		},
    mapCustom:{
      markers: {
        getMarker : function(data){
          mylog.log("dataaaaa", data.category);
          var imgM = modules.map.assets + '/images/markers/citizen-marker-default.png';
          if (typeof data.category != "undefined"){
            if ( data.category =="Artistes")
              imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_artiste.png";
            else if ( data.category == "Agriculteurs" || data.category == "Maraîchers")
              imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_agri.png";
            else if ( data.category == "Pêcheurs")
              imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_peche.png";
            else if(data.category == "Artisans")
              imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_artiasnt.png";
            else if (data.category =="Bien-être")
              imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_bienetre.png";
            else if  (data.category =="Associations" ||data.category =="Tiers Lieux" )
              imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/marker/marker_association.png";
          }
          return imgM;
        }
      }
    } 
  });
  /* Params d'initilisation de searchObj*/
  var paramsFilter = {
    container : "#filterActor<?= $kunik?>",
    options : {
      tags : {
        verb : '$all'
      }
    },
    mapCo : map<?= $kunik?>,
    loadEvent : {
      default : "scroll"
    },
    results :{
      dom : '#actorResult',
      renderView : "actorObj.ficheActeur<?= $kunik?>",
      map : {
        active : false,
        sameAsDirectory:true,
        showMapWithDirectory:true
      }
    },
    defaults : {
      fields : ["socialNetwork","email","geo","address","category","tags"],
      types : ["organizations"],
       forced :{
        filters : {
          tags : ["<?= $paramsData["tags"]?>","acteurskosasa"]
        }
       },

      filters : {
           
      }
    },
    filters : {
      //text:true,
      text:{
        placeholder :"Nom"
      },
      scope :true,
      typePlace : {
        view : "dropdownList",
        type : "category",
        name : "Activités",
        event : "filters",
        list : activity<?= $kunik?>
      },
      tags:{
        view : "dropdownList",
        type : "tags",
        name : "Mot clef",
        event : "tags",
        list : tags
      }

    }
  };
  var filterSearch={};
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

  jQuery(document).ready(function() {
    actorObj.mapLegende<?= $kunik?>();
    filterSearch = searchObj.init(paramsFilter);
    filterSearch.search.init(filterSearch,0);
    mylog.log("filtersss", filterSearch);
    <?php if(Authorisation::isInterfaceAdmin()){ ?>
      $(".container-filters-menu").append("<a href='javascript:;' class='addActeur<?= $kunik ?> btn '><i class='fa fa-plus padding-right-5'></i>Ajouter un acteur</a>");
      $(".container-filters-menu").append("<a href='#@assoKosasa.view.forms.dir.observatory.6131b072293a975b174b7022' class=' addActeur<?= $kunik ?> lbh btn '><i class='fa fa-eye padding-right-5'></i>Liste de demande</a>");
    <?php } ?>
    $(".addActeur<?= $kunik ?>").click(function() {
      actorObj.addActor<?= $kunik?>();
    });
    
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer la section1",
        "description" : "Personnaliser votre section1",
        "icon" : "fa-cog",
        "properties" : {  
          "tags" : {
            "label" : "tags à afficher",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.tags
          },
          "nameAlbum" : {
            "label" : "nameAlbum à afficher",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.nameAlbum
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};

          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
  });
</script>