<?php 
    $cssJS = array(
        '/css/altFramadate/form.css',
        '/css/altFramadate/popModal.min.css',
        '/js/altFramadate/popModal.min.js',
    );
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    HtmlHelper::registerCssAndScriptsFiles($cssJS, $assetsUrl);

    $keyTpl ="textWithCoformButton";
    $paramsData = [
        "contentTitle"=>"Lorem ipsum dolor sit amet",
        "introduction"=>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo."
    ];

    $childForm = PHDB::find("forms", array("parent.".$this->costum['contextId']=>['$exists'=>true]));

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php 

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
    $image = [];

    foreach ($initImage as $key => $value) {
        $image[]= $value["imagePath"];
    }

    if($image!=[]){
        $paramsData["image"] = $image[0];
    }
    
?>

<style>
    select {
        font-family: 'FontAwesome';
        font-size: 20px;
        border: none;
        background-color: transparent;
    }
    select.selectChoice {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-align: center;
    }
    .selectChoice:focus {
        outline: none !important;
    }
    td {
        text-align: center;
        vertical-align: inherit !important;
    }
    .my-table {
        margin-bottom: 0px !important;
    }
    .my-th {
        min-width: 95px !important;
        text-align: center;
    }
    .my-table-wrapper {
        overflow: auto;
        height: 500px;
    }
    .my-table thead th {
        position: sticky;
        top: 0;
        z-index: 1;
        background: white;
    }
    .my-table thead tr th:first-child {
        position: sticky;
        left: 0;
        z-index: 2;
        background: white;
    }
    .my-table tfoot {
        position: sticky;
        bottom: 0;
        z-index: 1;
        background: white;
    }
    .my-table tfoot tr th:first-child {
        position: sticky;
        left: 0;
        z-index: 2;
        background: white;
    }
    .my-table tbody th {
        position: sticky;
        left: 0;
        background: white !important;
        z-index: 1;
    }
</style>

<section id="assoLibre" class="row">
    <div class="col-xs-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h2 class="title-1 padding-20 text-center">
            <?php echo $paramsData["contentTitle"]; ?>
        </h2>
        <div class="text-center col-xs-12 description markdown">
            <?php echo "\n".$paramsData["introduction"]; ?>
        </div>
        <div class="col-xs-12 no-padding row" id="formList<?php echo $kunik ?>">
        </div>
    </div>
</section>
<br>
<br>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    $(document).ready(function() {
        let allForm = <?php echo json_encode($childForm) ?>;
        let header = [], headerExtra = [], body = [], footer = [], rowToEdit = [], bodyRowForInsert = [];
        let userLogged = '', headerHtml = '', bodyHtml = '', bodyForReset = '', footerHtml = '';
        let userLoggedInfo = {
            id: '',
            name: ''
        }
        let formInfoObj = {
            formid: '',
            subformid: '',
            userid: '',
            id: '',
            index: 0,
            parentForm: ''
        };
        let resetValue = {
            body: [],
            headerExtra: [],
            footer: []
        };
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        });

        function generateTable(body, footer, userLogged) {
            let _bodyHtml = '', _footerHtml = '';
            let footerCopy = [].concat(footer);
            footerCopy.splice(0, 1);
            const indexMaxVoted = footerCopy.indexOf(Math.max(...footerCopy));
            body.forEach((item, index) => {
                item.forEach((elem, iter) => {
                    if(iter == 0) {
                        if(elem.userid == userLogged) {
                            _bodyHtml += `<tr data-toggle="popModal" data-index="${index}" data-id="${elem.id}" data-formid="${elem.formid}" data-subformid="${elem.subform}" data-user="${elem.userid}">`;
                        } else {
                            _bodyHtml += `<tr>`
                        }
                        _bodyHtml += `<th>${elem.name}</th>`;
                        _footerHtml = `<tfoot><tr><th class='my-th'>Total<br>${body.length} Votant${body.length > 1 ? 's' : ''}</th>`
                    } else if(iter == elem.length - 1) {
                        _bodyHtml += `</tr>`;
                        _footerHtml += '</tr></tfoot>'
                    } else {
                        if(elem) {
                            _bodyHtml += `<td><i class="fa fa-check"></i></td>`
                        } else {
                            _bodyHtml += `<td></td>`
                        }
                        _footerHtml += `<td class="my-th ${indexMaxVoted > -1 && indexMaxVoted == iter-1 ? 'bg-green' : '' }">${footerCopy[iter-1]}</td>`;
                    }
                });
            });
            return {body: _bodyHtml, footer: _footerHtml}
        }

        function generateRowFOrInsert(rowIndex) {
            bodyRowForInsert = [{
                userid: userLogged,
                name: userLoggedInfo.name,
                id: '',
                formid: formInfoObj.formid,
                subform: formInfoObj.subformid
            }];
            let _row =`
                    <tr>
                        <th> 
                            ${userLoggedInfo.name}</br>
                            <a href="javascript:;" class="btn btn-xs btn-success btnSaveRes" data-id="${bodyRowForInsert[0].id}" data-formid="${formInfoObj.formid}" data-subformid="${formInfoObj.subformid}" data-index="${body.length}" data-user="${userLoggedInfo.id}">
                                <i class="fa fa-save"></i>
                            </a>
                        </th>`;
            header.forEach((item, index) => {
                _row += `
                    <td>
                        <select class="selectChoice" data-i="${index}" data-row="${rowIndex}">
                            <option value="true">&#xf00c;</option>
                            <option value="false" selected>&#xf128;</option>
                        </select>
                    </td>
                `;
                bodyRowForInsert.push(false);
                if(index == header.length - 1) {
                    _row += '</tr>';
                }
            });
            return _row
        }

        function refreshView() {
            resetValue.body = JSON.parse(JSON.stringify(body));
            resetValue.headerExtra = JSON.parse(JSON.stringify(headerExtra));
            resetValue.footer = JSON.parse(JSON.stringify(footer));
            const table = generateTable(body, footer, userLogged);
            const rowNew = generateRowFOrInsert(body.length);
            bodyForReset = bodyHtml+rowNew;
            const blockRes = `
                <div class="my-table-wrapper">
                    <table class="table table-bordered table-hover my-table">
                        ${headerHtml}
                        <tbody>${table.body}${rowNew}</tbody>
                        ${table.footer}
                    </table>
                </div>
            `;
            footerHtml = table.footer;
            $('#formList<?php echo $kunik ?>').html(blockRes)
        }

        $("#formList<?php echo $kunik ?>").on("click", '[data-toggle="popModal"]', function() {
            let id = $(this).attr("data-id");
            let formId = $(this).attr("data-formid");
            let subFormId = $(this).attr("data-subformid");
            let index = $(this).attr("data-index");
            let userId = $(this).attr("data-user");
            $(this).popModal({
                html: $(
                    `<div id="popContent" class="row col-xs-12 popModal_content">
                        <div id="btnEdit" class="col-xs-5">
                            <a 
                                href="javascript:;" 
                                class="btn btn-xs btn-danger btnEditResponse"
                                data-id="${id}"
                                data-formid="${formId}"
                                data-subformid="${subFormId}"
                                data-index="${index}"
                                data-user="${userId}"
                            >
                                <i class="fa fa-pencil"></i>
                                Modifier
                            </a>
                        </div>
                        <div id="btnDelete" class="col-xs-5">
                                <a 
                                    href="javascript:;" 
                                    class="btn btn-xs btn-danger btnDeleteResponse"
                                    data-id="${id}"
                                    data-formid="${formId}"
                                    data-subformid="${subFormId}"
                                    data-index="${index}"
                                    data-user="${userId}"
                                >
                                <i class="fa fa-trash"></i>
                                Supprimer
                            </a>
                        </div>
                    </div>`
                    )
            })
        });

        $('#formList<?php echo $kunik ?>').on("click", ".btnEditResponse", function() {
            if(!bodyHtml.includes('<a data-edit')) {
                if(body.length > footer[0]) body.splice(body.length-1, 1);
                const rowNew = generateRowFOrInsert(body.length);
                bodyForReset = bodyHtml+rowNew;
                resetValue.body = JSON.parse(JSON.stringify(body));
                resetValue.headerExtra = JSON.parse(JSON.stringify(headerExtra));
                resetValue.footer = JSON.parse(JSON.stringify(footer))
            }
            bodyHtml = '';
            rowToEdit.length = 0;
            let dataIndex = $(this).attr("data-index");
            let userOnRow = $(this).attr("data-user");
            let id = $(this).attr("data-id");
            let formId = $(this).attr("data-formid");
            let subFormId = $(this).attr("data-subformid");
            body.forEach((item, index) => {
                item.forEach((elem, iter) => {
                    if(iter == 0) {
                        if(userOnRow == userLogged) {
                            bodyHtml += `
                                <tr 
                                    data-toggle="popModal" 
                                    data-index="${index}" 
                                    data-id="${id}" 
                                    data-formid="${formId}" 
                                    data-subformid="${subFormId}" 
                                    data-user="${userOnRow}"
                                >`;
                            if(index == dataIndex) {
                                bodyHtml += `
                                    <th>
                                        ${elem.name}</br>
                                        <a data-edit
                                            href="javascript:;" 
                                            class="btn btn-xs btn-success btnSaveEdit"
                                            data-id="${id}"
                                            data-formid="${formId}"
                                            data-subformid="${subFormId}"
                                            data-index="${dataIndex}"
                                            data-user="${userId}"
                                        >
                                            <i class="fa fa-save"></i>
                                        </a>
                                        <a 
                                            href="javascript:;" 
                                            class="btn btn-xs btn-danger btnCancelEdit"
                                        >
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </th>
                                `
                            } else {
                                bodyHtml += `<th>${elem.name}</th>`
                            }
                        } else {
                            bodyHtml += `<tr><th>${elem.name}</th>`
                        }
                    } else if(iter == elem.length - 1) {
                        bodyHtml += `</tr>`
                    } else {
                        if(index == dataIndex) {
                            if(elem) {
                                const id1Finded = headerExtra.findIndex(e => e.parentValue == header[iter - 1].parentValue && e.subValue == header[iter - 1].subValue && e.index == dataIndex);
                                bodyHtml += `
                                    <td>
                                        <select class="selectChoice" data-i="${iter - 1}" data-row="${index}" data-parentvalue="${header[iter-1].parentValue}" data-subvalue="${header[iter-1].subValue}" data-parentkey="${headerExtra[id1Finded].parentKey}">
                                            <option value="true" selected>&#xf00c;</option>
                                            <option value="false">&#xf128;</option>
                                        </select>
                                    </td>`;
                                const keyTemp = headerExtra[id1Finded].parentKey;
                                const idFinded = rowToEdit.findIndex(e => e.value[keyTemp] == header[iter-1].parentValue);
                                
                                if(idFinded > -1) {
                                    rowToEdit[idFinded].subValue.push({
                                        subValue: header[iter - 1].subValue,
                                        parent: headerExtra[id1Finded].parentKey
                                    });
                                } else {
                                    rowToEdit.push({
                                        value: {
                                            [keyTemp]: header[iter-1].parentValue
                                        },
                                        subValue: [
                                            {
                                                subValue: header[iter-1].subValue,
                                                parent: headerExtra[id1Finded].parentKey
                                            }
                                        ]
                                    })
                                }
                                
                            } else {
                                bodyHtml += `
                                    <td>
                                        <select class="selectChoice" data-i="${iter - 1}" data-row="${index}">
                                            <option value="true">&#xf00c;</option>
                                            <option value="false" selected>&#xf128;</option>
                                        </select>
                                    </td>
                                `
                            }
                        } else {
                            if(elem) {
                                bodyHtml += `<td><i class="fa fa-check"></i></td>`
                            } else {
                                bodyHtml += `<td></td>`
                            }
                        }
                    }
                });
                if(index == body.length - 1) {
                    const blockRes = `
                        <div class="my-table-wrapper">
                            <table class="table table-bordered table-hover my-table">
                                ${headerHtml}
                                <tbody>${bodyHtml}</tbody>
                                ${footerHtml}
                            </table>
                        </div>
                    `;
                    $('#formList<?php echo $kunik ?>').html(blockRes);
                }
            }) 
        });

        $("#formList<?php echo $kunik ?>").on("change", ".selectChoice", function() {
            const index = $(this).attr("data-i")*1;
            const rowIndex = $(this).attr("data-row")*1;
            const parentValue = $(this).attr("data-parentvalue");
            const subValue = $(this).attr("data-subvalue");
            const parentKey = $(this).attr("data-parentkey");
            const selectVal = $(this).val();
                const indexFinded = rowToEdit.findIndex(e => Object.values(e.value)[0] == header[index].parentValue);
                if(indexFinded > -1) {
                    // const indexSubValue = rowToEdit.findIndex(e => e.subValue.findIndex(elem => elem.subValue == header[index].subValue && elem.parent == Object.keys(e.value)[0]) > -1);
                    const indexSubValue = rowToEdit.findIndex(e => e.subValue.findIndex(elem => elem.subValue == header[index].subValue && e.value[elem.parent] == header[index].parentValue) > -1);
                    if(indexSubValue > -1) {
                        if(selectVal == 'false') {
                            const indexToRemove = rowToEdit[indexSubValue].subValue.findIndex(e => e.subValue == header[index].subValue);
                            const indexOfExtraToRemove = headerExtra.findIndex(e => e.parentValue == header[index].parentValue && e.subValue == header[index].subValue && e.parentKey == parentKey);
                            if(rowToEdit[indexSubValue].subValue.length > 1) {
                                rowToEdit[indexSubValue].subValue.splice(indexToRemove, 1)
                            } else {
                                rowToEdit.splice(indexSubValue, 1);
                            }
                            if(indexOfExtraToRemove > -1) headerExtra.splice(indexOfExtraToRemove, 1);

                            body[rowIndex][index+1] = false;
                            footer[index + 1]--;
                        } else {
                            footer[index +1]++;
                        }
                    } else {
                        if(selectVal == 'true') {
                            rowToEdit[indexFinded].subValue.push({
                                subValue: header[index].subValue,
                                parent: rowToEdit[indexFinded].subValue[0].parent
                            });
                            body[rowIndex][index + 1] = true;
                            headerExtra.push({
                                parentValue: header[index].parentValue,
                                subValue: header[index].subValue,
                                parent: rowToEdit[indexFinded].subValue[0].parent,
                                index: rowIndex
                            })
                            footer[index + 1]++
                        }
                    }
                } else {
                    if(selectVal == 'true') {
                        if(body[rowIndex]) {
                            body[rowIndex][index + 1] = true
                        } else {
                            body.push(bodyRowForInsert);
                            body[rowIndex][index + 1] = true
                        }
                        headerExtra.push({
                            parentValue: header[index].parentValue,
                            subValue: header[index].subValue,
                            parent: 'date_'+rowToEdit.length,
                            index: rowIndex
                        })
                        footer[index + 1]++;
                        rowToEdit.push({
                            value: {
                                ['date_'+rowToEdit.length] : header[index].parentValue
                            },
                            subValue: [{
                                subValue: header[index].subValue,
                                parent: 'date_'+rowToEdit.length
                            }]
                        })
                    }
                }
        })

        $("#formList<?php echo $kunik ?>").on("click", ".btnSaveEdit", function() {
            let resValue = [], resSubValue = [], resValueJson;
            rowToEdit.forEach((item, index) => {
                resValue.push(item.value);
                resSubValue = resSubValue.concat(item.subValue);
                if(index == rowToEdit.length - 1) {
                    resValueJson = resValue.reduce((obj, item) => (obj[Object.keys(item)[0]] = Object.values(item)[0], obj), {});
                    let answerToSend = {
                        collection : "answers",
                        id : $(this).attr("data-id"),
                        path : `answers.${$(this).attr('data-formid')}.${$(this).attr("data-subformid")}`,
                        value: {
                            value : resValueJson,
                            subValue: resSubValue
                        }
                    };
                    dataHelper.path2Value( answerToSend , function(params) {
                        toastr.success('Reponse modifiee avec succes');
                        refreshView()
                    } );
                }
            });
            
        })

        $("#formList<?php echo $kunik ?>").on("click", ".btnCancelEdit", function() {
            bodyHtml = bodyForReset;
            body = JSON.parse(JSON.stringify(resetValue.body));
            headerExtra = JSON.parse(JSON.stringify(resetValue.headerExtra));
            footer = JSON.parse(JSON.stringify(resetValue.footer));
            const table = generateTable(body, footer, userLogged);
            const rowNew = generateRowFOrInsert(body.length);
            const blockRes = `
                <div class="my-table-wrapper">
                    <table class="table table-bordered table-hover my-table">
                        ${headerHtml}
                        <tbody>${table.body}${rowNew}</tbody>
                        ${table.footer}
                    </table>
                </div>
            `;
            $('#formList<?php echo $kunik ?>').html(blockRes);
        })

        $("#formList<?php echo $kunik ?>").on("click", ".btnSaveRes", function() {
            let resValue = [], resSubValue = [], resValueJson;
            rowToEdit.forEach((item, index) => {
                resValue.push(item.value);
                resSubValue = resSubValue.concat(item.subValue);
                if(index == rowToEdit.length - 1) {
                    resValueJson = resValue.reduce((obj, item) => (obj[Object.keys(item)[0]] = Object.values(item)[0], obj), {});
                    let answerToSend = {
                        collection : "answers",
                        id : '',
                        path : `answers.${$(this).attr('data-formid')}.${$(this).attr("data-subformid")}`,
                        value: {
                            value : resValueJson,
                            subValue: resSubValue
                        }
                    };
                    ajaxPost(
                        null,
                        `${baseUrl}/survey/answer/newanswer/form/${formInfoObj.parentForm}/contextId/<?= $this->costum["contextId"]; ?>/contextType/<?= $this->costum['contextType'] ?>`,
                        {action: ''},
                        function(data){
                            if(data) {
                                answerToSend.id = data._id.$id;
                                dataHelper.path2Value( answerToSend , function(params) {
                                    toastr.success('Reponse ajoute avec succes');
                                    refreshView()
                                } );
                            }
                        });
                }
            });
            
        })

        $("#formList<?php echo $kunik ?>").on("click", ".btnDeleteResponse", function() {
            let answerToSend = {
                collection : "answers",
                id : $(this).attr("data-id"),
                path : `answers.${$(this).attr('data-formid')}.${$(this).attr("data-subformid")}`,
                value: '{}'
            };
            const dataIndex = $(this).attr("data-index");
            dataHelper.path2Value( answerToSend , function(params) {
                toastr.success('Reponse supprimee avec succes');
                footer[0]--;
                body[dataIndex].forEach((item, iter) => {
                    item == true ? footer[iter]-- : ''
                });
                body.splice(dataIndex,1)
                refreshView()
            } );
        })

        let urlLogged = `${baseUrl}/co2/person/logged`;
        $.ajax({
            url: urlLogged,
            type: 'POST',
            data: {
                costumSlug: "<?php echo $this->costum['slug'] ?>",
                costumId: "<?php echo $this->costum['contextId'] ?>",
                costumType: "<?php echo $this->costum['contextType'] ?>"
            },
            success: function(res) {
                if(res.userId) {
                    userLogged = res.userId;
                    userLoggedInfo.id = res.userId;
                    <?php
                        $userIdSession = '';
                        $userInfoObj = new stdClass;
                        if(isset(Yii::app()->session["userId"])){
                            $userIdSession = Yii::app()->session["userId"];
                            $userInfoObj = PHDB::find(Person::COLLECTION, array('_id' => new MongoId($userIdSession)), array("name", "username", "slug"));
                        }
                    ?>
                    userLoggedInfo.name = <?php echo json_encode($userInfoObj[$userIdSession]['name']) ?>
                }
            },
            error: function(err) {
                conole.error(err)
            }
        })
        
        if(Object.keys(allForm).length > 0) {
            let blockForm = '';
            for(let [index, [key, value]] of Object.entries(Object.entries(allForm))) {
                blockForm += `
                    <div class="col-md-6 col-xs-12 panel-form" data-num="${index}">
                        <section class="widget">
                            <div class="widget-body">
                                <div class="widget-top-overflow windget-padding-md clearfix ${value.active ? 'bg-active' : 'bg-suspendu'} text-white">
                                    <h3 class="mt-lg mb-lg">
                                        ${value.name}
                                        <ul class="tags-form text-white pull-right">
                                            <li>
                                                <span>
                                                    <i class="fa fa-${value.active ? 'check' : 'times'}-circle"></i>
                                                    ${value.active ? ' Activé': 'Non activé'}
                                                </span>
                                            </li>
                                        </ul>
                                    </h3>
                                </div>
                                <p class="text-light text-desc">
                                    ${value.description ? value.description : '<b>(Pas de description)</b>'}
                                </p>
                            </div>
                            <footer class="bg-body-light">
                                <ul class="post-comments mt mb-0">
                                    <li class="form-date">
                                        <span class="thumb-xs avatar pull-left mr-sm">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <div class="comment-body">
                                            <h6 class="author fw-semi-bold">Début </h6>
                                            <p>${value.startDate ? value.startDate : '(Non renseigné(e))'}</p>
                                        </div>
                                    </li>
                                    <li class="form-date">
                                        <span class="thumb-xs avatar pull-left mr-sm">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <div class="comment-body">
                                            <h6 class="author fw-semi-bold">Fin </h6>
                                            <p>${value.endDate ? value.endDate : '(Non renseigné(e))'}</p>
                                        </div>
                                    </li>
                                    <li class="text-center form-btn-container">
                                        <a href="javascript:;" data-id="${key}" data-enddate="${value.endDate}" class="btn btn-default answer-form" data-toggle="tooltip" data-placement="bottom" data-original-title="Afficher resultat framadate alt.">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                        
                                    </li>
                                </ul>
                            </footer>
                        </section>
                    </div>
                `;
                if(index == Object.keys(allForm).length - 1) $('#formList<?php echo $kunik ?>').append(blockForm);
            }
        }
        $("#formList<?php echo $kunik ?>").on("click", ".answer-form", function() {
                let url = `${baseUrl}/costum/AltFramadate/getallanswers/form/${$(this).attr("data-id")}`;
                formInfoObj.parentForm = $(this).attr("data-id");
                let formEndDate = $(this).attr("data-enddate") ? $(this).attr("data-enddate").split("/") : $(this).attr("data-enddate");
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {parentForm: $(this).attr("data-id")},
                    success: function(ajaxRes) {
                        if(ajaxRes.length > 0) {
                            let answerOfFramadate = [];
                            
                            ajaxRes.forEach((item, iteration) => {
                                
                                if(item['answers']) {
                                    let valuesAnswer = Object.values(item['answers']);
                                    let keyAnswer = '';
                                    const valueOfFramadate = valuesAnswer.map((e, i) => {
                                        if(Object.values(e).findIndex(iter => iter.value) > -1){
                                            keyAnswer = Object.keys(item['answers'])[i];
                                            return {
                                                ...{subForm: Object.keys(e)[Object.values(e).findIndex(iter => iter.value)]},
                                                ...Object.values(e)[Object.values(e).findIndex(iter => iter.value)]}
                                        } else return null
                                    }).reduce(function(result, item) {
                                        if(item) {
                                            return result.concat(item)
                                        }
                                        return result
                                    }, []);
                                    
                                    if(valueOfFramadate[0] != null){
                                        answerOfFramadate.push({
                                            user: item.userinfo ? Object.values(item.userinfo)[0] : null,
                                            answer: valueOfFramadate[0],
                                            _id: item._id.$id,
                                            form: keyAnswer
                                        });
                                    }
                                }
                                if(iteration == ajaxRes.length -1) {
                                    
                                    if(answerOfFramadate.length > 0) {
                                        answerOfFramadate.forEach((itemAnswer, index) => {
                                            if(itemAnswer.answer.value) {
                                                const parentKey = Object.keys(itemAnswer.answer.value);
                                                Object.values(itemAnswer.answer.value).forEach((itemVal, indexVal) => {
                                                    if(itemAnswer.answer.subValue.length > 0) {
                                                        const subValue = itemAnswer.answer.subValue.filter(elemSubVal => elemSubVal.parent == parentKey[indexVal]);
                                                        if(subValue.length > 0) {
                                                            subValue.forEach(itemSubVal => {
                                                                const indexFinded = header.findIndex(elemSubValue => elemSubValue.subValue == itemSubVal.subValue && elemSubValue.parentValue == itemVal);
                                                                headerExtra.push({
                                                                    parentValue: itemVal,
                                                                    subValue: itemSubVal.subValue,
                                                                    parentKey: parentKey[indexVal],
                                                                    index: index
                                                                })
                                                                if(indexFinded > -1) {
                                                                    header[indexFinded].count += 1
                                                                } else {
                                                                    header.push({
                                                                        parentValue: itemVal,
                                                                        subValue: itemSubVal.subValue,
                                                                        count: 1
                                                                    })
                                                                }
                                                            })
                                                            
                                                        }
                                                    }
                                                })
                                            }
                                            
                                        });
                                    }
                                    
                                    const indexMaxVoted = header.findIndex(e => e.count == (Math.max(...header.map(e => e.count))));
                                    answerOfFramadate.forEach((item, index) => {
                                        if(item.user) {
                                            let bodyLine = [];
                                            header.forEach((itemHeader, indexH) => {
                                                const indexParentFinded = Object.values(item.answer.value).findIndex(elemParent => elemParent == itemHeader.parentValue);
                                                let indexSubFinded = -1;
                                                if(indexParentFinded > -1) indexSubFinded = item.answer.subValue.findIndex(elemSub => elemSub.subValue == itemHeader.subValue && itemHeader.parentValue == item.answer.value[elemSub.parent]);
                                                if(index == 0) {
                                                    if(indexH == 0) {
                                                        headerHtml = `<thead><tr><th style="min-width: 100px"></th>`;
                                                        footerHtml = `<tfoot><tr><th class='my-th'>Total<br>${answerOfFramadate.length} Votant${answerOfFramadate.length > 1 ? 's' : ''}</th>`;
                                                        footer.push(answerOfFramadate.length)
                                                    }

                                                    headerHtml += `<th class='my-th'>${itemHeader.parentValue}<br> ${itemHeader.subValue}</th>`;
                                                    footerHtml += `<td class="my-th ${indexMaxVoted > -1 && indexMaxVoted == indexH ? 'bg-green' : ''}">${itemHeader.count}</td>`;
                                                    footer.push(itemHeader.count);

                                                    if(indexH == header.length - 1) {
                                                        headerHtml += '</tr></thead>';
                                                        footerHtml += '</tr></tfoot>'
                                                    }
                                                }
                                                
                                                if(indexH == 0) {
                                                    if(userLogged == item.user._id.$id) {
                                                        bodyHtml += `<tr ${new Date() > new Date(formEndDate[2],formEndDate[1]*1-1,formEndDate[0]) ? '' : 'data-toggle="popModal"'} data-index="${index}" data-id="${item._id}" data-formid="${item.form}" data-subformid="${item.answer.subForm}" data-user="${item.user._id.$id}">`;
                                                    } else {
                                                        bodyHtml += `<tr>`;
                                                    }
                                                    bodyHtml += `<th>${item.user.name} </th>`;
                                                    bodyLine.push({
                                                        userid: item.user._id.$id,
                                                        name:item.user.name,
                                                        id: item._id,
                                                        formid: item.form,
                                                        subform: item.answer.subForm
                                                    })
                                                }

                                                if(indexSubFinded > -1) {
                                                    bodyHtml += `<td><i class="fa fa-check"></i></td>`;
                                                    bodyLine.push(true)
                                                } else {
                                                    bodyHtml += '<td></td>';
                                                    bodyLine.push(false)
                                                }

                                                if(indexH == header.length - 1) {
                                                    bodyHtml += '</tr>';
                                                    body.push(bodyLine)
                                                }
                                            });
                                            
                                            if(index == answerOfFramadate.length -1) {
                                                formInfoObj.formid = item.form;
                                                formInfoObj.subformid = item.answer.subForm;
                                                formInfoObj.id = item._id;
                                                const rowNew = new Date() > new Date(formEndDate[2],formEndDate[1]*1-1,formEndDate[0]) ? '' : generateRowFOrInsert(body.length);
                                                const blockRes = `
                                                    <div class="my-table-wrapper">
                                                        <table class="table table-bordered table-hover my-table">
                                                            ${headerHtml}
                                                            <tbody>${bodyHtml}${rowNew}</tbody>
                                                            ${footerHtml}
                                                        </table>
                                                    </div>
                                                `;
                                                $('#formList<?php echo $kunik ?>').html(blockRes)
                                            }
                                        }
                                    });
                                }
                            });
                            
                        }
                    },
                    error: function(err) {
                        console.error(err)
                    }
                })
        });
        sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre bloc",
                "description" : "Personnaliser votre bloc",
                "icon" : "fa-cog",
                
                "properties" : {
                    "contentTitle" : {
                        "inputType" : "text",
                        "label" : "Text du titre",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.contentTitle
                    },
                    "introduction":{
                        "inputType" : "textarea",
                        "markdown" : true,
                        "label" : "Text",
                        value : sectionDyf.<?php echo $kunik ?>ParamsData.introduction
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent")
                    tplCtx.value[k] = formData.parent;

                    if(k == "items")
                    tplCtx.value[k] = data.items;
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                        });
                    });
                    }
                }
			}

		};
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});
    })
</script>