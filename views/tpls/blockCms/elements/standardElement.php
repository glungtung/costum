<?php 
  HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>


<style>
  .content-element<?= $kunik ?> .btn-container button{display: none;}
  .content-element<?= $kunik ?> .btn-container{
    min-height: 35px;
    margin-bottom: 10px;
  }
  .content-element<?= $kunik ?>:hover .btn-container button{
    display: block;
  }

  .title-bg-<?= $kunik ?> {
    background-color: <?= $paramsData["titleBg"]?>;
  }
  .choose<?= $kunik?>{
    font-size : 18px;
  }

  .elementTag:hover {
    text-decoration : underline;
  }

  .button-showAll-title:hover {
    text-decoration : none;
  }

  .button-showAll-title:focus {
    text-decoration : none;
  }

  <?php if ($paramsData["buttonShowAllContentView"] === "2") {?>
    .divEndOfresults {
      display : none;
    }
  <?php } ?>
</style>
<div class="content-element<?= $kunik?>">
  <div class="stdr-elem">
      <?php if(($paramsData["title"] != "" && $costum["editMode"] == "false") ||( $costum["editMode"] == "true")){?>
        <div class="title-bg-<?= $kunik ?> padding-top-10 padding-bottom-10 padding-left-10">
          <h3 class="title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title" ><?= $paramsData["title"]?></h3>
        </div>
      <?php } ?>
      <?php if(($paramsData["title2"] != "" && $costum["editMode"] == "false") ||( $costum["editMode"] == "true")){?>
        <h4 class="title-2 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title2"><?= $paramsData["title2"]?></h4>
      <?php } ?> 
    <?php if (count($paramsData["elType"]) == 1 && $paramsData["elType"][0] == "events" && $paramsData["btnCreateNewEvent"] == 1) { ?>
      <div class="addEvent text-center">
        <button class="btn btn-primary" onclick="dyFObj.openForm('<?= Event::CONTROLLER ?>')">
            <i class="fa fa-plus" ></i> <?php echo Yii::t('cms', 'Add an event')?>
        </button> 
      </div>
    <?php }  ?>
    <?php if($costum["editMode"] == "true" ){?>
        <div class="text-center btn-container">
          <?php if(isset($costum["typeObj"][$addBtnType])) { ?> 
            <button class="btn btn-primary btn-sm add hiddenPreview" onclick="dyFObj.openForm('<?= $addBtnType ?>',null,null,null,null, { msgSuccess : '<?= $paramsData['msgSuccess']?>'}) "></button>
          <?php  }else{?>
              <?php if($paramsData["btnAdd"]==true && is_string($addType)){ ?>
                <button class="btn btn-primary btn-sm add hiddenPreview" onclick="dyFObj.openForm('<?= $addType ?>',null,null,null,commonElObj<?= $kunik?>)"></button>
              <?php }elseif ($paramsData["btnAdd"]==true && is_array($addType) && count($addType)==1 ){  ?>
                <button class="btn btn-primary btn-sm add hiddenPreview" onclick="dyFObj.openForm('<?= $addBtnType??$addBtnType ?>',null,null,null,commonElObj<?= $kunik?>)"></button>
              <?php } 
              } ?>
          <?php if($paramsData["btnChoose"]==true ){ ?>
              <a class="btn btn-primary btn-sm add hiddenPreview choose<?= $kunik?>" > Choisir le(s) élément(s) à afficher</a>
          <?php }  ?>
        </div>
    <?php }  
      if ($paramsData["buttonShowAllContentView"] === "") {
        $paramsData["buttonShowAllContentView"] = "1";
      }

      $style = "padding:".$paramsData["buttonShowAllWdPaddingVertical"]." ".$paramsData["buttonShowAllWdPaddingHorizontal"].";
                border-color:".$paramsData["buttonShowAllWdBorderColor"].";
                border-radius:".$paramsData["buttonShowAllWdBorderRadius"].";
                color:".$paramsData["buttonShowAllWdFontColor"].";
                font-weight:".$paramsData["buttonShowAllWdFontweight"]."; 
                font-size:".$paramsData["buttonShowAllWdFontSize"].";
                font-family:".$paramsData["buttonShowAllWdFontFamily"]."; 
                background-color:".$paramsData["buttonShowAllWdColor"].";";
      
      $btnSuccess = ($paramsData["buttonShowAllMore"] === false) ? "btn-success" : "";
    ?>
  
  </div>
  <div class="stdr-elem-content-<?= $kunik?>"> </div>
  <?php 
  $eltType = "";
  if(isset($paramsData["buttonShowAll"]) && $paramsData["buttonShowAll"]==true){
    $eltType = (is_array($addType) && count($addType) == 1) ? $addType[0] : $addType;
  ?>
      <div class="text-center">
        <a <?php if ($paramsData["buttonShowAllContentView"] === "1") { ?> href="#search?types=<?php echo $eltType ?>" <?php } ?> 
          <?php if ($paramsData["buttonShowAllContentView"] === "2") {?> onclick="<?php echo "modalShowAll.actions.showModal();" ?>" <?php } ?> class="btn <?= $btnSuccess?> lbh"
          style="<?= ($paramsData["buttonShowAllMore"]) ? $style : ""?>">
          <?php echo $paramsData["buttonShowAllText"]; echo ($paramsData["buttonShowAllType"]) ? " ( ".$eltType." )" : "";?> 
        </a>
      </div>
  <?php 
    } 
  ?>
</div>

<script>
let head = `<span class="showAllTitle" style="display: contents;font-size: 18px;font-weight: 700;text-transform: uppercase;word-break: break-word;font-family: 'marianne-regular', sans-serif;"> Tous les articles </span>`;
  var modalShowAll = {
    view : {
      init : function() {
        return `
        <div style="top: 0;z-index: 9;background-color: #fff;width: 80%;position: fixed; text-align: left;padding-top: 50px !important;padding-bottom: 20px !important;border-bottom: 1px solid #ddd;margin-left: 26px !important;">${head}</div>
        <div id="listMode" class="col-md-12">
          <div class='headerSearchShowAll no-padding col-xs-12' style="margin-top: 20px;"></div>
          <div id='filterShowAllContainer'></div>
              <div class='bodySearchEltContainer'>
                <div class="listContent" ><ul id="bodyList"  style= "margin-top: 40px;list-style: none;padding-left: 0px;"></ul></div>
              </div>
        </div>
        `;
      },
    }, 
    actions : {
      listContent : function (filterTag) {
        let defaultType = [];
        defaultType.push(<?php echo json_encode($eltType); ?>); 
				var paramsEltList = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: "#filterShowAllContainer",
					header: {
						dom: ".headerSearchShowAll",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding allArticles',
							},
						},
					},
					defaults: {
						types: defaultType,
						filters: {
              'source.key' : costum.contextSlug,
						}
					},
					results: {
						dom: "#bodyList",
						renderView: "directory.listElementHorizontal",
					
					}, 
				}
        if (notEmpty(filterTag)) {
          let tagArray = [];
          tagArray.push(filterTag);
          paramsEltList.defaults.filters["tags"] = tagArray;
        }
				var filterGroupEtl = searchObj.init(paramsEltList);
				filterGroupEtl.search.init(filterGroupEtl);
      },
      closeTag : function () {
        $(".tag-active").remove();
        modalShowAll.actions.listContent();
        $("#listMode").find(".visible-xs").remove();
        $("#listMode").find(".count-result-xs").remove();
      },
      filterTag : function (tagName) {
        let str = `
          <i class="fa fa-chevron-right tag-active"></i> 
          <span class="tag-active" style="margin: 0; padding: 4px 12px; border: 1px solid; border-radius: 3px;position: relative; display: inline-block; line-height: 1em;font-family: 'marianne-regular', sans-serif;text-transform: uppercase;word-break: break-word;color: #000;font-size: 18px;font-weight: 700;">${tagName}
            <span style="position: absolute; top: -7px; right: -9px; cursor: pointer; background-color: #fff; color: #bbb;line-height: 1em;font-family: 'marianne-regular', sans-serif;" onclick="modalShowAll.actions.closeTag()"><i class="fa fa-times-circle"></i>
            </span>
          </span>
          `;
        if ($(".tag-active").length >= 1)
          $(".tag-active").remove();

        $('.showAllTitle').after(str);
        modalShowAll.actions.listContent(tagName);
        $("#listMode").find(".visible-xs").remove();
        $("#listMode").find(".count-result-xs").remove();
      },
      showTitle : function () {
        let ulSelector = $("#listMode");
        ulSelector.find(".visible-xs").remove();
        ulSelector.find(".count-result-xs").remove();
        ulSelector.find("#filterContainer").attr("style", "margin-bottom : 0px !important");
        ulSelector.parent().parent().find(".close-modal").css("right","13vh").css("top","5vh").css("position","fixed");
        ulSelector.parent().parent().find(".lr").attr("style", "height: 40px !important;");
        ulSelector.parent().parent().find(".rl").attr("style", "height: 40px !important;");
        ulSelector.parent().parent().parent().css("top","0px");
      },
      showModal : function () {
        smallMenu.open(modalShowAll.view.init());
        modalShowAll.actions.listContent();
        modalShowAll.actions.showTitle();
      }
    }
  }

  var showAllAtendee = {
    views : {
      init :  function () {
            return `
                    <div class='headerSearchIncommunity no-padding col-xs-12' style="margin-top: 20px;"></div>
                    <div id='filterContainer' class='searchObjCSS'></div>
                      <div class='bodySearchContainer'>
                        <div class='no-padding col-xs-12' id='dropdown_search'>
                        </div>
                        <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
                      </div>
                  `;
        }
    }, 
    actions : {
      listParticipantParams: function (typeSelected, idSelected) {
				let paramsParticipatorList = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: "#filterContainer",
					header: {
						dom: ".headerSearchIncommunity",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding',
								group: {
									count: true,
									types: true
								}
							}
						},
					},
					defaults: { 
						notSourceKey: true,
						types: ["citoyens"],
						filters: {}
					},
					results: {
						dom: ".bodySearchContainer",
						smartGrid: true,
						renderView: "directory.elementPanelHtml",
						map: {
							active: false
						}
					},
					filters: {
						text: true,
					}
				} 
				paramsParticipatorList.defaults.filters["links." + typeSelected + "." + idSelected] = { "$exists": "true" };
				var filterGroupTemplate = searchObj.init(paramsParticipatorList);
				filterGroupTemplate.search.init(filterGroupTemplate);
      },
      listParticipant: function (typeSelected, idSelected) {
        smallMenu.open(showAllAtendee.views.init());
        showAllAtendee.actions.listParticipantParams(typeSelected, idSelected);
      }
    }
    
  }

  <?php if(!empty($paramsData["imgDefault"])){ ?>
    if(carouselObj && carouselObj.costumDefaultImage){
      carouselObj.costumDefaultImage["<?= $kunik ?>"] = "<?= $paramsData["imgDefault"] ?>";
    }
  <?php } ?>
                  
  <?php //if($blockCms["type"] == "blockChild") { ?>
    //$( ".stdr-elem" ).wrap( `<div class="content-element<?= $kunik ?> col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>` );
  <?php //} ?>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  if(typeof sectionDyf.<?php echo $kunik ?>ParamsData != "undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.elType != "undefined"&& typeof sectionDyf.<?php echo $kunik ?>ParamsData.elType == "string")
  <?php if(is_string($addType)){?>
    var addType = "<?= $addType ?>";
  <?php } ?>
  var contextData = <?php echo json_encode($el); ?>;

  var commonElObj<?= $kunik?> = {
      "beforeBuild":{
          "properties" : { 
          }
      },
      "onload" : {
          "actions" : {
              "src" : { 
                "infocustom" : "Remplir le champ"
              }, 
          }
      }
  };   
  commonElObj<?= $kunik?>.afterSave = function(data){
    dyFObj.commonAfterSave(data, function(){
      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
      $("#ajax-modal").modal('hide');
      //urlCtrl.loadByHash(location.hash);
    });
  }
  setTimeout(function() {
      if(sectionDyf.<?php echo $kunik ?>ParamsData["elPoiType"] != "")
    commonElObj<?= $kunik?>.onload.actions.presetValue = {"type" : "<?= $paramsData["elPoiType"] ?>"};
    if(sectionDyf.<?php echo $kunik ?>ParamsData["formTitle"] != "")
      commonElObj<?= $kunik?>.onload.actions.setTitle =  <?= json_encode($paramsData["formTitle"]) ?>;
  },600)

  $(function(){ 
    if(sectionDyf.<?php echo $kunik ?>ParamsData.elType.length == 1 &&sectionDyf.<?php echo $kunik ?>ParamsData.elType[0] == "poi" && sectionDyf.<?php echo $kunik ?>ParamsData["elPoiType"] != ""){
      $(".stdr-elem button.add").text(trad.add<?= $paramsData["elPoiType"]?> );
      $(".stdr-elem button.add").text(exists(trad.add<?= $paramsData["elPoiType"] ?>) ? trad.add<?= $paramsData["elPoiType"]?> : "Ajouter <?= $paramsData["elPoiType"] ?> " );
    }else if(sectionDyf.<?php echo $kunik ?>ParamsData.elType.length == 1 ){ 
       $(".stdr-elem button.add").text(exists(trad.add<?= $paramsData["elType"][0] ?>) ? trad.add<?= $paramsData["elType"][0] ?> : trad.add<?= $addBtnType ?> );
    }else if(typeof sectionDyf.<?php echo $kunik ?>ParamsData.elType == "string" ){ 
      $(".stdr-elem button.add").text(exists(trad["add"+sectionDyf.<?php echo $kunik ?>ParamsData.elType]) ? trad["add"+sectionDyf.<?php echo $kunik ?>ParamsData.elType] : trad.add<?php echo $addBtnType ?> );
    }
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {

        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;

            if(k == "description")
              tplCtx.value[k] = data.description;
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                  $("#ajax-modal").modal('hide');
                  // urlCtrl.loadByHash(location.hash);
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                });
              } );
            }

        }
      }
    };

    sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties = Object.assign(
        sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,
        carouselObj.commonProperties(sectionDyf.<?php echo $kunik ?>ParamsData)
    )

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {   
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        tplCtx.format = true;
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"buttonShowAll",4,6,null,null,"Bouton pour afficher tout","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"title",6,6,null,null,"Titre","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"el",6,6,null,null,"Options","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"img",4,6,null,null,"Image","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"info",2,6,null,null,"Information","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"card",4,6,null,null,"Carte","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"cardNumber",4,6,null,null,"Nombre","#000","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"form",4,6,null,null,"Formulaire","#000","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"show",2,6,null,null,"Afficher les <i class='fa fa-chevron-down'></i>","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"btn",6,6,null,null,"Bouton ","#e45858","");
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"buttonShowAllWd",4,6,null,null,"Personnaliser","#000","");


        $('#elDesign').change(function(){
          var elDesign = $(this).val();
          tplCtx.value = {};
          tplCtx.value["elDesign"] = elDesign;
          dataHelper.path2Value(tplCtx, function(params) {
            toastr.success("Chargement . . .");
              //urlCtrl.loadByHash(location.hash);
              location.reload();
          });           
        });
        $("#buttonShowAllWdFontFamily").append(fontOptions)
        $(".fieldsetbuttonShowAllWd").hide()
        if ($("#buttonShowAllMore").val() === "true")
          $(".fieldsetbuttonShowAllWd").fadeIn()

        $('.buttonShowAllMorecheckboxSimple').find("a[data-checkval='true']").click(() => {
          $(".fieldsetbuttonShowAllWd").fadeIn()
        })

        $("#elType").change(function() {            
            if ($(this).val().length == 1 && $(this).val().includes("events")) {
              $(".btnCreateNewEventcheckboxSimple").fadeIn(); 
              $(".btnShowAllParticipantcheckboxSimple").fadeIn();
            }
            else {
              $(".btnCreateNewEventcheckboxSimple").hide();
              $(".btnShowAllParticipantcheckboxSimple").hide(); 
            }
        })

        if ($("#elType").val().length == 1 && $("#elType").val().includes("events")) {
          $(".btnCreateNewEventcheckboxSimple").fadeIn() 
          $(".btnShowAllParticipantcheckboxSimple").fadeIn();
        } else {
          $(".btnCreateNewEventcheckboxSimple").hide()
          $(".btnShowAllParticipantcheckboxSimple").hide();
        } 

        $('.buttonShowAllMorecheckboxSimple').find("a[data-checkval='false']").click(() => {
          $(".fieldsetbuttonShowAllWd").hide()
        })

        if($('#elType').val()=="poi")
            $('.elPoiTypeselect').fadeIn();
          else
            $('.elPoiTypeselect').hide();     

        const fadeDateComposition = () => {
            $('.showYearOnlycheckboxSimple, .showDayOnlycheckboxSimple, .showMonthOnlycheckboxSimple').fadeIn();
          }

        const hideDateComposition = () => {
          $('.showYearOnlycheckboxSimple, .showDayOnlycheckboxSimple, .showMonthOnlycheckboxSimple').hide();  
        }

        if (!($("#showDateOnly").val() === "true" && $("#showDate").val() === "true")) {
          hideDateComposition();
        }

        if ($("#showMonthOnly").val() === "false" && $("#showYearOnly").val() === "true") {
          $('.showDayOnlycheckboxSimple').hide();
        }

        if ($("#showDayOnly").val() === "false") {
          $('.showDayNumberOnlycheckboxSimple').hide();
        }

        $('.showDatecheckboxSimple, .showDateOnlycheckboxSimple').find("a[data-checkval='true']").click(() => {
          if ($("#showDateOnly").val() === "true" || ($("#showDate").val() === "true" && $("#showDateOnly").val() === "false")) {
            fadeDateComposition();
          }
        });

        $('.showDatecheckboxSimple, .showDateOnlycheckboxSimple').find("a[data-checkval='false']").click(() => {
          hideDateComposition();
        });

        $('.showMonthOnlycheckboxSimple').find("a[data-checkval='true']").click(() => {
          $('.showDayOnlycheckboxSimple').fadeIn();
        });

        $('.showMonthOnlycheckboxSimple').find("a[data-checkval='false']").click(() => {
          if (!($("#showMonthOnly").val() === "false" && $("#showYearOnly").val() === "false")) {
            $('.showDayOnlycheckboxSimple').hide();
          }
        });

        $('.showYearOnlycheckboxSimple').find("a[data-checkval='true']").click(() => {
          if ($("#showMonthOnly").val() === "false") {
            $('.showDayOnlycheckboxSimple').hide();
          }
        });

        $('.showYearOnlycheckboxSimple').find("a[data-checkval='false']").click(() => {
          $('.showDayOnlycheckboxSimple').fadeIn();
        });

        $('.showDayOnlycheckboxSimple').find("a[data-checkval='true']").click(() => {
          $('.showDayNumberOnlycheckboxSimple').fadeIn();
        });

        $('.showDayOnlycheckboxSimple').find("a[data-checkval='false']").click(() => {
          $('.showDayNumberOnlycheckboxSimple').hide();
        });


        $('#elType').change(function(){
          if($(this).val()=="poi")
            $('.elPoiTypeselect').fadeIn();
          else
            $('.elPoiTypeselect').fadeOut();
        })
    });

    <?php if($paramsData["elDesign"]==1 || $paramsData["elDesign"]==3 || $paramsData["elDesign"]==4 || $paramsData["elDesign"]== 5 ){ ?> 
      carouselObj.multiCart.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>
    <?php if($paramsData["elDesign"]==2 || $paramsData["elDesign"]==6){ ?> 
     carouselObj.cardBlock.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>
    <?php if($paramsData["elDesign"]==7){ ?>
      carouselObj.rotatingDesign.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>
    $(".choose<?= $kunik?>").off().on("click",function() {
      mylog.log("dataElement",carouselObj.dataElement);
      var tplCtx = {};
      tplCtx.id = "<?= $blockCms['_id'] ?>" ;
      tplCtx.collection = "cms";
      tplCtx.path = "allToRoot";
      allElement = {}; 
      if(sectionDyf.<?php echo $kunik ?>ParamsData.elType.length == 1){
        $.each( carouselObj.dataElement[sectionDyf.<?php echo $kunik ?>ParamsData.elType], function(k,val) { 
          allElement[k] = val.name
        });
      }else{
        mylog.log("dataElement1",carouselObj.dataElement);
        $.each(carouselObj.dataElement, function(key,val) { 
          allElement[key] = val.name
        });
      
      } 
      mylog.log("dataElement",allElement);
      var activeForm = {
        "jsonSchema" : {
            "title" : "Choisir les éléments à  afficher",
            "type" : "object",
            onLoads : {
                onload : function(data){
                    $(".parentfinder").css("display","none");
                    selSortableObj.init("#element",allElement);

                    
                }
            },
            "properties" : {
              "element" : {
                "inputType" : "selectMultiple",
                "isSelect2" : true,
                "noOrder" :true,
                "label" : "Elément à Afficher (glisseret déposer pour organiser)",
                "class" : "form-control",
                "placeholder" : "Elément à Afficher",
                "value" : sectionDyf.<?=$kunik ?>ParamsData.element,
                options : allElement,
              }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?= $blockCms['_id'] ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( activeForm.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
              });
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) {
                  dyFObj.commonAfterSave(params,function(){
                    toastr.success("Élément bien ajouté");
                    $("#ajax-modal").modal('hide');
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //urlCtrl.loadByHash(location.hash);
                  });
                } );
              }
            }

        }
      };
      dyFObj.openForm( activeForm );
    })
  })
  var  selSortableObj = {
    init : function(myselect,options,config=null){
      $(myselect).select2({
          placeholder: '<?php echo Yii::t("cms", "Select and order your menu")?>'
      }).on("select2:select", function (evt) {
        var id = evt.params.data.id;
        var element = $(this).children("option[value="+id+"]");
        selSortableObj.moveElementToEndOfParent(element);
        $(this).trigger("change");
      });
      var ele=$(myselect).parent().find("ul.select2-choices");
      ele.sortable({
          containment: 'parent',
          update: function() {
            selSortableObj.orderSortedValues(myselect);
          }
      });
    },
   
    orderSortedValues : function(myselect) {
      var value = ''
      $(myselect).parent().find("ul.select2-choices").children("li").children("div").each(function(i, obj){
          var element = $(myselect).children('option').filter(function () { 
            return $(this).html() == $(obj).text() 
          });
          selSortableObj.moveElementToEndOfParent(element)
      });
    },
    moveElementToEndOfParent : function(element) {
      var parent = element.parent();
      element.detach();
      parent.append(element);
    }    
  };
</script>