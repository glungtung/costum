<?php
/* 
Super container:
Created by Ifaliana Arimanana
Edited by Sitraka Philippe
ifaomega@gmail.com
26 Apr 2021
*/

/***************** Required *****************/

use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;
$keyTpl      = "container";
$myCmsId     = $blockCms["_id"]->{'$id'};
$blockParent = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";

/*****************End get image uploaded***************/

/* Get settings */
$anchorTarget     = $blockCms["anchorTarget"] ?? "";
$name             = $blockCms["name"] ?? "";
$widthClass       = $blockCms["class"]["width"] ?? "std";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$balise         = $blockCms["advanced"]["balise"] ??  "div";
$order            = $blockCms["order"] ??  "0";
$height           = $blockCms["css"]["height"] ?? "";
$width            = $blockCms["css"]["width"] ?? "auto";
$backgroundType = $blockCms["css"]["backgroundType"] ?? "";
$backgroundColor = $blockCms["css"]["backgroundColor"] ?? "";
$backgroundImage = $blockCms["css"]["backgroundImage"] ?? "";
$objectCss        = $blockCms["css"] ?? [];
$styleCss         = (object) ['css' => $objectCss];
$blockChildren  = isset($blockCms["blockChildren"]) ? $blockCms["blockChildren"] : [];
if ( $backgroundType == "" && $backgroundColor != "" ){
    $backgroundType = "backgroundColor";
}
if ( count($blockChildren) == 0 && $backgroundType != "" && $height=="" ) {
    $height = "250px";
}

if (isset($blockCms['blockParent'])) {
    $width = $blockCms["css"]["width"] ?? "50%";
}
?>

<style type="text/css" id="sectionCss<?= $kunik ?>">
  .other-css-<?= $kunik ?> {  
    <?php 
      if (is_array($otherCss)) {
      foreach ($otherCss as $csskey => $cssvalue) {
        echo $csskey.":".$cssvalue.";\r\n";
      }
      }else{
        echo $otherCss;
      } ?>
  }
  
  <?php 
if ($backgroundImage == "" && count($blockChildren) > 0) { ?>
  @media (max-width: 800px) {
    .whole-<?= $kunik ?> {
      height: auto !important;
    }
  }
<?php } ?>
</style>

<script>
    $(function(){

        if (costum.editMode){
          cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>
        } else {
            cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? [ ]) ?>,'<?= $kunik ?>')
        }
        
        str="";
        str+=cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
        $("#sectionCss<?= $kunik ?>").append(str);


        mylog.log("children <?php echo $myCmsId ?>", <?= json_encode(@$blockCms["css"]) ?>)
    })

</script>

<<?= $balise ?> 
    class="<?=  (isset($blockCms['blockParent']))?"cmsbuilder-block cmsbuilder-block-droppable":"" ?>  <?= $kunik ?>-css cmsbuilder-block-container whole-<?= $kunik ?> spCustomBtn sp-cms-<?= $widthClass ?>  <?= $widthClass ?> <?= $kunik ?> super-cms <?= $otherClass ?> other-css-<?= $kunik ?> text-left <?= $myCmsId ?>" 
    style="order: <?= $order ?>;width : <?= $width ?>;height: <?= $height ?>;" 
    data-kunik="<?= $kunik ?>" 
    data-id="<?= $myCmsId ?>"
    data-blockType="<?= (isset($blockCms['blockParent']))?"column":"" ?>" 
    data-name="<?= (!isset($blockCms['blockParent']))?"": ($name != "" ? $name : "Colonne") ?>" 
    data-bloc-name="Container-<?= $myCmsId ?>"
    data-anchor-target="<?= $anchorTarget ?>"
>

<?php

    if (!empty($blockChildren)) {
        foreach ($blockChildren as $key => $value) {
            $pathExplode      = explode('.',@$value["path"]);
          //  $count            = count($pathExplode);
            $superKunik       = $pathExplode[count($pathExplode) - 1] . $value["_id"];
            $blockKey         = (string)$value["_id"];
            $params     = [
                "blockCms"  =>  $value,
                "kunik"     =>  $superKunik,
                "blockKey"  =>  $blockKey,
                "content"   =>  array(),
                "costum"    =>  $costum,
                "page"      =>  @$value["page"],
                "canEdit"   =>   $costum["editMode"],
                "clienturi" =>  @$clienturi,
                "el"        =>  $el
            ];
            //SPECIFIQUE FOR BLOCKCMS => ADD DIV CONTAINER TO GET ACTIONS FOR ALL TOOLBARS ON HOVER
            if (strpos(@$value["path"], "superCms") == false) { ?>
                <div class="cmsbuilder-block stop-propagation col-md-12 col-xs-12 no-padding sp-elt-<?= $blockKey ?> super-cms" data-blockType="custom" data-kunik="<?= $superKunik ?>" data-id="<?= $blockKey ?>" data-name="<?= isset($value["name"]) ? $value["name"] : (isset($value["blockName"]) ? $value["blockName"] : 'Block cms') ?>">
                <?php 
                // BOUBOULE JUST CLEAN : DONT ASK HIM WHY THAT 
                if(isset($value["blockCmsColorTitle1"]))
                    echo $this->renderPartial("costum.views.tpls.OldGarbageToKeepUntilTheEndCSS", array("value" => $value, "blockKey"=>$blockKey)); 
            }
            
            //GENERATE VIEW OF BLOCK CHILDREN
            if (is_file($this->getViewFile("costum.views." . @$value["path"]))) {
                echo BlockCmsWidget::widget([
                    "path" => @$value["path"],
                    //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
                    "notFoundViewPath" => "costum.views." . @$value["path"], 
                    "config" => $params
                ]);
            } else {
                echo $this->renderPartial("costum.views.tpls.blockNotFound", $params);
            }
            //
            // END OF SPECIFIQUE add Container div FOR BLOCKCMS CHILDREN 
            if (strpos(@$value["path"], "superCms") == false) echo "</div>"; ?>

        <!--  Required for old block inside a container -->
        <a href="javascript:;" style="display:none;" class="edit<?= $superKunik ?>Params" , data-path="<?= $value["path"] ?>" data-id="<?= $key ?>" data-collection="cms">
        </a>
        <?php }
        ?>
        <?php
    } 
    if ($costum["editMode"] == "true") { ?>
        <div class="empty-section-block empty-sp-element<?= $kunik ?> <?= ((count($blockChildren) > 0) || ( $backgroundType != "" && ( $backgroundColor != "" || $backgroundImage != "" )))  ? 'd-none' : '' ?>">
                <h3 style="font-weight: normal;"><?php echo Yii::t('cms', 'No element') ?></h3>
                <p><?php echo Yii::t('cms', 'Click to access the settings') ?></p>
        </div>
  <?php } ?>
</<?= $balise ?>>
