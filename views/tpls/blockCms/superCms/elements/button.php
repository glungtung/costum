<?php
/* 
Super button:
Created by Ifaliana Arimanana
ifaomega@gmail.com
Edited by Sitraka Philippe
13 Septemnbre 2022
*/

/***************** Required *****************/

/*if ($blockCms["type"] !== "blockCopy" && empty($blockCms["blockParent"])) {
  $blockCms["blockParent"] = @$blockCms["tplParent"];
} elseif ($blockCms["type"] !== "blockCopy" && empty($blockCms["tplParent"])) {
  $blockCms["tplParent"] = $blockCms["blockParent"];
}*/



$keyTpl     = "button";
$myCmsId    = $blockCms["_id"]->{'$id'};
$blockParent = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";
$name             = $blockCms["name"] ?? "";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$objectCss        = $blockCms["css"] ?? [];
$styleCss         = (object) ['css' => $objectCss];
$typeUrl = $blockCms["typeUrl"] ?? "internalLink";

/**********Link & text**********/
$text = $blockCms["text"] ?? Yii::t('cms', 'Button');
$url= (!empty($blockCms["link"])) ? $blockCms["link"] : "javascript:;";
//$external = $blockCms["externalLink"] ?? "";
if(!empty( $typeUrl == "internalLink")) $otherClass.=" lbh";
$targetBlank = (!empty($blockCms["targetBlank"]) && $blockCms["targetBlank"]) ? 'target="_blank" ' : "";
/********End Link & text********/


?>
<style type="text/css" id="button<?= $kunik ?>">
  .other-css-<?= $kunik ?> {  
      <?php 
      if (is_array($otherCss)) {
      foreach ($otherCss as $csskey => $cssvalue) {
        echo $csskey.":".$cssvalue.";\r\n";
      }
      }else{
        echo $otherCss;
      } ?>
   }
</style>


 <a href="<?= $url ?>"  <?= $targetBlank ?>  class="superButton <?= $kunik ?> <?= $kunik ?>-css spButtonBlock cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("commun","button") ?>" data-id="<?= $myCmsId ?>"  data-typeUrl="<?= @$blockCms["typeUrl"] ?>"> <?= $text ?></a>
 <!-- <button class="<?= $kunik ?> <?= $kunik ?>-css btn-functionLink cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" data-blockType="element" data-kunik="<?= $kunik ?>" data-name="button" data-id="<?= $myCmsId ?>" data-targetBlank="<?= $targetBlank ?>" data-internal="<?= @$internal ?>" data-external="<?= @$external ?>"> <?= $text ?></button>-->
 <script type="text/javascript">
  $(function(){

    if (costum.editMode){
      cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ;
      $(".<?= $kunik ?>").attr("href", "javascript:;").removeClass("lbh");
      $(".<?= $kunik ?>").removeAttr("target");
    } else {
      cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? [ ]) ?>,'<?= $kunik ?>')
    }
    
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
    $("#button<?= $kunik ?>").append(str);
  })
</script>
