<?php
/* 
Super image:
Created by Ifaliana Arimanana
Edited by Sitraka Philippe
ifaomega@gmail.com
26 Apr 2021
*/

/***************** Required *****************/

$keyTpl   = "image";
$myCmsId  = $blockCms["_id"]->{'$id'};
$kunik    = $keyTpl . $myCmsId;
$subtype  = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$blockParent = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";

$imagePath = isset($blockCms['image']) && $blockCms['image'] != "" ? $blockCms['image'] : Yii::app()->getModule("co2")->assetsUrl . '/images/thumbnail-default.jpg';

$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCssString = "";
  if (is_array($otherCss)) {
    foreach ($otherCss as $csskey => $cssvalue) {
      $otherCssString .= $csskey.":".$cssvalue.";\r\n";
    }
    $otherCss = $otherCssString;
  }


$name             = $blockCms["name"] ?? "";
$styleCss         = (object) ['css' => $blockCms["css"] ?? [] ];

/* End get settings */
?>

<style type="text/css" id="imageCss<?= $kunik ?>">
  .super-img-<?= $myCmsId ?> {
    width: 100%;
    height: 100%;    
  }

  @media (max-width: 800px) {
    .super-img-<?= $myCmsId ?> {
      padding: 0px;
      margin: 0px;
      left: 50% !important;
      /* transform: translate(-50%); */
    }
  }

  .other-css-<?= $kunik ?> {  
    <?php echo $otherCss; ?>
  }
</style>
<script>

$(function(){
  if (costum.editMode){
      cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>
  } else {
      cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? [ ]) ?>,'<?= $kunik ?>')
  }

  str="";
  str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
  $("#imageCss<?= $kunik ?>").append(str);

});
  
</script>


<div class="cmsbuilder-block <?= $kunik ?> <?= $kunik ?>-css super-cms  other-css-<?= $kunik ?> <?= $otherClass ?>  spCustomBtn <?= $myCmsId ?>" data-blockType="element" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>" data-name="image">
    <img class=" super-img-<?= $myCmsId ?>  this-content-<?= $kunik ?> unselectable" data-kunik="<?= $kunik ?>"  src="<?php echo $imagePath ?>" style="order: <?= $order ?> ;"/>
</div>