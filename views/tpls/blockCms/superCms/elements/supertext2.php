<?php 
 $myCmsId = $blockCms["_id"]->{'$id'};
 $blockParent = $blockCms["tplParent"] ?? $blockCms["tplParent"] ?? "";
 ?>
<style type="text/css">
    .underline{
        text-decoration: underline;
    }
    .sp-inline {
        /*display: inline;*/
    }
    .sb{        
        background-color: #00a0ff30;
        /*display: inline;*/
    }

    .<?= $kunik ?> {
        display: block;
    }
    #toolsBar .sp-replacer{
       height: 30px; 
       width: 50px; 
    }
    #toolsBar .sp-dd{
      padding: 5px 2px !important;
    }

</style>
<div class="mytext <?= $kunik ?>">Ipsum is simply dummy Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
<script>

<?php if(Authorisation::isInterfaceAdmin()){?>

function wrap(el, wrapper){
    el.parentNode.insertBefore(wrapper, el)
    wrapper.appendChild(el)
}

setTimeout(function() {
    if (costum.editMode == false) {
         $(".<?= $kunik ?>").attr('contenteditable','true');
         let allText = $(".<?= $kunik ?>")
         $(allText).each(function(i, objsptext) {
            let allTexts = $(objsptext).html();
            let p = document.createElement('div');
            p.innerHTML = allTexts;
            let links = p.querySelectorAll('.super-href');
            links.forEach(spx => {
              allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"]("+spx.href+")");
          });
            $(objsptext).html(allTexts);
            var spcmstextId = document.querySelector('spcms');
            if (spcmstextId !== null) {
              var texttextId = spcmstextId.textContent;
              spcmstextId.parentNode.replaceChild(document.createTextNode(texttextId), spcmstextId);
          }
        });
     }else{
       // convAndCheckLink(".<?= $kunik ?>");
       $(".<?= $kunik ?>").attr('contenteditable','false');
    }
},100)

$(".<?= $kunik ?>").on("mousedown", function(){ 
    $(".sb").removeClass("sb");
    //Remove tag for all text that change nothing
    $.each($('.<?= $kunik ?> span'), function(key, value) {
        if ($(value).text() == "") {
            $(value).remove()
        }else if(typeof $(value).attr('style') == "undefined" || $(value).attr('style') == ""){
            $(value).contents().unwrap()
        }
    })
    $(".sb:empty").remove()
})
document.addEventListener("mouseup", function(){ 
    // superCms.manage.addClassToSelection("sb")
    $(".sb:empty").remove()
})

if(costum.editMode){
    document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "true";
  }
  

$('.<?= $kunik ?>').blur(function(){

})

// $('.<?= $kunik ?>').mouseup(function() {
//     if (cmsBuilder.config.previewMode == false) {
//         $("#toolsBar").show();
//         setTimeout(function(){ 
//           $("#toolsBar").html(`
//             <div class="container-fluid">
//                 <div class="text-center">
//                   <i style="cursor:default;font-size: large;" aria-hidden="true">Text' settings</i>
//                   <button class="bg-transparent deleteLine delete-super-cms tooltips" data-path="" data-id="<?= $myCmsId ?>" data-parent="<?= $blockParent ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer tout le texte">
//                     <i class="fa fa-trash text-red" aria-hidden="true"></i>
//                   </button>
//                   <i class="fa fa-window-close closeBtn pull-right" aria-hidden="true"></i>
//                 </div>
//                 <div id="container" >
//                   <fieldset>
//                     <button class="fontStyle italic" title="Italicize Highlighted Text"><i class="fa fa-italic"></i></button>
//                     <button class="fontStyle bold" title="Bold Highlighted Text"><i class="fa fa-bold"></i></button>
//                     <button class="fontStyle underline"><i class="fa fa-underline"></i></button>
//                     <button class="fontStyle strikethrough"><i class="fa fa-strikethrough"></i></button>
//                     <select class="up-and-lowercase input" style="border: hidden;width:45px;height:30px;background-color: #efefef;">
//                       <option value="" disabled selected hidden>Aa</option>
//                       <option value="uppercase">MAJUSCULE</option>
//                       <option value="capitalize">Capitaliser</option>
//                       <option value="lowercase">minuscule</option>
//                     </select>
//                     <button onclick="insertTextAtCaret('[label](http://www)')" class="fontStyle" type="button">
//                       <i class="fa fa-link" aria-hidden="true"></i><br>
//                     </button>                    
//                     <select id="input-font" class="input" style="background-color: #efefef;border: hidden;height:30px"></select>
//                     <select style="background-color: #efefef;border: hidden; height: 30px" id="fontSize"></select>
//                     <input class="color-apply sp-color-picker spectrum sp-colorize myColor" data-cibler="color" type="color" id="myColor" value="#44536a"> 
//                     <input class="color-apply sp-color-picker spectrum sp-colorize myColor" data-cibler="bgcolor" type="color" id="myColor" value="#44536a"> 
//                     <button class="fontStyle align-left"><i class="fa fa-align-left"></i></button>
//                     <button class="fontStyle align-justify"><i class="fa fa-align-justify"></i></button>
//                     <button class="fontStyle align-center"><i class="fa fa-align-center"></i></button>
//                     <button class="fontStyle align-right"><i class="fa fa-align-right"></i></button>
                    
//                   </fieldset>
                  
//                 </div>
              
//             </div>

//           `);

//           $(".myColor").val($(".sb").parent().css("color"));
//           // alert($(".sb").parent().css("color"))
//           cmsBuilder.init();
//           $('#input-font').append(fontOptions);
//           $(".closeBtn").click(function(){
//             $("#toolsBar").html(``);
//             $("#toolsBar").fadeOut();
//             $(".sb").removeClass("sp-align");
//             //Remove a for all text that change nothing
//             $.each($('.<?= $kunik ?> span'), function(key, value) {
//                 if ($(value).text() == "") {
//                     $(value).remove()
//                 }else if(typeof $(value).attr('style') == "undefined" || $(value).attr('style') == ""){
//                     $(value).contents().unwrap()
//                 }
//             })
//           });
       
//     /******Selected area identification******/
//           function styleThisText (className){
//             parent = "";
//             nodes = $(".sb");
//             nodes.toggleClass(className);
//         }

//         $(document).on("cms-view-mode", function() {
//          convAndCheckLink(".<?= $kunik ?>");
//          $(".<?= $kunik ?>").attr('contenteditable','false');
//      })


//         $(document).on("cms-edit-mode", function() {
//          $(".<?= $kunik ?>").attr('contenteditable','true');
//             let allText = $(".<?= $kunik ?>")
//             $(allText).each(function(i, objsptext) {
//                 let allTexts = $(objsptext).html();
//                 let p = document.createElement('div');
//                 p.innerHTML = allTexts;
//                 let links = p.querySelectorAll('.super-href');
//                 links.forEach(spx => {
//                   allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"]("+spx.href+")");
//               });
//                 $(objsptext).html(allTexts);
//                 var spcmstextId = document.querySelector('spcms');
//                 if (spcmstextId !== null) {
//                   var texttextId = spcmstextId.textContent;
//                   spcmstextId.parentNode.replaceChild(document.createTextNode(texttextId), spcmstextId);
//               }
//           });
//         })

//     /************Font size options************/
//         var fontOpt ="";
//         for (var i = 5; i < 501; i++) {
//             fontOpt += "<option value='"+i+"'>"+i+"</option>"
//         }
//         $("#fontSize").append(fontOpt);
//         if ($(".sb").parent().text() != "") {
//             $("#fontSize").val($(".sb").parent().css("font-size").slice(0, - 2));
//         }else{
//             $("#fontSize").val(14);
//         }
//         $("#input-font").val($(".sb").parent().css("font-family"));
//         $(".up-and-lowercase").val($(".sb").parent().css("text-transform"))
//     /*********End font size options***********/

//     /***************Color management***************/
//         $(".myColor").change(function() {
//             if ($(this).data("cibler") == "color") {
//                 if($(".sb").parent().text() == $(".sb").text()) {           
//                     $(".sb").parent().css("color", $(this).val());
//                 }else{           
//                     $(".sb").css("color", $(this).val());
//                 }
//             }else if($(this).data("cibler") == "bgcolor"){ 
//                 if($(".sb").parent().text() == $(".sb").text() || $(".sb").parent().prop("tagName") !== "DIV") {           
//                     $(".sb").parent().css("background-color", $(this).val());
//                 }else{           
//                     $(".sb").css("background-color", $(this).val());
//                 }                
//             }
//         });
//     /*************End color management*************/

//     /*--------------------strikethrough------------------------------*/
//         $(".strikethrough").click(function(){
//             if($(".sb").parent().text() == $(".sb").text()) {
//                 if($(".sb").parent().css("text-decoration").match("line-through") == "line-through") { 
//                    $(".sb").parent().css("text-decoration", "");
//                }else{
//                    $(".sb").parent().css("text-decoration", "line-through");
//                }
//            }else{
//                 if($(".sb").css("text-decoration").match("line-through") == "line-through") { 
//                  $(".sb").css("text-decoration", "");
//              }else{
//                  $(".sb").css("text-decoration", "line-through");
//              }
//             }
//         });
//     /*----------------------italic----------------------------*/
//         $(".italic").click(function() {
//             if ($(".sb").parent().text() == $(".sb").text()) {                
//                 if ($(".sb").parent().css("font-style") === "normal") {            
//                     $(".sb").parent().css("font-style", "italic");
//                 }else{
//                     $(".sb").parent().css("font-style", "");
//                 }
//             }else{                
//                 if ($(".sb").css("font-style") === "normal") {            
//                     $(".sb").css("font-style", "italic");
//                 }else{
//                     $(".sb").css("font-style", "");
//                 }
//             }
//         });
//     /*------------------------bold--------------------------*/
//         $(".bold").click(function() { 
//             if($(".sb").parent().text() == $(".sb").text()) {       
//                 if($(".sb").parent().css("font-weight") === "400") {            
//                     $(".sb").parent().css("font-weight", "bold");
//                 }else{
//                     $(".sb").parent().css("font-weight", "");
//                 }
//             }else{      
//                 if($(".sb").css("font-weight") === "400") {            
//                     $(".sb").css("font-weight", "bold");
//                 }else{
//                     $(".sb").css("font-weight", "");
//                 }
//             } 
//         });
//     /*--------------------up-and-lowercase------------------------------*/
//         $(".up-and-lowercase").change(function() { 
//             if($(".sb").parent().text() == $(".sb").text()) { 
//                 $(".sb").parent().css("text-transform", $(this).val());
//             }else{               
//                 $(".sb").css("text-transform", "");
//                 $(".sb").css("text-transform", $(this).val());
//             } 
//         });
//     /*------------------------underline--------------------------*/

//         $(".underline").click(function() {  
//              if($(".sb").parent().text() == $(".sb").text()) {
//                 if($(".sb").parent().css("text-decoration").match("underline") == "underline") { 
//                    $(".sb").parent().css("text-decoration", "");           
//                }else{
//                    $(".sb").parent().css("text-decoration", "underline");
//                }
//             }else{ 
//                 if($(".sb").css("text-decoration").match("underline") == "underline") { 
//                    $(".sb").css("text-decoration", "");           
//                }else{
//                    $(".sb").css("text-decoration", "underline");
//                }
//             } 

//         });
//     /*-----------------------input-font---------------------------*/

//         $("#input-font").change(function() {
//             if($(".sb").parent().text() == $(".sb").text()) {         
//                 $(".sb").parent().css("font-family", $("#input-font").val())
//             }else{           
//                 $(".sb").css("font-family", $("#input-font").val())
//             }
//         });
//     /*------------------------fontSize--------------------------*/

//         $("#fontSize").change(function() {
//                if($(".sb").parent().text() == $(".sb").text()) {         
//                 $(".sb").parent().css("font-size", $("#fontSize").val()+"px","div")
//             }else{           
//                 $(".sb").css("font-size", $("#fontSize").val()+"px","div")
//             }
//         });
//     /*-------------------------align-left-------------------------*/

//         $(".align-left").click(function() {
//            if ($(".sb").parents().hasClass("sp-align")){                
//                 if ($(".sb").parents(".sp-align").css("text-align") !== "left") {
//                     $(".sb").parents(".sp-align").css("text-align", "left");
//                     $(".sb").parents(".sp-align").css("display", "inline-block");
//                     $(".sb").parents(".sp-align").css("width", "100%");
//                 }else{
//                     $(".sb").parent(".sp-align").contents().unwrap()
//                 }
//                }else if($(".sb").parent().not($(".<?= $kunik ?>")) && $(".sb").parent().not($(".this-was-aligned"))){
//                 if ($(".sb").parent().props("tagName") == "span" && $(".sb").parent().text() == $(".sb").text()) {
//                     $(".sb").parent().wrap("<div class='sp-align this-was-aligned' style='text-align: left; width:100%;'></div>") 
//                 }else{
//                  $(".sb").wrap("<div class='sp-align this-was-aligned' style='text-align: left; width:100%;'></div>")     
//                 }
//             }
//             $.each($('.<?= $kunik ?> span'), function(key, value) {
//                 if ($(value).text() == "") {
//                     $(value).remove()
//                 }
//             })
//         });
//     /*------------------------align-justify--------------------------*/
//         $(".align-justify").click(function() {
//             if ($(".sb").parents().hasClass("sp-align")){                
//                 if ($(".sb").parents(".sp-align").css("text-align") !== "justify") {
//                     $(".sb").parents(".sp-align").css("text-align", "justify");
//                     $(".sb").parents(".sp-align").css("display", "inline-block");
//                     $(".sb").parents(".sp-align").css("width", "100%");
//                 }else{
//                     $(".sb").parent(".sp-align").contents().unwrap()
//                 }
//                }else if($(".sb").parent().not($(".<?= $kunik ?>")) && $(".sb").parent().not($(".this-was-aligned"))){
//                  $(".sb").wrap("<div class='sp-align this-was-aligned' style='text-align: justify; width:100%;'></div>")     
//             }
//             $.each($('.<?= $kunik ?> span'), function(key, value) {
//                 if ($(value).text() == "") {
//                     $(value).remove()
//                 }
//             })
//         });
//     /*------------------------align-center--------------------------*/

//         $(".align-center").click(function() {
//             // styleThisText ("text-align", "center","div")    
//             if ($(".sb").parents().hasClass("sp-align")){                
//                 if ($(".sb").parents(".sp-align").css("text-align") !== "center") {
//                     $(".sb").parents(".sp-align").css("text-align", "center");
//                     $(".sb").parents(".sp-align").css("display", "inline-block");
//                     $(".sb").parents(".sp-align").css("width", "100%");
//                 }else{
//                     $(".sb").parent(".sp-align").contents().unwrap()
//                 }
//                }else if($(".sb").parent().not($(".<?= $kunik ?>")) && $(".sb").parent().not($(".this-was-aligned"))){
//                  $(".sb").wrap("<div class='sp-align this-was-aligned' style='text-align: center; width:100%;'></div>")     
//             }
//              $.each($('.<?= $kunik ?> span'), function(key, value) {
//                 if ($(value).text() == "") {
//                     $(value).remove()
//                 }
//             })
//         });
//     /*-----------------------align-right---------------------------*/

//         $(".align-right").click(function() {
//             if ($(".sb").parents().hasClass("sp-align")){                
//                 if ($(".sb").parents(".sp-align").css("text-align") !== "right") {
//                     $(".sb").parents(".sp-align").css("text-align", "right");
//                     $(".sb").parents(".sp-align").css("display", "inline-block");
//                     $(".sb").parents(".sp-align").css("width", "100%");
//                 }else{
//                     $(".sb").parent(".sp-align").contents().unwrap()
//                 }
//                }else if($(".sb").parent().not($(".<?= $kunik ?>")) && $(".sb").parent().not($(".this-was-aligned"))){
//                  $(".sb").wrap("<div class='sp-align this-was-aligned' style='text-align: right; width:100%;'></div>")     
//             }
//             $.each($('.<?= $kunik ?> span'), function(key, value) {
//                 if ($(value).text() == "") {
//                     $(value).remove()
//                 }
//            })
//         });
//     /*--------------------------------------------------*/

//         //color Picker
//         function loadColor(callback) {
//           mylog.log("loadColorPicker");
//           if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
//             mylog.log("loadDateTimePicker2");
//             lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
//               baseUrl+'/plugins/colorpicker/css/colorpicker.css',
//               callback);
//           }
//         }
//         function loadSpectrumColor(callback) {
//           mylog.log("loadSpectrumColor");
//           lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
//             baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
//             callback);

//         }
//         var initColor = function(){
//           mylog.log("init .colorpickerInput");
//           $(".colorpickerInput").ColorPicker({ 
//             color : "pink",
//             onSubmit: function(hsb, hex, rgb, el) {
//               $(el).val(hex);
//               $(el).ColorPickerHide();
//             },
//             onBeforeShow: function () {
//               $(this).ColorPickerSetColor(this.value);
//             }
//           }).bind('keyup', function(){
//             $(this).ColorPickerSetColor(this.value);
//           });
//         };

//         if($(".colorpickerInput").length){
//           loadColor(initColor);
//         }
//         loadSpectrumColor(function(){
//           if($(".sp-color-picker").length){
//                 $.each($(".sp-color-picker"),function (k,inputColor) {                
//                 if ($(inputColor).data("cibler") == "color") {
//                     $(inputColor).spectrum({
//                       type: "text",
//                       showInput: "true",
//                       showInitial: "true",
//                       color: $(".sb").parent().css("color")
//                     });
//                 }else{
//                     $(inputColor).spectrum({
//                       type: "text",
//                       showInput: "true",
//                       showInitial: "true",
//                       color: $(".sb").parent().css("background-color")
//                     });
//                 }
//             })
//             $('.sp-color-picker').addClass('form-control');
//             $('.sp-original-input-container').css("width","100%");
//           }
//         });
//         }, 0.1);        
//     }
//   });

<?php } else {?>
    // convAndCheckLink(".<?= $kunik ?>")
<?php } ?>
</script>
