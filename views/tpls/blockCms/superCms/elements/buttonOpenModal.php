<?php
/* 
Super button:
Created by Ifaliana Arimanana
ifaomega@gmail.com
30 Mey 2023
*/

/***************** Required *****************/

/*if ($blockCms["type"] !== "blockCopy" && empty($blockCms["blockParent"])) {
  $blockCms["blockParent"] = @$blockCms["tplParent"];
} elseif ($blockCms["type"] !== "blockCopy" && empty($blockCms["tplParent"])) {
  $blockCms["tplParent"] = $blockCms["blockParent"];
}*/

$keyTpl     = "button";
$myCmsId    = $blockCms["_id"]->{'$id'};
$blockParent= $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";

$name       = $blockCms["name"] ?? "";
$otherClass = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss   = $blockCms["advanced"]["otherCss"] ??  "height: max-content;";
$objectCss  = $blockCms["css"] ?? [];
$styleCss   = (object) ['css' => $objectCss];
$typeUrl    = $blockCms["typeUrl"] ?? "internalLink";

/**********Link & text**********/
$text = $blockCms["text"] ?? Yii::t('cms', 'Button');
//$external = $blockCms["externalLink"] ?? "";
// if(!empty( $typeUrl == "internalLink")) $otherClass.=" lbh";
// $targetBlank = (!empty($blockCms["targetBlank"]) && $blockCms["targetBlank"]) ? 'target="_blank" ' : "";
/********End Link & text********/

/*Modal managment*/
$openForm   = $costum["editMode"] == "false" ? "btn-open-form" : "";


?>
<style type="text/css" id="button<?= $kunik ?>">
  .other-css-<?= $kunik ?> {  
      <?php 
      if (is_array($otherCss)) {
      foreach ($otherCss as $csskey => $cssvalue) {
        echo $csskey.":".$cssvalue.";\r\n";
      }
      }else{
        echo $otherCss;
      } ?>
   }
</style>
<?php 

if (isset($blockCms["btnOpenModal"])) {
  switch ($blockCms["btnOpenModal"]) { 
    case "register" :
    if (!isset($_SESSION["userId"]) || $costum["editMode"] == "true") { ?>      
    <a class="<?= $kunik ?> <?= $kunik ?>-css btn-functionLink cmsbuilder-block bs super-cms btn-<?= $kunik ?> other-css-<?= $kunik ?> " href="javascript:;" data-toggle="modal" data-target="#modalRegister" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("commun","button") ?>" data-id="<?= $myCmsId ?>">
      <?= $text ?>
    </a>
    <?php 
    }
    break; 
    case "login" : 
    if (!isset($_SESSION["userId"]) || $costum["editMode"] == "true") { ?>    
      <a class="<?= $kunik ?> <?= $kunik ?>-css btn-functionLink cmsbuilder-block bs super-cms btn-<?= $kunik ?> other-css-<?= $kunik ?> " 
        href="javascript:;"  
        <?php if ($costum["editMode"] == "false") { ?>
        onClick="Login.openLogin()" 
        <?php } ?>
        data-kunik="<?= $kunik ?>" 
        data-name="<?= Yii::t("commun","button") ?>" 
        data-id="<?= $myCmsId ?>">
        <?= $text ?>
      </a>
      <?php 
    }
    break; 
    case "addExistingPage" : ?>    
      <a class="btn-functionLink superButton <?= $kunik ?> <?= $kunik ?>-css spButtonBlock cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" 
        href="javascript:;"  
        <?php if ($costum["editMode"] == "false" && isset($blockCms["addExistingPage"])) { ?>
        onClick="smallMenu.openAjaxHTML(baseUrl + '/co2/app/<?= $blockCms["addExistingPage"] ?>')" 
        <?php } ?>
        data-kunik="<?= $kunik ?>" 
        data-name="<?= Yii::t("commun","button") ?>" 
        data-id="<?= $myCmsId ?>">
        <?= $text ?>
      </a>  
      <?php 
    break; 
    case "createElement": ?>    
    
    <a class="<?= $openForm ?> superButton <?= $kunik ?> <?= $kunik ?>-css spButtonBlock cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" href="javascript:;" data-form-type="<?= strtolower(isset($blockCms["elementsType"]) ? $blockCms["elementsType"] : "") ?>" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("commun","button") ?>" data-id="<?= $myCmsId ?>"  data-typeUrl="<?= @$blockCms["typeUrl"] ?>">
      <?= $text ?>
    </a>
    <?php break;
    case "contact": ?>
    <!-- <a class="<?= $openForm ?> <?= $kunik ?> <?= $kunik ?>-css btn-functionLink cmsbuilder-block bs super-cms btn-<?= $kunik ?> other-css-<?= $kunik ?> " href="javascript:;" data-form-type="project" data-toggle="modal" data-target="#modalRegister" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("commun","button") ?>" data-id="<?= $myCmsId ?>"  data-typeUrl="<?= @$blockCms["typeUrl"] ?>">
      <?= $text ?>
    </a> -->
    <a href="javascript:;" class="tooltips openFormContact btn-functionLink superButton <?= $kunik ?> <?= $kunik ?>-css spButtonBlock cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" data-toggle="modal" data-target="#myModal-contact-us"
      data-id-receiver="" 
      data-email=""
      data-name=""
      data-id="<?= $myCmsId ?>"
      data-kunik="<?= $kunik ?>">
          <?= $text ?>
      </a>   

      <div class="portfolio-modal modal fade " id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content padding-top-15">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl">
              </div>
            </div>
          </div>

          <div class="sp-cms-container bg-white">

            <div id="form-group-contact">
              <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left padding-top-60 form-group">
                <h3>
                  <i class="fa fa-send letter-blue"></i> 
                  <small class="letter-blue">
                    <?php echo Yii::t('cms', 'Send an e-mail to')?> : </small>
                    <span id="contact-name" style="text-transform: none!important;"></span>
                    <br>
                    <small class="">
                      <small class=""><?php echo Yii::t('cms', 'This message will be sent to')?>
                    </small>
                    <b><span class="contact-email"></span></b>
                  </small>
                </h3>
                <hr><br>
                <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                  <label for="email" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your e-mail address')?>*</label>
                  <input type="email" class="form-control contact-input" placeholder="exemple@mail.com" id="emailSender">
                  <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
                  <label for="senderName" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Name / First name')?></label>
                  <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'What is your name?')?>" id="senderName">
                  <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
                  <label for="objet" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?></label>
                  <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject">
                  <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>

                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
                  <label for="objet" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Phone')?></label>
                  <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'Phone')?>" id="phoneSender">
                  <div class="invalid-feedback phoneSender"><?php echo Yii::t('cms', 'Invalid phone')?>.</div><br>

                </div>
              </div>
              <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group">
                <div class="col-md-12 contain-input">
                  <label for="message" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Image')?>Votre message</label>
                  <textarea placeholder="<?php echo Yii::t('cms', 'Your message')?>..." class="form-control contact-input txt-mail"
                    id="message" style="min-height: 200px;"></textarea>
                    <div class="invalid-feedback message"><?php echo Yii::t('cms', 'Your message is empty')?>.</div><br>
                  </div>
                  <div class="margin-top-15 contain-sendbtn">
                    <button type="submit" class="btn sendbtn" id="btn-send-mail">
                      <i class="fa fa-send"></i>  <?= $text ?>
                    </button>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
    <script type="text/javascript">

      <?php // ***************************send mail************************************* ?>
      if (notNull(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
        $("#formContact .contact-email").html(costum.admin.email);
      }
      $("#btn-send-mail").click(function(){
        sendEmail<?= $kunik ?>();
      });

      $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
      $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
      $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
      $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});

      $(".openFormContact").click(function(){
        mylog.log("openFormContact");
        if (notNull(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      //$("#formContact .contact-email").html(costum.admin.email);
          $("#formContact").modal("show");
        }else{
          bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
        }
      })
      function validateEmail<?= $kunik ?>(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }
      function sendEmail<?= $kunik ?>(){
        var acceptFields = true;
        $.each(["emailSender","senderName","subject","message"],(k,v)=>{
          if($("#"+v).val() == ""){
            $("."+v).show();
            acceptFields=false
          }
          if(validateEmail<?= $kunik ?>($("#emailSender").val())==false){
            acceptFields=false;
            $(".emailSender").show();
          }
          if(isNaN($("#phoneSender").val())) {
            acceptFields=false;
            $(".phoneSender").show();
          }

        });
        var seconds = Math.floor(new Date().getTime() / 1000);
        var allowedTime = localStorage.getItem("canSend");
        if(acceptFields){
          localStorage.removeItem("canSend");
          var emailSender = $("#emailSender").val();
          var phoneSender = $("#phoneSender").val();
          var subject = $("#subject").val();
          var senderName = $("#senderName").val();
          var message = $("#message").val()+"<br /> <?php echo Yii::t('cms', 'Phone')?><: "+phoneSender;
          var emailFrom = $(".contact-email").html();

          var params = {
            tpl : "contactForm",
            tplMail : emailFrom,
            fromMail: emailSender, 
            tplObject:subject,
            subject :subject, 
            names:senderName,
            emailSender:emailSender,
            message : message,
            sign : senderName+'<br>Téléphone: <a href="tel:'+phoneSender+'">'+phoneSender+'</a>',
            logo:"",
          };

          ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
            params,
            function(data){ 
              if(data.result == true){
                localStorage.setItem("canSend", (seconds+300));
                toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                $("#formContact").hide();
                $(".messageAfterSend").css("display" , "flex");

                $.each(["emailSender","senderName","subject","message","phoneSender"],(k,v)=>{$("#"+v).val("")})
              }else{
                toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
              }

            },
            function(xhr, status, error){
              toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
            }
            );  
        }
      }
    </script>
    <?php  break;
    case "subcribe": ?>
    <a class="<?= $openForm ?> btn-functionLink superButton <?= $kunik ?> <?= $kunik ?>-css spButtonBlock cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" href="javascript:;" data-form-type="project" data-toggle="modal" data-target="#modalRegister" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("commun","button") ?>" data-id="<?= $myCmsId ?>"  data-typeUrl="<?= @$blockCms["typeUrl"] ?>">
      <?= $text ?>
    </a>
  <?php  }
}else if($costum["editMode"] == "true"){ ?>
  <a class="btn-functionLink superButton <?= $kunik ?> <?= $kunik ?>-css spButtonBlock cmsbuilder-block super-cms btn-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>" href="javascript:;" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("commun","button") ?>" data-id="<?= $myCmsId ?>"  data-typeUrl="<?= @$blockCms["typeUrl"] ?>">
    <?= $text ?>
  </a>

<?php } ?>
 <script type="text/javascript">
  $(function(){

    if (costum.editMode){
      cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ;
      $(".<?= $kunik ?>").attr("href", "javascript:;").removeClass("lbh");
      $(".<?= $kunik ?>").removeAttr("target");
    } else {
      cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? [ ]) ?>,'<?= $kunik ?>')
    }
    
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
    $("#button<?= $kunik ?>").append(str);
  })


jQuery(document).ready(function() { 
    $(".btn-open-form").click(function(){
        var typeForm = "poi";
        mylog.log("test", $(this).data("form-subtype")),
        currentKFormType = "Group";
        //alert(contextData.type+" && "+contextData.id+" : "+typeForm);
        if(contextData && contextData.type && contextData.id )
            dyFObj.openForm(typeForm,"sub");
        else
            dyFObj.openForm(typeForm);
    });
});

 
</script>
