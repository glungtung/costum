<?php 
/***************** Required *****************/

// if ($blockCms["type"] !== "blockCopy" && empty($blockCms["cmsParent"])) {
//   $blockCms["cmsParent"] = @$blockCms["tplParent"];
// }elseif ($blockCms["type"] !== "blockCopy" && empty($blockCms["tplParent"])) {
//   $blockCms["tplParent"] = $blockCms["cmsParent"];
// }

$keyTpl     ="icon";
$myCmsId    = $blockCms["_id"]->{'$id'};
$params     = array();
$paramsData = array();

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

/* Get settings */
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["classes"] ??  "";
$Icon             =  $blockCms["iconStyle"] ?? "fa fa-angle-down";
$objectCss        = $blockCms["css"] ?? [];
$styleCss         = (object) ['css' => $objectCss];
/********Icon********/

?>
<style type="text/css" id="icon<?= $kunik ?>">

    .other-css-<?= $kunik ?> {  
        <?php 
        if (is_array($otherCss)) {
            foreach ($otherCss as $csskey => $cssvalue) {
                echo $csskey.":".$cssvalue.";\r\n";
            }
        }else{
            echo $otherCss;
        } ?>
    }
    
   @media (max-width: 800px) {
        .<?= $kunik ?> {
            width: 100% !important;
            background-size: contain;
            padding-left: 0px;
            padding-right: 0px;
            margin-left: 0px;
            margin-right: 0px;
        }
    }

</style>

<script type="text/javascript">
    $(function(){

      if (costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ;
      } else {
        cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? [ ]) ?>,'<?= $kunik ?>')
      }

      str="";
      str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
      $("#icon<?= $kunik ?>").append(str);

    })
</script>

<i id="icon-<?= $kunik ?>" class="superIcon <?= $kunik ?> cmsbuilder-block super-cms <?= $Icon ?> icon-<?= $kunik ?> <?= $kunik ?>-css" 
    data-blockType="element"data-kunik="<?= $kunik ?>"data-id="<?= $myCmsId ?>"data-name="icon">
</i>