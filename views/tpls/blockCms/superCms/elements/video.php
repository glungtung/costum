<?php

$myCmsId  = (string)$blockCms["_id"];
$kunik            = "video".$myCmsId;
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["classes"] ??  "";
$objectCss        = $blockCms["css"] ?? [];
$styleCss         = (object) ['css' => $objectCss];
$autoplay         = $blockCms["autoplay"] ?? "false";
$repeat           = $blockCms["repeat"] ?? "false";
$link             = $blockCms["link"] ?? "https://player.vimeo.com/video/133636468";

$autoplayVideo = "";
$repeatVideo = "";





if (strpos($link, "=") !== false) {
    $videoId = array_reverse(explode("=", $link))[0];
} else {
    $videoId = array_reverse(explode("/", $link))[0];
}

if ($repeat == "true")
    $repeatVideo = "playlist=".$videoId."&loop=1&";
if ($autoplay == "true")
    $autoplayVideo = "autoplay=1&mute=1&";

if (strpos($link,"vimeo") !== false) {
    $src = "https://player.vimeo.com/video/".$videoId;
}
if (strpos($link, "youtu") !== false) {
    $src = "https://www.youtube.com/embed/".$videoId."?".$autoplayVideo.$repeatVideo;
}
if (strpos($link,"dailymotion")!== false) {
    $src = "https://www.dailymotion.com/embed/video/".$videoId;
}
if (strpos($link,"indymotion")!== false) {
    $src = "https://indymotion.fr/videos/embed/w/".$videoId;
}
?>
<style type="text/css" id="video<?= $kunik ?>">
    .other-css-<?= $kunik ?> {
    <?php
    if (is_array($otherCss)) {
    foreach ($otherCss as $csskey => $cssvalue) {
      echo $csskey.":".$cssvalue.";\r\n";
    }
    }else{
      echo $otherCss;
    } ?>
    }
    @media (max-width: 978px) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 350px;
        }
    }
    @media (min-width: 767px) and (max-width: 992px ) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 235px;
        }
    }
    @media (min-width: 993px) and (max-width: 1200px ) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 280px;
        }
    }

    @media (min-width: 1201px) and (max-width: 1664px ) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 350px;
        }
    }
</style>
<div class="cmsbuilder-block super-cms" data-blockType="element" data-kunik="<?= $kunik ?>" data-name="video" data-id="<?= $myCmsId ?>" style="width:100%; height:max-content">
    <iframe class="<?= $kunik?> <?= $kunik ?>-css video-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>"  src="<?= $src?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


<script type="text/javascript">
    $(function(){

        if (costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ;
        }

        str="";
        str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
        $("#video<?= $kunik ?>").append(str);

    })
</script>