<?php 
/* 
Super container:
Created by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/


/***************** Required *****************/
$keyTpl     ="container";
$myCmsId    = $blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = array();
$blockParent = $blockCms["tplParent"] ?? $blockCms["tplParent"] ?? "";

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 
/*************** End required ***************/

/*****************get image uploaded***************/
$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $myCmsId,
    "type"=>'cms',
    "subKey"=>'block',
  ), "image"
);

  $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;

/*****************End get image uploaded***************/

/* Get settings */

$widthClass       = $blockCms["class"]["width"] ?? $blockCms["class"] ?? "col-md-4";
$otherClass       = $blockCms["class"]["other"] ?? $blockCms["class"]["other"] ?? "";
$otherCss         = $blockCms["css"]["other"] ?? $blockCms["css"]["other"] ?? "";
$array_position   = $blockCms["css"]['position'] ?? $blockCms["css"]['position'] ?? [];
$width            = $blockCms["css"]["size"]["width"] ?? $blockCms["css"]["size"]["width"] ?? "320px";
$order            = $blockCms["order"] ?? $blockCms["order"] ?? "0";
$objfit           = $blockCms["css"]["object-fit"] ?? $blockCms["css"]["object-fit"] ?? "none";
$height           = $blockCms["css"]["size"]["height"] ?? $blockCms["css"]["size"]["height"] ?? "240px";
$backgroundColor  = $blockCms["css"]["background"]["color"] ?? $blockCms["css"]["background"]["color"] ?? "transparent";


  /**********Border**********/
$borderColor                = $blockCms["css"]["border"]["color"] ?? $blockCms["css"]["border"]["color"] ?? "transparent";
$borderRadiustopLeft        = $blockCms["css"]["border"]["radius"]["top-left"] ?? $blockCms["css"]["border"]["radius"]["top-left"] ?? "0";
$borderRadiustopRight       = $blockCms["css"]["border"]["radius"]["top-right"] ?? $blockCms["css"]["border"]["radius"]["top-right"] ?? "0";
$borderRadiusbottomRight    = $blockCms["css"]["border"]["radius"]["bottom-right"] ?? $blockCms["css"]["border"]["radius"]["bottom-right"] ?? "0";
$borderRadiusbottomLeft     = $blockCms["css"]["border"]["radius"]["bottom-left"] ?? $blockCms["css"]["border"]["radius"]["bottom-left"] ?? "0";
$borderRadius               = max($borderRadiustopLeft,$borderRadiustopRight,$borderRadiusbottomRight,$borderRadiusbottomLeft);
  /********End border********/

  /**********Padding**********/
$paddingtop     = $blockCms["css"]["padding"]["top"] ?? $blockCms["css"]["padding"]["top"] ?? "1";
$paddingright   = $blockCms["css"]["padding"]["right"] ?? $blockCms["css"]["padding"]["right"] ?? "1";
$paddingleft    = $blockCms["css"]["padding"]["left"] ?? $blockCms["css"]["padding"]["left"] ?? "1";
$paddingbottom  = $blockCms["css"]["padding"]["bottom"] ?? $blockCms["css"]["padding"]["bottom"] ?? "1";
$padding        = max($paddingtop,$paddingright,$paddingleft,$paddingbottom);
  /********End padding********/

  /**********Margin**********/
$margintop     = $blockCms["css"]["margin"]["top"] ?? $blockCms["css"]["margin"]["top"] ?? "0";
$marginright   = $blockCms["css"]["margin"]["right"] ?? $blockCms["css"]["margin"]["right"] ?? "0";
$marginleft    = $blockCms["css"]["margin"]["left"] ?? $blockCms["css"]["margin"]["left"] ?? "0";
$marginbottom  = $blockCms["css"]["margin"]["bottom"] ?? $blockCms["css"]["margin"]["bottom"] ?? "0";
$margin        = max($margintop,$marginright,$marginleft,$marginbottom);
  /********End margin********/

  /**********shadow**********/
  $inset    = $blockCms["css"]["box-shadow"]["inset"] ?? $blockCms["css"]["box-shadow"]["inset"] ?? "";
  $shwColor = $blockCms["css"]["box-shadow"]["color"] ?? $blockCms["css"]["box-shadow"]["color"] ?? "transparent";
  $axeX     = $blockCms["css"]["box-shadow"]["x"] ?? $blockCms["css"]["box-shadow"]["x"] ?? "0";
  $axeY     = $blockCms["css"]["box-shadow"]["y"] ?? $blockCms["css"]["box-shadow"]["y"] ?? "0";
  $blur     = $blockCms["css"]["box-shadow"]["blur"] ?? $blockCms["css"]["box-shadow"]["blur"] ?? "0";
  $spread   = $blockCms["css"]["box-shadow"]["spread"] ?? $blockCms["css"]["box-shadow"]["spread"] ?? "0";
  /********End shadow********/

  /************Separtor************/
  $lineSeparator    = $blockCms["css"]["lineSeparator"] ?? $blockCms["css"]["lineSeparator"] ?? "";


/* End get settings */

?>
<style type="text/css">
  .unselectable {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select:none;
    user-select:none;
    -o-user-select:none;
  }

  .<?= $kunik ?> {  
    display: inline-block;
    width:100%;
    /*height: auto;*/
    position: sticky; 
    <?php if ($latestImg !== "empty") { ?>
    background-image: url('<?php echo $latestImg ?>');
    <?php } ?>     
    border: solid <?= $borderColor ?>;
    background-color: <?= $backgroundColor ?>;
    border-radius: <?= $borderRadiustopLeft ?>px <?= $borderRadiustopRight ?>px <?= $borderRadiusbottomRight ?>px <?= $borderRadiusbottomLeft ?>px; 
    padding: <?= $paddingtop ?>px <?= $paddingright ?>px <?= $paddingbottom ?>px <?= $paddingleft ?>px;
    margin: <?= $margintop ?>px <?= $marginright ?>px <?= $marginbottom ?>px <?= $marginleft ?>px;
    <?php 
      echo "-webkit-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "-moz-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
     ?>
  }

  .other-css-<?= $kunik ?> {  
    <?php 
    if (is_array($otherCss)) {
     foreach ($otherCss as $csskey => $cssvalue) {
      echo $csskey.":".$cssvalue.";\r\n";
    }
  }else{
    echo $otherCss;
  } ?>
}

  .edit-<?= $kunik ?> {
    border: dashed 2px gray;
  }


  .selected-mode-<?= $kunik ?>:hover {
    cursor: context-menu;
  }
  .selected-mode-<?= $kunik ?>:hover > .stop-propagation {
    cursor: default;
  }

  .selected-mode-<?= $kunik ?> {
    box-shadow: #92bfffa6 0px 0px 2px 2px;
    border : 2px dotted white;
  }

  .bg-transparent{
    background-color: transparent;
  }

  .super-<?= $myCmsId ?>{
    width : <?= $width ?>;
    min-height: <?= $width ?>;
  }

   .cards-list {
    z-index: 0;
    width: 100%;
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
  }

  .card {
    margin: 5px auto;
    width: 90px;
    height: 85px;
    border-radius: 10px;
    box-shadow:  0px 0px 6px 0px rgb(0 0 0 / 22%);
    cursor: pointer;
    transition: 0.4s;
  }

  .card .card_image {
    width: inherit;
    height: inherit;
    border-radius: 10px;
  }

  .card .card_image img {
    width: inherit;
    height: inherit;
    border-radius: 10px;
    object-fit: cover;
  }

  .card .card_title {
    text-align: center;
    font-family: sans-serif;
    font-size: 16px;
    margin-top: -55px;
  }

  .card:hover {
    transform: scale(0.9, 0.9);
    box-shadow: 0px 0px 0px 6px rgb(0 0 0 / 10%);
  }

  .title-white {
    color: white;
    top: -20px;
    position: relative;
  }

  #toolsBar .nav>li>a{
    padding: 0px 10px;
    font-family: 'Circular-Loom';
  }

  #toolsBar .nav{
    background-color: #ffffff42;
  }

  #toolsBar .tool-scms-title{
    background-color: #ffffff42;
  }

  #toolsBar .input-group {
    width: 100%;
    /* position: relative; */
    display: table;
    /* border-collapse: separate; */
  }

  .title-black {
    background-color: white;
    color: #37475e;
    border-radius: 10px 10px 0px 0px;
    top:-30px;
    position: relative;
  }
  .sp-cms-options {
    background-color: #cecece59;
    padding-bottom: 5px;
    padding-top: 10px;
  }

  @media all and (max-width: 500px) {
    .card-list {
      flex-direction: column;
    }
  }
  .super-cms-range-slider{ 
    -webkit-appearance: none;
    background: <?= $shwColor."6b" ?>;
    border: solid 1px #999;
    outline: none;
    opacity: 0.7;
    height: 10px;
    -webkit-transition: .2s;
    transition: opacity .2s;
  }

  .super-cms-range-slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 20px;
    height: 20px;
    background: black;
    cursor: pointer;
    border: solid 2px darkgray;
    border-radius: 50%;
  }

  .flex-container {
    /*display: flex;*/
    flex-direction: row;
    /*font-size: 30px;*/
    /*text-align: center;*/
  }

  .col-elem-value { 
    display: flex;
    flex-wrap: wrap;
  }

  .element-field {
    background-color: #47525d0d;
    padding: 10px;
    flex: 50%;
  }

  .super-element {
    padding: 5px;
  }

  @media (max-width: 800px) {
    .elem-value {
      flex-direction: column;
    }
  }

   @media (max-width: 800px) {
      body { 
      font-size: 25px!important; 
   }
    .<?= $kunik ?> {
      padding-left: 10px;
      padding-right: 10px;
      margin-left: 0px;    
      margin-right: 0px;      
      position: sticky; 
      left: 0px !important;
    }
    .whole-<?= $kunik ?> {
      padding-left: 10px;
      padding-right: 10px;
      margin-left: 0px;    
      margin-right: 0px;   
    }
    div {
    margin-right: 0px !important;
    margin-left: 0px !important;
    }

    <?php if ($blockCms["type"] === "blockCopy") { ?>
      .block-container-<?= $kunik ?> {
        margin-top: 1px !important;
        margin-bottom: 1px !important;
        padding-top: 10px !important;
        padding-bottom: 10px !important;
        padding: 0 !important;
      }
    <?php }else{ ?>
      .whole-<?= $kunik ?> {
        padding-left: 10px;
        padding-right: 10px;
        margin-left: 0px;    
        margin-right: 0px;   
      }
      .<?= $kunik ?> {
        height: auto !important;
        min-height: auto !important;
        background-size: contain;
      }
      

    <?php } ?>
  }
  .spDrag-<?= $kunik ?> {
    top: 0px;
    z-index: 10;
    position: absolute;
    right: 50%;
    cursor: pointer;
    display: none;
  }
</style>

<div href="#" class="no-padding cursor-pointer">
  <?php $params = ["blockCms"=>$blockCms,"kunik"=>$kunik,"blockKey"=>$blockKey,"content"=>array(),"costum"=>$costum,"page"=>$page, "el"=>$el];
  echo $this->renderPartial("costum.views.tpls.blockCms.superCms.container",$params);
  ?>
</div>


<script type="text/javascript">  

</script>