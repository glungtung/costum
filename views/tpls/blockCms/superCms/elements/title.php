<?php

/***************** Required *****************/
$myCmsId  = (string)$blockCms["_id"];
$kunik    = "title".$myCmsId;
$balise           = $blockCms["balise"] ??  "h1";
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["classes"] ??  "";
$title             = $blockCms["title"] ?? "Votre nouveau titre";
$objectCss        = $blockCms["css"] ?? [];
$styleCss         = (object) ['css' => $objectCss];

/* End get settings */
?>

<style type="text/css" id="textCss<?= $kunik ?>">

  .other-css-<?= $kunik ?> {  
    <?php 
    if (is_array($otherCss)) {
     foreach ($otherCss as $csskey => $cssvalue) {
      echo $csskey.":".$cssvalue.";\r\n";
    }
    }else{
      echo $otherCss;
    } ?>
  }
</style>
<!--<div class="cmsbuilder-block super-cms" data-blockType="element" data-kunik="<?= $kunik ?>" data-name="<?= Yii::t("common","title") ?>" data-id="<?= $myCmsId ?>" style="width:max-content; height:max-content">-->
    <?php echo 
    "<".$balise." class='".$kunik." ".$kunik."-css title-".$myCmsId." spTitleBlock  cmsbuilder-block super-cms other-css-".$kunik." ".$otherClass."' data-kunik='".$kunik."' data-id='".$myCmsId."' data-name='".Yii::t("common","title")."'>".
        $title.
    "</".$balise.">"; ?>
<!--</div>-->
<script>
$(function(){
    if (costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>
    } else {
        cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? [ ]) ?>,'<?= $kunik ?>')
    }

    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>,`<?= $kunik ?>`);
    $("#textCss<?= $kunik ?>").append(str);
});
</script>
