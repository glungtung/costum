<?php if($canEdit && Authorisation::isInterfaceAdmin() ){ ?>
  <?php if ($subtype != "supercms") { ?>
    <div class="text-center btn-edit-delete-<?= @$kunik?> edit-inside-sp-block hiddenPreview col-md-12" >
      <a class="tooltips edit<?php echo @$kunik?>Params padding-5" data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Modifier ce <?= @$name?>">
        <i class="fa fa-edit" aria-hidden="true"></i>
      </a>

      <a style="color: #f44336;" class="tooltips deleteLine padding-5" data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button" data-parent="<?= @$parentId ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer ce <?= @$name?>">
        <i class="fa fa-trash" aria-hidden="true"></i>
      </a>
    </div>
    <script type="text/javascript">
      cmsBuilder.manage.delete();
      $(document).ready(function() {
        let isEmpty = false;
       
        if (typeof sectionDyf.<?php echo @$kunik?>Params !== "undefined"){
                 isEmpty = Object.keys(sectionDyf.<?php echo @$kunik?>Params.jsonSchema.properties).length === 0;
              }

        if (isEmpty) {
          $(".edit<?php echo @$kunik?>Params").off().on('click', function(e){
            e.stopPropagation()
            setTimeout(function () {
              $("#toolsBar-edit-block").hide();    
              $("#toolsBar-edit-block").html(""); 
              // $("#supercms-overlay").empty();
              // $("#supercms-overlay").css({"display": "none"});
              // $(superCms.parents).append(superCms.element); 
              coInterface.initHtmlPosition();    
            },0.01)  
            toastr.info("Aucun paramètre à modifier!")
          });
        }else{
          $(".edit<?php echo $kunik?>Params").on("mousedown", function () {
           let path = '<?= @$path ?>';
           let blockName = ""
           path = path.replace(/./g, '');    
           // if(typeof callByName[path.replace(/./g, '')] !== 'undefined')
           blockName = "<i class='fa fa-info-circle cursor-pointer' title='<?= @$path ?>' style='color: #00bcd4;'></i> Config <?= preg_replace('/(?<!\ )[A-Z]/', ' $0', substr(@$path, strrpos(@$path, '.') + 1));?>";
           sectionDyf.<?php echo $kunik?>Params.jsonSchema.title = blockName;
          })
          $(".edit<?php echo $kunik?>Params").click(function(e) {  
            e.stopPropagation() 
            setTimeout(function () {
              $("#toolsBar-edit-block").hide(); 
              $("#toolsBar-edit-block").html("");   
              // $("#supercms-overlay").empty();
              // $("#supercms-overlay").css({"display": "none"});
              // $(superCms.parents).append(superCms.element);
              coInterface.initHtmlPosition();
                     
            },0.01) 
           
         });
        }
      })
    </script>
  <?php } ?>
<?php }?>




