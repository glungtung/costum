<script>
  var itemClassArray = [];
  var pageOptions = {},
    hasAnchor = false;
  if (notNull(costum) && exists(costum.app))
    $.each(costum.app, function(k, v) {
      if (exists(v.lbhAnchor) && v.lbhAnchor == true) hasAnchor = true;
      pageOptions[k] = exists(v.subdomainName) ? v.subdomainName : "";
    })
</script>
<style type="text/css">

.tooltipSpCms {
  display: inline-block;
  position: fixed;
  padding: .5em 1em;
  background-color: #f1f1f1;
}
</style>

<?php
$titleArray = [1, 2, 3, 4, 5, 6];
$cmsListCopy = $cmsList;
$blockPageBackground = array();
$blockStaticPageBackground = array();
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles([
  "/css/blockcms/cmsEngine.css",
  "/css/blockcms/image-picker/image-picker.css",

  "/js/blockcms/cmsEngine.js",
  "/js/blockcms/fontSelect/fontObj.js",
  "/js/blockcms/image-picker/paintObj.js",
  "/js/blockcms/image-picker/image-picker.min.js",
  "/js/blockcms/showmoreless.js",
  //"/js/cmsBuilder.js"
], $assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(['/plugins/html5sortable/html5sortable.js'], Yii::app()->request->baseUrl);

?>
<?php if (Authorisation::isInterfaceAdmin()) { ?>
    <div class="text-center col-xs-12 no-padding hidden">
      <a class="btn btn-primary openListTpls text tooltips col-xs-12 padding-20" href="javascript:;">
        <i class="fa fa-plus openListIcon"></i> <?php echo Yii::t("common", "Add a block in the page") ?>
      </a>
    </div>
    <div id="toolsBar" class="tools text-left unselectable">

    </div>
<?php } ?>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagecmsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <span class="close" data-dismiss="modal">&times;</span>
    <div class="modal-dialog">
      <img class="modal-content img-responsive" src="" id="viewThis">
    </div>
</div>

<?php
//--------------- A mettre dans une controlleur -------------////
$i = 0;
//sort cmsLIstCopy by updated time and get pageBackground
usort($cmsListCopy, function ($a, $b) {
    return (@$a["updated"] >= @$b["updated"]) ? -1 : 1;
});
foreach ($cmsListCopy as $ke => $va) {
    if (isset($va["blockCmsBgTarget"]) && $va["blockCmsBgTarget"] == "this" . $page) {
      $blockPageBackground = $va;
      break;
    }
}

//this function short array assoc $csmList,important in drag and drop
function shortCmsList($a, $b){
  return (@$a["position"] <= @$b["position"]) ? -1 : 1;
}
usort($cmsList, "shortCmsList");
//--------------- A mettre dans une controlleur -------------////
foreach ($cmsList as $e => $v) {

    // ??????
    if(isset($v["dontRender"]) && $v["dontRender"] == "true"){
      continue;
    }
    $initbackgroundPhoto = Document::getListDocumentsWhere(
        array(
          "id" => (string) $v["_id"],
          "type" => 'cms',
          "subKey" => 'blockCmsBgImg',
        ),
        "image"
    );

    $backgroundPhoto = [];
    foreach ($initbackgroundPhoto as $kbg => $vbg) {
        if (!empty($vbg["imagePath"]))
            $backgroundPhoto[] = $vbg["imagePath"];
        else
            $backgroundPhoto[] = $vbg["imageMediumPath"];
    }
    if (isset($v["path"])) {
        $path = $v["path"];
        $pathExplode = explode('.', $v["path"]);
        $count = count($pathExplode);
        $content = isset($v['content']) ? $v['content'] : [];
        $kunik = $pathExplode[$count - 1] . $v["_id"];
        $blockKey = (string)$v["_id"];
        $blockName = (string)@$v["name"];

        $params = [
          "cmsList"   =>  $cmsList,
          "blockKey"  => $blockKey,
          "blockCms"  =>  $v,
          "page"      =>  $page,
          "canEdit"   =>  $canEdit,
          "type"      =>  $path,
          "kunik"     =>  $kunik,
          "content"   =>  $content,
          "el"        => $el,
          'costum'    => $costum,
          'blockName' => $blockName,
          'range'     => $i,
          "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg"
        ];

        $menuSinglePage = [
          "menuSinglePage" =>  isset($v["menuSinglePage"]) ? $v["menuSinglePage"] : "#welcome"
        ];
        $background = [
          "blockCmsBgTarget" =>  isset($v["blockCmsBgTarget"]) ? $v["blockCmsBgTarget"] : "thisBlock",
          "blockCmsBgType" =>  isset($v["blockCmsBgType"]) ? $v["blockCmsBgType"] : "color",
          "blockCmsBgColor" =>  isset($v["blockCmsBgColor"]) ? $v["blockCmsBgColor"] : "transparent",
          "blockCmsBgImg" =>  isset($v["blockCmsBgImg"]) ? $v["blockCmsBgImg"] : "",
          "blockCmsBgPaint" =>  isset($v["blockCmsBgPaint"]) ? $v["blockCmsBgPaint"] : "",
          "blockCmsBgSize"  =>  isset($v["blockCmsBgSize"]) ? $v["blockCmsBgSize"] : "cover",
          "blockCmsBgPosition"  =>  isset($v["blockCmsBgPosition"]) ? $v["blockCmsBgPosition"] : "center",
          "blockCmsBgRepeat"  =>  isset($v["blockCmsBgRepeat"]) ? $v["blockCmsBgRepeat"] : "repeat",
        ];
        $hasPageBg = (isset($blockPageBackground["_id"]) && $blockKey == (string)$blockPageBackground["_id"]) ? true : false;

        $police = [
          "blockCmsPoliceTitle1" =>  isset($v["blockCmsPoliceTitle1"]) ? $v["blockCmsPoliceTitle1"] : "",
          "blockCmsPoliceTitle2" =>  isset($v["blockCmsPoliceTitle2"]) ? $v["blockCmsPoliceTitle2"] : "",
          "blockCmsPoliceTitle3" =>  isset($v["blockCmsPoliceTitle3"]) ? $v["blockCmsPoliceTitle3"] : "",
          "blockCmsPoliceTitle4" =>  isset($v["blockCmsPoliceTitle4"]) ? $v["blockCmsPoliceTitle4"] : "",
          "blockCmsPoliceTitle5" =>  isset($v["blockCmsPoliceTitle5"]) ? $v["blockCmsPoliceTitle5"] : "",
          "blockCmsPoliceTitle6" =>  isset($v["blockCmsPoliceTitle6"]) ? $v["blockCmsPoliceTitle6"] : ""
        ];

        $textSize = [
          "blockCmsTextSizeTitle1" =>  isset($v["blockCmsTextSizeTitle1"]) ? $v["blockCmsTextSizeTitle1"] : "",
          "blockCmsTextSizeTitle2" =>  isset($v["blockCmsTextSizeTitle2"]) ? $v["blockCmsTextSizeTitle2"] : "",
          "blockCmsTextSizeTitle3" =>  isset($v["blockCmsTextSizeTitle3"]) ? $v["blockCmsTextSizeTitle3"] : "",
          "blockCmsTextSizeTitle4" =>  isset($v["blockCmsTextSizeTitle4"]) ? $v["blockCmsTextSizeTitle4"] : "",
          "blockCmsTextSizeTitle5" =>  isset($v["blockCmsTextSizeTitle5"]) ? $v["blockCmsTextSizeTitle5"] : "",
          "blockCmsTextSizeTitle6" =>  isset($v["blockCmsTextSizeTitle6"]) ? $v["blockCmsTextSizeTitle6"] : ""
        ];

        $lineHeight = [
          "blockCmsTextLineHeightTitle1" =>  isset($v["blockCmsTextLineHeightTitle1"]) ? $v["blockCmsTextLineHeightTitle1"] : "",
          "blockCmsTextLineHeightTitle2" =>  isset($v["blockCmsTextLineHeightTitle2"]) ? $v["blockCmsTextLineHeightTitle2"] : "",
          "blockCmsTextLineHeightTitle3" =>  isset($v["blockCmsTextLineHeightTitle3"]) ? $v["blockCmsTextLineHeightTitle3"] : "",
          "blockCmsTextLineHeightTitle4" =>  isset($v["blockCmsTextLineHeightTitle4"]) ? $v["blockCmsTextLineHeightTitle4"] : "",
          "blockCmsTextLineHeightTitle5" =>  isset($v["blockCmsTextLineHeightTitle5"]) ? $v["blockCmsTextLineHeightTitle5"] : "",
          "blockCmsTextLineHeightTitle6" =>  isset($v["blockCmsTextLineHeightTitle6"]) ? $v["blockCmsTextLineHeightTitle6"] : ""
        ];

        $textAlign = [
          "blockCmsTextAlignTitle1" =>  isset($v["blockCmsTextAlignTitle1"]) ? $v["blockCmsTextAlignTitle1"] : "center",
          "blockCmsTextAlignTitle2" =>  isset($v["blockCmsTextAlignTitle2"]) ? $v["blockCmsTextAlignTitle2"] : "center",
          "blockCmsTextAlignTitle3" =>  isset($v["blockCmsTextAlignTitle3"]) ? $v["blockCmsTextAlignTitle3"] : "center",
          "blockCmsTextAlignTitle4" =>  isset($v["blockCmsTextAlignTitle4"]) ? $v["blockCmsTextAlignTitle4"] : "center",
          "blockCmsTextAlignTitle5" =>  isset($v["blockCmsTextAlignTitle5"]) ? $v["blockCmsTextAlignTitle5"] : "center",
          "blockCmsTextAlignTitle6" =>  isset($v["blockCmsTextAlignTitle6"]) ? $v["blockCmsTextAlignTitle6"] : "center"
        ];

        $textColor = [
          "blockCmsColorTitle1" =>  isset($v["blockCmsColorTitle1"]) ? $v["blockCmsColorTitle1"] : "#808080",
          "blockCmsColorTitle2" =>  isset($v["blockCmsColorTitle2"]) ? $v["blockCmsColorTitle2"] : "#808080",
          "blockCmsColorTitle3" =>  isset($v["blockCmsColorTitle3"]) ? $v["blockCmsColorTitle3"] : "#808080",
          "blockCmsColorTitle4" =>  isset($v["blockCmsColorTitle4"]) ? $v["blockCmsColorTitle4"] : "#808080",
          "blockCmsColorTitle5" =>  isset($v["blockCmsColorTitle5"]) ? $v["blockCmsColorTitle5"] : "#808080",
          "blockCmsColorTitle6" =>  isset($v["blockCmsColorTitle6"]) ? $v["blockCmsColorTitle6"] : "#808080"
        ];

        $aosAnimation = [
          "blockCmsAos" =>  isset($v["blockCmsAos"]) ? $v["blockCmsAos"] : "no animation",
          "blockCmsAosDuration" =>  isset($v["blockCmsAosDuration"]) ? $v["blockCmsAosDuration"] : "3000",
        ];

        //TODO convert to loop
        $textUnderline = [
          "blockCmsUnderlineTitle1" =>  isset($v["blockCmsUnderlineTitle1"]) ? $v["blockCmsUnderlineTitle1"] : "none",
          "blockCmsUnderlineColorTitle1" =>  isset($v["blockCmsUnderlineColorTitle1"]) ? $v["blockCmsUnderlineColorTitle1"] : @$textColor["blockCmsColorTitle1"],
          "blockCmsUnderlineWidthTitle1" =>  isset($v["blockCmsUnderlineWidthTitle1"]) ? $v["blockCmsUnderlineWidthTitle1"] : "300px",
          "blockCmsUnderlineHeightTitle1" =>  isset($v["blockCmsUnderlineHeightTitle1"]) ? $v["blockCmsUnderlineHeightTitle1"] : "3px",
          "blockCmsUnderlineSpaceTitle1" =>  isset($v["blockCmsUnderlineSpaceTitle1"]) ? $v["blockCmsUnderlineSpaceTitle1"] : "10px",
          "blockCmsUnderlineMargeBottomTitle1" =>  isset($v["blockCmsUnderlineMargeBottomTitle1"]) ? $v["blockCmsUnderlineMargeBottomTitle1"] : "30px",

          "blockCmsUnderlineTitle2" =>  isset($v["blockCmsUnderlineTitle2"]) ? $v["blockCmsUnderlineTitle2"] : "none",
          "blockCmsUnderlineColorTitle2" =>  isset($v["blockCmsUnderlineColorTitle2"]) ? $v["blockCmsUnderlineColorTitle2"] : @$textColor["blockCmsColorTitle2"],
          "blockCmsUnderlineWidthTitle2" =>  isset($v["blockCmsUnderlineWidthTitle2"]) ? $v["blockCmsUnderlineWidthTitle2"] : "300px",
          "blockCmsUnderlineHeightTitle2" =>  isset($v["blockCmsUnderlineHeightTitle2"]) ? $v["blockCmsUnderlineHeightTitle2"] : "3px",
          "blockCmsUnderlineSpaceTitle2" =>  isset($v["blockCmsUnderlineSpaceTitle2"]) ? $v["blockCmsUnderlineSpaceTitle2"] : "10px",
          "blockCmsUnderlineMargeBottomTitle2" =>  isset($v["blockCmsUnderlineMargeBottomTitle2"]) ? $v["blockCmsUnderlineMargeBottomTitle2"] : "30px",

          "blockCmsUnderlineTitle3" =>  isset($v["blockCmsUnderlineTitle3"]) ? $v["blockCmsUnderlineTitle3"] : "none",
          "blockCmsUnderlineColorTitle3" =>  isset($v["blockCmsUnderlineColorTitle3"]) ? $v["blockCmsUnderlineColorTitle3"] : @$textColor["blockCmsColorTitle3"],
          "blockCmsUnderlineWidthTitle3" =>  isset($v["blockCmsUnderlineWidthTitle3"]) ? $v["blockCmsUnderlineWidthTitle3"] : "300px",
          "blockCmsUnderlineHeightTitle3" =>  isset($v["blockCmsUnderlineHeightTitle3"]) ? $v["blockCmsUnderlineHeightTitle3"] : "3px",
          "blockCmsUnderlineSpaceTitle3" =>  isset($v["blockCmsUnderlineSpaceTitle3"]) ? $v["blockCmsUnderlineSpaceTitle3"] : "10px",
          "blockCmsUnderlineMargeBottomTitle3" =>  isset($v["blockCmsUnderlineMargeBottomTitle3"]) ? $v["blockCmsUnderlineMargeBottomTitle3"] : "30px",

          "blockCmsUnderlineTitle4" =>  isset($v["blockCmsUnderlineTitle4"]) ? $v["blockCmsUnderlineTitle4"] : "none",
          "blockCmsUnderlineColorTitle4" =>  isset($v["blockCmsUnderlineColorTitle4"]) ? $v["blockCmsUnderlineColorTitle4"] : @$textColor["blockCmsColorTitle4"],
          "blockCmsUnderlineWidthTitle4" =>  isset($v["blockCmsUnderlineWidthTitle4"]) ? $v["blockCmsUnderlineWidthTitle4"] : "300px",
          "blockCmsUnderlineHeightTitle4" =>  isset($v["blockCmsUnderlineHeightTitle4"]) ? $v["blockCmsUnderlineHeightTitle4"] : "3px",
          "blockCmsUnderlineSpaceTitle4" =>  isset($v["blockCmsUnderlineSpaceTitle4"]) ? $v["blockCmsUnderlineSpaceTitle4"] : "10px",
          "blockCmsUnderlineMargeBottomTitle4" =>  isset($v["blockCmsUnderlineMargeBottomTitle4"]) ? $v["blockCmsUnderlineMargeBottomTitle4"] : "30px",

          "blockCmsUnderlineTitle5" =>  isset($v["blockCmsUnderlineTitle5"]) ? $v["blockCmsUnderlineTitle5"] : "none",
          "blockCmsUnderlineColorTitle5" =>  isset($v["blockCmsUnderlineColorTitle5"]) ? $v["blockCmsUnderlineColorTitle5"] : @$textColor["blockCmsColorTitle5"],
          "blockCmsUnderlineWidthTitle5" =>  isset($v["blockCmsUnderlineWidthTitle5"]) ? $v["blockCmsUnderlineWidthTitle5"] : "300px",
          "blockCmsUnderlineHeightTitle5" =>  isset($v["blockCmsUnderlineHeightTitle5"]) ? $v["blockCmsUnderlineHeightTitle5"] : "3px",
          "blockCmsUnderlineSpaceTitle5" =>  isset($v["blockCmsUnderlineSpaceTitle5"]) ? $v["blockCmsUnderlineSpaceTitle5"] : "10px",
          "blockCmsUnderlineMargeBottomTitle5" =>  isset($v["blockCmsUnderlineMargeBottomTitle5"]) ? $v["blockCmsUnderlineMargeBottomTitle5"] : "30px",

          "blockCmsUnderlineTitle6" =>  isset($v["blockCmsUnderlineTitle6"]) ? $v["blockCmsUnderlineTitle6"] : "none",
          "blockCmsUnderlineColorTitle6" =>  isset($v["blockCmsUnderlineColorTitle6"]) ? $v["blockCmsUnderlineColorTitle6"] : @$textColor["blockCmsColorTitle6"],
          "blockCmsUnderlineWidthTitle6" =>  isset($v["blockCmsUnderlineWidthTitle6"]) ? $v["blockCmsUnderlineWidthTitle6"] : "300px",
          "blockCmsUnderlineHeightTitle6" =>  isset($v["blockCmsUnderlineHeightTitle6"]) ? $v["blockCmsUnderlineHeightTitle6"] : "3px",
          "blockCmsUnderlineSpaceTitle6" =>  isset($v["blockCmsUnderlineSpaceTitle6"]) ? $v["blockCmsUnderlineSpaceTitle6"] : "10px",
          "blockCmsUnderlineMargeBottomTitle6" =>  isset($v["blockCmsUnderlineMargeBottomTitle6"]) ? $v["blockCmsUnderlineMargeBottomTitle6"] : "30px",

        ];

        $width = [
          "modeLg" =>  isset($v["modeLg"]) ? $v["modeLg"] : "12",
          "modeMd" =>  isset($v["modeMd"]) ? $v["modeMd"] : "12",
          "modeSm" =>  isset($v["modeSm"]) ? $v["modeSm"] : "12",
          "modeXs" =>  isset($v["modeXs"]) ? $v["modeXs"] : "12"
        ];

        $margin = [
          "marginTop" =>  isset($v["marginTop"]) ? $v["marginTop"] : "0",
          "marginBottom" =>  isset($v["marginBottom"]) ? $v["marginBottom"] : "0",
          "marginLeft" =>  isset($v["marginLeft"]) ? $v["marginLeft"] : "0",
          "marginRight" =>  isset($v["marginRight"]) ? $v["marginRight"] : "0"
        ];

        $padding = [
          "paddingTop" =>  isset($v["paddingTop"]) ? $v["paddingTop"] : "0",
          "paddingBottom" =>  isset($v["paddingBottom"]) ? $v["paddingBottom"] : "0",
          "paddingLeft" =>  isset($v["paddingLeft"]) ? $v["paddingLeft"] : "0",
          "paddingRight" =>  isset($v["paddingRight"]) ? $v["paddingRight"] : "0"
        ];

        $border = [
          "blockCmsBorderTop" =>  isset($v["blockCmsBorderTop"]) ? $v["blockCmsBorderTop"] : false,
          "blockCmsBorderBottom" =>  isset($v["blockCmsBorderBottom"]) ? $v["blockCmsBorderBottom"] : false,
          "blockCmsBorderLeft" =>  isset($v["blockCmsBorderLeft"]) ? $v["blockCmsBorderLeft"] : false,
          "blockCmsBorderRight" =>  isset($v["blockCmsBorderRight"]) ? $v["blockCmsBorderRight"] : false,
          "blockCmsBorderColor" =>  isset($v["blockCmsBorderColor"]) ? $v["blockCmsBorderColor"] : "#ff007f",
          "blockCmsBorderWidth" =>  isset($v["blockCmsBorderWidth"]) ? $v["blockCmsBorderWidth"] : "5",
          "blockCmsBorderType" =>  isset($v["blockCmsBorderType"]) ? $v["blockCmsBorderType"] : "solid"
        ];

        $lineSeparator = [
          "blockCmsLineSeparatorTop" =>  isset($v["blockCmsLineSeparatorTop"]) ? $v["blockCmsLineSeparatorTop"] : false,
          "blockCmsLineSeparatorBottom" =>  isset($v["blockCmsLineSeparatorBottom"]) ? $v["blockCmsLineSeparatorBottom"] : false,
          "blockCmsLineSeparatorBg" =>  isset($v["blockCmsLineSeparatorBg"]) ? $v["blockCmsLineSeparatorBg"] : "#ff007f",
          "blockCmsLineSeparatorWidth" =>  isset($v["blockCmsLineSeparatorWidth"]) ? $v["blockCmsLineSeparatorWidth"] : "70",
          "blockCmsLineSeparatorHeight" =>  isset($v["blockCmsLineSeparatorHeight"]) ? $v["blockCmsLineSeparatorHeight"] : "5",
          "blockCmsLineSeparatorPosition" =>  isset($v["blockCmsLineSeparatorPosition"]) ? $v["blockCmsLineSeparatorPosition"] : "center",
          "blockCmsLineSeparatorIcon"  => isset($v["blockCmsLineSeparatorIcon"]) ? $v["blockCmsLineSeparatorIcon"] : "align-justify",
        ];

        if (is_file($this->getViewFile("costum.views." . $path, $params))) { ?>
          <div>
              <script type="text/javascript">
                <?php if (Authorisation::isInterfaceAdmin()) { ?>
                  setTimeout(function() {
                    if (exists(sectionDyf.<?php echo $kunik ?>ParamsData)) {
                        <?php foreach ($background as $key => $value) {
                          if ($key != "blockCmsBgImg") ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($police as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($textAlign as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($textColor as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($width as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($margin as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($padding as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($border as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($lineSeparator as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($textSize as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($textUnderline as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($lineHeight as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($menuSinglePage as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>
                        <?php foreach ($aosAnimation as $key => $value) { ?>
                          sectionDyf.<?php echo $kunik ?>ParamsData.<?php echo $key ?> = "<?php echo $value ?>";
                        <?php } ?>

                    }
                  }, 900)
                <?php } ?>

                $(function() {
                  <?php if (Authorisation::isInterfaceAdmin()) { ?>
                    setTimeout(function() {
                      var valueKunik = "<?php echo $kunik ?>";
                      var hasJsonProperties = false;
                      if (typeof sectionDyf[valueKunik + 'Params'] != "undefined" &&
                        typeof sectionDyf[valueKunik + 'Params'].jsonSchema != "undefined" &&
                        typeof sectionDyf[valueKunik + 'Params'].jsonSchema.properties != "undefined"
                      )
                        hasJsonProperties = true;

                      if(hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema["onLoads"] = {
                            onload: function() {
                              var blockCmsBgType = <?= json_encode(@$background["blockCmsBgType"]) ?>;
                              if (blockCmsBgType == "image") {
                                $('.<?= "blockCmsBgColor-" . $kunik ?>').parent().hide();
                                $('.<?= "blockCmsBgPaint-" . $kunik ?>').parent().hide();
                              }
                              if (blockCmsBgType == "color") {
                                $('.<?= "blockCmsBgImg-" . $kunik ?>').parent().css({'visibility':'hidden','height' : '0'});
                                $('.<?= "blockCmsBgPaint-" . $kunik ?>').parent().hide();
                                $('.<?= "blockCmsBgSize-" . $kunik ?>,.<?= "blockCmsBgPosition-" . $kunik ?>,.<?= "blockCmsBgRepeat-" . $kunik ?>').parent().hide();
                              }
                              if (blockCmsBgType == "paint") {
                                $('.<?= "blockCmsBgImg-" . $kunik ?>').parent().css({'visibility':'hidden','height' : '0'});
                                $('.<?= "blockCmsBgColor-" . $kunik ?>').parent().hide();
                              }

                              $('#btn-submit-form').addClass("btn-block btn-lg bg-green-k letter-white").removeClass("letter-green").css({"margin-left":"0","margin-right":"5","order": "3"});
                              $('.mainDynFormCloseBtn').addClass('btn-block btn-lg').css({'margin-right':"5px",'margin-top':"0"});
                              $('.form-actions').css({"display":"flex","margin-left":"0"});
                              $('.form-action hr').remove();
                            }
                          }

                      /* choose block cms for anchor***********************/
                      if (hasAnchor == true) {
                        $.each({
                          menuSinglePage: "Connecter au menu"
                        }, function(k, v) {
                          if (hasJsonProperties)
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: pageOptions,
                              values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                            }
                        });
                      }
                      /* end choose block cms for anchor*******************/

                      /*background*****************************************************/
                      var hasPageBg = "<?= $hasPageBg ?>";
                      <?php if (isset($blockPageBackground["_id"]) && $blockKey == (string)$blockPageBackground["_id"]) { ?>
                        $('.block-container-<?= $kunik ?>').append('<div class="hiddenPreview bg-support">support d\'arrière-plan</div>');
                      <?php }; ?>
                      $.each(blockCmsBgLabel, function(k, v) {
                        if (hasJsonProperties) {
                          if (k == "blockCmsBgTarget")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                'thisBlock': '<?php echo Yii::t("cms", "This block only")?>',
                                'this<?= $page ?>': '<?php echo Yii::t("cms", "All blocks on this page")?>',
                                /*"staticPage" : "Ce bloc et toutes les pages statiques"*/
                              },
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) && hasPageBg == "1" ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "thisBlock"

                            };
                          if (k == "blockCmsBgType")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "color": "<?php echo Yii::t('cms', 'Color')?>",
                                "image": "<?php echo Yii::t('cms', 'Image')?>",
                                "paint": "<?php echo Yii::t('cms', 'Paperpaint')?>"
                              },
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "color"
                            };
                          if (k == "blockCmsBgSize")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "cover": "<?php echo Yii::t('cms', 'Cover')?>",
                                "contain": "<?php echo Yii::t('cms', 'Contain')?>",
                                "10%": "10%",
                                "20%": "20%",
                                "30%": "30%",
                                "40%": "40%",
                                "50%": "50%",
                                "60%": "60%",
                                "70%": "70%",
                                "80%": "80%",
                                "90%": "90%",
                                "100%": "100%"
                              },
                              class: "<?= "blockCmsBgSize-" . $kunik ?>",
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "color"
                            };
                          if (k == "blockCmsBgPosition")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "left": "<?php echo Yii::t('cms', 'Left')?>",
                                "left top": "<?php echo Yii::t('cms', 'Left top')?>",
                                "left bottom": "<?php echo Yii::t('cms', 'Left bottom')?>",
                                "right": "<?php echo Yii::t('cms', 'Right')?>",
                                "right top": "<?php echo Yii::t('cms', 'Right')?>",
                                "right bottom": "<?php echo Yii::t('cms', 'Right top')?>",
                                "center": "<?php echo Yii::t('cms', 'Center')?>",
                                "center top": "<?php echo Yii::t('cms', 'Top center')?>",
                                "center bottom": "<?php echo Yii::t('cms', 'Bottom center')?>",
                              },
                              class: "<?= "blockCmsBgPosition-" . $kunik ?>",
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "color"
                            };
                          if (k == "blockCmsBgRepeat")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "repeat": "<?php echo Yii::t('cms', 'Repeat')?>",
                                "no-repeat": "<?php echo Yii::t('cms', 'Do not repeat')?>"
                              },
                              class: "<?= "blockCmsBgRepeat-" . $kunik ?>",
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "color"
                            };

                          if (k == "blockCmsBgColor") {
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "colorpicker",
                              class: "<?= "blockCmsBgColor-" . $kunik ?>",
                              values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                            };
                          }
                          if (k == "blockCmsBgImg") {
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              "inputType": "uploader",
                              "label": v,
                              "domElement": "blockCmsBgImg",
                              "docType": "image",
                              "contentKey": "slider",
                              "itemLimit": 1,
                              "filetypes": ["jpeg", "jpg", "gif", "png"],
                              "showUploadBtn": false,
                              "class": "<?= "blockCmsBgImg-" . $kunik ?>",
                              "endPoint": "/subKey/blockCmsBgImg",
                              initList: <?php echo json_encode($initbackgroundPhoto) ?>,

                            };
                          };
                          if (k == "blockCmsBgPaint") {
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              class: "<?= "blockCmsBgPaint-" . $kunik ?>",
                              options: paintObj,
                              noOrder: true,
                              isImagepicker: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                            };
                          }
                        }
                      });
                      /*end background*******************************************************/

                      /*animation**************************************************************/
                      $.each(blockCmsScrollAnimation, function(k, v) {
                          if (hasJsonProperties){
                              if (k == "blockCmsAos")
                                  sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                                      label: v,
                                      inputType: "select",
                                      options:aosAnimation,
                                      values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "no animation"
                                  }
                              if (k == "blockCmsAosDuration")
                                  sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                                      label: v,
                                      inputType: "text",
                                      value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "3000"
                                  }
                          }
                      });
                      /*end animation**********************************************************/

                      /*police***************************************************************/
                      $.each(blockCmsPoliceLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "select",
                            "options": exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ?
                              (JSON.parse(`{"${sectionDyf.<?php echo $kunik ?>ParamsData[k]}":"${sectionDyf.<?php echo $kunik ?>ParamsData[k].split(".ttf")[0]}"}`)) : {},
                            "isPolice": true,
                            noOrder: true,
                            value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end police**********************************************************/

                      /*color**************************************************************/
                      $.each(blockCmsColorLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "colorpicker",
                            values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "#808080"
                          }
                      });
                      /*end color**********************************************************/

                      /*text size***************************************************************/
                      $.each(blockCmsTextSizeLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "text",
                            rules: {
                              number: true,
                              min: 0,
                            },
                            value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end text size**********************************************************/

                      /*line height ***************************************************************/
                      $.each(blockCmsTextLineHeightLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "text",
                            placeholder: "1.5 <?php echo Yii::t('cms', 'or')?> 2px <?php echo Yii::t('cms', 'or')?> 20%",
                            value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end line height **********************************************************/

                      /*alignement**************************************************************/
                      $.each(blockCmsTextAlignLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "select",
                            options: {
                              "left": "<?php echo Yii::t('cms', 'Left')?>",
                              "right": "<?php echo Yii::t('cms', 'Right')?>",
                              "center": "<?php echo Yii::t('cms', 'Center')?>",
                              "justify": "<?php echo Yii::t('cms', 'Justified')?>"
                            },
                            noOrder: true,
                            values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end alignement**********************************************************/

                      /*soulignement**************************************************************/
                      $.each(blockCmsTextUnderlineLabel, function(k, v) {
                        if (hasJsonProperties) {
                          if (k == "blockCmsUnderlineTitle1" || k == "blockCmsUnderlineTitle2" || k == "blockCmsUnderlineTitle3" || k == "blockCmsUnderlineTitle4" || k == "blockCmsUnderlineTitle5" || k == "blockCmsUnderlineTitle6")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "none": "<?php echo Yii::t('cms', 'Do not underline')?>",
                                "underline": "<?php echo Yii::t('cms', 'Underline')?>"
                              },
                              noOrder: true,
                              values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "none"
                            };
                          else if (k == "blockCmsUnderlineColorTitle1" || k == "blockCmsUnderlineColorTitle2" || k == "blockCmsUnderlineColorTitle3" || k == "blockCmsUnderlineColorTitle4" || k == "blockCmsUnderlineColorTitle5" || k == "blockCmsUnderlineColorTitle6")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "colorpicker",
                              values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "#ffffff"
                            };
                          else
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "text",
                              values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                            };

                        }
                      });
                      /*end soulignement**********************************************************/

                      /*border************************************************************/
                      $.each(blockCmsBorderLabel, function(k, v) {
                        if (hasJsonProperties) {
                          if (k == "blockCmsBorderTop" || k == "blockCmsBorderBottom" || k == "blockCmsBorderLeft" || k == "blockCmsBorderRight")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "checkboxSimple",
                              params: checkboxSimpleParams,
                              checked: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : false
                            };
                          else if (k == "blockCmsBorderColor")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "colorpicker",
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "#808080"
                            };
                          else if (k == "blockCmsBorderWidth")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "text",
                              rules: {
                                number: true,
                                min: 0
                              },
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "0"
                            };
                          else if (k == "blockCmsBorderType")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "dashed": "dashed",
                                "dotted": "dotted",
                                "double": "double",
                                "groove": "groove",
                                "inset": "inset",
                                "ridge": "ridge",
                                "solid": "solid",
                                "outset": "outset"
                              },
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "solid"
                            };
                        }
                      });
                      /*end border*******************************************************/

                      /*line séparator***************************************************/
                      $.each(blockCmsLineSeparatorLabel, function(k, v) {
                        if (hasJsonProperties) {
                          if (k == "blockCmsLineSeparatorTop" || k == "blockCmsLineSeparatorBottom")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "checkboxSimple",
                              params: checkboxSimpleParams,
                              checked: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : false
                            };
                          else if (k == "blockCmsLineSeparatorWidth" || k == "blockCmsLineSeparatorHeight")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "text",
                              rules: {
                                number: true,
                                min: 0,
                                max: 100
                              },
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "0"
                            };
                          else if (k == "blockCmsLineSeparatorBg")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "colorpicker",
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "#ff007f"
                            };
                          else if (k == "blockCmsLineSeparatorPosition")
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              label: v,
                              inputType: "select",
                              options: {
                                "left": "<?php echo Yii::t('cms', 'Left')?>",
                                "center": "<?php echo Yii::t('cms', 'Center')?>",
                                "right": "<?php echo Yii::t('cms', 'Right')?>"
                              },
                              noOrder: true,
                              value: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : "solid"
                            };
                          else if (k == "blockCmsLineSeparatorIcon") {
                            var icon<?= $kunik ?> = {};
                            <?php if (!empty($lineSeparator["blockCmsLineSeparatorIcon"])) { ?>
                              icon<?= $kunik ?> = {
                                "<?= $lineSeparator["blockCmsLineSeparatorIcon"] ?>": "<?= $lineSeparator["blockCmsLineSeparatorIcon"] ?>"
                              }
                            <?php } ?>
                            sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                              "inputType": "select",
                              "label": "<?php echo Yii::t('cms', 'Icon in the center of the line')?>",
                              "options": icon<?= $kunik ?>,
                              //"noOrder": true,
                              value: "<?= $lineSeparator["blockCmsLineSeparatorIcon"] ?>"
                            };
                          }
                        }
                      });
                      /*end line séparator***********************************************/

                      /*width**************************************************************/
                      $.each(widthLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "text",
                            rules: {
                              number: true,
                              max: 12
                            },
                            values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end width**********************************************************/

                      /*margin*************************************************************/
                      $.each(marginLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "text",
                            rules: {
                              number: true,
                            },
                            values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end margin*********************************************************/

                      /*padding************************************************************/
                      $.each(paddingLabel, function(k, v) {
                        if (hasJsonProperties)
                          sectionDyf[valueKunik + 'Params'].jsonSchema.properties[k] = {
                            label: v,
                            inputType: "text",
                            rules: {
                              number: true,
                            },
                            values: exists(sectionDyf.<?php echo $kunik ?>ParamsData[k]) ? sectionDyf.<?php echo $kunik ?>ParamsData[k] : ""
                          }
                      });
                      /*end padding*******************************************************/
                    }, 1000)
                  <?php } ?>

                  /*generate title underline******************/
                  <?php $oldClass = "";
                  foreach ($titleArray as $kt => $vt) {
                    if ($vt == 1) $oldClass = ".block-container-" . $kunik . " .title,";
                    elseif ($vt == 2) $oldClass = ".block-container-" . $kunik . " .subtitle,";
                    elseif ($vt == 3) $oldClass = ".block-container-" . $kunik . " .description,";
                    elseif ($vt == 4) $oldClass = ".block-container-" . $kunik . " .other,";
                  ?>
                    //foreach content---------------------------------------------------------------------
                    <?php if (isset($textUnderline["blockCmsUnderlineTitle" . $vt]) && $textUnderline["blockCmsUnderlineTitle" . $vt] == "underline") { ?>
                      if ($("<?= substr($oldClass, 0, -1); ?>").text().trim() !== '' || $('.block-container-<?= $kunik ?> .title-<?= $vt ?>').text().trim() !== '') {
                        <?php if (isset($textAlign["blockCmsTextAlignTitle" . $vt]) && $textAlign["blockCmsTextAlignTitle" . $vt] == "center") { ?>
                          $("<?= $oldClass ?> .block-container-<?= $kunik ?> .title-<?= $vt ?>").after(`
                                <div class="underline <?= $kunik ?>" style="position:relative;background:<?= $textUnderline["blockCmsUnderlineColorTitle" . $vt] ?>;width:<?= $textUnderline["blockCmsUnderlineWidthTitle" . $vt] ?>;height:<?= $textUnderline["blockCmsUnderlineHeightTitle" . $vt] ?>;left:50%;transform:translateX(-50%);margin-bottom:<?= $textUnderline["blockCmsUnderlineMargeBottomTitle" . $vt] ?>;margin-top:<?= $textUnderline["blockCmsUnderlineSpaceTitle" . $vt] ?>"></div>
                              `);
                        <?php } ?>
                        <?php if (isset($textAlign["blockCmsTextAlignTitle" . $vt]) && $textAlign["blockCmsTextAlignTitle" . $vt] == "right") { ?>
                          $("<?= $oldClass ?> .block-container-<?= $kunik ?> .title-<?= $vt ?>").after(`
                                <div class="underline <?= $kunik ?>" style="position:relative;background:<?= $textUnderline["blockCmsUnderlineColorTitle" . $vt] ?>;width:<?= $textUnderline["blockCmsUnderlineWidthTitle" . $vt] ?>;height:<?= $textUnderline["blockCmsUnderlineHeightTitle" . $vt] ?>;left:100%;transform:translateX(-100%);margin-bottom:<?= $textUnderline["blockCmsUnderlineMargeBottomTitle" . $vt] ?>;margin-top:<?= $textUnderline["blockCmsUnderlineSpaceTitle" . $vt] ?>"></div>
                              `);
                        <?php } ?>
                        <?php if (isset($textAlign["blockCmsTextAlignTitle" . $vt]) && $textAlign["blockCmsTextAlignTitle" . $vt] == "left") { ?>
                          $("<?= $oldClass ?> .block-container-<?= $kunik ?> .title-<?= $vt ?>").after(`
                                <div class="underline <?= $kunik ?>" style="position:relative;background:<?= $textUnderline["blockCmsUnderlineColorTitle" . $vt] ?>;width:<?= $textUnderline["blockCmsUnderlineWidthTitle" . $vt] ?>;height:<?= $textUnderline["blockCmsUnderlineHeightTitle" . $vt] ?>;margin-bottom:<?= $textUnderline["blockCmsUnderlineMargeBottomTitle" . $vt] ?>;margin-top:<?= $textUnderline["blockCmsUnderlineSpaceTitle" . $vt] ?>"></div>
                              `);
                        <?php } ?>
                      }
                    <?php } ?>
                    //end foreach content---------------------------------------------------------------------
                  <?php } ?>
                  /*end generate title underline***************/
                })

                <?php if (Authorisation::isInterfaceAdmin()) { ?>
                  if (exists(itemClassArray))
                    itemClassArray.push('.sortable-<?= $kunik ?>');
                <?php } ?>
              </script>
              <?php // var_dump($menuSinglePage["menuSinglePage"]);
              ?>

           

              <div id="<?= $v["_id"] . "-" . $i ?>" class="row sortable-<?= $kunik ?> custom-block-cms" style="margin:0 !important;padding: 0 !important" data-id="<?= $v["_id"]?>">
                <div class="block-parent col-lg-<?= $width["modeLg"] ?> col-md-<?= $width["modeMd"] ?> col-sm-<?= $width["modeSm"] ?> col-xs-<?= $width["modeXs"] ?> block-container-<?= $kunik ?> <?= $menuSinglePage["menuSinglePage"] ?> sp-bg" data-field="blockCmsBgColor" data-value="<?= @$v['blockCmsBgColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>" id="<?= substr($menuSinglePage["menuSinglePage"], 1); ?>" data-id="<?= $v["_id"] ?>" data-path="<?= $v["path"] ?>" data-collection="cms" data-aos="<?= $aosAnimation["blockCmsAos"] ?>" data-aos-duration="<?= $aosAnimation["blockCmsAosDuration"] ?>">
                  <div class="<?= $v["_id"] ?>-load sp-is-loading" data-id='<?= $v["_id"] ?>'data-el="<?php json_encode($el) ?>" data-costum='<?php json_encode($costum) ?>' data-page='<?= $v["page"] ?>' data-path='<?= $v["path"] ?>' data-kunik='<?= $kunik ?>' style="width: 100%;height: 100%;position: absolute;"></div>
                  <!-- <?php// echo $this->renderPartial("costum.views." . $path, $params); ?> -->
                  <?php if (Authorisation::isInterfaceAdmin()) { ?>
                    <div class="handleDrag hiddenPreview tooltips" data-toggle='tooltip' data-placement="left" data-original-title="<?php echo Yii::t('cms', 'Hold and drag your mouse to change the order of your block!')?>">
                      <i class="fa fa-2x fa-arrows"></i>
                    </div>
                    <div class="editSectionBtns">
                      <?php
                      echo $this->renderPartial("costum.views.tpls.editTplBtns", ["path" => $v["path"],"blockName" => $blockName, "canEdit" => $canEdit, "kunik" => $kunik, "page" => @$page, "id" => (string) $v["_id"],"order" => $i]);
                      ?>
                    </div>
                  <?php } ?>

                  <!-- line separator beetwen bloc CMS------>
                  <?php
                  $posLineSep = "";
                  if (@$lineSeparator["blockCmsLineSeparatorPosition"] == "left")
                    $posLineSep = "left:0";
                  if (@$lineSeparator["blockCmsLineSeparatorPosition"] == "center")
                    $posLineSep = "left:50%";
                  if (@$lineSeparator["blockCmsLineSeparatorPosition"] == "right")
                    $posLineSep = "left:100%";

                  if (@$lineSeparator["blockCmsLineSeparatorTop"] == "true" || @$lineSeparator["blockCmsLineSeparatorBottom"] == "true")
                    $img = Document::getLastImageByKey((string) $v["_id"], "cms", "profil", "blockCmsLineSeparatorIcon");


                  ?>
                  <?php if (@$lineSeparator["blockCmsLineSeparatorTop"] == "true") { ?>
                    <div style="position:absolute;
                              <?= $posLineSep ?>;
                              top:0px;
                              transform:translate(-50%,-50%);
                              background:<?= @$lineSeparator["blockCmsLineSeparatorBg"] ?>;
                              width:<?= @$lineSeparator["blockCmsLineSeparatorWidth"] ?>%;
                              height:<?= @$lineSeparator["blockCmsLineSeparatorHeight"] ?>px;">
                      <?php if (!empty($lineSeparator["blockCmsLineSeparatorIcon"])) { ?>
                        <i class="fa fa-<?= $lineSeparator["blockCmsLineSeparatorIcon"] ?>  ?>" style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);padding: 0 5px 0 5px;background: white;color:<?= @$lineSeparator["blockCmsLineSeparatorBg"] ?>;
                              "></i>
                      <?php } ?>
                    </div>
                  <?php } ?>
                  <?php if (@$lineSeparator["blockCmsLineSeparatorBottom"] == "true") { ?>
                    <div style="position:absolute;
                              <?= $posLineSep ?>;
                              top:100%;
                              transform:translate(-50%,-50%);
                              background:<?= @$lineSeparator["blockCmsLineSeparatorBg"] ?>;
                              width:<?= @$lineSeparator["blockCmsLineSeparatorWidth"] ?>%;
                              height:<?= @$lineSeparator["blockCmsLineSeparatorHeight"] ?>px;">
                      <?php if (!empty($lineSeparator["blockCmsLineSeparatorIcon"])) { ?>
                        <i class="fa fa-<?= $lineSeparator["blockCmsLineSeparatorIcon"] ?>" style="position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);padding: 0 5px 0 5px;background: white;color:<?= @$lineSeparator["blockCmsLineSeparatorBg"] ?>;
                              "></i>
                      <?php } ?>
                    </div>
                  <?php } ?>
                  <!--end line separator beetwen bloc CMS------>
                </div>
              </div>
              <style>
                  /*Police ,color,size and alignement css***********************************************/
                  <?php $oldClass = "";
                  foreach ($titleArray as $ktt => $vtt) {
                    if ($vtt == 1) $oldClass = ".block-container-" . $kunik . " .title,.block-container-" . $kunik . " .title p,.block-container-" . $kunik . " .title p a,";
                    elseif ($vtt == 2) $oldClass = ".block-container-" . $kunik . " .subtitle,.block-container-" . $kunik . " .subtitle p,.block-container-" . $kunik . " .subtitle p a,";
                    elseif ($vtt == 3) $oldClass = ".block-container-" . $kunik . " .description,.block-container-" . $kunik . " .description *,.block-container-" . $kunik . " .description * a,";
                    elseif ($vtt == 4) $oldClass = ".block-container-" . $kunik . " .other,.block-container-" . $kunik . " .other *,.block-container-" . $kunik . " .other * a,";
                    else $oldClass = "";
                  ?><?= $oldClass ?> .block-container-<?= $kunik ?> .title-<?= $vtt ?>,
                  .block-container-<?= $kunik ?> .title-<?= $vtt ?> p,
                  .block-container-<?= $kunik ?> .title-<?= $vtt ?> p a {
                    <?php if (isset($police["blockCmsPoliceTitle" . $vtt]) && !empty($police["blockCmsPoliceTitle" . $vtt])) { ?>font-family: '<?= @join("_", explode(" ", explode(".ttf", @$police["blockCmsPoliceTitle" . $vtt])[0])) ?>' !important;
                    <?php } ?>text-align: <?= @$textAlign["blockCmsTextAlignTitle" . $vtt] ?> !important;
                    color: <?= @$textColor["blockCmsColorTitle" . $vtt] ?> !important;
                    font-size: <?= @$textSize["blockCmsTextSizeTitle" . $vtt] ?>px !important;
                    line-height: <?= @$lineHeight["blockCmsTextLineHeightTitle" . $vtt] ?> !important;
                    text-transform: none !important;
                  }

                  <?php } ?><?php if (Authorisation::isInterfaceAdmin()) { ?>.block-container-<?= $kunik ?> .title:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-1:not(:empty):before {
                    content: "[Titre 1]";
                  }

                  .block-container-<?= $kunik ?> .subtitle:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-2:not(:empty):before {
                    content: "[Titre 2]";
                  }

                  .block-container-<?= $kunik ?> .description:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-3:not(:empty):before {
                    content: "[Titre 3]";
                  }

                  .block-container-<?= $kunik ?> .other:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-4:not(:empty):before {
                    content: "[Titre 4]";
                  }

                  .block-container-<?= $kunik ?> .title-5:not(:empty):before {
                    content: "[Titre 5]";
                  }

                  .block-container-<?= $kunik ?> .title-6:not(:empty):before {
                    content: "[Titre 6]";
                  }

                  .block-container-<?= $kunik ?>:hover .title:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .title-1:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .subtitle:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .title-2:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .description:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .title-3:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .other:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .title-4:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .title-5:not(:empty):before,
                  .block-container-<?= $kunik ?>:hover .title-6:not(:empty):before {
                    display: inline-block;
                  }

                  .block-container-<?= $kunik ?> .title:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-1:not(:empty):before,
                  .block-container-<?= $kunik ?> .subtitle:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-2:not(:empty):before,
                  .block-container-<?= $kunik ?> .description:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-3:not(:empty):before,
                  .block-container-<?= $kunik ?> .other:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-4:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-5:not(:empty):before,
                  .block-container-<?= $kunik ?> .title-6:not(:empty):before {
                    display: none;
                    font-size: 12px;
                    color: red;
                    background: rgb(255, 255, 255, 100%);
                    border-radius: 3px;
                    font-family: 'arial';
                    padding: 2px 3px;
                  }

                  <?php } ?>
                  /*End police,color,size and alignement css***********************************************/

                  /*border css*****************************************/
                  <?php if (@$border["blockCmsBorderTop"] == "true") { ?>.block-container-<?= $kunik ?> {
                    border-top: <?= @$border["blockCmsBorderWidth"] . 'px ' . @$border["blockCmsBorderType"] . ' ' . @$border["blockCmsBorderColor"] ?>;
                  }

                  <?php } ?><?php if (@$border["blockCmsBorderBottom"] == "true") { ?>.block-container-<?= $kunik ?> {
                    border-bottom: <?= @$border["blockCmsBorderWidth"] . 'px ' . @$border["blockCmsBorderType"] . ' ' . @$border["blockCmsBorderColor"] ?>;
                  }

                  <?php } ?><?php if (@$border["blockCmsBorderLeft"] == "true") { ?>.block-container-<?= $kunik ?> {
                    border-left: <?= @$border["blockCmsBorderWidth"] . 'px ' . @$border["blockCmsBorderType"] . ' ' . @$border["blockCmsBorderColor"] ?>;
                  }

                  <?php } ?><?php if (@$border["blockCmsBorderRight"] == "true") { ?>.block-container-<?= $kunik ?> {
                    border-right: <?= @$border["blockCmsBorderWidth"] . 'px ' . @$border["blockCmsBorderType"] . ' ' . @$border["blockCmsBorderColor"] ?>;
                  }

                  <?php } ?>
                  /*End border css*****************************************/

                  /*container style and background*************************/
                  <?php if (isset($blockPageBackground["_id"]) && $blockKey == (string)$blockPageBackground["_id"]) { ?>#all-block-container {
                    <?php if (@$background["blockCmsBgType"] == "image") {
                      //var_dump($backgroundPhoto);
                    ?><?php if (count($backgroundPhoto) != 0) {  ?>background-image: url(<?php echo $backgroundPhoto[0]; ?>) !important;
                    <?php } ?>background-size: contain !important;
                    background-position: center !important;
                    background-repeat: repeat;
                    !important;
                    <?php } elseif (@$background["blockCmsBgType"] == "color") { ?>background: <?= @$background["blockCmsBgColor"] ?> !important;
                    <?php } elseif (@$background["blockCmsBgType"] == "paint") { ?>background-image: url(<?php echo $assetsUrl; ?>/images/blockCmsPaint/<?= @$background["blockCmsBgPaint"] ?>) !important;
                    background-size: contain !important;
                    background-position: <?= @$background["blockCmsBgPosition"] ?> !important;
                    background-repeat: repeat;
                    !important;
                    <?php } ?>
                  }

                  <?php } ?>.block-container-<?= $kunik ?> {
                    min-height: 200px;
                    margin-top: <?= $margin["marginTop"] ?>px;
                    margin-bottom: <?= $margin["marginBottom"] ?>px;
                    padding-top: <?= $padding["paddingTop"] ?>px;
                    padding-bottom: <?= $padding["paddingBottom"] ?>px;
                    padding-left: <?= $padding["paddingLeft"] ?>px;
                    padding-right: <?= $padding["paddingRight"] ?>px;
                    <?php if (@$background["blockCmsBgType"] == "image") { ?><?php if (count($backgroundPhoto) != 0) {  ?>background-image: url(<?php echo $backgroundPhoto[0]; ?>) !important;
                    <?php } ?>background-size: <?= @$background["blockCmsBgSize"] ?> !important;
                    background-position: <?= @$background["blockCmsBgPosition"] ?> !important;
                    background-repeat: <?= @$background["blockCmsBgRepeat"] ?> !important;
                    background: <?= @$background["blockCmsBgColor"] ?>;
                    <?php } elseif (@$background["blockCmsBgType"] == "color") { ?>background: <?= @$background["blockCmsBgColor"] ?> ;
                    <?php } elseif (@$background["blockCmsBgType"] == "paint") { ?>background-image: url(<?php echo $assetsUrl; ?>/images/blockCmsPaint/<?= @$background["blockCmsBgPaint"] ?>) !important;
                    background-size: <?= @$background["blockCmsBgSize"] ?> !important;
                    background-position: <?= @$background["blockCmsBgPosition"] ?> !important;
                    background-repeat: <?= @$background["blockCmsBgRepeat"] ?> !important;
                    background: <?= @$background["blockCmsBgColor"] ?>;
                    <?php } ?>
                  }

                  @media (min-width: 922px) {
                    .block-container-<?= $kunik ?> {
                      margin-left: <?= $margin["marginLeft"] ?>px;
                      margin-right: <?= $margin["marginRight"] ?>px;
                      padding-left: <?= $padding["paddingLeft"] ?>px;
                      padding-right: <?= $padding["paddingRight"] ?>px;
                    }
                  }

                  @media (max-width: 765px) {
                    .block-container-<?= $kunik ?> {
                      padding-left: calc(<?= $padding["paddingLeft"] ?>px/2);
                      padding-right: calc(<?= $padding["paddingRight"] ?>px/2);
                    }
                  }

                  @media (max-width: 500px) {
                    .block-container-<?= $kunik ?> {
                      padding-left: 3px;
                      padding-right: 3px;
                    }
                  }

                  /*end container style and background*************************/

                  /* by ifaliana arimanana */
                  /*Tools bar for edit cms******************/

                  #toolsBar {
                      left: 30%;
                      top: 10%;
                      width: 700px;
                      z-index: 100000;
                      position: fixed;
                      display: none;
                      min-height: 60px;
                      box-shadow: 0px 0px 4px 0px #777;
                      resize: vertical;
                  }




                  /*end tools bar for edit cms******************/


              </style>
          </div>
        <?php
        } else { ?>
          <div class="col-xs-12 text-center" id="<?php echo (string)$v["_id"]; ?>">
            <?php echo $this->renderPartial("costum.views.tpls.blockNotFound", ["blockKey" => $blockKey]) ?>
          </div>
        <?php
        }
    }
    $i++;
} ?>


<!-- <div class="tooltipSpCms">Clique pour modifier</div> -->
<script type="text/javascript">
  $('.stop-propagation').on("click", "input",function() {
    $(this).focus();
  });

  var pageCms = <?php echo json_encode($page); ?>;
  var contextData = <?php echo json_encode($el); ?>;
  var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
  var contextType = <?php echo json_encode(@$contextType); ?>;
  var contextName = <?php echo json_encode($el["name"]); ?>;
  contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
  var cmsList = <?= count($cmsList) ?>;
  jQuery(document).ready(function() {
    var title = contextName;

    if(notNull(costum) && typeof costum.metaTitle != "undefined"){
      title = costum.metaTitle
    }else if(notNull(costum) && typeof costum.title != "undefined"){
      title = costum.title;
    }
    setTitle(title);
    cmsBuilder.init();
      /*const ImageHome = document.querySelectorAll('img.lzy_img')
        ImageHome.forEach((v) => {
          imageObserver.observe(v);
        });*/
        $('img.lzy_img').each((i,v) => {
          imageObserver.observe(v);
        })
      //Make all blockCms sortable***********
      sortBlock(itemClassArray.join(","));
      mylog.log("itemClassArray", itemClassArray)
      $('.subtitle,.title,.description,.other,.title-1,.title-2,.title-3,.title-4,.title-5,.title-6').each(function() {
        if ($(this).text().trim() === '')
          $(this).remove();
      });
      $('[data-toggle="popover"]').popover();

      <?php if (Authorisation::isInterfaceAdmin()) {  ?>
        setTimeout(function(){
          if (cmsList == 0 && tplUsingId == "") {
            if (page == "welcome") {
             bootbox.dialog({
              message: `
              <div class="text-center text-green-k"><p>Bienvenue sur votre nouvelle COstum</p></div> <p class="">Pour commencer, vous pouvez choisir les 3 options suvants:<br>
              <strong class="text-green">- <?php echo Yii::t("common", "Choose a template")?>:</strong> Utiliser un Template fonctionnel.<br>
              <strong class="text-green">- <?php echo Yii::t("cms", "Generate a sample template")?>:</strong> Générer un Template exemple pour comprendre la fonctionnalitée du système.<br>
              <strong class="text-green">- <?php echo Yii::t("common", "Create my own template")?>:</strong> Créer votre propre page à l'aide de nos blocs CMS</p>`,
              title: "<?php echo Yii::t("cms", "Customize your page")?>",
              onEscape: function() {},
              show: true,
              backdrop: true,
              closeButton: true,
              animate: true,
              className: "my-modal",
              buttons: {
                success: {   
                  label: "<?php echo Yii::t("common", "Choose a template")?> ",
                  className: "btn-success",
                  callback: function() {
                    smallMenu.open(tplMenu);
                  }
                },
                "<?php echo Yii::t("cms", "Generate a sample template")?>": {
                  className: "btn-primary",
                  callback: function() {
                    tplObj.chooseConfirm(sampleEl,sampleId)
                    // tplObj.chooseThis(ide,tplSelected);
                  }
                },
                "<?php echo Yii::t("common", "Create my own template")?>": function() {
                  showCms();
                }
              }

            }
            );

             $(`<div class="col-md-12 sample-cms text-center custom-block-cms" data-id="undefined">
              <div class="selected cms-area-selected">Veuillez selectionner un bloc sur le menu à gauche</div>
              </div>`).insertBefore("#all-block-container");
           }else {
            tplObj.chooseConfirm(sampleStcEl,sampleStcId)
          }
        }else if (cmsList == 0 && tplUsingId !== ""){
           $(`<div class="col-md-12 sample-cms text-center custom-block-cms" data-id="undefined">
              <div class="selected cms-area-selected"><p><strong>Cette page ne contient aucun élément!</strong></p>Veuillez selectionner un bloc sur le menu à gauche</div>
              </div>`).insertBefore("#all-block-container");
           showCms()
        }
      },900)
      <?php } ?>
    });

  function loadThis(id,page,path,kunik) {
    mylog.log("scroll "+kunik+" id "+id, $("."+id+"-load").hasClass('currently-loading'))
   if (id != undefined && !$("."+id+"-load").hasClass('currently-loading')) {
    $("."+id+"-load").addClass("currently-loading")
     var required = {
      "idblock" : id ,
      "contextId" : contextId,
      "contextSlug" : thisContextSlug,
      "contextType" : contextType,
      "page" : page,
      "path" : path,
      "clienturi" : location.href
    };
    ajaxPost(
      null, 
      baseUrl+"/costum/blockcms/loadbloccms",
      required,
      function(data){
        $(".block-container-"+kunik).append(data.html)
        $(document).trigger("sp-loaded")
        if(previewMode){ 
          setTimeout(function() {
            mode = "v"
            convAndCheckLink(".sp-text");
            $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
            $(".sp-text").removeClass("edit-mode-textId");
            $(".sp-text").removeClass("selected-mode-textId");
            $(hideOnPreview).hide();
          }, 1);
        }else{
          tplObj.editTpl();
          $(".hiddenEdit").html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
        }
        // setTimeout(function () {
          $(".sp-text").show()
          $("."+id+"-load").remove();
        // },100)
      },
      {async : true}
      );
  }
}

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height() + 900;

  return elementBottom > viewportTop && elementTop < viewportBottom;
};


setTimeout(function () {
  var heightDispo = 900 - parseInt($("#mainNav").css("height"));

  let indexParent = 0;
  let firstBlock = $(".custom-block-cms").eq(indexParent).data("id");

  loadThis($("."+firstBlock+"-load").data("id"),$("."+firstBlock+"-load").data('page'),$("."+firstBlock+"-load").data("path"),$("."+firstBlock+"-load").data("kunik"))

  $(document).on("sp-loaded", function() {    
    heightDispo -= parseInt($("."+$("."+firstBlock+"-load").data("kunik")).css("height"))
    indexParent+=1
    firstBlock = $(".custom-block-cms").eq(indexParent).data("id");
    mylog.log("first" , heightDispo)
    if (heightDispo > indexParent) {
      // $("."+firstBlock+"-load").addClass("currently-loading")
      loadThis($("."+firstBlock+"-load").data("id"),$("."+firstBlock+"-load").data('page'),$("."+firstBlock+"-load").data("path"),$("."+firstBlock+"-load").data("kunik"))
    }
  })

  $(window).on('resize scroll', function() {
    $('.sp-is-loading').each(function() {
      if ($(this).isInViewport()) {
        loadThis($(this).data("id"),$(this).data('page'),$(this).data("path"),$(this).data("kunik"))
      }
    });
  });
},10)

</script>
<!--------------------------- cmsEngineJs ------------------------------->

<?php echo $this->renderPartial("costum.views.tpls.blockCms.cmsEngineJs", [
  "page" => $page,
  "el" => $el
]); ?>
<!--------------------------- cmsEngineJs ------------------------------->
