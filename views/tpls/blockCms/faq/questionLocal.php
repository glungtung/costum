
<?php 
$keyTpl = "questionLocal";
$paramsData = [
	"btnBg" => "#8A4F32",
	"btnBorderRadius" => 2,
	"btnPosition" => "right",
	"btnBorderColor" => "#8A4F32",
	"questionBg"=>"#8ABF32",
	"btncolorLabel" =>"#fff",
	"faqDisplay" =>[],
	"questionSize" =>"18",
	"background_color2" => "#f5f5f5",
	"questionAccordion" => "false",
	"questionColor" => "#333333",
	"textColor" => "#333333",
	"textSize" => "14",

];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style type="text/css">
	.faq-img {
		width:40%;
		height:auto;
		vertical-align:middle;
		float: left;
		padding-right:15px;
	}
	.template_faq {
		background: #edf3fe none repeat scroll 0 0;
	}
	.panel-group<?=$kunik ?> {
		margin-left: 5%;
		padding: 30px;
	}
	.panel-group<?=$kunik ?> #accordion .panel {
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin: 0 0 15px 0px;
		background : transparent;
	}
	.panel-group<?=$kunik ?> #accordion .panel-heading {
		border-radius: 30px;
		padding: 0;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title {
		text-transform: none;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a {
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["questionBg"]; ?> ;
		border: 1px solid transparent;
		border-radius: 30px;
		color: #fff;
		display: block;
		font-size:<?php echo $paramsData["questionSize"]; ?>px;
		font-weight: 600;
		padding: 12px 20px 12px 50px;
		position: relative;
		transition: all 0.3s ease 0s;
	}
	.panel-group<?=$kunik ?> .panel-default>.panel-heading{
		background-color: transparent ;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a.collapsed {
		background: transparent none repeat scroll 0 0;
		border: 1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : "#ffffff"; ?>;
		color: <?= $paramsData["questionColor"];?>;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a::after,.panel-group<?=$kunik ?> #accordion .panel-title a.collapsed::after {
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["questionBg"]; ?>;
		border: 1px solid transparent;
		border-radius: 50%;
		box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
		color: #fff;
		content: "";
		font-family: fontawesome;
		font-size: 25px;
		height: 55px;
		left: -20px;
		line-height: 55px;
		content: "";
		font-family: fontawesome;
		position: absolute;
		text-align: center;
		top: -5px;
		transition: all 0.3s ease 0s;
		width: 55px;
	}
	.panel-group<?=$kunik ?> #accordion .panel-title a.collapsed::after {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		box-shadow: none;
		color: #333;
		content: "";
	}
	.panel-group<?=$kunik ?> #accordion .panel-body {
		background: transparent none repeat scroll 0 0;
		border-top: medium none;
		padding: 20px 25px 10px 9px;
		color: <?= $paramsData["textColor"];?>;
		font-size: <?= $paramsData["textSize"]?>px;
		position: relative;
	}
	.panel-group<?=$kunik ?> #accordion .panel-body p, .panel-group<?=$kunik ?> #accordion .panel-body  text-response-faq {
		border-left: 1px dashed #8c8c8c;
		padding-left: 25px;
		font-size: <?= $paramsData["questionSize"]?>px;
	}
	.panel-group<?=$kunik ?> .text-response-faq {
		text-align: justify;
	}

	.btn-position{
		text-align: <?=$paramsData["btnPosition"] ?> ;
	}
	
	@media (max-width: 767px) {
		.faq-img {
			width:100%;
			height:auto;
			vertical-align:middle;
			float: left;
			padding-right:0px;
			padding-bottom: 15px;
		}
		.faq-container .panel-default {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
	.questionLocal_<?=$kunik?> {
		padding-bottom: 5%; 
		box-shadow: 0px 0px 10px -8px silver;

	}
	.questionLocal_<?=$kunik?> .addFaq<?= $kunik?>{
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBg"]; ?>;
		margin-top: 5%;	
		color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btncolorLabel"]; ?>;
		border-radius : <?= $paramsData["btnBorderRadius"]?>px;
		border-color : <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["btnBorderColor"]; ?>;
		border-width: 1px;
		padding: 10px 15px;
		text-align: center;
		font-size: 20px;
	}
	@media (max-width: 978px) {
		.questionLocal_<?=$kunik?> .addFaq<?= $kunik?>{
			margin-top: 5%;
			margin-left: 10%;
			padding: 10px;
			text-align: center;
			font-size: 17px;
		}
	}
	.questionLocal_<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none
    }
    .questionLocal_<?=$kunik?> .icon-<?=$kunik?> li a{
        display:block;width:35px;
        height:35px;
        line-height:35px;
        border-radius:50%;
        color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btncolorLabel"]; ?>;
        font-size:18px;
        background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["questionBg"]; ?>;
         margin-right:10px;transition:all .5s ease 0s
    }
    .questionLocal_<?=$kunik?> .icon-<?=$kunik?> li a:hover{
        transform:rotate(360deg)
    }
	.select2-container-multi .select2-choices{
		border: 1px solid #dae0e4;
	}
    /* .questionLocal_<?=$kunik?>{
        box-shadow:0 0 3px rgba(0,0,0,.3)
    } */
    .questionLocal_<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none;
        margin:0;
    }
    .faqDisplayselectMultiple .form-control{
		background-color: transparent;
	}
    .questionLocal_<?=$kunik?> .icon-<?=$kunik?> li{
        display:inline-block;
        text-align: center;
    }
	@media (max-width : 767px) {
		.panel-group<?=$kunik ?> {
			padding: 0px;
		}
		.panel-group<?=$kunik ?> #accordion .panel-title a {
			font-size: 16px;
			padding: 5px 0px 3px 30px;
		}
		.panel-group<?=$kunik ?> #accordion .panel-body p, .panel-group<?=$kunik ?> #accordion .panel-body  text-response-faq {
			font-size: 14px ;
			padding-left : 10px;
		}
		.panel-group<?=$kunik ?> #accordion .panel-title a::after,.panel-group<?=$kunik ?> #accordion .panel-title a.collapsed::after {
			border: 1px solid transparent;
			border-radius: 50%;
			box-shadow: 0 3px 10px rgb(0 0 0 / 58%);
			content: ""; 
			font-size: 20px;
			height: 40px;
			left: -20px;
			line-height: 40px;
			content: "";
			font-family: fontawesome;
			position: absolute;
			text-align: center;
			top: -5px;
			transition: all 0.3s ease 0s;
			width: 40px;
		}
		.panel-group<?=$kunik ?> #accordion .panel-body {  
			padding: 12px 6px 6px 6px; 
		}
		.questionLocal_<?=$kunik?> .addFaq<?= $kunik?>{
			margin-top: 3%;
			margin-left: 11%;
			padding: 5px;
			text-align: center;
			font-size: 14px;
		}
	}
</style>
<div class=" questionLocal_<?=$kunik?> ">

	<?php if(Authorisation::isInterfaceAdmin()){ ?>
		<div class="btn-position">
			<a href="javascript:;" class="btn btn-xs addFaq<?= $kunik?>" >
				<i class="fa fa-plus"></i>
				Ajouter une question
			</a>
		</div>
<?php } ?>
	<div class="panel-group<?=$kunik ?>">

	</div>


</div>

<script type="text/javascript">	

	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() { 
		var dyfPoi={
			"beforeBuild":{
				"properties" : {
					"name" : {
						"label" : "Votre question",	
						"inputType" : "textarea",
						"placeholder" : "",
						"order" : 1
					},
					"shortDescription" : {
						"inputType" : "textarea",
						"markdown" : true,
						"label" : "Réponse",
						"order" : 2
					},
					"sizeInColonne" : {
						"inputType" : "select", 
						"label" : "Affichage de la question",
						"options" : {
							1 : "Une ligne",
							2 : "Deux colonne"
						},
						"order" : 2
					}
				}
			},
			"onload" : {
				"actions" : {
					"setTitle" : "Ajouter un(e) Question",
					"src" : {
						"infocustom" : "<br/>Remplir le champ"
					},
					"presetValue" : {
						"type" : "faq",
					},
					"hide" : {
						"locationlocation" : 1,
						"breadcrumbcustom" : 1,
						"urlsarray" : 1,
						"parent" : 1,
						"tagstags" : 1,
						"formLocalityformLocality" : 1,
						"descriptiontextarea" : 1,
					}
				}
			}

		};
		dyfPoi.afterSave = function(data){
			dyFObj.commonAfterSave(data, function(){
				urlCtrl.loadByHash(location.hash);
			});
		}
		var kunik = <?= json_encode($kunik)?>; 
		var allPoi = {}; 
		var faqObj<?= $kunik?> = {
			defaultValue : [],
			views : function(index,value,i){
				var src = ""
				var classNumberLign = (typeof value.sizeInColonne != "undefined" && value.sizeInColonne == 1)? "col-md-12" : "col-md-6";
				if (typeof value.shortDescription != "undefined"){
					shortDescription = value.shortDescription;
				} else {
					shortDescription ="";
				}
				if (i==0 && typeof sectionDyf.<?php echo $kunik?>ParamsData.questionAccordion != "undefined" && sectionDyf.<?php echo $kunik?>ParamsData.questionAccordion == "false") {
					src += '<div class="panel panel-default col-xs-12 col-sm-12 '+classNumberLign+'">';
					src += '<div class="panel-heading sp-bg" data-id="<?= $blockKey ?>" data-field="background_color2" data-value="<?= $paramsData['background_color2']; ?>" data-sptarget="bd" data-kunik="<?= $kunik?>" role="tab" id="heading'+index+""+kunik+'">';
					src += '<h4 class="panel-title ">';
					src += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+""+kunik+'" aria-expanded="false" aria-controls="collapse'+index+""+kunik+'">' +
					value.name;

					src += '</a>';

					src += '</h4>';

					src += '</div>';
					src += '<div id="collapse'+index+""+kunik+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+index+""+kunik+'">';
					src += '<div class="panel-body">';
					mylog.log("profill", value.profilImageUrl);
					if (value.profilMediumImageUrl != null) {
						src += '<img class="faq-img" src="'+value.profilMediumImageUrl+'">';
					}

					
					src += '<div class="text-response-faq markdown" id="markdown'+value.id+'">'+shortDescription+'</div>';

					if(isInterfaceAdmin && costum.editMode)
						src +='<ul class="hiddenPreview icon-<?=$kunik?>">'+
					'<li>'+
					'<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-edit"></i></a>'+
					'</li>'+
					'<li>'+
					'<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-trash"></i></a>'+
					'</li>'+
					'</ul>';
					src += '</div>';
					src += '</div>';
					src += '</div>';

				
				}else{
					src += '<div class="panel panel-default col-xs-12 col-sm-12 '+classNumberLign+'">';
					src += '<div class="panel-heading sp-bg" data-id="<?= $blockKey ?>" data-field="background_color2" data-value="<?= $paramsData['background_color2']; ?>" data-sptarget="bd" data-kunik="<?= $kunik?>" role="tab" id="heading'+index+""+kunik+'">';
					src += '<h4 class="panel-title ">';
					src += '<a class="collapsed " role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+""+kunik+'" aria-expanded="false" aria-controls="collapse'+index+'">' +
					value.name;
					src += '</a>';
					src += '</h4>';
					src += '</div>';
					src += '<div id="collapse'+index+""+kunik+'" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading'+index+""+kunik+'">';
					src += '<div class="panel-body">';
					mylog.log("profill", value.profilImageUrl);
					if (value.profilMediumImageUrl != null) {
						src += '<img class="faq-img" src="'+value.profilMediumImageUrl+'">';
					}
					
					src += '<div class="text-response-faq markdown" id="markdown'+value.id+'">'+shortDescription+'</div>';
					if(isInterfaceAdmin && costum.editMode)
					src +='<ul class="hiddenPreview icon-<?=$kunik?>">'+
					'<li>'+
					'<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-edit"></i></a>'+
					'</li>'+
					'<li>'+
					'<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-trash"></i></a>'+
					'</li>'+
					'</ul>';
					src += '</div>';
					src += '</div>';
					src += '</div>';
				}
				setTimeout(function(){
					descHtml = dataHelper.markdownToHtml($("#shortDescription"+value.id).html()); 
					$("#shortDescription"+value.id).html(descHtml);
				},100)	
				setTimeout(function(){
					descHtml = dataHelper.markdownToHtml($("#markdown"+value.id).html()); 
					$("#markdown"+value.id).html(descHtml);
				},100)
				return src;

			},
			getFaqs : function(){
				var params = {
					source : cmsBuilder.config.context.slug,
					type: "faq",
					limit : ""
				}
				var allFaq = {};
				var el = {}
				ajaxPost(
					null,
					baseUrl+"/costum/costumgenerique/getpoi",
					params,
					function(data){ 
						if((sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay).length != 0) { 
							el = data.element;
							$.each(sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay, function(ke, ve){
								if(typeof el[ve] != "undefined"){ 
									allFaq[ve]=el[ve]["name"]
									allPoi[ve]=el[ve]
									delete el[ve];  
								}
							})			
							$.each(el, function(kel, vel){
								allFaq[kel] = vel.name; 
								allPoi[kel] = vel;
							})           
						}else{ 
							allPoi = data.element;
							$.each(allPoi,function(kf,vf){
								allFaq[kf] = vf.name
								faqObj<?= $kunik?>.defaultValue.push(kf);
							})
						}
						var src = "";
						src +='<div class="row">';
						src +='<div class="col-md-12">';
						src += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';	
						if (data.length != 0){
							if((sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay).length != 0){ 
								var i = 0;
								$.each(sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay,function(k,v){	
									if(exists(allPoi[v])){ 
										src += faqObj<?= $kunik?>.views(v,allPoi[v],i);
										i+=1;				
									}
								})	
								coInterface.bindLBHLinks();	 
							}else{ 
								var i = 0;
								$.each(allPoi, function( index, value ) { 
									src += faqObj<?= $kunik?>.views(index,value,i);
									i+=1;				
								});	
								coInterface.bindLBHLinks();		
							}

						}else {
							src += "<p class='text-center'>Il n'éxiste aucun question </p>";
						}

						src += '</div>';
						src += '</div>';
						src += '</div>';	
						$(".panel-group<?php echo $kunik ?>").html(src);
						$(".panel-group<?=$kunik?> .edit").off().on('click',function(){
							var id = $(this).data("id");
							var type = $(this).data("type");
							dyFObj.editElement('poi',id,type,dyfPoi);
						});
						$(".panel-group<?=$kunik?> .delete").off().on("click",function () {
									$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
									var btnClick = $(this);
									var id = $(this).data("id");
									var type = "poi";
									var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
									
									bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
									function(result) 
									{
								if (!result) {
									btnClick.empty().html('<i class="fa fa-trash"></i>');
									return;
								} else {
									ajaxPost(
										null,
										urlToSend,
										null,
										function(data){ 
											if ( data && data.result ) {
												toastr.success("élément effacé");
												$("#"+type+id).remove();
												urlCtrl.loadByHash(location.hash);
											} else {
											toastr.error("something went wrong!! please try again.");
											}
										}
									);
								}
								});
							});
					} ,null,null,{"async":false}
				); 
				return allFaq;
			}
		}
		allFaqs = faqObj<?= $kunik?>.getFaqs();
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				onLoads : {
					onload : function(data){
						$(".parentfinder").css("display","none");
						selSortableObj.init("#faqDisplay",allFaqs);
					}
				},
				"properties" : {
					"questionAccordion" :{
						label :"Fermer toutes les questions?",
						inputType: "checkboxSimple",
						"params" : {
							"onText" : trad.yes,
							"offText" : trad.no,
							"onLabel" : trad.yes+"",
							"offLabel" : trad.no+""
						},
						"checked" : false
					},
					"questionBg" : {
						"label" : "Couleur du background question",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.questionBg
					},
					"questionSize" : {
						"label" : "Taille de la question",
						inputType : "texte",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.questionSize
					},
					"questionColor" : {
						"label" : "Couleur de la question",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.questionColor
					},
					"faqDisplay" : {
						"inputType" : "selectMultiple",
						"isSelect2" : true,
						"noOrder" :true,
						"label" : "Question à afficher (glisser et déposer pour organiser)",
						"class" : "form-control",
						"placeholder" : "Question à afficher",
						value :  ((sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay).length != 0)?sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay:faqObj<?= $kunik?>.defaultValue,
						options : allFaqs
					},
					"textSize" : {
						"label" : "Taille de la question",
						inputType : "texte",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.textSize
					},
					"textColor" : {
						"label" : "Couleur de la question",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.textColor
					},
					"btnBg" : {
						"label" : "Couleur du background du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBg
					},					
					"btncolorLabel" : {
						"label" : "Couleur du label du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btncolorLabel
					},
					"btnBorderRadius" : {
						"label" : "Rond du bordure",
						inputType : "text",
						"rules" : {
							"number":true
						},
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBorderRadius
					},
					"btnBorderColor" : {
						"label" : "Couleur du bordure",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBorderColor
					},
					"btnPosition" : {
						"label" : "Couleur du background du bouton",
						inputType : "select",
						"options" : {
							"center" : "Au centre",
							"left" : "À gauche",
							"right" : "À droite"
						},
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnPosition
					}
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
					
				}
			}
		};
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"question",4,6,null,null,"Propriété du question","green","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"faq",12,12,null,null,"Affichage","green","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"text",4,6,null,null,"Propriété du texte","green","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton","green","");
		});
		$(".addFaq<?= $kunik?>").click(function(){
            dyFObj.openForm('poi',null, null,null,dyfPoi); 
        })

		
	});
	var  selSortableObj = {
    	init : function(myselect,options,config=null){
			$(myselect).select2({
				placeholder: '<?php echo Yii::t("cms", "Select and order your menu")?>'
			}).on("select2:select", function (evt) {
				var id = evt.params.data.id;
				var element = $(this).children("option[value="+id+"]");
				selSortableObj.moveElementToEndOfParent(element);
				$(this).trigger("change");
			});
			var ele=$(myselect).parent().find("ul.select2-choices");
			ele.sortable({
				containment: 'parent',
				update: function() {
					selSortableObj.orderSortedValues(myselect);
				}
			});
		},
	
		orderSortedValues : function(myselect) {
			var value = ''
			$(myselect).parent().find("ul.select2-choices").children("li").children("div").each(function(i, obj){
				var element = $(myselect).children('option').filter(function () { 
					return $(this).html() == $(obj).text() 
				});
				selSortableObj.moveElementToEndOfParent(element)
			});
		},
		moveElementToEndOfParent : function(element) {
			var parent = element.parent();
			element.detach();
			parent.append(element);
		}    
  	};
	
</script>