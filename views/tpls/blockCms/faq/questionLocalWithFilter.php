<?php 
$keyTpl = "questionLocalWithFilter";
$paramsData = [
	"btnBg" => "#3dad6f",
	"btnPosition" => "right",
	"btnEditBg"=>"#8ABF32",
	"btncolorLabel" =>"#fff"

];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style> 
	.<?= $kunik?> .annuaire-card { 
		background: #f2f2f2;
		border-radius: 9px;
		overflow: hidden;
		box-shadow: 0 3px 6px #00000029;
		margin-bottom: 10px;
		margin-top: 10px;
		padding-left: 20px;
		padding-right: 20px;
		padding-top: 15px;
		padding-bottom: 15px;
	}
	.<?=$kunik?> .annuaire-card .annuaire-title {
		color: #38414d;
		font-size: 16px;
		font-weight: 700;
	}
	.<?=$kunik?> .annuaire-card .annuaire-content {
		color: #38414d;
		font-size: 14px;
		margin-top: 25px;
		font-weight: 400;
		line-height: 25px;
	}
	#allFaq<?= $kunik?> {
		display: flex;
		flex-wrap: wrap;
		padding: 10px;
		justify-content: left;
	}
	.<?=$kunik?> .annuaire-card .annuaire-contact  {
	 
		text-align: right;
		cursor: pointer;
	}
	.<?=$kunik?> .annuaire-card .annuaire-contact a{
		color: #3dad6f;
		font-size: 16px;
		font-weight: 700;
		margin-top: 20px; 
	}
	.<?=$kunik?> {
		padding-bottom: 5%; 
		box-shadow: 0px 0px 10px -8px silver;

	}
	.<?=$kunik?> .addFaq<?= $kunik?>{
		background: <?= $paramsData["btnBg"]?>;	
		color: <?= $paramsData["btncolorLabel"]?>;
		margin-top: 1%;
		border-width: 1px;
		padding: 5px 10px;
		text-align: center;
		font-size: 15px;
	}
	@media (min-width: 978px) {
		.<?= $kunik?> .annuaire-card {
			width: calc(50% - 20px);
			margin-left: 10px;
			margin-right: 10px;
		}
	}
	@media (max-width: 978px) {
		.<?=$kunik?> .addFaq<?= $kunik?>{
			margin-top: 5%;
			margin-left: 10%;
			padding: 10px;
			text-align: center;
			font-size: 17px;
		}

		#filterfaq<?=$kunik?> #filterContainerInside .selectMultiple   {
			width: 100% !important;
		}
	} 
	.<?=$kunik?> .annuaire-card:hover  .icon-<?=$kunik?>{
		display:block;
	}
	.<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
		display: none;
        list-style:none
    }
    .<?=$kunik?> .icon-<?=$kunik?> li a{
        display:block;width:35px;
        height:35px;
        line-height:35px;
        border-radius:50%;
        color: <?php echo $paramsData["btncolorLabel"]; ?> ;
        font-size:18px;
        background:<?php echo $paramsData["btnEditBg"]; ?>;
         margin-right:10px;transition:all .5s ease 0s
    }
    .<?=$kunik?> .icon-<?=$kunik?> li a:hover{
        transform:rotate(360deg)
    }
    /* .<?=$kunik?>{
        box-shadow:0 0 3px rgba(0,0,0,.3)
    } */
    .<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none;
        margin:0;
    }
    .<?=$kunik?> .icon-<?=$kunik?> li{
        display:inline-block;
        text-align: center;
    }
	.deleteThisBtn ,.btn-edit-preview{
		display : none;
	}
	#filterfaq<?=$kunik?>{
		margin-top: 5px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .selectMultiple label  {
		text-align: left;
		color: white;
		font-weight: bold;
		font-size: 16px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .selectMultiple   {
		float: left;
		display: grid; 
		width: 30%;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 10px;
		padding-top: 0px;
		padding-bottom: 0px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .select2-container-multi:not(.aapstatus-container) .select2-choices .select2-search-choice{   
		background: #3cad6f!important;
		color: #fff!important;
		padding-left: 15px!important;
		padding-right: 15px!important;
		padding-top: 5px!important;
		padding-bottom: 5px!important;
		border: 1px solid #3cad6f!important;
		border-radius: 20px!important;
    	font-weight: 700!important;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .select2-search-choice-close {
		background: #fff!important; 
		margin: 0;
		padding-left: 2px;
    	border-radius: 10px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .select2-search-choice-close::before {
		color: #3cad6f !important;
		font-size: 10px;
	}
	.select2-result-label:hover{
		background-color:#3cad6f;
	}
	.select2-results .select2-highlighted{
		background-color:#3cad6f;
	}	
	.<?=$kunik?> .container-filters-menu #activeFilters .filters-activate{
		background-color:#3cad6f !important;
	}
	.select2-results .select2-result-label{
		border-bottom: 1px double #e2e2e2;
	}
	.<?=$kunik?> .list-filters{
		background-color: white;
		border: 1px solid #aaa;
		border-radius: 4px;
		box-sizing: border-box;
		display: block;
		position: absolute; 
		width: 100%;
		z-index: 1051;
		border-top: none;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}  
	.btn-position<?= $kunik?>{
		text-align: <?=$paramsData["btnPosition"] ?> ;
	} 
	#allFaq<?= $kunik?> .divEndOfresults{
		display : none;
	}
	#allFaq<?= $kunik?> .img-parent{
		height: 30px;
		width: 30px;
		margin-right: 5px;		
		object-fit: cover;
	}
	.portfolio-modal .close-modal .lr, .portfolio-modal .close-modal .lr .rl {
		background-color: #2C3E50;
	}
	.btn-return{ 
		display: none;
	}
	#allFaq<?= $kunik?> .btn-open-small-menu{
		cursor: pointer;
	}
</style>
<div class=" sp-cms-container <?= $kunik?>"> 
  <div id="filterfaq<?= $kunik?>"></div>
<div class='headerSearch<?= $kunik ?> no-padding col-xs-12'></div>
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
		<div class="btn-position<?= $kunik?>">
			<a href="javascript:;" class="btn btn-xs addFaq<?= $kunik?>" >
				<i class="fa fa-plus"></i>
				Ajouter une question
			</a>
		</div>
	<?php } ?>
	<div class="row">'
		<div class="col-md-12">' 
  			<div id="allFaq<?= $kunik?>"></div> 
		</div>
	</div>
</div>
<script>
	var statusListFilter = [
		{
			"id": "Une personne morale" ,
			"text": "personne morale (Entreprise)",
		},
		{
			"id": "Personne physique",
			"text": "personne physique (Particuliers)",
		},
		{
			"id": "PME/GE",
			"text": "PME/GE",
		}
	]
	var accompanimentTypeFilter = [
		{
			"id": "Accompagnement - Support" ,
			"text": "Un accompagmnement mentorat, des conseils",
		},
		{
			"id": "Financement",
			"text": "Un financement (lister les types)",
		},
		{
			"id": "Formation",
			"text": "Une formation",
		},
		{
			"id": "Mise en réseau",
			"text": "Des partenaires,du réseau, un associé etc.",
		},
		{
			"id": "Pilotage",
			"text": "Pilotage",
		},
		{
			"id": "Tiers Lieux",
			"text": "Un local, un local commercial, un bureau",
		}
	]
	var themes = [
		{
			"id": "Numérique" ,
			"text": "Numérique",
		},
		{
			"id": "Santé & Biotechnologies",
			"text": "Santé & Biotechnologies",
		},
		{
			"id": "Service de proximité & Tourisme",
			"text": "Service de proximité & Tourisme",
		},
		{
			"id": "Environnement & Energie",
			"text": "Environnement & Energie",
		},
		{
			"id": "Croissance bleue",
			"text": "Croissance bleue",
		},
		{
			"id": "Construction durable",
			"text": "Construction durable",
		},
		{
			"id": "Culture & Connaissance",
			"text": "Culture & Connaissance",
		},
		{
			"id": "Agri ressources & Agroalimentaire",
			"text": "Agri ressources & Agroalimentaire",
		},
		{
			"id": "Aéronautique",
			"text": "Aéronautique",
		}
	] 
    var statusList = {
        "Une personne morale" : "personne morale (Entreprise)",
        "Personne physique" : "personne physique (Particuliers)",
        "PME/GE" : "PME/GE"
    }
    var accompanimentType = {
        "Accompagnement - Support": "Un accompagmnement mentorat, des conseils",
        "Financement": "Un financement (lister les types)",
        "Formation": "Une formation ",
        "Mise en réseau": "Des partenaires,du réseau, un associé etc.",
        "Pilotage": "Pilotage", 
        "Tiers Lieux": "Un local, un local commercial, un bureau"
    } 
	var paramsFilter = {
		container : "#filterfaq<?= $kunik?>",
		results :{
			dom : '#allFaq<?= $kunik?>',
			renderView : "faqObj.views",
			map : {
				active : false,
				sameAsDirectory:true,
				showMapWithDirectory:true
			}
		},
		header :{
			dom: ".headerSearch<?= $kunik ?>",
			options:{
				left:{
					group :{
						count : true
					}
				}
			}
		},
		defaults : {
			fields : ["type","name","parent","shortDescription","description","profilImageUrl","profilThumbImageUrl"],
			types : ["poi"],
			filters : {
				type : "faq"				
			},
			sortBy: {"name": 1}
		},
		filters : {
			status : {
				view : "selectMultiple", 
				type : "select2",
				name : "Je suis",
				event : "select2",
	 			keyValue: false,
				list : statusListFilter		
			},
			family :{
				view : "selectMultiple", 
				type : "select2",
				name : "Je cherche", 
				keyValue : false,
				event : "select2",
				list : accompanimentTypeFilter

			},
			secteur :{
				view : "selectMultiple", 
				type : "select2",
				name : "Secteurs",
				event : "select2",
	 			keyValue: false,
				list :  themes

			},
		}
	};
	var filterSearch={};
	var dyfPoi={
		"beforeBuild":{
			"properties" : {
				"name" : {
					"label" : "Votre question",	
					"inputType" : "textarea",
					"placeholder" : "",
					"order" : 1
				},
				"shortDescription" : {
					"inputType" : "textarea",
                    "markdown" : true,
					"label" : "Réponse",
					"order" : 2
				},
				"status" : {
                    "inputType": "selectMultiple",
                    "placeholder": "",
                    "class": "multi-select",
                    "isSelect2": true, 
					"label" : "Status",
                    "options" : statusList,
					"order" : 3
				},
				"accompanimentType" : {
					"label" : "Type d'accompagnement ",
                    "inputType": "selectMultiple",
                    "placeholder": "",
                    "class": "multi-select",
                    "isSelect2": true, 
                    "options" : accompanimentType,
					"order" : 4
				},
				"thematic" : {
					"label" : "Secteur d'activités",
                    "inputType": "selectMultiple",
                    "placeholder": "",
                    "class": "multi-select",
                    "isSelect2": true, 
                    "options" : costum.lists.themes,
					"order" : 5
				},
				"parent" : {
					inputType : "finder",
					label : "Acteurs associés",
					initMe: false,
                	rules : { 
						required : false
					}, 
					placeholder:"Acteurs associés", 
					initType: ["organizations"], 
					initBySearch: true,
					openSearch :true,
					initContacts: false, 
					filters:{
						"category":"acteurMeir"
					}
				}
			}
		},
		"onload" : {
			"actions" : {
				"setTitle" : "Ajouter un(e) Question",
				"src" : {
					"infocustom" : "<br/>Remplir le champ"
				},
				"presetValue" : {
					"type" : "faq",
				},
				"hide" : { 
					"locationlocation" : 1,
					"breadcrumbcustom" : 1,
					"urlsarray" : 1,
					"parent" : 1,
					"tagstags" : 1,
					"formLocalityformLocality" : 1,
					"descriptiontextarea" : 1,
				}
			}
		}

	};
	dyfPoi.afterSave = function(data){
		dyFObj.commonAfterSave(data, function(){
			if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
				tplCtx.id = data.map._id.$id;
			}
			if(typeof data.id != "undefined" && typeof data.map._id == "undefined"){
				tplCtx.id = data.id;
			}
			tplCtx.path = "allToRoot";
			tplCtx.collection = "poi";
			tplCtx.value = {
				tags : []
			};
			if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {
				$.each(data.map.tags, function(i,tg){
					tplCtx.value.tags.push(tg.replace("&amp;","&"));
				});
			}
            if(typeof data.map.status != "undefined"){ 
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {
                    if(typeof data.map.status == "string" ){
                        if ((tplCtx.value.tags).indexOf(data.map.status) == -1){ 
                            tplCtx.value.tags.push(data.map.status);
                        }
                    }
                    else{
                        $.each(data.map.status, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(statusList,function(kf,vf){
                        if(typeof data.map.status == "string" && (tplCtx.value.tags).indexOf(vf) >-1 && data.map.status != vf ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }else if(typeof data.map.status == "object" && (tplCtx.value.tags).indexOf(vf) >-1  && (data.map.status).indexOf(vf) == -1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }
                    })
                } else{
                    if(typeof data.map.status == "string"){
                        tplCtx.value.tags.push(data.map.status);
                    }
                    else{
                        $.each(data.map.status, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                } 
            }
            if(typeof data.map.accompanimentType != "undefined"){ 
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) { 
                    if(typeof data.map.accompanimentType == "string" ){
                        if ((tplCtx.value.tags).indexOf(data.map.accompanimentType) == -1){
                            tplCtx.value.tags.push(data.map.accompanimentType);
                        }
                    }
                    else{
                        $.each(data.map.accompanimentType, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(accompanimentType,function(kf,vf){
                        if(typeof data.map.accompanimentType == "string" && (tplCtx.value.tags).indexOf(vf) >-1 && data.map.accompanimentType != vf ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }else if(typeof data.map.accompanimentType == "object" && (tplCtx.value.tags).indexOf(vf) >-1  && (data.map.accompanimentType).indexOf(vf) == -1){
							tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }
                    })
                } else{
                    if(typeof data.map.accompanimentType == "string"){
                        tplCtx.value.tags.push(data.map.accompanimentType);
                    }
                    else{
                        $.each(data.map.accompanimentType, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }                 
            }
            if(typeof data.map.thematic != "undefined"){
				tplCtx.value.thematic = []; 
                if(typeof data.map.thematic != "undefined" || notNull(data.map.thematic)) {
                    if(typeof data.map.thematic == "string" ){
						tplCtx.value.thematic  = data.map.thematic.replace("&amp;","&");
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
							tplCtx.value.thematic.push(tag.replace("&amp;","&"));
                        });
                    }

				}
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {
                    if(typeof data.map.thematic == "string" ){ 
                        if ((tplCtx.value.tags).indexOf(data.map.thematic) == -1){
                            tplCtx.value.tags.push(data.map.thematic.replace("&amp;","&"));
                        }
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){ 
							mylog.log("commonAfterSavedatatagsd",tplCtx.value.tags); 
							mylog.log("commonAffilterterSavedatatagsd",tag); 
                            if((tplCtx.value.tags).indexOf(tag) == -1){
								alert("fhdsfbsdjfbds")
                                tplCtx.value.tags.push(tag.replace("&amp;","&"));
                            }
                        });
                    }
                    $.each(costum.lists.themes,function(kf,vf){
                        if(typeof data.map.thematic == "string" && (tplCtx.value.tags).indexOf(vf) >-1 && data.map.thematic != vf ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }else if(typeof data.map.thematic == "object" && (tplCtx.value.tags).indexOf(vf) >-1  && (data.map.thematic).indexOf(vf) == -1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }
                    })
                } else{
                    if(typeof data.map.thematic == "string"){
                        tplCtx.value.tags.push(data.map.thematic);
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }
			mylog.log("commonAfterSavedatatags3",tplCtx.value.tags); 
                
            }  
			mylog.log("commonAfterSavedatatags",tplCtx.value.tags); 
			dataHelper.path2Value(tplCtx, function(params) {
				urlCtrl.loadByHash(location.hash);
			});
		});
	}; 
	var faqObj = {
		edit<?= $kunik?> : function(type,id){
			dyFObj.editElement('poi',id,type,dyfPoi);
		},
		delete<?= $kunik?> : function(id){
			$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');		
			var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/poi/id/"+id;
		
			bootbox.confirm("voulez vous vraiment supprimer cette question !!",
				function(result) {
					if (!result) { 
						return;
					} else {
						ajaxPost(
							null,
							urlToSend,
							null,
							function(data){ 
								if ( data && data.result ) {
									toastr.success("élément effacé");
									$("#poi"+id).remove();
									urlCtrl.loadByHash(location.hash);
								} else {
									toastr.error("something went wrong!! please try again.");
								}
							}
						);
					}
				}
			);
		},
		views : function(params){
			var src = "";
			var shortDescription = (typeof params.shortDescription != "undefined")?params.shortDescription:"";
			src+= "<div class='annuaire-card col-xs-12 col-sm-12 col-md-6'>"+
						"<div class='annuaire-title'> "+params.name+"</div>"+
						"<div class='annuaire-content markdown' id='markdown"+params.id+"'> "+shortDescription+"</div>";

						src+="<div class='col-xs-12 auhtor-poi no-padding margin-bottom-10'>";
						if(notNull(params.parent)){
							src+="<span class='font-montserrat letter-blue'></span>	"
							$.each(params.parent,function(k,v){
								src += faqObj.searchParent(k); 
							})
						} 
					src+="</div>";
						if(isInterfaceAdmin == true){
							src +="<ul class='hiddenPreview icon-<?=$kunik?>'>"+
								"<li>"+
								"<a href='javascript:;'  onclick='faqObj.edit<?= $kunik?>("+"\""+params.type+"\""+","+"\""+params.id+"\""+")'   ><i class='fa fa-edit'></i></a>"+
								"</li>"+
								"<li>"+
								"<a href='javascript:;'  onclick='faqObj.delete<?= $kunik?>("+"\""+params.id+"\""+")'   ><i class='fa fa-trash'></i></a>"+
								"</li>"+
							"</ul>";
						}
						src +="<div class='annuaire-contact' >";											
							src +="<a href='#page.type.poi.id."+params.id+"' class='btn lbh-preview-element' >En savoir plus</a>"+
						"</div>"+
					"</div>";

					setTimeout(function(){
						descHtml = dataHelper.markdownToHtml($("#markdown"+params.id).html()); 
						$("#markdown"+params.id).html(descHtml);
					},100)
		
			return src;
		},
		searchParent : function(parent){ 
			var src ="";
			var params = {
				id : parent,
				type : "organizations"
			}
			ajaxPost(
				null,
				baseUrl+"/co2/element/get/type/organizations", 
				params,
				function(data){
					id = (typeof data.map["_id"] != "undefined" && typeof data.map["_id"]['$id'] != "undefined")?data.map["_id"]['$id']:""
					imgPath = (data.map['profilThumbImageUrl'] && notNull(data.map['profilThumbImageUrl'])) ? baseUrl+'/'+data.map['profilImageUrl'] : baseUrl+'/'+'/images/thumb/default_'+data.map['type']+'.png'   
					src="<a class='btn-open-small-menu'  onClick='smallMenu.openAjaxHTML(\""+baseUrl+"/costum/meir/elementhome/type/organizations/id/"+id+"\");'>";
					src+="<img src='"+imgPath+"' class='img-parent' width='30' height='30'/>" +data.map.name;
					src+="</a><br><br>" ;
				},
				function(error){},
				"json",
				{async : false}
			) 
			return src;
		}
	}
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.search.init(filterSearch,0);
	
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
					"btnEditBg" : {
						"label" : "Couleur du background du bouton édition et suppression",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnEditBg
					},
					"btnBg" : {
						"label" : "Couleur du background du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBg
					},					
					"btncolorLabel" : {
						"label" : "Couleur du label du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btncolorLabel
					},
					
					"btnPosition" : {
						"label" : "Position du bouton",
						inputType : "select",
						"options" : {
							"center" : "Au centre",
							"left" : "À gauche",
							"right" : "À droite"
						},
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnPosition
					}
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("Élément bien ajouté");
	                      $("#ajax-modal").modal('hide');
	                      urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
					
				}
			}
		};
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() { 
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"question",4,6,null,null,"Propriété du question","green","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton","green","");
		});
		$(".addFaq<?= $kunik?>").click(function(){
            dyFObj.openForm('poi',null, null,null,dyfPoi); 
        })
		
	});
</script>