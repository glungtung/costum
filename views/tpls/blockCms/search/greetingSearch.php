  <div class="sp-cms-container no-padding">
    <div class="search-bar" id="costum-scope-search">
        <div class="row">
          <div class="form-group col-xs-3 no-padding-right">
              <select id="catSearch">
                <option value="organization">un fabLab</option>
                <option value="equipment">une machine</option>
              </select>
          </div>
          <div id="locality">
          <div class="form-group col-xs-3 no-padding-right">
            <input type="text" class="form-control" placeholder="<?php echo Yii::t("common","Where")?> ?" id="location">
          </div>
          <div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-4 col-lg-4 no-padding" style="display: none;"></div>
          </div>

          <form id="search-form" action="#"> 
          <div class="form-group col-xs-6 no-padding-left">
            <div class="input-group">        
                <input type="text" class="form-control" id="otherInput" name="otherInput" placeholder="Vous recherchez un fablab, une machine, ...">
                <span class="input-group-btn">
                    <button class="btn btn-search-home" id="btn-search-home"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            <div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-4 col-lg-4 no-padding" style="display: none;"></div>
          </div>
          </form>
        </div>
    </div>
  </div>


<script type="text/javascript">
jQuery(document).ready(function() {

  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();

  $("#catSearch").change(function(){
    var categ=$(this).val();

  });

  $("#btn-search-home").click(function(e){
       e.preventDefault();
  });

  $("#otherInput").off().on("keyup",delay(function (e) {
          if(e.keyCode != 13){
            searchVal = $(this).val();
            autoCompleteSearchGS(searchVal,0,30,"#search-form");
           
          }

        }, 750)).on("keyup", function(e){
          if(e.keyCode == 13){
            searchVal = $(this).val();
            autoCompleteSearchGS(searchVal,0,30,"#search-form");
          }

        });

  // $("#otherInput").on("keyup",function(){
  //   var searchVal=$(this).val();
  //   var paramOrg={};
  //   paramOrg['$or']={};
  //   paramOrg['$or']["equipment."+searchVal]={$exists:1};

  //   autoCompleteSearchGS(searchVal,0,30,"#search-form");

    // $("dropdown-result-global-search").html("<div class='col-xs-12 text-center'><div class='search-loader text-dark col-xs-12' style='font-size:20px;'><i class='fa fa-spin fa-circle-o-notch'></i> Recherche en cours ...</div></div>");
    // ajaxPost(
    //   null,
    //   baseUrl+"/" + moduleId + "/search/globalautocomplete",
    //   {
    //     searchType : ["organizations"],
    //     text : searchVal
    //   },
    //   function(data){
    //     var resHtml='';
    //     $.each(data.results,function(id,elem){
    //       resHtml='<a href="javascript:;></a>"'

    //     });
    //     $(".dropdown-result-search")

    //   }
        
    // );
      
  //});

  function autoCompleteSearchGS(search, indexMin, indexMax, input, callB){
    mylog.log("globalsearch.js autoCompleteSearchGS", search, indexMin, indexMax, input, callB);

    var data = {"name" : search, "locality" : "", "searchType" : searchTypeGS, "searchBy" : "ALL",
    "indexMin" : indexMin, "indexMax" : indexMax, "category" : searchObject.category  };

    if(!notNull(input)){
      data.indexStep=10;
      data.count=true;
      data.countType = [ "citoyens", "organizations", "projects", "events" ];
      data.searchType = [ "citoyens", "organizations", "projects", "events" ];
    }

    if(typeof costum != "undefined" && notNull(costum) && typeof costum.filters != "undefined" && $.inArray(input, ["#filter-scopes-menu", "#scopes-news-form"]) < 0){
      if(typeof costum.filters.searchTypeGS != "undefined"){ 
        data.countType = costum.filters.searchTypeGS;
        data.searchType = costum.filters.searchTypeGS;
      }
      if(typeof costum.filters.sourceKey != "undefined"){ 
        data.sourceKey=[costum.slug];
      }
      if (typeof costum.filters.sourceKey != "undefined" && typeof costum.isTemplate != "undefined" && costum.isTemplate) {
        data.sourceKey=[costum.contextSlug];
      }

      var fieldsToRender=["equipment"];
      data.fields=fieldsToRender;

      data.count=true;
    }
    var domTarget = (notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
    var dropDownVisibleDom=(notNull(input)) ? input+" .dropdown-result-global-search" : ".dropdown-result-global-search";
     if($(domTarget+" .content-result").length > 0)
          domTarget+=" .content-result";
               
    showDropDownGS(true, dropDownVisibleDom);
    if(indexMin > 0)
      $("#btnShowMoreResultGS").html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...");
    else{
      $(domTarget).html("<h5 class='text-dark center padding-15'><i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyresearching+" ...</h5>");  
    }

  //  showIsLoading(true);

    if(search.indexOf("co.") === 0 ){
      var searchT = search.split(".");
      if( searchT[1] && typeof co[ searchT[1] ] == "function" ){
        co[ searchT[1] ](search);
        return;
      } else {
        co.mands();
      }
    }
    ajaxPost(
          null,
          baseUrl+"/"+moduleId+"/search/globalautocomplete",
          data,
          function(data){
            spinSearchAddon();
        if(!data){ toastr.error(data.content); }
        else{
          mylog.log("DATA GS");
          mylog.dir(data);

          var countData = 0;
          if(typeof data.count != "undefined")
            $.each(data.count, function(e, v){countData+=v;});
          else
            $.each(data.results, function(i, v) { if(v.length!=0){ countData++; } });

          totalDataGS += countData;

          var str = "";
          var city, postalCode, totalDataGSMSG = "";

          if(totalDataGS == 0)      totalDataGSMSG = "<i class='fa fa-ban'></i> "+trad.noresult;
          else if(totalDataGS == 1) totalDataGSMSG = totalDataGS + " "+trad.result;   
          else if(totalDataGS > 1)  totalDataGSMSG = totalDataGS + " "+trad.results;   

          if(totalDataGS > 0){
            var labelSearch=(Object.keys(data.results).length == totalDataGS) ? trad.extendedsearch : "Voir tous les résultats";
            str += '<div class="text-left col-xs-12" id="footerDropdownGS" style="">';
              str += "<label class='text-dark margin-top-5'><i class='fa fa-angle-down'></i> " + totalDataGSMSG + "</label>";
              str += '<a href="#search" class="btn btn-default btn-sm pull-right lbh" id="btnShowMoreResultGS">'+
                    '<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> '+labelSearch+
                  '</a>';
            str += '</div>';
            str += "<hr style='margin: 0px; float:left; width:100%;'/>";
          }
                  //parcours la liste des résultats de la recherche
          $.each(data.results, function(i, o) {
            mylog.log("globalsearch res : ", o);
            var typeIco = i;
            var ico = "fa-"+typeObj["default"].icon;
            var color = mapColorIconTop["default"];

            mapElementsGS.push(o);
            /*if(typeof( typeObj[o.type] ) == "undefined")
              var itemType="poi";*/
            typeIco = o.type;

            if(typeof o.typeOrga != "undefined")
              typeIco = o.typeOrga;

            var obj = (dyFInputs.get(typeIco)) ? dyFInputs.get(typeIco) : typeObj["default"] ;
            ico =  "fa-"+obj.icon;
            color = obj.color;
            
            htmlIco ="<i class='fa "+ ico +" fa-2x bg-"+color+"'></i>";
            if("undefined" != typeof o.profilThumbImageUrl && o.profilThumbImageUrl != ""){
              var htmlIco= "<img width='80' height='80' alt='' class='img-circle bg-"+color+"' src='"+baseUrl+o.profilThumbImageUrl+"'/>";
            }

            city="";

            var postalCode = o.postalCode;
            if (o.address != null) {
              city = o.address.addressLocality;
              postalCode = o.postalCode ? o.postalCode : o.address.postalCode ? o.address.postalCode : "";
            }

            var id = getObjectId(o);
            // var insee = o.insee ? o.insee : "";
            var type = o.type;
            if(type=="citoyens") type = "person";
            //var url = "javascript:"; //baseUrl+'/'+moduleId+ "/default/simple#" + o.type + ".detail.id." + id;
            var url = (notEmpty(o.collection) && notEmpty(id)) ? 
                    '#page.type.'+o.collection+'.id.' + id : "";

            //var onclick = 'urlCtrl.loadByHash("#' + type + '.detail.id.' + id + '");';
            var onclickCp = "";
            var target = " target='_blank'";
            var dataId = "";
            if(type == "city"){
              dataId = o.name; //.replace("'", "\'");
            }


            var tags = "";
            if(typeof o.tags != "undefined" && o.tags != null){
              $.each(o.tags, function(key, value){
                if(value != "")
                  tags +=   "<a href='javascript:' class='badge bg-red btn-tag'>#" + value + "</a>";
              });
            }

            if(typeof o.equipment != "undefined" && o.equipment != null){
              // $.each(equipment)
            }

            var name = typeof o.name != "undefined" ? o.name : "";
            if(typeof o.title != "undefined")
              name =  o.title;
            postalCode = (  typeof o.address != "undefined" &&
                      o.address != null &&
                      typeof o.address.postalCode != "undefined") ? o.address.postalCode : "";

            if(postalCode == "") postalCode = typeof o.postalCode != "undefined" ? o.postalCode : "";
            var cityName = (typeof o.address != "undefined" &&
                    o.address != null &&
                    typeof o.address.addressLocality != "undefined") ? o.address.addressLocality : "";
                      var countryCode=(typeof o.address != "undefined" && notNull(o.address) && typeof o.address.addressCountry != "undefined") ? "("+o.address.addressCountry+")" : ""; 
            var fullLocality = postalCode + " " + cityName+" "+countryCode;
            if(fullLocality == " Addresse non renseignée" || fullLocality == "" || fullLocality == " ") 
              fullLocality = "<i class='fa fa-ban'></i>";
            mylog.log("fullLocality", fullLocality);

            var description = ( typeof o.shortDescription != "undefined" &&
                      o.shortDescription != null) ? o.shortDescription : "";
            if(description == "") description = ( typeof o.description != "undefined" &&
                                o.description != null) ? o.description : "";
             
            // var startDate = (typeof o.startDate != "undefined") ? "Du "+dateToStr(o.startDate, "fr", true, true) : null;
            // var endDate   = (typeof o.endDate   != "undefined") ? "Au "+dateToStr(o.endDate, "fr", true, true)   : null;

            var followers = (typeof o.links != "undefined" && o.links != null && typeof o.links.followers != "undefined") ?
                            o.links.followers : 0;
            var nbFollower = 0;
            if(followers !== 0)                 
              $.each(followers, function(key, value){
              nbFollower++;
            });

            target = "";
            if(type=="proposals")
              url="javascript:;";
            var classA=(type=="proposals") ? "openCoopPanelHtml" : "lbh";
            var attrA="";
            if(type=="proposals"){ 
              attrA="data-coop-type='proposals' data-coop-id='"+id+"' data-coop-idparentroom='";
              if(typeof o.idParentRoom != "undefined") attrA+=o.idParentRoom;
              attrA+="' data-coop-parentid='"+data.parentId+"' data-coop-parenttype='"+data.parentType+"'";
            }
            mylog.log("type", type);
            if(type != "city" && type != "zone" ){ 
              str += "<a href='"+url+"' class='"+classA+" col-md-12 col-sm-12 col-xs-12 no-padding searchEntity' "+attrA+">";
              str += "<div class='col-md-2 col-sm-2 col-xs-2 no-padding entityCenter text-center'>";
              str +=   htmlIco;
              str += "</div>";
              str += "<div class='col-md-10 col-sm-10 col-xs-10 entityRight'>";

              str += "<div class='entityName text-dark'>" + name + "</div>";

              str += '<div data-id="' + dataId + '"' + "  class='entityLocality'>"+
              "<i class='fa fa-home'></i> " + fullLocality;

              if(nbFollower >= 1)
              str +=    " <span class='pull-right'><i class='fa fa-chain margin-left-10'></i> " + nbFollower + " follower</span>";

              str += '</div>';

              str += "</div>";

              str += "</a>";
            }else{
              o.input = input;
                  mylog.log("Here",o, typeof o.postalCode);

                  
              if(type == "city"){
                var valuesScopes = {
                  city : o._id.$id,
                  cityName : o.name,
                  postalCode : (typeof o.postalCode == "undefined" ? "" : o.postalCode),
                  postalCodes : (typeof o.postalCodes == "undefined" ? [] : o.postalCodes),
                  country : o.country,
                  allCP : o.allCP,
                  uniqueCp : o.uniqueCp,
                  level1 : o.level1,
                  level1Name : o.level1Name
                };

                if( notEmpty( o.nameCity ) ){
                  valuesScopes.name = o.nameCity ;
                }

                if( notEmpty( o.uniqueCp ) ){
                  valuesScopes.uniqueCp = o.uniqueCp;
                }
                if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
                  valuesScopes.level5 = o.level5 ;
                  valuesScopes.level5Name = o.level5Name ;
                }
                if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
                  valuesScopes.level4 = o.level4 ;
                  valuesScopes.level4Name = o.level4Name ;
                }
                if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
                  valuesScopes.level3 = o.level3 ;
                  valuesScopes.level3Name = o.level3Name ;
                }
                if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
                  valuesScopes.level2 = o.level2 ;
                  valuesScopes.level2Name = o.level2Name ;
                }

                valuesScopes.type = o.type;
                valuesScopes.key = valuesScopes.city+valuesScopes.type+valuesScopes.postalCode ;
                o.key = valuesScopes.key;
                mylog.log("valuesScopes city", valuesScopes);
                    myScopes.search[valuesScopes.key] = valuesScopes;
                str += directory.cityPanelHtml(o);
              }
              else if(type == "zone"){


                valuesScopes = {
                  id : o._id.$id,
                  name : o.name,
                  country : o.countryCode,
                  level : o.level
                };
                mylog.log("valuesScopes",valuesScopes);
                var typeSearchCity, levelSearchCity;
                if(o.level.indexOf("1") >= 0){
                  typeSearchCity="level1";
                  levelSearchCity="1";
                  valuesScopes.numLevel = 1;
                }else if(o.level.indexOf("2") >= 0){
                  typeSearchCity="level2";
                  levelSearchCity="2";
                  valuesScopes.numLevel = 2;
                }else if(o.level.indexOf("3") >= 0){
                  typeSearchCity="level3";
                  levelSearchCity="3";
                  valuesScopes.numLevel = 3;
                }else if(o.level.indexOf("4") >= 0){
                  typeSearchCity="level4";
                  levelSearchCity="4";
                  valuesScopes.numLevel = 4;
                }else if(o.level.indexOf("5") >= 0){
                  typeSearchCity="level5";
                  levelSearchCity="5";
                  valuesScopes.numLevel = 5;
                }
                if(notNull(typeSearchCity))
                  valuesScopes.type = typeSearchCity;       

                mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);

                if( notEmpty( o.level1 ) && valuesScopes.id != o.level1){
                  mylog.log("valuesScopes test", (valuesScopes.id != o.level1), valuesScopes.id, o.level1);
                  valuesScopes.level1 = o.level1 ;
                  valuesScopes.level1Name = o.level1Name ;
                }

                var subTitle = "";
                if( notEmpty( o.level5 ) && valuesScopes.id != o.level5){
                  valuesScopes.level5 = o.level5 ;
                  valuesScopes.level5Name = o.level5Name ;
                  subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
                }
                if( notEmpty( o.level4 ) && valuesScopes.id != o.level4){
                  valuesScopes.level4 = o.level4 ;
                  valuesScopes.level4Name = o.level4Name ;
                  subTitle +=  (subTitle == "" ? "" : ", ") +  o.level4Name ;
                }
                if( notEmpty( o.level3 ) && valuesScopes.id != o.level3 ){
                  valuesScopes.level3 = o.level3 ;
                  valuesScopes.level3Name = o.level3Name ;
                  subTitle +=  (subTitle == "" ? "" : ", ") +  o.level3Name ;
                }
                if( notEmpty( o.level2 ) && valuesScopes.id != o.level2){
                  valuesScopes.level2 = o.level2 ;
                  valuesScopes.level2Name = o.level2Name ;
                  subTitle +=  (subTitle == "" ? "" : ", ") +  o.level2Name ;
                }
                //objToPush.id+objToPush.type+objToPush.postalCode
                valuesScopes.key = valuesScopes.id+valuesScopes.type ;
                mylog.log("valuesScopes.key", valuesScopes.key, valuesScopes);
                myScopes.search[valuesScopes.key] = valuesScopes;

                mylog.log("myScopes.search", myScopes.search);
                o.key = valuesScopes.key;
                str += directory.zonePanelHtml(o);
              }
            }
          }); //end each
          extendMsg=trad.extendedsearch;
          extendUrl="#search";
          if(typeof costum != "undefined" && notNull(costum) && typeof costum.searchOpenMenu != "undefined"){
            extendMsg=costum.searchOpenMenu.msg;
            extendUrl=costum.searchOpenMenu.url;
          }

  //<<<<<<< HEAD
          // str += '<div class="text-center" id="footerDropdownGS">';
          //  str += "<label class='text-dark'>" + totalDataGSMSG + "</label><br/>";
          //  str += '<a href="'+extendUrl+'" class="btn btn-default btn-sm lbh" id="btnShowMoreResultGS">'+
          //          '<i class="fa fa-angle-right"></i> <i class="fa fa-search"></i> '+extendMsg+
          //        '</a>';
          // str += '</div>';

                if(countData==0 && searchTypeGS == "cities"){
                    str="<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'>"+
                          '<label class="text-dark italic"><i class="fa fa-ban"></i> '+trad.nocityfoundfor+' "'+search+'"</label><br/>'+
                          '<span class="info letter-blue"><i class="fa fa-info-circle"></i> '+trad.explainnofoundcity+'</span><br/>'+
                          '<button class="btn btn-blue bg-blue text-white main-btn-create" '+
                            'data-target="#dash-create-modal" data-toggle="modal">'+
                              '<i class="fa fa-plus-circle"></i> '+trad.createpage+
                          '</button>'+
                        "</div>";
                }


                  //on ajoute le texte dans le html
                  $(domTarget).html(str);
          //on scroll pour coller le haut de l'arbre au menuTop
          $(domTarget).scrollTop(0);

          //on affiche la dropdown
          showDropDownGS(true, dropDownVisibleDom);
          bindScopesInputEvent();
          if(notEmpty(callB)){
            callB();
          }
                coInterface.bindLBHLinks();

                //signal que le chargement est terminé
                mylog.log("loadingDataGS false");
                loadingDataGS = false;

              }

        //si le nombre de résultat obtenu est inférieur au indexStep => tous les éléments ont été chargé et affiché
        if(indexMax - countData > indexMin){
          $("#btnShowMoreResultGS").remove(); 
          scrollEndGS = true;
        }else{
          scrollEndGS = false;
        }
          },
          function (data){
        mylog.log("error"); mylog.dir(data);          
      }
    );             
  }

  

  var minlength = 3,
    searchRequest = null,
    dataSearch = searchInterface.constructObjectAndUrl();

  dataSearch.searchType = ["cities"];

  $("#location").keyup(function () {
      var value = $(this).val();

     if (value.length >= minlength ) {
      delay(function(){
        $("#locality .dropdown-result-global-search").show();
          if (searchRequest != null) 
              searchRequest.abort();
          $("#locality .dropdown-result-global-search").html("<div class='col-xs-12 text-center'><div class='search-loader text-dark col-xs-12' style='font-size:20px;'><i class='fa fa-spin fa-circle-o-notch'></i> Recherche en cours ...</div></div>");
          dataSearch.name = value;
          searchRequest = $.ajax({
              type: "POST",
              url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
              data: dataSearch,
              success: function(result){
                  mylog.log("inputResult",result);
                  //alert(result.results.length+" resultat");
                  if (result.results.length == 0) {
                    var city = $("#location").val();
                    $("#locality .dropdown-result-global-search").html("<div class='text-center col-md-12 col-sm-12 col-xs-12 padding-10'><label class='text-dark italic'><i class='fa fa-ban'></i> Aucune ville trouvée pour '"+city+"'</label><br><span class='info letter-blue'><i class='fa fa-info-circle'></i> Il est possible qu\'aucune donnée ne soit enregistrée dans cette zone</span></div>");
                  }else {

                    htmlView="";
                    htmlView+='<div class="text-left col-xs-12" id="footerDropdownGS" style=""><label class="text-dark margin-top-5"><i class="fa fa-angle-down"></i> '+result.results.length+' résultats</label></div><hr style="margin: 0px; float:left; width:100%;">';
            $.each(result.results, function(e,v){
              var cp = (typeof v.postalCode != 'undefined') ? v.postalCode : '';
              var country = (typeof v.country != 'undefined') ? v.country : v.countryCode;
              mylog.log("valiny",v);
              htmlView+='<a href="javascript:" class="col-md-12 col-sm-12 col-xs-12 no-padding communecterSearch item-globalscope-checker searchEntity" data-scope-id="'+v._id.$id+'" data-scope-value="'+v._id.$id+'city'+v.postalCode+'" data-scope-level1name="'+v.level1Name+'" data-scope-level="'+v.level1+'" data-scope-country="'+v.country+'" data-scope-name="'+v.name+'" data-scope-level="city" data-scope-cp="'+cp+'" data-scope-postalcodes="'+v.postalCodes+'" data-scope-type="'+v.type+'" data-level3="'+v.level3+'" data-level3Name="'+v.level3Name+'" data.level4="'+v.level4+'" data-level4Name="'+v.level4Name+'" data-scope-notsearch="true" data-append-container="#costum-scope-search .scopes-container">'+
                  '<div class="col-xs-12 margin-bottom-10 city undefined ">'+
                    '<div class="padding-10 informations">'+
                      '<div class="entityRight no-padding">'+
                        '<span class="entityName letter-red ">'+
                          '<i class="fa fa-university"></i><span> '+v.name+'</span><br>'+
                            '<span style="color : grey; font-size : 13px">'+country+'</span>'+
                        '</span>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</a>';
            });
            $( '#locality .dropdown-result-global-search' ).html(htmlView);

            $('.communecterSearch').on('click', function () {
              var cityName=$(this).data("scope-name");
              $("#location").val(cityName);
              $( '#locality .dropdown-result-global-search' ).html("");
              $( '#locality .dropdown-result-global-search' ).css("display","none");
              $('#otherInput').attr("placeholder","Que souhaitez-vous localiser à "+cityName+" ?");
               
              // var btnScope = $(this);
              // if (btnScope.data("scope-type") == "city") var typeCitie = "cities"
              // else var typeCitie = "zones"
  
              // if (btnScope.data("scope-cp") != "") var cpData = "cp"+btnScope.data("scope-cp")
              // else var cpData = ""

              // location.hash = "#search?scopeType=open&"+typeCitie+"="+btnScope.data('scope-id')+cpData;
              // urlCtrl.loadByHash(location.hash);  
            });

        }


              }
          });
      }, 1000 );
     }else $("#locality .dropdown-result-global-search").hide();
  });

});    

</script>
