<?php 
  $keyTpl ="music_album";
  $paramsData = [ 
    "title" => "ALBUM",
    "musicColor" => "#FFFFFF",
    "musicActive" => "#F0AD16",
    "musicHover" => "#F0AD16"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
  HtmlHelper::registerCssAndScriptsFiles([
  	"/css/blockcms/soundcloud/colors.css",
  	"/css/blockcms/soundcloud/structure.css",

  	"/js/blockcms/soundcloud/soundcloud.player.api.js",
  	"/js/blockcms/soundcloud/sc-player.js"],
  	 $assetsUrl);


  //GET music playList
  $contextSlug = isset($costum["contextSlug"]) ? $costum["contextSlug"] : $el["slug"];
  $thisContextSlug = isset($costum["contextSlug"]) ? $costum["contextSlug"] : $el["slug"];
  $where =array();
  if(isset($costum["contextSlug"]))
  	$where = array("category" => "audio","type" => "link","source.key" => $contextSlug);
  if(isset($costum["contextSlug"]))
  	$where = array("category" => "audio","type" => "link","source.key" => $thisContextSlug);
  else
  	$where = array("category" => "audio","type" => "link","parent".(string)$el["_id"] => $contextSlug);
  	$playList = Poi::getPoiByWhereSortAndLimit($where);
  	/*var_dump($playList);*/
  	$albumName = array();
?>
<style>

	/* controls */

	.sc-player .sc-controls a {
	  color: transparent;
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play.png') no-repeat top /26px;
	}

	.sc-player .sc-controls a:hover {
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play-hover.png') no-repeat top /26px;
	}

	.sc-player .sc-controls a.sc-pause {
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play.png') no-repeat top /26px;
	}

	.sc-player .sc-controls a.sc-pause:hover{
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/play-hover.png') no-repeat top /26px;
	}

	.sc-player.playing .sc-controls a.sc-pause{
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/pause.png') no-repeat top /26px;
	}

	.sc-player.playing .sc-controls a.sc-pause:hover{
	  background: url('<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/pause-hover.png') no-repeat top /26px;
	}  

	.sc-scrubber .sc-time-indicators{
	  background: #fff;
	  color: #000;
	  -moz-border-radius: 4px;
	  -webkit-border-radius: 4px;
	  padding: 2px;
	  font-size: 0.4em;
	  font-weight: normal;
	  line-height: 1em;
	}
	.sc-player.playing ol.sc-trackslist li.active a {
	  color: <?= $paramsData["musicActive"] ?>;
	}
	.sc-player a {
	  text-decoration: none;
	  color: <?= $paramsData["musicColor"] ?>;
	}
	.sc-player:hover a {
	  color: <?= $paramsData["musicHover"] ?>;
	}

	.ablum-container-<?= $kunik ?>{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		margin-top: 80px;
	}
	.switch-album-<?= $kunik ?>{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		justify-content: center;
		align-items: center;
	}
	.switch-album-<?= $kunik ?>>div{
	    height: 100%;
	    position: relative;
	    display: flex;
	    flex-direction: column;
	    justify-content: center;
	    align-items: center;
	    cursor: pointer;
	    border-radius: 5px;
	}
	.switch-album-<?= $kunik ?>>div:hover{
		border:2px solid #f0ad16;
		box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);
	}
	.switch-album-<?= $kunik ?>>div>img{
		height: 155px;
		object-fit: cover;
	}
	.img-container-<?= $kunik ?>{
		text-align: -webkit-center !important;
	}
	.img-container-<?= $kunik ?> img{
		height: 100%;
		width: 100%;
		object-fit: contain;
		object-position: center;
		padding: 0 7%;
	}
	.table-container-<?= $kunik ?>{
		padding: 0 20px 0 20px;
	}
	.container-<?= $kunik ?> .table>tbody>tr{
		font-size: 23px;
    	font-family: 'Montserrat';
	}
	.container-<?= $kunik ?> .table>tbody>tr>td{
		padding: 5px !important;
		background: #000 !important;
    	border-top-color: #000 !important;
	}
	@media (max-width:500px){
		.img-container-<?= $kunik ?> img{padding: 0 5px;}
	}
	@media (max-width:765px){
		.ablum-container-<?= $kunik ?>{
			flex-direction: column;
		}
	}
</style>
<h1 class="title"><?= $paramsData["title"] ?></h1>
<?php if(Authorisation::isInterfaceAdmin()){ ?>
	<p class="text-center">
		<button class="btn btn-success add-audio<?=$kunik ?> hiddenPreview">Créer album</button>
	</p>
<?php } ?>
<div class="switch-album-<?= $kunik ?> container-<?= $kunik ?>">
	<?php $pj = 0; foreach ($playList as $ksw => $vsw) { ?>
		<div class="album-audio-menu" data-id="<?= $ksw ?>">
			<h6 class="title-6"><?= $vsw["name"] ?></h6>
			<img src="<?= isset($vsw["profilMediumImageUrl"]) ? $vsw["profilMediumImageUrl"] : $assetsUrl."/images/blockCmsImg/defaultImg/Optimized-CD-117mm.png" ?>" alt="">
			<?php if(Authorisation::isInterfaceAdmin()){ ?>
				<div class="btn-group hiddenPreview" style="width:100%">
				  <a href="javascript:;" class="btn btn-sm btn-primary edit-album-<?= $kunik ?>" data-id="<?= $ksw ?>"  style="width:50%">
				  	<i class="fa fa-edit"></i>
				  </a>
				  <a href="javascript:;" class="btn btn-sm btn-danger delete-album-<?= $kunik ?>" data-id="<?= $ksw ?>" style="width:50%">
				  	<i class="fa fa-trash"></i>
				  </a>
				</div>
			<?php } ?>
		</div>
	<?php $pj++; } ?>
</div>

<?php $pi = 0; foreach ($playList as $kpl => $vpl) { ?>
	<div class="ablum-audio-container ablum-container-<?= $kunik ?>  <?= $kpl ?>" style="<?= ($pi != 0) ? "display:none" : ""; ?>">
		<div class="img-container-<?= $kunik ?> bg-grey text-center" style="flex:50%">
			<!-- <img src="<?= $assetsUrl ?>/images/blockCmsImg/defaultImg/Optimized-CD-117mm.png" alt=""> -->
			<img src="<?= isset($vpl["profilImageUrl"]) ? $vpl["profilImageUrl"] : $assetsUrl."/images/blockCmsImg/defaultImg/Optimized-CD-117mm.png"  ?>" alt="">
		</div>
		<div class="table-container-<?= $kunik ?>" style="flex:50%">
		<?php foreach ($vpl["urls"] as $kurl => $vurl) { ?>
			<a href="<?= $vurl ?>" class="sc-player">My dub track</a>
		<?php } ?>
		</div>
	</div>
<?php $pi++; } ?>


	


<script>
	  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	  $(function(){
	      sectionDyf.<?php echo $kunik ?>Params = {
	        "jsonSchema" : {    
	          "title" : "Configurer votre section",
	          "description" : "Personnaliser votre gallerie",
	          "icon" : "fa-cog",
	          
	          "properties" : {
	              title:{
	              	inputType:"text",
	              	label:"Titre",
	              	value: "<?= $paramsData["title"] ?>"
	              },
	              musicColor:{
	              	inputType:"colorpicker",
	              	label:"Couleur des pistes musicales",
	              	value: "<?= $paramsData["musicColor"] ?>"
	              },
	              musicHover:{
	              	inputType:"colorpicker",
	              	label:"Couleur d'u piste musicale au survol de souris",
	              	value: "<?= $paramsData["musicHover"] ?>"
	              },
	              musicActive:{
	              	inputType:"colorpicker",
	              	label:"Couleur d'un piste musicale en lecture",
	              	value: "<?= $paramsData["musicActive"] ?>"
	              }
	          },
	          beforeBuild : function(){
	              uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
	          },
	          save : function (data) {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	              tplCtx.value[k] = $("#"+k).val();
	              if (k == "parent")
	                tplCtx.value[k] = formData.parent;
	            });
	            console.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	              toastr.error('value cannot be empty!');
	              else {
	                dataHelper.path2Value( tplCtx, function(params) {
	                  dyFObj.commonAfterSave(params,function(){
	                    toastr.success("Élément bien ajouté");
	                    $("#ajax-modal").modal('hide');
						var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    // urlCtrl.loadByHash(location.hash);
	                  });
	                } );
	              }

	          }
	        }
	      };


	      $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
	        tplCtx.id = $(this).data("id");
	        tplCtx.collection = $(this).data("collection");
	        tplCtx.path = "allToRoot";
	        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"music",4,12,null,null,"Musique","blue","");
	      });

	      $(".sc-player").click(function(){
	      	setTimeout(function(){
	      		$('.sc-player.playing').find('.sc-pause').removeClass('hidden');
	      	},300)
	      })
	      $(".album-audio-menu").click(function(){
	      	var id = $(this).data("id");
	      	$(".ablum-audio-container").fadeOut(1000);
	      	$("."+id).fadeIn(1000);
	      	$('.sc-player.playing a.sc-pause').click();
	      })

	      $('.add-audio<?=$kunik ?>').click(function(){
		        var dyfAudio={
		          "beforeBuild":{
		            "properties" : {
		            	info : {
			                inputType : "custom",
			                html:"<p class='text-"+typeObj["poi"].color+"'>"+
			                		"Exemple : https://soundcloud.com/brunomars/24k-magic"+
			                		"<hr>"+
								 "</p>",
			            },
		            	"name": {
		                    "label" : "Nom de l'album",
		                    "placeholder" : "24K MAGIC",
		                    "order" : 1,

		                },
		                image: {
						  "inputType": "uploader",
						  "docType": "image",
						  "label": "Photo de l'album",
						  "showUploadBtn": false,
						  "itemLimit" : 1,
						  "template": "qq-template-gallery",
						  "filetypes": ["jpeg","jpg","gif","png"]
						},
		                "category" : {
			                "inputType" : "hidden",
			                "value" : "audio"
		              	},
		              	"urls" : {
						  "label": "Url/Lien soundcloud.com",
						  "placeholder": "https://soundcloud.com/brunomars/24k-magic",
						  "inputType": "array",
						  "value": []
						}
		            }
		          },
		          "onload" : {
		            "actions" : {
		              	"setTitle" : "Ajouter des urls audio soundCloud",
		              	"src" : { 
		                	"infocustom" : "Remplir le champ"
		              	},
		              	"presetValue" : {
		                    "type" : "link"
		                },
			            "hide" : {
			              	"breadcrumbcustom" : 1,
		                    "tagstags" :1,
		                    "descriptiontextarea":1,
		                    "locationlocation":1,
		                    "formLocalityformLocality":1,
		                    "parentfinder" : 1,
		                    "removePropLineBtn" : 1
			            }
		        	}
		           }

		          }
		        dyfAudio.afterSave = function(data){
		          dyFObj.commonAfterSave(data, function(){
		           	urlCtrl.loadByHash(location.hash);

		          });
		        }
		        dyFObj.openForm('poi',null, null,null,dyfAudio);
			})


	      	$('.edit-album-<?= $kunik ?>').click(function(e){
	      		e.stopPropagation();
	      		var data = $(this).data('content');
	      		var id = $(this).data('id');
		        var dyfAudio={
		          "beforeBuild":{
		            "properties" : {
		            	info : {
			                inputType : "custom",
			                html:"<p class='text-"+typeObj["poi"].color+"'>"+
			                		"Exemple : https://soundcloud.com/brunomars/24k-magic"+
			                		"<hr>"+
								 "</p>",
			            },
		            	"name": {
		                    "label" : "Nom de l'album",
		                    "placeholder" : "24K MAGIC",
		                    "order" : 1,

		                },
		                image: {
						  "inputType": "uploader",
						  "docType": "image",
						  "label": "Photo de l'album",
						  "showUploadBtn": false,
						  "itemLimit" : 1,
						  "template": "qq-template-gallery",
						  "filetypes": ["jpeg","jpg","gif","png"]
						},
		                "category" : {
			                "inputType" : "hidden",
			                "value" : "audio"
		              	},
		              	"urls" : {
						  "label": "Url / Lien soundcloud.com",
						  "placeholder": "https://soundcloud.com/brunomars/24k-magic",
						  "inputType": "array",
						}
		            }
		          },
		          "onload" : {
		            "actions" : {
		              	"setTitle" : "Ajouter des urls audio soundCloud",
		              	"src" : { 
		                	"infocustom" : "Remplir le champ"
		              	},
		              	"presetValue" : {
		                    "type" : "link"
		                },
			            "hide" : {
			              	"breadcrumbcustom" : 1,
		                    "tagstags" :1,
		                    "descriptiontextarea":1,
		                    "locationlocation":1,
		                    "formLocalityformLocality":1,
		                    "parentfinder" : 1,
		                    "removePropLineBtn" : 1
			            }
		        	}
		           }

		          }
		        dyfAudio.afterSave = function(data){
		          dyFObj.commonAfterSave(data, function(){
		           	urlCtrl.loadByHash(location.hash);

		          });
		        }
		        dyFObj.editElement('poi',id,"link",dyfAudio);
			})
			$('.delete-album-<?= $kunik ?>').on('click',function(e){
				e.stopPropagation();
                  var idAlbum = $(this).data('id');
                  bootbox.confirm(trad.areyousuretodelete, function(result){ 
                      if(result){
                        var url = baseUrl+"/"+moduleId+"/element/delete/id/"+idAlbum+"/type/poi";
                        var param = new Object;
                        ajaxPost(null,url,param,function(data){ 
                                if(data.result){
                                  toastr.success(data.msg);
                                  urlCtrl.loadByHash(location.hash);
                                }else{
                                  toastr.error(data.msg); 
                              }
                        })
                      }
                  })
              });
	  });
</script>