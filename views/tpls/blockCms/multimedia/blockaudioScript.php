<script>
    let audio = new Audio(), metadata_loaded = false;

    const utils = {
        zeroFill: (number) => number < 10 ? `0${number}`:number,
        formatSeconds: (time) => `${utils.zeroFill(parseInt(time/60))}:${utils.zeroFill(parseInt(time%60))}`,
        getProgressPercent: (duration, currentTime) => parseInt((currentTime * 100)/duration),
        parsePercentToTime: (duration, percent) => (percent * duration) / 100
    }

    const dataFromBack = {
        kunik: "<?= $kunik ?>",
        paramsData: <?= json_encode( $paramsData ) ?>
    }

    $(function(){
        const { kunik, paramsData } = dataFromBack

        $('.play-audio').click(function(){
            const title = $(this).data('title'),
                  url = $(this).data('url');

            metadata_loaded = false
            audio.setAttribute('src', url)
            audio.load()
            audio.play()

            $('#player-title').text(title)
            //$('#player-status-state').text(`00:00 / ${ $(this).data('duration') }`)
            $('.player-container').css({
                bottom:  0,
                visibility: 'visible',
                opacity: 1
            })
        })

        $('#player-stop').click(() => {
            audio.pause()
            audio.currentTime = 0
            $('.player-container').css({
                bottom:  '-70px',
                visibility: 'hidden',
                opacity: 0
            })
        })

        $('#player-pause').click(function(){
            if(audio.paused)
                audio.play()
            else
                audio.pause()
            $(this).find('i').toggleClass('glyphicon-pause').toggleClass('glyphicon-play')
        })

        let manuel_change = false;
        $('#player-track-range').on('mousedown', ()=>{
            manuel_change = true;
        }).on('mouseup', function(){
            audio.currentTime = utils.parsePercentToTime(audio.duration, $(this).val())
            manuel_change = false
        })

        audio.onloadedmetadata = () =>  {
            metadata_loaded = true;
            $('#audio-volume').val(parseInt(audio.volume * 100))
        }

        $('#audio-volume').change(function(){
            audio.volume = $(this).val() / 100
        })

        audio.ontimeupdate = () => {
            if(metadata_loaded){
                const duration = audio.duration,
                    currentTime = audio.currentTime;
                const { formatSeconds, getProgressPercent } = utils;

                $('#player-status-state').text(`${formatSeconds(currentTime)} / ${formatSeconds(duration)}`)
                if(!manuel_change)
                    $('#player-track-range').val(getProgressPercent(duration, currentTime))
            }
        }
        
        sectionDyf[`${kunik}ParamsData`] = paramsData
        sectionDyf[`${kunik}Params`] = {
            jsonSchema:{
                title: "Configurer le block audio",
                icon: "fa-cog",
                properties:{
                    
                    title:{
                        label:"Titre",
                        value: sectionDyf[`${kunik}ParamsData`].title
                    },
                    colorTheme:{
                        label: "Theme",
                        inputType: "colorpicker",
                        values: sectionDyf[`${kunik}ParamsData`].colorTheme
                    },
                    itemDirection:{
                        label: "Direction",
                        inputType: "select",
                        options: {
                            'row': 'ligne',
                            'column': 'colonne'
                        },
                        values: sectionDyf[`${kunik}ParamsData`].itemDirection
                    }
                },
                save: () => {
                    tplCtx.value = {}

                    $.each(sectionDyf[`${kunik}Params`].jsonSchema.properties, (k, val) => {
                        tplCtx.value[k] = $(`#${k}`).val()
                    })

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!')
                    else{
                        dataHelper.path2Value(tplCtx, () => {
                            $("#ajax-modal").modal('hide');
                            toastr.success("élement mis à jour");
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        })
                    }
                }
            }
        }

        $(`.edit${ kunik }Params`).off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf[`${kunik}Params`],null, sectionDyf[`${kunik}ParamsData`]);
        });

        $('.btn-change-audio-layout').click(function(){
            const layout = $(this).data('layout')
            $('.audio-list').removeClass('d-block').addClass('d-none')
            $(`.${layout}`).removeClass('d-none').addClass('d-block')
            $('.btn-change-audio-layout').removeClass('active')
            $(this).addClass('active')
        })
    })
</script>