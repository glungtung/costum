<?php
    /**
     * Block qui permet d'afficher les videos youtube dans la bookmarks
     */

    /* CONSTANTS */
    $assets_url = Yii::app()->getModule('costum')->assetsUrl;

    /* Paramètres du bloc */
    $paramsData = [
        "title"=>"Mes videos",
        "titleAlign"=>"center"
    ];
    //surcharge du paramsData
    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

    /* Recuperer les liens youtube dans la collection bookmarks */
    $bookmarks = PHDB::find("bookmarks", [ "parentId"=>$costum["contextId"], "category"=>"youtube" ]);
    foreach($bookmarks as $index=>$bookmark){
        //parse l'url pour recuperer l'id du video
        parse_str( parse_url( $bookmark['url'], PHP_URL_QUERY ), $url_vars );
        $bookmarks[$index]['youtubeId'] = $url_vars['v'];
    }
?>

<!-- importation du style de youtube player -->
<link rel="stylesheet" href="<?= "$assets_url/css/blockcms/videoYoutube/plyr.css" ?>">
<!-- importation du script contenant le style du bloc -->
<?= 
    $this->renderPartial(
        "costum.views.tpls.blockCms.multimedia.youtubevideos.youtubevideosStyle", 
        ["kunik" => $kunik]
    ) 
?>

<div class="<?= $kunik ?>">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="youtubevideos-header text-<?= $paramsData['titleAlign'] ?>">
                <h1><?= $paramsData['title'] ?></h1>
            </div>
            <div class="row">
                <?php 
                    foreach($bookmarks as $bookmark) { 
                        $video_thumb = "https://img.youtube.com/vi/".$bookmark['youtubeId']."/hqdefault.jpg";
                ?>
                <div class="col-md-4 col-sm-12">
                    <div class="video-item" style="background-image: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,.6)), url('<?= $video_thumb ?>')">
                        <p><?= $bookmark['name'] ?></p>
                        <div class="btn-play-video-container">
                            <a href="#" class="play-video" data-vidid="<?= $bookmark['youtubeId'] ?>">
                                <i class="glyphicon glyphicon-play"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="youtube-player-container">
        <div class="youtube-player-content">
            <div class="youtube-player-header">
                <button class="btn-close-youtube-player">
                    <i class="glyphicon glyphicon-remove"></i>
                </button>
            </div>
            <div class="plyr__video-embed" id="player">
                <iframe
                    allowfullscreen
                    allowtransparency
                    allow="autoplay"
                ></iframe>
            </div>
        </div>
    </div>
    
</div>

<script src="<?= "$assets_url/js/blockcms/videoYoutube/plyr.min.js" ?>"></script>
<!-- importation du script contenant le script du bloc -->
<?= 
    $this->renderPartial(
        "costum.views.tpls.blockCms.multimedia.youtubevideos.youtubevideosScript", 
        [
            "kunik" => $kunik,
            "paramsData" => $paramsData
        ]
    ) 
?>
