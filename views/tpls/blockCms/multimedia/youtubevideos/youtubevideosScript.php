<script>
    const kunik = "<?= $kunik ?>";
    const paramsData = <?= json_encode( $paramsData ) ?>;

    const selectors = {
        playVideo: `.${kunik} .play-video`,
        youtubePlayerContainer: `.${kunik} .youtube-player-container`,
        playerIframe: `.${kunik} #player iframe`,
        player: `.${kunik} #player`,
        btnClosePlayer: `.${kunik} .btn-close-youtube-player`
    }

    $(function(){
        let player = null

        $(selectors.playVideo).click(function(){
            $(selectors.youtubePlayerContainer).css({ opacity:1, visibility: 'visible' })
            $(selectors.playerIframe).attr('src', `https://www.youtube.com/embed/${$(this).data('vidid')}`)
            player = new Plyr(selectors.player, {  
                autoplay: true 
            })
        })

        $(selectors.btnClosePlayer).click(function(){
            $(selectors.youtubePlayerContainer).css({ opacity:0, visibility: 'hidden' })
            player.destroy();
        })

        /* CONFIGURATION DYFORM PARAMETRAGE du bloc =======> */
        sectionDyf[`${kunik}ParamsData`] = paramsData;
        sectionDyf[`${kunik}Params`] = {
            jsonSchema:{
                title: "Configuration du bloc videos",
                icon: "fa-cog",
                properties:{
                    title:{
                        label:"Titre",
                        value: sectionDyf[`${kunik}ParamsData`].title
                    },
                    titleAlign:{
                        label: "Alignement du titre",
                        inputType: "select",
                        options: {
                            'center': 'Au centre',
                            'left': 'A gauche',
                            'right': 'A droite'
                        },
                        values: sectionDyf[`${kunik}ParamsData`].itemDirection
                    }
                },
                save: () => {
                    tplCtx.value = {}

                    $.each(sectionDyf[`${kunik}Params`].jsonSchema.properties, (k, val) => {
                        tplCtx.value[k] = $(`#${k}`).val()
                    })

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!')
                    else{
                        dataHelper.path2Value(tplCtx, () => {
                            $("#ajax-modal").modal('hide');
                            toastr.success("élement mis à jour");
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        })
                    }
                }
            }
        }
        /* <======= CONFIGURATION DYFORM PARAMETRAGE du bloc */

        //Open paramètres form
        $(`.edit${ kunik }Params`).off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf[`${kunik}Params`],null, sectionDyf[`${kunik}ParamsData`]);
        });
    })
</script>