<!-- variables global -->
<?php
    $keyTpl = "blockaudio";
    $paramsData = [
        "colorTheme" => "#5b1343",
        "itemDirection" => "row",
        "title" => "Mes audios"
    ];
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    $bookmarks = PHDB::find("bookmarks", [ "parentId"=>$costum["contextId"], "category"=>"link" ]);

    $bookmarks_audio = array();
    foreach($bookmarks as $bookmark){
        $bookmark_url_ext = array_reverse(explode(".", $bookmark['url']))[0];
        $audio_ext = ["mp3", "ogg"];
        if(in_array($bookmark_url_ext, $audio_ext)){
            $bookmarks_audio[] = $bookmark;
        }
    }

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<!-- block style -->
<?= $this->renderPartial("costum.views.tpls.blockCms.multimedia.blockaudioStyle", ["colorTheme" => $paramsData['colorTheme']]) ?>


</div>
<div class="blockaudio">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="action-container">
                <p class="title"><?= $paramsData['title'] ?></p>
                <ul>
                    <li class="btn-change-audio-layout <?= (($paramsData['itemDirection'] === "row")) ? "active":"" ?>" data-layout="audio-list-horizontal"><i class="glyphicon glyphicon-th-large"></i></li>
                    <li class="btn-change-audio-layout <?= (($paramsData['itemDirection'] === "column")) ? "active":"" ?>" data-layout="audio-list-vertical"><i class="glyphicon glyphicon-th-list"></i></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="audio-list audio-list-vertical <?= (($paramsData['itemDirection'] === "column")) ? "d-block":"d-none" ?>">
        <div class="row">
            <?php foreach($bookmarks_audio as $bookmark_audio) { ?>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="audio-item-column">
                        <div class="audio-icon-container">
                            <div class="icon">
                                <img src="<?= $assetsUrl ?>/images/blockCmsImg/icons8-music-192.png" alt="">
                            </div>
                        </div>
                        <div class="audio-description-container">
                            <p class="title">
                                <?= $bookmark_audio['name'] ?>
                            </p>
                            <div class="status"><!-- 
                                <div class="duration-likes-container">
                                    <p class="likes">
                                        <i class="glyphicon glyphicon-heart"></i>
                                        73
                                    </p>
                                </div> -->
                                <div class="date-added-container">
                                    <p>il y a 2 jours</p>
                                </div>
                            </div>
                        </div>
                        <div class="audio-actions-container">
                            <ul>
                                <li>
                                    <a 
                                        href="javascript:void(0)" 
                                        class="play-audio"
                                        data-url="<?= $bookmark_audio['url'] ?>"
                                        data-title="<?= $bookmark_audio['name'] ?>"
                                    >
                                        <span class="glyphicon glyphicon-play"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="glyphicon glyphicon-download-alt"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-heart-empty"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="audio-list audio-list-horizontal <?= (($paramsData['itemDirection'] === "row")) ? "d-block":"d-none" ?>"">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="row">
                    <?php foreach($bookmarks_audio as $bookmark_audio) { ?>
                        <div class="col-sm-3 mt-1">
                            <div class="audio-item-row">
                                <div class="header">
                                    <img src="<?= $assetsUrl ?>/images/blockCmsImg/icons8-music-192.png" alt="">
                                    <ul class="options">
                                        <li>
                                            <a 
                                                href="javascript:void(0)" 
                                                class="play-audio"
                                                data-url="<?= $bookmark_audio['url'] ?>"
                                                data-title="<?= $bookmark_audio['name'] ?>"
                                            >
                                                <span class="glyphicon glyphicon-play"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="glyphicon glyphicon-download-alt"></span></a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="glyphicon glyphicon-heart-empty"></span></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="content">
                                    <p class="title"><?= $bookmark_audio['name'] ?></p>
                                </div>
                                <div class="footer">
                                    <!-- <p>
                                        <i class="glyphicon glyphicon-heart"></i>
                                        73
                                    </p> -->
                                    <p>il y a 2 jours</p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="player-container">
        <div class="player-content">
            <div class="left-content">
                <div class="logo">
                    <img src="<?= $assetsUrl ?>/images/blockCmsImg/icons8-music-192.png" alt="">
                </div>
                <div class="player">
                    <div class="status">
                        <p class="title" id="player-title">Lorem ipsum dolor sit amet</p>
                        <p class="state" id="player-status-state">02:25 / 04:15</p>
                    </div>
                    <div class="controls">
                        <input type="range" min="0" max="100" value="0" class="slider" id="player-track-range">
                        <ul>
                            <li>
                                <a href="javascript:void(0)"  id="player-pause"><i class="glyphicon glyphicon-pause"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" id="player-stop"><i class="glyphicon glyphicon-stop"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right-content">
                <input type="range" min="0" max="100" value="70" class="slider" id="audio-volume">
                <a href="#">
                    <i class="glyphicon glyphicon-volume-up"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
<!-- block script -->
<?= 
    $this->renderPartial(
        "costum.views.tpls.blockCms.multimedia.blockaudioScript", 
        [
            "paramsData"=>$paramsData,
            "kunik"=>$kunik
        ]
    ) 
?>