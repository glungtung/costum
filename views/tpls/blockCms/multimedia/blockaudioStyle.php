<!-- main styles =======> -->
<style type="text/css">
    .blockaudio p{
        margin: 0;
        padding: 0;
    }

    .blockaudio ul{
        padding: 0;
        margin: 0;
    }

    .blockaudio .mt-1{
        margin-top:10px;
    }

    .d-none{
        display:none;
    }

    .d-block{
        display:block;
    }

    .blockaudio .action-container{
        display:flex;
        align-items:center;
        justify-content:space-between;
        margin-bottom: 20px;
        background-color: <?= $colorTheme ?>;
        color: white;
        padding: 0px 15px;
    }

    .blockaudio .action-container .title{
        font-size: 2em;
        font-weight: bold;
        text-transform: uppercase;
    }

    .blockaudio .action-container ul li{
        display: inline-block;
        list-style: none;
        padding: 10px 4px;
    }

    .blockaudio .action-container ul li i{
        font-size: 1.2em;
        color: rgb(255,255,255, .5);
        vertical-align: sub;
        cursor:pointer;
        transition: .3s
    }
    .blockaudio .action-container ul li i:hover{
        color: white;
    }

    .blockaudio .action-container ul li.active i{
        color: white;
    }
</style>
<!-- <========= main styles -->

<!-- audio item column styles =========> -->
<style type="text/css">
    .audio-item-column{
        width: 100%;
        display: flex;
        align-items: center;
        padding: 15px 0px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    }

    .audio-item-column:hover > .audio-actions-container ul li{
        transform: translateY(0);
        opacity: 1;
    }

    .audio-item-column .audio-icon-container .icon{
        width: 50px;
        height: 50px;
        border-radius: 100%;
        background-color: <?= $colorTheme ?>;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .audio-item-column .audio-icon-container .icon img{
        width: 35px;
    }

    .audio-item-column .audio-description-container .title{
        font-size: 1.2em;
        font-weight: bold;
    }

    .audio-item-column .audio-description-container{
        width: 100%;
        padding: 0px 15px;
    }

    .audio-item-column .audio-description-container .status{
        display: flex;
        align-items: center;
        color: #707070;
    }

    .audio-item-column .audio-description-container .status p {
        font-size: 14px;
    }

    .audio-item-column .audio-description-container .status .duration-likes-container{
        display: flex;
        align-items: center;
    }

    .audio-item-column .audio-description-container .status .duration-likes-container .likes{
        margin: 0px 15px;
    }

    .audio-item-column .audio-description-container .status .duration-likes-container .likes i{
        color: red;
    }

    .audio-item-column .audio-actions-container ul{
        white-space: nowrap;
        user-select: none;
    }

    .audio-item-column .audio-actions-container ul li{
        display: inline-block;
        margin: 0px 2px;
        opacity: 0;
        transform: translateY(20px);
        transition: 0.3s;
    }

    .audio-item-column .audio-actions-container ul li:nth-child(1){
        transition-delay: 0.0s;
    }
    .audio-item-column .audio-actions-container ul li:nth-child(2){
        transition-delay: 0.1s;
    }
    .audio-item-column .audio-actions-container ul li:nth-child(3){
        transition-delay: 0.2s;
    }
    .audio-item-column .audio-actions-container ul li:nth-child(4){
        transition-delay: 0.3s;
    }

    .audio-item-column .audio-actions-container ul li a{
        font-size: 16px;
        padding: 4px;
        color: #707070;
        transition: .3s;
    }

    .audio-item-column .audio-actions-container ul li a:hover{
        color:<?= $colorTheme ?>;
    }
</style>
<!-- <========= audio item column styles -->

<!-- audio item row styles ==========> -->
<style type="text/css">
    .audio-item-row{
        width: 100%;
        height: 250px;
        border: 1px solid rgba(0, 0, 0, .15);
    }

    .audio-item-row:hover > .header .options{
        visibility: visible;
        opacity: 1;
    }
    .audio-item-row:hover > .header .options li{
        transform: translateY(0);
        opacity: 1;
    }

    .audio-item-row .header{
        height: 120px;
        background-color: <?= $colorTheme ?>;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
    }

    .audio-item-row .header img{
        height: 70px;
    }

    .audio-item-row .header .options{
        position: absolute;
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: rgba(0, 0, 0, .8);
        visibility: hidden;
        opacity: 0;
        transition: .3s;
    }

    .audio-item-row .header .options li{
        list-style: none;
        display: inline-block;
        padding: 0px 8px;
        opacity: 0;
        transform: translateY(20px);
        transition: 0.3s;
    }

    .audio-item-row .header .options li:nth-child(1){
        transition-delay: 0.0s;
    }
    .audio-item-row .header .options li:nth-child(2){
        transition-delay: 0.1s;
    }
    .audio-item-row .header .options li:nth-child(3){
        transition-delay: 0.2s;
    }
    .audio-item-row .header .options li:nth-child(4){
        transition-delay: 0.3s;
    }

    .audio-item-row .header .options li a{
        color: white;
        font-size: 16px;
    }

    .audio-item-row .content{
        height: 100px;
        padding: 10px 10px 0px 10px;
    }

    .audio-item-row .content .title{
        font-weight: bold;
    }

    .audio-item-row .content .duration{
        font-size: 11px;
        margin-top: 5px;
        color: #707070;
    }

    .audio-item-row .footer{
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 0px 10px;
    }

    .audio-item-row .footer p{
        font-size: 12px;
        color:#707070;
    }

    .audio-item-row .footer p:first-child i{
        color: red;
    }
</style>
<!-- <========== audio item row styles -->

<!-- player styles ==========> -->
<style type="text/css">
    .player-container{
        width: 100%;
        height: 70px;
        position: fixed;
        background-color: <?= $colorTheme ?>;
        padding: 0px 150px;
        z-index: 99999;
        color: white;
        left:0;
        bottom:  -70px;
        visibility: hidden;
        opacity: 0;
        transition: .8s;
    }

    .player-content{
        position: relative;
        display: flex;
        justify-content: space-between;
        align-items: flex-end;
    }

    .player-content .logo{
        width: 70px;
        height: 70px;
        border-radius: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: <?= $colorTheme ?>;
        border: 5px solid white;
        transform: translateY(-35px);
    }

    .player-content .logo img{
        width: 40px;
    }

    .player-content .left-content{
        display: flex;
        width: 100%;
    }

    .player-content .left-content .player{
        width: 60%;
        padding: 10px 20px;
    }

    .player-content .left-content .player .status{
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .player-content .left-content .player .status p:last-child{
        font-size:14px;
    }

    .player-content .left-content .player .status .title{
        font-size: 1.2em;
        font-weight: bold;
    }

    .player-content .left-content .player .status .state p{
        font-size: 12px;
    }

    .player-content .left-content .player .controls{
        margin-top: 5px;
        display: flex;
    }

    .player-content .left-content .player .controls ul{
        white-space: nowrap;
        padding-left: 10px;
    }

    .player-content .left-content .player .controls ul li{
        list-style: none;
        display: inline-block;
    }

    .player-content .left-content .player .controls ul li a{
        color: white;
    }

    .player-content .right-content{
        display: flex;
        align-items: center;
        padding-bottom: 10px;
    }

    .player-content .right-content a{
        color: white;
        font-size: 18px;
        padding-left: 5px;
    }
</style>
<!-- <========== player styles -->