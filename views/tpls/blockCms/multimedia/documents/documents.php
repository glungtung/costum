<style>
    .file-icon{
        width: 55px;
        height: 65px;
        position:relative;
        border-radius: 4px;
        background-color: #f5362d;
    }
    .file-icon::before{
        display: block;
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        width: 0;
        height: 0;
        border-bottom-left-radius: 2px;
        border-width: 10px;
        border-style: solid;
        border-color: #fff #fff rgba(255,255,255,.35) rgba(255,255,255,.35);
    }
    .file-icon::after{
        content: attr(data-type);
        display:block;
        position: absolute;
        bottom:4px;
        left:4px;
        color: white;
        font-weight: bold;
    }

    .document-item{
        display: flex;
        align-items: center;
        border-bottom: 1px solid #eaeaea;
        padding: 10px 0px; 
    }
    .document-item:hover > .file-actions ul li{
        transform: translateY(0);
        opacity:1;
        visibility:visible;
    }

    .file-title{
        padding: 0px 10px;
        font-size: 16px;
        width: 100%;
    }

    .file-actions ul{
        white-space: nowrap;
    }

    .file-actions ul li{
        list-style:none;
        display: inline-block;
        margin: 0px 5px;
        transform: translateY(20px);
        transition: .3s;
        opacity:0;
        visibility:hidden;
    }
    .file-actions ul li:first-child{
        transition-delay: 0.1s;
    }
    .file-actions ul li:last-child{
        transition-delay: 0.2s;
    }

    .file-actions ul li a{
        display:flex;
        align-items: center;
        justify-content: center;
        width:45px;
        height: 45px;
        border-radius: 100%;
        background-color: #eaeaea;
        color:#707070;
        text-decoration: none;
        transition: .3s;
    }
    .file-actions ul li a:hover{
        background-color: #f5362d;
        color: white;
    }

    .document-header h1{
        margin-bottom: 30px;
        font-size: 2em;
    }

</style>

<?php
    $documents = PHDB::find("documents", [ "id"=>$costum["contextId"], "doctype"=>"file" ]);
?>

<div class="<?= $kunik ?>" style="padding: 40px 0px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="document-header text-center">
                <h1>Mes documents</h1>
            </div>
            <div class="row">
                <?php foreach($documents as $doc) { 
                    $file_ext = array_reverse(explode(".", $doc['name']))[0];    
                ?>
                <div class="col-md-12">
                    <div class="document-item">
                        <div class="file-icon" data-type="<?= $file_ext ?>"></div>
                        <div class="file-title"><?= $doc["name"] ?></div>
                        <div class="file-actions">
                            <ul>
                                <li><a href="#"><i class="glyphicon glyphicon-save"></i></a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-eye-open"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>