<?php 
$defaultColor = "#354C57"; 
$structField = "structags";

$keyTpl = "wizard";

$paramsData = [ "title" => "",
                "color" => "",
                "background" => "",
                "nbList" => 2,
                "defaultcolor" => "#354C57",
                "tags" => "structags"
                ];

if( isset($this->costum["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset($this->costum["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  $this->costum["tpls"][$keyTpl][$i];      
    }
}

?>
<div id="wizard" class="swMain">

    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;
            background-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;      
        }

        .icon-btn {
            padding: 1px 15px 3px 2px;
            border-radius: 50px;
            font-weight: 700;
            margin: 5px;
        }

        .btn-glyphicon {
            padding: 8px;
            background: #ffffff;
            margin-right: 4px;
            margin-top: 1px;
        }

    </style>

    <ul id="wizardLinks">
        <?php
        // var_dump($listSteps); exit;
        foreach ($listSteps as $k => $v) {
            $n = "todo";
            $p = null;
            $color = "";
            
            if( count(Poi::getPoiByStruct($cmsList, "step".$v,$structField ) ) != 0 ) { 
                $p = Poi::getPoiByStruct($cmsList, "step".$v,$structField )[0];
                $n =  $p["name"];
                //var_dump($p);
                $color = (isset($p["color"])) ? $p["color"] :'';
            }
            echo "<li>";

                $d = ( isset($p) ) ? 'class="done"' : '';
                $l = 'showStep(\'#'.$v.'\')' ;

                $lbl = ( isset($p) ) ? $k : "?";
                
                echo '<a onclick="'.$l.'" href="javascript:;" '.$d.' >';
                echo '<div class="stepNumber">'.$lbl.'</div>';
                echo '<span class="stepDesc" style="color: '.$color.'">'.$n.'</span></a>';
            echo "</li>";    
        }
        ?>

    </ul>
    

    <?php  
    foreach ($listSteps as $k => $v) {
        $hide = ($k==0) ? "" : "hide";
    ?>
    <div id='<?php echo $v ?>' class='col-sm-offset-1 col-sm-10 sectionStep <?php echo $hide ?>' style="padding-bottom:40px">
        <?php 

        if( count(Poi::getPoiByStruct($cmsList,"step".$v,$structField))!=0 )
        {   
            $p = Poi::getPoiByStruct($cmsList,"step".$v,$structField)[0];
            echo '<h1  style="color:'.@$p["color"].'">'.@$p["name"].'</h1>';
            echo "<div class='markdown'>".@$p["description"]."</div>";
            
            if( isset($p["documents"]) ){
              echo "<br/><h4>Documents</h4>";
              //var_dump($p["documents"]);
              foreach ($p["documents"] as $key => $doc) {
                $dicon = "fa-file";
                $fileType = explode(".", $doc["name"])[1]; 
                
                if( $fileType == "png" || $fileType == "jpg" || $fileType == "jpeg" || $fileType == "gif" )
                  $dicon = "fa-file-image-o";
                else if( $fileType == "pdf" )
                  $dicon = "fa-file-pdf-o";
                else if( $fileType == "xls" || $fileType == "xlsx" || $fileType == "csv" )
                  $dicon = "fa-file-excel-o";
                else if( $fileType == "doc" || $fileType == "docx" || $fileType == "odt" || $fileType == "ods" || $fileType == "odp" )
                  $dicon = "fa-file-text-o";
                else if( $fileType == "ppt" || $fileType == "pptx" )
                  $dicon = "fa-file-text-o";
                else 
                    $dicon = "fa-file";
                echo "<a href='".$doc["path"]."' target='_blanck'><i class='text-red fa ".$dicon."'></i> ".$doc["name"]."</a><br/>";
              }
            }


            $edit ="update";

            if( $canEdit ){
            ?>
            <a href='javascript:;' data-id='<?php echo $p["_id"] ?>' data-type='cms' class='editThisBtn btn btn-xs btn-primary icon-btn margin-left-10' > <span class='btn-glyphicon fa fa-pencil img-circle text-primary' aria-hidden='true'></span> Modifier </a>
            <!-- <a href='javascript:;' data-id='"+data.map["_id"]["$id"]+"' data-collection='cms' class='addBlockCms  btn btn-xs icon-btn btn-success margin-left-10' > <span class='btn-glyphicon fa fa-plus-circle img-circle text-success' aria-hidden='true'></span> Ajouter une section </a>  -->
            <?php
            }
        } 
        else 
        { ?>
            TEXT TODO <br/>
            as CMS type cms + tag : <span class="badge">step<?php echo $v ?></span><br/>

           
        <?php  
            $edit ="create";
        } ?>
        

        
        
    </div>
    <?php  
    }  
 ?>

    <script type="text/javascript">
jQuery(document).ready(function() {
     $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
       $(v).html(descHtml);
    });
    mylog.log("render","modules/costum/views/tpls/blockCms/wizard.php");
    
    $(".addBlockCms").off().on("click",function (){
        var optionsJs = <?= json_encode(array(
                //"tpls.blockCms.textImg.blockText" => "Nouveau Paragraphe",
                "tpls.blockCms.textImg.cte_blockwithimg" => "Un bloc avec un texte et une image",
                "tpls.blockCms.text.cte_bloctextwithtitle" => "Un bloc avec un Titre et description"
            )); ?>;
            //var optionsJs = <?//= json_encode(Cms::$option); ?>;
            var page = <?= json_encode(@$page); ?>;
            var tplCtx = { value : { tpls : {} } };


            var addBlockCms = {
                "jsonSchema" : {
                    "title" : "Ajouter une section",
                    "type" : "object",
                    onLoads : {
                        //pour creer un subevnt depuis un event existant
                        onload : function(){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : {
                        type : { 
                            label : "Choisir le template",
                            inputType : "select",
                            options : optionsJs,
                            value : "text" 
                        },
                        page : {
                            inputType : "hidden",
                            value : page
                        }
                    }
                }
            }; 

            tplCtx.id = $(this).data("id");
            tplCtx.key = $(this).data("key");
            tplCtx.collection = $(this).data("collection");
            count = 0;

            addBlockCms.jsonSchema.save = function () {
                tpl = $("#type").val().split(".");
                var keyTpl = tpl[tpl.length - 1];
                var i =0;

                $.each(allCms["parentTree"] , function(key,val){
                    if (typeof allCms["orphans"][key] != "undefined" && typeof allCms["orphans"][key]["tpls"] != "undefined") {
                        $.each(allCms["orphans"][key]["tpls"] , function(k,e){
                            mylog.log("save allcms each",e.id,$("#type").val()+"."+i);
                            if (e.id == $("#type").val()+"."+i) {
                                mylog.log("save allcms is equal : ",e.id,$("#type").val()+"."+i);
                                i++;
                            }
                        })
                    }
                });

                tplCtx.path = "tpls."+keyTpl+i;

                mylog.log("addBlockCms keyTpl: ",tpl,keyTpl);

                tplCtx.value = {
                    id : $("#type").val()+"."+i,
                    type : $("#type").val()
                };

                tplCtx.value.page = $("#page").val();

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    mylog.log("addBlockCms save tplCtx",tplCtx);
                    dataHelper.path2Value( tplCtx, function(params) {
                        urlCtrl.loadByHash(location.hash);
                        toastr.success("Élément ajouté");
                        $("#ajax-modal").modal('hide');
                    } );
                }
                // urlCtrl.loadByHash(location.hash);
            }
            dyFObj.openForm( addBlockCms );
        });
});
        
        //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    function showStep(id){
        $(".sectionStep").addClass("hide");
        $(id).removeClass("hide");    
    }

    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumStepCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'cms'},null,dynFormCostumStepCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });

    });
    
    $(".addBlockCms").off().on("click",function (){
            var optionsJs = <?= json_encode(array(
                //"tpls.blockCms.textImg.blockText" => "Nouveau Paragraphe",
                "tpls.blockCms.textImg.cte_blockwithimg" => "Un bloc avec un texte et une image",
                "tpls.blockCms.text.cte_bloctextwithtitle" => "Un bloc avec un Titre et description"
            )); ?>;
            //var optionsJs = <?= json_encode(Cms::$option); ?>;
            var page = <?= json_encode(@$page); ?>;
            var tplCtx = { value : { tpls : {} } };

            var addBlockCms = {
                "jsonSchema" : {
                    "title" : "Ajouter une section",
                    "type" : "object",
                    onLoads : {
                        //pour creer un subevnt depuis un event existant
                        onload : function(){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : {
                        type : { 
                            label : "Choisir le template",
                            inputType : "select",
                            options : optionsJs,
                            value : "text" 
                        },
                        page : {
                            inputType : "hidden",
                            value : page
                        }
                    }
                }
            }; 

            // tplCtx.id = $(this).data("id");
            // tplCtx.key = $(this).data("key");
            // tplCtx.collection = $(this).data("collection");
            // count = 0;

            addBlockCms.jsonSchema.save = function () {
                tplCtx.collection = "cms";
                tplCtx.path = "allToRoot";
                tplCtx.value = {
                    "path" : $("#type").val(),
                    "page" : $("#page").val(),
                    "haveTpl" : "false"
                };
                tplCtx.value.parent = {};
                tplCtx.value.parent[contextData.id] = {
                    "type" : costum.contextType,
                    "name" : costum.contextSlug
                };


                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    mylog.log("addBlockCms save tplCtx",tplCtx);
                    dataHelper.path2Value( tplCtx, function(params) {
                        location.reload();
                        toastr.success("Section ajouté");
                        $("#ajax-modal").modal('hide');
                    } );
                }
                // urlCtrl.loadByHash(location.hash);
            }
            dyFObj.openForm(addBlockCms);
        });

    var dynFormCostumStepCMS = {
    "beforeBuild":{
        "removeProp" : {
            "image": 1,
        },
        "properties" : {
            "structags" : {
                "inputType" : "tags",
                "placeholder" : "Structurer le contenu",
                "values" : null,
                "label" : "Structure et Hierarchie (parent ou parent.enfant)"
            },
            "documentation" : {
                "inputType" : "uploader",
                "label" : "Document associé (5Mb max)",
                "showUploadBtn" : true,
                "docType" : "file",
                "itemLimit" : 5,
                "contentKey" : "file",
                "domElement" : "documentationFile",
                "placeholder" : "Le pdf",
                "afterUploadComplete" : null,
                "template" : "qq-template-manual-trigger",
                "filetypes" : [
                    "pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif","eps"
                ]
            },
            "color" : {
                "inputType" : "colorpicker",
                "label" : "couleur du menu",
                "order" : 3
            },
            "path" : {
                "inputType" : "hidden",
                "value" : "tpls.blockCms.text.cte_desc"
            }
        }
    },
    "onload" : {
        "actions" : {
            "setTitle" : "Documentation",
            "html" : {
                "nametext>label" : "Titre de la documentation",
                "infocustom" : "<br/>Créer des sections et des sous chapitres"
            },
            "presetValue" : {
                "type" : "doc"
            },
            "hide" : {
                "page":1,
                "structagstags":1,
                "locationlocation" : 1,
                "formLocalityformLocality" : 1,
                "breadcrumbcustom" : 1,
                "urlsarray" : 1,
                "imguploader" : 1
            }
        }
    }
};
dynFormCostumStepCMS.afterSave = function(data){
    //mylog.log("afterSave cms", data, typeof callB);
    dyFObj.commonAfterSave(data,function(){
        mylog.log("data", data);
         location.hash = "#documentation";
         urlCtrl.loadByHash(location.hash);
         var checkExistId = function (time){
            if ($('#startSection').length) {
                $('#startSection').hide();
                $('#acteurKick').removeClass("hide");
            }
            else
              setTimeout(function(){
                checkExistId(time+200)
              }, time);
        }

        checkExistId(0);

    });
}
    </script>


</div>