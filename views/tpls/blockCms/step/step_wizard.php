<?php
$keyTpl ="step_wizard";
$paramsData = [
    "title" => "Explication des etape d'un costume ",
    "subtitle"=> " « Contribuer aux communs avec un contexte personnalisé. » ",
    "stepNumber" => 4,
    "imgShow" => "yes"
];


if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$latestLogo = [];

  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
            "id"=> $blockKey,
            "type"=>'cms',
            "subKey"=>"logo"
        ),"image"
    );
    foreach ($initImage as $key => $value) {
        $latestLogo[]= $value["imagePath"];
    }


$poiList = array();
if(isset($costum["contextType"]) && isset($costum["contextId"])){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION,
        array( "source.key" => $costum["slug"],
            "type"=>"cms") );

}
?>
<style>
    .explication{
        margin-top:3%;
        margin-left: 10%;
        margin-right: 10%;
    }
    .explication-title{
        text-align : center;
        border-top: dashed 1.5px #92bf34;
        border-bottom:dashed 1.5px #92bf34;
        color : #92bf34
    }

    .explication-img {
        text-align: center;
    }

    @media (min-width: 768px) {
        .explication-img img {
            width: 70%;
        }
    }

    @media (max-width: 767px) {
        .explication{
            margin-left: 0%;
            margin-right:0%;
        }
        .explication-title > h1{
            font-size : 3rem;
        }
        .explication-img > h1 {
            font-size : 2.5rem;
        }
    }
</style>

<div class="explication row">
    <div class="explication-title col-xs-12 col-sm-12">
        <h1 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h1>
    </div>
    <div class="col-xs-12 margin-top-20 poi-m">
        <?php
        $number = array();
        for ($i=1; $i<= $paramsData["stepNumber"]; $i++) {
            $number[] = ''.$i;
        }
        $params = array(
            "poiList"=>$poiList,
            "listSteps" => $number,
            "el" => $el,
            "color1" => "#92bf34",
            "costum" => $costum
        );
        echo $this->renderPartial("survey.views.tpls.wizard",$params); ?>
    </div>
    <div class="explication-img col-xs-12 col-sm-12 no-padding">
        <?php if ($paramsData["imgShow"] == "yes") { ?>
            <center>
                <img src="<?php echo !empty($latestLogo) ? $latestLogo[0] : Yii::app()->getModule("costum")->assetsUrl.'/images/costumDesCostums/illu.svg' ?>">
            </center>
        <?php } ?>
        <br>
        <h2 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitle"><?= $paramsData["subtitle"]?></h2>
        <br>
        <br>
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready( function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "stepNumber" : {
                        "label" : "<?php echo Yii::t('cms', 'Number of steps')?>",
                        "rules" : {
                            "required": true,
                            "number" : true
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.stepNumber
                    },
                    "imgShow" : {
                        "label" : "<?php echo Yii::t('cms', 'Show image')?>",
                        inputType : "select",
                        options : {
                            "yes" : "<?php echo Yii::t('common', 'Yes')?> ",
                            "no" : "<?php echo Yii::t('common', 'No')?> ",
                        },
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.imgShow
                    },
                    "logo" : {
                        "inputType" : "uploader",
                        "label" : "logo",
                        "showUploadBtn" : false,
                        "docType" : "image",
                        "itemLimit" : 1,
                        "contentKey" : "slider",
                        "order" : 9,
                        "domElement" : "logo",
                        "placeholder" : "image logo",
                        "afterUploadComplete" : null,
                        "endPoint" : "/subKey/logo",
                        "filetypes" : [
                            "png","jpg","jpeg","gif"
                        ],
                        initList : <?php echo json_encode($initImage) ?>
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent")
                            tplCtx.value[k] = formData.parent;

                        if(k == "items")
                            tplCtx.value[k] = data.items;
                        mylog.log("andrana",data.items)
                    });
                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');

                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

    });
</script>

