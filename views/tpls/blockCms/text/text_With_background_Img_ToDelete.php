<?php 
$keyTpl = "text_With_background_Img";
$paramsData = [
	"title" => "Lorem Ipsum",
	"sizeTitle"=>"35",	
	"colorTitle"=>"#000000",	
	"sizeDesc"=>"20",
	"content"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"colorDesc"=>"#000",
	"colorContent" => "#fcc845"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	} 
}  
?>
<style type="text/css">
	.well_<?= $kunik ?> h2 {
		font-size: <?= $paramsData["sizeTitle"]?>px;
		color: <?= $paramsData["colorTitle"]?>;
	}
	.well_<?= $kunik ?> .markdown p{		
		font-size: <?= $paramsData["sizeDesc"]?>px ;
		color: <?= $paramsData["colorDesc"]?>;
	}
	.bgImg_<?=$kunik?> {
		position: relative;
	    padding: 0em 0em 0em 0em;
	}
	.bgImg_<?= $kunik?> {
		width: 100%;
	    height: 270px!important;
	    /*margin-bottom: 130px!important;*/  <?php // USE BLOCKCMS MARGIN IN DYNFORM PLEASE by jean ?>
	}

	.well_<?= $kunik ?> {
	    background-color: <?= $paramsData["colorContent"]?>;
	    text-align: center;
	    text-transform: none;
	    /*border-bottom: 6px solid <?//= $paramsData["colorTitle"]?>;*/
	    box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
	    border-radius: 25px;
	}

	@media (min-width: 768px){
		.well_<?= $kunik ?> {
		    top: 100px;
		}
	}

	@media (max-width: 767px) {
		.well_<?= $kunik ?> {
		    margin-left: 5%;
		    margin-right: 5%;
		    width: 90%;
		    top: 90px;
		}
		.bgImg_<?= $kunik?> {
		    height: 170px;
		    margin-bottom: 150px;
		}
	}
	
	@media (max-width: 414px) {
		.well_<?= $kunik ?> h2 {
			font-size:20px;
			margin-bottom: 0px;
		}
		.well_<?= $kunik ?> .markdown p{
			font-size: 18px !important;
			margin-bottom: 20px;
			padding: 0;
		}
	}
	@media screen and (min-width: 1500px){
		.well_<?=$kunik?> h2{
		    font-size: <?= $paramsData["sizeTitle"]?>px;
		    text-transform: none;
		}
		.well_<?=$kunik?> .markdown p{
		    font-size: 25px;
		    text-transform: none;
	  	}
	}
</style>
<div class=" bgImg_<?= $kunik?>">
	<div class="well_<?= $kunik?> well col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2"> 
		<div class="text-center">
			<h2 class="title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h2>
			<div class="description sp-text" data-id="<?= $blockKey ?>"  data-field="content"><?= $paramsData["content"]?></div> 
		</div>
	</div>
	
</div>

<!-- <div id="about-cmcas" class="well col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
	<h1 style="color:black">Qui sommes nous ?</h1>
	<div class="markdown">
		Organisation sociale bénéficiant d'une autonomie de gestion, la Caisse Mutuelle Complémentaire et d'activités sociales est chargée de dispenser et de développer les activités sociales et l'action sanitaire et sociale auprès du personnel des industries électrique et gazière et leurs ayants-droit. 
	</div>
</div> -->

<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {	
					"colorContent":{
						label : "<?php echo Yii::t('cms', 'Content background color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>