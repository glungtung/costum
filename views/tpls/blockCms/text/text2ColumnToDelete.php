<?php 
$keyTpl = "text2Column";
$paramsData=[
	"title"=>"Lorem Ipsum",
	"oneSousTitle" => "Connecté",
	"oneContent"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"twoSousTitle" => "Autonomie",
	"twoContent"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry",	
	"threeSousTitle" => "Résilience",
	"threeContent"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"fourSousTitle" => "Dans la réel",
	"fourContent"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry",	
	"oneLogo"=>"",
	"twoLogo"=>"",
	"threeLogo"=>"",
	"fourLogo"=>"" 
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<?php 

$image1 = [];
$initImage1 = [];
$image2 = [];
$initImage2 = [];
$image3 = [];
$initImage3 = [];
$image4 = [];
$initImage4 = [];

$initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
    ),"image"
);

foreach ($initImage as $key => $value) {
	if ($value["subKey"] == "oneLogo") {
		$initImage1[] = $value; 
		$image1[]= $value["imageThumbPath"];
	}
	if ($value["subKey"] == "twoLogo") {
		$initImage2[] = $value; 
		 $image2[]= $value["imageThumbPath"];
	}
	if ($value["subKey"] == "threeLogo") {
		$initImage3[] = $value; 
		 $image3[]= $value["imageThumbPath"];
	}
	if ($value["subKey"] == "fourLogo") {
		$initImage4[] = $value; 
		 $image4[]= $value["imageThumbPath"];
	}
}
?>
<style type="text/css">
	
	#fh5co-services_<?=$kunik?> {
		overflow: hidden;
		position: relative;
		padding-left: 10%;
		padding-right: 10%;
	}
	#fh5co-services_<?=$kunik?> .fh5co-service {
		padding-right: 30px;
	}
	
	#fh5co-services_<?=$kunik?> .icon {
		font-size: 70px;
		margin-top: 50px;
		display: -moz-inline-stack;
		display: inline-block;
		zoom: 1;
		*display: inline;
	}
	#fh5co-services_<?=$kunik?> .icon img{
		width: 100px;
		height: 100px;
	}
	
	.section-heading_<?=$kunik?> {
		float: left;
		width: 100%;
		clear: both;
	}

	.content<?= $kunik?> {
		padding-top: 3%;
	}
	@media screen and (max-width: 414px) {
		#fh5co-services_<?=$kunik?> .section-heading_<?=$kunik?> .content<?= $kunik?> h3 {
			font-size: 20px !important;
			line-height: 34px;
			margin-top: -15px;
		}
		
		#fh5co-services_<?=$kunik?> .section-heading_<?=$kunik?> .content<?= $kunik?>  p {
			font-size: 15px;
			line-height: 15px;
			text-align: justify;
		}
	}
	 @media (max-width: 768px) {
	 	#fh5co-services_<?=$kunik?>   .icon img {
		    width: 70px;
		    height: 70px;
		}
	 }
	

</style>
<div id="fh5co-services_<?=$kunik?>" data-section="services">
	<div class="">
		<div class="row">
			<div class="col-md-12 section-heading_<?=$kunik?> ">
				<h1 class=" fadeInUp animated  sp-text" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h1>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 fh5co-service fadeInUp animated">
						<i class="col-md-3 col-xs-3 icon fadeInUp animated-2 icon-anchor">
								<img class="sp-image" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="image" data-src="<?= isset($image1[0]) ? $image1[0] : ''?>" src="<?= isset($image1[0]) ? $image1[0] :  Yii::app()->getModule('costum')->assetsUrl.'/images/cocity/icone/molecular.png'?>">
						</i>
						<div class="col-md-9 col-xs-9 content<?= $kunik?>">
							<h3 class="sp-text" data-id="<?= $blockKey ?>" data-field="oneSousTitle"><?= $paramsData["oneSousTitle"]?></h3>
							<div class="sp-text" data-id="<?= $blockKey ?>" data-field="oneContent"><?= $paramsData["oneContent"]?></div>
						</div>
						
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 fh5co-service fadeInUp animated">
						<i class="col-md-3 col-xs-3 icon fadeInUp animated-2 icon-layers2">
							<img src="<?= isset($image2[0]) ? $image2[0] :  Yii::app()->getModule('costum')->assetsUrl.'/images/cocity/icone/lighting.png'?>">
							
						</i>
						<div class="col-md-9  col-xs-9 content<?= $kunik?>">
							<h3 class="sp-text" data-id="<?= $blockKey ?>" data-field="twoSousTitle"><?= $paramsData["twoSousTitle"]?></h3>
							<div class="sp-text" data-id="<?= $blockKey ?>" data-field="twoContent"><?= $paramsData["twoContent"]?></div>
						</div>
						
					</div>
					<div class="clearfix visible-sm-block"></div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 fh5co-service fadeInUp animated">
						<i class="col-md-3 col-xs-3 icon fadeInUp animated-2 icon-anchor">
								<img src="<?= isset($image3[0]) ? $image3[0] :  Yii::app()->getModule('costum')->assetsUrl.'/images/cocity/icone/fist.png'?>">
						</i>
						<div class="col-md-9 col-xs-9 content<?= $kunik?>">
							<h3 class="sp-text" data-id="<?= $blockKey ?>" data-field="threeSousTitle"><?= $paramsData["threeSousTitle"]?></h3>
							<div class="sp-text" data-id="<?= $blockKey ?>" data-field="threeContent"><?= $paramsData["threeContent"]?></div>
						</div>
						
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 fh5co-service fadeInUp animated">
						<i class="col-md-3 col-xs-3 icon fadeInUp animated-2 icon-layers2">
							<img src="<?= isset($image4[0]) ? $image4[0] :  Yii::app()->getModule('costum')->assetsUrl.'/images/cocity/icone/real-state.png'?>">
							
						</i>
						<div class="col-md-9 col-xs-9 content<?= $kunik?>">
							<h3 class="sp-text" data-id="<?= $blockKey ?>" data-field="fourSousTitle"><?= $paramsData["fourSousTitle"]?></h3>
							<div class="sp-text" data-id="<?= $blockKey ?>" data-field="fourContent"><?= $paramsData["fourContent"]?></div>
						</div>
						
					</div>
					<div class="clearfix visible-sm-block"></div>
				</div>
			</div>

		</div>
		
	</div>
</div>
<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {
          		"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		      	"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					
					"oneLogo" : {
						"inputType" : "uploader",
						"showUploadBtn" : false,
						"docType" : "image",
						"label":"<?php echo Yii::t('cms', 'Left image')?>",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "oneLogo",
						"placeholder" : "image logo",
						"endPoint" : "/subKey/oneLogo",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
						initList : <?php echo json_encode($initImage1) ?>
					},
					"twoLogo" : {
						"inputType" : "uploader",
						"showUploadBtn" : false,
						"docType" : "image",
						"label":"<?php echo Yii::t('cms', 'Left image')?>",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "twoLogo",
						"placeholder" : "image logo",
						"endPoint" : "/subKey/twoLogo",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
						initList : <?php echo json_encode($initImage2) ?>
					},
					"threeLogo" : {
						"inputType" : "uploader",
						"showUploadBtn" : false,
						"label":"<?php echo Yii::t('cms', 'Left image')?>",
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "threeLogo",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						"endPoint" : "/subKey/threeLogo",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
						initList : <?php echo json_encode($initImage3) ?>
					},
					"fourLogo" : {
						"inputType" : "uploader",
						"label":"<?php echo Yii::t('cms', 'Left image')?>",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "fourLogo",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						"endPoint" : "/subKey/fourLogo",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
						initList : <?php echo json_encode($initImage4) ?>
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');4
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"one",12,12,0,null,"<?php echo Yii::t('cms', 'First content')?>","green","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"two",12,12,0,null,"<?php echo Yii::t('cms', 'Second content')?>","green","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"three",12,12,0,null,"<?php echo Yii::t('cms', 'Third content')?>","green","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"four",12,12,0,null,"<?php echo Yii::t('cms', 'Fourth content')?>","green","");
		});
	});
</script>