<?php 
  $keyTpl ="kaf_biographie";
  $paramsData = [ 
    "titleBlock" => "Lorem ipsum",
    "subtitleBlock" => "Sous titre",
    "descriptionBlock" => "C'est aujourd’hui qu’on se rencontre et c’est maintenant qu’on agit !"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>

<style>
  subtitle<?php echo $kunik ?> {
  	font-weight: 400;
  }
  .item<?php echo $kunik ?> {
    text-transform: none !important;
  }
  .container<?php echo $kunik ?> {
    padding-top: 30px;
    text-align: center;
  }
  .container<?php echo $kunik ?> morelink<?php echo $kunik ?>{
    text-align: center;
  }
 
  .morecontent<?php echo $kunik ?> {
	display: none;
  }
  .morelink<?php echo $kunik ?> {
    font-size: 16px;
    font-weight: bold;
    margin-left: 2px; 
    color: #ff9800;
    cursor: pointer;
  }

</style>

<div class="container<?php echo $kunik ?> col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
  
  <h2 class="title-1 title<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock">
    <?php echo $paramsData["titleBlock"] ?>
  </h2>
  <h2 class="title-2 subtitle<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitleBlock">
    <?php echo $paramsData["subtitleBlock"] ?>
  </h2>
  <div class=" title-3 more<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="descriptionBlock"><?php echo $paramsData["descriptionBlock"] ?></div>
</div>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
    function readMoreLess() {
	    var showChar = 400;
	    var ellipsestext = "...";
	    var moretext = "Lire la suite";
	    var lesstext = "Lire moins";
	    $('.more<?php echo $kunik ?>').each(function() {
	      var content = $(this).html();
	      var textcontent = $(this).text();

	      if (textcontent.length > showChar) {

	        var c = textcontent.substr(0, showChar);
	        //var h = content.substr(showChar-1, content.length - showChar);

	        var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent<?php echo $kunik ?>">' + content + '</span>';

	        $(this).html(html);
	        $(this).after('<a href="" class="morelink<?php echo $kunik ?> btn btn-default">' + moretext + '</a>');
	      }

	    });

	    $(".morelink<?php echo $kunik ?>").click(function() {
	      if ($(this).hasClass("less")) {
	        $(this).removeClass("less");
	        $(this).html(moretext);
	        $(this).prev().children('.morecontent<?php echo $kunik ?>').fadeToggle(500, function(){
	          $(this).prev().fadeToggle(500);
	        });
	       
	      } else {
	        $(this).addClass("less");
	        $(this).html(lesstext);
	        $(this).prev().children('.container').fadeToggle(500, function(){
	          $(this).next().fadeToggle(500);
	        });
	      }
	      
	      return false;
	    });
	}
	setTimeout(function(){ 
		readMoreLess();
	}, 1500);
</script>