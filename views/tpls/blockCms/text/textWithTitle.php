<?php
    $blockKey = $blockCms["_id"];
  $keyTpl ="textWithTitle";
    $kunik = $keyTpl.(string)$blockCms["_id"];
  $paramsData = [
    "title" => "Lorem ipsum",
    "subTitle" =>" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  ];

   if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>

<style>
  .container<?php echo $kunik ?>{
    min-height: 100px !important;
  }
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    font-size:23px;
  }
  @media (max-width: 414px) {

    .container<?php echo $kunik ?> h2{
      font-size: 18px !important;
    }
    .container<?php echo $kunik ?> h3{
      font-size: 15px !important;
    }
    .subTitle<?php echo $kunik ?>{
      margin-bottom: 10px;
    }
    .container<?php echo $kunik ?> p{
      font-size: 14px !important;
    }
  }
</style>

<div class="col-xs-12 container<?php echo $kunik ?>">
  <h2 class=" title title<?php echo $kunik ?> Oswald sp-text" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h2>
  <?php if($paramsData["subTitle"]!=""){ ?>
  <!--h3 class=" subTitle<?php echo $kunik ?> ReenieBeanie sp-text" data-id="<?= $blockKey ?>" data-field="subTitle">
    <?php echo $paramsData["subTitle"] ?>
  </h3-->
  <?php } ?>
  <?php if($paramsData["subTitle"]!=""){ ?>
  <!--p class="item<?php echo $kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="description"><?php echo $paramsData["description"]?></p-->
<?php } ?>
</div>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            "properties" : {
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
