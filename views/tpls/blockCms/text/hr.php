<?php 
$keyTpl = "hr";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$paramsData = [
    "hrColor" => "#6CC3AC",
    "hrBorder" => "dashed",
    "hrWidth" => "80" ,  
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
  .<?= $kunik?> {
    width:<?= $paramsData["hrWidth"]?>%; 
    margin:15px auto;
    border: 1px <?= $paramsData["hrBorder"]?> <?= $paramsData["hrColor"]?>;
  }
</style>
<hr class="<?= $kunik?>" >
<script type="text/javascript">
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		    "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
          "hrColor" : {
            "inputType" : "colorpicker",
            label : "<?php echo Yii::t('cms', 'Color')?>",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.hrColor
          },
          "hrBorder" : {
            label: "<?php echo Yii::t('cms', 'Type')?>",
            inputType: "select",
            options: {
              "dashed": "dashed",
              "dotted": "dotted",
              "double": "double",
              "groove": "groove",
              "inset": "inset",
              "ridge": "ridge",
              "solid": "solid",
              "outset": "outset"
            },
            value: sectionDyf.<?php echo $kunik?>ParamsData.hrBorder
          },
          "hrWidth" : {
            inputType : "text",
            label : "<?php echo Yii::t('cms', 'Width')?>",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.hrWidth,
            rules: {
              number: true,
            }
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
      alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"hr",4,4,null,null,"<?php echo Yii::t('cms', 'Property of the dividing line')?>","green","");
    });
  });
</script>