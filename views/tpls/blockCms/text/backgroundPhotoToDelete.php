<?php 
$keyTpl = "backgroundPhoto";
$paramsData = [
	"title" => "Lorem Ipsum",
	"content"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"photo"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) { 
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}

?>
<style type="text/css">
	#<?= $kunik?>  .item {
	  background-size: cover;
	  height: 500px; 
	}

	#<?= $kunik?>   .item:after {
	  content: " ";
	  position: absolute;
	  top: 0;
	  bottom: 0;
	  left: 0;
	  right: 0;
	}

	#<?= $kunik?>   .active {
	  opacity: 1;
	  background: rgb(0,0,0,0.6);
	}

	#<?= $kunik?> #home-slider {
	  overflow: hidden;
	  position: relative;
	}

	#<?= $kunik?> #home-slider .caption {
	  position: absolute;
	  top: 20%;
	  margin-top: -104px;
	  left: 0;
	  right: 0;
	  text-align: center;
	  z-index: 15;
	  font-size: 18px;
	  font-weight: 300;
	  color: #fff;
	}
	
	.contenu_<?= $kunik ?> h2 {
		margin-top: 8% !important ;
	}
	.contenu_<?= $kunik ?> p {
		margin-top: 3%;
	}
	@media (max-width: 414px) {
		.contenu_<?= $kunik ?> h2 {
			font-size:20px;
			margin-bottom: 0px;
		}
		.contenu_<?= $kunik ?> p {
			font-size: 13px !important;
			margin-bottom: 20px;
			padding: 0;
		}
		#<?= $kunik?>   .item {
		  height: 640px; 
		}
		#<?= $kunik?> {
			margin-top: 0%;
		}

	}
	@media screen and (min-width: 1500px){
	  	.contenu_<?=$kunik?> h2{
		    margin: 0 0 10px 0;
		    font-size: 40px;
		    line-height: 35px;
		    text-transform: none;
		    margin-top: 8% ! important;
	 	}
	  	.contenu_<?=$kunik?> p{
		    line-height: 35px;
		    font-size: 25px;
		    text-transform: none;
		    margin-bottom: -90px;
	  	}
	}
</style>
<div id="<?= $kunik?>">
	<div id="home-slider" class="">
      	<div class="item active ">
        <div class="caption">
          	<div class="container contenu_<?= $kunik?>">
				<div class="text-center">
					
					<h2 class="title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h2>
					<br> 
					<div class="description sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["content"]?></div> 
				</div>
			</div>
        </div>
    </div>  
  </div>
</div>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {	
				},
				beforeBuild : function(){
		            uploadObj.set("cms","<?php echo $blockKey ?>");
		        },
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                      $("#ajax-modal").modal('hide');
						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	              	}

				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>
