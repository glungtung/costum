<?php 
$keyTpl = "contentMarkdown";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$paramsData = [
    "hrColor" => "#6CC3AC",
    "hrBorder" => "dashed",
    "allContent" => null,
    "margContenuTop" => "0",
    "margContenuBottom" => "0"
    
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$image = [];

$initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"image"
  ),"image"
);
foreach ($initImage as $key => $value) {
    $image[]= $value["imagePath"];
}
?>
<style type="text/css">
    .container<?= $kunik?> {
        padding-left:0px;
        background-color: #f6f6f6;
    }
    .content<?= $kunik?> {
        background-color: #fff;
        font-size: 14px;
        z-index: 5;
        margin-top: <?= $paramsData["margContenuTop"]?>px;
        margin-bottom: <?= $paramsData["margContenuBottom"]?>px;
       /*box-shadow: 0px 0px 5px -2px rgba(0,0,0,0.5);*/
       box-shadow: 0 9px 0px 0px white, 0 -12px 0px 0px white, 12px 0 15px -4px rgba(0,0,0,0.1), -15px 0 11px -11px rgba(0,0,0,0.1);

    }
    .content<?= $kunik?> .ourvalues {
        padding: 0 8% 0 7%;        
    }
    .content<?= $kunik?> iframe {
        padding: 0% 8% 0% 0%;
    }
    .content<?= $kunik?> .title-1 ol li {
        font-size: <?= @$blockCms["blockCmsTextSizeTitle1"]?>px
    }
    .content<?= $kunik?> .title-4 ol li {
        font-size: <?= @$blockCms["blockCmsTextSizeTitle4"]?>px
    }
    .container<?= $kunik?> hr {
        width:40%; 
        margin:20px auto; 
        border: 1px <?= $paramsData["hrBorder"]?> <?= $paramsData["hrColor"]?>;
    }
    .listEntry {
        border: 1px solid #24284d;
        margin: 10px;
        padding-bottom: 6px !important;
    }

    @media (max-width: 767px) {
        .content<?= $kunik?> .title-1 p,
        .content<?= $kunik?> .title-2 p,
        .content<?= $kunik?> .title-3 p,
        .content<?= $kunik?> .title-6 p{
            font-size: 18px !important;
        }

        .content<?= $kunik?> .title-4 p{
            font-size: 14px !important;
        }
        .content<?= $kunik?> .title-5 p{
            font-size: 10px !important;
        }
        .content<?= $kunik?> .ourvalues {
            padding: 0 ;        
        }
        .content<?= $kunik?> iframe {
            width: 100%;
            height: 245px;
            padding: 0;
        }
        .container<?= $kunik?> hr {
            width:80%; 
            margin:15px auto;
        }
    }
</style>
<div class="col-md-12 col-sm-12 col-xs-12 container<?= $kunik?>">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 content<?= $kunik?>" >
        <div class="col-xs-12 font-montserrat sp-cms-container ourvalues  "> 
              <?php 
              if (isset($paramsData["allContent"])) { ?>  
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
                    <?php 
                      foreach ($paramsData["allContent"] as $key => $value) {?>
                        <div class="markdown <?= isset($value["class"])?$value["class"] :"" ?>"><?php echo $value["content"]; ?></div>
                        <hr style="display: <?= isset($value["hrAffiche"]) ? $value["hrAffiche"]: ""; ?>">
                     <?php } ?>         
            </div>
            <?php } ?>  
            <div class="text-center col-xs-12">
                <img class="img-responsive"src="<?php echo !empty($image) ? $image[0] : ""; ?>">
            </div>
        </div>                      

    </div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",
                "properties" : {  
                 "allContent" : {
                    "label" : "Contenus",
                    "inputType" : "lists",
                    "entries":{
                        "key":{
                            "type":"hidden",
                            "class":""
                        },
                        "content":{
                            "label":"Contenu {num}",
                            "type":"textarea", 
                            "markdown" : true,                          
                            "class":"col-md-12 col-sm-12 col-xs-12 "
                        },
                        "class":{
                            "label":"class",
                            "type":"select",
                             "options": {
                                "" :"",
                                "title-1" :"title-1",
                                "title-2" :"title-2",
                                "title-3" :"title-3",
                                "title-4" :"title-4",
                                "title-5" :"title-5",
                                "title-6" :"title-6"
                            },
                            "class":"col-md-5 col-sm-5 col-xs-11"
                        },
                        "hrAffiche"  : {
                            "label" : "Afficher la ligne séparatrice",
                            "type":"select",
                             "options": {
                                "block" :"Affché",
                                "none" :"Caché"
                            },
                            "class":"col-md-6 col-sm-6 col-xs-11"
                        }
                    }
                },                  
                "image" :{
                    "inputType" : "uploader",
                    "label" : "image",
                    "docType": "image",
                    "contentKey" : "slider",
                    "domElement" : "image",
                    "itemLimit" : 1,
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,              
                    "endPoint" :"/subKey/image",
                    initList : <?php echo json_encode($initImage) ?>
                },
                "hrColor" : {
                    "inputType" : "colorpicker",
                    label : "Couleur de la ligne séparatrice ",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.hrColor
                },
                "hrBorder" : {
                    label: "Type de ligne",
                    inputType: "select",
                    options: {
                        "dashed": "dashed",
                        "dotted": "dotted",
                        "double": "double",
                        "groove": "groove",
                        "inset": "inset",
                        "ridge": "ridge",
                        "solid": "solid",
                        "outset": "outset"
                    },
                    value: sectionDyf.<?php echo $kunik?>ParamsData.hrBorder
                },
                "margContenuTop" : {
                    "inputType" : "text",
                    label : "Haut ",
                    rules: {
                        number: true,
                    },
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.margContenuTop
                },
                "margContenuBottom" : {
                    "inputType" : "text",
                    label : "Bas ",
                    rules: {
                        number: true,
                    },
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.margContenuBottom
                },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                   
                    tplCtx.value[k] = $("#"+k).val();
                     if(k == "allContent")
                      tplCtx.value[k] = data.allContent;
                });
                mylog.log("save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                  });
                } );
              }
          }
      }
  };
  

  $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = "allToRoot";
    createFontAwesomeSelect<?php echo $kunik ?>();
    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"hr",4,6,null,null,"Propriété de la ligne séparatrice","green","");
    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"img",4,6,null,null,"Propriété de l'image","green","");
    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton","green","");
    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"margContenu",4,6,null,null,"Propriété du contenu","green","");
});
})
function createFontAwesomeSelect<?php echo $kunik ?>(){
    dyFObj.init.buildEntryList = function(num, entry,field, value){
        mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
        countEntry=Object.keys(entry).length;
        defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
        
        str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
            $.each(entry, function(e, v){
                name=field+e+num;
                classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                str+="<div class='"+classEntry+"'>";
                if(typeof v.label != "undefined"){
                    str+='<div class="padding-5 col-xs-12">'+
                        '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                    '</div>'+
                    '<div class="space5"></div>';
                }
                valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                if(v.type=="hidden")
                    str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="text")
                    str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="textarea"){
                    // if(typeof v.markdown != "undefined" && v.markdown  == true )
                    //     dataHelper.activateMarkdown(".teste ");
                        //sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties.allContent.entries[e].markdown = true;
                    str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                }
                else if(v.type=="select"){
                    str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                    //This is the select Options-----------------//

                    $.each(v.options,function(k,v){
                        str+='<option value="'+k+'" '+((valueEntry == k) ? "selected" : "" )+' >'+v+'</option>';
                    });
                    //str+= createSelectOptions(valueEntry,fontAwesome);
                    str+='</select>';             
                }
                str+="</div>";
            });
            str+='<div class="col-sm-1">'+
                    '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                '</div>'+
            '</div>';
        return str;
    }; 
}
function createSelectOptions(current,Obj){
    var html = "";
    $.each(clssss,function(k,v){
        html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
    });
    return html;
}
</script>
 

