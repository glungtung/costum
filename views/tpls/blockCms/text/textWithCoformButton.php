
<?php 
    $keyTpl ="textWithCoformButton";
    $paramsData = [
        "contentTitle"=>"Lorem ipsum dolor sit amet",
        "buttonCoform"=>"bouton coform",
        "buttonColor"=>"#ffb153", 
        "buttonInfos"=>"",
        "linkInfos"=>"",
        "introduction"=>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
        "formId"=>""
    ];

    $childForm = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<!-- ****************get image uploaded************** -->
<?php 

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
    $image = [];

    foreach ($initImage as $key => $value) {
        $image[]= $value["imagePath"];
    }

    if($image!=[]){
        $paramsData["image"] = $image[0];
    }
    
 ?>
<!--****************end get image uploaded**************-->

<?php 

    $userId = "";

    if(isset(Yii::app()->session["userId"])){
        $userId = Yii::app()->session["userId"];
    }

    # If user is admin
    #$is_admin = false;
    #$admins = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
    #if(count($admins)!=0){
    #    $is_admin = true;
    #}

    $formId = "";

    # If user has already joined
    $members = array();
    if($userId!="" && strlen($userId)>1){
        $members = PHDB::find("organizations", array("links.members.".$userId=>['$exists' => true ], "source.keys"=>$costum['slug']));
    }
    
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    if($paramsData["formId"]!=""){
        $formId = $paramsData["formId"];
    }else{
        # Get the form id
        $form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
        foreach($form as $key => $value) { $formId = $key; }
    }

    if(!empty($formId))
        $formParams=PHDB::findOneById(Form::COLLECTION,$formId);
    //var_dump($formParams);exit;
    # If user has submited the coform
    $answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId ,  "user" => $userId, "draft"=>['$exists' => false ]));
    $has_answered = false;
    
    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if($userId == $value["user"] && isset($value['_id']->{'$id'})){
            $myAnswer = $value['_id']->{'$id'};
        }
    }

    if($userId!=""){
        
        $data = PHDB::findByIds("citoyens", [$userId] ,["links.follows", "links.memberOf"]);
        
        $memberOf_ids = array();

        if(isset($data[$userId]["links"]["memberOf"])){
            $links2 = $data[$userId]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }
?>
<style type="text/css">
    .btn<?= $kunik ?> {
       -webkit-transition: 0.5s ease;
       -moz-transition: 0.5s;
       -ms-transition: 0.5s;
       -o-transition: 0.5s;
       transition: 0.5s;
       font-size: clamp(8px, 10px, 12px)
    }

    .btn<?= $kunik ?>.btn-action {
        font-size: 16px; 
        /*border: 2px solid <?= $paramsData["buttonColor"] ?>; */
        background:transparent;
        padding:15px 40px; 
        text-transform:uppercase;
        border-radius:30px;
        color:<?= $paramsData["buttonColor"] ?>;
    }

    .btn<?= $kunik ?>.btn-coform{
        color: white;
        font-weight: bold;
        background:<?= $paramsData["buttonColor"] ?>;
    }

    .btn<?= $kunik ?>.btn-action:hover {
        background:<?= $paramsData["buttonColor"] ?>;
        border-radius:10px;
        /*border: 2px solid <?= $paramsData["buttonColor"] ?>;*/
        color:white;
    }
</style>
<section id="AssoLibre" class="row">
    <div class="text-center col-xs-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h1 class=" padding-20 text-center sp-text title-1" data-id="<?= $blockKey ?>" data-field="contentTitle"> 
            <?php echo $paramsData["contentTitle"]; ?>
        </h1>
        <div class="description text-center col-xs-12 sp-text" data-id="<?= $blockKey ?>" data-field="introduction">
            <?php echo "\n".$paramsData["introduction"]; ?>
        </div>

        <?php if(isset($_SESSION["userId"]) || (isset($formParams) && isset($formParams["temporarymembercanreply"]) && ($formParams["temporarymembercanreply"]==true || $formParams["withconfirmation"]==true))){ 
            $myAnswer = PHDB::findOne("answers", array("user"=>$userId, "form"=>$formId, "draft" => ['$exists' => false ]));
            $formString = "";
            if(isset($myAnswer["_id"]) && !empty($myAnswer["_id"]) && $formParams["oneAnswerPerPers"]==true){
                $formString = "#answer.index.id.".$myAnswer["_id"]->{'$id'}.".mode.w";
            }else{
                $formString = "#answer.index.id.new.form.".$formId;
            }
        ?>
        <div class="col-xs-12 margin-top-20">
            <?php if($formId!=""){ ?>
                <a class="btn<?= $kunik ?> btn-coform btn-action btn-yes btn-lg lbh sp-bg" data-id="<?= $blockKey ?>" data-field="buttonColor" data-value="<?= $paramsData['buttonColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>" href="<?php echo $formString ?>" style="text-decoration : none;"> 
                    <?php echo $paramsData["buttonCoform"]; ?>
                </a>
            <?php } ?>

            <?php if($paramsData["buttonInfos"]!=""){ 

                $urlAction = "href='javascript:void(0);'";
                
                if(filter_var($paramsData["linkInfos"], FILTER_VALIDATE_URL)){
                    $urlAction = 'target="_blank" href="'.$paramsData["linkInfos"].'"';
                } ?>

                &nbsp; &nbsp;
                <a class="btn<?= $kunik ?> btn-action btn-primary btn-lg btn-plusinfo sp-bg" data-id="<?= $blockKey ?>" data-field="buttonColor" data-value="<?= $paramsData['buttonColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>"  <?php echo $urlAction; ?> style="text-decoration : none;"> 
                    <?php echo $paramsData["buttonInfos"] ?>
                </a>
            <?php } ?>
        </div>
    <?php }else if($formId!=""){ ?>
        <div class="text-center">
            <button class="btn<?= $kunik ?> btn-primary btn-action sp-bg" data-id="<?= $blockKey ?>" data-field="buttonColor" data-value="<?= $paramsData['buttonColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>" data-toggle="modal" data-target="#modalLogin" style="padding: .8em;">
                <i class="fa fa-sign-in" style="margin-right: .6em"></i>
                <?php echo Yii::t("common","Please Login First") ?>
            </button>
        </div>
    <?php } ?>
    </div>
</section>
<br/>
<br/>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    jQuery(document).ready(function() {
        
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",            
            "properties" : {
                "buttonCoform" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Text of the button for access to the coform')?>",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonCoform
                },
                "buttonInfos" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Text of the button for more information')?>",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonInfos
                },
                "buttonColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button color')?>",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColor
                },
                "linkInfos" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Link to more information')?>",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.linkInfos
                },
                "formId": {
                    "label" : "<?php echo Yii::t('cms', 'Forms')?> :",
                    "class" : "form-control",
                    "inputType" : "select",
                    "options": {
                        <?php 
                            foreach($childForm as $key => $value) { 
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }    
                        ?>
                    }
                }
            },
           beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                   dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  });
                }
            }
        }
    }

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        
    });
    
</script>
