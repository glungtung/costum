
<div id="<?php echo @$keyTpl; ?>" class="section-desc markdown"><?= @$desc; ?></div>

<script>
    jQuery(document).ready(function(){
        $.each($(".markdown"), function(k,v){
          descHtml = dataHelper.markdownToHtml($(v).html());
          $(v).html(descHtml);
        });
    });
</script>