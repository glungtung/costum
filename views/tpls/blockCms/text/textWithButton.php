<?php
$keyTpl     = "textWithButton";
$paramsData = [
    "title"             => "Lorem Ipsum",
    "sizeTitle"         => "35",
    "content"           => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "sizeContent"       => "20",
    "lienButton"        => "",
    "labelButton"       => "Boutton",
    "colorlabelButton"  => "#000000",
    "colorBorderButton" => "#000000",
    "colorButton"       => "#ffffff",
    "colorButtonHover" => "#000",
    "colorlabelButtonHover" => "#fff",
    "class"             => "lbh",
    "link"              => ""
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">
  .textBack_<?=$kunik?> h2{
    margin-bottom: 10px;
    margin-top: 10px;
  }
  .button_<?=$kunik?> {
    background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorButton"]; ?>;
    border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["colorBorderButton"]; ?>;
    color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["colorlabelButton"]; ?>;
    margin-bottom: 4%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    padding: 10px;
    font-size: 16px;
    cursor: pointer;
    /*border-radius: 20px ;*/
    padding: 8px 8px;
  }

  .button_<?=$kunik?>:hover {
    background-color: <?=$paramsData["colorButtonHover"]?>;
    color: <?=$paramsData["colorlabelButtonHover"]?>;
    font-weight:700;
  }
/*  .block-container-<?=$kunik?> {
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }*/

@media (max-width: 414px) {
  .textBack_<?=$kunik?> h2{
   font-size: 20px;
  }
  .textBack_<?=$kunik?> p{
    font-size: 14px
  }

  .button_<?=$kunik?> {
    padding: 8px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 11px;
    cursor: pointer;
    border-radius: 20px;
  }
}

@media screen and (min-width: 1500px){
  .textBack_<?=$kunik?> h2{
    font-size: 40px;
  }
  .textBack_<?=$kunik?> p{
    line-height: 35px;
    font-size: 25px;
  }
  .button_<?=$kunik?> {
    padding: 10px 20px;
    margin-bottom: 2%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 25px;
    cursor: pointer;
    border-radius: 35px;
  }
}
</style>

  <section  class="parallax_<?=$kunik?>" >
    <div class="col-xs-12 textBack_<?=$kunik?> text-center container"  >
      <h2 class="title-1 sp-text" data-id="<?= $blockKey ?>" data-field="title"> <?=$paramsData["title"]?></h2>
      <div class="description sp-text" data-id="<?= $blockKey ?>" data-field="content" ><?=$paramsData["content"]?></div>

      <a href="<?=$paramsData["lienButton"]?>" class="<?=$paramsData["link"]?> button_<?=$kunik?> btn btn-lg btn-primary"  target="_blank">
       <?=$paramsData["labelButton"]?>
     </a>
   </div>
   
 </section>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
          "labelButton" : {
            "label" : "<?php echo Yii::t('cms', 'Button label')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelButton
          },
          "link":{ 
            "label" : "<?php echo Yii::t('cms', 'Internal or external link')?>",
            inputType : "select",
            options : {              
              "lbh " : "<?php echo Yii::t('cms', 'Internal')?> : <?php echo Yii::t('cms', 'full page')?>",
              "lbh-preview-element " : "<?php echo Yii::t('cms', 'Internal')?> : <?php echo Yii::t('cms', 'prévisualisation')?>",
              " " : "<?php echo Yii::t('cms', 'External')?>",
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.link
          },
          "lienButton" : {
            "label" : "<?php echo Yii::t('cms', 'Button link')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.lienButton
          },
          "colorlabelButton":{
            label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButton
          },
          "colorButton":{
            label : "<?php echo Yii::t('cms', 'Button color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButton
          },
          "colorButtonHover":{
            label : "<?php echo Yii::t('cms', 'Background color on hover')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButtonHover
          },
          "colorlabelButtonHover" :{
            label : "<?php echo Yii::t('cms', 'Text color on hover')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButtonHover
          },
          "colorBorderButton":{
            label : "<?php echo Yii::t('cms', 'Button border color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorderButton
          }
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


  });
</script>