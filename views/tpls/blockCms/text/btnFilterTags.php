

<?php
$keyTpl = "btnFilterTags";
$paramsData = [
	"btnBgColor" => "#0dab76",
    "btnLabelColor" => "white",
    "btnLabel" => "Observatoire de l’innovation",
    "btnBorderColor" => "#0dab76",
    "btnRadius" => "12",
    "btnLink" => "#mapping",
	"btnPadding" => "6px 20px",
	"btnPolice" => "",
	"btnfontWeight" => "600",
	"btnfontSize" => "18",
	"btnMarginTop" => "10px"


];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style>
    .<?= $kunik ?> .btnprez {
        font-family: <?php echo str_replace(" ", "_", (explode(".ttf",(array_reverse(explode("/", $paramsData["btnPolice"]))[0]))[0])) ?>;
        padding: <?php echo $paramsData["btnPadding"] ?>;
        font-weight: <?= $paramsData["btnfontWeight"]?>;
        cursor: pointer;
        text-decoration : none;
        font-size:  <?= $paramsData["btnfontSize"]?>px;
        background-color:<?php echo $paramsData["btnBgColor"] ?>;
	    color:<?php echo $paramsData["btnLabelColor"] ?>;
	    border-radius: <?php echo $paramsData["btnRadius"] ?>px !important;
	    border:2px solid <?php echo $paramsData["btnBorderColor"] ?>;
    }
    @media (min-width: 992px) {       
		.<?= $kunik ?> {
			margin-top : <?= $paramsData["btnMarginTop"]?>
		}
    }
</style>
<div class="<?= $kunik ?>">
    <a href="<?php echo $paramsData["btnLink"] ?>" class="btnprez lbh" id="btnLink"><?php echo $paramsData["btnLabel"] ?></a>
</div>
<script>
    var btn = document.getElementById('btnLink'); 
    var tags = searchObj.search.obj.filters.tags;
    if(typeof searchObj.search.obj.filters.tags != "undefined")
        btn.href = "<?= $paramsData["btnLink"]?>?tags="+tags
    else    
        btn.href = "<?= $paramsData["btnLink"]?>"
    
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    				
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				 "properties" : {	
	                "btnLabel" : {
	                  "label" : "<?php echo Yii::t('cms', 'Button label')?>",
	                  inputType : "text",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
	                },
	                "btnLabelColor":{
	                  label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
	                  inputType : "colorpicker",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabelColor
	                },
	                "btnBgColor":{
	                  label : "<?php echo Yii::t('cms', 'Color')?>",
	                  inputType : "colorpicker",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
	                },
	                "btnBorderColor":{
	                  label : "<?php echo Yii::t('cms', 'Border colo')?>",
	                  inputType : "colorpicker",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBorderColor
	                },
	                "btnRadius" : {
	                  "label" : "<?php echo Yii::t('cms', 'Border radius(px)')?>",
	                  inputType : "text",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnRadius
	                },					
					"btnPadding" : {
	                  "label" : "<?php echo Yii::t('cms', 'Button padding (px or %)')?>",
	                  inputType : "text",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnPadding
	                },
					"btnPolice" : {
	                  "label" : "<?php echo Yii::t('cms', 'Font')?>",
	                  inputType : "select",
					  options : fontObj,
					  "isPolice": true,
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnPolice
	                },
					"btnfontWeight" : {
	                  "label" : "<?php echo Yii::t('cms', 'Font weight')?>",
	                  inputType : "text",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnfontWeight
	                },
					"btnfontSize" : {
	                  "label" : "<?php echo Yii::t('cms', 'Text size')?>(px) ",
	                  inputType : "text",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnfontSize,
					  rules :{
						  number :true
					  }
	                },
					"btnMarginTop" : {
	                  "label" : "<?php echo Yii::t('cms', 'Margin on top of the button (in px or %)')?>",
	                  inputType : "text",
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnMarginTop
	                }
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                      $("#ajax-modal").modal('hide');
						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
					
				}
			}
		};
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"btn",4,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","green","");
		});
    });
</script>