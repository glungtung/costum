<?php 
$keyTpl = "text3Columns";
$paramsData=[
	"title1" => "Lorem Ipsum ",
	"content1"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"firstLinkImage" => "",
	"firstTypeLink" => "",
	"title2" => "Lorem Ipsum ",
	"content2"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"linkImage2" => "",
	"secondLinkImage" => "",
	"secondTypeLink" => "",
	"title3" => "Lorem Ipsum ",
	"content3"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"linkImage3" => "",
	"thirdLinkImage" => "",
	"thirdTypeLink" => ""
];
 
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$arrayImgOne = [];
$initImageOne = [];
$arrayImgTwo = [];
$initImageTwo = [];
$arrayImgThree = [];
$initImageThree = [];

$initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
    ),"image"
);

foreach ($initImage as $key => $value) {
	if ($value["subKey"] == "imgOne") {
		$initImageOne[] = $value; 
		if (!empty($value["imageMediumPath"]) && Document::urlExists($baseUrl.$value["imageMediumPath"]))
			$arrayImgOne[]= $value["imageThumbPath"];
		else 
			$arrayImgOne[]= $value["imagePath"];

	}
	if ($value["subKey"] == "imgTwo") {
		$initImageTwo[] = $value; 
		if (!empty($value["imageMediumPath"]) && Document::urlExists($baseUrl.$value["imageMediumPath"]))
			$arrayImgTwo[]= $value["imageThumbPath"];
		else 
			$arrayImgTwo[]= $value["imagePath"];
	}
	if ($value["subKey"] == "imageThree") {
		$initImageThree[] = $value; 
		if (!empty($value["imageMediumPath"]) && Document::urlExists($baseUrl.$value["imageMediumPath"]))
			$arrayImgThree[]= $value["imageThumbPath"];
		else 
			$arrayImgThree[]= $value["imagePath"];
	}
}

?>

<style type="text/css">
	#fh5co-intro_<?= $kunik ?>{
		margin-bottom: -50px;
	}

	#fh5co-intro_<?= $kunik ?> .fh5co-block {
		float: left;
		text-align: center;
		font-size: 16px;
		min-height: 420px;
		vertical-align: middle;
		padding: 40px;
		background: #fff;
		background-size: cover;
		position: relative;
		backgroun-repeat: no-repeat;
	}
	#fh5co-intro_<?= $kunik ?> .fh5co-block >  .fh5co-intro_<?= $kunik ?>-icon {

		font-size: 50px;
		margin-bottom: 30px;
		display: block;
		text-align: center;
	}
	#fh5co-intro_<?= $kunik ?> .fh5co-block >  .fh5co-intro_<?= $kunik ?>-icon img {
		width: 100px;
		height: 90px;
	}
	#fh5co-intro_<?= $kunik ?> .fh5co-block h2 {
		text-align: center;
		font-size: 24px;
		font-weight: 400;
	}
	
	@media (max-width: 414px) {
		#fh5co-intro_<?= $kunik ?> .fh5co-block h2 {
			font-size: 20px;
			margin-top: 3px;
		}
		#fh5co-intro_<?= $kunik?> .fh5co-block {
			float: left;
			text-align: left;
			min-height: 180px;
			padding: 12px;
		}
		#fh5co-intro_<?= $kunik ?> .fh5co-block >  .fh5co-intro_<?= $kunik ?>-icon {
			font-size: 15px;
			text-align: center;
			margin-bottom: 0px;
		}

		#fh5co-intro_<?= $kunik ?> .fh5co-block >  .fh5co-intro_<?= $kunik ?>-icon img {
			width: 40px;
			height: 40px;
			text-align: center;
		}
		.text_<?=$kunik?> p {
			text-align: center;
			height: 100%;
			color: #000;
			font-size: 12px !important;
			line-height: 25px;
			padding: 0;
			text-align: center;
			font-style: italic;
			text-shadow: 0.5px 0.5px 1px rgba(0, 0, 0, 0.3);
		}


	}
	@media (max-width: 768px) {
		#fh5co-intro_<?= $kunik ?> .fh5co-block h2 {
			font-size: 20px;
			margin-top: 3px;
		}
		.text_<?=$kunik?> {
			width: 50%;
			height: 100%;
			position: relative;
			margin: 0;
			
		}
	}

	@media screen and (min-width: 1500px){
		#fh5co-intro_<?= $kunik ?> .fh5co-block h2 {
			font-size: 35px !important;
		}
		.text_<?=$kunik?> p {		
			font-size: 17px !important;
		}
	}


	.text_<?=$kunik?> {
		width: 100%;
		height: 100%;
		position: relative;
		margin: 0;
		
	}

	.text_<?=$kunik?> p {
		height: 100%;
		color: #000;
		font-size: 14px;
		line-height: 25px;
		padding: 0;
		text-align: center;
		font-style: italic;
		text-shadow: 0.5px 0.5px 1px rgba(0, 0, 0, 0.3);
	}





</style>
<div id="fh5co-intro_<?= $kunik ?>">

	<div class="sp-cms-container">
		<div class="row row-bottom-padded-lg">
			<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4 fh5co-block fadeInUp animated" >	<i class="fh5co-intro_<?= $kunik ?>-icon ">
			
					<?php if ($paramsData["firstLinkImage"] == " " ){
						if (count($arrayImgOne ) != 0) {  ?>
							<img src="<?php echo $arrayImgOne[0] ?>">
							<?php }else { ?>
								<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/LIENS-02.svg">
							<?php } 
						} else {
							if (count($arrayImgOne ) != 0) {  ?>
							<a href="<?= $paramsData["firstLinkImage"]?>" class="<?=$paramsData["firstTypeLink"]?>"  target="_blank">
								<img src="<?php echo $arrayImgOne[0] ?>">
							</a>
							<?php }else { ?>
								<a href="<?= $paramsData["firstLinkImage"]?>" class="<?=$paramsData["firstTypeLink"]?>"  target="_blank">
									<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/LIENS-02.svg">
								</a>
							<?php } 
						}?>
				</i>
				<h2 class="title sp-text" data-id="<?= $blockKey ?>" data-field="title1"><?= $paramsData["title1"]?></h2>
				<div class="">
					<div class="text_<?=$kunik?>" >
						<div class=" description text-center sp-text" data-id="<?= $blockKey ?>" data-field="content1" ><?= $paramsData["content1"]?></div >
					</div>
				</div>
			</div>
			<div class=" col-lg-4 col-xs-12 col-md-4  col-sm-4 fh5co-block fadeInUp animated" >
				<i class="fh5co-intro_<?= $kunik ?>-icon">
					<?php if ($paramsData["secondLinkImage"] == " " ){
							if (count($arrayImgTwo ) != 0) {  ?>
								<img src="<?php echo $arrayImgTwo[0] ?>">
							<?php }else { ?>
								<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/Ethik-02.svg">
							<?php } 
						} else {
							if (count($arrayImgTwo ) != 0) {  ?>
							<a href="<?= $paramsData["secondLinkImage"]?>" class="<?=$paramsData["secondTypeLink"]?>"  target="_blank">
								<img src="<?php echo $arrayImgTwo[0] ?>">
							</a>
							<?php }else { ?>
								<a href="<?= $paramsData["secondLinkImage"]?>" class="<?=$paramsData["secondTypeLink"]?>"  target="_blank">
									<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/LIENS-02.svg">
								</a>
							<?php } 
					 }?>

				</i>
				<h2 class=" title sp-text" data-id="<?= $blockKey ?>" data-field="title2"><?= $paramsData["title2"]?></h2>
				<div class="text_<?=$kunik?>" >
					<div class="description text-center sp-text" data-id="<?= $blockKey ?>" data-field="content2" ><?= $paramsData["content2"]?></div >
				</div>
			</div>
			<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4 fh5co-block fadeInUp animated" >
				<i class="fh5co-intro_<?= $kunik ?>-icon icon-bulb">



				<?php if ($paramsData["thirdLinkImage"] == "" ){
						if (count($arrayImgThree ) != 0) {  ?>
							<img src="<?php echo $arrayImgThree[0] ?>">
						<?php }else { ?>
							<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/Ethik-02.svg">
						<?php } 
					} else {
						if (count($arrayImgThree ) != 0) {  ?>
						<a href="<?= $paramsData["thirdLinkImage"]?>" class="<?=$paramsData["thirdTypeLink"]?>"  target="_blank">
							<img src="<?php echo $arrayImgThree[0] ?>">
						</a>
						<?php }else { ?>
							<a href="<?= $paramsData["thirdLinkImage"]?>" class="<?=$paramsData["thirdTypeLink"]?>"  target="_blank">
								<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/LOCAL-02.svg">
							</a>
						<?php } 
					}?>
				</i>
				<h2 class="title sp-text" data-id="<?= $blockKey ?>" data-field="title3"><?= $paramsData["title3"]?></h2>
				<div class="">
					<div class="text_<?=$kunik?> " >
						<div class="description text-center sp-text" data-id="<?= $blockKey ?>" data-field="content3" ><?= $paramsData["content3"]?></div >
					</div>
				</div>
			</div>			
		</div>
	</div>
		
</div>

	
	<script type="text/javascript">
		sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
		jQuery(document).ready(function() {
			sectionDyf.<?php echo $kunik?>Params = {
				"jsonSchema" : {    
					"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
					"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
					"icon" : "fa-cog",
					"properties" : {
						
						"imageOne" :{
			              "inputType" : "uploader",
			              "label" : "<?php echo Yii::t('cms', 'Image')?> 1",
			              "docType": "image",
							"contentKey" : "slider",
			              "domElement" : "imgOne",
			              "itemLimit" : 1,
			              "filetypes": ["jpeg", "jpg", "gif", "png"],
			              "showUploadBtn": false,              
			                "endPoint" :"/subKey/imgOne",
			              initList : <?php echo json_encode($initImageOne) ?>
			            },
						"firstLinkImage" : {
							label : "<?php echo Yii::t('cms', 'Link')?> ",
							"inputType" : "text",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.firstLinkImage
						},
						"firstTypeLink":{ 
							"label" : "<?php echo Yii::t('cms', 'Internal or external link')?> ",
							inputType : "select",
							options : {              
								"lbh " : "<?php echo Yii::t('cms', 'Internal')?> :<?php echo Yii::t('cms', 'full page')?>",
								" " : "<?php echo Yii::t('cms', 'External')?>",
							},
							values :  sectionDyf.<?php echo $kunik ?>ParamsData.firstTypeLink
						},
						"imageTwo" :{
			              "inputType" : "uploader",
			              "label" : "<?php echo Yii::t('cms', 'Image')?> 2",
			              "docType": "image",
						  "contentKey" : "slider",
			              "domElement" : "imgTwo",
			              "itemLimit" : 1,
			              "filetypes": ["jpeg", "jpg", "gif", "png"],
			              "showUploadBtn": false,              
			                "endPoint" :"/subKey/imgTwo",
			              initList : <?php echo json_encode($initImageTwo) ?>
			            },
						"secondLinkImage" : {
							label : "<?php echo Yii::t('cms', 'Link')?> ",
							"inputType" : "text",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.secondLinkImage
						},
						"secondTypeLink":{ 
							"label" : "<?php echo Yii::t('cms', 'Internal or external link')?> ",
							inputType : "select",
							options : {              
								"lbh " : "<?php echo Yii::t('cms', 'Internal')?> :<?php echo Yii::t('cms', 'full page')?>",
								" " : "<?php echo Yii::t('cms', 'External')?>",
							},
							values :  sectionDyf.<?php echo $kunik ?>ParamsData.secondTypeLink
						},

						"imageThree" :{
			              "inputType" : "uploader",
			              "label" : "<?php echo Yii::t('cms', 'Image')?> 3",
			              "docType": "image",
						  "contentKey" : "slider",
			              "domElement" : "imgThree",
			              "itemLimit" : 1,
			              "filetypes": ["jpeg", "jpg", "gif", "png"],
			              "showUploadBtn": false,              
			                "endPoint" :"/subKey/imageThree",
			              initList : <?php echo json_encode($initImageThree) ?>
			            },
						"thirdLinkImage" : {
							label : "<?php echo Yii::t('cms', 'Link')?> ",
							"inputType" : "text",
							values :  sectionDyf.<?php echo $kunik?>ParamsData.thirdLinkImage
						},
						"thirdTypeLink":{ 
							"label" : "<?php echo Yii::t('cms', 'Internal or external link')?> ",
							inputType : "select",
							options : {              
								"lbh " : "<?php echo Yii::t('cms', 'Internal')?> :<?php echo Yii::t('cms', 'full page')?>",
								" " : "<?php echo Yii::t('cms', 'External')?>",
							},
							values :  sectionDyf.<?php echo $kunik ?>ParamsData.thirdTypeLink
						},
					},
					beforeBuild : function(){
		                uploadObj.set("cms","<?php echo $blockKey ?>");
		            },
					save : function () {  
						tplCtx.value = {};

						$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
							tplCtx.value[k] = $("#"+k).val();
						});

						mylog.log("save tplCtx",tplCtx);

						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						 else {
			              dataHelper.path2Value( tplCtx, function(params) {
			                dyFObj.commonAfterSave(params,function(){
			                  toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
			                  $("#ajax-modal").modal('hide');
			                  dyFObj.closeForm();
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
			                    // urlCtrl.loadByHash(location.hash);
			                });
			              } );
			            }
					}
				}

			};
			mylog.log("paramsData",sectionDyf);
			$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
				tplCtx.id = $(this).data("id");
				tplCtx.collection = $(this).data("collection");
				tplCtx.path = "allToRoot";
				dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
				alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"first",6,6,null,null,"<?php echo Yii::t('cms', 'Image link')?> 1 (<?php echo Yii::t('cms', 'optional')?>)","#1da0b6");
				alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"second",6,6,null,null,"<?php echo Yii::t('cms', 'Image link')?> 2 (<?php echo Yii::t('cms', 'optional')?>)","#1da0b6");
				alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"third",6,6,null,null,"<?php echo Yii::t('cms', 'Image link')?> 3 (<?php echo Yii::t('cms', 'optional')?>)","#1da0b6");
			});
		});
	</script>