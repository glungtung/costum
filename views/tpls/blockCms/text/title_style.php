<?php 
$keyTpl = "title_style";
$paramsData = [
	"title"=>"Questions Fréquentes",
	"colordecor" =>"#5f9f0d",
	"colorLabelButton" =>"#ffffff",
	"bgcolor" => "#e8e8e8",
	"typeInLeft" => "text",
	"textLeft" => "?",
	"textLeftColor" => "#ffffff",
	"iconLeft" => "share", 
	"iconLeftColor" => "#ffffff"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>

<style type="text/css">
	.hex<?php echo $kunik ?> {
      position: relative;
      float: left;
      height: 60px;
      min-width: 75px;

      font-weight: bold;
      text-align: center;
      background: <?php echo $paramsData["bgcolor"]; ?>;
      -webkit-clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
    }
    .hex<?php echo $kunik ?>.gradient-bg {
      left: 50%;
      transform: translate(-50%,-50%);
      min-width: 60%;
    }

    .hex<?php echo $kunik ?>:before {
      position: absolute;
      content: '';
      height: calc(100% - 14px);  /* 100% - 2 * border width */
      width: calc(100% - 14px);  /* 100% - 2 * border width */
      left: 7px; /* border width */
      top: 7px; /* border width */
      -webkit-clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
      clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
      z-index: -1;
    }
    .hex<?php echo $kunik ?>.gradient-bg:before {
      background: <?php echo $paramsData["bgcolor"]; ?>;
    }
    .hex<?php echo $kunik ?>.white-bg:before {
      background: #ffffff!important;
    }

    .hex<?php echo $kunik ?>.white-bg {
      background: #ffffff!important;
    }
    .hex<?php echo $kunik ?> span {
        /* display: inline-block; */
        margin-top: 30px;
        padding: 8px;
        transform: translateY(-50%);
        margin-left: 20px;
        margin-right: 20px;
        font-size: 28px;
    }
    .hex<?php echo $kunik ?> .bg-green {
      background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colordecor"]; ?>!important;
      /*color: #fff;*/
    }

    .hex<?php echo $kunik ?> .bg-green span {
      font-size: 40px;
      margin-left: 10px;
      margin-right: 10px;
    }
    .hex<?php echo $kunik ?> .bg-green span.fa {
      font-size: 30px;
    }
    .contain-faq {
        display: flex;
    }
    @media (max-width: 991px ) {
        .hex<?php echo $kunik ?>.gradient-bg {
          width: 100%;
        }
        .hex<?php echo $kunik ?> span {
            display: block;
        }
    }
    @media (max-width: 767px ) {
        .hex<?php echo $kunik ?> span {
            font-size: 20px;
        }
        .prez-heading-text {
            font-size: 20px;
        }
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 no-padding prez-multi-col">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding contain-faq">
       <div class='hex<?php echo $kunik ?> gradient-bg mt-40'>
            <div class='hex<?php echo $kunik ?> bg-green'>
            	<?php if ($paramsData['typeInLeft'] == "text") { ?>
              		<span style="color: <?php echo $paramsData['textLeftColor'] ?>;"> <?php echo $paramsData['textLeft'] ?> </span>  
            	<?php }else if ($paramsData['typeInLeft'] == "icon") { ?>
            		<span class="fa fa-<?php echo $paramsData['iconLeft'] ?>" style="color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["iconLeftColor"]; ?>;"></span>
            	<?php } ?>
            </div>
          <span class="title-1 sp-text" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></span>
        </div> 
    </div>

</div>



<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
      			"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					"bgcolor" : {
						"label" : "<?php echo Yii::t('cms', 'Background color of the title')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.bgcolor
					},
					"colordecor" : {
						"label" : "<?php echo Yii::t('cms', 'Other color')?> (<?php echo Yii::t('cms', 'decoration')?>)",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colordecor
					},					
					"colorLabelButton" : {
						"label" : "<?php echo Yii::t('cms', 'Button label color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorLabelButton
					},
					"typeInLeft" :{
	                  "inputType" : "select",
	                    "label" : "<?php echo Yii::t('cms', 'Display on the left of the title')?>",
	                    "options":{
	                      "text":"<?php echo Yii::t('cms', 'Text')?>",
	                      "icon":"<?php echo Yii::t('cms', 'Icon')?>"
	                    }
	                },
					"textLeft" : {
						"label" : "<?php echo Yii::t('cms', 'Text on the left')?>",
						"inputType" :"text",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.textLeft
					},
					"textLeftColor" : {
						"label" : "<?php echo Yii::t('cms', 'Text color on the left')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.textColor
					},
					"iconLeft" : {
						"label" : "<?php echo Yii::t('cms', 'Icon on the left')?> (fa fa-)",
						"inputType" : "select",
						"options":fontAwesome,
						"noOrder":true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.iconColor
					},
					"iconLeftColor" : {
						"label" : "<?php echo Yii::t('cms', 'Color of the icon')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.iconColor
					}	
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                      $("#ajax-modal").modal('hide');
						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
					
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() { 
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"text",6,6,null,null,"<?php echo Yii::t('cms', 'Text property')?>","green","");
			alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"icon",6,6,null,null,"<?php echo Yii::t('cms', 'Icon property')?>","green","");
			if ($("#typeInLeft").val() == "text") {
	            $('.fieldseticon').fadeOut();
	          }else {
	          	$('.fieldsettext').fadeOut();
	          }

	          $("#typeInLeft").change(function(){
	            if ($(this).val() == "text") {
	              $('.fieldseticon').fadeOut();
	            }else
	            $('.fieldseticon').fadeIn();
	            if ($(this).val() == "icon") {
	              $('.fieldsettext').fadeOut();
	            }else
	            $('.fieldsettext').fadeIn();
	          });
	          

		});

	});
</script>