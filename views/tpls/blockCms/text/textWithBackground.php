<?php 
$keyTpl = "textWithBackground";
$paramsData=[
	"title" => "Lorem Ipsum ",
	"sizeTitle"=>"39",
	"content"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"sizeContent" => "20",
	"colorTitle" =>"#000",
	"colorContent" =>"#000"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>

<style type="text/css">
	
	#fh5co-work_<?= $kunik ?> {
		overflow: hidden;
		position: relative;
	}

	#fh5co-work_<?= $kunik ?> .section-heading_<?= $kunik ?> h2 {
		color:  <?= $paramsData["colorTitle"]?>;
	}
	.section-heading_<?= $kunik ?>{
		float: left;
		width: 100%;
		
		clear: both;
	}
	.section-heading_<?= $kunik ?> h2 {
		color:  <?= $paramsData["colorTitle"]?>;
		margin: 55px 0 31px 0;
		font-size:<?= $paramsData["sizeTitle"]?>px;
		font-weight: 300;
		position: relative;
		/*display: block;*/
		padding-bottom: 20px;
		line-height: 1.5;
	}
	.section-heading_<?= $kunik ?> p {
		color:  <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorContent"]; ?>;
		font-size: <?= $paramsData["sizeContent"]?>px;
    	margin-bottom: 6%;
	}
	.section-heading_<?= $kunik ?>h2.left-border:after {
		content: "";
		position: absolute;
		display: block;
		width: 80px;
		height: 2px;
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorContent"]; ?>;
		left: 0%;
		margin-left: 0px;
		bottom: 0;
	}
	.section-heading_<?= $kunik ?>h2:after {
		content: "";
		position: absolute;
		display: block;
		width: 80px;
		height: 2px;
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorContent"]; ?>;
		left: 50%;
		margin-left: -40px;
		bottom: 0;
	}


	@media (max-width: 414px) {
		.section-heading_<?= $kunik ?> h2 {
			font-size: 20px;
			margin-top: 15px;
			margin-bottom: 0px
		}
		.section-heading_<?= $kunik ?> p {
			font-size: 13px;
		}
	}


	

	@media screen and (min-width: 1500px){
		.section-heading_<?=$kunik?> h2{
		    margin: 0 0 10px 0;
		    font-size: 40px;
		    line-height: 35px;
		    text-transform: none;
		    margin-top: 5%;
	  	}
	  	.section-heading_<?=$kunik?> p{
		    line-height: 35px;
		    font-size: 25px;
		    text-transform: none;
		    padding: 1% 0% 10% 0%;
		    margin-bottom: -90px;
	  	}
	}
</style>
<div class="col-md-12" id="fh5co-work_<?= $kunik ?>" data-section="work">
	<div class="container">
		<div class="row">
			<div class="col-md-12 section-heading_<?= $kunik ?> text-center">
				<h2 class="title sp-text" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h2>
				<div class="row">
					<p class="description sp-text" data-id="<?= $blockKey ?>" data-field="content"> <?= $paramsData["content"]?></p>
				</div>
			</div> 
		</div>

</div>


<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
		                      $("#ajax-modal").modal('hide');
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	});
</script>
