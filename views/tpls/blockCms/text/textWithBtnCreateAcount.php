<?php
$keyTpl     = "textWithBtnCreateAcount";
$paramsData = [
  "blockTitle" => "Lorem Ipsum",
  "blockContent" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
  "buttonLabel"  => "Découvrez",
  "buttonColorlabel"  => "#000000",
  "buttonColorBorder" => "#000000",
  "buttonColor" => "#ffffff",
  "blockUrlRedirect" => "",
  "buttonDBg" => "#e2e2e2",
	"buttonDPadding" => "8px 8px",
	"buttonDBorderRadius" => "20",
	"buttonDsize"=> "16" 
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if ( isset($blockCms[$e])) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>
<style type="text/css">
.textBack_<?=$kunik?> h2{
  margin-bottom: 10px;
  margin-top: 10px;
}
.button_<?=$kunik?> {
  background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonColor"]; ?>;
  border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["buttonColorBorder"]; ?>;
  color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["buttonColorlabel"]; ?>;
  margin-bottom: 4%;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  padding: 10px;
  font-size: <?=$paramsData["buttonDsize"]?>px;
  cursor: pointer;
  border-radius: <?=$paramsData["buttonDBorderRadius"]?>px ;
  padding: <?=$paramsData["buttonDPadding"]?>;
}

/*.block-container-<?=$kunik?> {
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}*/

@media (max-width: 414px) {
  .textBack_<?=$kunik?> h2{
   font-size: 20px;
 }
 .textBack_<?=$kunik?> p{
  font-size: 14px
}

.button_<?=$kunik?> {
  padding: 8px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 11px;
  cursor: pointer;
  border-radius: 20px;
}
}

@media screen and (min-width: 1500px){
  .textBack_<?=$kunik?> h2{
    font-size: 40px;
  }
  .textBack_<?=$kunik?> p{
    line-height: 35px;
    font-size: 25px;
  }
  .button_<?=$kunik?> {
    margin-bottom: 2%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    cursor: pointer;
  }
}
</style>

<section  class="parallax_<?=$kunik?>" >
  <div class="textBack_<?=$kunik?> text-center container"  >
    <h2 class="title-1 sp-text" data-id="<?= $blockKey ?>" data-field="blockTitle"> <?=$paramsData["blockTitle"]?></h2>
    <div class="title-6 sp-text" data-id="<?= $blockKey ?>" data-field="blockContent" ><?=$paramsData["blockContent"]?></div>
    <a href="javascript:;" class="btn button_<?=$kunik?>" data-toggle="modal" data-target="#modalRegister">
     <?=$paramsData["buttonLabel"]?>
   </a>
 </div>

</section>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {

    Login.runRegisterValidator = function() {  
      mylog.log("runRegisterValidator!!!!");
      var form3 = $('.form-register');
      var errorHandler3 = $('.errorHandler', form3);
      var createBtn = null;
      if(Login.registerOptions.mode=="normal"){
        Login.addPostalCodeInRegister();
      }
      form3.validate({
        rules : Login.rulesRegister,
        submitHandler : function(form) { 
          mylog.log("runRegisterValidator submitHandler");
           
          errorHandler3.hide();
          //createBtn.start();
          $(".createBtn").prop('disabled', true);
          $(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
          params=Login.formatDataRegister();
          ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/person/register",
            params,
            function(data){
              if(data.result) {
                $(".createBtn").prop('disabled', false);
                $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                $("#registerName").val("");
                $("#username").val("");
                $("#email3").val("");
                $("#password3").val("");
                $("#passwordAgain").val("");
                $(".form-register #addressLocality").val("");
                $(".form-register #postalCode").val("");
                $(".form-register #city").val("");
                $('#agree').prop('checked', true);
                mylog.log("authenticate",data);
                if((typeof data.isInvitation != "undefined" && data.isInvitation) || Login.registerOptions.loginAfter){
                  toastr.success(data.msg);
                  if(typeof themeParams.pages["#app.index"].redirect != "undefined" 
                    && typeof themeParams.pages["#app.index"].redirect.register != "undefined"){
                    history.pushState(null, "New Title","#"+themeParams.pages["#app.index"].redirect.register);
                    document.location.href =  "<?= $paramsData["blockUrlRedirect"]?>";
                    urlCtrl.loadByHash(location.hash);
                  }else{
                    history.pushState(null, "New Title",'#page.type.citoyens.id.'+params.pendingUserId);
                    document.location.href = "<?= $paramsData["blockUrlRedirect"]?>";
                    urlCtrl.loadByHash(location.hash);
                  }
                } else{
                  $('.modal').modal('hide');
                  $("#modalRegisterSuccessblockContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
                  $("#modalRegisterSuccess").modal({ show: 'true' }); 
                  $('#modalRegisterSuccess .btn-default').click(function() {
                    mylog.log("hide modale and reload");
                    $('.modal').modal('hide');
                    document.location.href =  "<?= $paramsData["blockUrlRedirect"]?>";
                    urlCtrl.loadByHash(location.hash);
                  });
                }
              }else {
                $('.registerResult').html(data.msg);
                $('.registerResult').show();
                $(".createBtn").prop('disabled', false);
                $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
              }
            },
            function(data) {
              toastr.error(trad["somethingwentwrong"]);
              $(".createBtn").prop('disabled', false);
              $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in"); 
            }
          );
          return false;
        },
        invalidHandler : function(event, validator) {//display error alert on form submit
          errorHandler3.show();
          $(".createBtn").prop('disabled', false);
          $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
          //createBtn.stop();
        }
      });
    }
  Login.runRegisterValidator();

  sectionDyf.<?php echo $kunik ?>Params = {
    "jsonSchema" : {
      "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
      "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
      "icon" : "fa-cog",
      "properties" : {
        "blockUrlRedirect" : {
          "inputType" : "text",
          "label" : "<?php echo Yii::t('cms', 'Url of the redirection')?>",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.blockUrlRedirect
        },
        "buttonLabel" : {
          "label" : "<?php echo Yii::t('cms', 'Button label')?>",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonLabel
        },
        "buttonColorlabel":{
          label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
          inputType : "colorpicker",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColorlabel
        },
        "buttonColor":{
          label : "<?php echo Yii::t('cms', 'Button color')?>",
          inputType : "colorpicker",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColor
        },
        "buttonColorBorder":{
          label : "<?php echo Yii::t('cms', 'Button border color')?>",
          inputType : "colorpicker",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColorBorder
        },
        "buttonDsize":{
          label:"Taille de la texte( en px)",
          inputType :"text",
          values : sectionDyf.<?php echo $kunik?>ParamsData.btnDsize,
          rules:{
            number :true
          }
        },
        "buttonDPadding":{
          label:"Rembourage du bouton( en px)",
          inputType :"text",
          values : sectionDyf.<?php echo $kunik?>ParamsData.btnDPadding         
        },
        "buttonDBorderRadius":{
          label:"Rayon de la bordure( en px)",
          inputType :"text",
          values : sectionDyf.<?php echo $kunik?>ParamsData.btnDBorderRadius,
          rules:{
            number :true
          }
        }
      },

      beforeBuild : function(){
        uploadObj.set("cms","<?php echo $blockKey ?>");
      },
      save : function () {
        tplCtx.value = {};
        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
          tplCtx.value[k] = $("#"+k).val();
          if (k == "parent") {
            tplCtx.value[k] = formData.parent;
          }
        });
        mylog.log("save tplCtx",tplCtx);

        if(typeof tplCtx.value == "undefined")
          toastr.error('value cannot be empty!');
        else {
          dataHelper.path2Value( tplCtx, function(params) {
            dyFObj.commonAfterSave(params,function(){
              toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
              $("#ajax-modal").modal('hide');
              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
              // urlCtrl.loadByHash(location.hash);
            });
          });
        }

      }
    }
  };
mylog.log("sectiondyfff",sectionDyf);
$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
  tplCtx.id = $(this).data("id");
  tplCtx.collection = $(this).data("collection");
  tplCtx.path = "allToRoot";
  dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
				alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"button",6,6,null,null,"Lien image 1 (optionnel)","#1da0b6");
});


});
</script>