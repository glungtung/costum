<?php 
  $keyTpl ="blockFeatures";
  $paramsData = [ 
    "titre" => " FONCTIONNALITES",
    "description"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco "
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    } 
  }

  $cardIndex = 0;

  // ****************get image uploaded**************
  
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );

  $listOfImage = [];
  $imageUrlList = [];

  foreach ($initImage as $keyImg => $valueImg) {

    $imageUrl = isset($valueImg["imagePath"])?$valueImg["imagePath"]:"";

    array_push($imageUrlList, $imageUrl);

    array_push($listOfImage, $valueImg);
  }

//****************end get image uploaded**************
?>
<style type="text/css">
/*============ Service Features style ============*/
.service-heading-block{
	display:block;
	margin-bottom:30px;
	}
.title {
  display: block;
  font-size: 30px;
  font-weight: 200;
  margin-bottom: 10px;
}
.sub-title {
  font-size: 18px;
  font-weight: 100;
  line-height: 24px;
}
.feature-block {
  background-color: #fff;
  border-radius: 2px;
  padding: 15px;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
  margin-bottom: 15px;
  transition:all ease .5s
}
.ms-feature:hover, 
.ms-feature:focus {
  background-color: #fafafa;
  box-shadow: 0 3px 4px 3px rgba(0, 0, 0, 0.14), 0 3px 3px -2px rgba(0, 0, 0, 0.2), 0 1px 8px 3px rgba(0, 0, 0, 0.12);
}
.fb-icon {
  border-radius: 50%;
  display: block;
  font-size: 36px;
  height: 80px;
  line-height: 80px;
  text-align:center;
  margin:1rem auto;
  width: 80px;
  transition: all 0.5s ease 0s;
}
.feature-block:hover .fb-icon,
.feature-block:focus .fb-icon {
  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.2);
  transform: rotate(360deg);
}
.fb-icon.color-info {
  background-color: #5bc0de;
  color: #fff;
}
.fb-icon.color-warning {
  background-color: #eea236;
  color: #fff;
}
.fb-icon.color-success {
  background-color: #5cb85c;
  color: #fff;
}
.fb-icon.color-danger {
  background-color: #d9534f;
  color: #fff;
}
.feature-block h4 {
  text-transform: none;
  font-weight: 500;
  margin: 3rem 0 1rem;
}
.color-info {
  color: #46b8da;
}
.color-warning {
  color: #f0ad4e;
}
.color-success {
  color: #4cae4c;
}
.color-danger {
  color: #d43f3a;
}
.btn-custom{
  border: medium none;
  border-radius: 2px;
  cursor: pointer;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;  
  margin: 10px 1px;
  outline: 0 none;
  padding: 8px 30px;
  position: relative;
  text-decoration: none;
  text-transform: uppercase;
}

.icone<?= $kunik?> span{
  color: white;
}

</style>
<div class="sp-cms-container text-center  pb-5">
  <div class="row">
    <div class="service-heading-block">
      <h2 class="text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titre"><?= $paramsData["titre"]?></h2>
      <div class="text-center sub-title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description"><?= $paramsData["description"]?></div>
    </div> 
    <div class="text-center editSectionBtns">
      <div class="" style="width: 100%; display: inline-table; padding: 10px;">
        <?php if(Authorisation::isInterfaceAdmin()){?>
          <div class="text-center addElement<?= $blockCms['_id'] ?>" data-index="<?= $cardIndex ?>">
            <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add content')?></button>
          </div>  
        <?php } ?>
      </div>
    </div>      
    <?php if (isset($blockCms["content"])) {
          $content = $blockCms["content"];
          foreach ($content as $key => $value) {
            $title = isset($value["title"])?$value["title"]:"";
            $description = isset($value["description"])?$value["description"]:"";
            $link = isset($value["titleLink"])?$value["titleLink"]:"";
            $icon = isset($value["titleIcon"])?$value["titleIcon"]:"";
            $image = (isset($imageUrlList[$cardIndex])?$imageUrlList[$cardIndex]:"");
            $titlecolor = isset($blockCms["titleBackgroundColor"])?$blockCms["titleBackgroundColor"]:"#ccc";
    ?>
      <div class="col-lg-3 col-md-6 col-sm-6 col-center-block" >
        <div class="text-center feature-block">
          <a  href="<?= $link ?>" target="_blank">
          <div class="icone<?= $kunik?>">            
            <span class="fb-icon" style="background: <?= $titlecolor?>; color: white">
              <?php if($image==""){ ?>
                <i class="fa fa-<?= $icon?>" aria-hidden="true"></i>
              <?php }else if(isset($image)){ ?>
                <img src="<?= $image ?>" height="90" width="90" alt="Icon ou Logo"/>
              <?php } ?>
            </span>
          </div>
          <h4 class="title-2" style ="color:<?= $titlecolor?>;  ">
            <?= $title?>
          </h4>
          <div class="title-4 markdown" style="color:<?= $titlecolor?>"><?= $description?></div>
          <?php if(Authorisation::isInterfaceAdmin()){ ?>
            <div class="editdelete<?= $blockKey?>">
              <a  href="javascript:;"
                class="btn material-button text-center editElement<?= $blockKey ?> "
                data-key="<?= $key ?>" 
                data-title="<?= $title?>"
                data-icon ='<?= $icon ?>'
                data-image ='<?= $image ?>'
                data-index="<?= $cardIndex ?>"
                data-description="<?= $description?>" >
                <i class="fa fa-edit"></i>
              </a>
              <a  href="javascript:;"
                class="btn material-button btn-danger text-center deletePlan<?= $blockKey ?> "
                data-key="<?= $key ?>" 
                data-id ="<?= $blockKey ?>"
                data-path="content.<?= $key ?>"
                data-collection = "cms"
                >
                <i class="fa fa-trash"></i>
              </a>
          </div>  
        <?php } ?>
          </a>
        </div>
      </div>
    <?php 
        $cardIndex++;
        } 
      }
    ?>
  </div> 
</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  
  if(localStorage["previewMode"]=="v"){
    $(".editdelete<?= $blockKey?>").hide();
  }

  jQuery(document).ready(function() {
    $(".addElement<?= $blockCms['_id'] ?>").click(function() {  
      var keys = Object.keys(<?php echo json_encode($content); ?>);
      var lastKey = 0; 
      var lastIndex = <?= $cardIndex; ?>;
      alert(lastIndex);
      if (keys.length!=0) 
      var lastKey = parseInt((keys[(keys.length)-1]), 10);
      var tplCtx = {};
      tplCtx.id = "<?= $blockKey ?>";
      tplCtx.collection = "cms";

      tplCtx.path = "content."+(lastKey+1)+"."+lastIndex;
      var obj = {
          subKey : (lastKey+1),
          icon :     $(this).data("icon"),
          title :     $(this).data("title"),
          description:    $(this).data("description")
      };
      var activeForm = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Add content')?>",
          "type" : "object",
          onLoads : {
            onload : function(data){
              $(".parentfinder").css("display","none");
            }
          },
          "properties" : getProperties(obj, lastIndex),
          beforeBuild : function(){
            uploadObj.set("cms","<?= $blockCms['_id'] ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( activeForm.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            mylog.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              });
            }
          }
        }
      };
      dyFObj.openForm( activeForm );
      alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"<?php echo Yii::t('cms', 'Title')?>","#1da0b6");
      alignInput2(activeForm.jsonSchema.properties,"description",12,12,null,null,"<?php echo Yii::t('cms', 'Text')?>","#1da0b6");
      
      $(',.fieldsettitle,.fieldsetdescription').show();
    }); 
    $(".editElement<?= $blockCms['_id'] ?>").click(function() { 

      var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
      var key = $(this).data("key");
      var tplCtx = {};
      var lastIndex = $(this).data("index");
      tplCtx.id = "<?= $blockCms['_id'] ?>"
      tplCtx.collection = "cms";
      tplCtx.path = "content."+(key)+"."+lastIndex;
      var obj = {
        subKey : key,
        icon : $(this).data("icon"),
        title :     $(this).data("title"),
        image :     $(this).data("image"),
        description:    $(this).data("description")
      };
      var activeForm = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Edit content')?>",
          "type" : "object",
          onLoads : {
            onload : function(data){
              $(".parentfinder").css("display","none");
            }
          },
          "properties" : getProperties(obj,lastIndex),
          beforeBuild : function(){
            uploadObj.set("cms","<?= $blockCms['_id'] ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( activeForm.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Modification enregistrer");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }
            }
        }
      };    
      dyFObj.openForm( activeForm );
      $('.fieldsettitle,.fieldsetdescription').show();
    });
    $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
      var deleteObj ={};
      deleteObj.id = $(this).data("id");
      deleteObj.path = $(this).data("path");      
      deleteObj.collection = "cms";
      deleteObj.value = null;
      bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",
        function(result){
          if (!result) {
            return;
          }else {
            dataHelper.path2Value( deleteObj, function(params) {
              mylog.log("deleteObj",params);
              toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
              // urlCtrl.loadByHash(location.hash);
            });
          }
        });
    });
    function getProperties(obj={}, lastIndex, newContent){
      mylog.log("objjjjj",obj);
      var props = {
        "titleIcon":{
           "inputType" : "select",
            "label" : "<?php echo Yii::t('cms', 'Icon')?>",
            "options" : fontAwesome,
            "value" : obj["icon"]
        },
        "titleImage":{
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t('cms', 'Image')?>",
            "docType": "image",
            "contentKey" : "slider",
            "endPoint": "/subKey/block",
            "domElement" : "image",
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : (<?= json_encode($listOfImage) ?>[lastIndex] && <?= json_encode($listOfImage) ?>[lastIndex].length>0)?[<?= json_encode($listOfImage) ?>[lastIndex]]:[]
        },
        "titleBackgroundColor":{
          "inputType" : "colorpicker",
          "label" : "<?php echo Yii::t('cms', 'Background color of the title')?>",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleBackgroundColor
        },
        "titleLink":{
          "inputType" : "text",
          "label" : "<?php echo Yii::t('cms', 'link')?>",
          values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleLink
        },
        "title" : {
          label : "<?php echo Yii::t('cms', 'Title')?>",
          "inputType" : "text",
          value : obj["title"]
        },
        "description" : {
          "inputType" : "textarea",
          "label" : "<?php echo Yii::t('cms', 'Description')?>",
          "markdown" : true,
          value :  obj["description"]
        }   
      };
      return props;
    }
     sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		      "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
          "icon" : "fa-cog",

          "properties" : {
             
          },
          beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();

              if (k == "parent"){
                tplCtx.value[k] = formData.parent;
              }

              if(k == "items"){
                tplCtx.value[k] = data.items;
              }
            });

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Modification enregistré!");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
            }

          }
        }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.subKey = "imgParent";
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
     
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });


</script>