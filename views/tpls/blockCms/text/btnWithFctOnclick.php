<?php 
$keyTpl = "btnWithFctOnclick";
$paramsData = [
	"contentTitle" => "Lorem ipsum dolor sit amet",
	"introduction" => "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
	"btnLabel" => "Créer térritoire",
	"btnDLabelColor" => "#cccccc",
	"btnDBg" => "#e2e2e2",
	"btnDPadding" => "10",
	"btnDBorderRadius" => "2",
	'btnFctOnClick' => "coInterface.optionModalOpen('project');",
	"btnDsize"=> "20"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}

?>
<style type="text/css">
	/*.<?= $kunik?>{
		    display: flex;
		    align-items: center;
		    justify-content: center;
		    min-height: 200px;
	}*/

	.<?= $kunik?> a{
		color: <?= $paramsData["btnDLabelColor"]?>;
		background-color: <?= $paramsData["btnDBg"]?>;
		padding: <?= $paramsData["btnDPadding"]?>px;
		border-radius: <?= $paramsData["btnDBorderRadius"]?>px;
		font-size :<?= $paramsData["btnDsize"]?>px;
		height:inherit;
	}
</style>
	<div class="text-center col-xs-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
		<?php if($paramsData["contentTitle"] != ""){?>
			<h1 class=" title-1 padding-20 text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="contentTitle"> 
				<?php echo $paramsData["contentTitle"]; ?>
			</h1>
		<?php } ?>
		<?php if($paramsData["introduction"] != ""){?>
			<div class="text-center description col-xs-12 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="introduction">
				<?php echo $paramsData["introduction"]; ?>
			</div> 
		<?php } ?>

		<div class="text-center col-xs-12 margin-bottom-10 margin-top-15 <?= $kunik?>">
			<a href="javascript:;" class="create<?= $kunik?>"><?= $paramsData["btnLabel"] ?></a>
		</div>
	</div>	
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$(".create<?= $kunik?>").click(function(){
			<?= $paramsData["btnFctOnClick"] ?>
		});
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		     	"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"icon" : "fa-cog",
				"properties" : {
					"btnLabel" : {
						label : "<?php echo Yii::t('cms', 'Button label')?>",
						inputType :"text",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnLabel
					},
					"btnFctOnClick" :{
						label : "<?php echo Yii::t('cms', 'OnClick function of the button')?>",
						inputType :"text",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnFctOnClick
					},
					"btnDLabelColor" : {
						label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
						inputType:"colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnDLabelColor
					},
					"btnDBg":{
						label : "<?php echo Yii::t('cms', 'Button color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnDBg
					},
					"btnDsize":{
						label: "<?php echo Yii::t('cms', 'Text size')?>(px)",
						inputType :"text",
						values : sectionDyf.<?php echo $kunik?>ParamsData.btnDsize,
						rules:{
							number :true
						}
					},
					"btnDPadding":{
						label:"<?php echo Yii::t('cms', 'Button padding')?>(px)",
						inputType :"text",
						values : sectionDyf.<?php echo $kunik?>ParamsData.btnDPadding,
						rules:{
							number :true
						}
					},
					"btnDBorderRadius":{
						label:"<?php echo Yii::t('cms', 'Border radius')?>(px)",
						inputType :"text",
						values : sectionDyf.<?php echo $kunik?>ParamsData.btnDBorderRadius,
						rules:{
							number :true
						}
					}

				},
				beforeBuild : function(){
		            uploadObj.set("cms","<?php echo $blockKey ?>");
		        },
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                      $("#ajax-modal").modal('hide');
						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	              	}

				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","#000091","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btnD",4,6,null,null,"<?php echo Yii::t('cms', 'Button design')?>","#000091","");
		});

	})
</script>