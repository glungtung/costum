<?php 
$keyTpl = "textCollapseonHover";
$paramsData = [
	"titleWithColor1" => "",
	"titleWithColor2" => "",
	"content" => "Ariane est une méthodologie d’accompagnement personnel et professionnel solide et éprouvée depuis 30 ans.
	Créée et développée par Olivier Ronceray et Maryse Tournon-Ronceray, elle est adaptée :
	- À tout contexte (professionnel ou personnel)
	- À toute situation (des questions quotidiennes, en passant par les problématiques organisationnelles jusqu’à la stratégie d’entreprises)
	- À tout format (individuel ou collectif)
	Grâce à ses outils dynamiques d’aide à l’introspection, à la décision et à la résolution de problème, Ariane permet l’élaboration de solutions et le passage à l’action.
	Ariane possède également une dimension philosophique qui s’appuie sur les théories de la connaissance, des systèmes et de la complexité, tout en restant très pratique, favorisant la mise en œuvre de la créativité collective au service d’objectifs concrets, sans oublier de prendre soin de la richesse humaine.",
	"iconColor"        => "black",
	"iconSize"        => "150"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style type="text/css">
.<?= $kunik?> {
	background: transparent !important;
}
.<?= $kunik?> .panel-body{
}
.title<?= $kunik?> {
	padding-top: 60px;
}
.content-<?= $kunik?> {
	margin-top: -10px
}
.<?= $kunik?> .scroll {
	animation: down 1.5s infinite;
	-webkit-animation: down 1.5s infinite;
}
.<?= $kunik?> .scroll i {
	font-size: <?= $paramsData["iconSize"]; ?>px;
	color: <?= $paramsData["iconColor"];?>;
}

@keyframes down {
	0% {
		transform: translate(0);
	}
	20% {
		transform: translateY(15px);
	}
	40% {
		transform: translate(0);
	}
}
@media (max-width: 978px) {
	.<?= $kunik?> .title<?= $kunik?> h2 .title-1 {
		font-size: 40px !important;
	}
	.<?= $kunik?> .title<?= $kunik?> h2 .title-2 {
		font-size: 40px !important;
	}
	.<?= $kunik?> .scroll i {
		font-size: 100px;
	}
	.<?= $kunik?> #collapseOne .content-<?= $kunik?> .title-4 p{
		font-size: 17px !important;
	}
}


/*.block-container-<?=$kunik?> {
	background-attachment: fixed !important;
}*/
.<?= $kunik?> #collapseOne .content-<?= $kunik?> .panel-body p{
	margin: -3px !important;
}

.collapse-<?= $kunik?> {
    min-height: 245px;
}
</style>
<div class="panel <?= $kunik?> ">
	<div class="wholeHead-<?= $kunik?> ">
		<div class="title<?= $kunik?>">
			<div class="text-center fadeInLeft animated sp-text" data-id="<?= $blockKey ?>"  data-field="titleWithColor1">
				<font class="title-1"> <?= $paramsData["titleWithColor1"]?></font> 
			</div>
		</div>
		<div class="panel-heading no-padding" role="tab" id="headingOne">
			<div class="scroll ">
				<h4 class="panel-title text-center">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor:pointer;">
						<i class="fa fa-angle-down "></i> 
					</a>
				</h4>
			</div>
		</div>
	</div>
	<div id="collapseOne" class="panel-collapse collapse-<?= $kunik?>" role="tabpanel" aria-labelledby="headingOne">
		<div class="content-<?= $kunik?> container">
			<div class="panel-body sp-text" data-id="<?= $blockKey ?>"  data-field="content" style="line-height: 1.2;"><div><?= $paramsData["content"]	?></div></div>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre bloc",
				"description" : "Personnaliser votre bloc",
				"icon" : "fa-cog",
				"properties" : {          
					// "titleWithColor1" : {
					// 	label : "titre avec la premier couleur",
					// 	values :  sectionDyf.<?php echo $kunik?>ParamsData.titleWithColor1
					// },
					// "titleWithColor2" : {
					// 	"label" : "titre avec la deuxième couleur",
					// 	values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleWithColor2
					// },
					// "content" : {
					// 	"label" : "Contenu",
					// 	inputType : "textarea",
					// 	markdown :true,
					// 	values :  sectionDyf.<?php echo $kunik ?>ParamsData.content
					// },
					"iconColor":{
						label : "Couleur de l'icone",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconColor
					},
					"iconSize":{
						label : "Taille de l'icone(px)",
						inputType : "text",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconSize
					},

				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						});
					}
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"icon",6,6,0,null,"Propriété de l'icone","green","");
		});

	});

	$(".collapse-<?= $kunik?>").hide();
	$(".scroll").mouseenter(function() {
		$(".collapse-<?= $kunik?>").show();
		$(".wholeHead-<?= $kunik?> ").hide();
	}
	);

	$(".panel-body ").mouseleave(function() {
		$(".wholeHead-<?= $kunik?> ").show();
		$(".collapse-<?= $kunik?>").hide();
	}
	);

	</script>