<?php 
$keyTpl = "bandeau";
$paramsData = [
  "title" => "UNE NOUVELLE PLATEFORME POUR LA TRANSITION ÉCOLOGIQUE",
  "content"=>"La plateforme des Contrats de transition écologique évolue pour accueillir tout porteur d’un projet de territoire ambitieux en matière de transition écologique. </br> Découvrez des ressources et des partages d’expériences et, sur demande, un accès à des fonctionnalités de gestion de projet."
  
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>

<style type="text/css">  
  #<?= $kunik?> .new-message-box {
    margin: 15px 0;
    padding-left: 20px;
    margin-bottom: 25px!important;
  }
  #<?= $kunik?> .new-message-box p{
    font-size: 18px;
    line-height: 30px;
    font-weight: 600;
    padding-left: 20px
  }
  #<?= $kunik?> .new-message-box h4{
    color:#24284D; 
    font-weight:bold; 
    font-size: 25px; 
    text-align: center;
  }
  #<?= $kunik?> .info-tab {
    width: 40px;
    height: 40px;
    display: inline-block;
    position: relative;
    top: 8px;
  }

  #<?= $kunik?> .info-tab {
    float: left;
    margin-left: -23px;
  }

  #<?= $kunik?> .info-tab i::before {
    width: 24px;
    height: 24px;
    box-shadow: inset 12px 0 13px rgba(0,0,0,0.5);
  }

  #<?= $kunik?> .info-tab i::after {
    width: 0;
    height: 0;
    border: 12px solid transparent;
    border-bottom-color: #fff;
    border-left-color: #fff;
    bottom: -18px;
  }

  #<?= $kunik?> .info-tab i::before, .info-tab i::after {
    content: "";
    display: inline-block;
    position: absolute;
    left: 0;
    bottom: -17px;
    transform: rotateX(60deg);
  }

  #<?= $kunik?> .tip-box-info,  {
    padding: 12px 8px 3px 26px;
  }

  #<?= $kunik?> .new-message-box-info {
    background: #eeeeee;
    padding: 3px;
    margin: 10px 0;
  }

  #<?= $kunik?> .tip-box-info {
    color: #1a1a1a;
  }

  #<?= $kunik?> .tip-icon-info {
    background: #03A9F4;
  }

  #<?= $kunik?> .tip-icon-info::before {
    font-size: 25px;
    content:"\f129";
    top: 8px;
    left: 11px;
    font-family: FontAwesome;
    position: absolute;
    color: white
  }

  #<?= $kunik?> .tip-icon-info i::before {
    background: #03A9F4;
  }

</style>
<div id="<?= $kunik?>">
  <div class="container-fluid ">
    <div class="row">
      <div class="col-xs-12 col-sm-12 ">
        <div class="new-message-box">
          <div class="new-message-box-info">
            <div class="info-tab tip-icon-info" ><i></i></div>
            <div class="tip-box-info"> 
              <h4 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title" ><?= $paramsData["title"]?><br/></h4>
              <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"><?= $paramsData["content"]?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {          
          
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

        }
      }
    };
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
  });
</script>