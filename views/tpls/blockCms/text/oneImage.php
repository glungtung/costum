<?php 
  $keyTpl ="oneImage";
  $paramsData = [ 
    "text" => "Veuillez ajouter un background dans le formulaire d'édition.Puis mettre un texte,une petite image en dessus",
    "textColor" => "white",
    "addOver" => "text",
  ];
 
  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<!-- ****************get image uploaded************** -->
<?php 
   $blockKey = (string)$blockCms["_id"];
  $initImageParent = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'imgParent',
    ), "image"
  );
  $initImageOver = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'imgOver',
    ), "image"
  );
$arrayImg = [];
$arrayImgOver = [];
foreach ($initImageParent as $key => $value) {
  $arrayImg[]= $value["imagePath"];
}
foreach ($initImageOver as $key => $value) {
  $arrayImgOver[]= $value["imagePath"];
}
//$latestImg = Document::getLastImageByKey($blockKey,Cms::COLLECTION,Document::IMG_PROFIL);
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
 ?>
<!-- ****************end get image uploaded************** -->

<style>
  .container<?php echo $kunik ?>{
    padding: 0px !important;
    height: 520px;
  }
  <?php if($paramsData["addOver"]=="aucun"){ ?>
      @media (max-width: 992px ){
          .container<?php echo $kunik ?>{
            height: 400px;
          }
      }
      @media (max-width: 567px ){
          .container<?php echo $kunik ?>{
            height: 300px;
          }
      }
  <?php } ?>
  .container<?php echo $kunik ?> .p-img{
    text-align: center;
    margin: 0px;
    padding: 0px;
  }
.container<?php echo $kunik ?> .p-img img{
  display: inline;
  width: 100%;
}
.container<?php echo $kunik ?> .btn-edit-delete{
  display: none;
}
.container<?php echo $kunik ?>:hover .btn-edit-delete{
  display: block;
  position: absolute;
  top:0%;
  left: 50%;
  transform: translate(-50%,0%);
}
.container<?php echo $kunik ?> .img-over{
  position: absolute;
  top:50%;
  left: 50%;
  transform: translate(-50%,-50%);
  height: 176px;
}
.container<?php echo $kunik ?> .text-over{
    font-size: 50px;
}
.sub-container<?=$kunik ?>{
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  min-height: 500px;
}
@media (max-width: 500px){
  .container<?php echo $kunik ?> .img-over{
    display:none !important;
  }
}
/*@media (max-width: 800px){
  .container<?php //echo $kunik ?> .text-over{
    font-size: 40px;
  }
}
@media (max-width: 600px){
  .container<?php //echo $kunik ?> .text-over{
    font-size: 30px;
  }
}
@media (max-width: 440px){
  .container<?php //echo $kunik ?> .text-over{
    display:none;
  }
}*/
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  <?php if ($paramsData["addOver"]=="text") { ?>
    <div class="sub-container<?=$kunik ?>">
      <div class="description text-over  sp-text " id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text"><?php echo $paramsData["text"]; ?></div>
    </div>
  <?php }elseif ($paramsData["addOver"]=="image") { ?>
  <?php  if (count($initImageOver)!=0) { ?>
        <img class="img-responsive img-over"  src="<?php  echo $initImageOver[0]["imageThumbPath"] ?>">  
  <?php  }else{ ?>
       <img class="img-responsive img-over"  src="<?php  echo $assetsUrl?>/images/blockCmsImg/defaultImg/logo_glaz-02.png">
  <?php  } ?>
 <?php } ?>
</div>
 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
              "addOver" :{
                "inputType" : "select",
                "label" : "<?php echo Yii::t('cms', 'Add Text or Image over it')?>",
                "options":{
                  "text":"Text",
                  "image":"Image",
                  "aucun":"Aucun"
                }
              },
              "imageOver" :{
                "inputType" : "uploader",
                "label" : "<?php echo Yii::t('cms', 'Small image')?>",
                "domElement" : "imageOver",
                "docType": "image",
              "contentKey" : "slider",
                "itemLimit" : 1,
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "showUploadBtn": false,
                "endPoint" :"/subKey/imgOver",
                initList : <?php echo json_encode($initImageOver) ?>
              }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              console.log("save tplCtx",tplCtx);
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          var current = "<?php echo $paramsData["addOver"] ?>";
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);

          if( current == "text")
              $('.imageOveruploader').hide();
          else if(current == "aucun")
              $('.imageOveruploader').hide();

          $("#addOver").change(function(){
            if($(this).val()=="aucun"){
              $('.imageOveruploader').hide();
            }else if($(this).val()=="text"){
                $('.imageOveruploader').hide();
            }else if($(this).val()=="image"){
                $('.imageOveruploader').show();
            }


          })
        });
    });
</script>
