<?php 
$keyTpl = "backgroundPhotoParallax";
$paramsData = [
	"title" => "Lorem Ipsum",
	"sizeTitle"=>"35",	
	"colorTitle"=>"#000",	
	"sizeContent"=>"20",
	"content"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"colorContent"=>"#000",
	"photo"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style type="text/css">
	.contenu_<?= $kunik ?> h2 {
		font-size: <?= $paramsData["sizeTitle"]?>px;
		color: <?= $paramsData["colorTitle"]?>;
	}
	.contenu_<?= $kunik ?> .description {	
		font-size: <?= $paramsData["sizeContent"]?>px ;
		color: <?= $paramsData["colorContent"]?>;
		margin-top: 4%;
		margin-bottom: 5%;
	}
	 
	
	  
	@media (max-width: 414px) {
		.contenu_<?= $kunik ?> h2 {
			font-size:20px;
			margin-top: 36px ;
			margin-bottom: 0px;
		}
		.contenu_<?= $kunik ?> .description {
			font-size: 13px !important;
			margin-bottom: 20px;
			padding: 0;
		}
	}
	@media screen and (min-width: 1500px){
		.contenu_<?=$kunik?> h2{
		    font-size: 40px;
		    line-height: 35px;
		    text-transform: none;
		}
		.contenu_<?=$kunik?> .description{
		    line-height: 35px;
		    font-size: 25px;
		    text-transform: none;
		    /* margin-bottom: -90px; */
	  	}
		.contenu_<?=$kunik?> .description {
			margin-top: 4%;
			margin-bottom: 5%;
		}

	}
</style>

	<div class=" parallax_<?= $kunik?>" >
	<div class="container contenu_<?= $kunik?>">
		<div class="text-center">
			<h2 class="title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h2>

			<div class="description sp-text img-text-bloc " id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"><?= $paramsData["content"]?></div> 
		</div>
	</div>
	
</div>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {	
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
		                      $("#ajax-modal").modal('hide');
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>