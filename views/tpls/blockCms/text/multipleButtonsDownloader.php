<?php 
  $keyTpl ="multipleButtonsDownloader";
  $paramsData = [ 
    "title" => "TÉLECHARGEMENT",
    "multipleThemeColor" => "#f0ad16",
    "multipleBtnColor" => "#f0ad16",
    "multipleBtnHoverColor" => "#ffffff",
    "item" => array()  
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
  //get item last key
  $lastKey = 0;
  $itemHeaderSize = array();
  if(isset($paramsData["item"]) && is_array($paramsData["item"])){
    $itemHeaderSize = array_keys($paramsData["item"]);
    $lastKey = end($itemHeaderSize);
  }

  $screenShoot = "";
  $initPhotos = Document::getListDocumentsWhere(
    array(
      "id"=> (string)$blockCms["_id"],
      "type"=>'cms',
    ), "image"
  );
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
?>
<style>
	.button-container-<?= $kunik ?> #accordion .panel {
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin: 0 0 15px 0px;
	}
	.button-container-<?= $kunik ?> .panel-group {
	    width: 100%;
	}
	.button-container-<?= $kunik ?> #accordion .panel-heading {
		border-radius: 30px;
		padding: 0;
	}
	.button-container-<?= $kunik ?> #accordion .panel-title {
		text-transform: none;
	}
	.button-container-<?= $kunik ?> #accordion .panel-title a {
		background: <?= $paramsData["multipleThemeColor"] ?> ;
		border: 1px solid transparent;
		border-radius: 30px;
		color: #fff;
		display: block;
		font-size: 18px;
		font-weight: 600;
		padding: 12px 20px 12px 50px;
		position: relative;
		transition: all 0.3s ease 0s;
	}
	.button-container-<?= $kunik ?> #accordion .panel-title a.collapsed {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		color: #333;
	}
	.button-container-<?= $kunik ?> #accordion .panel-title a::after, #accordion .panel-title a.collapsed::after {
		background: <?= $paramsData["multipleThemeColor"] ?>;
		border: 1px solid transparent;
		border-radius: 50%;
		box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
		color: #fff;
		content: "";
		font-family: fontawesome;
		font-size: 25px;
		height: 55px;
		left: -20px;
		line-height: 55px;
		position: absolute;
		text-align: center;
		top: -5px;
		transition: all 0.3s ease 0s;
		width: 55px;
	}
	.button-container-<?= $kunik ?> #accordion .panel-title a.collapsed::after {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		box-shadow: none;
		color: #333;
		content: "";
	}
	.button-container-<?= $kunik ?> #accordion .panel-body {
		background: transparent none repeat scroll 0 0;
		border-top: medium none;
		padding: 20px 25px 10px 9px;
		position: relative;
	}
	.button-container-<?= $kunik ?> #accordion .panel-body p {
		border-left: 1px dashed #8c8c8c;
		padding-left: 25px;
	}
	.button-container-<?= $kunik ?> .panel,.button-container-<?= $kunik ?> .panel-group {
		background: transparent !important;
	}
	.button-container-<?= $kunik ?>{
		display: flex;
		flex-direction: column;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
	}
	/*.button-container-<?= $kunik ?> .collapse.in {
	    display: table-column;
	}*/
	.type-container-<?= $kunik ?>{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		align-items: center;
		justify-content: center;
	}
	.download-container-<?= $kunik ?>{
		display: flex;
		flex-direction: column;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		align-items: center;
	}
	.download-container-<?= $kunik ?> .btn{
		width: 350px;
		margin:5px 5px;
		/*line-height: 3.5;		color: white;*/
		background: <?= $paramsData["multipleBtnColor"] ?>;
		white-space: normal;
	}
  	.button-container-<?= $kunik ?> .btn:hover .title-6{
    	color: <?= $paramsData["multipleBtnColor"] ?> !important;
  	}
  	.screenShoot-container-<?= $kunik ?>{
  		display: flex;
  		flex-direction: row;
  		flex-wrap: wrap;
  		align-items: center;
  		justify-content: center;
  	}
  	.screenShoot-<?= $kunik ?>{
  		width:80%;
  		height: 500px;
  		object-fit: contain;
  		object-position: center;
  	}
	.download-container-<?= $kunik ?> .btn:hover{
		color: <?= $paramsData["multipleBtnColor"] ?> !important;
		background: <?= $paramsData["multipleBtnHoverColor"] ?>;
		border:2px solid <?= $paramsData["multipleBtnColor"] ?>;
	}
	.button-container-<?= $kunik ?> .btn-label{
		line-height: 3.1;
    	font-size: 24px;
	}
	@media (max-width: 400px){
		.download-container-<?= $kunik ?> .btn{
			width: 100%;
		}
	}
	@media (max-width: 991px){
		.screenShoot-container-<?= $kunik ?>{
			flex-direction: column;
		}
		.screenShoot-<?= $kunik ?>{
			width: 100%;
			object-fit: contain;
			height: auto;
		}
	}
</style>
<h1 class="title sp-text img-text-bloc  text-center" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"] ?></h1>
<?php if(Authorisation::isInterfaceAdmin()){ ?>
	<p class="text-center">
		<button class="btn btn-success add-link-<?=$kunik ?> hiddenPreview"><?php echo Yii::t('cms', 'Add links')?></button>
	</p>
<?php } ?>
<div class="button-container-<?= $kunik ?>">
	<div class="panel-group" id="accordion">
		<?php $i = 1; foreach ($paramsData["item"] as $kl => $vl) { ?>
		<?php     foreach ($initPhotos as $kis => $vis) {
	      if($vis["subKey"] == "screenShoot".@$kl ){
	        $screenShoot = $vis["imagePath"];
	        $screenShootImgId =  (string)$vis["_id"];
	      }
	    } ?>
		<?php if(!empty($vl["urls"])) { ?>
		 <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a class="title-5" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $i ?>"><?= $vl["dossier"] ?></a>
            <?php if(Authorisation::isInterfaceAdmin()){ ?>
		        <button class='btn btn-primary btn-sm hiddenPreview margin-left-30 edit-link-<?= $kunik ?>' data-content='<?= json_encode($vl); ?>' data-id="<?= $kl ?>"><i class="fa fa-edit"></i></button>
		        <button class='btn btn-danger btn-sm hiddenPreview delete-link-<?= $kunik ?>' data-id="<?= $kl ?>"><i class="fa fa-trash"></i></button>
            <?php } ?>
		      </h4>
		    </div>
		    <div id="collapse<?= $i ?>" class="panel-collapse collapse in">
		      <div class="panel-body">
		      		<div class="download-container-<?= $kunik ?>">
							<?php foreach ($vl["urls"] as $kurls => $vurls) { ?>
								<?php if(isset($vurls["link"]) && isset($vurls["title"])){ ?>
									<a  class="title-6 btn ribbon btn-lg" href="<?= $vurls["link"] ?>" target="_blank">
						                	<?= $vurls["title"] ?> <i class="fa fa-download fa-2x" style="float: right;"></i>
						            </a>
								<?php } ?>
							<?php } ?>
					</div>
					<?php $img1 = (!empty($screenShoot) && isset($screenShoot)) ? $screenShoot : "" ?>
					<?php if (!empty($img1)){ ?>
					<div class="screenShoot-container-<?= $kunik ?>">
						
						<a style="display: contents;" href="<?= $img1 ?>" data-lightbox="all">
							<img class="screenShoot-<?= $kunik ?>" src="<?= $img1 ?>" loading="lazy" alt="">
						</a>
					</div>
					<?php } ?>
		      </div>
		    </div>
		</div>
		<?php } ?>
		<?php $i++; } ?>
	</div>
</div>
<script>
	  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	  $(function(){
      	var headerItem = <?php echo json_encode($paramsData["item"]) ?>;
      	var initPhotos<?= $kunik ?> = <?= json_encode($initPhotos); ?>;
		mylog.log('headeritem',headerItem["item"]);
	      sectionDyf.<?php echo $kunik ?>Params = {
	        "jsonSchema" : {    
          	"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		      "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
	          "icon" : "fa-cog",
	          "properties" : {
	              multipleThemeColor:{
	              	inputType:"colorpicker",
	              	label:"<?php echo Yii::t('cms', 'Theme color')?>",
	              	value: <?= json_encode($paramsData["multipleThemeColor"]) ?>
	              },
	              multipleBtnColor:{
	              	inputType:"colorpicker",
	              	label:"<?php echo Yii::t('cms', 'Button color')?>",
	              	value: <?= json_encode($paramsData["multipleBtnColor"]) ?>
	              },
	              multipleBtnHoverColor:{
	              	inputType:"colorpicker",
	              	label:"<?php echo Yii::t('cms', 'Button color on hover')?>",
	              	value: <?= json_encode($paramsData["multipleBtnHoverColor"]) ?>
	              }
	          },
	          beforeBuild : function(){
	              uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
	          },
	          save : function (data) {  
	            tplCtx.value = {};
	            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	              tplCtx.value[k] = $("#"+k).val();
	              if (k == "parent")
	                tplCtx.value[k] = formData.parent;
	            });
	            console.log("save tplCtx",tplCtx);

	            if(typeof tplCtx.value == "undefined")
	              toastr.error('value cannot be empty!');
	              else {
	                dataHelper.path2Value( tplCtx, function(params) {
	                  dyFObj.commonAfterSave(params,function(){
	                    toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                    $("#ajax-modal").modal('hide');
						var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    // urlCtrl.loadByHash(location.hash);
	                  });
	                } );
	              }

	          }
	        }
	      };


	      $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
	        tplCtx.id = $(this).data("id");
	        tplCtx.collection = $(this).data("collection");
	        tplCtx.path = "allToRoot";
	        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"multiple",4,12,null,null,"<?php echo Yii::t('cms', 'Themes')?>","blue","");
	      });

	       $('.add-link-<?=$kunik ?>').off().on('click',function(){
	         <?= $kunik ?>afficheDynForm();
	      })
	      $('.edit-link-<?= $kunik ?>').off().on('click',function(){
	        var id = $(this).data('id');
	        var content = $(this).data('content');
	        var screenShoot = [];
	       	$.each(initPhotos<?= $kunik ?>,function(k,v){
	        	if(v.subKey == "screenShoot"+id)
	            	screenShoot.push(v);
	        });
	        <?= $kunik ?>afficheDynForm(id,content,screenShoot);
	      })
	      $('.delete-link-<?= $kunik ?>').off().on('click',function(){
	          tplCtx.id = "<?= (string)$blockCms["_id"] ?>";
	          tplCtx.collection = "cms";
	          tplCtx.path =  "item."+$(this).data('id') ;
	          tplCtx.value = null;
	          bootbox.confirm(trad["areyousuretodelete"], function(result){ 
	              if(result)
	                dataHelper.path2Value( tplCtx, function(params) {
	                    toastr.success("Suppression réussie");
						var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    // urlCtrl.loadByHash(location.hash);
	                });
	          });
	      });


	      function <?= $kunik ?>afficheDynForm(id=null,content=null,screenShoot=null){
	        var headerItem<?= $kunik ?> = {
	            "jsonSchema" : {    
	              "title" : notNull(id) ? "<?php echo Yii::t('cms', 'Modify links')?>" : "<?php echo Yii::t('cms', 'Add links')?>",
	              "description" : "AJOUTER DES URLS",
	              "icon" : "fa-cog",
	              "properties" : {
		                "dossier" : {
		                    "inputType" : "text",
		                    "label" : "<?php echo Yii::t('cms', 'Name of the folder')?>",    
		                },
	                  	"screenShoot" : {
	                      	"inputType" : "uploader",
	                      	"label" : "<?php echo Yii::t('cms', 'Image')?>",
	                      	"domElement" : "screenShoot",
	                      	"docType": "image",
	                      	"contentKey" : "slider",
	                      	"itemLimit" : 1,
	                      	"filetypes": ["jpeg", "jpg", "gif", "png"],
	                      	"showUploadBtn": false,
	                      	"endPoint" : notNull(id) ? "/subKey/screenShoot"+id : "/subKey/screenShoot<?= $lastKey+1 ?>"
                       	},
			          	"urls" : {
	                    	"label" : "<?php echo Yii::t('cms', 'Urls')?>",
	                    	"inputType" : "lists",
	                    	"entries":{
	                        	"key":{
	                            	"type":"hidden",
	                            	"class":""
	                        	},
		                        "title":{
		                            "label":"<?php echo Yii::t('cms', 'Title')?>",
		                            "type":"text",
		                            "class":"col-md-5 col-sm-5 col-xs-10"
		                        },
		                        "link":{
		                            "label":"<?php echo Yii::t('cms', 'Link')?>",
		                            "type":"text",
		                            "class":"col-md-5 col-sm-5 col-xs-10"
		                        }
	                    	}
	                  	}
	              },
	              beforeBuild : function(){
	                  uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
	              },
	              save : function (data) {  
	                tplCtx.id = "<?= (string)$blockCms["_id"] ?>";
	                tplCtx.collection = "cms";
	                tplCtx.path =  notNull(id) ? "item."+id : "item.<?= $lastKey+1 ?>";
	                tplCtx.value = {};
	                $.each(headerItem<?= $kunik ?>.jsonSchema.properties , function(k,val) { 
	                  tplCtx.value[k] = $("#"+k).val();
	                  if (k == "parent")
	                    tplCtx.value[k] = formData.parent;
	                  if(k == "urls")
	                  	 tplCtx.value[k] = data.urls;
	                });
	                console.log("save tplCtx",tplCtx);

	                if(typeof tplCtx.value == "undefined")
	                  toastr.error('value cannot be empty!');
	                  else {
	                    dataHelper.path2Value( tplCtx, function(params) {
	                      dyFObj.commonAfterSave(params,function(){
	                        toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                        $("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                        // urlCtrl.loadByHash(location.hash);
	                      });
	                    } );
	                  }

	              }
	            }
	          };
	          if(notNull(screenShoot)){
	              headerItem<?= $kunik ?>.jsonSchema.properties.screenShoot["initList"] = screenShoot;
	          }
	          if(notNull(id) && notNull(content))
	          	dyFObj.openForm(headerItem<?= $kunik ?>,null,content);
	          else
	          	dyFObj.openForm(headerItem<?= $kunik ?>);
	      }
	  });
</script>