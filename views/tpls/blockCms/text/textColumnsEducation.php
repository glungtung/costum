<?php 
$keyTpl = "textColumnsEducation";
$paramsData=[
	"title"=>"Je suis Admin",
	"title2"=>"Je suis etudiant",
	"content1"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"content2"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry",	
	"logo1"=>"",
	"logo2"=>""
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
$formId1 = array_keys($form)[0];
$formId2 = isset(array_keys($form)[1]) ? array_keys($form)[1] : "";
/*echo "<pre>";
var_dump(array_keys($form)[0]);
echo "</pre>";*/
/*
echo "<pre>";
var_dump($me);
echo "</pre>";*/

    # If user has submited the coform
$student_answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId1 ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));
$student_answer = isset(array_keys($student_answers)[0])? array_keys($student_answers)[0] : "";

$staff_answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId2 ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));
$staff_answer = isset(array_keys($staff_answers)[0])? array_keys($staff_answers)[0] : "";

$student_answered = false;
if(count($student_answers)!=0){
	$student_answered = true;
}

$staff_answered = false;
if(count($staff_answers)!=0){
	$staff_answered = true;
}

?>
<?php 
$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'logo1',
    ), "image"
  );
$initImage2 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'logo2',
    ), "image"
  );
?>
<style type="text/css">

	.rounded-<?= $kunik?>{
		border-radius: 20px;
		padding: 20px;
		width: 80%;
		background-color: #039eb6;
	}

	.rounded-<?= $kunik?> p, button{
		color: #ffffff;
		font-family: 'Circular-Loom' !important;
		font-size: 25px;
	}
	.rounded-<?= $kunik?> img{
		max-width: 200px;
		cursor: pointer;
	}
	.centered{ 
		margin-left: auto;
		margin-right: auto;
		width: 50%
		padding: 10px;
	}
	.btn-<?= $kunik?>{
		background-color: #fdd767;
		color: #61605c !important;
		border-radius: 50px;
		box-shadow: 0px 0px 10px 0px #00000087;
		padding-left: 30px;
		padding-right: 30px;
	}
</style>
<div class="container">
	<div class="row">
			<div class="col-md-12">
				<div class="col-md-6 col-sm-6 padding-25">
					<div class="fadeInUp animated rounded-<?= $kunik?>">		
						<?php if(!$staff_answered){ ?>
						<a style="text-decoration: none;" href="#answer.index.id.new.form.<?= $formId2; ?>" class="lbh">
						<p class="text-center markdown" style="cursor: pointer;" ><?= $paramsData["title"]?></p>	
							<p class="text-center">
								<i class="icon fadeInUp animated-2 text-center">
									<?php if (!empty(Document::getLastImageByKey($blockKey, "cms", "profil","logo2"))){ ?>
										<img src="<?php echo Document::getLastImageByKey($blockKey, "cms", "profil","logo2"); ?>">
									<?php }else { ?>
										<img width="400px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/AUTRE-02.svg">
									<?php } ?>

								</i>		
							</p>
						</a>	

						<?php }else{ ?>
							
						<a style="text-decoration: none;" href="#answer.index.id.<?= $staff_answer; ?>.mode.r" class="lbh">
							<p class="text-center markdown" style="cursor: pointer;"><?= $paramsData["title"]?></p>	
							<p class="text-center">
								<i class="icon fadeInUp animated-2 text-center">
									<?php if (!empty(Document::getLastImageByKey($blockKey, "cms", "profil","logo2"))){ ?>
										<img src="<?php echo Document::getLastImageByKey($blockKey, "cms", "profil","logo2"); ?>">
									<?php }else { ?>
										<img width="400px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/AUTRE-02.svg">
									<?php } ?>

								</i>		
							</p>
						</a>	

						<?php } ?>						
						<p class="text-center">
							<button class="btn text-center btn-<?= $kunik?>" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapseExample">
								Voir plus
							</button>
						</p>

						<div class="collapse" id="collapse1">
							<div class="">
								<p class="markdown description text-left"><?= $paramsData["content1"]?></p>
							</div>
						</div>						
					</div>
				</div>
				<div class="col-md-6 col-sm-6 padding-25">
					<div class="fadeInUp animated rounded-<?= $kunik?>">
						<?php if(!$student_answered){ ?>

						<a style="text-decoration: none;" href="#answer.index.id.new.form.<?= $formId1; ?>" class="lbh">
							<p class="text-center markdown" style="cursor: pointer;"><?= $paramsData["title2"]?></p>	
							<p class="text-center">
								<i class="icon fadeInUp animated-2 text-center">
									<?php if (!empty(Document::getLastImageByKey($blockKey, "cms", "profil","logo2"))){ ?>
										<img src="<?php echo Document::getLastImageByKey($blockKey, "cms", "profil","logo2"); ?>">
									<?php }else { ?>
										<img width="400px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/AUTRE-02.svg">
									<?php } ?>

								</i>		
							</p>
						</a>	

						<?php }else{ ?>
							
						<a style="text-decoration: none;" href="#answer.index.id.<?= $student_answer; ?>.mode.r" class="lbh">
							<p class="text-center markdown" style="cursor: pointer;"><?= $paramsData["title2"]?></p>	
							<p class="text-center">
								<i class="icon fadeInUp animated-2 text-center">
									<?php if (!empty(Document::getLastImageByKey($blockKey, "cms", "profil","logo2"))){ ?>
										<img src="<?php echo Document::getLastImageByKey($blockKey, "cms", "profil","logo2"); ?>">
									<?php }else { ?>
										<img width="400px" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/AUTRE-02.svg">
									<?php } ?>

								</i>		
							</p>
						</a>	

						<?php } ?>					
						<p class="text-center">
							<button class="btn btn-<?= $kunik?> text-center" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapseExample">
								Voir plus
							</button>
						</p>

						<div class="collapse" id="collapse2">
							<div class="">
								<p class="markdown description"><?= $paramsData["content2"]?></p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

</div>
<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
					
					"title" : {
						label : "titre",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					
					"content1" : {
						label : "Contenu1",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content1
					},
					"logo1" : {
						"inputType" : "uploader",
						"label" : "photo 1",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
							"contentKey" : "slider",
						"domElement" : "logo1",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						//"template" : "qq-template-manual-trigger",
						"endPoint" : "/subKey/logo1",
						"filetypes" : [
							"png","jpg","jpeg","gif"
						],
                        initList : <?php echo json_encode($initImage1) ?>
					},
					
					"title2" : {
						label : "Titre 2",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.title
					},
					
					"content2" : {
						label : "Contenu 2",
						"inputType" : "textarea",
						"markdown" : true,
						values :  sectionDyf.<?php echo $kunik?>ParamsData.content2
					},
					"logo2" : {
						"inputType" : "uploader",
						"label" : "photo 2",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"domElement" : "logo2",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						//"template" : "qq-template-manual-trigger",
						"endPoint" : "/subKey/logo2",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
                        initList : <?php echo json_encode($initImage2) ?>
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
		                      $("#ajax-modal").modal('hide');
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	});


</script>
<!-- "#search" : {
            "hash" : "#app.search",
            "subdomainName" : "CARTO",
            "urlExtra" : "page/search",
            "filterObj" : "costum.views.custom.university.filters",
            "img" : "/images/university/icon/carto.svg",
            "icon" : "map-marker",
            "showMap" : true,
            "placeholderMainSearch" : "Map",
            "filters" : {
                "types" : [ 
                    "organizations", 
                    "events"
                ]
            }
        },
        "#filiere" : {
            "hash" : "#app.search",
            "icon" : "search",
            "filterObj" : "costum.views.custom.university.filters",
            "urlExtra" : "/page/filiere",
            "img" : "/images/coeurEducation/icon/annuaire.svg",
            "subdomainName" : "ANNUAIRE",
            "placeholderMainSearch" : "what are you looking for ?",
            "useFilter" : true,
            "useFooter" : true,
            "filters" : {
                "types" : [ 
                    "organizations", 
                    "citoyens"
                ]
            },
            "searchObject" : {
                "indexStep" : "0"
            }
        },
        "#Etudiant" : {
            "hash" : "#app.search",
            "icon" : "search",
            "filterObj" : "costum.views.custom.university.filters",
            "urlExtra" : "/page/Etudiant",
            "subdomainName" : "Tous les acteurs étudiants",
            "placeholderMainSearch" : "what are you looking for ?",
            "useHeader" : false,
            "useFilter" : true,
            "useFooter" : true,
            "filters" : {
                "types" : [ 
                    "organizations"
                ]
            },
            "searchObject" : {
                "indexStep" : "0"
            }
        },
        "#live" : {
            "subdomainName" : "ACTUS",
            "slug" : "university",
            "formCreate" : false,
            "useFilter" : false,
            "viewMode" : "list"
        },
        "#agenda" : {
            "subdomainName" : "AGENDA",
            "nameMenuTop" : "Agenda"
        },
        "#projects" : {
            "hash" : "#app.search",
            "filterObj" : "costum.views.custom.eywa.filters",
            "urlExtra" : "/page/projects",
            "subdomainName" : "Projects",
            "icon" : "lightbulb-o",
            "img" : "",
            "placeholderMainSearch" : "",
            "useFooter" : "true",
            "useFilter" : "true",
            "filters" : {
                "types" : [ 
                    "projects"
                ]
            },
            "searchObject" : {
                "indexStep" : "10"
            }
        } 



        costum htmlConstruct

        "menuLeft" : {
            "addClass" : "bg-default align-middle",
            "buttonList" : {
                "app" : {
                    "buttonList" : {
                        "#filiere" : true,
                        "#live" : true,
                        "#agenda" : true,
                        "#search" : true
                    }
                },
                "add" : true
            }
        },

        "header" : {
            "menuTop" : {
                "left" : {
                    "buttonList" : {
                        "searchBar" : {
                            "class" : "pull-left margin-top-5 margin-left-15 hidden-xs"
                        }
                    }
                },
                "right" : {
                    "addClass" : "margin-top-5",
                    "buttonList" : {
                        "login" : true,
                        "userProfil" : {
                            "img" : true,
                            "name" : true,
                            "dashboard" : true
                        }
                    }
                }
            }
        },
        -->