<?php 
  $keyTpl ="descriptionWithSimpleTitle";
  $paramsData = [ 
    "title" => "Lorem ipsum",
    "titleColor" => "black",
    "titleAlign" => "center",
    "descriptions" => "L'Homme et l'Environnement a voulu reveler le défi de montrer que lutter contre la pauvreté et préservation de l'environnement peuvent aller de pair.",
    "descriptionColor" => "black",
    "descriptionAlign" => "center"/*,
    "background" => "transparent",
    "backgroundAlign" => "center"*/
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
          if($e=="description")
              $paramsData[$e] = array_merge($paramsData[$e],$blockCms[$e]);
          else
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<style>
  @font-face {backgroundPhotoParallax
    font-family: "ReenieBeanie";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/ReenieBeanie-Regular.ttf");
  }
  @font-face {
    font-family: "Oswald";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/Oswald-VariableFont_wght.ttf");
  }
  @font-face {
    font-family: "JosefinSansRegular";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/JosefinSans-Regular.ttf");
  }
  @font-face {
    font-family: "JosefinSansLight";
    src: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/font/glazBijoux/JosefinSans-Light.ttf");
  }
  .ReenieBeanie{
    font-family: "ReenieBeanie";
  }
  .Oswald{
    font-family: Oswald;
  }
  .JosefinSansRegular{
    font-family: JosefinSansRegular;
  }
  .JosefinSansLight{
    font-family: JosefinSansLight;
  }
 /* .container<?php// echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }*/
  .title<?php echo $kunik ?>{
    text-transform: none !important;
    text-align: <?php echo $paramsData["titleAlign"] ?> !important;
    color:<?php echo $paramsData["titleColor"] ?> !important;
    margin-bottom: 30px;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    text-align: <?php echo $paramsData["descriptionAlign"] ?>;
    font-size:23px;
    color:<?php echo $paramsData["descriptionColor"] ?>;
  }
  .container<?php echo $kunik ?> {
    padding-top: 30px;
  }
  hr.<?php echo $kunik ?> {
    border: 2px solid;
    border-color: <?php echo $paramsData["titleColor"] ?> ;
    display: block;
    width: 30%;
    margin: 0 auto!important;
    
}
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  
  <h2 class="title title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
    <?php echo $paramsData["title"] ?>
  </h2> 
  <div class="description item<?php echo $kunik ?> JosefinSansLight sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="descriptions"><?php echo $paramsData["descriptions"] ?></div>
  
  <hr class="<?php echo $kunik ?>">
</div>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		     	  "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {     
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

