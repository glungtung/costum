<?php 
	if(!isset($costum)){
		$costum = CacheHelper::getCostum();
	}
	
	$keyTpl = "communityWithBadge";
	$myCmsId     = $blockCms["_id"]->{'$id'};
	
	$paramsData = [
		"thematicPreference" => [],
		"elementsType" => [Organization::COLLECTION, Citoyen::COLLECTION, Project::COLLECTION],
		"filtersWith" => [],
		"blockTitle" => "COMMUNAUTÉ AVEC LEURS BADGES",
		"headerBg" => "transparent",
		"buttonBg" => "#c4d552ff",
		"buttonAddonBg" => "#E16B37",
		"badgeBg" => "#D0D0D0",
		"badgeColor" => "black",
		"showRole" => false,
		"newRole" => "Financeur,Partenaire,Sponsor,Organisateur,Président,Directeur,Conférencier,Intervenant",
		"assignRole" => "",
		"showRoleCard" => false,
		"alignHead" => false,
		"buttonUnderInfo" => false,
		"assignRoleBg" => "#2C3E50",
		"AssignBadgeBg" => "#2C3E50",

	];

	if(isset($blockCms)){
  		foreach($paramsData as $e => $v) {
    		if( isset($blockCms[$e]) ) {
      			$paramsData[$e] = $blockCms[$e];
    		}
  		}
	}
?>

<style type="text/css">
	.member<?= $kunik ?>{
		position: relative;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-direction: column;
		min-width: 0;
		word-wrap: break-word;
		background-color: #fff;
		background-clip: border-box;
		border: 1px solid rgba(0, 0, 0, .125);
		border-radius: 15px;
		box-shadow: 3px 4px 5px 3px rgb(0 0 0 / 20%);
		margin-bottom: 20px;
		margin-left: 0px;
		margin-right: 0px;
	}
	.member<?= $kunik ?> button{    
		font-size: 12px !important;
		padding: 2px;
		font-weight: bold;
		margin-top: 5px;
	}
	.member<?= $kunik ?> .contentImg{
		padding: 18px;
	}

	.memberImg<?= $kunik ?>{
		border-radius: 50%;
		width: 100px;
		height: 100px;
		background-color: transparent;
		object-fit: cover;
		object-position: center;
	}
	.icon<?= $kunik ?>{
		width: 30px;
		height: 30px;
		font-size: 12px;
		line-height: 28px;
		text-align: center;
		border-radius: 5px 0 21px 0;
		border-right: 2px solid #fff;
		border-bottom: 2px solid #fff;
		color: #fff;
	}
	.member<?= $kunik ?> .contentBagde{
		margin-bottom: 5px;
	}
	.member<?= $kunik ?> .badge{
		font-size: 11px;
		margin-right: 2px;
		font-weight: 100;
		padding: 2px 7px 2px 3px;
		white-space: normal !important;
		margin-bottom: 2px;
		max-width: 200px;
		background-color: <?= $paramsData["badgeBg"] ?>;
		color: <?= $paramsData["badgeColor"] ?>;
		border-radius: 40px !important;
		text-align: left !important;
	}

	.member<?= $kunik ?> .detail-box h4 {
		font-size: 20px;
		text-transform: none;
		line-height: 32px;
		color: #2C3E50;
		padding-top: 10px;
		margin-bottom: 5px;
	} 

	.rounded<?= $kunik ?>{
        border-radius: 40px;
        border: 2px solid #ddd;
    }

    /*.btn-assign-badge<?= $kunik ?>{
    	position: absolute;
		top: 90%;
		left: 0;
    }*/

    .display-1{
    	font-size: 50pt;
    	color: #999;
    	padding: 1em;
    	border-radius: 50%;
    	background-color: #ddd;
    }

    .textNoCommunity<?= $kunik ?>{
    	color: #999;
    	line-height: 1.5;
    }

    .communityHeader<?= $kunik ?>{
    	background-color: <?= $paramsData["headerBg"] ?>;
    }
	.communityHeader<?=$kunik?> #filterContainerInside .dropdown .btn-menu, 
	div#filters-nav .dropdown .btn-menu, 
	div#filters-nav .filters-btn, 
	#content-admin-panel .filterContainer .dropdown .btn-menu {
		font-size: 16px;
		text-decoration: none;
		border-radius: 4px !important;
		/*box-shadow: inset 0 -3px 0 0 #2D328D!important;*/
		background-color: #f0f0f0!important;
		color: #1e1e1e!important;
		border: 1px solid #f0f0f0!important;
		padding: 5px 10px;
	}
	.communityHeader<?=$kunik?> #filterContainerInside #input-sec-search, 
	.communityHeader<?=$kunik?> #filterContainerInside #input-sec-search {
		display: grid;
		margin-bottom: 5px;
		margin-right: 10px;
		float: left;
	}
	.communityHeader<?=$kunik?> #filterContainerInside .dropdown, .communityHeader<?=$kunik?> #filterContainerInside .dropdown {
		float: left;
		display: grid;
		height: 40px;
		margin-bottom: 5px;
		margin-right: 10px;
	}	
	.communityHeader<?=$kunik?> #filterContainerInside .dropdown, 
	.communityHeader<?=$kunik?> #filterContainerInside .search-bar {
		font-size: 18px;
		/*box-shadow: inset 0 -3px 0 0 #2D328D;*/
		border-radius: 4px !important;
		background-color: #f0f0f0;
		margin-top: 0px;
		color: #1e1e1e;
		border: 1px solid #f0f0f0 !important;
	}
	.communityHeader<?=$kunik?> #filterContainerInside .main-search-bar-addon{
        border: 1px solid #dadada !important;
        background-color: <?= $paramsData["buttonAddonBg"] ?> !important;
        color: #fff!important;
        font-size: 14px;
        height: 40px;
        padding: 11px 15px;
        border-radius: 4px !important;
        border-left: 0px !important;
        margin-top: 0px;
		margin-left: -4px;
    }

	.communityHeader<?=$kunik?> #filterContainerInside .searchBar-filters {
		margin-right: 10px !important;
	}

	.communityHeader<?=$kunik?> #filterContainerInside #input-sec-search .input-group, 
	.communityHeader<?=$kunik?> #filterContainerInside #input-sec-search .input-group {
		display: inline-flex;
		margin: 4px 15px 0 0 !important;
	}

	.communityHeader<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu .list-filters {
		max-height: 300px;
		overflow-y: scroll;
	}

	.communityHeader<?=$kunik?> #filterContainerInside  .dropdown .dropdown-menu {
		position: absolute;
		overflow-y: visible !important;
		top: 50px;
		left: 5px;
		width: 250px;
		border-radius: 2px;
		border: 1px solid #2D328D;
		padding: 0px;
	}
	.communityHeader<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu button {
		border: none;
		background: white;
		border-bottom: 1px solid #2D328D!important;
		color: black;
		font-weight: 100 ;
		text-align: left;
		border-radius: 0;
		font-size: 14px !important;
		padding: 5px;
		padding-left: 15px;
	}
	.communityHeader<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu:before,
	.communityHeader<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu:after {
		bottom: 100%;
		margin-top: -20px;
		border: solid transparent;
		content: " ";
		height: 0px;
    	border-bottom-color: #2D328D;
		left: 19px;
		width: 0;
		border-width: 11px;
		position: absolute;
	}
	.communityWithBadges<?= $kunik ?> .divEndOfresults:not(.communityWithBadges<?= $kunik ?> .divEndOfresults.searchEntityContainer) {
		display: none;
	}
    .communityHeader<?= $kunik ?> .container-filters-menu {
        min-height: 20px;
        padding: 19px;
        margin-bottom: 20px;
        /*background-color: #f5f5f5;
        border: 1px solid #e3e3e3;*/
        border-radius: 4px;
        width: auto;
    }
	@media screen and (max-width: 767px){
		.searchBar-filters{
			display: none;
		}
		.searchBarInMenu #main-search-bar-addon, #show-filters-xs {
			background-color: #2D328D !important;
			color: #fff;
		}
		.container-filters-menu {
			background: #fff!important;
		}
		.menu-filters-xs{
			padding-top: 5px;
    		box-shadow: 0 2px 5px 0 rgb(23 47 145), 0 2px 10px 0 rgb(23 47 145);
		}
		.count-result-xs{
			text-align: left;
		}
		.communityHeader<?=$kunik?> #filterContainerInside .dropdown{
			width: 100% !important;
		}
	}

	.searchBarInMenu #main-search-bar{
		font-size: 16px;
		box-shadow: inset 0 -3px 0 0 #2D328D;
		background-color: #f0f0f0;
		color: #1e1e1e;
		border: 1px solid #f0f0f0!important;
	}
	.headerSearchright{
		display: none;
	}

	.communityWithBadges<?= $kunik ?> .headerSearchContainer{
		display:none !important;
	}

	#activeFilters .filters-activate{
		background-color: <?= $paramsData["buttonBg"] ?>;
		margin-top: 4px;
	}

	#invitebtn<?= $kunik ?>{
		background: <?= $paramsData["buttonBg"] ?>
	}

	#Rolebtn<?= $kunik ?>{
		background: <?= $paramsData["buttonBg"] ?>
	}

	.membercommunityWithBadge<?= $blockKey ?> {
		box-shadow: 3px 4px 5px 3px rgb(0 0 0 / 05%) !important;
	}

	.roleContent {
		margin-left: 0;
		color: black;
		background: #d0d0d0;
		padding: 0px 6px;
		transform: skewX(-15deg);
		flex-wrap: wrap;
		font-size: 9pt;
		border-radius: 4px;
	}

	.btnAndFiltersContent {
		display: flex;
		justify-content: space-between;
		margin: 0px 5px;
	}

	.headContentButton {
		padding: 19px;
		margin-bottom: 20px;
	}

	.card-img-top {
		padding-left: 0px;
	}

	#btnAssignRole<?= $blockKey ?> {
		background: <?= $paramsData["buttonBg"] ?>
	}

	#btnAssignBadge<?= $blockKey ?> {
		background: <?= $paramsData["buttonBg"] ?>
	}

</style>
<?php 
$badgeMouv = PHDB::find("badges",["source.keys"=>$costum["slug"]]);
$badges = [];
foreach($badgeMouv as $kb =>$vb ){
	$badges[$kb] = $vb["name"];
}
?>
<div class="text-center padding-top-10 padding-bottom-20" style="background:<?= $paramsData["headerBg"] ?>">
	<h2 class="sp-text padding-bottom-20" data-id="<?= $blockKey ?>" data-field="blockTitle"><?= $paramsData["blockTitle"] ?></h2>
	<!-- //**align */ -->
	<?php if ($paramsData['alignHead']) { ?>
		<div class="btnAndFiltersContent">
			<div class=" communityWithBadges<?= $kunik ?> communityHeader<?= $kunik ?>">
				<div id="filters<?= $kunik ?>" class="padding-top-20 paddig-right-20 text-center padding-left-20 bg-light"></div>
				<div id="matchedBadges<?= $kunik ?>" class="padding-top-20 paddig-right-20 text-center padding-left-20 bg-light"></div>
			</div>
			<div class="headContentButton">
				<?php if(Authorisation::isInterfaceAdmin()){ ?>	
					<a id="invitebtn<?= $kunik ?>" href="#element.invite.type.<?= $costum["contextType"] ?>.id.<?= $costum["contextId"] ?>" class="btn lbhp btn-primary margin-bottom-20 margin-right-10 " data-placement="bottom" data-original-title="Inviter un membre"><i class="fa fa-user-plus"></i> INVITER DE MEMBRE</span></a>
					<?php if ($paramsData['showRole']) {?> 
						<a id="Rolebtn<?= $kunik ?>" onclick="createRole('<?= $costum['contextId'] ?>', '<?= $costum['contextType'] ?>')" class="btn lbhp btn-primary margin-bottom-20" ><i class="fa fa-suitcase"></i>  AJOUTER RÔLE </a>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	<?php } else { ?>
		<div class="headContentButton">
				<?php if(Authorisation::isInterfaceAdmin()){ ?>	
					<a id="invitebtn<?= $kunik ?>" href="#element.invite.type.<?= $costum["contextType"] ?>.id.<?= $costum["contextId"] ?>" class="btn lbhp btn-primary margin-bottom-20 margin-right-10 " data-placement="bottom" data-original-title="Inviter un membre"><i class="fa fa-user-plus"></i> INVITER DE MEMBRE</span></a>
					<?php if ($paramsData['showRole']) {?> 
						<a id="Rolebtn<?= $kunik ?>" onclick="createRole('<?= $costum['contextId'] ?>', '<?= $costum['contextType'] ?>')" class="btn lbhp btn-primary margin-bottom-20" ><i class="fa fa-suitcase"></i> AJOUTER RÔLE </a>
					<?php } ?>
				<?php } ?>
			</div>
			<div class=" communityWithBadges<?= $kunik ?> communityHeader<?= $kunik ?>">
				<div id="filters<?= $kunik ?>" class="padding-top-20 paddig-right-20 text-center padding-left-20 bg-light"></div>
				<div id="matchedBadges<?= $kunik ?>" class="padding-top-20 paddig-right-20 text-center padding-left-20 bg-light"></div>
			</div>
	<?php } ?>	
</div>

<div class='headerSearch<?= $kunik ?> no-padding col-xs-12'></div>
<div class="communityWithBadges<?= $kunik ?>">
	<div class="headerSearchContainer">
		<div class="headerSearchleft"></div>
	</div>
	<div class="container-fluid">
		<div id="communities<?= $kunik ?>" class="col-xs-12 no-padding"></div>
	</div>

	<div class="portfolio-modal modal fade in" id="modal-finder" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;">
	    <div class="modal-content padding-top-15">
	        <div class="close-modal" data-dismiss="modal">
	            <div class="lr">
	                <div class="rl">
	                </div>
	            </div>
	        </div>
	        
	        <div class="container">
	            <div id="badge-finder-content" style="height: 100%; width: 100%"></div>
	        </div>
	    </div>
	</div>
</div>

<script type="text/javascript">
	let currentElement = {};
	let badgeMouv = <?= json_encode($badges)?>;
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	if(!userId){
		$(".inviteMembers<?= $kunik ?>").hide();
	}

	$(function(){
		let communityWithBadgeObj<?= $kunik ?> = {
			currentElement: {},
			badges: {}, // badges from community data (without repeat)
			badgesWithCategories:{}, // badges data
			matchedBadges: {}, // the matched match from filters
			activeFilters: {},
			toFilterWith: sectionDyf.<?php echo $kunik ?>ParamsData.filtersWith,
			filtersWith: [],
			filterSearch:{},

			init:function(cwb){
				cwb.filters(cwb);
			},

			filters:function(cwb){
				let params = {
					notSourceKey:true,
			        searchType : <?= json_encode($paramsData["elementsType"]) ?>,
			        fields : ["name","tags", "type", "profilImageUrl", "links", "badges", "address"],
			        indexStep: "0",
			        filters:{
			        	'$or': cwb.activeFilters
			        }
			    }
				if(costum.contextType == "organizations"){
					params.filters["links.memberOf."+costum.contextId] = {'$exists':true};
					params.filters["links.memberOf."+costum.contextId+".isInviting"] = {'$exists':false};

				}
				else if(costum.contextType == "projects"){
					params.filters["links.projects."+costum.contextId] = {'$exists':true};
					params.filters["links.projects."+costum.contextId+".isInviting"] = {'$exists':false};

				}

				cwb.getCommunityWithBadge(cwb, params);
			},

			getBadges: function(cwb){
				if(Object.keys(cwb.badgesWithCategories).length==0){
					ajaxPost(
						null,
						baseUrl+"/costum/tierslieuxgenerique/getbyids",
						{ids: Object.keys(cwb.badges), collection:"badges", fields:["name", "profilImageUrl"]},
						function(data){
							if(data.results){
								cwb.badgesWithCategories = data.results;
								cwb.updateBadgeImages(cwb);
							}
						}
					)
				}else{
					cwb.updateBadgeImages(cwb)
				}
			},

			getCommunityWithBadge:function(cwb, params){
				$(".noCommunity<?= $kunik ?>").hide();
				//$(".communityWithBadges<?= $kunik ?>").hide();
				var badge = {
					"formateur" : {
						label: "formateur Mouv’outremer",
						field : "badges.631ad900f852a3aed80ae439",
						value : true,
					}
				}
				var paramsFilter = {
					container : ".communityHeader<?= $kunik ?>",
					
					loadEvent : {
						default : "scroll"
					},
					header: {
						dom: ".headerSearch<?= $kunik ?>",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding',
								group: {
									count: true,
									types: true
								}
							}
						},
					},
					results :{
						dom : '#communities<?= $kunik ?>',
						smartGrid:true,
						renderView : "directory.communityWithBadgeHtml"
					},
					defaults : {
						notSourceKey:true,
						fields :  ["name","tags", "type", "profilImageUrl", "links", "badges", "address"],
						types : <?= json_encode($paramsData["elementsType"]) ?>,
						filters :{

						}
					},
					filters : {
						text:{
							placeholder :"Que recherchez vous ?"
						},
					}
				};

				if(sectionDyf.<?php echo $kunik ?>ParamsData.filtersWith.includes("types")){
					paramsFilter.filters["types"] = {
							lists : ["projects", "citoyens"]
						}
				}

				// add costum lists in filter
				if(typeof costum.lists != "undefined" && costum.lists.zone){
					paramsFilter.filters["zone"] = {
						view : "dropdownList",
						type : "filters",						
						name : "zone", 
						event : "filters",
						field: "zone",
						list : costum.lists.zone
					}
				}

				if(sectionDyf.<?php echo $kunik ?>ParamsData.filtersWith.includes("territoire")){
					paramsFilter.filters["scopeList"] = {
						name : "Territoire",
						params : {
							countryCode : ["GY", "PF", "MQ", "PM", "YT", "RE", "GP", "NC", "NW"], 
							level : ["1"],
							sortBy:"name"
						}
					};
				}

				if(typeof costum.lists != "undefined" && costum.lists.secteur){
					paramsFilter.filters["themes"] = {
						label: "Secteurs :",
						lists: <?php echo json_encode(CO2::getContextList("filliaireCategories")); ?>
					}
				}

				if(typeof costum.lists != "undefined" && costum.lists.zero){
					paramsFilter.filters["zero"] = {
						view : "dropdownList",
						type : "filters",						
						name : "zero", 
						event : "filters",
						field: "zero",
						list : costum.lists.zero
					}
				}

				if(typeof costum.lists != "undefined" && costum.lists.profil){
					paramsFilter.filters["profil"] = {
						view : "dropdownList",
						type : "filters",						
						name : "profil", 
						event : "exists",
						field: "profil",
						list : costum.lists.profil
					}
				}

				paramsFilter.filters["badge"] ={
							view : "dropdownList",
				            type : "filters",
				            action : "filters",
				            typeList : "object",							
							name : "Badge", 
				            event : "exists",
							value : true,
	 						list : {}
						}

				if(typeof costum.lists != "undefined" && costum.lists.thematic){
					paramsFilter.filters["thematic"] = {
						view : "dropdownList",
						type : "filters",						
						name : "Thématique", 
						event : "filters",
						field: "tags",
						list : costum.lists.thematic
					}
				}

				$.each(badgeMouv, function(i, v){ 
					paramsFilter.filters.badge.list[v] = {
						label: v,
						field : "badges."+i,
						value : true,
					}
				});
				if(costum.contextType == "organizations"){
					paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {'$exists':true};
					paramsFilter.defaults.filters["links.memberOf."+costum.contextId+".isInviting"] = {'$exists':false};

				}else if(costum.contextType == "projects"){
					paramsFilter.defaults.filters["links.projects."+costum.contextId] = {'$exists':true};
					paramsFilter.defaults.filters["links.projects."+costum.contextId+".isInviting"] = {'$exists':false};

				}
				// paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {'$exists':true};
				// paramsFilter.defaults.filters["links.memberOf."+costum.contextId+".isInviting"] = {'$exists':false};
				
				cwb.filterSearch = searchObj.init(paramsFilter);
				cwb.filterSearch.search.callBack = function(fobj, results){
					searchObj.search.callBack(fobj, results);
					cwb.getBadges(cwb);
				}
				cwb.filterSearch.search.init(cwb.filterSearch, 0);
				if(cwb.filterSearch.results.count.citoyens == 0 || cwb.filterSearch.results.count.organizations == 0){
					$(".noCommunity<?= $kunik ?>").show();
				}
			},

			updateBadgeImages : function(cwb){
				$.each(cwb.badgesWithCategories, function(id, badge){
					mylog.log("ID BADGES", id, badge);
					//$("#bname"+id).text(badge.name);
					$("."+id).each(function(){
						if(badge.profilImageUrl){
							$(this).attr("src", badge.profilImageUrl);
						}else{
							$(this).replaceWith("<i class='fa fa-bookmark margin-right-5 padding-5'></i> ");
						}
					})
				})
			}
		}

		directory.communityWithBadgeHtml = function(element){
			var str ='';
			let badgeHtml = "";
			let btnAssignBadgeHtml = "";
			let localityHtml = "";
			let tobj = {};
			let rindex = element.id;
			let memberRole = "";
			if (typeof element.links != 'undefined' && typeof element.links[costum.contextType]!= 'undefined' 
					&& typeof element.links[costum.contextType][costum.contextId] != 'undefined'
					&& typeof element.links[costum.contextType][costum.contextId].roles != 'undefined') { 
				memberRole = element.links[costum.contextType][costum.contextId].roles;
			}
			if(typeObj[element.collection].icon){
				tobj["icon"] = typeObj[element.collection].icon;
				tobj["color"] = typeObj[element.collection].color
			}else{
				const sameAs = typeObj[element.collection].sameAs;
				tobj["icon"] = typeObj[sameAs].icon;
				tobj["color"] = typeObj[sameAs].color
			}
			// *1
			<?php if (!$paramsData['buttonUnderInfo']) { ?>
				if(costum.isCostumAdmin){
					btnAssignBadgeHtml=`
						<button id="btnAssignRole<?= $blockKey ?>" class="btn btn-primary btn-assign-badge<?= $kunik ?>" data-id="${rindex}" data-name="${element.name}" data-type="${element.collection}" data-profileurl="${element.profilImageUrl}">Lui assigner un badge</button>
						<?php if ($paramsData['showRole']) {?> 
							<button id="btnAssignBadge<?= $blockKey ?>" class="btn btn-primary" onclick="assignRole('${rindex}','${element.collection}','${element.name}','contributors','${memberRole}')">Assigner un rôle</button>	
						<?php } ?>
					`;
				}
			<?php } ?>
			// *1
			if(typeof element.address != "undefined" && (element.address.addressLocality || element.address.level1Name || element.address.level3Name)){
				localityHtml+= `<div class="entityLocality margin-bottom-5"><span class="fa fa-map-marker"></span> ${element.address.addressLocality} - ${element.address.level1Name}</div>`;
			}
			if(element.badges){
				badgeHtml+= "<div class='contentBagde'>";
				$.each(element.badges, function(index, ebadge){
					var badgeImg = '';
					if(index.length==24){
						communityWithBadgeObj<?= $kunik ?>.badges[index]=ebadge.name;
						badgeImg = '<img class="margin-right-5 '+index+'" height="28" width="28" style="border-radius:50%"/>';
					}else{
						badgeImg = "<i class='fa fa-bookmark margin-right-5 padding-5'></i> ";
					}
					badgeHtml += `<span title="${ebadge.name}" style="display:inline-flex" class='badge badge${index}'>${badgeImg} <span class="margin-top-5 margin-bottom-5">${ebadge.name.substring(0, 40)+(ebadge.name.length>40?"...":"")}</span></span>`;
				});
				badgeHtml+= "</div>";
			}
			<?php if ($paramsData['showRoleCard']) {?>
				if (typeof memberRole !== "undefined") {
					var roleImg = '<img class="margin-right-5 " height="28" width="28" style="border-radius:50%"/>';
					badgeHtml+= "<div class='contentRole'>";
					$.each(memberRole, function(index, role){
						badgeHtml += `<span title="" style="display:inline-flex" class='roleContent'>${roleImg} <span class="margin-top-5 margin-bottom-5">${role.toUpperCase()}</span></span>
						`;
					});
					badgeHtml+= "</div>";
				}
			<?php } ?>
			
			<?php if ($paramsData['buttonUnderInfo']) { ?>
				badgeHtml+= "<div class='contentButton margin-bottom-10'>";		
					badgeHtml += `
							<button id="btnAssignRole<?= $blockKey ?>" class="btn btn-primary btn-assign-badge<?= $kunik ?>" data-id="${rindex}" data-name="${element.name}" data-type="${element.collection}" data-profileurl="${element.profilImageUrl}">Lui assigner un badge</button>
							<?php if ($paramsData['showRole']) {?> 
								<button id="btnAssignBadge<?= $blockKey ?>" class="btn btn-primary" onclick="assignRole('${rindex}','${element.collection}','${element.name}','contributors','${memberRole}')">Assigner un rôle</button>	
							<?php } ?>
					`;
				badgeHtml+= "</div>";
			<?php } ?>

			str +=`
			<div id="entity_${element.collection}_${element.id}" class="detailBadgeCard searchEntityContainer smartgrid-slide-element mt-20 col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="member<?= $kunik ?> card text-center">
					<div class=" col-lg-4 col-md-4 col-sm-3 col-xs-3 card-img-top">
                		<div>
							<div class="entityCenter" style="position: absolute;">
								<span><i class="fa fa-${tobj["icon"]} icon<?= $kunik ?>" style='background-color:${tobj["color"]}; color:white'></i></i></span>
							</div>					
							<div class="contentImg">	
								<img height="100px" width="100px" class="memberImg<?= $kunik ?>" src="${(typeof element.profilImageUrl!="undefined")?element.profilImageUrl:defaultImage||""}" />
								${btnAssignBadgeHtml}
							</div>
						</div>
					</div>
					<div class="card-body text-left col-lg-8 col-md-8 col-sm-9 col-xs-9">
						<div class="detail-box ">
							<a href="#page.type.${element.collection}.id.${rindex}"  class="lbh-preview-element"> 
								<h4 id="bname${rindex}">${element.name} </h4>
							</a>
						</div>			
						${localityHtml}
						${badgeHtml}
					</div>
				</div>
			</div>`;
			
			$(document).off("click", ".btn-assign-badge<?= $kunik ?>").on('click', ".btn-assign-badge<?= $kunik ?>", function(e){
				e.preventDefault();
				currentElement = {id:$(this).data("id"), type:$(this).data("type"), name: $(this).data("name"), profileUrl:$(this).data("profileurl")};
				openBadgeFinder(event);
			});
			return str;
		}

		communityWithBadgeObj<?= $kunik ?>.init(communityWithBadgeObj<?= $kunik ?>);

		if(costum && !costum.lists){
			costum["lists"] = {};
		}

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {
                "elementsType" :{
                    "inputType" : "selectMultiple",
					"isSelect2":true,
                    "label" : "Elément du map",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.elementsType,
                    "class" : "multi-select select2 form-control padding-0",
                    "options" : {
                        "organizations" : trad.organizations,
                        "citoyens" : tradCategory.citizen,
						"projects" : tradCategory.projects
                    }
                },
                "filtersWith" : {
                    "inputType" : "selectMultiple",
					"isSelect2":true,
					"class":"multi-select select2 form-control padding-0",
                    "label" : "Ajouter de filtre sur : ",
                    "options" : Object.keys(costum.lists).reduce((a,b)=> (a[b]=b,a),{"badges":"badges", "types":"types", "territoire":"territoires"})
                },
                "headerBg" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du fond du titre : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.headerBg
                },
				"buttonBg" : {
                    "inputType" : "colorpicker",
                    "label" : "Fond de bouton et filtre active : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.buttonBg
                },
				"buttonAddonBg" : {
                    "inputType" : "colorpicker",
                    "label" : "Fond du bouton recherche : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.buttonAddonBg
                },
				"badgeBg" : {
                    "inputType" : "colorpicker",
                    "label" : "Fond de badge : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.badgeBg
                },
				"badgeColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur de texte de badge : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.badgeColor
                },
				"assignRoleBg" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du boutton d'assignement de rôle : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.assignRoleBg
                },
				"AssignBadgeBg" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur du boutton d'assignement de badge : ",
                    "options" : sectionDyf.<?php echo $kunik ?>ParamsData.AssignBadgeBg
                },
				"showRole" : {
					"label" : "Afficher role : ",
					"inputType" : "checkboxSimple",
					"params" : checkboxSimpleParams,
				},
				"showRoleCard" : {
					"label" : "Montrer le rôle sur la carte ",
					"inputType" : "checkboxSimple",
					"params" : checkboxSimpleParams,
				},
				"alignHead" : {
					"label" : "Aligner les boutons et le filtre : ",
					"inputType" : "checkboxSimple",
					"params" : checkboxSimpleParams,
				},
				"buttonUnderInfo" : {
					"label" : "bouton en bas des informations : ",
					"inputType" : "checkboxSimple",
					"params" : checkboxSimpleParams,
				}
				
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            afterBuild : function(){
                if(sectionDyf.<?php echo $kunik?>ParamsData.legendShow!="true"){
                    $(".legendConfiglists").hide();
                    $(".legendCibleselect").hide();
                }
                $("#legendCible").on("change", function(){
                    sectionDyf.<?php echo $kunik ?>ParamsData.legendCible = $(this).val();
                    if($(this).val()!=""){
                        sectionDyf.<?= $kunik ?>ParamsData.legendConfig = [];
                    }
                    mapLegende<?= $kunik ?>(sectionDyf.<?= $kunik ?>ParamsData.legendConfig);
                    dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
                    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
                    alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"legend",6,12,null,null,"Préférence sur le légende","green","");
                });
            },
            save : function (data) {
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
                if (k == "legendConfig"){
                    tplCtx.value[k] = [];
                    $.each(data.legendConfig, function(index, va){
                        tplCtx.value[k].push(va);
                    })
                }
              });
              if(typeof tplCtx.value == "undefined"){
                toastr.error('value cannot be empty!');
              }else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }
            }
          }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btnAnnuaire",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"legend",6,12,null,null,"Préférence sur le légende","green","");
        });
    });

	function createRole (id, collection) {
		let roleParams = sectionDyf.<?php echo $kunik ?>Params;
		roleParams['jsonSchema']['properties'] = {
			"newRole" : {
				"label" : "Ajouter un rôle",
				"inputType" : "tags"
			}
		}

		roleParams['jsonSchema']['title'] = "Ajouter un ou plusieurs rôles"

		tplCtx.id = id;
		tplCtx.collection = collection;
		tplCtx.path = "allToRoot";
		dyFObj.openForm( roleParams,null, sectionDyf.<?php echo $kunik ?>ParamsData);
	}

	function assignRole (childId, childType, childName, connectType, roles) {	
		links.updateRoles(childId, childType, childName, connectType, roles)
	}

	function openBadgeFinder(event) {
        coInterface.showLoader("#badge-finder-content");
        getAjax("#badge-finder-content", baseUrl+'/costum/tierslieuxgenerique/badgefinder/email/false/source/true', null, 'html');
        $("#modal-finder").modal("show")
    }

    function handleButtonAssign(event, badgeId){
		let defaultValue = {};

		defaultValue[currentElement.id] = {"type":currentElement.type, "name":currentElement.name, "profilThumbImageUrl":currentElement.profileUrl};

        dyFObj.openForm("assignbadge", null, {badgeId: badgeId, award:defaultValue}, null, {
        	afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                openBadgeFinder();
            })
        });
    }


	if (costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ; 
		communityInput = {configTabs : {
                general : [
                    {
                        type: "backgroundColor",
                        options: {
                            name: "buttonBg",
                            label: tradCms.background+" des bouttons",
                        },
                    },
                    ],
                style: [

                ]
        }};
		cmsConstructor.blocks.communityWithBadge =  communityInput;
    }

</script>