<?php 
$keyTpl = "partners_Logo_Slider";
$paramsData=[
	"title" => "Our  Partners or Clients",
];


if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}

 $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

    HtmlHelper::registerCssAndScriptsFiles(["/js/blockcms/slick.js"], $assetsUrl);
?>
<style type="text/css">
	.block_<?= $kunik?> h2{
	  padding: 20px;
	}
	.block_<?= $kunik?> section {
	    padding: 20px 0;
	}
	.customer-logos .slide h5{
		text-transform: none;
	    box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
	    min-height: 60px;
	    vertical-align: baseline;
	    display: flex;
	    justify-content: center;
	    align-items: center;
	    text-align: center;
	}
	/* Slider */

	.slick-slide {
	    margin: 0px 20px;
	}

	.slick-slide img {
	    width: 100%;
	}

	.slick-slider
	{
	    position: relative;
	    display: block;
	    box-sizing: border-box;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	            user-select: none;
	    -webkit-touch-callout: none;
	    -khtml-user-select: none;
	    -ms-touch-action: pan-y;
	        touch-action: pan-y;
	    -webkit-tap-highlight-color: transparent;
	}

	.slick-list
	{
	    position: relative;
	    display: block;
	    overflow: hidden;
	    margin: 0;
	    padding: 0;
	}
	.slick-list:focus
	{
	    outline: none;
	}
	.slick-list.dragging
	{
	    cursor: pointer;
	    cursor: hand;
	}

	.slick-slider .slick-track,
	.slick-slider .slick-list
	{
	    -webkit-transform: translate3d(0, 0, 0);
	       -moz-transform: translate3d(0, 0, 0);
	        -ms-transform: translate3d(0, 0, 0);
	         -o-transform: translate3d(0, 0, 0);
	            transform: translate3d(0, 0, 0);
	}

	.slick-track
	{
	    position: relative;
	    top: 0;
	    left: 0;
	    display: block;
	}
	.slick-track:before,
	.slick-track:after
	{
	    display: table;
	    content: '';
	}
	.slick-track:after
	{
	    clear: both;
	}
	.slick-loading .slick-track
	{
	    visibility: hidden;
	}

	.slick-slide
	{
	    display: none;
	    float: left;
	    height: 100%;
	    min-height: 1px;
	}
	[dir='rtl'] .slick-slide
	{
	    float: right;
	}
	.slick-slide img
	{
	    display: block;
	}
	.slick-slide.slick-loading img
	{
	    display: none;
	}
	.slick-slide.dragging img
	{
	    pointer-events: none;
	}
	.slick-initialized .slick-slide
	{
	    display: block;
	}
	.slick-loading .slick-slide
	{
	    visibility: hidden;
	}
	.slick-vertical .slick-slide
	{
	    display: block;
	    height: auto;
	    border: 1px solid transparent;
	}
	.slick-arrow.slick-hidden {
	    display: none;
	}
	
</style>

<div class="block_<?= $kunik?>">
	<div class="container">
	  <h2  class="sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h2>
	  <?php
		$partners =Element::getCommunityByTypeAndId(Organization::COLLECTION, $costum["contextId"], Organization::COLLECTION);
		$partner = array();
		foreach ($partners as $keyElt => $valueElt) {
			array_push($partner, Element::getElementSimpleById($keyElt,Organization::COLLECTION,null,["name", "profilMediumImageUrl","collection"]));
		}
		if (!empty($partner)) {
			?>
		   <section class="customer-logos slider">
		   		<?php
				foreach ($partner as $key => $value) { ?>
					<div class="slide">
						<?php if (empty($value["profilMediumImageUrl"]) || $value["profilMediumImageUrl"]== null ) {?>
							<h5><?=  $value["name"];?></h5>
						<?php
						}else{
							$img = Yii::app()->request->baseUrl.$value["profilMediumImageUrl"];
						?>
                        <a href="#page.type.<?= $value["collection"]?>.id.<?= (String) $value["_id"] ?>" class="lbh-preview-element">
						    <img src="<?=  $img ?>" class="part img-responsive">
                        </a>
						<?php
					}
					?>
					</div>
				<?php } ?>
		   </section>
		   <?php
		 }else { ?>
			<div class="text-center padding-30">
                <?php echo Yii::t('cms', 'No partner')?>
			</div>
		<?php }	?>
	   
	</div>
</div>


<script type="text/javascript">
	 sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$('.customer-logos').slick({
	        slidesToShow: 6,
	        slidesToScroll: 1,
	        autoplay: true,
	        autoplaySpeed: 1500,
	        arrows: false,
	        dots: false,
	        pauseOnHover: true,
	        responsive: [{
	            breakpoint: 768,
	            settings: {
	                slidesToShow: 4
	            }
	        }, {
	            breakpoint: 520,
	            settings: {
	                slidesToShow: 3
	            }
	        }]
	    });
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					"title" : {
						label : "<?php echo Yii::t('cms', 'Title')?>",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
					},
					"colorTitle":{
						label : "<?php echo Yii::t('cms', 'Title color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					}
				},
	            beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
                }
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
