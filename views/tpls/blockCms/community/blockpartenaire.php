<?php 
$keyTpl = "blockpartenaire";
$paramsData=[
	"title" => "Block partenaires",
];


if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style type="text/css">
	.partenaire_<?= $kunik?> .blockpartenaire{
		margin-top: 20px;
		padding-left: 40px;
	}
	.partenaire_<?= $kunik?> {
		min-height: 150px;
	}
	.partenaire_<?= $kunik?> .part{
		max-height: 120px;
		max-width: 120px;
		margin-bottom: 10px;
	}
	@media (max-width: 978px) {
		.partenaire_<?= $kunik?> .part{
			max-height: 80px;
			max-width: 80px;
			margin-bottom: 10px;
		}
		.partenaire_<?= $kunik?> .blockpartenaire{
			margin-top: 20px;
			padding-left: 0px;
		}
		.partenaire_<?= $kunik?> .blockpartenaire h4{			
			font-size: 20px;
		}
	}	
	.partenaire_<?= $kunik?> h4{
		font-size: 30px;
		margin-bottom: 20px;
	}
	
</style>

<div class=" partenaire_<?= $kunik?>" >
	<h4 class="sp-text img-text-bloc text-center" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
	 	<?php echo $paramsData["title"]?>
	</h4>
	<?php
	$partenaires = Element::getCommunityByTypeAndId(Organization::COLLECTION, $costum["contextId"], Organization::COLLECTION);
	$partenaire = array();
	foreach ($partenaires as $keyElt => $valueElt) {
		array_push($partenaire, Element::getElementSimpleById($keyElt,Organization::COLLECTION,null,["name", "profilMediumImageUrl"]));
	}
	if (!empty($partenaire)) {
		?>
		<div class="text-center">
			<?php
			foreach ($partenaire as $key => $value) {
				if (empty($value["profilMediumImageUrl"]) || $value["profilMediumImageUrl"]== null ) {?>
					<?=  $value["name"];?>
					<?php
				}else{
					$img = Yii::app()->request->baseUrl.$value["profilMediumImageUrl"];
					?>
					<img src="<?=  $img ?>" class="part img-responsive">
					<?php
				}
				?>

			<?php } ?>
		</div>
		<?php
	}else { ?>
		<div class="text-center">
            <?php echo Yii::t('cms', 'No partner')?>
		</div>
	<?php }	?>

</div>

<script type="text/javascript">
	 sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					
				},
				beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
				save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }
                }
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
