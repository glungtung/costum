<?php 
  $keyTpl ="e_community_caroussel";

  $paramsData = [ 
    "title" => "ACTEURS ET COLLABORATEURS",
    "hidde"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
 <?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style>
  .caroussel-container<?= $kunik?> .swiper-container {
    width: 100%;
    height: auto;
    margin-left: auto;
    margin-right: auto;
    box-shadow: 0 15px 30px rgb(0 0 0 / 20%);
    border-bottom-right-radius: 15px;
    border-bottom-left-radius: 15px;
    padding: 45px 0px;
    background-color: white;
    overflow: visible;
  }
  .caroussel-container<?= $kunik?> .hand{
      position: absolute;
      right: -131px;
      bottom: -137px;
      width: 50%;
  }

  .caroussel-container<?= $kunik?> .swiper-slide {
    background-size: cover;
    background-position: center;
  }
  .caroussel-container<?= $kunik?> .swiper-slide:hover .fa-eye{
    display: inline-block;
  }
  .caroussel-container<?= $kunik?> .swiper-slide .fa-eye{
    display: none;
    padding: 0 5px 0 5px;
    background: rgb(0,0,0,0.6);
    color: white;
    border-radius: 0 0 16px 0;
    font-size: 24px;
  }
  .caroussel-container<?= $kunik?> .swiper-slide .img-container{
    position: absolute;
    height: 185px;
    width: 185px;
    border-radius: 50%;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .caroussel-container<?= $kunik?> .swiper-slide-active .img-container{
    border: 11px solid #5C5D5E !important;
  }
  .caroussel-container<?= $kunik?> .swiper-slide-next .img-container{
    border: 11px solid #008037 !important;
  }
  .caroussel-container<?= $kunik?> .swiper-slide .img-container{
    border: 11px solid #f77d0d;
  }
  .caroussel-container<?= $kunik?> .swiper-slide .inner{
    position: relative;
    height: 200px;
    width: 200px;
    margin: 10px auto;
    border-radius: 50%;
    background-size: initial;
    background-position: center;
    border: 4px solid #008037;
  }

  .caroussel-container<?= $kunik?> .title{
    margin-bottom: 15px;
  }
  .caroussel-container<?= $kunik?> .description{
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3; /* number of lines to show */
    -webkit-box-orient: vertical;
    font-size: 13px;
  }
  /*.caroussel-container<?= $kunik?> .swiper-slide{
    padding-right: 55px;
    padding-left: 55px;
  }
  @media (max-width: 765px ){
    .caroussel-container<?= $kunik?> .swiper-slide{
      padding-right: 5px;
      padding-left: 5px;
    }
  }*/

  .caroussel-container<?= $kunik?> .caroussel-container{
    position: relative;
    height: auto;
    width: auto;
  }
</style>
  <!-- <img class="hand" src="<?= $assetsUrl ?>/images/blockCmsImg/defaultImg/mains-02.png" alt=""> -->
<h2 class="title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?php echo $paramsData["title"] ?> </h2>
<div class="caroussel-container caroussel-container<?= $kunik?>">
  <div class="swiper-container">
    <div class="swiper-wrapper"></div>
     <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
  <img class="hand" src="<?= $assetsUrl ?>/images/blockCmsImg/defaultImg/mains-02.png" alt="">
</div>


<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
              "icon" : "fa-cog",
            
            "properties" : {
                    
            },
              beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        /***********CAROUSSEL**************************************/
      
      var linkContextType = links.connectType[costum.contextType];
      getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/"+linkContextType,
        function(data){
            mylog.log("andrana acteurs-collab",data);
            var html = "";
              $.each(data,function(k,v){
                html += 
                `<div class="swiper-slide">
                    <div class="inner">
                      <div class="img-container" style="background-image:url(${v.profilMediumImageUrl})"></div>
                    </div>
                    <div class="info">
                      <h3 class="text-center">
                        <a class="subtitle lbh" href="#page.type.${v.collection}.id.${k}"> ${v.name}</a>
                      </h3>
                      <h5 class="other">${exists(v.rolesLink)? v.rolesLink.join(",") : ""}</h5>
                      <p class="description">${exists(v.shortDescription) ? v.shortDescription : ""}</p>
                    </div>
                </div>`;
              })

              $('.caroussel-container<?= $kunik?> .swiper-wrapper').html(html);
              var swiper = new Swiper(".caroussel-container<?= $kunik?> .swiper-container", {
                slidesPerView: 1,
                spaceBetween: 0,
                // init: false,
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                keyboard: {
                  enabled: true,
                },
               /* autoplay: {
                  delay: 2500,
                  disableOnInteraction: false,
                },*/
                breakpoints: {
                  640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                  },
                  768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                  },
                  1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                  },
                }
              });
              coInterface.bindLBHLinks();
        },
    "");
        //***************END CAROUSSEL***********************************/
  });

</script>