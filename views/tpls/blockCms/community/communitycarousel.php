<?php

$keyTpl = "communitycarousel";

$paramsData = [ 
    "title"         => "communautée en carousel",
    "roles"         =>  "",
    "icon"          =>  "",
    "color"         => "#000000",
    "background"    => "#FFFFFF",
    "isCostum"      => false
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style>
    #community-caroussel-<?= $kunik?>{
        height : 690px;
    }
    #community-caroussel-<?= $kunik?> .container{
       margin-top: 1%;
        width: 70%;
    }
    #community-caroussel-<?= $kunik?> .border{
        background: white;
        border-radius: 10px 0px;
        padding: 1%;
        box-shadow: 0px 0px 2px 1px #e1dbdb;
        font-size: 3em;
        padding-left: 7%;
        padding-right: 7%;
    }

    #community-caroussel-<?= $kunik?> .p-mobile{
        font-size: 2vw;
    }

    #community-caroussel-<?= $kunik?> .arrow-f{
        font-size : 6rem;
    }

    #community-caroussel-<?= $kunik?> a:link{
        text-decoration:none;
    } 
    @media (max-width:768px){
        #community-caroussel-<?= $kunik?> .carousel-inner{
            height : 200px;
        }
        #community-caroussel-<?= $kunik?> .arrow-f{
            font-size : 3rem;
        }
        #community-caroussel-<?= $kunik?> .border{
            font-size: 20px;
        }
        
    }
   
</style>
<div id="community-caroussel-<?= $kunik?>" class="communityCms-caroussel" style="margin-top:2%;">
    <h1 class="text-center" style="background-color: <?= $paramsData['background']; ?> ;color:<?= $paramsData['color']; ?>"> 
        <i class="fa <?= $paramsData['icon'] ?>"></i> 
        <?= $paramsData["title"]; ?> 
    </h1>
    
    <div id="carousel"  class="carousel slide" data-ride="carousel">
        <?php 
        if(!empty($costum["contextType"]) && !empty($costum["contextId"])){
            $data = Element::getCommunityByTypeAndId($costum["contextType"], $costum["contextId"], "organizations", null, null ,null);
            if($data){
                ?>
                <div class="carousel-inner">
                    <?php
                    foreach($data as $k => $v){
                        $element[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilRealBannerUrl","links","slug"));
                    }
                    end($element);
                    $lk = key($element);
                    foreach($element as $key => $value){ 
                        $bannerImg = (!empty($value["profilRealBannerUrl"])) ? "/ph".$value["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/templateCostum/banner.jpeg"; ?>
                        <div class="<?php if($key === $lk) echo "item active"; else echo "item"; ?>">
                            <div class="col-xs-12 text-center" style="position : absolute; margin-top: 2%">
                                <span class="border">
                                    <?php if($paramsData["isCostum"] == "true") { ?>
                                        <a href="<?= Yii::app()->createUrl('/costum/co/index/slug/'.$value['slug']); ?>" target="_blank" class="text-decoration-none">  
                                            <?= $value["name"]; ?>
                                        </a>

                                    <?php }else {?>
                                        <a href="#@<?= $value["slug"]; ?>" class="lbh text-decoration-none">
                                            <?= $value["name"]; ?>                       
                                        </a>
                                    <?php } ?>
                                </span>

                                <div class="container" >
                                    <div class="col-xs-3">
                                        <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/acteur_icone.svg">
                                        <span class="p-mobile text-white">
                                            <?= count(@$value["links"]["members"]); ?> <br> Acteur(s)
                                        </span>
                                        <a href="#@<?= $value["slug"]; ?>.view.directory.dir.members" target="_blank">
                                            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg">
                                        </a>
                                        
                                    </div>
                                    <div class="col-xs-3">
                                        <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/agenda_icone.svg">
                                        <span class="p-mobile text-white">
                                            <?= count(@$value["links"]["projects"]); ?><br> Projet(s)
                                        </span>
                                        <a href="#@<?= $value["slug"]; ?>.view.directory.dir.projects" target="_blank">
                                            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/actu_icone.svg">
                                        <span class="p-mobile text-white">
                                            <?= @PHDB::count(News::COLLECTION,array("type" => "news","source.key" => $value["slug"])); ?>
                                            <br> Actu(s)
                                        </span>
                                        <a href="#@<?= $value["slug"]; ?>.view.directory.dir.newspaper" target="_blank">
                                            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/projet_icone.svg">
                                        <span class="p-mobile text-white">
                                            <?= @PHDB::count(Event::COLLECTION,array("source.key" => $value["slug"])); ?><br> Évènement(s)
                                        </span>
                                         <a href="#@<?= $value["slug"]; ?>.view.directory.dir.events" target="_blank">
                                            <img src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <img src="<?= $bannerImg; ?>"  alt="<?= $value["name"]; ?>" style="width:100%">
                        </div>
                    <?php } ?>
                </div>
                    <!-- ARROW -->
                <?php if(count($element) > 1) { ?>
                    <a class="left carousel-control" href="#carousel" data-slide="prev">
                        <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only"><?php echo Yii::t('cms', 'Previous')?></span>
                    </a>
                    <a class="right carousel-control" href="#carousel" data-slide="next">
                        <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only"><?php echo Yii::t('cms', 'Next')?></span>
                    </a> 
                <?php } ?>
    </div>
    <?php   }
        } else echo "<div class='container'>".Yii::t('cms', 'You haven\'t created a community yet')."</div>";
    ?>
</div>
<script type="text/javascript">
    tplCtx = {};
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        label : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    "roles" : {
                        label : "<?php echo Yii::t('common', 'Roles')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.roles
                    },
                    icon : { 
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        inputType : "select",
                        options : <?= json_encode(Cms::$icones); ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                    },
                    color : {
                        label : "<?php echo Yii::t('cms', 'Title color')?>",
                        "inputType" : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                    },
                    background : {
                        label : "<?php echo Yii::t('cms', 'Background color of the title')?>",
                        "inputType" : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                    },
                    isCostum : {
                        "inputType" : "checkboxSimple",
                        "label" : "<?php echo Yii::t('common', 'Enable redirection to a costum')?>",
                        "params" : {
                            "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                            "offText" : "<?php echo Yii::t('common', 'No')?>",
                            "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                            "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                            "labelText" : "Activer la redirection vers un costum"
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.isCostum
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                            $("#ajax-modal").modal('hide');
                            //   urlCtrl.loadByHash(location.hash);
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        });
                      } );
                    }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() { 
            tplCtx = {}; 
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>