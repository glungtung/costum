<?php 
  $keyTpl ="communityWithListAndDescription";
  $paramsData = [ 
    "title" => "COMMUNAUTE&nbsp;<div>&nbsp;Glaz communauté  \t</div>"
    ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<?php
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style>
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
    z-index: 9999;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    font-size:23px;
  }
  .container<?php echo $kunik ?> .owl-item img{
    vertical-align: middle;
    height: 100%;
    border: 2px solid grey;
    /*box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);*/
    /*zoom:50px;*/
  }
  .container<?php echo $kunik ?> .owl-item .item{
    height: 100%;
  }
  .container<?php echo $kunik ?> .owl-item {
    transition: all .2s ease-in-out;
  }
  .container<?php echo $kunik ?> .event-place{
      color:#bca87d;
      text-transform: none !important;
  }
.container<?php echo $kunik ?> .event-name{
    color: #000000;
    text-transform: none !important;
}
.container<?php echo $kunik ?> .letter-orange{
  color:#000000 !important;
  text-transform: capitalize !important;
  font-weight: normal !important;
}
.container<?php echo $kunik ?> .community-information{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
}
.container<?php echo $kunik ?> .col-community-information{
  height: 300px;
}
@media (max-width: 992px){
  .container<?php echo $kunik ?> .col-community-information{
    height: 200px;
  }
}
</style>
<div class="container<?php echo $kunik ?> col-md-12">
	<h2 class="title title<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
    	<?php echo $paramsData["title"] ?>
  	</h2>
  <div class="swiper-container">
    <div class="swiper-wrapper"></div>
     <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
              "icon" : "fa-cog",
            
            "properties" : {
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              	else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
	});

</script>

<script>
  var imgPlaceholder =  "<?php echo Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg'; ?>";
$(function ($) {
    getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/members",
        function(data){
            var htmlContent= "";
              $.each(data,function(k,v){

                htmlContent += 
                    `<div class="swiper-slide" style="">
                        <div class="col-md-6 col-xs-12" style="
                            background-position: center;
                            background-size: contain;
                            border-radius: 50%;
                            background-repeat:no-repeat;
                            height:300px;background-image:url(${((exists(v.profilImageUrl) && v.profilImageUrl != "") ? v.profilImageUrl : imgPlaceholder)})">
                        </div>
                        <div class=" col-community-information col-md-6 col-xs-12">
                            <div class="community-information">
                              <h3 class="title-3 profil-name Oswald">${v.name}</h3>
                              <h6 class="other profil-role">
                                  ${((typeof v.rolesLink !="undefined") ? v.rolesLink.toString():"")}
                              </h6>
                              <p class="description profil-description SharpSansNo1Medium">
                                  ${((typeof v.shortDescription !="undefined") ? v.shortDescription:"")}
                            </p>
                        </div>
                        </div>
                    </div>`;
                
              });
              $('.container<?php echo $kunik ?> .swiper-wrapper').append(htmlContent);
                var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                  slidesPerView: 1,
                  spaceBetween: 0,
                  // init: false,
                  pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                  },
                  navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                  keyboard: {
                    enabled: true,
                  },
                  /*autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                  },*/
                  breakpoints: {
                    640: {
                      slidesPerView: 1,
                      spaceBetween: 20,
                    },
                    768: {
                      slidesPerView: 1,
                      spaceBetween: 40,
                    },
                    1024: {
                      slidesPerView: 1,
                      spaceBetween: 50,
                    },
                  }
                });
                
        },
    "");
    /***** end acteur collaborateur *******/

});
</script>
