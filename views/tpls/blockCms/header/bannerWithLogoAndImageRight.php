<?php 

  $keyTpl ="bannerWithLogoAndImageRight";
  $paramsData = [ 
    "descriptionVille" =>"<font color=\"#ffffff\" style=\"font-size: 26px;\" face=\"Please_write_me_a_song\">Un nôtre monde est un collectif de citoyens en dehors de tout parti politique. Notre volonté est de mettre en place une démocratie participative opérationnelle</font>",
    "buttonUrl" => "",
    "buttonLabel" => "En savoir plus",
    "buttonColor" => "white",
    "buttonBackground" => "#ea631f",
    "buttonfontSize" => "25"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
 
<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
  $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
  $initFiles = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"notBackground"
    ), "image"
  );
  $initFilesLogo = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"logo"
    ), "image"
  );
//$arrayImg = [];

$imgUrl =Yii::app()->getModule('costum')->assetsUrl.'/images/cocity/unNotreMonde/ILLU.png';
$logoUrl =Yii::app()->getModule('costum')->assetsUrl.'/images/cocity/unNotreMonde/LOGOUNAUTREMONDE.png';
if(isset($initFiles[0])){
  $images = $initFiles[0];
  if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
      $imgUrl = $baseUrl.$images["imagePath"];
  elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
      $imgUrl = $baseUrl.$images["imageMediumPath"];
}
if(isset($initFilesLogo[0])){
    $logo = $initFilesLogo[0];
    if(!empty($logo["imagePath"]) && Document::urlExists($baseUrl.$logo["imagePath"]))
        $logoUrl = $baseUrl.$logo["imagePath"];
    elseif (!empty($logo["imageMediumPath"]) && Document::urlExists($baseUrl.$logo["imageMediumPath"]))
        $logoUrl = $baseUrl.$logo["imageMediumPath"];
  }
?>
<!-- ****************end get image uploaded************** -->

<style>
     .container<?php echo $kunik ?>{
        padding-bottom: 5%;
    }
    .container<?php echo $kunik ?> .main-title{
        color: #2d2d2d;
        text-align: center;
        text-transform: capitalize;
        padding: 0.7em 0;
    }


    .container<?php echo $kunik ?> .content-image{
        width: 80%;
        margin-left: 10%;
    }
    .container<?php echo $kunik ?> .button-link:hover{
        text-decoration: none;
        color: white;
    }

    @media (max-width: 567px){
        .container<?php echo $kunik ?>,.content<?php echo $kunik ?>{
            padding-right: 0;
            padding-left: 0;
        }
    }
    @media (max-width: 992px){
        .container<?php echo $kunik ?> .title-xs{
            display: block
        }
        .container<?php echo $kunik ?> .title{
            display: none
        }
    }

    @media (min-width: 993px){
        .container<?php echo $kunik ?> .title-xs{
            display: none
        }
        .container<?php echo $kunik ?> .title{
            display: block
        }
    }
   
    .container<?= $kunik?> .content<?= $kunik?>{
        padding-left: 7%;    
        padding-bottom: 3%;
    }
    .container<?= $kunik?> .content<?= $kunik?> .bouton{
		text-transform: uppercase;
		padding: 3% 6% 3% 6%;
		border-radius: 10px;
		margin-top: 5%;
		margin-bottom: 15%;
		font-weight: bold;
        color : <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["buttonColor"]; ?>;
        font-family:Please_write_me_a_song;
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonBackground"]; ?>;
        font-size: <?= $paramsData["buttonfontSize"]?>px;
	}
    .container<?= $kunik?> .content<?= $kunik?> .bouton:hover{
		text-decoration:none;
	}
    .container<?= $kunik?> .content<?= $kunik?> .sp-text{
        margin-bottom: 11%;
        padding-right: 20%;
        margin-top: 10%;
    }
    .container<?= $kunik?> .<?= $kunik?>{
        padding-top: 6%;
    }
    .content<?= $kunik?> img{
        width : 80%;
    }
</style>
<div class="container<?php echo $kunik ?> col-md-12 col-xs-12">
    <div class ="<?= $kunik?>">
        <div class="col-md-6 content<?= $kunik?> col-xs-12">
            <img class=" lzy_img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/unNotreMonde/LOGOUNAUTREMONDE.png" data-src="<?= $logoUrl ?>" alt="">
            <!-- <img class="content-image" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logoUrl ?>" data-src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logoUrl ?>" alt="">  -->
        
            <div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="descriptionVille"  > <?= $paramsData["descriptionVille"]?> </div>
            <a href="<?= $paramsData["buttonUrl"] ?>" class=" bouton lbh"><?= $paramsData["buttonLabel"] ?></a>
        </div>

        <div class=" col-md-6 col-xs-12">
            <img class="content-image lzy_img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/unNotreMonde/ILLU.png" data-src="<?= $imgUrl ?>" alt="">
            <!-- <img class="content-image" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$imgUrl ?>" data-src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$imgUrl ?>" alt="">  -->
        </div>
    </div>
    
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {
                "image" :{
                  "inputType" : "uploader",
                    "label" : "image à droite",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                   "domElement" : "notBackground",
                    "endPoint" :"/subKey/notBackground",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    initList : <?php echo json_encode($initFiles) ?>
                },
                "imageLogo" :{
                  "inputType" : "uploader",
                    "label" : "Logo ",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                   "domElement" : "logo",
                    "endPoint" :"/subKey/logo",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    initList : <?php echo json_encode($initFilesLogo) ?>
                },
                "buttonUrl":{
                    "inputType" : "text",
                    "label": "Lien",
                    "values": sectionDyf.<?php echo $kunik ?>ParamsData.buttonUrl
                },
                "buttonLabel":{
                    "inputType" : "text",
                    "label": "Label",
                    "values": sectionDyf.<?php echo $kunik ?>ParamsData.buttonLabel
                },
                "buttonColor":{
                    "inputType" : "colorpicker",
                    "label": "Couleur du texte du bouton",
                    "values": sectionDyf.<?php echo $kunik ?>ParamsData.buttonColor
                },
                "buttonBackground" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur de fond du bouton",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonBackground
                },
                "buttonfontSize" : {
                    "inputType" : "text",
                    "label" : "Taille du texte du  bouton(en px)",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonfontSize,
                    rules:{
                      number:true
                    }

                },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          tplCtx.format = true;
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"image",6,6,null,null,"Images","green","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"button",4,6,null,null,"Propriété du bouton","green","");
        });

        <?php if($paramsData["btnReadMore"]==true){ ?>
          setTimeout(() => {
            $(".more<?php echo $kunik ?>").myOwnLineShowMoreLess({
            showLessLine: 10,
            showLessText:'Lire Moins',
            showMoreText:'Lire plus',
            //lessAtInitial:false,
            //showLessAfterMore:false
            });
          }, 900);
        <?php } ?>
    });
</script>
