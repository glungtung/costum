<?php 
$keyTpl = "slideWithColumn";
$paramsData = [
];
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/js/blockcms/slick.js"], $assetsUrl);
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style type="text/css">
	.plan_travail<?= $kunik?> {
		height: 550px;
		background-size: cover;
	}
	.slick-slide {
	    margin: 0px 5px 0px 5px;
	}
	.slick-slider
	{
		padding: 1%;
	    position: relative;
	    display: block;
	    box-sizing: border-box;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	            user-select: none;
	    -webkit-touch-callout: none;
	    -khtml-user-select: none;
	    -ms-touch-action: pan-y;
	        touch-action: pan-y;
	    -webkit-tap-highlight-color: transparent;
	}

	.slick-list
	{
	    position: relative;
	    display: block;
	    overflow: hidden;
	    margin: 0;
	    padding: 0;
	}
	.slick-list:focus
	{
	    outline: none;
	}
	.slick-list.dragging
	{
	    cursor: pointer;
	    cursor: hand;
	}

	.slick-slider .slick-track,
	.slick-slider .slick-list
	{
	    -webkit-transform: translate3d(0, 0, 0);
	    -moz-transform: translate3d(0, 0, 0);
	   -ms-transform: translate3d(0, 0, 0);
	    -o-transform: translate3d(0, 0, 0);
	    transform: translate3d(0, 0, 0);
	}

	.slick-track
	{
	    position: relative;
	    top: 0;
	    left: 0;
	    display: block;
	}
	.slick-track:before,
	.slick-track:after
	{
	    display: table;
	    content: '';
	}
	.slick-track:after
	{
	    clear: both;
	}
	.slick-loading .slick-track
	{
	    visibility: hidden;
	}

	.slick-slide
	{
	    display: none;
	    float: left;
	    min-height: 1px;
	}
	[dir='rtl'] .slick-slide
	{
	    float: right;
	}
	.slick-slide img
	{
	    display: block;
	}
	.slick-initialized .slick-slide
	{
	    display: block;
	}
	.slick-loading .slick-slide
	{
	    visibility: hidden;
	}
	.slick-vertical .slick-slide
	{
	    display: block;
	    height: auto;
	    border: 1px solid transparent;
	}
	.slick-arrow.slick-hidden {
	    display: none;
	}
	.customer-plans_<?= $kunik?> h4{
		text-transform: none;
	    color: #fff;
	    background: rgba(0,0,0,.7);
	    padding: 10%;
	    line-height: 30px;
	    margin-top: 38%;
	    font-size: 21px;
	    opacity: 0.7;
	}

	.plan_travail<?= $kunik?> .desc_<?= $kunik?> {
		display: none;
	}
	.plan_travail<?= $kunik?> .desc_<?= $kunik?> p{
		font-size: 14px;
	    color: white;
	    padding: 0% 7% 0% 7%;
	}
	.plan_travail<?= $kunik?>:hover .desc_<?= $kunik?>{
		display: block;
	    margin-top: 2%;
	}
	.plan_travail<?= $kunik?>:hover .background{
	    background: rgba(0,0,0,.8);
	    opacity:1;
	}
	.plan_travail<?= $kunik?>:hover h4{
		margin-top: 1%;
		margin-bottom: -1%; 
		background: rgba(0,0,0,0);
	    opacity: 0.7;
	}
	
	.plan_travail<?= $kunik?> .title<?= $kunik?> img{
		width: 40px;
		height: 40px;
		margin-bottom: 3%;
	}
	.editdelete<?= $blockKey?>{
		display: none;
	    position: absolute;
	    top: 20%;
	    margin-left: 1%;
	    z-index: 1;
	}

	.plan_travail<?= $kunik?>:hover .editdelete<?= $blockKey?>{
		display: block;
	}
	.plan_travail<?= $kunik?> .pointille{
		z-index: 0;
		margin-top: 150px;
		height:80px;
	}
	.desc_<?= $kunik?> hr{
		margin-top: 2%;
		border: 1px solid #fff
	}
</style>
<section id="test"class="customer-plans_<?= $kunik?> slider">
<?php if (isset($blockCms["content"])) {
	$content = $blockCms["content"];
	foreach ($content as $key => $value) {
		// background
		${'initFiles' . $key}= Document::getListDocumentsWhere(
			array(
				"id"=> $blockKey,
				"type"=>'cms',
				"subKey"=> (string)$key 
			), "image"
		);
		${'arrFile' . $key}= [];
		foreach (${'initFiles' . $key} as $k => $v) {
			${'arrFile' . $key}[] =$v['imagePath'];
		}
		//icone
		${'initFilesIcone' . $key}= Document::getListDocumentsWhere(
			array(
				"id"=> $blockKey,
				"type"=>'cms',
				"subKey"=> 'icone'.$key 
			), "image"
		);
		${'arrFileIcone' . $key}= [];
		foreach (${'initFilesIcone' . $key} as $k => $v) {
			${'arrFileIcone' . $key}[] =$v['imagePath'];
		}
	?>
	<div class="plan_travail<?= $kunik?> slide" style="background-image: url(<?= @${'arrFile' . $key}[0] ?>)">
		<div class="background">
			<?php if(Authorisation::isInterfaceAdmin()){ ?>
			<div class="hiddenPreview editdelete<?= $blockKey?>">
				<a  href="javascript:;"
					class="btn material-button text-center editElement<?= $blockKey ?> "
					data-key="<?= $key ?>" 
					data-title="<?= $value["title"] ?>" 
					data-titleColor="<?= @$value["titleColor"] ?>" 
					data-descriptionColor="<?= @$value["descriptionColor"] ?>" 
					data-background ='<?= json_encode(${'initFiles' . $key})?>'
					data-icone ='<?= json_encode(${'initFilesIcone' . $key})?>'
					data-description="<?= $value["description"] ?>">
					<i class="fa fa-edit"></i>
				</a>
				<a  href="javascript:;"
					class="btn material-button btn-danger text-center deletePlan<?= $blockKey ?> "
					data-key="<?= $key ?>" 
					data-id ="<?= $blockKey ?>"
					data-path="content.<?= $key ?>"
					data-collection = "cms"
					>
					<i class="fa fa-trash"></i>
				</a>
			</div>	
			<?php } ?>
			<div class="col-xs-12">
				<div class="col-xs-9">
					<?php if (@$value["title"]){ ?>
					<h4 class="title<?= $kunik?>"> 
						<img src="<?= isset(${'arrFileIcone' . $key}[0])?${'arrFileIcone' . $key}[0]:"" ?>">
						<?= $value["title"] ?>						
					</h4>
					<?php } ?>
				</div>
				<div class="col-xs-3">
					<img class="pointille " src="<?= Yii::app()->getModule("costum")->assetsUrl ; ?>/images/cocity/trait_pointille-06.png ">
				</div>		
			</div>
			
			<div class="desc_<?= $kunik?>">		
				<hr>	
				<p > <?= $value["description"]?></p>
			</div>
		</div>
	</div>
	<?php }
} ?>
	
</section>
<div class="text-center ">
	<div class="" style="width: 100%; display: inline-table; padding: 10px;">
		<?php if(Authorisation::isInterfaceAdmin()){?>
			<div class="text-center addElement<?= $blockKey?>">
				<?php if (!isset($blockCms["content"])) { ?>
					<p>Le contenu sera afficher ici</p>
				<?php } ?>
				<button class="hiddenPreview btn btn-primary">Ajouter contenu</button>
			</div>	
		<?php } ?>
	</div>
</div>

<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {

		$('.customer-plans_<?= $kunik?>').slick({
	        slidesToShow: 4,
	        slidesToScroll: 1,
	        autoplay: true,
	        autoplaySpeed: 1500,
	        arrows: false,
	        dots: false,
	        pauseOnHover: true,
	        responsive: [{
	            breakpoint: 768,
	            settings: {
	                slidesToShow: 2
	            }
	        }, {
	            breakpoint: 520,
	            settings: {
	                slidesToShow: 1
	            }
	        }]
	    });
		$(".addElement<?= $blockCms['_id'] ?>").click(function() {  
			var keys = Object.keys(<?php echo json_encode($content); ?>);
			var	lastKey = 0; 
			if (keys.length!=0) 
				var	lastKey = parseInt((keys[(keys.length)-1]), 10);
			var tplCtx = {};
			tplCtx.id = "<?= $blockKey ?>";
			tplCtx.collection = "cms";

			tplCtx.path = "content."+(lastKey+1);
			var obj = {
					subKey : (lastKey+1),
					title : 		$(this).data("title"),
					description:    $(this).data("description")
					
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Ajouter nouveau bloc CMS",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj),
					beforeBuild : function(){
		               uploadObj.set("cms","<?= $blockCms['_id'] ?>");
		            },
		            save : function (data) {  
		              tplCtx.value = {};
		              $.each( activeForm.jsonSchema.properties , function(k,val) { 
		                tplCtx.value[k] = $("#"+k).val();
		              });
		              mylog.log("save tplCtx",tplCtx);

		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                     	// urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		              }

		            }
				}
				};          
				dyFObj.openForm( activeForm );
				alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"<?php echo Yii::t('cms', 'Title')?>","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"description",6,12,null,null,"<?php echo Yii::t('cms', 'Description')?>","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"im",6,12,null,null,"<?php echo Yii::t('cms', 'Description')?>","#1da0b6");
			
			});	


		$(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",

            function(result){
              if (!result) {
                return;
              }else {
				dataHelper.path2Value( deleteObj, function(params) {
					mylog.log("deleteObj",params);
					toastr.success("Element effacé");
					var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					cmsBuilder.block.loadIntoPage(id, page, path, kunik);
					// urlCtrl.loadByHash(location.hash);
				});
              }
            }); 
			
		});
		$(".editElement<?= $blockCms['_id'] ?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>"
			tplCtx.collection = "cms";
			tplCtx.path = "content."+(key);
			var obj = {
				subKey : key,
				background : $(this).data("background"),
				icone 		: $(this).data("icone"),
				title : 		$(this).data("title"),
				description:    $(this).data("description"),
				titleColor : 		$(this).data("titleColor"),
				descriptionColor:    $(this).data("descriptionColor")
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Ajouter nouveau bloc CMS",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj),
					beforeBuild : function(){
		                uploadObj.set("cms","<?= $blockCms['_id'] ?>");
		            },
		            save : function (data) {  
		              tplCtx.value = {};
		              $.each( activeForm.jsonSchema.properties , function(k,val) { 
		                tplCtx.value[k] = $("#"+k).val();
		              });
		              console.log("save tplCtx",tplCtx);

		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
								toastr.success("Modification enregistrer");
								$("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		              }

		            }
				}
				};          

				dyFObj.openForm( activeForm );
				alignInput2(activeForm.jsonSchema.properties,"title",4,6,null,null,"<?php echo Yii::t('cms', 'Title')?>","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"description",6,12,null,null,"<?php echo Yii::t('cms', 'Description')?>","#1da0b6");
				alignInput2(activeForm.jsonSchema.properties,"im",6,12,null,null,"<?php echo Yii::t('cms', 'Description')?>","#1da0b6");
			
			});
		function getProperties(obj={}){
			var props = {
				title : {
					label : "Titre",
					"inputType" : "text",
					value : obj["title"]
				},
				titleColor : {
					label : "Couleur du titre",
					inputType : "colorpicker",
					value :  obj["descriptionColor"]
				},
				description : {
					label : "Description",
					"inputType" : "textarea",
					"markdown" : true,
					value :  obj["description"]
				},
				descriptionColor : {
					label : "Couleur de la description",
					inputType : "colorpicker",
					value :  obj["descriptionColor"]
				},
				img : {
					"inputType" : "uploader",	
					"label" : "image de fond",
	              	"docType": "image",
					"contentKey" : "slider",
					"domElement" : obj["subKey"],		
	              	"filetypes": ["jpeg", "jpg", "gif", "png"],
	              	"showUploadBtn": false,
	                "endPoint" :"/subKey/"+obj["subKey"],
	              	initList : obj["background"]		
				},
				"imageOne" :{
	              "inputType" : "uploader",
	              "label" : "Icone",
	              "docType": "image",
				  "contentKey" : "slider",
	              "domElement" : "icone"+obj["subKey"],
	              "itemLimit" : 1,
	              "filetypes": ["jpeg", "jpg", "gif", "png"],
	              "showUploadBtn": false,              
	              "endPoint" :"/subKey/icone"+obj["subKey"],
	              initList : obj["icone"]
	            }
			};
			return props;
		}
		 sectionDyf.<?php echo $kunik ?>Params = {
      	"jsonSchema" : {    
	        "title" : "Configurer votre section",
	        "description" : "Personnaliser votre section",
	        "icon" : "fa-cog",

	        "properties" : {
	        },
	        beforeBuild : function(){
	          uploadObj.set("cms","<?php echo $blockKey ?>");
	        },
	        save : function (data) {  
	          tplCtx.value = {};
	          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
	            tplCtx.value[k] = $("#"+k).val();
	            if (k == "parent")
	              tplCtx.value[k] = formData.parent;

	            if(k == "items")
	              tplCtx.value[k] = data.items;
	          });

	          if(typeof tplCtx.value == "undefined")
	            toastr.error('value cannot be empty!');
	          else {
	            dataHelper.path2Value( tplCtx, function(params) {
	              dyFObj.commonAfterSave(params,function(){
	                toastr.success("Modification enregistré!");
	                $("#ajax-modal").modal('hide');

					var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                // urlCtrl.loadByHash(location.hash);
	              });
	            } );
	          }

	        }
      	}
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.subKey = "imgParent";
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
	});
</script>