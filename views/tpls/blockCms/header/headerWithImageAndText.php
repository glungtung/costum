<?php
$keyTpl ="headerWithImageAndText";
$paramsData = [
    "logo" => null,
    "title" => "Créer votre propre réseau sociale personnalisée !",
    "subtitle"=> "Votre design, vos données et innover ",
    "text" => " Tout en contribuant aux communs"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>

<!-- ****************get image uploaded************** -->
<?php
$blockKey = (string)$blockCms["_id"];
$baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
$initFiles = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"notBackground"
    ), "image"
);
$initFilesLogo = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"logo"
    ), "image"
);
//$arrayImg = [];

$imgUrl =Yii::app()->getModule('costum')->assetsUrl.'/images/costumDesCostums/photo.jpg';
$logoUrl =Yii::app()->getModule('costum')->assetsUrl.'/images/costumDesCostums/header.png';
if(isset($initFiles[0])){
    $images = $initFiles[0];
    if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
        $imgUrl = $baseUrl.$images["imagePath"];
    elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
        $imgUrl = $baseUrl.$images["imageMediumPath"];
}
if(isset($initFilesLogo[0])){
    $logo = $initFilesLogo[0];
    if(!empty($logo["imagePath"]) && Document::urlExists($baseUrl.$logo["imagePath"]))
        $logoUrl = $baseUrl.$logo["imagePath"];
    elseif (!empty($logo["imageMediumPath"]) && Document::urlExists($baseUrl.$logo["imageMediumPath"]))
        $logoUrl = $baseUrl.$logo["imageMediumPath"];
}
?>

<style>
    .header_<?= $kunik?> {
        background-image : url(<?php echo $imgUrl ?>);
        background-size: cover;
        padding-bottom: 49%;
    }
    .intro_<?= $kunik?> {
        background-color: #ededed;
        margin-left: 10%;
        margin-right: 10%;
        margin-top: -6%;
        padding-bottom: 3%;
    }
    .intro_<?= $kunik?> .intro-description{
        background: white;
        text-align: center;
        padding-top: 3%;
        padding-bottom: 3%;
        /*color : <?php //echo $costum["css"]["color"]["blue"]; ?>;*/
        border: solid 1px silver;
    }
    /*.intro-description > h1,h2,h3,h4,h5,h6{
        font-family: Homestead !important;
    }*/
</style>

<div class="header_<?= $kunik?>">
    <center class="col-xs-12 col-sm-12">
        <img src="<?php echo $logoUrl; ?>" class="img-responsive" style="margin-top: 3%; width:80%"  usemap="#headerimg">



        <!--<map name="headerimg">
            <area shape="circle" coords="450,97,92" alt="Costum pour le pacte de la transition" href="https://pacte-transition.org" target="_blank" />
            <area shape="circle" coords="501,346,68" alt="Costum du site Open Altas" target="_blank" href="http://www.open-atlas.org/" />
            <area shape="circle" coords="736,131,75" alt="Site du costum de la Raffinerie" href="https://www.communecter.org/costum/co/index/id/laRaffinerie" target="_blank" />
            <area shape="circle" coords="770,442,90" alt="Costum de la filière Numérique" href="https://www.communecter.org/costum/co/index/id/coeurNumerique" target="_blank" />
        </map>-->

    </center>
</div>

<div class="intro_<?= $kunik?>  row" >
    <div style="margin-top: 3%; margin-bottom: 0%; margin-left: 3%; margin-right: 3%;">
        <div class="intro-description col-xs-12 col-sm-12" >
            <h1 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h1>
            <h2 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitle"><?= $paramsData["subtitle"]?></h2>
            <h4 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text"> <?= $paramsData["text"]?></h4>
        </div>
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready( function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "Configurer votre bloc",
                "description" : "Personnaliser votre bloc",
                "icon" : "fa-cog",
                "properties" : {
                    "image" :{
                        "inputType" : "uploader",
                        "label" : "image de fond",
                        "docType" : "image",
                        "contentKey" : "slider",
                        "itemLimit" : 1,
                        "endPoint" :"/subKey/notBackground",
                        "domElement" : "image",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        initList : <?php echo json_encode($initFiles) ?>
                    },
                    "imageLogo" :{
                        "inputType" : "uploader",
                        "label" : "Logo ",
                        "docType" : "image",
                        "contentKey" : "slider",
                        "itemLimit" : 1,
                        "domElement" : "logo",
                        "endPoint" :"/subKey/logo",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        initList : <?php echo json_encode($initFilesLogo) ?>
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent")
                            tplCtx.value[k] = formData.parent;

                        if(k == "items")
                            tplCtx.value[k] = data.items;
                        mylog.log("andrana",data.items)
                    });
                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

    });
</script>
