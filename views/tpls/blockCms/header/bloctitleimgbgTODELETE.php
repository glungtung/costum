<?php 
$keyTpl = "bloctitleimgbg";
$paramsData=[
  "text" => "QUI SOMMES-NOUS ?",
  "textColor" =>"#000000"
];


if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
?>

<style type="text/css">
  .row<?php echo $kunik ?>{
    padding-top: 100px!important;
    color: #fff;
    height: 500px; /* You must set a specified height */
    background-position: center; /* Center the image */
    background-repeat: no-repeat; /* Do not repeat the image */
    background-size: cover;
  }

 
</style>
<div class="row row<?php echo $kunik ?> container<?php echo $kunik ?>">
  <div class="text-center padding-20" style="top:25%; position: relative;">
    <div>
      <h1 class="sp-text title img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text" style=" font-family: Lato-Italic;color: <?= $paramsData["textColor"]; ?>"><?= $paramsData["text"]; ?></h1>
      <button style="display: none;  background-color: #44536a;"><span><i class="fa fa-plus"></i></span></button>
    </div>
  </div>
</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",

        "properties" : {
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;

            if(k == "items")
              tplCtx.value[k] = data.items;
          });

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Modification enregistré!");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.subKey = "imgParent";
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>