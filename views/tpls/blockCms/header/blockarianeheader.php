<?php 
$keyTpl = "blockarianeheader";
$paramsData = [
  "blockTitle"   => "Lorem ipsum",
  "titleSize"    => "64",
  "titleColor"   => "#2C3E50",

  "subtitles"     => "Lorem ipsum",
  "subtitleSize"    => "49",
  "subtitleColor"   => "#2C3E50",

  "text"      => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore",
  "textSize"    => "39",
  "textColor"   => "#2C3E50",

  "imageSize"=> "35",
  "circle_color"   => "#f9576d"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
if(isset($costum["contextType"]) && isset($costum["contextId"])){
  $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
 }
$profil = isset($el["profilImageUrl"])?$el["profilImageUrl"]: Yii::app()->getModule("costum")->assetsUrl+"/images/costumDesCostums/costum.png";
?>
<!-- ****************get image uploaded************** -->
<?php 

  $initImageOver = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
  $arrayImg = [];
  foreach ($initImageOver as $key => $value) {
    if ($value["imageMediumPath"]=='') {
      $arrayImg[]= $value["imagePath"];
    }else{
      $arrayImg[]= $value["imageMediumPath"];
    }
  }
 ?>
<!-- ****************end get image uploaded************** -->

<style type="text/css">
  
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?> .btn-edit-delete .btn{
    box-shadow: 0px 0px 20px 3px #ffffff;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .text<?php echo $kunik ?> p{
    font-family: Lato-Italic;
  }
</style>
<div class="container<?php echo $kunik ?>" style="position: relative;">
  <div style="padding-top: 150px!important;padding-left: 10%!important;padding-right: 10%!important;">
    <div class="row">
      <div class="visible-xs visible-sm col-md-6 text-center">
        <div style="border: 4px solid <?php echo $paramsData["circle_color"]; ?>;border-radius: 100%;display: inline-table;">
          <?php if (count($arrayImg)!=0) { ?>
              <img style="width: <?php echo $paramsData["imageSize"]; ?>vw;height: <?php echo $paramsData["imageSize"]; ?>vw; border-radius: 100%;padding: 20px" src="<?php echo $arrayImg[0] ?> ">  
          <?php }else{ ?>
            
          <?php } ?>
        </div>
      </div>
      <div class="col-md-6">
        <font class="bold sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="blockTitle" style="font-size: <?php echo $paramsData["titleSize"]; ?>px;color: <?php echo $paramsData["titleColor"]; ?>"><?php echo $paramsData["blockTitle"]; ?></font><br>
        <font class=" sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitles" style="font-size: <?php echo $paramsData["subtitleSize"]; ?>px;color:<?php echo $paramsData["subtitleColor"]; ?>"><?php echo $paramsData["subtitles"]; ?></font><br><br><br><br>
        <div class=" text<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text" style='font-size: <?php echo $paramsData["textSize"]; ?>px;
    color: <?php echo $paramsData["textColor"]; ?>; '><?php echo $paramsData["text"]; ?></div>
      </div>
      <div class="hidden-xs hidden-sm col-md-6">
        <div style="border: 4px solid <?php echo $paramsData["circle_color"]; ?>;border-radius: 100%;display: inline-table;position: absolute;z-index: 1; top: 100px">
          <?php if (count($arrayImg)!=0) { ?>
              <img style="width: <?php echo $paramsData["imageSize"]; ?>vw;height: <?php echo $paramsData["imageSize"]; ?>vw; border-radius: 100%;padding: 20px" src="<?php echo $arrayImg[0] ?> ">  
          <?php }else{ ?>
            
          <?php } ?>
          
        </div>
      </div>      
    </div>
  </div>
  <?php if (@$costum['slug'] == "numerisationlila") { ?>
    <div class="visible-xs visible-sm " style="position: relative; bottom: 0;z-index: 1;padding-left: 10%!important;">
    <?php if ($profil != "") { ?>      
      <img style="max-height: 100px" src="<?= $profil ?>">
    <?php } else{?>
      <img style="max-height: 100px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/numerisationlila/logo.png">
    <?php }?>
  </div>
  <div class="hidden-xs hidden-sm" style="position: inherit; bottom: 0; padding-top: 100px !important;padding-left: 10%!important;">
    <?php if ($profil != "") { ?>      
      <img style="max-height: 100px" src="<?= $profil ?>">
    <?php } else{?>
      <img style="max-height: 100px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/numerisationlila/logo.png">
    <?php }?>
  </div>
 <?php } ?>

</div>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "CConfigurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
              "imageSize" : {
                "label" : "Taille de photo",
                "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.imageSize
              },
              "circle_color" : {
                "label" : "Couleur de cercle",
                "inputType" : "colorpicker",
                "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.circle_color
              },
              "image" :{
                "inputType" : "uploader",
                "label" : "image",
                "docType": "image",
                "contentKey" : "slider",
                "itemLimit" : 1,
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "showUploadBtn": false,
                "domElement" : "image",
                "endPoint" :"/subKey/image",
                initList : <?php echo json_encode($initImageOver) ?>
              }
            },
            
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>