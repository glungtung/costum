<?php 
$keyTpl = "banner_With_2_btn";
$paramsData = [
	"title" => "   <font color=\"#ffffff\" face=\"OpenSans-Bold\" style=\"font-size: 50px;line-height: 50px;\"> Le monde change,</font><div><font color=\"#ffffff\" face=\"NotoSans-Regular\" style=\"font-size: 31px;line-height: 50px;\">connecter vous au mapping by Technopole </font></div>   ",
	"buttonLink"        => "",
    "buttonLabel1"       => "Qu'est ce que l'innovation ?",
    "buttonLabel2"       => "Je souhaite rejoindre l'écosystème",

    "linkLabel" => "Le Mapping de l'Ecosystème Innovation de La Réunion" ,
    "linkSize" => "40px",
    "linkColor" => "#ffffff",
    "linkLink" => "mapping",

    "button1Link"        => "",
    "button1Label"       => "Qu'est ce que l'innovation ?",
    "button1Class"       => "lbh",
    "formShow" => false
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<style type="text/css">
    <?php if(!Authorisation::isInterfaceAdmin()){?>
		#menuTopLeft a ,.header_<?= $kunik?> .desactive{
			pointer-events: none;
		}
	<?php } ?>
	#menuTopLeft a {
		text-decoration: none;
	}
	.header_<?= $kunik?> .content-text{
		margin-top: 10%;
		margin-left: 7%;
        /*background: linear-gradient(
                70deg, rgb(6, 10, 25,.5), rgb(22, 32, 82,.5));
        border-radius: 10px;
        padding-top: 20px;*/
	}
	.header_<?= $kunik?> .content{
		margin-left: 7%;
	}
	.header_<?= $kunik?> .content .bouton:hover {
		background: transparent;
		color: white !important;
		border: 1px solid #F0FCFF;
	}
	.header_<?= $kunik?> .content p {
		margin-top: 33px;
	}
	.header_<?= $kunik?> .content .bouton{
        font-family: 'NotoSans-Regular' !important;
		background-color: #fff;
		padding: 5px 8px;
		border-radius: 30px;
		font-size: 15px;
        margin-top: 10px;
        margin-bottom: 20px;
		font-weight: bold;
        line-height: 28px;
	}
	.header_<?= $kunik?> .content button{
		margin-left : 10px;
	}
	.btn-<?= $kunik?> {
		margin-top : 50px;
	}

    .link-btn {
        font-size: <?= $paramsData["linkSize"]?> ;
        color: <?= $paramsData["linkColor"]?> ;
        text-transform: none;
        text-decoration: none;
        font-weight: 700;
    }
    a.link-btn:hover, a.link-btn:focus, a.link-btn:active, a.active {
        text-transform: none;
        text-decoration: none;
        color: #bfc2c4;
    }
    a.btnTwo {
        white-space: initial;
        word-wrap: break-word;
        min-height: 40px;
        height: auto;
    }

	
	@media (max-width: 978px) {
		#menuTopLeft .menu-btn-top:before {
			font-size: 18px;

		}
		.header_<?= $kunik?> .content-text{
			margin-top: 20%;
		}
		
		.header_<?= $kunik?> .content .bouton{
			width: 39%;
			border-radius: 30px;
			height: auto;
			margin-top: 7%;
			font-weight: 600;
			padding: 1% 3% 1% 3%;
			margin-bottom: 10%;
			font-size: 18px !important;
		}
		.container<?= $blockCms["blockParent"]?>{
			background-image : none;
		}
	}
    .header-video-bg {
        position: relative;
        /* background-color: black; */
        width: 100%;
        /* height: 95vh; */
        top: 0; right: 0; bottom: 0; left: 0;
        min-height: 25rem;
        overflow: hidden;
    }

    .video-foreground,
    .header-video-bg iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        pointer-events: none;
        object-fit: cover;
    }

    .header_<?= $kunik?> {
        background: linear-gradient(
                90deg, rgb(6, 10, 25, .9) 35%, rgba(22, 32, 82,0) 100%);
        height: 100%;
    }
    @media (min-width: 768px) {
		.header-video-bg {        
			height: 95vh;
		}
	}
	@media (max-width: 767px) {
		.sp-elt-<?= $blockKey?>{
			margin-bottom: 20%;
		}
        .header_<?= $kunik?> .content .bouton{
            width: 100%;
            height: auto;
            margin-top: 2%;
            margin-bottom: 0%;
        }
        .link-btn {
            font-size: 30px;
        }
        .header_<?= $kunik?> .content-text{
            margin-top: 5px;
        }
        .header-video-bg .video-container {
            margin-bottom: 5px;
        }
        .video-foreground,
        .header-video-bg iframe {
            pointer-events: all;
            object-fit: cover;
        }
        .header-video-bg {
            /* height: 100vh; */
        }
		.filter<?= $kunik?> #s2id_selectMultipleStatus .select2-choices{ 
			border-top-left-radius: 0 !important;
    		border-bottom-left-radius: 0 !important;
		} 
		.filter<?= $kunik?>{
			margin-top: 10%;
		}
    }
	.select2-container-multi .select2-choices{
		border : none;
	}

    @media (max-height: 700px) {
        .header-video-bg {
            /* height: 720px; */
        }
    }

	.select2-container-multi .select2-choices .select2-search-field{
		width: 0.75em !important;
		padding-bottom: 8px !important;
	}
    .header-video-bg .header_<?= $kunik?> {
        position: relative;
        z-index: 2;
    }
    @media (min-aspect-ratio: 16/9) {
        .video-foreground { height: 300%; top: -100%; }
    }
    @media (max-aspect-ratio: 16/9) {
        .video-foreground { width: 300%; left: -100%; }
    }
    @media all and (max-width: 600px) {
        .vid-info { width: 50%; padding: .5rem; }
        .vid-info h1 { margin-bottom: .2rem; }
    }
    @media all and (max-width: 500px) {
        .vid-info .acronym { display: none; }
    } 
	
    .filter<?= $kunik?> .selectMultiple label{
        color: white;
        font-size: 12px;
    }
	.filter<?= $kunik?>{
		margin-top: 3%;
		margin-left: 7%;
	}

    .filter<?= $kunik?> #s2id_selectMultipleStatus .select2-choices{ 
		border-top-left-radius: 20px;
    	border-bottom-left-radius: 20px;
	}   
	.filter<?= $kunik?> .select2-container-multi .select2-choices{
		border-radius: 0;
		border : 1px solid;

	}
    .filter<?= $kunik?> .search<?= $kunik?>{    
		padding: 10px;
		font-size: 21px;
		background-color: white;
		border: 1px solid;
		color: #1a2660; 
		border-top-right-radius: 20px;
    	border-bottom-right-radius: 20px;
    }
    .filter<?= $kunik?> .select2-container-multi:not(.aapstatus-container) .select2-choices .select2-search-choice{   
		background: #1a2660!important;
		color: #fff!important;
		padding-left: 15px!important;
		padding-right: 15px!important;
		padding-top: 5px!important;
		padding-bottom: 5px!important;
		border: 1px solid #1a2660!important;
		border-radius: 5px!important;
    	font-weight: 700!important;
	}
	.filter<?= $kunik?> .select2-search-choice-close {
		background: #fff!important; 
		margin: 0 !important;
		padding-left: 2px;
    	border-radius: 10px;
	}
	.filter<?= $kunik?> .select2-search-choice-close::before {
		color: #1a2660 !important;
		font-size: 10px;
	}
    .btnSearch<?=$kunik?>{
        margin-top: 34px;
    }
	.filter<?= $kunik?> .select2-dropdown-open.select2-drop-above .select2-choices,
	.select2-container-active .select2-choices{
		background-color : white !important;
	}

	/* .select2-container-multi .select2-choices  {
		background: none;
		border: none;
		padding: 20px 0 15px;
		width: 100%;
		height: 50px;
		border-radius: 10px;
		padding: 0 15px;
		box-shadow: none;
		border-right: 6px solid #9fbd38;
		font-size: 18px;
	} */

	.filter<?= $kunik?>  .select2-container-multi{
		width: 100%;
	}
	.filter<?= $kunik?> label {
		color: white;
	}
	.select2-result-label:hover{
		background-color:#1a2660;
	}
	.select2-results .select2-highlighted{
		background-color:#1a2660;
	}	
	.<?=$kunik?> .container-filters-menu #activeFilters .filters-activate{
		background-color:#1a2660 !important;
	}
	.select2-results .select2-result-label{
		border-bottom: 1px double #e2e2e2;
	}
	.select2-container-multi .select2-choices .select2-search-field input:focus{
		background-color: white;
	}
</style>




<div class="col-xl-12 no-padding header-video-bg" >
     <div class="video-foreground hidden-xs"> 
        <iframe id="player" frameborder="0" allowfullscreen   src="https://www.youtube.com/embed/GGEr6in6G1w?rel=0&enablejsapi=1&widgetid=1&mute=1&loop=1&autoplay=1&controls=0&showinfo=0&autohide=1&playlist=GGEr6in6G1w"></iframe>
    </div>
    <div class="video-container visible-xs col-xs-12">
        <iframe id="player" frameborder="0" allowfullscreen   src="https://www.youtube.com/embed/GGEr6in6G1w?rel=0&enablejsapi=1&widgetid=1&mute=1&loop=1&autoplay=1&controls=0&showinfo=0&autohide=1&playlist=GGEr6in6G1w"></iframe>
    </div>
    <div class="header_<?= $kunik?> search_<?= $kunik?> col-xs-12" >
        <div class="content-text col-lg-6 col-md-8 col-sm-10 col-xs-10" >
            <a class="link-btn desactive" href="javascript:;" data-link="<?= $paramsData["linkLink"]?>" id="scrrol<?= $kunik?>"><?= $paramsData["linkLabel"]?> </a>
            <div class="sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"  > <?= $paramsData["title"]?> </div>
        </div>
		<div class="filter<?= $kunik?> col-xs-12">
			<div class='search-bar'>
				<div class="row">
					<div class=" col-lg-2 col-md-2  col-sm-2 col-xs-12 no-padding ">
						<div>
							<label>Je suis<label>
						</div>
						<input id="selectMultipleStatus"  data-params="statusListFilter" data-type="select2">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding">
						<div>
							<label>Je cherche<label>
						</div>
						<input id="selectMultipleAccompaniment" data-params="accompanimentTypeFilter" data-type="select2">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding">
						<div>
							<label>Secteurs<label>
						</div>
						<input id="selectMultipleThemes" data-params="themes" data-type="select2">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-11 no-padding">
						<div>
							<label>Types de financements<label>
						</div>
						<input id="selectMultipleTypeFinancing" data-params="TypeFinancing" data-type="select2">
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 btnSearch<?=$kunik?> no-padding" >
						<a class="search<?= $kunik?>" href="javascript:;"> <i class="fa fa-search"></i></a>  							 
					</div>
				</div>       
			</div>
		</div>
		<div class="content col-lg-6 col-md-8 col-sm-10 col-xs-12">
			<div class="btn-<?= $kunik?>">
                <a href="<?= $paramsData["button1Link"]?>"  class=" btn bouton btnTwo desactive <?= $paramsData["button1Class"]?>">
                    <?= $paramsData["button1Label"]?>
                </a>
                <a href="javascript:;" id="joinBtn<?= $kunik ?>" class="btn bouton btnTwo" style="text-decoration : none;">
					<?= $paramsData["buttonLabel2"]?>
                </a>
            </div> 
        </div>
		
    </div>
</div> 
<script type="text/javascript">	
	$('#player').css('display', 'none');
	function onYouTubeIframeAPIReady() {
      var player = new YT.Player('player', {
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
    }
    function onPlayerReady(event) {
      event.target.playVideo();
    }
    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING) {
		$('.container<?= $blockCms["blockParent"]?>').css('background-image', 'none');
		$('#player').css('display', 'block');
      }else{
		$('#player').css('display', 'none');
	  }
    }	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() { 
		var themes = [];
		var accompanimentTypeFilter = [];
		var statusListFilter = [];
		var typeFinancing = [];
		if(typeof costum.lists.themes){
			$.each(costum.lists.themes,function(kth,vth){
				themes.push({"id":kth,"text":vth})
			 })
		}
		if(typeof costum.lists.accompanimentTypeFilter){
			$.each(costum.lists.accompanimentTypeFilter,function(ka,va){
				accompanimentTypeFilter.push({"id":ka,"text":va})
			 })
		}
		if(typeof costum.lists.status){
			$.each(costum.lists.status,function(ks,vs){
				statusListFilter.push({"id":ks,"text":vs})
			 })
		}
		if(typeof costum.lists.typeFinancing){
			$.each(costum.lists.typeFinancing,function(kt,vt){
				typeFinancing.push({"id":vt,"text":vt})
			 })
		}
        if( sectionDyf.<?php echo $kunik?>ParamsData.formShow == "true"){
            dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"});
        }
		$("#btn-search-<?= $kunik?>").click(function(){
			$( "#search-form<?= $kunik?>" ).submit();
			var str = $("#otherInput").val();
			location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
		});
		$( "#search-form<?= $kunik?>" ).submit(function( event ) {
			var str = $("#otherInput").val();
			location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
			event.preventDefault();
		});
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					"button1Label":{
			            label : "<?php echo Yii::t('cms', 'Button label')?> 1",
			            inputType : "text",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.button1Label
			         }, 
					 "buttonLabel2":{
			            label : "<?php echo Yii::t('cms', 'Button label')?> 2",
			            inputType : "text",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonLabel2
			         },
                     "button1Class":{ 
                        "label" : "<?php echo Yii::t('cms', 'Internal or external link')?> 1",
                        inputType : "select",
                        options : {              
                        "lbh " : "<?php echo Yii::t('cms', 'Internal')?> : <?php echo Yii::t('cms', 'full page')?>",
                        "lbh-preview-element " : "<?php echo Yii::t('cms', 'Internal')?> : <?php echo Yii::t('cms', 'prévisualisation')?>",
                        " " : "<?php echo Yii::t('cms', 'External')?>",
                        },
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.button1Class
                    },
                    "button1Link" : {
                        "label" : "<?php echo Yii::t('cms', 'Button link')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.button1Link
                    },
                    "linkLabel":{
                        label : "<?php echo Yii::t('cms', 'Label of the link')?> ",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.linkLabel
                    },
                    "linkColor":{
                        label : "<?php echo Yii::t('cms', 'Link color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.linkColor
                    },
                    "linkSize":{
                        label : "<?php echo Yii::t('cms', 'Link size')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.linkSize
                    },
                    "linkLink":{
                        label : "<?php echo Yii::t('cms', 'Link')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.linkLink
                    },
                    "formShow":{
                        label : "Afficher directement le formulaire",
                        inputType : "checkboxSimple",
                        params: checkboxSimpleParams,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.formShow
                    },
					
				},				
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
					
				}
			}
		};
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() { 
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"link",6,6,null,null,"<?php echo Yii::t('cms', 'Link property')?>","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"button1",6,6,null,null,"<?php echo Yii::t('cms', 'Button property' )?> 1","#e45858","");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"buttonLabel2",6,6,null,null,"<?php echo Yii::t('cms', 'Button property' )?> 2","#e45858","");
		});
		$("#joinBtn<?= $kunik ?>").off().click(function(){            
            dyFObj.openForm('organization');
		});
		// $("#addProject<?= $kunik?>").off().click(function(){			
        //     dyFObj.openForm('project');
		// });
        $("#scrrol<?= $kunik?>").off().click(function(){
            var link = $(this).data("link");
            $('html, body').animate(
				{ scrollTop: $("#"+link).offset().top - $("#mainNav").height() },
				800,
				() => {
				},
			);
		});
		$(".search<?= $kunik?>").click(function(){
			var statusActor = $("#selectMultipleStatus").val();
			var accompaniment = $("#selectMultipleAccompaniment").val();
			var themes = $("#selectMultipleThemes").val();
			var typeFinancingtags = $("#selectMultipleTypeFinancing").val();
			var allTypeFinancing = "";
			var allTags = "";
			if(statusActor != "")
                allTags += statusActor
			if(accompaniment != ""){
                if(statusActor != "")
                    allTags += ","+accompaniment
                else 
                    allTags +=  accompaniment
            }
			if(themes != ""){
                if(accompaniment != "" )
                    allTags += ","+themes
                else if(accompaniment == "" && statusActor != "")
                    allTags += ","+themes
                else if(accompaniment == "" && statusActor == "")
                    allTags +=  themes
            }
			if(typeFinancingtags != ""){
                if(themes != "" )
                    allTags += ","+typeFinancingtags	
                else if(themes == "" && accompaniment != "" && statusActor == "")
                    allTags += ","+typeFinancingtags
                else if(themes == "" && accompaniment == "" && statusActor != "")
                    allTags += ","+typeFinancingtags
                else if(themes == "" && accompaniment != "" && statusActor != "")
                    allTags += ","+typeFinancingtags
                else if(accompaniment == "" && statusActor == "" && themes=="" )
                    allTags +=  typeFinancingtags
            }  
			if(allTags == ""){ 
			    urlCtrl.loadByHash("#search");
            }
			if(allTags != "") {
				allTags = allTags.split(","); 
			    urlCtrl.loadByHash("#search?tags="+allTags);
            }
			

			// if(allTags != "" && typeFinancing != "" ){
			// 	allTags = allTags.split(","); 
			// 	typeFinancing = typeFinancing.split(","); 
			//     urlCtrl.loadByHash("#search?tags="+allTags+"&"+"typeFinancing="+typeFinancing);
			// }			         
            // if(allTags == "" && typeFinancing == ""){ 
			//     urlCtrl.loadByHash("#search");
            // }
			// if(allTags != "" && typeFinancing == "") {
			// 	allTags = allTags.split(","); 
			//     urlCtrl.loadByHash("#search?tags="+allTags);
            // }
			// if(allTags == "" && typeFinancing != "") {
			// 	typeFinancing = typeFinancing.split(","); 
			//     urlCtrl.loadByHash("#search?typeFinancing="+typeFinancing);
            // }
		})
        setTimeout(function(){		 		
            $("#selectMultipleStatus").select2({
                data: statusListFilter,
                tags : true, 
            });  		
            $("#selectMultipleAccompaniment").select2({
                data: accompanimentTypeFilter,
                tags : true, 
            });  		
            $("#selectMultipleThemes").select2({
                data: themes,
                tags : true, 
            });  	
            $("#selectMultipleTypeFinancing").select2({
                data: typeFinancing,
                tags : true, 
            });  
        },100)

	});
</script>
<script src="https://www.youtube.com/iframe_api"></script>
