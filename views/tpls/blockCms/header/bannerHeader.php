
<?php 
  $keyTpl ="bannerHeader";
  $paramsData = [ 
    "text" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco ",
    "textColor" => "white",
    "addOver" => "text",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<!-- ****************get image uploaded************** -->
<?php 

  $initImageOver = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'imgOver',
    ), "image"
  );
$arrayImgOver = [];
$arrayImg = [];

foreach ($initImageOver as $key => $value) {
  $arrayImgOver[]= $value["imagePath"];
}
$assetsUrl = Yii::app()->controller->module->assetsUrl;
 ?>
<!-- ****************end get image uploaded************** -->

<style>
  .container<?php echo $kunik ?>{
    padding: 0px !important;
  }
  .container<?php echo $kunik ?> .p-img{
    text-align: center;
    margin: 0px;
    padding: 0px;
  }
.container<?php echo $kunik ?> .btn-edit-delete{
  display: none;
}
.container<?php echo $kunik ?>:hover .btn-edit-delete{
  display: block;
  position: absolute;
  top:0%;
  left: 50%;
  transform: translate(-50%,0%);
}
.container<?php echo $kunik ?> .img-over{
  position: absolute;
  top:50%;
  left: 50%;
  transform: translate(-50%,-50%);
}
.container<?php echo $kunik ?> .text-over{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    font-size: 50px;
    color: <?php echo $paramsData["textColor"] ?>;
    background: transparent;
    width: 87%;
    text-align: center;
    margin: 0px;
    padding: 0px;
}
@media (max-width: 500px){
  .container<?php echo $kunik ?> .img-over{
    display:none !important;
  }
}
@media (max-width: 800px){
  .container<?php echo $kunik ?> .text-over{
    font-size: 40px;
  }
}
@media (max-width: 600px){
  .container<?php echo $kunik ?> .text-over{
    font-size: 30px;
  }
}
@media (max-width: 440px){
  .container<?php echo $kunik ?> .text-over{
    display:none;
  }
}
</style>
<div class="container<?php echo $kunik ?> col-md-12">
  <?php if (count($arrayImg)!=0) { ?>
    <p class="p-img">
      <img class="img-responsive" src="<?php echo $arrayImg[0] ?> " alt="">    
    </p>
  <?php }else{ ?>
    <p class="p-img">
      <img class="img-responsive" src="<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/images/glazBijoux/header_img-02.jpg">
    </p>
  <?php } ?>

  <?php if ($paramsData["addOver"]=="text") { ?>
    <p class="text-over  description"><?php echo $paramsData["text"]; ?></p>
  <?php }elseif ($paramsData["addOver"]=="image") { ?>
  <?php  if (count($arrayImgOver)!=0) { ?>
        <img class="img-responsive img-over" width="150" height="200" src="<?php  echo $arrayImgOver[0] ?>">  
  <?php  }else{ ?>
       <img class="img-responsive img-over" width="150" height="200" src="<?php  echo $assetsUrl?>/images/glazBijoux/logo_glaz-02.png">
  <?php  } ?>
 <?php } ?>
  

</div>
 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
              
              "addOver" :{
                "inputType" : "select",
                "label" : "Ajouter Text ou Image par dessus",
                "options":{
                  "text":"Text",
                  "image":"Image",
                  "aucun":"Aucun"
                }
              },
              "text" : {
                "inputType" : "textarea",
                "label" : "Votre text",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.text
              },
              "textColor" : {
                "inputType" : "colorpicker",
                "label" : "Couleur de la description",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
              },
              "imageOver" :{
                "inputType" : "uploader",
                "label" : "petite image",
                "domElement" : "imageOver",
                "docType": "image",
                "contentKey" : "slider",
                "itemLimit" : 1,
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "showUploadBtn": false,
                "endPoint" :"/subKey/imgOver",
                initList : <?php echo json_encode($initImageOver) ?>
              }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              console.log("save tplCtx",tplCtx);
              mylog.log("andrana data",data);
              mylog.log("andrana formData",formData);
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

