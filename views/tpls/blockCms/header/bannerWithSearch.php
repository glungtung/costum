<?php 
$keyTpl ="bannerWithSearch";
$paramsData = [ 
  "titre" => " <div style=\"text-align: center;\"><span style=\"font-size: xxx-large;\"><font color=\"#ffffff\" face=\"SharpSans-No1-Bold\">Cocity présentation</font></span></div>",
  "description" => "<div style=\"text-align: center;\"><span style=\"font-size: 25px; color: rgb(255, 255, 255);\"><br></span></div><div style=\"text-align: center;\"><span style=\"font-size: 25px; color: rgb(255, 255, 255);\">Votre territoire est un espace d'interaction Locale (citoyens, la biodiversité, acteurs socio-économiques)\nLa collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions.\nC'est un espace où on peut (se) poser des questions librement et construire une vision partagée afin d'apporter des solutions en commun.\nSoutenir les élu.e.s dans leur rôle de facilitateur.rice des dynamiques locales qui font converger les acteurs vers des objectifs communs\nAccompagner les grands enjeux de résilience des collectivités en facilitant l'adhésion et l'action de chacun à travers la visibilité de l'impact local de ses activités.\nun système vivant où toute vie y est pleinenemnt respecter</span></div>   "
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 
$initBg= Document::getListDocumentsWhere(
  array(
    "id"=> $blockKey,
    "type"=>'cms',
    "subKey"=> "background"
  ), "image"
);

$arrayBackground= [];
foreach ($initBg as $k => $v) {
  $arrayBackground[] =$v['imagePath'];
}
  //$allVille = PHDB::find(Organization::COLLECTION);
?>
<style type="text/css">

  #homeCocityPrez<?= $kunik?>  {
    overflow: hidden;
    position: relative;
  }
  #homeCocityPrez<?= $kunik?> form {
    width: 100%;
    max-width: 700px;
    padding-top: 3%;
    margin-left: 23%;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form {
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
    box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 30px;
    height: 50px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field {
    height: 68px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input {
    height: 100%;
    border: 0;
    display: block;
    width: 100%;
    padding: 10px 0;
    font-size: 16px;
    color: #000;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input.placeholder {
    color: #222;
    font-size: 14px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:-moz-placeholder {
    color: #222;
    font-size: 14px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input::-webkit-input-placeholder {
    color: #222;
    font-size: 14px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:hover, #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:focus {
    box-shadow: none;
    outline: 0;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap {
    -ms-flex-positive: 1;
    flex-grow: 1;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    background: #fff;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap input {
    -ms-flex-positive: 1;
    flex-grow: 1;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap .svg-wrapper {
    min-width: 60px;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    font-size: 23px;
    color: #0A96B5;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap svg {
    width: 36px;
    height: 36px;
    fill: #222;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap {
    min-width: 50px;
  }
  #costumActif<?= $kunik?> .smartgrid-slide-element .img-back-card {
    width: 100%;
    height: 300px;
    object-position: initial;
    float: left;
    object-fit: contain;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    overflow: hidden;
    border: 1px solid #ccc;
    background: #bdbdbd;
  }
  #costumActif<?= $kunik?> .smartgrid-slide-element .img-back-card .container-img-card  .fa {
    position: absolute;
    top: 36%;
    left: 50%;
    transform: translate(-50%,-50%);
    /* opacity: 0.2; */
    font-size: 110px;
    color: #fff;
  }
  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search {
    height: 100%;
    width: 100%;
    white-space: nowrap;
    font-size: 21px;
    color: #fff;
    border: 0;
    cursor: pointer;
    position: relative;
    z-index: 0;
    background: #0A96B5;
    transition: all .2s ease-out, color .2s ease-out;
    font-weight: 300;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:hover {
    background: #0A96B5;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:focus {
    outline: 0;
    box-shadow: none;
  }
  .initialiseOrga {
    background-color: #0A96B5;
  }
  .initialiseCocity{
    background-color: #0A96B5;
  }
  #homeCocityPrez<?= $kunik?> .caption{
    margin: 10% 10%;
  }
  #homeCocityPrez<?= $kunik?>{
    <?php  if (count($arrayBackground)!=0) { ?>
      background-image: url(<?= $arrayBackground[0] ?>); 
    <?php }else { ?>
      background-image: url(<?= Yii::app()->getModule("costum")->assetsUrl ; ?>/images/cocity/etang4-2.jpg); 
    <?php } ?>
    background-size: cover;
  }
  @media (max-width: 978px) {
    #homeCocityPrez<?= $kunik?> form {
      width: 100%;
      max-width: 500px;
      height: 40px;
    }
    #homeCocityPrez<?= $kunik?> .caption h1{
      font-size: 45px;
      margin-top: 10%;
    }

    .#homeCocityPrez<?= $kunik?> .caption p {
      font-size: 18px;
      margin-top: 13%;
    }
    #homeCocityPrez<?= $kunik?> form {
      margin-left: 0%;
    }
  }

</style>

<header id="homeCocityPrez<?= $kunik?>">
  <div class="caption"> 
    <h1 class="animated fadeInLeftBig ">
      <span class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titre" ><?= $paramsData["titre"]?></span>
    </h1>
    <div class="animated sp-cms-container fadeInRightBig   sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description"> <?= $paramsData["description"]?></div>
    <form id="formCocity">
      <div class="inner-form">
        <div class="input-field first-wrap">
          <div class="svg-wrapper">
            <i class="fa fa-search"> </i>              
          </div>
          <input type="text" id="nameCity" name="nameCity" placeholder="Taper votre ville ..." value="" onkeyup="cocityObj<?= $kunik?>.searchCity() " />
        </div>
        <div class="input-field second-wrap">
          <button onclick="cocityObj<?= $kunik?>.searchCity() " class="btn-search" type="button">
            <i class="fa fa-arrow-right"></i>
          </button>
        </div>
      </div>
    </form>
  </div>
</header>
<div id="costumActif<?= $kunik?>">

</div>
<script type="text/javascript">
  var allThem = {
    "alimentation" : {
      "name" : "Food",
      "icon" : "fa-cutlery",
      "tags" : [ 
      "agriculture", 
      "alimentation", 
      "nourriture", 
      "AMAP"
      ]
    },
    "santé" : {
      "name" : "Health",
      "icon" : "fa-heart-o",
      "tags" : [ 
      "santé"
      ]
    },
    "déchets" : {
      "name" : "Waste",
      "icon" : "fa-trash-o ",
      "tags" : [ 
      "déchets"
      ]
    },
    "transport" : {
      "name" : "Transport",
      "icon" : "fa-bus",
      "tags" : [ 
      "Urbanisme", 
      "transport", 
      "construction"
      ]
    },
    "éducation" : {
      "name" : "Education",
      "icon" : "fa-book",
      "tags" : [ 
      "éducation", 
      "petite enfance"
      ]
    },
    "citoyenneté" : {
      "name" : "Citizenship",
      "icon" : "fa-user-circle-o",
      "tags" : [ 
      "citoyen", 
      "society"
      ]
    },
    "Économie" : {
      "name" : "Economy",
      "icon" : "fa-money",
      "tags" : [ 
      "ess", 
      "économie social solidaire"
      ]
    },
    "énergie" : {
      "name" : "Energy",
      "icon" : "fa-sun-o",
      "tags" : [ 
      "énergie", 
      "climat"
      ]
    },
    "culture" : {
      "name" : "Culture",
      "icon" : "fa-universal-access",
      "tags" : [ 
      "culture", 
      "animation"
      ]
    },
    "environnement" : {
      "name" : "Environnement",
      "icon" : "fa-tree",
      "tags" : [ 
      "environnement", 
      "biodiversité", 
      "écologie"
      ]
    },
    "numérique" : {
      "name" : "Numeric",
      "icon" : "fa-laptop",
      "tags" : [ 
      "informatique", 
      "tic", 
      "internet", 
      "web"
      ]
    },
    "sport" : {
      "name" : "Sport",
      "icon" : "fa-futbol-o",
      "tags" : [ 
      "sport"
      ]
    }
  };
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>; 
  var  cocityObj<?= $kunik?> ={
    generateFiliere:function(dataThematic) {
      ajaxPost(
        null,
        '<?php Yii::app()->baseUrl; ?>/costum/filiere/generate',
        dataThematic,
        function(data){
        },
        function(error){
          toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
        },
        "json"
      )
    },
    createOrgaFiliere:function(params,orga,thematic) {
      mylog.log("filiereee22",orga);
      ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params,
        function(data){
          let referenceData = {
						"source":orga.map.source
					};

					referenceData.source.keys = referenceData.source.keys.concat([orga.map.slug]);
          
          var dataThematic = {"cocity":orga.map._id.$id, "ville":orga.map.name, "thematic":thematic.charAt(0).toUpperCase()+thematic.toLowerCase().slice(1),...data,...referenceData};
          mylog.log("ftftf",dataThematic);
          cocityObj<?= $kunik?>.generateFiliere(dataThematic);
          mylog.log("cndsjfnsdlfs",data);
          
        },
        function(){ 
          toastr.error( "Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur");
        },
        "json"
      );
    },
    // ********************** search a city**********************
    searchCity:function(){
      var nameCity =document.getElementById("nameCity").value;
      var data = {      
        "searchType" : ["organizations"],
        "notSourceKey": true,
        "fields" :["costum","ville"],
        "name" :nameCity

      }; 
      coInterface.showLoader("#costumActif<?= $kunik?>", "Recherche en cours");
      ajaxPost(
        null,
        baseUrl+"/" + moduleId + "/search/globalautocomplete",
        data,
        function(data){
          mylog.log("nomcity",data);
          var hsrc = '';
          if ( data.results.length == 0){
            hsrc += "<p class='text-center'>Il n'éxiste aucune cocity </p>";
            hsrc += '<div class=" text-center">';
            hsrc += '<a  class="initialiseCocity btn btn-default  "> Initialiser le costum pour la ville de '+nameCity;
            hsrc += '</a>';
            hsrc += '</div>';
          } else {
            hsrc += '<div class=" text-center">';
            hsrc += '<a  class="initialiseCocity btn btn-default  "> Initialiser le costum pour la ville de '+nameCity;
            hsrc += '</a>';
            hsrc += '</div>';
            $.each(data.results, function( index, value ) {
              var image = typeof value.profilImageUrl!= "undefined" ? '<img class="img-responsive img-profil-entity"  src="'+ value.profilImageUrl +'">' : '<i class="fa fa-image fa-2x"></i>';
              if (typeof value.costum == "undefined") {
                hsrc += "";
              } 
              if (typeof value.costum != "undefined" && value.costum.slug != "cocity") {
                hsrc += "";
              }
              if (typeof value.costum != "undefined" && value.costum.slug == "cocity") {
                if(typeof value.ville == "undefined"){
                  hsrc += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds classifieds">';
                  hsrc += '<div class="item-slide">';
                  hsrc += '<div class="entityCenter">';
                  hsrc += ' <a href="" class="pull-right" ';
                  hsrc += '<i class="fa fa-rocket bg-azure"></i> </a>';
                  hsrc += '</div>';
                  hsrc += '<div class="img-back-card">';
                  hsrc += '<div class="container-img-card">';
                  hsrc += image;
                  hsrc += ' </div>';
                  hsrc += '  <div class="text-wrap searchEntity">';
                  hsrc += '<div class="entityRight profil no-padding">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" class="entityName letter-turq" >';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<i class="fa fa-rocket"></i>  '+ value.name;

                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '<div class="slide-hover co-scroll">';
                  hsrc += '<div class="text-wrap">';
                  hsrc += '<h4 class="entityName">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" class=" letter-turq  " >';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<i class="fa fa-rocket"> </i> ' + value.name ;
                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</h4>';
                  hsrc += '<div class="entityType col-xs-12 no-padding">';
                  hsrc += '<span class="type">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</span>';
                  hsrc += '</div>';
                  hsrc += '<div class="desc">';
                  hsrc += '<div class="socialEntityBtnActions btn-link-content">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'"   class="btn btn-info btn-link btn-share-panel" >';
                  hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;"> Voir le costum de cette ville'; 
                  hsrc += '</font>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                }
              }
            });
          }
          $("#costumActif<?= $kunik?>").html(hsrc);
          $(".initialiseCocity").off().on("click",function(){
            mylog.log("");
            var dyfOrga={
              "beforeBuild":{
                "properties" : {
                  "name" : {
                    "label" : "Nom de votre ville", 
                    "inputType" : "text",
                    "placeholder" :"Nom de votre ville",
                    "value" :nameCity,
                    "order" : 1
                  },
                  "formLocality" : {
                    "label" : "Adresse de votre ville",
                    "rules" :{
                      "required" : true
                    }
                  },
                  "template" :{
                    "order" : 4,
                    "label" : "Choisir un template", 
                    "inputType" : "tplList",
                    list :{
                      "unnotremonde" : {
                        "photo" :"<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/tpl/unautremonde.jpg"
                      },
                      "cocity" : {
                        "photo" :"<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/tpl/cocity.png"
                      }
                    },
                    "rules" :{
                      "required" : true
                    }
                  },
                  "thematic":{
                    "inputType" : "tags",
                    "label" : "Sélectionner les filières que vous voulez dés la création de la page",
                    "values" : [
                      tradTags["food"],
                      tradTags["health"] ,
                      tradTags["waste"] ,
                      tradTags["transport"] ,
                      tradTags["education"],
                      tradTags["citizenship"],
                      tradTags["economy"],
                      tradTags["energy"],
                      tradTags["culture"],
                      tradTags["environment"],
                      tradTags["numeric"],
                      tradTags["sport"]
                    ],
                    "value" : [
                      tradTags["food"],
                      tradTags["health"] ,
                      tradTags["waste"] ,
                      tradTags["transport"] ,
                      tradTags["education"],
                      tradTags["citizenship"],
                      tradTags["economy"],
                      tradTags["energy"],
                      tradTags["culture"],
                      tradTags["environment"],
                      tradTags["numeric"],
                      tradTags["sport"]
                    ],
                    "order" : 3
                  }
                }
              },
              "onload" : {
                "actions" : {
                  "setTitle" : "Initialiser une ville",
                  "src" : {
                    "infocustom" : "<br/>Remplir le champ"
                  }          
                } 
              }   
            };
            dyfOrga.afterSave = function(orga){
              dyFObj.commonAfterSave(data, function(){
                var dataCity = {"cocity":"cocity", ...orga,"allCities":false};
                let cocityCoordonate = {};
                mylog.log("filiereee1",data);
                cocityCoordonate["address"] = orga.map.address;
                cocityCoordonate["geo"] = orga.map.geo;
                cocityCoordonate["geoPosition"] = orga.map.geoPosition;  
                if (notNull(orga.map.thematic) ) {
                  var thema = orga.map.thematic;
                  thematic = thema.split(",");
                  $.each(thematic , function(k,val) {
                    mylog.log("filiereee4",val);
                    var defaultTags = [];
                    if (typeof  allThem[val.toLowerCase()] != "undefined" && notNull(allThem[val.toLowerCase()]["tags"])) {
                      defaultTags = allThem[val.toLowerCase()]["tags"]; 
                    }   
                    var defaultName = orga.map.name +" "+ val;
                    var dataThem = {};
                    var params = {
                      name: defaultName,
                      collection : "organizations",
                      type: "Group",
                      role: "creator",
                      image: "",
                      tags: defaultTags,
                      ...cocityCoordonate,
                      addresses : undefined,
                      shortDescription: "Filière "+val+" "+orga.map.name
                    }
                    cocityObj<?= $kunik?>.createOrgaFiliere(params,orga,val);  
                    mylog.log("filiereee4",params); 

                  });

                }
                              
                ajaxPost(
                  null,
                  baseUrl+"/costum/cocity/generatecocity",
                  dataCity,
                  function(data){
                    window.location="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/"+orga.map.slug;
                  },
                  function(error){
                    toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
                  },
                  "json"
                  );
              });
            }
            dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"},null,dyfOrga);
          })
        }
      );
    }
  }
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    $("#formCocity").on('submit', function(event){ 
      event.preventDefault();
      var data = $(this).serialize();
      cocityObj<?= $kunik?>.searchCity();
      return false;      
    });
  });
  </script>