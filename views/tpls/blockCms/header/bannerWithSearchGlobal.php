<?php 
$keyTpl = "bannerWithSearchGlobal";
$paramsData = [
	"descriptionVille" => " <font color=\"#ffffff\" style=\"font-size: 25px;\"> Votre territoire est un espace d'interaction Locale (citoyens, la biodiversité, acteurs socio-économiques)\nLa collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions.\nC'est un espace où on peut (se) poser des questions librement et construire une vision partagée afin d'apporter des solutions en commun.\nSoutenir les élu.e.s dans leur rôle de facilitateur.rice des dynamiques locales qui font converger les acteurs vers des objectifs communs\nAccompagner les grands enjeux de résilience des collectivités en facilitant l'adhésion et l'action de chacun à travers la visibilité de l'impact local de ses activités.\nun système vivant où toute vie y est pleinenemnt respecter </font> "
    
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 

$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name", "description","descriptionVille","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl"]);



?>
<style type="text/css">
	#menuTopLeft a {
		text-decoration: none;

	}
	.headerCocity_<?= $kunik?> .contentCocity{
		margin-top: 10%;
		margin-left: 5%;
	}
	.headerCocity_<?= $kunik?> .contentCocity a:hover {
		background: transparent;
		color: white !important;
		border: 3px solid #F0FCFF;
	}
	.headerCocity_<?= $kunik?> .contentCocity p {
		margin-top: 5%;
	}
	.headerCocity_<?= $kunik?> .contentCocity .bouton{
		background-color: #fff;
		text-transform: uppercase;
		padding: 3%;
		border-radius: 10px;
		font-size: 20px;
		margin-top: 5%;
		margin-bottom: 15%;
		font-weight: bold;
	}

	
	@media (max-width: 978px) {
		#menuTopLeft .menu-btn-top:before {
			font-size: 18px;

		}
		.headerCocity_<?= $kunik?> .contentCocity{
			margin-top: 30%;
		}
		.headerCocity_<?= $kunik?> .contentCocity h1{
			font-size: 55px !important; 
			margin-top: 10%;
		}

		.headerCocity_<?= $kunik?> .contentCocity p {
			font-size: 18px !important;
		}
		
		.headerCocity_<?= $kunik?> .contentCocity .bouton{
			width: 39%;
			border-radius: 10px;
			height: 40px;
			margin-top: 7%;
			font-weight: 600;
			padding: 7px;
			margin-bottom: 10%;
			font-size: 18px !important;
		}
	}
</style>
<div class="headerCocity_<?= $kunik?> searchCocity_<?= $kunik?>" >
	<div class="contentCocity col-md-5" >
		<h1 class="title"><?= $parent["name"]?></h1>
		<div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="descriptionVille"  > <?= $paramsData["descriptionVille"]?> </div>
		<a href="#@<?= $parent["slug"]?>" class="btn lbh bouton title-5" >En savoir plus </a>
	</div>
</div>
<script type="text/javascript">	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$("#btn-search-<?= $kunik?>").click(function(){
			$( "#search-form<?= $kunik?>" ).submit();
			var str = $("#otherInput").val();
			location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
		});
		$( "#search-form<?= $kunik?>" ).submit(function( event ) {
			var str = $("#otherInput").val();
			location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
			event.preventDefault();
		});
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {
					"descriptionVille" : {
						"label" : "description de la ville",
						"inputType":"textarea",
						"markdown":"true",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.descriptionVille
					}
				},				
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
					
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
</script>