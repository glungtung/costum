<?php 
$keyTpl = "bannerAvecTitre";
$paramsData = [
	"title" => "Lorem Ipsum",
	"subtitle"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",

];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<?php 
  $latestLogo = [];

  $latestLogo = [];

  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"logo"
        ),"image"
    );

    foreach ($initImage as $key => $value) {
         $latestLogo[]= $value["imagePath"];
    }

?>
<style >
    #bg-homepage{
        width: 100%;
        border-top: 1px solid #ccc;
    }
    #header-wrapper {
	    position: relative;
	    padding: 0em 0em 0em 0em;
	    width: 100%;
	    height: 600px;
	    /*top: -50px;*/
	    text-align: center;
	}
	.header-logo{
	    text-align: center;
	}
	.header-logo img {
		height: 120px;
		width: auto;
	}

	.well {
	    min-height: 20px;
	    padding: 19px;
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
	    border: none;
	    border-radius: 0px;

	}
	#header-about {
	    background-color: transparent;
	    /*text-transform: none;
	    opacity: 0.8;
    	padding: 30px 70px;*/
    	position: absolute;
	    top: 50%;
	    left: 50%;
	    transform: translate(-50%, -50%);
	}
	.header-about-desc{
		font-weight: 700;
	}
	@media (max-width: 1399px ) {
		#header-about {
		    /*background-color: #0e0e0e;
		    padding: 20px 50px;*/
		}
		.header-logo{
			text-align: center;
		    /*padding-left: 50px;
		    padding-top: 80px;
		    padding-bottom: 40px;*/
		}
		.header-logo img {
			height: 100px;
			width: auto;
		}
	}
	@media (min-width: 1400px) {
		#header-wrapper {
		    height: 700px;
		}
		#header-about {
	    	/*padding: 30px 70px;*/
		}
	}
	
	@media (max-width: 767px){
		
		#header-about h1 {
		   font-size: 22px;
		}
		.header-about-desc{
			font-size: 18px;
			font-weight: 700;
		}
		#header-about {
		   /* background-color: #0e0e0e;
		    text-transform: none;
		    text-align: center;
		    margin-left: 5%;
		    margin-right: 5%;
		    width: 90%;
		    margin-bottom: 50px;
		    padding-bottom: 30px;
		    top: 60px;*/
		}
		#header-wrapper {
		    height: 320px;
		}
		.header-logo{
			padding-left: inherit;
		    padding-top: 20px;
		    padding-bottom: 20px;
		    text-align: center;
		}

		.header-logo{
			padding-left: inherit;
		    padding-top: 20px;
		    padding-bottom: 20px;
		    text-align: center;
		}
		.header-logo img {
			height: 50px;
			width: auto;
		}
	}
	
	/*L'HOMME ET L'ENVIRONNEMENT 
		a voulue relever le défi de montrer que lutter contre la pauvreté et preservation de l'environnement peuvent aller de paire.
	*/
</style>

<div id="header-wrapper" >

		<div class="header-logo">
			<a class="lbh-menu-app" href="#welcome">
				<img src="<?php echo !empty($latestLogo) ? $latestLogo[0] : ""; ?>">
			</a>
		</div>

	<div id="header-about" class="well col-md-6 col-sm-8 col-xs-12">
		<div class="header-about-desc  other sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitle">
			<?= $paramsData["subtitle"]?>
		</div>
		<p class="sp-text title img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></p>
	</div>

<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre bloc",
				"description" : "Personnaliser votre bloc",
				"icon" : "fa-cog",
				"properties" : {				
					"logo" : {
						"inputType" : "uploader",
						"label" : "logo",
						"showUploadBtn" : false,
						"docType" : "image",
						"itemLimit" : 1,
						"contentKey" : "slider",
						"order" : 9,
						"domElement" : "logo",
						"placeholder" : "image logo",
						"afterUploadComplete" : null,
						"endPoint" : "/subKey/logo",
						"filetypes" : [
						"png","jpg","jpeg","gif"
						],
						initList : <?php echo json_encode($initImage) ?>
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});

	});
	</script>