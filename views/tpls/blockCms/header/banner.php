<?php 
    $keyTpl ="banner";
    $paramsData = [
        "logo" => null,
        "logoPosition" => "",
        "height" => "500"
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

    $initLogo = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'logo',
        ), "image"
    );

    $logoImg = [];

    foreach ($initLogo as $key => $value) {
        array_push($logoImg, $value["imagePath"]);
    }

    $paramsData["logo"] = $logoImg;
    

    if($paramsData["logoPosition"]=="bottom-left"){
        $left = "10%";
        $top = "auto";
        $bottom = "10%";
    }else if($paramsData["logoPosition"]=="top-left"){
        $left = "10%";
        $top = "15%";
        $bottom="auto";
    }else{
        $left = "35%";
        $top = "30%";
        $bottom = "auto";
    }
?>


<style>
    .header<?= $kunik?>{
        width: 101%;
        height: <?= $paramsData['height']."px"?>;
        background-size: cover;
        display: block;
        overflow: hidden;
    }

    .logofn{
        position: absolute;
        left: <?=$left ?>;
        top: <?=$top ?>;
        bottom: <?=$bottom ?>;
    }

    .row{
        margin:0px;
    }

    .img-responsive-<?= $kunik?>{
        width: 100%
    }
</style>

<div class="header<?= $kunik?> row content-<?= $kunik?>">
    <div class="logofn col-md-3 col-sm-5 col-xs-6">
        <?php if(count($logoImg)>0){ ?>
		<img src="<?php echo $paramsData['logo'][0]; ?>" class="img-responsive img-responsive-<?= $kunik?>">
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready( function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            "properties" : {
                "logo" :{                    
                   "inputType" : "uploader",
                   "docType": "image",
                   "contentKey":"slider",
                   "endPoint": "/subKey/logo",
                   "domElement" : "logo",
                   "filetypes": ["jpeg", "jpg", "gif", "png"],
                   "label": "Logo :",
                   "itemLimit" : 1,
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initLogo) ?> 
                },
                "logoPosition" : {
                    "class":"form-control",
                    "inputType" : "select",
                    "label" : "Position du logo",
                    "options":{
                        "top-left":"Haut gauche",
                        "center":"Centre",
                        "bottom-left":"Bas gauche"
                    },
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.logoPosition
                },
                "height" : {
                  "inputType" : "text",
                  "label" : "Hauteur de la bannière (%)",
                  value :  sectionDyf.<?php echo $kunik ?>ParamsData.height
                }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

    });
</script>
