<?php
    $keyTpl = 'calendrier_federe';
    $paramsData = [];
    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e])) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
    $scripts_and_css = [];
    HtmlHelper::registerCssAndScriptsFiles($scripts_and_css, Yii::app()->request->baseUrl);
?>
<div class="container">
    <div id="<?= $kunik ?>calendrier_federe"></div>
</div>
<script>
    $(function() {
        var calendar_container = $('#<?= $kunik ?>calendrier_federe');
        calendar_container.fullCalendar({
            events: [],
            selectable: true
        });
        mylog.log('Rinelfi calendar', calendar_container);
    })
</script>