<style type="text/css">
  .section-home.elt-section{
    padding: 0px 0px 18px 0px;
  }
  .section-home button{
    visibility: hidden;
    transition: all;
  }
  .section-home:hover button{
    visibility: visible;
  }
  .section-home.elt-section .content-img{
    height:180px;
    overflow-y: hidden;
    font-size: large;
    justify-content: center;
    padding-top: auto;
    padding-bottom: auto;
  }
  .section-home.elt-section .header-section h3{
    font-size: 20px;
    margin-bottom: 20px !important;
  }
  .section-home.elt-section .header-section hr{
    margin-left: 0px;
    margin-top: 0px;
    bottom: 5px;
    margin-bottom: 7px;
  }
  #sub-doc-page .section-home .header-section{
    line-height: inherit !important;
    margin-bottom: 0px;
  }
  .section-home.elt-section .textExplain{
    font-size: 18px;
  }
  .support-section .text-explain {
    font-size: 22px;
  }
  .text-explain, .text-partner {
    color: #555;
  }
  #sub-doc-page{
    margin-top: 0px;
  }
  @media (min-width: 768px) {
    .cont-desc{
      height: 100px;
    }
    .section-home.elt-section .textExplain{
      font-size: 18px;
      display: -webkit-box;
      -webkit-line-clamp: 4;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
      padding-top: 10px;
    }
  }
  .ressourcePacte .text-explain{
    line-height: 20px;
    max-height: inherit;
    font-size: 16px;
    white-space: pre-line;
    word-wrap: break-word;
  }
  .ressourcePacte{
    padding-left: 20px 10px;
    margin-bottom: 20px;
  }
  .ressourcePacte:hover{
    background-color: rgba(211, 211, 211, 0.2);
  }
  .ressourcePacte h3{
    font-size: 20px !important;
    color: #2E388D !important;
  }
  .title-section {
    font-size: 30px;
    color: #2E388D;
  }
  .header-section{
    margin-bottom: 20px;
  }
  .display-1{
    font-size: 50pt;
    color: #999;
    padding: 1em;
    border-radius: 50%;
    background-color: #ddd;
  }
  .action<?=$kunik?>{
    position: absolute;
    bottom: 0px;
    right: 10px;
  }
  .section-home .header-section hr {
    border: 2px solid #2E388D;
    margin-bottom: 2px;
    margin-left: 5px !important;
    margin-top: 10px;
    width: 5%;
  }
</style>

<?php 
  $paramsData = [
    "categories" => [],
    "blockIntroduction" => "La boite à outils",
    "titlesColor" => "#2E388D",
    "accessByRole" => [],
    "main" => array(
      "accessByRoles" => []
    )
  ];

  if(isset($blockCms)){
      foreach($paramsData as $e => $v) {
        if( isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
      }
  }

  $categoryArray = array();
  $lastIndex = 0;

  $arrFileImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey
    ), "image"
  );

    $cardImage = array();
              
    foreach ($arrFileImage as $k => $v) {
      $cardImage[$v["subKey"]][] = $v;
    }
?>

<div id="sub-doc-page">
  <div class="container-fluid" >
    <h3 class="sp-text text-center" data-id="<?= $blockKey ?>" data-field="blockIntroduction">
    <?= $paramsData["blockIntroduction"] ?></h3></div>
  <div class="col-xs-12 text-center margin-bottom-20">
      <?php if(Authorisation::isInterfaceAdmin() || Authorisation::hasRoles(array("roles"=>$paramsData["main"]["accessByRoles"]))){ ?>
        <button class="btn btn-primary btn-open-form-ressource" style="display: inline-block;padding: 10px 25px;border-radius: 4px;">
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin">
                      Ajouter une ressource
                    </h4>
                </div>
            </div>
        </button>
      <?php } ?>
    </div>
    <div class=" col-lg-12 col-md-12 col-xs-12 col-sm-12">
      <div class="col-xs-12">
          <?php foreach ($paramsData["categories"] as $catkey => $catvalue){
              if(isset($catvalue) && $catvalue!=""){
                $categoryArray[$catvalue["blockTitle"]]=$catvalue["blockTitle"];
                $lastIndex = (int)$catkey;
                $image = (isset($cardImage[$catkey]) && isset($cardImage[$catkey][count($cardImage[$catkey])-1]))?$cardImage[$catkey][count($cardImage[$catkey])-1]['imagePath']:"";
            ?>
            <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
              <div class="col-xs-12 shadow2 section-home elt-section margin-bottom-20">
                  <a href="javascript:;" class="btn-category" data-id="<?= $blockKey ?>" data-key="<?= $catkey ?>" title="<?= $catvalue['blockTitle'] ?>" data-category="<?= $catvalue['blockTitle'] ?>" >
                    <?php if(empty($catvalue["blockBgColor"]) || $catvalue["blockBgColor"]==""){ ?>
                        <div class="content-img">
                        <img src="<?= $image ?>" class="img-responsive" style="width: 100%; height: 100%; object-fit: cover" />
                        </div>
                      <?php }else{ ?>
                      <div class="content-img" style="background-color:<?= $catvalue["blockBgColor"]; ?>">
                        <div style="position:absolute; top:5px; font-weight:bolder">
                          <h1 class="text-center" style="color:<?= $catvalue["blockTitleColor"]??"white" ?>"><?= $catvalue["blockTitle"]??""; ?></h1>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 cont-desc">
                        <!--h6><?= $catvalue["blockTitle"]; ?></h6-->
                        <span class="textExplain">
                          <?= $catvalue["blockDescription"]??""; ?>
                        </span>
                    </div>
                  </a>
                  <?php if(Authorisation::isInterfaceAdmin()){ ?>
                    <span class="action<?= $kunik ?>">
                      <button class="btn btn-primary btn-edit-category" data-id="<?= $blockKey ?>" data-collection="cms" data-key="<?= $catkey ?>" data-image='<?php echo json_encode($cardImage[$catkey]??[]) ?>'><i class="fa fa-edit"></i></button>
                      <button class="btn btn-danger btn-remove-category" data-id="<?= $blockKey ?>" data-collection="cms" data-action="remove" data-key="<?= $catkey ?>"><i class="fa fa-trash"></i></button>
                    </span>
                  <?php } ?>
              </div>
            </div>
          <?php } } ?>
          <div id="ressources" class="col-xs-12"></div>
          <?php if(Authorisation::isInterfaceAdmin() || Authorisation::hasRoles(array("roles"=>$paramsData["main"]["accessByRoles"]))){ ?>
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
            <div class="col-xs-12 section-home elt-section text-center margin-bottom-20" style="border:2px dashed #ddd; display:block; height:100%">
              <a href="javascript:;" class="btn-new-category" title="Nouveau Catégorie" data-key="<?= $lastIndex+1 ?>" data-collection="cms" data-id="<?= $blockKey ?>" data-category="new">
                <div class="text-center padding-top-20">
                    <i class="fa fa-plus-circle display-1"></i>
                </div>
                <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0">
                    <span class="textExplain" style="text-transform:uppercase">
                      Ajouter une dossier ou Categorie
                    </span>
                </div>
              </a>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
</div>

<div class="portfolio-modal modal fade in no-padding" id="modal-ressources-by-categories" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="sp-cms-container text-left">
          <div class="row">
            <div id="contentRessources" class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 margin-top-10"></div></div>
        </div>
    </div>
</div>

<script type="text/javascript">
  
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

  jQuery(document).ready(function() {
    var ressourcesDynForm = {
      "jsonSchema": {
        "title" : "Ajouter une ressource",
        "description" : "nouvelle ressource",
        "icon" : "fa-bullhorn",
        "properties": {
          "name": {
              "label": "Titre de la ressources",
              "placeholder": "Titre de la ressources"
          },
          "documentation": {
              "inputType": "uploader",
              "label": "Ajouter des ressources (document pdf, image et vidéo mois de 5Mo)",
              "showUploadBtn": false,
              "docType": "file",
              "contentKey": "file",
              "order": 8,
              "domElement": "documentationFile",
              "placeholder": "Document",
              "afterUploadComplete": null,
              "template": "qq-template-manual-trigger",
              "filetypes": [
                  "pdf", "png", "jpeg", "jpg", "mp4", "mp3", "odt", "doc","docx", "ppt", "pptx"
              ]
          },
          "category": {
              "order": 9,
              "rules":{
                "required":true
              },
              "label": "Choisissez la catégorie de la ressource",
              "inputType": "select",
              "placeholder": "Ajouter une catégorie de ressource",
              "options": <?= json_encode($categoryArray) ?>
          },
          "description": {
              "inputType": "textarea",
              "markdown": true,
              "label": "Contenue de la ressource"
          },
          "tags": dyFInputs.tags(),
          "isPublic":{
            "inputType" : "checkboxSimple",
            "label" : "A Dupliquer dans la boîte à outils pour tous",
            "params" : {
                "onText" : trad.yes,
                "offText" : trad.no,
                "onLabel" : trad.yes,
                "offLabel" : trad.no
            },
            "checked" : false
          }
      },
      "onload": {
        "actions": {
          "setTitle": "Ajouter une ressource",
          "hide": {
            "imageuploader": 1,
            "typeselect": 1,
            "locationlocation": 1,
            "urlsarray": 1,
            "breadcrumbcustom": 1
          }
        }
      },
      beforeSave : function(){
        if(($("#type").val() == "other") && $("#customType").val() !== ""){
          $(".typehidden").remove()
          $("#customType").attr("name", "type")
        }else{
          $(".customTypetext").remove()
        }
	    },
	    beforeBuild : function(){
	    	dyFObj.setMongoId('poi', function(){
	    		uploadObj.gotoUrl = (contextData != null && contextData.type && contextData.id ) ? "#page.type."+contextData.type+".id."+contextData.id+".view.directory.dir.poi" : location.hash+"?preview=poi."+uploadObj.id;
	    	});
	    },
		  afterSave : function(data, callB){
			  mylog.log("afterSave poi", data, typeof callB);
			  dyFObj.commonAfterSave(data, function(){
          if(typeof callB =="function" ){
            callB();
          }
          urlCtrl.loadByHash( (uploadObj.gotoUrl) ? uploadObj.gotoUrl : location.hash );
        });
	    },
	    canSubmitIf : function () {
	    	 return ( $("#ajaxFormModal #type").val() ) ? true : false ;
	    },
      save : function (data) {
        tplCtx = data;
        tplCtx.collection = "poi";
        tplCtx["type"] = "doc";
        tplCtx["parent"] = {};
        tplCtx["parent"][costum.contextId] = {
          "name": costum.title,
          "type":costum.contextType
        };
      
        $.each( ressourcesDynForm.jsonSchema.properties , function(k,val) { 
            tplCtx[k] = $("#"+k).val();    
            if(k=="tags"){
              tplCtx[k]=$("#"+k).val().split(",")
            }
        });
        
        ajaxPost(
          null,
          baseUrl+"/"+moduleId+"/element/save", 
          tplCtx,
          function(params){
            dyFObj.commonAfterSave(params,function(){
              toastr.success("L'élément a été bien ajouté");
              $("#ajax-modal").modal('hide');
              //urlCtrl.loadByHash(location.hash);
              });
          },
          function(error){},
          "json"
        );
      }
    }
  }

  $(".btn-open-form-ressource").off().on("click",function(){
    let user = {};
    user[userId] = {"type":userConnected.collection, "name": userConnected.name, "profilThumbImageUrl":userConnected.profilMediumImageUrl};
    dyFObj.openForm( ressourcesDynForm, null, {parent:user})
  });

  $(".btn-category").off().on("click", function(){
      getByCategory($(this).data("category"));
  });

  function getByCategory(category){
    let ressourceFilter = {
      "category": category,
      "source.keys": costum.contextSlug,
      <?php if(count($paramsData["accessByRole"]) == 0 || $paramsData["accessByRole"]=="false"){ ?>
      "source.keys": costum.contextSlug,
      <?php } ?>
      <?php if(count($paramsData["accessByRole"]) == 0 || $paramsData["accessByRole"]=="true"){ ?>
      //"creator" : userId
      <?php } ?>
    };
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
    console.log("app config", appConfig);

    ajaxPost(
      null,
      baseUrl+"/co2/search/globalautocomplete", 
      {
        searchType : ["poi"],
        fields : ["name","tags", "type"],
        indexStep: "0",
        filters: ressourceFilter,
        sort:{updated:1}
      },
      function(response){
        $("#contentRessources").empty();
        let html = '';
        if(typeof response.results != "undefined" && Object.keys(response.results).length>0){
          html = `<div class="col-xs-12">`;
            
            /*if(costum.isCostumAdmin){
                html+=`
                  <a href="javascript:;"  class="btn-open-form-ressource text-purple pull-right" style="font-size:16px;">
                       <i class="fa fa-plus-circle"></i> Ajouter une ressource (admin)
                  </a>`;
            }*/
            
          html+=`<div class="padding-top-50 row">
            <div class="col-xs-12 support-section section-home no-padding">
              <div class="col-xs-12 header-section no-padding margin-bottom-20">
                <h3 class="title-section col-xs-12 no-padding margin-bottom-10">
                  ${category}
                </h3>
                <hr>
              </div>
              <div class="col-xs-12 no-padding">`;
           
          $.each(response.results, function(objId, ressource){
            html+=`
              <div class="ressourcePacte col-xs-12 shadow2">
                <a href="#page.type.poi.id.${objId}" class="lbh-preview-element">
                  <h3 class="sub-header-section col-xs-12"> <i class="fa fa-file-text-o margin-right-10"></i> ${ressource.name}</h3>
                </a>
              </div>`;
          });

          html+=`</div>
            </div>`;
        }else{
          html = `<div class="text-center padding-top-50">
            <i class="fa fa-tags display-1"></i>
              <h4 class="margin-top-50" style="line-height:2">Pour le moment, il n'y a pas de ressources <br/> dans cette categorie.</h4>
            </div>`;
        }
        $("#contentRessources").append(html);
        coInterface.bindLBHLinks();
        $("#modal-ressources-by-categories").modal("show")
      },
      null,
      "json"
    );
  }

  $(document).on("click",".edit<?php echo $kunik?>Params",function(event) { 
      tplCtx["id"] = $(this).data("id");
      tplCtx["collection"] = $(this).data("collection");
      tplCtx["path"] = "allToRoot";
      sectionDyf.<?= $kunik ?>Params = {
        "jsonSchema" : {
          "title" : "Configurer votre section",
          "description" : "Personnaliser votre section",
          "icon" : "fa-cog",
          "properties" : {
              "accessByRoles" : {
                "label" : "Configurer l'accès par rôles :",
                "inputType" : "tags"
              }
          },
          beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
          },
          save : function (data) {
            tplCtx.value = {};
            tplCtx.value["main"] = sectionDyf.<?php echo $kunik ?>ParamsData.main;
            
            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
              tplCtx.value.main[k] = $("#"+k).val();
            });

            dataHelper.path2Value( tplCtx, function(params) {
              toastr.success("Les paramètres ont été enregistrés");
              urlCtrl.loadByHash(location.hash);
            });
          }
        }
      };
      dyFObj.openForm(sectionDyf.<?= $kunik ?>Params, null, sectionDyf.<?= $kunik ?>ParamsData.main);
    });

  /********* dynform category *******************/
  let getDynformSchema = function(categoryData){
    let dynformSchema = {
      "jsonSchema" : {
        "title" : "Gerer vos dossier/catégorie",
        "description" : "Personnaliser dossier/catégorie",
        "icon" : "fa-cog",
        "properties" : {
          "cardImage" :{
              "inputType" : "uploader",
              "label" : "Image :",
              "docType": "image",
              "contentKey" : "slider",
              "endPoint": "/subKey/"+(categoryData.key),
              "domElement" : categoryData.key,
              "filetypes": ["jpeg", "jpg", "gif", "png"],
              "showUploadBtn": false,
              initList: categoryData.image
          },
          "blockBgColor":{
            "inputType": "colorpicker",
            "label":"couleur d'arrière plan :",
          },
          "blockTitle" :{
              "label" : "Titre",
              "class" : "form-control"
          },
          "blockTitleColor":{
            "inputType": "colorpicker",
            "label":"couleur du titre :",
          },
          "blockDescription" :{
              "inputType" : "textarea",
              "markdown": true,
              "label" : "Description :",
              "class" : "form-control",
              "rules":{
                "required":true
              }
          }/*,
          "accessByRole" : {
            "label" : "Les rôles qui ont accès au ressources",
            "inputType" : "tags"
          }*/
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {
          tplCtx.value = {};
          tplCtx.value["categories"] = sectionDyf.<?php echo $kunik ?>ParamsData.categories;
          tplCtx.value.categories[categoryData.key] = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value.categories[categoryData.key][k] = $("#"+k).val();
          });

          tplCtx.value.categories[categoryData.key]["subKey"] = categoryData.key;

          if(typeof tplCtx.value == "undefined"){
            toastr.error('value cannot be empty!');
          }else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("la catégorie a été bien ajoutée");
                  $("#ajax-modal").modal('hide');
                  dyFObj.closeForm();
                  urlCtrl.loadByHash(location.hash);
                });
            });
          }
        }
      }
    };
    return dynformSchema;
  }

  $(".btn-config-category, .btn-new-category, .btn-edit-category, .btn-remove-category").off().on("click", function(){
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = "allToRoot";
    if(typeof $(this).data("action")=="undefined" || $(this).data("action")!="remove"){
      sectionDyf.<?php echo $kunik ?>Params = getDynformSchema($(this).data());
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData.categories[$(this).data("key")]);
    }else{
      tplCtx.path = "categories."+$(this).data("key");
      dataHelper.unsetPath(tplCtx, function(params){
        toastr.info("la catégorie a été retiré");
        urlCtrl.loadByHash(location.hash);
      });
    }
  });
});
  
</script>