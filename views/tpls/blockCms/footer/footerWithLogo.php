<?php 
$keyTpl = "footerWithLogo";
$paramsData = [];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$editeur = isset($blockCms["mentionLegal"]["editeur"])?$blockCms["mentionLegal"]["editeur"]:"";
$hebergeur = isset($blockCms["mentionLegal"]["hebergeur"])?$blockCms["mentionLegal"]["hebergeur"]:"";
?>
<style type="text/css">
	.footer_<?= $kunik?>{
		margin-top: 1%;
		width: 100%;
		height: 100px;
		background-color: #f5833c;
	}
	.footer_<?= $kunik?> .f_social{
		padding: 1%;
	}
	.footer_<?= $kunik?> .f_social a{
		color: #e2e2e2;
		font-size:  50px;
		text-decoration: none;
		padding: 3%;
	}
	.footer_<?= $kunik?> .f_social a img{
		width: 70px;
    	height: 70px;
	}
	.btn-edit-delete-<?= $kunik?>{
		display: none;
		z-index: 9999;
	}
	.footer_<?= $kunik?>:hover .btn-edit-delete-<?= $kunik?>{
		display: block;
		position: absolute;
		top:40%;
		left: 50%;
		transform: translate(-50%,-50%);
	}
	.<?= $kunik?> .logo-footer-co {
   	 width: 10%;
	}
	.<?= $kunik?> .logo-footer-openaltas {
	    width: 20%;
	}
	.<?= $kunik?> .logo-footer {
	    width: 10%;
	    margin-left: 2%;
	}
	.<?= $kunik?> {
		color: black;
		margin-bottom: 2%;
	}
	@media (max-width: 414px) {
		.footer_<?= $kunik?>{
			margin-top: 1%;
			width: 100%;
			height: 65px;
			background-color: #f5833c;
		}
		.footer_<?= $kunik?> .f_social a img{
			width: 40px;
	    	height: 40px;
		}
		.footer_<?= $kunik?> .f_social a{
			text-decoration: none;
			padding: 3%;
		}
		.footer_<?= $kunik?> .f_social{
			padding: 0%;
		}
	}


	
	@media screen and (min-width: 1500px){
		.footer_<?= $kunik?>{
			margin-top: 1%;
			width: 100%;
			height: 140px;
			background-color: #f5833c;
		}
		.footer_<?= $kunik?> .f_social a img{
			width: 100px;
	    	height: 100px;
		}
		.footer_<?= $kunik?> .f_social{
			padding: 1%;
		}
		.logo-footer-co {
	   	 width: 13%;
		}
		.logo-footer-openaltas {
		    width: 17%;
		}
		.logo-footer {
		    width: 8%;
		     margin-left: 2%;
		}
	}
	@media (max-width: 768px) {
	 	.footer_<?= $kunik?>{
			margin-top: 1%;
			width: 100%;
			height: 65px;
			background-color: #f5833c;
		}
		.footer_<?= $kunik?> .f_social a img{
			width: 40px;
	    	height: 40px;
		}
		.footer_<?= $kunik?> .f_social{
			padding: 0%;
		}
		.footer_<?= $kunik?> .f_social a{
			padding: 1%;
		}
		.<?= $kunik?> {
			margin-bottom: 6%;
		}
		.<?= $kunik?> span {
		 	font-size: 10px;
		 }
	 }	 
	 
	.modal_<?=$kunik?> .modal-dialog-centered {
		top:10%;
		background: #eee;
	}
	.modal_<?=$kunik?> .modal-dialog-centered .modal-header {
		background : #327753;
		color: white;
	}
	
	.modal_<?=$kunik?> .modal-dialog-centered .modal-content .modal-body p{
		text-transform: none;
		font-size: 15px;
		color: #000;
	}
	.modal_<?=$kunik?> .modal-dialog-centered .modal-content .modal-body h4{
		text-transform: none;
		color: #f5833c;
	}
	.modal_<?=$kunik?> .modal-dialog-centered .modal-footer button{
		background: #327753;
		color: white;
	}
	.editEditeur<?= $kunik ?>:hover:after {
	  content: 'Mettre à jour éditeur';
	}
	.editHeb<?= $kunik ?>:hover:after {
	  content: 'Mettre à jour hebergeur';
	}
	.mention<?= $kunik?>{
		padding: 10px;
		cursor: pointer;
	}
</style>

<footer class="<?= $kunik?>">
	<div class="footer_<?= $kunik?> text-center">
		<div class="f_social">
			<a class="text-center" href=" https://web.facebook.com/AssociationKosasa/?_rdc=1&_rdr"  >
				<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/logo-facebook.png">
			</a>
			<a class="text-center" href=" https://www.youtube.com/channel/UCEbon8cfMwXuZMB2V2W5wPA/featured?view_as=subscriber" >
				<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/logo-youtube.png">
			</a>
			<a class="text-center" href="https://www.instagram.com/associationkosasa/" >
				<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/logo-instagram.png ">
			</a>
		</div>
	</div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12  col-footer col-footer-step dfooter" >
            <div class="col-xs-12 col-sm-6">
            <span>Logiciel libre </span>
            <a href="https://communecter.org" target="_blank"> 
            	<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/assoKosasa/partenaire/logo-co.png" class="logo-footer-co">
            </a>
            <span>Propulsé par</span>
             
            <a href="http://www.open-atlas.org/" target="_blank"> 
            	<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/assoKosasa/partenaire/logo-openatlas.png" class="logo-footer-openaltas">
            </a>
            <a type="button" class="mention<?= $kunik?>" data-toggle="modal" data-target="#exampleModalCenter">
            	Mention légal
            	
            </a>
            </div>

            <div class="col-xs-12 col-sm-6" style="text-align: right; font-size: 12px;">
           <span>En partenariat avec</span>   
               <a href="https://www.regionreunion.com/" target="_blank"> 
               	<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/assoKosasa/partenaire/logo-region.png" class="logo-footer">
               </a>
                <a href="https://www.tco.re" target="_blank">
                	<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/assoKosasa/partenaire/logo-tco-accueil.png" class="logo-footer"> 
                </a>
            </div>
            
        </div>
    </div>
</footer>

<div class="modal fade modal_<?=$kunik?>" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title text-center" id="exampleModalLongTitle">Mention légal</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php 
      	$nameEditeur = isset($editeur["name"])?$editeur["name"]:"";
      	$link = isset($editeur["link"])?$editeur["link"]:"";
      	$paysEditeur = isset($editeur["address"]["level1Name"])?$editeur["address"]["level1Name"]:"";
      	$cpEditeur = isset($editeur["address"]["postalCode"])?$editeur["address"]["postalCode"]:"";
      	$villeEditeur= isset($editeur["address"]["addressLocality"])?$editeur["address"]["addressLocality"]:"";

      	$nameHeb = isset($hebergeur["name"])?$hebergeur["name"]:"";
      	$paysHeb = isset($hebergeur["address"]["level1Name"])?$hebergeur["address"]["level1Name"]:"";
      	$cpHeb = isset($hebergeur["address"]["postalCode"])?$hebergeur["address"]["postalCode"]:"";
      	$villeHeb= isset($hebergeur["address"]["addressLocality"])?$hebergeur["address"]["addressLocality"]:"";
      	$telEditeur = isset($editeur["tel"])?$editeur["tel"]:"";
      	$siretEditeur = isset($editeur["siret"])?$editeur["siret"]:"";
      	$emailEditeur = isset($editeur["email"])?$editeur["email"]:"";
      	$nameHebergeur = isset($hebergeur["name"])?$hebergeur["name"]:"";
       ?>
      <div class="modal-body">
      	<p class="text-center">Nous portons à la connaissance des utilisateurs et visiteurs du site : <?= $link ?> les informations suivantes :</p>
      	<h4 style="">Editeur 
      	<a href="javascript:;" role="button" class="btn btn-secondary popover-test editEditeur<?= $kunik ?>"> <i class="fa fa-pencil"></i></a>
		<p>
			Le site 
			<a href="<?= $link ?>" target="_blank">
				<?= $link?>
			</a>&nbsp;est la propriété exclusive de&nbsp;
			<strong><?= $nameEditeur ?></strong>
		</p>
		<p>
			<?= $nameEditeur ?> est située à 
			<?= $paysEditeur." ".$cpEditeur ."  ".$villeEditeur?> 
		</p>
		<p>SIRET :<?= $siretEditeur?></p>
		<p>Adresse de courrier électronique  : <?= $emailEditeur?> </p>
		<h4 style="">Hébergement 
			<a href="javascript:;" role="button" class="btn btn-secondary popover-test editHeb<?= $kunik ?>">
				<i class="fa fa-pencil"></i>
				
			</a>
		</h4>
		<p style="text-align:justify">Le site est hébergé <?= $nameHeb." ". $paysHeb."  ".$cpHeb." ".$villeHeb?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
        </button>        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		 sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configurer la section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",
                "properties" : { 

                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
							// urlCtrl.loadByHash(location.hash);
                      });
                    } );
                  }
              }
            }
        };
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        });
		$(".editEditeur<?= $kunik ?>").click(function() {  
			var keys = Object.keys(<?php echo json_encode($content); ?>);
			var	lastKey = 0; 
			if (keys.length!=0) 
				var	lastKey = parseInt((keys[(keys.length)-1]), 10);
			var tplCtx = {};
			tplCtx.id = "<?= $blockKey ?>";
			tplCtx.collection = "cms";

			tplCtx.path = "content."+(lastKey+1);
			var obj = {
					subKey : (lastKey+1),
					title : 		$(this).data("title"),
					description:    $(this).data("description"),
					titlecolor:     $(this).data("titlecolor"),
					textcolor:      $(this).data("textcolor")
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Modifier le mention légal",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" :{
						name : {
							label : "Nom de la société",
							"inputType" : "text",
							value:"<?= $nameEditeur?>"
						},
						link :{
							label : "LIen du site",
							"inputType" : "text",
							value:"<?= $link?>"
						},						
						addressSociete : dyFInputs.formLocality("Adresse de la société", "Adresse de la société"),					
						location : dyFInputs.location,
						tel : {
							label : "Téléphone de la société",
							inputType : "text",
							value:"<?= $telEditeur?>"
						},
						siret : {
							label : "Numéro de SIRET",
							inputType : "text",
							value:"<?= $siretEditeur?>"
						},
						email : {
							label : "Email de la société	",
							inputType : "text",
							value:"<?= $emailEditeur?>"
						},
 			
					},
					beforeBuild : function(){
		               uploadObj.set("cms","<?= $blockCms['_id'] ?>");
		            },
		            save : function (data) {  		              
		              	tplCtx.id = "<?= $blockCms['_id'] ?>";
				      	tplCtx.collection = "cms";
				      	tplCtx.path = "mentionLegal.editeur";
				      	tplCtx.value = {};
		              	$.each( activeForm.jsonSchema.properties , function(k,val) { 
		              		if(val.inputType == "formLocality"){
                              tplCtx.value[k] = getArray('.'+k+val.inputType);
                              mylog.log(tplCtx.value[k]);
                            }
                            else{
                              tplCtx.value[k] = $("#"+k).val();
                              mylog.log(tplCtx.value[k]);
                            }
		              	});
		                if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["coord"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }
		              mylog.log("save tplCtx",tplCtx);

		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		            }
		            }
				}
				};          
				dyFObj.openForm( activeForm );
			});	
	$(".editHeb<?= $kunik ?>").click(function() {  
			var keys = Object.keys(<?php echo json_encode($content); ?>);
			var	lastKey = 0; 
			if (keys.length!=0) 
				var	lastKey = parseInt((keys[(keys.length)-1]), 10);
			var tplCtx = {};
			tplCtx.id = "<?= $blockKey ?>";
			tplCtx.collection = "cms";

			tplCtx.path = "content."+(lastKey+1);
			var obj = {
					subKey : (lastKey+1),
					title : 		$(this).data("title"),
					description:    $(this).data("description"),
					titlecolor:     $(this).data("titlecolor"),
					textcolor:      $(this).data("textcolor")
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "Modifier le mention légal",
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" :{
						name : {
							label : "Nom de l’hébergeur du site",
							"inputType" : "text",
							value:"<?= $nameHebergeur?>"
						},
												
						addressSociete : dyFInputs.formLocality("Adresse de l’hébergeur du site", "Adresse de l’hébergeur du site"),					
						location : dyFInputs.location					
 			
					},
					beforeBuild : function(){
		               uploadObj.set("cms","<?= $blockCms['_id'] ?>");
		            },
		            save : function (data) {  		              
		              	tplCtx.id = "<?= $blockCms['_id'] ?>";
				      	tplCtx.collection = "cms";
				      	tplCtx.path = "mentionLegal.hebergeur";
				      	tplCtx.value = {};
		              	$.each( activeForm.jsonSchema.properties , function(k,val) { 
		              		if(val.inputType == "formLocality"){
                              tplCtx.value[k] = getArray('.'+k+val.inputType);
                              console.log(tplCtx.value[k]);
                            }
                            else{
                              tplCtx.value[k] = $("#"+k).val();
                              console.log(tplCtx.value[k]);
                            }
		              	});
		                if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["coord"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }
		              mylog.log("save tplCtx",tplCtx);

		              if(typeof tplCtx.value == "undefined")
		                toastr.error('value cannot be empty!');
		              else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //  urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
		            }
		            }
				}
				};          
				dyFObj.openForm( activeForm );
			});	
	});

</script>