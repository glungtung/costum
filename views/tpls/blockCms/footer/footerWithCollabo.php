<?php
$keytpl = "footerWithCollabo";

$paramsData = [
	"titleColumn1" => "Porteur du projet",
	"titleColumn2" => "Supporteur du projet",
	"titleSocialMedia" => "Suivez-nous sur :",
	"apropos"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Nam convallis enim a finibus congue. Phasellus et tristique dolor. Integer volutpat viverra ornare.",
	"logoHeight"=>50,
	"facebook" =>"",
	"youtube"=>"",
	"instagram"=>"",
	"twitter"=>"",
	"linkedin"=>"",
	"github"=>"",
	"colorTitle"=>"#000",
	"colorText"=>"#000",
	"colorButton"=>"#005E6F",
	"backgroundBoutton"=>"",
	"links"=>array()
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
// Get project owner's logo
$initLogoPorteur = Document::getListDocumentsWhere(
    array(
    "id"=> $blockKey,
    "type"=>'cms',
    "subKey"=>'logoporteur',
    ), "image"
);

$logoImg = [];

foreach ($initLogoPorteur as $key => $value) {
    array_push($logoImg, $value["imagePath"]);
}

$paramsData["porteur"] = $logoImg;

// Logo project collaborater's logo
$initLogoSupporteur = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'logosupporteur',
        ), "image"
    );

    $logoImg = [];

    foreach ($initLogoSupporteur as $key => $value) {
        array_push($logoImg, $value["imagePath"]);
    }

    $paramsData["supporteur"] = $logoImg;
?>

<style type="text/css">
	.single-footer-widget_<?= $kunik?> ul li a {
		line-height: 25px;
		display: inline-block;
		color: #777;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-bottom: 13px;
	}

	.single-footer-widget_<?= $kunik?> ul li a:hover {
		color: <?= $paramsData["colorButton"]?>;
	}

	.single-footer-widget_<?= $kunik?> .instafeed {
		margin-left: -5px;
		margin-right: -5px;
	}


	.single-footer-widget_<?= $kunik?> .subscribe_form {
		padding-top: 25px;
	}

	.single-footer-widget_<?= $kunik?> .input-group {
		display: block !important;
	}

	.single-footer-widget_<?= $kunik?> input {
		width: 100%;
		border: 1px solid <?= $paramsData["colorButton"]?>;
		font-size: 13px;
		line-height: 30px;
		padding-right: 40px;
		margin-top: 5px;
		height: 40px;
		color: #999999;
		background: #fff;
		padding-left: 20px;
	}

	.single-footer-widget_<?= $kunik?> input.placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input:-moz-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input::-moz-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input::-webkit-input-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input:focus {
		outline: none;
	}

	.single-footer-widget_<?= $kunik?> .sub-btn {
		background: <?= $paramsData["colorButton"]?>;
		color: #fff;
		font-weight: 300;
		margin-top: 5px;
		border-radius: 0;
		line-height: 34px;
		padding: 4px 11px 0px;
		cursor: pointer;
		position: absolute;
		right: 0px;
		top: 0px;
	}

	.single-footer-widget_<?= $kunik?> .sub-btn span {
		position: relative;
		top: -1px;
	}

	@media (max-width: 978px) {
		.imageArticle_<?= $kunik?>{
			height: 40px;
			width: 100px;
			margin-top: 10px;
		}
		.titreArcticle_<?= $kunik?>{
			font-size: 11px;
			text-transform: initial;
			color: <?= $paramsData["colorText"]?>;
			padding: initial;
			margin-left: 16px;
		}
		.form-inline input {
			margin: 10px 0;
		}

		.form-inline {
			flex-direction: column;
			align-items: stretch;
		}
	}
	
	.single-footer-widget_<?= $kunik?> p {
		margin-bottom: 0px;
		color: <?= $paramsData["colorText"]?>;
		max-width: 90%;
	}
	
	.f_social_wd_<?= $kunik?> p {
		font-size: 14px;
		color: <?= $paramsData["colorText"]?>;
		margin-bottom: 15px;
	}

	.f_social_wd_<?= $kunik?> .f_social a {
		font-size: 18px;
		color: <?= $paramsData["colorText"]?>;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-right: 20px;
	}

	.f_social_wd_<?= $kunik?> .f_social a:hover {
		color: #8ABF32;
	}

	.f_social_wd_<?= $kunik?> .f_social a:last-child {
		margin-right: 0px;
	}
	
	.footer_title_<?= $kunik?>{
		font-size: 25px;
	}
	@media (max-width: 978px){
		.footer-area_<?= $kunik?> {
			padding-top: 54px;
			padding-bottom: 40px;
		}
		.footer_title_<?= $kunik?>{
			font-size: 15px;
		}
		.single-footer-widget_<?= $kunik?> p {
			font-size: 14px;
	}
	}

	.icon<?= $kunik ?>{
		padding: 0.4em;
		border: 2px solid #999;
		border-radius: 40px;
	}

	.mx-3-<?= $kunik ?>{
		margin: 0.3em;
	}
</style>
<footer class="footer-area_<?= $kunik?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?>">
					<h6 class="footer_title_<?= $kunik?> title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleColumn1">
						<?php echo $paramsData["titleColumn1"]; ?>
					</h6>
					<div class="text-center">
						<?php foreach ($paramsData["porteur"] as $ks => $imgs) {
							echo "<img height='".$paramsData["logoHeight"]."px' src='".$imgs."' class='mx-3-".$kunik."' style='margin:1em'>";
						} ?>
					</div>
					<?php //echo  '<p class="description">'.$paramsData["apropos"].'</p>'?>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?>">
					<h6 class="footer_title_<?= $kunik?> title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleColumn2">
						<?php echo $paramsData["titleColumn2"]; ?>
					</h6>
					<div class="text-center">
						<?php foreach ($paramsData["supporteur"] as $ks => $imgs) {
							echo "<img height='".$paramsData["logoHeight"]."' src='".$imgs."' class='mx-3-".$kunik."' style='margin:1em'>";
						} ?>
					</div>
				</div>
			</div>	
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?> f_social_wd_<?= $kunik?>">
					<h6 class="footer_title_<?= $kunik?> title text-left">
						<?php echo $paramsData["titleSocialMedia"]; ?>
					</h6>
					<br>
					<div class="f_social text-center">
						<?php if ($paramsData["facebook"]!="") {
							echo '<a target="_blank" href="'.$paramsData["facebook"].'">
								<i class="fa fa-facebook icon'.$kunik.'"></i> </a>';
						} ?>

						<?php if ($paramsData["youtube"]!="") {
							echo '<a target="_blank" href="'.$paramsData["youtube"].'">
							<i class="fa fa-youtube icon'.$kunik.'"></i>
						</a>';
						} ?>
						<?php if ($paramsData["instagram"]!="") {
							echo '<a target="_blank" href="'.$paramsData["instagram"].'">
							<i class="fa fa-instagram icon'.$kunik.'"></i>
						</a>';
						} ?>
						<?php if ($paramsData["twitter"]!="") {
							echo '<a target="_blank" href="'.$paramsData["twitter"].'">
							<i class="fa fa-twitter icon'.$kunik.'"></i>
						</a>';
						} ?>
						<?php if ($paramsData["linkedin"]!="") {
							echo '<a target="_blank" href="'.$paramsData["linkedin"].'">
							<i class="fa fa-linkedin icon'.$kunik.'"></i>
						</a>';
						} ?>
						<?php if ($paramsData["github"]!="") {
							echo '<a target="_blank" href="'.$paramsData["github"].'">
							<i class="fa fa-github icon'.$kunik.'"></i>
						</a>';
						} ?>
						<?php foreach ($paramsData["links"]as $lkey => $lvalue) {
							echo '<a target="_blank" href="'.$lvalue["link"].'">
							<i class="fa fa-link icon'.$kunik.'"></i>
						</a>';
						}?>
					</div>
				</div>
			</div>						
		</div>
	</div>
	<br><br>
	<?php if ($paramsData["apropos"]!="") { ?>
		<hr>
		<div></div>
		<div class="description sp-text  img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="apropos">
			<?php echo "\n ".$paramsData["apropos"]; ?>
		</div>
	<?php } ?>
</footer>
<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		type = <?php echo json_encode($kunik); ?>;
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre footer",
				"icon" : "fa-cog",
				"properties" : {
					"porteur" :{                    
	                   "inputType" : "uploader",
	                   "docType": "image",
	                   "contentKey":"slider",
	                   "endPoint": "/subKey/logoporteur",
	                   "domElement" : "logoporteur",
	                   "filetypes": ["jpeg", "jpg", "gif", "png"],
	                   "label": "Logo porteur :",
	                   "showUploadBtn": false,
	                    initList : <?php echo json_encode($initLogoPorteur) ?> 
	                },
					"supporteur":{
					   "inputType" : "uploader",
	                   "docType": "image",
	                   "contentKey":"slider",
	                   "endPoint": "/subKey/logosupporteur",
	                   "domElement" : "logosupporteur",
	                   "filetypes": ["jpeg", "jpg", "gif", "png"],
	                   "label": "Logo supporteur :",
	                   "showUploadBtn": false,
	                    initList : <?php echo json_encode($initLogoSupporteur) ?>
					},

					"logoHeight":{
					   "inputType" : "number",
					   "label": "Hauteur de logo :",
	                    values : sectionDyf.<?php echo $kunik?>ParamsData.logoHeight
					},

	                "titleSocialMedia":{
						label : "Titre pour liens réseaux sociaux",
						inputType:"text",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.titleSocialMedia
					},
					"facebook":{
						label : "Lien facebook",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.facebook
					},
					"youtube":{
						label : "Lien youtube",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.youtube
					},
					"instagram":{
						label : "Lien instagram",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.instagram
					},
					"twitter":{
						label : "Lien twitter",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.twitter
					},
					"linkedin":{
						label : "Lien linkedin",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.linkedin
					},
					"github":{
						label : "Lien github",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.linkedin
					},
					"links" : {
                        inputType : "lists",
                        label : "autres liens",
                        entries: {
                            link: {
                                type:"text",
                                label:"Lien",
                                placeholder: ""
                            }
                        },
                        values : sectionDyf.<?php echo $kunik?>ParamsData.links
                    }
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function (data) {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						if(val.inputType == "formLocality"){
                          tplCtx.value[k] = getArray('.'+k+val.inputType);
                          mylog.log(tplCtx.value[k]);
                        }

                        if(k=="links"){
                            let links = {};
                            $.each(data.links, function(index, va){
                                links["link"+index] = va;
                            });
                            tplCtx.value[k] = links;
                        }else{
                          tplCtx.value[k] = $("#"+k).val();
                        }
					});
					if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                            tplCtx.value["coord"] = formData.geo;
                            tplCtx.value["address"] = formData.address;
                            tplCtx.value["geo"] = formData.geo;
                            tplCtx.value["geoPosition"] = formData.geoPosition;

                            if(typeof formData.addresses != "undefined")
                                tplCtx.value["addresses"] = formData.addresses;
                        }
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');

							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }

				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});


	});
</script>
