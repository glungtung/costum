<?php
	$keytpl = 'footerCocity';
	$paramsData = [
		'firstColumn' =>
		"<div><font size=\"6\" color=\"#ffffff\" face=\"SharpSans-No1-Medium\"><br></font></div><font size=\"6\" color=\"#ffffff\" face=\"SharpSans-No1-Medium\"> ENSEMBLE\n\t\tTROUVER&nbsp;</font><div><font size=\"6\" color=\"#ffffff\" face=\"SharpSans-No1-Medium\">et AJOUTER DES ACTIONS LOCALES POSITIVES</font></div>",
		'secondColumn' =>
		"<div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\"><br></font></div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\"> Se rassembler\n\t\tpour regler des problème mondiaux</font><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;</font><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #covidFreedom&nbsp;</font></div><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #deforestation&nbsp;</font></div><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #climateChange&nbsp;</font></div><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #LocalPositif</font></div></div>",
		'thirdColumn' =>
		"<div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\"><br></font></div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">CONNECTER LE LOCAL&nbsp;</font><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\"><br></font><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Producteur</font></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Artisans</font></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Ingenieurs</font><span style=\"font-size: x-large; color: rgb(255, 255, 255); font-family: SharpSans-No1-Medium;\">&nbsp;</span></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Contributeur&nbsp;</font></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Etc...</font></div></div>",
		'blockCmsBgColor' => '#052434',
		'tpl' => ""
	];
	if (isset($blockCms)) {
		foreach ($paramsData as $e => $v) {
			if (isset($blockCms[$e])) {
				$paramsData[$e] = $blockCms[$e];
			}
		}
	}
?>
<style type="text/css">
	.create<?= $kunik ?> a{
	transition: border-radius 1s;
		border-radius: 10px 5px 10px 5px;
	-webkit-transition: border-radius 1s;
	-moz-transition: border-radius 1s;
	-o-transition: border-radius 1s;
	-ms-transition: border-radius 1s;
	padding: 13px;
	font-size: 30px;
	color: #052434;
		text-decoration: none;
		margin-left : 2%;
		margin-right : 2% ;
		background-color: white;
	}
	.create<?= $kunik ?> a:hover {   
		border-radius: 50px 5px 50px 5px;
		-webkit-border-radius: 50px 5px 50px 5px;
		-moz-border-radius-topleft: 50px;
		-moz-border-radius-topright: 5px;background: transparent;
		color: white !important;
		border: 3px solid #F0FCFF;
		-moz-border-radius-bottomleft: 5px;
		-moz-border-radius-bottomright: 50px;
		}
	
	.create<?= $kunik ?>{
		margin-top: 3%;
		padding-left: 8%;
		margin-bottom: 1%;
		font-weight: bold;
	}

	.col<?= $kunik ?>{
		padding-left: 5%;
	}
	@media (max-width: 978px) {
		.create<?= $kunik ?> a{
			width: 100%;
			margin-bottom : 5px ;
			padding: 7px;
			font-size: 18px  !important;
			border-radius: 10px;
			text-decoration: none;
			background-color: white;
		}
	}
</style>
<div class="<?= $kunik ?>">
	<div class="col<?= $kunik ?>">
		<div class="col-xs-12  col-md-4">
			<div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="firstColumn"><?= $paramsData['firstColumn'] ?></div>
		</div>
		<div class="col-xs-12  col-md-4">
			<div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="secondColumn"><?= $paramsData['secondColumn'] ?></div>
		</div>
		<div class="col-xs-12  col-md-4">
			<div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="thirdColumn"><?= $paramsData['thirdColumn'] ?></div>
		</div>
	</div>
	<div class="col-md-12 text-center create<?= $kunik ?>">
		<a class="col-md-3 col-xs-12" href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/cocityPrez" > Qu’est ce qu’un cocity</a>
		<a class="col-md-3 col-xs-12 createCocity<?= $kunik ?>" href="javascript:;" > Créer votre cocity</a>
		<a class="col-md-3 col-xs-12" href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/cocityPrez?map=scroll" > Voir les autres cocity</a>
	</div>
</div>
<script type="text/javascript">
	var allThem = {
		"alimentation" : {
			"name" : "Food",
			"icon" : "fa-cutlery",
			"tags" : [ 
			"agriculture", 
			"alimentation", 
			"nourriture", 
			"AMAP"
			]
		},
		"santé" : {
			"name" : "Health",
			"icon" : "fa-heart-o",
			"tags" : [ 
			"santé"
			]
		},
		"déchets" : {
			"name" : "Waste",
			"icon" : "fa-trash-o ",
			"tags" : [ 
			"déchets"
			]
		},
		"transport" : {
			"name" : "Transport",
			"icon" : "fa-bus",
			"tags" : [ 
			"Urbanisme", 
			"transport", 
			"construction"
			]
		},
		"éducation" : {
			"name" : "Education",
			"icon" : "fa-book",
			"tags" : [ 
			"éducation", 
			"petite enfance"
			]
		},
		"citoyenneté" : {
			"name" : "Citizenship",
			"icon" : "fa-user-circle-o",
			"tags" : [ 
			"citoyen", 
			"society"
			]
		},
		"Économie" : {
			"name" : "Economy",
			"icon" : "fa-money",
			"tags" : [ 
			"ess", 
			"économie social solidaire"
			]
		},
		"énergie" : {
			"name" : "Energy",
			"icon" : "fa-sun-o",
			"tags" : [ 
			"énergie", 
			"climat"
			]
		},
		"culture" : {
			"name" : "Culture",
			"icon" : "fa-universal-access",
			"tags" : [ 
			"culture", 
			"animation"
			]
		},
		"environnement" : {
			"name" : "Environnement",
			"icon" : "fa-tree",
			"tags" : [ 
			"environnement", 
			"biodiversité", 
			"écologie"
			]
		},
		"numérique" : {
			"name" : "Numeric",
			"icon" : "fa-laptop",
			"tags" : [ 
			"informatique", 
			"tic", 
			"internet", 
			"web"
			]
		},
		"sport" : {
			"name" : "Sport",
			"icon" : "fa-futbol-o",
			"tags" : [ 
			"sport"
			]
		}
	};
	var cocityObj<?= $kunik ?> ={
		generateFiliere:function(dataThematic) {
			mylog.log("dfgfgfg",dataThematic);
			ajaxPost(
				null,
				'<?php Yii::app()->baseUrl; ?>/costum/filiere/generate',
				dataThematic,
				function(data){
				},
				function(error){
					toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
				},
				"json"
			)
		},
		createOrgaFiliere:function(params,orga,thematic) {
			mylog.log("filiereee22",params);
			ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params,
				function(data){
				mylog.log("dfgfgfg1",orga);
					let referenceData = {
						"source":orga.map.source
					};
					referenceData.source.key = orga.map.slug;					
					referenceData.source.keys = [orga.map.slug];
					var dataThematic = {"cocity":orga.map._id.$id, "ville":orga.map.name, "thematic":thematic.charAt(0).toUpperCase()+thematic.toLowerCase().slice(1),...data,...referenceData};
					mylog.log("ftftf",dataThematic);
					cocityObj<?= $kunik ?>.generateFiliere(dataThematic);
					mylog.log("cndsjfnsdlfs",data);
					
				},
				function(){
					toastr.error( "Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur");
				},
				"json"
			);
		}
	};

	sectionDyf.<?php echo $kunik; ?>ParamsData = <?php echo json_encode($paramsData); ?>;
	
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik; ?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
		
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey; ?>");
				},
				save : function (data) {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik; ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								dyFObj.closeForm();
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
			}
		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik; ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik; ?>Params,null, sectionDyf.<?php echo $kunik; ?>ParamsData);
		});
		$(".createCocity<?= $kunik ?>").off().on("click",function(){
			mylog.log("");
			var dyfOrga={
				"beforeBuild":{
					"properties" : {
						"name" : {
							"label" : "Nom de votre ville", 
							"inputType" : "text",
							"placeholder" :"Nom de votre ville",
							"order" : 1
						},
						"formLocality" : {
							"label" : "Adresse de votre ville",
							"rules" :{
								"required" : true
							}
						},
						"template" :{
							"order" : 4,
							"label" : "Choisir un template", 
							"inputType" : "tplList",
							list :{
								"unnotremonde" : {
									"photo" :"<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/tpl/unautremonde.jpg"
								},
								"cocity" : {
									"photo" :"<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/tpl/cocity.png"
								}
							},
							"rules" :{
								"required" : true
							}
						},
						"thematic":{
							"inputType" : "tags",
							"label" : "Sélectionner les filières que vous voulez dés la création de la page",
							"values" : [
								tradTags["food"],
								tradTags["health"] ,
								tradTags["waste"] ,
								tradTags["transport"] ,
								tradTags["education"],
								tradTags["citizenship"],
								tradTags["economy"],
								tradTags["energy"],
								tradTags["culture"],
								tradTags["environment"],
								tradTags["numeric"],
								tradTags["sport"]
							],
							"value" : [
								tradTags["food"],
								tradTags["health"] ,
								tradTags["waste"] ,
								tradTags["transport"] ,
								tradTags["education"],
								tradTags["citizenship"],
								tradTags["economy"],
								tradTags["energy"],
								tradTags["culture"],
								tradTags["environment"],
								tradTags["numeric"],
								tradTags["sport"]
							],
							"order" : 3
						}
    				}
				},
				"onload" : {
					"actions" : {
						"setTitle" : "Initialiser une ville",
						"src" : {
							"infocustom" : "<br/>Remplir le champ"
						}          
					}
				}   

    		};
			dyfOrga.afterSave = function(orga){
				dyFObj.commonAfterSave(null, function(){
					mylog.log("filiereee",orga);
					var dataCity = {"cocity":"cocity", ...orga,"allCities":false};
					let cocityCoordonate = {};
					cocityCoordonate["address"] = orga.map.address;
					cocityCoordonate["geo"] = orga.map.geo;
					cocityCoordonate["geoPosition"] = orga.map.geoPosition;

					if (notNull(orga.map.thematic) ) {
					var thema = orga.map.thematic;
					thematic = thema.split(",");
					$.each(thematic , function(k,val) {
						mylog.log("filiereee4",val);
						var defaultTags = [];
						if (typeof  allThem[val.toLowerCase()] != "undefined" && notNull(allThem[val.toLowerCase()]["tags"])) {
							defaultTags = allThem[val.toLowerCase()]["tags"]; 
						}   
						var defaultName = orga.map.name +" "+ val;
						var dataThem = {};
						var params = {
							name: defaultName,
							collection : "organizations",
							type: "Group",
							role: "creator",
							image: "",
							tags: defaultTags,
							...cocityCoordonate,
							addresses : undefined,
							shortDescription: "Filière "+val+" "+orga.map.name
						}
						cocityObj<?= $kunik?>.createOrgaFiliere(params,orga,val);  
						mylog.log("filiereee4",params); 

					});

					}
					ajaxPost(
						null,
						baseUrl+"/costum/cocity/generatecocity",
						dataCity, 
						function(data){
							window.location="<?php echo Yii::app()->createUrl('/costum'); ?>/co/index/slug/"+orga.map.slug;
						},
						function(error){
							toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
						},
						"json"
					);
					
				});
			}
			dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"},null,dyfOrga);
    	})
	})
</script>