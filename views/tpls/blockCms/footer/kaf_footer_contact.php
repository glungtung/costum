<?php 
  $keyTpl ="kaf_footer_contact";
  $paramsData = [ 
    "titleBlock" => "CONTACTS",
    "subtitleBlock" => "Sous titre",
    "contact" => "+0000",
    "mail" => "mail@mail.com",
    "socialColor" => "#ffffff",
    "socialBgColor" => "#D3D3D3",
    "socialNetwork"       => null
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
<style type="text/css">

	.subtitle<?php echo $kunik ?>,  h4.contact-foot{
	  	font-weight: 400;
	  }

	ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	ul.social-network li {
		display: inline;
		margin: 0 5px;
	}

	
	.social-circle li a:hover {
		background-color:<?= $paramsData["socialColor"] ?>;
	}
	.social-circle li a:hover i {
		color: <?= $paramsData["socialBgColor"] ?>;
	}

	.social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		-moz-border-radius:50%;
		-webkit-border-radius:50%;
		border-radius:50%;
		text-align:center;
		width: 50px;
		height: 50px;
		font-size:24px;
	}
	.social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}

	.social-circle li a:hover i, .triggeredHover {
		-moz-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-ms--transform: rotate(360deg);
		transform: rotate(360deg);
		-webkit-transition: all 0.2s;
		-moz-transition: all 0.2s;
		-o-transition: all 0.2s;
		-ms-transition: all 0.2s;
		transition: all 0.2s;
	}
	.social-circle .socialIcon i {
		color: <?= $paramsData["socialColor"] ?>;
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}

	.social-circle a.socialIcon {
	 background-color: <?= $paramsData["socialBgColor"] ?>;   
	}
</style>

<div class="container<?php echo $kunik ?> col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
	<h2 class="title<?php echo $kunik ?> title-1  sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock">
    	<?php echo $paramsData["titleBlock"] ?>
  	</h2>
  	<hr>
  	<div class="col-xs-12">
  		<h3 class="subtitle<?php echo $kunik ?> title-2 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitleBlock"">
		    <?php echo $paramsData["subtitleBlock"] ?>
		</h3>
  	</div>
  	<div class="row">
  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  			<h4 class=" contact-foot title-3 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="contact"><i class="fa fa-phone"></i>&nbsp;<?php echo $paramsData["contact"] ?></h4>
  		</div>
  		<?php 
              if (isset($paramsData["socialNetwork"])) { ?>  
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
			     <ul class="social-network social-circle">
			     	<?php 
	                  foreach ($paramsData["socialNetwork"] as $key => $value) {?>
			        	<li><a href="<?php echo isset($value['urlnetwork'])??$value['urlnetwork'] ?>"  target="_blank" class="socialIcon" title="<?php echo isset($value['nom'])??$value['nom'] ?>"><i class="fa fa-<?php echo $value['icon'] ?>"></i></a></li>
			    	<?php } ?>
			    </ul> 				
			</div>
		<?php } ?>	
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  			<a href="mailto:<?php echo $paramsData["mail"] ?>" class="title-4 contact-foot"><i class="fa fa-at"></i>&nbsp;:&nbsp;<?php echo $paramsData["mail"] ?></a>
  		</div>
	</div>

</div>

<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
      sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
          "title" : "Configurer votre section",
          "description" : "Personnaliser votre section",
          "icon" : "fa-cog",
          
          "properties" : {
            "mail" : {
                "inputType" : "text",
                "label" : "E-mail",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.mail
            },
            "socialColor":{
              label : "Couleur de l'icone reseau social",
              inputType : "colorpicker",
              values :  sectionDyf.<?php echo $kunik?>ParamsData.socialColor
            },
            "socialBgColor":{
              label : "Couleur du fond reseau social",
              inputType : "colorpicker",
              values :  sectionDyf.<?php echo $kunik?>ParamsData.socialBgColor
            },
            "socialNetwork" : {
              "label" : "Réseau sociaux",
              "inputType" : "lists",
              "entries":{
                  "key":{
                      "type":"hidden",
                      "class":""
                  },
                  "icon":{
                      "label":"Icon",
                      "type":"select",
                      "class":"col-md-6 col-sm-6 col-xs-12"
                  },
                  "nom":{
                      "label":"Nom du reseau social",
                      "type":"text",
                      "class":"col-md-6 col-sm-6 col-xs-12"
                  },
                  "urlnetwork":{
                      "label":"Nom de la page",
                      "type":"text",
                      "class":"col-md-11 col-sm-11 col-xs-11"
                  }
                }
              }     
          },
          beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
              if (k == "parent")
                tplCtx.value[k] = formData.parent;

              if(k == "socialNetwork")
                tplCtx.value[k] = data.socialNetwork;
            });
            console.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                    toastr.success("Élément bien ajouté");
                    $("#ajax-modal").modal('hide');
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                  });
              });
            }
          }
        }
      };

      $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        createFontAwesomeSelect<?php echo $kunik ?>();
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      });
  });

	function createFontAwesomeSelect<?php echo $kunik ?>(){
    dyFObj.init.buildEntryList = function(num, entry,field, value){
        mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
        countEntry=Object.keys(entry).length;
        defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
        
        str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
            $.each(entry, function(e, v){
                name=field+e+num;
                classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                str+="<div class='"+classEntry+"'>";
                if(typeof v.label != "undefined"){
                    str+='<div class="padding-5 col-xs-12">'+
                        '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                    '</div>'+
                    '<div class="space5"></div>';
                }
                valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                if(v.type=="hidden")
                    str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="text")
                    str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="textarea")
                    str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                else if(v.type=="select"){
                    str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                    //This is the select Options-----------------//
                    str+= createSelectOptions(valueEntry,fontAwesome);
                    str+='</select>';             
                }
                str+="</div>";
            });
            str+='<div class="col-sm-1">'+
                    '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                '</div>'+
            '</div>';
        return str;
    }; 
  }

  function createSelectOptions(current,Obj){
      var html = "";
      $.each(Obj,function(k,v){
          html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
      });
      return html;
  }
    
</script>