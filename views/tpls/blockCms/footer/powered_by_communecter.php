<?php 
  $keyTpl ="powered_by_communecter";
  $paramsData = [ 
    "showForm" => "show",
    "btnBgColor" => "#bca87d",
    "btnLabelColor" => "white",
    "btnLabel" => "POWERED BY",
    "btnBorderColor" => "#eeeeee",
    "btnRadius" => "4",
    "showForm" => "show",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  ?>
<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );
  
 ?>
<!-- ****************end get image uploaded************** -->

<style type="text/css">

	.container<?php echo $kunik ?> #poweredBy .body-img {
        height: auto!important;
        width: 100%;
        object-fit: cover;
        object-position: center;
        min-height: 380px;
        max-height: 420px;
    }
    @media (max-width: 1199px) and (min-width: 992px) {
      .container<?php echo $kunik ?> #poweredBy .body-img {
          height: 480px!important;
          max-height: 480px;
      }
    }
    
	.container<?php echo $kunik ?> .powered_by_co {
	    background-color:<?php echo $paramsData["btnBgColor"] ?>;
	    color:<?php echo $paramsData["btnLabelColor"] ?>;
	    padding: 14px 36px;
	    border-radius: <?php echo $paramsData["btnRadius"] ?>px !important;
	    border:2px solid <?php echo $paramsData["btnBorderColor"] ?>;
	    font-size: large !important;
   }
   .container<?php echo $kunik ?> .powered_by_co:hover {
      background-color:<?php echo $paramsData["btnLabelColor"] ?>;
      color:<?php echo $paramsData["btnBgColor"] ?>;
      border: 2px solid <?php echo $paramsData["btnBgColor"] ?>;
  	}
  	.container<?php echo $kunik ?> #poweredBy .absolute-content {
	    top: 10px;
	}
	@media  (max-width: 767px) {
		.container<?php echo $kunik ?> .poweredBy-logo {
			top: auto!important;
		    bottom: 65px!important;
		    text-align: center;
		}
		.container<?php echo $kunik ?> .poweredBy-logo img {
			height: 120px;
			margin-left: auto;
    		margin-right: auto;
		}
	}
	.container<?php echo $kunik ?> #poweredBy .media {
	  	display: -ms-flexbox;
		display: flex;
	    -ms-flex-align: start;
	    align-items: flex-start;
	}
	.container<?php echo $kunik ?> #poweredBy .img-right {
		right: 10px;
		max-width: 360px;
	}
	@media (max-width: 1199px) and (min-width: 992px) {
		.container<?php echo $kunik ?> #poweredBy .img-right {
	    right: 5px;
	    top: 125px;
	    width: 200px;
	}
	}
	@media (max-width: 991px) and (min-width: 768px) {
     .container<?php echo $kunik ?> #poweredBy .body-img {
          height: 600px!important;
    			max-height: 700px;
      }
      .container<?php echo $kunik ?> #poweredBy .img-right {
		    right: auto;
		    max-width: 201px;
		    top: 420px;
		    left: 50%;
		    transform: translate(-50%,-50%);
			}
    }
     @media (max-width: 767px){
      .container<?php echo $kunik ?> #poweredBy .body-img {
          height: 670px!important;
          max-height: 670px;
      }
      
    }
	@media  (min-width: 768px) {
		.container<?php echo $kunik ?> #poweredBy .media {
		  	min-height: 80px;
		}
	}
</style>


<div class="container<?php echo $kunik ?> col-md-12 no-padding">
  <?php if ($paramsData["showForm"] == "show") { ?>

	  <div id="poweredBy"  class="col-xs-12  no-padding highlight-co"> 
		<img class="img-responsive body-img" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/BONHOMME.png">
		<div class="col-sm-12 col-md-7 absolute-content">
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="media">
	        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
	        <div class="media-body">
	            <p class="padding-left-10"> Nos données sont sécurisées et ne seront jamais exploitées </p>
	        </div>
	    </div>
	    <div class="media">
	        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
	        <div class="media-body">
	            <p class="padding-left-10"> Je contribue au developpement d'un commun numerique (comme Wikipedia) </p>
	        </div>
	    </div>
	    <div class="media">
	        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
	        <div class="media-body">
	            <p class="padding-left-10"> Je soutiens, participes et contribue à une communauté qui respecte l'homme, l'environnement et la finitude des ressources  </p>
	        </div>
	    </div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="media">
	        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
	        <div class="media-body">
	            <p class="padding-left-10"> Un réseau social societal et territorial 100% Gratuit et Open source. </p>
	        </div>
	    </div>
	    <div class="media">
	        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
	        <div class="media-body">
	            <p class="padding-left-10"> Pour une societé libre ouverte, rééllement démocratique et transparente. </p>
	        </div>
	    </div>
	    <div class="media">
	        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
	        <div class="media-body">
	            <p class="padding-left-10"> Je construit mon site en marque blanche </p>
	        </div>
	    </div>
			</div>
		</div>
		<div class="col-md-3 col-xs-12 img-right absolute-content no-padding poweredBy-logo">
			<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/engrenage.png" >
		</div>
	</div>
    

  <?php }else {?>
  <div class="col-sm-12 col-md-12 text-center padding-top-20">
      <a href="javascript:;" class="tooltips btn open_poweredBy btn powered_by_co" data-toggle="modal" data-target="#myModal-contact-us"
      data-id-receiver="" 
      data-email=""
      data-name="">
          <?php echo $paramsData["btnLabel"] ?>
      </a>     
  </div>
    
	<div class="portfolio-modal modal fade" id="poweredBy" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-content padding-top-15">
	        <div class="close-modal" data-dismiss="modal">
	            <div class="lr">
	                <div class="rl">
	                </div>
	            </div>
	        </div>

	        <div class="col-xs-12 bg-white">

		     	<img class="img-responsive body-img" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/BONHOMME.png">
				<div class="col-sm-12 col-md-7 absolute-content all-padding-section">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="media">
			        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
			        <div class="media-body">
			            <p class="padding-left-10"> Nos données sont sécurisées et ne seront jamais exploitées </p>
			        </div>
			    </div>
			    <div class="media">
			        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
			        <div class="media-body">
			            <p class="padding-left-10"> Je contribue au developpement d'un commun numerique (comme Wikipedia) </p>
			        </div>
			    </div>
			    <div class="media">
			        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
			        <div class="media-body">
			            <p class="padding-left-10"> Je soutiens, participes et contribue à une communauté qui respecte l'homme, l'environnement et la finitude des ressources  </p>
			        </div>
			    </div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="media">
			        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
			        <div class="media-body">
			            <p class="padding-left-10"> Un réseau social societal et territorial 100% Gratuit et Open source. </p>
			        </div>
			    </div>
			    <div class="media">
			        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
			        <div class="media-body">
			            <p class="padding-left-10"> Pour une societé libre ouverte, rééllement démocratique et transparente. </p>
			        </div>
			    </div>
			    <div class="media">
			        <img class="" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/CHECK.png" style="width: 28px;">
			        <div class="media-body">
			            <p class="padding-left-10"> Je construit mon site en marque blanche </p>
			        </div>
			    </div>
					</div>
				</div>
				<div class="col-md-3 col-xs-12  img-right absolute-content no-padding poweredBy-logo">
					<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/co/engrenage.png" >
				</div>
		    </div>
		</div>
	</div>

<?php }?>

</div>
 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre bloc",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog", 
            "properties" : {
           
                "showForm":{ 
                  "label" : "Type d'affichage formulaire ",
                  inputType : "select",
                  options : {              
                    "modal " : "Modal",
                    "show" : "Afficher direct"
                  },
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.showForm
                },
                "btnLabel" : {
                  "label" : "Labelle",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnLabelColor":{
                  label : "Couleur du labelle",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabelColor
                },
                "btnBgColor":{
                  label : "Couleur",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "btnBorderColor":{
                  label : "Couleur du bordure",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBorderColor
                },
                "btnRadius" : {
                  "label" : "Rayon du bordure (en px)",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnRadius
                }
                
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton","green","");
          if ($("#showForm").val() == "show") {
            $('.fieldsetbtn').fadeOut();
          }
          $("#showForm").change(function(){
            if ($(this).val() == "show") {
              $('.fieldsetbtn').fadeOut();
            }else
            $('.fieldsetbtn').fadeIn();
          })
        });


        $(".container<?php echo $kunik ?> .open_poweredBy").click(function(){
		    mylog.log("openFormContact");
		      //$("#formContact .contact-email").html(costum.admin.email);
		      $("#poweredBy").modal("show");
		  })

        });

</script>
