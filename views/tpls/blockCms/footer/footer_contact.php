<?php 
  $keyTpl ="footer_contact";
  $paramsData = [ 
    "titleBlock" => "CONTACTS",
    "subtitleBlock" => " Lorem ipsum dolor sit amet, consectetur adipiscing",
    "contact" => "+0000",
    "mail" => "mail@mail.com",
    "socialColor" => "#ffffff",
    "socialBgColor" => "#D3D3D3",
    "socialPosition" => "center",
    "socialNetwork"       => null,
    "address" => "",
    "showType" => "logo",
    "labelFooter" => "",
    "dataType" => "newData",
    "geo" => ""
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
 <?php 
    $latestLogo = [];

  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"logo"
        ),"image"
    );

    foreach ($initImage as $key => $value) {
         $latestLogo[]= $value["imagePath"];
    }

?>
<style type="text/css">

	.subtitle<?php echo $kunik ?>,  h4.contact-foot{
	  	font-weight: 400;
	  }

	ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	ul.social-network li {
		display: inline;
		margin: 0 5px;
	}

	
	.social-circle li a:hover {
		background-color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["socialBgColor"]; ?>;
	}
	.social-circle li a:hover i {
		color: <?php echo (isset($costum["css"]["color"]["btncolorLabel"])) ? $costum["css"]["color"]["btncolorLabel"] : $paramsData["socialColor"]; ?>;
	}

	.social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		-moz-border-radius:50%;
		-webkit-border-radius:50%;
		border-radius:50%;
		text-align:center;
		width: 50px;
		height: 50px;
		font-size:24px;
	}
	.social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}
  .social-circle li img {
    width: 25px;
    height: 25px;
    text-align: center;
    margin-top: 12px;
    vertical-align: sub;
  }

	.social-circle li a:hover i, .social-circle li a:hover img, .triggeredHover {
		-moz-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-ms--transform: rotate(360deg);
		transform: rotate(360deg);
		-webkit-transition: all 0.5s;
		-moz-transition: all 0.5s;
		-o-transition: all 0.5s;
		-ms-transition: all 0.5s;
		transition: all 0.5s;
	}
	.social-circle .socialIcon i {
		color: <?= $paramsData["socialColor"] ?>;
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}
  .footer-logo {
        text-align: center
     }
  @media (min-width: 768px) {
     .footer-left {
        padding-left: 50px;
     }
     .footer-logo img{
        width: 50%;
        margin-left: auto;
        margin-right: auto;
        display: block;
     }
  }
  @media (max-width: 767px) {
     .footer-left {
        padding-left: 5px;
     }
     .footer-logo img{
        margin-top: 20px;
        height: 50px;
        margin-left: auto;
        margin-right: auto;
        display: block;
     }
  }




	.social-circle a.socialIcon {
	 background-color: background-color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["socialBgColor"]; ?>;
	}
</style>

<?php 
    $where = array(
            "slug" => $costum["contextSlug"]
        );
        
    $costumOrga = PHDB::find(Organization::COLLECTION,$where);
        //var_dump($costumOrga);//exit;

    $costumProject = PHDB::find(Project::COLLECTION,$where);
        //var_dump($costumProject);//exit;

    $costumFus = array_merge($costumOrga,$costumProject);
    //var_dump($costumFus);exit;

 ?>

<div class="container<?php echo $kunik ?> col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
	  <p class="title-1 title<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock">
    	<?php echo $paramsData["titleBlock"] ?>
      </p>
    
    <p class="title-2 title<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitleBlock">
      <?php echo $paramsData["subtitleBlock"] ?>
      </p>
    <?php 
        if ($paramsData["dataType"] == "newData") { ?> 
    	<div class="row">

        <?php if ($paramsData["showType"] != "") { ?>
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 footer-left">
        <?php } ?>
      		<div id="footer-phone" class="col-xs-12">
      			<h4 class="title-3 contact-foot"><i class="fa fa-phone "></i>&nbsp;<span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Numéros téléphone : </span>&nbsp;<?php echo $paramsData["contact"] ?></h4>
      		</div>
          <?php 
                  if (isset($paramsData["socialNetwork"])) { ;?>  
          <div id="footer-social" class="col-xs-12 text-<?php echo $paramsData["socialPosition"] ?>">
               <ul class="social-network social-circle">
                <?php 
                        foreach ($paramsData["socialNetwork"] as $key => $value) {?>
                    <?php if (@$value['urlnetwork'] != "" && @$value['nom'] != "" && @$value['icon'] != "") {?>
                      <li><a href="<?php echo $value['urlnetwork'] ?>"  target="_blank" class="socialIcon" title="<?php echo $value['nom'] ?>"><i class="fa fa-<?php echo $value['icon'] ?>"></i></a></li>
                         
                    
                <?php } 
                }?>
              </ul>         
          </div>
        <?php } ?>  
          
          <div id="footer-mail" class="col-xs-12 ">
            <a href="mailto:<?php echo $paramsData["mail"] ?>">
              <h4 class="title-3 contact-foot">
                <i class="fa fa-at"></i>&nbsp;<span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Mail : </span><?php echo $paramsData["mail"] ?>
              </h4>
            </a>
        </div>

        <?php if(!empty($paramsData["address"]["addressLocality"])) { ?>
            <div id="footer-adress" class="col-xs-12">
              <h4 class="title-3 contact-foot">
                <i class='fa fa-map-marker fs-18'></i> 
                <span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Adresse :</span>
                <?php 
                      echo !empty($paramsData["address"]["streetAddress"]) ? $paramsData["address"]["streetAddress"].", " : "";
                      echo !empty($paramsData["address"]["postalCode"]) ? $paramsData["address"]["postalCode"].", " : "";
                      echo $paramsData["address"]["addressLocality"];
                  ?>
              </h4>
            </div>
            <?php  } ?>
      		
        <?php if ($paramsData["showType"] != "") { ?>
      </div>
      <?php } ?>


      <?php 
          if (isset($paramsData["showType"]) && $paramsData["showType"] == "logo") { ?> 
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 footer-logo">
              <a class="lbh-menu-app" href="#welcome">
                <img class="img-responsive" src="<?php echo !empty($latestLogo) ? $latestLogo[0] : ""; ?>">
              </a>
        </div>
      <?php }elseif ($paramsData["showType"] == "map") { ?>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 footer-logo">
              <div class="well no-padding" id="mapDiv" style="border-radius: 3px; background-color: rgb(255, 255, 255); border: none; height: 230px; width: 100%;">
              </div>
              
            </div>
       <?php }?>

  	</div>

  <?php }else {  
      foreach ($costumFus  as $key => $val) { 
    ?>

      <div class="row">
        <?php if ($paramsData["showType"] != "") { ?>
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 footer-left">
        <?php } ?>
        <?php if (isset($val["telephone"]["fixe"][0]) != "" ) { ;?>  
          <div id="footer-phone" class="col-xs-12">
            <h4 class="title-3 contact-foot"><i class="fa fa-phone "></i>&nbsp;<span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Numéros téléphone : </span>&nbsp;<?php echo $val["telephone"]["fixe"][0] ?></h4>
          </div>
          <?php } if (isset($val["telephone"]["mobile"][0]) != "" ) { ?> 
            <div id="footer-phone" class="col-xs-12">
              <h4 class="title-3 contact-foot"><i class="fa fa-mobile "></i>&nbsp;<span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Numéros mobile : </span>&nbsp;<?php echo $val["telephone"]["mobile"][0] ?></h4>
            </div>
          <?php } ?>
          <?php if (isset($val["socialNetwork"])) { 
            $diaspora =  (!empty($val["socialNetwork"]["diaspora"])? $val["socialNetwork"]["diaspora"]:"") ;
            $mastodon =  (!empty($val["socialNetwork"]["mastodon"])? $val["socialNetwork"]["mastodon"]:"") ;
            $facebook = (!empty($val["socialNetwork"]["facebook"])? $val["socialNetwork"]["facebook"]:"") ;
            $twitter =  (!empty($val["socialNetwork"]["twitter"])? $val["socialNetwork"]["twitter"]:"") ;
            $gitlab =  (!empty($val["socialNetwork"]["gitlab"])? $val["socialNetwork"]["gitlab"]:"") ;
            $github =  (!empty($val["socialNetwork"]["github"])? $val["socialNetwork"]["github"]:"") ;
            $instagram =  (!empty($val["socialNetwork"]["instagram"])? $val["socialNetwork"]["instagram"]:"") ;
          ;?>  
          <div id="footer-social" class="col-xs-12 text-<?php echo $paramsData["socialPosition"] ?>">
               <ul class="social-network social-circle">
                <?php if ($facebook != "") ?>
                      <li><a href="<?php echo $facebook ?>"  target="_blank" class="socialIcon" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <?php if ($instagram != "") ?>
                      <li><a href="<?php echo $instagram ?>"  target="_blank" class="socialIcon" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                <?php if ($twitter != "") ?>
                      <li><a href="<?php echo $twitter ?>"  target="_blank" class="socialIcon" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <?php if ($github != "") ?>
                      <li><a href="<?php echo $github ?>"  target="_blank" class="socialIcon" title="Github"><i class="fa fa-github"></i></a></li>
                <?php if ($mastodon != "") ?>
                      <li><a href="<?php echo $mastodon ?>"  target="_blank" class="socialIcon" title="Mastodon"><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/mastodon.png"></a></li>  
                <?php if ($diaspora != "") ?>
                      <li><a href="<?php echo $diaspora ?>"  target="_blank" class="socialIcon" title="Diaspora"><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/diaspora_icon.png"></a></li> 
                <?php if ($gitlab != "") ?>
                      <li><a href="<?php echo $gitlab ?>"  target="_blank" class="socialIcon" title="Gitlab"><img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/gitlab_icon.png" ></a></li>         
               
              </ul>         
          </div>
        <?php } ?>  
          <?php if (isset($val["email"]) != "" ) { ;?> 
          <div id="footer-mail" class="col-xs-12 ">
            <a href="mailto:<?php echo $val["email"] ?>">
              <h4 class="title-3 contact-foot">
                <i class="fa fa-at"></i>&nbsp;<span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Mail : </span><?php echo $val["email"] ?>
              </h4>
            </a>
          </div>
        <?php } ?>

        <?php if(!empty($val["address"]["addressLocality"])) { ?>
            <div id="footer-adress" class="col-xs-12">
              <h4 class="title-3 contact-foot">
                <i class='fa fa-map-marker fs-18'></i> 
                <span class="hidden-xs <?php echo $paramsData["labelFooter"] ?>">Adresse :</span>
                <?php 
                      echo !empty($val["address"]["streetAddress"]) ? $val["address"]["streetAddress"].", " : "";
                      echo !empty($val["address"]["postalCode"]) ? $val["address"]["postalCode"].", " : "";
                      echo $val["address"]["addressLocality"];
                  ?>
              </h4>
            </div>
            <?php  } ?>
          
        <?php if ($paramsData["showType"] != "") { ?>
      </div>
    <?php } ?>


    <?php 
        if (isset($paramsData["showType"]) && $paramsData["showType"] == "logo") { ?>
        <?php if (isset($val["profilImageUrl"]) != "" ) { ;?> 
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 footer-logo">
                <a class="lbh-menu-app" href="#welcome">
                  <img class="img-responsive" src="<?php echo @$val["profilImageUrl"]; ?>">
                </a>
          </div>
        <?php } ?>
    <?php }elseif ($paramsData["showType"] == "map") { ?>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 footer-logo">
            <div class="well no-padding" id="mapDiv" style="border-radius: 3px; background-color: rgb(255, 255, 255); border: none; height: 230px; width: 100%;">
            </div>
            
          </div>
     <?php }?>

  </div>
  <?php } 
  } ?>

</div>

<script type="text/javascript">
  var mapContact = {};

  function showMap(){
    <?php if ($paramsData["dataType"] == "newData") { ?> 
      var contextDataMap = <?php echo json_encode( $paramsData["geo"] ); ?>;
    <?php }else{ ?>
      var contextDataMap = <?php echo json_encode( $val["geo"] ); ?>;
    <?php } ?>
    mylog.log("contextDataMap",contextDataMap);
    var showHideMap = <?php echo json_encode( $paramsData["showType"] ); ?>;
    if (showHideMap == "map") {
      var paramsMapContent = {
            container : "mapDiv",
            activeCluster : false,
            zoom : 12,
            activePopUp : false
        };
        mapContact = mapObj.init(paramsMapContent);

            mapContact.addMarker({
                elt : {
                    geo : contextDataMap
                }
            });
        
        mapContact.hideLoader();
    }
        
  };
  var showType = <?php echo json_encode( $paramsData["showType"] ); ?>;
  if (showType == "") {
    $("#footer-phone").addClass('col-lg-4 col-md-4 col-sm-4');
    $("#footer-adress").addClass('col-lg-4 col-md-4 col-sm-4');
    $("#footer-mail").addClass('col-lg-4 col-md-4 col-sm-4');
     $("#footer-social").addClass('col-lg-4 col-md-4 col-sm-4');
  };
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {

      showMap();
      
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                
                "dataType" : {
                    "label" : "Type de données",
                    "inputType" : "select",
                      options : {
                        "newData": "Nouvelle Données",
                        "coData": "Données dans communecter"
                      },
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.dataType
                },
                "contact" : {
                    "inputType" : "text",
                    "label" : "Contact",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.contact
                },
                "mail" : {
                    "inputType" : "text",
                    "label" : "E-mail",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.mail
                },
                "socialColor":{
  		            label : "Couleur de l'icone",
  		            inputType : "colorpicker",
  		            values :  sectionDyf.<?php echo $kunik?>ParamsData.socialColor
    		        },
  		          "socialBgColor":{
  		            label : "Couleur du fond",
  		            inputType : "colorpicker",
  		            values :  sectionDyf.<?php echo $kunik?>ParamsData.socialBgColor
    		        },
                "socialPosition":{
                    label : "position",
                    inputType : "select",
                          options : {
                              "center"    : "Centre",
                              "left " : "A gauche",
                              "right "  :"A droite"
                          },
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.socialPosition
                },
    		        "socialNetwork" : {
                        "label" : "Réseau sociaux",
                        "inputType" : "lists",
                        "entries":{
                            "key":{
                                "type":"hidden",
                                "class":""
                            },
                            "icon":{
                                "label":"Icon",
                                "type":"select",
                                "class":"col-md-6 col-sm-6 col-xs-12"
                            },
                            "nom":{
                                "label":"Nom du reseau social",
                                "type":"text",
                                "class":"col-md-6 col-sm-6 col-xs-12"
                            },
                            "urlnetwork":{
                                "label":"Lien du reseau social",
                                "type":"text",
                                "placeholder":"http://www.exemple.com",
                                "class":"col-md-11 col-sm-11 col-xs-11"
                            }
                        },
                    },
                "labelFooter"  : {
                    "label" : "Label",
                    "inputType" : "select",
                      options : {
                        "": "Afficher",
                        "hidden": "Masquer"
                      },
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.showType
                },
                "showType" : {
                    "label" : "Afficher (map, logo, aucun)",
                    "inputType" : "select",
                      options : {
                        "map": "Map",
                        "logo": "Logo",
                        ""   :"aucun"
                      },
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.showType
                },
                
                "logo" : {
                    "inputType" : "uploader",
                    "label" : "logo",
                    "showUploadBtn" : false,
                    "docType" : "image",
                    "itemLimit" : 1,
                    "contentKey" : "slider",
                    "order" : 9,
                    "domElement" : "logo",
                    "placeholder" : "image logo",
                    "afterUploadComplete" : null,
                    "endPoint" : "/subKey/logo",
                    "filetypes" : [
                    "png","jpg","jpeg","gif"
                    ],
                    initList : <?php echo json_encode($initImage) ?>
                  },
                  coord : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality),
                  location : dyFInputs.location     
              },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                
                if(val.inputType == "formLocality"){
                  tplCtx.value[k] = getArray('.'+k+val.inputType);
                  console.log(tplCtx.value[k]);
                }else 
                  tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "socialNetwork")
                  tplCtx.value[k] = data.socialNetwork;
              });
              mylog.log("save tplCtx",tplCtx);
              mylog.log("formData",formData);
              if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                    tplCtx.value["coord"] = formData.geo;
                    tplCtx.value["address"] = formData.address;
                    tplCtx.value["geo"] = formData.geo;
                    tplCtx.value["geoPosition"] = formData.geoPosition;

                    if(typeof formData.addresses != "undefined")
                        tplCtx.value["addresses"] = formData.addresses;
                }

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          createFontAwesomeSelect<?php echo $kunik ?>();
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"social",12,12,null,null,"Propriété du reseau social","green","");

          if ($("#showType").val() == "map") {
            $('.logouploader').fadeOut();
          }else if ($("#showType").val() == "" || $("#showType").val() == "logo") {
            $('.coordformLocality').fadeOut();
          }else{
            $('.logouploader').fadeOut();
            $('.coordformLocality').fadeOut();
          }
          $("#showType").change(function(){
            if ($(this).val() == "map") {
              $('.logouploader').fadeOut();
              $('.coordformLocality').fadeIn();
            }else if ($(this).val() == "logo") {
              $('.logouploader').fadeIn();
              $('.coordformLocality').fadeOut();
            }else{
              $('.logouploader').fadeOut();
              $('.coordformLocality').fadeOut();
            }
          });

          if ($("#dataType").val() == "coData") {
            $('.contacttext').fadeOut();
            $('.mailtext').fadeOut();
            $('.socialNetworklists').fadeOut();
            $('.logouploader').fadeOut();
            $('.coordformLocality').fadeOut();
          }else {
            $('.contacttext').fadeIn();
            $('.mailtext').fadeIn();
            $('.socialNetworklists').fadeIn();
            $('.logouploader').fadeIn();
            $('.coordformLocality').fadeIn();
          }
          $("#dataType").change(function(){
            if ($(this).val() == "coData") {
              $('.contacttext').fadeOut();
              $('.mailtext').fadeOut();
              $('.socialNetworklists').fadeOut();
              $('.logouploader').fadeOut();
              $('.coordformLocality').fadeOut();
            }else if ($(this).val() == "coData") {
              $('.contacttext').fadeIn();
              $('.mailtext').fadeIn();
              $('.socialNetworklists').fadeIn();
              $('.logouploader').fadeIn();
              $('.coordformLocality').fadeIn();
            } 
          });
          
        });
    });

	function createFontAwesomeSelect<?php echo $kunik ?>(){
    dyFObj.init.buildEntryList = function(num, entry,field, value){
        mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
        countEntry=Object.keys(entry).length;
        defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
        
        str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
            $.each(entry, function(e, v){
                name=field+e+num;
                classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                str+="<div class='"+classEntry+"'>";
                if(typeof v.label != "undefined"){
                    str+='<div class="padding-5 col-xs-12">'+
                        '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                    '</div>'+
                    '<div class="space5"></div>';
                }
                valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                if(v.type=="hidden")
                    str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="text")
                    str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="textarea")
                    str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                else if(v.type=="select"){
                    str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                    //This is the select Options-----------------//
                    str+= createSelectOptions(valueEntry,fontAwesome);
                    str+='</select>';             
                }
                str+="</div>";
            });
            str+='<div class="col-sm-1">'+
                    '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                '</div>'+
            '</div>';
        return str;
    }; 
}

function createSelectOptions(current,Obj){
    var html = "";
    $.each(Obj,function(k,v){
        html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
    });
    return html;
}
    
</script>