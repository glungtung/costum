<?php 
$keyTpl = "homefooter";
$paramsData = [
  "title"     => "Intéressé?",
  "titleAlign"  => "center",
  "titleSize"     => "22",
  "titleColor"    => "#ffffff",
  "textAlign"   => "center",
  "textSize"      => "18",
  "textColor"     => "#ffffff",
  "bg_color"    => "#de4c7c",
  "link"    => "#",
  "text"      => "pour aller plus loin! A très vite…"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}

   # If user is admin
    #$is_admin = false;
    #$admins = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
    #if(count($admins)!=0){
    #    $is_admin = true;
    #}

    # If user has already joined
    $members = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"]=>['$exists' => true ], "slug"=>$costum['slug']));
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    if($paramsData["link"]!=""){
        $formId = $paramsData["link"];
    }else{
        # Get the form id
        $form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
        $formId = "";
        foreach($form as $key => $value) { $formId = $key; }
    }

    # If user has submited the coform
    $answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));
    $has_answered = false;
    
    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if(Yii::app()->session["userId"] == $value["user"]){
            $myAnswer = $value['_id']->{'$id'};
        }
    }

    if(isset(Yii::app()->session["userId"])){
        
        $data = PHDB::findByIds("citoyens", [Yii::app()->session["userId"]] ,["links.follows", "links.memberOf"]);
        
        $memberOf_ids = array();

        if(isset($data[Yii::app()->session["userId"]]["links"]["memberOf"])){
            $links2 = $data[Yii::app()->session["userId"]]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }
?>

<style type="text/css">

  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?> .btn-edit-delete .btn{
    box-shadow: 0px 0px 20px 3px #ffffff;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  @media (max-width: 768px) {
    .text-<?php echo $kunik ?> {
    }
   }
</style>



<div class="row container<?php echo $kunik ?> text-center" style='background-color: <?= $paramsData['bg_color'] ?>'>
	<div  style="text-align: <?= $paramsData['titleAlign'] ?>" class="padding-20 text-<?php echo $kunik ?>">
		<div style="padding: 50px;" class="text-center">      
      <font style="font-size: <?= $paramsData['titleSize'] ?>px; font-family: Lato-Italic;color: <?= $paramsData['titleColor'] ?>" class="bold"><?= $paramsData['title'] ?></font>
      <p style="font-size: <?= $paramsData['textSize'] ?>px; color: <?= $paramsData['textColor'] ?>; text-align: <?= $paramsData['textAlign'] ?>"> Cliquer <a href="<?= $paramsData['link'] ?>" class="lbh"><b class="">ICI </b></a><?= $paramsData['text'] ?>
    </p>
  </div>
</div>
</div>
  <script type="text/javascript">

function loadFormAfterLog() {
  localStorage.setItem("loadFormAfterLog", "load");

    urlCtrl.loadByHash("#answer.index.id.new.form.<?= $formId; ?>");
}

if(localStorage.getItem("loadFormAfterLog") == "load") {   
 bootbox.confirm(`<div role="alert">
   <font style="font-size: <?= $paramsData['titleSize'] ?>px; font-family: Lato-Italic;color: <?= $paramsData['titleColor'] ?>" class="bold"><?= $paramsData['title'] ?></font>
          <p style="font-size: <?= $paramsData['textSize'] ?>px; color: <?= $paramsData['textColor'] ?>; text-align: <?= $paramsData['textAlign'] ?>"> Cliquer <a href="#answer.index.id.new.form.<?= $formId; ?>" class="lbh"><b class="">ICI </b></a><?= $paramsData['text'] ?>
        </p>
  </div> `,
  function(result){
    if (!result) {
      return;
    }else{   

    }
    urlCtrl.loadByHash(location.hash);
  }); 
  localStorage.removeItem("loadFormAfterLog");
}

  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

  jQuery(document).ready(function() {

    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          title : {
            label : "Titre",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
          },
          titleSize : {
            label : "Taille du titre",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleSize
          },
          titleColor : {
            label : "Couleur du titre",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
          },
          titleAlign : {
            label : "Alignement du titre",
            inputType : "select",
            options : {
              "left"    : "À Gauche",
              "right" : "À droite",
              "center"  :"Centré",
              "justify"  :"Justifié"
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleAlign
          },
          text : {
            label : "Taille du text",
            "inputType" : "textarea",
            "markdown" : true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
          },
          link : {
            label : "Lien",
            "inputType" : "textarea",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.link
          },
          textSize : {
            label : "Taille du text",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
          },
          textColor : {
            label : "Couleur du text",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
          },  
          textAlign : {
            label : "Alignement du texte",
            inputType : "select",
            options : {
              "left"    : "À Gauche",
              "right" : "À droite",
              "center"  :"Centré",
              "justify"  :"Justifié"
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textAlign
          },
          bg_color : {
            label : "Couleur du fond",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.bg_color
          }
        },
        beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};

          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');

                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";

      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>