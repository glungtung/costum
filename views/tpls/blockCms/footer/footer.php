<?php
$keytpl = "footer";
$paramsData = [
	"mairie" => "",
	"apropos"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Nam convallis enim a finibus congue.Phasellus et tristique dolor. Integer volutpat viverra ornare.",
	"email"=>"",
	"facebook" =>"",
	"youtube"=>"",
	"instagram"=>"",
	"twitter"=>"",
	"linkedin"=>"",
	"colorTitle"=>"#000",
	"colorText"=>"#000",
	"colorButton"=>"#005E6F",
	"backgroundBoutton"=>""

];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 
$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["emailContact","address","geo","geoPosition","email"]);
$postalCode = isset($parent["address"]["postalCode"])?$parent["address"]["postalCode"]:"";
$ville= isset($parent["address"]["addressLocality"])?$parent["address"]["addressLocality"]:"";
$email = isset($parent["email"])?$parent["email"]:"";
$emailContact = isset($parent["emailContact"])?$parent["emailContact"]:[];
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
$idMe = isset($me)?(String)$me["_id"]:"";

?>

<style type="text/css">
	.single-footer-widget_<?= $kunik?> ul li a {
		line-height: 25px;
		display: inline-block;
		color: #777;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-bottom: 13px;
	}

	.single-footer-widget_<?= $kunik?> ul li a:hover {
		color: <?= $paramsData["colorButton"]?>;
	}

	.single-footer-widget_<?= $kunik?> .instafeed {
		margin-left: -5px;
		margin-right: -5px;
	}


	.single-footer-widget_<?= $kunik?> .subscribe_form {
		padding-top: 25px;
	}

	.single-footer-widget_<?= $kunik?> .input-group {
		display: block !important;
	}

	.single-footer-widget_<?= $kunik?> input {
		width: 100%;
		border: 1px solid <?= $paramsData["colorButton"]?>;
		font-size: 13px;
		line-height: 30px;
		padding-right: 40px;
		margin-top: 5px;
		height: 40px;
		color: #999999;
		background: #fff;
		padding-left: 20px;
	}

	.single-footer-widget_<?= $kunik?> input.placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input:-moz-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input::-moz-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input::-webkit-input-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input:focus {
		outline: none;
	}

	.single-footer-widget_<?= $kunik?> .sub-btn<?= $kunik?> {
		background: <?= $paramsData["colorButton"]?>;
		color: #fff;
		font-weight: 300;
		margin-top: 5px;
		border-radius: 0;
		line-height: 34px;
		padding: 4px 11px 0px;
		cursor: pointer;
		position: absolute;
		right: 0px;
		top: 0px;
	}

	.single-footer-widget_<?= $kunik?> .sub-btn<?= $kunik?> span {
		position: relative;
		top: -1px;
	}
	@media (max-width: 978px) {
		.imageArticle_<?= $kunik?>{
			height: 40px;
			width: 100px;
			margin-top: 10px;
		}
		.titreArcticle_<?= $kunik?>{
			font-size: 11px;
			text-transform: initial;
			color: <?= $paramsData["colorText"]?>;
			padding: initial;
			margin-left: 16px;
		}
		.form-inline input {
			margin: 10px 0;
		}

		.form-inline {
			flex-direction: column;
			align-items: stretch;
		}
	}
	
	.single-footer-widget_<?= $kunik?> p {
		margin-bottom: 0px;
		color: <?= $paramsData["colorText"]?>;
		max-width: 90%;
	}
	
	.f_social_wd_<?= $kunik?> p {
		font-size: 14px;
		color: <?= $paramsData["colorText"]?>;
		margin-bottom: 15px;
	}

	.f_social_wd_<?= $kunik?> .f_social a {
		font-size: 18px;
		color: <?= $paramsData["colorText"]?>;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-right: 20px;
	}

	.f_social_wd_<?= $kunik?> .f_social a:hover {
		color: #8ABF32;
	}

	.f_social_wd_<?= $kunik?> .f_social a:last-child {
		margin-right: 0px;
	}
	
	.footer_title_<?= $kunik?>{
		color: <?= $paramsData["colorTitle"]?>;
		font-size: 25px;
	}
	@media (max-width: 978px){
		.footer-area_<?= $kunik?> {
			padding-top: 54px;
			padding-bottom: 40px;
		}
		.footer_title_<?= $kunik?>{
			font-size: 15px;
		}
		.single-footer-widget_<?= $kunik?> p {
			font-size: 14px;
	}
	}
</style>
<footer class="footer-area_<?= $kunik?>">
	

	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?>">
					<h6 class="title footer_title_<?= $kunik?> ">Apropos</h6>
					<p class=" description sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="apropos"><?= $paramsData["apropos"]?>
				</p>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="single-footer-widget_<?= $kunik?>">
				<h6 class="footer_title_<?= $kunik?> title">Actualité</h6>
				<p class="description">Recevez notre dernier article dans votre boîte e-mail</p>		
				<div id="">
						<div class="input-group d-flex flex-row">
							<input name="email" placeholder="<?= isset($parent["emailContact"][$idMe])?$parent["emailContact"][$idMe]:"Email Address"?>" required=""  id="email" type="email">
							<button class="btn sub-btn<?= $kunik?>" onclick="saveEmail<?= $kunik?>()">
								<span class="fa fa-arrow-right"></span>
						</button>		
						</div>									
						<div class="mt-10 info"></div>
				</div>
			</div>
		</div>	
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="single-footer-widget_<?= $kunik?> f_social_wd_<?= $kunik?>">
				<h6 class="footer_title_<?= $kunik?> title">Adresse postale</h6>
				<p class="description"> Mairie de  <?= ($paramsData["mairie"]=="")?$ville:$paramsData["mairie"]?></p>
				<p class="description"><?= $postalCode." ".$ville?></p>

				<h6 class="footer_title_<?= $kunik?> title">Suivez-nous</h6>
				<div class="f_social text-center">
					<?php if ($paramsData["facebook"]!="") {
						echo '<a class="text-center" href=" $paramsData["facebook"]" >
							<i class="fa fa-facebook"></i> </a>';
					} ?>

					<?php if ($paramsData["youtube"]!="") {
						echo '<a href="$paramsData["youtube"]">
						<i class="fa fa-youtube"></i>
					</a>';
					} ?>
					<?php if ($paramsData["instagram"]!="") {
						echo '<a href=" $paramsData["instagram"]">
						<i class="fa fa-instagram"></i>
					</a>';
					} ?>
					<?php if ($paramsData["twitter"]!="") {
						echo '<a href =  $paramsData["twitter"] ">
						<i class="fa fa-twitter"></i>
					</a>';
					} ?>
					<?php if ($paramsData["linkedin"]!="") {
						echo '<a href=" $paramsData["linkedin"]">
						<i class="fa fa-linkedin"></i>
					</a>';
					} ?>
				</div>
			</div>
		</div>						
	</div>
</div>
</footer>
<script type="text/javascript">
	function saveEmail<?= $kunik?>() { 
		var contentLength = Object.keys(<?php echo json_encode($emailContact); ?>).length;
		mylog.log("contentLength",contentLength);
		var email =document.getElementById("email").value;
		tplCtx.id = '<?= (String)$parent["_id"] ?>';
        tplCtx.collection = "<?= $costum["contextType"]?>";
        tplCtx.path = "emailContact.<?= $idMe?>";
        tplCtx.value = email;
        mylog.log("tplCtx0000", tplCtx);
        dataHelper.path2Value( tplCtx, function(params) {
        	toastr.success("Votre email est bien enregistré !");
			var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
			var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
			var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
			cmsBuilder.block.loadIntoPage(id, page, path, kunik);
        	// urlCtrl.loadByHash(location.hash);
    	})
	} 
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik ?>Params = {

			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre footer",
				"icon" : "fa-cog",
				"properties" : {
					
					"mairie":{
						label : "Mairie de",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.mairie 
					},					
					"facebook":{
						label : "Lien facebook",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.facebook
					},
					"youtube":{
						label : "Lien youtube",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.youtube
					},
					"instagram":{
						label : "Lien instagram",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.instagram
					},
					"twitter":{
						label : "Lien twitter",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.twitter
					},
					"linkedin":{
						label : "Lien linkedin",
						inputType:"url",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.linkedin
					},
					"colorButton":{
						label : "Background du boutton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorButton
					}
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {
		          tplCtx.value = {};
		          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
		            tplCtx.value[k] = $("#"+k).val();
		            if (k == "parent") {
		              tplCtx.value[k] = formData.parent;
		            }
		          });
		          mylog.log("save tplCtx",tplCtx);

		          if(typeof tplCtx.value == "undefined")
		            toastr.error('value cannot be empty!');
		          else {
		              dataHelper.path2Value( tplCtx, function(params) {
		                dyFObj.commonAfterSave(params,function(){
		                  	toastr.success("Élément bien ajouté");
		                  	$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                			// urlCtrl.loadByHash(location.hash);
		                });
		              } );
		          }

		        }
			}
		};
		
			

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});


	});
</script>