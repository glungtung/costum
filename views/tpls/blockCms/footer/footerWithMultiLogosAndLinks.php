<?php
$keytpl = "footerWithMultiLogosAndLinks";

$paramsData = [
	"porteurTitle" => "Porteur du projet",
	"supporteurTitle" => "Supporteur du projet",
	"socialFollowTitle" => "Suivez-nous sur :",
	"apropos"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Nam convallis enim a finibus congue. Phasellus et tristique dolor. Integer volutpat viverra ornare.",
	"porteurLinks"=>array(),
	"supporteurLinks"=>array(),
	"colorTitle"=>"#000",
	"colorText"=>"#000",
	"colorButton"=>"#005E6F",
	"backgroundBoutton"=>"",
	"socialLinks"=>array()
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}

// Get project owner's logo
$initLogoPorteur = Document::getListDocumentsWhere(
    array(
    "id"=> $blockKey,
    "type"=>'cms',
    "subKey"=>'logoporteur',
    ), "image"
);

$logoImg = [];

foreach ($initLogoPorteur as $key => $value) {
    array_push($logoImg, array('logo' => $value["imagePath"], 'link' => $paramsData["supporteurLinks"]["logo".$key."Link"]["link"],'width' => $paramsData["supporteurLinks"]["logo".$key."Link"]["width"]));
}

$paramsData["porteurLogos"] = $logoImg;

// Logo project collaborater's logo
$initLogoSupporteur = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'logosupporteur',
        ), "image"
    );

    $logoImg = [];

    foreach ($initLogoSupporteur as $key => $value) {
        array_push($logoImg, array('logo' => $value["imagePath"], 'link' => $paramsData["supporteurLinks"]["logo".$key."Link"]["link"],'width' => $paramsData["supporteurLinks"]["logo".$key."Link"]["width"]));
    }

    $paramsData["supporteurLogos"] = $logoImg;
?>

<style type="text/css">
	.single-footer-widget_<?= $kunik?> ul li a {
		line-height: 25px;
		display: inline-block;
		color: #777;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-bottom: 13px;
	}

	.single-footer-widget_<?= $kunik?> ul li a:hover {
		color: <?= $paramsData["colorButton"]?>;
	}

	.single-footer-widget_<?= $kunik?> .instafeed {
		margin-left: -5px;
		margin-right: -5px;
	}


	.single-footer-widget_<?= $kunik?> .subscribe_form {
		padding-top: 25px;
	}

	.single-footer-widget_<?= $kunik?> .input-group {
		display: block !important;
	}

	.single-footer-widget_<?= $kunik?> input {
		width: 100%;
		border: 1px solid <?= $paramsData["colorButton"]?>;
		font-size: 13px;
		line-height: 30px;
		padding-right: 40px;
		margin-top: 5px;
		height: 40px;
		color: #999999;
		background: #fff;
		padding-left: 20px;
	}

	.single-footer-widget_<?= $kunik?> input.placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input:-moz-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input::-moz-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input::-webkit-input-placeholder {
		color: #999999;
	}

	.single-footer-widget_<?= $kunik?> input:focus {
		outline: none;
	}

	.single-footer-widget_<?= $kunik?> .sub-btn {
		background: <?= $paramsData["colorButton"]?>;
		color: #fff;
		font-weight: 300;
		margin-top: 5px;
		border-radius: 0;
		line-height: 34px;
		padding: 4px 11px 0px;
		cursor: pointer;
		position: absolute;
		right: 0px;
		top: 0px;
	}

	.single-footer-widget_<?= $kunik?> .sub-btn span {
		position: relative;
		top: -1px;
	}

	@media (max-width: 978px) {
		.imageArticle_<?= $kunik?>{
			height: 40px;
			width: 100px;
			margin-top: 10px;
		}
		.titreArcticle_<?= $kunik?>{
			font-size: 11px;
			text-transform: initial;
			color: <?= $paramsData["colorText"]?>;
			padding: initial;
			margin-left: 16px;
		}
		.form-inline input {
			margin: 10px 0;
		}

		.form-inline {
			flex-direction: column;
			align-items: stretch;
		}
	}

	.single-footer-widget_<?= $kunik?> p {
		margin-bottom: 0px;
		color: <?= $paramsData["colorText"]?>;
		max-width: 90%;
	}

	.f_social_wd_<?= $kunik?> p {
		font-size: 14px;
		color: <?= $paramsData["colorText"]?>;
		margin-bottom: 15px;
	}

	.f_social_wd_<?= $kunik?> .f_social a {
		font-size: 18px;
		color: <?= $paramsData["colorText"]?>;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-right: 20px;
	}

	.f_social_wd_<?= $kunik?> .f_social a:hover {
		color: #8ABF32;
		text-decoration: none;
	}

	.f_social_wd_<?= $kunik?> .f_social a:last-child {
		margin-right: 0px;
	}

	.footer_title_<?= $kunik?>{
		font-size: 25px;
	}
	@media (max-width: 978px){
		.footer-area_<?= $kunik?> {
			padding-top: 54px;
			padding-bottom: 40px;
		}
		.footer_title_<?= $kunik?>{
			font-size: 15px;
		}
		.single-footer-widget_<?= $kunik?> p {
			font-size: 14px;
	}
	}

	.icon<?= $kunik ?>{
		padding: 0.5em 0em !important;
		height: 40px;
		width: 40px;
		border: 2px solid #999;
		border-radius: 40px;
	}

	.mx-3-<?= $kunik ?>{
		margin: 0.3em;
	}
</style>
<footer class="footer-area_<?= $kunik?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?>">
					<h6 class="footer_title_<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="porteurTitle">
						<?php echo $paramsData["porteurTitle"]; ?>
					</h6>
					<div class="text-center">
						<?php foreach ($paramsData["porteurLogos"] as $ks => $imgs) {
							echo "<a target='_blank' href='".$imgs["link"]."'><img width='".$imgs["width"]."' src='".$imgs["logo"]."' class='mx-3-".$kunik."' style='margin:1em;'></a>";
						} ?>
					</div>
					<?php //echo  '<p class="description">'.$paramsData["apropos"].'</p>'?>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?>">
					<h6 class="footer_title_<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="supporteurTitle">
						<?php echo $paramsData["supporteurTitle"]; ?>
					</h6>
					<div class="text-center">
						<?php foreach ($paramsData["supporteurLogos"] as $ks => $imgs) {
							echo "<a target='_blank' href='".$imgs["link"]."'><img width='".$imgs["width"]."' src='".$imgs["logo"]."' class='mx-3-".$kunik."' style='margin:1em;'></a>";
						} ?>
					</div>
				</div>
			</div>
			<?php //if(count($paramsData["socialLinks"])!=0) ?>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="single-footer-widget_<?= $kunik?> f_social_wd_<?= $kunik?>">
					<h6 class="footer_title_<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="socialFollowTitle">
						<?php echo $paramsData["socialFollowTitle"]; ?>
					</h6>
					<br>
					<div class="f_social text-center">
						<?php foreach ($paramsData["socialLinks"] as $lkey => $lvalue) {
							echo '<a target="_blank" href="'.$lvalue["link"].'">
							<i class="fa fa-'.$lvalue["icon"].' icon'.$kunik.'"></i>
						</a>';
						}?>
					</div>
				</div>
			</div>
			<?php //} ?>
		</div>
	</div>
	<br><br>
	<?php if ($paramsData["apropos"]!="") { ?>
		<hr>
		<div></div>
		<div class="sp-text img-text-bloc padding-15" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="apropos">
			<?php echo "\n ".$paramsData["apropos"]; ?>
		</div>
	<?php } ?>
</footer>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		type = <?php echo json_encode($kunik); ?>;
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre footer",
				"icon" : "fa-cog",
				"properties" : {
					"porteurLogos" :{
	                   "inputType" : "uploader",
	                   "docType": "image",
	                   "contentKey":"slider",
	                   "endPoint": "/subKey/logoporteur",
	                   "domElement" : "logoporteur",
	                   "filetypes": ["jpeg", "jpg", "gif", "png"],
	                   "label": "Logo porteur :",
	                   "showUploadBtn": false,
	                    initList : <?php echo json_encode($initLogoPorteur) ?>
	                },
	                "porteurLinks" : {
                        inputType : "lists",
                        label : "liens corréspondants au(x) logo(s)",
                        entries: {
                            link: {
                                type:"text",
                                label:"Lien",
                                placeholder: "URL"
                            },
                            width: {
                                type:"text",
                                label:"Largeur",
                                placeholder: "Valeur Numérique"
                            }
                        },
                        values : sectionDyf.<?php echo $kunik?>ParamsData.porteurLinks
                    },
					"supporteurLogos":{
					   "inputType" : "uploader",
	                   "docType": "image",
	                   "contentKey":"slider",
	                   "endPoint": "/subKey/logosupporteur",
	                   "domElement" : "logosupporteur",
	                   "filetypes": ["jpeg", "jpg", "gif", "png"],
	                   "label": "Logo supporteur :",
	                   "showUploadBtn": false,
	                    initList : <?php echo json_encode($initLogoSupporteur) ?>
					},
					"supporteurLinks" : {
                        inputType : "lists",
                        label : "liens corréspondants au(x) logo(s)",
                        entries: {
                            link: {
                                type:"text",
                                rules: {
                                	"url":true
                                },
                                label:"Lien",
                                placeholder: "URL"
                            },
                            width: {
                                type:"text",
                                label:"Largeur",
                                placeholder: "Valeur Numérique"
                            }
                        },
                        values : sectionDyf.<?php echo $kunik?>ParamsData.supporteurLinks
                    },
					"socialLinks" : {
                        inputType : "lists",
                        label : "liens vers réseaux sociaux",
                        entries: {
                        	icon: {
                                type:"select",
                                label:"Icon",
                                placeholder: "Icon de réseaux sociaux",
                                options: Object.keys(fontAwesome),
                            },
                            link: {
                                type:"text",
                                label:"Lien",
                                placeholder: "Lien vers un réseau"
                            }
                        },
                        value : sectionDyf.<?php echo $kunik?>ParamsData.socialLinks
                    }
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function (data) {
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
						if(k=="socialLinks"){
                            let links = {};
                            $.each(data.socialLinks, function(index, va){
                                links["socialLink"+index] = va;
                            });
                            tplCtx.value[k] = links;
                        }else if(k=="porteurLinks" || k=="supporteurLinks"){
                            let links = {};
                            $.each(data[k], function(index, va){
                                links["logo"+index+"Link"] = va;
                            });
                            tplCtx.value[k] = links;
                        }else{
                          tplCtx.value[k] = $("#"+k).val();
                        }
					});

					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("Élément bien ajouté");
	                      $("#ajax-modal").modal('hide');

						  	var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }

				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"supporteur",6,12,null,null,"Logos Supporteurs / Partenaies","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"porteur",6,12,null,null,"Logos Porteurs / Partenaires","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"social",12,12,null,null,"Liens Réseaux Sociaux","");
		});


	});
</script>
