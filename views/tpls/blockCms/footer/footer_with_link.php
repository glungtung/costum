<?php
$keyTpl     = "footer_with_link";
$paramsData = [
    "text1" => "     <u><font face=\"OpenSans-Bold\" size=\"3\">         CONTACT</font></u><div><font face=\"OpenSans-Bold\"><u><br></u></font><div><font face=\"OpenSans-Bold\" size=\"5\">Technopole de la réunion&nbsp;</font></div><div><font face=\"OpenSans-Bold\" size=\"5\"><br></font></div><div><font face=\"OpenSans-SemiBold\">14 rue Henri Cornu&nbsp;</font></div><div><font face=\"OpenSans-SemiBold\">Immeuble Darwin</font></div><div><font face=\"OpenSans-SemiBold\">97490 Sainte-Clotilde</font></div><div><font face=\"OpenSans-SemiBold\">Tél : 0262 90 71 80 – Fax : 0262 90 71 81</font></div><div><font face=\"OpenSans-SemiBold\">Email :&nbsp;<a target=\"_blank\" href=\"<span style=\" color:=\"\" rgb(32,=\"\" 33,=\"\" 36\"=\"\" style=\"\"><font color=\"#bdd424\" style=\"\">courrier@technopole-reunion.com</font></a></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\">© 2021 – Copyright Technopole de La Réunion<br style=\"\">Tous droits réservés –</font><font face=\"-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif\">&nbsp;</font><a target=\"_blank\" href=\"https://technopole-reunion.com/mentions-legales/\" style=\"\"><font color=\"#bdd424\" face=\"OpenSans-SemiBold\">Mentions légales</font></a></div><div><br></div></div>",
    "text2" => "        <div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/la-technopole/\"><font color=\"#ffffff\">LA TECHNOPOLE</font></a></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/nos-missions/\"><font color=\"#ffffff\">NOS MISSIONS</font></a><br></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/lincubateur/\"><font color=\"#ffffff\">L'INCUBATEUR</font></a><br></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/la-reunion-innovante/\"><font color=\"#ffffff\">LA RÉUNION INNOVANTE</font></a><br></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/contact/\"><font color=\"#ffffff\">CONTACTEZ-NOUS</font></a><br></font></div><div><br></div><div><a target=\"_blank\" href=\"https://technopole-reunion.com/adherer/\"><font face=\"OpenSans-SemiBold\" size=\"3\" color=\"#ffffff\" style=\"<br>    line-height: 35px;<br>\">ADHÉRER</font></a><br></div>",
    "text3" => "        <a target=\"_blank\" href=\"https://technopole-reunion.com/politique-de-confidentialite/\"><font color=\"#ffffff\" size=\"3\" face=\"OpenSans-SemiBold\" style=\"<br>    line-height: 35px;<br>\">POLITIQUE DE CONFIDENTIALITÉ</font></a><div><br><div><a target=\"_blank\" href=\"https://technopole-reunion.com/politique-des-cookies/\"><font color=\"#ffffff\" size=\"3\" face=\"OpenSans-SemiBold\">POLITIQUE DES COUKIES</font></a><br></div></div>",
    
    "socialBgColor" => "#2C3E50",
    "socialBgColorOnHover" => "#788713",
    "socialColorOnHover" => "white",
    "socialColor" => "white",
    "socialNetwork" => [
        [
            "icon" => "facebook",
            "nom" => "facebook",
            "urlnetwork" => "https://www.facebook.com/technopole.reunion/"
        ], 
        [
            "icon" => "linkedin",
            "nom" => "linkedin",
            "urlnetwork" => "https://www.linkedin.com/company/technopole-reunion?trk=hp-feed-company-name"
        ], 
        [
            "icon" => "twitter",
            "nom" => "twitter",
            "urlnetwork" => "https://twitter.com/Technopole_974"
        ], 
        [
            "icon" => "youtube",
            "nom" => "youtube",
            "urlnetwork" => "https://www.youtube.com/channel/UC8krX-rTMeyhwPscEAm4Anw"
        ]

        ],
        "financeur" => " "
]; 
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php 
$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'pdf1',
    ), "file"
  );
?>
<style>
    .social-network<?= $kunik?>{
        margin-top : 40px;   
        margin-bottom : 20px;   
    }
    .social-network<?= $kunik?> ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	.social-network<?= $kunik?> ul.social-network li {
		display: inline;
		margin: 0 5px;
	}
    .social-network<?= $kunik?> .social-circle li a:hover {
		 background-color:<?=$paramsData["socialBgColorOnHover"] ?>;
	}
	.social-network<?= $kunik?> .social-circle li a:hover i {
		 color: <?= $paramsData["socialColorOnHover"] ?>; 
	}

	.social-network<?= $kunik?> .social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		-moz-border-radius:50%;
		-webkit-border-radius:50%;
		border-radius:50%;
		text-align:center;
		width: 45px;
        height: 45px;
        font-size: 25px;
    }
	.social-network<?= $kunik?> .social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}
	.social-network<?= $kunik?> .social-circle .socialIcon i {
		 color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["socialColor"]; ?>;
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}

	.social-network<?= $kunik?> .social-circle a.socialIcon {
	  background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["socialBgColor"]; ?>;
	}
    .footer-area_<?= $kunik?> a{
        text-decoration : none;
        cursor: pointer;
        
    }
    .footer-area_<?= $kunik?> {
        padding-left: 10%;
    }

    .footer-area_<?= $kunik?> #btnScrollTop {
        display: none;
        position: fixed;
        bottom: 20px;
        left: 100px;
        z-index: 99;
        font-size: 35px;
        border: none;
        outline: none;
        background-color: #BDD424;
        color: white;
        height: 55px;
        width: 55px;
        cursor: pointer;
        border-radius: 50%;
        line-height: 0;
    }

    @media (max-width: 767px) {
        .footer-area_<?= $kunik?> #btnScrollTop {
            left: 5px;
            height: 45px;
            width: 45px;
            font-size: 28px;
        }
    }
    #particl-js {
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: 50% 50%;
        position: absolute;
        top: 0px;
        z-index: 0;
    }
    .allFin<?= $kunik?>{
        display: flex;
    }
    .addFin<?= $kunik?>{
        color: white;
        background-color: #2C3E50;
        border-color: #2C3E50;
        font-weight: 700;
    }
    .financ<?= $kunik?> .btnAdd{
        margin-bottom : 10px;
    }
    .fin<?= $kunik?>{
        padding: 5px;
    }


    .financ<?= $kunik?> img {
        width: auto !important;
        height: 100px !important;
        object-fit : contain;
    }

    .fin<?= $kunik?>{
        padding: 0 3px;
    }

    @media (min-width: 992px) and (max-width: 1199px) {
        .financ<?= $kunik?> img {
            width: auto !important;
            height: 70px !important;
        }
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .financ<?= $kunik?> img {
            width: auto !important;
            height: 45px !important;
        }
    }
    @media  (max-width: 767px)   {
        .financ<?= $kunik?> img {
            width: auto !important;
            height: 75px !important;
        }
        .footer-area_<?= $kunik?> {
            padding-left: 1%;
        }
    }
</style>
<?php 
    $initFilesLogo= Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms'
        ), "image"
    );
    $allLogo = [];
    foreach($initFilesLogo as $ke => $va){
        $allLogo[$va["subKey"]] = $va;
    }
?>
<footer class="footer-area_<?= $kunik?>">
    <div class="row">
        <div class="col-lg-5  col-md-5 col-sm-5 col-xs-12 z-index-10">
            <div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text1"> <?= $paramsData["text1"]?></div>                
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 z-index-10">
            <div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text2"> <?= $paramsData["text2"]?></div>
        </div>
        <div class="col-lg-4  col-md-4 col-sm-3 col-xs-12 z-index-10">
            <div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text3"> <?= $paramsData["text3"]?></div>
            <?php 
                if (isset($paramsData["socialNetwork"])) { ?>  
                    <div class="social-network<?= $kunik?>">
                        <ul class="social-network social-circle">
                            <?php 
                            foreach ($paramsData["socialNetwork"] as $key => $value) {?>
                                <li><a href="<?php echo $value['urlnetwork'] ?>"  target="_blank" class="socialIcon" title="<?php echo $value['nom'] ?>"><i class="fa fa-<?php echo $value['icon'] ?>"></i></a></li>
                            <?php } ?>
                        </ul> 				
                    </div>
            <?php } ?>
            <div class="financ<?= $kunik?>">
                <?php if($costum["editMode"] == "true"){ ?>
                    <div class="btnAdd">
                        <a class="hiddenPreview addFin<?= $kunik?>"> Ajouter un financeur</a>
                    </div>
                <?php } ?>
                <div class="allFin<?= $kunik?>">

                    <?php 
                        foreach ($paramsData["financeur"] as $kF => $vF) {
                            ${'initFilesIcone' . $kF}= Document::getListDocumentsWhere(
                                array(
                                    "id"=> $blockKey,
                                    "type"=>'cms',
                                    "subKey"=> (string)$kF
                                ), "image"
                            );
                            ${'arrFileIcone' . $kF}= [];
                            foreach (${'initFilesIcone' . $kF} as $k => $v) {
                                ${'arrFileIcone' . $kF}[] =$v['imagePath'];
                            }?>

                        <div class="fin<?= $kunik?>">
                           <?php if($vF['link']!= ""){?>
                                <a href="<?= $vF['link']?> " target="_blank">
                            <?php 
                            }
                            if (count(${'arrFileIcone' . $kF})!=0) {?>
                                <img src="<?php echo ${'arrFileIcone' . $kF}[0] ?>"  style=" width: <?= @$vF["width"]?>; height: <?= @$vF["height"] ?>">
                            <?php } ?>
                            <?php if($costum["editMode"] == "true"){ ?>
                                <div class="text-center editSectionBtns hiddenPreview" >
                                    <a  href="javascript:;"
                                    class="btn  btn-primary btn-editSection editFin<?= $kunik?>"
                                    data-key="<?= $kF ?>"
                                    data-link='<?= @$vF["link"] ?>'
                                    data-position='<?= $kF ?>'
                                    data-logo='<?php echo json_encode(${"initFilesIcone" . $kF}) ?>' >
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a  href="javascript:;"
                                    class="btn  bg-red text-center btn-editSection deleteElement<?= $kunik?> "
                                    data-id ="<?= $blockKey ?>"
                                    data-path="financeur.<?= $kF ?>"
                                    data-collection = "cms"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            <?php }
                            if($vF['link']!= ""){?>
                                </a>
                            <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>
        </div>
    </div>
    <button onclick="topFunction()" id="btnScrollTop" title="Go to top"> <i class="fa fa-angle-up"></i></button>
    <div id="particl-js"></div>
</footer>
<script>
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        var allFin = <?= json_encode($paramsData["financeur"])?>;
        var allLogo = <?= json_encode($allLogo)?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {   
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                
                "properties" : {
                    "socialNetwork" : {
                        "label" : "<?php echo Yii::t('cms', 'Social networks')?>",
                        "inputType" : "lists",
                        "entries":{
                            "key":{
                                "type":"hidden",
                                "class":""
                            },
                            "icon":{
                                "label":"<?php echo Yii::t('cms', 'Icon')?>",
                                "type":"select",
                                "class":"col-md-6 col-sm-6 col-xs-12"
                            },
                            "nom":{
                                "label":"<?php echo Yii::t('cms', 'Name of the social network')?>",
                                "type":"text",
                                "class":"col-md-6 col-sm-6 col-xs-12"
                            },
                            "urlnetwork":{
                                "label":"<?php echo Yii::t('cms', 'Url of the page')?>",
                                "type":"text",
                                "class":"col-md-11 col-sm-11 col-xs-11"
                            }
                        }
                    },
                    "socialColor":{
                        label : "<?php echo Yii::t('cms', 'Icon color')?> ",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.socialColor
                    },
                    "socialBgColor":{
                        label : "<?php echo Yii::t('cms', 'Background color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.socialBgColor
                    }, 
                    "socialColorOnHover":{
                        label : "<?php echo Yii::t('cms', 'Color of the icon on hover')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.socialColorOnHover
                    },
                    "socialBgColorOnHover":{
                        label : "<?php echo Yii::t('cms', 'Background color on hover')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.socialBgColorOnHover
                    }, 
                },
                beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent")
                            tplCtx.value[k] = formData.parent;

                        if(k == "socialNetwork")
                            tplCtx.value[k] = data.socialNetwork;
                    });
                    mylog.log("save tplCtx",tplCtx);
                    mylog.log("formData",formData);
                    if(typeof formData != "undefined" && typeof formData.geo != "undefined"){
                        tplCtx.value["coord"] = formData.geo;
                        tplCtx.value["address"] = formData.address;
                        tplCtx.value["geo"] = formData.geo;
                        tplCtx.value["geoPosition"] = formData.geoPosition;

                        if(typeof formData.addresses != "undefined")
                            tplCtx.value["addresses"] = formData.addresses;
                    }

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            createFontAwesomeSelect<?php echo $kunik ?>();
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"social",12,12,null,null,"<?php echo Yii::t('cms', 'Social network property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"socialColor",6,6,null,null,"<?php echo Yii::t('cms', 'Icon property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"socialBgColor",6,6,null,null,"<?php echo Yii::t('cms', 'Background  property')?>","green","");

        });
        $(".addFin<?= $kunik?>").on("click",function(){  
            var keys<?= $blockCms['_id'] ?> = Object.keys(<?php echo json_encode($paramsData["financeur"]); ?>);
            var	lastContentK = 0; 
            if (keys<?= $blockCms['_id'] ?>.length!=0) 
                lastContentK = parseInt((keys<?= $blockCms['_id'] ?>[(keys<?= $blockCms['_id'] ?>.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockKey ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "financeur."+(lastContentK+1);
            var financeur = {};
            

            var obj = {
                subKey : (lastContentK+1),
                position :     lastContentK+1,
                link :     $(this).data("link"),
                logo :     $(this).data("logo"),
                
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add a file')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".positiontext").css("display","none"); 
                        }
                    },
                    "properties" : getProperties(obj),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        position = "";
                        value = {}; 
                        if(typeof sectionDyf.<?php echo $kunik?>ParamsData.financeur == "string"){
                            $.each( activeForm.jsonSchema.properties , function(k,val) {
                                tplCtx.value[k] = $("#"+k).val();
                            })
                            tplCtx.path = "financeur."+(lastContentK+1);
                        }else{
                            $.each( activeForm.jsonSchema.properties , function(k,val) { 
                                if(k == "position") {
                                    position = $("#"+k).val();
                                    if(typeof allFin[position] != "undefined" && position != lastContentK+1){                                    
                                        $.each( allFin , function(k1,val1) { 
                                            if(parseInt(k1) >= parseInt(position)){
                                                financeur[parseInt(k1)+1] = val1; 
                                                financeur[parseInt(k1)+1]["position"] = parseInt(k1)+1;
                                                if(typeof allLogo[k1] != "undefined" && typeof allLogo[k1]._id != "undefined"){
                                                    id = allLogo[k1]._id.$id ;
                                                    changeDocument(id,parseInt(k1)+1);
                                                }
                                            } 
                                            else {
                                                financeur[parseInt(k1)] = val1;
                                            }
                                        });
                                    }else{
                                        financeur = allFin; 
                                    }
                                }
                                value[k] =  $("#"+k).val();
                            });
                            financeur[position] = value;
                            tplCtx.path = "financeur";
                            tplCtx.value = financeur;
                        } 
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                });
                            } );
                        }
                    }
                }
            }; 
            dyFObj.openForm(activeForm );
           
        });
        $(".editFin<?= $kunik?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($paramsData["financeur"]); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>" ;
			tplCtx.path = "financeur."+(key);
			tplCtx.collection = "cms";
            var financeur = {};
			var obj = {
                subKey : key,
                position :     key,
                link :     $(this).data("link"),
                logo :     $(this).data("logo"),
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'File modification')?> "+$(this).data("title"),
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?php echo $blockKey ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        position = "";
                        value = {};
                        $.each( activeForm.jsonSchema.properties , function(k,val) {  
                            if(k == "position"){
                                position = $("#"+k).val();
                                if(typeof allFin[position] != "undefined" && position != obj.position){                                  
                                    $.each( allFin , function(k1,val1) { 
                                        if(parseInt(k1) >= parseInt(position)){ 
                                            financeur[parseInt(k1)+1] = val1;
                                            financeur[parseInt(k1)+1]["position"] = parseInt(k1)+1;
                                            if(typeof allLogo[k1] != "undefined" && typeof allLogo[k1]._id != "undefined"){
                                                id = allLogo[k1]._id.$id ;
                                                changeDocument(id,parseInt(k1)+1);
                                            }
                                        }
                                        else {
                                            financeur[parseInt(k1)] = val1;
                                        }
                                    });
                                    if(position > obj.position){
                                        financeur[obj.position] = undefined;
                                    }else{
                                        financeur[obj.position+1] = undefined;
                                    }
                                } else{
                                    financeur = allFin;
                                    if(position != obj.position){
                                        financeur[obj.position] = undefined;
                                        if(typeof obj.logo[0] != "undefined" && typeof obj.logo[0]._id != "undefined"){
                                            id = obj.logo[0]._id.$id ;
                                            changeDocument(id,position);
                                        }
                                    }
                                }   


                            } 
                            value[k] = $("#"+k).val(); 
                        });
                        if(typeof obj.logo[0] != "undefined" && typeof obj.logo[0]._id != "undefined"){
                            id = obj.logo[0]._id.$id ;
                            changeDocument(id,position);
                        } 
                        financeur[position] = value;
			            tplCtx.path = "financeur";
                        tplCtx.value = financeur;
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                });
                            } );
                        }
                    }
                }
            };          
            dyFObj.openForm( activeForm );				 
        });
        $(".deleteElement<?= $kunik?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?> ?",
            function(result){
                if (!result) {
                return;
                }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
                }
            }); 
			
		});
        function changeDocument(id,newPosition){ 
            tplCtx.id = id;
            tplCtx.collection = "documents"; 
            tplCtx.path = "subKey";
            tplCtx.value = newPosition;
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                    });
                } );
            }
        }
        function getProperties(obj={}){
            var props = {
                position : {
                    label : "<?php echo Yii::t('cms', 'Position')?>",   
                    inputType : "text",
                    value : obj["position"],
                    rules:{
                        number :true
                    }
                }, 
                link : {
                    label : "<?php echo Yii::t('cms', 'Link')?>",   
                    inputType : "text",
                    value : obj["link"]
                },
				logo : {
					inputType : "uploader",	
					label : "<?php echo Yii::t('cms', 'Logo')?>",
                    docType: "image",
                    itemLimit : 1,
					contentKey : "slider",
					domElement : obj["subKey"],		
                    filetype : ["jpeg", "jpg", "gif", "png"],
                    showUploadBtn: false,
                    endPoint :"/subKey/"+obj["subKey"],
                    initList : obj["logo"]
				}
            };
            return props;
        }
        function createFontAwesomeSelect<?php echo $kunik ?>(){
            dyFObj.init.buildEntryList = function(num, entry,field, value){
                mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
                countEntry=Object.keys(entry).length;
                defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
            
                str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
                    $.each(entry, function(e, v){
                        name=field+e+num;
                        classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                        placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                        str+="<div class='"+classEntry+"'>";
                        if(typeof v.label != "undefined"){
                            str+='<div class="padding-5 col-xs-12">'+
                                '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                            '</div>'+
                            '<div class="space5"></div>';
                        }
                        valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                        if(v.type=="hidden")
                            str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                        else if(v.type=="text")
                            str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                        else if(v.type=="textarea")
                            str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                        else if(v.type=="select"){
                            str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                            //This is the select Options-----------------//
                            str+= createSelectOptions(valueEntry,fontAwesome);
                            str+='</select>';             
                        }
                        str+="</div>";
                    });
                    str+='<div class="col-sm-1">'+
                            '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                        '</div>'+
                    '</div>';
                return str;
            }; 
        }

        function createSelectOptions(current,Obj){
            var html = "";
            $.each(Obj,function(k,v){
                html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
            });
            return html;
        }
    });
    var mybutton = document.getElementById("btnScrollTop");

    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }

    function topFunction() {
        $('html, body').animate({scrollTop : 0},600);
    }

    callParticle("particl-js");
</script>
