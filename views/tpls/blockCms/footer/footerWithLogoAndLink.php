<?php 
$keyTpl = "footerWithLogoAndLink";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$paramsData = [
  "height"=> "200",
  "contentIcone" =>"map-marker",
  "content"=>"Toutes les informations et la carte de France des CTE",
  "contentMarginTop" => "20",
  "contentMarginBottom" => ""

];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$image1 = [];
$baseUrl = Yii::app()->getRequest()->getBaseUrl(true);

$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"image1"
  ),"image"
);
foreach ($initImage1 as $key => $value) {
	if(!empty($value["imagePath"]) && Document::urlExists($baseUrl.$value["imagePath"]))
      $image1 []= $baseUrl.$value["imagePath"];
    elseif (!empty($value["imageMediumPath"]) && Document::urlExists($baseUrl.$value["imageMediumPath"]))
      $image1[] = $baseUrl.$value["imageMediumPath"];
}

$image2 = [];
$initImage2 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"image2"
  ),"image"
);
foreach ($initImage2 as $key => $value) {
	if(!empty($value["imagePath"]) && Document::urlExists($baseUrl.$value["imagePath"]))
      $image2 []= $baseUrl.$value["imagePath"];
    elseif (!empty($value["imageMediumPath"]) && Document::urlExists($baseUrl.$value["imageMediumPath"]))
      $image2[] = $baseUrl.$value["imageMediumPath"];
}
?>
<style type="text/css">
	.content<?= $kunik?> .logo<?= $kunik ?> {
		width : 200px;
	} 
	@media (max-width: 767px) {
		.content<?= $kunik?> .title-1 p{
			font-size: 18px !important;
		}
		.content<?= $kunik?> .logo<?= $kunik ?> {
			width : 100px;
		}
	}
</style>
<div class="col-xs-12 no-padding support-section text-center content<?= $kunik?>">

	<div class="col-xs-12 no-padding">
		<h3 class="title-1 sp-text  img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content" > <?= $paramsData["content"]?></h3>

		<!-- style="color:#24284D" Toutes les informations et la carte de France des CTE <a href="http://www.ecologique-solidaire.gouv.fr/contrat-transition-ecologique" target="_blank">ici</a>  -->

		<img  class="logo<?= $kunik ?>" src="<?php echo !empty($image1) ? $image1[0] :  Yii::app()->getModule('costum')->assetsUrl."/images/ctenat/PictoCTE.png"; ?> ">
		<img  class="logo<?= $kunik ?>"  src="<?php echo !empty($image2) ? $image2[0] : Yii::app()->getModule('costum')->assetsUrl."/images/ctenat/Gouvernement_RVB.png"; ?>">
		<br/>

	</div>

</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer la section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {  
					"image1" :{
						"inputType" : "uploader",
						"label" : "image",
						"docType": "image",
						"contentKey" : "slider",
						"domElement" : "image1",
						"itemLimit" : 1,
						"filetypes": ["jpeg", "jpg", "gif", "png"],
						"showUploadBtn": false,              
						"endPoint" :"/subKey/image1",
						initList : <?php echo json_encode($initImage1) ?>
					},
					"image2" :{
						"inputType" : "uploader",
						"label" : "image",
						"docType": "image",
						"contentKey" : "slider",
						"domElement" : "image2",
						"itemLimit" : 1,
						"filetypes": ["jpeg", "jpg", "gif", "png"],
						"showUploadBtn": false,              
						"endPoint" :"/subKey/image2",
						initList : <?php echo json_encode($initImage2) ?>
					},
					
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function (data) {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 

						tplCtx.value[k] = $("#"+k).val();
						if(k == "allContent")
							tplCtx.value[k] = data.allContent;
					});
					mylog.log("save tplCtx",tplCtx);
					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
			}
		};


		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
		});
	})
</script>