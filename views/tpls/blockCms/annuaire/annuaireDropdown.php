    <style>

        .carto-<?= $kunik?>{
            margin-top : -7%;
        }

        .dropmenu-<?= $kunik?>{
            margin-top: -10%;
            position: relative;
            margin-left: -5%;
        }
        .form-control{
            padding-left: 0.8em !important;
        }
        .dropdown-menu{
            padding : 0px 0px !important;
        }
        .dropdown-menu > li >  a {
            padding: 5px 10px !important;
            font-size: 1.2em;
        }
        .carto-h1-<?= $kunik?> {
            color : white;
        }
        .carto-d-<?= $kunik?> {
            box-shadow: 0px 0px 20px -2px #777;
            width: 80%;
            left: 10%;
            background : white;
            font-size : 1.5vw;
            border-radius: 0.2em;
        }
    	.title-carto-<?= $kunik?>{
    		font-size : 200%;
    		text-shadow: 0px 0px 7px gray;
    	}
        .c-description-<?= $kunik?>{
            width: 80%;
            left: 10%;
            font-size: 2vw;
            margin-top: 3%;
    	}

        .px-4{
            padding: 20px !important;
        }

    </style>
    <?php

        

        $typeObj = PHDB::findOne($costum["contextType"], array("slug"=>$costum["slug"]));
        
        /*if(isset($typeObj["costum"]["typeObj"])){
            $costum["typeObj"] = $typeObj["costum"]["typeObj"];
        }*/
        
        if(isset($typeObj["costum"]["typeObj"][$costum["contextType"]]["dynFormCostum"]["beforeBuild"]["properties"]["category"]["options"])){
            $typeActeurs = $typeObj["costum"]["typeObj"][$costum["contextType"]]["dynFormCostum"]["beforeBuild"]["properties"]["category"]["options"];
        }else{
            $typeActeurs = [];
        }

        $types = array();

        $index = 0;
        foreach ($typeActeurs as $tkey => $type){
            $types["typeActeurs$index"] = ["category" => $type];
            $index++;
        }
        # print_r($typeActeurs);
    ?>

    <?php 
        $keyTpl ="annuaireDropdown";
        
        $paramsData = [
            "textSlogan"=>"Lorem ipsum dolor sit amet...",
            "iconTitre"=>"",
            "sectionTitle"=>"CARTO/ANNUAIRE",
            "typeActeurs"=> $types,
            "imageCarte"=> "",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta voluptas explicabo nesciunt exercitationem placeat? Cupiditate harum debitis nesciunt deleniti hic? Similique obcaecati dicta hic aspernatur officia dolor eum ut. Sit."
        ];

      if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
          if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
          }
        }
      }

      //$paramsData["typeActeurs"] = $types;
    ?>

    <!-- ****************get image uploaded************** -->
    <?php 
        $initFiles = Document::getListDocumentsWhere(
            array(
                "id"=> $blockKey,
                "type"=>'cms',
                "subKey"=>  $kunik
            ),
            "image"
        );
        $imageCarte = [];

        foreach ($initFiles as $key => $value) {
            $imageCarte[]= $value["imagePath"];
        }

        if($imageCarte!=[]){
            $paramsData["imageCarte"] = $imageCarte[0];
        }
        
     ?>
    <!-- ****************end get image uploaded************** -->

    <div class="carto-<?= $kunik?> row content-<?= $kunik?>">
    	<div class="carto-h1-<?= $kunik?> col-xs-6 col-sm-6 no-padding">

    		<p class="sp-text img-text-bloc title-carto-<?= $kunik?> hidden-sm hidden-xs title-2" style="margin-left: 19.5%;" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="textSlogan">
                <?php echo $paramsData["textSlogan"]; ?>
            </p>
    	</div>
    	<div class="carto-img col-xs-6 col-sm-6" >

    	</div>
    	<div class="carto-d-<?= $kunik?> col-xs-12 col-sm-12">
    		<div class="row">
    			<div class=" <?php echo ($paramsData['imageCarte']!='')?'col-md-7 text-left':'col-md-12 text-center' ?> px-4">
                    <?php if($paramsData["sectionTitle"]!=""){
                        echo '<a href="javascript:;" data-hash="#search" class="lbh-menu-app" style="text-decoration : none;">';
                        
                            echo '<h1 class="title">';
                            
                            if($paramsData["iconTitre"]!=""){
                                echo '<i class="fa '.$paramsData["iconTitre"].'"></i>';
                                }
                            echo $paramsData["sectionTitle"].'<br></h1>';
                        } 
                     echo "</a>"
                    ?>
    				<p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content">
                      <?php echo "\n".$paramsData["content"]; ?>
                    </p>
                    <div class="text-center">
                    <?php foreach($typeActeurs as $aKey => $aType){ ?>
                        <a href="javascript:;" data-hash="#<?php echo $aKey ?>" data-search="" class="lbh-menu-app" style="text-decoration : none;">
                            <span class="badge badge-secondary" style="text-decoration : none; padding: 4px; margin: 2px">
                                #<?php echo $aType ?>
                            </span>
                        </a>
                    <?php } ?>
                    </div>
    			</div>
                <?php if($paramsData["imageCarte"]!=""){ ?>
        			<div class="col-md-5 text-right">
        				<a href="javascript:;" data-hash="#search" class="lbh-menu-app px-4" style="text-decoration : none;">
        					<img src="<?php echo $paramsData["imageCarte"]; ?>" class="img-responsive" style="max-height: 230px">
        				</a>
        			</div>
                <?php } ?>
    		</div>
    	</div>

    	<!-- Carto NEWS -->
    	<div class="row">
    		<div class="c-description-<?= $kunik?> col-sm-12 col-xs-12">
    			<div class="description col-md-6">
    			</div>
    			<div class="col-md-6">
    			</div>
    		</div>
    	</div>
    </div>

    <script type="text/javascript">
        
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        jQuery(document).ready(function() {
            sectionDyf.<?php echo $kunik ?>Params = {
              "jsonSchema" : {
                  "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                  "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                
                "properties" : {
                    "iconTitre" : {
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        inputType : "select",
                        options : <?= json_encode(Cms::$icones); ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconTitre
                    },
                    "sectionTitle" : {
                        "inputType" : "text",
                        "label" : "<?php echo Yii::t('cms', 'Directory title')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.sectionTitle
                    },
                    "imageCarte":{
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'Image on the right')?> :",
                        "docType" : "image",
                        "contentKey" : "slider",
                        "domElement" : "imageCarte",
                        "endPoint" :"/subKey/<?= $kunik?>",
                        value : sectionDyf.<?php echo $kunik ?>ParamsData.imageCartes,
                        "initList": <?php echo json_encode($initFiles) ?>
                    },
                    /*"typeActeurs" : {
                        inputType : "lists",
                        label : "Types d'acteurs",
                        entries: {
                            category: {
                                type:"text",
                                label:"Type d'acteurs",
                                placeholder: ""
                            }
                        },
                        values: sectionDyf.<?php //echo $kunik ?>ParamsData.typeActeurs
                    }*/
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {  
                  tplCtx.value = {};
                  let types = {
                    "placeholder" : "Identifiez-vous à un acteur",
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Actor category')?>*",
                    "class" : "form-control",
                    "order" : 1,
                    "labelInformation" : "Catégorie d'acteur",
                    "options":{}
                  };
                  let apps = {};
                  $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    /*if(k=="typeActeurs"){
                        $.each(data.typeActeurs, function(index, va){

                            let hashKey = "";
                            
                            var match = va.category.replace("", " ");
                            
                            if (match) {
                                hashKey = toCamelCase(va.category.substring(0, match.index)).trim();
                            }else{
                                hashKey = toCamelCase(va.category).trim();
                            }

                            types["options"][""+hashKey] = va.category;

                            apps["#"+hashKey] = {
                                    "hash" : "#app.search",
                                    "icon" : "search",
                                    "filterObj" : "costum.views.custom.<?php //echo $costum["slug"] ?>.filters",
                                    "urlExtra" : "/page/"+hashKey,
                                    "subdomainName" : "Tous les acteurs "+hashKey,
                                    "placeholderMainSearch" : "what are you looking for ?",
                                    "useHeader" : false,
                                    "useFilter" : true,
                                    "useFooter" : true,
                                    "filters" : {
                                        "types" : [ 
                                            "organizations"
                                        ]
                                    },
                                    "searchObject" : {
                                        "indexStep" : "0"
                                    }
                                };
                        });
                        //tplCtx.value[k] = types;
                    }*/

                    if (k == "parent")
                      tplCtx.value[k] = formData.parent;

                    if(k == "items")
                      tplCtx.value[k] = data.items;
                      mylog.log("Test",data.items)
                  });
                  console.log("save tplCtx",tplCtx);

                  if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                  else {
                    var params = {"slug": '<?php echo $costum["slug"]; ?>', "category": types, "filters":apps}
                    ajaxPost(
                        null,
                        '<?= Yii::app()->baseUrl; ?>/costum/filiere/updatecategory',
                        params,
                        function(data){
                            toastr.success("Enregistrement des donées");
                        },
                        function(xhr,textStatus,errorThrown,data){
                            toastr.error("Fail :"+JSON.stringify(xhr)+textStatus+JSON.stringify(errorThrown)+JSON.stringify(data));
                        });

                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');

						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  });
                }
            }
        }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });

    //Dropdown
    $('.dropdown-a').mouseover(function() {
        $(this).attr("aria-expanded",true);
        $(".description").addClass("open");
    });

    // $('.img-dropdown').mouseover(function() {
    //     $(".description").addClass("open");
    // });

    $(".dropdown-menu").mouseover(function(){
        $(".description").addClass("open");
    });

    $(".dropdown-a").mouseleave(function(){
        if($('.description').hasClass('open')){
            $(".dropdown-a").attr("aria-expended",false);
            $(".description").removeClass("open");
        }
    });

    $(".dropdown-menu").mouseleave(function(){
        $(".dropdown-a").attr("aria-expended",false);
        $(".description").removeClass("open");
    });

    function toCamelCase(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
      });
    }
    </script>
