<?php if(Authorisation::isInterfaceAdmin()){ ?>
<div class="container">
	<h3 class="sp-text" data-id="<?= $blockKey ?>" data-field="blockTitle"><?= (isset($blockCms) && isset($blockCms["blockTitle"]))?$blockCms["blockTitle"]:"Les différents badges des membres du club" ?></h3>
	<div class="sp-text" data-id="<?= $blockKey ?>" data-field="blockSubTitle"><?= (isset($blockCms) && isset($blockCms["blockSubTitle"]))?$blockCms["blockSubTitle"]:"En plus d'être membre, certains membres peuvents prendre des casquettes supplémentaires. Découvrez ce que signifie chaque badge et comment accéder à chaque rôle." ?></div>
</div>
<div id="badgeLists"></div>
<script type="text/javascript">
	$("document").ready(function(){
		ajaxPost(
			null, 
			baseUrl+'/co2/badges/finder/email/false/source/'+costum.contextSlug, 
			null, 
			function(htmlData){
				$("#badgeLists").append(htmlData);
				if(!costum.isCostumAdmin){
					$("#badgeLists .container .row")[0].remove();
				}
			},
			null
		);
	});
</script>
<?php } ?>