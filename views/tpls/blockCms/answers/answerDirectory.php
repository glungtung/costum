<style>
    .ans-dir-<?= $kunik ?> hr {
        border: 1px solid #858585;
    }
    .ans-dir-<?= $kunik ?> .team {
        margin-top: 25px;
    }
    .ans-dir-<?= $kunik ?> .team h1 {
        font-weight: normal;
        font-size: 22px;
        margin: 10px 0 0 0;
    }
    .ans-dir-<?= $kunik ?> .team h2 {
        font-size: 16px !important;
        font-weight: lighter;
        margin-top: 5px;
    }
    .ans-dir-<?= $kunik ?> .team .img-box {
        opacity: 1;
        display: block;
        position: relative;
    }
    .ans-dir-<?= $kunik ?> .team .img-box:after {
        content: "";
        opacity: 0;
        background-color: rgba(0, 0, 0, 0.75);
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
    }
    .ans-dir-<?= $kunik ?> .img-box ul {
        position: absolute;
        z-index: 2;
        bottom: 50px;
        text-align: center;
        width: 100%;
        padding-left: 0px;
        height: 0px;
        margin: 0px;
        opacity: 0;
    }
    .ans-dir-<?= $kunik ?> .team .img-box:after, .img-box ul, .img-box ul li {
        -webkit-transition: all 0.5s ease-in-out 0s;
        -moz-transition: all 0.5s ease-in-out 0s;
        transition: all 0.5s ease-in-out 0s;
    }
    .ans-dir-<?= $kunik ?> .img-box ul i {
        font-size: 20px;
        letter-spacing: 10px;
    }
    .ans-dir-<?= $kunik ?> .img-box ul li {
        width: 30px;
        height: 30px;
        text-align: center;
        border: 1px solid #fff;
        margin: 2px;
        padding: 5px;
        display: inline-block;
    }
    .ans-dir-<?= $kunik ?> .img-box a {
        color: #fff;
    }
    .ans-dir-<?= $kunik ?> .img-box:hover:after {
        /* opacity: 1;
        */
    }
    .ans-dir-<?= $kunik ?> .img-box:hover ul {
        /* opacity: 1;
        */
    }
    .ans-dir-<?= $kunik ?> .img-box ul a {
        -webkit-transition: all 0.3s ease-in-out 0s;
        -moz-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }
    .ans-dir-<?= $kunik ?> .img-box a:hover li {
        border-color: #FFEA05;
        color: #FFEA05;
    }
    .ans-dir-<?= $kunik ?> .img-box a {
        color: #FFEA05;
    }
    .ans-dir-<?= $kunik ?> .img-box a:hover {
        text-decoration: none;
        color: #519548;
    }
    .ans-dir-<?= $kunik ?> .card-team{
        position: relative;
        /* height: 614px;
        */
        width: 100%;
        padding-top: 15px;
    }
    .ans-dir-<?= $kunik ?> .team{
        box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
        flex: 0 0 calc(98%/3);
        padding: 5px 10px;
    }
    .ans-dir-<?= $kunik ?> .bodySearchContainer{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir-<?= $kunik ?> .divEndOfresults{
        display: none;
    }
    .ans-dir-<?= $kunik ?> .team .desc{
        word-break: break-word;
        line-height:120%;
        text-align: left !important;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp:3;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .ans-dir-<?= $kunik ?> .img-responsive-action{
        width: 100%;
        height: 253px;
        object-fit: cover;
    }
    @media only screen and (min-width : 320px) {
        .ans-dir-<?= $kunik ?> .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }
    /* Extra Small Devices, Phones */
    @media only screen and (min-width : 480px) {
        .ans-dir-<?= $kunik ?> .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }
    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 49%;
            padding: 5px 10px;
        }
    }
    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        .ans-dir-<?= $kunik ?> .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/2);
            padding: 5px 10px;
        }
    }
    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        .ans-dir-<?= $kunik ?> .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/3);
            padding: 5px 10px;
        }
    }
    .ans-dir-<?= $kunik ?> #filterContainerInside {
        overflow-y: unset !important;
        max-height: 800px;
    }
    .ans-dir-<?= $kunik ?> #sticky,.aap-footer,.aap-options-btn,.rater-sheet,.openAnswersComment,.votant-modal,.view-contributors,.rebtnFinancer{
        display: none !important;
    }
    .ans-dir-<?= $kunik ?> .portfolio-modal.modal#modal-action .modal-content{
        background: rgb(0 0 0 / 72%);
    }
    .ans-dir-<?= $kunik ?> .portfolio-modal.modal#modal-action .modal-content .container{
        background: #fff;
        padding-top: 10px;
        border-radius:5px;
    }
    .ans-dir-<?= $kunik ?> .portfolio-modal.modal#modal-action .modal-content .container .row h2{
        padding-left:15px;
    }
    .ans-dir-<?= $kunik ?> .portfolio-modal.modal#modal-action .close-modal .lr .rl, .portfolio-modal.modal#modal-action .close-modal .lr {
        background-color: #fff;
    }
</style>

<div class="container padding-bottom-20 padding-left-0 padding-right-0 ans-dir-<?= $kunik ?>">
    <div class="row">
        <h2 class="text-left">Les tiers-lieux</h2>
        <hr />
    </div>
    <div class="col-md-12 no-padding">
        <div id='filterContainercacs' class='searchObjCSS'></div>
        <div class='headerSearchIncommunity no-padding col-xs-12'></div>
        <div class='bodySearchContainer margin-top-30'>
            <div class='no-padding col-xs-12' id='dropdown_search'>
            </div>
        </div>
        <div class='padding-top-20 col-xs-12 text-left footerSearchContainer'></div>
    </div>
</div>

<script>
    $(function(){
        let <?=$kunik?>Obj = {
            events : function(cobj){

            },
            prepareData : function(params){
                var results = {
                    reseau : notEmpty(params?.links?.organizations) ? Object.values(params?.links?.organizations)[0].name : null,
                    reseauId : notEmpty(params?.links?.organizations) ? Object.keys(params?.links?.organizations)[0] : null,
                    created : ucfirst(moment.unix(params.created).format('dddd Do MMMM YYYY h:mm:ss')),
                    creator : params.user
                }
                return results;
            }
        }

        directory.<?=$kunik?>PanelHtml = function(params){
            var params = <?=$kunik?>Obj.prepareData(params);
            var str = `
                <div class="team">
                    <div class="card-team">
                        <div class="img-box">
                            <img class="img-responsive-action" alt="Responsive Team Profiles" src="${exists(params?.answers?.aapStep1?.images) && exists(params.answers?.aapStep1?.images[0]) ? params.answers?.aapStep1?.images[0] : defaultImage}" />
                            <ul class="text-center">
                                <a href="#">
                                    <li><i class="fa fa-facebook"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-twitter"></i></li>
                                </a>
                                <a href="#">
                                    <li><i class="fa fa-linkedin"></i></li>
                                </a>
                            </ul>
                        </div>
                        <h1>
                            <a href="javascript:;" class="aapgetaapview2">
                                ${params.reseau}
                            </a>
                        </h1>
                        <h2>
                            <a href="javascript:;" target="_blank">
                                ${params.reseau}
                            </a>
                        </h2>
                        <hr />
                        <p class="text-left desc">${params.created}<br>
                            Par ${params.creator}
                        </p>
                        <div class="btn-group btn-group-xs pull-right like-project-container hidden">
                         
                        </div>
                    </div>
                </div>
            `;
            return str;
        }

        var paramsFilters<?=$kunik?>= {
            urlData : baseUrl+"/costum/aap/directory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7",
            container : "#filterContainercacs",
            interface : {
                events : {
                    //page : true,
                    scroll : true,
                    //scrollOne : true
                }
            },
            header : {
                dom : ".headerSearchIncommunity",
                options : {
                    left : {
                        classes : 'col-xs-8 elipsis no-padding',
                        group:{
                            count : true,
                            types : true
                        }
                    }
		        },
            },
            defaults : {
                notSourceKey : true,
                textPath : "answers.aapStep1.titre",
                types : ["answers"],
                indexStep: 10,
                forced: {
                    filters: {
                    }
                },
                fields:[],
                sortBy:{updated : -1}
            },
            results : {
                dom:".bodySearchContainer",
                smartGrid : true,
                renderView :"directory.<?=$kunik?>PanelHtml",
                map : {
            	    active : false
                },
                smartGrid: true,
                events : function(fObj){
                    $(fObj.results.dom+" .processingLoader").remove();
                    <?=$kunik?>Obj.events(<?=$kunik?>Obj);
                    coInterface.bindLBHLinks();
                    directory.bindBtnElement();
                    coInterface.bindButtonOpenForm();
                },
            },
            filters : {
                text : true,
            }
        };
        var filter<?=$kunik?> = searchObj.init(paramsFilters<?=$kunik?>);
        filter<?=$kunik?>.search.init(filter<?=$kunik?>);
    })
</script>