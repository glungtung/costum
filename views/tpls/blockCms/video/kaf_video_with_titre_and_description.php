<?php 
$keyTpl = "kaf_video_with_titre_and_description";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
    "title" => "Video",
    "description" => "Loren upsum dolor",
    "bgBoxColor" => "#eeeeee"   
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>
<style type="text/css">
    .box-video {
        background: <?php echo $paramsData["bgBoxColor"]; ?>;
        padding: 30px;
        margin: 0 0 24px 0;
    }
    @media (max-width: 767px) {
    	.box-video {
	        padding: 10px;
	        margin: 0 0 15px 0;
	    }
    }
	.morelink {
	    font-size: 16px;
	    font-weight: bold;
	    margin-left: 2px;
	    color: #ff9800;
	    cursor: pointer;
	}

    .rte .boxHeadline {
        font-weight: 400;
        margin: 0 0 15px 0;
    }
    .rte .boxHeadline+.boxHeadlineSub {
        font-weight: 400;
        margin: 0px 0 20px 0;
    }
    .section-title { margin-bottom: 40px; }
    .morecontent {
	    display: none;
	}
	.morecontent {
  		display: none;
	}


</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
	<div class="section-title">
        <h2 class="title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></h2>
        <div class="title-2 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description"><?php echo $paramsData["description"]; ?></div>
    </div>
    <?php 
        if (isset($content)) {
            foreach ($content as $key => $value) {?> 
		    <div class="box-video rte col-xs-12">
		        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">
		            <iframe width="100%" height="281" src="<?= $value["linkvideo"] ?>" frameborder="0" allowfullscreen=""></iframe>
		        </div>

		        <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">
		            <h2 class="boxHeadline title-3"><?= $value["titrevideo"] ?></h2>
		            <h3 class="boxHeadlineSub title-4"><?= $value["soustitrevideo"] ?></h3>
		            <div class="title-5 more markdown"><?= $value["descriptionvideo"] ?></div>
		            <div class="col-xs-12 text-center">
		        	<?php if(Authorisation::isInterfaceAdmin()){ ?>
                        <a href="javascript:;" class="btn btn-primary btn-sm editElement<?= $blockCms['_id'] ?> editSectionBtns"
                                data-key="<?= $key ?>"
                                data-linkvideo="<?= $value["linkvideo"] ?>" 
                                data-titrevideo="<?= $value["titrevideo"] ?>"
                                data-soustitrevideo="<?= $value["soustitrevideo"] ?>"
                                data-descriptionvideo="<?= $value["descriptionvideo"] ?>"
                            ><i class="fa fa-pencil"></i></a>

                        <a  href="javascript:;" class="btn btn-sm btn-danger deletePlan<?= $blockKey ?> "
                            data-key="<?= $key ?>" 
                            data-id ="<?= $blockKey ?>"
                            data-path="content.<?= $key ?>"
                            data-collection = "cms">
                            <i class="fa fa-trash"></i>
                        </a>
                        
                    <?php } ?>
		        </div>
		        </div>
		        
		    </div>
	<?php } 
            } ?>
    <div class="text-center editSectionBtns">
        <div class="" style="width: 100%; display: inline-table; padding: 10px;">
            <?php if(Authorisation::isInterfaceAdmin()){?>
                <div class="text-center addElement<?= $blockCms['_id'] ?>">
                    <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add a video')?></button>
                </div>  
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {                    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : { 
                    "title" : {
                        label : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.title
                    },    
                    "description" : {
                        label : "<?php echo Yii::t('cms', 'Description')?>",
                        "inputType" : "textarea",
            			"markdown" : true,
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.description
                    },            
                    "bgBoxColor" : {
                        label : "<?php echo Yii::t('cms', 'Background color of the box')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.bgBoxColor
                    }
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("<?php echo Yii::t('cms', 'updated item')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            } );
                        }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        

        $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
            var deleteObj ={};
            deleteObj.id = $(this).data("id");
            deleteObj.path = $(this).data("path");          
            deleteObj.collection = $(this).data("collection");
            deleteObj.value = null;
            bootbox.confirm(trad.areyousuretodelete,
            function(result){
              if (!result) {
                return;
              }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
              }
            }); 
        });

        
        $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
            //var contentLength = Object.keys(<?php //echo json_encode($content); ?>).length;
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>"
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(key);
            var obj = {
                linkvideo :         $(this).data("linkvideo"),
                titrevideo:    		$(this).data("titrevideo"),
                soustitrevideo:     $(this).data("soustitrevideo"),
                descriptionvideo:   $(this).data("descriptionvideo")
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Edit video')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        $.each( activeForm.jsonSchema.properties , function(k,val) { 
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                 dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                        // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                            } );
                        }

                    }
                }
            };          
            dyFObj.openForm( activeForm );
        });


        $(".addElement<?= $blockCms['_id'] ?>").click(function() { 
            var keys = Object.keys(<?php echo json_encode($content); ?>);
            var lastContentK = 0; 
            if (keys.length!=0) 
                lastContentK = parseInt((keys[(keys.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(lastContentK+1);
            var obj = {
                linkvideo :         $(this).data("linkvideo"),
                titrevideo:    		$(this).data("titrevideo"),
                soustitrevideo:     $(this).data("soustitrevideo"),
                descriptionvideo:   $(this).data("descriptionvideo")
            };

            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add a video')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,lastContentK+1),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                        // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });

        
        function getProperties(obj={},subKey){
            var props = {
                titrevideo : {
                    label : "<?php echo Yii::t('cms', 'Title of the video')?>",
                    "inputType" : "text",
                    value : obj["titrevideo"]
                },
                soustitrevideo : {
                    label : "<?php echo Yii::t('cms', 'Subtitle of the video')?>",
                    "inputType" : "text",
                    value : obj["soustitrevideo"]
                },
                descriptionvideo : {
                    label : "<?php echo Yii::t('cms', 'Description')?>",
                    "inputType" : "textarea",
            		"markdown" : true,
                    value :  obj["descriptionvideo"]
                }, 
                linkvideo : {
                    label : "<?php echo Yii::t('cms', 'Url of the video')?>",
                    "inputType" : "text",
                    value : obj["linkvideo"]
                } 
            };
            return props;
        }
    });


    function AddReadMore() {
        var showChar = 160;
        var ellipsestext = "...";
        var moretext = "<?php echo Yii::t('cms', 'Read more')?>";
        var lesstext = "<?php echo Yii::t('cms', 'Read less')?>";
        $('.more').each(function() {
          var content = $(this).html();
          var textcontent = $(this).text();

          if (textcontent.length > showChar) {

            var c = textcontent.substr(0, showChar);
            //var h = content.substr(showChar-1, content.length - showChar);

            var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

            $(this).html(html);
            $(this).after('<a href="" class="morelink btn btn-default">' + moretext + '</a>');
          }

        });

        $(".morelink").click(function() {
          if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
            $(this).prev().children('.morecontent').fadeToggle(500, function(){
              $(this).prev().fadeToggle(500);
            });
           
          } else {
            $(this).addClass("less");
            $(this).html(lesstext);
            $(this).prev().children('.container').fadeToggle(500, function(){
              $(this).next().fadeToggle(500);
            });
          }
          
          return false;
        });
    }
setTimeout(function(){ 
	AddReadMore();
}, 1500);

</script>