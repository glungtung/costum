<?php 
$keyTpl = "quatreDerniersVideo";
$paramsData = [
	];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$reportage = Poi::getPoiByTagsAndLimit(4, 15, array(), array("source.key" => $costum["slug"], "type" =>"video"), array("name", "urls","medias", "description", "shortDescription", "tags"));

?>
<style type="text/css">
	.<?= $kunik?>{
		margin-top: 3%;
	}
	.<?= $kunik?> iframe{
		width: 100%;
		height: 310px;
	}
	.addElement<?= $blockCms['_id'] ?> {
		padding: 7px;
		font-size: 18px;
		border-radius: 35px;
		margin-bottom: 2%;
		color: #fff;
		border: 1px solid #f5853c;
		background: #f5853c;
	}
	.addElement<?= $blockCms['_id'] ?>:hover, .addElement<?= $blockCms['_id'] ?>:focus {
		padding: 7px;
		font-size: 18px;
		border-radius: 35px;
		margin-bottom: 1%;
		color: #fff;
		border: 1px solid #f5853c;
		background: #f5853c;
	}
	
	@media (max-width: 978px) {
		.addElement<?= $blockCms['_id'] ?> {
			padding: 5px;
			font-size: 14px;
			border-radius: 26px;
			margin-bottom: 1%;
		}
		.<?= $kunik?> iframe{
			height: 250px;
		}

	}
</style>

<div class="sp-cms-container <?= $kunik?>">
	<?php 
		foreach ($reportage as $key => $value) {
			$urls = $value["urls"][0];
			if(strpos($urls, "vimeo") !== false){
				$youtubeId = array_reverse(explode("/", $value["urls"][0]))[0];

			?>

				<div class="col-md-6">
					<iframe class="content-video" src="https://player.vimeo.com/video/<?=$youtubeId ?>" allowfullscreen>
					</iframe>
				</div>
			<?php } 
			if(strpos($urls, "youtu") !== false){
				if(strpos($urls, "=") !== false){
					$youtubeId = array_reverse(explode("=", $value["urls"][0]))[0];
				} else{
					$youtubeId = array_reverse(explode("/", $value["urls"][0]))[0];
				}
			?>
				<div class="col-md-6">
					<iframe class="content-video" src="https://www.youtube.com/embed/<?=$youtubeId ?>" allowfullscreen>
					</iframe>
				</div>
			<?php }	
		}
	?>
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
                "label" : "<?php echo Yii::t('cms', 'Name of the video')?>",
                "placeholder" : "<?php echo Yii::t('cms', 'Name of the video')?>",
				"icon" : "fa-cog",
				"properties" : {
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
                      		toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
		                      $("#ajax-modal").modal('hide');
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
						} );
					}

				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	});
</script>
