<?php 
$keyTpl = "media";
$paramsData = [];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$poi = Poi::getPoiByTagsAndLimit(0, 15, array(), array("source.key" =>$costum["slug"], "type" =>"video"), array("tags"));
$allTags = array();
foreach ($poi as $key => $value) {
	if (isset($value["tags"])) {
		foreach ($value["tags"] as $k => $v) {
			$allTags[] = $v;
		}
	}
}
?>
<style type="text/css">

	.<?= $kunik?>{
		margin-top: 3%;
	}
	.<?= $kunik?> iframe{
		width: 100%;
		height: 310px;
		
	}
	.<?= $kunik?> .videoYoutube<?= $kunik?>{
		height: 50vh;
	}

	.addVideo<?= $kunik?> {
		margin-left:  50px; 
		font-size: 18px;
		border-radius: 35px;
		margin-bottom: 1%;
		color: #fff;
		border: 1px solid #f5853c;
		background: #f5853c;

	}
	.addVideo<?= $kunik?>:hover, .addVideo<?= $kunik?>:focus {
		color: #fff;
		background: #f5853c;
	}
	
	@media (max-width: 978px) {
		.<?= $kunik?> iframe{
			height: 250px;
		
		}
		.addVideo<?= $kunik?> {
			padding: 6px;
			font-size: 14px;
		}
	}
	.<?= $kunik?> .searchVideo {
	  margin-left: 15%; 
	  margin-bottom:5%
	}
  .<?= $kunik?> .searchBar-filters .search-bar{
    margin-top: 5px;
    border-radius: 20px 0px 0px 20px !important;
    box-shadow: none;
    width: 300px;
    max-width: 700px;
    border-color: #f5833c;
  }
  .<?= $kunik?> .searchBar-filters .main-search-bar-addon {
    width: 15%;
    background-color: #f5833c!important;
    border: 1px solid #f5833c;
    font-size: 14px;
    padding: 9px 7px;
  }
  .<?= $kunik?> #filterVideo<?= $kunik?> li {
  	list-style: none;
  }
  .<?= $kunik?> #filterVideo<?= $kunik?> .dropdown .btn-menu  {
  	padding: 10px 15px;
    font-size: 16px;
    border: 1px solid #f5853c;
    color: #f5853c;
    top: 0;
    position: relative;
    text-align: center;
    border-radius: 20px;
    text-decoration: none;
    background: #fff;
    line-height: 20px;
    display: inline-block;
    margin-left: 5px;
  }
  .<?= $kunik?> #filterVideo<?= $kunik?> .dropdown   {
  	    float: left;
    display: grid;
    margin-bottom: 5px;
    margin-top: 5px;
    margin-right: 10px;
    padding-top: 0px;
    padding-bottom: 0px;
  }
   .<?= $kunik?> #filterVideo<?= $kunik?> .dropdown .dropdown-menu  {
   	    border: 1px solid #f5833c;
    padding: 0px;
  	    position: absolute;
	    overflow-y: visible!important;
	    top: 50px;
	    left: 5px;
	    width: 250px;
	    border-radius: 2px;
  }
  .<?= $kunik?> #filterVideo<?= $kunik?> .dropdown .dropdown-menu:before  {
   	        
    left: 27px;
  }
  .<?= $kunik?> .searchBar-filters .main-search-bar-addon i{
    font-size: 20px
  }
  .<?= $kunik?> .btn-group .btnHeader {
    font-size: 25px;
    width: 100%;
    height: 50px;
    border-radius: 20px;
    margin-top: 4px;
    background: #f5833c;
    color: white;
}
}
.<?= $kunik?> .searchBar-filters .main-search-bar-addon {
  width: 15%;
  height: 50px;
  background-color: #f5833c!important;
  font-size: 14px;
  border: 1px solid #f5833c;
  padding: 12px 7px;
}
.<?= $kunik?> .searchBar-filters .main-search-bar-addon i{
  font-size: 25px
}
.<?= $kunik?> .searchBar-filters .nameActeur{
  height: 50px;
  margin-top: 5px;
  border-radius: 20px 0px 0px 20px;
  box-shadow: none;  
  width: 400px;
  max-width: 600px;
  border-color: #f5833c;
}
.<?= $kunik?> .searchBar-filters .motClef{
  height: 50px;
  margin-top: 5px;
  border-radius: 20px 20px 20px 20px;
  box-shadow: none;  
  width: 300px;
  max-width: 450px;
  border-color: #f5833c;
}
.<?= $kunik?> .btn-group .dropdown-item {
   width: 100%;
   text-align: left;
   font-size: 18px;

}
.<?= $kunik?> #activeFilters .filters-activate{
	background-color: #327753;
}

@media (max-width: 414px){
	.<?= $kunik?>{
		margin-top: 0;
	}
	.<?= $kunik?> .searchBar-filters {
  		display: none;
	}
	.<?= $kunik?>  .dropdown{
  		width: 100% !important;
  	}
  	.<?= $kunik?> .searchBarInMenu #main-search-bar{
	    
	    border: 1px solid #f5833c !important;
  	}
  	.<?= $kunik?> .searchBarInMenu #main-search-bar-addon {
  		padding: 10px 10px;
	    font-size: 14px;
	    height: 40px;
	    background-color: #f5833c !important;
	    border: 1px solid #f5833c !important;
	}
	.<?= $kunik?> .searchBarInMenu #main-search-bar-addon i{
		font-size: 20px;
	}
	.<?= $kunik?> #show-filters-xs{
	    border: 1px solid #f5833c !important;
		background-color: #e2e2e2;
	}
	.<?= $kunik?> #filterVideo<?= $kunik?> .dropdown .btn-menu  {
		border-radius: 0;
	}
}
	.edit<?= $kunik?> .material-button{
		border-radius: 0 0 50% 0 !important;
	}
	.edit<?= $kunik?>{
		position: absolute;
		display: none;
		top: 0;
		left: 15px;
		z-index: 9999;
	}
	.video<?= $kunik?>:hover .edit<?= $kunik?>{
		display: block;
	}


</style>
<div class="sp-cms-container <?= $kunik?>">	 
	<div id="filterVideo<?= $kunik?>"></div>
	<div id="videoResult"></div>
</div>
<script type="text/javascript">
	var activity = [
		"<?php echo Yii::t('cms', 'Associations')?>",
		"<?php echo Yii::t('cms', 'Artists')?>",
		"<?php echo Yii::t('cms', 'Agriculturists')?>",
		"<?php echo Yii::t('cms', 'Marketers')?>",
		"<?php echo Yii::t('cms', 'Fishers')?>",
		"<?php echo Yii::t('cms', 'Wellness')?>",
		"<?php echo Yii::t('cms', 'Third Places')?>",
		"<?php echo Yii::t('cms', 'Artisans')?>"
	];
	var tags = <?= json_encode(array_unique($allTags))?> ;
	var paramsFilter= {
	 	container : "#filterVideo<?= $kunik?>",
	 	options : {
	 		tags : {
	 			verb : '$all'
	 		}
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	results :{	 		
 			dom : '#videoResult',
 			renderView : "videoHtml<?= $kunik?>"
        },
	 	defaults : {
	 		filters :{
	 			type : "video"
	 		},
 			fields : ["urls"],
	 		types : ["poi"]
	 	},
	 	filters : {

	 		text : true,
	 		typePlace : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Type",
	 			event : "tags",
	 			list : activity
	 		},
	 		tags:{
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Mot clef",
	 			event : "tags",
	 			list : tags
	 		}
	 	}
	};
	var filterSearch={};
	function addVideo(){
        var dyfPoi={
          "beforeBuild":{
            "properties" : {
            	"name": {
                    "label" : "<?php echo Yii::t('cms', 'Actor\'s name')?>",
                    "placeholder" : "<?php echo Yii::t('cms', 'Actor\'s name')?>",
                    "order" : 1,

                },
                "category" : {
	                "placeholder" : "<?php echo Yii::t('common', 'Activity')?>",
	                "inputType" : "select",
	                "label" : "<?php echo Yii::t('common', 'Activity')?>",
	                "order" : 2,
	                "options" : {				
	                  "Associations" : "<?php echo Yii::t('cms', 'Associations')?>",
	                  "Artistes" : "<?php echo Yii::t('cms', 'Artists')?>",
	                  "Agriculteurs" : "<?php echo Yii::t('cms', 'Agriculturists')?>",
	                  "Maraîchers" : "<?php echo Yii::t('cms', 'Marketers')?>",
	                  "Pêcheurs" : "<?php echo Yii::t('cms', 'Fishers')?>",
	                  "Bien-être" : "<?php echo Yii::t('cms', 'Wellness')?>",
	                  "Tiers Lieux" : "<?php echo Yii::t('cms', 'Third Places')?>",
	                  "Artisans" :"<?php echo Yii::t('cms', 'Artisans')?>"
	                },    
	                "rules":{
		                "required":true
		            }
              },
            	"urls": {
                    "label" : "<?php echo Yii::t('cms', 'Url of the video')?>",
                    "order" : 3,
                    "rules":{
		                "required":true
		             }
                },
              
              "tags" : {
                "label" : "<?php echo Yii::t('cms', 'key word')?> ",
                "inputType" : "tags",
                "tagsList" : "motClef",
                "value" :["acteurs kosasa"],
                "rules":{
		            "required":true
		        },
                "order" : 4
              },
            }
          },
          "onload" : {
            "actions" : {
              	"setTitle" : "<?php echo Yii::t('cms', 'Add a video')?>",
              	"src" : { 
                	"infocustom" : "<?php echo Yii::t('cms', 'Fill in the field')?>"
              	},
              	"presetValue" : {
                    "type" : "video"
                },
	            "hide" : {
	              	"breadcrumbcustom" : 1,
                    "imageuploader" : 1,
                    "urlsarrayBtn" : 1,
                    "parentfinder" : 1,
                    "removePropLineBtn" : 1
	            }
        	}
           }

          }
        dyfPoi.afterSave = function(data){
          dyFObj.commonAfterSave(data, function(){
            mylog.log("dataaaa", data);
            saveTags(data.id,data.map.category,data.map.tags);
           	urlCtrl.loadByHash(location.hash);

          });
        }
        dyFObj.openForm('poi',null, null,null,dyfPoi);
      }
      function saveTags(idPoi,category,tags){
        tags.push(category) ;
        var tplCtx = {};
        tplCtx.id = idPoi;
        tplCtx.collection = "poi";
        tplCtx.path = "tags";
        tplCtx.value = tags;
        mylog.log("tplCtx0000", tplCtx);
        dataHelper.path2Value( tplCtx, function(params) {
           
        })
      }

	  	function deleteVideo(id) {
		     bootbox.confirm(trad.areyousuretodelete,
		        function(result){
		            if (!result) {
		               	return;
		            } 
		            else {
		            	var type = "poi";
           				var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
		            $.ajax({
		                type: "POST",
		                url: urlToSend,
		                dataType : "json"
		            })
		            .done(function (data) {
		                if ( data && data.result ) {
							toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
		                    $("#"+type+id).remove();
		                } else {
		                   toastr.error("something went wrong!! please try again.");
		                }
		                urlCtrl.loadByHash(location.hash);
		            });
		   		}
			}
		);
	}
	function videoHtml<?= $kunik?>(params){
		mylog.log("logggggggg",params);
		var url = params.urls[0];		
		//var resultat = url.indexOf("youtube");

		var videoId = "";
		var src = "";
		var str = "";

		if (url.indexOf("=") != -1) {
			videoId = (((url).split("=")).reverse())[0];
		} else {
			videoId = (((url).split("/")).reverse())[0];
		}

		if (url.indexOf("vimeo") != -1) {
			src = "https://player.vimeo.com/video/"+videoId
		}
		if (url.indexOf("youtu") != -1) {
			src = "https://www.youtube.com/embed/"+videoId
		}
		if (url.indexOf("dailymotion") != -1) {
			src = "https://www.dailymotion.com/embed/video/"+videoId
		}
		str += `<div class="col-md-6 video<?= $kunik?>">`;
		<?php if(Authorisation::isInterfaceAdmin()){ ?>
			str +=`<div class = "edit<?= $kunik?>">
			<a href="#page.type.poi.id.`+params.id+`" class="btn material-button lbh-preview-element"> 
			<i class="fa fa-edit"></i>
			</a>
			</div>`
		<?php }?>
		str +=` <iframe class="content-image" src="`+src+`" allowfullscreen >
		</iframe>
		</div>`;
			
		// if( url.indexOf("youtu") != -1){
		// 	if (url.indexOf("=") != -1) {
		// 		videoId = (((url).split("=")).reverse())[0];
		// 	} else {
		// 		videoId = (((url).split("/")).reverse())[0];
		// 	}		
		// 	str += `<div class="col-md-6 video<?= $kunik?>">`;
		// 		<?php if(Authorisation::isInterfaceAdmin()){ ?>
		// 			str +=`<div class = "edit<?= $kunik?>">
		// 						<a href="#page.type.poi.id.`+params.id+`" class="btn material-button lbh-preview-element"> 
		// 							<i class="fa fa-edit"></i>
		// 						</a>
		// 					</div>`
		// 		<?php }?>
		//         str +=` <iframe class="content-image" src="https://www.youtube.com/embed/`+videoId+`" allowfullscreen>
		//         		</iframe>
	 //       </div>`;

		// }
		mylog.log("videoId",videoId);
		
		return str;
	}
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {

		filterSearch = searchObj.init(paramsFilter);
		filterSearch.search.init(filterSearch,0);
		<?php if(Authorisation::isInterfaceAdmin()){ ?>
		$(".container-filters-menu").append("<a href='javascript:;' class='addVideo<?= $kunik ?> btn '><i class='fa fa-plus padding-right-5'></i><?php echo Yii::t('cms', 'Add a video')?></a>");
	<?php } ?>
		$(".addVideo<?= $kunik ?>").click(function() {
			addVideo();
		 });
		
		
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {   				
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            	"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                  dataHelper.path2Value( tplCtx, function(params) {
		                    dyFObj.commonAfterSave(params,function(){
		                      toastr.success("Élément bien ajouté");
		                      $("#ajax-modal").modal('hide');
							  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                    //   urlCtrl.loadByHash(location.hash);
		                    });
		                  } );
					}

				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	});
</script>