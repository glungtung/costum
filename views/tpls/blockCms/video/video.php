<?php 
$keyTpl = "video";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
    "blocvtitle" => "Video",
    "blocvdescription" => "Loren upsum dolor",
    "videobgBoxColor" => "#eeeeee",
    "numberVideo"=> "6",
    "videoNumberInColumn" => 2,
    "videoHeight" => "310px",
    "videoWidth" => "100%",
    "textName" => "true",
    "textShortDesc" => "true",
    "textDesc" =>"true",
    "useFilter"=> "true"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
	.box-video {
		background: <?php echo $paramsData["videobgBoxColor"]; ?>;
		padding: 30px;
		margin: 0 0 24px 0;
	}
	@media (max-width: 767px) {
		.box-video {
			padding: 10px;
			margin: 0 0 15px 0;
		}
	}
	#videolist<?= $kunik ?> .boxHeadline{
		display: <?= $paramsData["textName"] == "true" ? "" : "none"; ?>;
	}
	#videolist<?= $kunik ?> .boxHeadlineSub{
		display: <?= $paramsData["textShortDesc"] == "true" ? "" : "none"; ?>;
	}
	#videolist<?= $kunik ?> .more{
		display: <?= $paramsData["textDesc"] == "true" ? "" : "none"; ?>;
	}




	/* width */
	#videolist<?= $kunik ?>::-webkit-scrollbar {
		width: 2px;
	}

	/* Track */
	#videolist<?= $kunik ?>::-webkit-scrollbar-track {
		box-shadow: inset 0 0 5px grey; 
		border-radius: 10px;
	}

	/* Handle */
	#videolist<?= $kunik ?>::-webkit-scrollbar-thumb {
		background: #f0ad16; 
		border-radius: 10px;
	}

	/* Handle on hover */
	#videolist<?= $kunik ?>::-webkit-scrollbar-thumb:hover {
		background: #b30000; 
	}
	#videolist<?= $kunik ?> {
		max-height: 760px;
		overflow-y: auto;
	}
	.morelink {
		font-size: 16px;
		font-weight: bold;
		margin-left: 2px;
		color: #ff9800;
		cursor: pointer;
	}

	.rte .boxHeadline {
		font-weight: 400;
		margin: 0 0 15px 0;
	}
	.rte .boxHeadline+.boxHeadlineSub {
		font-weight: 400;
		margin: 0px 0 20px 0;
	}
	.section-title { margin-bottom: 40px; }
	.morecontent {
		display: none;
	}
	.morecontent {
		display: none;
	}

	#videolist<?= $kunik ?> iframe{
		width: <?= $paramsData["videoWidth"]?>;
		height:  <?= $paramsData["videoHeight"]?>;
	}



	#videolist?= $kunik?> .searchBar-filters .search-bar{
		margin-top: 5px;
		border-radius: 20px 0px 0px 20px !important;
		box-shadow: none;
		width: 300px;
		max-width: 700px;
		border-color: #f5833c;
	}
	#videolist?= $kunik?> .searchBar-filters .main-search-bar-addon {
		width: 15%;
		background-color: #f5833c!important;
		border: 1px solid #f5833c;
		font-size: 14px;
		padding: 9px 7px;
	}
	#videolist?= $kunik?> #filterVideo<?= $kunik?> li {
		list-style: none;
	}
	#videolist?= $kunik?> #filterVideo<?= $kunik?> .dropdown .btn-menu  {
		padding: 10px 15px;
		font-size: 16px;
		border: 1px solid #f5853c;
		color: #f5853c;
		top: 0;
		position: relative;
		text-align: center;
		border-radius: 20px;
		text-decoration: none;
		background: #fff;
		line-height: 20px;
		display: inline-block;
		margin-left: 5px;
	}
	#videolist?= $kunik?> #filterVideo<?= $kunik?> .dropdown   {
		float: left;
		display: grid;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 10px;
		padding-top: 0px;
		padding-bottom: 0px;
	}
	#videolist?= $kunik?> #filterVideo<?= $kunik?> .dropdown .dropdown-menu  {
		border: 1px solid #f5833c;
		padding: 0px;
		position: absolute;
		overflow-y: visible!important;
		top: 50px;
		left: 5px;
		width: 250px;
		border-radius: 2px;
	}
	#videolist?= $kunik?> #filterVideo<?= $kunik?> .dropdown .dropdown-menu:before  {

		left: 27px;
	}
	#videolist?= $kunik?> .searchBar-filters .main-search-bar-addon i{
		font-size: 20px
	}
	#videolist?= $kunik?> .btn-group .btnHeader {
		font-size: 25px;
		width: 100%;
		height: 50px;
		border-radius: 20px;
		margin-top: 4px;
		background: #f5833c;
		color: white;
	}

	#videolist?= $kunik?> .searchBar-filters .main-search-bar-addon {
		width: 15%;
		height: 50px;
		background-color: #f5833c!important;
		font-size: 14px;
		border: 1px solid #f5833c;
		padding: 12px 7px;
	}
	#videolist?= $kunik?> .searchBar-filters .main-search-bar-addon i{
		font-size: 25px
	}
	#videolist?= $kunik?> .searchBar-filters .nameActeur{
		height: 50px;
		margin-top: 5px;
		border-radius: 20px 0px 0px 20px;
		box-shadow: none;  
		width: 400px;
		max-width: 600px;
		border-color: #f5833c;
	}
	#videolist?= $kunik?> .searchBar-filters .motClef{
		height: 50px;
		margin-top: 5px;
		border-radius: 20px 20px 20px 20px;
		box-shadow: none;  
		width: 300px;
		max-width: 450px;
		border-color: #f5833c;
	}
	#videolist?= $kunik?> .btn-group .dropdown-item {
		width: 100%;
		text-align: left;
		font-size: 18px;

	}
	#videolist?= $kunik?> #activeFilters .filters-activate{
		background-color: #327753;
	}

	@media (max-width: 414px){
		#videolist?= $kunik?>{
			margin-top: 0;
		}
		#videolist?= $kunik?> .searchBar-filters {
			display: none;
		}
		#videolist?= $kunik?>  .dropdown{
			width: 100% !important;
		}
		#videolist?= $kunik?> .searchBarInMenu #main-search-bar{

			border: 1px solid #f5833c !important;
		}
		#videolist?= $kunik?> .searchBarInMenu #main-search-bar-addon {
			padding: 10px 10px;
			font-size: 14px;
			height: 40px;
			background-color: #f5833c !important;
			border: 1px solid #f5833c !important;
		}
		#videolist<?= $kunik?> .searchBarInMenu #main-search-bar-addon i{
			font-size: 20px;
		}
		#videolist<?= $kunik?> #show-filters-xs{
			border: 1px solid #f5833c !important;
			background-color: #e2e2e2;
		}
		#videolist<?= $kunik?> #filterVideo<?= $kunik?> .dropdown .btn-menu  {
			border-radius: 0;
		}
	}
</style>
<div class="col-xs-12">
	<div class="section-title">
        <h2 class="title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="blocvtitle"><?php echo $paramsData["blocvtitle"]; ?></h2>
        <div class="title-2 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="blocvdescription"><?php echo $paramsData["blocvdescription"]; ?></div>
    </div>
    
    <div class="text-center editSectionBtns">
        <div class="" style="width: 100%; display: inline-table; padding: 10px;">
            <?php if(Authorisation::isInterfaceAdmin()){?>
                <div class="text-center addElement<?= $blockCms['_id'] ?>">
                    <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add a video')?></button>
                </div>  
            <?php } ?>
        </div>
    </div>

	<div id="filterVideo<?= $kunik?>"></div>
    <div class="col-xs-12" id="videolist<?= $kunik ?>"></div>


</div>
<script type="text/javascript">
	function addVideo(){
		var dyfPoi={
			"beforeBuild":{
				"properties" : {
					"name": {
						"label" : "<?php echo Yii::t('cms', 'Name of the video')?>",
						"placeholder" : "<?php echo Yii::t('cms', 'Name of the video')?>",
						"order" : 1

					},
					"shortDescription" : {
						"label" : "<?php echo Yii::t('cms', 'Short description')?>",
						"placeholder" : "<?php echo Yii::t('cms', 'Short description')?>",
						"order" : 3
					},
					"tags" :{
						"label" : "<?php echo Yii::t('cms', 'key word')?>",
					} ,
					"urls": {
						"label" : "<?php echo Yii::t('cms', 'Url of the video')?>",
						"order" : 2,
						"rules":{
							"required":true
						}
					}

				}
			},
			"onload" : {
				"actions" : {
					"setTitle" : "<?php echo Yii::t('cms', 'Edit video')?>",
					"src" : { 
						"infocustom" : "<?php echo Yii::t('cms', 'Fill in the field')?>"
					},
					"presetValue" : {
						"type" : "video"
					},
					"hide" : {
						"breadcrumbcustom" : 1,
						"imageuploader" : 1, 
						"urlsarrayBtn" : 1,
						"parentfinder" : 1,
						"removePropLineBtn" : 1,
						"tagstags" :1,
						"formLocalityformLocality" :1,
						"locationlocation" :1
					}
				}
			}

		}
		dyfPoi.afterSave = function(data){
			dyFObj.commonAfterSave(data, function(){
				mylog.log("dataaaa", data);
				urlCtrl.loadByHash(location.hash);

			});
		}
		dyFObj.openForm('poi',null, null,null,dyfPoi);
	}
	
	
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	function videoLink<?= $kunik?> (params){
		var params = {
			"type" : "video",        
			"searchType" : ["poi"],
			fields : ["urls","parent"],
			"indexStep" : "<?= $paramsData["numberVideo"]?>"
		};
		var strVideo = '';
		ajaxPost(
			null,
			baseUrl+"/" + moduleId + "/search/globalautocomplete",
			params,
			function(data){
				mylog.log("videotest",data.results);   
				var params = data.results;         
				var str = '';
				$.each( data.results, function( key, params ) {
					var url = params.urls[0];
					var videoId = "";
					var src = "";
					
					if (url.indexOf("=") != -1) {
						videoId = (((url).split("=")).reverse())[0];
					} else {
						videoId = (((url).split("/")).reverse())[0];
					}

					if (url.indexOf("vimeo") != -1) {
						src = "https://player.vimeo.com/video/"+videoId
					}
					if (url.indexOf("youtu") != -1) {
						src = "https://www.youtube.com/embed/"+videoId
					}
					if (url.indexOf("dailymotion") != -1) {
						src = "https://www.dailymotion.com/embed/video/"+videoId
					}
					if (url.indexOf("peertube.communecter") != -1) {
						src = "https://peertube.communecter.org/videos/embed/"+videoId
					}

					


					var vidName = (typeof params.name != 'undefined') ? params.name : "";
					var vidShortDescription = (typeof params.shortDescription != 'undefined') ? params.shortDescription : "";
					var vidDescription = (typeof params.description != 'undefined') ? params.description : "";
					<?php if ($paramsData["videoNumberInColumn"] == "1") {?>

						str += '<div class="box-video rte col-xs-12">'+
						'<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">'+
						'<iframe width="100%" height="281" src="'+src+'" frameborder="0" allowfullscreen=""></iframe>'+
						'</div>'+

						'<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">'+
						'<h2 class="boxHeadline title-3" style="font-weight:700;">'+vidName+'</h2>'+
						'<h3 class="boxHeadlineSub title-4">'+vidShortDescription+'</h3>'+
						'<div class="title-5 more markdown">'+vidDescription+'</div>'+
						'</div>';
						<?php if($costum["editMode"] == "true"){ ?>
							str +='<div class = "hiddenPreview col-xs-12 col-sm-6 col-md-7 col-lg-8 text-center edit<?= $kunik?>">'+
							'<a href="javascript:;" data-value=\''+JSON.stringify(params)+'\' data-id="'+params._id.$id+'" class="btn btn-primary btn-sm btn-edit-vid margin-5"> <i class="fa fa-edit"></i>'+
							'</a>'+
							'<a href="javascript:;" data-id="'+params._id.$id+'"  class="btn btn-danger btn-sm btn-delete-vid margin-5"> <i class="fa fa-trash"></i>'+
							'</a>'+
							'</div>';
						<?php }?>

						str += '</div>'
					<?php } else { ?>
						var col = 12 / parseInt("<?= $paramsData["videoNumberInColumn"]?>");
						str += '<div class="box-video rte col-xs-12 col-sm-'+col+'">'+
						'<iframe  src="'+src+'" frameborder="0" allowfullscreen=""></iframe>'+
						'<h2 class="boxHeadline title-3" style="font-weight:700;">'+vidName+'</h2>'+
						'<h3 class="boxHeadlineSub title-4">'+vidShortDescription+'</h3>'+
						'<div class="title-5 more markdown">'+vidDescription+'</div>';
						<?php if($costum["editMode"] == "true"){ ?>
							str +='<div class = " hiddenPreview col-xs-12 col-sm-6 col-md-7 col-lg-8 text-center edit<?= $kunik?>">'+
							'<a href="javascript:;" data-value=\''+JSON.stringify(params)+'\' data-id="'+params._id.$id+'" class="btn btn-primary btn-sm btn-edit-vid margin-5"> <i class="fa fa-edit"></i>'+
							'</a>'+
							'<a href="javascript:;" data-id="'+params._id.$id+'"  class="btn btn-danger btn-sm btn-delete-vid margin-5"> <i class="fa fa-trash"></i>'+
							'</a>'+
							'</div>';
						<?php } ?>
						str += '</div>';
					<?php } ?>
				});


				$("#videolist<?= $kunik ?>").html(str);
				$.each($(".markdown"), function(k,v){
					descHtml = dataHelper.markdownToHtml($(v).html());
					$(v).html(descHtml);
				});
				<?php if($paramsData["textDesc"] == "true"){ ?>
					AddReadMoreLink();
				<?php } ?>

				$('.btn-delete-vid').on('click',function(){
					var idPoi = $(this).data('id');
					bootbox.confirm(trad.areyousuretodelete, function(result){ 
						if(result){
							var url = baseUrl+"/"+moduleId+"/element/delete/id/"+idPoi+"/type/poi";
							var param = new Object;
							ajaxPost(null,url,param,function(data){ 
								if(data.result){
									toastr.success(data.msg);
									urlCtrl.loadByHash(location.hash);
								}else{
									toastr.error(data.msg); 
								}
							})
						}
					}) 
				});

				$('.btn-edit-vid').on('click',function(){
					var params = $(this).data("value");
					var id = $(this).data("id");
					var dyfPoi={
						"beforeBuild":{
							"properties" : {
								"name": {
									"label" : "<?php echo Yii::t('cms', 'Name of the video')?>",
              						"placeholder" : "<?php echo Yii::t('cms', 'Name of the video')?>",
									"order" : 1

								},
								"shortDescription" : {
									"label" : "<?php echo Yii::t('cms', 'Short description')?>",
									"placeholder" : "<?php echo Yii::t('cms', 'Short description')?>",
									"order" : 3
								},
								"tags" :{
									"label" : "<?php echo Yii::t('cms', 'key word')?>",
								} ,
								"urls": {
									"label" : "<?php echo Yii::t('cms', 'Url of the video')?>",
									"order" : 2,
									"rules":{
										"required":true
									}
								}

							}
						},
						"onload" : {
							"actions" : {
								"setTitle" : "<?php echo Yii::t('cms', 'Edit video')?>",
								"src" : { 
									"infocustom" : "<?php echo Yii::t('cms', 'Fill in the field')?>"
								},
								"presetValue" : {
									"type" : "video"
								},
								"hide" : {
									"breadcrumbcustom" : 1,
									"imageuploader" : 1,
									"urlsarrayBtn" : 1,
									"parentfinder" : 1,
									"removePropLineBtn" : 1,
									"formLocalityformLocality" :1,
									"locationlocation" :1
								}
							}
						}

					}
					dyfPoi.afterSave = function(data){
						dyFObj.commonAfterSave(data, function(){
							mylog.log("dataaaa", data);
							urlCtrl.loadByHash(location.hash);

						});
					}
					dyFObj.editElement('poi',id,'video',dyfPoi);
				});
			}
		)
	};

    jQuery(document).ready(function() {
    	videoLink<?= $kunik?>();
		$(".addElement<?= $blockCms['_id'] ?>").on('click', function () {
			addVideo();
		});

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : { 
                    "blocvtitle" : {
                        label : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.blocvtitle
                    },    
                    "blocvdescription" : {
                        label : "<?php echo Yii::t('cms', 'Description')?>",
                        "inputType" : "textarea",
            			"markdown" : true,
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.blocvdescription
                    },            
                    videobgBoxColor : {
                        label : "<?php echo Yii::t('cms', 'Background color of the box')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.videobgBoxColor
                    },
                    videoNumberInColumn : {
                        label : "<?php echo Yii::t('cms', 'Number of videos per column')?>",
                        "inputType" : "select",
						options : {
							"1": "1",
							"2"   :"2",
							"3": "3"

						},
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.videoNumberInColumn
                    },
                    "numberVideo":{
                    	"label":"<?php echo Yii::t('cms', 'Number of videos to display')?>",
                    	 values :  sectionDyf.<?php echo $kunik ?>ParamsData.numberVideo
                    },
                    "videoWidth" : {
                    	"inputType" : "text",
                    	"label" : "<?php echo Yii::t('cms', 'Video width (% or px)')?>",
                    	 values :  sectionDyf.<?php echo $kunik ?>ParamsData.videoWidth
                    },
                    "videoHeight" : {
                    	"inputType" : "text",
                    	"label" : "<?php echo Yii::t('cms', 'Video height')?>", 
                    	values :  sectionDyf.<?php echo $kunik ?>ParamsData.videoHeight
                    },
                    "textName":{
                    	label : "<?php echo Yii::t('cms', 'Display video name')?>",
						"inputType" : "checkboxSimple",
						"params" : checkboxSimpleParams,
						"checked" : <?= json_encode($paramsData["textName"]) ?>
                    },
                    "textShortDesc":{
                    	label : "<?php echo Yii::t('cms', 'Displays the short description of the video')?>",
						"inputType" : "checkboxSimple",
						"params" : checkboxSimpleParams,
						"checked" : <?= json_encode($paramsData["textShortDesc"]) ?>
                    },
                    "textDesc":{
                    	label : "<?php echo Yii::t('cms', 'Display the long description of the video')?>",
						"inputType" : "checkboxSimple",
						"params" : checkboxSimpleParams,
						"checked" : <?= json_encode($paramsData["textDesc"]) ?>
                    }

                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("<?php echo Yii::t('cms', 'updated item')?>");
                                $("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            } );
                        }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        	tplCtx.id = $(this).data("id");
        	tplCtx.collection = $(this).data("collection");
        	tplCtx.path = "allToRoot";

        	dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        	alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"video",4,6,null,null,"<?php echo Yii::t('cms', 'Video')?>","#e45858","");
        	alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"text",4,6,null,null,"<?php echo Yii::t('cms', 'Video description')?>","#e45858","");
        	alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"blocv",12,12,null,null,"<?php echo Yii::t('cms', 'Text')?>","#e45858","");
        });

     });
	function AddReadMoreLink() {
		var showChar = 160;
		var ellipsestext = "...";
        var moretext = "<?php echo Yii::t('cms', 'Read more')?>";
        var lesstext = "<?php echo Yii::t('cms', 'Read less')?>";
		$('.more').each(function() {
			var content = $(this).html();
			var textcontent = $(this).text();
			if (textcontent.length > showChar) {

				var c = textcontent.substr(0, showChar);
	            //var h = content.substr(showChar-1, content.length - showChar);
	            var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

	            $(this).html(html);
	            $(this).after('<a href="" class="morelink btn btn-default">' + moretext + '</a>');
	        }
	    });
		$(".morelink").click(function() {
			if ($(this).hasClass("less")) {
				$(this).removeClass("less");
				$(this).html(moretext);
				$(this).prev().children('.morecontent').fadeToggle(500, function(){
					$(this).prev().fadeToggle(500);
				});
			} else {
				$(this).addClass("less");
				$(this).html(lesstext);
				$(this).prev().children('.container').fadeToggle(500, function(){
					$(this).next().fadeToggle(500);
				});
			}
			return false;
		});
	}

</script>