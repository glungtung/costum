<?php
$default = "black";
$structField = "structags";

$keyTpl = "simpleTextAndImg";

$paramsData = [
                "title" =>  "Simple text avec image",
                "color" => "",
                "defaultcolor" => "#354C57",
                "tag" => "structags"
                ];


if (isset($blockCms)) {
    $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
    foreach ($paramsData as $e => $v) {
        if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
            $paramsData[$e] = $tplsCms[$e];
        }
    }
}

?>
<div id="simpleTextAndImg" class="simpleTextAndImg row">
    <?php
    $result = null;
    if(count ( Cms::getCmsByStruct($cmsList,@$tag,$structField ) ) != 0){
      $result = Cms::getCmsByStruct($cmsList,@$tag,$structField)[0];
            $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $result["_id"]));
        ?>

        <?php 
                if(isset($img)){
        ?>
                <div class="simpleTextAndImg-img col-md-12">
                        <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
                </div>
            <?php } ?>

        <div class="simpleTextAndImg-txt col-md-12 text-center">
          <h1 style="color: <?= @$eltColor; ?>" class="title"><?php echo @$result["name"]; ?></h1>
          <span style="color: <?= @$eltColor; ?>" class="markdown"><?= @$result["description"]; ?></span>
        </div>

          <?php
                $edit = "update";
      }
        else { ?>
            <div class="col-xs-12 text-center">
               Ajouter une bannière, et un texte explicatif
                <?php 
                    $edit = "create"; 
            echo"</div>";
        }
        echo "<center>";
            echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                    array(
                                        'edit' => $edit,
                                        'tag' => @$tag,
                                        'id' => (isset($result["_id"])) ? (string)$result["_id"] : null
                                     ),true);
            echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
        echo "</center>";
?>
<script type="text/javascript">
tplCtx = {};
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";
page = <?= json_encode(@$page); ?>;
type = 'tpls.blockCms.textImg.<?= $keyTpl ?>';

sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function(){
    mylog.log("---------- Render blockimg","costum.views.tpls.simpleTextAndImg");
    
        sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section bloc un texte et une image",
            "description" : "Personnaliser votre section sur les blocs d'un texte et une image",
            "icon" : "fa-cog",
            "properties" : {
                title : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                color : {
                    label : "Couleur du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color

                },
                background : {
                    label : "Couleur du fond du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                txtcolor : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.txtcolor

                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});

  $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dataObj.structags = $(this).data("tag");

        dyFObj.openForm('cms',null,dataObj,null);
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                          urlCtrl.loadByHash(location.hash);
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });

    
</script>
</div>