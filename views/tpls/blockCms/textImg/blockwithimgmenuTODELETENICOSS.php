<?php
$default = "black";
$structField = "structags";

$keyTpl = "blockwithimenu";

$paramsData = [
    "title" =>  "",
    "color" => "",
    "defaultcolor" => "#354C57",
    "tags" => "structags"
];

if( isset($costum["tpls"][$kunik]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset($costum["tpls"][$kunik][$i]) ) 
            $paramsData[$i] =  $costum["tpls"][$kunik][$i];      
    }
}

?>
<style type="text/css">
    @font-face{
        font-family: "DINAlternate-Bold";
        src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DINAlternate-Bold.ttf")
    }

    #blockwithimg{
        margin-top: 7vw;
    }

    .name{
        cursor: pointer;
    }

    #blockwithimg .blockwithimg-img .name{
        position: absolute;
        top: 2vw;
        left:12vw;
        font-size: 1.2vw;
    }

    #blockwithimg .blockwithimg-img .type-sub-events{
        position: absolute;
        top: 5vw;
        left:12vw;
        text-align: left;
        display: grid;
        max-height: 186px;
        overflow: auto;
    }

    .blockwithimg-txt .markdown h2{
        color: #EE302C;
    }

    .blockwithimg-txt .name-type{
        color: #EE302C;
    }

    #blockwithimg .blockwithimg-img .imgCms{
        width:15vw;
        margin-left: 9vw;
        margin-top: 1vw;
        position: absolute;
    }

    .alternateBold{
        font-family: 'DINAlternate-Bold';
    }
</style>

    <?php
    $result = null;
    $data = "";
    $i = 0;
    if(count ( Cms::getCmsByStruct($cmsList,"blockimg0",$structField ) ) != 0){
     $result = Cms::getCmsByStruct($cmsList,"blockimg0",$structField)[0];
     $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $result["_id"]));?>
     <div class="blockwithimg-img col-md-6">
        <img class="img-responsive imgCms" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/block_bg_apropos.png">
        <h5 class="name text-white alternateBold"><?php echo $result["name"]; ?></h5>
        <div class="type-sub-events col-md-3">
            <?php 
            $elementType = array();
            foreach ($subevent as $key => $value) {
                if (! in_array($value["type"], $elementType)) {
                    $elementType[] = $value["type"];
                    $eventType = array();
                    foreach ($subevent as $kType => $vType) {
                        if (! in_array($vType["type"], $eventType)) {
                            $eventType[] = $vType["type"];
                        }
                    }
                    foreach ($elementType as $kType => $vType) {
                        echo '<a style="font-size: 2vw;cursor: pointer;" data-id="'.$vType.'" class="text-white dataType dinalternate">'.$vType.'</a>';
                    }                    
                    foreach ($eventType as $key => $value) {
                        echo '<a style="font-size: 2vw;cursor: pointer;" data-id="'.$value.'" class="text-white dataType dinalternate">'.$value.'</a>';
                        $i++;
                    } 

                }?>


            </div>
            <?php if(isset($img)){ ?> 
                <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
            <?php }else{ ?>
                <img style="width:9vw;margin-top: 3vw;left:2vw;position: relative;" class="img-responsive" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/points_block_membre.png">
                <img style="width:17vw;margin-left: 21vw;margin-top: -18vw;" class="img-responsive" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/img4.jpg">
                <img style="width: 24vw;margin-left: 11vw;margin-top: -1vw;" class="img-responsive" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/img3.png">
            <?php } ?>
        </div>
        <div class="blockwithimg-txt col-md-5 no-padding">
            <span style="text-align: left;" class="markdown markdown-type-desc col-md-12"><?= @$result["description"]; ?></span>
            <?php foreach ($subevent as $key => $value) {
                if ($data == "" || $data != $value["type"]) {
                    $data = $value["type"];
                    if (count ( Cms::getCmsByStruct($cmsList,$data,$structField ) ) != 0) {
                        $dataRes = Cms::getCmsByStruct($cmsList,$data,$structField)[0]; ?>
                        <h2 style="text-align: left;display:none;"class="name-type col-md-12 name<?= $data ?>"><?= @$dataRes["name"] ?></h2>
                        <span style="text-align: left;display:none;" class="markdown markdown-type col-md-12 md<?= $data ?>"><?= @$dataRes["description"]; ?></span>
                        =======
                        <?php foreach ($eventType as $key => $value) {
                            if (count ( Poi::getPoiByStruct($poiList,$value,$structField ) ) != 0) {
                                $dataRes = Poi::getPoiByStruct($poiList,$value,$structField)[0]; ?>
                                <h2 style="text-align: left;display:none;"class="name-type col-md-12 name<?= $value ?>"><?= @$dataRes["name"] ?></h2>
                                <span style="text-align: left;display:none;" class="markdown markdown-type col-md-12 md<?= $value ?>"><?= @$dataRes["description"]; ?></span>
                                >>>>>>> fc257cf60d0eae7462828b9a8306086f1eb94b2c:views/tpls/blockwithimgmenu.php

                                <?php
                                $edit = "update";
                            }else{
                                ?>
                                <center>
                                    <div style="display: none;" class="createType create-type-<?= $value ?> col-xs-12 text-center">
                                        Ajouter un titre et une description au type <?= $value ?>
                                        <?php 
                                        $edit = "create"; 
                                        ?>
                                    </div>
                                </center>
                                <?php
                            }
                            ?>
                            <center>
                                <div style="display:none;" class="col-md-12 type <?= $value ?> button-type button<?= $value ?>">
                                    <?php
                                    echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                        array(
                                            'edit' => $edit,
                                            'tag' => $value,
                                            'id' => (string)@$dataRes["_id"]),true); 

                                    echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
                                    ?>
                                </div>
                            </center>
                            <a data-key="<?= $value ?>" style="display: none;color:white;background: #EE302C;padding: 11px;border-radius: 23px;color:white;margin-top:2vw;" class="dinalternate pull-left accessFilter filters<?= $value ?>">Voir les <?= $value ?>
                        </a>
                        <?php
                        $i++;
                    } ?>
                    <?php
                    $edit = "update";
                }else{
                    ?>
                    <div class="col-xs-12 text-center">
                        Ajouter un titre et une description pour qui somme nous
                        <?php 
                        $edit = "create";
                        // $dataNew = "blockimg0"; 
                        ?>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-6 editTitleDesc">
                    <?php
                    echo $this->renderPartial("costum.views.tpls.openFormBtn",
                        array(
                            'edit' => $edit,
                            'tag' => "blockimg0",
                            'id' => (string)@$result["_id"]),true); ?>
                    <?php
                    echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS");                ?>
                </div>
            </div>



        <script type="text/javascript">
            cmsList = <?php echo json_encode($cmsList); ?>;
            subEvent = <?php echo json_encode($subevent); ?>;

            $(".accessFilter").click(function(){
                filterType = $(this).attr("data-key");
                $(".type").css("display","none");
                $(".description").css("display","none");
                $(".button-type-prg").css("display","none");
                $(".generate-pdf").css("display","none");
                $("."+filterType).css("display","initial");
                $(".generate-pdf").css("display","none");
                $(".pdf-"+filterType).css("display","initial");
                if ( $("."+filterType).parent(".item") ) {
                    $("."+filterType).parent(".item").removeClass().addClass("item active");
                }
            });

            sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
            jQuery(document).ready(function(){
              mylog.log("---------- Render blockimg","costum.views.tpls.blockwithimg");
              $(".name").click(function(){
                $('.button-type').css("display","none");
                $('.markdown-type').css("display","none");
                $(".markdown-type-desc").css("display","initial");
                $('.createType').css("display","none");
                $('.name-type').css("display","none");
                $('.accessFilter').css("display","none");
                $(".editTitleDesc").css("display","initial");
            });

              $(".dataType").click(function(){
                $('.button-type').css("display","none");
                $('.markdown-type').css("display","none");
                $(".markdown-type-desc").css("display","none");
                $(".editTitleDesc").css("display","none");
                $(".createType").css("display","none");
                $('.name-type').css("display","none");
                $('.accessFilter').css("display","none");

                data = $(this).attr('data-id');

                $(".md"+data).css('display','initial');
                $(".button"+data).css('display','initial');
                $('.create-type-'+data).css("display","initial");
                $('.name'+data).css("display","initial");
                $(".filters"+data).css("display","initial");
            });

              sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {    
                    "title" : "Configurer la section bloc un texte et une image",
                    "description" : "Personnaliser votre section sur les blocs d'un texte et une image",
                    "icon" : "fa-cog",
                    "properties" : {
                        "title" : {
                            label : "Titre",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                        },
                        "color" : {
                            label : "Couleur du texte",
                            inputType : "colorpicker",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.color

                        },
                        defaultcolor : {
                            label : "Couleur",
                            inputType : "colorpicker",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.defaultcolor
                        },
                        tags : {
                            inputType : "tags",
                            label : "Tags",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.tags
                        }
                    },
                    save : function () {  
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                            tplCtx.value[k] = $("#"+k).val();
                        });
                        console.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                //urlCtrl.loadByHash(location.hash);
                            } );
                        }

                    }
                }
            };

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path");
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });

        });

$(".editThisBtn").off().on("click",function (){
    mylog.log("editThisBtn");
    var id = $(this).data("id");
    var type = $(this).data("type");
    dyFObj.editElement(type,id,null,dynFormCostumCMS)
});
$(".createBlockBtn").off().on("click",function (){
    mylog.log("createBtn");
    dyFObj.openForm('cms',null,{structags:$(this).data("tag") },null,dynFormCostumCMS)
});

$(".deleteThisBtn").off().on("click",function (){
    mylog.log("deleteThisBtn click");
    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    var btnClick = $(this);
    var id = $(this).data("id");
    var type = $(this).data("type");
    var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;

    bootbox.confirm(trad.areyousuretodelete,
        function(result) 
        {
            if (!result) {
              btnClick.empty().html('<i class="fa fa-trash"></i>');
              return;
          } else {
              $.ajax({
                type: "POST",
                url: urlToSend,
                dataType : "json"
            })
              .done(function (data) {
                if ( data && data.result ) {
                  toastr.info("élément effacé");
                  $("#"+type+id).remove();
              } else {
                 toastr.error("something went wrong!! please try again.");
             }
         });
          }
      });

});


</script>