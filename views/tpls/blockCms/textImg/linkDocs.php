<?php 
$keyTpl = "linkDocs";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$page = isset($page)?$page:"";
$paramsData = [
    "title" => "TUTORIELS",
    "btnColor" => "#FFFFFF",
    "btnBgColor" => "#000091",
    "btnBorderColor"   => "#000091"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">  
    section.file<?php echo $kunik ?>  {
        padding: 25px 0;
    }
    .file<?php echo $kunik ?>  {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }
    .file<?php echo $kunik ?>   {
        min-height: 85px;
    }

    .file<?php echo $kunik ?>  .card_<?=$kunik?> .card-title {
        font-weight: bold;

    }
    .file<?php echo $kunik ?>  img {
        height: 200px;
        object-fit: cover;
        object-position: center;
    }
    .file<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none
    }
    .file<?=$kunik?> .icon-<?=$kunik?> li a{
        display:block;width:35px;
        height:35px;
        line-height:35px;
        border-radius:50%;
        color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?> ;
        font-size:18px;
        background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
        border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["btnBorderColor"]; ?>;
        margin-right:10px;transition:all .5s ease 0s

    }
    .file<?=$kunik?> .icon-<?=$kunik?> li a:hover{
        transform:rotate(360deg)
    }
    .file<?=$kunik?>{
        box-shadow:0 0 3px rgba(0,0,0,.3)
    }
    .file<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none;
        margin:0;
        text-align:center;
    }
    
    .file<?=$kunik?> .icon-<?=$kunik?> li {
        display: none;
    }

    .file<?=$kunik?>:hover .icon-<?=$kunik?> li{
        display:inline-block;
    }
    @media (max-width: 767px) {
        .file<?php echo $kunik ?>  img {
            max-width: 300px;
        }
    }
    .addDoc<?= $blockCms['_id'] ?> button{
      margin-left: 12px;
      font-size: 16px;
      line-height: 24px;
      min-height: 40px;
      padding: 8px 24px;
      background-color: #000091!important;
      color: #fff!important;
      border-radius: 2px;
    }


</style>
<section class="file<?php echo $kunik ?>" class="pb-5">
    <div class=" editSectionBtns<?php echo $kunik ?>">
        <div class="" style="width: 100%; display: inline-table; padding: 10px;">
            <?php if(Authorisation::isInterfaceAdmin()){?>
                <div class=" addDoc<?= $blockCms['_id'] ?>">
                    <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo Yii::t('cms', 'Add a resource block')?></button>
                </div>  
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <h5 class="section-title text-center title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></h5>
        <div class="card_<?=$kunik?>">
            
        </div>
    </div>
</section>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var dyfPoiDoc={
        "beforeBuild":{
            "properties" : {
                "name": {
                    "label" : "<?php echo Yii::t('cms', 'File title')?>",
                    "placeholder" : "<?php echo Yii::t('cms', 'File title')?>",
                    "order" : 1,
                    rules :{
                        "required" :true
                    }
                },
                "subtype" : {
                    "label" : "<?php echo Yii::t('cms', 'Name of the page')?>",
                    value : ["<?= $page?>"]
                },
                "category" : {
                    "label" : "<?php echo Yii::t('cms', 'Display type')?>",
                    inputType :"select",
                    "placeholder" : "<?php echo Yii::t('cms', 'Display type')?>",
                    "class":"form-control",
                    options : {
                        "link" : "<?php echo Yii::t('cms', 'External link')?>",
                        "content" : "<?php echo Yii::t('cms', 'Preview')?>",
                        "fullScreen" : "<?php echo Yii::t('cms', 'Full screen')?>",
                    },
                    order : 2,
                    rules :{
                        "required" :true
                    }
                },
                "urls": {
                    "label" : "<?php echo Yii::t('cms', 'File link')?>",
                    "order" : 3
                },
                "description" : {
                    "label" : "<?php echo Yii::t('cms', 'Content')?> ",
                    "order" : 4
                }
        }
        },
        "onload" : {
            "actions" : {
                "setTitle" : "<?php echo Yii::t('cms', 'Add a file')?>",
                "html" : {
                    "infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
                },
                "presetValue" : {
                    "type" : "affiche",
                },
                "hide" : {
                    "breadcrumbcustom" : 1,
                    "formLocalityformLocality": 1,
                    "urlsarrayBtn" : 1,
                    "parentfinder" : 1
                }
            }
        }
    };
    dyfPoiDoc.afterSave = function(data){
        dyFObj.commonAfterSave(data,function(){
            mylog.log("data", data);
            urlCtrl.loadByHash(location.hash);
        });
    };
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : { 
                    "btnColor" : {
                        label : "<?php echo Yii::t('cms', 'Button label color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.btnColor
                    },            
                    "btnBgColor" : {
                        label : "<?php echo Yii::t('cms', 'Button color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                    },
                    "btnBorderColor": {
                        label : "<?php echo Yii::t('cms', 'Button border color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBorderColor
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                          $("#ajax-modal").modal('hide');
                          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                      });
                    } );
                  }
              }
            }
        };

        $(".addDoc<?= $blockCms['_id'] ?>").click(function(){
               dyFObj.openForm('poi',null, null,null,dyfPoiDoc); 
        })
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","green","");
        });
        getDoc<?= $kunik?>();
        function getDoc<?= $kunik?>(){  
            var params = {
                searchType : ["poi"],
                filters : {
                    type : "affiche",
                    $or : {
                        tags : ["<?= $page?>"],
                        subtype : "<?= $page?>"
                    }
                },
                fields : ["urls","parent"],
                sortBy :["name"]

            };
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    var html = "";
                        $.each(data.results, function( index, value ) {
                            category = exists(value.category)?value.category:"";
                            html += 
                                '<div class="col-md-4 col-sm-6 both-box mb-30 text-center" style="height:350px">'+
                                    '<div class="">'+       
                                        '<div class=" ">'+
                                            '<h4 class="card-title title-2">'+value.name+'</h4>'+
                                        '</div>';
                                        <?php if($costum["editMode"] == "true"){?>
                                            html +='<ul class="icon-<?=$kunik?> hiddenPreview">'+
                                                '<li>'+
                                                    '<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value._id.$id+'" ><i class="fa fa-edit"></i></a>'+
                                                '</li>'+
                                                '<li>'+
                                                    '<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value._id.$id+'" ><i class="fa fa-trash"></i></a>'+
                                                '</li>'+
                                            '</ul>';
                                        <?php } ?>
                                        if ((value.category  == "link" && typeof value.urls!= "undefined" && typeof value.urls[0]!= "undefined")) 
                                            html +=  
                                                '<a href="'+value.urls[0]+'" target="_blank">';
                                        else 
                                            html +=         
                                            '<a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element '+category+' ">';
                                        if (typeof value.profilMediumImageUrl && value.profilMediumImageUrl != null) 
                                            html +=  '<img class="" src="'+value.profilMediumImageUrl+'" alt="card image">';
                                        else
                                            html +=         
                                                '<img class="" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/ctenat/mars2019.png"> ';

                                    html += '</a>';
                            
                                html += '</div>'+
                            '</div>';
                        });
                        $(".card_<?=$kunik?>").html(html);
                        $(".card_<?=$kunik?> .edit").off().on('click',function(){
                            var id = $(this).data("id");
                            var type = $(this).data("type");
                            dyFObj.editElement('poi',id,type,dyfPoiDoc);
                        });
                        $(".card_<?=$kunik?> .delete").off().on("click",function () {
                            $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                            var btnClick = $(this);
                            var id = $(this).data("id");
                            var type = "poi";
                            var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
                            
                            bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
                            function(result) {
                                if (!result) {
                                    btnClick.empty().html('<i class="fa fa-trash"></i>');
                                    return;
                                } else {
                                    ajaxPost(
                                        null,
                                        urlToSend,
                                        null,
                                        function(data){ 
                                            if ( data && data.result ) {
                                            toastr.success("élément effacé");
                                            $("#"+type+id).remove();
                                            getDoc<?= $kunik?>();
                                            } else {
                                            toastr.error("something went wrong!! please try again.");
                                            }
                                        }
                                    );
                                }
                            });
                        });                 
                    coInterface.bindLBHLinks();   
               }
            );
        }
    });
</script>