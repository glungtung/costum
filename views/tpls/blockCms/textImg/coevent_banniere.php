<?php
    $keyTpl = "coevent_banniere";
    $costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
    $event = PHDB::findOneById(Event::COLLECTION, strval($costum["contextId"]));
    
    $paramsData = [
        "text"            => "Slogan",
        "title"           => "Titre",
        "banner"          => Yii::app()->getModule("costum")->getAssetsUrl() . "/images/coevent/no-banner.jpg",
        "logo"            => $event['profilMediumImageUrl'] ?? Yii::app()->getModule("costum")->getAssetsUrl() . "/images/coevent/logo.png",
        "blockCmsBgColor" => "transparent",
        "primaryColor"    => "#EE302C"
    ];
    
    if (isset($event["profilImageUrl"]) && !empty($event["profilImageUrl"])) {
        $paramsData["logo"] = $event["profilImageUrl"];
    }
    
    $init_image = Document::getListDocumentsWhere(
        array(
            "id"   => $blockKey,
            "type" => 'cms',
        ),
        "image"
    );
    
    $banners = [];
    $logos = [];
    
    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e])) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
    
    foreach ($init_image as $value) {
        if ($value["subKey"] == "banner" && !empty($value["subKey"]) && !empty($value["imagePath"])) {
            $banners[] = $value;
            $paramsData["banner"] = $value["imagePath"];
        }
        if ($value["subKey"] == "logo" && !empty($value["subKey"]) && !empty($value["imagePath"])) {
            $logos[] = $value;
            $paramsData["logo"] = $value["imagePath"];
        }
    }
?>
<style>
    .class<?= $kunik ?>header {
        background-image: url(<?= $paramsData["banner"] ?>);
        padding: 10% 20px;
        display: flex;
        flex-direction: row;
        background-size: cover;
        align-items: center;
        justify-content: center;
        gap: 10px;
    }

    .class<?= $kunik ?>header:before {
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, .5);
    }

    .class<?= $kunik ?>header > img {
        width: 150px;
        height: auto;
    }

    @media only screen and (max-width: 768px) {
        .class<?= $kunik ?>header {
            flex-direction: column;
        }
    }
</style>
<div id="accueil" class="container-fluid">
    <div class="class<?= $kunik ?>header">
        <img src="<?= $paramsData["logo"] ?>">
        <div style="display: flex; flex-direction: column; gap: 10px;">
            <div style="color: white; font-size: 31px;" data-id="<?= $blockKey ?>" class="sp-text" data-field="text"><?= $paramsData["text"] ?></div>
            <div style="color: white; font-size: 31px;" data-id="<?= $blockKey ?>" class="sp-text" data-field="title"
                 style="font-size: 31px;"><?= $paramsData["title"] ?></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    sectionDyf.<?= $kunik ?>ParamsData = <?= json_encode($paramsData) ?>;
    sectionDyf.<?= $kunik ?>Params = {
        jsonSchema: {
            title      : `<?= Yii::t('cms', 'Set up your section') ?>`,
            description: `<?= Yii::t('cms', 'Customize your section') ?>`,
            icon       : 'fa-cog',
            properties : {
                banner: {
                    inputType          : 'uploader',
                    label              : tradCms.blockbackgroundcolororimage,
                    showUploadBtn      : false,
                    docType            : 'image',
                    itemLimit          : 1,
                    contentKey         : 'slider',
                    order              : 9,
                    domElement         : 'banner',
                    placeholder        : 'image logo',
                    afterUploadComplete: null,
                    endPoint           : '/subKey/banner',
                    filetypes          : [
                        'png', 'jpg', 'jpeg', 'gif'
                    ],
                    initList           : <?php echo json_encode($banners) ?>
                },
                logo  : {
                    inputType          : 'uploader',
                    label              : 'Logo',
                    showUploadBtn      : false,
                    docType            : 'image',
                    itemLimit          : 1,
                    contentKey         : 'slider',
                    order              : 9,
                    domElement         : 'logo',
                    placeholder        : '',
                    afterUploadComplete: null,
                    endPoint           : '/subKey/logo',
                    filetypes          : [
                        'png', 'jpg', 'jpeg', 'gif'
                    ],
                    initList           : <?php echo json_encode($logos) ?>
                }
            },
            beforeBuild: function () {
                uploadObj.set('cms', '<?= $blockKey ?>');
            },
            save       : function () {
                tplCtx.value = {}
                for (var property in sectionDyf.<?= $kunik ?>Params.jsonSchema.properties) {
                    var value = $(`#${property}`).val();
                    if (value) tplCtx.value[property] = value
                }
                if (tplCtx.value === undefined) toastr.error('value cannot be empty!')
                else {
                    dataHelper.path2Value(tplCtx, function (params) {
                        dyFObj.commonAfterSave(params, function () {
                            toastr.success('<?= Yii::t('cms', 'Element well added') ?>')
                            $('#ajax-modal').modal('hide')
                            urlCtrl.loadByHash(location.hash)
                        })
                    })
                }
            }
        }
    };

    $(`.edit<?= $kunik ?>Params`).on("click", function () {
        tplCtx.id = $(this).data('id');
        tplCtx.collection = $(this).data('collection');
        tplCtx.path = 'allToRoot';
        dyFObj.openForm(sectionDyf.<?= $kunik ?>Params, null, sectionDyf.<?= $kunik ?>ParamsData);
    })
</script>