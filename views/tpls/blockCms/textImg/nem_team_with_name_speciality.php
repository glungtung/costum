
<?php 
$keyTpl = "nem_team_with_name_speciality";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
    "title" => "OUR TEAM",
    "btnColor" => "#ffffff",
    "btnBgColor" => "#108d6f",
    "btnBorderColor"   => "#119c39",
    "imgWidth" => "300",
    "imgHeight" => "300",
    "imgRadius" => "0",
    "imgBorder" => '0',
    "imgBorderColor" => "#0dab76"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>


<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>
<style type="text/css">  
   

    .team<?php echo $kunik ?> .team-desc-cont {
	    display:inline-block;
	    float:none;
	    vertical-align: text-top;
	    text-align:center;
	    margin-right:-4px;
	    padding-bottom: 30px;
    }
    .team<?php echo $kunik ?> .row-centered {
	    text-align:center;
	}


    .team<?php echo $kunik ?> .btn-action-team {
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
	    border: 1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["btnBorderColor"]; ?>;
	    color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?>;
	    text-align: center;
	    border-radius: 20px;
    }



    section.team<?php echo $kunik ?>  {
        padding: 25px 0;
    }

    .team<?php echo $kunik ?> .frontside {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }

    .team<?php echo $kunik ?> .frontside .card-body .card-text {
    	min-height: 85px;
    }

    .team<?php echo $kunik ?> .frontside .card-title {
        text-transform: none;
        font-weight: 500;
    }

    .team<?php echo $kunik ?> .frontside .card-body img {
        width: <?php echo $paramsData["imgWidth"]; ?>px;
        height: <?php echo $paramsData["imgHeight"]; ?>px;
        border-radius: <?php echo $paramsData["imgRadius"]; ?>%;
        border: <?php echo $paramsData["imgBorder"]; ?>px solid;
        border-color: <?php echo $paramsData["imgBorderColor"]; ?>;
    }


    @media (max-width: 767px) {
    	.team<?php echo $kunik ?> .frontside .card-body img {
	        max-width: 300px;
	    }
    }
    </style>
  


<!-- Team -->
<section class="team<?php echo $kunik ?>" class="pb-5">
    <div class="container">
        <h5 class="title-1 section-title text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></h5>
        <div class="row row-centered">
            <!-- Team member -->

         <?php 
        if (isset($content)) {
            foreach ($content as $key => $value) {?>   
            <div class="col-xs-12 col-sm-6 col-md-4 team-desc-cont  col-center-block">
                <div class="frontside">
                    <?php  
                        $initFiles = Document::getListDocumentsWhere(
                            array(
                              "id"=> $blockKey,
                              "type"=>'cms',
                              "subKey"=> (string)$key 
                            ), "image"
                        );
                        $arrFile = [];
                        //var_dump($initFiles);
                        foreach ($initFiles as $k => $v) {
                            $arrFile[] =$v['imagePath'];
                        }
                    ?> 
                    <div class="card-body text-center">
                        
                            <?php if (!empty($arrFile[0])){ ?>
                                <img class=" img-fluid" src=" <?php echo $arrFile[0] ?>" alt="card image">
                            <?php }else { ?>
                                <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"> 
                            <?php } ?>
                        
                        <h4 class="card-title  title-2"><?= $value["nameteam"] ?></h4>
                        <p class="card-text  title-3"><?= $value["speciality"] ?></p>
                        <div class="col-xs-12 text-center mt-4">
                       
	                        <?php if(Authorisation::isInterfaceAdmin()){ ?>
	                            <a href="javascript:;" class="btn btn-primary btn-action-team editElement<?= $blockCms['_id'] ?> editSectionBtns"
	                                    data-key="<?= $key ?>" 
	                                    data-img='<?php echo json_encode($initFiles) ?>' 
	                                    data-nameteam="<?= $value["nameteam"] ?>"
	                                    data-speciality="<?= $value["speciality"] ?>"
	                                    data-descriptionteam="<?= $value["descriptionteam"] ?>"
	                                    data-labelbtn="<?= $value["labelbtn"] ?>"
	                                ><i class="fa fa-pencil"></i></a>
	                            <?php } ?>
	                            <?php if (isset($value["descriptionteam"]) && !empty($value["descriptionteam"])){ ?>
	                            	<a href="<?= $value["descriptionteam"] ?>" class="btn btn-primary btn-action-team" target="_blank"><?= $value["labelbtn"] ?></a>
	                            <?php } ?>
	                            <?php if(Authorisation::isInterfaceAdmin()){ ?>
	                            <a  href="javascript:;" class="btn btn-danger btn-action-team deletePlan<?= $blockKey ?> "
	                                data-key="<?= $key ?>" 
	                                data-id ="<?= $blockKey ?>"
	                                data-path="content.<?= $key ?>"
	                                data-collection = "cms">
	                                <i class="fa fa-trash"></i>
	                            </a>
	                            
	                        <?php } ?>
	                        
	                    </div>
                    </div>

                </div>


            </div>
            <!-- ./Team member -->
            <?php } 
            } ?>

        </div>
        <div class="text-center editSectionBtns">
            <div class="" style="width: 100%; display: inline-table; padding: 10px;">
                <?php if(Authorisation::isInterfaceAdmin()){?>
                    <div class="text-center addElement<?= $blockCms['_id'] ?>">
                        <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo Yii::t('common', 'Add')?></button>
                    </div>  
                <?php } ?>
            </div>
        </div>

    </div>
</section>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : { 
                    "title" : {
                        label : "<?php echo Yii::t('cms', 'Title')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.title
                    },
                    "imgWidth"  : {
                        label : "<?php echo Yii::t('cms', 'Image width (in px)')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgWidth
                    },
                    "imgHeight" : {
                        label : "<?php echo Yii::t('cms', 'Height of the image (in px)')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgHeight
                    },
                    "imgRadius" : {
                        label : "<?php echo Yii::t('cms', 'Radius of the image border (in %)')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgRadius
                    },
                    "imgBorderColor" :{
                        label : "<?php echo Yii::t('cms', 'Color of the image border')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.imgBorderColor
                     },
                     "imgBorder" :{
                        label : "<?php echo Yii::t('cms', 'Thickness of the image border')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.imgBorder
                     },    
                    "btnColor" : {
                        label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.btnColor
                    },            
                    "btnBgColor" : {
                        label : "<?php echo Yii::t('cms', 'Button background color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                    },
                    "btnBorderColor": {
                        label : "<?php echo Yii::t('cms', 'Button border color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBorderColor
                    }
                     
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("élement mis à jour");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            } );
                        }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"img",4,6,1,null,"<?php echo Yii::t('cms', 'Image property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","green","");
        });

        

        $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
            var deleteObj ={};
            deleteObj.id = $(this).data("id");
            deleteObj.path = $(this).data("path");          
            deleteObj.collection = $(this).data("collection");
            deleteObj.value = null;
            bootbox.confirm("Etes-vous sûr de vouloir supprimer cet élément ?",
            function(result){
              if (!result) {
                return;
              }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
              }
            }); 
        });
        
        $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
            //var contentLength = Object.keys(<?php //echo json_encode($content); ?>).length;
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>"
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(key);
            var obj = {
                nameteam :         $(this).data("nameteam"),
                descriptionteam:    $(this).data("descriptionteam"),
                img:            $(this).data("img"),
                speciality:     $(this).data("speciality"),
                labelbtn:     $(this).data("labelbtn")
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add new team')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                        // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        $(".addElement<?= $blockCms['_id'] ?>").click(function() { 
            var keys = Object.keys(<?php echo json_encode($content); ?>);
            var lastContentK = 0; 
            if (keys.length!=0) 
                lastContentK = parseInt((keys[(keys.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(lastContentK+1);
            var obj = {
                nameTeam :         $(this).data("nameTeam"),
                descriptionTeam:    $(this).data("descriptionTeam"),
                speciality:     $(this).data("speciality"),
                labelbtn:     $(this).data("labelbtn")
            };

            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add new team')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,lastContentK+1),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                        // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        
        function getProperties(obj={},subKey){
            var props = {
                img : {
                    "inputType" : "uploader",
                    "label" : "<?php echo Yii::t('cms', 'Image')?>",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/"+subKey,
                    "domElement" : "image",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "label": "Image :",
                    "showUploadBtn": false,
                    initList : obj["img"]
                },
                nameteam : {
                    label : "<?php echo Yii::t('cms', 'Name of the person')?>",
                    "inputType" : "text",
                    value : obj["nameteam"]
                },

                 speciality : {
                    label : "<?php echo Yii::t('cms', 'Speciality')?>",
                    "inputType" : "text",
                    value : obj["speciality"]
                },
                
                descriptionteam : {
                    label : "<?php echo Yii::t('cms', 'Button link')?>",
                    "inputType" : "text",
                    value :  obj["descriptionteam"]
                },
                labelbtn : {
                    label : "<?php echo Yii::t('cms', 'Button label')?>",
                    "inputType" : "text",
                    value : obj["labelbtn"]
                }, 
                
            };
            return props;
        }
    });
</script>