
<?php 
$keyTpl = "text_img_and_bouton";
$paramsData = [
	"titleLeft" => "Lorem Ipsum",
	"titleRight" => "Lorem Ipsum",
	"lienButtonLeft"        => "",
	"lienButtonRight"        => "",
	"labelButtonLeft"       => "Boutton",
	"labelButtonRight"       => "Boutton",
	"colorlabelButton"  => "#000000",
	"colorBorderButton" => "#000000",
	"colorButton"       => "#ffffff",
  "link" => "",
  "onglet"=>""
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<?php 
  $latestLogo = [];
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"logo"
        ),"image"
    );

    foreach ($initImage as $key => $value) {
         $latestLogo[]= $value["imagePath"];
    }
?>
<style >
    #bg-homepage{
        width: 100%;
        border-top: 1px solid #ccc;
    }
    #header-wrapper {
      position: relative;
      padding: 0em 0em 0em 0em;
      width: 100%;
      height: 550px;
      /*top: -50px;*/
      text-align: center;
  }
  .title {
    font-weight: 500;
    text-transform: none;
  }
  
  .header-about-left{
    background-color: transparent;
    position: absolute;
    top: 100px;
    left: 10%;
 }
  .header-about-right{
    background-color: transparent;
    position: absolute;
    top: 275px;
    right: 10%;
 }
  .header-logo<?= $kunik?>{
      text-align: center;
      padding-top: 130px;
  }
  .button_<?=$kunik?> {
    background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorButton"]; ?>;
    border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["colorBorderButton"]; ?>;
    color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["colorlabelButton"]; ?>;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    padding: 10px;
    font-size: 16px;
    cursor: pointer;
    border-radius: 20px ;
    padding: 8px 8px;
  }

  .well {
      min-height: 20px;
      padding: 19px;
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      border: none;
      border-radius: 0px;
  }

  @media (max-width: 1399px ) {

    .header-logo<?= $kunik?>{
      text-align: center;
    }
    .header-logo<?= $kunik?> img {
      height: 310px;
      width: auto;
    }
    .header-about-right{
	    right: 14%;
	 }
	.header-about-left{
	    left: 14%;
	 }
  }
  @media (min-width: 1400px) {
    #header-wrapper {
        height: 600px;
    }
    .header-logo<?= $kunik?> img {
	    height: 360px;
	    width: auto;
	  }
	 .header-logo<?= $kunik?>{
	      text-align: center;
	      padding-top: 120px;
  	}
  	.header-about-right{
	    right: 18%;
	 }
	.header-about-left{
	    left: 18%;
	 }
	 .button_<?=$kunik?> {
	    padding: 10px 20px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    cursor: pointer;
	    border-radius: 35px;
	  }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    #header-wrapper {
        height: 460px;
    }
    .header-logo<?= $kunik?> img {
	    height: 200px;
	    width: auto;
	  }
	  .header-logo<?= $kunik?>{
	      text-align: center;
	      padding-top: 130px;
  	}
  	.header-about-right{
	    right: 5%;
	    top: 200px;
	 }
	.header-about-left{
	    left: 5%;
	    top: 90px;
	 }
  }

  
  @media (max-width: 767px){
    
   #header-about h1 {
       font-size: 22px!important;
    }
    #header-wrapper {
        height: 300px;
    }

    .header-logo<?= $kunik?>{
      	padding-left: inherit;
        padding-top: 110px;
       /* padding-bottom: 20px;*/
        text-align: center;
    }
    .header-logo<?= $kunik?> img {
      height: 80px;
      width: auto;
    }
    .header-about-right{
	    right: 5px;
	    top: 90px;
	 }
   .button_<?=$kunik?> {
      padding: 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 10px;
      cursor: pointer;
      border-radius: 20px;
    }
	.header-about-left{
	    left: 5px;
	    top: 30px;
	 }
	 .header-about-left h3.title, .header-about-right h3.title {
	 	font-size: 16px;
	 }
	 .button_<?=$kunik?> {
	    font-size: 18px;
	  }
  }

  @media (max-width: 414px) {

	  .button_<?=$kunik?> {
	    padding: 8px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 10px;
	    cursor: pointer;
	    border-radius: 20px;
	  }
}
  
  
</style>

<div id="header-wrapper" >
<?//= $paramsData["class"]?>
	<div  class="header-about-left well">
    	<h3 class="text-center title  sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleLeft"><?= $paramsData["titleLeft"]?></h3>
    	<a href="<?=$paramsData["lienButtonLeft"]?>" class=" button_<?=$kunik?> <?= $paramsData["link"]?>" target="<?= $paramsData["onglet"]?>">
       <?=$paramsData["labelButtonLeft"]?>
     </a>
  </div>

    <div class="header-logo<?= $kunik?>">
      <a class="lbh-menu-app" href="#welcome">
        <img src="<?php echo !empty($latestLogo) ? $latestLogo[0] : ""; ?>">
      </a>
    </div>

  <div class="well header-about-right">
    <h3 class="text-center title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleRight"><?= $paramsData["titleRight"]?></h3>
    	<a href="<?=$paramsData["lienButtonRight"]?>" class=" button_<?=$kunik?> <?= $paramsData["link"]?>" >
       <?=$paramsData["labelButtonRight"]?>
     </a>
  </div>

<script type="text/javascript">
  
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {          
          "titleLeft" : {
            label : "<?php echo Yii::t('cms', 'Title on left')?>",
            values :  sectionDyf.<?php echo $kunik?>ParamsData.titleLeft
          },
          "labelButtonLeft" : {
            "label" : "<?php echo Yii::t('cms', 'Button label left')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelButtonLeft
          },
           "lienButtonLeft" : {
            "label" : "<?php echo Yii::t('cms', 'Button link left')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.lienButtonLeft
          },
          "titleRight" : {
            "label" : "<?php echo Yii::t('cms', 'Title on right')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleRight
          },
          "labelButtonRight" : {
            "label" : "<?php echo Yii::t('cms', 'Button label right')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelButtonRight
          },
           "lienButtonRight" : {
            "label" : "<?php echo Yii::t('cms', 'Button link right')?>",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.lienButtonRight
          },
          "colorlabelButton":{
            label : "<?php echo Yii::t('cms', 'Button label color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorlabelButton
          },
          "colorButton":{
            label : "<?php echo Yii::t('cms', 'Button color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorButton
          },
          "colorBorderButton":{
            label : "<?php echo Yii::t('cms', 'Button border color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBorderButton
          },
          "link":{ 
            "label" : "<?php echo Yii::t('cms', 'Internal or external link')?> ",
            inputType : "select",
            options : {              
              "lbh " : "<?php echo Yii::t('cms', 'Internal')?>",
              "lbh-preview-element " : "<?php echo Yii::t('cms', 'Internal preview')?>",
              "" : "<?php echo Yii::t('cms', 'External')?>",
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.link
          },
          "onglet":{ 
            "label" : "<?php echo Yii::t('cms', 'Link opening')?>",
            inputType : "select",
            options : {              
              "_blank" : "<?php echo Yii::t('cms', 'New tab')?>",
              "" : "<?php echo Yii::t('cms', 'Same tab')?>",
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.onglet
          },
          "logo" : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t('cms', 'Logo')?>",
            "showUploadBtn" : false,
            "docType" : "image",
            "itemLimit" : 1,
            "contentKey" : "slider",
            "order" : 9,
            "domElement" : "logo",
            "placeholder" : "image logo",
            "afterUploadComplete" : null,
            "endPoint" : "/subKey/logo",
            "filetypes" : [
            "png","jpg","jpeg","gif"
            ],
            initList : <?php echo json_encode($initImage) ?>
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};

          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                 toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
          }
        }
      }
    };
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });

  });
  </script>