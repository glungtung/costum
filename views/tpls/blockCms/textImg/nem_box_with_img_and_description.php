
<?php 
$keyTpl = "nem_box_with_img_and_description";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
    "title" => "OUR TEAM",
    "colorDecor" => "#0dab76",
    "textColor" => "white",
    "titleColor" => "white",
    // ,"background_color" => "#eee"   
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>


<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>
<style type="text/css">  
    .box-container:after,.box-container:before{content:""}
    .box-container{overflow:hidden}
    .box-container .title{letter-spacing:1px}
    /*.box-container .post{font-style:italic}*/
    .mt-30{margin-top:30px}
    .mt-40{margin-top:40px}
    .mb-30{margin-bottom:30px}


    /*********************** Demo - 7 *******************/
    .box-container{position:relative}
    .box-container:after,.box-container:before{width:100%;height:100%;background:rgba(11,33,47,.9);position:absolute;top:0;left:0;opacity:0;transition:all .5s ease 0s}
    .box-container:after{background:rgba(255,255,255,.3);border:2px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorDecor"]; ?>;top:0;left:170%;opacity:1;z-index:1;transform:skewX(45deg);transition:all 1s ease 0s}
    .both-box:hover .box-container:before{opacity:1}
    .both-box:hover .box-container:after{left:-170%}
    /*.box-container img{width:auto;height:400px;}*/
    .box-container img.img-responsive{
        height: 400px;
        float: inherit;
        width: 100%;
        object-fit: cover;
    }
    .box-container .box-content{width:100%;position:absolute;bottom:-100%;left:0;transition:all .5s ease 0s}
    .both-box:hover .box-container .box-content{bottom:10%}
    .box-container .title,.box-footer .title {display:block;font-weight:500;color:#fff;margin:0 0 10px}
    .box-container .post{display:block;font-size:15px;font-weight:500;color:#fff;margin:10px}
    .box-container .icon{margin:0}
    .box-container .icon li{display:inline-block}
    .box-container .icon li a{display:block;width:35px;height:35px;line-height:35px;border-radius:50%;background:<?php echo $paramsData["colorDecor"]; ?>;font-size:18px;color:<?= $paramsData["titleColor"]; ?>;margin-right:10px;transition:all .5s ease 0s}
    .<?php echo $kunik ?> .btn-add {
        background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorDecor"]; ?>;
    }
    .box-container .icon li a:hover{transform:rotate(360deg)}
    .box-container .title{text-transform:none}
    .box-container,.box9{box-shadow:0 0 3px rgba(0,0,0,.3)}
    .box-container .icon{padding:0;list-style:none}
    .box-container,.box-container .icon li a{text-align:center}
    @media only screen and (max-width:990px){
        .box{margin-bottom:30px}
    }

    .box-footer {
        padding: 15px 5px 10px 5px;
        background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorDecor"]; ?>;
        text-align: center;
    }

    .title .title-2{
        color:white !important;
    }

</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 <?php echo $kunik ?>">
    <h3 class="text-center title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></h3>
    <div class="row mt-30">
        <?php 
        if (isset($content)) {
            foreach ($content as $key => $value) {?>
                <div class="col-md-4 col-sm-6 both-box mb-30">
                    <div class="box-container">
                        <?php  
                            $initFiles = Document::getListDocumentsWhere(
                                array(
                                  "id"=> $blockKey,
                                  "type"=>'cms',
                                  "subKey"=> (string)$key 
                                ), "image"
                            );
                            $arrFile = [];
                            //var_dump($initFiles);
                            foreach ($initFiles as $k => $v) {
                                $arrFile[] =$v['imagePath'];
                            }
                        ?> 

                        <?php if (!empty($arrFile[0])){ ?>
                            <img class="img-responsive" src=" <?php echo $arrFile[0] ?>" alt="card image">
                        <?php }else { ?>
                            <img class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"> 
                        <?php } ?>

                        <div class="box-content">
                            <?php echo (isset($value["link"]))?'<a href="'.$value["link"].'" target="_blank">':''; ?>
                            <h3 class="title-3" style="color:<?= $paramsData["textColor"] ?>!important"><?= $value["speciality"] ?></h3>
                            <div class="post title-4"  style="color:<?= $paramsData["textColor"] ?>!important"><?= $value["descriptionteam"] ?></div>
                            <?php echo (isset($value["link"]))?'</a>':''; ?>
                            
                            <?php if(Authorisation::isInterfaceAdmin()){ ?>
                                <ul class="icon">
                                    <li>
                                        <a href="javascript:;" class="editElement<?= $blockCms['_id'] ?>"
                                            data-key="<?= $key ?>" 
                                            data-img='<?php echo json_encode($initFiles) ?>' 
                                            data-nameteam="<?= $value["nameteam"] ?>"
                                            data-speciality="<?= $value["speciality"] ?>"
                                            data-link="<?= $value["link"]??"" ?>"
                                            data-descriptionteam="<?= $value["descriptionteam"] ?>"><i class="fa fa-edit"></i></a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="deletePlan<?= $blockKey ?>"
                                            data-key="<?= $key ?>" 
                                            data-id ="<?= $blockKey ?>"
                                            data-path="content.<?= $key ?>"
                                            data-collection = "cms"><i class="fa fa-trash"></i></a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-footer">
                        <?php echo (isset($value["link"]))?'<a href="'.$value["link"].'" target="_blank">':''; ?>
                        <h3 class="title-2" style="color:<?= $paramsData["titleColor"] ?>!important"><?= $value["nameteam"] ?></h3>
                        <?php echo (isset($value["link"]))?'</a>':''; ?>
                    </div>
                </div>
        <?php } 
            } ?>
    </div>
        <div class="text-center editSectionBtns">
            <div class="" style="width: 100%; display: inline-table; padding: 10px;">
                <?php if(Authorisation::isInterfaceAdmin()){?>
                    <div class="text-center addElement<?= $blockCms['_id'] ?>">
                        <button class="btn btn-primary btn-add"><i class="fa fa-plus-circle"></i> <?php echo Yii::t('common', 'Add')?></button>
                    </div>  
                <?php } ?>
            </div>
        </div>

</div>
  

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {                    
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    // background_color : {
                    //     label : "Couleur du fond",
                    //     inputType : "colorpicker",
                    //     values :  sectionDyf.<?php echo $kunik ?>ParamsData.background_color
                    // }
                    titleColor : {
                         label : "Couleur du titre",
                         inputType : "colorpicker",
                         values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
                    },
                    textColor : {
                         label : "Couleur des textes",
                         inputType : "colorpicker",
                         values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
                    },
                    colorDecor : {
                         label : "Couleur du fond du titre",
                         inputType : "colorpicker",
                         values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorDecor
                    }
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("<?php echo Yii::t('cms', 'updated item')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            } );
                        }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        

        $(".deletePlan<?= $blockCms['_id'] ?>").click(function() { 
            var deleteObj ={};
            deleteObj.id = $(this).data("id");
            deleteObj.path = $(this).data("path");          
            deleteObj.collection = $(this).data("collection");
            deleteObj.value = null;
            bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?>?",
            function(result){
              if (!result) {
                return;
              }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
              }
            }); 
        });
        
        $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
            //var contentLength = Object.keys(<?php //echo json_encode($content); ?>).length;
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>"
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(key);
            var obj = {
                nameteam :         $(this).data("nameteam"),
                descriptionteam:    $(this).data("descriptionteam"),
                img:            $(this).data("img"),
                speciality:     $(this).data("speciality"),
                link:           $(this).data("link")
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add new team')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    urlCtrl.loadByHash(location.hash);
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    }else{
                                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                        // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        $(".addElement<?= $blockCms['_id'] ?>").click(function() { 
            var keys = Object.keys(<?php echo json_encode($content); ?>);
            var lastContentK = 0; 
            if (keys.length!=0) 
                lastContentK = parseInt((keys[(keys.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "content."+(lastContentK+1);
            var obj = {
                nameTeam :         $(this).data("nameTeam"),
                descriptionTeam:    $(this).data("descriptionTeam"),
                speciality:     $(this).data("speciality"),
                link:           $(this).data("link")
            };

            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add new team')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,lastContentK+1),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                      tplCtx.value = {};
                      $.each( activeForm.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                      });

                      if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                      else {
                          dataHelper.path2Value( tplCtx, function(params) { 
                               dyFObj.commonAfterSave(null, function(){
                                    if(dyFObj.closeForm()){
                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }else{
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                    }
                                });
                          } );
                      }

                    }
                }
                };          
                dyFObj.openForm( activeForm );
            });


        
        function getProperties(obj={},subKey){
            var props = {
                img : {
                    "inputType" : "uploader",
                    "label" : "<?php echo Yii::t('cms', 'image')?>",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint": "/subKey/"+subKey,
                    "domElement" : "image",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "label": "Image :",
                    "showUploadBtn": false,
                    initList : obj["img"]
                },
                nameteam : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",
                    "inputType" : "text",
                    value : obj["nameteam"]
                },

                 speciality : {
                    label : "<?php echo Yii::t('cms', 'Subtitle')?>",
                    "inputType" : "text",
                    value : obj["speciality"]
                },
                
                descriptionteam : {
                    label : "<?php echo Yii::t('cms', 'Description')?>",
                    "inputType" : "textarea",
                    value :  obj["descriptionteam"]
                },

                link : {
                    label : "<?php echo Yii::t('cms', 'link')?>",
                    "inputType" : "text",
                    value : obj["link"]
                },
                
            };
            return props;
        }
    });
</script>