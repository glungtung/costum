<?php 
$keyTpl = "blocimgroundright";
$paramsData=[
	"title" => "Block description avec image à droite",
	"blockTitle"   => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.",
	"titleSize"    => "34",
	"titleColor"   => "#2C3E50",

	"text"		  => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation",
	"textSize"	  => "18",
	"textColor"	  => "#2C3E50",
	"imageSize"=> "22",
  "link"    => "#",
  "linkLabel"    => "Participer",
	"circle_color"   => "#f9576d"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );
  $latestImg = isset($initImage["0"]["imageMediumPath"])?$initImage["0"]["imageMediumPath"]:"empty" ;
  if($latestImg == ""){
    $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
  }
 ?>
<!-- ****************end get image uploaded************** -->
<style type="text/css">

	.container<?php echo $kunik ?> .btn-edit-delete{
		display: none;
	}
	.container<?php echo $kunik ?> .btn-edit-delete .btn{
		box-shadow: 0px 0px 20px 3px #ffffff;
	}
	.container<?php echo $kunik ?>:hover .btn-edit-delete{
		display: block;
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%,0%);
	} 
	.text<?php echo $kunik ?> *{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}
</style>
<div class="container<?php echo $kunik ?>" style="; color: white">
	<div style="padding-top: 50px!important;padding-left: 10%!important;padding-right: 10%!important;font-family: Lato-Italic">
		<div class="row">
			<div class="col-md-6">
				<div class="bold bg-blur sp-text" data-id="<?= $blockKey ?>" data-field="blockTitle" style="font-size: <?php echo $paramsData["titleSize"]; ?>px;color: <?php echo $paramsData["titleColor"]; ?>"><?php echo $paramsData["blockTitle"]; ?></div><br>
				<div class="text<?php echo $kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="text">
					<div class=" bg-blur"><?= $paramsData["text"]; ?></div>
				</div>
			</div>
			<div class="col-md-6 text-center">
				<div style="border: 4px solid <?= $paramsData["circle_color"]?>;border-radius: 100%;display: inline-table; padding: 20px;">
					<?php if (!empty($latestImg)) { ?>
							<img style="width: <?php echo $paramsData["imageSize"]; ?>vw;height: <?php echo $paramsData["imageSize"]; ?>vw; border-radius: 100%;" src="<?php echo $latestImg ?>">
					<?php }else{ ?>
						
					<?php } ?>
				</div>
        <div style="padding-top: 70px !important;">
          <a class="lbh" style='color: #000000;text-decoration: none;background-color: #ffffff;border-color: #00a8b3;font-size: 35px;padding:20px;padding-left:100px;padding-right:100px;' href='<?= $paramsData['link'] ?>'><?= $paramsData['linkLabel'] ?></a>
        </div>
			</div>			
		</div>
	</div>
</div>
<script type="text/javascript">
	  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
      		  "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {              
              "imageSize" : {
              	"label" : "<?php echo Yii::t('cms', 'Photo size')?>",
              	"value" :  sectionDyf.<?php echo $kunik ?>ParamsData.imageSize
              },
              "circle_color" : {
              	"label" : "<?php echo Yii::t('cms', 'Circle color')?>",
              	"inputType" : "colorpicker",
              	"value" :  sectionDyf.<?php echo $kunik ?>ParamsData.circle_color
              },
              "image" :{
               "inputType" : "uploader",
               "label" : "<?php echo Yii::t('cms', 'Image')?>",
               "docType": "image",
               "contentKey" : "slider",
               "itemLimit" : 1,
               "endPoint": "/subKey/block",
               "domElement" : "image",
               "filetypes": ["jpeg", "jpg", "gif", "png"],
               "label": "Image :",
               "showUploadBtn": false,
               initList : <?php echo json_encode($initImage) ?>
              },
              link : {
                label : "<?php echo Yii::t('cms', 'Link')?>",
                "inputType" : "textarea",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.link
              },
              linkLabel : {
                label : "<?php echo Yii::t('cms', 'Button label')?>",
                "inputType" : "textarea",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.linkLabel
              }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>