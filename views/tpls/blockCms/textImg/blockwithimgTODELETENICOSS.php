<?php
$keyTpl = "blockwithimg";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = $blockCms["_id"];
$paramsData = [
    "title" =>  "block avec une image",
    "color" => "#000000",
    "icon"  =>"",
    "txtcolor"  =>  "#000000"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (    isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
    .withimg_<?= $kunik?> h1 {
        color : <?= $paramsData["color"] ?>;
    } 
    .withimg_<?= $kunik?> .markdown {
        color : <?= $paramsData["txtcolor"] ?>;
    }
</style>
<div class="withimg_<?= $kunik?>">
    <h1 class="text-center">
        <?= $paramsData["icon"]." ".$paramsData["title"]; ?>      
    </h1>


        <?php
        $tags = (isset($tags)) ? $tags : "blockimg0";
        $page = (isset($page)) ? $page : "";
        $cmsTpl = PHDB::findOne(Cms::COLLECTION ,  [
            "structags"     => $tags.$page.$kunik,
            "source.key"    => $costum["slug"]
        ]);

        if(!empty($cmsTpl)):

            $edit = "update";
            $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $cmsTpl["_id"]));
            $name = isset($cmsTpl["name"])?$cmsTpl["name"]:"";
            $description = isset($cmsTpl["description"])?$cmsTpl["description"]:"";
            ?>
            <div class="blockwithimg-txt<?= $kunik?> col-md-6">
                 <h1 class="title-blockwithimg"><?=  $name ?></h1>
                 <span class="markdown"><?= $description ?></span>
            </div>
         <?php 
         if(isset($img)):
          ?>
          <div class="blockwithimg-img<?= $kunik?>  col-md-6">
            <img src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
        </div>
    <?php endif;
    else: ?>
        <div class="col-xs-12 text-center">
           <!--  TODO : <br />
            as POI type cms + tag : blockimg0; -->
            <?php 
            $edit = "create"; 
            echo"</div>";
        endif;

        echo '<div class="col-xs-12 text-center">';

        echo $this->renderPartial("costum.views.tpls.openFormBtn",[
            'edit' => $edit,
            'tag' => $tags.$page.$kunik,
            'page' => $page,
            'id' => (string)$cmsTpl["_id"]],true);

        echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
        $pathId = isset($path)?$path.$kunik : "";
        $page = isset($page)?$page:"";
        echo "</div>";

        ?>
    </div>
</div>
        <script type="text/javascript">
            sectionDyf.<?php echo $kunik ?>ParamsData = { };
            parent = <?= json_encode($cmsTpl["parent"]); ?>;
            sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        mylog.log("---------- Render blockimg","costum.views.tpls.blockwithimg");

              mylog.log("---------- Render blockimg","costum.views.tpls.blockwithimg");
              sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {    
                    "title" : "Configurer la section bloc un texte et une image",
                    "description" : "Personnaliser votre section sur les blocs d'un texte et une image",
                    "icon" : "fa-cog",
                    "properties" : {
                        title : {
                            label : "Titre",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                        },
                        color : {
                            label : "Couleur du titre",
                            inputType : "colorpicker",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.color

                        },
                        txtcolor : {
                            label : "Couleur du texte",
                            inputType : "colorpicker",
                            values :  sectionDyf.<?php echo $kunik ?>ParamsData.txtcolor

                        }
                    },
                    save : function () {  
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                            tplCtx.value[k] = $("#"+k).val();
                        });
                        console.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                toastr.success("élément mis à jour");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                //urlCtrl.loadByHash(location.hash);
                            } );
                        }
                    }
                }
            };

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx = {};  
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = "allToRoot";

                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });


            $(".editThisBtn").off().on("click",function (){
                var id = $(this).data("id");
                var type = $(this).data("type");

                dyFObj.editElement(type,id,null);
            });

            $(".createBlockBtn").off().on("click",function (){
                dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS);
            });

            $(".deleteThisBtn").off().on("click",function (){
                $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                var btnClick = $(this);
                var id = $(this).data("id");
                var type = $(this).data("type");
                var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;

                bootbox.confirm(trad.areyousuretodelete,
                    function(result) {
                        if (!result) {
                            btnClick.empty().html('<i class="fa fa-trash"></i>');
                            return;
                        } 
                        else {
                            $.ajax({
                                type: "POST",
                                url: urlToSend,
                                dataType : "json"
                            })
                            .done(function (data) {
                                if ( data && data.result ) {
                                  toastr.success("élément effacé");
                                  $("#"+type+id).remove();
                              } else {
                                 toastr.error("something went wrong!! please try again.");
                             }
                         });
                        }
                    });
            });


        </script>