<?php 
$keyTpl = "blocimgleftcustom";
$paramsData = [  
	"title" => "Lorem ipsum",
	"titleColor" => "#ffffff",
	"text" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	"titleSize"	  => "24",
	"textSize"	  => "18",
	"textColor"	  => "#566d7b",
	"background_color"=> "#f9576d",  
	"colorCircle" =>"#c0cb1f",
	"backgroundTitle" => ""
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>

<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );
  $latestImg = isset($initImage["0"]["imageMediumPath"])?$initImage["0"]["imageMediumPath"]:"empty" ;
  if($latestImg == ""){
    $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
  }
 ?>
<!-- ****************end get image uploaded************** -->
<style type="text/css">
	.container<?php echo $kunik ?> .left-circle{
		border: 4px solid <?= $paramsData["colorCircle"]; ?>;
		border-radius: 100%;
		position: absolute;
		width: 20vw;
		height: 20vw;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}

	.container<?php echo $kunik ?> .circle{
		background-color: <?= $paramsData["background_color"]; ?>;
		border-radius: 100%;
		position: absolute;
		width: 3.4vw;
		height: 3.4vw;
		transform: translate(-50%, -50%);
	    top: 10%;
	    left: 70%;
	}

	.container<?php echo $kunik ?> .left-circle-img {
	    border-radius: 100%;
	    position: absolute;
	    z-index: 2;
	    top: 40%;
	    left: 40%;
	    transform: translate(-50%, -50%);
	}
	.container<?php echo $kunik ?> span{
		font-size: <?= $paramsData["titleSize"]; ?>px;
		width: 60%; 
		background-color: <?= $paramsData["background_color"]; ?>;
		color: <?= $paramsData["titleColor"]; ?>
	}
	.text<?php echo $kunik ?>{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}
	.text<?php echo $kunik ?> p{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}
	.text<?php echo $kunik ?> p, li{
		font-size: <?php echo $paramsData["textSize"]; ?>px;
		color: <?php echo $paramsData["textColor"]; ?>; 
	}

	.container<?php echo $kunik ?> .btn-edit-delete{
		display: none;
	}
	.container<?php echo $kunik ?> .btn-edit-delete .btn{
		box-shadow: 0px 0px 20px 3px #ffffff;
	}
	.container<?php echo $kunik ?>:hover .btn-edit-delete{
		display: block;
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%,0%);
	}
</style>
<div class="container<?php echo $kunik ?>">
	<div style="padding-top: 50px!important;padding-left: 5%!important;padding-right: 5%!important;">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-12 text-center" style="min-height: 600px">
				<div class="text-center">
					<div class="left-circle">					
					</div>
					<div class="circle">					
					</div>
					<div class="left-circle-img">
						<?php if (!empty($latestImg)) { ?>
								<img style="width: 18vw;height: 18vw; border-radius: 100%;z-index: 1" src="<?php echo $latestImg ?>">
						<?php }else{ ?>
							
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-md-6 padding-20">
				<div class="text-center" style="padding-bottom: 40px">
					<span class="bold text-center title  padding-20 sp-text" data-id="<?= $blockKey ?>" data-field="title">
						<?= $paramsData["title"]; ?>
					</span>
				</div>
				<div class="bg-blur">
					<div class="text<?php echo $kunik ?> description sp-text" data-id="<?= $blockKey ?>" data-field="text"><?= $paramsData["text"]; ?></div>
				</div>
			</div>			
		</div>
	</div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
      		"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",            
            "properties" : {              
              "image" :{
                  "inputType" : "uploader",
                  "label" : "<?php echo Yii::t('cms', 'Image')?>",
                  "docType": "image",
                  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/block",
                  "domElement" : "image",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "label": "Image :",
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImage) ?>
              },
              "colorCircle" : {
                "inputType" : "colorpicker",
                "label" : "<?php echo Yii::t('cms', 'Color of the big circle')?>",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorCircle
              },
              "background_color" : {
                "inputType" : "colorpicker",
                "label" : "<?php echo Yii::t('cms', 'Background color of the title')?>",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.background_color
              },
            },
            beforeBuild : function(){
              
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
					  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>