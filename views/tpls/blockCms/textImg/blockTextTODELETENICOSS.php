<?php
$keyTpl = "blockText";
$kunik = $keyTpl.(string)$blockCms["_id"];
$paramsData = [
    "title" =>  "Titre - Texte",
    "color" => "#000000",
    "background" => "#FFFFFF",
    "txtcolor"  =>  "#000000", 
    "name" => "",
    "description" => ""
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
    #<?php echo $keyTpl ?> .<?php echo $keyTpl ?>-txt .markdown {color : <?= $paramsData["txtcolor"] ?>;}
    #<?php echo $keyTpl ?> .<?php echo $keyTpl ?>-txt .title-<?php echo $keyTpl ?> {color : <?= $paramsData["txtcolor"] ?>;}
    #<?php echo $keyTpl ?> .<?php echo $keyTpl ?>-txt .title-<?php echo $keyTpl ?> {background-color:  : <?= $paramsData["background"] ?>;}
</style>

<div id="<?php echo $keyTpl ?>" class="<?php echo $keyTpl ?> row" data-archi="bloc CMS permettant d'afficher un titre et un texte">
    <div class="col-md-12">
        <h1 style="color:<?= @$paramsData["txtcolor"]?>"><?= @$paramsData["name"]?></h1>
        <span style="color:<?= @$paramsData["txtcolor"]?> !important;text-align:left;" class="markdown"><?= @$paramsData["description"]?></span>
    </div>
<?php
$edit ="update";
?>

<div class="col-md-12">
    <center>
    <?php
      echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                      array(
                                          'edit' => $edit,
                                          'tag' => @$tag,
                                          'page'  => @$page,
                                          'id' => (string)$blockCms["_id"]
                                       ),true);

        echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS");  
    ?>
    </center>
</div>




<script type="text/javascript">
tplCtx = {};
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
page = <?= json_encode(@$page); ?>;
type = 'tpls.blockCms.textImg.<?= $kunik ?>';

sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

jQuery(document).ready(function(){

        mylog.log("---------- Render blockimg costum.views.tpls.<?php echo $kunik ?>");
        sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
            title : "Configurer la section bloc un texte et une image",
            description : "Personnaliser votre section sur les blocs d'un texte et une image",
            icon : "fa-cog",
            onLoads : {
                onload : function(){
                    $(".parentfinder").css("display","none");
                }
            },
            properties : {
                name : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.name
                },
                color : {
                    label : "Couleur du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.color

                },
                background : {
                    label : "Couleur du fond du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
                },
                txtcolor : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.txtcolor
                },
                description : {
                    label : "Description",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.description
                },
                // parent : {
                //     inputType : "finder",
                //     label : tradDynForm.whoiscarrytheproject,
                //     multiple : true,
                //     rules : { required : true, lengthMin:[1, "parent"]}, 
                //     initType: ["organizations"],
                //     openSearch :true
                // }
            },
            save : function () {  
                tplCtx.value = {};

                if (typeof idToPath != "undefined") 
                    tplCtx.value["id"] = idToPath;

                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value["parent"] = formData.parent ;
                    }
                });

                tplCtx.value["page"] = '<?= @$page ?>';
                tplCtx.value["type"] = 'tpls.blockCms.textImg.<?= $keyTpl ?>';
                
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        toastr.success("élément mis à jour");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
        tplCtx = {};  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = (typeof $(this).data("path") != "undefined" && $(this).data("path") != "") ? $(this).data("path") : "allToRoot";
        idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
});

    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dataObj = {parent : {} };
        // dataObj.parent[contextId] = {collection : costum.col , name : costum.name}; 
        dataObj.structags = $(this).data("tag");

        dyFObj.openForm('cms',null,dataObj,null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
        var btnClick = $(this);
        var id = $(this).data("id");
        var type = $(this).data("type");
        var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
        bootbox.confirm(trad.areyousuretodelete,
            function(result) {
                if (!result) {
                    btnClick.empty().html('<i class="fa fa-trash"></i>');
                    return;
                } 
                else {
                $.ajax({
                    type: "POST",
                    url: urlToSend,
                    dataType : "json"
                })
                .done(function (data) {
                    if ( data && data.result ) {
                      toastr.success("élément effacé");
                      $("#"+type+id).remove();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    } else {
                       toastr.error("something went wrong!! please try again.");
                    }
                });
            }
        });
    });

    
</script>
</div>