<!-- ****************get image uploaded************** -->
<?php
if(!isset($costum))
    $costum = CacheHelper::getCostum();
$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name", "description","descriptionVille","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl"]);


$initFiles = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'block',
    ), "image"
);
$initLogoFiles = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"logo"
    ),"image"
);
$logoPicture = [];
foreach ($initLogoFiles as $key => $value) {
    $logoPicture[]= $value["imagePath"];
}
$imagesBackground = [];
foreach ($initFiles as $key => $value) {
    array_push($imagesBackground, $value["imagePath"]);
}

?>
<?php
$keyTpl = "text_img_prez";
$paramsData = [
    "paragraph" => "\n \t\t\t\t\n \t\t\t\t\n \t\t\t\t\n \t\t\t\t<font color=\"#5f9f0d\" style=\"font-size: 25px;\">Nouveaux outils et méthode de travail Collaboratif pour une Economie COopérative</font> \n\t\t\t\t<div><font color=\"#2b4d56\">Améliorer la qualité de vie en commun et travail avec OCECO</font></div><div><font color=\"#2b4d56\">COnstruire ensemble une dynamique transparente, productive et efficiente.</font></div> \n\t\t\t\t \n\t\t\t",
    "colordecor" =>"#5f9f0d",
    "btnBgColor" => "#0dab76",
    "btnLabelColor" => "white",
    "btnLabel" => "Essayez",
    "btnBorderColor" => "#0dab76",
    "btnRadius" => "12",
    "btnLink" => "#".@$parent["slug"],
    "showBtn" => "show",
    "imagePosition" => "left",
    "textShadow" => "no",
    "logo" => "",
    "imagesBackground"=>"",
    "imageLinkClass"=> "lbh",
    "imageLink"=> "",
    "borderLeftRadius"=> "no",
    "borderBg"=> "#e8f1dc",
    "logoHeightResponsive" => "55px",
    "logoHeight" => "auto",
    "logoWidth" => "",
    "btnPadding" => "6px 20px",
    "btnPolice" => "",
    "btnfontWeight" => "600",
	"btnClass" => "lbh",
    "btnOpening" => "_blank",
    "btnfontSize" => "18",
    "btnMarginTop" => "",
    "colImageParentPadding" => "",
    "colImagePadding" => "10%",
    "colInfoPadding" => "0 10% 0 10%",
    "btnDownload" => "no",
    "btnDownloadLink" => "#"


];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if(count($logoPicture)!=0){
    $paramsData["logo"] = $logoPicture;
}
if(count($imagesBackground)!=0){
    $paramsData["imagesBackground"] = $imagesBackground;
}

?>
<style>

    <?php if($paramsData["borderLeftRadius"] == "yes") { ?>
    .prez-right-radius{
        background: <?php echo $paramsData["borderBg"]; ?>;
        border-top-left-radius: 15% 50%;
        border-bottom-left-radius: 15% 50%;
    }
    <?php } ?>

    .prez<?=$kunik ?> .img-logo {
        height: <?php echo $paramsData["logoHeight"]; ?>;
        width : <?php echo $paramsData["logoWidth"]; ?>;
    }
    .mt-40 {
        margin-top: 40px;
    }

    .prez<?=$kunik ?> .prez-img{
        padding: <?= $paramsData["colImagePadding"]?>;
    }

    .prez<?=$kunik ?> .prez-right-radius{
        padding : <?= $paramsData["colImageParentPadding"]?>
    }
    .prez<?=$kunik ?> .prez-img img{
        width: 100%;
    }
    .prez-text .sp-text{
        margin-bottom: 2%;
    }
    .prez<?=$kunik ?> .btnprez{

        font-family: <?php echo str_replace(" ", "_", (explode(".ttf",(array_reverse(explode("/", $paramsData["btnPolice"]))[0]))[0])) ?>;
        padding: <?php echo $paramsData["btnPadding"] ?>;
        font-weight: <?= $paramsData["btnfontWeight"]?>;
        cursor: pointer;
        font-size:  <?= $paramsData["btnfontSize"]?>px;
        background-color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
        color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnLabelColor"]; ?>;
        border-radius: <?php echo $paramsData["btnRadius"] ?>px !important;
        border:2px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["btnBorderColor"]; ?>;
    }
    .prez<?=$kunik ?> .btn-contain {
        position: absolute;
        top: 85%;
        text-align: center;
    }
    .btn-download { padding:8px; background:#ffffff; margin-right:4px; }
    .icon-btn { padding: 1px 15px 3px 2px; border-radius:50px;}

    @media (min-width: 992px) {
        .prez<?=$kunik ?> .prez-text {
            padding :<?= $paramsData["colInfoPadding"]?>
        }
        .btn<?= $kunik?> {
            margin-top : <?= $paramsData["btnMarginTop"]?>
        }
    }
    @media (min-width: 768px) {
        .prez<?=$kunik ?> .prez-right-radius, .prez<?=$kunik ?> .prez-left, .prez<?=$kunik ?>.prez-right {
            min-height: 320px;
        }
        .prez<?=$kunik ?> .prez-right {
            <?php if ($paramsData['imagePosition'] == "none") { ?>
                padding-top: 60px;
                padding-bottom: 60px;
            <?php } ?>
        }
        .prez<?=$kunik ?> .prez-text {

            <?php if ($paramsData['imagePosition'] != "none") { ?>
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            <?php } ?>
            
            width: 100%;
            padding-left: 15px;
            padding-right: 15px;
        }
        .prez<?=$kunik ?> {
            display: flex;
            float: none;
        <?php if ($paramsData['textShadow'] == "yes") { ?>
            background: linear-gradient( 90deg, rgb(6, 10, 25, 0) 35%, rgba(6, 10, 25,9) 60%);
            height: 100%;
        <?php } ?>
        }
        .prez<?=$kunik ?> .prez-multi-col {
            padding-top: 40px;
            padding-bottom: 40px;
        }
    }
    @media (max-width: 991px) {
        .prez<?=$kunik ?> .img-logo {
            height: <?= $paramsData["logoHeightResponsive"]?>;
        }
        .prez<?=$kunik ?> .prez-text {
            padding-top: 15px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 767px) {
        .prez<?=$kunik ?> {
            float: none;
        <?php if ($paramsData['textShadow'] == "yes") { ?>
            background: linear-gradient( 90deg, rgb(6, 10, 25, .7) 100%, rgba(22, 32, 82,0) 100%);
            height: 100%;
        <?php } ?>
        }
        .prez<?=$kunik ?> .btn-contain {
            position: absolute;
            top: 50%;
            left: 50%;
            text-align: center;
            transform: translate(-50%,-50%);
        }
    }
</style>
<div class="prez<?=$kunik ?> col-xs-12 no-padding">
    <?php if ($paramsData['imagePosition'] == "left") { ?>
        <div class="col-sm-6 col-md-6 prez-left">
            <div class="prez-img">
            <?php if(isset($paramsData['imageLink']) &&  $paramsData['imageLink']!= ""){ ?>
                <a href="<?= $paramsData["imageLink"]?>" class="<?= $paramsData["imageLinkClass"]?>">
            <?php } ?>
                <?php if(isset($paramsData['imagesBackground'])){ ?>
                    <?php if(is_string($paramsData["imagesBackground"]) && $paramsData["imagesBackground"]!=""){?>
                        <img class="img-responsive" src="<?= $paramsData['imagesBackground']; ?>">
                    <?php }else if(isset($paramsData['imagesBackground']['0'])){ ?>
                        <img class="img-responsive" src="<?= $paramsData['imagesBackground']['0'] ?>">
                    <?php } ?>
                <?php }else { ?>
                    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg'>
                <?php } ?>
            <?php if(isset($paramsData['imageLink']) &&  $paramsData['imageLink']!= ""){ ?>
                </a>
                <?php } ?> 
            </div>
        </div>
    <?php } ?>
    <?php if ($paramsData['imagePosition'] == "none") { ?>
        <div class="col-sm-6 col-md-6 prez-left">
            <?php if ($paramsData['btnDownload'] == "yes") { ?>
            <div class="btn-contain">
                <a class="btn icon-btn btn-primary" href="<?= $paramsData['btnDownloadLink']; ?>" target="_blank"><span class="fa fa-download btn-download img-circle text-muted"></span>Télécharger le mapping</a>
            </div>
            <?php } ?>
        </div>
    <?php } ?>
     <div class="col-sm-6 col-md-6 prez-right">
        <div class="prez-text text-left">
            <?php if( $paramsData["logo"] != ""){?>
                <p class="">
                    <?php if(is_string($paramsData["logo"]) && $paramsData["logo"]!=""){?>
                        <img class="img-logo" src="<?= $paramsData['logo']; ?>">
                    <?php }else if(isset($paramsData['logo']['0'])){ ?>
                        <img class="img-logo" src="<?= $paramsData['logo']['0'] ?>">
                    <?php } ?>
                </p>
            <?php } ?>
            <div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="paragraph" ><?= $paramsData["paragraph"]?> </div>
            <?php if ($paramsData["showBtn"] == "show") {
                ?>
                <div class="btn<?= $kunik?>">
                    <a href="<?php echo $paramsData["btnLink"] ?>" class="btnprez <?= $paramsData["btnClass"]?>" target="<?= $paramsData["btnOpening"]?>" ><?php echo $paramsData["btnLabel"] ?></a>
                </div>

            <?php } ?>
        </div>
    </div>
    <?php if ($paramsData['imagePosition'] == "right") { ?>
        <div class="col-sm-6 col-md-6 prez-right-radius">
            <div class="prez-img">
                <?php if(isset($paramsData['imagesBackground'])){ ?>
                    <?php if(is_string($paramsData["imagesBackground"]) && $paramsData["imagesBackground"]!=""){?>
                        <img class="img-responsive" src="<?= $paramsData['imagesBackground']; ?>">
                    <?php }else if(isset($paramsData['imagesBackground']['0'])){ ?>
                        <img class="img-responsive" src="<?= $paramsData['imagesBackground']['0'] ?>">
                    <?php } ?>
                <?php }else { ?>
                    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg'>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    // 	"colordecor" : {
                    // 		"label" : "Autre couleur (decoration)",
                    // 		inputType : "colorpicker",
                    // 		values :  sectionDyf.<?php echo $kunik?>ParamsData.colordecor
                    // 	},
                    "imagePosition" :{
                        "inputType" : "select",
                        "label" : "<?php echo Yii::t('cms', 'Align image')?>",
                        "options":{
                            "left":"<?php echo Yii::t('cms', 'To the left')?>",
                            "right":"<?php echo Yii::t('cms', 'To the right')?>",
                            "none":"<?php echo Yii::t('cms', 'None')?>",
                        }
                    },
                    "textShadow" :{
                        "inputType" : "select",
                        "label" : "<?php echo Yii::t('cms', 'Shadow under the text')?>",
                        "options":{
                            "yes":"<?php echo Yii::t('common', 'Yes')?>",
                            "no":"<?php echo Yii::t('common', 'No')?>"
                        }
                    },
                    "btnDownload" :{
                        "inputType" : "select",
                        "label" : "<?php echo Yii::t('cms', 'show the download button')?>",
                        "options":{
                            "yes":"<?php echo Yii::t('common', 'Yes')?>",
                            "no":"<?php echo Yii::t('common', 'No')?>"
                        }
                    },
                    "btnDownloadLink" :{
                        "label" : "<?php echo Yii::t('cms', 'Download link ')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnDownloadLink
                    },
                    "logo" :{
                        "inputType" : "uploader",
                        "docType": "image",
                        "contentKey":"slider",
                        "endPoint": "/subKey/logo",
                        "domElement" : "logo",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        "label": "<?php echo Yii::t('cms', 'Logo')?> :",
                        "itemLimit" : 1,
                        "showUploadBtn": false,
                        initList : <?php echo json_encode($initLogoFiles) ?>
                    },

                    "imagesBackground" :{
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'Background image')?>",
                        "docType": "image",
                        "contentKey" : "slider",
                        "endPoint": "/subKey/block",
                        "domElement" : "image",
                        "filetypes": ["jpeg", "jpg", "gif", "png", "svg"],
                        "label": "Image :",
                        "itemLimit" : 1,
                        "showUploadBtn": false,
                        initList : <?php echo json_encode($initFiles) ?>
                    },
                    "imageLink":{ 
                        "label" : "<?php echo Yii::t('cms', 'Link when you click on the image')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.imageLink
                    },
					"imageLinkClass" : {
						"label" : "<?php echo Yii::t('cms', 'Internal or external link')?>",
						inputType : "select",
						options : {              
							"lbh " : "<?php echo Yii::t('cms', 'Internal')?>",
                            "lbh-preview-element " : "<?php echo Yii::t('cms', 'Internal preview')?>",
							" " : "<?php echo Yii::t('cms', 'External')?>",
						},
	                  	values :  sectionDyf.<?php echo $kunik ?>ParamsData.imageLinkClass
	                },
                    "borderBg" : {
                        "label" : "<?php echo Yii::t('cms', 'Color of the left part')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.borderBg
                    },
                    "borderLeftRadius" :{
                        "inputType" : "select",
                        "label" : "<?php echo Yii::t('cms', 'Left border radius')?>",
                        "options":{
                            "yes":"<?php echo Yii::t('common', 'Yes')?>",
                            "no":"<?php echo Yii::t('common', 'No')?>"
                        }
                    },
                    "showBtn":{
                        "label" : "<?php echo Yii::t('cms', 'Show button')?>",
                        inputType : "select",
                        options : {
                            "show" : "<?php echo Yii::t('cms', 'Show')?>",
                            "hide" : "<?php echo Yii::t('cms', 'Hide')?>"
                        },
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.showBtn
                    },
                    "btnLabel" : {
                        "label" : "<?php echo Yii::t('cms', 'Button label')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                    },
                    "btnLink" : {
                        "label" : "<?php echo Yii::t('cms', 'Button link')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLink
                    },
                    "btnLabelColor":{
                        label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabelColor
                    },
                    "btnBgColor":{
                        label : "<?php echo Yii::t('cms', 'Color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                    },
                    "btnBorderColor":{
                        label : "<?php echo Yii::t('cms', 'Border colo')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBorderColor
                    },
                    "btnRadius" : {
                        "label" : "<?php echo Yii::t('cms', 'Border radius(px)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnRadius
                    },
                    "logoWidth" : {
                        "label" : "<?php echo Yii::t('cms', 'Logo width (px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.logoWidth
                    },
                    "logoHeight" : {
                        "label" : "<?php echo Yii::t('cms', 'Logo height (px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.logoHeight
                    },
                    "logoHeightResponsive" : {
                        "label" : "<?php echo Yii::t('cms', 'Logo height in tablet mode (px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.logoHeightResponsive
                    },
					"btnClass" : {
						"label" : "<?php echo Yii::t('cms', 'Internal or external link')?>",
						inputType : "select",
						options : {              
							"lbh " : "<?php echo Yii::t('cms', 'Internal')?>",
                            "lbh-preview-element " : "<?php echo Yii::t('cms', 'Internal preview')?>",
							" " : "<?php echo Yii::t('cms', 'External')?>",
						},
	                  	values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnClass
	                },
                    "btnOpening":{ 
                      "label" : "<?php echo Yii::t('cms', 'Link opening')?>",
                      inputType : "select",
                      options : {              
                        "_blank" : "<?php echo Yii::t('cms', 'New tab')?>",
                        "" : "<?php echo Yii::t('cms', 'Same tab')?>",
                      },
                      values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnOpening
                    },
                    "btnPadding" : {
                        "label" : "<?php echo Yii::t('cms', 'Button padding (px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnPadding
                    },
                    "btnPolice" : {
                        "label" : "<?php echo Yii::t('cms', 'Font')?>",
                        inputType : "select",
                        options : fontObj,
                        "isPolice": true,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnPolice
                    },
                    "btnfontWeight" : {
                        "label" : "<?php echo Yii::t('cms', 'Font weight')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnfontWeight
                    },
                    "btnfontSize" : {
                        "label" : "<?php echo Yii::t('cms', 'Text size')?>(px) ",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnfontSize,
                        rules :{
                            number :true
                        }
                    },
                    "btnMarginTop" : {
                        "label" : "<?php echo Yii::t('cms', 'Margin on top of the button (in px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnMarginTop
                    },
                    "colImageParentPadding" : {
                        "label" : "<?php echo Yii::t('cms', 'Parent column padding( in px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colImageParentPadding
                    },
                    "colImagePadding" : {
                        "label" : "<?php echo Yii::t('cms', 'Column padding (in px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colImagePadding
                    },
                    "colInfoPadding" : {
                        "label" : "<?php echo Yii::t('cms', 'Column padding (in px or %)')?>",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colInfoPadding
                    },


                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"btn",4,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"image",6,6,null,null,"<?php echo Yii::t('cms', 'Image property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"imageLink",6,6,null,null,"<?php echo Yii::t('cms', 'Image link property')?>","green","");
           alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"logo",4,6,null,null,"<?php echo Yii::t('cms', 'Logo property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"border",6,6,null,null,"<?php echo Yii::t('cms', 'Border property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"colInfo",6,6,null,null,"<?php echo Yii::t('cms', 'Text column property')?>","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"colImage",6,6,null,null,"<?php echo Yii::t('cms', 'Image column property')?>","green","");
            if ($("#showBtn").val() == "hide") {
                $('.fieldsetbtn').fadeOut();
            }
            $("#showBtn").change(function(){
                if ($(this).val() == "hide") {
                    $('.fieldsetbtn').fadeOut();
                }else
                    $('.fieldsetbtn').fadeIn();
            })
        });

    });
</script>