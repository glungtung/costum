<?php 
$keyTpl = "blocimgroundleft";
$paramsData=[
	"title" => "Lorem ipsum dolor sit amet",
	"titleColor" => "#000000",
	"sousTitre1" => "Lorem ",
	"text1" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
	"sousTitre1" => "Lorem ",
	"text2" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
	"sousTitre2" => "Lorem ",
	"text3" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
	"sousTitre3" => "Lorem ",
	"text4" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
	"sousTitre4" => "Lorem ",
	"text5" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
	"sousTitre5" => "Lorem ",
	"titleSize"	  => "24",
	"textSize"	  => "18",
	"imageSize"=> "25",
	"textColor"	  => "#ffffff",
	"background_color"=> "#de4c7c",
	"second_color"=> "#c0cb1f"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );
  $latestImg = isset($initImage["0"]["imageMediumPath"])?$initImage["0"]["imageMediumPath"]:"empty" ;
  if($latestImg == ""){
  	$latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
  }

  $Parsedown = new Parsedown();

 ?>
<!-- ****************end get image uploaded************** -->
<style type="text/css">
	@media screen and (max-width: 992px) {
		.container<?php echo $kunik ?> .clarte{
			left:0;
			top: 0;
			width:100%;
		}

		.container<?php echo $kunik ?> .soutien{
			left:0;
			top: 140px;
			width: 100%;
		}

		.container<?php echo $kunik ?> .savoir-faire{
			left:0;
			top: 280px;
			width: 100%;
		}

		.container<?php echo $kunik ?> .reactivite{
			width: 100%;
			left:0;
			top: 420px;
		}

		.container<?php echo $kunik ?> .recul{
			left:0;
			top: 560px;
			width:100%;
		}
	}

	@media screen and (min-width: 992px) {
		.container<?php echo $kunik ?> .clarte{
			left:25%;
			top: 0;
			width:70%;
		}
		.container<?php echo $kunik ?> .clarte .ariane-suggest-body-content{
			width: 80%
		}
		.container<?php echo $kunik ?> .soutien{
			left:30%;
			top: 140px;
			width: 67.4%;
		}
		.container<?php echo $kunik ?> .soutien .ariane-suggest-body-content{
			width: 76.8%;
		}
		.container<?php echo $kunik ?> .savoir-faire{
			left:32%;
			top: 280px;
			width: 66.4%;
		}
		.container<?php echo $kunik ?> .savoir-faire .ariane-suggest-body-content{
			width: 75.5%; 
		}
		.container<?php echo $kunik ?> .reactivite{
			width: 100%;
			left:30%;
			top: 420px;
			width: 67.4%;
		}
		.container<?php echo $kunik ?> .reactivite .ariane-suggest-body-content{
			width: 76.8%;
		}
		.container<?php echo $kunik ?> .recul{
			left:25%;
			top: 560px;
			width:70%;
		}
		.container<?php echo $kunik ?> .recul .ariane-suggest-body-content{
			width: 80%
		}
	}
	.container<?php echo $kunik ?> .side {
		flex: 30%;
		padding: 20px;
		width: 100%;
	}
	.container<?php echo $kunik ?> .main {
		flex: 70%;
	}
	.container<?php echo $kunik ?> .rows {  
		display: flex;
		flex-wrap: wrap;
	}

	
	.container<?php echo $kunik ?> .btn-edit-delete{
		display: none;
	}
	.container<?php echo $kunik ?> .btn-edit-delete .btn{
		box-shadow: 0px 0px 20px 3px #ffffff;
	}
	.container<?php echo $kunik ?>:hover .btn-edit-delete{
		display: block;
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%,0%);
	}

	@media screen and (min-width: 992px) {
		.container<?php echo $kunik ?> .ariane-suggest-container{
			width: 90%;
			position: relative;
			text-align: left;
			left:80px;
		}  

		.container<?php echo $kunik ?> .ariane-suggest-body{
			position: absolute;
			transform: translate(0,-50%);
			padding: 20px;
			color: #fff;
		}


		.container<?php echo $kunik ?> .ariane-suggest-body-content{
			background-color: <?= $paramsData["background_color"]; ?>;
			border-radius:50px 0 0 50px;
			position: relative;
			text-align: left;
			left:30%;
			top: 50%;
			transform: translate(0,0);
		}

	}


	@media screen and (max-width: 992px) {
		.container<?php echo $kunik ?> .ariane-suggest-container{
			width: 100%;
			position: relative;
			text-align: left;
		}
		.container<?php echo $kunik ?> .ariane-suggest-body{
			transform: translate(0,-50%);
			padding: 20px;
			color: #fff;
		}


		.container<?php echo $kunik ?> .ariane-suggest-body-content{
			background-color: <?= $paramsData["background_color"]; ?>;
			border-radius:50px 0 0 50px;
			position: relative;
			text-align: left;
			top: 50%;
			transform: translate(0,0);
		}

	}

	.container<?php echo $kunik ?> .text-suggest{
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%,-50%);
		font-weight: bold;
	}

	.container<?php echo $kunik ?> .ariane-suggest-body{
		transform: translate(0,-50%);
		padding: 20px;
		color: #fff;
	}

	.container<?php echo $kunik ?> .ariane-suggest-badge{
		width: 110px;
		height: 110px; 
		text-align: center;
		background-color: #fff; 
		color: black;
		border-radius: 50%;
		position: relative;
		border: solid 4px <?= $paramsData["background_color"]; ?>;
	}

	.container<?php echo $kunik ?> .ariane-suggest-elt{
		font-size: 2rem;
		position: absolute;
		left:100px; top: 50%;
		transform: translate(0,-50%);
		padding: 20px;
		color: #fff;
		font-family: Lato-Italic;
	}
</style>
<div class="container<?php echo $kunik ?>" style="min-height: 780px;">
	<div class="row" style="display: flex;">
		<div class="col-md-6"></div>
		<div class="col-md-6">
			<div class="padding-right-20 padding-top-20 padding-bottom-20">
				<span style="text-align: right !important; font-size: <?= $paramsData["titleSize"]; ?>px; font-family: Lato-Italic; color: <?= $paramsData["titleColor"]; ?>" class="bold bg-blur sp-text" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]; ?></span>
			</div>
		</div>
	</div>
	<div style="padding-top: 50px!important;">
		<div class="ariane-suggest-container">
			<div class="rows">
				<div class="side">
					<div style="border: 5px solid #f9576d;border-radius: 100%;display: inline-table;padding: 20px; ">
						<?php if (!empty($latestImg)) { ?>
						<img style="width: <?php echo $paramsData["imageSize"]; ?>vw;height: <?php echo $paramsData["imageSize"]; ?>vw; border-radius: 100%;" src="<?php echo $latestImg ?>">
						<?php }else{ ?>							
						<?php } ?>
					</div>
					<div style="border: 4px solid <?= $paramsData["second_color"]; ?>;border-radius: 100%; width: 100px;height: 100px;bottom: 380px;left: 100px"></div>
				</div>
				<div class="main">
					<div class="ariane-suggest-body clarte">
						<div class="ariane-suggest-body-content">
							<div class="ariane-suggest-badge">
								<span class="text-suggest sp-text" data-id="<?= $blockKey ?>" data-field="sousTitre1"><?= $paramsData["sousTitre1"]; ?></span>
							</div>
							<div class="ariane-suggest-elt">
								<span class="sp-text" data-id="<?= $blockKey ?>" data-field="text1"><?= $Parsedown->text($paramsData["text1"]); ?></span>
							</div>
						</div>
					</div>
					<div class="ariane-suggest-body soutien">
						<div class="ariane-suggest-body-content">
							<div class="ariane-suggest-badge">
								<span class="text-suggest sp-text" data-id="<?= $blockKey ?>" data-field="sousTitre2"><?= $paramsData["sousTitre2"]; ?></span>
							</div>
							<div class="ariane-suggest-elt">
								<span class="sp-text" data-id="<?= $blockKey ?>" data-field="text2"><?= $Parsedown->text($paramsData["text2"]); ?></span>
							</div>
						</div>
					</div>
					<div class="ariane-suggest-body savoir-faire">
						<div class="ariane-suggest-body-content">
							<div class="ariane-suggest-badge">
								<span class="text-suggest sp-text" data-id="<?= $blockKey ?>" data-field="sousTitre3"><?= $paramsData["sousTitre3"]; ?></span>
							</div>
							<div class="ariane-suggest-elt">
								<span class="sp-text" data-id="<?= $blockKey ?>" data-field="text3"><?= $Parsedown->text($paramsData["text3"]); ?></span>
							</div>
						</div>
					</div>
					<div class="ariane-suggest-body reactivite">
						<div class="ariane-suggest-body-content">
							<div class="ariane-suggest-badge">
								<span class="text-suggest sp-text" data-id="<?= $blockKey ?>" data-field="sousTitre4"><?= $paramsData["sousTitre4"]; ?></span>
							</div>
							<div class="ariane-suggest-elt">
								<span class="sp-text" data-id="<?= $blockKey ?>" data-field="text4"><?= $Parsedown->text($paramsData["text4"]); ?></span>
							</div>
						</div>
					</div>
					<div class="ariane-suggest-body recul">
						<div class="ariane-suggest-body-content">
							<div class="ariane-suggest-badge">
								<span class="text-suggest sp-text" data-id="<?= $blockKey ?>" data-field="sousTitre5"><?= $paramsData["sousTitre5"]; ?></span>
							</div>
							<div class="ariane-suggest-elt">
								<span class="sp-text" data-id="<?= $blockKey ?>" data-field="text5"><?= $Parsedown->text($paramsData["text5"]); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
      		"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",            
            "properties" : {
              "background_color" : {
                "inputType" : "colorpicker",
                "label" : "<?php echo Yii::t('cms', 'Background color of the title')?>",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.background_color
              },
              "second_color" : {
                "inputType" : "colorpicker",
                "label" : "<?php echo Yii::t('cms', 'Second color')?>",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.second_color
              },
              "image" :{
              	"inputType" : "uploader",
              	"label" : "<?php echo Yii::t('cms', 'Image')?>",
              	"docType": "image",
				"contentKey" : "slider",
              	"itemLimit" : 1,
              	"endPoint": "/subKey/block",
              	"domElement" : "image",
              	"filetypes": ["jpeg", "jpg", "gif", "png"],
              	"label": "Image :",
              	"showUploadBtn": false,
              	initList : <?php echo json_encode($initImage) ?>
              }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
					  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>