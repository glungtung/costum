<?php
    $keyTpl ="imgLeftWithBtnRight";
    $paramsData = [
        "buttonRejoindre" => "Je rejoint le filière",
        "buttonRejoindreBg" => "#2C3E50",
        "buttonRejoindreColor" => "white",
        "buttonRejoindreRadius" => "50",
        "buttonForm"=>"Je rempli le formulaire membre",
        "buttonUpdate"=>"Je mis à jour mes données",
        "buttonProjet"=>"Je souhaite ajouter un projet",
        "buttonProjetBg" => "2C3E50",
        "buttonProjetColor" => "white",
        "buttonProjetRadius" => "50",
        "backgroundColor"=>"#97bf1e",
        "formId"=>"",
        "image"=> Yii::app()->getModule('costum')->assetsUrl."/images/smarterre/phototerritoire.jpg",
        "content" => "_Restons connectés pour faire avancer notre filière_",
        "positionText" => "bottom"
    ];

//    $childForm = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (  isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<!-- ****************get image uploaded************** -->
<?php

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
    $image = [];

    foreach ($initImage as $key => $value) {
        $image[]= $value["imagePath"];
    }

    if($image!=[]){
        $paramsData["image"] = $image[0];
    }

 ?>
<!-- ****************end get image uploaded************** -->

<?php

    # If user is admin
    #$is_admin = false;
    #$admins = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
    #if(count($admins)!=0){
    #    $is_admin = true;
    #}

    /*
    $members = array();
    $answers = array();
    if(isset(Yii::app()->session["userId"])){
        # If user has already joined
        $members = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"]=>['$exists' => true ], "source.keys"=>$costum['slug']));

        if($paramsData["formId"]!=""){
            $formId = $paramsData["formId"];
        }else{
            # Get the form id
            $form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
            $formId = "";
            foreach($form as $key => $value) { $formId = $key; }
        }
        # If user has submited the coform
        $answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));
    }
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    $has_answered = false;

    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if($_SESSION["userId"] && Yii::app()->session["userId"] == $value["user"]){
            $myAnswer = $value['_id']->{'$id'};
        }
    }

    if(isset(Yii::app()->session["userId"])){

        $data = PHDB::findByIds("citoyens", [Yii::app()->session["userId"]] ,["links.follows", "links.memberOf"]);

        $memberOf_ids = array();

        if(isset($data[Yii::app()->session["userId"]]["links"]["memberOf"])){
            $links2 = $data[Yii::app()->session["userId"]]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }*/
?>

<style>
    .join-<?= $kunik?> {
        background : <?php echo $paramsData["backgroundColor"] ?>;
        margin: 0 !important;
        margin-top : 2%;
        padding-right: 0px;

    }
    .join-<?= $kunik?> .col-md-6{
        padding-right: 0;
    }
    .join-btn-<?= $kunik?>{
        text-align : center;
        color : white;
        margin-top:<?=($paramsData["positionText"]=="top")?0:3 ?>%;
        margin-bottom:<?=($paramsData["positionText"]=="top")?4:0 ?>%;
    }
    .carto-p-m-<?= $kunik?>{
        font-size: 2.85vw;
        margin-top: 3%;
        margin-bottom:<?=($paramsData["positionText"]=="top")?0:3 ?>%;
        line-height: 3vw;
    }

    .carto-p-m-<?= $kunik?> p{
        font-size: 2.85vw;
        margin-top: 3%;
        line-height: 3vw;
    }

    .join-btn-<?= $kunik?> a{
        padding: .6em 1em;
    }

    .join-btn-<?= $kunik?> #joinBtn<?= $kunik ?>{
        border-radius: <?= $paramsData["buttonRejoindreRadius"] ?>px;
        background-color: <?= $paramsData["buttonRejoindreBg"] ?>;
        color: <?= $paramsData["buttonRejoindreColor"] ?>;
        border: 2px solid <?= $paramsData["buttonRejoindreColor"] ?>;
        font-size: 1.3em;
    }
    .join-btn-<?= $kunik?> #prjBtn<?= $kunik ?>{
        border-radius: <?= $paramsData["buttonProjetRadius"] ?>px;
        background-color: <?= $paramsData["buttonProjetBg"] ?>;
        color: <?= $paramsData["buttonProjetColor"] ?>;
        border: 2px solid <?= $paramsData["buttonProjetColor"] ?>;
        font-size: 1.3em;
    }
</style>

<div class="join-<?= $kunik?> carto-n-<?= $kunik?> row content-<?= $kunik?>">

    <div class="join-btn-<?= $kunik?> <?= ($paramsData['image']!='')?'col-md-6':'col-md-12' ?>  ">

        <br><br>

        <?php if($paramsData["positionText"]=="top"){ ?>
            <div class=" title carto-p-m-<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"><?php echo $paramsData["content"] ?></div>
            <br> <br>
        <?php } ?>

        <?php //if(!$is_member){ ?>

        <!-- Btn je rejoins -->
        <?php if($paramsData["buttonRejoindre"]!=""){ ?>
            <a id="joinBtn<?= $kunik ?>" class="btn btn-open-form btn-lg" href="javascript:;" data-form-type="organization" style="text-decoration : none;">
                <?php echo $paramsData["buttonRejoindre"] ?>
            </a>

            <!--a href="javascript:;" data-form-type="organization" class="addBtnFoot btn btn-default  btn-open-form bg-green"><i class="fa fa-group "></i> <span>organisation</span></a-->
        <?php } ?>

        <br> <br>

        <!-- Btn je propose -->
        <?php if($paramsData["buttonProjet"]!=""){ ?>
            <a id="prjBtn<?= $kunik ?>" class="btn btn-lg btn-open-form" href="javascript:;" data-form-type="project" style="text-decoration : none">
                <?php echo $paramsData["buttonProjet"] ?>
            </a>
        <?php } ?>

        <?php if($paramsData["positionText"]=="bottom"){ ?>
            <div class=" title carto-p-m-<?= $kunik?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content"><?php echo $paramsData["content"] ?></div>
        <?php } ?>
    </div>
    <?php if($paramsData["image"]!=""){ ?>
        <div class="join-img col-md-6 ">
            <img src="<?php echo $paramsData["image"] ?>" width="100%" class="img-responsive">
        </div>
    <?php } ?>
</div>
<?php

    // $typeObj = PHDB::findOne("organizations", array("slug"=>$costum["slug"]));
    // if(isset($typeObj["costum"]["typeObj"])){
    //     $costum["typeObj"] = $typeObj["costum"]["typeObj"];
    // }

if(isset($_SESSION["userId"])){ ?>
    <script type="text/javascript">

        //costum["typeObj"] = <?php echo json_encode($costum["typeObj"]) ?>;

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        var specificInputs = {};

        jQuery(document).ready(function(){
        /***********Hide & show field when Other option in select chosen (RIES)***********/
           /* $("#joinBtn<?= $kunik ?>").click(function(){
                setTimeout(function() {
                    $('.addListLineBtnextraInfo-certifications').text(trad.addrow);
                    if (contextName == "RIES") {
                        $('#btn-submit-form').attr("disabled", true);
                    }

                    $('.extraInfo-agreecheckboxSimple').click(function(){
                        if ($('#extraInfo-agree').val() == "true")
                            $('#btn-submit-form').attr("disabled", false);
                        else
                            $('#btn-submit-form').attr("disabled", true);
                    });

                    $(".hidshow option").each(function()
                    {
                        $("."+$(this).val()+"selectMultiple").hide();
                    });

                    let opened = "";
                    $(".hidshow").change(function(){
                        $("."+opened+"selectMultiple").hide()
                        opened = $(this).val();
                        $("."+opened+"selectMultiple").show()
                    });

                    $(".groupselect").hide();
                    $("#type").change(function(){
                        if ($("#type").val() == "Group") {
                            $(".groupselect").show()
                        }else{
                            $(".groupselect").hide()
                        }
                    });
                }, 800);
            });*/
        /***********End Hide & show field when other option in select chosen (RIES)***********/

            sectionDyf.<?php echo $kunik ?>Params = {
              "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "backgroundColor" : {
                        "inputType" : "colorpicker",
                        "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.backgroundColor
                    },
                    "positionText":{
                        "inputType" : "select",
                        "markdown" : true,
                        "label" : "<?php echo Yii::t('cms', 'Text position')?>",
                        "options" : {
                            "top" : "<?php echo Yii::t('cms', 'At the top of the buttons')?>",
                            "bottom" : "<?php echo Yii::t('cms', 'At the bottom of the buttons')?>"
                        },
                        value : sectionDyf.<?php echo $kunik ?>ParamsData.positionText
                    },
                    "buttonRejoindre" : {
                        "inputType" : "text",
                        "label" : "<?php echo Yii::t('cms', 'Text')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonRejoindre
                    },
                    "buttonRejoindreBg" : {
                        "inputType" : "colorpicker",
                        "label" : "<?php echo Yii::t('cms', 'Button color')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonRejoindreBg
                    },
                    "buttonRejoindreColor" : {
                        "inputType" : "colorpicker",
                        "label" : "<?php echo Yii::t('cms', 'Text color')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonRejoindreColor
                    },
                    "buttonRejoindreRadius" : {
                        "inputType" : "text",
                        "rules" : {
                            "number":true
                        },
                        "label" : "<?php echo Yii::t('cms', 'Border radius')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonRejoindreRadius
                    },
                    "buttonProjet" : {
                        "inputType" : "text",
                        "label" : "<?php echo Yii::t('cms', 'Text')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonProjet
                    },
                    "buttonProjetBg" : {
                        "inputType" : "colorpicker",
                        "label" : "<?php echo Yii::t('cms', 'Button color')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonProjetBg
                    },
                    "buttonProjetColor" : {
                        "inputType" : "colorpicker",
                        "label" : "<?php echo Yii::t('cms', 'Text color')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonProjetColor
                    },
                    "buttonProjetRadius" : {
                        "inputType" : "text",
                        "rules" : {
                            "number":true
                        },
                        "label" : "<?php echo Yii::t('cms', 'Border radius')?>",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonProjetRadius
                    },
                    /*
                    "buttonForm" : {
                        "inputType" : "text",
                        "label" : "Texte du bouton pour remplir le formulaire",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonForm
                    },
                    "buttonUpdate" : {
                        "inputType" : "text",
                        "label" : "Texte du bouton pour mettre à jour le formulaire",
                        value :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonUpdate
                    },
                    "formId": {
                        "label" : "Formulaire pour membre",
                        "class" : "form-control",
                        "inputType" : "select",
                        "options": {
                            <?php
                                /*foreach($childForm as $key => $value) {
                                    echo  '"'.$key.'" : "'.$value["name"].'",';
                                } */
                            ?>
                        }
                    },*/
                    "image" :{
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'Image')?>",
                        "docType": "image",
                        "contentKey" : "slider",
                        "itemLimit" : 1,
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        "showUploadBtn": false,
                        "domElement" : "image",
                        "endPoint" :"/subKey/image",
                        initList : <?php echo json_encode($initImage) ?>
                    }
                },
               beforeBuild : function(){
                  uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {
                  tplCtx.value = {};
                  $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent")
                      tplCtx.value[k] = formData.parent;

                    if(k == "items")
                      tplCtx.value[k] = data.items;
                  });

                  if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                  else {
                       dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                         toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                          $("#ajax-modal").modal('hide');
                          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                          //urlCtrl.loadByHash(location.hash);
                        });
                      });
                    }
                }
            }
        }

            $("#btnOk").click(function(){
                var test = new Array();
                $("input[name='organization']:checked").each(function() {
                    test.push($(this).val());
                });
            });

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
              tplCtx.id = $(this).data("id");
              tplCtx.collection = $(this).data("collection");
              tplCtx.path = "allToRoot";
              dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
              alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"buttonRejoindre",4,6,null,null,"<?php echo Yii::t('cms', 'Organization creation button')?>","#4AB5A1","");
              alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"buttonProjet",4,6,null,null,"<?php echo Yii::t('cms', 'Project creation button')?>","#4AB5A1","");
            });

            $("#joinBtn<?= $kunik ?>, #prjBtn<?= $kunik ?>").off().click(function(){
                if($(this).data("form-type")){

                    if($(this).data("form-type")=="project" || $(this).data("form-type")=="organization"){
                        costum.searchExist = function (type,id,name,slug,email){
                            var sourceKeys = [];

                            var params = {
                                "type" :type,
                                "id": id,
                                "slug": slug
                            };

                            ajaxPost(
                                null,
                                 '<?= Yii::app()->baseUrl; ?>/costum/filiere/get-sourcekeys',
                                params,
                                function(data){
                                    if(typeof data.creator != undefined && data.creator==="<?php echo $_SESSION['userId'] ?>"){
                                        if(typeof data.source != "undefined" && typeof data.source.keys != "undefined"){
                                            sourceKeys = data.source.keys;
                                            sourceKeys.push("<?php echo $costum["slug"]; ?>")
                                        }else{
                                            sourceKeys = ["<?php echo $costum["slug"]; ?>"];
                                        }

                                        /**if(costum.typeObj.organization.dynFormCostum && costum.typeObj.organization.dynFormCostum.beforeBuild.properties){
                                            var specProperties = costum.typeObj.organization.dynFormCostum.beforeBuild.properties;
                                            for (const [keyT, valueT] of Object.entries(specProperties)) {
                                                specificInputs[""+keyT]=$("#"+keyT).val();
                                            }
                                        }*/

                                        specificInputs["source"] = {
                                                    "keys" : sourceKeys,
                                                };

                                        specificInputs["reference"]={
                                                    costum : sourceKeys
                                                }

                                        var data = {
                                            collection : type,
                                            id : id,
                                            type: type,
                                            path : "allToRoot",
                                            value : specificInputs
                                        }

                                        if(type == "projects"){
                                            //costumInputs.elementproject.afterSave(data);
                                            delete data.value.reference;

                                            data.typeStruct = $("#ajaxFormModal #typeStruct").val();

                                            dataHelper.path2Value( data, function(params) {
                                                if(dyFObj.closeForm()){
                                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                                    //urlCtrl.loadByHash(location.hash);
                                                }else{
                                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                                    // urlCtrl.loadByHash(location.hash);
                                                }
                                            });
                                        }else{
                                            data.typeStruct = $("#ajaxFormModal #typeStruct").val();
                                            dataHelper.path2Value( data, function(params) {
                                                if(dyFObj.closeForm()){
                                                    $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");
                                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                                    // urlCtrl.loadByHash(location.hash);
                                                }else{
                                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                                    // urlCtrl.loadByHash(location.hash);
                                                }
                                            });
                                        }
                                    }else{
                                        toastr.error("Erreur de permission, Vous n'etes pas createur de l'organisation en question");
                                    }

                                },
                                function(xhr,textStatus,errorThrown,data){
                                    toastr.error("Une erreur s'est produite, veuillez signaler ce problème à notre administrateur");
                                }
                            );
                        }
                    }

                    dyFObj.openForm(
                        $(this).data("form-type"),
                        null,
                        {},
                        null,
                        {
                            afterSave : function(orga){
                                let referenceData = {
                                    "source":orga.map.source
                                };

                                referenceData.source.keys = referenceData.source.keys.concat(contextData.source.keys);

                                var data = {
                                    "elementSlug":orga.map.slug,
                                    "elementType":orga.map.collection,
                                    ...referenceData};

                                ajaxPost(
                                    null,
                                    '<?= Yii::app()->baseUrl; ?>/costum/filiere/populate',
                                    data,
                                    function(data){
                                        window.location.reload()
                                    },
                                    function(error){
                                        toastr.error("Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur")
                                    },
                                    "json"
                                )
                            }
                        }
                    );
                }
            });
        });
    </script>
<?php } ?>
