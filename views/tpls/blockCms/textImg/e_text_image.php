<?php 
  $keyTpl ="e_text_image";
    $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  $paramsData = [ 
    "title" => "Titre",
    "subTitle" => "SOUS TITRE",
    "description" => $loremIpsum,
    "btnBgColor" => "#008037",
    "btnColor" => "#ffffff",
    "btnLabel" => "EN SAVOIR PLUS",
    "btnUrl" => "#about",
    "btnReadMore" => false,
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
 <!-- ****************get image uploaded************** -->
<?php 
  $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey" => 'imageLeft'
    ),"image"
  );
  if(isset($initImage[0])){
    $images = $initImage[0];
    $imgUrl ="";
    if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
      $imgUrl = $baseUrl.$images["imagePath"];
    elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
      $imgUrl = $baseUrl.$images["imageMediumPath"];
    
  }

 ?>

<!-- ****************end get image uploaded************** -->
<style>
  .sub-container-right<?=$kunik ?>{
      background-size: cover;
      background-position: center;
      /*height: 500px;*/
      background:rgb(255,255,255,0.7);
  }
  .sub-container-right<?=$kunik ?> img{
    position: absolute;
    top: 50%;
    left:50%;
    transform: translate(-50%,-50%);
    height: 50%;
  }
  .sub-container-left<?= $kunik ?>{
        /*background-image:url('<?=  $assetsUrl ?>/apropos_illu-02.png') !important;*/
    /*height: 125px; */     
    background-size: cover;
      background-position: center;
      /*height: 500px; */
  }
  .sub-container-left<?= $kunik ?> img{
      height: 577px;
      position: relative;
      left: -154px; 
  }

    .block<?= $kunik?> .btn-contactez-nous{
      color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?> !important;
      background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?> !important;
      font-weight: bold;
      margin: 0px 10px 38px 29px;
      padding: 14px 36px;
      border-radius: 12px;
      font-size: large;
  }
  .block<?= $kunik?> .btn-contactez-nous:hover{
      background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
    letter-spacing: 1px;
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
    transition: all 0.4s ease 0s;
  }
  .block<?= $kunik?> .btn-contactez-nous{
    margin:0px;
  }
  @media (max-width: 360px){
     .sub-container-right<?= $kunik?> .btn-savoir-plus{
        width:100%;
        display: block;
      }
  }
  
  .block<?= $kunik?>  .hr-title-apropos{
      display: inline-block;
      width: 50%;
      height: 5px;
      background-color:<?php echo @$blockCms["blockCmsColorSubtitle"] ?>
  }
  .sub-container-right<?= $kunik?> .btn-savoir-plus:hover{
      background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
      color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?>;
    letter-spacing: 1px;
    -webkit-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    -moz-box-shadow: 0px 5px 40px -10px rgba(0,0,0,0.57);
    box-shadow: 5px 40px -10px rgba(0,0,0,0.57);
    transition: all 0.4s ease 0s;
  }
.sub-container-right<?= $kunik?> .btn-savoir-plus{
      color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?>;
      background-color:  <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
      font-weight: bold;
      margin: 44px 0px 38px 0px;
      padding: 14px 36px;
      border-radius: 12px;
      font-size: large;
  }
    .block<?= $kunik?> .description{
      margin-top: 26px;
      line-height: 1.6;
    }
  @media (max-width: 992px){
    /*.block-container-<?= $kunik?> .description{
      position: relative;
      background: rgb(255,255,255,0.9);
      width: 100%;
      margin-top: 26px;*/
    }
    .sub-container-right<?=$kunik ?>{
      /*background:rgb(255,255,255,0.9);*/
    }
    @media (max-width: 978px){
      .sub-container-left<?= $kunik ?> img {
          height: 250px !important;
          left: 0;
      }
    }
</style>
<h1 class="title sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"] ?></h1>
<div class="col-md-5 col-xs-12 text-center  sub-container-left<?= $kunik ?> block<?= $kunik?>">

      <?php if(!empty($initImage)){ 
?>
        <img class="lzy_img" src="<?=  $imgUrl ?>">
      <?php }else{ 
        ?>
            <img  class="lzy_img" src="<?=  $assetsUrl ?>/apropos_illu-02.png" alt="">
      <?php }?>
</div>
<div class="col-md-7 col-xs-12 sub-container-right<?= $kunik ?> block<?= $kunik?>">
    <h2 class=" title-apropos subtitle sp-text" data-id="<?= $blockKey ?>" data-field="subTitle"><?php echo $paramsData["subTitle"] ?></h2>
    <hr class="title hr-title-apropos ChangoRegular">
    <div class=" description more<?php echo $kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="description"><?= $paramsData["description"] ?></div>
    <a href="<?php echo $paramsData["btnUrl"] ?>" class="lbh btn btn-savoir-plus"><?php echo $paramsData["btnLabel"] ?> 
    </a>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
      var urlOptions = {}
      if (notNull(costum) && exists(costum.app))
        $.each(costum.app,function(k,v){
            urlOptions[k] = exists(v.subdomainName) ? v.subdomainName : "";
        })
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
                "btnLabel" : {
                    "inputType" : "<?php echo Yii::t('cms', 'Button text')?>",
                    "label" : "Label du boutton",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button label color')?>",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnColor
                },
                "btnBgColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button background color')?>",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "btnUrl" : {
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Button link')?>",
                    "options" : urlOptions,
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.btnUrl
                },
                "imageLeft" :{
                  "inputType" : "uploader",
                  "label" : "<?php echo Yii::t('cms', 'Left image')?>",
                  "docType": "image",
                  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/imageLeft",
                  "domElement" : "imageLeft",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImage) ?>
                },
                "btnReadMore":{  
                  "inputType" : "checkboxSimple",
                  "label" : "<?php echo Yii::t('cms', 'See more')?>",
                  "params" : checkboxSimpleParams,
                  "checked" : <?= json_encode($paramsData["btnReadMore"]) ?> 
                }        
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      //urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        <?php if($paramsData["btnReadMore"]==true){ ?>
          setTimeout(() => {
            $(".more<?php echo $kunik ?>").myOwnLineShowMoreLess({
            showLessLine: 10,
            showLessText:'Lire Moins',
            showMoreText:'Lire plus',
            //lessAtInitial:false,
            //showLessAfterMore:false
            });
          }, 900);
        <?php } ?>
    });
</script>