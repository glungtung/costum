<?php
$keyTpl = "textLeft";

$paramsData = [
                "title" =>  "block avec une image",
                "color" => "#000000",
                "background" => "#FFFFFF",
                "txtcolor"  =>  "#000000"
                ];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (    isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

$tags = (@$tags && !empty($tags)) ? $tags : "txtLeft";
$page = (@$page && !empty($page)) ? $page : "";
$cmsTpl = PHDB::findOne(Cms::COLLECTION ,  [
                                            "structags"     => $tags.@$page,
                                            "source.key"    => $costum["slug"]
                                        ]);
?>
<h1 class="text-center" style="color:<?= $paramsData["color"] ?>;background-color: <?= $paramsData["background"] ?>">
    <?= @$params["icon"]." ".$paramsData["title"]; ?>      
</h1>
<?php
	if(@$cmsTpl):
		echo '<div style="margin-top:2vw;" class="no-padding col-xs-12 col-sm-12 col-lg-12">';
            $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $cmsTpl["_id"]));

            echo'<div class="col-md-6 text-center">';
      			echo '<h1 style="color:black;">'.@$cmsTpl["name"].'</h1>';
      			echo '<span style="color:black !important;text-align:left;" class="markdown">'.@$cmsTpl["description"].'</span>';
  				if (@$cmsTpl["media"]){ 
  					echo '<iframe style="margin-top:2%;" width="400" height="300" src="'.@$cmsTpl["media"].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
  				}
    		echo '</div>';
			
            if (isset($img)) 
			{
				echo '<div class="col-md-6 no-padding">';
                	echo '<img src="'.Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"].'" class="img-responsive">';
               	echo '</div>';
			}
		$edit = "update";
	else:
		echo '<div style="margin-top:2vw;" class="no-padding col-xs-12 col-sm-12 col-lg-12">';
			echo '<p class="text-center"> Ajouter un texte et une image a ce block </p>';
		$edit = "create";			
	endif;

	echo '<center>'; 
	echo '<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 editBlock">';
	echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
  	echo '</div>';
	echo '</center>';
	echo "</div>";

?>

<script type="text/javascript">
page = <?= json_encode(@$page); ?> ;

parent = <?= json_encode($cmsTpl["parent"]); ?>;
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
page = (typeof page != "undefined") ? page : "";
type = 'tpls.blockCms.textImg.<?= $keyTpl ?>';

sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

jQuery(document).ready(function(){

		mylog.log("---------- Render blockimg","costum.views.tpls.blockwithimg");

        sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section bloc un texte et une image",
            "description" : "Personnaliser votre section sur les blocs d'un texte et une image",
            "icon" : "fa-cog",
            "properties" : {
                title : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                color : {
                    label : "Couleur du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color

                },
                background : {
                    label : "Couleur du fond du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                txtcolor : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.txtcolor

                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
            save : function () {  
                tplCtx.value = {};

                if (typeof idToPath != "undefined") {
                    tplCtx.value["id"] = idToPath;
                }

                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value["parent"] = formData.parent ;
                    }
                });

                tplCtx.value["page"] = '<?= @$page ?>';
                tplCtx.value["type"] = 'tpls.blockCms.textImg.<?= $keyTpl ?>';
                
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        toastr.success("élément mis à jour");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = (typeof $(this).data("path") != "undefined" && $(this).data("path") != "") ? $(this).data("path") : "allToRoot";
        idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});

	$(".editThisBtn").off().on("click",function (){
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS);
    });

    $(".createBlockBtn").off().on("click",function (){
        dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS);
    });

    $(".deleteThisBtn").off().on("click",function (){
        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
        var btnClick = $(this);
        var id = $(this).data("id");
        var type = $(this).data("type");
        var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
        bootbox.confirm(trad.areyousuretodelete,
            function(result) {
                if (!result) {
                    btnClick.empty().html('<i class="fa fa-trash"></i>');
                    return;
                } 
                else {
                $.ajax({
                    type: "POST",
                    url: urlToSend,
                    dataType : "json"
                })
                .done(function (data) {
                    if ( data && data.result ) {
                      toastr.success("élément effacé");
                      $("#"+type+id).remove();
                    } else {
                       toastr.error("something went wrong!! please try again.");
                    }
                });
            }
        });
    });
</script>