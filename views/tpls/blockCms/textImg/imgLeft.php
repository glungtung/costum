<?php
  $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  $keyTpl ="imgLeft";
  $paramsData = [  
    "title"=>"Art",
    "description" =>$loremIpsum,
    "imagePosition" => "left",
    "borderColor" => "#008037",
    "btnReadMore" => false,
    "buttonShow" => false,
    "buttonUrl" => "",
    "buttonLabel" => "",
    "buttonBackground" => "#008037",
    "buttonfontSize" => ""
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
 ?>
 
<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
  $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
  $initFiles = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"notBackground"
    ), "image"
  );
//$arrayImg = [];
$imgUrl = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
if(isset($initFiles[0])){
  $images = $initFiles[0];
  if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
      $imgUrl = $baseUrl.$images["imagePath"];
  elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
      $imgUrl = $baseUrl.$images["imageMediumPath"];
}
//var_dump($baseUrl.$images["imageMediumPath"]);
//var_dump($initFiles);
 ?>
<!-- ****************end get image uploaded************** -->

<style>
  .container<?php echo $kunik ?>{
    /*box-shadow: 0 0px 20px rgba(0, 0, 0, 0.2);*/
    /*background-image: url("<?php  //echo Yii::app()->getModule('costum')->assetsUrl?>/images/eywa/background.png");
    text-align: left; 
    /*margin-top: 10px;*/
    /*margin-bottom: 40px;*/ 
    padding-bottom: 38px;
  }
</style>

<style>
.container<?php echo $kunik ?> .main-title{
  color: #2d2d2d;
  text-align: center;
  text-transform: capitalize;
  padding: 0.7em 0;
}



.container<?php echo $kunik ?> .content {
  position: relative;
  width: 90%;
  max-width: 700px;
  margin: auto;
  overflow: hidden;
  border-radius: 10px;
  margin-top: 38px;
}

.container<?php echo $kunik ?> .content .content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 99%;
  width: 100%;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s;
}

.container<?php echo $kunik ?>  .content:hover .content-overlay{
  opacity: 1;
}

.container<?php echo $kunik ?> .content-image{
  width: 100%;
}

.container<?php echo $kunik ?> .content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
}

.container<?php echo $kunik ?> .content:hover .content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}

.container<?php echo $kunik ?> .content-details h3{
  color: #fff;
  font-weight: 500;
  letter-spacing: 0.15em;
  margin-bottom: 0.5em;
  text-transform: uppercase;
}

.container<?php echo $kunik ?> .content-details p{
  color: #fff;
  font-size: 0.8em;
}

.container<?php echo $kunik ?> .fadeIn-bottom{
  top: 80%;
}

.container<?php echo $kunik ?> .fadeIn-top{
  top: 20%;
}

.container<?php echo $kunik ?> .fadeIn-left{
  left: 20%;
}

.container<?php echo $kunik ?> .fadeIn-right{
  left: 80%;
}
.container<?php echo $kunik ?> .description{
  text-align: justify;
}
.container<?php echo $kunik ?> .description::first-letter{
  font-size: 30px;
  color: <?php echo $paramsData["borderColor"] ?>;
  text-transform: capitalize !important; 

}
.container<?php echo $kunik ?> div[class^="illustration"]{
  position: absolute;
  background-color: <?php echo $paramsData["borderColor"] ?>;
    -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
}

.container<?php echo $kunik ?> .illustration-left-1 {
    width: 5px;
    height: 80px;
    top: 0;
    left: 0;
}
.container<?php echo $kunik ?>:hover .illustration-left-1{
  -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
  height: 100%;
}

.container<?php echo $kunik ?> .illustration-left-2 {
    width: 50%;
    height: 5px;
    top: 0;
    left: 0;
}
.container<?php echo $kunik ?>:hover .illustration-left-2{
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
  width: 100%;
}


.container<?php echo $kunik ?> .illustration-right-1 {
    width: 5px;
    height: 80px;
    bottom: 0;
    right: 0;
}
.container<?php echo $kunik ?>:hover .illustration-right-1{
  -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
  height: 100%;
}

.container<?php echo $kunik ?> .illustration-right-2 {
    width: 50%;
    height: 5px;
    bottom: 0;
    right: 0;
}
.container<?php echo $kunik ?>:hover .illustration-right-2{
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
  width: 100%;
}


.container<?php echo $kunik ?> .btn-edit-delete{
  display: none;
}
.container<?php echo $kunik ?>:hover .btn-edit-delete{
  display: block;
  -webkit-transition: all 0.9s ease-in-out 9s;
  -moz-transition: all 0.9s ease-in-out 9s;
  transition: all 0.9s ease-in-out 0.9s;
  position: absolute;
  top:50%;
  left: 50%;
  transform: translate(-50%,-50%);
}

.container<?php echo $kunik ?> .button-link{
  display: block;
  width: fit-content;
  text-decoration: none;
  padding: 10px 20px;
  background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonBackground"]; ?>;
  font-size: <?= $paramsData["buttonfontSize"]?>px;
  margin-top: 20px;
  color: white;
  font-weight: bold;
  border-radius: 4px;
  transition: .3s;
}

.container<?php echo $kunik ?> .button-link:hover{
  text-decoration: none;
  color: white;
}

#filters-nav {
  display:none !important;
}
@media (max-width: 567px){
  .container<?php echo $kunik ?>,.content<?php echo $kunik ?>{
    padding-right: 0;
    padding-left: 0;
  }
}
@media (max-width: 992px){
  .container<?php echo $kunik ?> .title-xs{
    display: block
  }
  .container<?php echo $kunik ?> .title{
    display: none
  }
}

@media (min-width: 993px){
  .container<?php echo $kunik ?> .title-xs{
    display: none !important;
  }
  .container<?php echo $kunik ?> .title{
    display: block
  }
} 
</style>
<div class="container<?php echo $kunik ?> col-md-12 col-xs-12">
  <div class="illustration-left-1"></div>
  <div class="illustration-left-2"></div>
<h3 class="title-xs title title-color ChangoRegular sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h3>
    <?php if($paramsData["imagePosition"]=="right"){ ?>
  <div class="col-md-6 col-xs-12">
    <h3 class=" title-color title ChangoRegular text-center sp-text img-text-bloc  text-center hidden-xs" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h3>
    <div class="description text-color more<?php echo $kunik ?> sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="description"><?php echo $paramsData["description"] ?></div>
    <?php if($paramsData["buttonShow"]){ ?>
      <a href="<?= $paramsData["buttonUrl"] ?>" class="button-link lbh"><?= $paramsData["buttonLabel"] ?></a>
    <?php } ?>
  </div>
  <?php } ?>

  <div class="content<?php echo $kunik ?> col-md-6 col-xs-12">
    <div class="content">
      <a href="javascript:;">
            <img class="content-image lzy_img" src="<?php echo Yii::app()->controller->module->assetsUrl ?>/images/thumbnail-default.jpg" data-src="<?= $imgUrl ?>" alt="">
            <!-- <img class="content-image" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$imgUrl ?>" data-src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$imgUrl ?>" alt="">  -->
      </a>
    </div>
  </div>

  <?php if($paramsData["imagePosition"]=="left"){ ?>
  <div class="col-md-6 col-xs-12">
    <h3 class=" title title-color ChangoRegular sp-text img-text-bloc  text-center hidden-xs" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h3>
    <div class="description text-color more<?php echo $kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="description"><?php echo $paramsData["description"] ?></div>
    <?php if($paramsData["buttonShow"]){ ?>
      <a href="<?= $paramsData["buttonUrl"] ?> "class="button-link lbh"><?= $paramsData["buttonLabel"] ?></a>
    <?php } ?>
  </div>
  <?php } ?>

  <div class="illustration-right-1"></div>
  <div class="illustration-right-2"></div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
				    "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		     	  "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
                "borderColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Border color')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.borderColor
                },
                "btnReadMore":{  
                  "inputType" : "checkboxSimple",
                  "label" : "<?php echo Yii::t('cms', 'See more')?>",
                  "params" : checkboxSimpleParams,
                  "checked" : <?= json_encode($paramsData["btnReadMore"]) ?> 
                },
                "image" :{
                  "inputType" : "uploader",
                    "label" : "<?php echo Yii::t('cms', 'Image')?>",
                    "docType" : "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "endPoint" :"/subKey/notBackground",
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    initList : <?php echo json_encode($initFiles) ?>
                },
                "imagePosition" :{
                  "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Align image')?>",
                    "options":{
                      "left":"<?php echo Yii::t('cms', 'Left')?>",
                      "right":"<?php echo Yii::t('cms', 'Right')?>"
                    }
                },
                "buttonShow":{
                  "label": "<?php echo Yii::t('cms', 'Show the button')?>",
                  "inputType" : "checkboxSimple",
                  "params" : checkboxSimpleParams,
                  "checked" : <?= json_encode($paramsData["buttonShow"]) ?>
                },
                "buttonUrl":{
                  "inputType" : "text",
                  "label": "<?php echo Yii::t('cms', 'Link')?>",
                  "values": sectionDyf.<?php echo $kunik ?>ParamsData.buttonUrl
                },
                "buttonLabel":{
                  "inputType" : "text",
                  "label": "<?php echo Yii::t('cms', 'Label')?>",
                  "values": sectionDyf.<?php echo $kunik ?>ParamsData.buttonLabel
                },
                "buttonBackground" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button colo')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonBackground
                },
                "buttonfontSize" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Button text size (in px)')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonfontSize,
                    rules:{
                      number:true
                    }

                },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      //urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          tplCtx.format = true;
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"image",6,6,null,null,"<?php echo Yii::t('cms', 'Image property')?>","green","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"button",4,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","green","");
        });

        <?php if($paramsData["btnReadMore"]==true){ ?>
          setTimeout(() => {
            $(".more<?php echo $kunik ?>").myOwnLineShowMoreLess({
            showLessLine: 10,
            showLessText:'Lire Moins',
            showMoreText:'Lire plus',
            //lessAtInitial:false,
            //showLessAfterMore:false
            });
          }, 900);
        <?php } ?>
    });
</script>
