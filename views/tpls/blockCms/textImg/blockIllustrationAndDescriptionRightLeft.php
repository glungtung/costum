<?php 
	$loremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
	$keyTpl ="blockIllustrationAndDescriptionRightLeft";
	$paramsData = [ 
	  "title"=>"Lorem Ipsum",
	  "grandIconPLace" =>"right",
	  "items" => $loremIpsum,
	  "btnReadMore" => false,
	  "btnReadMoreLineNUmber" => 15,
	  "flip" => "no",  
	];

	if (isset($blockCms)) {
	  foreach ($paramsData as $e => $v) {
	    if (  isset($blockCms[$e]) ) {
	      $paramsData[$e] = $blockCms[$e];
	    }
	  }
	} 
 
 ?>
<!-- ****************get image uploaded************** -->
<?php 
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";
  $initFiles = Document::getListDocumentsWhere(
  	array(
	    "id"=> $blockKey,
	    "type"=>'cms',
	    "subKey"=>"illustration"
  	), "image"
  );
$arrayImg = [];
foreach ($initFiles as $key => $value) {
	$arrayImg[]= $value["imagePath"];
}
 ?>

<!-- ****************end get image uploaded************** -->
<style>
	.container<?php echo $kunik ?>{
		min-height: 400px !important;
	}
	.list<?php echo $kunik ?> {
	    list-style: none;
	    margin: 0;
	    <?php if ($paramsData["grandIconPLace"] == "left") { ?>
		padding: 0;
		<?php }else{ ?>
		padding: 0;
		<?php } ?>
		/*padding-top: 166px;*/
	    z-index: 99;
	}
	/*.list<?php echo $kunik ?> p {
	    position: relative;
	    display: inline;
	    padding: 5px 0;
	    border: 0px solid;
	    /*background: RGBA(255, 255, 255, 0.72);*/
	    word-wrap: break-word;
	    /*border-radius: 50%;*/
	    /*margin: 0 15px 0px 0px;*/
	    /*box-shadow: -5px 5px 10px -5px rgba(0, 0, 0, 0.4);*/
	}*/
	.list<?php echo $kunik ?> li {
		margin-bottom: 20px;
	}
	.list<?php echo $kunik ?> li p::first-letter { 
		text-transform: capitalize !important;
	}


	<?php if ($paramsData["grandIconPLace"] == "left") { ?>
		.container<?= $kunik ?> [class*='col-'] {
	        padding-left:0 !important;
	    }
	<?php }else{ ?>
		.container<?= $kunik ?> [class*='col-'] {
	        padding-right: 0 !important ;
	    }
	<?php } ?>

	.container<?php echo $kunik ?> .div-icon-right{
		overflow-y: visible;
		position: relative;
		height: 760px;
		<?php echo $paramsData["flip"] == "yes" ? "-webkit-transform: scaleX(-1);" : ""; ?>
	}
		#filters-nav {
		display:none !important;
	}

@media (max-width :992px){
	.bg<?php echo $kunik ?>{
		margin-top:0;
	}
}
@media (max-width:567px){
	.list<?php echo $kunik ?> {
		padding: 0;
	}
	.bg<?php echo $kunik ?>{
		<?php if ($paramsData["grandIconPLace"] == "left") { ?>
		    margin-left: 0;
		<?php }else{ ?>
			margin-right: 0;
		<?php } ?>
	}
	.container<?php echo $kunik ?>{
		padding-left: 0;
    	padding-right: 0;
	}
}
</style>
<div class="container<?php echo $kunik ?> col-md-12">
<!-- 	<img class="img-responsive bg<?php //echo $kunik ?>" 
		 src="<?php // echo Yii::app()->getModule('costum')->assetsUrl?>/images/<?php //echo $costum["contextSlug"] ?>/bg_header.png"> -->
	<?php if (count($arrayImg) !=0){ ?>
		
	<?php }else{ ?>
		<div class="placeholder<?php echo $kunik ?>"></div>
	<?php } ?>

	

	<?php if ($paramsData["grandIconPLace"] == "left" && count($arrayImg) !=0) { ?>
		<div class="col-md-6">
			<div class="div-icon-right" style="background: url('<?php echo $arrayImg[0] ?>') no-repeat;background-size: cover;"></div>
		</div>
	<?php } ?> 

	<?php if ($paramsData["grandIconPLace"] == "left" && count($arrayImg) == null) { ?>
		<div class="col-md-6">
			<div class="div-icon-right" style="background: url('<?= $assetsUrl ?>/mains-02.png') no-repeat;background-size: cover;"></div>
		</div>
	<?php } ?>


	<div class="list<?php echo $kunik ?> col-md-6">
		<h2 class=" title title-color  sp-text" data-id="<?= $blockKey ?>" data-field="title" ><?php echo $paramsData["title"] ?></h2>
		<div class="description text-color   more<?php echo $kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="items"><?php echo $paramsData["items"] ?></div>
	</div>

	<?php if ($paramsData["grandIconPLace"] == "right" && count($arrayImg) !=0) { ?>
		<div class="col-md-6">
			<div class="div-icon-right" style="background: url('<?php echo $arrayImg[0] ?>') no-repeat left /cover;"></div>
		</div>
	<?php } ?> 

	<?php if ($paramsData["grandIconPLace"] == "right" && count($arrayImg) ==null) { ?>
		<div class="col-md-6">
			<div class="div-icon-right" style="background: url('<?= $assetsUrl ?>/mains-02.png') no-repeat left /cover;"></div>
		</div>
	<?php } ?>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
			"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            "properties" : {
		        "grandIconPLace" :{
		        	"inputType" : "select",
		            "label" : "<?php echo Yii::t('cms', 'Align the illustration')?> : ",
		            "options":{
		            	"left":"à gauche",
		            	"right":"à droite"
		            },
		            values :  sectionDyf.<?php echo $kunik ?>ParamsData.grandIconPLace
		        },
		       	"illustration" :{
                  "label" : "<?php echo Yii::t('cms', 'Illustration')?>",
                  "inputType":"uploader",
                  "docType": "image",
				  "contentKey" : "slider",
                  "itemLimit" : 1,
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "showUploadBtn": false,
                  "endPoint":"/subKey/illustration",
	              initList : <?php echo json_encode($initFiles) ?>
		        },
              	"flip" :{
		        	"inputType" : "select",
		            "label" : "<?php echo Yii::t('cms', 'Flip the image')?> : ",
		            "options":{
		            	"yes":"<?php echo Yii::t('cms', 'Return')?>",
		            	"no":"<?php echo Yii::t('cms', 'Do not return')?>"
		            },
		            values :  sectionDyf.<?php echo $kunik ?>ParamsData.flip
		        },
				"btnReadMore":{  
					"inputType" : "checkboxSimple",
					"label" : "<?php echo Yii::t('cms', 'See more')?>",
					"params" : checkboxSimpleParams,
					"checked" : <?= json_encode($paramsData["btnReadMore"]) ?> 
				},
				"btnReadMoreLineNUmber" : {
					"inputType" : "text",
					"label" : "<?php echo Yii::t('cms', 'Number of lines')?>",
					"rules" : {
						number : true
					}
				}
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                	tplCtx.value[k] = data.items;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
					 var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
					 var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
					 var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
					 cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
		  tplCtx.format = true;
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

		<?php if($paramsData["btnReadMore"]==true){ ?>
			setTimeout(() => {
				$(".more<?php echo $kunik ?>").myOwnLineShowMoreLess({
				showLessLine: <?= $paramsData["btnReadMoreLineNUmber"] ?>,
				showLessText:'Lire Moins',
				showMoreText:'Lire plus',
				//lessAtInitial:false,
				//showLessAfterMore:false
				});
			}, 900);
		<?php } ?>
 });
</script>
