<?php 
    $keyTpl ="content";
    $paramsData = [
        "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta voluptas explicabo nesciunt exercitationem placeat? Cupiditate harum debitis nesciunt deleniti hic? Similique obcaecati dicta hic aspernatur officia dolor eum ut. Sit.",
        "fontSize" => "20"
    ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }
?>

<style>
    .text-<?= $kunik?>{
        font-size: <?=$paramsData["fontSize"]?>px;
        margin-top: 20px;
        margin-bottom: 20px;
    }
</style>

<div class="sp-cms-container content-<?= $kunik?> col-xs-12">
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-12"></div>
        <div class="text col-md-8 col-sm-8 col-xs-12">
            <br>
            <p class="text-<?= $kunik?> sp-text" data-id="<?= $blockKey ?>" data-field="title" style="font-family: 'ml';">
                <?php echo $paramsData["content"];?>
            </p>
            <br><br>
        </div> 
        <div class="col-md-2 col-sm-2 col-xs-12"></div>
    </div>
   
</div>
<br><br>

<script type="text/javascript">
    $("polygon").each(function(){
        $(".text"+$(this).attr('id')).css("filter", "invert(100%)");
        // $(".text"+$(this).attr('id')).css("visibility", "hidden");
    });
</script>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockCms["_id"] ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      //urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
