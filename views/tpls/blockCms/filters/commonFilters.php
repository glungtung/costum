<?php
  $keyTpl ="commonFilters";

  $paramsData = [
    "activeFilter" => true,
    "mapHeight" => 550,
    "elementsType" => ["projects","organizations","events", "citoyens"],
    "filterHeaderColor" =>  "#AFCB21",
    "btnText" =>  "Voir dans l'annuaire",
    "btnBackground" =>  "#AFCB21",
    "btnBorderColor" =>  "#AFCB21",
    "btnBorderRadius" =>  2,
    "btnTextColor" =>  "white",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  if($paramsData["elementsType"]==""){
    $paramsData["elementsType"] = ["projects","organizations","events", "citoyens"];
  }
?>

 <style media="screen">
  .block-<?php echo $kunik ?> {
    min-height: auto !important;
  }

  .rounded-pill{
    border-radius: 40px;
    padding:inherit 4em;
    font-size: 10pt;
    text-transform: uppercase;
  }

  .btn-theme{
    background-color: #8FCF1B;
    color: white;
  }
</style>

<div id="filters<?= $kunik ?>" class="searchObjCSS menuFilters menuFilters-vertical col-xs-12 bg-light text-center block-<?php echo $kunik ?>"></div>

<script>
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
    var domId="#filters<?= $kunik ?>";

    var thematic = null;

    var startedData;

    var defaultType = {};

    if(costum && typeof costum.lists != "undefined" && typeof costum.lists.types != "undefined"){
      defaultType = Object.keys(costum.lists.types);
      //defaultType.push("organizations");
      //Object.assign(defaultType, costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"]["options"]);
    }else{
      defaultType = Object.keys(
        {"NGO" : trad.ong,
  				"Cooperative" : trad.servicepublic,
  				"Group":trad.group,
  				"LocalBusiness":trad.LocalBusiness,
  				"GovernmentOrganization":trad.GovernmentOrganization
  			});
      //defaultType = Object.keys(costum.lists.types);
    }

    if(costum && typeof costum.lists != "undefined" && typeof costum.lists.theme != "undefined"){
      thematic = {};
      Object.assign(thematic, costum.lists.theme);
    }

    var defaultScopeList = [];

    if(costum && costum.slug && costum.slug=="ries"){
      defaultScopeList = ["IT"];
    }else{
      defaultScopeList = ["FR", "RE"];
    }

    var defaultFilters<?= $kunik ?> = {'$or':{}};
    defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
    defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};

    var paramsFilter<?= $kunik ?>= {
        container : "#filters<?= $kunik ?>",
        loadEvent: {
            default:"dashboard"
        },
        defaults : {
            types : ["organizations"],
            fields:{"extraInfo.reteofapartment":"extraInfo.reteofapartment", "tags":"tags", "name":"name"},
            indexStep : 600,
            filters:defaultFilters<?= $kunik ?>

        },
        filters : {
          theme : {
            view : "megaMenuDropdown",
            type : "tags",
            remove0: true,
            countResults: true,
            name : "<?php echo Yii::t("common", "search by theme")?>",
            event : "tags",
            keyValue: true,
            list : thematic
          },
          category : {
            view : "dropdownList",
            type : "filters",
            field: "type",
            name : "<?php echo Yii::t("common", "search by type")?>",
            event : "filters",
            list : defaultType
          },
          network : {
            view : "dropdownList",
            type : "filters",
            field: "extraInfo.reteofapartment",
            name : "<?php echo Yii::t("common", "search by network")?>",
            event : "filters",
            list : ["RIES", "RESS ROMA"]
          },
          scopeList : {
            name : "<?php echo Yii::t("common", "Search by place")?>",
            params : {
              countryCode : defaultScopeList,
              level : ["3"]
            }
          },
          text : {
            placeholder: "Cerca per #tag o testo"
          }
        }
    }

    var filterSearch<?= $kunik ?>;

    jQuery(document).ready(function() {
      filterSearch<?= $kunik ?> = searchObj.init(paramsFilter<?= $kunik ?>)

        filterSearch<?= $kunik ?>.filters.actions.themes.callBack = function(data){
            
            let thisData = data;
            if(typeof startedData != "undefined"){
              thisData = startedData;
            }

            let network = [];
            for(const [key, value] of Object.entries(thisData)) {
              //paramsFilter<?= $kunik ?>.filters.network;
              if(typeof value.extraInfo != "undefined" && typeof value.extraInfo.reteofapartment != "undefined" && value.extraInfo.reteofapartment != ""){
                if(Array.isArray(value.extraInfo.reteofapartment)){
                  let rete = value.extraInfo.reteofapartment;
                  network.push(...rete);
                }else{
                  network.push(value.extraInfo.reteofapartment);
                }
              }
            }

            paramsFilter<?= $kunik ?>.filters["network"]["list"] = Array.from(new Set(network));
            filterSearch<?= $kunik ?>.filters.actions.themes.callBack = function(){}
            filterSearch<?= $kunik ?>.filters.initViews(filterSearch<?= $kunik ?>, paramsFilter<?= $kunik ?>);

            filterSearch<?= $kunik ?>.filters.initEvents(filterSearch<?= $kunik ?>, paramsFilter<?= $kunik ?>);

            filterSearch<?= $kunik ?>.filters.initDefaults(filterSearch<?= $kunik ?>, paramsFilter<?= $kunik ?>);

            $(filterSearch<?= $kunik ?>.container+' .badge-theme-count').each(function(index) {
              if($(this).text()=="0"){
                filterSearch<?= $kunik ?>.filters.actions.themes.isLoaded = false; 
              }
            });

            filterSearch<?= $kunik ?>.filters.actions.themes.setThemesCounts(filterSearch<?= $kunik ?>);

              $(".container-filters-menu").append("<button class='btn btn-lg btn-theme rounded-pill btn-reinit'> resettare il filtro </button>")
          }

          setTimeout(()=>{
            filterSearch<?= $kunik ?>.filters.actions.themes.setThemesCounts(filterSearch<?= $kunik ?>);
          }, 100);

          $(document).on("click", ".btn-reinit", function(){
                urlCtrl.loadByHash(location.hash);
              });

          $(document).on("click", "[data-type='types'], [data-type='filters'], [data-type='scopeList'], [data-type='tags']", function(e){
              e.preventDefault();
      				var selectValue = ( notNull($(this).data("value")) ) ? $(this).data("value") : $(this).data("key") ;
      				if($(this).data("type") != "filters" && $(this).data("type") != "tags"){
      					  if(notNull($(this).data("multiple"))){
      							if(typeof searchObj.search.obj[$(this).data("type")]=="undefined")
      								searchObj.search.obj[$(this).data("type")]=[];
      							if (searchObj.search.obj[$(this).data("type")].indexOf(selectValue) == -1)
      								searchObj.search.obj[$(this).data("type")].push( selectValue );
      						} else
      							searchObj.search.obj[$(this).data("type")] = selectValue;
      	        }else if(typeof $(this).data("field") != "undefined"){
      						if(typeof searchObj.search.obj.filters == "undefined" )
      							searchObj.search.obj.filters = {};

      						if(typeof searchObj.search.obj.filters[$(this).data("field")] == "undefined" )
      							searchObj.search.obj.filters[$(this).data("field")] = [];
      	                    searchObj.search.obj.filters[$(this).data("field")].push( selectValue );
      	        }
      					//searchObj.search.init(filterSearch<?= $kunik ?>)//, function(fObj, thedata){
                for (var member in window){
                  try {
                    if (window[member]!=null && exists(window[member].pInit)){
                      window[member].filters.actions.themes.isLoaded=true;

                        window[member].search.obj.filters = {...window[member].search.obj.filters, ...searchObj.search.obj.filters}

                        if(window[member].search.loadEvent.active == "graph"){
                          window[member].graph.init(window[member]);
                        }
                        coInterface.showLoader(window[member].results.dom, trad.currentlyresearching);
                        window[member].search.init(window[member]);
                      }
                    }catch(e){}
                }
    /*
                Chart.helpers.each(Chart.instances, function(instance){
                  instance.chart.update();
                });
    */


                //urlCtrl.loadByHash(location.hash);

              //})
            /*}else{
              searchObj.init(paramsFilter<?= $kunik ?>);
            }*/
          })

        $(document).on("click",".filters-activate", function(){
          $(".tooltip[role='tooltip']").remove();
          $(this).fadeOut().remove();
          //searchObj.filters.manage.xsActiveFilter(searchObj);
          searchObj.filters.manage.removeActive(searchObj, $(this));

          coInterface.initHtmlPosition();

          //searchObj.search.init(filterSearch<?= $kunik ?>);

          for (var member in window){
              if (window[member]!=null && exists(window[member].pInit)){
                try {
                  window[member].search.obj.filters = {...window[member].search.obj.filters, ...searchObj.search.obj.filters}
                  delete window[member].search.obj.filters[$(this).data("field")];
                  if(window[member].search.loadEvent.active == "graph"){
                    window[member].graph.init(window[member]);
                  }
                  window[member].search.init(window[member]);
                }catch(e){}
              }
          }
        })
    });

    var filteredTheme = "";

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {

            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        dyFObj.closeForm();
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
        });
</script>
