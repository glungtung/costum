<?php
    $keyTpl ="directFilterEventNews";
    $kunik = $keyTpl.(string)$blockCms["_id"];
    $blockKey = (string)$blockCms["_id"];
    $paramsData = [ 
        "themeBackground" => "#F1472C",
        "themeText" => "white",
        "themeBorderRadius" => "40px",
        "designTagFilter" => "",
        "agendaTitle" => "AGENDA",
        "newsTitle" => "ACTUALITÉS",
        "polesBorderRadius" => "50%",
        "poles" => array()
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php
$raf = PHDB::findOne(Organization::COLLECTION, 
                            array( "slug" => $costum["slug"]) , 
                            array("name", "slug", "links", "description") ) ;

Yii::import("parsedown.Parsedown", true);
$Parsedown = new Parsedown();
?>

<?php
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>

<style>
#newsstream  .loader{
	display: none;
}
.list-inline {
  display: flex;
  justify-content: center;
}


.carousel-inner img.img-responsive {
	width: 100%;
}
#myCarouselRaf .carousel-inner>.item>img.img-responsive {
    max-height: fit-content!important;
}

.table-container {
	display: table;
	table-layout: fixed;
	width: 100%;
	height: 100%;
}
.table-container2 {
	display: table;
	table-layout: fixed;
	width: 130px;
	height: 130px;
	font-size: 20px;
}

.table-container .col-table-cell, .table-container2 .col-table-cell {
	display: table-cell;
	vertical-align: middle;
	float: none;
}

.glyphicon{
	font-family: 'Glyphicons Halflings' !important;
}

.titleLabel{
    background: <?= ($paramsData["themeBackground"]) ?>;
    border-radius: <?= ($paramsData["themeBorderRadius"]) ?>;
    padding-top: 5px;
    padding-bottom:5px;
}

#projectChildren .col{
  	padding-left: 60px;
  	padding-right: 60px;
}

#projectChildren .projectsList, #projectChildren .poles{
    /*width: 180px;
    height: 180px;*/
    width: 130px;
    height: 130px;
    padding: 5px;
    word-wrap: break-word;
}

#projectChildren a{
    display: block;
    position: relative;
    padding-bottom: 100%;
    /*text-transform: uppercase;*/
    background-size: cover;
    background-position: center;
    color: #fff;
    border-radius: <?= ($paramsData["polesBorderRadius"]) ?>;
    width: 130px;
    height: 130px;
    font-size: 24px;
}
#projectChildren a span{
    display: table;
    position: absolute;
    top: 0;
    left: 0;
        width: 100%;
        height: 100%;
    font-size: 25px;
    text-align: center;
    border-radius: 50%;
    /*background-color: rgba(0,0,0, 0.5);*/
    border: 4px solid transparent;
    /*Xpadding: 15px;*/
}
#projectChildren a span:hover{
    color: rgba(0,0,0,0.2);
    background-color: rgba(0,0,0,0);
}
#projectChildren a span h4{
    display: table-cell;
    height: 100%;
    width: 100%;
    vertical-align: middle;
}
#projectChildren h3{
    margin-bottom: 45px;
}

@media screen and (max-width: 992px){
    #projectChildren .col{
        padding-left: 15px;
        padding-right: 15px;
        margin-bottom: 20px;
    }
    #projectChildren a span h4 {
        font-size: 17px;
        line-height: 23px;
    }
}

.swipter-pagination{
    margin-top: 2rem;
}

.swiper-pagination-bullet {
        margin-top: 2rem;
        background-color: transparent;
        display: inline-block;
        width: 2rem;
        height: 2rem;
        border: 3px solid white;
        opacity: .9;
    }

.swiper-pagination-bullet-active {
      border: 3px solid #1a2660;
      opacity: 1;
    }

</style>

<?php
    $strPoles = "";
    $strPolesSelect = "";

    $strProjects = "";
    $strProjectsSelect = "";

    if(!empty($raf) && !empty($raf["links"])){
        if(!empty($raf["links"]["projects"])){
            $idArray = array();
            foreach ($raf["links"]["projects"] as $idSubProject => $valSubProject) {
                $idArray[] = new MongoId($idSubProject) ;
            }
            $allSubProject = PHDB::find(Project::COLLECTION, 
                                        array( "_id" => 
                                            array('$in' => $idArray)), 
                                        array("name", "slug", "tags", "preferences", "links") );
        }
    }

    $tagAdmin = array();
    $tagsSlug = array();
    if(isset($costum["paramsData"]["poles"])){
        foreach ($costum["paramsData"]["poles"] as $keyP => $valPoles) {
            $slugTag = InflectorHelper::slugify2($keyP);
            $tagsSlug[$slugTag] = $keyP;
            if(!empty($valPoles["isAdmin"]))
                $tagAdmin[$keyP] = 0;

            if(!empty($allSubProject )){
                foreach ($allSubProject as $key => $value) {

                    if(!empty($value["tags"]) && in_array($keyP, $value["tags"])){
                        $seeP = false ;
                        if( !empty($value["preferences"]) && 
                            !empty($value["preferences"]["private"]) ){

                            $seeP = Link::isLinked($key, Project::COLLECTION, Yii::app()->session["userId"], @$value["links"]);
                        }else{
                            $seeP = true ;
                        }

                        if( $seeP ){
                            if(isset($tagAdmin[$keyP]))
                                $tagAdmin[$keyP]++;

                            $strProjects .= "<div class='subProject ".$slugTag." '>".
                                                "<a href='#@".$value["slug"]."' class='lbh titleFont1' style='opacity: 0.8; background-color: ".$valPoles["color"]." ;'>".
                                                "<div class='table-container2 text-center no-padding'>".
                                                    "<div class='col-table-cell col-xs-12 no-padding'>".$value["name"]."</div>".
                                                "</div></a>".
                                            "</div>";
                            $strProjectsSelect .= "<div class='subProject ".$slugTag." '>".
                                                    "<a href='#@".$value["slug"]."' class='lbh' style='background-color: ".$valPoles["color"]." ;'>".$value["name"]."</a>".
                                                "</div>";	
                        }
                    }
                    
                }
            }
            $seePoles = false ;

            if( isset($tagAdmin[$keyP]) && $tagAdmin[$keyP] > 0 )
                $seePoles = true;
            else if(!isset($tagAdmin[$keyP]))
                $seePoles = true;

            if($seePoles == true){
                $strPolesSelect .= "<option value='".$slugTag."'>".$keyP."</option>";
                $strPoles .= "<li class=''>".
                                "<a href='javascript:;' class='linkMenu poles titleFont1' data-pole='".$slugTag."' data-polename='".$keyP."'" .
                                    "style='background-color: ".$valPoles["color"]."'>".
                                "<div class='table-container text-center no-padding'>".
                                    "<div class='col-table-cell col-xs-12 no-padding'>".$keyP."</div>".
                                "</div></a>".
                            "</li>";
            }
        }
    }
?>
	<!-- col-xs-1 col-xs-offset-1 -->
	<div id="" class="col-xs-12 hidden-sm hidden-md hidden-lg no-padding">
		<div class='col-xs-12 '>
			<select id="chooseTags" class='col-xs-12'>
				<option value="">Choisir un pole</option>
				<?php echo $strPolesSelect; ?>
			</select>
		</div>
		<div class='col-xs-12 '>
			<ul class='col-xs-12 '>
				<?php echo $strProjectsSelect; ?>
			</ul>
		</div>
	</div>
	<div id="projectChildren" class="col-sm-12 hidden-xs col-sm-offset-1 col-sm-10 no-padding container<?php echo $kunik ?>">
		<div class='row listPoles'>
			<ul class="list-inline">
				<?php echo $strPoles; ?>
			</ul>
		</div>
		<div class='swiper-container'>
			<ul class='swiper-wrapper'>
				<?php echo $strProjects; ?>
			</ul>
            <div class="swiper-pagination"></div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-offset-1 col-sm-10 margin-top-50 no-padding">
		<div  class="col-xs-12 no-padding">
			<h2 class="titleFont2 titleSectionHome "><span class="titleLabel padding-left-20 padding-right-20 text-white sp-text" data-id="<?= $blockKey ?>" data-field="agendaTitle"><?= $paramsData["agendaTitle"] ?></span> <span class="namePole titleFont2"></span> </h2>
			<!--hr style="margin-top: 5px; border-bottom: 1px dashed #000000;"/-->
		</div>
		<div id="lastevents" class="col-xs-12 no-padding">
		</div>
		<div id="" class="col-xs-12">
			<a href="#@<?= $costum["slug"] ?>.view.directory.dir.events" class="lbh btn btn-primary pull-right" style="">Accès à l’agenda</a>
		</div>
    </div>
	<div class="col-xs-12 col-sm-10 col-sm-offset-1 no-padding margin-top-50">
		<div  class="col-xs-12 no-padding">
			<h2 class="titleFont2 titleSectionHome"><span class="titleLabel padding-left-20 padding-right-20 text-white sp-text" data-id="<?= $blockKey ?>" data-field="newsTitle"><?= $paramsData["newsTitle"] ?></span> <span class="namePole titleFont2"></span> </h2>
			<!--hr style="margin-top: 5px; border-bottom: 1px dashed #000000;"/-->
		</div>
		<div id="newsstream" class="hidden-xs hidden-sm col-xs-12 no-padding no-margin">
		</div>
		<div id="" class="col-xs-12 text-center">
			<a href="#@<?= $costum["slug"] ?>" class="lbh btn btn-primary pull-right hidden-xs hidden-sm" style="">Accès à l’actualité</a>

			<a href="#@<?= $costum["slug"] ?>" class="lbh btn btn-primary visible-xs visible-sm" style="">Accès à l’actualité</a>
		</div>
    </div>
</div>

<script type="text/javascript">

var slugTag = "";
var tagsSlug = <?php echo json_encode($tagsSlug) ?>;

var hashUrlPage= "#welcome";
    
var poleGet = "<?php echo @$_GET['pole']; ?>";
jQuery(document).ready(function() {
	// mylog.log("HERE <?= $costum["slug"] ?>");
	$(".subProject").hide();

	$(".poles").off().click(function () {
		$(".subProject").hide();
		//$("."+$(this).data("pole")).show();
		slugTag = $(this).data("pole");
		var nameSlug = $(this).data("polename");
		$(".namePole").html($(this).data("pole"));
		var style = "color: #d6d6d6;";
		$.each(costum.paramsData.poles, function(kT, vT){
			mylog.log("namePole poles", kT, vT, nameSlug);
			if(kT == nameSlug)
				style = "color: "+vT.color+";";
		});
		$(".namePole").attr("style", style);
		$(".titleSectionHome").addClass("initpole");
		loadPole();
	});

	$("#chooseTags").change(function () {
		$(".subProject").hide();
		//$("."+$(this).val()).show();
		slugTag = $(this).val();
		loadPole();
	});

	if(typeof poleGet != "undefined" && poleGet != null && typeof tagsSlug[poleGet] != "undefined" )
		slugTag = poleGet;
    loadPole();
});


function loadPole(){
	mylog.log("loadPole", slugTag);
    $(".subProject").removeClass("swiper-slide");
	if(slugTag == ""){
		getEvents();
		getActu();
		$(".subProject").hide();
	}else{
		$("."+slugTag).show();
		$("."+slugTag).addClass("swiper-slide");
		getEvents([tagsSlug[slugTag]]);
		getActu([tagsSlug[slugTag]]);
        var swiper = new Swiper(".container<?= $kunik ?> .swiper-container", {
            slidesPerView: 3,
            spaceBetween: 1,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            autoplay: {
                delay: 1500,
            },
            centerInsufficientSlides:true,
            keyboard: {
                enabled: true,
            },
            breakpoints: {
                486: {
                    slidesPerView: 2,
                },
                768: {
                    slidesPerView:4,
                    spaceBetween: 10,
                },
                860: {
                    slidesPerView:5,
                    spaceBetween: 10,
                },
                982: {
                    slidesPerView: 6,
                    spaceBetween: 10,
                },
                1100: {
                    slidesPerView: 7,
                    spaceBetween:10,
                },
                1200: {
                    slidesPerView: 8,
                    spaceBetween:10,
                },
                1400: {
                    slidesPerView: 9,
                    spaceBetween:10,
                },
                1600: {
                    slidesPerView: 11,
                    spaceBetween:10,
                },
                1800: {
                    slidesPerView: 13,
                    spaceBetween:10,
                },
                1920: {
                    slidesPerView: 14,
                    spaceBetween:10,
                },
            }
        });
	}

	$(".initpole").off().click(function () {
		slugTag = "";
		$(".namePole").html("");
		$(".titleSectionHome").removeClass("initpole");
		loadPole();
	});
	
}

function getDateTodayRaffinerie(){
	//TODO refaire la fonction et rendre custumisable la date de debut et de fin 
	mylog.log('directory.js getDateTodayRaffinerie');

	var today = new Date();
	today = new Date(today.setSeconds(0));
	today = new Date(today.setMinutes(0));
	today = new Date(today.setHours(0));
	today = new Date(today.setMonth(today.getMonth()-3));
	var dateToday = today.setDate(today.getDate());
	return dateToday;
}

function getEvents(tags){
	ajaxPost("",baseUrl+"/co2/element/getlastevents",
    			{
    				col:costum.contextType, 
    				id:costum.contextId, 
    				nbEvent:4,
    				tags : tags,
    				startDateUTC : moment(getDateTodayRaffinerie()).format()
    			}, function(events){
    	//mylog.log("raffinerie2 getlastevents events", events);
    	var str = "<div class='col-xs-12 padding-10 margin-top-10'>";
    	if(typeof events != "undefined" && 
    		events != null && 
    		Object.keys(events).length > 0 ){
    		$.each(events, function(keyE, valE){
    			str += costum.<?= $costum["slug"] ?>.directoryEvent(keyE, valE);
    		});
    		$("#lastevents").html(str);
    	} else {
    		str += "Il n'y a pas d'événement." ;
    	}
    	str +=  "</div>";
    	$("#lastevents").html(str);
    }, "json");
}

function getActu(tags){
	var urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false/";
	searchObj.live.init = function(fObj){		
		fObj.live.options.dateLimit=0;
		coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
		var scrollTopPosFilter=(typeof pageProfil != "undefined" && typeof pageProfil.affixPageMenu != "undefined") ? pageProfil.affixPageMenu+5 : 0;
	}
	ajaxPost("#newsstream",baseUrl+"/"+urlNews,{
		typeNews : [ "news" ],
		indexStep : 4, 
		search:true, 
		formCreate:false, 
		scroll:false,
		searchTags : tags
	}, function(news){

	}, "html");
}
</script>


<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer la section",
            "description" : "Personnaliser la section",
            "icon" : "fa-cog",
            "properties" : {
                "themeBackground" : {
                    "inputType" : "colorpicker",
                    "label" : "Thème d'arrière plan de titres"
                },
                "themeBorderRadius" : {
                    "inputType" : "text",
                    "label" : "Rondeur de la bordure de titres"
                },
                "polesBorderRadius" : {
                    "label": "Rodeur de poles",
                    "inputType" : "text"
                },
                "poles" : {
                    "label": "Tags et couleurs des pôles",
                    "inputType":"lists",
                    "entries": {
                        "tag" : {
                            "label": "Tag",
                            "type" : "text",
                            "class": "col-md-4",
                            "placeholder":"Tag"
                        },
                        "color" : {
                            "label" : "Couleur",
                            "type" : "colorpicker",
                            "class": "col-md-3",
                            "placeholder":"Couleur"
                        },
                        "isAdmin" : {
                            "label" : "Accès",
                            "type" : "select",
                            "class": "col-md-4",
                            "options":{
                                "":"Selectionner",
                                "true":"Admin Seulement",
                                "false":"Public",
                            },
                        }
                    }
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = data;
              var poles = {};
              $.each(data["poles"], function(k, val){
                poles[val.tag] = {"color":val.color};
                if(val.isAdmin=="true"){
                    poles[val.tag]["isAdmin"] = true;
                }
              });
              /*$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                if(k=="poles"){
                    tplCtx.value[k] = $("#"+k).val();
                }else{
                    tplCtx.value[k] = $("#"+k).val();
                }
                if (k == "parent"){
                    alert("Parent");
                    tplCtx.value[k] = formData.parent;
                }
                
              });*/

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    
                    $("#ajax-modal").modal('hide');
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);


                    tplCtx.id = costum._id.$id;
                    tplCtx["collection"]= "costum";
                    tplCtx["path"]= "paramsData.poles"
                    tplCtx["value"] = poles
                    tplCtx["removeCache"] = true;
                    dataHelper.path2Value(tplCtx, function(params){
                        toastr.success("Élément bien ajouter");
                    })
                  });
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",3,6,null,null,"Bouton Call To Action","green","");
        });
    });
</script>