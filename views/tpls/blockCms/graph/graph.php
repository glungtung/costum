<?php
if(!isset($costum)){
  $costum = CacheHelper::getCostum();
}

$keyTpl     = "graph";
$paramsData = [
    "dataSource" => [$costum["contextId"] => ["name" => $costum["contextSlug"], "type" => $costum["contextType"]]],
    "tags" => "",
    "graphType" => "circle",
    "depth" => 1,
    "max" => 40,
    "isSearch" => false,
    "isTagFilter" => false,
    "tagDirect" => true,
    "filtersParent" => [],
    "filtersChild" => [],
    "goToPreview" => true,
    "chartDataManual" => [],
    "chartDataFromFile" => [],
    "chartDataImportOrType" => true,
    "chartDataUse" => false
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

// Get project owner's logo
    $csvData = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        ), "csv"
    );

    $csvChartData = [];

    foreach ($csvData as $key => $value){
        array_push($csvChartData, array('folder' => $value["folder"], 'moduleId' => $value["moduleId"],'name' => $value["name"]));
    }

if(count($csvChartData)>0){
    foreach ($csvChartData as $key => $f) {
      $file = fopen("upload/".$f["moduleId"]."/".$f["folder"]."/".$f["name"], 'r');
      
      if($file){
          $paramsData["chartDataManual"] = [];
      }
      
      while($line=fgetcsv($file)){
          if($line[0]!="" && $line[1]!=""){
              array_push($paramsData["chartDataManual"], 
              array('label'=>$line[0], 'value' =>$line[1], 'color' =>(empty($line[2])?"#69b3a2":$line[2])));
          }
      }
      fclose($file);
    }
}
?>


<?php
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
  $graphAssets = [
    '/plugins/d3/d3.v6.min.js',  '/js/venn.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>


<style>
  #graph-container-<?= $kunik ?>{
    height: 100%;
    width: 100%;
    overflow: hidden;
  }

  .super-cms .graph-panel{
    background-color: transparent !important;
    width: 100%;
    height: 100%;
  }
</style>
<?php if($paramsData["isSearch"] == "true"){ ?>
    <div id="search-container-<?= $kunik ?>" class="col-xs-12 searchObjCSS">
    </div>
<?php } ?>

<div id="graph-container-<?= $kunik ?>" class="graph-panel">

</div>

<script>
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;

    var rawTags = "<?= $paramsData["tags"] ?>";
    var authorizedTags = []
    if(rawTags.trim() != ""){
      authorizedTags = rawTags.split(',');
    }
    <?php 
      foreach($paramsData["filtersParent"] as $filtersParent){
        ?> 
        if(costum.lists && costum.lists.<?= $filtersParent ?>){
        <?php  
          foreach($paramsData["filtersChild"] as $filtersChild){
        ?>

          if(costum.lists.<?= $filtersParent ?>["<?= $filtersChild ?>"]){
            authorizedTags.push(...Object.keys(costum.lists.<?= $filtersParent ?>["<?= $filtersChild ?>"]));
          }
          <?php 
          }
        ?>
        }
        <?php
      }
    ?>
    var l<?= $kunik ?> = {
    container: "#search-container-<?= $kunik ?>",
    loadEvent: {
      default: "graph"
    },
    defaults: {
      types: ["organizations"],
      indexStep: 900,
      filters:{
        toBeValidated:{'$exists':false}
      }
    },
    results: {
      dom: "#loader-container"
    },
    graph: {
      dom: "#graph-container-<?= $kunik ?>",
      authorizedTags: authorizedTags,
      defaultGraph: "<?= $paramsData["graphType"] ?>",
      mindmapDepth: <?= $paramsData["depth"] ?>,
      max: <?= $paramsData["max"] ?>
    },
    filters: {
      text: true,
      
    },
    header: {
      options : {
      }
    }
  };
  // l<?= $kunik ?>.defaults.forced = {
  //   filters: {
  //       "$or" : {}
  //   }
  //   }
  // var dataSource<?= $kunik ?> = <?= json_encode($paramsData["dataSource"]) ?>;
  // if(Object.keys(dataSource<?= $kunik ?>).length > 0){
  //   l<?= $kunik ?>.defaults["notSourceKey"] = true;
  //   for (const [id, value] of Object.entries(dataSource<?= $kunik ?>)) {
  //     l<?= $kunik ?>.defaults.forced.filters["$or"]["links.memberOf."+id] = {"$exists" : true};    
  //   }
  // }
    if(!window.initedGraph){
      window.initedGraph = [];
    }

    var p<?= $kunik ?> = {};

    

    function initGraph<?= $kunik ?>() {
        p<?= $kunik ?> = searchObj.init(l<?= $kunik ?>);

        <?php if($paramsData["graphType"]=="circularbarplot"){ ?>
        var chartData<?= $kunik ?> = [];
        var manualData = sectionDyf.<?php echo $kunik?>ParamsData.chartDataManual;
          if((!Array.isArray(manualData) || (Array.isArray(manualData) && manualData.length!=0)) && sectionDyf.<?= $kunik ?>ParamsData.chartDataUse=="true"){
            
            p<?= $kunik ?>.graph.successComplete = function(fObj, rawData){
              const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
              const authorizedTags = tags.length > 0 ? tags.filter(el => p<?= $kunik ?>.graph.authorizedTags.includes(el)) : p<?= $kunik ?>.graph.authorizedTags;
              var chartData<?= $kunik ?> = {};
              var dataColors<?= $kunik ?> = [];
              $.each(sectionDyf.<?php echo $kunik?>ParamsData.chartDataManual, function(index, manData){
                  chartData<?= $kunik ?>[manData.label] = 0;
                  if(typeof manData.value != "undefined" && !isNaN(manData.value)){
                    chartData<?= $kunik ?>[manData.label] = parseInt(manData.value);
                  }

                  dataColors<?= $kunik ?>.push(manData.color);
              })
              p<?= $kunik ?>.graph.graph.setAuthorizedTags(Object.keys(chartData<?= $kunik ?>));
              p<?= $kunik ?>.graph.graph._defaultColor=d3.scaleOrdinal(dataColors<?= $kunik ?>);
              this.lastResult = rawData;
              p<?= $kunik ?>.graph.graph.updateData(chartData<?= $kunik ?>, p<?= $kunik ?>.graph.autoDraw);
              p<?= $kunik ?>.graph.graph.initZoom();
            }
        }
      <?php } ?>

        p<?= $kunik ?>.graph.init(p<?= $kunik ?>);
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);
        setTimeout(() => {
            p<?= $kunik ?>.graph.graph.initZoom();
        }, 500);
    }
    setTimeout(() => {
      if($("#graph-container-<?= $kunik ?>").is(":visible")){
        window.initedGraph.push('<?= $kunik ?>');
        initGraph<?= $kunik ?>();
      }
    },200)
</script>
<script type="text/javascript">
  
  jQuery(document).ready(function() {
    const filtersData = {};
    if(costum && costum.lists){
      for (const key of Object.keys(costum.lists)) {
        filtersData[key] = key;
      }
    }
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "graphType" : {
            "label" : "Choisir le graphe que vous voulez",
             "inputType": "select",
              options : {
                circle : "Circle Graph",
                mindmap : "Mindmap Graph",
                relation : "Relation Graph",
                network : "Network Graph",
                circlerelation : "Circle Relation Graph",
                venn : "Venn Graph",
                circularbarplot : "Circular Barplot Graph",
              }, 
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.graphType
          },
          "depth" : {
            "label" : "Profondeur max pour le collapse",
             "inputType": "quantity",
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.depth
          },
          "max" : {
            "label" : "Nombre d'item max",
             "inputType": "quantity",
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.max
          },
          "goToPreview" : {
              "label" : "Go to preview",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Filtre activé",
                    "offLabel" : "Filtre desactivé",
                    "labelText" : "Ajouter une fonction filtre par tags"
                },
                "checked" : false
          },
          "dataSource" : {
            "label" : "Organisation source",
             "inputType": "finder",
             "initType": ["organizations"],
            "openSearch" :true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.dataSource
          },
          "isSearch" : {
              "label" : "Ajouter une fonction recherche",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Recherche activé",
                    "offLabel" : "Recherche desactivé",
                    "labelText" : "Ajouter une fonction recherche"
                },
                "checked" : false
          },
          "isTagFilter" : {
              "label" : "Ajouter une fonction filtre par tags",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Filtre activé",
                    "offLabel" : "Filtre desactivé",
                    "labelText" : "Ajouter une fonction filtre par tags"
                },
                "checked" : false
          },
          "filtersParent": {
              "label" : "Choisir un ou plusieurs donnée :",
              "class" : "form-control <?php echo $kunik ?>",
              "inputType" : "select",
              select2 : {
                  multiple : true
              },
              "options" : filtersData,
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.filtersParent
          },
          "filtersChild": {
              "label" : "Choisir un ou plusieurs donnée :",
              "class" : "form-control <?php echo $kunik ?>",
              "inputType" : "select",
              select2 : {
                  multiple : true
              },
              "options" : [],
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.filtersChild
          },
          "tags" : {
              "label" : "Les tags à visualiser",
              "inputType": "tags"
          },
          "chartDataUse" : {
              "label" : "Utiliser données à part",
              "inputType" : "checkboxSimple",
              "params" : {
                  "onText" : trad.yes,
                  "offText" : trad.no,
                  "onLabel" : trad.yes,
                  "offLabel" : trad.no
              },
              "checked" : true,
              "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartDataUse
          },
          "chartDataImportOrType" : {
              "label" : "Saisir les données ou Importer un Fichier CSV",
              "inputType" : "checkboxSimple",
              "params" : {
                  "onText" : "Saisir les données",
                  "offText" : "Importer Fichier CSV",
                  "onLabel" : "",
                  "offLabel" : ""
              },
              "checked" : true,
              "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartDataImportOrType
          },
          "chartDataManual" : {
              "inputType" : "lists",
              "label" : "Inserer ici vos Données manuellement",
              "entries":{
                  "label":{
                      type:"text",
                      label:"Libellé",
                      class:"col-md-3 padding-5",
                      placeholder: ""
                  },
                  "value":{
                      type:"text",
                      label:"Valeur",
                      class:"col-md-3 padding-5",
                      placeholder: ""
                  },
                  "color":{
                      type:"text",
                      label:"Couleur",
                      class:"col-md-3 padding-5",
                      placeholder: "blue"
                  }
              },
              value : sectionDyf.<?php echo $kunik?>ParamsData.chartDataManual
          },
          "chartDataFromFile" : {
              "inputType" : "uploader",
              "label" : "Importer un Fichier csv avec le format (libellé, valeur, couleur)",
              "showUploadBtn" : false,
              "docType" : "file",
              "itemLimit" : 1,
              "contentKey" : "file",
              "domElement" : "documentationFile",
              "placeholder" : "Fichier csv",
              "afterUploadComplete" : null,
              "template" : "qq-template-manual-trigger",
              "filetypes" : ["csv"],
              initList : <?php echo json_encode($csvData) ?>
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        afterBuild : function() {
            if(sectionDyf.<?php echo $kunik ?>ParamsData.filtersParent.length > 0){
              updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.filtersParent);
              $("#filtersChild.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.filtersChild).trigger('change');
            }
            if(sectionDyf.<?= $kunik ?>ParamsData.chartDataUse!=("true"||true)){
                costum.checkboxSimpleEvent.false("chartDataUse")
            }
        },
        save : function (data) {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
            if (k == "dataSource") {
              tplCtx.value[k] = formData.dataSource;
            }
            if(k=="chartDataManual"){
                let datasets = {};
                $.each(data[k], function(index, va){
                    datasets["dataset"+index] = va;
                });
                tplCtx.value[k] = datasets;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    $(document).on("change", "#filtersParent.<?= $kunik ?>", function(){
      const val = $(this).val();
      $("#filtersChild.<?php echo $kunik ?>").empty();
        if(val){
          updateInputList(val.toString().split(","));
        }
    });
    function updateInputList(values) {
      if(!values) return;
      $("#filtersChild.<?php echo $kunik ?>").empty();
      for (const value of values) {
        if(!costum || !costum.lists || !costum.lists[value]) continue;
        const keys = Object.keys(costum.lists[value]);
        for (let i = 0; i < keys.length; i++) {
          const tag = keys[i];
          $("#filtersChild.<?php echo $kunik ?>").append('<option value="'+tag+'">'+tag+'</option>'); 
        }
      }
    }
    function switchFiltre(isDirect) {
      if(isDirect){
        $("#ajaxFormModal .filtersParentselectMultiple").hide();
        $("#ajaxFormModal .filtersChildselect").hide();
        $("#ajaxFormModal .tagstags").show();
      }else{
        $("#ajaxFormModal .filtersParentselectMultiple").show();
        $("#ajaxFormModal .filtersChildselect").show();
        $("#ajaxFormModal .tagstags").hide();
      }
    }
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      setTimeout(() => {
        onload();
        $("select[name='graphType']").change(onload);
      },1200)
    });
    function onload() {
      if($("select[name='graphType']").val() == "mindmap"){
        $('.depthquantity').show();
      }else{
        $('.depthquantity').hide();
      }
      if($("select[name='graphType']").val() == "circle" || $("select[name='graphType']").val() == "relation" || $("select[name='graphType']").val() == "venn"){
        $('.tagstags').show();
      }else{
        $('.tagstags').hide();
      }
    }

    costum.checkboxSimpleEvent = {
        true : function(id){
            if(id=="chartDataUse"){
                $("#ajax-modal .chartDataImportOrTypecheckboxSimple").show();
                costum.checkboxSimpleEvent[sectionDyf.<?= $kunik ?>ParamsData.chartDataImportOrType]("chartDataImportOrType");
            }
            if(id=="chartDataImportOrType"){
                $("#ajax-modal .chartDataManuallists").show();
                $("#ajax-modal .chartDataFromFileuploader").hide();
            }
        },
        false : function(id){
            if(id=="chartDataUse"){
                $("#ajax-modal .chartDataImportOrTypecheckboxSimple .btn-dyn-checkbox").trigger("click");
                $("#ajax-modal .chartDataImportOrTypecheckboxSimple").hide();
                $("#ajax-modal .chartDataFromFileuploader").hide();


            }
            if(id=="chartDataImportOrType"){
                $("#ajax-modal .chartDataFromFileuploader").show();
                $("#ajax-modal .chartDataManuallists").hide();
            }
        }
    }
  });
</script>
