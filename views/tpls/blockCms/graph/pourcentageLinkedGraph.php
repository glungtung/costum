<?php 
    $keyTpl     = "pourcentageLinkedGraph";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "color01" => "#9B6FAC",
        "color02" => "#ddd",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>
<svg id="Calque_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 973.13 394.55">
    <defs>
        <style>
            .<?=$kunik?>.clslg-1,
            .<?=$kunik?>.clslg-2{font-family:Poppins-SemiBold, Poppins;font-size:16px;font-weight:600;}
            .<?=$kunik?>.clslg-2,
            .<?=$kunik?>.clslg-3,
            .<?=$kunik?>.clslg-4{fill:<?=$paramsData["color02"]?>;}
            .<?=$kunik?>.clslg-5{fill:#a5c145;}
            .<?=$kunik?>.clslg-6{fill:#3d4d57;}
            .<?=$kunik?>.clslg-4{stroke:#a5c145;}
            .<?=$kunik?>.clslg-4,
            .<?=$kunik?>.clslg-7{stroke-miterlimit:10;stroke-width:3px;}
            .<?=$kunik?>.clslg-7{fill:none;stroke:<?=$paramsData["color01"]?>;stroke-dasharray:0 4;}
        </style>
    </defs>
    <line class="<?=$kunik?> clslg-7" x1="163.51" y1="181.65" x2="270.01" y2="218.29"/>
    <line class="<?=$kunik?> clslg-7" x1="409.6" y1="171.78" x2="490.08" y2="115.81"/>
    <line class="<?=$kunik?> clslg-7" x1="605.87" y1="119.7" x2="663.27" y2="146.65"/>
    <text class="<?=$kunik?> clslg-1" transform="translate(46.26 239.5)"><tspan id="Architecturesetpatrimoines<?=$kunik?>" class="<?=$kunik?> clslg-5" x="-20" y="0"></tspan><tspan class="<?=$kunik?> clslg-3"><tspan x="15" y="0">Architectures </tspan></tspan><tspan class="<?=$kunik?> clslg-3" x="0" y="19.2">et </tspan><tspan class="<?=$kunik?> clslg-5" x="19.78" y="19.2">patrimoines</tspan></text>
    <text class="<?=$kunik?> clslg-2" transform="translate(862.25 353.81)"><tspan id="Musiques<?=$kunik?>" class="<?=$kunik?> clslg-5" x="-20" y="0"></tspan><tspan x="15" y="0">Musiques</tspan></text>
    <text class="<?=$kunik?> clslg-2" transform="translate(309.5 280.94)"><tspan id="Ecritures<?=$kunik?>" class="<?=$kunik?> clslg-5" x="-20" y="0"></tspan><tspan x="15" y="0">Ecritures</tspan></text>
    <text class="<?=$kunik?> clslg-1" transform="translate(484.47 162.4)"><tspan id="Artsplastiques<?=$kunik?>" class="<?=$kunik?> clslg-5" x="-20" y="0"></tspan><tspan class="<?=$kunik?> clslg-3" x="20" y="0">Arts </tspan><tspan class="<?=$kunik?> clslg-5" x="61.4" y="0">plastiques</tspan></text>
    <text class="<?=$kunik?> clslg-1" transform="translate(654.52 238.66)"><tspan id="Spectaclevivant<?=$kunik?>" class="<?=$kunik?> clslg-5" x="-20" y="0"></tspan><tspan class="<?=$kunik?> clslg-3" x="20" y="0">Spectacle </tspan><tspan class="<?=$kunik?> clslg-5" x="104.81" y="0">vivant</tspan></text>
    <line class="<?=$kunik?> clslg-7" x1="786.69" y1="194.37" x2="852.4" y2="241.71"/><circle class="<?=$kunik?> clslg-4" cx="730.24" cy="157.52" r="51.65"/><g>
        <path class="<?=$kunik?> clslg-6" d="M717.96,175.85c0,.27,.1,.53,.29,.72,.19,.19,.45,.3,.72,.3h22.36c.54,0,.99-.44,1.01-.98,0-.14,.04-1.57-.26-3.53h16.32c.54,0,.99-.44,1.01-.98,.01-.34,.22-8.53-4.91-13.24-.36-.33-.91-.35-1.3-.05-.06,.04-6.03,4.53-11.59,0-.38-.31-.93-.3-1.3,.02-.12,.1-1.75,1.56-3.17,4.36-.33-.14-.72-.1-1.01,.12-.25,.19-6.06,4.51-11.59,0-.27-.22-.62-.28-.94-.18-.71-1.53-1.7-3.03-3.07-4.28-.36-.33-.91-.35-1.3-.05-.06,.04-6.03,4.53-11.59,0-.38-.31-.93-.3-1.3,.02-.21,.18-5.15,4.58-5.28,13.23,0,.27,.1,.53,.29,.72,.19,.19,.45,.3,.72,.3h16.22c-.19,1.08-.32,2.23-.34,3.49Zm23.12-15.63c5.28,3.68,10.67,1.12,12.61-.04,3,3.22,3.58,8.1,3.69,10.16h-15.69c-.51-2.06-1.41-4.38-3-6.35,.84-1.83,1.81-3.1,2.41-3.77Zm-17.07,4.51c5.28,3.69,10.67,1.13,12.61-.04,3,3.22,3.58,8.1,3.69,10.16h-20.27c.36-5.43,2.85-8.83,3.98-10.12Zm-20.87,5.61c.36-5.43,2.85-8.84,3.98-10.12,5.28,3.68,10.67,1.12,12.61-.04,1.04,1.11,1.78,2.42,2.32,3.74-1,1.24-2.4,3.38-3.27,6.42h-15.63Z"/>
    <path class="<?=$kunik?> clslg-6" d="M703.7,147.73c0,5.27,4.29,9.55,9.56,9.55h0c2.55,0,4.95-1,6.76-2.8,.25-.25,.48-.51,.7-.79,.71,4.58,4.66,8.1,9.44,8.1h0c2.55,0,4.95-1,6.75-2.8,1.51-1.51,2.44-3.43,2.71-5.5,1.75,2.3,4.5,3.79,7.61,3.79h0c2.55,0,4.95-1,6.75-2.8,1.81-1.81,2.8-4.21,2.8-6.76,0-5.27-4.29-9.55-9.56-9.55-2.55,0-4.95,1-6.76,2.8-1.51,1.51-2.44,3.43-2.71,5.5-1.75-2.3-4.5-3.8-7.61-3.8-2.55,0-4.95,1-6.76,2.8-.25,.25-.48,.51-.7,.79-.71-4.58-4.66-8.1-9.44-8.1-2.55,0-4.95,1-6.76,2.8-1.81,1.81-2.8,4.21-2.8,6.76Zm38.19-5.33c1.43-1.43,3.32-2.21,5.33-2.21,4.15,0,7.54,3.38,7.54,7.54,0,2.01-.78,3.9-2.21,5.33-1.43,1.43-3.32,2.21-5.33,2.21h0c-4.16,0-7.54-3.38-7.54-7.54,0-2.01,.78-3.9,2.21-5.33Zm-17.07,4.51c1.43-1.43,3.32-2.21,5.33-2.21,4.16,0,7.54,3.38,7.54,7.54,0,2.01-.78,3.9-2.21,5.33-1.43,1.43-3.32,2.21-5.33,2.21h0c-4.16,0-7.54-3.38-7.54-7.54,0-2.01,.78-3.9,2.21-5.33Zm-11.56-6.72c4.15,0,7.54,3.38,7.54,7.54,0,2.01-.79,3.9-2.21,5.33-1.42,1.43-3.32,2.21-5.33,2.21h0c-4.16,0-7.54-3.38-7.54-7.54,0-2.01,.78-3.9,2.21-5.33,1.43-1.43,3.32-2.21,5.33-2.21Z"/></g><g><circle class="<?=$kunik?> clslg-4" cx="906.32" cy="279.79" r="51.65"/><g>
        <path class="<?=$kunik?> clslg-6" d="M913.79,271.03c-.18-.15-.43-.21-.66-.17l-20.9,4.03c-.37,.07-.64,.4-.64,.78v20.62c-1.5-.18-3.22,.06-4.87,.7-3.95,1.52-6.27,4.7-5.29,7.26,.49,1.29,1.73,2.17,3.48,2.49,.46,.08,.94,.12,1.43,.12,1.24,0,2.58-.26,3.88-.76,3.29-1.26,5.54-3.76,5.51-6.09,0-.02,0-.03,0-.05v-13.51l14.22-2.74v8.03c-1.5-.18-3.22,.06-4.87,.7-3.95,1.52-6.27,4.71-5.29,7.26,.49,1.29,1.73,2.17,3.48,2.49,1.61,.29,3.49,.06,5.31-.63,3.21-1.23,5.46-3.68,5.51-5.97,0-.03,0-.07,0-.1v-23.85c0-.23-.1-.44-.26-.59,0,0-.02-.02-.03-.03Zm-1.3,24.38s0,.06,0,.1c.01,1.65-1.87,3.57-4.48,4.57-1.55,.59-3.13,.79-4.45,.55-1.18-.21-1.99-.74-2.28-1.49-.64-1.67,1.36-4.05,4.37-5.2,1.73-.66,3.51-.83,4.89-.46,.24,.06,.5,.02,.69-.14,.2-.15,.31-.38,.31-.63v-9.96c0-.24-.11-.46-.29-.61-.18-.15-.42-.21-.66-.17l-15.81,3.05c-.37,.07-.65,.4-.65,.78v14.11s0,.06,0,.09c.05,1.63-1.87,3.62-4.49,4.62-1.55,.59-3.13,.79-4.45,.55-1.18-.21-1.99-.74-2.28-1.49-.64-1.67,1.36-4.05,4.37-5.2,1.12-.43,2.27-.65,3.31-.65,.56,0,1.1,.06,1.58,.2,.24,.07,.5,.02,.69-.14,.2-.15,.31-.38,.31-.63v-20.92l19.31-3.73v22.8Z"/>
    <path class="<?=$kunik?> clslg-6" d="M894.93,283.7s.1,0,.15-.01l15.81-3.05c.37-.07,.64-.4,.64-.78v-2.2c0-.24-.11-.46-.29-.62-.18-.15-.42-.21-.66-.17l-15.81,3.05c-.37,.07-.65,.4-.65,.78v2.2c0,.24,.11,.46,.29,.61,.14,.12,.32,.18,.51,.18Zm.8-2.34l14.22-2.74v.58l-14.22,2.74v-.58Z"/>
    <path class="<?=$kunik?> clslg-6" d="M931.42,255.6s-.01-.04-.02-.05c-.05-.12-.15-.2-.28-.23l-11.57-2.59c-.19-.04-.38,.05-.45,.23l-4.61,10.58c-.72-.44-1.67-.7-2.67-.75-1.07-.04-2.1,.17-2.89,.6-.85,.47-1.34,1.14-1.38,1.89-.03,.75,.39,1.46,1.2,2.01,.75,.5,1.75,.8,2.83,.85,1.94,.09,3.63-.68,4.12-1.86,0,0,0-.01,0-.02l3.02-6.93,7.96,1.78-1.83,4.2c-.73-.44-1.67-.7-2.67-.75-2.32-.1-4.2,.99-4.27,2.49-.03,.75,.39,1.46,1.2,2.01,.75,.5,1.75,.8,2.83,.85,.1,0,.19,0,.29,0,1.78,0,3.31-.72,3.81-1.81,0-.02,.02-.03,.02-.05l5.3-12.17c.04-.09,.04-.18,.02-.26Zm-6.03,12.08s-.02,.03-.02,.05c-.37,.86-1.78,1.43-3.35,1.36-.93-.04-1.79-.29-2.42-.72-.57-.38-.87-.84-.85-1.31,.04-.97,1.48-1.73,3.18-1.73,.08,0,.17,0,.25,0,1.04,.05,2,.36,2.63,.87,.1,.08,.22,.1,.34,.08,.12-.03,.22-.11,.27-.23l2.21-5.08c.05-.11,.04-.23-.01-.34-.05-.11-.15-.18-.27-.21l-8.75-1.96c-.19-.04-.38,.05-.45,.23l-3.14,7.21s-.01,.03-.02,.04c-.34,.86-1.79,1.46-3.36,1.39-.93-.04-1.79-.29-2.42-.72-.57-.38-.87-.84-.85-1.31,.02-.46,.36-.9,.96-1.23,.66-.36,1.54-.54,2.47-.5,1.04,.05,2,.36,2.62,.87,.1,.08,.22,.1,.34,.08,.12-.03,.22-.11,.27-.23l4.67-10.72,10.78,2.41-5.09,11.69Z"/>
    <path class="<?=$kunik?> clslg-6" d="M919.28,256.09l-.49,1.13c-.05,.11-.04,.23,0,.34,.05,.11,.15,.18,.27,.21l8.75,1.96s.06,0,.09,0c.16,0,.3-.09,.36-.24l.49-1.13c.05-.11,.04-.23-.01-.34-.05-.11-.15-.18-.27-.21l-8.75-1.96c-.19-.04-.38,.05-.45,.23Zm.6,.62l7.96,1.78-.17,.38-7.96-1.78,.17-.38Z"/></g></g><circle class="<?=$kunik?> clslg-4" cx="546.08" cy="74.42" r="51.65"/><circle class="<?=$kunik?> clslg-4" cx="344.15" cy="199.97" r="51.65"/><g>
        <path class="<?=$kunik?> clslg-6" d="M562.88,52.68c.4,.28,.85,.42,1.31,.42,.7,0,1.39-.32,1.83-.93,.72-1,.49-2.41-.51-3.13-1-.72-2.41-.49-3.13,.51,0,0,0,0,0,0-.72,1-.49,2.41,.51,3.13Zm1.16-1.93c.06-.08,.18-.1,.26-.04,.08,.06,.1,.18,.04,.26-.06,.08-.18,.1-.26,.04-.08-.06-.1-.18-.04-.26Z"/>
    <path class="<?=$kunik?> clslg-6" d="M523.3,90.78c.09,.78,.78,1.21,1.51,.86,.65-.31,2.35-1.5,3.08-1.27,.85,.27,.76,.88,1.25,1.38,.69,.7,1.15,1.09,2.19,1.17,.63,.05,1.33-.28,1.94-.19,1.72,.27,1.33,.97,2.05,1.87,.61,.77,1.3,1.02,2.27,.99,.88-.03,1.16-.34,2.05,.09,.95,.45,.87,.95,1.46,1.73,.53,.71,1.09,1.2,2,1.34,2.07,.31,3.79-1.66,5.13-2.92,.11-.1,.18-.21,.23-.32,.03,0,.05,.02,.08,.03l2.48-3.96c.06-.09,.1-.19,.12-.28,0,0,0-.03,.01-.05l7.66-13.56c.04-.06,.06-.13,.08-.2,0-.01,0-.03,.01-.04,.02-.07,.03-.14,.03-.21,0-.01,0-.02,0-.04l.23-10.42,9.34-13c.05-.06,.09-.14,.12-.21,.07-.17,1.69-4.29-1.24-6.51-.01-.01-.02-.02-.04-.03-.04-.03-.07-.05-.11-.08-.02-.02-.04-.03-.06-.04-.03-.02-.05-.04-.08-.06-.03-.02-.06-.04-.09-.05-3.04-2.05-6.42,.8-6.56,.92-.06,.05-.12,.12-.16,.18l-9.36,13.04-8.9,4.05c-.19,.04-.37,.14-.51,.29l-8.96,9.49c-.07,.05-.14,.1-.2,.17l-4.76,5.47c-.11,.07-.21,.16-.29,.27l-3.28,4.56c-.09,.07-.18,.15-.25,.26-.1,.15-.18,.29-.26,.44l-.08,.11s.01,0,.02,0c-.76,1.56-.36,2.94-.16,4.74Zm28.72-28.1c.16-.07,.31-.19,.41-.34l3.78-5.26,5.67-7.89c.22-.17,.83-.62,1.59-.88,.19-.07,.39-.12,.59-.16,.1-.02,.21-.03,.31-.04,.52-.04,1.05,.06,1.54,.41,.02,.02,.04,.03,.07,.04,0,0,0,0,0,0,0,0,.01,0,.02,0,.01,0,.02,.02,.03,.02,.02,.02,.04,.03,.06,.05,.18,.13,.32,.28,.45,.44,.11,.15,.2,.3,.28,.47,.19,.43,.26,.9,.25,1.34,0,0,0,0,0,.01,0,.19-.02,.37-.04,.54-.03,.28-.09,.54-.14,.76-.05,.21-.11,.38-.14,.48l-6.95,9.67-2.5,3.47c-.12,.17-.19,.37-.19,.58l-.2,8.79-8.4-6.02-4.2-3.01,7.7-3.5Zm-9.63,4.65l14.16,10.16-2.89,5.11-2.89,5.11h0c-.06-.13-.12-.26-.19-.38-.07-.13-.15-.26-.23-.39-.14-.21-.29-.42-.46-.61-.16-.19-.33-.36-.52-.53-.84-.77-1.95-1.34-3.32-1.71-.46-.12-.94-.23-1.45-.31-.05,0-.1,0-.16,0-.06,0-.12,0-.17,.01,0,0,0,0-.02,0-.02,0-.06,0-.12,.01-.03,0-.06,0-.1,0-.04,0-.08,0-.12,0-.13,0-.28,0-.45-.02-.32-.04-.68-.13-1.01-.32-.11-.06-.21-.14-.31-.23-.12-.11-.23-.23-.32-.38-.23-.35-.36-.83-.42-1.39-.01-.11-.03-.21-.03-.34,0-.14,0-.29,0-.44,0,0,0,0,0,0,0-.03,.02-.08,.02-.11,.04-.16,.08-.35,.11-.58,0-.01,0-.02,0-.04,.01-.07,.02-.14,.03-.2,.01-.08,.03-.15,.04-.24,.11-.74,.12-1.36,.09-1.88-.02-.27-.06-.51-.1-.72-.14-.63-.38-1.05-.61-1.32-.16-.18-.31-.3-.44-.37-.35-.21-.71-.29-1.08-.28-.19,0-.37,.03-.56,.08-.84,.21-1.69,.81-2.38,1.38-.08,.06-.15,.13-.23,.19-.05,.03-.12,.07-.18,.1-.2,.11-.45,.21-.69,.28-.08,.03-.16,.04-.24,.06-.08,.02-.15,.03-.22,.03-.07,0-.14,0-.2,0-.06,0-.12-.03-.16-.05-.16-.08-.33-.39-.4-.99l8.2-8.68Zm-16.9,19.19s.08,.02,.12,.03l3.18-4.43c.06-.04,.11-.09,.15-.14l3.64-4.19c.25,.46,.6,.81,1.04,1.04,1.61,.84,3.61-.42,3.83-.56,.04-.03,.08-.05,.11-.08,1.19-1.03,1.74-1.17,1.9-1.19,.25,.4,.2,1.96-.12,3.31-.01,.06-.02,.12-.03,.19-.09,1.9,.4,3.35,1.46,4.29,1.32,1.18,3.08,1.12,3.66,1.06,2.03,.35,3.44,1.09,4.17,2.19,.71,1.07,.6,2.24,.54,2.62l-2.36,3.76h0s0,0,0,0c-.98,.92-2.63,3.06-4.16,1.74-.67-.59-.71-1.77-1.5-2.37-.69-.53-1.22-.51-2.06-.53-.51,.19-1.02,.18-1.53,0-.63,.12-.98-.11-1.06-.69-.37-.37-.37-.8-.79-1.17-.61-.54-1.35-.67-2.13-.67-.51,0-1.12,.26-1.63,.18-2.18-.35-1.06-.91-2.01-1.68-1.59-1.29-3.12-.77-4.77,.02-.03,.02-.07,.03-.1,.05,0-.03,0-.06-.01-.1-.13-.9-.15-1.78,.43-2.68Z"/></g>
    <path class="<?=$kunik?> clslg-6" d="M364.92,219.58h-16.57c1.79-1.84,3.59-3.68,5.38-5.52,.09,0,.18-.02,.26-.06,.01,0,.02,0,.04-.01,0,0,.01,0,.02,0,.18-.07,.33-.21,.42-.43,3.54-8.5,7.08-16.99,10.62-25.49,.5-1.21,1-2.41,1.51-3.62,.04-.08,.06-.16,.07-.23,.69-2.28-.22-4.78-2.53-5.74-2.29-.96-5.02,.17-5.98,2.46-3.54,8.5-7.08,16.99-10.62,25.49-.5,1.2-1,2.41-1.51,3.61-.12,.3-.05,.65,.16,.88-.01,.69-.02,1.37-.03,2.05,0,.32-.01,.65-.02,.97-.03,1.88-.06,3.76-.09,5.64h-23.78c-1.17,0-1.17,2.25,0,2.25h42.65c1.17,0,1.17-2.25,0-2.25Zm.33-36.59c-1.72-.72-3.43-1.43-5.15-2.14,1.66-2.18,5.38-.61,5.15,2.14Zm-7.07,2.19c.4-.95,.79-1.9,1.19-2.85,1.79,.75,3.59,1.5,5.38,2.24-3.43,8.24-6.87,16.49-10.3,24.73-.4,.95-.79,1.9-1.19,2.85-1.79-.75-3.59-1.5-5.38-2.24,3.43-8.24,6.87-16.48,10.3-24.73Zm-10.36,26.84c1.33,.55,2.65,1.11,3.98,1.66-.76,.78-1.53,1.56-2.29,2.35-.58-.24-1.16-.48-1.75-.73,.02-1.09,.04-2.18,.05-3.28Z"/><circle class="<?=$kunik?> clslg-4" cx="98.28" cy="149.98" r="51.65"/><g>
        <path class="<?=$kunik?> clslg-6" d="M83.69,148.35c0,1.84,.11,3.46,.69,5.83,1.02,4.14,3.49,9.79,4.36,11.71-.37,.53-.57,1.14-.58,1.78,0,.8,.29,1.56,.82,2.15-.51,.57-.81,1.3-.82,2.08,0,.91,.37,1.75,1.02,2.36-.46,.56-.73,1.25-.74,1.99-.02,1.83,1.58,3.35,3.57,3.37l12.89,.17h.05c.98,0,1.93-.38,2.61-1.03,.62-.61,.97-1.41,.98-2.25,0-.91-.37-1.74-1.02-2.36,.46-.55,.73-1.25,.73-1.99,0-.8-.29-1.56-.82-2.15,.51-.57,.81-1.3,.82-2.08,0-.81-.3-1.55-.81-2.14,.9-2.01,3.32-7.55,4.32-11.62,.57-2.37,.69-3.98,.69-5.83,0-7.93-6.45-14.37-14.37-14.37s-14.37,6.45-14.37,14.37Zm22.34,28.85c-.28,.27-.68,.44-1.11,.42l-12.89-.17c-.79,0-1.42-.54-1.42-1.18,0-.49,.4-.92,.97-1.08,.5-.13,.83-.59,.81-1.1-.02-.51-.4-.94-.91-1.02-.66-.11-1.16-.61-1.15-1.15,0-.46,.37-.89,.91-1.06,.45-.14,.76-.56,.77-1.04,0-.47-.3-.9-.76-1.04-.56-.18-.93-.62-.92-1.09,0-.21,.07-.38,.16-.51h15.25c.2,.2,.32,.45,.32,.72,0,.46-.37,.89-.91,1.05-.45,.14-.76,.56-.77,1.03,0,.48,.3,.9,.76,1.04,.56,.18,.93,.62,.92,1.09,0,.49-.4,.92-.97,1.08-.5,.13-.83,.59-.81,1.1,.02,.51,.4,.94,.91,1.02,.66,.11,1.16,.61,1.15,1.15,0,.33-.18,.57-.32,.72Zm-7.96-41.05c6.73,0,12.2,5.47,12.2,12.2,0,1.68-.1,3.16-.62,5.31-.96,3.92-3.4,9.49-4.24,11.35h-14.67c-.84-1.86-3.27-7.43-4.24-11.34-.52-2.16-.62-3.63-.62-5.32,0-6.73,5.47-12.2,12.2-12.2Z"/>
    <path class="<?=$kunik?> clslg-6" d="M83,123.29c-.21-.26-.51-.43-.85-.47-.33-.04-.67,.05-.94,.26-.27,.21-.44,.51-.47,.85-.04,.34,.05,.67,.26,.93l6.49,8.24c.24,.3,.61,.48,1,.48h.04c.27,0,.53-.1,.74-.27,.27-.21,.44-.51,.48-.85,.04-.34-.05-.67-.26-.94l-6.49-8.24Zm5.6,9.16s-.07,.04-.11,.04c-.06,0-.11-.02-.15-.07l-6.49-8.24s-.04-.1-.04-.13c0-.03,.02-.08,.07-.12,.04-.03,.08-.04,.11-.04,0,0,.02,0,.02,0,.03,0,.08,.02,.12,.07l6.49,8.24s.04,.1,.04,.13c0,.03-.02,.08-.07,.12Z"/>
    <path class="<?=$kunik?> clslg-6" d="M106.5,133.27h0c.21,.14,.45,.21,.7,.21h.04c.41-.01,.79-.23,1.01-.57l5.35-8.05c.19-.28,.26-.62,.19-.95-.07-.33-.26-.62-.54-.8-.28-.19-.62-.26-.95-.19-.33,.07-.62,.26-.81,.54l-5.35,8.05c-.19,.28-.25,.62-.19,.95,.07,.33,.26,.62,.54,.81Zm.55-1.16l5.35-8.05c.04-.05,.09-.07,.11-.08,.03,0,.08,0,.13,.03,.06,.04,.07,.09,.08,.12,0,.03,0,.08-.03,.14l-5.35,8.05c-.05,.07-.12,.08-.15,.08-.04,0-.07,0-.1-.03-.05-.04-.07-.09-.08-.12,0-.03,0-.08,.03-.14Z"/>
    <path class="<?=$kunik?> clslg-6" d="M97.8,131.39h.1c.66-.02,1.2-.56,1.22-1.22l.45-10.69c.01-.34-.1-.66-.33-.91-.23-.25-.54-.39-.88-.41-.34-.01-.66,.1-.91,.33-.25,.23-.4,.54-.41,.88l-.45,10.69c-.02,.34,.1,.66,.33,.91,.23,.25,.54,.39,.88,.41h0Zm-.13-1.28l.45-10.69c0-.07,.04-.11,.06-.13,.02-.02,.07-.05,.13-.05,.06,0,.1,.03,.13,.06,.02,.02,.05,.07,.05,.13l-.45,10.69c0,.09-.08,.17-.18,.17h0c-.06,0-.1-.03-.13-.06-.02-.02-.05-.07-.05-.13Z"/>
    <path class="<?=$kunik?> clslg-6" d="M90.35,146.32h0c.65,.04,1.23-.46,1.27-1.11,.02-.32,.07-.62,.14-.91,.5-1.97,2.1-2.74,3.35-3.04,.59-.14,1.02-.16,1.06-.16,.66-.02,1.18-.56,1.17-1.22-.01-.65-.55-1.17-1.22-1.17-2.24,.05-6.56,1.43-6.88,6.34-.04,.66,.45,1.23,1.11,1.27Z"/></g>
    <text/></svg>

<script>
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    if(data.length>0){
        $.each(data, function(index, d){
            $("#"+d["label"].replace(/\s/g, "")+"<?=$kunik?>").text(d["value"]+"%")
        })
    }
</script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "color01": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    },
                    "showLegend" : {
                        "label" : "Afficher la légende",
                        "inputType" : "checkboxSimple",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true,
                        "values" :  sectionDyf.<?php echo $kunik?>ParamsData.showLegend
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
