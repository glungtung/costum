<?php
    $graphAssets = [
        '/plugins/d3/d3.v6.min.js'
    ];

    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
    );
?>

<?php 
    $keyTpl     = "horizontalBar";
    // formulaire
    $paramsData = [
        "titleText" => "   ",
        "titleBottom" => "",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "percentColor" => "white",
        "completeColor" => "#F0506A"
    ];


    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

 ?>

 <style type="text/css">
     .yAxis {
        font-size: 12pt;
        word-break:keep-all;
    }
 </style>

<span class="sp-text margin-top-10" data-id="<?= $blockKey ?>" data-field="titleText"><?= $paramsData["titleText"] ?></span>
<div id="horizontalbar<?= $kunik ?>" class="text-center"></div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var data = [];
        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
            data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
        }
        var width = document.getElementById("horizontalbar<?= $kunik ?>").clientWidth,
        height = data.length*40,
        margin = {top: 20, right: 30, bottom: 40, left: (width/2)};

        // append the svg object to the body of the page
        var svg = d3.select("#horizontalbar<?= $kunik ?>")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
                .attr("class", "w-100")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var domainMax = 0;
        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"]!="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["percentageArray"]){
            domainMax = Math.max(...costum["dashboardData"]["<?= $blockKey ?>"]["percentageArray"]);
        }

        // Add X axis
        var x = d3.scaleLinear()
            .domain([0, domainMax+20])
            .range([ 0, width/2]);

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            //.call(d3.axisBottom(x))
            .selectAll("text")
                .attr("transform", "translate(-10,0)rotate(-45)")
                .style("text-anchor", "end");

        // Y axis
        var y = d3.scaleBand()
            .range([0, height ])
            .domain(data.map(function(d) { return d.label;}))
            .padding(.1);

        svg.append("g")
            .attr("class", "yAxis")
            .call(d3.axisLeft(y))

        //Bars
        var bars = svg.selectAll("barRect")
            .data(data)
            .enter()
            .append("g");
            
        bars.append("rect")
            .attr("x", x(0) )
            .attr("y", function(d) { return y(d.label); })
            .attr("width", function(d) { return x(d.value); })
            .attr("height", y.bandwidth() )
            .attr("fill", "<?= $paramsData["completeColor"] ?>");
            
        bars.append("text")
                .attr("x", function(d) {
                    const pos = (d.value <= 15)? -30: 5;
                    return x(d.value) - pos;
                })
                .attr("y", function(d) {
                    return y(d.label) + y.bandwidth()/2 + 2;
                })
                .attr("text-anchor", "end")
                .attr("font-weight", "bolder")
                .attr("fill", function(d){
                    const color = (d.value <= 15)? "<?= $paramsData["completeColor"] ?>": "<?= $paramsData["percentColor"] ?>";
                    return color;
                })
                .text(function(d) {
                    return Math.round(d.value)+"%";
                });


        var insertLinebreaks = function (t, d, width) {
            var el = d3.select(t);
            var p = d3.select(t.parentNode);

            var regExp = /\(([^)]+)\)/;
            var inParths = regExp.exec(d);
            var outParths = d;
            if(inParths == null){
                inParths = "";
            }

            if(typeof d == "string"){
                if(Array.isArray(inParths)){
                    outParths = d.replace(inParths[0], "");
                    inParths = inParths[0];
                }
            }

            p.append("foreignObject")
                .attr("x", -margin.left)
                .attr("y", -15)
                .attr("width", margin.left-10)
                .attr("height", height/2)
              .append("xhtml:div")
                .attr('style',"line-height:16px;text-align:right;color:<?= $paramsData["completeColor"] ?>")
                .html(outParths+"<small style='font-size:8pt;line-height:9px'>"+((inParths.length>60)?(inParths.substring(0, 66)+"...)"):inParths)+"</small>");

            el.remove();
        };

        d3.select('#horizontalbar<?= $kunik ?> g')
            .selectAll('text')
            .each(function(d,i){ if(typeof d == "string"){insertLinebreaks(this, d, width );} });

    });

</script>

<script type="text/javascript">
    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la bar chart"
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                    
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La paramètre est bien enregistrée");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>