<?php 
    $keyTpl     = "pourcentage03";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "emptyColor" => "#FF286B",
        "completeColor" => "#9B6FAC",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 877.33 467.21">
    <defs>
        <style>
            .cls3-1{fill:url(#radial-gradient-2);}
            .cls3-1,
            .cls3-2,
            .cls3-3,
            .cls3-4{mix-blend-mode:multiply;}
            .cls3-2{fill:url(#radial-gradient-3);}
            .cls3-3{fill:url(#radial-gradient-4);}
            .cls3-5{fill:#717272;font-size:57.11px;}
            .cls3-5,
            .cls3-6,
            .cls3-7{font-family:OpenSans-Bold, 'Open Sans';font-weight:700;}
            .cls3-8,
            .cls3-6{fill:#fff;}
            .cls3-9{fill:#c1c1c1;}
            .cls3-10{fill:#a5c145;}
            .cls3-11{fill:#ccc;}
            .cls3-12{fill:#95aa3f;}
            .cls3-13{filter:url(#drop-shadow-4);}
            .cls3-14{filter:url(#drop-shadow-2);}
            .cls3-15{filter:url(#drop-shadow-3);}
            .cls3-16{filter:url(#drop-shadow-1);}
            .cls3-6,
            .cls3-7{font-size:14.64px;}
            .cls3-7{fill:#4b5259;}
            .cls3-4{fill:url(#radial-gradient);}
            .cls3-17{isolation:isolate;}
        </style>
        <filter id="drop-shadow-1" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur" operator="in"/>
            <feComposite in="SourceGraphic"/>
        </filter>
            <radialGradient id="radial-gradient" cx="147.48" cy="241.43" fx="147.48" fy="241.43" r="62.96" gradientTransform="translate(361.02 55.86) rotate(80.43)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#000"/><stop offset="1" stop-color="#fff"/></radialGradient>
        <filter id="drop-shadow-2" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-2" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-2" operator="in"/>
            <feComposite in="SourceGraphic"/>
        </filter>
            <radialGradient id="radial-gradient-2" cx="2613.97" cy="2234.82" fx="2613.97" fy="2234.82" r="62.96" gradientTransform="translate(-820.23 3298.43) rotate(-103.38)" xlink:href="#radial-gradient"/>
        <filter id="drop-shadow-3" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-3" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-3" operator="in"/>
            <feComposite in="SourceGraphic"/>
        </filter>
            <radialGradient id="radial-gradient-3" cx="548.64" fx="548.64" r="62.96" gradientTransform="translate(656.55 -348.19) rotate(76.62)" xlink:href="#radial-gradient"/>
        <filter id="drop-shadow-4" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-4" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-4" operator="in"/>
            <feComposite in="SourceGraphic"/>
        </filter>
        <radialGradient id="radial-gradient-4" cx="3015.14" cy="2234.82" fx="3015.14" fy="2234.82" r="62.96" gradientTransform="translate(-1372.31 3573.89) rotate(-99.27)" xlink:href="#radial-gradient"/>
    </defs>
        <g class="cls3-17">
            <g id="Calque_1">
                <g>
                    
                <path class="cls3-11" d="M26.22,231.48c0-61.47,49.83-111.3,111.3-111.3s111.3,49.83,111.3,111.3H26.22Z"/><g class="cls3-16">
                        <path class="cls3-11" d="M137.53,142.13c-49.29,0-89.25,39.96-89.25,89.25v120.75c0,49.29,39.96,89.25,89.25,89.25s89.25-39.96,89.25-89.25v-120.75c0-49.29-39.96-89.25-89.25-89.25Z"/>
                </g>
            <text class="cls3-7" transform="translate(61.67 336.55)"><tspan x="0" y="0">Agrément Entreprise</tspan><tspan x="11.23" y="17.57">Solidaire d’Utilité </tspan><tspan x="26.03" y="35.15">Sociale (ESUS)</tspan></text>
            <circle class="cls3-4" cx="147.48" cy="241.43" r="62.96" transform="translate(-115.12 346.7) rotate(-80.43)"/>
            <circle class="cls3-8" cx="137.53" cy="231.48" r="59.72"/>
            <text class="cls3-5" transform="translate(105.13 253.9)"><tspan id="AgrémentEntrepriseSolidairedUtilitéSocialeESUS" x="0" y="0">01</tspan></text><g>
            <path class="cls3-9" d="M154.09,93.85c7.34-6.22,11.93-15.59,11.66-26.02-.45-17.09-14.47-31.32-31.55-32.02-18.81-.76-34.3,14.26-34.3,32.9,0,10.08,4.54,19.1,11.67,25.14l.05,.04c5.89,4.98,13.35,7.75,21.06,7.75h.15c4.86,0,9.47-1.06,13.63-2.95,0,0,5.27-2.29,7.63-4.84Zm-44.61-25.14c0-12.9,10.46-23.36,23.36-23.36s23.36,10.46,23.36,23.36-10.46,23.36-23.36,23.36-23.36-10.46-23.36-23.36Z"/>
            <path class="cls3-9" d="M135.61,69.86v-15.08c0-1.3-1.06-2.36-2.36-2.36s-2.36,1.06-2.36,2.36v13.24l-5.76,5.76c-.92,.92-.92,2.41,0,3.34,.92,.92,2.41,.92,3.34,0l7.08-7.08h.06v-.06l.06-.06-.06-.06Z"/></g>
            <path class="cls3-12" d="M850.53,231.25c0,61.47-49.83,111.3-111.3,111.3s-111.3-49.83-111.3-111.3h222.61Z"/><g class="cls3-14">
                <path class="cls3-12" d="M739.23,320.61c49.29,0,89.25-39.96,89.25-89.25V110.6c0-49.29-39.96-89.25-89.25-89.25s-89.25,39.96-89.25,89.25v120.75c0,49.29,39.96,89.25,89.25,89.25Z"/></g>
            <circle class="cls3-1" cx="748.98" cy="238.23" r="62.96" transform="translate(343.89 911.75) rotate(-76.62)"/>
            <circle class="cls3-8" cx="739.23" cy="231.25" r="59.72"/>
            <text class="cls3-5" transform="translate(709.45 253.9)"><tspan id="Autreagrément" x="0" y="0">04</tspan></text>
            <path class="cls3-9" d="M736.37,420.46c-9.7,0-17.57-7.86-17.57-17.57s7.86-17.57,17.57-17.57,17.57,7.86,17.57,17.57-7.86,17.57-17.57,17.57Zm33.72-11.18v-12.77h-5.64c-.73-3.23-2-6.24-3.72-8.95l3.99-3.99-9.03-9.03-3.99,3.99c-2.71-1.71-5.73-2.99-8.95-3.72v-5.64h-12.77v5.64c-3.23,.73-6.24,2-8.95,3.72l-3.99-3.99-9.03,9.03,3.99,3.99c-1.71,2.71-2.99,5.73-3.72,8.95h-5.64v12.77h5.64c.73,3.23,2,6.24,3.72,8.95l-3.99,3.99,9.03,9.03,3.99-3.99c2.71,1.71,5.73,2.99,8.95,3.72v5.64h12.77v-5.64c3.23-.73,6.24-2,8.95-3.72l3.99,3.99,9.03-9.03-3.99-3.99c1.71-2.71,2.99-5.73,3.72-8.95h5.64Zm-33.72,2.21c-4.75,0-8.59-3.85-8.59-8.59s3.85-8.59,8.59-8.59,8.59,3.85,8.59,8.59-3.85,8.59-8.59,8.59Z"/>
            <path class="cls3-10" d="M427.39,231.48c0-61.47,49.83-111.3,111.3-111.3s111.3,49.83,111.3,111.3h-222.61Z"/><g class="cls3-15">
                <path class="cls3-10" d="M538.69,142.13c-49.29,0-89.25,39.96-89.25,89.25v120.75c0,49.29,39.96,89.25,89.25,89.25s89.25-39.96,89.25-89.25v-120.75c0-49.29-39.96-89.25-89.25-89.25Z"/></g>
            <circle class="cls3-2" cx="548.64" cy="241.43" r="62.96" transform="translate(186.79 719.31) rotate(-76.62)"/>
            <circle class="cls3-8" cx="538.69" cy="231.48" r="59.72"/>
            <text class="cls3-5" transform="translate(508.86 253.9)"><tspan id="Ateliersetchantiersd’insertionACI" x="0" y="0">03</tspan></text>
            <path class="cls3-9" d="M538.9,39.03c-16.11,0-29.17,13.06-29.17,29.17,0,6.23,1.97,11.99,5.29,16.72l-4.57,13.48,14.15-4.8c4.23,2.38,9.1,3.76,14.3,3.76,16.11,0,29.17-13.06,29.17-29.17s-13.06-29.17-29.17-29.17Zm-12.27,32.3c-1.78,0-3.23-1.44-3.23-3.22s1.44-3.22,3.23-3.22,3.22,1.44,3.22,3.22-1.44,3.22-3.22,3.22Zm13.16,0c-1.78,0-3.23-1.44-3.23-3.22s1.44-3.22,3.23-3.22,3.22,1.44,3.22,3.22-1.44,3.22-3.22,3.22Zm13.16,0c-1.78,0-3.22-1.44-3.22-3.22s1.44-3.22,3.22-3.22,3.22,1.44,3.22,3.22-1.44,3.22-3.22,3.22Z"/>
            <path class="cls3-12" d="M449.37,231.25c0,61.47-49.83,111.3-111.3,111.3s-111.3-49.83-111.3-111.3h222.61Z"/><g class="cls3-13">
                <path class="cls3-12" d="M338.07,320.61c49.29,0,89.25-39.96,89.25-89.25V110.6c0-49.29-39.96-89.25-89.25-89.25s-89.25,39.96-89.25,89.25v120.75c0,49.29,39.96,89.25,89.25,89.25Z"/></g>
            <circle class="cls3-3" cx="347.82" cy="238.23" r="62.96" transform="translate(56.69 543.15) rotate(-80.73)"/>
            <circle class="cls3-8" cx="338.07" cy="231.25" r="59.72"/>
            <text class="cls3-5" transform="translate(305.72 253.9)"><tspan id="AgrémentCAFentantqueCentreSocialouEspacedeVieSociale" x="0" y="0">02</tspan></text><g>
                <path class="cls3-9" d="M330.88,371.3c-14.75,.81-26.64,13.14-26.95,27.91-.2,9.25,4.02,17.5,10.67,22.85,2.72,2.18,4.55,5.23,5.08,8.6h25.64c.54-3.38,2.38-6.43,5.1-8.62,6.49-5.23,10.66-13.23,10.66-22.22,0-16.32-13.68-29.44-30.19-28.53Zm1.07,9.41c0,1.3-.94,2.38-2.21,2.65-7.22,1.55-12.91,7.44-14.24,14.71-.23,1.27-1.29,2.22-2.58,2.22h-.29c-1.7,0-2.94-1.54-2.65-3.21,1.66-9.59,9.15-17.38,18.66-19.4,1.69-.36,3.3,.9,3.3,2.63v.4Z"/>
            <path class="cls3-9" d="M347.47,433.32h-30.12c-1.76,0-3.19,1.43-3.19,3.19s1.43,3.19,3.19,3.19h7.56c.71,3.49,3.8,6.11,7.5,6.11s6.79-2.62,7.5-6.11h7.56c1.76,0,3.19-1.43,3.19-3.19s-1.43-3.19-3.19-3.19Z"/></g>
            <text class="cls3-6" transform="translate(255.4 110.39)"><tspan x="0" y="0">Agrément CAF en tant </tspan><tspan x="17.09" y="17.57">que Centre Social </tspan><tspan x="71.17" y="35.15">ou </tspan><tspan x="6" y="52.72">Espace de Vie Sociale</tspan></text>
            <text class="cls3-6" transform="translate(680.92 110.39)"><tspan x="0" y="0">Autre agrément</tspan></text>
            <text class="cls3-7" transform="translate(463.75 336.55)"><tspan x="0" y="0">Ateliers et chantiers </tspan><tspan x="17.72" y="17.57">d’insertion (ACI)</tspan></text></g></g></g>
</svg>
<script>
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    if(data.length>0){
        $.each(data, function(index, d){
            try{
                $("#"+d["label"].replace(/[()/]|\'|\s/g, "")).text(d["value"]+"%")
            }catch(e){

            }
        })
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
