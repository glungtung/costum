<?php
$keyTpl = "mapd3region";
$paramsData = [
    "cluster" => "pie",
    "marker" => "default",
    "showLegende" => 'true',
    "dynamicLegende" => 'true',
    "legendConfig" => [],
    "legendeVar" => "type",
    "legendeCible" => "",
    "legendeLabel" => "Type",
    "useFilters" => 'false',
    "filtersConfig" => [],
    "linkData" => "co2/search/globalautocomplete"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms["graph"][$e]) ) {
            $paramsData[$e] = gettype($blockCms["graph"][$e]) == "boolean" ? json_encode($blockCms["graph"][$e]) : $blockCms["graph"][$e];
        }
    }
}
?>

<style>
    #mapD3{
        position: relative;
    }
    #mapd3region<?= $kunik ?>{
        height: 700px;
        position: relative;
    }
    .chart-legende-container{
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999999;
        background: white;
    }
</style>
<div id="app-map-filters" class="searchObjCSS"></div>
<div id="mapD3">
    <div id="mapd3region<?= $kunik ?>">
    
    </div>
</div>
<script>
    var appMap = null;
    var filterSearch;
    var useFilter = <?= $paramsData["useFilters"]; ?>;
    var legende = <?= gettype($paramsData["legendConfig"]) == "array" ? json_encode($paramsData["legendConfig"]) : $paramsData["legendConfig"]?>;
    var dynamicLegende = true;
    $(function(){
        var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "mapbox"};
        var mapOptions = {
            container : "#mapd3region<?= $kunik ?>",
            activePopUp : true,
            clusterType : '<?= $paramsData["cluster"] ?>',
            markerType : '<?= $paramsData["marker"] ?>',
            showLegende: '<?= $paramsData["showLegende"] ?>',
            dynamicLegende: '<?= $paramsData["dynamicLegende"] ?>',
            mapOpt:{
                zoomControl:false
            },
            mapCustom:customMap,
            elts : {}
        };
        if(dynamicLegende == true){
            mapOptions.groupBy = '<?= $paramsData["legendeVar"] ?>';
            mapOptions.legendeLabel = '<?= $paramsData["legendeLabel"] ?>';
        }else{
            mapOptions.legende = legende.map(function(d){
                return d.label;
            });
            mapOptions.legendeVar = '<?= $paramsData["legendeVar"] ?>';
            mapOptions.legendeLabel = '<?= $paramsData["legendeLabel"] ?>';
        }
        appMap = new MapD3(mapOptions);
        if(useFilter){
            var filterUser = <?= json_encode($paramsData["filtersConfig"]) ?>;
            if(typeof filterUser.filters != "undefined"){
                filterUser.mapCo = appMap;
                filterUser.container = "#app-map-filters";
                filterUser.layoutDirection = "vertical";
                filterSearch = searchObj.init(filterUser);
                filterSearch.search.init(filterSearch);
            }else{
                filterUser = {
                    "options" : {
                        "tags" : {
                            "verb" : "$and"
                        }
                    },
                    "results" : {
                        "renderView" : "directory.elementPanelHtml",
                        "smartGrid" : true,
                        "map" : {
                            "active" : true
                        }
                    },
                    "defaults" : {
                        "indexStep" : 0,
                        "types" : [ 
                            "organizations"
                        ]
                    },
                    "filters" : {},
                    container:"#app-map-filters",
                    layoutDirection:"vertical",
                    mapCo:appMap,
                    results:{
                        multiCols:false,
                        map:{
                            active:true
                        }
                    },
                    mapContent:{
                        hideViews: ["btnHideMap"]
                    }
                };
                filterSearch = searchObj.init(filterUser);
                filterSearch.search.init(filterSearch);
            }
        }else{
            var url = '<?= $paramsData['linkData'] ?>';
            var params = {};
            if(url.indexOf("globalautocomplete") > -1){
                var defaultFilters<?= $kunik ?> = {'$or':{}};
                defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
                defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
                /*if(costum.contextType!="projects"){
                    defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
                }*/
                defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
                defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
                var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
                params = {
                    notSourceKey: false,
                    searchType : <?= json_encode(['organizations']) ?>,
                    fields : mapSearchFields,
                    indexStep: 50,
                    filters: defaultFilters<?= $kunik ?>
                };
            }else{

            }
            ajaxPost(
                null,
                (url.indexOf("http") > -1 ? url : baseUrl + "/" + url),
                params,
                function(data){
                    appMap.clearMap();
                    appMap.addElts(data.results);
                }
            )
        }
    });
</script>

<script type="text/javascript">
    $(function(){
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "marker": {
                        "label" : "<?= Yii::t("graph", "Type of marker") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"default": "Par défaut","pie": "Pie Chart", "bar": "Bar Chart", "donut": "Donut Chart", "geoshape": "GéoGraphe"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "cluster": {
                        "label" : "Type de cluster",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"pie": "Pie Chart", "bar": "Bar Chart", "donut": "Donut Chart"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "showLegende" : {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher de legende",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.showLegende
                    },
                    "dynamicLegende" : {
                        "inputType" : "checkboxSimple",
                        "label" : "Legende automatique",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.dynamicLegende
                    },
                    "legendeVar":{
                        "label" : "Variable du légende sur les données",
                        "inputType" : "text",
                        "placeholder":"ex: type, tags",
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendeVar
                    },
                    "legendeCible":{
                        "inputType" : "select",
                        "label" : "<?= Yii::t("graph", "Legend on") ?>",
                        "options" : Object.keys((costum.lists||{})).reduce((a,b)=> {
                            return (a[b]=b, a);
                        },{})
                    },
                    "legendConfig":{
                        "label" : "Préférence sur le légende",
                        "inputType" : "lists",
                        "entries" : {
                            "label" : {
                                "type" : "text",
                                "label": "label",
                                "class": "col-sm-10",
                                "placeholder":"Libellé"
                            }
                        },
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendConfig
                    },
                    "legendeLabel":{
                        "label" : "<?= Yii::t("graph", "Name of the legend") ?>",
                        "inputType" : "text",
                        "placeholder":"ex: Taille",
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendeLabel
                    },
                    "useFilters" : {
                        "inputType" : "checkboxSimple",
                        "label" : "Utiliser le filtre",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.useFilters
                    },
                    "filtersConfig":{
                        "label" : "Configuration du filtre",
                        "inputType" : "textarea",
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.filtersConfig
                    },
                    "linkData":{
                        "label" : "URL de la Data ",
                        "inputType" : "text",
                        "placeholder":"Defaut : co2/search/globalautocomplete",
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.linkData
                    },
                },
                beforeBuild: function(){
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        {
                            level: [3]
                        }, function(data){
                            mylog.log("data for map", data);
                        }, function(data){
                            mylog.log('filertObj.initFilters ajaxPost error', data);
                    });
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik?>ParamsData.useFilters!="true" && sectionDyf.<?php echo $kunik?>ParamsData.useFilters!=true){
                        $(".filtersConfigtextarea").hide();
                        $(".linkDatatext").show();
                    }else{
                        $(".filtersConfigtextarea").show();
                        $(".linkDatatext").hide();
                    }
                    if(sectionDyf.<?php echo $kunik?>ParamsData.dynamicLegende!="true" && sectionDyf.<?php echo $kunik?>ParamsData.dynamicLegende!=true){
                        $(".legendConfiglists").show();
                    }else{
                        $(".legendConfiglists").hide();
                    }
                    $("#legendeCible").on("change", function(){
                        var legendeCible = $(this).val();
                    })
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value["graph"][k] = $("#"+k).val();
                        if (k == "legendConfig"){
                            tplCtx.value["graph"][k] = [];
                            $.each(data.legendConfig, function(index, va){
                                tplCtx.value["graph"][k].push(va);
                            })
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        });
                    }
                }
            }
        }
        costum.checkboxSimpleEvent = {
            true : function(id){
                if(id=="dynamicLegende"){
                    $("#ajaxFormModal .legendConfiglists").hide();
                }
                if(id=="useFilters"){
                    $("#ajaxFormModal .filtersConfigtextarea").show();
                    $("#ajaxFormModal .linkDatatext").hide();
                }
            },
            false : function(id){
                if(id=="dynamicLegende"){
                    $("#ajaxFormModal .legendConfiglists").show();
                }
                if(id=="useFilters"){
                    $("#ajaxFormModal .filtersConfigtextarea").hide();
                    $("#ajaxFormModal .linkDatatext").show();
                }
            }
        }
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>