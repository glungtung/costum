<?php

    $graphAssets = [
        '/plugins/d3/d3.v6.min.js'
    ];

    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
    );

    $keyTpl     = "progressCircleMultiple";
    $paramsData = [
        "titleTop" => "",
        "titleBottom" => "",
        "textValueRight" => "",
        "textColor" => "#000000",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "percentColor" => "#F0506A",
        "emptyColor" => "#ccc",
        "completeColor" => "#F0506A",
        "figure" => "",
        "nombre" => 6,
        "textRight" => false
    ];

    $initFigure = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"figure"
        ), "image" );

    $figureFile = [];

    foreach ($initFigure as $key => $value) {
        $figureFile[] = $value["imagePath"];
    }

    /*var_dump($figureFile); */

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
 ?>
<style>
    .flex-center{
        justify-content: center;
        display: flex;
        flex-wrap: wrap;
    }
    #chartContainer<?=$kunik?> .label-text-color {
        color: <?= $paramsData["textColor"] ?>;
    }

    .circle-content<?=$kunik?> {
        width: <?= 100/$paramsData["nombre"] ?>%;
    }

    @media screen and (max-width: 991.99px) {
        .circle-content<?=$kunik?> {
            width: 25%;
        }
    }
    @media screen and (max-width: 767.99px) {
        .circle-content<?=$kunik?> {
            width: 33.33%;
        }
    }
    @media screen and (max-width: 575.99px) {
        .circle-content<?=$kunik?> {
            width: 100%;
        }
    }
</style>
<?php if(!isset($paramsData["answerPath"]) || $paramsData["answerPath"]==""){ ?>
    <div class="text-center">
        <div class="alert alert-danger">Paramètre manquant :  answerPath</div>
    </div>
<?php } ?>
<?php if(!isset($paramsData["answerValue"]) || $paramsData["answerValue"]==""){ ?>
    <div class="text-center">
        <div class="alert alert-danger">Paramètre manquant :  answerValue</div>
    </div>
<?php } ?>
<div id="chartContainer<?=$kunik?>" class="chartviz margin-top-10">
        <?php if(isset($paramsData["answerValue"]) && count($paramsData["answerValue"])>0 ){ 
            echo '<div id="arc'.$kunik.'" class="row flex-center">';
            
            foreach ($paramsData["answerValue"] as $key => $value) {?>
                <div id="<?= 'circle'.$kunik.$key ?>" data-label="<?= $value ?? "" ?> <?=$kunik?>" class="circle-content<?=$kunik?> text-center padding-bottom-10"></div>
        <?php } 
            echo "</div>";
        }else{
            echo "<h4>Votre données sera affiché ici</h4>";
        } ?>
    
</div>
<table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz margin-top-10"></table>

<script type="text/javascript">
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    
    //jQuery(document).ready(function() {
        var progressCircle<?= $kunik ?> = {
            width: 120,
            height: 120,
            pi : 2 * Math.PI,
            data: { percentColor:"<?= $paramsData['percentColor'] ?>", emptyColor:"<?= $paramsData['emptyColor'] ?>", completeColor:"<?= $paramsData['completeColor'] ?>"},
            arc : null,
            parent:null,
            init: function(procir, DOMselector){
                procir.arc = d3.arc()
                    .innerRadius(40)
                    .outerRadius(60) // arc width
                    .startAngle(0);

                procir.parent = d3.select(DOMselector).append("svg")
                    .attr("width", procir.width)
                    .attr("height", procir.height);

                procir.draw(procir, procir.data);
            },
            initData:function(procir, d){
                procir.data.id = d.label+"<?=$kunik ?>";
                procir.data.percent = d.value||0;
            },
            draw: function(procir, data){
                var d3Ring = procir.parent  
                .append("g")
                .attr("transform", "translate("+ procir.width/2 +"," +60+")")
                .attr("id",data.id);
            
                // Background
                d3Ring
                .append("path")
                .datum({endAngle: procir.pi})
                .style("fill", data.emptyColor)
                .attr("d", procir.arc); 
                
                // Foreground
                var foreground = d3Ring
                .append("path")
                .datum({endAngle: 0})
                    .style("stroke", "none")
                    .style("stroke-width", "0px")
                    .style("opacity", 1)
                .attr("d", procir.arc)
                .attr("fill", data.completeColor);
                
                // Text
                d3Ring
                .append("text")
                .attr("x", -20)
                .attr("y", 8) 
                .style("font-size", 25)
                .style("font-family", "Georgia, Arial, sans-serif")
                .style("font-weight", "bolder")
                .style("fill", data.percentColor)
                .text(data.percent + "%");
                    
                var angle = procir.helpers.convertPercentToAngle(procir);
                
                // Animation
                foreground
                .transition()
                  .duration(1500)
                        .delay(500)
                  .call(procir.arcTween, procir, angle);
            },
            arcTween: function(transition, procir, newAngle){
                transition.attrTween("d", function(d) {  
                    var interpolate = d3.interpolate(d.endAngle, newAngle);
                    return function(t) {
                        d.endAngle = interpolate(t);
                        return procir.arc(d);
                    };
                });
            },
            helpers:{
                convertPercentToAngle: function(procir){
                    return ( procir.data.percent / 100 ) * procir.pi
                }
            }
        }

        if(data.length>0){
            $("#tableData<?=$kunik?>").empty();
            $.each(data, function(index, value){
                var procircle = '*[data-label="'+value.label+' <?=$kunik?>"';
                progressCircle<?= $kunik ?>.initData(progressCircle<?= $kunik ?>, value);
                progressCircle<?= $kunik ?>.init(progressCircle<?= $kunik ?>, procircle);
                $(procircle).append('<div class="text-center label-text-color" title="'+value.label+' ( '+value.value+' )" style="height:55px; overflow:hidden">'+value.label+'</div>');
                $("#tableData<?=$kunik?>").append(`<tr>
                    <td>${value.label}</td>
                    <td class="tableValue">${value.value}</td>
                </tr>`)
            })
        }
        $("#tableData<?=$kunik?>").hide();
    //});
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {

        sectionDyf.<?=$kunik ?>ParamsData = <?=json_encode( $paramsData ); ?>;

        sectionDyf.<?=$kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?=$kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?=$kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?=$kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?=$kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?=$kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?=$kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "textColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du label"
                    },
                    "nombre": {
                        "inputType" : "number",
                        "label" : "Nombre d'element par ligne"
                    },
                    "emptyColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc vide"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc rempli"
                    },
                    "figure" :{                    
                        "inputType" : "uploader",
                        "docType": "image",
                        "contentKey":"slider",
                        "endPoint": "/subKey/figure",
                        "domElement" : "figure",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        "label": "Figure :",
                        "itemLimit" : 1,
                        "showUploadBtn": false,
                        initList : <?=json_encode($initFigure); ?> 
                    },
                    "textRight": {
                        "inputType" : "checkboxSimple",
                        "label" : "Texte à droit du cercle",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : false,
                        "value":sectionDyf.<?=$kunik ?>ParamsData.textRight
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?=$kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?=$kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?=$kunik ?> option[value='"+sectionDyf.<?=$kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?=$kunik ?>").val(sectionDyf.<?=$kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?=$kunik ?>").change();
                                $("#answerValue.<?=$kunik ?>").val(sectionDyf.<?=$kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?=$kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?=$kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params, function(){
                                toastr.success("La paramètre est bien enregistrée");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            });
                        });
                    }
                }
            }
        }

        $(".edit<?=$kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?=$kunik ?>Params,null, sectionDyf.<?=$kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?=$kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?=$kunik ?>", function(){
            $("#answerValue.<?=$kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?=$kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?=$kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(typeof input !="undefined" && (input.includes("multiRadio") || input.includes("multiCheckboxPlus"))){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    $("#answerValue.<?=$kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?=$kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?=$kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?=$kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?=$kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>