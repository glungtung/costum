<?php
    $keyTpl = "chartForCoform";

    // formulaire
    $paramsData = [
        "titleText" => "   ",
        "titleBottom" => "",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "percentColor" => "white",
        "completeColor" => "#F0506A",
        "chartType" => "bar",
        'legendPosition'=>"right",
        'showLegend'=>true,
    ];


    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

 ?>
<style>
    .tableValue{
        font-size:14pt;
        font-weight:bold;
    }
</style>

<?php if(!isset($paramsData["answerPath"]) || $paramsData["answerPath"]==""){ ?>
    <div class="text-center">
        <div class="alert alert-danger">Paramètre manquant :  answerPath</div>
    </div>
<?php } ?>

<?php if(!isset($paramsData["answerValue"]) || $paramsData["answerValue"]==""){ ?>
    <div class="text-center">
        <div class="alert alert-danger">Paramètre manquant :  answerValue</div>
    </div>
<?php } ?>

<center>
    <!--div class="btn-group padding-top-10 padding-bottom-20">
        <button id="showDiagramme<?=$kunik?>" data-visualization="chart" class="btn btn-default switch">Visualisation en Diagramme</button>
        <button id="showTable<?=$kunik?>" data-visualization="table" class="btn btn-default switch">Visualisation en Tableau</button>
    </div-->
    <div id="chartContainer<?=$kunik?>" class="chartviz">
        <canvas id="chart<?=$kunik?>" height="<?=(in_array($paramsData["chartType"], ["doughnut", "pie"]))?"80px":"90px"?>" ></canvas>
    </div>
    <table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz"></table>
</center>

<script src="/plugins/Chart-2.8.0/Chart.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var data = [];
        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
            data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
        }

        if(data.length>0){
            $("#tableData<?=$kunik?>").empty();
            $.each(data, function(index, value){
                $("#tableData<?=$kunik?>").append(`<tr>
                    <td>${value.label}</td>
                    <td class="tableValue">${value.value}</td>
                </tr>`)
            })
        }

        $("#tableData<?=$kunik?>").hide();
        
        var ctx = document.getElementById('chart<?=$kunik?>').getContext('2d');
        
        const groupByKey = function(arr, key){
            let arrayFromKeyOfObj = [];
            $.each(arr, function(k, v){
                let value = v[key];
                if(typeof value == "string"){
                    value = value.substr(0, 20)+((value.length>20)?"...":"");
                }
                arrayFromKeyOfObj.push(value);
            })
            return arrayFromKeyOfObj;
        }

        const addCountOnLabel = function(data){
            fullLabels = [];
            $.each(data, function(index,d){
                const v = ("( "+d.value+" ) "+d.label)
                fullLabels.push(v.substring(0, 25)+((v.length >25)?"...":""));
            })
            return fullLabels;
        }

        var dataFormated = {
            labels: addCountOnLabel(data),
            datasets: [
                {
                    label: "",
                    backgroundColor: '<?=$paramsData["completeColor"]?>',//"rgba(255,99,132,0.2)",
                    borderColor:  '<?=$paramsData["completeColor"]?>',
                    borderWidth: 2,
                    hoverBackgroundColor:  '<?=$paramsData["completeColor"]?>',
                    hoverBorderColor:  '<?=$paramsData["completeColor"]?>',
                    data: groupByKey(data, 'value'),
                }
            ]
        };

        var chartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        suggestedMax: Math.max(...groupByKey(data, 'value'))
                    }
                }]
            },
            legend:{
                display: <?= (isset($paramsData['showLegend']) && $paramsData['showLegend']!="" && $paramsData['showLegend']=="true" && $paramsData['chartType']!="bar")?"true":"false" ?>,
                position: "<?= $paramsData['legendPosition'] ?>",
            }
        }

        <?php if(in_array($paramsData["chartType"], ["pie","doughnut"])){ ?>
            dataFormated.datasets[0] = {
                ...dataFormated.datasets[0],
                backgroundColor: generateColor(data),//"rgba(255,99,132,0.2)",
                borderColor:  generateColor(data),
                hoverBackgroundColor:  generateColor(data),
                hoverBorderColor:  generateColor(data),
            }
            chartOptions.scales = {};
        <?php } ?>

        var myChart = new Chart(ctx,{
            type: '<?=($paramsData["chartType"]!="")?$paramsData["chartType"]:"bar"?>',
            data:dataFormated,
            options: chartOptions
        });

        function generateColor(d){
            var generatedColor = [];
            if(d){
                generatedColor = Object.keys(d).map(function(v, i){
                    //let hash = 0;
                    /*for (let k = 0; k < v.length; k++) {
                        hash = v.charCodeAt(k) + ((hash << 16) - hash);
                        //hash = hash & hash;
                    }*/
                    let per = (i)*100/(Object.keys(d).length);
                    return `hsl(${(i*34)+137}, 50%, 50%)`;// uni-color
                    //return `hsl(${Math.round(per)}, ${50}%, ${50}%)`;
                });
            }
            return generatedColor;
        }

    })
</script>


<script type="text/javascript">
    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "chartType": {
                        "inputType" : "select",
                        "class":"select2 form-control",
                        "label" : "Type du chart",
                        "options":{"bar":"Bar", "doughnut":"Circulaire", "line":"Line", "pie":"Pie"}
                    },/*
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },*/
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    },
                    "showLegend" : {
                        "label" : "Afficher la légende",
                        "inputType" : "checkboxSimple",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik?>ParamsData.showLegend
                    },
                    "legendPosition" :{
                        "inputType" : "select",
                        "label" : "Position des légendes",
                        "values" : sectionDyf.<?=$kunik ?>ParamsData.legendPosition,
                        "class" : "form-control",
                        "options" : {
                            "top" : "En Haut",
                            "bottom" : "En Bas",
                            "right" : "Droite",
                            "left" : "Gauche"
                        }
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>
