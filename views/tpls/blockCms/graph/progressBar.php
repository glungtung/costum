<?php 
    $keyTpl     = "progressBar";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "emptyColor" => "#FF286B",
        "completeColor" => "#9B6FAC",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
 ?>

<style type="text/css">
    .progress-contain<?= $kunik ?>{
        margin-top: <?= -5 + (($paramsData['labelSize']-16)*2+35-$paramsData["progressBarHeight"]) ?>px;
    }
    .progress-contain<?= $kunik ?> .progress-bar {
        text-align: left;
        white-space: nowrap;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        cursor: pointer;
        height: <?= $paramsData["progressBarHeight"] ?>px;
        z-index: 0;
        /*margin-top: <?= 35-$paramsData["progressBarHeight"] ?>px;*/
        background-color: <?= $paramsData["emptyColor"] ?>;
    }
    .progress-contain<?= $kunik ?> .progress-type {
        font-size: 16pt;
        padding-left: <?= (($paramsData["progressBarHeight"]<30)?0: 1) ?>em;
        padding-bottom: 1em;
        z-index: 4;
        position: absolute;
        margin-top: <?= - (($paramsData['labelSize']-16)*2 + 35-$paramsData["progressBarHeight"]) ?>px;
        color: <?= $paramsData["percentColor"] ?>;
    }
    .progress-contain<?= $kunik ?> .progress {
        background-color: <?= $paramsData["completeColor"]; ?>;
        height: <?= $paramsData["progressBarHeight"] ?>px;
        z-index: 0;
        /*margin-top: <?= 35-$paramsData["progressBarHeight"] ?>px;*/
        border-radius: 2px;
    }


     .flex-container {
        display: flex;
        flex-direction: row;
    }

    .flex-left<?= $kunik ?> {
        position: relative;
        font-weight: bolder;
        font-size: <?= $paramsData['labelSize']; ?>pt;
    }
/*
.flex-right {
    width: 25%;
}*/
</style>

<div class="progress-contain<?= $kunik ?> padding-0">
    <div class="progress-type flex-container">
        <span class="flex-left<?= $kunik ?> padding-right-10"></span>
        <span class="flex-right">
            <span class="sp-text" data-id="<?= $blockKey ?>" data-field="textOnProgressBar"><?= $paramsData["textOnProgressBar"] ?></span>
        </span>
    </div>
    <div class="mp-progress padding-0">
        <div class="progress progress<?= $kunik ?>"></div>
        <?php if($paramsData["withStaticTextBottom"]==true || $paramsData["withStaticTextBottom"]=="true"){ ?>
            <div class="sp-text margin-top-10" data-id="<?= $blockKey ?>" data-field="label"><?= $paramsData["label"] ?></div>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        let countAllAnswers = 1;
        let progressValue = 0;

        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] != "undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["countAllAnswers"]){
            countAllAnswers = costum["dashboardData"]["<?= $blockKey ?>"]["countAllAnswers"];
        }

        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] != "undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["value"]){
            progressValue = costum["dashboardData"]["<?= $blockKey ?>"]["value"];
        }

        const percentageValue = Math.round((progressValue*100)/countAllAnswers);


        $(".flex-left<?= $kunik ?>").text(percentageValue+" %");
        if(percentageValue!=0){
            $(".progress<?= $kunik ?>").append(`
            <div class="progress-bar" role="progressbar" aria-valuenow="${percentageValue}" aria-valuemin="0" aria-valuemax="100" style="width: ${percentageValue}% !important;" data-toggle="tooltip" data-placement="top" title="${percentageValue||0}%">
                <span class="sr-only">
                    ${percentageValue}% Complete
                </span>
            </div>
        `)
        }

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "emptyColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc rempli"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc vide"
                    },
                    "labelSize": {
                        "inputType" : "text",
                        "rules":{"number":true},
                        "label" : "Taille du texte dans le barre"
                    },
                    "progressBarHeight": {
                        "inputType" : "text",
                        "rules":{"number":true},
                        "label" : "Couleur de l'arc vide"
                    },
                    "withStaticTextBottom": {
                        "inputType" : "checkboxSimple",
                        "label" : "Texte en bas",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                    
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La paramètre est bien enregistrée");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                    if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                        for(const paramValue of coform["params"][input]["global"]["list"]){
                            $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                        }
                    }
                }

                if(input.includes("checkboxNew") || input.includes("radioNew")){
                    for(const paramValue of coform["params"][input]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>
