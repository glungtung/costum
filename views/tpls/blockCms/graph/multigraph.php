<?php
if (!isset($costum)) {
  $costum = CacheHelper::getCostum();
}

$keyTpl     = "multigraph";
$paramsData = [
  "dataSource" => [$costum["contextId"] => ["name" => $costum["contextSlug"], "type" => $costum["contextType"]]],
  "externalData" => [
    ["label" => "Donnée ext 1", "value" => "2", "color" => "#e69138"],
    ["label" => "Donnée ext 2", "value" => "3", "color" => "#93c47d"],
    ["label" => "Donnée ext 3", "value" => "5", "color" => "#744700"]
  ],
  "internalTitle" => "title",
  "internalTitleBackgroundColor" => "#00723F",
  "internalBackgroundColor" => "#FF0014",
  "labelCircleColor" => "#77B82A",
  "internalData" => [
    ["label" => "Donnée 1", "value" => "1;2", "color" => "#e69138"],
    ["label" => "Donnée 2", "value" => "3;2", "color" => "#93c47d"],
    ["label" => "Donnée 3", "value" => "10;3", "color" => "#744700"]
  ],
  "externalTitle" => "title",
  "externalTitleBackgroundColor" => "#00723F",
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (isset($blockCms[$e])) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}

?>


<?php
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
  $graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>


<style>
  #graph-container-<?= $kunik ?> {
    height: 100%;
    width: 100%;
    overflow: hidden;
    min-height: 100px;
  }

  .super-cms .graph-panel {
    background-color: transparent !important;
    width: 100%;
    height: 100%;
  }
</style>
<!-- <button class="edit<?php echo $kunik ?>Params" data-collection="cms" data-id="<?= (string) $blockCms["_id"] ?>">Dynform</button> -->
<div id="graph-container-<?= $kunik ?>" class="graph-panel">

</div>

<script>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  //Avoid change DATA IN sectionDyf.<?php echo $kunik ?>ParamsData variable
  var internalData = <?php echo json_encode($paramsData["internalData"]); ?>;
  var circularGraph = new MultiGraph([]);
  circularGraph.setInternalTitleBackgroundColor(sectionDyf.<?php echo $kunik ?>ParamsData.internalTitleBackgroundColor)
  circularGraph.setInternalBackgroundColor(sectionDyf.<?php echo $kunik ?>ParamsData.internalBackgroundColor)
  circularGraph.setExternalTitleBackgroundColor(sectionDyf.<?php echo $kunik ?>ParamsData.externalTitleBackgroundColor)
  circularGraph.setLabelCircleColor(sectionDyf.<?php echo $kunik ?>ParamsData.labelCircleColor)
  circularGraph.draw("#graph-container-<?= $kunik ?>")
  circularGraph.updateData(circularGraph.preprocessResults({...sectionDyf.<?php echo $kunik ?>ParamsData, internalData}))
</script>
<script type="text/javascript">
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema": {
        "title": "Configurer votre section",
        "description": "Personnaliser votre section",
        "icon": "fa-cog",
        "properties": {
          "internalTitle": dyFInputs.text("Titre intérieur", "titre"),
          "internalTitleBackgroundColor": dyFInputs.colorpicker(null, "Couleur de fond titre interieur"),
          "internalBackgroundColor": dyFInputs.colorpicker(null, "Couleur de fond interieur"),
          "labelCircleColor": dyFInputs.colorpicker(null, "Couleur de fond des labels interieur"),
          "internalData": {
            inputType: "lists",
            label: "Donnée de l'intérieur",
            entries: {
              title: {
                label: "Donnée {num}",
                class: "col-md-12",
                type: "label"
              },
              label: {
                label: "Label",
                placeholder: "Donnée",
                type: "text",
                class: "col-md-4"
              },
              value: {
                label: "Valeur",
                placeholder: "7;12",
                type: "text",
                class: "col-md-4"
              },
              color: {
                type: "colorpicker",
                placeholder: "Couleur",
                label: "Couleur",
                class: "col-md-4"
              }
            }
          },
          "externalTitle": dyFInputs.text("Titre extérieur", "titre"),
          "externalTitleBackgroundColor": dyFInputs.colorpicker(null, "Couleur de fond titre exterieur"),
          "externalData": {
            inputType: "lists",
            label: "Donnée de l'exterieur",
            entries: {
              title: {
                label: "Donnée {num}",
                class: "col-xs-12",
                type: "label"
              },
              label: {
                label: "Label",
                placeholder: "Donnée",
                type: "text",
                class: "col-xs-12 col-md-4"
              },
              value: {
                label: "Valeur",
                placeholder: "1",
                type: "text",
                class: "col-xs-12 col-md-4"
              },
              color: {
                type: "colorpicker",
                placeholder: "Couleur",
                label: "Couleur",
                class: "col-xs-12 col-md-4"
              }
            }
          },
        },
        save: function(data) {
          data["collection"] = "cms"
          tplCtx.value = data;
          mylog.log("save tplCtx", tplCtx);
          if (typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value(tplCtx, function(params) {
              dyFObj.commonAfterSave(params, function() {
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    mylog.log("sectiondyfff", sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click", function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      console.log(tplCtx, sectionDyf.<?php echo $kunik ?>ParamsData)
      dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>