<?php
if(!isset($costum)){
    $costum = CacheHelper::getCostum();
}
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
    $graphAssets = [
        '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
    );
}
?>

<?php

    $keyTpl     = "graphOfElements";
    $paramsData = [
        "title" => "Titre du graph",
        "coform" => "",
        "path" => "",
        "name" => "",
        "elementTypes" => Organization::COLLECTION,
        "fields" => "type",
        "graphRoot" => "",
        "graphTypes" => ["circle"=>"Cercle D3", "mindmap"=>"Carte mentale D3", "network"=>"Réseau D3", "relation"=>"Relation D3"],
        "width" => ["4"=>4, "6"=>6, "8"=>8, "9"=>9, "10"=>10, "12"=>12],
        "graphType" => "mindmap",
        "widthValue" => "100",
        "height" => 400,
        "graph" => null,
        "graphDataToShow" => [],
        "graphColor" => ""
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) && $blockCms[$e]!="" ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<style>
    #graph-container-<?= $kunik ?>{
        margin-bottom: 0px !important;
        width: <?= $paramsData["widthValue"] ?>% !important;
        background-color: transparent !important;
        overflow: hidden !important;
    }

    #graph-container-<?= $kunik ?> svg:not(:root) {
        min-height: <?= $paramsData["height"] ?>px !important;
        width: <?= $paramsData["widthValue"] ?>% !important;
    }
</style>

<div>
    <h4 class="padding-top-5 sp-text padding-left-10" data-field="title" data-id="<?= $blockKey ?>" >
      <?php echo $paramsData['title'] ?>
    </h4>
    <div id="search-container-<?= $kunik ?>" class="searchObjCSS" style='background-color:transparent!important'></div>
    <div id="loader-container-<?= $kunik ?>"></div>
    <div id="graph-container-<?= $kunik ?>"></div>
</div>

<script>
    var authorizedTags<?= $kunik ?> = <?= json_encode($paramsData["graphDataToShow"]) ?>;
    var data<?= $kunik ?> = {};
    var fields<?= $kunik ?> = {};// Generic fiels to fetch
    var path<?= $kunik ?> = "<?= (is_array($paramsData["fields"]))?$paramsData["fields"][0]:$paramsData["fields"] ?>";// field to make statistic

    if(typeof sectionDyf == "undefined"){
      var sectionDyf = {};
    }

    if(typeof tplCtx == "undefined"){
      var tplCtx = {};
    }

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    const getValueByPath = (object, path) => {
        if (path === undefined || path === null) {
            return object;
        }
        const parts = path.split('.');
        for (let i = 0; i < parts.length; ++i) {
            if (object === undefined || object === null) {
                return undefined;
            }
            const key = parts[i];
            object = object[key];
        }
        return object;
    }

    /**
     * liste des fields
     **/

    let extraFields = [];

    if(typeof getValueByPath(costum, "typeObj.<?php echo $paramsData['elementTypes'] ?>.dynFormCostum.beforeBuild.properties")!="undefined"){
        extraFields = costum.typeObj.<?php echo $paramsData['elementTypes'] ?>.dynFormCostum.beforeBuild.properties;
    }

    Object.keys(extraFields).forEach(function(keyValue, k) {
        let keyV = keyValue;
        let keyL = "";

        if(keyValue.indexOf("-")!=-1){
            keyV = keyValue.replace("-", ".");

        }else if(keyValue.indexOf("[")!=-1){
            keyV = keyValue.replace("[", ".").replace("]", "");
        }

        if(typeof extraFields[keyValue].inputType != "undefined" && extraFields[keyValue].inputType != "text" && extraFields[keyValue].inputType != "textarea"){

            if(typeof extraFields[keyValue] != "undefined" && typeof extraFields[keyValue].label != "undefined"){
                keyL = extraFields[keyValue].label
            }else{
                keyL = keyV;
            }

            fields<?= $kunik ?>[keyV] = (trad[keyL])?trad[keyL]:keyL;
        }
    });

    fields<?= $kunik ?>["source.keys"] = tradDynForm.network+" communecter";
    fields<?= $kunik ?>["type"] = trad.type;
    fields<?= $kunik ?>["address.level1Name"] = "Pays";
    fields<?= $kunik ?>["address.level3Name"] = "Région";
    fields<?= $kunik ?>["badges"] = "badges";

    if(typeof fields<?= $kunik ?>["tags"]=="undefined"){
        fields<?= $kunik ?>["tags"] = trad.tags;
    }

    let defaultFilters<?= $kunik ?> = {'$or':{}, 'toBeValidated':{'$exists':false}};
    defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
    defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};

    var l<?= $kunik ?> = {
        //container: "#search-container-<?= $kunik ?>",
        loadEvent: {
            default: "graph"
        },
        defaults: {
            notSourceKey:true,
            types: ["<?php echo $paramsData['elementTypes'] ?>"],
            fields: {...fields<?= $kunik ?>, "tags":"tags", "type":"type", "name":"name", "links":"links", "profilThumbImageUrl":"profilThumbImageUrl"},
            filters:defaultFilters<?= $kunik ?>,
            indexStep : 900
        },
        graph: {
            dom: "#graph-container-<?= $kunik ?>",
            mindmapDepth: 1,
            authorizedTags: authorizedTags<?= $kunik ?>,
            defaultGraph: "<?php echo $paramsData['graphType'] ?>",
        },
        header: {
            options : {}
        },
        results:{
          dom:"#loader-container-<?= $kunik ?>"
        }
    };

    coInterface.showLoader("#loader-container-<?= $kunik ?>");

    var p<?= $kunik ?> = {};

    var root<?= $kunik ?> = "<?php echo $paramsData['graphRoot'] ?>";

    setTimeout(() => {
        p<?= $kunik ?> = searchObj.init(l<?= $kunik ?>);

        p<?= $kunik ?>.graph.successComplete = function(fObj, rawData){
            p<?= $kunik ?>.graph.lastResult = rawData;
            let valueFromPath = path<?= $kunik ?>.split(".");

            <?php if(isset($paramsData["graphType"]) && $paramsData["graphType"]=="mindmap"){ ?>

            let structuredData = {
                id:"root<?= $kunik ?>",
                label:(root<?= $kunik ?>!="")?root<?= $kunik ?>:costum.title,
                children:[]
            }

            let lists = null;
            let isCostumList = false;

            if(costum.lists && exists(costum.lists[path<?= $kunik ?>])){
                lists = costum.lists[path<?= $kunik ?>];
                data<?= $kunik ?> = {};
                $.each(lists, function(icl, vcl){
                  if(typeof vcl == "object"){
                    Object.assign(data<?= $kunik ?>, vcl);
                  }else{
                    data<?= $kunik ?>[icl]=vcl;
                  }
                });
                isCostumList == true;
            }else{
                lists = {}
                Object.keys(rawData.results).forEach((key, index) => {
                    let val = getValueByPath(rawData.results[key], path<?= $kunik ?>);
                    if(val && val!=""){
                        if(Array.isArray(val)){
                            for(var i in val){
                                lists[val[i]] = val[i];
                            }
                        }else if(typeof val=="string"){
                            lists[val] = val;
                        }
                    }
                });
                data<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>.length==0){
                authorizedTags<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>!=null){
                for (var i in authorizedTags<?= $kunik ?>) {
                    let child = {
                        id:"element"+i,
                        label:(typeof authorizedTags<?= $kunik ?>[i] == "string")?authorizedTags<?= $kunik ?>[i]:i,
                        children: []
                    }
                    Object.keys(rawData.results).forEach((key, index) => {
                        let element = rawData.results[key];
                        let itIsIn = false;
                        if(typeof authorizedTags<?= $kunik ?>[i] == "object"){
                            for(var k in authorizedTags<?= $kunik ?>[i]){
                                if(typeof element.tags!="undefined" && element.tags.includes(k)){
                                    itIsIn = true;
                                }
                            }
                        }
                        if(child.label == getValueByPath(element, path<?= $kunik ?>)||(Array.isArray(getValueByPath(element, path<?= $kunik ?>)) && getValueByPath(element, path<?= $kunik ?>).includes(child.label)) || (isCostumList && typeof(element.tags)!="undefined" && element.tags.includes(child.label)) || itIsIn){
                            child.children.push({
                                id:key,
                                label:element.name,
                                collection: element.collection
                            });
                        }
                    });
                    structuredData["children"].push(child)
                }
            }
        <?php } ?>


        <?php if(isset($paramsData["graphType"]) && in_array($paramsData["graphType"], array("network","circle", "relation"))){ ?>

            let structuredData = [];

            <?php if($paramsData["graphType"]=="network"){ ?>
            structuredData.push({
                label: (root<?= $kunik ?>!="")?root<?= $kunik ?>:costum.title,
                type: "root",
                group: "root"
            });
            <?php } ?>

            let lists = null;
            let isCostumList = false;

            if(costum.lists && exists(costum.lists[path<?= $kunik ?>])){
                lists = costum.lists[path<?= $kunik ?>];
                data<?= $kunik ?> = {};
                $.each(lists, function(icl, vcl){
                  if(typeof vcl == "object"){
                    Object.assign(data<?= $kunik ?>, vcl);
                  }else{
                    data<?= $kunik ?>[icl]=vcl;
                  }
                });
                isCostumList = true;
            }else{
                lists = {};
                $.each(rawData.results, (key, index) => {
                    let val = getValueByPath(rawData.results[key], path<?= $kunik ?>);
                    if(val && val!=""){
                        if(Array.isArray(val)){
                            for(var i in val){
                                lists[val[i]] = val[i];
                            }
                        }else if(typeof val=="string"){
                            lists[val] = val;
                        }else if(typeof val=="object" && val!=null){
                            $.each(val, function(k, v){
                                lists[k] = v.name;
                            });
                        }
                    }
                });
                data<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>.length==0){
                authorizedTags<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>!=null){
                for(var i in authorizedTags<?= $kunik ?>){
                    let child = {
                        id:"element"+slugify(i, i),
                        label:(typeof authorizedTags<?= $kunik ?>[i] == "string")?authorizedTags<?= $kunik ?>[i]:i,
                        children: []
                    }

                    Object.keys(rawData.results).forEach((key, index) => {
                        let element = rawData.results[key];
                        let itIsIn = false;
                        if(typeof authorizedTags<?= $kunik ?>[i] == "object"){
                            for(var k in authorizedTags<?= $kunik ?>[i]){
                                if(typeof element.tags!="undefined" && element.tags.includes(k)){
                                    itIsIn = true;
                                }
                            }
                        }

                        if(
                            child.label==getValueByPath(element, path<?= $kunik ?>)
                            || (Array.isArray(getValueByPath(element, path<?= $kunik ?>)) && getValueByPath(element, path<?= $kunik ?>).includes(child.label)) 
                            || ((isCostumList||path<?= $kunik ?>=="tags") && typeof element.tags !="undefined" && element.tags.includes(child.label)) 
                            || itIsIn
                            ){
                                structuredData.push({
                                    "id":key,
                                    "label":element.name,
                                    "collection": element.collection,
                                    "type": child.label,
                                    "group": child.label,
                                    "img":element.profilThumbImageUrl
                                });
                        }
                    });
                }
            }
        <?php } ?>

        p<?= $kunik ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
        p<?= $kunik ?>.graph.graph.initZoom();
        
        setTimeout(() => {
            coInterface.bindLBHLinks();
        }, 4000);

        // Dynform after load
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "graphRoot" : {
                        "inputType" : "text",
                        "label" : "Racine du graph",
                        "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.graphRoot
                    },
                    "graphType" : {
                        "inputType" : "select",
                        "label" : "Quelle type de graph",
                        "class" : "form-control <?php echo $kunik ?>",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $kunik ?>ParamsData.graphTypes,
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.graphType
                    },
                    "graphColor" : {
                        "label" : "Couleur de cercle de graph",
                        "inputType" : "colorpicker",
                        "values" :  sectionDyf.<?php echo $kunik?>ParamsData.graphColor
                    },
                    "elementTypes" : {
                        "inputType" : "select",
                        "label" : "Type d'élément",
                        "values" : sectionDyf.<?=$kunik ?>ParamsData.elementTypes,
                        "class" : "form-control",
                        "options" : {
                            "organizations" : trad.organizations,
                            "events" : trad.events,
                            "projects" : trad.projects,
                            "citoyens" : trad.person
                        }
                    },
                    "fields" :{
                        "inputType" : "select",
                        "label" : "Champs cible",
                        "values" : sectionDyf.<?=$kunik ?>ParamsData.fields,
                        "class" : "form-control",
                        "options" : fields<?= $kunik ?>
                    },
                    "graphDataToShow" :{
                        "inputType" : "selectMultiple",
                        "isSelect2" : true,
                        "label" : "Données à Afficher",
                        "class" : "form-control",
                        "noOrder" : true,
                        "placeholder" : "Données à Afficher",
                        "options" : data<?= $kunik ?>,
                        "values" : sectionDyf.<?=$kunik ?>ParamsData.graphDataToShow
                    },
                    "height" : {
                        "inputType" : "text",
                        "label" : "Hauteur du graph",
                        "rules":{
                            "number":true
                        },
                        "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.height
                    },
                },
                afterBuild : function(){
                    // Initialize something here
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        

        }
        p<?= $kunik ?>.graph.init(p<?= $kunik ?>);
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);

        setTimeout(() => {
            $("#loader-container-<?= $kunik ?>").remove();
            <?php if($paramsData["graphColor"]!=""){ ?>
                $("#graph-container-<?= $kunik ?> circle").css({
                    "fill":"<?= $paramsData["graphColor"] ?>"
                })
            <?php } ?>
        }, 1000);

    }, 200);
</script>