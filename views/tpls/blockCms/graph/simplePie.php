<?php
    if (isset($costum["contextType"]) && isset($costum["contextId"])) {

        $graphAssets = [
            '/plugins/d3/d3.v6.min.js'
        ];

        HtmlHelper::registerCssAndScriptsFiles(
            $graphAssets,
            Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
        );
    }
?>

<?php 
    $keyTpl     = "simplePie";

    $paramsData = [
        "titleTop" => "",
        "titleBottom" => "",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "percentColor" => "#413F99",
        "nextTo" => "",
        "emptyColor" => "#DDD",
        "completeColor" => "#F0506A",
        "pieColors" => ["#918CC4", "#C2BEDF","#FFFFFF"]
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

    // Default Form
    if(isset($paramsData["coform"]) && !empty($paramsData["coform"])){
        $formTL = PHDB::findOneById(Form::COLLECTION, $paramsData["coform"]);
    }else{
        $formTL = PHDB::find(Form::COLLECTION, array("parent.".$costum["contextId"] => array('$exists' => true)));
    }
 ?>

<style type="text/css">
   .percentageText<?= $kunik ?>{
    fill: <?= $paramsData['percentColor'] ?> !important;
   }

   .labelText{
    color: white;
    font-weight: bolder;
   }

   .percentageNext{
    line-height: 1;
   }
</style>

<div id="pie<?= $kunik; ?>" class="<?= (($paramsData['nextTo']=='')?'text-center':'') ?>"></div>

<script>

    jQuery(document).ready(function() {
        var data = [];
        if(typeof costum["dashboardData"] != "undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["nameCountArray"]){
            data = costum["dashboardData"]["<?= $blockKey ?>"]["nameCountArray"];
        }
        var coef = ((data && data.length>=3)?2:4);
        
        var width = document.getElementById("pie<?= $kunik; ?>").clientWidth,
        height = (width/2)+20,
        radius = 90,
        innerRadius = 30;


        if(data && data.length < 3){
            //height = (width/4)+20;
            radius = 60;
            innerRadius = 0;
        }
        try{
            var arc = d3.arc()
            .outerRadius(radius - 10)
            .innerRadius(innerRadius);

        var pie = d3.pie()
            .sort(null)
            .value(function(d) {
                return d.count;
            });

        var svg = d3.select('#pie<?= $kunik; ?>')
          .append("svg")
          .attr("width", width)
          .attr("height", height)
          .append("g")
          .attr("transform", "translate("+width/coef+","+height/2+")");

        var g = svg.selectAll(".arc")
          .data(pie(data))
          .enter().append("g");    

        g.append("path")
            .attr("d", arc)
          .style("fill", function(d,i) {
            return d.data.color;
          });

        
          if(data.length>2){
                g.append("text")
                .attr("transform", function(d) {
                var _d = arc.centroid(d);
                _d[0] *= 1; //multiply by a constant factor
                _d[1] *= 1; //multiply by a constant factor
                return "translate(" + _d + ")";
              })
              .attr("dy", ".50em")
              .attr('font-size', '1.8em')
              .attr("class", "percentageText<?= $kunik ?>")
              .style("text-anchor", "middle")
              .style("font-weight", "bolder")
              .text(function(d) {
                if(d.data.percentage < 8) {
                  return '';
                }
                return d.data.percentage + '%';
              });

            g.append("foreignObject")
              .attr("x", function(d){
                if(arc.centroid(d)[0]<0){
                    return arc.centroid(d)[0]*3.2;
                }else{
                    return arc.centroid(d)[0]*1.6;
                }
              })
              .attr("y", function(d){
                return arc.centroid(d)[1];
              })
              .attr("width", 100)
              .attr("height", 100)
              .attr("dy", ".50em")
              .style("text-transform", "uppercase")
              .style("text-anchor", "middle")
              .append("xhtml:div")
                .attr("class", "labelText")
                .html(function(d) {
                    return d.data.name + '';
                });

            
          }else{
            svg.append("foreignObject")
                .attr("x", ((width/5)-20))
                .attr("y", -height/3)
                .attr("width", (width - width/3))
                .attr("height", height)  
                .append("xhtml:div")
                    .style("color", "<?= $paramsData["percentColor"] ?>")
                    .style("line-height", 1)
                    .style("margin-bottom", "3px")
                    .style("font-weight", "bolder")
                    .html("<span style='font-size:90pt'>"+(costum["dashboardData"]["<?= $blockKey ?>"]["nextTo"]||0)+"</span><sup style='font-size:50pt'>%</sup>");
          }
        }catch(ex){}

    	

    });
   
  </script>

  <script type="text/javascript">
    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                    
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus")){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>