<?php
$keyTpl ="elementCount";

$paramsData = [
    "countTitle" => "",
    "countTitleColor" => "#4AB5A1",
    "elementIconBackground" => "#eee",
    "elementIcon" => "group",
    "elementType" => Organization::COLLECTION,
    "design" => ""
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if(!isset($costum)){
    $costum = CacheHelper::getCostum();
}


$queryFilter = [
    array("source.keys" => $costum["contextSlug"]),
    array("parent.".$costum["contextId"] => ['$exists'=>true])
];

if($paramsData['elementType']==Person::COLLECTION){
    array_push($queryFilter, array("links.memberOf.".$costum["contextId"] => ['$exists'=>true]));
    array_push($queryFilter, array("links.projects.".$costum["contextId"] => ['$exists'=>true]));
}

if($paramsData['elementType']==Organization::COLLECTION){
    array_push($queryFilter, array("links.memberOf.".$costum["contextId"] => ['$exists'=>true]));
}

$count = PHDB::count($paramsData["elementType"], array('$or' => $queryFilter));

?>

<style type="text/css">
    .text-green-theme<?= $kunik ?>{
        color: <?= $paramsData["countTitleColor"] ?>/*#7cb927*/;
    }

    .effectif{
        padding: 0px ;
    }

    a{
        text-decoration: none !important;
    }

     <?php if(($paramsData["elementIcon"]=="")){ ?>
        h2.cedarville-number{
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            margin-top: 0px  !important;
            margin-bottom: 0px  !important;
        }
     <?php } ?>

</style>

<?php if($paramsData["design"] == "horizontalDesign"){ ?>
<style type="text/css">
    /*
    .dash-icon<?= $kunik ?>{
        font-size: 2em;
        border-radius: 50%;
        padding: 0.8em;
        margin: auto !important;
        background: <?= $paramsData["elementIconBackground"] ?>

    }*/
   
    .dbox-icon<?= $kunik ?> {
        margin: auto;
        width: 60%;
        padding: 1.3em;
    }
    .dbox-icon<?= $kunik ?>:before {
        width: 95px;
        height: 95px;
        position: absolute;
        background: rgba(255,255,255, 1);
        content: '';
        border-radius: 50%;
        left: -17px;
        top: -17px;
        z-index: -2;
        clear: both;
        display: table;
    }
    .dbox-icon<?= $kunik ?>:after {
        width: 80px;
        height: 80px;
        position: absolute;
        background: <?= $paramsData["elementIconBackground"] ?>;
        content: '';
        border-radius: 50%;
        left: -10px;
        top: -10px;
        z-index: -1;
        clear: both;
        display: table;
    }
    .dbox-icon<?= $kunik ?> > i {
        background: <?= $paramsData["elementIconBackground"] ?>;
        border-radius: 50%;
        line-height: 40px;
        width: 60px;
        height: 60px;
        font-size:2em;
        padding: 15%;
        text-align: center;
        clear: both;
        display: table;
        margin: auto;
    }
</style>
<?php }else{ ?>
<style type="text/css">
    .dbox<?= $kunik ?> {
        position: relative;
        background: <?= $paramsData["elementIconBackground"] ?>;
        border-radius: 4px;
        text-align: center;
    }
    .p-1{
        padding: .3em;
    }
    .dbox-body-<?= $kunik ?> {
        padding: <?= ($paramsData["elementIcon"]!="")?"10px 0px 40px 0px":"0px";?> !important;
    }
    .dbox-title-<?= $kunik ?> {
        font-size: 14px;
        color: #888;
    }
    .dbox-icon<?= $kunik ?> {
        position: absolute;
        transform: translateY(-50%) translateX(-50%);
        left: 50%;
    }
    .dbox-icon<?= $kunik ?>:before {
        width: 75px;
        height: 75px;
        position: absolute;
        background: rgba(255,255,255, 1);
        content: '';
        border-radius: 50%;
        left: -17px;
        top: -17px;
        z-index: -2;
    }
    .dbox-icon<?= $kunik ?>:after {
        width: 60px;
        height: 60px;
        position: absolute;
        background: <?= $paramsData["elementIconBackground"] ?>;
        content: '';
        border-radius: 50%;
        left: -10px;
        top: -10px;
        z-index: -1;
    }
    .dbox-icon<?= $kunik ?> > i {
        background: #FFF;
        border-radius: 50%;
        line-height: 40px;
        width: 40px;
        height: 40px;
        font-size:22px;
    }
    .dbox-action<?= $kunik ?> {
        transform: translateY(-50%) translateX(-50%);
        position: absolute;
        left: 50%;
    }
</style>
<?php } ?>

<div class="col-xs-12 effectif">
    <a class="lbh" href="#search?searchType=organization">
   		<div class="container-fluid">
   			<div class="row">
                <?php if($paramsData["design"] == "horizontalDesign"){ ?>
                    <?php if($paramsData["elementIcon"]!=""){ ?>
           				<div class="col-md-4">
                            <span class="dbox-icon<?= $kunik ?>">
                                <i class="fa text-green-theme<?= $kunik ?> fa-<?= $paramsData['elementIcon'] ?> dash-icon<?= $kunik ?>"></i>
                            </span>
           				</div>
                        <div class="col-md-1"></div>
                    <?php } ?>
   				<div class="col-md-<?= ($paramsData["elementIcon"]!="")?"7":"12";?> text-center dbox-body-<?= $kunik ?>">
                    <h1 data-value="<?= $count ?>" class="text-green-theme<?= $kunik ?> cedarville-number"><?= $count ?></h1>
                    <?php if($paramsData["countTitle"]!=""){ ?>
   					    <h6><?=Yii::t("common", $paramsData["countTitle"])?></h6>
                    <?php } ?>
   				</div>

                <?php }else{ ?>

                <div class="dbox<?= $kunik ?>">
                    <div class="dbox-body-<?= $kunik ?>">
                        <h2 data-value="<?= $count ?>" class="text-green-theme<?= $kunik ?> cedarville-number"><?= $count ?></h2>
                        <?php if($paramsData["countTitle"]!=""){ ?>
                        <h6 class="dbox-title-<?= $kunik ?>">
                            <?=Yii::t("common", $paramsData["elementType"])?>
                        </h6>
                        <?php } ?>
                    </div>
                    <?php if($paramsData["elementIcon"]!=""){ ?>
                        <div class="dbox-action<?= $kunik ?>">
                            <a href="javascript:;" class="dbox-icon<?= $kunik ?>">
                                <i class="fa fa-<?= $paramsData['elementIcon'] ?> text-green-theme<?= $kunik ?>"></i>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
   			</div>
   		</div>
    </a>
</div>

<script type="text/javascript">
    /*********DYNFORM CONFIG OF GRAPH*******************/
    sectionDyf.<?=$kunik ?>ParamsData = <?=json_encode( $paramsData ); ?>;

    sectionDyf.<?=$kunik ?>Params = {
        "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {
                "countTitle" : {
                    "label" : "Titre",
                    "inputType" : "text",
                    "values" :  sectionDyf.<?=$kunik?>ParamsData.countTitle
                },
                "countTitleColor" : {
                    "label" : "Couleur du titre",
                    "inputType" : "colorpicker",
                    "values" :  sectionDyf.<?=$kunik?>ParamsData.countTitleColor
                },
                "elementIconBackground" : {
                    "label" : "Couleur de fond",
                    "inputType" : "colorpicker",
                    "values" :  sectionDyf.<?=$kunik?>ParamsData.elementIconBackground
                },
                "elementIcon" :{
                    "inputType" : "select",
                    "label" : "Icon",
                    values : sectionDyf.<?=$kunik ?>ParamsData.elementIcon,
                    "class" : "form-control",
                    "options" : fontAwesome
                },
                "elementType" : {
                    "inputType" : "select",
                    "label" : "Type d'élément",
                    "values" : sectionDyf.<?=$kunik ?>ParamsData.elementType,
                    "class" : "form-control",
                    "options" : {
                        "<?= Organization::COLLECTION ?>" : trad.organizations,
                        "<?= Person::COLLECTION ?>" : tradCategory.citizen,
                        "<?= Event::COLLECTION ?>" : trad.events,
                        "<?= Project::COLLECTION ?>" : trad.projects,
                        "<?= Answer::COLLECTION ?>" : trad.answers
                    }
                },
                "design" :{
                    "label" : "Choisir un design",
                    "inputType" : "tplList",
                    list :{
                        "horizontalDesign" : {
                            "photo" :"<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/openCms/count-horizontal-design.png"
                        },
                        "verticalDesign" : {
                            "photo" :"<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/openCms/count-vertical-design.png"
                        }
                    },
                    "rules" :{
                        "required" : true
                    }
                }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?=$blockKey ?>");
            },
            save : function (data) {
                tplCtx.value = {};
                $.each( sectionDyf.<?=$kunik ?>Params.jsonSchema.properties , function(k,val) {
                    tplCtx.value[k] = $("#"+k).val();
                    if(k=="design"){
                        if($("#horizontalDesign").is(':checked')){
                            tplCtx.value[k] = "horizontalDesign"
                        }else if($("#verticalDesign").is(':checked')){
                            tplCtx.value[k] = "verticalDesign"
                        }
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');
                                dyFObj.closeForm();
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                    });
                }
            }
        }
    }

    $(".edit<?=$kunik ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?=$kunik ?>Params,null,sectionDyf.<?=$kunik ?>ParamsData);
          //alignInput2(sectionDyf.<?=$kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
    });

    jQuery(document).ready(function(){
        /*$(".dbox-body-<?= $kunik ?> h2, .dbox-body-<?= $kunik ?> h1").each(function() {
            $(this).prop('Counter',0).animate(
               {
                    Counter: $(this).data("value")
                },
                {
                    duration: 3000,
                    step: function(now) {
                        $(this).text(Math.ceil(this.Counter));
                    }
                }
            );
        });*/
    });

</script>
