<?php 
    $keyTpl     = "pourcentageLinkedGraphLosange";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "color01" => "#9B6FAC",
        "color02" => "#ddd",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>

<svg id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 569.8 309">
    <defs>
        <style>
            .<?=$kunik?>.cls-1 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-2 {
                letter-spacing: -.04em;
            }

            .<?=$kunik?>.cls-3 {
                font-size: 8.24px;
            }

            .<?=$kunik?>.cls-3,
            .<?=$kunik?>.cls-4 {
                font-family: SharpSansNo1-Semibold, 'Sharp Sans No1';
                font-weight: 600;
            }

            .<?=$kunik?>.cls-3,
            .<?=$kunik?>.cls-5,
            .<?=$kunik?>.cls-6,
            .<?=$kunik?>.cls-7,
            .<?=$kunik?>.cls-8,
            .<?=$kunik?>.cls-9,
            .<?=$kunik?>.cls-10,
            .<?=$kunik?>.cls-11,
            .<?=$kunik?>.cls-12,
            .<?=$kunik?>.cls-13,
            .<?=$kunik?>.cls-14,
            .<?=$kunik?>.cls-15,
            .<?=$kunik?>.cls-16 {
                fill: #fff;
            }

            .<?=$kunik?>.cls-17 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-18 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-19 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-20,
            .<?=$kunik?>.cls-4 {
                fill: #a4bf45;
            }

            .<?=$kunik?>.cls-21 {
                fill: #c6c8ce;
            }

            .<?=$kunik?>.cls-22 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-23 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-24 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-25 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-26 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-27 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-28 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-29 {
                letter-spacing: -.1em;
            }

            .<?=$kunik?>.cls-4 {
                font-size: 15.78px;
            }

            .<?=$kunik?>.cls-5 {
                filter: url(#drop-shadow-5);
            }

            .<?=$kunik?>.cls-6 {
                filter: url(#drop-shadow-7);
            }

            .<?=$kunik?>.cls-7 {
                filter: url(#drop-shadow-4);
            }

            .<?=$kunik?>.cls-8 {
                filter: url(#drop-shadow-9);
            }

            .<?=$kunik?>.cls-9 {
                filter: url(#drop-shadow-1);
            }

            .<?=$kunik?>.cls-10 {
                filter: url(#drop-shadow-6);
            }

            .<?=$kunik?>.cls-11 {
                filter: url(#drop-shadow-8);
            }

            .<?=$kunik?>.cls-12 {
                filter: url(#drop-shadow-2);
            }

            .<?=$kunik?>.cls-13 {
                filter: url(#drop-shadow-3);
            }

            .<?=$kunik?>.cls-30 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-31 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-14 {
                filter: url(#drop-shadow-12);
            }

            .<?=$kunik?>.cls-15 {
                filter: url(#drop-shadow-11);
            }

            .<?=$kunik?>.cls-16 {
                filter: url(#drop-shadow-10);
            }

            .<?=$kunik?>.cls-32 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-33 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-34 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-35 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-36 {
                letter-spacing: -.01em;
            }

            .<?=$kunik?>.cls-37 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-38 {
                letter-spacing: 0em;
            }
        </style>
        <filter id="drop-shadow-1" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-2" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-2" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-2" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-3" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-3" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-3" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-4" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-4" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-4" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-5" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-5" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-5" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-6" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-6" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-6" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-7" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-7" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-7" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-8" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-8" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-8" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-9" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-9" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-9" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-10" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-10" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-10" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-11" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-11" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-11" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
        <filter id="drop-shadow-12" filterUnits="userSpaceOnUse">
            <feOffset dx="0" dy="0" />
            <feGaussianBlur result="blur-12" stdDeviation="1.73" />
            <feFlood flood-color="#000" flood-opacity=".16" />
            <feComposite in2="blur-12" operator="in" />
            <feComposite in="SourceGraphic" />
        </filter>
    </defs>
    <path class="<?=$kunik?> cls-20" d="m259,6.83H21.08c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-9" d="m76.72,5.81H19.47c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(135.24 23.38)">
        <tspan class="<?=$kunik?> cls-27" x="0" y="0">A</tspan>
        <tspan class="<?=$kunik?> cls-17" x="5.5" y="0">c</tspan>
        <tspan x="10.1" y="0">cue</tspan>
        <tspan class="<?=$kunik?> cls-38" x="24.2" y="0">i</tspan>
        <tspan x="25.97" y="0">l d’élus </tspan>
        <tspan class="<?=$kunik?> cls-38" x="52.75" y="0">p</tspan>
        <tspan x="57.78" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-31" x="62.61" y="0">u</tspan>
        <tspan x="67.31" y="0">r </tspan>
        <tspan x="9.97" y="11.33">a</tspan>
        <tspan class="<?=$kunik?> cls-17" x="14.98" y="11.33">c</tspan>
        <tspan x="19.58" y="11.33">cult</tspan>
        <tspan class="<?=$kunik?> cls-31" x="33.61" y="11.33">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="38.3" y="11.33">r</tspan>
        <tspan class="<?=$kunik?> cls-19" x="41.29" y="11.33">ation</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(32.19 30.38)">
        <tspan x="0" y="0" id="Accueildeluspouracculturation<?= $kunik ?>">15%</tspan>
    </text>
    <path class="<?=$kunik?> cls-21" d="m259,58.24H21.08c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-12" d="m76.72,57.22H19.47c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <text class="<?=$kunik?> cls-3" transform="translate(112.75 73.97)">
        <tspan x="0" y="0">Aide à la </tspan>
        <tspan class="<?=$kunik?> cls-23" x="33.94" y="0">r</tspan>
        <tspan x="36.93" y="0">éa</tspan>
        <tspan class="<?=$kunik?> cls-38" x="46.77" y="0">l</tspan>
        <tspan x="48.55" y="0">isation de do</tspan>
        <tspan class="<?=$kunik?> cls-38" x="96.34" y="0">s</tspan>
        <tspan x="100.27" y="0">sie</tspan>
        <tspan class="<?=$kunik?> cls-35" x="110.72" y="0">r</tspan>
        <tspan x="113.87" y="0">s </tspan>
        <tspan class="<?=$kunik?> cls-38" x="13.21" y="11.33">p</tspan>
        <tspan x="18.23" y="11.33">o</tspan>
        <tspan class="<?=$kunik?> cls-25" x="23.07" y="11.33">u</tspan>
        <tspan x="27.76" y="11.33">r AMI ou </tspan>
        <tspan class="<?=$kunik?> cls-35" x="59.82" y="11.33">s</tspan>
        <tspan x="63.67" y="11.33">u</tspan>
        <tspan class="<?=$kunik?> cls-32" x="68.31" y="11.33">bv</tspan>
        <tspan x="77.16" y="11.33">e</tspan>
        <tspan class="<?=$kunik?> cls-23" x="81.99" y="11.33">n</tspan>
        <tspan x="86.43" y="11.33">tions</tspan>
    </text>
    <text class="<?=$kunik?> cls-4" transform="translate(32.19 81.79)">
        <tspan x="0" y="0" id="AidealarealisationdedossierspourAMIousubventions<?= $kunik ?>">22%</tspan>
    </text>
    <path class="<?=$kunik?> cls-20" d="m259,109.65H21.08c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-13" d="m76.72,108.62H19.47c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(110.18 126.86)">
        <tspan class="<?=$kunik?> cls-26" x="0" y="0">C</tspan>
        <tspan x="6.35" y="0">onse</tspan>
        <tspan class="<?=$kunik?> cls-38" x="24.55" y="0">i</tspan>
        <tspan x="26.32" y="0">l </tspan>
        <tspan class="<?=$kunik?> cls-33" x="29.69" y="0">e</tspan>
        <tspan x="34.36" y="0">t orie</tspan>
        <tspan class="<?=$kunik?> cls-23" x="53.6" y="0">n</tspan>
        <tspan class="<?=$kunik?> cls-1" x="58.04" y="0">t</tspan>
        <tspan x="60.9" y="0">ation a</tspan>
        <tspan class="<?=$kunik?> cls-34" x="86.78" y="0">v</tspan>
        <tspan x="90.92" y="0">ec </tspan>
        <tspan class="<?=$kunik?> cls-17" x="102" y="0">c</tspan>
        <tspan x="106.6" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-23" x="111.43" y="0">n</tspan>
        <tspan class="<?=$kunik?> cls-1" x="115.88" y="0">t</tspan>
        <tspan x="118.73" y="0">a</tspan>
        <tspan class="<?=$kunik?> cls-28" x="123.74" y="0">c</tspan>
        <tspan x="128.3" y="0">t </tspan>
        <tspan class="<?=$kunik?> cls-1" x="40.87" y="11.33">t</tspan>
        <tspan x="43.73" y="11.33">éléphonique</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(32.19 133.2)">
        <tspan x="0" y="0" id="Conseiletorientationaveccontacttelephonique<?= $kunik ?>">19%</tspan>
    </text>
    <path class="<?=$kunik?> cls-21" d="m259,161.05H21.08c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-7" d="m76.72,160.03H19.47c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(114.08 177.6)">
        <tspan class="<?=$kunik?> cls-26" x="0" y="0">C</tspan>
        <tspan x="6.35" y="0">onse</tspan>
        <tspan class="<?=$kunik?> cls-38" x="24.55" y="0">i</tspan>
        <tspan x="26.32" y="0">l / </tspan>
        <tspan class="<?=$kunik?> cls-22" x="35.42" y="0">I</tspan>
        <tspan x="37.39" y="0">ncu</tspan>
        <tspan class="<?=$kunik?> cls-22" x="51.31" y="0">b</tspan>
        <tspan x="56.33" y="0">ation de p</tspan>
        <tspan class="<?=$kunik?> cls-23" x="93.66" y="0">r</tspan>
        <tspan x="96.65" y="0">oj</tspan>
        <tspan class="<?=$kunik?> cls-1" x="103.21" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-26" x="107.88" y="0">t</tspan>
        <tspan x="110.98" y="0">s</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(32.19 184.6)">
        <tspan x="0" y="0" id="ConseilIncubationdeprojets<?= $kunik ?>">08%</tspan>
    </text>
    <path class="<?=$kunik?> cls-20" d="m259,212.46H21.08c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-5" d="m76.72,211.44H19.47c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(108.29 229)">
        <tspan x="0" y="0">Me</tspan>
        <tspan class="<?=$kunik?> cls-23" x="11.64" y="0">n</tspan>
        <tspan class="<?=$kunik?> cls-33" x="16.08" y="0">t</tspan>
        <tspan x="18.94" y="0">oring / </tspan>
        <tspan class="<?=$kunik?> cls-37" x="45.72" y="0">c</tspan>
        <tspan class="<?=$kunik?> cls-38" x="50.32" y="0">o</tspan>
        <tspan x="55.2" y="0">aching de </tspan>
        <tspan class="<?=$kunik?> cls-38" x="93.97" y="0">p</tspan>
        <tspan x="99" y="0">or</tspan>
        <tspan class="<?=$kunik?> cls-33" x="107.03" y="0">t</tspan>
        <tspan x="109.89" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-31" x="114.71" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-35" x="119.41" y="0">r</tspan>
        <tspan x="122.57" y="0">s </tspan>
        <tspan x="46.38" y="11.33">de p</tspan>
        <tspan class="<?=$kunik?> cls-23" x="62.83" y="11.33">r</tspan>
        <tspan x="65.82" y="11.33">oj</tspan>
        <tspan class="<?=$kunik?> cls-33" x="72.38" y="11.33">e</tspan>
        <tspan x="77.05" y="11.33">t</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(32.19 236.01)">
        <tspan x="0" y="0" id="Mentoringcoachingdeporteursdeprojet<?= $kunik ?>">12%</tspan>
    </text>
    <path class="<?=$kunik?> cls-21" d="m259,263.87H21.08c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-10" d="m76.72,262.84H19.47c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(111.92 280.41)">
        <tspan class="<?=$kunik?> cls-24" x="0" y="0">P</tspan>
        <tspan class="<?=$kunik?> cls-25" x="4.79" y="0">a</tspan>
        <tspan class="<?=$kunik?> cls-23" x="9.84" y="0">r</tspan>
        <tspan class="<?=$kunik?> cls-17" x="12.83" y="0">c</tspan>
        <tspan x="17.43" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-31" x="22.27" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-35" x="26.96" y="0">r</tspan>
        <tspan x="30.12" y="0">s de </tspan>
        <tspan class="<?=$kunik?> cls-18" x="47.1" y="0">f</tspan>
        <tspan x="49.81" y="0">ormation </tspan>
        <tspan class="<?=$kunik?> cls-38" x="86.09" y="0">p</tspan>
        <tspan x="91.12" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-25" x="95.95" y="0">u</tspan>
        <tspan x="100.65" y="0">r des </tspan>
        <tspan class="<?=$kunik?> cls-38" x="25.7" y="11.33">p</tspan>
        <tspan x="30.72" y="11.33">or</tspan>
        <tspan class="<?=$kunik?> cls-1" x="38.75" y="11.33">t</tspan>
        <tspan x="41.61" y="11.33">e</tspan>
        <tspan class="<?=$kunik?> cls-25" x="46.44" y="11.33">u</tspan>
        <tspan class="<?=$kunik?> cls-35" x="51.14" y="11.33">r</tspan>
        <tspan x="54.29" y="11.33">s de p</tspan>
        <tspan class="<?=$kunik?> cls-23" x="76.25" y="11.33">r</tspan>
        <tspan x="79.25" y="11.33">oj</tspan>
        <tspan class="<?=$kunik?> cls-1" x="85.81" y="11.33">e</tspan>
        <tspan x="90.47" y="11.33">t</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(32.19 287.41)">
        <tspan x="0" y="0" id="Parcoursdeformationpourdesporteursdeprojet<?= $kunik ?>">09%</tspan>
    </text>
    <path class="<?=$kunik?> cls-21" d="m557.06,6.83h-237.93c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-6" d="m374.77,5.81h-57.25c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(400.99 22.4)">
        <tspan class="<?=$kunik?> cls-24" x="0" y="0">P</tspan>
        <tspan x="4.79" y="0">oi</tspan>
        <tspan class="<?=$kunik?> cls-23" x="11.35" y="0">n</tspan>
        <tspan x="15.79" y="0">t d’éch</tspan>
        <tspan class="<?=$kunik?> cls-25" x="41.23" y="0">a</tspan>
        <tspan x="46.29" y="0">nge p</tspan>
        <tspan class="<?=$kunik?> cls-23" x="67.4" y="0">h</tspan>
        <tspan class="<?=$kunik?> cls-36" x="71.85" y="0">y</tspan>
        <tspan x="76.32" y="0">sique p</tspan>
        <tspan class="<?=$kunik?> cls-23" x="103.03" y="0">r</tspan>
        <tspan x="106.02" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-38" x="110.86" y="0">p</tspan>
        <tspan x="115.88" y="0">osé </tspan>
        <tspan class="<?=$kunik?> cls-38" x="131.07" y="0">p</tspan>
        <tspan x="136.09" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-31" x="140.93" y="0">u</tspan>
        <tspan x="145.62" y="0">r </tspan>
        <tspan x="-2.36" y="11.33">mo</tspan>
        <tspan class="<?=$kunik?> cls-23" x="9.85" y="11.33">n</tspan>
        <tspan class="<?=$kunik?> cls-33" x="14.29" y="11.33">t</tspan>
        <tspan x="17.15" y="11.33">er en </tspan>
        <tspan class="<?=$kunik?> cls-37" x="37.91" y="11.33">c</tspan>
        <tspan x="42.51" y="11.33">om</tspan>
        <tspan class="<?=$kunik?> cls-38" x="54.72" y="11.33">p</tspan>
        <tspan class="<?=$kunik?> cls-33" x="59.74" y="11.33">ét</tspan>
        <tspan x="67.26" y="11.33">en</tspan>
        <tspan class="<?=$kunik?> cls-17" x="76.74" y="11.33">c</tspan>
        <tspan x="81.34" y="11.33">es </tspan>
        <tspan class="<?=$kunik?> cls-23" x="91.68" y="11.33">r</tspan>
        <tspan x="94.67" y="11.33">es</tspan>
        <tspan class="<?=$kunik?> cls-38" x="103.39" y="11.33">p</tspan>
        <tspan x="108.42" y="11.33">e</tspan>
        <tspan class="<?=$kunik?> cls-28" x="113.25" y="11.33">c</tspan>
        <tspan x="117.8" y="11.33">ti</tspan>
        <tspan class="<?=$kunik?> cls-32" x="122.56" y="11.33">v</tspan>
        <tspan x="126.69" y="11.33">eme</tspan>
        <tspan class="<?=$kunik?> cls-30" x="143.72" y="11.33">n</tspan>
        <tspan x="148.16" y="11.33">t</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(330.25 30.38)">
        <tspan x="0" y="0" id="Pointdechangephysiqueproposepourmonterencompetencesrespectivement<?=$kunik?>">31%</tspan>
    </text>
    <path class="<?=$kunik?> cls-20" d="m557.06,58.24h-237.93c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-11" d="m374.77,57.22h-57.25c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(407.21 79.59)">
        <tspan x="0" y="0">Visi</tspan>
        <tspan class="<?=$kunik?> cls-38" x="12.85" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-17" x="17.73" y="0">c</tspan>
        <tspan x="22.33" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-1" x="27.16" y="0">n</tspan>
        <tspan class="<?=$kunik?> cls-18" x="31.65" y="0">f</tspan>
        <tspan x="34.36" y="0">é</tspan>
        <tspan class="<?=$kunik?> cls-23" x="39.18" y="0">r</tspan>
        <tspan x="42.17" y="0">en</tspan>
        <tspan class="<?=$kunik?> cls-17" x="51.65" y="0">c</tspan>
        <tspan x="56.25" y="0">e de p</tspan>
        <tspan class="<?=$kunik?> cls-23" x="79.15" y="0">r</tspan>
        <tspan x="82.14" y="0">ése</tspan>
        <tspan class="<?=$kunik?> cls-23" x="95.69" y="0">n</tspan>
        <tspan class="<?=$kunik?> cls-33" x="100.13" y="0">t</tspan>
        <tspan x="102.99" y="0">ations</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(330.25 81.79)">
        <tspan x="0" y="0" id="Visioconferencedepresentations<?=$kunik?>">29%</tspan>
    </text>
    <path class="<?=$kunik?> cls-21" d="m557.06,109.65h-237.93c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-8" d="m374.77,108.62h-57.25c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(403.51 127.34)">
        <tspan class="<?=$kunik?> cls-29" x="0" y="0">V</tspan>
        <tspan class="<?=$kunik?> cls-2" x="4.68" y="0">oy</tspan>
        <tspan x="13.44" y="0">age </tspan>
        <tspan class="<?=$kunik?> cls-31" x="29.92" y="0">a</tspan>
        <tspan x="34.98" y="0">pp</tspan>
        <tspan class="<?=$kunik?> cls-23" x="44.95" y="0">r</tspan>
        <tspan x="47.94" y="0">en</tspan>
        <tspan class="<?=$kunik?> cls-25" x="57.42" y="0">a</tspan>
        <tspan class="<?=$kunik?> cls-23" x="62.47" y="0">n</tspan>
        <tspan x="66.91" y="0">t </tspan>
        <tspan class="<?=$kunik?> cls-38" x="71.57" y="0">p</tspan>
        <tspan x="76.59" y="0">o</tspan>
        <tspan class="<?=$kunik?> cls-25" x="81.43" y="0">u</tspan>
        <tspan x="86.13" y="0">r dé</tspan>
        <tspan class="<?=$kunik?> cls-17" x="100.78" y="0">c</tspan>
        <tspan x="105.38" y="0">ou</tspan>
        <tspan class="<?=$kunik?> cls-32" x="114.86" y="0">v</tspan>
        <tspan x="119" y="0">er</tspan>
        <tspan class="<?=$kunik?> cls-33" x="127.02" y="0">t</tspan>
        <tspan x="129.88" y="0">e </tspan>
        <tspan x="52.9" y="11.33">de </tspan>
        <tspan class="<?=$kunik?> cls-38" x="64.36" y="11.33">l</tspan>
        <tspan x="66.14" y="11.33">ie</tspan>
        <tspan class="<?=$kunik?> cls-35" x="72.69" y="11.33">u</tspan>
        <tspan x="77.3" y="11.33">x</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(330.25 133.2)">
        <tspan x="0" y="0" id="Voyageapprenantpourdecouvertedelieux<?=$kunik?>">11%</tspan>
    </text>
    <path class="<?=$kunik?> cls-20" d="m557.06,161.05h-237.93c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-16" d="m374.77,160.03h-57.25c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(434.83 182.28)">
        <tspan x="0" y="0">Étude d’op</tspan>
        <tspan class="<?=$kunik?> cls-38" x="40.08" y="0">p</tspan>
        <tspan x="45.11" y="0">ort</tspan>
        <tspan class="<?=$kunik?> cls-25" x="56.16" y="0">u</tspan>
        <tspan x="60.86" y="0">ni</tspan>
        <tspan class="<?=$kunik?> cls-1" x="67.24" y="0">t</tspan>
        <tspan x="70.09" y="0">é</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(330.25 184.6)">
        <tspan x="0" y="0" id="Etudedopportunite<?=$kunik?>">29%</tspan>
    </text>
    <path class="<?=$kunik?> cls-21" d="m557.06,212.46h-237.93c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-15" d="m374.77,211.44h-57.25c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(424.43 229.97)">
        <tspan class="<?=$kunik?> cls-24" x="0" y="0">P</tspan>
        <tspan x="4.79" y="0">as d’a</tspan>
        <tspan class="<?=$kunik?> cls-28" x="27" y="0">c</tspan>
        <tspan x="31.56" y="0">tion mais i</tspan>
        <tspan class="<?=$kunik?> cls-23" x="68.79" y="0">n</tspan>
        <tspan class="<?=$kunik?> cls-1" x="73.23" y="0">t</tspan>
        <tspan x="76.09" y="0">é</tspan>
        <tspan class="<?=$kunik?> cls-23" x="80.92" y="0">r</tspan>
        <tspan x="83.91" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-22" x="88.74" y="0">s</tspan>
        <tspan x="92.67" y="0">sé </tspan>
        <tspan class="<?=$kunik?> cls-38" x="28.27" y="11.33">p</tspan>
        <tspan x="33.29" y="11.33">o</tspan>
        <tspan class="<?=$kunik?> cls-25" x="38.13" y="11.33">u</tspan>
        <tspan x="42.82" y="11.33">r le </tspan>
        <tspan class="<?=$kunik?> cls-18" x="55.85" y="11.33">f</tspan>
        <tspan x="58.56" y="11.33">ai</tspan>
        <tspan class="<?=$kunik?> cls-23" x="65.3" y="11.33">r</tspan>
        <tspan x="68.29" y="11.33">e</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(330.25 236.01)">
        <tspan x="0" y="0" id="Pasdactionmaisinteressepourlefaire<?=$kunik?>">10%</tspan>
    </text>
    <path class="<?=$kunik?> cls-20" d="m557.06,263.87h-237.93c-1.58,0-3.05.85-3.84,2.22l-8.3,14.38c-.79,1.37-.79,3.06,0,4.44l8.3,14.38c.79,1.37,2.26,2.22,3.84,2.22h237.93c1.58,0,3.05-.85,3.84-2.22l8.3-14.38c.79-1.37.79-3.06,0-4.44l-8.3-14.38c-.79-1.37-2.26-2.22-3.84-2.22Z" />
    <path class="<?=$kunik?> cls-14" d="m374.77,262.84h-57.25c-1.58,0-3.05.85-3.84,2.22l-8.89,15.4c-.79,1.37-.79,3.06,0,4.44l8.89,15.4c.79,1.37,2.26,2.22,3.84,2.22h57.25c1.58,0,3.05-.85,3.84-2.22l8.89-15.4c.79-1.37.79-3.06,0-4.44l-8.89-15.4c-.79-1.37-2.26-2.22-3.84-2.22Z" /><text class="<?=$kunik?> cls-3" transform="translate(451.81 286.15)">
        <tspan class="<?=$kunik?> cls-24" x="0" y="0">P</tspan>
        <tspan x="4.79" y="0">as d’a</tspan>
        <tspan class="<?=$kunik?> cls-28" x="27" y="0">c</tspan>
        <tspan x="31.56" y="0">tion</tspan>
    </text><text class="<?=$kunik?> cls-4" transform="translate(330.25 287.41)">
        <tspan x="0" y="0" id="Pasdaction<?=$kunik?>">02%</tspan>
    </text>
</svg>

<script>
    function removeDiacritics (str) {

        var defaultDiacriticsRemovalMap = [
            {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
            {'base':'AA','letters':/[\uA732]/g},
            {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
            {'base':'AO','letters':/[\uA734]/g},
            {'base':'AU','letters':/[\uA736]/g},
            {'base':'AV','letters':/[\uA738\uA73A]/g},
            {'base':'AY','letters':/[\uA73C]/g},
            {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
            {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
            {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
            {'base':'DZ','letters':/[\u01F1\u01C4]/g},
            {'base':'Dz','letters':/[\u01F2\u01C5]/g},
            {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
            {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
            {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
            {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
            {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
            {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
            {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
            {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
            {'base':'LJ','letters':/[\u01C7]/g},
            {'base':'Lj','letters':/[\u01C8]/g},
            {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
            {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
            {'base':'NJ','letters':/[\u01CA]/g},
            {'base':'Nj','letters':/[\u01CB]/g},
            {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
            {'base':'OI','letters':/[\u01A2]/g},
            {'base':'OO','letters':/[\uA74E]/g},
            {'base':'OU','letters':/[\u0222]/g},
            {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
            {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
            {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
            {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
            {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
            {'base':'TZ','letters':/[\uA728]/g},
            {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
            {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
            {'base':'VY','letters':/[\uA760]/g},
            {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
            {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
            {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
            {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
            {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
            {'base':'aa','letters':/[\uA733]/g},
            {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
            {'base':'ao','letters':/[\uA735]/g},
            {'base':'au','letters':/[\uA737]/g},
            {'base':'av','letters':/[\uA739\uA73B]/g},
            {'base':'ay','letters':/[\uA73D]/g},
            {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
            {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
            {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
            {'base':'dz','letters':/[\u01F3\u01C6]/g},
            {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
            {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
            {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
            {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
            {'base':'hv','letters':/[\u0195]/g},
            {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
            {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
            {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
            {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
            {'base':'lj','letters':/[\u01C9]/g},
            {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
            {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
            {'base':'nj','letters':/[\u01CC]/g},
            {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
            {'base':'oi','letters':/[\u01A3]/g},
            {'base':'ou','letters':/[\u0223]/g},
            {'base':'oo','letters':/[\uA74F]/g},
            {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
            {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
            {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
            {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
            {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
            {'base':'tz','letters':/[\uA729]/g},
            {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
            {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
            {'base':'vy','letters':/[\uA761]/g},
            {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
            {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
            {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
            {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
        ];

        for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
            str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
        }

        return str;

}
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    if(data.length>0){
        $.each(data, function(index, d){
            $("#"+removeDiacritics(d["label"]).replace(/[^a-zA-Z]/g, "")+"<?=$kunik?>").text(d["value"]+"%")
        })
    }
</script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "color01": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    },
                    "showLegend" : {
                        "label" : "Afficher la légende",
                        "inputType" : "checkboxSimple",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true,
                        "values" :  sectionDyf.<?php echo $kunik?>ParamsData.showLegend
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
