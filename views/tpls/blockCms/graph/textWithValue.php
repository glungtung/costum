<?php 
    $keyTpl = "textWithValue";

    $paramsData = [
        "name" => "",
        "textWithValue" => "",
        "textBg" => "transparent",
        "textRight" => "",
        "textLeft" => "",
        "valueFontSize" => 40,
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "withStaticTextRight"=>true,
        "withStaticTextLeft"=>false,
        "valueColor" => "#9B6FAC",
        "emptyColor" => "#FF286B",
        "roundMillionValue" => true,
        "isAverage" => false,
        "applyFormForAll" => false,
        "valBloc01" => "",
        "valBloc02" => "",
        "textAlign" => "center",
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
 ?>

<style type="text/css">
    .textWithValue<?= $kunik; ?>{
        font-size: <?php echo $paramsData["valueFontSize"]; ?>pt;
        color: <?php echo $paramsData["valueColor"]; ?> !important;
        font-weight: bolder;
        margin-bottom: -20px;
        line-height: 1;
    }

    .profil-icon{
        background-image: url("<?=Yii::app()->getModule('costum')->assetsUrl; ?>/images/franceTierslieux/profil_pro.png");
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
    }
</style>
<?php /*if($paramsData["answerPath"]=="" && $paramsData["answerValue"]==""){ ?>
    <div class="alert alert-info text-center">answerPath et answerValue ne sont pas spécifié donc la valeur affiché est le nombre total des réponses</div>
<?php } */ ?>

<div class="chartviz">
    <div class="text-<?= $paramsData["textAlign"] ?>">
        <span class="padding-10 padding-top-40" style="background:<?= $paramsData["textBg"]; ?>">
            <span class="sp-text margin-top-10" data-id="<?= $blockKey ?>" data-field="textLeft"><?= $paramsData["textLeft"] ?></span>
            <span class="textWithValue<?= $kunik; ?> margin-top-10" ></span>
            <span class="sp-text margin-top-10" data-id="<?= $blockKey ?>" data-field="textRight"><?= $paramsData["textRight"] ?></span>
        </span>
    </div>
</div>
<table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz margin-top-10">
    <tr><td>Valeur :</td><td class="textWithValue<?= $kunik; ?>"></td></tr>
</table>

<script type="text/javascript">
    
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        
        $(".textWithValue<?= $kunik; ?>").text("0");
        
        if(typeof costum["dashboardData"] != "undefined" && costum["dashboardData"]["<?= $blockKey ?>"]){
            $(".textWithValue<?= $kunik; ?>").text(costum["dashboardData"]["<?= $blockKey ?>"]);

            if(typeof costum["dashboardData"]["<?= $blockKey ?>"]['$numberDecimal'] !="undefined"){
                $(".textWithValue<?= $kunik; ?>").text(costum["dashboardData"]["<?= $blockKey ?>"]['$numberDecimal']);
            }
            var pada = sectionDyf.<?php echo $kunik ?>ParamsData;
            
            if( pada.valBloc01!="" && pada.valBloc02!="" && costum["dashboardData"][pada.valBloc01].$numberDecimal && costum["dashboardData"][pada.valBloc02].$numberDecimal){
                var v1 = parseInt(costum["dashboardData"][pada.valBloc01].$numberDecimal);
                var v2 = parseInt(costum["dashboardData"][pada.valBloc02].$numberDecimal);
                var sum = v1+v2;
                $(".textWithValue<?= $kunik; ?>").text((sum));
            }
        }

        $("#tableData<?=$kunik?>").hide();

        //$(".textWithValue<?= $kunik; ?>").text(parseFloat($(".textWithValue<?= $kunik; ?>").text()).toLocaleString('en'));
        
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "textAlign": {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Alignment des textes",
                        "options" : {
                            "center" : "Centre",
                            "left": "Gauche",
                            "right": "Droite"
                        }
                    },
                    "valueColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "textBg": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de fond du texte"
                    },
                    "valueFontSize": {
                        "inputType" : "text",
                        "rules": {"number":true},
                        "label" : "Taille du texte"
                    },
                    "withStaticTextLeft": {
                        "inputType" : "checkboxSimple",
                        "label" : "Texte à gauche",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : false
                    },
                    "withStaticTextRight": {
                        "inputType" : "checkboxSimple",
                        "label" : "Texte à droite",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : false
                    },
                    "roundMillionValue": {
                        "inputType" : "checkboxSimple",
                        "label" : "Arrondir la valeur en million par M",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true
                    },
                    "isAverage": {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher la valeur en moyen",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.isAverage
                    },
                    "applyFormForAll": {
                        "inputType" : "checkboxSimple",
                        "label" : "Appliquer le formulaire à tous les chartes graphiques de la page",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : false
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
                    var formToApply = "";
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val();
                        }else if(k=="applyFormForAll"){
                            if($("#applyFormForAll").val()=="true"){
                                formToApply = $("#coform").val();
                            }
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                            if(formToApply!=""){
                                ajaxPost(
                                    null, 
                                    baseUrl+"/costum/blockgraph/applyformtodashboard", 
                                    {
                                        "contextId":costum.contextId,  
                                        "page": location.hash.replace("#",""),
                                        "coform": $("#coform").val()
                                    }, 
                                    function(res){
                                        $("#ajax-modal").modal('hide');
                                        urlCtrl.loadByHash(location.hash);
                                    },
                                    null,
                                    null,
                                    {async:false}
                                )
                            }else{
                                toastr.success("La paramètre est bien enregistrée");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            }
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(typeof input !="undefined"){
                if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                    if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                        for(const paramValue of coform["params"][input]["global"]["list"]){
                            $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                        }
                    }
                }
                if(input.includes("checkboxNew") || input.includes("radioNew")){
                    for(const paramValue of coform["params"][input]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            $("#answerPath.<?php echo $kunik ?>").append('<option value="">Nombre de réponses</option>');
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""
if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }else if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }else if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }else if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }else if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }else if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }else{
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>

