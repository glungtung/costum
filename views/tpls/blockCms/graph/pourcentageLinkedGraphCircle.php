<?php 
    $keyTpl     = "pourcentageLinkedGraphCircle";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "color01" => "#9B6FAC",
        "color02" => "#ddd",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>

<svg id="Calque_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 588.41 349.42">
    <defs>
        <style>
            .<?=$kunik?>.cls-1 {
                font-size: 7.31px;
            }

            .<?=$kunik?>.cls-1,
            .<?=$kunik?>.cls-2 {
                fill: #fff;
                font-family: SharpSansNo1-Semibold, 'Sharp Sans No1';
                font-weight: 600;
            }

            .<?=$kunik?>.cls-2 {
                font-size: 11px;
            }

            .<?=$kunik?>.cls-3 {
                stroke-width: 1.08px;
            }

            .<?=$kunik?>.cls-3,
            .<?=$kunik?>.cls-4,
            .<?=$kunik?>.cls-5,
            .<?=$kunik?>.cls-6,
            .<?=$kunik?>.cls-7,
            .<?=$kunik?>.cls-8,
            .<?=$kunik?>.cls-9,
            .<?=$kunik?>.cls-10,
            .<?=$kunik?>.cls-11,
            .<?=$kunik?>.cls-12,
            .<?=$kunik?>.cls-13,
            .<?=$kunik?>.cls-14,
            .<?=$kunik?>.cls-15,
            .<?=$kunik?>.cls-16,
            .<?=$kunik?>.cls-17 {
                fill: none;
                stroke: #a4bf45;
                stroke-miterlimit: 10;
            }

            .<?=$kunik?>.cls-4 {
                stroke-width: .56px;
            }

            .<?=$kunik?>.cls-18 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-19 {
                letter-spacing: -.04em;
            }

            .<?=$kunik?>.cls-20 {
                letter-spacing: -.04em;
            }

            .<?=$kunik?>.cls-21 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-22 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-5 {
                stroke-width: .82px;
            }

            .<?=$kunik?>.cls-23 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-6 {
                stroke-width: .81px;
            }

            .<?=$kunik?>.cls-24 {
                fill: #a4bf45;
            }

            .<?=$kunik?>.cls-25 {
                letter-spacing: -.07em;
            }

            .<?=$kunik?>.cls-26 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-27 {
                letter-spacing: .01em;
            }

            .<?=$kunik?>.cls-7 {
                stroke-width: .91px;
            }

            .<?=$kunik?>.cls-28 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-29 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-30 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-31 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-8 {
                stroke-dasharray: 0 2.08;
                stroke-width: 1.03px;
            }

            .<?=$kunik?>.cls-32 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-9 {
                stroke-width: 1.03px;
            }

            .<?=$kunik?>.cls-10 {
                stroke-width: 1.05px;
            }

            .<?=$kunik?>.cls-11 {
                stroke-width: 1.02px;
            }

            .<?=$kunik?>.cls-33 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-12 {
                stroke-width: 1.16px;
            }

            .<?=$kunik?>.cls-34 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-13 {
                stroke-width: 1.11px;
            }

            .<?=$kunik?>.cls-14 {
                stroke-width: 1.16px;
            }

            .<?=$kunik?>.cls-35 {
                letter-spacing: .01em;
            }

            .<?=$kunik?>.cls-36 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-37 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-38 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-39 {
                letter-spacing: 0em;
            }

            .<?=$kunik?>.cls-15 {
                stroke-width: 1.3px;
            }

            .<?=$kunik?>.cls-40 {
                letter-spacing: -.03em;
            }

            .<?=$kunik?>.cls-41 {
                letter-spacing: -.02em;
            }

            .<?=$kunik?>.cls-16 {
                stroke-width: .67px;
            }

            .<?=$kunik?>.cls-42 {
                letter-spacing: -.01em;
            }

            .<?=$kunik?>.cls-17 {
                stroke-width: .97px;
            }

            .<?=$kunik?>.cls-43 {
                letter-spacing: 0em;
            }
        </style>
    </defs><text class="<?=$kunik?> cls-1" transform="translate(256.81 54.61)">
        <tspan x="0" y="0">Uni</tspan>
        <tspan class="<?=$kunik?> cls-40" x="10.19" y="0">v</tspan>
        <tspan x="13.86" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-39" x="18.15" y="0">r</tspan>
        <tspan x="20.95" y="0">si</tspan>
        <tspan class="<?=$kunik?> cls-18" x="25.93" y="0">t</tspan>
        <tspan x="28.47" y="0">és - </tspan>
        <tspan x="9.53" y="10.05">é</tspan>
        <tspan class="<?=$kunik?> cls-21" x="13.81" y="10.05">c</tspan>
        <tspan x="17.89" y="10.05">oles </tspan>
        <tspan x="-9.86" y="20.11">d’ingénie</tspan>
        <tspan class="<?=$kunik?> cls-31" x="20.4" y="20.11">u</tspan>
        <tspan class="<?=$kunik?> cls-39" x="24.57" y="20.11">r</tspan>
        <tspan x="27.37" y="20.11">s ou de </tspan>
        <tspan class="<?=$kunik?> cls-21" x="-9.32" y="30.16">c</tspan>
        <tspan x="-5.24" y="30.16">omme</tspan>
        <tspan class="<?=$kunik?> cls-30" x="16.42" y="30.16">r</tspan>
        <tspan class="<?=$kunik?> cls-29" x="19.08" y="30.16">c</tspan>
        <tspan x="23.16" y="30.16">e - E</tspan>
        <tspan class="<?=$kunik?> cls-43" x="37.62" y="30.16">P</tspan>
        <tspan class="<?=$kunik?> cls-30" x="42.04" y="30.16">S</tspan>
        <tspan x="45.97" y="30.16">T </tspan>
        <tspan x="1.47" y="40.21">ou </tspan>
        <tspan class="<?=$kunik?> cls-43" x="11.33" y="40.21">p</tspan>
        <tspan x="15.79" y="40.21">ôles de </tspan>
        <tspan class="<?=$kunik?> cls-21" x="-1.61" y="50.26">c</tspan>
        <tspan x="2.47" y="50.26">om</tspan>
        <tspan class="<?=$kunik?> cls-43" x="13.3" y="50.26">p</tspan>
        <tspan class="<?=$kunik?> cls-18" x="17.76" y="50.26">é</tspan>
        <tspan x="21.9" y="50.26">titivi</tspan>
        <tspan class="<?=$kunik?> cls-41" x="35.78" y="50.26">t</tspan>
        <tspan x="38.32" y="50.26">é</tspan>
    </text><text class="<?=$kunik?> cls-1" transform="translate(15.22 177.91)">
        <tspan class="<?=$kunik?> cls-27" x="0" y="0">O</tspan>
        <tspan class="<?=$kunik?> cls-30" x="6.06" y="0">r</tspan>
        <tspan x="8.71" y="0">g</tspan>
        <tspan class="<?=$kunik?> cls-31" x="13.17" y="0">a</tspan>
        <tspan x="17.66" y="0">nismes de </tspan>
        <tspan class="<?=$kunik?> cls-23" x="7.3" y="10.05">f</tspan>
        <tspan x="9.71" y="10.05">ormations</tspan>
    </text>
    <circle class="<?=$kunik?> cls-9" cx="41.81" cy="180.26" r="34.17" /><text class="<?=$kunik?> cls-1" transform="translate(52.2 274.24)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">S</tspan>
        <tspan x="3.91" y="0">tru</tspan>
        <tspan class="<?=$kunik?> cls-34" x="13.55" y="0">c</tspan>
        <tspan x="17.6" y="0">t</tspan>
        <tspan class="<?=$kunik?> cls-37" x="20.28" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="24.45" y="0">r</tspan>
        <tspan x="27.1" y="0">es de </tspan>
        <tspan x="-2.42" y="10.05">l’insertion d</tspan>
        <tspan class="<?=$kunik?> cls-31" x="35.36" y="10.05">a</tspan>
        <tspan x="39.85" y="10.05">ns </tspan>
        <tspan x="-6.75" y="20.11">l’emploi (mi</tspan>
        <tspan class="<?=$kunik?> cls-43" x="31.42" y="20.11">s</tspan>
        <tspan x="34.91" y="20.11">sions </tspan>
        <tspan x="-10.92" y="30.16">l</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-9.37" y="30.16">o</tspan>
        <tspan class="<?=$kunik?> cls-21" x="-5.04" y="30.16">c</tspan>
        <tspan x="-.96" y="30.16">ales, </tspan>
        <tspan class="<?=$kunik?> cls-43" x="15.74" y="30.16">p</tspan>
        <tspan x="20.2" y="30.16">ôle emploi, </tspan>
        <tspan x="-5.08" y="40.21">a</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-.64" y="40.21">s</tspan>
        <tspan x="2.85" y="40.21">s</tspan>
        <tspan class="<?=$kunik?> cls-36" x="6.3" y="40.21">o</tspan>
        <tspan x="10.63" y="40.21">ciation, </tspan>
        <tspan class="<?=$kunik?> cls-18" x="36.32" y="40.21">et</tspan>
        <tspan x="43" y="40.21">c)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-15" cx="75.78" cy="293.46" r="43.14" />
    <circle class="<?=$kunik?> cls-12" cx="276.55" cy="76.21" r="47.65" /><text class="<?=$kunik?> cls-1" transform="translate(352.53 94.27)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">A</tspan>
        <tspan class="<?=$kunik?> cls-34" x="4.88" y="0">c</tspan>
        <tspan class="<?=$kunik?> cls-18" x="8.93" y="0">t</tspan>
        <tspan x="11.46" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-31" x="15.75" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-22" x="19.92" y="0">r</tspan>
        <tspan x="22.71" y="0">s de </tspan>
        <tspan x="-1.6" y="10.05">l’orie</tspan>
        <tspan class="<?=$kunik?> cls-30" x="14.38" y="10.05">n</tspan>
        <tspan class="<?=$kunik?> cls-18" x="18.32" y="10.05">t</tspan>
        <tspan x="20.86" y="10.05">ation</tspan>
    </text>
    <circle class="<?=$kunik?> cls-6" cx="370.92" cy="96.41" r="33.17" /><text class="<?=$kunik?> cls-1" transform="translate(308.61 181.66)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">S</tspan>
        <tspan x="3.91" y="0">tru</tspan>
        <tspan class="<?=$kunik?> cls-34" x="13.55" y="0">c</tspan>
        <tspan x="17.6" y="0">t</tspan>
        <tspan class="<?=$kunik?> cls-37" x="20.28" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="24.45" y="0">r</tspan>
        <tspan x="27.1" y="0">es je</tspan>
        <tspan class="<?=$kunik?> cls-31" x="42.1" y="0">u</tspan>
        <tspan x="46.27" y="0">ne</tspan>
        <tspan class="<?=$kunik?> cls-36" x="54.68" y="0">s</tspan>
        <tspan x="58.16" y="0">se </tspan>
        <tspan class="<?=$kunik?> cls-18" x="67.35" y="0">e</tspan>
        <tspan x="71.48" y="0">t </tspan>
        <tspan x="6.4" y="10.05">d’</tspan>
        <tspan class="<?=$kunik?> cls-31" x="12.33" y="10.05">a</tspan>
        <tspan x="16.82" y="10.05">nimation l</tspan>
        <tspan class="<?=$kunik?> cls-43" x="49.09" y="10.05">o</tspan>
        <tspan class="<?=$kunik?> cls-21" x="53.42" y="10.05">c</tspan>
        <tspan x="57.5" y="10.05">ale </tspan>
        <tspan x="-3.53" y="20.11">(Mi</tspan>
        <tspan class="<?=$kunik?> cls-43" x="7.04" y="20.11">s</tspan>
        <tspan x="10.53" y="20.11">sions </tspan>
        <tspan class="<?=$kunik?> cls-38" x="28.83" y="20.11">L</tspan>
        <tspan class="<?=$kunik?> cls-36" x="32.36" y="20.11">o</tspan>
        <tspan class="<?=$kunik?> cls-29" x="36.69" y="20.11">c</tspan>
        <tspan x="40.77" y="20.11">ales, </tspan>
        <tspan class="<?=$kunik?> cls-26" x="57.47" y="20.11">P</tspan>
        <tspan x="61.72" y="20.11">oi</tspan>
        <tspan class="<?=$kunik?> cls-28" x="67.55" y="20.11">n</tspan>
        <tspan class="<?=$kunik?> cls-27" x="71.49" y="20.11">t</tspan>
        <tspan x="74.24" y="20.11">s </tspan>
        <tspan class="<?=$kunik?> cls-43" x="4.23" y="30.16">I</tspan>
        <tspan class="<?=$kunik?> cls-41" x="5.98" y="30.16">n</tspan>
        <tspan class="<?=$kunik?> cls-23" x="9.96" y="30.16">f</tspan>
        <tspan x="12.37" y="30.16">os Je</tspan>
        <tspan class="<?=$kunik?> cls-31" x="29.39" y="30.16">u</tspan>
        <tspan x="33.56" y="30.16">ne</tspan>
        <tspan class="<?=$kunik?> cls-43" x="41.97" y="30.16">s</tspan>
        <tspan x="45.46" y="30.16">se, </tspan>
        <tspan class="<?=$kunik?> cls-41" x="56.17" y="30.16">et</tspan>
        <tspan x="62.85" y="30.16">c)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-14" cx="343.43" cy="194.81" r="47.62" /><text class="<?=$kunik?> cls-1" transform="translate(201.44 172.56)">
        <tspan x="0" y="0">Un </tspan>
        <tspan class="<?=$kunik?> cls-43" x="10.1" y="0">p</tspan>
        <tspan x="14.56" y="0">ôle </tspan>
        <tspan class="<?=$kunik?> cls-41" x="26.13" y="0">t</tspan>
        <tspan x="28.67" y="0">erri</tspan>
        <tspan class="<?=$kunik?> cls-18" x="40.16" y="0">t</tspan>
        <tspan x="42.69" y="0">orial de </tspan>
        <tspan class="<?=$kunik?> cls-21" x="13.17" y="10.05">c</tspan>
        <tspan class="<?=$kunik?> cls-43" x="17.25" y="10.05">o</tspan>
        <tspan x="21.58" y="10.05">o</tspan>
        <tspan class="<?=$kunik?> cls-43" x="25.87" y="10.05">p</tspan>
        <tspan x="30.33" y="10.05">é</tspan>
        <tspan class="<?=$kunik?> cls-30" x="34.61" y="10.05">r</tspan>
        <tspan x="37.27" y="10.05">ation </tspan>
        <tspan x=".33" y="20.11">é</tspan>
        <tspan class="<?=$kunik?> cls-21" x="4.61" y="20.11">c</tspan>
        <tspan x="8.69" y="20.11">onomique (</tspan>
        <tspan class="<?=$kunik?> cls-19" x="46.76" y="20.11">P</tspan>
        <tspan class="<?=$kunik?> cls-25" x="50.84" y="20.11">T</tspan>
        <tspan x="54.68" y="20.11">CE)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-11" cx="234.49" cy="179.06" r="41.83" /><text class="<?=$kunik?> cls-1" transform="translate(194.27 274.17)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">S</tspan>
        <tspan x="3.91" y="0">tru</tspan>
        <tspan class="<?=$kunik?> cls-34" x="13.55" y="0">c</tspan>
        <tspan x="17.6" y="0">t</tspan>
        <tspan class="<?=$kunik?> cls-37" x="20.28" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="24.45" y="0">r</tspan>
        <tspan x="27.1" y="0">es </tspan>
        <tspan x="-12.83" y="10.05">mé</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-2.01" y="10.05">d</tspan>
        <tspan x="2.47" y="10.05">i</tspan>
        <tspan class="<?=$kunik?> cls-21" x="4" y="10.05">c</tspan>
        <tspan x="8.08" y="10.05">o-s</tspan>
        <tspan class="<?=$kunik?> cls-43" x="19.16" y="10.05">o</tspan>
        <tspan x="23.49" y="10.05">ciale </tspan>
        <tspan class="<?=$kunik?> cls-41" x="40.85" y="10.05">e</tspan>
        <tspan x="44.99" y="10.05">t </tspan>
        <tspan x="2.89" y="20.11">s</tspan>
        <tspan class="<?=$kunik?> cls-31" x="6.34" y="20.11">a</tspan>
        <tspan x="10.83" y="20.11">ni</tspan>
        <tspan class="<?=$kunik?> cls-18" x="16.49" y="20.11">t</tspan>
        <tspan x="19.03" y="20.11">ai</tspan>
        <tspan class="<?=$kunik?> cls-30" x="25.01" y="20.11">r</tspan>
        <tspan x="27.66" y="20.11">e</tspan>
    </text>
    <circle class="<?=$kunik?> cls-5" cx="211.13" cy="280.23" r="33.92" /><text class="<?=$kunik?> cls-1" transform="translate(484.02 290.03)">
        <tspan class="<?=$kunik?> cls-23" x="0" y="0">A</tspan>
        <tspan x="4.87" y="0">uc</tspan>
        <tspan class="<?=$kunik?> cls-31" x="13.09" y="0">u</tspan>
        <tspan x="17.26" y="0">n</tspan>
    </text>
    <circle class="<?=$kunik?> cls-4" cx="494.88" cy="287.19" r="23.18" /><text class="<?=$kunik?> cls-1" transform="translate(107.64 186.55)">
        <tspan class="<?=$kunik?> cls-27" x="0" y="0">O</tspan>
        <tspan class="<?=$kunik?> cls-30" x="6.06" y="0">r</tspan>
        <tspan x="8.71" y="0">g</tspan>
        <tspan class="<?=$kunik?> cls-31" x="13.17" y="0">a</tspan>
        <tspan x="17.66" y="0">nismes du </tspan>
        <tspan class="<?=$kunik?> cls-21" x="-12.3" y="10.05">c</tspan>
        <tspan x="-8.22" y="10.05">omm</tspan>
        <tspan class="<?=$kunik?> cls-31" x="9.15" y="10.05">u</tspan>
        <tspan x="13.32" y="10.05">nau</tspan>
        <tspan class="<?=$kunik?> cls-18" x="26.01" y="10.05">t</tspan>
        <tspan x="28.55" y="10.05">ai</tspan>
        <tspan class="<?=$kunik?> cls-30" x="34.53" y="10.05">r</tspan>
        <tspan x="37.18" y="10.05">e </tspan>
        <tspan class="<?=$kunik?> cls-18" x="42.91" y="10.05">e</tspan>
        <tspan x="47.05" y="10.05">t des </tspan>
        <tspan x="-11.06" y="20.11">so</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-3.32" y="20.11">l</tspan>
        <tspan x="-1.74" y="20.11">id</tspan>
        <tspan class="<?=$kunik?> cls-31" x="4.23" y="20.11">a</tspan>
        <tspan x="8.72" y="20.11">ri</tspan>
        <tspan class="<?=$kunik?> cls-41" x="13.09" y="20.11">t</tspan>
        <tspan x="15.63" y="20.11">és (</tspan>
        <tspan class="<?=$kunik?> cls-33" x="27.8" y="20.11">s</tspan>
        <tspan x="31.19" y="20.11">tru</tspan>
        <tspan class="<?=$kunik?> cls-33" x="40.83" y="20.11">c</tspan>
        <tspan x="44.88" y="20.11">t</tspan>
        <tspan class="<?=$kunik?> cls-31" x="47.56" y="20.11">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="51.73" y="20.11">r</tspan>
        <tspan x="54.38" y="20.11">es </tspan>
        <tspan class="<?=$kunik?> cls-21" x="-7.88" y="30.16">c</tspan>
        <tspan class="<?=$kunik?> cls-31" x="-3.8" y="30.16">a</tspan>
        <tspan x=".68" y="30.16">ri</tspan>
        <tspan class="<?=$kunik?> cls-18" x="5.06" y="30.16">t</tspan>
        <tspan x="7.59" y="30.16">ati</tspan>
        <tspan class="<?=$kunik?> cls-40" x="16.26" y="30.16">v</tspan>
        <tspan x="19.93" y="30.16">es, </tspan>
        <tspan class="<?=$kunik?> cls-38" x="30.64" y="30.16">s</tspan>
        <tspan class="<?=$kunik?> cls-42" x="33.93" y="30.16">y</tspan>
        <tspan class="<?=$kunik?> cls-33" x="37.9" y="30.16">s</tspan>
        <tspan class="<?=$kunik?> cls-18" x="41.29" y="30.16">t</tspan>
        <tspan x="43.83" y="30.16">ème </tspan>
        <tspan x="-2.56" y="40.21">d’éch</tspan>
        <tspan class="<?=$kunik?> cls-31" x="15.89" y="40.21">a</tspan>
        <tspan x="20.38" y="40.21">nge l</tspan>
        <tspan class="<?=$kunik?> cls-43" x="36.23" y="40.21">o</tspan>
        <tspan class="<?=$kunik?> cls-21" x="40.55" y="40.21">c</tspan>
        <tspan x="44.63" y="40.21">al)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-3" cx="133.08" cy="203.36" r="44.57" /><text class="<?=$kunik?> cls-1" transform="translate(432.27 77.96)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">A</tspan>
        <tspan class="<?=$kunik?> cls-34" x="4.88" y="0">c</tspan>
        <tspan class="<?=$kunik?> cls-18" x="8.93" y="0">t</tspan>
        <tspan x="11.46" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-31" x="15.75" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-22" x="19.92" y="0">r</tspan>
        <tspan x="22.71" y="0">s de la </tspan>
        <tspan x="6.1" y="10.05">t</tspan>
        <tspan class="<?=$kunik?> cls-30" x="8.78" y="10.05">r</tspan>
        <tspan class="<?=$kunik?> cls-31" x="11.44" y="10.05">a</tspan>
        <tspan x="15.93" y="10.05">nsition </tspan>
        <tspan x="3.22" y="20.11">é</tspan>
        <tspan class="<?=$kunik?> cls-21" x="7.5" y="20.11">c</tspan>
        <tspan x="11.58" y="20.11">ol</tspan>
        <tspan class="<?=$kunik?> cls-43" x="17.42" y="20.11">o</tspan>
        <tspan class="<?=$kunik?> cls-21" x="21.74" y="20.11">g</tspan>
        <tspan x="26.17" y="20.11">ique</tspan>
    </text>
    <circle class="<?=$kunik?> cls-6" cx="454.21" cy="84.03" r="33.17" /><text class="<?=$kunik?> cls-1" transform="translate(414.47 169.12)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">S</tspan>
        <tspan x="3.91" y="0">tru</tspan>
        <tspan class="<?=$kunik?> cls-34" x="13.55" y="0">c</tspan>
        <tspan x="17.6" y="0">t</tspan>
        <tspan class="<?=$kunik?> cls-37" x="20.28" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="24.45" y="0">r</tspan>
        <tspan x="27.1" y="0">es de </tspan>
        <tspan x="3.04" y="10.05">l’</tspan>
        <tspan class="<?=$kunik?> cls-31" x="6.08" y="10.05">a</tspan>
        <tspan x="10.57" y="10.05">rt </tspan>
        <tspan class="<?=$kunik?> cls-18" x="17.54" y="10.05">e</tspan>
        <tspan x="21.67" y="10.05">t de la </tspan>
        <tspan x="10.72" y="20.11">cult</tspan>
        <tspan class="<?=$kunik?> cls-31" x="23.17" y="20.11">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="27.34" y="20.11">r</tspan>
        <tspan x="30" y="20.11">e</tspan>
    </text>
    <circle class="<?=$kunik?> cls-7" cx="436.98" cy="175.07" r="37.42" /><text class="<?=$kunik?> cls-1" transform="translate(409.94 273.02)">
        <tspan class="<?=$kunik?> cls-23" x="0" y="0">A</tspan>
        <tspan x="4.87" y="0">ut</tspan>
        <tspan class="<?=$kunik?> cls-30" x="11.67" y="0">r</tspan>
        <tspan x="14.33" y="0">es </tspan>
        <tspan x="-5.76" y="10.05">tie</tspan>
        <tspan class="<?=$kunik?> cls-39" x="2.74" y="10.05">r</tspan>
        <tspan x="5.54" y="10.05">s-</tspan>
        <tspan class="<?=$kunik?> cls-43" x="12.33" y="10.05">l</tspan>
        <tspan x="13.91" y="10.05">ie</tspan>
        <tspan class="<?=$kunik?> cls-39" x="19.73" y="10.05">u</tspan>
        <tspan x="23.82" y="10.05">x</tspan>
    </text>
    <circle class="<?=$kunik?> cls-16" cx="420.97" cy="274.72" r="27.63" /><text class="<?=$kunik?> cls-1" transform="translate(297.69 293.36)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">S</tspan>
        <tspan x="3.91" y="0">tru</tspan>
        <tspan class="<?=$kunik?> cls-34" x="13.55" y="0">c</tspan>
        <tspan x="17.6" y="0">t</tspan>
        <tspan class="<?=$kunik?> cls-37" x="20.28" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="24.45" y="0">r</tspan>
        <tspan x="27.1" y="0">es </tspan>
        <tspan x="-21.03" y="10.05">d’a</tspan>
        <tspan class="<?=$kunik?> cls-21" x="-10.66" y="10.05">cc</tspan>
        <tspan x="-2.5" y="10.05">om</tspan>
        <tspan class="<?=$kunik?> cls-36" x="8.34" y="10.05">p</tspan>
        <tspan x="12.8" y="10.05">a</tspan>
        <tspan class="<?=$kunik?> cls-37" x="17.24" y="10.05">g</tspan>
        <tspan x="21.74" y="10.05">neme</tspan>
        <tspan class="<?=$kunik?> cls-30" x="40.97" y="10.05">n</tspan>
        <tspan x="44.91" y="10.05">t </tspan>
        <tspan class="<?=$kunik?> cls-41" x="49.04" y="10.05">e</tspan>
        <tspan x="53.18" y="10.05">t </tspan>
        <tspan x="-14.11" y="20.11">de fin</tspan>
        <tspan class="<?=$kunik?> cls-31" x="4.35" y="20.11">a</tspan>
        <tspan x="8.83" y="20.11">n</tspan>
        <tspan class="<?=$kunik?> cls-21" x="12.96" y="20.11">c</tspan>
        <tspan x="17.04" y="20.11">eme</tspan>
        <tspan class="<?=$kunik?> cls-28" x="32.15" y="20.11">n</tspan>
        <tspan x="36.09" y="20.11">t de </tspan>
        <tspan x="-10.74" y="30.16">l’e</tspan>
        <tspan class="<?=$kunik?> cls-30" x="-3.42" y="30.16">n</tspan>
        <tspan x=".52" y="30.16">t</tspan>
        <tspan class="<?=$kunik?> cls-30" x="3.2" y="30.16">r</tspan>
        <tspan x="5.86" y="30.16">ep</tspan>
        <tspan class="<?=$kunik?> cls-30" x="14.56" y="30.16">r</tspan>
        <tspan x="17.22" y="30.16">ene</tspan>
        <tspan class="<?=$kunik?> cls-31" x="29.91" y="30.16">u</tspan>
        <tspan x="34.08" y="30.16">riat</tspan>
    </text>
    <circle class="<?=$kunik?> cls-10" cx="315.11" cy="305.78" r="43.12" /><text class="<?=$kunik?> cls-1" transform="translate(518.75 83.76)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">A</tspan>
        <tspan class="<?=$kunik?> cls-31" x="4.88" y="0">g</tspan>
        <tspan x="9.38" y="0">ricult</tspan>
        <tspan class="<?=$kunik?> cls-31" x="26.2" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="30.37" y="0">r</tspan>
        <tspan x="33.02" y="0">e </tspan>
        <tspan class="<?=$kunik?> cls-18" x="38.75" y="0">e</tspan>
        <tspan x="42.89" y="0">t </tspan>
        <tspan x="-17.44" y="10.05">a</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-13" y="10.05">l</tspan>
        <tspan x="-11.42" y="10.05">ime</tspan>
        <tspan class="<?=$kunik?> cls-30" x=".95" y="10.05">n</tspan>
        <tspan class="<?=$kunik?> cls-41" x="4.89" y="10.05">t</tspan>
        <tspan x="7.42" y="10.05">ation (ch</tspan>
        <tspan class="<?=$kunik?> cls-37" x="37.17" y="10.05">a</tspan>
        <tspan x="41.66" y="10.05">mb</tspan>
        <tspan class="<?=$kunik?> cls-30" x="52.63" y="10.05">r</tspan>
        <tspan x="55.28" y="10.05">es </tspan>
        <tspan x="-8.71" y="20.11">d’a</tspan>
        <tspan class="<?=$kunik?> cls-31" x="1.66" y="20.11">g</tspan>
        <tspan x="6.16" y="20.11">ricult</tspan>
        <tspan class="<?=$kunik?> cls-31" x="22.98" y="20.11">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="27.15" y="20.11">r</tspan>
        <tspan x="29.8" y="20.11">es, </tspan>
        <tspan class="<?=$kunik?> cls-18" x="40.52" y="20.11">et</tspan>
        <tspan x="47.19" y="20.11">c)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-13" cx="542.03" cy="91.44" r="45.82" /><text class="<?=$kunik?> cls-1" transform="translate(496.49 188.94)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">S</tspan>
        <tspan x="3.91" y="0">tru</tspan>
        <tspan class="<?=$kunik?> cls-34" x="13.55" y="0">c</tspan>
        <tspan x="17.6" y="0">t</tspan>
        <tspan class="<?=$kunik?> cls-37" x="20.28" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-30" x="24.45" y="0">r</tspan>
        <tspan x="27.1" y="0">es de </tspan>
        <tspan x="-12.65" y="10.05">l’É</tspan>
        <tspan class="<?=$kunik?> cls-21" x="-5.67" y="10.05">c</tspan>
        <tspan x="-1.59" y="10.05">onomie </tspan>
        <tspan class="<?=$kunik?> cls-35" x="24.93" y="10.05">S</tspan>
        <tspan class="<?=$kunik?> cls-43" x="29.15" y="10.05">o</tspan>
        <tspan x="33.48" y="10.05">ciale </tspan>
        <tspan class="<?=$kunik?> cls-41" x="50.83" y="10.05">e</tspan>
        <tspan x="54.97" y="10.05">t </tspan>
        <tspan class="<?=$kunik?> cls-35" x="-4.55" y="20.11">S</tspan>
        <tspan x="-.33" y="20.11">o</tspan>
        <tspan class="<?=$kunik?> cls-36" x="3.97" y="20.11">l</tspan>
        <tspan x="5.55" y="20.11">idai</tspan>
        <tspan class="<?=$kunik?> cls-30" x="17.5" y="20.11">r</tspan>
        <tspan x="20.15" y="20.11">e l</tspan>
        <tspan class="<?=$kunik?> cls-43" x="27.43" y="20.11">o</tspan>
        <tspan class="<?=$kunik?> cls-29" x="31.75" y="20.11">c</tspan>
        <tspan x="35.83" y="20.11">ales</tspan>
    </text>
    <circle class="<?=$kunik?> cls-17" cx="518.22" cy="195.87" r="40.01" />
    <circle class="<?=$kunik?> cls-9" cx="58.75" cy="70.45" r="52.97" /><text class="<?=$kunik?> cls-1" transform="translate(40.66 40.83)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">A</tspan>
        <tspan class="<?=$kunik?> cls-34" x="4.88" y="0">c</tspan>
        <tspan class="<?=$kunik?> cls-18" x="8.93" y="0">t</tspan>
        <tspan x="11.46" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-31" x="15.75" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-22" x="19.92" y="0">r</tspan>
        <tspan x="22.71" y="0">s du </tspan>
        <tspan x="-8.37" y="10.05">d</tspan>
        <tspan class="<?=$kunik?> cls-23" x="-3.93" y="10.05">é</tspan>
        <tspan class="<?=$kunik?> cls-40" x=".13" y="10.05">v</tspan>
        <tspan x="3.8" y="10.05">elop</tspan>
        <tspan class="<?=$kunik?> cls-36" x="18.34" y="10.05">p</tspan>
        <tspan x="22.8" y="10.05">eme</tspan>
        <tspan class="<?=$kunik?> cls-30" x="37.92" y="10.05">n</tspan>
        <tspan x="41.86" y="10.05">t </tspan>
        <tspan x="-22.17" y="20.11">é</tspan>
        <tspan class="<?=$kunik?> cls-21" x="-17.88" y="20.11">c</tspan>
        <tspan x="-13.8" y="20.11">onomique (ch</tspan>
        <tspan class="<?=$kunik?> cls-31" x="32.5" y="20.11">a</tspan>
        <tspan x="36.98" y="20.11">mb</tspan>
        <tspan class="<?=$kunik?> cls-28" x="47.95" y="20.11">r</tspan>
        <tspan x="50.6" y="20.11">es </tspan>
        <tspan class="<?=$kunik?> cls-21" x="-14.26" y="30.16">c</tspan>
        <tspan x="-10.18" y="30.16">on</tspan>
        <tspan class="<?=$kunik?> cls-22" x="-1.77" y="30.16">s</tspan>
        <tspan x="1.65" y="30.16">ulai</tspan>
        <tspan class="<?=$kunik?> cls-30" x="13.29" y="30.16">r</tspan>
        <tspan x="15.95" y="30.16">es, CRE</tspan>
        <tspan class="<?=$kunik?> cls-36" x="40.63" y="30.16">S</tspan>
        <tspan x="44.78" y="30.16">S, </tspan>
        <tspan class="<?=$kunik?> cls-43" x="-21.7" y="40.21">p</tspan>
        <tspan x="-17.24" y="40.21">épiniè</tspan>
        <tspan class="<?=$kunik?> cls-30" x="2.94" y="40.21">r</tspan>
        <tspan x="5.59" y="40.21">es, incu</tspan>
        <tspan class="<?=$kunik?> cls-36" x="30.2" y="40.21">b</tspan>
        <tspan x="34.66" y="40.21">a</tspan>
        <tspan class="<?=$kunik?> cls-18" x="39.1" y="40.21">t</tspan>
        <tspan x="41.64" y="40.21">e</tspan>
        <tspan class="<?=$kunik?> cls-31" x="45.92" y="40.21">u</tspan>
        <tspan class="<?=$kunik?> cls-22" x="50.09" y="40.21">r</tspan>
        <tspan x="52.89" y="40.21">s, </tspan>
        <tspan class="<?=$kunik?> cls-21" x="-26.03" y="50.26">c</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-21.95" y="50.26">o</tspan>
        <tspan x="-17.62" y="50.26">o</tspan>
        <tspan class="<?=$kunik?> cls-43" x="-13.33" y="50.26">p</tspan>
        <tspan x="-8.87" y="50.26">é</tspan>
        <tspan class="<?=$kunik?> cls-30" x="-4.59" y="50.26">r</tspan>
        <tspan x="-1.93" y="50.26">ati</tspan>
        <tspan class="<?=$kunik?> cls-40" x="6.73" y="50.26">v</tspan>
        <tspan x="10.4" y="50.26">es d’a</tspan>
        <tspan class="<?=$kunik?> cls-33" x="29.96" y="50.26">c</tspan>
        <tspan x="34" y="50.26">tivi</tspan>
        <tspan class="<?=$kunik?> cls-41" x="43.66" y="50.26">t</tspan>
        <tspan x="46.2" y="50.26">és </tspan>
        <tspan class="<?=$kunik?> cls-18" x="55.38" y="50.26">e</tspan>
        <tspan x="59.52" y="50.26">t </tspan>
        <tspan x="-6.29" y="60.32">d’emplois, </tspan>
        <tspan class="<?=$kunik?> cls-18" x="28.69" y="60.32">et</tspan>
        <tspan x="35.37" y="60.32">c)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-9" cx="171.06" cy="99.77" r="42.52" /><text class="<?=$kunik?> cls-1" transform="translate(153.06 86.3)">
        <tspan class="<?=$kunik?> cls-32" x="0" y="0">A</tspan>
        <tspan class="<?=$kunik?> cls-34" x="4.88" y="0">c</tspan>
        <tspan class="<?=$kunik?> cls-18" x="8.93" y="0">t</tspan>
        <tspan x="11.46" y="0">e</tspan>
        <tspan class="<?=$kunik?> cls-31" x="15.75" y="0">u</tspan>
        <tspan class="<?=$kunik?> cls-22" x="19.92" y="0">r</tspan>
        <tspan x="22.71" y="0">s de </tspan>
        <tspan x="-17.49" y="10.05">l’ensei</tspan>
        <tspan class="<?=$kunik?> cls-31" x="3.22" y="10.05">g</tspan>
        <tspan x="7.72" y="10.05">neme</tspan>
        <tspan class="<?=$kunik?> cls-30" x="26.95" y="10.05">n</tspan>
        <tspan x="30.89" y="10.05">t (MFR, </tspan>
        <tspan x="-12.59" y="20.11">é</tspan>
        <tspan class="<?=$kunik?> cls-21" x="-8.3" y="20.11">c</tspan>
        <tspan x="-4.22" y="20.11">oles / </tspan>
        <tspan class="<?=$kunik?> cls-29" x="15.88" y="20.11">c</tspan>
        <tspan x="19.95" y="20.11">o</tspan>
        <tspan class="<?=$kunik?> cls-36" x="24.25" y="20.11">l</tspan>
        <tspan x="25.83" y="20.11">lèges / </tspan>
        <tspan x="-2.69" y="30.16">l</tspan>
        <tspan class="<?=$kunik?> cls-20" x="-1.14" y="30.16">y</tspan>
        <tspan class="<?=$kunik?> cls-29" x="2.63" y="30.16">c</tspan>
        <tspan x="6.71" y="30.16">ées / </tspan>
        <tspan class="<?=$kunik?> cls-41" x="25.26" y="30.16">et</tspan>
        <tspan x="31.93" y="30.16">c)</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="58.75" cy="15.11" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(47.66 19.13)">
        <tspan x="0" y="0" id="ActeursdudeveloppementeconomiquechambresconsulairesCRESSpepinieresincubateurscooperativesdactivitesetdemploisetc<?=$kunik?>">1%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="41.81" cy="143.72" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(30.73 147.74)">
        <tspan x="0" y="0" id="Organismesdeformations<?=$kunik?>">2%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="171.06" cy="56.16" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(159.97 60.18)">
        <tspan x="0" y="0" id="ActeursdelenseignementMFRecolescollegeslyceesetc<?=$kunik?>">3%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="133.98" cy="154.92" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(122.89 158.94)">
        <tspan x="0" y="0" id="Organismesducommunautaireetdessolidaritesstructurescaritativessystemedechangelocal<?=$kunik?>">4%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="211.59" cy="241.75" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(200.5 245.77)">
        <tspan x="0" y="0" id="Structuresmedicosocialeetsanitaire<?=$kunik?>">5%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="75.19" cy="245.22" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(64.1 249.24)">
        <tspan x="0" y="0" id="Structuresdelinsertiondanslemploimissionslocalespoleemploiassociationetc<?=$kunik?>">6%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="275.04" cy="23.7" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(263.95 27.72)">
        <tspan x="0" y="0" id="UniversitesecolesdingenieursoudecommerceEPSToupolesdecompetitivite<?=$kunik?>">7%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="371.99" cy="59.45" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(360.9 63.47)">
        <tspan x="0" y="0" id="Acteursdelorientation<?=$kunik?>">8%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="453.32" cy="47.29" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(442.24 51.31)">
        <tspan x="0" y="0" id="Acteursdelatransitionecologique<?=$kunik?>">9%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="541.6" cy="44.58" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(530.52 48.6)">
        <tspan x="0" y="0" id="Agricultureetalimentationchambresdagriculturesetc<?=$kunik?>">10%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="518.22" cy="153.49" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(507.13 157.51)">
        <tspan x="0" y="0" id="StructuresdelEconomieSocialeetSolidairelocales<?=$kunik?>">11%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="436.72" cy="135.48" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(425.63 139.5)">
        <tspan x="0" y="0" id="Structuresdelartetdelaculture<?=$kunik?>">12%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="421.01" cy="243.8" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(409.92 247.82)">
        <tspan x="0" y="0" id="Autrestierslieux<?=$kunik?>">13%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="495.75" cy="261.13" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(484.66 265.15)">
        <tspan x="0" y="0" id="Aucun<?=$kunik?>">14%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="343.43" cy="144.83" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(332.34 148.85)">
        <tspan x="0" y="0" id="StructuresjeunesseetdanimationlocaleMissionsLocalesPointsInfosJeunesseetc<?=$kunik?>">15%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="233.01" cy="134.88" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(221.92 138.9)">
        <tspan x="0" y="0" id="UnpoleterritorialdecooperationeconomiquePTCE<?=$kunik?>">16%</tspan>
    </text>
    <circle class="<?=$kunik?> cls-24" cx="315.65" cy="262.86" r="15.11" /><text class="<?=$kunik?> cls-2" transform="translate(304.56 266.88)">
        <tspan x="0" y="0" id="Structuresdaccompagnementetdefinancementdelentrepreneuriat<?=$kunik?>">17%</tspan>
    </text>
    <line class="<?=$kunik?> cls-8" x1="109.78" y1="84.98" x2="129.96" y2="91.71" />
    <line class="<?=$kunik?> cls-8" x1="214.14" y1="96.82" x2="231.09" y2="90.9" />
    <line class="<?=$kunik?> cls-8" x1="75.69" y1="186.26" x2="88.93" y2="195.53" />
    <line class="<?=$kunik?> cls-8" x1="177.23" y1="197.74" x2="195.33" y2="191.12" />
    <line class="<?=$kunik?> cls-8" x1="324.68" y1="78.98" x2="339.72" y2="88.94" />
    <line class="<?=$kunik?> cls-8" x1="404.09" y1="96.41" x2="421.36" y2="87.81" />
    <line class="<?=$kunik?> cls-8" x1="487.38" y1="84.03" x2="496.85" y2="84.72" />
    <line class="<?=$kunik?> cls-8" x1="479.4" y1="186.18" x2="472.26" y2="187.2" />
    <line class="<?=$kunik?> cls-8" x1="390.86" y1="187.59" x2="403.88" y2="192.18" />
    <line class="<?=$kunik?> cls-8" x1="274.92" y1="188.53" x2="296.55" y2="184.37" />
    <line class="<?=$kunik?> cls-8" x1="119.71" y1="290.73" x2="177.21" y2="280.23" />
    <line class="<?=$kunik?> cls-8" x1="245.25" y1="274.88" x2="272.78" y2="294.9" />
    <line class="<?=$kunik?> cls-8" x1="357.44" y1="296.15" x2="393.31" y2="278.63" />
    <line class="<?=$kunik?> cls-8" x1="448.6" y1="274.72" x2="472.14" y2="283.22" />
    <line class="<?=$kunik?> cls-8" x1="542.03" y1="137.27" x2="540.09" y2="162.07" />
    <line class="<?=$kunik?> cls-8" x1="518.22" y1="235.88" x2="499.45" y2="248.67" />
    <line class="<?=$kunik?> cls-8" x1="47.58" y1="213.84" x2="75.19" y2="230.11" />
    <line class="<?=$kunik?> cls-8" x1="41.81" y1="128.61" x2="48.06" y2="122.88" />
</svg>
<script>
    function removeDiacritics (str) {

        var defaultDiacriticsRemovalMap = [
            {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
            {'base':'AA','letters':/[\uA732]/g},
            {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
            {'base':'AO','letters':/[\uA734]/g},
            {'base':'AU','letters':/[\uA736]/g},
            {'base':'AV','letters':/[\uA738\uA73A]/g},
            {'base':'AY','letters':/[\uA73C]/g},
            {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
            {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
            {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
            {'base':'DZ','letters':/[\u01F1\u01C4]/g},
            {'base':'Dz','letters':/[\u01F2\u01C5]/g},
            {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
            {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
            {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
            {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
            {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
            {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
            {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
            {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
            {'base':'LJ','letters':/[\u01C7]/g},
            {'base':'Lj','letters':/[\u01C8]/g},
            {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
            {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
            {'base':'NJ','letters':/[\u01CA]/g},
            {'base':'Nj','letters':/[\u01CB]/g},
            {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
            {'base':'OI','letters':/[\u01A2]/g},
            {'base':'OO','letters':/[\uA74E]/g},
            {'base':'OU','letters':/[\u0222]/g},
            {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
            {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
            {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
            {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
            {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
            {'base':'TZ','letters':/[\uA728]/g},
            {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
            {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
            {'base':'VY','letters':/[\uA760]/g},
            {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
            {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
            {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
            {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
            {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
            {'base':'aa','letters':/[\uA733]/g},
            {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
            {'base':'ao','letters':/[\uA735]/g},
            {'base':'au','letters':/[\uA737]/g},
            {'base':'av','letters':/[\uA739\uA73B]/g},
            {'base':'ay','letters':/[\uA73D]/g},
            {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
            {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
            {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
            {'base':'dz','letters':/[\u01F3\u01C6]/g},
            {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
            {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
            {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
            {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
            {'base':'hv','letters':/[\u0195]/g},
            {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
            {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
            {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
            {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
            {'base':'lj','letters':/[\u01C9]/g},
            {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
            {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
            {'base':'nj','letters':/[\u01CC]/g},
            {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
            {'base':'oi','letters':/[\u01A3]/g},
            {'base':'ou','letters':/[\u0223]/g},
            {'base':'oo','letters':/[\uA74F]/g},
            {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
            {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
            {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
            {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
            {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
            {'base':'tz','letters':/[\uA729]/g},
            {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
            {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
            {'base':'vy','letters':/[\uA761]/g},
            {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
            {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
            {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
            {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
        ];

        for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
            str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
        }

        return str;

}
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    if(data.length>0){
        $.each(data, function(index, d){
            $("#"+removeDiacritics(d["label"]).replace(/[^a-zA-Z]/g, "")+"<?=$kunik?>").text(d["value"]+"%")
        })
    }
</script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "color01": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    },
                    "showLegend" : {
                        "label" : "Afficher la légende",
                        "inputType" : "checkboxSimple",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true,
                        "values" :  sectionDyf.<?php echo $kunik?>ParamsData.showLegend
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
