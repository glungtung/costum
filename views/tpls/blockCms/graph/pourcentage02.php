<?php 
    $keyTpl     = "pourcentage02";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "emptyColor" => "#FF286B",
        "completeColor" => "#9B6FAC",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>

<svg id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 934.41 359.56">
    <defs>
        <style>
            .cls2-1,
            .cls2-2,
            .cls2-3,
            .cls2-4,
            .cls2-5,
            .cls2-6,
            .cls2-7,
            .cls2-8,
            .cls2-9,
            .cls2-10,
            .cls2-11,
            .cls2-12,
            .cls2-13{
                fill:#555;
            }

            .cls2-14{
                fill:#a5c145;
            }

            .cls2-15{
                fill:#ccc;
            }

            .cls2-2{
                font-family:Poppins-Bold, Poppins;font-size:12px;font-weight:700;
                fill: #fff;
            }

            .cls2-3{
                filter:url(#drop-shadow-5);
            }

            .cls2-4{
                filter:url(#drop-shadow-7);
            }

            .cls2-5{
                filter:url(#drop-shadow-4);
            }

            .cls2-6{
                filter:url(#drop-shadow-9);
            }

            .cls2-7{
                filter:url(#drop-shadow-1);
            }

            .cls2-8{
                filter:url(#drop-shadow-6);
            }

            .cls2-9{
                filter:url(#drop-shadow-8);
            }

            .cls2-10{
                filter:url(#drop-shadow-2);
            }

            .cls2-11{
                filter:url(#drop-shadow-3);
            }

            .cls2-16{
                fill:#fff;/*#4b5259;*/
                font-family:Poppins-SemiBold, Poppins;
                font-size:22px;
                font-weight:600;
            }

            .cls2-12{
                filter:url(#drop-shadow-11);
            }

            .cls2-13{
                filter:url(#drop-shadow-10);
            }
        </style>
        <filter id="drop-shadow-1" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-2" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-2" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-2" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-3" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-3" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-3" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-4" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-4" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-4" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-5" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-5" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-5" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-6" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-6" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-6" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-7" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-7" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-7" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-8" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-8" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-8" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-9" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-9" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-9" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-10" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-10" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-10" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
        <filter id="drop-shadow-11" filterUnits="userSpaceOnUse">
            <feOffset dx="3" dy="3"/>
            <feGaussianBlur result="blur-11" stdDeviation="4"/>
            <feFlood flood-color="#000" flood-opacity=".15"/>
            <feComposite in2="blur-11" operator="in"/>
            <feComposite in="SourceGraphic"/></filter>
    </defs>
    <path class="cls2-14" d="M60.36,84.75h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35H35.01V110.1c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-7" d="M73.37,106.34h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1H58.27v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-15" d="M119.89,154.66h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35H94.54v-81.48c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-10" d="M131.23,176.25h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-14" d="M204.9,84.75h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48V110.1c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-11" d="M216.25,106.34h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-14" d="M350.08,84.75h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48V110.1c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-5" d="M361.42,106.34h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-15" d="M264.29,154.66h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48v-81.48c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-3" d="M275.63,176.25h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <text class="cls2-16" transform="translate(73.02 146.67)"><tspan id="Atelierdefabricationartistique" x="0" y="0">12%</tspan></text>
    <text class="cls2-16" transform="translate(213.2 146.67)"><tspan id="Manufacturesdeproximité" x="0" y="0">23%</tspan></text>
    <text class="cls2-16" transform="translate(130.03 218.86)"><tspan id="FabriqueNumériquedeTerritoire" x="0" y="0">16%</tspan></text>
    <text class="cls2-16" transform="translate(273.97 218.86)"><tspan id="FranceServices" x="0" y="0">38%</tspan></text>
    <text class="cls2-16" transform="translate(358.56 146.67)"><tspan id="Fabriquedeterritoire" x="0" y="0">34%</tspan></text><polygon class="cls2-1" points="110.25 77.39 92.96 54.15 75.67 77.39 87.49 77.39 87.49 116.41 98 116.41 98 77.39 110.25 77.39"/><polygon class="cls2-1" points="126.1 269.48 143.39 292.72 160.68 269.48 148.86 269.48 148.86 230.46 138.35 230.46 138.35 269.48 126.1 269.48"/><polygon class="cls2-1" points="255.39 77.39 238.1 54.15 220.81 77.39 232.63 77.39 232.63 116.41 243.14 116.41 243.14 77.39 255.39 77.39"/><polygon class="cls2-1" points="402.21 77.39 384.92 54.15 367.63 77.39 379.45 77.39 379.45 116.41 389.96 116.41 389.96 77.39 402.21 77.39"/><polygon class="cls2-1" points="271.24 269.48 288.53 292.72 305.82 269.48 294 269.48 294 230.46 283.49 230.46 283.49 269.48 271.24 269.48"/>
    <text class="cls2-2" transform="translate(64 26.93)"><tspan x="0" y="0">Atelier de </tspan><tspan x="-35.44" y="14.4">fabrication artistique</tspan></text>
    <text class="cls2-2" transform="translate(193.13 26.93)"><tspan x="0" y="0">Manufactures </tspan><tspan x="5.84" y="14.4">de proximité</tspan></text>
    <text class="cls2-2" transform="translate(87.26 317.26)"><tspan x="0" y="0">Fabrique Numérique </tspan><tspan x="27.24" y="14.4">de Territoire</tspan></text>
    <text class="cls2-2" transform="translate(240.72 317.26)"><tspan x="0" y="0">France Services</tspan></text>
    <path class="cls2-15" d="M427.7,154.66h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48v-81.48c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-8" d="M439.04,176.25h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/><polygon class="cls2-1" points="434.65 269.48 451.94 292.72 469.23 269.48 457.41 269.48 457.41 230.46 446.9 230.46 446.9 269.48 434.65 269.48"/>
    <text class="cls2-2" transform="translate(392.37 317.26)"><tspan x="0" y="0">Labellisation locale </tspan><tspan x="27.52" y="14.4">/ régionale</tspan></text>
    <text class="cls2-2" transform="translate(349.35 26.93)"><tspan x="0" y="0">Fabrique </tspan><tspan x="-7.69" y="14.4">de territoire</tspan></text>
    <text class="cls2-2" transform="translate(521.63 26.93)"><tspan x="0" y="0">DEFFINOV</tspan></text>
    <text class="cls2-2" transform="translate(640.73 26.93)"><tspan x="0" y="0">Pôle territorial de </tspan><tspan x="-24.16" y="14.4">coopération économique</tspan></text>
    <text class="cls2-2" transform="translate(550.92 317.26)"><tspan x="0" y="0">Campus Connecté</tspan></text>
    <text class="cls2-2" transform="translate(711.91 317.26)"><tspan x="0" y="0">Contrat de </tspan><tspan x="-31.75" y="14.4">transition écologique</tspan></text>
    <text class="cls2-2" transform="translate(780.7 26.93)"><tspan x="0" y="0">Projet Alimentaire </tspan><tspan x="26.45" y="14.4">Territorial</tspan></text>
    <path class="cls2-14" d="M521.61,84.75h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48V110.1c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-4" d="M534.62,106.34h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-15" d="M581.14,154.66h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48v-81.48c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-9" d="M592.48,176.25h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-14" d="M666.15,84.75h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48V110.1c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-6" d="M677.49,106.34h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-14" d="M811.33,84.75h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48V110.1c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-13" d="M822.67,106.34h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/>
    <path class="cls2-15" d="M725.53,154.66h81.48v81.48c0,13.99-11.36,25.35-25.35,25.35h-81.48v-81.48c0-13.99,11.36-25.35,25.35-25.35Z"/>
    <path class="cls2-12" d="M736.88,176.25h48.55v48.55c0,8.34-6.77,15.1-15.1,15.1h-48.55v-48.55c0-8.34,6.77-15.1,15.1-15.1Z"/><polygon class="cls2-1" points="571.5 77.39 554.21 54.15 536.92 77.39 548.74 77.39 548.74 116.41 559.25 116.41 559.25 77.39 571.5 77.39"/><polygon class="cls2-1" points="587.35 269.48 604.64 292.72 621.93 269.48 610.1 269.48 610.1 230.46 599.6 230.46 599.6 269.48 587.35 269.48"/><polygon class="cls2-1" points="716.63 77.39 699.34 54.15 682.05 77.39 693.88 77.39 693.88 116.41 704.39 116.41 704.39 77.39 716.63 77.39"/><polygon class="cls2-1" points="863.45 77.39 846.16 54.15 828.87 77.39 840.7 77.39 840.7 116.41 851.21 116.41 851.21 77.39 863.45 77.39"/><polygon class="cls2-1" points="732.49 269.48 749.78 292.72 767.07 269.48 755.24 269.48 755.24 230.46 744.74 230.46 744.74 269.48 732.49 269.48"/>
    <text class="cls2-16" transform="translate(532.73 146.67)"><tspan id="DEFFINOV" x="0" y="0">26%</tspan></text>
    <text class="cls2-16" transform="translate(674.94 146.67)"><tspan id="Pôleterritorialdecoopérationéconomique" x="0" y="0">67%</tspan></text>
    <text class="cls2-16" transform="translate(819.94 146.67)"><tspan id="ProjetAlimentaireTerritorialPAT" x="0" y="0">69%</tspan></text>
    <text class="cls2-16" transform="translate(435.11 218.86)"><tspan id="Labellisationlocalerégionale" x="0" y="0">78%</tspan></text>
    <text class="cls2-16" transform="translate(589.26 218.86)"><tspan id="CampusConnecté" x="0" y="0">89%</tspan></text>
    <text class="cls2-16" transform="translate(732.49 218.86)"><tspan id="Contratdetransitionécologique" x="0" y="0">98%</tspan></text>
</svg>

<script>
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    
    if(data.length>0){
        $.each(data, function(index, d){
            try{
                $("#"+d["label"].replace(/[()/]|\s/g, "")).text(d["value"]+" %")
            }catch(e){

            }
        })
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "chartType": {
                        "inputType" : "select",
                        "class":"select2 form-control",
                        "label" : "Type du chart",
                        "options":{"bar":"Bar", "doughnut":"Circulaire", "line":"Line", "pie":"Pie"}
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
