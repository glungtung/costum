<?php

    $graphAssets = [
        '/plugins/d3/d3.v6.min.js'
    ];

    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
    );

    $keyTpl     = "progressCircle";
    $paramsData = [
        "titleTop" => "",
        "titleBottom" => "",
        "textValueRight" => "",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "percentColor" => "white",
        "emptyColor" => "#DDD",
        "completeColor" => "#F0506A",
        "figure" => "",
        "textRight" => false
    ];

    $initFigure = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"figure"
        ), "image" );

    $figureFile = [];

    foreach ($initFigure as $key => $value) {
        $figureFile[] = $value["imagePath"];
    }

    /*var_dump($figureFile); */

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }

 ?>

<div class="chartviz">
    <div class="sp-text text-center" data-field="titleTop" data-id="<?= $blockKey ?>"><?= $paramsData['titleTop'] ?></div>
    <?php if($paramsData["textRight"]!=false){ ?>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
    <?php } ?>
            <div id="arc<?= $kunik ?>" class="text-center"></div>
    <?php if($paramsData["textRight"]!=false){ ?>
        </div>
        <div class="tvr<?= $kunik ?>" class="col-md-7">
            <div style="font-size:15pt" data-field="textValueRight" data-id="<?= $blockKey ?>"><?php echo implode(',',$paramsData['answerValue']) //$paramsData['textValueRight'] ?></div>
        </div>
    </div>
    <?php } ?>
    <div class="sp-text text-center" data-field="titleBottom" data-id="<?= $blockKey ?>"><?= $paramsData['titleBottom'] ?></div>
</div>
<table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz margin-top-10">
    <tr><td>Valeur :</td><td class="arcValue<?= $kunik; ?>"></td></tr>
</table>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var countValue = 0;
        var allCount = 1;

        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"].value){
            countValue = costum["dashboardData"]["<?= $blockKey ?>"]["value"];
        }

        if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"].allCount){
            allCount = costum["dashboardData"]["<?= $blockKey ?>"]["allCount"];
        }

        $("#tableData<?=$kunik?>").hide();;
        
        var progressCircle<?= $kunik ?> = {
            width: 120,
            height: 120,
            pi : 2 * Math.PI,
            data: {id:"<?php echo $kunik ?>", percent: Math.round((countValue*100)/allCount)||0, percentColor:"<?php echo  $paramsData['percentColor'] ?>", emptyColor:"<?= $paramsData['emptyColor'] ?>", completeColor:"<?= $paramsData['completeColor'] ?>"},
            arc : null,
            parent:null,
            init: function(procir){
                procir.arc = d3.arc()
                    .innerRadius(40)
                    .outerRadius(60) // arc width
                    .startAngle(0);

                procir.parent = d3.select("#arc<?= $kunik ?>").append("svg")
                    .attr("width", procir.width)
                    .attr("height", procir.height);

                procir.draw(procir, procir.data);
            },
            draw: function(procir, data){
                var d3Ring = procir.parent  
                .append("g")
                .attr("transform", "translate("+ procir.width/2 +"," +60+")")
                .attr("id",data.id);
            
                // Background
                d3Ring
                .append("path")
                .datum({endAngle: procir.pi})
                .style("fill", data.emptyColor)
                .attr("d", procir.arc); 
                
                // Foreground
                var foreground = d3Ring
                .append("path")
                .datum({endAngle: 0})
                    .style("stroke", "none")
                    .style("stroke-width", "0px")
                    .style("opacity", 1)
                .attr("d", procir.arc)
                .attr("fill", data.completeColor);
                
                // Text
                d3Ring
                .append("text")
                .attr("x", -20)
                .attr("y", 8) 
                .style("font-size", 25)
                .style("font-family", "Georgia, Arial, sans-serif")
                .style("font-weight", "bolder")
                .style("fill", data.percentColor)
                .text(data.percent + "%");
                $(".arcValue<?= $kunik; ?>").text(data.percent + "%");

                var angle = procir.helpers.convertPercentToAngle(procir);
                
                // Animation
                foreground
                .transition()
                  .duration(1500)
                        .delay(500)
                  .call(procir.arcTween, procir, angle);
            },
            arcTween: function(transition, procir, newAngle){
                transition.attrTween("d", function(d) {  
                    var interpolate = d3.interpolate(d.endAngle, newAngle);
                    return function(t) {
                        d.endAngle = interpolate(t);
                        return procir.arc(d);
                    };
                });
            },
            helpers:{
                convertPercentToAngle: function(procir){
                    return ( procir.data.percent / 100 ) * procir.pi
                }
            }
        }

        progressCircle<?= $kunik ?>.init(progressCircle<?= $kunik ?>);
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "emptyColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc vide"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de l'arc rempli"
                    },
                    "figure" :{                    
                        "inputType" : "uploader",
                        "docType": "image",
                        "contentKey":"slider",
                        "endPoint": "/subKey/figure",
                        "domElement" : "figure",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        "label": "Figure :",
                        "itemLimit" : 1,
                        "showUploadBtn": false,
                        initList : <?php echo json_encode($initFigure); ?> 
                    },
                    "textRight": {
                        "inputType" : "checkboxSimple",
                        "label" : "Texte à droit du cercle",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : false,
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.textRight
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params, function(){
                                toastr.success("La paramètre est bien enregistrée");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            });
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(typeof input !="undefined" && (input.includes("multiRadio") || input.includes("multiCheckboxPlus"))){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
    
</script>