<?php 
    $keyTpl     = "textWithValueAndIcon";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "text" => 'Personnes ont assisté <br><span class="accentuated">à un événement artistico-culturel</span>',
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "color01" => "#9B6FAC",
        "color02" => "#ddd",
        "profilImageUrl" => "",
        "withStaticTextBottom" => true,
        "position" => false,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
    $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
    if(!($paramsData['profilImageUrl'] != "" && Document::urlExists($baseUrl.$paramsData["profilImageUrl"]))){
        unset($paramsData['profilImageUrl']);   
    }
?>

<?php ?>

<style> 
    .custom-card {
        background-color: white;
        border-radius: 10px;
        text-align: center;
        width: 320px;
        height: 230px;
        position: relative;
        padding: 30px;
        margin: 50px auto
        
    }
    .custom-card.data-top{
        border: 1px dotted #b3b3b3;
        background: none;
        padding-top: 60px;
        height: 290px;
        display: flex;
        align-items: center;
    }
    .custom-card .number{
        font-size: 40px;
    }
    .data-top .number{
        color: white;
        position: absolute;
        top: -25px;
        margin: 0 auto;
        left: 0;
        width: 100%;
    }
    .data-top .number span{
        padding: 10px 15px;
        background: #a5c145;
        border-radius: 10px;
    }
    .data-top .number span::after{
        content:""; 
        border-left: 15px solid transparent;
        border-right: 15px solid transparent;
        border-top: 10px solid #a4c147;
        position: absolute;
        top: 55px;
        left: 145px;
    }
    .custom-card .title{
        font-size: 24px;
    }
    .custom-card .title .accentuated{
        color: #a9c34b;
    }
    .custom-card .img-absolute{
        position: absolute;
        border-radius: 100%;
        background: white;
        padding: 30px;
        top: -20%;
        right: -20%;
    }
    .custom-card .img-absolute .img-icon{
        width: 50px;
        height: 50px;
        object-fit: contain;
    }
</style>

<div class="<?= $kunik ?>">
    <div class="row">
        <div class="col-md-4">
            <div class="custom-card <?= $paramsData['position'] ? "data-top" : "" ?> ">
                <div>
                    <h4 class="number">
                        <span>2 537</span>
                    </h4>
                    <p class="title"><?= $paramsData["text"] ?></p>
                    <?php if(isset($paramsData["profilImageUrl"])){ ?>
                        <div class="img-absolute">
                            <img src="<?= $paramsData["profilImageUrl"] ?>" alt="" class="img-icon">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="custom-card">
                <h4 class="number">249</h4>
                <p class="title">Personnes ont <br><span class="accentuated">travaillé ou developpé des projets</span></p>
                <div class="img-absolute">
                    <img src="<?= Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/dev projet.png" alt="" class="img-icon">
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="custom-card">
                <h4 class="number">638</h4>
                <p class="title">Bénévolats ont <br><span class="accentuated">été immobilisés</span></p>
                <div class="img-absolute">
                    <img src="<?= Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/benevoles mobilisés.png" alt="" class="img-icon">
                </div>
            </div>
        </div> -->
    </div>
</div>

<script>
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]['$numberDecimal']){
        $(".<?= $kunik ?> .number>span").html((new Intl.NumberFormat('fr')).format(costum["dashboardData"]["<?= $blockKey ?>"]["$numberDecimal"]));
    }else if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined"){
        $(".<?= $kunik ?> .number>span").html(costum["dashboardData"]["<?= $blockKey ?>"]);
    }
</script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "text": {
                        "inputType" : "textarea",
                        "label" : "Texte à afficher"
                    },
                    "position": {
                        "inputType" : "checkboxSimple",
                        "label" : "Afficher le donnée en haut",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.position
                    },
                    "icons": {
                        "inputType" : "uploader",
                        "label" : "Icone",
                        "label" : "image",
                        "docType": "image",
                        "itemLimit" : 1,
							
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "color01": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                    if(sectionDyf.<?= $kunik?>ParamsData.position){
                        $("#ajaxFormModal .iconsuploader").hide();
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
			                    //   urlCtrl.loadByHash(location.hash);
                            });
                        });
                    }
                }
            }
        }
        costum.checkboxSimpleEvent = {
            true : function(id){
                if(id=="position"){
                    $("#ajaxFormModal .iconsuploader").hide();
                }
            },
            false : function(id){
                if(id=="position"){
                    $("#ajaxFormModal .iconsuploader").show();
                }
            }
        }
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text" || input["type"]=="number"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="number"){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
