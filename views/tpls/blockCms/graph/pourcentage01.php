<?php 
    $keyTpl     = "pourcentage01";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "emptyColor" => "#FF286B",
        "completeColor" => "#9B6FAC",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 787.83 524.81">
    <defs>
        <style>.cls1-1{
            font-size:16px;
        }
        .cls1-1,.cls1-2{
            fill:#3d4d57;
            font-family:Poppins-SemiBold, Poppins;
            font-weight:600;
        }
        .cls1-3{
            fill:url(#radial-gradient-2);
        }
        .cls1-3,.cls1-4,.cls1-5,.cls1-6,.cls1-7,.cls1-8,.cls1-9,.cls1-10,.cls1-11,.cls1-12,.cls1-13,.cls1-14{
            mix-blend-mode:multiply;
        }
        .cls1-4{
            fill:url(#radial-gradient-5);
        }
        .cls1-5{
            fill:url(#radial-gradient-6);
        }
        .cls1-6{
            fill:url(#radial-gradient-7);
        }
        .cls1-7{
            fill:url(#radial-gradient-3);
        }
        .cls1-8{
            fill:url(#radial-gradient-4);
        }
        .cls1-9{
            fill:url(#radial-gradient-9);
        }
        .cls1-10{
            fill:url(#radial-gradient-8);
        }
        .cls1-15{
            fill:#fff;
        }
        .cls1-16{
            fill:#e83880;
        }
        .cls1-17{
            fill:#a5c145;
        }
        .cls1-18{
            fill:#ccc;
        }
        .cls1-11{
            fill:url(#radial-gradient);
        }
        .cls1-12{
            fill:url(#radial-gradient-12);
        }
        .cls1-13{
            fill:url(#radial-gradient-11);
        }
        .cls1-14{
            fill:url(#radial-gradient-10);
        }
        .cls1-2{
            font-size:12px;
        }
        .cls1-19{
            isolation:isolate;
        }
        .cls1-20{
            fill:#4b5259;
            font-family:OpenSans-Bold, 'Open Sans';
            font-size:23.36px;
            font-weight:700;
        }
    </style>
    <radialGradient id="radial-gradient" cx="60.18" cy="483.18" fx="60.18" fy="483.18" r="19.63" gradientTransform="translate(117.99 -.29) rotate(13.92)" gradientUnits="userSpaceOnUse">
        <stop offset="0" stop-color="#000"/>
        <stop offset="1" stop-color="#fff"/>
    </radialGradient>
    <radialGradient id="radial-gradient-2" cy="400.67" fy="400.67" r="19.63" gradientTransform="translate(98.14 -2.71) rotate(13.92)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-3" cy="307.66" fy="307.66" r="19.63" gradientTransform="translate(75.77 -5.44) rotate(13.92)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-4" cy="56.74" fy="56.74" r="19.63" gradientTransform="translate(53.37 -25.69) rotate(41.97)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-5" cy="139.25" fy="139.25" r="19.63" gradientTransform="translate(108.55 -4.53) rotate(41.97)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-6" cy="225.15" fy="225.15" gradientTransform="translate(55.92 -7.86) rotate(13.92)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-7" cx="513.98" fx="513.98" r="19.63" gradientTransform="translate(131.31 -109.44) rotate(13.92)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-8" cx="513.98" cy="400.67" fx="513.98" fy="400.67" r="19.63" gradientTransform="translate(111.47 -111.87) rotate(13.92)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-9" cx="513.98" cy="307.66" fx="513.98" fy="307.66" r="19.63" gradientTransform="translate(89.1 -114.6) rotate(13.92)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-10" cx="513.98" cy="56.74" fx="513.98" fy="56.74" r="19.63" gradientTransform="translate(169.76 -329.15) rotate(41.97)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-11" cx="513.98" cy="139.25" fx="513.98" fy="139.25" r="19.63" gradientTransform="translate(224.94 -307.98) rotate(41.97)" xlink:href="#radial-gradient"/>
    <radialGradient id="radial-gradient-12" cx="513.98" cy="225.15" fx="513.98" fy="225.15" r="19.63" gradientTransform="translate(69.25 -117.02) rotate(13.92)" xlink:href="#radial-gradient"/>
</defs>
<g class="cls1-19">
    <g id="Calque_1">
        <g>
            <rect class="cls1-17" x="51.49" y="395.79" width="8.95" height="83.48"/>
            <rect class="cls1-17" x="51.49" y="312.91" width="8.95" height="82.88"/>
            <circle class="cls1-15" cx="56.17" cy="480.12" r="34.19" transform="translate(-323.04 180.35) rotate(-45)"/>
            <path class="cls1-17" d="M53.32,454.77H313.48c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35H53.32v-50.69Z"/>
            <circle class="cls1-16" cx="56.17" cy="480.12" r="25.35"/>
            <circle class="cls1-17" cx="56.17" cy="480.12" r="25.35"/>
            <circle class="cls1-11" cx="60.18" cy="483.18" r="19.63" transform="translate(-114.46 28.66) rotate(-13.92)"/>
            <circle class="cls1-15" cx="56.17" cy="480.12" r="19.63" transform="translate(-323.04 180.35) rotate(-45)"/>
            <text class="cls1-20" transform="translate(89.85 489.98)">
                <tspan id="HubOccitanie" x="0" y="0">60%</tspan>
            </text>
            <circle class="cls1-15" cx="56.17" cy="397.61" r="34.19"/>
            <path class="cls1-18" d="M56.17,372.26H233.71c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35H56.17v-50.69Z"/>
            <circle class="cls1-18" cx="56.17" cy="397.61" r="25.35"/>
            <circle class="cls1-3" cx="60.18" cy="400.67" r="19.63" transform="translate(-94.61 26.24) rotate(-13.92)"/>
            <circle class="cls1-15" cx="56.17" cy="397.61" r="19.63" transform="translate(-264.7 156.18) rotate(-45)"/>
            <text class="cls1-20" transform="translate(89.85 408.26)">
                <tspan id="HubAntilles-Guyane" x="0" y="0">50%</tspan>
            </text>
            <g>
        <path class="cls1-18" d="M55.09,383.82c-5.19,.29-9.37,4.62-9.48,9.82-.07,3.25,1.41,6.16,3.75,8.04,.96,.77,1.6,1.84,1.79,3.03h9.02c.19-1.19,.84-2.26,1.79-3.03,2.28-1.84,3.75-4.66,3.75-7.82,0-5.74-4.81-10.36-10.62-10.04Zm.38,3.31c0,.46-.33,.84-.78,.93-2.54,.55-4.54,2.62-5.01,5.17-.08,.45-.45,.78-.91,.78h-.1c-.6,0-1.03-.54-.93-1.13,.58-3.37,3.22-6.11,6.57-6.82,.6-.13,1.16,.32,1.16,.93v.14Z"/>
        <path class="cls1-18" d="M60.93,405.64h-10.6c-.62,0-1.12,.5-1.12,1.12s.5,1.12,1.12,1.12h2.66c.25,1.23,1.34,2.15,2.64,2.15s2.39-.92,2.64-2.15h2.66c.62,0,1.12-.5,1.12-1.12s-.5-1.12-1.12-1.12Z"/>
    </g>
    <g>
        <g>
            <polygon class="cls1-17" points="49.83 479.51 49.83 489.42 45.8 489.42 45.8 480.82 49.83 479.51"/>
            <polygon class="cls1-17" points="56.7 477.29 56.7 489.42 52.67 489.42 52.67 478.59 56.7 477.29"/>
            <polygon class="cls1-17" points="63.9 474.96 63.9 489.42 59.88 489.42 59.88 476.26 63.9 474.96"/>
        </g>
        <polygon class="cls1-17" points="69.07 470.16 63.49 467.93 64.19 470.43 44.04 476.96 44.74 479.11 64.8 472.62 65.45 474.95 69.07 470.16"/>
    </g>
    <rect class="cls1-17" x="51.49" y="220.27" width="8.95" height="83.48"/>
    <rect class="cls1-17" x="51.49" y="137.39" width="8.95" height="82.88"/>
    <rect class="cls1-17" x="51.49" y="32.51" width="8.95" height="104.89"/>
    <circle class="cls1-15" cx="56.17" cy="304.6" r="34.19" transform="translate(-198.93 128.94) rotate(-45)"/>
    <path class="cls1-17" d="M46.17,279.26H326.34c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35H46.17v-50.69Z"/>
    <circle class="cls1-16" cx="56.17" cy="304.6" r="25.35"/>
    <circle class="cls1-17" cx="56.17" cy="304.6" r="25.35"/>
    <circle class="cls1-7" cx="60.18" cy="307.66" r="19.63" transform="translate(-72.24 23.51) rotate(-13.92)"/>
    <circle class="cls1-15" cx="56.17" cy="304.6" r="19.63" transform="translate(-198.93 128.94) rotate(-45)"/>
    <text class="cls1-20" transform="translate(89.85 314.47)">
        <tspan id="Autres" x="0" y="0">60%</tspan>
    </text>
    <circle class="cls1-15" cx="56.17" cy="53.68" r="34.19"/>
    <circle class="cls1-18" cx="56.17" cy="53.68" r="25.35"/>
    <path class="cls1-18" d="M56.95,28.33H240.15c17.14,0,31.03,11.35,31.03,25.35h0c0,14-13.89,25.35-31.03,25.35H56.95V28.33Z"/>
    <circle class="cls1-8" cx="60.18" cy="56.74" r="19.63" transform="translate(-22.51 54.79) rotate(-41.97)"/>
    <circle class="cls1-15" cx="56.17" cy="53.68" r="19.63"/>
    <text class="cls1-20" transform="translate(89.85 62.31)">
    <tspan id="HubduSud" x="0" y="0">45%</tspan></text>
    <g>
        <path class="cls1-18" d="M63.48,62.64c2.6-2.2,4.22-5.51,4.12-9.2-.16-6.04-5.11-11.07-11.15-11.32-6.65-.27-12.13,5.04-12.13,11.63,0,3.56,1.6,6.75,4.13,8.89h.02c2.08,1.78,4.72,2.75,7.45,2.75h.05c1.72,0,3.35-.38,4.82-1.04,0,0,1.86-.81,2.7-1.71Zm-15.77-8.89c0-4.56,3.7-8.26,8.26-8.26s8.26,3.7,8.26,8.26-3.7,8.26-8.26,8.26-8.26-3.7-8.26-8.26Z"/>
        <path class="cls1-18" d="M56.95,54.16v-5.33c0-.46-.37-.83-.83-.83s-.83,.37-.83,.83v4.68l-2.04,2.04c-.33,.33-.33,.85,0,1.18,.33,.33,.85,.33,1.18,0l2.5-2.5h.02v-.02l.02-.02-.02-.02Z"/>
    </g>
    <circle class="cls1-15" cx="56.17" cy="136.19" r="34.19"/>
    <path class="cls1-17" d="M60.17,110.84H248.8c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35H60.17v-50.69Z"/>
    <circle class="cls1-17" cx="56.17" cy="136.19" r="25.35"/>
    <circle class="cls1-4" cx="60.18" cy="139.25" r="19.63" transform="translate(-77.68 75.96) rotate(-41.97)"/>
    <circle class="cls1-15" cx="56.17" cy="136.19" r="19.63"/>
    <text class="cls1-20" transform="translate(89.85 144.04)">
        <tspan id="Hubik" x="0" y="0">75%</tspan>
    </text>
    <path class="cls1-17" d="M55.02,142.87c-3.99,0-7.22-3.23-7.22-7.22s3.23-7.22,7.22-7.22,7.22,3.23,7.22,7.22-3.23,7.22-7.22,7.22Zm13.86-4.59v-5.25h-2.32c-.3-1.33-.82-2.56-1.53-3.68l1.64-1.64-3.71-3.71-1.64,1.64c-1.12-.7-2.35-1.23-3.68-1.53v-2.32h-5.25v2.32c-1.32,.3-2.56,.82-3.68,1.53l-1.64-1.64-3.71,3.71,1.64,1.64c-.7,1.12-1.23,2.35-1.53,3.68h-2.32v5.25h2.32c.3,1.33,.82,2.56,1.53,3.68l-1.64,1.64,3.71,3.71,1.64-1.64c1.12,.7,2.35,1.23,3.68,1.53v2.32h5.25v-2.32c1.33-.3,2.56-.82,3.68-1.53l1.64,1.64,3.71-3.71-1.64-1.64c.7-1.12,1.23-2.35,1.53-3.68h2.32Zm-13.86,.91c-1.95,0-3.53-1.58-3.53-3.53s1.58-3.53,3.53-3.53,3.53,1.58,3.53,3.53-1.58,3.53-3.53,3.53Z"/>
    <circle class="cls1-15" cx="56.17" cy="222.09" r="34.19"/>
    <path class="cls1-18" d="M56.17,196.74h227.54c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35H56.17v-50.69Z"/>
    <circle class="cls1-18" cx="56.17" cy="222.09" r="25.35"/>
    <circle class="cls1-5" cx="60.18" cy="225.15" r="19.63" transform="translate(-52.39 21.09) rotate(-13.92)"/>
    <circle class="cls1-15" cx="56.17" cy="222.09" r="19.63" transform="translate(-140.59 104.77) rotate(-45)"/>
    <text class="cls1-20" transform="translate(89.85 232.74)">
        <tspan id="LesAssembleurs" x="0" y="0">50%</tspan>
    </text>
    <g>
        <path class="cls1-18" d="M55.09,208.3c-5.19,.29-9.37,4.62-9.48,9.82-.07,3.25,1.41,6.16,3.75,8.04,.96,.77,1.6,1.84,1.79,3.03h9.02c.19-1.19,.84-2.26,1.79-3.03,2.28-1.84,3.75-4.66,3.75-7.82,0-5.74-4.81-10.36-10.62-10.04Zm.38,3.31c0,.46-.33,.84-.78,.93-2.54,.55-4.54,2.62-5.01,5.17-.08,.45-.45,.78-.91,.78h-.1c-.6,0-1.03-.54-.93-1.13,.58-3.37,3.22-6.11,6.57-6.82,.6-.13,1.16,.32,1.16,.93v.14Z"/>
        <path class="cls1-18" d="M60.93,230.12h-10.6c-.62,0-1.12,.5-1.12,1.12s.5,1.12,1.12,1.12h2.66c.25,1.23,1.34,2.15,2.64,2.15s2.39-.92,2.64-2.15h2.66c.62,0,1.12-.5,1.12-1.12s-.5-1.12-1.12-1.12Z"/>
    </g>
    <g>
        <g>
            <polygon class="cls1-17" points="49.83 304 49.83 313.9 45.8 313.9 45.8 305.3 49.83 304"/>
            <polygon class="cls1-17" points="56.7 301.77 56.7 313.9 52.67 313.9 52.67 303.08 56.7 301.77"/>
            <polygon class="cls1-17" points="63.9 299.44 63.9 313.9 59.88 313.9 59.88 300.74 63.9 299.44"/>
        </g>
        <polygon class="cls1-17" points="69.07 294.64 63.49 292.41 64.19 294.91 44.04 301.44 44.74 303.6 64.8 297.1 65.45 299.43 69.07 294.64"/>
    </g>
    <rect class="cls1-17" x="505.29" y="395.79" width="8.95" height="83.48"/>
    <rect class="cls1-17" x="505.29" y="312.91" width="8.95" height="82.88"/>
    <circle class="cls1-15" cx="509.97" cy="480.12" r="34.19" transform="translate(-190.13 501.23) rotate(-45)"/>
    <path class="cls1-17" d="M509.97,454.77h210.16c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35h-210.16v-50.69Z"/>
    <circle class="cls1-16" cx="509.97" cy="480.12" r="25.35"/>
    <circle class="cls1-17" cx="509.97" cy="480.12" r="25.35"/>
    <circle class="cls1-6" cx="513.98" cy="483.18" r="19.63" transform="translate(-101.13 137.82) rotate(-13.92)"/>
    <circle class="cls1-15" cx="509.97" cy="480.12" r="19.63" transform="translate(-190.13 501.23) rotate(-45)"/>
    <text class="cls1-20" transform="translate(543.65 489.98)">
        <tspan id="MedNumBourgogne-Franche-Comté" x="0" y="0">60%</tspan>
    </text>


    <circle class="cls1-15" cx="509.97" cy="397.61" r="34.19"/>
    <path class="cls1-18" d="M509.97,372.26h247.54c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35h-247.54v-50.69Z"/>
    <circle class="cls1-18" cx="509.97" cy="397.61" r="25.35"/>
    <circle class="cls1-10" cx="513.98" cy="400.67" r="19.63" transform="translate(-81.29 135.39) rotate(-13.92)"/>
    <circle class="cls1-15" cx="509.97" cy="397.61" r="19.63" transform="translate(-131.78 477.06) rotate(-45)"/>
    <text class="cls1-20" transform="translate(543.65 408.26)">
        <tspan id="HubPiNG" x="0" y="0">50%</tspan>
    </text>
    <g>
        <path class="cls1-18" d="M508.89,383.82c-5.19,.29-9.37,4.62-9.48,9.82-.07,3.25,1.41,6.16,3.75,8.04,.96,.77,1.6,1.84,1.79,3.03h9.02c.19-1.19,.84-2.26,1.79-3.03,2.28-1.84,3.75-4.66,3.75-7.82,0-5.74-4.81-10.36-10.62-10.04Zm.38,3.31c0,.46-.33,.84-.78,.93-2.54,.55-4.54,2.62-5.01,5.17-.08,.45-.45,.78-.91,.78h-.1c-.6,0-1.03-.54-.93-1.13,.58-3.37,3.22-6.11,6.57-6.82,.6-.13,1.16,.32,1.16,.93v.14Z"/>
        <path class="cls1-18" d="M514.72,405.64h-10.6c-.62,0-1.12,.5-1.12,1.12s.5,1.12,1.12,1.12h2.66c.25,1.23,1.34,2.15,2.64,2.15s2.39-.92,2.64-2.15h2.66c.62,0,1.12-.5,1.12-1.12s-.5-1.12-1.12-1.12Z"/>
    </g>
    <g>
        <g>
            <polygon class="cls1-17" points="503.62 479.51 503.62 489.42 499.6 489.42 499.6 480.82 503.62 479.51"/>
            <polygon class="cls1-17" points="510.49 477.29 510.49 489.42 506.47 489.42 506.47 478.59 510.49 477.29"/>
            <polygon class="cls1-17" points="517.69 474.96 517.69 489.42 513.67 489.42 513.67 476.26 517.69 474.96"/>
        </g>
        <polygon class="cls1-17" points="522.86 470.16 517.29 467.93 517.98 470.43 497.84 476.96 498.54 479.11 518.59 472.62 519.24 474.95 522.86 470.16"/>
    </g>
    <rect class="cls1-17" x="505.29" y="220.27" width="8.95" height="83.48"/>
    <rect class="cls1-17" x="505.29" y="137.39" width="8.95" height="82.88"/>
    <rect class="cls1-17" x="505.29" y="32.51" width="8.95" height="104.89"/>
    <circle class="cls1-15" cx="509.97" cy="304.6" r="34.19" transform="translate(-66.02 449.82) rotate(-45)"/>
    <path class="cls1-17" d="M509.97,279.26h240.16c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35h-240.16v-50.69Z"/>
    <circle class="cls1-16" cx="509.97" cy="304.6" r="25.35"/>
    <circle class="cls1-17" cx="509.97" cy="304.6" r="25.35"/>
    <circle class="cls1-9" cx="513.98" cy="307.66" r="19.63" transform="translate(-58.91 132.66) rotate(-13.92)"/>
    <circle class="cls1-15" cx="509.97" cy="304.6" r="19.63" transform="translate(-66.02 449.82) rotate(-45)"/>
    <text class="cls1-20" transform="translate(543.65 314.47)">
        <tspan id="HubUltraNumérique" x="0" y="0">60%</tspan>
    </text>

    <circle class="cls1-15" cx="509.97" cy="53.68" r="34.19"/>
    <circle class="cls1-18" cx="509.97" cy="53.68" r="25.35"/>
    <path class="cls1-18" d="M509.97,28.33h189.66c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35h-189.66V28.33Z"/>
    <circle class="cls1-14" cx="513.98" cy="56.74" r="19.63" transform="translate(93.88 358.25) rotate(-41.97)"/>
    <circle class="cls1-15" cx="509.97" cy="53.68" r="19.63"/>
    <text class="cls1-20" transform="translate(543.65 62.31)">
        <tspan id="HubAURA" x="0" y="0">45%</tspan>
    </text>
    <g>
        <path class="cls1-18" d="M517.28,62.64c2.6-2.2,4.22-5.51,4.12-9.2-.16-6.04-5.11-11.07-11.15-11.32-6.65-.27-12.13,5.04-12.13,11.63,0,3.56,1.6,6.75,4.13,8.89h.02c2.08,1.78,4.72,2.75,7.45,2.75h.05c1.72,0,3.35-.38,4.82-1.04,0,0,1.86-.81,2.7-1.71Zm-15.77-8.89c0-4.56,3.7-8.26,8.26-8.26s8.26,3.7,8.26,8.26-3.7,8.26-8.26,8.26-8.26-3.7-8.26-8.26Z"/>
        <path class="cls1-18" d="M510.75,54.16v-5.33c0-.46-.37-.83-.83-.83s-.83,.37-.83,.83v4.68l-2.04,2.04c-.33,.33-.33,.85,0,1.18,.33,.33,.85,.33,1.18,0l2.5-2.5h.02v-.02l.02-.02-.02-.02Z"/>
    </g>
    <circle class="cls1-15" cx="509.97" cy="136.19" r="34.19"/>
    <path class="cls1-17" d="M509.97,110.84h178.63c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35h-178.63v-50.69Z"/>
    <circle class="cls1-17" cx="509.97" cy="136.19" r="25.35"/>
    <circle class="cls1-13" cx="513.98" cy="139.25" r="19.63" transform="translate(38.7 379.41) rotate(-41.97)"/>
    <circle class="cls1-15" cx="509.97" cy="136.19" r="19.63"/>
    <text class="cls1-20" transform="translate(543.65 144.04)">
        <tspan id="Hubert" x="0" y="0">75%</tspan>
    </text>
    <path class="cls1-17" d="M508.81,142.87c-3.99,0-7.22-3.23-7.22-7.22s3.23-7.22,7.22-7.22,7.22,3.23,7.22,7.22-3.23,7.22-7.22,7.22Zm13.86-4.59v-5.25h-2.32c-.3-1.33-.82-2.56-1.53-3.68l1.64-1.64-3.71-3.71-1.64,1.64c-1.12-.7-2.35-1.23-3.68-1.53v-2.32h-5.25v2.32c-1.32,.3-2.56,.82-3.68,1.53l-1.64-1.64-3.71,3.71,1.64,1.64c-.7,1.12-1.23,2.35-1.53,3.68h-2.32v5.25h2.32c.3,1.33,.82,2.56,1.53,3.68l-1.64,1.64,3.71,3.71,1.64-1.64c1.12,.7,2.35,1.23,3.68,1.53v2.32h5.25v-2.32c1.33-.3,2.56-.82,3.68-1.53l1.64,1.64,3.71-3.71-1.64-1.64c.7-1.12,1.23-2.35,1.53-3.68h2.32Zm-13.86,.91c-1.95,0-3.53-1.58-3.53-3.53s1.58-3.53,3.53-3.53,3.53,1.58,3.53,3.53-1.58,3.53-3.53,3.53Z"/>
    <circle class="cls1-15" cx="509.97" cy="222.09" r="34.19"/>
    <path class="cls1-18" d="M509.97,196.74h237.54c14,0,25.35,11.35,25.35,25.35h0c0,14-11.35,25.35-25.35,25.35h-237.54v-50.69Z"/>
    <circle class="cls1-18" cx="509.97" cy="222.09" r="25.35"/>
    <circle class="cls1-12" cx="513.98" cy="225.15" r="19.63" transform="translate(-39.07 130.24) rotate(-13.92)"/>
    <circle class="cls1-15" cx="509.97" cy="222.09" r="19.63" transform="translate(-7.67 425.65) rotate(-45)"/>
    <text class="cls1-20" transform="translate(543.65 232.24)">
        <tspan id="HubIle-de-France" x="0" y="0">50%</tspan>
    </text>
    <g>
        <path class="cls1-18" d="M508.89,208.3c-5.19,.29-9.37,4.62-9.48,9.82-.07,3.25,1.41,6.16,3.75,8.04,.96,.77,1.6,1.84,1.79,3.03h9.02c.19-1.19,.84-2.26,1.79-3.03,2.28-1.84,3.75-4.66,3.75-7.82,0-5.74-4.81-10.36-10.62-10.04Zm.38,3.31c0,.46-.33,.84-.78,.93-2.54,.55-4.54,2.62-5.01,5.17-.08,.45-.45,.78-.91,.78h-.1c-.6,0-1.03-.54-.93-1.13,.58-3.37,3.22-6.11,6.57-6.82,.6-.13,1.16,.32,1.16,.93v.14Z"/>
        <path class="cls1-18" d="M514.72,230.12h-10.6c-.62,0-1.12,.5-1.12,1.12s.5,1.12,1.12,1.12h2.66c.25,1.23,1.34,2.15,2.64,2.15s2.39-.92,2.64-2.15h2.66c.62,0,1.12-.5,1.12-1.12s-.5-1.12-1.12-1.12Z"/>
    </g>
    <g>
        <g>
            <polygon class="cls1-17" points="503.62 304 503.62 313.9 499.6 313.9 499.6 305.3 503.62 304"/>
            <polygon class="cls1-17" points="510.49 301.77 510.49 313.9 506.47 313.9 506.47 303.08 510.49 301.77"/>
            <polygon class="cls1-17" points="517.69 299.44 517.69 313.9 513.67 313.9 513.67 300.74 517.69 299.44"/>
        </g>
        <polygon class="cls1-17" points="522.86 294.64 517.29 292.41 517.98 294.91 497.84 301.44 498.54 303.6 518.59 297.1 519.24 299.43 522.86 294.64"/>
    </g>
    <text class="cls1-1" transform="translate(150.47 59.86)">   
        <tspan x="0" y="0">Hub du Sud</tspan>
    </text>
    <text class="cls1-1" transform="translate(602.59 59.86)">
        <tspan x="0" y="0">Hub AURA</tspan>
    </text>
    <text class="cls1-1" transform="translate(602.59 140.77)">
        <tspan x="0" y="0">Hubert</tspan>
    </text>
    <text class="cls1-1" transform="translate(602.59 228.54)">
        <tspan x="0" y="0">Hub Ile-de-France</tspan>
    </text>
    <text class="cls1-1" transform="translate(602.59 311.83)">
        <tspan x="0" y="0">Hub Occitanie</tspan>
    </text>
    <text class="cls1-1" transform="translate(602.59 404.59)">
        <tspan x="0" y="0">Hub Antilles-Guyane</tspan>
    </text>
    <text class="cls1-1" transform="translate(602.59 486.07)">
        <tspan x="0" y="0">Autres</tspan>
    </text>
    <text class="cls1-1" transform="translate(150.47 142.14)">
        <tspan x="0" y="0">Hubik</tspan>
    </text>
    <text class="cls1-1" transform="translate(150.47 228.04)">
        <tspan x="0" y="0">Les Assembleurs</tspan>
    </text>
    <text class="cls1-2" transform="translate(157.57 300.88)">
        <tspan x="0" y="0">MedNum </tspan>
        <tspan x="0" y="14.4">Bourgogne-Franche-Comté</tspan>
    </text>
    <text class="cls1-1" transform="translate(152.57 405.14)">
        <tspan x="0" y="0">Hub PiNG</tspan>
    </text>
    <text class="cls1-1" transform="translate(149.57 488.52)">
        <tspan x="0" y="0">Hub Ultra Numérique</tspan>
    </text>
</svg>

<script>
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["labelValueArray"];
    }
    
    if(data.length>0){
        $.each(data, function(index, d){
            $("#"+d["label"].replace(/\s/g, "")).text(d["value"]+" %")
        })
    }
</script>


<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
