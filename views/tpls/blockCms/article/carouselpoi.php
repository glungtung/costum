<?php
$keyTpl = "carouselpoi";
$paramsData=[
    "titleBlock" => "AFFICHE",
    /*"colorDecor" => "#0dab76",*/
    "buttonColorlabel"  => "#ffffff",
    "buttonColorBorder" => "#ffffff",
    "buttonColor"       => "transparent",
    "buttonColorRadius" => "25",
    "imgRadius" => "10",
    "imgHeight" => "160",
    "typePoi" => "affiche",
    "cardBgColor" => "white",
    "cardTitleColor" => "#121C4B",
    "cardSubTitleColor" => "#121C4B"
    

];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}


?>

<?php
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>

<!-- ****************get image uploaded************** -->
<?php
$blockKey = (string)$blockCms["_id"];
$initFiles = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"notBackground"
    ), "image"
);
$arrayImg = [];
foreach ($initFiles as $key => $value) {
    $arrayImg[]= $value["imagePath"];
}
?>
<!-- ****************end get image uploaded************** -->

<style type="text/css">

    /*Card*/

    .case-study-gallery {
        margin-top: 50px;
        width: 90%;
        margin: 50px auto;
        max-width: 1100px;
    }

    .case-study {
        position: relative;
        display: block;
        width: 100%;
        height: 280px;
        margin: 0 auto 2rem;
        background-size: cover;
        border-radius: <?php echo $paramsData["imgRadius"]; ?>px;
        box-shadow: 0px 15px 20px rgba(0, 0, 0, 0.3);
        overflow: hidden;
        transition: all .4s ease;
    }
    @media screen and (min-width: 768px) {
        .case-study {
            height: 340px;
        }
    }
    @media screen and (min-width: 768px) {
        .case-study {
            display: inline-block;
        }
    }

    .case-study__img {
        display: block;
        margin: 0 auto;
    }

    .case-study__overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        z-index: 0;
        text-align: center;
        overflow-y: auto;
        overflow-x: hidden;
        background-color: <?php echo $paramsData["cardBgColor"]; ?>;
    }
    .case-study__overlay:after {
        content: '';
        width: 100%;
        border-radius: 10px;
        height: 100%;
        /*background-color: #49CBE3;
        opacity: .5;*/
        position: absolute;
        top: 0;
        z-index: -10;
        left: 0;
        transition: all .3s ease;
    }
   
    .case-study__title {
        font-family: 'NotoSans-Regular' !important;
        position: relative;
        margin-bottom: 8px;
        margin-top: 10px;
        font-size: 14px;
        font-weight: 100;
        color: <?php echo $paramsData["cardTitleColor"]; ?>;
        text-align: center;
        letter-spacing: 5px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        padding-left: 5%;
        padding-right: 5%;

    }
    .case-study__subtitle{
        font-family: 'NotoSans-Regular' !important;
        position: relative;
        margin-bottom: 20px;
        font-weight: 100;
        color:  <?php echo $paramsData["cardSubTitleColor"]; ?>;
        text-align: center;
        letter-spacing: 5px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        padding-left: 5%;
        padding-right: 5%;
        font-size: 12px;

    }

    .case-study__link {
        font-family: 'OpenSans-Regular' !important;
        position: relative;
        display: inline-block;
        width: fit-content;
        padding: 10px 60px;
        margin: 0 auto;
        color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["buttonColorlabel"]; ?>;
        letter-spacing: 3px;
        text-decoration: none!important;
        text-align: center;
        border: 4px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["buttonColorBorder"]; ?>;
        border-radius: <?php echo $paramsData["buttonColorRadius"]; ?>px;
        font-size: 18px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        margin-left: 5px;
        margin-right: 5px;
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonColor"]; ?>;
    }
    .case-study__link:hover, .carousel-btn-action:hover {
        background-color: <?php echo $paramsData["buttonColorBorder"]; ?>;
        color: <?php echo $paramsData["buttonColor"]; ?>;
    }
    .case-study__logo {
        width: auto;
        height: <?php echo $paramsData["imgHeight"]; ?>px;
        position: relative;
        padding: 30px 10px 0px 10px;
        border-radius: 10px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    /*.case-study__overlay:hover .case-study__logo {
        width: auto;
    }*/
    .case-study__logo img{
        object-fit: contain;
        width: 100%;
        height: 100px;
    }
    .carousel-btn-action {
        position: relative;
        display: inline-block;
        width: fit-content;
        padding: 10px;
        margin: 0 auto;
        color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["buttonColorlabel"]; ?>;
        letter-spacing: 3px;
        text-decoration: none!important;
        text-align: center;
        border: 4px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["buttonColorBorder"]; ?>;
        border-radius: <?php echo $paramsData["buttonColorRadius"]; ?>px;
        font-size: 18px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonColor"]; ?>;
        transition: all .5s ease 0s;
    }
    .action-button {
        -webkit-transition: all 0.5s ease;
        -moz-transition: all 0.5s ease;
        -o-transition: all 0.5s ease;
        transition: all 0.5s ease;
        opacity: 0;
    }
    .case-study__overlay:hover .action-button {
        opacity: 1;
    }
    .case-study figure .content-image{
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        -ms-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }
    .case-study:hover figure .content-image{
        -webkit-transform: scale(1.2) rotate(-7deg);
        -moz-transform: scale(1.2) rotate(-7deg);
        -ms-transform: scale(1.2) rotate(-7deg);
        -o-transform: scale(1.2) rotate(-7deg);
        transform: scale(1.2) rotate(-7deg);
    }
    #add_<?php echo $kunik ?>{
        font-family: 'NotoSans-Regular' !important;
        background-color: #fff;
        padding: 5px 8px;
        font-size: 16px;
        margin-top: 10px;
        margin-bottom: 20px;
        font-weight: bold;
        line-height: 28px;
    }
</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 container<?php echo $kunik ?> ">
    <h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock"><?php echo $paramsData["titleBlock"]; ?></h3>
    <div class="text-center">
        <div class="text-center padding-top-20">
            <button id="add_<?php echo $kunik ?>" class="btn  btn-add ">
                <i class="fa fa-plus-circle"></i> J’ajoute ma startup
            </button>
        </div>
    </div>
    <div class="swiper-container">
        <div class="swiper-wrapper"></div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>

    
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {

        $("#add_<?php echo $kunik ?>").click(function(){
            dyFObj.openForm('startup');
        })
        var dyfOrga = {
            "beforeBuild": {
                "properties": {
                    "name": {
                        "label": "Dénomination",
                        "inputType": "text",
                        "placeholder": "Raison Sociale de l’organisme"    
                    },
                    "namesStartupers": {
                        "label": "Nom(s) du(es) Startuper(uses)s ",
                        "inputType": "lists",
                        "entries": {
                            "key": {
                                "type": "hidden",
                                "class": ""
                            },
                            "nameStartuper": {
                                "label": "Nom du Startuper(use)",
                                "type": "text",
                                "class": "col-md-11 col-sm-11 col-xs-12"
                            }
                        },
                        "order": 3
                    },
                    "yearEntryInpri": {
                        "label": "Année d'entrée au sein de l'Incubateur de la Recherche Publique",
                        "inputType": "monthyear",
                        "order": 4
                    },
                    "areaOfIntervention": {
                        "label": "Domaine d'intervention",
                        "inputType": "text",
                        "order": 5
                    },
                    "link": {
                        "label": "Lien WEB",
                        "inputType": "text",
                        "placeholder": "Lien WEB de l’organisme",
                        "order": 6
                    },
                    "formLocality": {
                        "label": "Adresse postale complète de l’organisme"
                    },
                    "description": {
                        "label": "Offres de services",
                        "inputType": "textarea",
                        "order": 7
                    },
                    "email": {
                        "label": "Mail",
                        "placeholder": "Adresse(s) électronique(s) de l’organisme",
                        "order": 8
                    },
                    "mobile": {
                        "label": "Téléphone",
                        "inputType": "text",
                        "placeholder": "Téléphone fixe ou portable de l’organisme",
                        "order":9
                    },
                    "objectiveOdd": {
                        "label": "A quel(s) objectif(s) ONUSIEN de Développement Durable répondez-vous…?",
                        "inputType": "selectMultiple",
                        "placeholder": "",
                        "class": "multi-select",
                        "isSelect2": true,
                        "list": "objectiveOdd",
                        "order":810 
                    },
                    "thematic": {
                        "label": "Secteurs",
                        "inputType": "selectMultiple",
                        "placeholder": "",
                        "class": "multi-select",
                        "isSelect2": true,
                        "list": "themes",
                        "order": 11 
                    },
                    "image": {
                        "label":"Télécharger votre LOGO en Haute Définition (HD)",
                        "rules" : {
                            "required": true
                        }
                    },
                    "category": {
                        "inputType": "hidden",
                        "value": "startup"
                    }
                }
            },
            "afterSave": "costum.meir.startup.afterSave",
            "afterBuild" :{},
            "onLoads" : {
                "onload" :{}
            },
            "onload": {
                "actions": {
                    "setTitle": "PRÉSENTEZ VOUS",
                    "html": {
                        "infocustom": "<br/><?php echo Yii::t('cms', 'Fill in the field')?>"
                    },
                    "hide": {
                        "shortDescriptiontextarea": 1,  
                        "breadcrumbcustom" : 1,
                        "parentfinder" : 1,                        
                        "urltext": 1,
                        "typeselect": 1,
                        "roleselect": 1
                    }
                }
            }
        }

        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "imgRadius" : {
                        label : "<?php echo Yii::t('cms', 'Radius of the card')?> (px)",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgRadius
                    },
                    "buttonColorlabel":{
                        label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColorlabel
                    },
                    "buttonColor":{
                        label : "<?php echo Yii::t('cms', 'Button color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColor
                    },
                    "buttonColorBorder":{
                        label : "<?php echo Yii::t('cms', 'Button border color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColorBorder
                    },
                    "buttonColorRadius": {
                        label : "<?php echo Yii::t('cms', 'Border radius')?> (px)",
                        inputType : "text",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.buttonColorRadius
                    },
                    "typePoi":{
                        "label" : "<?php echo Yii::t('cms', 'Type of Point of Interest')?>",
                        inputType : "select",
                        options : poiOptions,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.typePoi
                    },
                    "image" :{
                        "inputType" : "uploader",
                        "label" : "image",
                        "docType" : "image",
                        "contentKey" : "slider",
                        "itemLimit" : 1,
                        "endPoint" :"/subKey/notBackground",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        initList : <?php echo json_encode($initFiles) ?>
                    },
                    "imgHeight" : {
                        label : "<?php echo Yii::t('cms', 'Height of the image (in px)')?> ",
                        inputType : "text",
                        "rules" : {
                            "required": true,
                            "number" : true
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgHeight
                    },
                    "cardBgColor":{
                        label : "<?php echo Yii::t('cms', 'Background Color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.cardBgColor
                    },
                    "cardTitleColor":{
                        label : "<?php echo Yii::t('cms', 'Title color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.cardTitleColor
                    },
                    "cardSubTitleColor":{
                        label : "<?php echo Yii::t('cms', 'Subtitle color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.cardSubTitleColor
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }
                }
            }
        };
        mylog.log("paramsData",sectionDyf);
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"img",4,6,1,null,"Propriété de l'image","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"button",6,6,null,null,"Propriété du bouton","green","");
        });


        /**************************** list**************************/
        var isInterfaceAdmin = false
        <?php
        if(Authorisation::isInterfaceAdmin()){ ?>
        isInterfaceAdmin = true
        <?php } ?>

        getAffiche();

        function getAffiche(){
            var params = {
                searchType : ["organizations"],
                filters : {
                    category : "startup"
                },
                fields : ["areaOfIntervention"],
                "sortBy" : {"created":-1},
                indexStep:60
            };
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    console.log("blockcms poi",data);
                    var html = "";

                    $.each(data.results, function( index, value ) {
                        html += '<div class="swiper-slide" >' +
                            '<div class="">' +
                            '<div class="case-study study1">' +
                            '<figure class="text-center">';
                        <?php if (count($arrayImg)!=0) {?>
                            html += '<img class="content-image" src="<?php echo $arrayImg[0] ?>" alt="">';
                        <?php }else{ ?>
                            /*html += '<img class="content-image" src="<?php echo Yii::app()->controller->module->assetsUrl ?>/images/thumbnail-default.jpg">';*/
                        <?php } ?>

                        html +='</figure>' +
                            '<div class="case-study__overlay text-center co-scroll animated  fadeInLeft">' ;
                        if (typeof value.profilMediumImageUrl && value.profilMediumImageUrl != null)
                            html +='<div class="case-study__logo"><div class="text-center"><img class="img-responsive"  src="'+value.profilMediumImageUrl+'" alt=""></div></div>';
                        else
                            html +='<div class="case-study__logo"><div class="text-center"><img class="img-responsive"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"></div></div>';

                             html +=
                            '<div class="case-study__title">'+value.name+'</div>';
                            if(typeof value.areaOfIntervention != "undefined"){
                                html +='<div class="case-study__subtitle">'+value.areaOfIntervention+'</div>';
                            }
                            html +='<div class="col-xl-12 action-button">';
                        if(isInterfaceAdmin == true)
                            html +='<a href="javascript:;" class="edit carousel-btn-action hiddenPreview" data-id="'+value._id.$id+'"  data-type="'+value.type+'" ><i class="fa fa-edit"></i></a>';

                            html +='<a class="case-study__link" href="javascript:;" data-id="'+value._id.$id+'" data-type="'+value.type+'" data-collection="'+value.collection+'">Voir</a>' ;
                        if(isInterfaceAdmin == true)
                            html +='<a href="javascript:;" class="delete carousel-btn-action hiddenPreview" data-id="'+value._id.$id+'" data-type="'+value.type+'" ><i class="fa fa-trash"></i></a>';

                            html +='</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';


                    });
                    $('.container<?php echo $kunik ?> .swiper-wrapper').html(html);
                    $(".container<?php echo $kunik ?> .swiper-wrapper .edit").off().on('click',function(){
                        var id = $(this).data("id");
                        var type = $(this).data("type");
                        dyFObj.editElement('organizations',id,null,dyfOrga);
                    });
                    $('.case-study__link').off().on('click',function(){
                        var btn = $(this);
                        urlCtrl.openPreview("view/url/costum.views.custom.meir.element.preview",{
                             id:btn.data("id"),
                            type:btn.data("type"),
                            collection:btn.data("collection"),
                      });
                    });
                    $(".container<?php echo $kunik ?> .swiper-wrapper .delete").off().on("click",function () {
                        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                        var btnClick = $(this);
                        var id = $(this).data("id");
                        var type = "organizations";
                        var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;

                        bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
                            function(result)
                            {
                                if (!result) {
                                    btnClick.empty().html('<i class="fa fa-trash"></i>');
                                    return;
                                } else {
                                    ajaxPost(
                                        null,
                                        urlToSend,
                                        null,
                                        function(data){
                                            if ( data && data.result ) {
                                                toastr.success("élément effacé");
                                                $("#"+type+id).remove();
                                                getAffiche();
                                            } else {
                                                toastr.error("something went wrong!! please try again.");
                                            }
                                        }
                                    );
                                }
                            });
                    });

                    var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        // init: false,
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        },
                        navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                        keyboard: {
                            enabled: true,
                        },
                        autoplay: {
                           delay: 5000,
                           disableOnInteraction: false,
                        },
                        breakpoints: {
                            640: {
                                slidesPerView: 1,
                                spaceBetween: 20,
                            },
                            768: {
                                slidesPerView: 2,
                                spaceBetween: 40,
                            },
                            1024: {
                                slidesPerView: 3,
                                spaceBetween: 50,
                            },
                        }
                    });

                    coInterface.bindLBHLinks();
                }
            );
        }


    });


</script>