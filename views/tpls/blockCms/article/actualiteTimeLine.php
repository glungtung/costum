<?php 
      $keyTpl ="actualiteTimeLine";
      $paramsData = [
          "title" =>"Actualité",
          "titleIcon" => "fa-newspaper-o",
          "underline" => true
      ];

    if (isset($blockCms)) {
      foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
          $paramsData[$e] = $blockCms[$e];
        }
      }
    }
  ?>

<?php  

	if(isset($costum["contextType"]) && isset($costum["contextId"])){
        $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
    }

?>
<style type="text/css">
  .actu-<?= $kunik?>{
    text-align: center;
    position: relative;
  }

  .actu-<?= $kunik?> #newsstream<?= $kunik?>{
    min-height: auto !important;
  }

  .actu-<?= $kunik?> #newsstream<?= $kunik?> .timeline{
    max-height: 600px;
    overflow: hidden;
    margin-bottom: 0px;
  }

  .actu-<?= $kunik?> .actu-footer{
    width: 100%;
    position: absolute;
    bottom: 0;
    padding: 50px 0px;
    background: linear-gradient(0deg, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 50%, rgba(255,255,255,0) 100%);
  }

  .actu-<?= $kunik?> .actu-footer a{
    text-decoration: none;
    background-color: #2C3E50;
    color: white;
    padding: 8px 20px;
    border-radius: 50px;
    font-size: 16px;
    transition: .5s;
    border: 2px solid #2C3E50;
  }

  .actu-<?= $kunik?> .actu-footer a:hover{
    background-color: white;
    color: #2C3E50;
  }
</style>


<!-- Actualité -->
<div class="actu-<?= $kunik?> row content-<?= $kunik?>">
    <div class="col-xs-12 col-sm-12">
        <div class="actu-titre col-xs-6 col-sm-6">
            <h1 class="title actu-<?= $kunik?>">
              <i class="fa <?php echo $paramsData["titleIcon"]; ?>"></i> 
              <span class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></span>
            </h1>
        </div>
        <br>
        <div class="actucard col-xs-12 col-sm-12 col-md-12">
            <div id="newsstream<?= $kunik?>"></div>
        </div>
		
        <div class="text-center actu-footer">
            <a href="javascript:;" data-hash="#live" class="lbh-menu-app h3">
                <i class="fa fa-plus circle"></i> Voir plus d'actualités
            </a>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
	
		urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";
		
		ajaxPost("#newsstream<?= $kunik?><?= $kunik?>",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
		if(notNull(currentUser)) currentUser.addressCountry = "RE";
		
		contextData = {
			id : "<?php echo $costum["contextId"] ?>",
			type : "<?php echo $costum["contextType"] ?>",
			name : "<?php echo $el['name'] ?>",
			slug : "<?php echo $el['slug'] ?>",
			profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
		};
	});
</script>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        
        "properties" : {
          
        },
        beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  })
</script>