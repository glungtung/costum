<?php
$keyTpl = "blockarticlescmcas";

$paramsData = [
	"title"					=>	"Nos Actualités",
	"Textcolor"				=>	"#2c407a",
	"descriptionColor"		=>	"#2c407a",
	"description"			=>	"Toute l'actualité de la CMCAS : reportages, portraits et agenda des activités sociales et culturelles."
];

if (isset($blockCms)) {
    $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
    foreach ($paramsData as $e => $v) {
        if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
            $paramsData[$e] = $tplsCms[$e];
        }
    }
}
?>

<div id="actu-cmcas" class="container text-center col-md-10 col-sm-12 col-xs-12 col-md-offset-1" style="margin-top: 100px;">
	<h2 style="color:<?= $paramsData["Textcolor"] ?>"><?= $paramsData["title"]; ?></h2>
	<p style="color:<?= $paramsData["descriptionColor"]; ?>" class="text-blue-cmcas">
		<?= $paramsData["description"]; ?>
	</p>
	<div id="result-actus" class="featured-services-grids">
		<!-- contain the actus at the end off the file -->
		
	</div>
</div>


<script type="text/javascript">
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
	sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            onLoads : {
                onload : function(){
                    $(".parentfinder").css("display","none");
                }
            },
            "properties" : {
                "title" : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                Textcolor : {
                    label : "<?php echo Yii::t('cms', 'Title color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.Textcolor
                },
                description : {
                    label : "<?php echo Yii::t('cms', 'Description')?>",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.description
                },
                descriptionColor : {
                    label : "<?php echo Yii::t('cms', 'Color of the description')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.descriptionColor
                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
            save : function () {  
            	tplCtx.value = {};

            	if (typeof idToPath != "undefined") {
                    tplCtx.value["id"] = idToPath;
                }

                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value["parent"] = formData.parent ;
                    }
                });

                tplCtx.value["page"] = '<?= @$page ?>';
                tplCtx.value["type"] = 'tpls.blockCms.article.<?= $keyTpl ?>';
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("élement mis à jour");
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }
    };

	$(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
		tplCtx = { value : {} };
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = (typeof $(this).data("path") != "undefined" && $(this).data("path") != "") ? $(this).data("path") : "allToRoot";
        idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
	});

	params = {
		"sourceKey" : contextSlug
	};

	ajaxPost(
		null, 
		baseUrl+"/costum/cmcashautebretagne/getarticles", 
		params,
		function(results){ 
			mylog.log("articles :",results);

			if (results.dataArticles = true) {

				var ctr = "";
				var ctCol = 0;
				var color = [
					"#e36f41",
					"#791880",
					"#00a890",
					"#dc094b"
				];
				
				if(typeof results.article != "undefined" && results.article != null){
					$.each(results.article,function(key,value){

						var imgMedium = (value.imgMedium != "none") ? value.imgMedium : url;

						ctr += '<div class="col-md-6 col-sm-6 col-xs-12 grid-actu">';
							ctr += '<div class="well">';
								ctr += '<div class="contain">';
									ctr += '<div class="date-actu">';
										ctr += '<a href="#" class="actu-link"  style="background-color: '+color[ctCol]+'">';
											ctr += value.dCreate+' '+value.mCreate+' '+value.yCreate;
										ctr += '</a>';
									ctr += '</div>';
									ctr += '<div class="actu-name">';
										ctr += value.name;
									ctr += '</div>';
									ctr += '<a href="#page.type.'+value.collection+'.id.'+value.id+'" class="lbh-preview-element btn btn-default btn-more-actu"><?php echo Yii::t("cms", "More information")?></a>';
								ctr += '</div>';
								ctr += '<div class="featured-services-grid pull-right">';
									ctr += '<div class="featured-services-grd" style="background: url('+baseUrl+value.imgMedium+') center;">';
									ctr += '</div>';
								ctr += '</div>';
							ctr += '</div>';
						ctr += '</div>';
						ctCol++;
					});
				}
				
				
			}else{
				ctr += "<?php echo Yii::t('cms', 'No news for the moment')?>";
			}

			$("#result-actus").html(ctr);
			coInterface.bindLBHLinks();
		},
		function(){
			mylog.log("error articles");
		}
	);
});
</script>