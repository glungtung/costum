<?php
$keyTpl ="actualite";
$paramsData = [ 
  "title" => "Actualité",
    "icon"          =>  ""
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 
$where = array("source.key"=> $costum["contextSlug"]);


if (!empty($filters)) {
  $where["thematique"] = $filters;
}
$actualite = PHDB::findAndLimitAndIndex(Poi::COLLECTION,$where,3);
?>


<style type="text/css">
  #containActuality_<?= $kunik?> .img-event{
    height : 200px;
    width: 500px;
  }
  #containActuality_<?= $kunik?> .card-event{
    margin-top: -30%;

  }

  #services_<?= $kunik?> .logoActualite{
    width :250px;
    height : 50px;
  }
  #services_<?= $kunik?>{
    margin-top: 40px;
  }

  #services_<?= $kunik?> h5{
    font-size: 16px;
    line-height: 22px;0;
  }
  @media (max-width: 978px) {
   
    #services_<?= $kunik?> h4{
      font-size: 20px !important;
      margin: 0px;
      padding:0 !important;
      
    }
    #services_<?= $kunik?> h5{
      padding: 0 !important;
      font-size: 12px !important;
      margin: 0;
    }
    #services_<?= $kunik?> .logoActualite{
      width :125px;
      height : 30px;
    }
    #services_<?= $kunik?>{
      margin-top: 0px;
    }
    #containActuality_<?= $kunik?> .img-event {
      height: 100px;
      width: 250px;
    }
  }
  #services_<?= $kunik?> .btn-<?= $kunik?>{
    display: none;
    z-index: 9999;
  }
  #services_<?= $kunik?>:hover .btn-<?= $kunik?>{
    display: block;
    position: absolute;
    top:40%;
    left: 50%;
    transform: translate(-50%,-50%);
  }

</style>


<div id="services_<?= $kunik?>" class="services_<?= $kunik?> content_<?= $kunik?> ">
  <div class="container">  
     <div class="btn-<?= $kunik?> text-center">
   
    <?php if(Authorisation::isInterfaceAdmin()){ ?>
      <button id="add-article<?= $kunik?>" class="btn btn-primary btn-xs" ><?php echo Yii::t('cms', 'Add an article')?></button>
    <?php } ?>
  </div>
  <p class="sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> 
    <i class="fa <?=$paramsData['icon']?>"></i> 
    <?= $paramsData["title"]?>
    </p>
  <div id="containActuality_<?= $kunik?>" class="col-xs-12" >
    <div class="row"> 

  </div>
</div>

<script type="text/javascript">
  function addArticle(){
    var dyfPoi={
      "onload" : {
          "actions" : {
                "setTitle" : "<?php echo Yii::t('cms', 'Add an article')?>",
                "html" : {
                    "infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
                },
                "presetValue" : {
                    "type" : "article",
                },
                "hide" : {
                    "breadcrumbcustom" : 1,
                    "parentfinder" : 1,
                }
            }
      }
    };
    dyfPoi.afterSave = function(data){
      dyFObj.commonAfterSave(data,function(){
        mylog.log("data", data);
        urlCtrl.loadByHash(location.hash);
      });
    };
    dyFObj.openForm('poi',null, null,null,dyfPoi);
  }
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    $("#add-article<?= $kunik?>").click(function(){
      addArticle();
    })
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        
        "properties" : {
          icon : { 
            label : "<?php echo Yii::t('cms', 'Icon')?>",
            inputType : "select",
            options : <?= json_encode(Cms::$icones); ?>,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
          }
        },
        beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    var params = {
      "source" : contextSlug,
      "type"  : "article"
    };
    ajaxPost(
      null,
      baseUrl+"/costum/costumgenerique/getpoi",
      params,
      function(data){
        mylog.log("success", data);
        var str = "";
        if(data.result == true)
        {
          var i = 0;
          var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

          $(data.element).each(function(key,value){
            if (i <= 2) {
              i++;
              var img = (typeof value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;
              var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "Aucune description";
              
              str += '<div class="sp-cms-container text-center"> ';
              str +='<div class="portfolio_list_inner">';
              str +='<div class="col-lg-4 col-md-4 col-xs-12" >';
              str +='<div class="portfolio_item" >';
              str +='<div class="portfolio_img "  style="visibility: visible; animation-delay: 0.5s; animation-name: bounceInDown;">';
              str += '<img src="'+img+'" alt="" class="img-event img-responsive " style="visibility: visible;">';
              str +='</div>';
              str +='<div class="portfolio-info " >';
              str +='<h4>'+value.name+'</h4>';
              str +='<h5>'+description+'</h5>';
              str +='</div>';
              str +='<div class="text-center ">';
              str +='<a href="#page.type.poi.id.'+value.id+'" class="lbh-preview-element">';
              str +='<img class="logo2 text-center" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/assoKosasa/en savoir plus orange.svg" width="150" height="100" />';
              str +='</a>';
              str +='</div>';
              str +='</div>'; 
              str +='</div>';
              str +='</div>';


            }else{
            }

          });

          mylog.log("str actualité description",str);
        }else
        {
          str += "<center>Il n'éxiste aucune actualité</center>";
        }
        $("#containActuality_<?= $kunik?>").html(str);
      }

    )

  });
 
</script>
    