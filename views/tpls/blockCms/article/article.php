<?php
$keyTpl ="article";
$paramsData = [ 
	"title" => "Actualité",
	"titleIcon"=>  "",
	"affichagelimit"   => 3,
	"titleBackground" => "#ffffff",
	"affichageType" => "carrousel",
	"categorieBg"=>"#f2b202",
	"categorieIcon" =>"",	
	"colorCategorie" => "#f2b202",
	"colorDescription" => "#000000",
	"colorTitle"=> "#f2b202",
	"background"=>"#FFFFFF",
	"btnDelete"	=> "",
	"btnEdit"	=> "",
	"btnEnsvp"	=> "",
	"btnColorLabel"=>"#f2b202"

];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 
?>
<?php
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);

$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name","emailContact","profilMediumImageUrl","email"]);
$emailContact = isset($parent["emailContact"])?$parent["emailContact"]:[];
$email = isset($parent["email"])?$parent["email"]:"cocity@gmail.com";
?>


<style type="text/css">
	#services_<?= $kunik?> .img<?= $kunik?>{
		height: 300px;
		object-fit: cover;
		object-position: center;
	}
	#services_<?= $kunik?> .bgTitle<?= $kunik?> h1{
		background-color: <?php echo $paramsData["titleBackground"]; ?>;
		padding: 10px;
		font-weight: bold;
	}
	.post-module_<?=$kunik?> {
		position: relative;
		z-index: 1;
		display: block;
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["background"]; ?>;
		min-width: 270px;
		-webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
		-moz-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
		box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
		-webkit-transition: all 0.3s linear 0s;
		-moz-transition: all 0.3s linear 0s;
		-ms-transition: all 0.3s linear 0s;
		-o-transition: all 0.3s linear 0s;
		transition: all 0.3s linear 0s;
	}
	.post-module_<?=$kunik?>:hover,
	.hover {
		-webkit-box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);
		-moz-box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);
		box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);
	}
	.post-module_<?=$kunik?>:hover .thumbnail img,
	.hover .thumbnail img {
		-webkit-transform: scale(1.1);
		-moz-transform: scale(1.1);
		transform: scale(1.1);
		opacity: .6;
	}
	.post-module_<?=$kunik?> .thumbnail {
		background: #000000;
		height: 350px;
		overflow: hidden;padding: 0;
		margin-top: 0;
	}
	.post-module_<?=$kunik?> .thumbnail .date {
		position: absolute;
		top: 20px;
		right: 20px;
		z-index: 1;
		display:  <?= $paramsData["categorieIcon"]?>;
		background: <?= $paramsData["categorieBg"]?>;
		width: 55px;
		height: 55px;
		padding: 12.5px 0;
		-webkit-border-radius: 100%;
		-moz-border-radius: 100%;
		border-radius: 100%;
		color: <?= $paramsData["colorCategorie"]?>;
		font-weight: 700;
		text-align: center;
		-webkti-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}
	.post-module_<?=$kunik?> .thumbnail .date .day {
		font-size: 18px;
		line-height: 31px;
		color: #fff;
	}
	.post-module_<?=$kunik?> .thumbnail .date .month {
		font-size: 12px;
		text-transform: uppercase;
	}
	.post-module_<?=$kunik?> .thumbnail img {
		display: block;
		width: 120%;
		-webkit-transition: all 0.3s linear 0s;
		-moz-transition: all 0.3s linear 0s;
		-ms-transition: all 0.3s linear 0s;
		-o-transition: all 0.3s linear 0s;
		transition: all 0.3s linear 0s;
	}
	.post-module_<?=$kunik?> .post-content {
		position: absolute;
		bottom: 0;
		background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["background"]; ?>;
		width: 100%;
		padding: 0 30px;
		-webkti-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
		-moz-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
		-ms-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
		-o-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
		transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
	}
	.post-module_<?=$kunik?> .post-content .category {
		position: absolute;
		top: -34px;
		left: 0;	  
		display:  <?= $paramsData["categorieIcon"]?>;
		background: <?= $paramsData["categorieBg"]?>;
		padding: 10px 15px;
		color: <?= $paramsData["colorCategorie"]?>;
		font-size: 14px;
		font-weight: 600;
		text-transform: uppercase;
	}
	.post-module_<?=$kunik?> .post-content .title-2 {
		margin: 0;
		padding: 0 0 10px;
		color: <?= $paramsData["colorTitle"]?> !important;
		font-size: 20px !important;
		font-weight: 700;    
		margin: 25px 0 0 !important;
		text-transform: none;
	}
	.post-module_<?=$kunik?> .post-content .description {
		display: none;
		color: <?= $paramsData["colorDescription"]?>;
		font-size: 14px;
		line-height: 1.8em;
		overflow-y: auto;
		max-height: 100px;
	}
	.post-module_<?=$kunik?> .post-content .post-meta {
		margin: 0px 0px 10px;
		color: <?= $paramsData["categorieBg"]?>;
	}
	.post-module_<?=$kunik?> .post-content .post-meta .timestamp {
		margin: 0 16px 0 0;
	}
	.post-module_<?=$kunik?> .post-content .post-meta a {
		color: <?= $paramsData["btnColorLabel"]?>;
		text-decoration: none;
	}
	.hover .post-content .description {
		display: block !important;
		height: auto !important;
		opacity: 1 !important;
	}
	.co-scroll::-webkit-scrollbar-thumb {
		background-color: #2c3e50;
	}
	.co-scroll::-webkit-scrollbar-thumb:hover {
		background-color: #c1c1c1;
	}
	.co-scroll {
		padding-right: 10px;
	}
	@media (max-width: 768px) {
		.card_<?=$kunik?>{
			padding: 5px;
		}
	}
	.title_pricingTable {
		margin-top: 20px;
	}
	.card_<?=$kunik?>{
		padding: 30px;
		padding-top: 5px;
	}
	.delete {
		display: <?= $paramsData["btnDelete"]?>
	}
	.edit {
		display: <?= $paramsData["btnEdit"]?>
	}
	.btnEnsvp {
		display: <?= $paramsData["btnEnsvp"]?>
	}
	#add-article<?= $kunik?> {
		display: none;
		padding: 5px;
		font-size: 16px;
	}
	#services_<?= $kunik?>:hover #add-article<?= $kunik?>{
		display: block;
		position: absolute;
		top:7%;
		left: 50%;
		transform: translate(-50%,-50%);
	}
	#services_<?= $kunik?> .actu<?= $kunik?>{
		margin-bottom: 10px;
	}
</style>


<div id="services_<?= $kunik?>" class="services_<?= $kunik?> content_<?= $kunik?> ">
	<div class="btn-<?= $kunik?> text-center">

		<?php if(Authorisation::isInterfaceAdmin()){ ?>
			<button id="add-article<?= $kunik?>" class="btn btn-primary btn-xs" >Ajouter un article</button>
		<?php } ?>
	</div>
	<div class=" bgTitle<?= $kunik?>"  >
		<h1 cclass="sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> 
			<i class="fa fa-<?=$paramsData['titleIcon']?>"></i> 
			<?= $paramsData["title"]?>
		</h1>
	</div>
	<div class="row card_<?=$kunik?> <?= $kunik?>">
		<div class="swiper-container">
			<div class="swiper-wrapper"></div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
	</div>

</div> 

<script type="text/javascript">
	function sendEmail(data){
		var msg = "<div class='container'>"+
		"<a href='http://communecter56-dev/costum/co/index/slug/"+costum.contextSlug+"' target='_blank'> <h2> <?= $parent["name"]?> </h2> </a>  vient de publier un nouvel article "+
		"<a href='"+baseUrl+"#page.type.poi.id."+data.id+"' target='_blank' >"+
		"<h4>"+ data.map.name +"</h4>"+
		"</a>"
		"</div>";
		var allEmail = <?= json_encode($emailContact)?>;
		mylog.log("allEmail",allEmail);
		$.each(allEmail,function(k,email){
			var params={
				tpl : "basic",
	             tplObject:"article",
	            tplMail : email,
	            fromMail: "<?= $email?>",
	            html: msg
	        };
	        ajaxPost(
	        	null,
	        	baseUrl+"/co2/mailmanagement/createandsend",
	        	params,
	        	function(data){ 
	        	}
	        );
	    });	 
	}
	function addArticle(){
		var dyfPoi={
			"beforeBuild" : {
				"properties" : {
					"name" : {
						"label" : "<?php echo Yii::t('cms', 'Icon')?>Titre de l'actualité",
						"placeholder" : "Entrez le titre de votre Actualité...",
						"order" : 1
					},
					"parent" : {
						"label" : "Auteur(s)"
					},
					"description" : {
						"inputType" : "textarea",
						"label" : "<?php echo Yii::t('cms', 'Icon')?>Court résumé ",
						"placeholder" : "Entrez un résumé de votre actualité (250 caractères max.)...",
						"rules" : {
							"maxlength" : 250,
							"required" : true
						},
						"order" : 3
					},
					"image" : {
						"label" : "Image(s)",
						"order" : 4,
						"rules" : {
							"required" : true
						}
					}
				}
			},
			"onload" : {
				"actions" : {
					"setTitle" : "<?php echo Yii::t('cms', 'Add an article')?>",
					"html" : {
						"infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
					},
					"presetValue" : {
						"type" : "article",
					},
					"hide" : {
						"breadcrumbcustom" : 1,
						"parentfinder" : 1,
					}
				}
			}
		};
		dyfPoi.afterSave = function(data){
          		dyFObj.commonAfterSave(data, function(){
            		mylog.log("dataaaa", data);
            		sendEmail(data);
            		urlCtrl.loadByHash(location.hash);
          });
        }  
		dyFObj.openForm('poi',null, null,null,dyfPoi);
	}
	
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {

		$("#add-article<?= $kunik?>").click(function(){
			addArticle();
		})
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					titleIcon : { 
						label : "<?php echo Yii::t('cms', 'Icon')?>",
						inputType : "select",
						options : fontAwesome,
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleIcon
					},
					affichagelimit : { 
						label : "<?php echo Yii::t('cms', 'Number of news items displayed')?>",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.affichagelimit
					},
					titleBackground : { 
						label : "<?php echo Yii::t('cms', 'Background color of the title')?>",
						"inputType" : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleBackground
					},
					affichageType : { 
						label : "<?php echo Yii::t('cms', 'Display type')?>",
						"inputType" : "select",
						options : {
							"carrousel": "<?php echo Yii::t('cms', 'Carrousel')?>",
							"enPod"   :"<?php echo Yii::t('cms', 'In POD')?>",
							"timeline" : "<?php echo Yii::t('cms', 'Timeline')?>"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.affichageType
					},
					categorieBg:{
						label : "<?php echo Yii::t('cms', 'Color background category')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.categorieBg
					},
					categorieIcon : { 
						label : "<?php echo Yii::t('cms', 'Display')?>",
						"inputType" : "select",
						options : {
							"none": "<?php echo Yii::t('cms', 'None')?>",
							""   :"<?php echo Yii::t('cms', 'Show')?>"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.categorieIcon
					},
					btnDelete : { 
						label : "<?php echo Yii::t('cms', 'Deleted')?>",
						"inputType" : "select",
						options : {
							"none": "<?php echo Yii::t('cms', 'None')?>",
							""   :"<?php echo Yii::t('cms', 'Show')?>"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnDelete
					},
					btnEdit : { 
						label : "<?php echo Yii::t('cms', 'Edit')?>",
						"inputType" : "select",
						options : {
							"none": "<?php echo Yii::t('cms', 'None')?>",
							""   :"<?php echo Yii::t('cms', 'Show')?>"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnEdit
					},
					btnEnsvp : { 
						label : "<?php echo Yii::t('cms', 'More information')?>",
						"inputType" : "select",
						options : {
							"none": "<?php echo Yii::t('cms', 'None')?>",
							""   :"<?php echo Yii::t('cms', 'Show')?>"
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnEnsvp
					},
					"btnColorLabel":{
						label : "<?php echo Yii::t('cms', 'Label color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnColorLabel
					},
					"background":{
						label : "<?php echo Yii::t('cms', 'Card background color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.background
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						});
					}

				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"title",4,6,null,null,"<?php echo Yii::t('cms', 'Title')?>","#1da0b6");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"affichage",6,12,null,null,"<?php echo Yii::t('cms', 'Display')?>","#1da0b6");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"categorie",4,6,null,null,"<?php echo Yii::t('cms', 'Icon category')?>","#1da0b6");
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"btn",4,6,null,null,"<?php echo Yii::t('cms', 'Button')?>","#1da0b6");

			$('.fieldsettitle,.fieldsetaffichage').show();
		});

		getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+'/id/'+costum.contextId+'/dataName/poi/limit/<?= $paramsData["affichagelimit"] ?>?tpl=json',
			function(data){
				console.log("success", data);
				var str = "";

				var i = 0;

				$.each(data,function(key,value){
					console.log("typeof", value);
					if (value.type == "article") {

						var img = ( value.profilMediumImageUrl  != null)? baseUrl+value.profilMediumImageUrl : "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
						var descriptio = value.description != undefined ? value.description : "";
						var description = descriptio.substr(0, 35);

						str += 
						'<div class="swiper-slide actu<?= $kunik?>">'+
						'<div class="column">'+
						'<div class="post-module_<?=$kunik?>">'+
						'<div class="thumbnail">'+
						'<div class="date">'+
						'<a class="lbh-preview-element" href="#page.type.poi.id.'+value._id.$id+'">'+
						'<div class="day">'+
						'<i class="fa fa-bars" aria-hidden="true"></i>'+
						'</div>'+
						'</a> '+
						'</div>'+
						'<img src="'+img+'" class="img-responsive img<?= $kunik?>" alt=""> '+
						'</div>'+
						'<div class="post-content">'+
						'<div class="category"></div>'+
						'<h2 class="title-2"><a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element">'+value.name+'</a></h2>'+
						'<p class="title-4">'+description+' ... </p>'+
						'<p class="description co-scroll">'+
	                          		//value.shortDescription +
	                          		'</p>'+
	                          		'<div class="post-meta text-center">';
	                          		if(isInterfaceAdmin == true)
	                          			str += 
	                          		'<span class="timestamp"><a class="btn btn-danger btn-xs delete" href="javascript:;" data-type="'+value.type+'" data-id="'+value._id.$id+'">'+
	                          		'<i class="fa fa-trash"></i> supprimer'+
	                          		'</a></span>'+
	                          		'<span class="comments "><a class="btn btn-primary btn-xs edit margin-right-10" href="javascript:;" data-id="'+value._id.$id+'">'+
	                          		'<i class="fa fa-pencil"></i> modifier'+
	                          		'</a></span>';
	                          		str +=
	                          		'<a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element btnEnsvp btn btn-primary btn-xs">Voir plus <i class="fa fa-long-arrow-right"></i></a>'+
	                          		'</div>'+
	                          		'</div>'+
	                          		'</div>'+
	                          		'</div>'+
	                          		'</div>';
	                          	}

	                          });

				mylog.log("str actualité description",str);

				<?php  if ($paramsData["affichageType"] == "carrousel") {
					?>
					$("#services_<?= $kunik?> .swiper-wrapper").html(str);
					$('.post-module_<?=$kunik?>').hover(function() {
						$(this).find('.description').stop().animate({
							height: "toggle",
							opacity: "toggle"
						}, 300);
					});
					var swiper = new Swiper("#services_<?php echo $kunik ?> .swiper-container", {
						slidesPerView: 1,
						spaceBetween: 0,
                // init: false,
		                pagination: {
		                	el: '.swiper-pagination',
		                	clickable: true,
		                },
		                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
		                keyboard: {
		                	enabled: true,
		                },
		                autoplay: {
		                	delay: 2500,
		                	disableOnInteraction: false,
		                },
		                breakpoints: {
		                	640: {
		                		slidesPerView: 1,
		                		spaceBetween: 20,
		                	},
		                	768: {
		                		slidesPerView: 2,
		                		spaceBetween: 40,
		                	},
		                	1024: {
		                		slidesPerView: 3,
		                		spaceBetween: 50,
		                	},
		                }
		            });
				<?php } elseif($paramsData["affichageType"] == "enPod") { ?>
					$(".<?= $kunik?>").html(str);

					$("#services_<?= $kunik?> .actu<?= $kunik?>").addClass(" col-lg-4 col-md-6 col-sm-6 col-xs-12");
					$("#services_<?= $kunik?> .actu<?= $kunik?>").removeClass("swiper-slide");
				<?php }else { ?>
					urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/true";

					ajaxPost(".<?= $kunik?>",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
					if(notNull(currentUser)) currentUser.addressCountry = "RE";
					
				<?php } ?>
				coInterface.bindLBHLinks();
			},"");

  			/****************************edit list**************************/
	        setTimeout(function(){
	            $(".card_<?=$kunik?> .edit").off().on('click',function(){
	              dyFObj.editElement('poi',$(this).data('id'));
	            });
	        },2000)
	        /****************************end edit list**************************/

	        /****************************delete list***************************/
	        setTimeout(function(){
	        	$(".card_<?=$kunik?> .delete").off().on("click",function () {
	        		$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	        		var btnClick = $(this);
	        		var id = $(this).data("id");
	        		var type = "poi";
	        		var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;

	        		bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
	        			function(result) 
	        			{
	        				if (!result) {
	        					btnClick.empty().html('<i class="fa fa-trash"></i>');
	        					return;
	        				} else {
	        					ajaxPost(
	        						null,
	        						urlToSend,
	        						null,
	        						function(data){ 
	        							if ( data && data.result ) {
	        								toastr.success("élément effacé");
	        								$("#"+type+id).remove();
	        								urlCtrl.loadByHash(location.hash);
	        							} else {
	        								toastr.error("something went wrong!! please try again.");
	        							}
	        						}
	        					);
	        				}
	        			}
	        		);
	        	});
	        },2000)
    /****************************end delete list**************************/

});

</script>
