<?php  
$keyTpl = "pricingTable";
$paramsData=[
	"title"=>"",
	"bgCategorie"=>"#f2b202",
	"colorCategorie" =>"#FFFFFF",	
	"colorTitle" =>"#222222",
	"colorDescription" =>"#666666",
	"background"=>"#FFFFFF"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
} 

?>

<style type="text/css">
	.post-module_<?=$kunik?> {
	  position: relative;
	  z-index: 1;
	  display: block;
	  background: <?= $paramsData["background"]?>;
	  min-width: 270px;
	  height: 350px;
	  -webkit-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
	  -moz-box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.15);
	  box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
	  -webkit-transition: all 0.3s linear 0s;
	  -moz-transition: all 0.3s linear 0s;
	  -ms-transition: all 0.3s linear 0s;
	  -o-transition: all 0.3s linear 0s;
	  transition: all 0.3s linear 0s;
	}
	.post-module_<?=$kunik?>:hover,
	.hover {
	  -webkit-box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);
	  -moz-box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);
	  box-shadow: 0px 1px 35px 0px rgba(0, 0, 0, 0.3);
	}
	.post-module_<?=$kunik?>:hover .thumbnail img,
	.hover .thumbnail img {
	  -webkit-transform: scale(1.1);
	  -moz-transform: scale(1.1);
	  transform: scale(1.1);
	  opacity: .6;
	}
	.post-module_<?=$kunik?> .thumbnail {
	  background: #000000;
	  height: 350px;
	  overflow: hidden;padding: 0;
	  margin-top: 30px;
	}
	.post-module_<?=$kunik?> .thumbnail .date {
	  position: absolute;
	  top: 20px;
	  right: 20px;
	  z-index: 1;
	  background: <?= $paramsData["bgCategorie"]?>;
	  width: 55px;
	  height: 55px;
	  padding: 12.5px 0;
	  -webkit-border-radius: 100%;
	  -moz-border-radius: 100%;
	  border-radius: 100%;
	  color: <?= $paramsData["colorCategorie"]?>;
	  font-weight: 700;
	  text-align: center;
	  -webkti-box-sizing: border-box;
	  -moz-box-sizing: border-box;
	  box-sizing: border-box;
	}
	.post-module_<?=$kunik?> .thumbnail .date .day {
	 font-size: 18px;
	    line-height: 31px;
	    color: #fff;
	}
	.post-module_<?=$kunik?> .thumbnail .date .month {
	  font-size: 12px;
	  text-transform: uppercase;
	}
	.post-module_<?=$kunik?> .thumbnail img {
	  display: block;
	  width: 120%;
	  -webkit-transition: all 0.3s linear 0s;
	  -moz-transition: all 0.3s linear 0s;
	  -ms-transition: all 0.3s linear 0s;
	  -o-transition: all 0.3s linear 0s;
	  transition: all 0.3s linear 0s;
	}
	.post-module_<?=$kunik?> .post-content {
	  position: absolute;
	  bottom: 0;
	  background: <?= $paramsData["background"]?>;
	  width: 100%;
	    padding: 0 30px;
	  -webkti-box-sizing: border-box;
	  -moz-box-sizing: border-box;
	  box-sizing: border-box;
	  -webkit-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
	  -moz-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
	  -ms-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
	  -o-transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
	  transition: all 0.3s cubic-bezier(0.37, 0.75, 0.61, 1.05) 0s;
	}
	.post-module_<?=$kunik?> .post-content .category {
	  position: absolute;
	  top: -34px;
	  left: 0;
	  background: <?= $paramsData["bgCategorie"]?>;
	  padding: 10px 15px;
	  color: <?= $paramsData["colorCategorie"]?>;
	  font-size: 14px;
	  font-weight: 600;
	  text-transform: uppercase;
	}
	.post-module_<?=$kunik?> .post-content .title {
	  margin: 0;
	  padding: 0 0 10px;
	  color: <?= $paramsData["colorTitle"]?> !important;
	  font-size: 20px !important;
	  font-weight: 700;    
	  margin: 25px 0 0 !important;
	  text-transform: none;
	}
	.post-module_<?=$kunik?> .post-content .sub_title {
	  margin: 0;
	  padding: 0 0 20px;
	  color: <?= $paramsData["bgCategorie"]?>;
	  font-size: 18px;
	  font-weight: 400;
	}
	.post-module_<?=$kunik?> .post-content .description {
	  display: none;
	  color: <?= $paramsData["colorDescription"]?>;
	  font-size: 14px;
	  line-height: 1.8em;
	  overflow-y: auto;
	  max-height: 100px;
	}
	.post-module_<?=$kunik?> .post-content .post-meta {
	  margin: 0px 0px 10px;
	  color: <?= $paramsData["bgCategorie"]?>;
	}
	.post-module_<?=$kunik?> .post-content .post-meta .timestamp {
	  margin: 0 16px 0 0;
	}
	.post-module_<?=$kunik?> .post-content .post-meta a {
	  color: <?= $paramsData["bgCategorie"]?>;
	  text-decoration: none;
	}
	.hover .post-content .description {
	  display: block !important;
	  height: auto !important;
	  opacity: 1 !important;
	}
	.co-scroll::-webkit-scrollbar-thumb {
	    background-color: #2c3e50;
	}
	.co-scroll::-webkit-scrollbar-thumb:hover {
	    background-color: #c1c1c1;
	}
	.co-scroll {
	    padding-right: 10px;
	}
	.card_<?=$kunik?>{
		padding: 30px;
	}
	@media (max-width: 768px) {
		.card_<?=$kunik?>{
			padding: 5px;
		}
	}
	.title_pricingTable {
		margin-top: 20px;
	}

</style>


<h3 class="title_pricingTable sp-text img-text-bloc text-center" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></h3>
<div class="row card_<?=$kunik?>">

      
</div>

<?php if(Authorisation::isInterfaceAdmin()){ ?>
	<div class="text-center padding-top-20">
		
		<button id="add-social" class="btn btn-primary btn-xs">
			<i class="fa fa-plus-circle"></i> <?php echo Yii::t('cms', 'Add an article')?>
		</button>	
	</div>
<?php } ?>

<script type="text/javascript">
function addList(){
    	var dyfPoi={
			 "onload" : {
		        "actions" : {
	                "setTitle" : "<?php echo Yii::t('cms', 'Add an article')?>",
	                "html" : {
	                    "infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
	                },
	                "presetValue" : {
	                    "type" : "article",
	                },
	                "hide" : {
	                    "breadcrumbcustom" : 1,
	                    "parentfinder" : 1,
	                }
	            }
		    }
		};
		dyfPoi.afterSave = function(data){
			dyFObj.commonAfterSave(data,function(){
				mylog.log("data", data);
				 //location.hash = "#documentation."+data.id;
				 urlCtrl.loadByHash(location.hash);

			});
		};
		dyFObj.openForm('poi',null, null,null,dyfPoi);
    }
	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {

		$("#add-social").click(function(){
			addList();
		})
		

		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					"title" : {
                        label : "<?php echo Yii::t('cms', 'Block title')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.title
                    },
					"colorTitle":{
						label : "<?php echo Yii::t('cms', 'Title color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
					"colorCategorie":{
						label : "<?php echo Yii::t('cms', 'Color category')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorCategorie
					},
					"colorDescription":{
						label : "<?php echo Yii::t('cms', 'Color description')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorDescription
					},
					"bgCategorie":{
						label : "<?php echo Yii::t('cms', 'Color background category')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.bgCategorie
					},
					"background":{
						label : "<?php echo Yii::t('cms', 'Background color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.background
					}
				},
            	beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					 if(typeof tplCtx.value == "undefined")
	                  toastr.error('value cannot be empty!');
	                else {
	                  mylog.log("activeForm save tplCtx",tplCtx);
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
				}
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});


		/**************************** list**************************/  
		var isInterfaceAdmin = false
		<?php 
			if(Authorisation::isInterfaceAdmin()){ ?>
				isInterfaceAdmin = true
		 <?php } ?>
		 
        getPricingTable();
        
        function getPricingTable(){  
        	var params = {
	          "source" : contextSlug,
	          "type"  : "article"
	        };
	        ajaxPost(
	          null,
	          baseUrl+"/costum/costumgenerique/getpoi",
	          params,
	          function(data){
	              mylog.log("blockcms poi",data);
	              var html = "";
	              $.each(data.element, function( index, value ) {
	                html += 
	                  	'<div class="col-md-4 col-sm-6 col-xs-12">'+
					        '<div class="column">'+
					          '<div class="post-module_<?=$kunik?>">'+
					            '<div class="thumbnail">'+
					              	'<div class="date">'+
					              		'<a class="lbh-preview-element" href="#page.type.poi.id.'+value.id+'">'+
					                		'<div class="day">'+
					                			'<i class="fa fa-bars" aria-hidden="true"></i>'+
					                		'</div>'+
					                	'</a> '+
					                '</div>'+
					              	'<img src="'+value.profilMediumImageUrl+'" class="img-responsive" alt=""> '+
					          	'</div>'+
					            '<div class="post-content">'+
					              '<div class="category"></div>'+
					              '<h1 class="title">'+value.name+'</h1>'+
					              '<h2 class="sub_title">'+value.type+'</h2>'+
					              '<p class="description co-scroll">'+
	                          		//value.shortDescription +
					              '</p>'+
					              '<div class="post-meta text-center">';
					        		if(isInterfaceAdmin == true)
					html += 
					                  '<span class="timestamp"><a class="btn btn-danger btn-xs delete" href="javascript:;" data-type="'+value.type+'" data-id="'+value.id+'">'+
					                            '<i class="fa fa-trash"></i> <?php echo Yii::t("common", "Delete")?>'+
					                          '</a></span>'+
					                          '<span class="comments"><a class="btn btn-primary btn-xs edit margin-right-10" href="javascript:;" data-id="'+value.id+'">'+
					                            '<i class="fa fa-pencil"></i> <?php echo Yii::t("cms", "Edit")?>'+
					                          '</a></span>';
					html +=
										'<a href="#page.type.poi.id.'+value.id+'" class="lbh-preview-element btn btn-primary btn-xs">Voir plus <i class="fa fa-long-arrow-right"></i></a>'+
					              '</div>'+
					            '</div>'+
					          '</div>'+
					        '</div>'+
				      	'</div>';

	              });
	              $(".card_<?=$kunik?>").html(html);
	              $('.post-module_<?=$kunik?>').hover(function() {
					$(this).find('.description').stop().animate({
					      height: "toggle",
					      opacity: "toggle"
					    }, 300);
					});
	           }
	        );
	    }
        /****************************end list**********************/

        /****************************edit list**************************/
        setTimeout(function(){
            $(".card_<?=$kunik?> .edit").off().on('click',function(){
              dyFObj.editElement('poi',$(this).data('id'));
            });
        },2000)
        /****************************end edit list**************************/

        /****************************delete list***************************/
        setTimeout(function(){
          $(".card_<?=$kunik?> .delete").off().on("click",function () {
                $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                var btnClick = $(this);
                var id = $(this).data("id");
                var type = "poi";
                var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
                
                bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
                function(result) 
                {
              if (!result) {
                btnClick.empty().html('<i class="fa fa-trash"></i>');
                return;
              } else {
                ajaxPost(
                      null,
                      urlToSend,
                      null,
                      function(data){ 
                          if ( data && data.result ) {
                          toastr.success("élément effacé");
                          $("#"+type+id).remove();
                          getPricingTable();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                      }
                  );
              }
            });
          });
        },2000)
    /****************************end delete list**************************/
        
	});


</script>