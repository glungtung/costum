<?php
$keyTpl = "oneActualite";

$paramsData = [ 
	"title"         => "Actualité",
	"icon"          => "",
	"color"         => "#005E6F"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name","emailContact","profilMediumImageUrl","email"]);
$emailContact = isset($parent["emailContact"])?$parent["emailContact"]:[];
$email = isset($parent["email"])?$parent["email"]:"cocity@gmail.com";
//var_dump($parent);
//var_dump(Yii::app()->baseUrl);
?>

<style type="text/css">
	.actus_<?= $kunik?>{
		box-shadow: 0px 0px 10px silver;
	}

	.desc_<?= $kunik?> {
		text-align: initial;
		margin-top: 10%;
		left: 9%;
	}
	.desc_<?= $kunik?> h1 {
		text-transform: none ;
		color: <?= $paramsData["color"]?>;
		font-size: 45px !important;
	}
	.desc_<?= $kunik?> h2 {
		text-transform: none;
		color: <?= $paramsData["color"]?>;
		line-height: 11px;
		font-weight: 100;
		font-size: 42px;
	}
	.desc_<?= $kunik?> a {
		text-transform: none ;
		color: #000;
		font-size: 14px;
	}
	.desc_<?= $kunik?> p {
		margin-top: 20px;
		line-height: 20px;
		font-size: 15px;

		margin-right: 30%;
	}

	.divImg_<?= $kunik?> {
		border-radius: 100%;
		display: inline-table;
		right: 4%;
	}
	.imgActu_<?= $kunik?> {
		width: 35vw;
		height: 35vw; 
		border-radius: 100%;

	}
	.actus_<?= $kunik?> .btn-plusActu{
		background: #0A96B5;
		margin-top: 3%;
		margin-bottom: 3%;
		width: 20%;
		height: 50px;
		color: #fff;
		line-height: 35px;
		font-size: 19px;
		font-weight: 800;
		text-transform: uppercase;
	}

	
	.actus_<?= $kunik?> h1{
		padding-top: 4%;
		color: <?= $paramsData["color"]?>;
		font-size: 30px;

	}
	.actus_<?= $kunik?> .btnAddActu {
		background: #8ABF32;
		margin-bottom: 3%;
		color: #fff;
		text-align: center;
		font-size: 20px;
	}
	@media (max-width: 978px) {
		.desc_<?= $kunik?> {
			text-align: initial;
			margin-top: 7%;
			left: 1%;
		}
		.desc_<?= $kunik?> h2 {
			font-size: 27px;
			text-align: center;
		}
		.desc_<?= $kunik?> p {

			font-size: 24px !important;
		}

		.actus_<?= $kunik?> h1{
			padding-top: 1%;
			font-size: 20px;
		}
		.actus_<?= $kunik?> .btn-plusActu {
			background: #0A96B5;
		    margin-top: -41%;
		    margin-left: 36%;
		    margin-bottom: 3%;
		    width: 30%;
		    height: 40px;
		    line-height: 30px;
		    font-size: 16px;
		    font-weight: 600;
		}
		.desc_<?= $kunik?> p {
			margin-right: 1%;
			text-align: center;
		}
	}
	
</style>
<div class="actus_<?= $kunik?>">	
	<div class="text-center">
		<h1 class="sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> 
        <i class="fa <?=$paramsData['icon']?>"></i> 
        <?= $paramsData["title"]?>
      </h1>
	</div>
	<div id="content-results-articles" class="carousel-inner ">    		
	</div>
	
</div>
<script type="text/javascript">
	function addActuality(){
		var dyfPoi={
			"onload" : {
				"actions" : {
					"setTitle" : "<?php echo Yii::t('cms', 'Add an article')?>",
					"html" : {
						"infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
					},
					"presetValue" : {
						"type" : "article",
					}
				}
			}
		}; 
		dyfPoi.afterSave = function(data){
          		dyFObj.commonAfterSave(data, function(){
            		mylog.log("dataaaa", data);
            		sendEmail(data);
            		urlCtrl.loadByHash(location.hash);
          });
        }  
		dyFObj.openForm('poi',null, null,null,dyfPoi);
	}
	
	function sendEmail(data){
		var msg = "<div class='container'>"+
					"<a href='http://communecter56-dev/costum/co/index/slug/"+contextSlug+"' target='_blank'> <h2> <?= $parent["name"]?> </h2> </a>  vient de publier une nouvelle article "+
					"<a href='"+baseUrl+"#page.type.poi.id."+data.id+"' target='_blank' >"+
						"<h4>"+ data.map.name +"</h4>"+
					"</a>"
				  "</div>";
		var allEmail = <?= json_encode($emailContact)?>;
		$.each(allEmail,function(k,email){
			var params={
	            tpl : "basic",
	            //tplObject : "Nouveau partenaire : "+$("#nom").val()+" "+$("#prenom").val(),
	            tplObject:"article",
	            tplMail : email,
	            fromMail: "<?= $email?>",
	            html: msg
          	};
			ajaxPost(
	            null,
	            baseUrl+"/co2/mailmanagement/createandsend",
	            params,
	            function(data){ 
	            }
	        );
		});	 
	}
	tplCtx = {};
	sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	$(document).ready(function(){
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",

				"properties" : {
					title : {
						label : "<?php echo Yii::t('cms', 'Title')?>",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
					},
					icon : { 
						label : "<?php echo Yii::t('cms', 'Icon')?>",
						inputType : "select",
						options : <?= json_encode(Cms::$icones); ?>,
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
					},
					color : {
						label : "<?php echo Yii::t('cms', 'Title color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
					},
				},

            	beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
				save : function () {  
					tplCtx.value = {};

					if (typeof idToPath != "undefined") {
						tplCtx.value["id"] = idToPath;
					}

					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value["parent"] = formData.parent ;
						}
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
		                dataHelper.path2Value( tplCtx, function(params) {
		                  	dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
		                  	});
		                });
		            }
				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";

			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
		});
		var params = {
			"source" : contextSlug,
			"limit": "1"
		};

		ajaxPost(
          	null,
          	baseUrl+"/costum/costumgenerique/getarticlescarousel",
          	params,   
          	function(data){
				mylog.log("success", data);

				var y= 0;

				var str = "<div class='item active'>";

				if(data.result == true)
				{
					var i = 0;
					var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

					$(data.elt).each(function(key,value){

						var img = (typeof value.profilMediumImageUrl  != "undefined" && value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;

						var description = typeof (value.description) != "undefined" && value.description != null ? value.description : "<?php echo Yii::t('cms', 'No description')?>";
						var shortDescription = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "<?php echo Yii::t('cms', 'No short description')?>";

						i++;

						str += "<div class='col-md-12 actu_<?= $kunik?>'>";
						str += "<div class=' col-md-6 desc_<?= $kunik?>'>";
						str += "<h2>"+value.name+"</h2>"

						str += "<p class='description'>"+shortDescription+"<a onclick='addActuality();'>";
						str+= "<i class='fa fa-plus'></i>"
						str+= "   <?php echo Yii::t('cms', 'Add a news item')?>";
						str += "</a> </p>";
						str += "<p>"+description+"</p><br>"; 
						
						if(notEmpty(value.tags)){
							value.tags.forEach(v =>
								str += "<span style='margin-top:1%;margin-left:2%;' class='pull-left text-danger'>#"+v+"</span> " );
						}
						str += "</div>"; 
						str+="<div class = ' col-md-6 text-center divImg_<?= $kunik?>'>";
						str+="<img  class = 'imgActu_<?= $kunik?>' src='"+img+"'>";
						str+="</div>";

						str+='<div class="text-center">'
						str+='<a href="javascript:;" data-hash="#article"  class="btn lbh-menu-app btn-plusActu"><?php echo Yii::t("cms", "More news")?></a>'
						str+='</div>'
						str += "</div>";
						str += "</div>";      
					});
				}
				else
				{
					str += "<div class='text-center'>"
					str += "<p >Il n'éxiste aucun article</p>";
					str+= "<a class= 'text-center btn btnAddActu' onclick='addActuality();'>";
					str+= "<i class='fa fa-plus'></i>"
					str+= "   <?php echo Yii::t('cms', 'Add a news item')?>";
					str += "</a> </div>"
				}
				$("#content-results-articles").html(str);
			}
		);
	});
</script>

