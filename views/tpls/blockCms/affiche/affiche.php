<?php  
$keyTpl = "affiche";
$paramsData=[
	"titleBlock" => "AFFICHE",
    "colorDecor" => "#0dab76",
    "ButtonColorlabel"  => "#ffffff",
	"buttonColorBorder" => "#0dab76",
	"buttonColor"       => "#0dab76",
	"imgWidth" => "300",
	"imgHeight" => "300",
	"imgRadius" => "0",
	"imgborder" => '0',
	"imgBorderColor" => "#0dab76",
	"showType" => "preview",
	"showBtn" => "showBottom",
	"typePoi" => "affiche"

];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  


?>

<style type="text/css">  
    .box-container-<?=$kunik?>:after,.box-container-<?=$kunik?>:before{content:""}
    .box-container-<?=$kunik?>{overflow:hidden}
    .box-container-<?=$kunik?> .title{letter-spacing:1px}
    /*.box-container .post{font-style:italic}*/
    .mt-30{margin-top:30px}
    .mt-40{margin-top:40px}
    .mb-30{margin-bottom:30px}


    /*********************** Demo - 7 *******************/
    .box-container-<?=$kunik?>{position:relative}
    .box-container-<?=$kunik?>:after,.box-container-<?=$kunik?>:before{width:100%;height:100%;background:rgba(11,33,47,.9);position:absolute;top:0;left:0;opacity:0;transition:all .5s ease 0s}
    .box-container-<?=$kunik?>:after{background:rgba(255,255,255,.3);border:2px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["colorDecor"]; ?>;top:0;left:170%;opacity:1;z-index:1;transform:skewX(45deg);transition:all 1s ease 0s}
	.card_<?=$kunik?> .both-box {
		margin-bottom: 30px;
    	display: inline-block;
    	float: none;
    }
    .both-box:hover .box-container-<?=$kunik?>:before{opacity:1}
    .both-box:hover .box-container-<?=$kunik?>:after{left:-170%}
    /*.box-container-<?=$kunik?> img{width:auto;height:400px;}*/
    .box-container-<?=$kunik?> img.img-responsive{
        height: <?php echo $paramsData["imgHeight"]; ?>px;
        float: inherit;
        width: 100%;
        object-fit: cover;
    }
    .box-container-<?=$kunik?> .box-content{width:100%;position:absolute;bottom:-100%;left:0;transition:all .5s ease 0s}
    .both-box:hover .box-container-<?=$kunik?> .box-content{bottom:30%}
    .box-container-<?=$kunik?> .title,.box-footer-<?=$kunik?> .title {display:block;font-weight:500;color:#fff;margin:0 0 10px}
    .box-container-<?=$kunik?> .post{display:block;font-size:15px;font-weight:500;color:#fff;margin:10px}
    .box-container-<?=$kunik?> .icon-<?=$kunik?>{margin:0}
    .box-container-<?=$kunik?> .icon-<?=$kunik?> li{display:inline-block}
    .box-container-<?=$kunik?> .icon-<?=$kunik?> li a{
    	display:block;width:35px;
    	height:35px;
    	line-height:35px;
    	border-radius:50%;
    	background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonColor"]; ?>;
    	font-size:18px;
    	color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["ButtonColorlabel"]; ?>;
    	border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["buttonColorBorder"]; ?>;
    	margin-right:10px;transition:all .5s ease 0s
    }
    .<?php echo $kunik ?> .btn-add {
        background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["buttonColor"]; ?>;
        color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["ButtonColorlabel"]; ?>;
        border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["buttonColorBorder"]; ?>;
    }
    .box-container-<?=$kunik?> .icon-<?=$kunik?> li a:hover{transform:rotate(360deg)}
    .box-container-<?=$kunik?> .title{text-transform:none}
    .box-container-<?=$kunik?>{box-shadow:0 0 3px rgba(0,0,0,.3)}
    .box-container-<?=$kunik?> .icon-<?=$kunik?>{padding:0;list-style:none}
    .box-container-<?=$kunik?>,.box-container-<?=$kunik?> .icon-<?=$kunik?> li a{text-align:center}
    .box-container-<?=$kunik?> {
    	width: <?php echo $paramsData["imgWidth"]; ?>px;
    	height: <?php echo $paramsData["imgHeight"]; ?>px;
    	border-radius: <?php echo $paramsData["imgRadius"]; ?>%;
    	margin: auto;
    	border: <?php echo $paramsData["imgborder"]; ?>px solid;
    	border-color: <?php echo $paramsData["imgBorderColor"]; ?>;
    }
    @media only screen and (max-width:990px){
        .box{margin-bottom:30px}
    }
    @media (max-width: 767px) {
    	.box-container-<?=$kunik?> {
    		max-width: 300px;
    	}
    }

    .box-footer-<?=$kunik?> {
        padding: 15px 5px 10px 5px;
        background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorDecor"]; ?>;
        text-align: center;
    }

</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 <?php echo $kunik ?>">
    <h3 class="sp-text img-text-bloc title-1" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock"><?php echo $paramsData["titleBlock"]; ?></h3>
    <div class="row mt-30 text-center card_<?=$kunik?>">


	</div>

	<div class="text-center">
		<?php if(Authorisation::isInterfaceAdmin()){ ?>
			<div class="text-center padding-top-20">
				
				<button id="add_poi_<?php echo $paramsData["typePoi"]  ?>" class="btn btn-primary btn-add ">
					<i class="fa fa-plus-circle"></i> Ajouter point d'intérêt
				</button>	
			</div>
		<?php } ?>
	</div>
</div>

<script type="text/javascript">

    	var dyfPoi_<?php echo $paramsData["typePoi"]  ?>={
    		"beforeBuild":{
		        "properties" : {
	                "shortDescription" : {
	                	"label":"Description courte",
	                	"inputType" : "text"
	                }
	                <?php if ($paramsData["showType"] == "page") { ?>
	                ,"category" : {
	                	"label":"Lien du bouton (lien vers une page statique)",
	                	"inputType" : "text"
	                }
	            	<?php } ?>
		           
		        }
		    },
			 "onload" : {
		        "actions" : {
	                "setTitle" : "Ajouter une <?php echo $paramsData["typePoi"]  ?>",
	                "html" : {
	                    "infocustom" : "<br/>Remplir le formulaire"
	                },
	                "presetValue" : {
	                    "type" : "<?php echo $paramsData["typePoi"]  ?>"
	                },
	                "hide" : {
	                    "breadcrumbcustom" : 1,
	                    "parentfinder" : 1,
	                }
	            }
		    }
		};
		dyfPoi_<?php echo $paramsData["typePoi"]  ?>.afterSave = function(data){
			dyFObj.commonAfterSave(data,function(){
				mylog.log("data", data);
				 //location.hash = "#documentation."+data.id;
				 urlCtrl.loadByHash(location.hash);

			});
		};
		
   
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {

		$("#add_poi_<?php echo $paramsData["typePoi"]  ?>").click(function(){
			dyFObj.openForm('poi',null, null,null,dyfPoi_<?php echo $paramsData["typePoi"]  ?>);
		})
		

			sectionDyf.<?php echo $kunik?>Params = {
		      "jsonSchema" : {    
		        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
		        "icon" : "fa-cog",
		        "properties" : { 
                    "colorDecor" : {
                        label : "<?php echo Yii::t('cms', 'Color of the section foot')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.colorDecor
                    },
                    "imgWidth"  : {
                        label : "<?php echo Yii::t('cms', 'Image width (in px)')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgWidth
                    },
					"imgHeight" : {
                        label : "<?php echo Yii::t('cms', 'Height of the image (in px)')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgHeight
                    },
					"imgRadius" : {
                        label : "<?php echo Yii::t('cms', 'Radius of the image border (in %)')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgRadius
                    },
                    "imgBorderColor" :{
			            label : "<?php echo Yii::t('cms', 'Color of the image border')?>",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.imgBorderColor
			         },
			         "imgborder" :{
			            label : "<?php echo Yii::t('cms', 'Thickness of the image border')?>",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.imgborder
			         },
                    "ButtonColorlabel":{
			            label : "<?php echo Yii::t('cms', 'Color of the button label')?>",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.ButtonColorlabel
			         },
			         "buttonColor":{
			            label : "<?php echo Yii::t('cms', 'Button color')?>",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColor
			         },
			         "buttonColorBorder":{
			            label : "<?php echo Yii::t('cms', 'Button border color')?>",
			            inputType : "colorpicker",
			            values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonColorBorder
			         },
			         "showType":{ 
	                  "label" : "<?php echo Yii::t('cms', 'Display type')?>",
	                  inputType : "select",
	                  options : {              
	                    "preview" : "<?php echo Yii::t('cms', 'Preview')?>",
	                    "page" : "<?php echo Yii::t('cms', 'Page')?>",
	                    "empty" : "<?php echo Yii::t('cms', 'None')?>"
	                  },
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.showType
	                },
	                "showBtn":{ 
	                  "label" : "<?php echo Yii::t('cms', 'Show button')?>",
	                  inputType : "select",
	                  options : {              
	                    "showBottom" : "<?php echo Yii::t('cms', 'Display at the bottom of the image')?>",
	                    "showOn" : "<?php echo Yii::t('cms', 'Display on the image in hover')?>"
	                  },
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.showBtn
	                },
	                "typePoi":{ 
	                  "label" : "<?php echo Yii::t('cms', 'Type of Point of Interest')?>",
	                  inputType : "select",
	                  options : poiOptions,
	                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.typePoi
	                }
				},
            	 beforeBuild : function(){
		          uploadObj.set("cms","<?php echo $blockKey ?>");
		        },
		        save : function () {  
		          tplCtx.value = {};

		          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
		            tplCtx.value[k] = $("#"+k).val();
		          });

		          mylog.log("save tplCtx",tplCtx);

		          if(typeof tplCtx.value == "undefined")
		            toastr.error('value cannot be empty!');
		          else {
		              dataHelper.path2Value( tplCtx, function(params) {
		                dyFObj.commonAfterSave(params,function(){
		                  toastr.success("Élément bien ajouté");
		                  $("#ajax-modal").modal('hide');
						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
		                  //urlCtrl.loadByHash(location.hash);
		                });
		              } );
		          }
		        }
		      }
		    };
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"img",4,6,1,null,"Propriété de l'image","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"button",4,6,null,null,"Propriété du bouton","green","");
		});


		/**************************** list**************************/  
		var isInterfaceAdmin = false
		<?php 
			if(Authorisation::isInterfaceAdmin()){ ?>
				isInterfaceAdmin = true
		 <?php } ?>
		 
        getAffiche();
        
        function getAffiche(){  
        	var params = {
	          searchType : ["poi"],
	          filters : {
	          	type : "<?php echo $paramsData["typePoi"]  ?>"
	          }
	        };
	        ajaxPost(
	          null,
	          baseUrl + "/" + moduleId + "/search/globalautocomplete",
	          params,
	          function(data){
	              console.log("blockcms poi",data);
	              var html = "";
	              $.each(data.results, function( index, value ) {
	                html += 

	                	'<div class="col-md-4 col-sm-6 both-box mb-30">'+
		                    '<div class="box-container-<?=$kunik?>">';
		                
		                         if (typeof value.profilMediumImageUrl && value.profilMediumImageUrl != null) 
		             html +=         '<img class="img-responsive" src="'+value.profilMediumImageUrl+'" alt="card image">';
		                         else
		             html +=         '<img class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/blockCmsImg/Avatar.png"> ';
		                        

		             html +=    '<div class="box-content">';
		             			<?php if ($paramsData["showBtn"] == "showOn") { ?>
	         					<?php if ($paramsData["showType"] == "preview") { ?>
			         html +=    '<a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element">'+
			                            '<h3 class="title title-2">'+value.name+'</h3>'+
			                        '</a>';
		                        <?php }elseif ($paramsData["showType"] == "page") { ?>
		             html +=        '<a href="#'+value.category+'" class="lbh">'+
			                            '<h3 class="title title-2">'+value.name+'</h3>'+
			                        '</a>';
		                        <?php }elseif ($paramsData["showType"] == "empty") { ?>
			         html +=        '<h3 class="title title-2">'+value.name+'</h3>';
		                        <?php } ?>
		                        <?php } ?>
		                        if (typeof value.shortDescription && value.shortDescription != null)
		            html +=          '<div class="post title-4">'+value.shortDescription+'</div>';
		                            if(isInterfaceAdmin == true)
		            html +=             '<ul class="icon-<?=$kunik?>">'+
		                                    '<li>'+
		                                        '<a href="javascript:;" class="edit" data-id="'+value._id.$id+'"  data-type="'+value.type+'" ><i class="fa fa-edit"></i></a>'+
		                                    '</li>'+
		                                    '<li>'+
		                                        '<a href="javascript:;" class="delete" data-id="'+value._id.$id+'" data-type="'+value.type+'" ><i class="fa fa-trash"></i></a>'+
		                                    '</li>'+
		                                '</ul>';
		             html +=                
		                        '</div>'+
		                    '</div>'+
		                   <?php if ($paramsData["showBtn"] == "showBottom") { ?>
		                    '<div class="box-footer-<?=$kunik?>">'+
		                    	<?php if ($paramsData["showType"] == "preview") { ?>
			                        '<a href="#page.type.poi.id.'+value._id.$id+'" class="lbh-preview-element">'+
			                            '<h3 class="title title-2">'+value.name+'</h3>'+
			                        '</a>'+
		                        <?php }elseif ($paramsData["showType"] == "page") { ?>
		                        	'<a href="#'+value.category+'" class="lbh">'+
			                            '<h3 class="title title-2">'+value.name+'</h3>'+
			                        '</a>'+
		                        <?php }elseif ($paramsData["showType"] == "empty") { ?>
			                            '<h3 class="title title-2">'+value.name+'</h3>'+
		                        <?php } ?>
		                    '</div>'+
		                    <?php } ?>
		                '</div>';

	              });
	              $(".card_<?=$kunik?>").html(html);
	              $(".card_<?=$kunik?> .edit").off().on('click',function(){
		            	var id = $(this).data("id");
				     	var type = $(this).data("type");
		              dyFObj.editElement('poi',id,type,dyfPoi_<?php echo $paramsData["typePoi"]  ?>);
		            });
	              $(".card_<?=$kunik?> .delete").off().on("click",function () {
		                $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
		                var btnClick = $(this);
		                var id = $(this).data("id");
		                var type = "poi";
		                var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
		                
		                bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
		                function(result) 
		                {
		              if (!result) {
		                btnClick.empty().html('<i class="fa fa-trash"></i>');
		                return;
		              } else {
		                ajaxPost(
		                      null,
		                      urlToSend,
		                      null,
		                      function(data){ 
		                          if ( data && data.result ) {
		                          toastr.success("élément effacé");
		                          $("#"+type+id).remove();
		                          getAffiche();
		                        } else {
		                           toastr.error("something went wrong!! please try again.");
		                        }
		                      }
		                  );
		              }
		            });
		          });
	              $('.post-module_<?=$kunik?>').hover(function() {
					$(this).find('.description').stop().animate({
					      height: "toggle",
					      opacity: "toggle"
					    }, 300);
					});
	              coInterface.bindLBHLinks();	
	           }
	        );
	    }
       
        
	});


</script>