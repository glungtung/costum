<?php

/***************** Required *****************/
$keyTpl     ="slide_map_scenario_configurable";
$myCmsId    = @$blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = [
    "progressColor" => "#66b00b",
    "title" => "CARTE DES TIERS LIEUX",
    "marker" => "default",
    "legendeCible" => "typePlace",
    "level" => "3",
    "countryCode" => [],
    "zones" => [],
    "legendeLabel" => "Type",
    "config" => []
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

$blockChildren  = isset($blockCms["blockChildren"]) ? $blockCms["blockChildren"] : [];

//var_dump($blockChildren);
/*************** End required ***************/
?>

<style type="text/css">
    html, body {
        overflow-x: clip;
        overflow-y: visible!important;
    }
    #graphD3{
        position: relative;
    }
    #graphformap<?= $kunik ?>{
        height: 700px;
        position: relative;
    }
    .chart-legende-container {
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999999;
        background: white;
    }
    .titleControlsContainer{
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .titleControlsContainer h3{
        margin-top: 10px;
        margin-bottom: 0;
    }
    .controls<?= $kunik?>{
        display: flex;
        justify-content: center;
        align-items: center;
        margin-left: 15px;
    }
    .controls<?= $kunik?> button.control{
        border: none;
        background: none;
        font-size: 50px;
        opacity: 0;
        transition: all ease .5s;
        color: <?= $paramsData["progressColor"] ?>;
    }
    .controls<?= $kunik?> button.enabled{
        opacity: 1;
    }
</style>

<div class="<?= $kunik?> no-padding col-xs-12">
    <div class="text-center titleControlsContainer">
        <h3 class="text-center"><?= $paramsData["title"] ?></h3>
        <div class="controls<?= $kunik?>">
            <button class="left-controls control">
                <i class="fa fa-angle-left"></i>
            </button>
            <button class="btn btn-info btn-addScenario text-center"><?= Yii::t("graph", "Add a scenario") ?></button>
            <button class="right-controls control">
                <i class="fa fa-angle-right"></i>
            </button>
        </div>
    </div>
    <div class="text-center">
        <h6><?= Yii::t("graph", "Zone") ?> : <span class="zones"></span></h6>
        <h6><?= Yii::t('graph', "Display") ?> : <span class="affichage"></span></h6>
    </div>
    <div id="graphD3">
        <div id="graphformap<?= $kunik ?>">
        
        </div>
    </div>
</div>

<script type="text/javascript">
    var mapD3<?= $kunik?>= null;
    var zonesValues = {},pays = {}, index = 0, zone = null;
    var config = <?= json_encode($paramsData["config"]) ?>;
    var typeChart = {
        "default": "<?= Yii::t("graph", "Default") ?>",
        "pie": "<?= Yii::t("graph", "Pie Chart") ?>",
        "bar": "<?= Yii::t("graph", "Bar Chart") ?>",
        "donut": "<?= Yii::t('graph', "Donut Chart") ?>",
        "geoshape": "<?= Yii::t("graph", "Geoshape") ?>",
        "heatmap": "<?= Yii::t("graph", "Heatmap") ?>"
    };
    var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "maptiler"};
    var comapOptions = {
        container : "#graphformap<?= $kunik ?>",
        activePopUp : true,
        showLegende: true,
        dynamicLegende: false,
        groupBy: 'tags',
        legendeVar: 'tags',
        mapOpt:{
            zoomControl:false
        },
        elts: {

        },
        mapCustom:customMap
    };
    $(function(){
        if(costum.editMode !== true){
            $(".btn-addScenario").hide();
        }
        if(config.length > 0){
            comapOptions.markerType = (config[index].marker == "heatmap") ? config[index].marker : "default";
            comapOptions.clusterType = (config[index].marker != "heatmap" && config[index].marker != "default") ? config[index].marker : "pie";
            comapOptions.legende = costum.lists[config[index].legendeCible];
            comapOptions.legendeLabel= config[index].legendeLabel;
        }
        (new Promise(function(resolve){
            mapD3<?= $kunik?> = new MapD3(comapOptions)
            resolve();
        })).then(function(){
            if(config.length > 0){
                getData(config[index].zones, config[index].zones[Object.keys(config[index].zones)[0]].name, config[index])
            }
        })
        $(".btn-addScenario").on('click', function(){
            tplCtx.id = "<?= $myCmsId ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params);
        })

        if(index < Object.keys(config).length - 1){
            $(".<?= $kunik?> .right-controls").addClass("enabled");
            $(".<?= $kunik?> .right-controls").prop("disabled", false);
        }
        $(".<?= $kunik?> .left-controls").off().on("click", function(){
            $(".<?= $kunik?> .right-controls").addClass("enabled");
            $(".<?= $kunik?> .right-controls").prop("disabled", false);
            if(index > 0){
                index--;
                zone = {};
                zone = config[index].zones;
                comapOptions.markerType = (config[index].marker == "heatmap") ? config[index].marker : "default";
                comapOptions.clusterType = (config[index].marker != "heatmap" && config[index].marker != "default") ? config[index].marker : "pie";
                comapOptions.legende = costum.lists[config[index].legendeCible];
                comapOptions.legendeLabel= config[index].legendeLabel;
                mapD3<?= $kunik?>= new MapD3(comapOptions);
                getData(zone, config[index].zones[Object.keys(config[index].zones)[0]]["name"],config[index]);
                if(index == 0){
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $(".<?= $kunik?> .right-controls").off().on("click", function(){
            $(".<?= $kunik?> .left-controls").addClass("enabled");
            $(".<?= $kunik?> .left-controls").prop("disabled", false);
            if(index <= Object.keys(config).length - 2){
                index++;
                zone = {};
                zone = config[index].zones;
                comapOptions.markerType = (config[index].marker == "heatmap") ? config[index].marker : "default";
                comapOptions.clusterType = (config[index].marker != "heatmap" && config[index].marker != "default") ? config[index].marker : "pie";
                comapOptions.legende = costum.lists[config[index].legendeCible];
                comapOptions.legendeLabel= config[index].legendeLabel;
                mapD3<?= $kunik?>= new MapD3(comapOptions);
                getData(zone, config[index].zones[Object.keys(config[index].zones)[0]]["name"],config[index]);
                if(index == Object.keys(config).length - 1){
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
    });
    function getData(zones, name, dataConfig) {
        var defaultFilters<?= $kunik ?> = {};
        // defaultFilters<?= $kunik ?>['$or'] = {}; 
        // defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
        // defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
        // /*if(costum.contextType!="projects"){
        //     defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
        // }*/
        // defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
        // defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
        var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
        var params = {
            // notSourceKey: false,
            searchType : <?= json_encode(['organizations']) ?>,
            fields : mapSearchFields,
            filters: defaultFilters<?= $kunik ?>,
            indexStep: 0,
            activeContour: true,
            locality: zones
        };
        params["options"] = {
            'tags' : {
                'verb': '$all'
            }
        };
        ajaxPost(
            null,
            baseUrl+'/co2/search/globalautocomplete',
            params,
            function(data){
                var dataToSend = data.results ? data.results : data;
                if(typeof data.zones != "undefined"){
                    Object.assign(dataToSend, data.zones);
                }
                mapD3<?= $kunik?>.clearMap();
                mapD3<?= $kunik?>.addElts(dataToSend);
                // mapD3<?= $kunik?>.getMap().invalidateSize()
                // mapD3<?= $kunik?>.fitBounds();
                $(".<?= $kunik ?> .zones").html(name);
                $(".<?= $kunik ?> .affichage").html(dataConfig.marker == "heatmap" ? typeChart[dataConfig.marker] : typeChart[dataConfig.marker] + " <?= Yii::t("graph", "of") ?> " + dataConfig.legendeLabel+"s");
            }, function(data){
                    mylog.log('Get zones errors', data);
            },
            null, {
                async: false,
                beforeSend: function(){
                    mapD3<?= $kunik?>.showLoader();
                }
            }
        )
    }
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?= $kunik?>Params = {
        jsonSchema: {
            "title": "<?= Yii::t("graph", "Inserting a new element") ?>",
            "description": "<?= Yii::t("graph", "Add your item to be displayed in the slide") ?>",
            "properties" : {
                "marker": {
                    "label" : "<?= Yii::t("graph", "Type of marker") ?>",
                    "class" : "<?php echo $kunik ?>",
                    "inputType" : "select",
                    "rules" : {
                        "required" : true
                    },
                    "options": typeChart,
                    "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                },
                "legendeCible":{
                    "inputType" : "select",
                    "class" : "<?php echo $kunik ?>",
                    "label" : "<?= Yii::t("graph", "Legend on") ?>",
                    "options" : Object.keys((costum.lists||{})).reduce((a,b)=> {
                        return (a[b]=b, a);
                    },{})
                },
                "legendeLabel":{
                    "label" : "<?= Yii::t("graph", "Name of the legend") ?>",
                    "inputType" : "text",
                    "class" : "<?php echo $kunik ?>",
                    "placeholder":"ex: Taille"
                },
                "countryCode":{
                    "inputType" : "select",
                    "label" : "<?= Yii::t("graph", "Country") ?>",
                    "class" : "<?php echo $kunik ?>",
                    "options" : pays
                },
                "level": {
                    "inputType" : "select",
                    "class" : "<?php echo $kunik ?>",
                    "label" : "<?= Yii::t("graph", "Zone level") ?>",
                    "options" : {
                        // "1": "Niveau 1",
                        "2": "Niveau 2",
                        "3": "Niveau 3",
                        "4": "Niveau 4",
                        "5": "Niveau 5",
                        "6": "Niveau 6"
                    }
                },
                "region": {
                    "inputType" : "select",
                    "label" : "Région",
                    "class" : "form-control <?php echo $kunik ?>",
                    "options" : []
                },
                "zones":{
                    "inputType" : "select",
                    "label" : "<?= Yii::t("graph", "Zone") ?>",
                    "class" : "<?php echo $kunik ?>",
                    "options" : zonesValues,
                    "values": sectionDyf.<?php echo $kunik?>ParamsData.zones
                }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
                ajaxPost(null,
                    baseUrl + '/co2/search/getzone/',
                    {
                        level: [1]
                    },function(data){
                        $.each(data, function(kV, vV){
                            if(!pays[vV.countryCode]){
                                pays[vV.countryCode] = vV.name;
                            }
                        })
                    }, function(data){
                        mylog.log('Get zones errors', data);
                    },
                    null, {
                    async: false
                });
            },
            afterBuild: function(){
                $(".regionselect.<?= $kunik ?>").hide();
                $("#level.<?= $kunik ?>").on("change", function(){
                    var country = $("#countryCode.<?= $kunik ?>").val();
                    var params = {};
                    if($(this).val()){
                        params['level'] = [];
                        params['level'].push($(this).val());
                    }
                    if(country && country.length > 0){
                        params['countryCode'] = country;
                    }
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        params,
                        function(data){
                            zoneDatas = data;
                            var text = "<option></option>";
                            $.each(data, function(kV, vV){
                                text += `<option value='${kV}'>${vV.name}</option>`;
                            })
                            $("select#zones.<?= $kunik ?>").html(text)
                        }, function(data){
                            mylog.log('Get zones errors', data);
                    },
                    null, {
                        async: false
                    });
                    if($(this).val() == "4"){
                        $(".regionselect").show();
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            {
                                level: [3],
                                countryCode: params['countryCode']
                            },
                            function(data){
                                var text = "<option></option>";
                                $.each(data, function(kV, vV){
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#region.<?= $kunik ?>").html(text)
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    }else{
                        $(".regionselect").hide();
                    }
                });
                $("#region.<?= $kunik ?>").on("change", function(){
                    var country = $("#countryCode.<?= $kunik ?>").val();
                    var params = {};
                    if($("#level.<?= $kunik ?>").val()){
                        params['level'] = [];
                        params['level'].push($("#level.<?= $kunik ?>").val());
                    }
                    if(country && country.length > 0){
                        params['countryCode'] = country;
                    }
                    if($(this).val()){
                        params["upperLevelId"] = $(this).val();
                    }
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        params,
                        function(data){
                            zoneDatas = data;
                            var text = "<option></option>";
                            $.each(data, function(kV, vV){
                                text += `<option value='${kV}'>${vV.name}</option>`;
                            })
                            $("select#zones.<?= $kunik ?>").html(text)
                        }, function(data){
                            mylog.log('Get zones errors', data);
                    },
                    null, {
                        async: false
                    });
                });
                $("#countryCode.<?= $kunik ?>").on("change", function(){
                    var country = $(this).val();
                    var params = {};
                    if($("#level.<?= $kunik ?>").val()){
                        params['level'] = [];
                        params['level'].push($("#level.<?= $kunik ?>").val());
                    }
                    if(country.length > 0){
                        params['countryCode'] = country;
                    }
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        params,
                        function(data){
                            zoneDatas = data;
                            var text = "<option></option>";
                            $.each(data, function(kV, vV){
                                text += `<option value='${kV}'>${vV.name}</option>`;
                            })
                            $("select#zones.<?= $kunik ?>").html(text)
                        }, function(data){
                            mylog.log('Get zones errors', data);
                    },
                    null, {
                        async: false
                    });
                });
            },
            save : function (data) {
                tplCtx.value = {};
                tplCtx.value['config'] = sectionDyf.<?php echo $kunik?>ParamsData.config;
                var value = {};
                $.each( sectionDyf.<?= $kunik?>Params.jsonSchema.properties , function(k,val) {
                    value[k] = $("#"+k).val();
                    if (k == "parent") {
                        value[k] = formData.parent;
                    }
                    if (k == "zones"){
                        value[k] = {};
                        if(typeof data.zones == "string"){
                            value[k][data.zones+'level'+zoneDatas[data.zones].level[0]] = {
                                    name: zoneDatas[data.zones].name,
                                    active: true,
                                    id: data.zones,
                                    countryCode: zoneDatas[data.zones].countryCode,
                                    level: zoneDatas[data.zones].level[0],
                                    type: "level"+zoneDatas[data.zones].level[0],
                                    key: data.zones+'level'+zoneDatas[data.zones].level[0]
                                }
                        }else if(typeof data.zones == "object"){
                            $.each(data.zones, function (kV, vV) {
                                mylog.log("ZOnes key", kV, vV);
                                value[k][vV+'level'+zoneDatas[vV].level[0]] = {
                                    name: zoneDatas[vV].name,
                                    active: true,
                                    id: vV,
                                    countryCode: zoneDatas[vV].countryCode,
                                    level: zoneDatas[vV].level[0],
                                    type: "level"+zoneDatas[vV].level[0],
                                    key: vV+'level'+zoneDatas[vV].level[0]
                                }
                            })
                        }else{
                            value[k] = "";
                        }
                    }
                });
                tplCtx.value["config"].push(value);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                    } );
                }
            }
        }
    }
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, config[index]);
        // alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"hr",4,4,null,null,"<?php echo Yii::t('cms', 'Property of the dividing line')?>","green","");
    });
</script>