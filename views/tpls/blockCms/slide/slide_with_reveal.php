<?php

/***************** Required *****************/
$keyTpl     ="slide_with_reveal";
$myCmsId    = @$blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = [
    "progressColor" => "#66b00b",
    "hrBorder" => "dashed",
    "hrWidth" => "80" ,
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

$blockChildren  = isset($blockCms["blockChildren"]) ? $blockCms["blockChildren"] : [];

//var_dump($blockChildren);
/*************** End required ***************/

$cssAnsScriptFilesModule = array(
    '/plugins/reveal/css/reveal.css',
    '/plugins/reveal/lib/css/theme/black.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);

?>

<style type="text/css">
    .<?= $kunik?>  .reveal.center, .<?= $kunik?>  .reveal.center .slides, <?= $kunik?>  .reveal.center .slides section {
        /* min-height: 100vh !important;
        height: max-content !important; */
        display: contents;
    } 
    .<?= $kunik?>  .reveal.center .slides section {
        top: 0px!important;
    }
    html, body {
        overflow-x: clip;
        overflow-y: visible!important;
    }
    @media (max-width: 767px) {
        
    }
    .<?= $kunik?> .reveal .progress {
        color: <?= $paramsData["progressColor"]?>;
    }
    .<?= $kunik?> .reveal .controls .controls-arrow:before, .<?= $kunik?> .reveal .controls .controls-arrow:after {
        background-color: <?= $paramsData["progressColor"]?>;
    }

    .reveal .controls{
        display: none;
        position: fixed !important;
        right: 100px;
        z-index: 999 !important;
    }

    .reveal .controls[data-controls-back-arrows="faded"] .navigate-left.enabled, .reveal .controls[data-controls-back-arrows="faded"] .navigate-up.enabled {
        opacity: 1 !important; 
    }

    .reveal:hover  .controls{
        display:inline !important;
        position: fixed !important;
        z-index: 999 !important;
    }

    <?php if($costum["editMode"] != "true"){ ?>
        .reveal .slides > section.stack {
            pointer-events: none;
        }
    <?php } ?>

</style>

<div class="<?= $kunik?> no-padding col-xs-12">
    <?php if(Authorisation::isInterfaceAdmin() && $costum["editMode"]=="true"){ ?>
        <div class="text-center">
            <a class="btn add-type-cms btn-default">
                <i class="fa fa-plus-circle"></i>  Ajouter un block
            </a>
            <a class="btn order-children btn-default">
                <i class="fa fa-list-ol"></i>  Changer l'ordre du slide
            </a>
        </div>
    <?php } ?>
    <div class="reveal">
        <div class="slides">
            <?php
            //$idElem = PHDB::find("cms", array("type" => "blockChild" , "blockParent" => $myCmsId ));
            $elementToSort = array();
            if (!empty($blockChildren)) {    
                $i = 1;
                foreach ($blockChildren as $key => $value) {
                    $elementToSort[$key] = isset($value["name"]) ? $value["name"] : "Slide $i";
                    $pathExplode = explode('.', $value["path"]);
                    $count = count($pathExplode);
                    $superKunik = $pathExplode[$count-1].$value["_id"];
                    $blockKey = (string)$value["_id"];
                    $cmsElement = $value["path"];       
                    $params = [
                        "blockCms"  =>  $value,
                        "kunik"     =>  $superKunik,
                        "blockKey"  =>  $blockKey,
                        "content"   =>  array(),
                        "el"        =>  $el,
                        "costum"    =>  $costum,
                        "page"      =>  $page
                    ];
                ?>
                <?php 
                    //if(strpos($cmsElement, "superCms") == false){
                        echo $this->renderPartial("costum.views.".$cmsElement,$params);
                    /*}else{
                        var_dump("C'est pas du super cms");
                        if(Authorisation::isInterfaceAdmin()){
                            echo '<div class="alert alert-warning " role="alert"><h3>Bloc non pris en charge!</h3></div>';
                            ?>                         
                            <script type="text/javascript">
                                $(".edit<?= $superKunik?>Params").remove();
                            </script>
                            
                            <?php
                        }
                    }*/
                    if(Authorisation::isInterfaceAdmin() && strpos($cmsElement, "superCms") == false){  
                        echo $this->renderPartial("costum.views.tpls.blockCms.superCms.settings", [
                            "canEdit" => true, 
                            "kunik"   => $superKunik,
                            "parentN" => $blockCms["name"]??"bloc",
                            "name"    => $value["name"]??"bloc",
                            "path"    => $value["path"],
                            "subtype" => isset($value["subtype"]) ? $value["subtype"] : "",
                            "id"      => $blockKey
                        ]);
                    }
                    $i++;
                }
            }?>
        </div>
    </div>
</div>

<?php  HtmlHelper::registerCssAndScriptsFiles(["/plugins/reveal/lib/js/head.min.js","/plugins/reveal/js/reveal.js"], Yii::app()->request->baseUrl); ?>
<script type="text/javascript">

    $(".add-type-cms").click(function(){
        // cmsConstructor["blockParent"] = "<?php echo $myCmsId ?>";

        var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
                '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
            '</div>';
        $(".<?= $kunik ?> .slides").append(strNewBlc);
        ajaxPost(
            null,
            baseUrl+"/co2/cms/insertslide",
            {   
                "page":cmsBuilder.config.page,
                "blockParent": "<?php echo $myCmsId ?>"
            }, 
            function (response) {
                // mylog.log(response);
                // Reveal.destroy();
                $(".sample-cms").replaceWith(response.html);
                cmsBuilder.block.initEvent(response.params._id.$id, response.params._id.$id);
                cmsConstructor.block.events.init();
                coInterface.initHtmlPosition();
                Reveal.sync();
                toastr.success(tradCms.elementwelladded);
            }
        );
    });
    var  selSortableObj = {
        init : function(myselect,options,config=null){
            $(myselect).select2({
                placeholder: '<?php echo Yii::t("cms", "Select and order your menu")?>'
            }).on("select2:select", function (evt) {
                var id = evt.params.data.id;
                var element = $(this).children("option[value="+id+"]");
                selSortableObj.moveElementToEndOfParent(element);
                $(this).trigger("change");
            });
            var ele=$(myselect).parent().find("ul.select2-choices");
            ele.sortable({
                containment: 'parent',
                update: function() {
                    selSortableObj.orderSortedValues(myselect);
                }
            });
        },
    
        orderSortedValues : function(myselect) {
            var value = ''
            $(myselect).parent().find("ul.select2-choices").children("li").children("div").each(function(i, obj){
                var element = $(myselect).children('option').filter(function () { 
                    return $(this).html() == $(obj).text() 
                });
                selSortableObj.moveElementToEndOfParent(element)
            });
        },
        moveElementToEndOfParent : function(element) {
            var parent = element.parent();
            element.detach();
            parent.append(element);
        }    
    };
    var elementSortable = <?= json_encode($elementToSort) ?>;
    $(".order-children").click(function(){
        var activeForm = {
            "jsonSchema" : {
                "title" : "Configurer les ordres du slide",
                "type" : "object",
                onLoads : {
                    onload : function(data){
                        $(".parentfinder").css("display","none");
                        selSortableObj.init("#element",elementSortable);

                        
                    }
                },
                "properties" : {
                    "element" : {
                        "inputType" : "selectMultiple",
                        "isSelect2" : true,
                        "noOrder" :true,
                        "label" : "Ordre du slide(glisseret déposer pour organiser)",
                        "class" : "form-control",
                        "placeholder" : "Ordre du slide",
                        "value" : Object.keys(elementSortable),
                        options : elementSortable,
                    }
                },
                save : function (data) {  
                    var value = [];
                    $.each( activeForm.jsonSchema.properties , function(k,val) {
                        value = $("#"+k).val();
                    });
                    if(typeof value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        ajaxPost(null, 
                            baseUrl+'/co2/cms/updateorderreveal',
                            {
                                elements: value
                            },
                            function(response){
                                toastr.success("<?php echo Yii::t('cms', 'Order successfully changed')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            }
                        )
                    }
                }

            }
        };
        dyFObj.openForm( activeForm );
    });
    Reveal.initialize({
        embedded: true,
        disableLayout: false,
        progress: false
    });
    
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        $(".<?= $kunik ?> .navigate-right").click(function(){
            $(".<?= $kunik ?>").get(0).scrollIntoView({behavior: 'smooth'})
        });
        $(".<?= $kunik ?> .navigate-left").click(function(){
            $(".<?= $kunik ?>").get(0).scrollIntoView({behavior: 'smooth'})
        });
        $(".<?= $kunik ?> .navigate-up").click(function(){
            $(".<?= $kunik ?>").get(0).scrollIntoView({behavior: 'smooth'})
        });
        $(".<?= $kunik ?> .navigate-down").click(function(){
            $(".<?= $kunik ?>").get(0).scrollIntoView({behavior: 'smooth'})
        });
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "progressColor" : {
                        "inputType" : "colorpicker",
                        label : "<?php echo Yii::t('cms', 'Color')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.progressColor
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        mylog.log("sectiondyfff",sectionDyf);
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"hr",4,4,null,null,"<?php echo Yii::t('cms', 'Property of the dividing line')?>","green","");
        });
    });
</script>