<?php

/***************** Required *****************/
$keyTpl     ="slide_map_scenario";
$myCmsId    = @$blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = [
    "progressColor" => "#66b00b",
    "hrBorder" => "dashed",
    "hrWidth" => "80",
    "title" => "CARTE DES TIERS LIEUX",
    "marker" => "default",
    "cluster" => "pie",
    "legendeCible" => "typePlace",
    "level" => "3",
    "countryCode" => [],
    "zones" => [],
    "legendeLabel" => "Type"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

$blockChildren  = isset($blockCms["blockChildren"]) ? $blockCms["blockChildren"] : [];

//var_dump($blockChildren);
/*************** End required ***************/
?>

<style type="text/css">
    html, body {
        overflow-x: clip;
        overflow-y: visible!important;
    }
    #graphD3{
        position: relative;
    }
    #graphformap<?= $kunik ?>{
        height: 700px;
        position: relative;
    }
    .chart-legende-container {
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999999;
        background: white;
    }
    .controls<?= $kunik?>{
        position: absolute;
        top: 10px;
        right: 20px;
    }
    .controls<?= $kunik?> button{
        border: none;
        background: none;
        font-size: 50px;
        opacity: 0;
        transition: all ease .5s;
        color: <?= $paramsData["progressColor"] ?>;
    }
    .controls<?= $kunik?> button.enabled{
        opacity: 1;
    }
</style>

<div class="<?= $kunik?> no-padding col-xs-12">
    <h3 class="text-center"><?= $paramsData["title"] ?></h3>
    <div class="text-center">
        <h6><?= Yii::t("graph", "Zone") ?> :<span class="zones"></span></h6>
        <h6><?= Yii::t('graph', "Display") ?> : <span class="affichage"><?= ($paramsData["marker"] == "heatmap" ? $paramsData["marker"] : $paramsData["cluster"] ) ?></span></h6>
    </div>
    <div id="graphD3">
        <div id="graphformap<?= $kunik ?>">
        
        </div>
    </div>
    <div class="controls<?= $kunik?>">
        <button class="left-controls">
            <i class="fa fa-angle-left"></i>
        </button>
        <button class="right-controls">
            <i class="fa fa-angle-right"></i>
        </button>
    </div>
</div>

<?php  HtmlHelper::registerCssAndScriptsFiles(["/plugins/reveal/lib/js/head.min.js","/plugins/reveal/js/reveal.js"], Yii::app()->request->baseUrl); ?>
<script type="text/javascript">
    var appMap<?= $kunik ?> = null;
    var zones = <?php echo json_encode( $paramsData['zones'] ); ?>,paysData = <?php echo json_encode( $paramsData['countryCode'] ); ?>,levelData = <?php echo json_encode( $paramsData['level'] ); ?>, zonesValues = {},zonesValues = {},pays = {}, index = 0;
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $kunik?>ParamsData.zones = Object.values(sectionDyf.<?php echo $kunik?>ParamsData.zones).map((value) => {
        return value.id;
    });
    function getData(zones, name) {
        var defaultFilters<?= $kunik ?> = {};
        // defaultFilters<?= $kunik ?>['$or'] = {}; 
        // defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
        // defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
        // /*if(costum.contextType!="projects"){
        //     defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
        // }*/
        // defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
        // defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
        var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
        var params = {
            // notSourceKey: false,
            searchType : <?= json_encode(['organizations']) ?>,
            fields : mapSearchFields,
            filters: defaultFilters<?= $kunik ?>,
            indexStep: 0,
            activeContour: true,
            locality: zones
        };
        params["options"] = {
            'tags' : {
                'verb': '$all'
            }
        };
        ajaxPost(
            null,
            baseUrl+'/co2/search/globalautocomplete',
            params,
            function(data){
                var dataToSend = data.results ? data.results : data;
                if(typeof data.zones != "undefined"){
                    Object.assign(dataToSend, data.zones);
                }
                appMap<?= $kunik ?>.clearMap();
                appMap<?= $kunik ?>.addElts(dataToSend);
                // appMap<?= $kunik ?>.getMap().invalidateSize()
                // appMap<?= $kunik ?>.fitBounds();
                $(".<?= $kunik ?> .zones").html(name)
            }
        )
    }
    jQuery(document).ready(function() {
        var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "maptiler"};
        var mapOptions = {
            container : "#graphformap<?= $kunik ?>",
            activePopUp : true,
            clusterType : '<?= $paramsData["cluster"] ?>',
            markerType : '<?= $paramsData["marker"] ?>',
            showLegende: true,
            dynamicLegende: false,
            groupBy: 'tags',
            legende: costum.lists['<?= $paramsData["legendeCible"] ?>'],
            legendeLabel: '<?= $paramsData["legendeLabel"] ?>',
            legendeVar: 'tags',
            mapOpt:{
                zoomControl:false
            },
            mapCustom:customMap,
            elts : {}
        };
        (new Promise(function(resolve){
            appMap<?= $kunik ?> = new MapD3(mapOptions);
            resolve();
        })).then(function(){
            if(Object.keys(zones).length > 0){
                var zone = {};
                zone[Object.keys(zones)[index]] = zones[Object.keys(zones)[index]];
                getData(zone, zones[Object.keys(zones)[index]]["name"]);
            }
        })
        if(index <= Object.keys(zones).length - 1){
            $(".<?= $kunik?> .right-controls").addClass("enabled");
            $(".<?= $kunik?> .right-controls").prop("disabled", false);
        }
        $(".<?= $kunik?> .left-controls").off().on("click", function(){
            $(".<?= $kunik?> .right-controls").addClass("enabled");
            $(".<?= $kunik?> .right-controls").prop("disabled", false);
            if(index > 0){
                index--;
                zone = {};
                zone[Object.keys(zones)[index]] = zones[Object.keys(zones)[index]];
                getData(zone, zones[Object.keys(zones)[index]]["name"]);
                if(index == 0){
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $(".<?= $kunik?> .right-controls").off().on("click", function(){
            $(".<?= $kunik?> .left-controls").addClass("enabled");
            $(".<?= $kunik?> .left-controls").prop("disabled", false);
            if(index <= Object.keys(zones).length - 2){
                index++;
                zone = {};
                zone[Object.keys(zones)[index]] = zones[Object.keys(zones)[index]];
                getData(zone, zones[Object.keys(zones)[index]]["name"]);
                if(index == Object.keys(zones).length - 1){
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        var cible = Object.keys((costum.lists||{})).reduce((a,b)=> {
            return (a[b]=b, a);
        },{});
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "title": {
                        "label" : "<?= Yii::t('cms', 'Title') ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "text",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"default": "Par défaut","pie": "Pie Chart", "bar": "Bar Chart", "donut": "Donut Chart", "heatmap": "Carte de chaleur"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "marker": {
                        "label" : "<?= Yii::t("graph", "Type of marker") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"default": "Par défaut","pie": "Pie Chart", "bar": "Bar Chart", "donut": "Donut Chart", "heatmap": "Carte de chaleur"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "cluster": {
                        "label" : "Type de cluster",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"pie": "Pie Chart", "bar": "Bar Chart", "donut": "Donut Chart"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "legendeCible":{
                        "inputType" : "select",
                        "label" : "<?= Yii::t("graph", "Legend on") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "options" : cible
                    },
                    "legendeLabel":{
                        "label" : "<?= Yii::t("graph", "Name of the legend") ?>",
                        "inputType" : "text",
                        "placeholder":"ex: Taille",
                        "class" : "form-control <?php echo $kunik ?>",
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendeLabel
                    },
                    "countryCode":{
                        "inputType" : "selectMultiple",
                        "label" : "<?= Yii::t("graph", "Country") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "options" : pays
                    },
                    "level": {
                        "inputType" : "select",
                        "label" : "<?= Yii::t("graph", "Zone level") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "options" : {
                            // "1": "Niveau 1",
                            "2": "Niveau 2",
                            "3": "Niveau 3",
                            "4": "Niveau 4",
                            "5": "Niveau 5"
                            // "6": "Niveau 6"
                        }
                    },
                    "region": {
                        "inputType" : "select",
                        "label" : "Région",
                        "class" : "form-control <?php echo $kunik ?>",
                        "options" : []
                    },
                    "zones":{
                        "inputType" : "selectMultiple",
                        "label" : "<?= Yii::t("graph", "Zone") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "options" : zonesValues,
                        "values": sectionDyf.<?php echo $kunik?>ParamsData.zones
                    },
                    "progressColor" : {
                        "inputType" : "colorpicker",
                        "class" : "form-control <?php echo $kunik ?>",
                        label : "<?php echo Yii::t('cms', 'Color')?>",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.progressColor
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        {
                            level: [1]
                        },function(data){
                            $.each(data, function(kV, vV){
                                if(!pays[vV.countryCode]){
                                    pays[vV.countryCode] = vV.name;
                                }
                            })
                        }, function(data){
                            mylog.log('Get zones errors', data);
                        },
                        null, {
                        async: false
                    });
                    
                    if(paysData.length > 0){
                        mylog.log("Call zone Data", paysData);
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            {
                                level: [levelData],
                                countryCode: paysData
                            },
                            function(data){
                                zoneDatas = data;
                                $.each(data, function(kV, vV){
                                    if(!zonesValues[kV])
                                        zonesValues[kV] = vV.name;
                                })
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    }
                },
                afterBuild: function(){
                    $(".regionselect.<?= $kunik ?>").hide();
                    $("#countryCode.<?= $kunik ?>").on("change", function(){
                        var country = $(this).val();
                        country = country.filter(function (el) {
                            return el != null && el != '';
                        });
                        var params = {};
                        if($("#level.<?= $kunik ?>").val()){
                            params['level'] = [];
                            params['level'].push($("#level.<?= $kunik ?>").val());
                        }
                        if(country.length > 0){
                            params['countryCode'] = country;
                        }
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            params,
                            function(data){
                                zoneDatas = data;
                                var text = "<option></option>";
                                $.each(data, function(kV, vV){
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#zones.<?= $kunik ?>").html(text)
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    });
                    $("#level.<?= $kunik ?>").on("change", function(){
                        var country = $("#countryCode.<?= $kunik ?>").val();
                        var params = {};
                        if($(this).val()){
                            params['level'] = [];
                            params['level'].push($(this).val());
                        }
                        if(country && country.length > 0){
                            params['countryCode'] = country;
                        }
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            params,
                            function(data){
                                zoneDatas = data;
                                var text = "<option></option>";
                                $.each(data, function(kV, vV){
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#zones.<?= $kunik ?>").html(text)
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                        if($(this).val() == "4"){
                            $(".regionselect").show();
                            ajaxPost(null,
                                baseUrl + '/co2/search/getzone/',
                                {
                                    level: [3],
                                    countryCode: params['countryCode']
                                },
                                function(data){
                                    var text = "<option></option>";
                                    $.each(data, function(kV, vV){
                                        text += `<option value='${kV}'>${vV.name}</option>`;
                                    })
                                    $("select#region.<?= $kunik ?>").html(text)
                                }, function(data){
                                    mylog.log('Get zones errors', data);
                            },
                            null, {
                                async: false
                            });
                        }else{
                            $(".regionselect").hide();
                        }
                    });
                    $("#region.<?= $kunik ?>").on("change", function(){
                        var country = $("#countryCode.<?= $kunik ?>").val();
                        var params = {};
                        if($("#level.<?= $kunik ?>").val()){
                            params['level'] = [];
                            params['level'].push($("#level.<?= $kunik ?>").val());
                        }
                        if(country && country.length > 0){
                            params['countryCode'] = country;
                        }
                        if($(this).val()){
                            params["upperLevelId"] = $(this).val();
                        }
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            params,
                            function(data){
                                zoneDatas = data;
                                var text = "<option></option>";
                                $.each(data, function(kV, vV){
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#zones.<?= $kunik ?>").html(text)
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    });
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                        if (k == "zones"){
                            tplCtx.value[k] = {};
                            if(typeof data.zones == "string"){
                                tplCtx.value[k][data.zones+'level'+data.level] = {
                                        name: zoneDatas[data.zones].name,
                                        active: true,
                                        id: data.zones,
                                        countryCode: zoneDatas[data.zones].countryCode,
                                        level: zoneDatas[data.zones].level[0],
                                        type: "level"+data.level,
                                        key: data.zones+'level'+data.level
                                    }
                            }else if(typeof data.zones == "object"){
                                $.each(data.zones, function (kV, vV) {
                                    mylog.log("ZOnes key", kV, vV);
                                    tplCtx.value[k][vV+'level'+data.level] = {
                                        name: zoneDatas[vV].name,
                                        active: true,
                                        id: vV,
                                        countryCode: zoneDatas[vV].countryCode,
                                        level: zoneDatas[vV].level[0],
                                        type: "level"+data.level,
                                        key: vV+'level'+data.level
                                    }
                                })
                            }else{
                                tplCtx.value[k] = "";
                            }
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"hr",4,4,null,null,"<?php echo Yii::t('cms', 'Property of the dividing line')?>","green","");
        });
    });
</script>