<?php

/***************** Required *****************/
$keyTpl     = "slide_map_scenario_for_zone";
$myCmsId    = @$blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = [
    "progressColor" => "#66b00b",
    "title" => "CARTE DES TIERS LIEUX",
    "level" => "3",
    "type" => ["pie", "bar", "heatmap"],
    "typeGraph" => [],
    "legende" => [],
    "countryCode" => [],
    "zones" => []
];
$typeChart = array(
    "default" => Yii::t("graph", "Default"),
    "pie" => Yii::t("graph", "Pie Chart"),
    "bar" => Yii::t("graph", "Bar Chart"),
    "donut" => Yii::t('graph', "Donut Chart"),
    "geoshape" => Yii::t("graph", "Geoshape"),
    "heatmap" => Yii::t("graph", "Heatmap")
);
$typeGraph = array(
    "circlerelation" => Yii::t("graph", "Circle Relation"),
    "mindmap" => Yii::t("graph", "Mindmap"),
    "circularbarplot" => Yii::t("graph", "Circular Barplot")
);
$iconsmapPng = array(
    "pie" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/pie.png",
    "bar" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/bar.png",
    "geoshape" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/geoshape.png",
    "heatmap" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/heatmap.png",
    "donut" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/donut.png",
);
$iconsGraphPng = array(
    "mindmap" => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/icons/mindmap.png",
    "circlerelation" => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/icons/circlepacking.png",
    "circularbarplot" => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/icons/circularbarplot.png",
);
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}


$blockChildren  = isset($blockCms["blockChildren"]) ? $blockCms["blockChildren"] : [];
$graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
//var_dump($blockChildren);
/*************** End required ***************/
?>

<style type="text/css">
    html,
    body {
        overflow-x: clip;
        overflow-y: visible !important;
    }

    #graphD3 {
        position: relative;
    }

    #graphformap<?= $kunik ?> {
        height: 700px;
        position: relative;
    }

    .chart-legende-container {
        /* position: absolute;
        right: 0;
        top: 0; */
        z-index: 9999999;
        background: white;
        /* display: none; */
    }

    .titleControlsContainer {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .titleControlsContainer h3 {
        margin-top: 10px;
        margin-bottom: 0;
    }

    .controls<?= $kunik ?> {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-left: 15px;
    }

    .controls<?= $kunik ?> button.control {
        border: none;
        background: none;
        font-size: 50px;
        opacity: 0;
        transition: all ease .5s;
        color: <?= $paramsData["progressColor"] ?>;
    }

    .controls<?= $kunik ?> button.enabled {
        opacity: 1;
    }

    select#typeChart<?= $kunik ?> {
        font-family: 'FontAwesome', "sans-serif";
        width: 300px;
        margin: 10px auto 20px;
    }

    .img-icon {
        width: 25px;
    }

    .btn-type {
        display: flex;
        /* flex-direction: column; */
        align-items: center;
        justify-content: center;
        order: -1
    }
    .graph-icons-container {
        display: flex;
        /* flex-direction: column; */
        align-items: center;
        justify-content: center;
    }

    .btn-type button {
        margin: 5px;
        background-color: white;
        box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
        box-sizing: border-box;
        color: #1A202C;
        border-radius: 8px;
        cursor: pointer;
    }

    .btn-type button.type-active {
        background-color: #8f8f8f;
        box-shadow: rgba(0, 0, 0, .15) 0 3px 9px 0;
        transform: translateY(-2px);
    }
    .graph-icons-container button {
        margin: 5px;
        background-color: white;
        box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
        box-sizing: border-box;
        color: #1A202C;
        border-radius: 8px;
        cursor: pointer;
    }

    .graph-icons-container button.type-active {
        background-color: #8f8f8f;
        box-shadow: rgba(0, 0, 0, .15) 0 3px 9px 0;
        transform: translateY(-2px);
    }

    .leaflet-top.leaflet-right {
        display: flex;
        flex-direction: column;
        background-color: white;
    }
    #graphformap<?= $kunik ?> g.divide>circle {
        fill: transparent !important;
    }
    #graphformap<?= $kunik ?> #mobile-section {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        z-index: 999;
        padding: 4%;
        overflow-y: scroll;
    }
    .switches-container<?= $kunik ?> {
        width: 20%;
        position: relative;
        display: flex;
        padding: 0;
        position: relative;
        background: #a5c145;
        line-height: 3rem;
        border-radius: 3rem;
        margin-left: auto;
    }

    /* input (radio) for toggling. hidden - use labels for clicking on */
    .switches-container<?= $kunik ?> input {
        visibility: hidden;
        position: absolute;
        top: 0;
    }

    /* labels for the input (radio) boxes - something to click on */
    .switches-container<?= $kunik ?> label {
        width: 50%;
        padding: 10px;
        margin: 0;
        text-align: center;
        cursor: pointer;
        color: white;
    }

    /* switch highlighters wrapper (sliding left / right) 
        - need wrapper to enable the even margins around the highlight box
    */
    .switch-wrapper<?= $kunik ?> {
        position: absolute;
        top: 0;
        bottom: 0;
        width: 50%;
        padding: 0.15rem;
        z-index: 3;
        transition: transform .5s cubic-bezier(.77, 0, .175, 1);
        /* transition: transform 1s; */
    }

    /* switch box highlighter */
    .switch<?= $kunik ?> {
        border-radius: 3rem;
        background: white;
        height: 100%;
    }

    /* switch box labels
        - default setup
        - toggle afterwards based on radio:checked status 
    */
    .switch<?= $kunik ?> div {
        width: 100%;
        text-align: center;
        opacity: 0;
        padding: 10px;
        display: block;
        color: #a5c145 ;
        transition: opacity .2s cubic-bezier(.77, 0, .175, 1) .125s;
        will-change: opacity;
        position: absolute;
        top: 0;
        left: 0;
    }

    /* slide the switch box from right to left */
    .switches-container<?= $kunik ?> input:nth-of-type(1):checked~.switch-wrapper<?= $kunik ?> {
        transform: translateX(0%);
    }

    /* slide the switch box from left to right */
    .switches-container<?= $kunik ?> input:nth-of-type(2):checked~.switch-wrapper<?= $kunik ?> {
        transform: translateX(100%);
    }

    /* toggle the switch box labels - first checkbox:checked - show first switch div */
    .switches-container<?= $kunik ?> input:nth-of-type(1):checked~.switch-wrapper<?= $kunik ?> .switch<?= $kunik ?> div:nth-of-type(1) {
        opacity: 1;
    }

    /* toggle the switch box labels - second checkbox:checked - show second switch div */
    .switches-container<?= $kunik ?> input:nth-of-type(2):checked~.switch-wrapper<?= $kunik ?> .switch<?= $kunik ?> div:nth-of-type(2) {
        opacity: 1;
    }
</style>
<div class="<?= $kunik ?> no-padding col-xs-12">
    <div class="text-center titleControlsContainer">
        <h3 class="text-center"><?= $paramsData["title"] ?></h3>
        <div class="controls<?= $kunik ?>">
            <button class="left-controls control" disabled="true">
                <i class="fa fa-angle-left"></i>
            </button>
            <button class="right-controls control" disabled="true">
                <i class="fa fa-angle-right"></i>
            </button>
        </div>
    </div>
    <div class="text-center">
        <h6><?= Yii::t("graph", "Zone") ?> : <span class="zones"></span></h6>
        <h6><?= Yii::t('graph', "Display") ?> : <span class="affichage"></span></h6>
    </div>
    <div id="graphD3">
        <?php if(count($paramsData["type"]) > 0 && count($paramsData["typeGraph"]) > 0){ ?>
            <div class="graph-button">
                <div class="switches-container<?= $kunik ?>">
                    <input type="radio" id="switchMap" name="switchType" value="map" checked="checked" />
                    <input type="radio" id="switchGraph" name="switchType" value="graph" />
                    <label for="switchMap">Carte</label>
                    <label for="switchGraph">Graphique</label>
                    <div class="switch-wrapper<?= $kunik ?>">
                        <div class="switch<?= $kunik ?>">
                            <div>Carte</div>
                            <div>Graphique</div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if(count($paramsData['typeGraph']) > 0) { ?>
            <div class="graph-icons-container">

            </div>
        <?php } ?>
        <div id="graphformap<?= $kunik ?>">

        </div>
    </div>
</div>

<script type="text/javascript">
    var mapD3<?= $kunik ?> = null;
    var iconmapPng = <?= json_encode($iconsmapPng) ?>;
    var iconGraphPng = <?= json_encode($iconsGraphPng) ?>;
    var dataConfig = <?= json_encode($paramsData) ?>;
    var zonesValues = {},
        pays = {},
        index = 0,
        zone = null;
    var typeChart = <?= json_encode($typeChart) ?>;
    var typeGraph = <?= json_encode($typeGraph) ?>;
    var customMap = (typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {
        tile: "maptiler"
    };
    var mapSelected = 'pie';
    var graphSelected = 'circlerelation';
    var getDataActive = dataConfig.type.length > 0 && dataConfig.typeGraph.length > 0 ? "map" : (dataConfig.typeGraph.length > 0 ? "graph" : "map") ;
    var comapOptions = {
        container: "#graphformap<?= $kunik ?>",
        activePopUp: true,
        showLegende: true,
        dynamicLegende: false,
        groupBy: 'tags',
        legendeVar: 'tags',
        mapOpt: {
            zoomControl: false
        },
        mapCustom: customMap,
        elts: {

        }
    };
    var graph<?= $kunik ?> = null;

    function changeelement(type) {
        window[getDataActive+'Selected'] = type;
        getData(dataConfig.zones, dataConfig.zones[Object.keys(dataConfig.zones)[0]]["name"])
        
    }

    function addIcon() {
        var button = L.control({
            position: 'topright'
        });
        button.onAdd = function(map) {
            var div = L.DomUtil.create('div', 'info button btn-type');
            var str = "";
            dataConfig.type.forEach((element) => {
                str += `<button class='btn btn-chart-type ${element == mapSelected ? "type-active" : ''}' title='${typeChart[element]}' onclick="changeelement('${element}')">
                    <img class='img-icon' src="${baseUrl+iconmapPng[element]}" ?>
                </button>`;
            });
            div.innerHTML = str;
            return div;
        }
        button.addTo(mapD3<?= $kunik ?>.getMap());
    }
    function addIconGraph() {
        var str = "";
        if($(".graph-icons-container").length == 0){
            $("#").append('<div class="graph-icons-container"></div>');
        }
        var $div = $(".graph-icons-container");
        dataConfig.typeGraph.forEach((element) => {
            str += `<button class='btn btn-chart-type ${element == graphSelected ? "type-active" : ''}' title='${typeGraph[element]}' onclick="changeelement('${element}')">
                <img class='img-icon' src="${baseUrl+iconGraphPng[element]}" ?>
            </button>`;
        });
        $div.html(str);
    }

    function initMap(legende, data) {
        if (Object.keys(legende).length > 0) {
            comapOptions.legende = costum.lists[legende.legendeCible];
            comapOptions.legendeLabel = legende.legendeLabel;
        } else {
            comapOptions.dynamicLegende = true;
            comapOptions.groupBy = 'type';
            comapOptions.legendeVar = 'type';
        }
        comapOptions.markerType = (mapSelected == "heatmap" || mapSelected == "geoshape") ? mapSelected : "default";
        comapOptions.clusterType = (mapSelected != "heatmap" && mapSelected != "default" && mapSelected != "geoshape") ? mapSelected : "pie";

        var dataToSend = data.results ? data.results : data;
        if(typeof data.zones != "undefined"){
            Object.assign(dataToSend, data.zones);
        }
        (new Promise(function(resolve) {
            mapD3<?= $kunik ?> = new MapD3(comapOptions)
            resolve();
        })).then(function() {
            mapD3<?= $kunik?>.clearMap();
            mapD3<?= $kunik?>.addElts(dataToSend);
            addIcon();
            $(".<?= $kunik ?> .affichage").html((mapSelected == "heatmap" || mapSelected == "geoshape") ? typeChart[mapSelected] : typeChart[mapSelected] + " de " + (dataConfig.legende.length > 0 ? dataConfig.legende[index].legendeLabel : "Type"));
        })
    }
    function countTags(tags, results){
        var counts = {};
        for (const [key, value] of Object.entries(results)) {
            for (const [index, tag] of Object.entries(costum.lists[tags])) {
                if(value.tags.includes(tag)){
                    counts[tag] = counts[tag] || 0;
                    counts[tag] += 1;
                }
            }
        }
        return counts;
    }
    var activeCircle<?= $kunik ?> = "";

    function processMindmap(tags, data) {
        let structuredData = {
            id:"root<?= $kunik ?>",
            label:'Tiers-lieux',
            children:[]
        }
        for (var i in costum.lists[tags]) {
            let child = {
                id:"element"+i,
                label:costum.lists[tags][i],
                children: []
            }
            Object.keys(data).forEach((key, index) => {
                let element = data[key];
                let itIsIn = false;
               if(element.tags.includes(costum.lists[tags][i])){
                    child.children.push({
                        id:key,
                        label:element.name,
                        collection: element.collection
                    });
                }
            });
            structuredData["children"].push(child)
        }
        return structuredData;
    }

    function initmindmap(tags, data) {
        var dataToSend = data.results ? data.results : data;
        (new Promise(function(resolve){
            graph<?= $kunik ?> = new MindmapGraph([], 1, {id: "root", label: "Tiers-lieux"})
            resolve();
        })).then(function(){
            var tooltip = new GraphTooltip("#graphformap<?= $kunik ?>");
            tooltip.hide();
            window.onresize = (e) => {
                tooltip.goToNode();
            }
            // graph<?= $kunik ?>.setTheme(tags);
            graph<?= $kunik ?>.draw("#graphformap<?= $kunik ?>");
            graph<?= $kunik ?>.updateData(processMindmap(tags, dataToSend), true)
        })
    }

    function initcircularbarplot(tags, data) {
        var dataToSend = data.results ? data.results : data;
        (new Promise(function(resolve){
            graph<?= $kunik ?> = new CircularBarplotGraph()
            resolve();
        })).then(function(){
            // var tooltip = new GraphTooltip("#graphformap<?= $kunik ?>");
            // tooltip.hide();
            // window.onresize = (e) => {
            //     tooltip.goToNode();
            // }
            $("#graphformap<?= $kunik ?>").empty();
            graph<?= $kunik ?>.setAuthorizedTags(costum.lists[tags]);
            graph<?= $kunik ?>.setMax(40);
            graph<?= $kunik ?>.draw("#graphformap<?= $kunik ?>");
            graph<?= $kunik ?>.updateData(graph<?= $kunik ?>.preprocessResults(dataToSend), true)
        })
    }

    function initcirclerelation(tags, data) {
        if(!window.initedGraph){
            window.initedGraph = [];
        }
        if($('div#circle-filter-container').length == 0){
            $("#graphformap<?= $kunik ?>").before('<div id="circle-filter-container" class="margin-top-10 text-center"></div>');
        }
        $("div#circle-filter-container").empty();
        var dataToSend = data.results ? data.results : data;
        var authorizedTags = costum.lists[tags];
        var color = d3.scaleOrdinal()
                .domain(authorizedTags)
                .range(d3.schemeTableau10);
        // authorizedTags.push("Autre");
        (new Promise(function(resolve){
            graph<?= $kunik ?> = new CircleRelationGraph([], d => d.group, authorizedTags, [])
            resolve();
        })).then(function(){
            var tooltip = new GraphTooltip("#graphformap<?= $kunik ?>");
            tooltip.hide();
            window.onresize = (e) => {
                tooltip.goToNode();
            }
            graph<?= $kunik ?>.draw("#graphformap<?= $kunik ?>");
            graph<?= $kunik ?>.setOnClickNode((e,d,n) => {
                tooltip.node = d3.select(e.currentTarget)
                tooltip.setContent(d)
                tooltip.show()
            })
            graph<?= $kunik ?>.setBeforeDrag(() => {
                graph<?= $kunik ?>.setDraggable(costum.editMode);
            });
            graph<?= $kunik ?>.setOnZoom((e,d,n) => {
                tooltip.goToNode();
            })
            var counts = countTags(tags,dataToSend);
            d3.select("div#circle-filter-container")
            .selectAll("button")
            .data(authorizedTags)
            .join((enter) => {
                enter.append("xhtml:button")
                    .text(d => d)
                    .classed("btn margin-right-5 btn-circle-zoomer", true)
                    .on("click", (e,d) => {
                    var thisElement = $(e.target);
                    if(activeCircle<?= $kunik ?> != d){
                        graph<?= $kunik ?>.focus(d);
                        activeCircle<?= $kunik ?> = d;
                        $(".btn-circle-zoomer").removeClass("btn-primary");
                        thisElement.addClass("btn-primary");
                        //$("#"+GraphUtils.slugify(d)+" .list-group-item").show();
                    }else{
                        activeCircle<?= $kunik ?> = "";
                        thisElement.removeClass("btn-primary");
                        graph<?= $kunik ?>.unfocus().then(() => {
                        mylog.log("UNFOCUSED");
                        });
                    }
                    
                    $("*[href='#"+GraphUtils.slugify(d)+"']").addClass("collapsed");
                    $("*[href='#"+GraphUtils.slugify(d)+"'] > i.fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    $("#"+GraphUtils.slugify(d)).addClass("in");
                })
                .append("xhtml:span")
                .classed("badge badge-theme-count margin-left-5", true)
                .attr("data-countvalue", d => d)
                .attr("data-countkey", d => d)
                .attr("data-countlock", "false")
                .html(d => counts[d]);
            });
            
                // p<?= $kunik ?>.filters.actions.themes.setThemesCounts(p<?= $kunik ?>);
            graph<?= $kunik ?>._circlePadding = 20;
            graph<?= $kunik ?>._externalCircleMargin = 30;
            graph<?= $kunik ?>._color = (da, index) => {
                /*p<?= $kunik ?>.graph.graph.rootG.selectAll(".divide circle:last-child")
                .filter(function (d,i) { return i === index;})
                .style("fill-opacity", 0.8)*/

                //   $(".nodes-container div").css({"color": "white"});
                return color(index);
            };
            graph<?= $kunik ?>.switchMode = (mode) => {
                if (mode == "graph"){
                    graph<?= $kunik ?>._buttonGraph.classed("active", true);
                    graph<?= $kunik ?>._buttonList.classed("active", false);
                    $("#mobile-section").hide();
                    graph<?= $kunik ?>._currentMode = mode;
                    graph<?= $kunik ?>._transition = 750;
                }else if(mode == "list"){
                    $("#mobile-section").show();
                    graph<?= $kunik ?>._buttonGraph.classed("active", false);
                    graph<?= $kunik ?>._buttonList.classed("active", true);
                    graph<?= $kunik ?>._currentMode = mode;
                    graph<?= $kunik ?>._transition = 0
                }else{
                    console.error("MODE UNKNOWN");
                }
            }
            graph<?= $kunik ?>.updateData(graph<?= $kunik ?>.preprocessResults(dataToSend), true)
        })
        if($("#graphformap<?= $kunik ?>").is(":visible")){

            window.initedGraph.push('<?= $kunik ?>');
            // initGraph<?= $kunik ?>();
        }
    }
    function initGraph(tags, data) {
        window["init"+graphSelected](tags, data);
        addIconGraph();
        $(".<?= $kunik ?> .affichage").html( typeGraph[graphSelected] + " de " + (dataConfig.legende.length > 0 ? dataConfig.legende[index].legendeLabel : "Type"));
    }
    $(function() {
        if(Object.keys(dataConfig.zones).length > 0){
            getData(dataConfig.zones, dataConfig.zones[Object.keys(dataConfig.zones)[0]]["name"])
        }else{
            if(typeof costum.address != "undefined" && typeof costum.address.level3 != "undefined"){
                var zones = {};
                zones[costum.address.level3+"level3"] = {
                    name: costum.address.level3Name,
                    active: true,
                    id: costum.address.level3,
                    countryCode: costum.address.addressCountry,
                    level: 3,
                    type: "level3",
                    key: costum.address.level3+'level3'
                }
                dataConfig.zones = zones;
                getData(zones, costum.address.level3Name);
            }
        }
        if (index < dataConfig.legende.length - 1) {
            $(".<?= $kunik ?> .right-controls").addClass("enabled");
            $(".<?= $kunik ?> .right-controls").prop("disabled", false);
        }
        $(".<?= $kunik ?> .left-controls").off().on("click", function() {
            $(".<?= $kunik ?> .right-controls").addClass("enabled");
            $(".<?= $kunik ?> .right-controls").prop("disabled", false);
            if (index > 0) {
                index--;
                getData(dataConfig.zones, dataConfig.zones[Object.keys(dataConfig.zones)[0]]["name"])
                // initMap(dataConfig.legende[index])
                if (index == 0) {
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $(".<?= $kunik ?> .right-controls").off().on("click", function() {
            $(".<?= $kunik ?> .left-controls").addClass("enabled");
            $(".<?= $kunik ?> .left-controls").prop("disabled", false);
            if (index <= dataConfig.legende.length - 2) {
                index++;
                getData(dataConfig.zones, dataConfig.zones[Object.keys(dataConfig.zones)[0]]["name"]);
                if (index == dataConfig.legende.length - 1) {
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $("input[name=switchType]").off("change").on('change', function () {
            getDataActive = $(this).val();
            getData(dataConfig.zones, dataConfig.zones[Object.keys(dataConfig.zones)[0]]["name"]);
        })
    })

    function getData(zones, name) {
        var defaultFilters<?= $kunik ?> = {};
        // defaultFilters<?= $kunik ?>['$or'] = {}; 
        // defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
        // defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
        // /*if(costum.contextType!="projects"){
        //     defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
        // }*/
        // defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
        // defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
        var mapSearchFields = (costum != null && typeof costum.map != "undefined" && typeof costum.map.searchFields != "undefined") ? costum.map.searchFields : ["urls", "address", "geo", "geoPosition", "tags", "type", "zone"];
        var params = {
            // notSourceKey: false,
            searchType: <?= json_encode(['organizations']) ?>,
            fields: mapSearchFields,
            filters: defaultFilters<?= $kunik ?>,
            indexStep: 0,
            activeContour: getDataActive === "map",
            locality: zones
        };
        params["options"] = {
            'tags': {
                'verb': '$all'
            }
        };
        ajaxPost(
            null,
            baseUrl + '/co2/search/globalautocomplete',
            params,
            function(data) {
                $("div#circle-filter-container").empty();
                $("div.graph-icons-container").empty();
                if(getDataActive == "map") {
                    initMap(dataConfig.legende[index], data)
                }else{
                    initGraph(dataConfig.legende[index].legendeCible, data.results);
                }
                // mapD3<?= $kunik ?>.getMap().invalidateSize()
                // mapD3<?= $kunik ?>.fitBounds();
                $(".<?= $kunik ?> .zones").html(name);
            },
            function(data) {
                mylog.log('Get zones errors', data);
            },
            null, {
                async: false,
            }
        )
    }
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    sectionDyf.<?= $kunik ?>Params = {
        jsonSchema: {
            "title": "<?= Yii::t("graph", "Inserting a new element") ?>",
            "description": "<?= Yii::t("graph", "Add your item to be displayed in the slide") ?>",
            "properties": {
                "title": {
                    "label": "<?= Yii::t("cms", "Title") ?>",
                    "class": "<?php echo $kunik ?>",
                    "inputType": "text",
                    "rules": {
                        "required": true
                    },
                    "value": sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "type": {
                    "label": "<?= Yii::t("graph", "Type of marker") ?>",
                    "class": "<?php echo $kunik ?>",
                    "inputType": "selectMultiple",
                    "rules": {
                        "required": true
                    },
                    "options": typeChart,
                    "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                },
                "typeGraph": {
                    "label": "<?= Yii::t("graph", "Type pour le graph") ?>",
                    "class": "<?php echo $kunik ?>",
                    "inputType": "selectMultiple",
                    "rules": {
                        "required": true
                    },
                    "options": typeGraph,
                    "value": sectionDyf.<?php echo $kunik ?>ParamsData.typeGraph
                },
                "legende": {
                    "label": "Préférence sur le légende",
                    "inputType": "lists",
                    "entries": {
                        "legendeCible": {
                            "type": "select",
                            "class": "<?php echo $kunik ?> col-xs-5",
                            "label": "<?= Yii::t("graph", "Legend on") ?>",
                            "options": Object.keys((costum.lists || {})).reduce((a, b) => {
                                return (a[b] = b, a);
                            }, {})
                        },
                        "legendeLabel": {
                            "label": "<?= Yii::t("graph", "Name of the legend") ?>",
                            "type": "text",
                            "class": "<?php echo $kunik ?> col-xs-6",
                            "placeholder": "ex: Taille"
                        },
                    },
                    "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendConfig
                },
                "countryCode": {
                    "inputType": "select",
                    "label": "<?= Yii::t("graph", "Country") ?>",
                    "class": "<?php echo $kunik ?>",
                    "options": pays
                },
                "level": {
                    "inputType": "select",
                    "class": "<?php echo $kunik ?>",
                    "label": "<?= Yii::t("graph", "Zone level") ?>",
                    "options": {
                        // "1": "Niveau 1",
                        "2": "Niveau 2",
                        "3": "Niveau 3",
                        "4": "Niveau 4",
                        "5": "Niveau 5",
                        "6": "Niveau 6"
                    }
                },
                "region": {
                    "inputType": "select",
                    "label": "Région",
                    "class": "form-control <?php echo $kunik ?>",
                    "options": []
                },
                "zones": {
                    "inputType": "select",
                    "label": "<?= Yii::t("graph", "Zone") ?>",
                    "class": "<?php echo $kunik ?>",
                    "options": zonesValues,
                    "values": sectionDyf.<?php echo $kunik ?>ParamsData.zones
                }
            },
            beforeBuild: function() {
                uploadObj.set("cms", "<?php echo $blockKey ?>");
                ajaxPost(null,
                    baseUrl + '/co2/search/getzone/', {
                        level: [1]
                    },
                    function(data) {
                        $.each(data, function(kV, vV) {
                            if (!pays[vV.countryCode]) {
                                pays[vV.countryCode] = vV.name;
                            }
                        })
                    },
                    function(data) {
                        mylog.log('Get zones errors', data);
                    },
                    null, {
                        async: false
                    });
            },
            afterBuild: function() {
                $(".regionselect").hide();
                $("#level.<?= $kunik ?>").on("change", function() {
                    var country = $("#countryCode.<?= $kunik ?>").val();
                    var params = {};
                    if ($(this).val()) {
                        params['level'] = [];
                        params['level'].push($(this).val());
                    }
                    if (country && country.length > 0) {
                        params['countryCode'] = country;
                    }
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        params,
                        function(data) {
                            zoneDatas = data;
                            var text = "<option></option>";
                            $.each(data, function(kV, vV) {
                                text += `<option value='${kV}'>${vV.name}</option>`;
                            })
                            $("select#zones.<?= $kunik ?>").html(text)
                        },
                        function(data) {
                            mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    if ($(this).val() == "4") {
                        $(".regionselect").show();
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/', {
                                level: [3],
                                countryCode: params['countryCode']
                            },
                            function(data) {
                                var text = "<option></option>";
                                $.each(data, function(kV, vV) {
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#region.<?= $kunik ?>").html(text)
                            },
                            function(data) {
                                mylog.log('Get zones errors', data);
                            },
                            null, {
                                async: false
                            });
                    } else {
                        $(".regionselect").hide();
                    }
                });
                $("#region.<?= $kunik ?>").on("change", function() {
                    var country = $("#countryCode.<?= $kunik ?>").val();
                    var params = {};
                    if ($("#level.<?= $kunik ?>").val()) {
                        params['level'] = [];
                        params['level'].push($("#level.<?= $kunik ?>").val());
                    }
                    if (country && country.length > 0) {
                        params['countryCode'] = country;
                    }
                    if ($(this).val()) {
                        params["upperLevelId"] = $(this).val();
                    }
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        params,
                        function(data) {
                            zoneDatas = data;
                            var text = "<option></option>";
                            $.each(data, function(kV, vV) {
                                text += `<option value='${kV}'>${vV.name}</option>`;
                            })
                            $("select#zones.<?= $kunik ?>").html(text)
                        },
                        function(data) {
                            mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                });
                $("#countryCode.<?= $kunik ?>").on("change", function() {
                    var country = $(this).val();
                    var params = {};
                    if ($("#level.<?= $kunik ?>").val()) {
                        params['level'] = [];
                        params['level'].push($("#level.<?= $kunik ?>").val());
                    }
                    if (country.length > 0) {
                        params['countryCode'] = country;
                    }
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        params,
                        function(data) {
                            zoneDatas = data;
                            var text = "<option></option>";
                            $.each(data, function(kV, vV) {
                                text += `<option value='${kV}'>${vV.name}</option>`;
                            })
                            $("select#zones.<?= $kunik ?>").html(text)
                        },
                        function(data) {
                            mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                });
            },
            save: function(data) {
                tplCtx.value = {};
                $.each(sectionDyf.<?= $kunik ?>Params.jsonSchema.properties, function(k, val) {
                    tplCtx.value[k] = $("#" + k).val();
                    if (k == "parent") {
                        value[k] = formData.parent;
                    }
                    if (k == "legende") {
                        tplCtx.value[k] = [];
                        $.each(data.legende, function(index, va) {
                            tplCtx.value[k].push(va);
                        })
                    }
                    if (k == "zones") {
                        tplCtx.value[k] = {};
                        if (typeof data.zones == "string") {
                            tplCtx.value[k][data.zones + 'level' + zoneDatas[data.zones].level[0]] = {
                                name: zoneDatas[data.zones].name,
                                active: true,
                                id: data.zones,
                                countryCode: zoneDatas[data.zones].countryCode,
                                level: zoneDatas[data.zones].level[0],
                                type: "level" + zoneDatas[data.zones].level[0],
                                key: data.zones + 'level' + zoneDatas[data.zones].level[0]
                            }
                        } else if (typeof data.zones == "object") {
                            $.each(data.zones, function(kV, vV) {
                                mylog.log("ZOnes key", kV, vV);
                                tplCtx.value[k][vV + 'level' + zoneDatas[vV].level[0]] = {
                                    name: zoneDatas[vV].name,
                                    active: true,
                                    id: vV,
                                    countryCode: zoneDatas[vV].countryCode,
                                    level: zoneDatas[vV].level[0],
                                    type: "level" + zoneDatas[vV].level[0],
                                    key: vV + 'level' + zoneDatas[vV].level[0]
                                }
                            })
                        } else {
                            tplCtx.value[k] = "";
                        }
                    }
                });
                if (typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value(tplCtx, function(params) {
                        dyFObj.commonAfterSave(params, function() {
                            toastr.success("<?php echo Yii::t('cms', 'Element well added') ?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                    });
                }
            }
        }
    }
    $(".edit<?php echo $kunik ?>Params").off().on("click", function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData);
        alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, "hr", 4, 4, null, null, "<?php echo Yii::t('cms', 'Property of the dividing line') ?>", "green", "");
    });
</script>