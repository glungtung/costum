<?php 
/**
 * TPLS QUI PERMET AFFICHAGE DES PROJETS
 */
$keyTpl = "blockcarousel";

$paramsData = [
    "title"         => "Block caroussel d'éléments",
    "description"   => "Block caroussel d'éléments",
    "priority" => "all",
    "elementType"   => "crowdfunding",
    "background"    => "#FFFFFF",
    "icon"          => " ",
    "color"         => "",
    "limit"         => 3,
    "iconActionColor" =>  "#ABB76B",
    "imageForm" => "circle",
    "textPreview" => "En savoir plus",
    "colorHoverBlock" =>"",
    "colorActionHoverBlock"=>""
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if(isset($costum["contextType"]) && isset($costum["contextId"])){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
}

?>
<style type="text/css">

    .mx-2{
        margin-left: 1em;
        margin-right: 1em;
    }

    .truncate {
        /*white-space: nowrap;*/
        overflow: hidden;
        text-overflow: ellipsis;
    }

    li.indicator_<?=$kunik?>{
        background-color: #dddddd !important;
        width: 15px !important;
        height: 15px !important;
        margin-right: .3em !important;
        margin-left: .3em !important;
    }

    .projet_<?= $kunik ?> .color-h2 {
        margin-top: 5%;
        
    }
    .block-container-<?= $kunik ?> .title2{
        text-transform:capitalize!important;
    }
    .projet_<?= $kunik ?> .color-h2:hover{
        text-decoration: underline;
        text-decoration-color:#05323e !important;

    }
    .projet_<?= $kunik ?> .a-caroussel{
        /*color: <?= $paramsData["iconActionColor"] ?>;
        text-decoration : none;*/
        min-width:60%;
    }
    .projet_<?= $kunik ?> .modal-footer{
        border : none !important;
    }
    .projet_<?= $kunik ?> .carousel-indicators{
        bottom: 0px;
        position: relative;
    }
    .projet_<?= $kunik ?> .carousel-indicators li{
        border: 1px solid #8fcd52;
    }

    .projet_<?= $kunik ?> .arrow{
        margin-top: 2%; 
        font-size: 3rem; 
        color: #abb76b;
    }
    .projet_<?= $kunik?> .btn-edit-delete{
        display: none;
        z-index: 1000;
    }
    .projet_<?= $kunik?>:hover  .btn-edit-delete {
        display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        left: 50%;
        transform: translate(-50%,-50%);
    }

    .projet_<?= $kunik ?> .card-info .color-h2{
            text-transform: uppercase !important;
            border-bottom: 1px solid #aaa;
    }

    .projet_<?= $kunik ?> .card-info{
        min-height:300px !important;
    }
    .projet_<?= $kunik ?> .card-info div{
        /*border : solid 1px #808080;
        padding:20px;*/
        /*background-color: rgb(255, 255, 255);*/
       /*// box-sizing: border-box;*/
    /* width: 100%; */
        /*border: solid # 1px;*/
       /* margin:20px;*/
        /*min-height:250px;*/
        /*height:450px;*/
    }

    
    .addDonation:hover{
       /* background-color:#2C3E50;*/
    }

    @media (max-width:768px){
        
        .projet_<?= $kunik ?> .card-info .color-h2{
            margin-top: 5%;
        }
        .projet_<?= $kunik ?>  .a-caroussel{
            /*color:<?= $paramsData["iconActionColor"] ?>;;
            text-decoration : none;*/
            min-width:20%;
        }
        .projet_<?= $kunik ?>  .modal-footer{
            border : none !important;
        }
        .projet_<?= $kunik ?>  .carousel-indicators{
            bottom: 0px;
            position: relative;
        }
        .projet_<?= $kunik ?>  .carousel-indicators li{
            border: 1px solid #8fcd52;
        }
        .projet_<?= $kunik ?>  .carousel-indicators .active{
            background-color: #8fcd52;
        }
    }
    
</style>
<section class="projet_<?= $kunik ?> text-center" style="padding-top:0px !important">
    <h1 id="title<?= $kunik ?>" class="text-center sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <i class="fa <?= $paramsData["icon"] ?>"></i>
        <span><?= $paramsData["title"]; ?></span>
    </h1>
    <div class="text-center btn-edit-delete ">
        <?php if(Authorisation::isInterfaceAdmin()){ ?>
            <?php if($paramsData["elementType"]=="crowdfunding"){ ?>
                <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('crowdfunding')">
                    Ajouter un élément
                </button>
            <?php } ?>

            <?php if($paramsData["elementType"]=="citoyens"){ ?>
                <a class="btn lbh btn-primary btn-xs" href="#element.invite.type.citoyens.id.<?= $_SESSION['userId'] ?>"><?php echo Yii::t('cms', 'Add an item')?></a>
            <?php } ?>
        <?php } ?>
    </div>
    <p class="sp-text img-text-bloc description" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description">
        <?= "\n".$paramsData["description"]; ?>
    </p>
    <div id="elementsCarousel<?= $kunik ?>" class="carousel slide text-center" data-ride="carousel" data-type="multi">
        <div id="slider<?= $kunik ?>" class="text-center arrow hidden" style="">
            <a class="left a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="prev">  
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
            <a class="right a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="next">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
        <br>

        <div id="content-results-project<?= $kunik ?>" class="carousel-inner"></div>
       
        <div class="col-md-12" style="margin-top: 1%;">
           <ol class="carousel-indicators" id="indactors-elementsCarousel<?= $kunik ?>"></ol>
       </div>
   </div>
    
</section>
<?php 
//var_dump($paramsData["colorActionHoverBlock"]);
?>
<script type="text/javascript">
    contextData = {
        id : "<?php echo $costum["contextId"] ?>",
        type : "<?php echo $costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };

    let nbItem<?= $kunik ?> = <?= $paramsData["limit"] ?>;
    let elementType<?= $kunik ?> = "<?= $paramsData["elementType"] ?>";

    mylog.log("elementType",elementType<?= $kunik ?>);

    tplCtx = {};
    
    sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    $(document).ready(function(){
        

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up the projects section as a carousel')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section on carousel projects')?>",
                "icon" : "fa-cog",
                "properties" : {
                    elementType : {
                        label : "<?php echo Yii::t('cms', 'Element')?>",
                        inputType : "select",
                        options : {
                            "crowdfunding":"<?php echo Yii::t('cms', 'List of community campaigns')?>"
                        },
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.elementType
                    },

                    priority : {
                        label : "<?php echo Yii::t('cms', 'Campaigns to display')?>",
                        class : "form-control",
                        inputType : "select",
                        options : {'main' : '<?php echo Yii::t("cms", "Main")?>','secondary':'<?php echo Yii::t("cms", "Secondary")?>','all':'<?php echo Yii::t("cms", "All")?>'},
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.priority
                    },
                    description : {
                        inputType: "textarea",
                        label : "<?php echo Yii::t('cms', 'Subtitle or Description')?>",
                        markdown: true,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.description
                    },
                    imageForm : {
                        label : "<?php echo Yii::t('cms', 'Shape of the images')?>",
                        class : "form-control",
                        inputType : "select",
                        options : {'circle' : "<?php echo Yii::t('cms', 'Round image')?>", 'rounded': "<?php echo Yii::t('cms', 'Square image')?>"},
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.imageForm
                    },
                    icon : { 
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        class : "form-control",
                        inputType : "select",
                        options : <?= json_encode(Cms::$icones); ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                    },
                    limit : {
                        label : "<?php echo Yii::t('cms', 'Number of projects displayed')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.limit
                    },
                    textPreview : {
                        label : "<?php echo Yii::t('cms', 'Text of the read more button')?>",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.textPreview
                    },
                    iconActionColor : { 
                        label : "<?php echo Yii::t('cms', 'Color of the buttons read more')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.iconActionColor
                    },
                    colorHoverBlock : {
                        label : "<?php echo Yii::t('cms', 'Color when passing over the block')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorHoverBlock
                    },
                    colorActionHoverBlock : {
                        label : "<?php echo Yii::t('cms', 'Color of the learn more button above the block')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorActionHoverBlock
                    }
                },
                beforeBuild : function(){
                  uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                        if (k == "parent") {
                            tplCtx.value[k] = formData.parent;
                        }
                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) { 
                           dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            //   urlCtrl.loadByHash(location.hash);
                          });
                        } );
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            //alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"icon",2,6,null,null,"Paramètre d'icons","blue","");
            
        });
        
        var params<?= $kunik ?> = {
            source : costum.contextSlug,
            element : elementType<?= $kunik ?>,
            id : contextData.id
        };

        if (notNull(sectionDyf.<?php echo $kunik ?>ParamsData.priority) &&
            sectionDyf.<?php echo $kunik ?>ParamsData.priority!="all"){
            params<?= $kunik ?>.priority = sectionDyf.<?php echo $kunik ?>ParamsData.priority;
        }

        mylog.log("params kunik",params<?= $kunik ?>);


        ajaxPost(
            null,
            baseUrl+"/costum/filiere/get-related-elements",
            params<?= $kunik ?>,
            function(data){
                mylog.log("success", data);
                var html = "";
                var li = "";
                var y= 0;

                if(data.result == true){
                    i = 0;
                    url1 = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
                    url2 = "<?= Yii::app()->getModule('co2')->assetsUrl; ?>/images/thumb/default_citoyens.png";
                    html += "<div class='item active'>";
                    li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='active indicator_<?=$kunik?>'></li>";

                    if(Object.entries(data.elt).length <= nbItem<?= $kunik ?>){
                        $("#slider<?= $kunik ?>").remove();
                        $("#title<?= $kunik ?>").css("margin-bottom","1em");
                    }

                    for (const [k, v] of Object.entries(data.elt)) {
                        
                        let imgThumb = url1;
                        
                        if(elementType<?= $kunik ?>=="crowdfunding"){
                            imgThumb = url1;
                        }else{
                            imgThumb = url2;
                        }
                        
                        if(typeof v.project !="undefined" && v.project!=null){
                            if (typeof v.project.profilImageUrl != "undefined" && v.project.profilImageUrl != null && v.project.profilImageUrl != ""){
                            imgThumb = v.project.profilImageUrl; 
                            }
                        }    

                        let descript = "";

                        if(i >= nbItem<?= $kunik ?>){
                            html += "</div>";
                            html += "<div class='item'>";
                            y++;
                            li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='indicator_<?=$kunik?>'></li>";
                            i = 0;
                            $(".arrow").removeClass("hidden");
                        }

                        i++;
                                    
                        html += "<div class='card-info col-sm-12 col-md-"+(12/nbItem<?= $kunik ?>)+"'>";
                        html += "<div style='margin: 10px;border: solid 1px;'>";
                        if(sectionDyf.<?php echo $kunik ?>ParamsData.priority!="secondary"){
                            html += "<center><a href='#page.type."+elementType<?= $kunik ?>+".id."+k+"' class='lbh-preview-element a-caroussel' ><img src='"+imgThumb+"' class='img-responsive bg-light' style='<?=($paramsData["imageForm"]=="circle")?"border-radius : 50%; width: 150px; max-width: 150px;":"width: 100%; max-width: 100%;";?> height: 150px; max-height: 150px; object-fit: cover; background-color:#eeeeee'></a></center>";
                        }
                        var explainHeight=(sectionDyf.<?php echo $kunik ?>ParamsData.priority=="secondary") ? "150" : "250";     
                        html += "<div class='col-xs-12' style='overflow-y:auto;height:"+explainHeight+"px;'>";
                        html += "<a href='#page.type."+elementType<?= $kunik ?>+".id."+k+"' class='lbh-preview-element a-caroussel' ><h2 class='color-h2 mb-1 title-2 text-center truncate padding-5'>"+v.project.name+"</h2></a>";
                                
                        if(v.project.shortDescription){
                            descript = v.project.shortDescription;
                        }else if(v.email){
                            descript = v.project.email;
                        }

                        html += "<p class='description padding-5'>"+descript.substring(0, 350)+"</p>";
                         html += "</div>";
                        html += "<a href='#page.type."+elementType<?= $kunik ?>+".id."+k+"' class='lbh-preview-element a-caroussel margin-5 btn btn-lg btn-primary' ><?= $paramsData["textPreview"] ?></a>";
                        
                       // html += "<div class='text-center'>";
                       if(sectionDyf.<?php echo $kunik ?>ParamsData.priority!="secondary"){
                            html += '<a href="javascript:;" class="margin-5 btn btn-lg btn-primary a-caroussel addDonation" data-id="'+v.project._id.$id+'" data-name="'+v.project.name+'" data-type="'+v.project.collection+'" data-id-camp="'+k+'" data-name-camp="'+v.name+'" data-iban="'+v.iban+'" data-donationPlatform="'+v.donationPlatform+'" data-directdonation="'+v.directDonation+'" data-pledgeamount="'+v.pledgeNumber+'" data-donationamount="'+v.donationNumber+'" data-goal="'+v.goal+'" data-logopath="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>">Je donne ></a>';
                        }
                        //html += "</div>";
                        html += "</div>";
                        html += "</div>";
                    }
                }else{
                    html += "<?php echo Yii::t('cms', 'No items to display')?>";
                    li += "";
                }
                $("#content-results-project<?= $kunik ?>").html(html);
                $("#indactors-elementsCarousel<?= $kunik ?>").html(li);

                

                $(".projet_<?= $kunik ?> #content-results-project<?= $kunik ?> .card-info>div").hover(
                    function(){
                    $(this).css("background-color", '<?php echo $paramsData["colorHoverBlock"]?>');
                    // $(this).find(".a-caroussel").css("color", '<?php echo $paramsData["colorActionHoverBlock"]?>');
                    $(this).find(".a-caroussel").css({"background-color":"white","color":"#8ac4c5"});
                    $(".a-caroussel").hover(
                        function(){
                            $(this).css({"background-color":"#2C3E50","color":"white"});
                        },
                        function(){
                            $(this).css({"background-color":"white","color":"#8ac4c5"});
                        }
                    );    

                    },
                    function(){
                    $(this).css("background-color", "white");
                    // $(this).find(".a-caroussel").css("color", '<?php echo $paramsData["iconActionColor"]?>');
                    $(this).find(".a-caroussel").css({"background-color":"#2C3E50","color":"white"});
                    }
            
                );

                $(".projet_<?= $kunik ?> .card-info img").hover(
                    function(){
                    $(this).closest('.card-info').find("h2").css({"text-decoration":'underline',"text-decoration-color":"#05323e"}); 
                    },
                    function(){
                    $(this).closest('.card-info').find("h2").css("text-decoration", 'unset'); 
                    }
            
                );

                $(".projet_<?= $kunik ?> .card-info h2").hover(
                            function(){
                                $(this).closest('.card-info').find("h2").css({"text-decoration":'underline',"text-decoration-color":"#05323e"});
                            },
                            function(){
                                $(this).closest('.card-info').find("h2").css("text-decoration", 'unset'); 
                            }
                        );

                coInterface.bindLBHLinks();
                directory.bindBtnElement();
            
            }
        );

        $('.carousel[data-type="multi"] .item').each(function() {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));

            for (var i = 0; i < nbItem<?= $kunik ?>; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }

                next.children(':first-child').clone().appendTo($(this));
            }
        });

        
    });

</script>