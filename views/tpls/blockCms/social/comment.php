<?php  
$keyTpl = "comment";
$paramsData=[];

if (isset($blockCms)) {
	$paramsData = $blockCms;
}

$profile_img = "";

if(isset(Yii::app()->session["userId"])){
	$me = Person::getMinimalUserById(Yii::app()->session["userId"]);
    $profile_img = Element::getImgProfil($me, "profilThumbImageUrl", $this->module->assetsUrl); 
}
?>

<style>
    .col-avatar-<?=$kunik?>, .col-body-<?=$kunik?>{
        padding:0;
    }

    .msg_receive-<?=$kunik?>{
        padding-left:0;
        margin-left:0;
    }

    .msg_sent-<?=$kunik?>{
        padding-bottom:20px !important;
        margin-right:0;
    }

    .messages-<?=$kunik?> {
        background: white;
        padding: 10px;
        border-radius: 2px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
        max-width:100%;
    }

    .messages-<?=$kunik?> > p {
        font-size: 16px;
        padding: 1em;
        margin: 0 0 0.2rem 0;
    }
    .messages-<?=$kunik?> > time {
        padding: 1em;
        font-size: 14px;
        color: #999;
    }
    .msg_container-<?=$kunik?> {
        padding: 10px;
        overflow: hidden;
        display: flex;
    }
    .avatar-<?=$kunik?> img {
        display: block;
        width: 100%;
    }

    .avatar-<?=$kunik?> {
        position: relative;
    }

    .base_sent-<?=$kunik?> {
    justify-content: flex-end;
    align-items: flex-end;
    }

    #comments-<?=$kunik?>{
        margin-bottom: 3em;
    }

    .replies-<?=$kunik?>{
        margin-bottom: 2em;
    }
</style>

<div class="container">
    <h4>Laisser un <span class="styled-border">commentaire</span></h4>
    <div class="row msg_container-<?=$kunik?> base_sent-<?=$kunik?>">
        <div class="col-md-1 col-xs-2 avatar-<?=$kunik?>"></div>
        <div class="col-md-9 col-body-<?=$kunik?> col-xs-9">
            <textarea name="comment" id="body" class="form-control form-control-lg" rows="5" placeholder="Laisser un commentaire ici ..."></textarea>
        </div>
        <div class="col-md-2 col-xs-2 avatar-<?=$kunik?>">
            <button class="btn btn-theme" id="send">ENVOYER</button>
        </div>
    </div>

    <br><br>
    <h4 id="comments-title">Commentaire(s)</h4>
    <br>
    <div class="row">
        <div id="comments-<?=$kunik?>" class="col-xs-12 col-md-12">
            
        </div>
        <div class="text-center">
            <button class="btn btn-theme" id="loadMore">Afficher plus de commentaires</button>
        </div>
    </div>
</div>


<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script type="text/javascript">
    $(".btn-theme").css({ 'color' : '<?= $paramsData["txtColorButton"];?>', 'background': '<?= $paramsData["bgColorButton"];?>'})

    function get_comments(){
        $.ajax({
            url: '<?= Yii::app()->baseUrl; ?>/costum/comment',
            type: 'GET',
            data: {"comment":"<?= $paramsData["_id"] ?>"},
            dataType: 'json'
        }).done(function(data){
            for(let [key, comment] of Object.entries(data)) {
                $('#comments-title').text((Object.entries(data).length)+" Commentaire(s)");
                const comment_element = `<div class="row msg_container-<?=$kunik?> base_receive">
                        <div class="col-md-1 col-avatar-<?=$kunik?> col-xs-2 avatar">
                            <img src="${comment.avatar}" class="img-responsive ">
                        </div>
                        <div class="col-xs-9 col-body-<?=$kunik?> col-md-9">
                            <div class="messages-<?=$kunik?> msg_receive-<?=$kunik?>">
                                <p>${comment.body}</p>
                                <time>${comment.date}</time>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-2 avatar-<?=$kunik?>">
                            <button class="btn btn-theme " data-toggle="collapse" data-parent="#comments-<?=$kunik?>" data-target="#formreply${comment._id["$id"]}" aria-expanded="false" aria-controls="formreply${comment._id["$id"]}">Répondre</button>
                        </div>
                    </div>
                    <div class="collapse" id="formreply${comment._id["$id"]}">
                        <div class="row msg_container-<?=$kunik?> base_sent-<?=$kunik?>">
                            <div class="col-md-1 col-xs-2"></div>
                            <div class="col-md-9 col-body-<?=$kunik?> col-xs-9">
                                <textarea id="rf${comment._id["$id"]}" class="form-control form-control-lg" rows="2" placeholder="Laisser un commentaire ici ..."></textarea>
                            </div>
                            <div class="col-md-1 col-xs-12 avatar-<?=$kunik?>">
                                <button class="btn btn-theme reply" onclick="reply('${comment._id["$id"]}')" id="${comment._id["$id"]}">Envoyer</button>
                            </div>
                            <div class="col-md-1 col-xs-1"></div>
                        </div>
                    </div>
                    <div id="comment${comment._id["$id"]}" class="replies-<?=$kunik?>"></div>`;

                $('#comments-<?=$kunik?>').append(comment_element);
                $(".btn-theme").css({ 'color' : '<?= $paramsData["txtColorButton"];?>', 'background': '<?= $paramsData["bgColorButton"];?>', 'font-weight':'bolder'})
                if(comment.replies){
                    for(let [k, r] of Object.entries(comment.replies) ){
                        const reply_element = `<div class="row msg_container-<?=$kunik?> base_sent-<?=$kunik?>">
                                <div class="col-md-1 col-xs-2 avatar-<?=$kunik?>"></div>
                                <div class="col-md-9 col-body-<?=$kunik?> col-xs-9">
                                    <div class="messages-<?=$kunik?> msg_sent-<?=$kunik?>">
                                        <p>${r.body}</p>
                                        <time>${r.date}</time>
                                    </div>
                                </div>
                                <div class="col-md-1 col-avatar-<?=$kunik?> col-xs-2 avatar">
                                    <img src="${r.avatar}" class=" img-responsive">
                                </div>
                                <div class="col-md-1 col-xs-2"></div>
                            </div>`;

                        $('#comment'+comment._id["$id"]).append(reply_element);
                    }
                }
            }
        }).fail(function(xhr,textStatus,errorThrown,data){
            alert(xhr+textStatus+errorThrown+data);
        });
    }

    // submit comment
    $('#send').click(function(){
        var parent = "<?= $paramsData["_id"] ?>";
        var date    = new Date();
        var avatar	 = "<?= ($profile_img!="")?$profile_img: Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>";
        var body = $('#body').val();
        var replies = [];
        if(body!=""){
            $.ajax({
                url: '<?php echo Yii::app()->baseUrl; ?>/costum/comment/save',
                type: 'POST',
                data: {"parent":parent,"date":date,"avatar":avatar,"body":body,'replies':replies},
                dataType: 'json'
            }).done(function(data){
                $('#comments-<?=$kunik?>').empty()
                get_comments();
                $('#body').val("");
            }).fail(function(xhr,textStatus,errorThrown,data){
                alert(textStatus);
                console.log(textStatus)	
            });
        }else{
            alert("Ecrire un commentaire avant de l'envyer")
        }
        
    });

    function reply(id){
        var date    = new Date();
        var avatar	 = "<?= ($profile_img!="")?$profile_img: Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>";
        var body = $('#rf'+id).val();
        if(body!=""){
            $.ajax({
                url: '<?php echo Yii::app()->baseUrl; ?>/costum/comment/reply',
                type: 'POST',
                data: {"comment":id, "reply":{ 'date': date, 'avatar': avatar, 'body': body}},
                dataType: 'json',
            }).done(function(data){
                $('#comments-<?=$kunik?>').empty();
                get_comments();
            }).fail(function(data){  
                console.log("ERREUR", data);
            });
        }else{
            alert("Ecrire un commentaire avant de l'envyer")
        }
    }

    get_comments();

    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
        "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section commentaire",
            "icon" : "fa-cog",
            "properties" : {
                "txtColorButton":{
                    label : "Couleur text de bouton",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.txtColorButton
                },
                "bgColorButton":{
                    label : "Couleur arrière-plan de bouton",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.bgColorButton
                }
            },

            save : function(){
                tplCtx.value = {};

                $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                console.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('Value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("élement mis à jour");
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }

    };
    
    mylog.log("paramsData",sectionDyf);

    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });

    setTimeout(() => {
        var x=8+7;
        var msg_size = $(".msg_container-<?=$kunik?>").length;

        (msg_size > x)? $("#loadMore").show(): $("#loadMore").hide();

        $(".msg_container-<?=$kunik?>").hide();
        $(".msg_container-<?=$kunik?>:lt("+x+")").show();

        $('#loadMore').click(function () {
            x= (x+5 <= msg_size)? x+5: msg_size;
            $('.msg_container-<?=$kunik?>:lt('+x+')').show();
        });
    }, 1000);
});
</script>