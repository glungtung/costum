<?php 
    $keyTpl = "profile";
    
    $paramsData=[];

    if (isset($blockCms)) {
        $paramsData = $blockCms;
    }
?>

<style>
.profile-head-<?=$kunik?> {
    /**transform: translateY(7rem);*/
    text-align:center;
    /**margin-left: 2em;*/
}

#dragNdrop{
    margin:0!important;
    padding:0!important;
}

.profile-<?=$kunik?>{
    /**transform: translateY(7rem);*/
}

.cover-<?=$kunik?> {
    background-image: url("<?=(isset($paramsData["backgroundImg"])?$paramsData["backgroundImg"]:Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg')?>");
    background-size: cover;
    background-repeat: no-repeat;
    padding-top: 3em;
    padding-bottom: 3em;
    margin-bottom: 3em;
    min-height: 20em;
}

.subtitle-<?=$kunik?>{
    font-size: 14pt;
    color: <?=(isset($paramsData["subtitleColor"]))?$paramsData["subtitleColor"]:'#ffffff'?>;
}

.text-right-<?=$kunik?>{
    text-align: right;
    vertical-align: bottom
}

.fa-<?=$kunik?>{
    margin-left: 1em;
    padding: 1rem;
    border-radius: 50%;
    border: 3px solid <?=(isset($paramsData["socialColor"]))?$paramsData["socialColor"]:'#ffffff'?>;
    color: <?=(isset($paramsData["socialColor"]))?$paramsData["socialColor"]:'#ffffff'?>;
    font-size: 14pt;
}

.title-color-<?=$kunik?>{
    color: <?=(isset($paramsData["titleColor"]))?$paramsData["titleColor"]:'#ffffff'?>;
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="bg-white shadow rounded">
            <div class="px-4 pt-0 pb-4 cover-<?=$kunik?>">
                <div class="media profile-head-<?=$kunik?>">
                    <div class="profile-<?=$kunik?>">
                        <img src="<?=(isset($paramsData['profilePhoto']))?$paramsData['profilePhoto']:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" alt="Logo or Profile photo" width="130" class="rounded img-thumbnail img-circle">
                    </div>
                    <div class="media-body text-white">
                        <h3 class="title-color-<?=$kunik?>"><?=(isset($paramsData["profileTitle"])&&$paramsData["profileTitle"]!="")?$paramsData["profileTitle"]:"Nom et prénom / Nom Organisation" ?></h3>
                        <p class="subtitle-<?=$kunik?>"><?=(isset($paramsData["profileSubtitle"])&&$paramsData["profileSubtitle"]!="")?$paramsData["profileSubtitle"]:"Sous titre / Slogans" ?></p>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            
                        </div>
                        <div class="col-md-4 text-right-<?=$kunik?>">
                            <a href="<?=$paramsData['facebookLink']?>" target="_blank" title="Facebook"><span class="fa fa-facebook-square fa-<?=$kunik?>"></span></a>
                            <a href="<?=$paramsData['twitterLink']?>" target="_blank" title="Twitter"><span class="fa fa-twitter fa-<?=$kunik?>"></span></a>
                            <a href="<?=$paramsData['websiteLink']?>" target="_blank" title="Site Web"><span class="fa fa-globe fa-<?=$kunik?>"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section profile",
            "icon" : "fa-cog",
            "properties" : {
                "profileTitle":{
                    label : "Titre",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.profileTitle
                },
                "titleColor": {
                    label: "Couleur du titre",
                    inputType: "colorpicker",
                    values : sectionDyf.<?php echo $kunik?>ParamsData.titleColor
                },
                "profileSubtitle":{
                    label : "Sous titre",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.profileSubtitle
                },
                "subtitleColor": {
                    label: "Couleur du sous-titre",
                    inputType: "colorpicker",
                    values : sectionDyf.<?php echo $kunik?>ParamsData.subtitleColor
                },
                "profilePosition":{
                    label : "Position du logo / photo de profile",
                    inputType : "select",
                    options: ["centre", "gauche - bas", "centre - bas"],
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.profilePosition
                },
                "profilePhoto":{
                    label : "Logo / Photo de profile",
                    inputType : "uploader",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.profilePhoto
                },
                "backgroundImg":{
                    label : "Image d'arrière plan",
                    inputType : "uploader",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.backgroundImg
                },
                "socialColor": {
                    label: "Couleur des icons des réseaux sociaux",
                    inputType: "colorpicker",
                    values : sectionDyf.<?php echo $kunik?>ParamsData.socialColor
                },
                "facebookLink": {
                    label: "Lien de page ou profile Facebook",
                    inputType: "text",
                    values: sectionDyf.<?php echo $kunik?>ParamsData.facebookLink
                },
                "twitterLink": {
                    label: "Lien de profile twitter",
                    inputType: "text",
                    values: sectionDyf.<?php echo $kunik?>ParamsData.twitterLink
                },
                "websiteLink": {
                    label: "Lien de votre site web",
                    inputType: "text",
                    values: sectionDyf.<?php echo $kunik?>ParamsData.websiteLink
                }
                /**,
                "socialNetwork":{
                    label : "Réseaux sociaux",
                    inputType : "lists",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork
                } */
            },

            save : function () {  
                tplCtx.value = {};

                $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                console.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("élement mis à jour");
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }
    };

    mylog.log("paramsData",sectionDyf);

    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
});
</script>