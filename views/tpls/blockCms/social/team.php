<?php 
    $keyTpl = "team";
    
    $paramsData=[];

    if (isset($blockCms)) {
        $paramsData = $blockCms;
    }

?>

<style>
    .title-<?=$kunik?> {
        color: grey;
    }

    .card-<?=$kunik?>{
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        margin: 3em;
    }

    .input-<?=$kunik?>{
        padding: 1em;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-<?=$kunik?>">
                <img src="<?=(isset($paramsData['profilePhoto']))?$paramsData['profilePhoto']:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" alt="Jane" style="width:100%">
                <div class="container">
                    <h4>Nom et prénom</h4>
                    <p class="title-<?=$kunik?>">CEO &amp; Founder</p>
                    <p>example@contact.com</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card card-<?=$kunik?>">
                <img src="<?=(isset($paramsData['profilePhoto']))?$paramsData['profilePhoto']:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" alt="Mike" style="width:100%">
                <div class="container">
                    <h4>Nom et prénom</h4>
                    <p class="title-<?=$kunik?>">Art Director</p>
                    <p>example@contact.com</p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card card-<?=$kunik?>">
                <img src="<?=(isset($paramsData['profilePhoto']))?$paramsData['profilePhoto']:Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_citoyens.png' ?>" alt="John" style="width:100%">
                <div class="container">
                    <h4>Nom et prénom</h4>
                    <p class="title-<?=$kunik?>">Designer</p>
                    <p>example@contact.com</p>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section profile",
            "icon" : "fa-cog",
            "properties" : {
                "members":{
                    "label": "Membre",
                    "inputType": "lists",
                    "entries": {
                        /**"photo": {
                            label: "Photo",
                            type: "uploader",
                            class: "col-md-2 col-sm-12",
                            values : sectionDyf.<?php echo $kunik?>ParamsData.titleColor
                        },*/
                        "name":{
                            label : "Nom et prénom",
                            type : "text",
                            class: "col-md-3 col-sm-12",
                            values :  sectionDyf.<?php echo $kunik?>ParamsData.profileTitle
                        },
                        "title":{
                            label : "Titre",
                            type : "text",
                            class: "col-md-3 col-sm-12",
                            values :  sectionDyf.<?php echo $kunik?>ParamsData.profileSubtitle
                        },
                        "contact":{
                            label : "contact",
                            type : "text",
                            class: "col-md-3 col-sm-12",
                            values :  sectionDyf.<?php echo $kunik?>ParamsData.profileSubtitle
                        }
                    }
                }
                /**,
                "socialNetwork":{
                    label : "Réseaux sociaux",
                    inputType : "lists",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork
                } */
            },

            save : function () {  
                tplCtx.value = {};

                $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                console.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("élement mis à jour");
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    } );
                }
            }
        }
    };

    mylog.log("paramsData",sectionDyf);

    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
});
</script>