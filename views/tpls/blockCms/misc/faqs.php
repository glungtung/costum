
<?php 
	$keyTpl = "faqs";
	$paramsData = [ 
	  "title" => array(
	  	"title" => "Frequently Asked Question",
	  	"color" => "",
	  	"align" => "left",
      "size" => "1"
	  ),
    "container" => array(
      "color" => "",
      "marginTop" =>"0",
      "marginBottom" =>"0",
      "marginLeft" =>"0",
      "marginRight" =>"0",
      "paddingTop" =>"0",
      "paddingBottom" =>"0",
      "paddingLeft" =>"0",
      "paddingRight" =>"0",
      "backgroundColor" => "#fff"
    )
	];

	if (isset($blockCms)) {
	    foreach ($paramsData as $e => $v) {
          $paramsData["lg"] = isset($blockCms["lg"]) ? $blockCms["lg"] : "12";
          $paramsData["md"] = isset($blockCms["md"]) ? $blockCms["md"] : "12";
          $paramsData["sm"] = isset($blockCms["sm"]) ? $blockCms["sm"] : "12";
          $paramsData["xs"] = isset($blockCms["xs"]) ? $blockCms["xs"] : "12";
	        if (  isset($blockCms[$e]) ) {
	            $paramsData[$e] = $blockCms[$e];
	        }
	    }
	}

 ?>
 <style>
  .title<?php echo $kunik ?>{
    color: <?php echo $paramsData["title"]["color"] ?>;
    text-align: <?php echo $paramsData["title"]["align"] ?>;
    background-color : <?php echo isset($paramsData["title"]["backgroundColor"])?$paramsData["title"]["backgroundColor"]:""; ?>
  }
  .container<?php echo $kunik ?>{
    background-color : <?php echo $paramsData["container"]["backgroundColor"] ?>;
    margin-top: <?php echo $paramsData["container"]["marginTop"] ?>px;
    margin-bottom: <?php echo $paramsData["container"]["marginBottom"] ?>px;
    margin-left: <?php echo $paramsData["container"]["marginLeft"] ?>px;
    margin-right: <?php echo $paramsData["container"]["marginRight"] ?>px;
    padding-top: <?php echo $paramsData["container"]["paddingTop"] ?>px;
    padding-bottom: <?php echo $paramsData["container"]["paddingBottom"] ?>px;
    padding-left: <?php echo $paramsData["container"]["paddingLeft"] ?>px;
    padding-right: <?php echo $paramsData["container"]["paddingRight"] ?>px;
    box-shadow: 0 0px 20px rgba(0, 0, 0, 0.2);
  }
  .panel-group<?php echo $kunik ?> .panel-collapse{
      word-break: break-all;
      padding: 15px;
  }
</style>
<div class="container<?php echo $kunik ?> col-md-12">
  <h<?php echo @$paramsData["title"]["size"] ?> class="title<?php echo $kunik ?>">
    <?php echo $paramsData["title"]["title"] ?>
  </h<?php echo @$paramsData["title"]["size"] ?>>
  <div class="panel-group panel-group<?php echo $kunik ?>" id="accordion" role="tablist" aria-multiselectable="true"></div>
  <?php if(Authorisation::isInterfaceAdmin()){ ?>

  <div class="text-center">
      
      <a 
        href="javascript:;" 
        class="btn btn-xs btn-primary" onclick="dyFObj.openForm('poi')">
        Ajouter Faqs
     </a>
  </div>
</div>



<script>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
      jQuery(document).ready(function() {
/********************for title**************************/
        sectionDyf.<?php echo $kunik ?>TitleParams = {
          "jsonSchema" : {    
            "title" : "Configurer le titre",
            "description" : "Personnaliser le titre",
            "icon" : "fa-cog",
            
            "properties" : {
              
              "title" : {
                "inputType" : "text",
                "label" : "titre",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.title.title
              },

              "color" : {
                "inputType" : "colorpicker",
                "label" : "Couleur du titre",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.title.color
              },
              "size" : {
                "inputType" : "select",
                "label" : "Taille",
                "markdown" : true,
                options :  {
                  '1':'Niveau 1',
                  '2':'Niveau 2',
                  '3':'Niveau 3',
                  '4':'Niveau 4',
                  '5':'Niveau 5',
                  '6':'Niveau 6',
                },
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.title.size
              },
              "align" : {
                "inputType" : "select",
                "label" : "Aligné à(au)",
                "markdown" : true,
                options :  {
                  'left':'left',
                  'center':'center',
                  'right': 'right',
                },
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.title.align
              }
            },
            save : function () {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>TitleParams.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent") {
                  tplCtx.value[k] = formData.parent;
                }
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) { 
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>1Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "title";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>TitleParams,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
  /********************for container**************************/
sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer le bloc",
            "description" : "Personnaliser le bloc",
            "icon" : "fa-cog",
            
            "properties" : {
             "backgroundColor" : {
                "inputType" : "colorpicker",
                "label" : "Couleur du fond",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.backgroundColor
              },
              "marginTop" : {
                "inputType" : "text",
                "label" : "margin top",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.marginTop
              },
              "marginBottom" : {
                "inputType" : "text",
                "label" : "margin bottom",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.marginBottom
              },
              "marginLeft" : {
                "inputType" : "text",
                "label" : "margin left",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.marginLeft
              },
              "marginRight" : {
                "inputType" : "text",
                "label" : "margin right",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.marginRight
              },
              "paddingTop" : {
                "inputType" : "text",
                "label" : "padding top",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.paddingTop
              },
              "paddingBottom" : {
                "inputType" : "text",
                "label" : "padding bottom",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.paddingBottom
              },
              "paddingLeft" : {
                "inputType" : "text",
                "label" : "padding left",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.paddingLeft
              },
              "paddingRight" : {
                "inputType" : "text",
                "label" : "padding right",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.container.paddingRight
              }
            },
            save : function () {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent") {
                  tplCtx.value[k] = formData.parent;
                }
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) { 
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>2Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        /********************end for container**************************/

       

        /****************************edit faq**************************/
        setTimeout(function(){
            $(".panel-group<?php echo $kunik ?> .edit").off().on('click',function(){
              dyFObj.editElement('poi',$(this).data('id'));
            });
        },2000)
        /****************************end edit faq**************************/
  });
/****************************delete faq***************************/
        //setTimeout(function(){
        function deleteFaq(btnClick,id){
          //$(".panel-group<?php //echo $kunik ?> .delete").off().on("click",function () {
                $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                //var btnClick = $(this);
                //var id = $(this).data("id");
                var type = "poi";
                var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
                
                bootbox.confirm("confirm please !!",
                function(result) 
                {
              if (!result) {
                btnClick.empty().html('<i class="fa fa-trash"></i>');
                return;
              } else {
                ajaxPost(
                      null,
                      urlToSend,
                      null,
                      function(data){ 
                          if ( data && data.result ) {
                          toastr.success("élément effacé");
                          $("#"+type+id).remove();
                          getFaqs();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                      }
                  );
              }
            });
          //});
       // },2000)
        }
    /****************************end delete faq**************************/

     /****************************faqs list**************************/ 
        getFaqs();
        function getFaqs(){
            $.ajax({
             url : baseUrl+"/costum/blockcms/getpoiaction",
             type : 'POST', 
             data : {
              sourceKey : contextSlug,
              type: "faq"
            },
             dataType : "json",
             success:function(data){
                mylog.log("blockcms poi",data);
                var html = "";
                $.each(data, function( index, value ) {
                  html += 
                    '<div class="panel panel-default">'+
                      '<div class="panel-heading" role="tab" id="heading'+index+'">'+
                        '<h4 class="panel-title">'+
                          '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+'" aria-expanded="false" aria-controls="collapseTwo">'+
                            value.name +
                          '</a>';
                          <?php if (Authorisation::isInterfaceAdmin()) {?>
                  html += '<a class="btn btn-danger btn-xs delete pull-right" href="javascript:;" onclick="deleteFaq(this,\''+value["_id"]["$id"]+'\')">'+
                            '<i class="fa fa-trash"></i>'+
                          '</a>'+
                          '<a class="btn btn-danger btn-xs edit pull-right margin-right-10" href="javascript:;" data-id="'+value["_id"]["$id"]+'">'+
                            '<i class="fa fa-pencil"></i>'+
                          '</a>';  
                          <?php } ?>
              html +=   ' </h4>'+
                      '</div>'+
                      '<div id="collapse'+index+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+index+'"'+
                        '<div class="panel-body">'+
                         value.description+
                        '</div>'+
                      '</div>'+
                    '</div>';

                });
                $(".panel-group<?php echo $kunik ?>").html(html);
             }
          });
        } 
        /****************************end faqs list**********************/
</script>