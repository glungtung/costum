<?php 
	$keyTpl = "calendar";
	$paramsData = [ 
	    "title" => "Calendrier",
	    "headerLeft"=> "prev,next",
      "headerCenter"=> "title",
      "headerRight"=> "today",
      "defaultView"=>"month",
	    "icon" =>  "",
      "marginTop" =>"",
      "marginBottom" =>"",
      "marginLeft" =>"",
      "marginRight" =>"",
      "paddingTop" =>"",
      "paddingBottom" =>"",
      "paddingLeft" =>"",
      "paddingRight" =>"",
	    "backgroundColor" => "#FFFFFF",
	];

	if (isset($blockCms)) {
	    foreach ($paramsData as $e => $v) {
	        if (  isset($blockCms[$e]) ) {
	            $paramsData[$e] = $blockCms[$e];
	        }
	    }
	}
 ?>
<style>
  .calendar-container<?php echo $kunik ?>{
    background-color: <?php echo $paramsData["backgroundColor"]; ?>;
    margin-top: <?php echo @$paramsData["marginTop"] ?>px;
    margin-bottom: <?php echo @$paramsData["marginBottom"] ?>px;
    margin-left: <?php echo @$paramsData["marginLeft"] ?>px;
    margin-right: <?php echo @$paramsData["marginRight"] ?>px;
    padding-top: <?php echo @$paramsData["paddingTop"] ?>px;
    padding-bottom: <?php echo @$paramsData["paddingBottom"] ?>px;
    padding-left: <?php echo @$paramsData["paddingLeft"] ?>px;
    padding-right: <?php echo @$paramsData["paddingRight"] ?>px;   
  }
  #filters-nav{
    position: relative !important;
    top:0px !important;
  }
  .calendar-filter<?php echo $kunik ?> .dropdown-menu{
    height: 265px;
    overflow: auto;
  }
  .calendar-filter<?php echo $kunik ?> .autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}

.calendar-filter<?php echo $kunik ?> input[type=text] {
  width: 100%;
}
.calendar-filter<?php echo $kunik ?> input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
}
.calendar-filter<?php echo $kunik ?> .autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.calendar-filter<?php echo $kunik ?> .autocomplete-items div {
  padding: 5px 10px 5px 10px;
  cursor: pointer;
  background-color: #fff;
  border-bottom: 1px solid #d4d4d4;
  display: none;
}
.calendar-filter<?php echo $kunik ?> .autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9;
}
.calendar-filter<?php echo $kunik ?> .autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important;
  color: #ffffff;
}
</style>

<div class="calendar-container<?php echo $kunik ?> col-md-12">
  <h1><?php echo $paramsData["title"] ?></h1>
  <div class="calendar-filter<?php echo $kunik ?> col-md-6">

    <ul class="list-inline">
      <li>
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Ajouter un filtre
          <span class="caret"></span></button>
          <ul class="dropdown-menu"></ul>
        </div>
      </li>
      <li>
        <div class="autocomplete" style="width:300px;">
          <input id="input-tags" type="text" placeholder="#tags" class="form-control">
          <div id="myInputautocomplete-list" class="autocomplete-items">
             <div class="items" data-content="">FFFF</div>
          </div>
        </div>
      </li>
    </ul>

    <div class="btn-filters col-md-12 margin-top-10 margin-bottom-10">
      
    </div>
    
  </div>
    <div class="calendar-filter-tag<?php echo $kunik ?> col-md-6">
    
  </div>
  <div class="calendar<?php echo $kunik ?> col-md-12"></div>
    
</div>



<?php if(Authorisation::isInterfaceAdmin()){ ?>
<div class="text-center">
    
  <a 
    href="javascript:;" 
    class="btn btn-xs btn-primary addForumBtn" 
    onclick="dyFObj.openForm('event');">
    Ajouter un evenement
 </a>
</div>
<?php 

$where= array('source.key' => 'blockcmsorga');

$events = PHDB::find(Event::COLLECTION,$where);
?>
<script>
    var eventFilters = [];
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    
    function deleteBtnFilter(me,v){
      var index = eventFilters.indexOf(v);
      eventFilters.splice(index,1);
      me.style.display = 'none';
    }
  // <?php // $filliaireCategories = CO2::getContextList("filliaireCategories"); ?> 
  // var filliaireCategories=<?php // echo json_encode(@$filliaireCategories); ?>;
  // var arrayType = [];
  // $.each(<?php // echo json_encode(Event::$types); ?>,function(k,v){
  //   arrayType.push(k);
  // })
  // mylog.log('kkkkkk',arrayType);
  // var paramsFilter= {
  //   container : ".calendar-filter-tag<?php // echo $kunik ?>",
  //   interface : {
  //     events : {
  //       scroll : true,
  //       scrollOne : true
  //     }
  //   },
  //   filters : {
  //     /*text : true,
  //     scope : true,*/
  //     themes : {
  //       view : "themes",
  //       type : "themes",
  //       name : "Thématique",
  //       action : "themes",
  //       event : "themes",
  //       filliaireCategories : filliaireCategories
  //     },
  //     type : {
  //       view : "dropdownList",
  //       event : "selectList",
  //       type : "type",
  //       list : <?php //echo json_encode(Event::$types); ?>
  //     }
  //   },
  //   defaults : {}
  // };

  // var filterSearch={};
  // calendarObj.searchInCalendar = function(fObj, startDate, endDate){};
  // /**********************surcharger searchObj.********************/
  // var eventsData = null;
  //       searchObj.search.callBack = function(fObj, results){
  //         eventsData = results;
  //         mylog.log("searchObj.search.callBack", results);
  //         fObj.search.currentlyLoading = false;
  //         fObj.search.obj.count=false;
  //         fObj.search.obj.countType=[];
  //         fObj.actions.text.spin(fObj, false);
  //         if(typeof fObj[fObj.search.loadEvent.active].callBack != "undefined") fObj[fObj.search.loadEvent.active].callBack(fObj,results);
  //         if(results.length < fObj.search.obj.indexStep){
  //           fObj[fObj.search.loadEvent.active].stopPropagation=true;
  //           $(fObj.results.dom+" .processingLoader").remove();
  //         }else if(fObj.results.smartGrid)
  //           fObj.results.smartCallBack(fObj, 0);
  //            mylog.log("eventsData",eventsData); 
  //       }

  // /*********************end surcharger searchObj*****************/

	 $(document).ready(function(){
    // filterSearch = searchObj.init(paramsFilter);
	 	sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer le calendrier",
            "description" : "Personnaliser votre calendrier",
            "icon" : "fa-cog",
            
            "properties" : {
              
              "title" : {
                "inputType" : "text",
                "label" : "Titre",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.title
              },
              "headerLeft" : {
                "inputType" : "select",
                "label" : "Entête à gauche",
                "markdown" : true,
                options :  {
                  'prev,next':'prev,next',
                  'title':'title',
                  'today,month': 'today,month',
                  'today,agendaWeek': 'today,agendaWeek',
                  'month,agendaWeek':'month,agendaWeek',
                  'today,month,agendaWeek':'today,month,agendaWeek',
                },
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.headerLeft
              },
              "headerCenter" : {
                "inputType" : "select",
                "label" : "Entête au centre",
                "markdown" : true,
                options :  {
                  'prev,next':'prev,next',
                  'title':'title',
                  'today,month': 'today,month',
                  'today,agendaWeek': 'today,agendaWeek',
                  'month,agendaWeek':'month,agendaWeek',
                  'today,month,agendaWeek':'today,month,agendaWeek',
                },
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.headerCenter
              },"headerRight" : {
                "inputType" : "select",
                "label" : "Entête à droite",
                "markdown" : true,
                options :  {
                  'prev,next':'prev,next',
                  'title':'title',
                  'today,month': 'today,month',
                  'today,agendaWeek': 'today,agendaWeek',
                  'month,agendaWeek':'month,agendaWeek',
                  'today,month,agendaWeek':'today,month,agendaWeek',
                },
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.headerRight
              },
              "defaultView" : {
                "inputType" : "select",
                "label" : "Vue par défaut",
                "markdown" : true,
                options :  {
                  'month':'month',
                  'agendaWeek':'agendaWeek'              
                },
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.defaultView
              },
              "icon" : {
                "inputType" : "text",
                "label" : "Icon",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
              },
              "backgroundColor" : {
                "inputType" : "colorpicker",
                "label" : "Couleur du fond",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.backgroundColor
              },
              "marginTop" : {
                "inputType" : "text",
                "label" : "margin top",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.marginTop
              },
              "marginBottom" : {
                "inputType" : "text",
                "label" : "margin bottom",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.marginBottom
              },
              "marginLeft" : {
                "inputType" : "text",
                "label" : "margin left",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.marginLeft
              },
              "marginRight" : {
                "inputType" : "text",
                "label" : "margin right",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.marginRight
              },
              "paddingTop" : {
                "inputType" : "text",
                "label" : "padding top",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.paddingTop
              },
              "paddingBottom" : {
                "inputType" : "text",
                "label" : "padding bottom",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.paddingBottom
              },
              "paddingLeft" : {
                "inputType" : "text",
                "label" : "padding left",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.paddingLeft
              },
              "paddingRight" : {
                "inputType" : "text",
                "label" : "padding right",
                "markdown" : true,
                value :  sectionDyf.<?php echo $kunik ?>ParamsData.paddingRight
              },
            },
            save : function () {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent") {
                  tplCtx.value[k] = formData.parent;
                }
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) { 
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

      /***************************initialize calendar******************/
      initilizeCalendar();
      function initilizeCalendar(type=[]){
        var dashEventCalendarr = {};
        $.ajax({
          url : baseUrl+"/costum/blockcms/geteventsaction",
             type : 'POST', 
             data : {
              sourceKey : contextSlug,
              type: type
            },
             dataType : "json",
             success:function(data){
              $.each(data,function(k,v){
                data[k].startDate = moment(data[k].startDate.sec*1000).format();
                  data[k].start = moment(data[k].startDate.sec*1000).format();
                  data[k].end = moment(data[k].endDate.sec*1000).format();
                  data[k].endDate = moment(data[k].endDate.sec*1000).format();

              })
                var domCal = {
                 container : ".calendar<?php echo $kunik ?>",
                     options :{
                         header: {
                          left: "<?php echo $paramsData["headerLeft"] ?>",
                           center: "<?php echo $paramsData["headerCenter"] ?>",
                           right: "<?php echo $paramsData["headerRight"] ?>",
                         },
                         defaultView: "<?php echo $paramsData["defaultView"] ?>",
                         eventLimit: false
                        
                     }
                 }; 
                 mylog.log('initilizeCalendar data',data);
                 dashEventCalendarr = calendarObj.init(domCal);
                 calendarObj.searchInCalendar = function(fObj, startDate, endDate){};
                 dashEventCalendarr.results.add(dashEventCalendarr,data);
             }
        }) 
      }
      /***************************end initialize calendar**************/




      /*************initialize filter dropdown for event type***********/
      var eventType = <?php echo json_encode(Event::$types) ?>;
      $.each(eventType,function(k,v){
        $('.calendar-filter<?php echo $kunik ?> .dropdown-menu')
          .append(
            '<li><a href="javascript:;" class="event-type" data-type="'+k+'">'+tradCategory[k]+'</a></li>'
          )
      });
      setTimeout(function(){
        $('.calendar-filter<?php echo $kunik ?> .dropdown-menu .event-type').on('click',function(){
            var typeCliked = $(this).data('type');
            if($.inArray(typeCliked, eventFilters) < 0 )
                eventFilters.push(typeCliked);
            var btnFilters = "";
            $.each(eventFilters,function(k,v){
              btnFilters += '<button onclick="deleteBtnFilter(this,\''+v+'\')" class="btn btn-xs btn-success current-filters margin-5"><i class="fa fa-close"></i>&nbsp;'+tradCategory[v]+'</button>'
            });
            $('.calendar-filter<?php echo $kunik ?> .btn-filters').html(btnFilters);
            mylog.log("eventFiltersArray",eventFilters);
            initilizeCalendar(eventFilters);
            $('.calendar-filter<?php echo $kunik ?> .current-filters').on('click',function(){
                setTimeout(function(){
                  initilizeCalendar(eventFilters);
                },400)
            })
        })
      },1000)
       /*************end initialize filter dropdown for event type**********/


       /************initialize filter dropdown for event tags***************/
        $('.calendar-filter<?php echo $kunik ?> #input-tags').on('keyup',function(){
            if($(this).val() !='')
              $('.calendar-filter<?php echo $kunik ?> .items').html($(this).val()).show();
            else
               $('.calendar-filter<?php echo $kunik ?> .items').hide();
        });
        var btnFilters = "";
        $('.calendar-filter<?php echo $kunik ?> .items').on('click',function(){
          eventFilters.push($(this).html());
          $(this).hide();
          $('.calendar-filter<?php echo $kunik ?> #input-tags').val('');
          mylog.log('tafiditra ve',eventFilters);
          $.each(eventFilters,function(k,v){
            btnFilters += '<button onclick="deleteBtnFilter(this,\''+v+'\')" class="btn btn-xs btn-success current-filters margin-5"><i class="fa fa-close"></i>&nbsp;'+(((typeof tradCategory[v])!="undefined") ? tradCategory[v] : v)+'</button>'
          });
          $('.calendar-filter<?php echo $kunik ?> .btn-filters').html(btnFilters);
          mylog.log("eventFiltersArray",eventFilters);
          initilizeCalendar(eventFilters);
        });


       /************end initialize filter dropdown for event tags***********/
       
	 })


</script>
