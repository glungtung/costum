<?php 
  $keyTpl ="contact";
  $paramsData = [ 
    "title" => "NOUS CONTACTER",
    "subTitle" => "Lorem ipsum dolor sit amet, consectetur adipiscing",
    "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "btnBgColor" => "#bca87d",
    "btnColor" => "white",
    "btnLabel" => "ME CONTACTER"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  // $cssAnsScriptFiles = array(
  //   '/assets/vendor/jquery_realperson_captcha/jquery.realperson.css',
  //   '/assets/vendor/jquery_realperson_captcha/jquery.plugin.js',
  //   '/assets/vendor/jquery_realperson_captcha/jquery.realperson.min.js'
  // //  '/assets/css/referencement.css'
  //   );
  //   HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl); 

  ?>
<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );
  
 ?>
<!-- ****************end get image uploaded************** -->

<style>
  .invalid-feedback {
    display: none;
    width: 100%;
    margin-top: 0.25rem;
    font-size: 80%;
    color: #dc3545;
  }
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    font-size:23px;
  }
  .container<?php echo $kunik ?> .p-img img{
    width: 100%;
    border:2px solid #bca87d;
  }
  .container<?php echo $kunik ?> .p-img{
    text-align: center
  }
  .container<?php echo $kunik ?> .btn-contact-me{
    background-color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
    color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?>;
    padding: 14px 36px;
    border-radius: 0px !important;
    font-size: large !important;
  }
  .container<?php echo $kunik ?> .btn-contact-me:hover{
      background-color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnColor"]; ?>;
      color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
      border: 2px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
  }
  .<?php echo $kunik ?>.modal{
    margin-top: 91px;
  }
  .<?php echo $kunik ?>.modal .modal-body .fa{
    color :#bca87d;
  }
  .container<?php echo $kunik ?> .container-img{
      padding: 0 80px;
  }
  .container<?php echo $kunik ?> .container-img{
    background-color:white;
    <?php if (!empty($initImage)) { ?>
    background-image:url(<?php echo $initImage[0]["imageThumbPath"] ?>) !important;
    <?php  }else{ ?>
      background-image:url(<?php  echo $defaultImg ; ?>) !important;
    <?php  } ?>
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    min-height: 413px;
    margin: 0 77px 0 58px;
  }
  @media (max-width:768px ){
    .container<?php echo $kunik ?> .container-img{
      margin:0 15px 50px 15px !important;
    }
  }
  @media (max-width:1000px ){
    .container<?php echo $kunik ?> .container-img{
      margin:0 15px 0px 15px;
    }
  }
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  <h2 class="title<?php echo $kunik ?> Oswald sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
    <?php echo $paramsData["title"] ?>
  </h2>
  <h3 class="subTitle<?php echo $kunik ?> ReenieBeanie sp-text img-text-bloc subtitle" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subTitle">
    <?php echo $paramsData["subTitle"] ?>
  </h3>
  <div class="col-sm-6">
      <div class="container-img"></div>
  </div>
  <div class="col-sm-6">
      <div class="description item<?php echo $kunik ?> JosefinSansLight sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description"><?php echo $paramsData['description']?></div>
    <a href="javascript:;" class="tooltips btn openFormContact btn btn-contact-me" data-toggle="modal" data-target="#myModal-contact-us"
    data-id-receiver="" 
    data-email=""
    data-name="">
        <?php echo $paramsData["btnLabel"] ?>
    </a>
  </div>
</div>

<div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="sp-cms-container bg-white">

      <div id="form-group-contact">
        <div class="col-md-10 text-left padding-top-60 form-group">
          <h3>
            <i class="fa fa-send letter-blue"></i> 
            <small class="letter-blue">
            Envoyer un e-mail à : </small>
            <span id="contact-name" style="text-transform: none!important;"></span>
            <?php echo @$element["organizer"]["name"]; ?><br>
            <small class="">
              <small class="">Ce message sera envoyé à 
              </small>
              <b><span class="contact-email"></span></b>
            </small>
          </h3>
          <hr><br>
          <div class="col-md-6">
            <label for="email"><i class="fa fa-angle-down"></i> Votre addresse e-mail*</label>
              <input type="email" class="form-control" placeholder="votre addresse e-mail : exemple@mail.com" id="emailSender">
              <div class="invalid-feedback emailSender">Email invalide.</div><br>
          </div>

          <div class="col-md-6">
            <label for="senderName"><i class="fa fa-angle-down"></i> Nom / Prénom</label>
              <input class="form-control" placeholder="comment vous appelez-vous ?" id="senderName">
              <div class="invalid-feedback senderName">Champ requis.</div><br>
          </div>

          <div class="col-md-12">
            <label for="objet"><i class="fa fa-angle-down"></i> Objet de votre message</label>
              <input class="form-control" placeholder="c'est à quel sujet ?" id="subject">
              <div class="invalid-feedback subject">Objet requis.</div><br>

          </div>
        </div>
        <div class="col-md-12 text-left form-group">
          <div class="col-md-12">
            <label for="message"><i class="fa fa-angle-down"></i> Votre message</label>
            <textarea placeholder="Votre message..." class="form-control txt-mail" 
                  id="message" style="min-height: 200px;"></textarea>
            <div class="invalid-feedback message">Votre message est vide.</div><br>
            <div class="col-md-12 margin-top-15 pull-left">
              <button type="submit" class="btn btn-success pull-left" id="btn-send-mail">
                <i class="fa fa-send"></i> Envoyer le message
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
              "icon" : "fa-cog",
            
            "properties" : {
                "btnLabel" : {
                    "inputType" : "Texte du boutton",
                    "label" : "<?php echo Yii::t('cms', 'Button text')?>",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button color')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnColor
                },
                "btnBgColor" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Button background color')?>",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "image" :{
                  "inputType" : "uploader",
                  "label" : "<?php echo Yii::t('cms', 'Image')?>",
                  "docType": "image",
                 "contentKey" : "slider",
                  "itemLimit" : 1,
                  "endPoint": "/subKey/block",
                  "domElement" : "image",
                  "filetypes": ["jpeg", "jpg", "gif", "png"],
                  "label": "Image :",
                  "showUploadBtn": false,
                  initList : <?php echo json_encode($initImage) ?>
              }            
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });



<?php // ***************************send mail************************************* ?>
  $("#btn-send-mail").click(function(){
    sendEmail();
  });

  $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});

  $(".openFormContact").click(function(){
    mylog.log("openFormContact");
    if (exists(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      $("#formContact .contact-email").html(costum.admin.email);
      $("#formContact").modal("show");
    }else{
      bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
    }
  })
});
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function sendEmail(){
  var acceptFields = true;
  $.each(["emailSender","senderName","subject","message"],(k,v)=>{
      if($("#"+v).val() == ""){
        $("."+v).show();
        acceptFields=false
      }
      if(validateEmail($("#emailSender").val())==false){
        acceptFields=false;
        $(".emailSender").show();
      }

  });
  var seconds = Math.floor(new Date().getTime() / 1000);
  var allowedTime = localStorage.getItem("canSend");
  if(acceptFields){
    if(seconds < allowedTime){
      return bootbox.alert("<p class='text-center text-dark'><?php echo Yii::t('cms', 'Send back after')?> "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
    }
    localStorage.removeItem("canSend");
        var emailSender = $("#emailSender").val();
        var subject = $("#subject").val();
        var senderName = $("#senderName").val();
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
          tpl : "contactForm",
          tplMail : emailFrom,
          fromMail: emailSender, 
          tplObject:subject,
          subject :subject, 
          names:senderName,
          emailSender:emailSender,
          message : message,
          logo:"",
        };

        ajaxPost(
          null,
          baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
          params,
              function(data){ 
                if(data.result == true){
                  localStorage.setItem("canSend", (seconds+300));
                  toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                  bootbox.alert("<p class='text-center text-green-k'><?php echo Yii::t('cms', 'Email sent to')?> "+emailFrom+" !</p>");
                  $.each(["emailSender","senderName","subject","message"],(k,v)=>{$("#"+v).val("")})
                }else{
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                  bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
                }   
              },
              function(xhr, status, error){
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
              }
        );  
  }
}
</script>

