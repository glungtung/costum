<?php 
$keyTpl = "contacterNous";
$paramsData = [ 
	"title"=>"Lorem Ipsum",
	"content" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
	"align" => "left", 
	"lienButton"=>"",
	"buttonAlign" => "left",
	"labelbutton"=>"Contacter-Nous",
	"colorLabelButton"=>"#000",
	"colorbutton"=>"#ffffff",
	"colorBorderButton"=>"#000"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	} 
}

  // //$latestImg = Document::getLastImageByKey($blockKey,"cms","presentation");
  // $cssAnsScriptFiles = array(
  //   '/assets/vendor/jquery_realperson_captcha/jquery.realperson.css',
  //   '/assets/vendor/jquery_realperson_captcha/jquery.plugin.js',
  //   '/assets/vendor/jquery_realperson_captcha/jquery.realperson.min.js'
  // //  '/assets/css/referencement.css'
  //   );
  //   HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->theme->baseUrl); 

?>

<style type="text/css">
	.markdown {
		text-align: <?= $paramsData["align"] ?>;	
	}

	.textBack_<?= $kunik ?> a{
		float: <?= $paramsData["buttonAlign"] ?>;	
	}
	.invalid-feedback {
	    display: none;
	    width: 100%;
	    margin-top: 0.25rem;
	    font-size: 80%;
	    color: #dc3545;
	}
	.textBack_<?= $kunik ?>{
		width: 100%;
		padding: 0 15px;		
	}
	.textBack_<?= $kunik ?> h2{
		text-align: center;
	    margin-top: 4%;
		
	}	
	.textBack_<?= $kunik ?> p{
	    line-height: 30px;
	    text-transform: none;
	    padding-top: 4%;
	    padding-left: 2%;
	}
	.button_<?= $kunik ?> {
		background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["colorbutton"]; ?>;
		border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["colorBorderButton"]; ?>;
		color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["colorLabelButton"]; ?>;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		padding: 8px 26px;
		margin-bottom: 6%;
		margin-top: 6%;
		cursor: pointer;
		border-radius: 20px ;
	}
	.textBack_<?= $kunik ?> li {
		font-size: <?= @$paramsData["sizeContent"]?>px;
		color: <?= @$paramsData["colorContent"]?>;

	}
	

	@media screen and (min-width: 1500px){
		.textBack_<?=$kunik?> h2{
			margin-top: 0;
    		padding: 5% 0% 8% 1%;
		    line-height: 35px;
		    text-transform: none;
		}
		.textBack_<?=$kunik?> p{
		    line-height: 35px;
		    text-transform: none;
    		padding: 0% 0% 10% 1%;
		    margin-bottom: -90px;
	  	}
	  	.button_<?= $kunik ?> {
			text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 25px;
		    padding: 8px 26px;
		    margin-bottom: 5%;
		    cursor: pointer;
		    border-radius: 40px;
		}
	}

	@media (max-width: 414px) {
		.textBack_<?= $kunik ?> h2{
			margin-top: 20px;
			font-size: 20px;
		}
		.textBack_<?= $kunik ?> p{
			line-height: 15px;
			text-transform: none;
			margin-bottom: -6px;
			font-size: 13px;
		}
		.textBack_<?= $kunik ?> li{
			font-size: 13px;
		}
		.button_<?= $kunik ?> {
			padding: 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 11px;
			margin: 4px 2px;
			cursor: pointer;
			border-radius: 20px;
		}
	}
</style>
<div  class="textBack_<?= $kunik ?>">
	<div class="sp-cms-container " >  
		<h2 class="text-center sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?= $paramsData["title"]?></h2>
		<div class="markdown sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content" ><?= $paramsData["content"]?></div>
		<div class="col-xs-12 text-center no-padding">
			<a href="javascript:;" class="button_<?= $kunik ?> tooltips btn openFormContact text-center " data-toggle="modal" data-target="#myModal-contact-us"
		    data-id-receiver="" 
		    data-email=""
		    data-name="">  
				<?= $paramsData["labelbutton"]?>
			</a>
		</div>	
	</div>
	<div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="sp-cms-container bg-white">

      <div id="form-group-contact">
        <div class="col-md-10 text-left padding-top-60 form-group">
          <h3>
            <i class="fa fa-send letter-blue"></i> 
            <small class="letter-blue">
                <?php echo Yii::t('cms', 'Send an e-mail to')?> : </small>
            <span id="contact-name" style="text-transform: none!important;"></span>
            <?php 

            //echo @$element["organizer"]["name"];  ?><br>
            <small class="">
              <small class=""><?php echo Yii::t('cms', 'This message will be sent to')?>
              </small>
              <b><span class="contact-email"></span></b>
            </small>
          </h3>
          <hr><br>
          <div class="col-md-6">
            <label for="email"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your e-mail address')?>*</label>
              <input class="form-control" placeholder="<?php echo Yii::t('cms', 'Your e-mail address')?> : exemple@mail.com" id="emailSender">
              <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
            <br>
          </div>
          <div class="col-md-6">
            <label for="name"><i class="fa fa-angle-down"></i><?php echo Yii::t('cms', 'Name / First name')?> </label>
              <input class="form-control" placeholder="<?php echo Yii::t('cms', 'What is your name?')?>" id="name">
              <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
            <br>
          </div>
          <div class="col-md-12">
            <label for="objet"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?></label>
              <input class="form-control" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject">
              <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>
          </div>
        </div>
        <div class="col-md-12 text-left form-group">
          <div class="col-md-12">
            <label for="message"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your message')?></label>
            <textarea placeholder="<?php echo Yii::t('cms', 'Your message')?>..." class="form-control txt-mail"
                  id="message" style="min-height: 200px;"></textarea>
            <div class="invalid-feedback message"><?php echo Yii::t('cms', 'Your message is empty')?>.</div><br>
            <br>
           

            <div class="col-md-12 margin-top-15 pull-left">            
            <button type="submit" class="btn btn-success pull-right" id="btn-send-mail">
              <i class="fa fa-send"></i> <?php echo Yii::t('cms', 'Send the message')?>
            </button>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>	
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	var currentCategory = "";
	jQuery(document).ready(function() {
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					
					"align" : {
						"inputType" : "select",
						"label" : "<?php echo Yii::t('cms', 'Paragraph alignment')?>",
						"options" : {"left" : "gauche","center" : "centre","right" : "droite"},
						values :  sectionDyf.<?php echo $kunik?>ParamsData.align
					},
					"labelbutton" : {
						"label" : "<?php echo Yii::t('cms', 'Button label')?>",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.labelbutton
					},
					"lienButton" : {
						"label" : "<?php echo Yii::t('cms', 'Button link')?>",
						"inputType" :"textarea",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.lienButton
					},
					"buttonAlign" : {
						"inputType" : "select",
						"label" : "<?php echo Yii::t('cms', 'Button alignment')?>",
						"options" : {"left" : "gauche","center" : "centre","right" : "droite"},
						values :  sectionDyf.<?php echo $kunik?>ParamsData.buttonAlign
					},
					"colorLabelButton":{
						label : "<?php echo Yii::t('cms', 'Button label color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorLabelButton
					},
					"colorbutton":{
						label : "<?php echo Yii::t('cms', 'Button color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorContent
					},
					"colorBorderButton":{
						label : "<?php echo Yii::t('cms', 'Button border color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorBorderButton
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});
					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
								toastr.success("Élément bien ajouté");
								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
								// urlCtrl.loadByHash(location.hash);
							});
						} );
					}
				}
			}
		};
		mylog.log("sectiondyfff",sectionDyf);
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
		});
	  $("#btn-send-mail").click(function(){
	    sendEmail();
	  });


  $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $(".openFormContact").click(function(){
    mylog.log("openFormContact");
    if (exists(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      $("#formContact .contact-email").html(costum.admin.email);
      $("#formContact").modal("show");
    }else{
      bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
    }
  });
});

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function sendEmail(){
  var acceptFields = true;
  $.each(["emailSender","senderName","subject","message"],(k,v)=>{
      if($("#"+v).val() == ""){
        $("."+v).show();
        acceptFields=false
      }
      if(validateEmail($("#emailSender").val())==false){
        acceptFields=false;
        $(".emailSender").show();
      }

  });
   var seconds = Math.floor(new Date().getTime() / 1000);
  var allowedTime = localStorage.getItem("canSend");
  if(acceptFields){
    if(seconds < allowedTime){
      return bootbox.alert("<p class='text-center text-dark'><?php echo Yii::t('cms', 'Send back after')?> "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
    }
    localStorage.removeItem("canSend");
      var emailSender = $("#emailSender").val();
        var subject = $("#subject").val();
        var senderName = $("#senderName").val();
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
          tpl : "contactForm",
          tplMail : emailFrom,
          fromMail: emailSender, 
          tplObject:subject,
          subject :subject, 
          names:senderName,
          emailSender:emailSender,
          message : message,
          logo:"",
        };
  
  		ajaxPost(
		    null,
		    baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
		    params,
		        function(data){ 
		           	if(data.result == true){
		               	localStorage.setItem("canSend", (seconds+300));
		               	toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
		               	bootbox.alert("<p class='text-center text-green-k'><?php echo Yii::t('cms', 'Email sent to')?> "+emailFrom+" !</p>");
		                $.each(["emailSender","senderName","subject","message"],(k,v)=>{$("#"+v).val("")});
		                urlCtrl.loadByHash(location.hash);
		            }else{
		                toastr.error("An error occurred while sending your message");
		                bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
		            }   
		        },
        		function(xhr, status, error){
                  	toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
              	}
    	);
  	}  
  
}
</script>