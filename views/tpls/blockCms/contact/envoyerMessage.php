<?php 
$keyTpl = "envoyerMessage";
$paramsData=[
	"textColor" => "#2C3E50",
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
  
?>
<style type="text/css">
	.invalid-feedback {
	    display: none;
	    width: 100%;
	    margin-top: 0.25rem;
	    font-size: 80%;
	    color: #dc3545;
	}
	.form-control_<?= $kunik?>  {
		background-color: #dddddd69;
		border-color: #dddddd69;
		height: 50px;
		border-radius: 0;
		box-shadow: none;
	}
	.classInput_<?= $kunik?>{
		width: 80%;
	}
	.form-group_<?= $kunik?> {
		margin-bottom: 30px;
	}
	.form-radio-container_<?= $kunik?> {
		display: block;
		margin: 5px 0px 7px 0px !important;
		color: <?= $paramsData["textColor"]?>;
	}
	.form-radio-container_<?= $kunik?> .primary-btn{
		display: inline-block;
		font-size: 14px;
		padding: 8px 25px 6px;
		color: <?= $paramsData["textColor"]?>;
		text-align: center;
		text-transform: uppercase;
		font-weight: 700;
		border-radius: 20px;
		border: 1px solid #dddddd69;
		background: #f5833c;
		margin-top: 2%;
		margin-bottom: 2%;
		cursor: pointer;
	}
	.form-radio-container_<?= $kunik?> .primary-btn:hover{
		transform: scale(1.1);
	}
	.form-radio-container_<?= $kunik?> h4{
		font-size: 20px;
	}
	.radio label {
		font-size: 18px;
		line-height: 24px;
	}
	
	.form-radio-container_<?= $kunik?> img:hover{
		transform: scale(1.1);
	}
	input[type='radio']:checked:after {
		width: 15px;
		height: 15px;
		border-radius: 15px;
		top: -2px;
		left: -1px;
		position: relative;
		background-color: #f5833c;
		content: '';
		display: inline-block;
		visibility: visible;
		border: 2px solid white;
	}
	.form-radio-container_<?= $kunik?>" .radio{
		left: 13%;
	}

	.title_<?= $kunik?>{
		color: #327753;
		margin-left: 11%;
		margin-bottom: 3%;
	}
	.form-group>label {
		top: 8px;
		left: 6px;
		position: relative;
		padding: 0px 5px 0px 5px;
		font-size: 0.9em;
	}
	.form-radio-container_<?= $kunik?> .form-group>label{
		font-size: 20px;
	}
	.form-radio-container_<?= $kunik?> .form-group>input{
		height: 50px;
	}
	.form-radio-container_<?= $kunik?> .form-group #autre{
		width:80%;
	}
	@media (max-width: 414px) {
		.form-radio-container_<?= $kunik?> .form-group>label{
			font-size: 15px;
		}
		.form-radio-container_<?= $kunik?> .form-group>input{
			width: 100%;
    		height: 35px;
		}
		.form-radio-container_<?= $kunik?> img{
		    margin-right: 28%;
		    height: 130px;
		    margin-left: 28%;
		}
		.radio label {
			font-size: 14px;
			line-height: 20px;
		}
		.form-radio-container_<?= $kunik?> h4{
			font-size: 15px;
		}
		.form-radio-container_<?= $kunik?> h2{
			font-size: 18px;
		}
		.form-radio-container_<?= $kunik?> p{
			font-size: 13px;
			margin-bottom: 2%;
		}
		.form-radio-container_<?= $kunik?> .primary-btn{
			margin-bottom: 10%;
			font-size: 12px;
		}
		.title_<?= $kunik?>{
			color: #327753;
			margin-left: 11%;
			font-size: 20px;
			margin-bottom: 3%;
		}
	}
	@media screen and (min-width: 1500px){
		.form-radio-container_<?= $kunik?> .primary-btn{
			font-size: 25px;
		}
	}
	@media (max-width: 768px) {
		.form-radio-container_<?= $kunik?> img{}
	}
	.cp .realperson-challenge{
		color: <?= $paramsData["textColor"]?>;
	}
	
</style>
<div class="form-radio-container_<?= $kunik?>">
	<div class="col-md-12  " >
		<div class="col-md-3 col-xs-12 ">
			<img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/assoKosasa/sondage.gif" >
		</div>
		<div class="col-md-9 col-xs-12">
			<h2 class="title_<?= $kunik?> title">
                <?php echo Yii::t('cms', 'If you have some time !')?>
			</h2>
			<div class="col-md-12">
				<div class="col-md-6 col-xs-12">
				<div class=" form-group">
            		<label class="description" for="email"><?php echo Yii::t('cms', 'Your e-mail address')?>*</label>
              		<input class="form-control" placeholder="<?php echo Yii::t('cms', 'Your e-mail address')?> : exemple@mail.com" id="emailSender" style="background-color: #dddddd69;border: 1px solid #dddddd69;color: <?= $paramsData["textColor"]?>">
              		<div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div>
            		<br>
          		</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="form-group">
			    	<label class="description"  for="age"><?php echo Yii::t('cms', 'How old are you ?')?></label>
			    	<input id="age" type="number" class="form-control" style="background-color: #dddddd69;border: 1px solid #dddddd69;color: <?= $paramsData["textColor"]?>" >
			    	<div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Age requirement')?>.</div><br>
				</div>
			</div>		
			
			</div>
			
			<div class="col-md-6 col-xs-12">
				<h4 class="description" >
                    <?php echo Yii::t('cms', 'How did you discover this site ?')?>
				</h4>
				<form>
					<div class="radio">
						<label>
							<input id="recherche" type="radio" name="optradioD" value="Recherche sur Internet" checked><?php echo Yii::t('cms', 'Research on the Internet')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="ami" type="radio" name="optradioD" value="Ami(e)"><?php echo Yii::t('common', 'friends')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="youtube" type="radio" name="optradioD" value="Youtube" >Youtube
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="facebook" type="radio" name="optradioD" value="Facebook" >Facebook
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="evenement" type="radio" name="optradioD" value="Evénement" ><?php echo Yii::t('common', 'Event')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="cooperActeur" type="radio" name="optradioD" value="Coopér'acteur" ><?php echo Yii::t('cms', 'Cooper\'actor')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="autreR" type="radio" name="optradioD" value="Autre"><?php echo Yii::t('cms', 'Other')?>
						</label>
					</div>
					<div class="form-group">
					    <label for="autre">Si autre à preciser</label>
					    <input id="autre" type="text" class="form-control" style="background-color: #dddddd69; border: 1px solid #dddddd69;color: <?= $paramsData["textColor"]?>">
					</div>
				</form>
				<div class="invalid-feedback subject"><?php echo Yii::t('cms', 'required')?>.</div><br>


			</div>
			<div class="col-md-6 col-xs-12">
				<h4 class="description" >
                    <?php echo Yii::t('cms', 'Does Kosasa\'s action seem useful to you ?')?>
				</h4>
				<form>
					<div class="radio">
						<label>
							<input id="tresImportant" type="radio" name="optradioUtilite" value="Très Importante" checked><?php echo Yii::t('cms', 'Very Important')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="important" type="radio" name="optradioUtilite" value="Importante"><?php echo Yii::t('cms', 'Important')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="neutre" type="radio" name="optradioUtilite" value="Neutre"><?php echo Yii::t('cms', 'Neutral')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="plusOuMoins" type="radio" name="optradioUtilite" value="Plus ou moins Importante" ><?php echo Yii::t('cms', 'More or less important')?>
						</label>
					</div>
					<div class="radio">
						<label>
							<input id="pasDutout" type="radio" name="optradioUtilite" value="Pas du tout Importante"><?php echo Yii::t('cms', 'Not at all Important')?>
						</label>
					</div>
				</form>
				<div class="invalid-feedback subject"><?php echo Yii::t('cms', 'required')?>.</div><br>
			</div>

			
		</div>	
		<p class="text-center">
			<a id="btn-send-mes" class="primary-btn"><?php echo Yii::t('cms', 'Send')?></a>
		</p>

	</div>

</div>

	<script type="text/javascript">
		sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
		jQuery(document).ready(function() {
			$(".form-radio-container_<?= $kunik?> #btn-send-mes").click(function(){
			 	 	envoyer();
    			}); 
			 if (exists(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
			      $("#formContact .contact-email").html(costum.admin.email);
			      $("#formContact").modal("show");
			    }else{
			      bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
			    }
			sectionDyf.<?php echo $kunik?>Params = {
				"jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                    "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
					"icon" : "fa-cog",
					"properties" : {
						
					},
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
					save : function () {  
						tplCtx.value = {};
						$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
							tplCtx.value[k] = $("#"+k).val();
						});
						mylog.log("save tplCtx",tplCtx);

						if(typeof tplCtx.value == "undefined")
							toastr.error('value cannot be empty!');
						else {
							dataHelper.path2Value( tplCtx, function(params) {
								dyFObj.commonAfterSave(params,function(){
									toastr.success("Élément bien ajouté");
									$("#ajax-modal").modal('hide');
									var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
									var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
									var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
									cmsBuilder.block.loadIntoPage(id, page, path, kunik);
									// urlCtrl.loadByHash(location.hash);
								});
							} );
						}
					}

				}

			};
			mylog.log("paramsData",sectionDyf);
			$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
				tplCtx.id = $(this).data("id");
				tplCtx.collection = $(this).data("collection");
				tplCtx.path = "allToRoot";
				dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			});
		});
		function validateEmail(email) {
		  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
		}
		function envoyer(){
			var acceptFields = true;
			$.each(["emailSender","optradioD","optradioUtilite","age"],(k,v)=>{
			    if($("#"+v).val() == ""){
			        $("."+v).show();
			        acceptFields=false
			    }
			    if(validateEmail($("#emailSender").val())==false){
			        acceptFields=false;
			        $(".emailSender").show();
			    }
			});


			var seconds = Math.floor(new Date().getTime() / 1000);
			  var allowedTime = localStorage.getItem("canSend");
			  if(acceptFields){
			    if(seconds < allowedTime){
			      return bootbox.alert("<p class='text-center text-dark'><?php echo Yii::t('cms', 'Send back after')?> "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" <?php echo Yii::t('cms', 'second(s)')?>" : Math.floor((allowedTime-seconds)/60)+" <?php echo Yii::t('cms', 'minute(s)')?>")+ "</p>");
			    }
			    localStorage.removeItem("canSend");
			var emailFrom = $(".contact-email").html();
			var emailSender = $("#emailSender").val();
			var decouvre = document.getElementsByName('optradioD');
			var valeurDecouvre;
			for(var i = 0; i < decouvre.length; i++){
				if(decouvre[i].checked){
					valeurDecouvre = decouvre[i].value;
				}				
			}
			if (valeurDecouvre == "Autre") {
				if (document.getElementById("autre").value == "") {

				}else {
					valeurDecouvre = document.getElementById("autre").value;
				}
			}
			//UTILITE
			var utilite = document.getElementsByName('optradioUtilite');
			var valeurUtilite;
			for(var i = 0; i < utilite.length; i++){
				if(utilite[i].checked){
					valeurUtilite = utilite[i].value;
				}
			}
			var age = $("#age").val();

		 	var subject = "Retour";
			var mesage = "Âge: "+age +"ans;  \n Utilité: " +valeurUtilite + ";\n Decouverte par: "+valeurDecouvre
		  	var params = {
		  		tpl : "contactForm",
          		tplMail : emailFrom,
          		tplObject:subject,
			  	names:" ",
			  	emailSender:emailSender, 
			    emailFrom : emailFrom,
			    message :mesage ,
			    subject:subject,
          		logo:"",
		  	};
		  	ajaxPost(
			    null,
		      baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
    		params,
		      function(data){ 
	           	if(data.result == true){
	               	localStorage.setItem("canSend", (seconds+300));
	               	toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
	                $.each(["emailSender","senderName","subject","message"],(k,v)=>{$("#"+v).val("")});
	                urlCtrl.loadByHash(location.hash);
	            }else{
	                toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
	                bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
	            }   
        },
		        // function(xhr, status, error){
		        //     toastr.error("Une erreur est survenue pendant l'envoie de votre message - error");
		        // }
		        function(xhr, status, error){
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
              	}
		    );		  
		}
	}
	</script>