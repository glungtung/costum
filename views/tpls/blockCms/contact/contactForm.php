<?php 
  $keyTpl ="Lorem ipsum";
  $paramsData = [ 
    "titleBlock" => "",
    "subTitleBlock" => "Envoyer un e-mail à :",
    "subTitleBlockTextSize" => "25",
    "btnBgColor" => "#bca87d",
    "btnLabelColor" => "white",
    "btnLabel" => "ME CONTACTER",
    "btnBorderColor" => "#eeeeee",
    "btnRadius" => "4",
    "showForm" => "show",
    //Formulaire et input
    "inputSize" => "16",
    "inputHeight" => "38",
    "inputRadius" => "10",
    "inputBorderType" => "border-bottom",
    "inputBorder" => "1",
    "inputBorderColor" => "#cccccc",
    "inputOtheBorderColor" => "#cccccc",
    "inputBgColor" => "#ffffff",
    "inputTextColor" => "#555555",
    "inputPlaceholderColor" => "",
    //Label input
    "labelColor" => "#333333",
    "labelSize" => "15",
    "labelAlign" => "left",
    //bouton envoie mail
    "sendBtnBgColor" => "#18BC9C",
    "sendBtnLabelColor" => "white",
    "sendBtnLabel" => "Envoyer le message",
    "sendBtnBorderColor" => "#18BC9C",
    "sendBtnRadius" => "4",
    "sendBtnAlign" => "left",
    "sendBtnTextSize" => "15",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  ?>
<!-- ****************get image uploaded************** -->
<?php 
  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'block',
    ), "image"
  );

 ?>
<!-- ****************end get image uploaded************** -->

<style>
 
  .invalid-feedback {
    display: none;
    width: 100%;
    margin-top: 0.25rem;
    font-size: 80%;
    color: #dc3545;
  }
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .contact-email{
    text-transform : none !important;
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
    font-size: <?= $paramsData["subTitleBlockTextSize"] ?>pt;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    font-size:23px;
  }
  .container<?php echo $kunik ?> .p-img img{
    width: 100%;
    border:2px solid #bca87d;
  }
  .container<?php echo $kunik ?> .p-img{
    text-align: center
  }
  .container<?php echo $kunik ?> .btn-contact-me{
    background-color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
    color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnLabelColor"]; ?>;
    padding: 14px 36px;
    border-radius: <?php echo $paramsData["btnRadius"] ?>px !important;
    border:2px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $paramsData["btnBorderColor"]; ?>;
    font-size: large !important;
  }
  .container<?php echo $kunik ?> .btn-contact-me:hover{
      background-color:<?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["btnLabelColor"]; ?>;
      color:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
      border: 2px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["btnBgColor"]; ?>;
  }
  .<?php echo $kunik ?>.modal{
    margin-top: 91px;
  }
  .<?php echo $kunik ?>.modal .modal-body .fa{
    color :#bca87d;
  }
  .container<?php echo $kunik ?> .container-img{
      padding: 0 80px;
  }
  .container<?php echo $kunik ?> .container-img{
    background-color:white;
    <?php if (!empty($initImage)) { ?>
    background-image:url(<?php echo $initImage[0]["imageThumbPath"] ?>) !important;
    <?php  }else{ ?>
      background-image:url(<?php  echo isset($defaultImg)??$defaultImg ; ?>) !important;
    <?php  } ?>
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
    min-height: 413px;
    margin: 0 77px 0 58px;
  }
  @media (max-width:768px ){
    .container<?php echo $kunik ?> .container-img{
      margin:0 15px 50px 15px !important;
    }
  }
  @media (max-width:1000px ){
    .container<?php echo $kunik ?> .container-img{
      margin:0 15px 0px 15px;
    }
  }
  .label-contact {
    color: <?php echo $paramsData["labelColor"] ?>;
    font-size: <?php echo $paramsData["labelSize"] ?>px;
  }
  .contain-input {
    text-align: <?php echo $paramsData["labelAlign"] ?>;
  }
  <?php if ($paramsData["inputBorderType"]== "border-bottom") { ?>
  .form-control {
      border: 1px solid <?php echo $paramsData["inputOtheBorderColor"]?>;
  }
  <?php } ?>

  .contact-input {
    font-size: <?php echo $paramsData["inputSize"] ?>px;
    height: <?php echo $paramsData["inputHeight"] ?>px;
    border-radius: <?php echo $paramsData["inputRadius"] ?>px;
    <?php echo $paramsData["inputBorderType"] ?>: <?php echo $paramsData["inputBorder"] ?>px solid <?php echo $paramsData["inputBorderColor"] ?>;
    background-color: <?php echo $paramsData["inputBgColor"] ?>;
    color: <?php echo $paramsData["inputTextColor"] ?>;
  }

  .contact-input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
      color: <?php echo $paramsData["inputPlaceholderColor"] ?>;
      opacity: 1; /* Firefox */
    }

    .contact-input:-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: <?php echo $paramsData["inputPlaceholderColor"] ?>;
    }

    .contact-input::-ms-input-placeholder { /* Microsoft Edge */
      color: <?php echo $paramsData["inputPlaceholderColor"] ?>;
    }
    .sendbtn {
        color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["sendBtnLabelColor"]; ?>;
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["sendBtnBgColor"]; ?>;
        border: 2px solid <?php echo $paramsData["sendBtnBorderColor"] ?>;
        border-radius: <?php echo $paramsData["sendBtnRadius"] ?>px;
        font-size: <?php echo $paramsData["sendBtnTextSize"] ?>pt;
    }
    .sendbtn:hover {
        color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["sendBtnBgColor"]; ?>;
        background-color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $paramsData["sendBtnLabelColor"]; ?>;
        border: 2px solid <?php echo $paramsData["sendBtnBorderColor"] ?>;
    }
    .contain-sendbtn {
      text-align: <?php echo $paramsData["sendBtnAlign"] ?>;
    }


.container<?php echo $kunik ?> #messageAfterSend i {
  color: <?php echo $paramsData["labelColor"] ?>;
  font-size: 70px;
  width: 100%;
  display: block;
}
.container<?php echo $kunik ?> #messageAfterSend p {
  font-size: 25px;
  width: 100%;
  margin-top: 22px;
  text-align: center;
    color: <?php echo $paramsData["labelColor"] ?>;
}
.container<?php echo $kunik ?> #messageAfterSend{
  display: none;
  padding-top: 10%;
  justify-content: center;
  width: 100%;
}
.container<?php echo $kunik ?> #messageAfterSend .mess {
  width: 50%;
  box-shadow: 1px 1px 5px 1px <?php echo $paramsData["inputBorderColor"] ?>;
  padding: 3%;
}
@media (max-width:768px ){
  .container<?php echo $kunik ?> #messageAfterSend p {
    font-size: 18px;
  }
}
    .color-label {
        color: <?php echo $paramsData["labelColor"] ?>;
    }

</style>



<div class="container<?php echo $kunik ?> col-md-12">
  <h2 class="title-1"><?php echo $paramsData["titleBlock"] ?></h2>
  <?php if ($paramsData["showForm"] == "show") { ?>

  <div id="formContact" class="col-xs-12 col-lg-10 col-lg-offset-1" >

      <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left padding-top-60 form-group">
          
          <div class="col-xs-12">
          <h3 class="subTitle<?php echo $kunik ?>">
            <?php if ($paramsData["subTitleBlock"] !== "") { ?>
            <i class="fa fa-envelope color-label"></i>
            <small class="color-label"><?php echo $paramsData["subTitleBlock"] ?> </small>
            <span id="contact-name" style="text-transform: none!important;"></span><br>
            <?php } ?>
            
          </h3>
            <div>
              <span class="">
              <span class="color-label"><?php echo Yii::t('cms', 'This message will be sent to')?></span>
              <b><span class="contact-email color-label"></span></b>
            </span>
            </div>
          <hr><br>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
            <label for="email" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t("common","Your mail"); ?>*</label>
              <input type="email" class="form-control contact-input" placeholder="exemple@mail.com" id="emailSender">
              <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
            <label for="senderName" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Name / First name')?></label>
              <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'What is your name?')?>" id="senderName">
              <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
            <label for="objet" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?></label>
              <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject">
              <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>

          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
            <label for="objet" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Phone')?></label>
              <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'Phone')?>" id="phoneSender">
              <div class="invalid-feedback phoneSender"><?php echo Yii::t('cms', 'Invalid phone')?>.</div><br>

          </div>
        </div>
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group">
          <div class="col-md-12 contain-input">
            <label for="message" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your message')?></label>
            <textarea placeholder="Votre message..." class="form-control contact-input txt-mail" 
                  id="message" style="min-height: 200px;"></textarea>
            <div class="invalid-feedback message"><?php echo Yii::t('cms', 'Your message is empty')?>.</div><br>
            <div class="col-md-12 margin-top-15 contain-sendbtn">
              <button type="submit" class="btn sendbtn" id="btn-send-mail">
                <i class="fa fa-send"></i> <?php echo $paramsData["sendBtnLabel"] ?>
              </button>
            </div>
          </div>
        </div>
    
  </div>
  <div id="messageAfterSend" class="  messageAfterSend text-center  footer-arianelila" > 
    <div class="mess footer-arianelila">
      <i class="fa fa-check-circle"> </i>
      <p><?php echo Yii::t('cms', 'Email sent to')?> <?= isset($costum["admin"]["email"]) ? $costum["admin"]["email"] : ""?> !</p>
    </div>
  </div>
  

  <?php }else {?>
  <div class="col-sm-12 col-md-12 text-center padding-top-20">
      <a href="javascript:;" class="tooltips btn openFormContact btn btn-contact-me" data-toggle="modal" data-target="#myModal-contact-us"
      data-id-receiver="" 
      data-email=""
      data-name="">
          <?php echo $paramsData["btnLabel"] ?>
      </a>     
  </div>
    
<div class="portfolio-modal modal fade" id="formContact" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="sp-cms-container bg-white">

      <div id="form-group-contact">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left padding-top-60 form-group">
          <h3>
            <i class="fa fa-send letter-blue"></i> 
            <small class="letter-blue">
                <?php echo Yii::t('cms', 'Send an e-mail to')?> : </small>
            <span id="contact-name" style="text-transform: none!important;"></span>
            <br>
            <small class="">
              <small class=""><?php echo Yii::t('cms', 'This message will be sent to')?>
              </small>
              <b><span class="contact-email"></span></b>
            </small>
          </h3>
          <hr><br>
          <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
            <label for="email" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Your e-mail address')?>*</label>
              <input type="email" class="form-control contact-input" placeholder="exemple@mail.com" id="emailSender">
              <div class="invalid-feedback emailSender"><?php echo Yii::t('cms', 'Invalid email')?>.</div><br>
          </div>

          <div class="col-md-6 col-sm-6 col-xs-12 contain-input">
            <label for="senderName" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Name / First name')?></label>
              <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'What is your name?')?>" id="senderName">
              <div class="invalid-feedback senderName"><?php echo Yii::t('cms', 'Required field')?>.</div><br>
          </div>

          <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
            <label for="objet" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Subject of your message')?></label>
              <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'what is it about?')?>" id="subject">
              <div class="invalid-feedback subject"><?php echo Yii::t('cms', 'Required object')?>.</div><br>

          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 contain-input">
            <label for="objet" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Phone')?></label>
              <input class="form-control contact-input" placeholder="<?php echo Yii::t('cms', 'Phone')?>" id="phoneSender">
              <div class="invalid-feedback phoneSender"><?php echo Yii::t('cms', 'Invalid phone')?>.</div><br>

          </div>
        </div>
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-left form-group">
          <div class="col-md-12 contain-input">
            <label for="message" class="label-contact"><i class="fa fa-angle-down"></i> <?php echo Yii::t('cms', 'Image')?>Votre message</label>
            <textarea placeholder="<?php echo Yii::t('cms', 'Your message')?>..." class="form-control contact-input txt-mail"
                  id="message" style="min-height: 200px;"></textarea>
            <div class="invalid-feedback message"><?php echo Yii::t('cms', 'Your message is empty')?>.</div><br>
            </div>
            <div class="margin-top-15 contain-sendbtn">
              <button type="submit" class="btn sendbtn" id="btn-send-mail">
                <i class="fa fa-send"></i> <?php echo $paramsData["sendBtnLabel"] ?>
              </button>
            </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php }?>

 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog", 
            "properties" : {
                "titleBlock" : {
                  "label" : "<?php echo Yii::t('cms', 'Block title')?>",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleBlock
                },
                "subTitleBlock" : {
                  "label" : "<?php echo Yii::t('cms', 'Subtitle of the block')?>",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitleBlock
                },
                "subTitleBlockTextSize" : {
                  "label" : "<?php echo Yii::t('cms', 'Text size')?>",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitleBlockTextSize
                },
                "showForm":{ 
                  "label" : "<?php echo Yii::t('cms', 'Type of display form')?> ",
                  inputType : "select",
                  options : {              
                    "modal " : "<?php echo Yii::t('cms', 'Modal')?>",
                    "show" : "<?php echo Yii::t('cms', 'Show direct')?>"
                  },
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.showForm
                },
                "btnLabel" : {
                  "label" : "<?php echo Yii::t('cms', 'Label')?>",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabel
                },
                "btnLabelColor":{
                  label : "<?php echo Yii::t('cms', 'Color of the label')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnLabelColor
                },
                "btnBgColor":{
                  label : "<?php echo Yii::t('cms', 'Color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBgColor
                },
                "btnBorderColor":{
                  label : "<?php echo Yii::t('cms', 'Border color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnBorderColor
                },
                "btnRadius" : {
                  "label" : "<?php echo Yii::t('cms', 'Border radius')?> (px)",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnRadius
                },
                "labelColor" :{
                  label : "<?php echo Yii::t('cms', 'Color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelColor
                },
                "labelSize" : {
                  "label" : "<?php echo Yii::t('cms', 'Size')?>",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelSize
                },
                "labelAlign":{ 
                  "label" : "<?php echo Yii::t('cms', 'Alignment')?> ",
                  inputType : "select",
                  options : {              
                    "left" : "<?php echo Yii::t('cms', 'Left')?>",
                    "center" : "<?php echo Yii::t('cms', 'Center')?>",
                    "right" : "<?php echo Yii::t('cms', 'Right')?>"
                  },
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.labelAlign
                },
                "inputSize" : {
                  "label" : "<?php echo Yii::t('cms', 'Text size')?>",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputSize
                },
                "inputHeight" : {
                  "label" : "<?php echo Yii::t('cms', 'Height')?>",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputHeight
                },
                "inputRadius" : {
                  "label" : "<?php echo Yii::t('cms', 'Border radius')?> (px)",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputRadius
                },
                "inputBorderType":{ 
                  "label" : "<?php echo Yii::t('cms', 'Type of display form')?> ",
                  inputType : "select",
                  options : {              
                    "border-bottom" : "<?php echo Yii::t('cms', 'Border bottom')?>",
                    "border" : "<?php echo Yii::t('cms', 'Simple border')?>"
                  },
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.showForm
                },
                "inputBorder" : {
                  "label" : "<?php echo Yii::t('cms', 'Border thickness')?>",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputBorder
                },
                "inputBorderColor" : {
                  label : "<?php echo Yii::t('cms', 'Border color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputBorderColor
                },
                "inputOtheBorderColor" : {
                    label : "<?php echo Yii::t('cms', 'Other border color')?>",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputOtheBorderColor
                },
                "inputBgColor" : {
                  label : "<?php echo Yii::t('cms', 'Background color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputBgColor
                },
                "inputTextColor" :{
                  label : "<?php echo Yii::t('cms', 'Text color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputTextColor
                },
                "inputPlaceholderColor" :{
                  label : "<?php echo Yii::t('cms', 'Placeholder color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.inputPlaceholderColor
                },
                "sendBtnLabel" : {
                  "label" : "<?php echo Yii::t('cms', 'Label')?>",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnRadius
                },
                "sendBtnBgColor" :{
                  label : "<?php echo Yii::t('cms', 'Background color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.sendBtnBgColor
                },
                "sendBtnLabelColor" :{
                  label : "<?php echo Yii::t('cms', 'Label color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.sendBtnLabelColor
                },
                "sendBtnTextSize" : {
                  "label" : "<?php echo Yii::t('cms', 'Text size')?>",
                  inputType : "text",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.sendBtnTextSize
                },
                "sendBtnBorderColor" :{
                  label : "<?php echo Yii::t('cms', 'Border color')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.sendBtnBorderColor
                },
                "sendBtnRadius" : {
                  "label" : "<?php echo Yii::t('cms', 'Border radius')?> (px)",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.btnRadius
                },
                "sendBtnAlign" :{ 
                  "label" : "<?php echo Yii::t('cms', 'Alignment')?>",
                  inputType : "select",
                  options : {              
                    "left" : "<?php echo Yii::t('cms', 'Left')?>",
                    "center" : "<?php echo Yii::t('cms', 'Center')?>",
                    "right" : "<?php echo Yii::t('cms', 'Right')?>"
                  },
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.sendBtnAlign
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"btn",3,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","green","");
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"input",4,6,null,null,"<?php echo Yii::t('cms', 'Property of the form input')?>","green","");
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"label",4,6,null,null,"<?php echo Yii::t('cms', 'Property of the label of the form')?>","green","");
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"send",4,6,null,null,"<?php echo Yii::t('cms', 'Property of the label of the form')?>","green","");
          if ($("#showForm").val() == "show") {
            $('.fieldsetbtn').fadeOut();
          }
          $("#showForm").change(function(){
            if ($(this).val() == "show") {
              $('.fieldsetbtn').fadeOut();
            }else
            $('.fieldsetbtn').fadeIn();
          })
        });



<?php // ***************************send mail************************************* ?>
if (notNull(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      $("#formContact .contact-email").html(costum.admin.email);
    }
  $("#btn-send-mail").click(function(){
    sendEmail<?= $kunik ?>();
  });

  $('#emailSender').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#name').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
  $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});

  $(".container<?php echo $kunik ?> .openFormContact").click(function(){
    mylog.log("openFormContact");
    if (notNull(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
      //$("#formContact .contact-email").html(costum.admin.email);
      $("#formContact").modal("show");
    }else{
      bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
    }
  })
});
function validateEmail<?= $kunik ?>(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function sendEmail<?= $kunik ?>(){
  var acceptFields = true;
  $.each(["emailSender","senderName","subject","message"],(k,v)=>{
      if($("#"+v).val() == ""){
        $("."+v).show();
        acceptFields=false
      }
      if(validateEmail<?= $kunik ?>($("#emailSender").val())==false){
        acceptFields=false;
        $(".emailSender").show();
      }
      if(isNaN($("#phoneSender").val())) {
        acceptFields=false;
        $(".phoneSender").show();
      }

  });
  var seconds = Math.floor(new Date().getTime() / 1000);
  var allowedTime = localStorage.getItem("canSend");
  if(acceptFields){
    /*if(seconds < allowedTime){
      return bootbox.alert("<p class='text-center text-dark'>Renvoyer après "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
    }*/
    localStorage.removeItem("canSend");
        var emailSender = $("#emailSender").val();
        var phoneSender = $("#phoneSender").val();
        var subject = $("#subject").val();
        var senderName = $("#senderName").val();
        var message = $("#message").val()+"<br /> <?php echo Yii::t('cms', 'Phone')?><: "+phoneSender;
        var emailFrom = $(".contact-email").html();

        var params = {
          tpl : "contactForm",
          tplMail : emailFrom,
          fromMail: emailSender, 
          tplObject:subject,
          subject :subject, 
          names:senderName,
          emailSender:emailSender,
          message : message,
          sign : senderName+'<br>Téléphone: <a href="tel:'+phoneSender+'">'+phoneSender+'</a>',
          logo:"",
        };

        ajaxPost(
          null,
          baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
          params,
              function(data){ 
                if(data.result == true){
                  localStorage.setItem("canSend", (seconds+300));
                  toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                  //bootbox.alert("<p class='text-center text-green-k'>Email envoyé à "+emailFrom+" !</p>");
                    $("#formContact").hide();
                    $(".messageAfterSend").css("display" , "flex");
               
                  $.each(["emailSender","senderName","subject","message","phoneSender"],(k,v)=>{$("#"+v).val("")})
                }else{
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                  bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
                }

              },
              function(xhr, status, error){
                  toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
              }
        );  
  }
}
</script>

