<?php 
	if(!isset($costum)){
		$costum = CacheHelper::getCostum();
	}

	$keyTpl = "newsLetterSubscription";

	$paramsData = [
		'blockTitle' => "Recevez continuellement les dernières infos sur ".$costum['title'],
		'blockSubTitle' => "Inscrivez-vous à notre newsletters",
		'btnText' => "S'INSCRIRE MAINTENANT",
		'btnColor' => "#444",
		'btnBorder' => "#444",
		'btnBackground' => "transparent",
		'blockCmsBgColor' => "transparent",
		"blockCmsImgBg" => Yii::app()->getModule("costum")->assetsUrl."/images/blockCmsImg/news/block-newsletter-bg.png"
	];

	if (isset($blockCms)) {
    	foreach ($paramsData as $e => $v) {
      		if (isset($blockCms[$e]) ) {
            	$paramsData[$e] = $blockCms[$e];
      		}
    	}
  	}

	if(isset($blockCms["blockCmsBgImg"])){
		$paramsData["blockCmsImgBg"] = "";
	}
?>

<style type="text/css">
	.btn-subscribe<?= $kunik ?>{
		border:  3px solid <?= $paramsData["btnBorder"] ?>;
		color: <?= $paramsData["btnColor"] ?>;
		padding: .6em 3em !important;
		background: <?= $paramsData["btnBackground"] ?>;
		font-weight: bolder;
		transition-property: all;
		transition-duration: 1s;
	}

	#email<?= $kunik ?>{
		border-left: none;
	}

	.icon<?= $kunik ?>{
		padding: 0px 1em 0px 1.5em;
	}

	.intro-header<?= $kunik ?>{
		height: calc(100vh - 79px);
		padding-top: 3em ;
		line-height: 1.5 !important;
		background: url(<?= $paramsData["blockCmsImgBg"] ?>);
		background-position:center;
		background-size:cover;
		color:#444
	}
</style>

<div class="intro-header<?= $kunik ?>"> 
	<div class="container"  align="center">
		<div class="tab-content custom-tab-content" align="center">
			<div class="subscribe-panel padding-top-20 padding-bottom-20">
				<h1 class="sp-text" data-id="<?= $blockKey ?>" data-field="blockTitle"><?= $paramsData["blockTitle"]; ?></h1>
				<br/>
				<div class="sp-text" data-id="<?= $blockKey ?>" data-field="blockSubTitle"><?= $paramsData["blockSubTitle"] ;?></div>
				<br/>
				<div class="container">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon bg-white icon<?= $kunik ?>"> <i class="fa fa-envelope" aria-hidden="true"></i></span>
								<input type="text" class="form-control input-lg" name="email" id="email<?= $kunik ?>"  placeholder="Votre Adresse Mail"/>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
                <br/><br/>
                <button class="btn btn-subscribe<?= $kunik ?> btn-lg padding-left-20 padding-right-20"><?= $paramsData["btnText"]; ?></button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	$(document).ready(function(){

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {
                "btnColor" :{
                    "inputType" : "colorpicker",
                    "label" : "Couleur de texte",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.btnColor,
                },
                "btnBorder" :{
                    "inputType" : "colorpicker",
                    "label" : "Couleur de border",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.btnBorder,
                },
                "btnBackground" :{
                    "inputType" : "colorpicker",
                    "label" : "Couleur d'arrière plan",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.btnBackground,
                },
				"blockCmsImgBg" :{
                    "inputType" : "uploader",
                    "label" : "Image de fond",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.blockCmsImgBg,
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                tplCtx.value[k] = $("#"+k).val();
              });
              if(typeof tplCtx.value == "undefined"){
                toastr.error('value cannot be empty!');
              }else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }
            }
          }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btnAnnuaire",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"legend",6,12,null,null,"Préférence sur le légende","green","");
        });

		$(".btn-subscribe<?= $kunik ?>").on("click", function(){
			if($("#email<?= $kunik ?>").val()==""){
				toastr.error("Adresse mail invalide, Veuillez entrer une email valide");
			}else{
				let paramsData = {
					contextId : costum.contextId,
					contextType : costum.contextType,
					mailToSubscribe : $("#email<?= $kunik ?>").val(),
					userName : "<?= (isset($_SESSION['user']) && isset($_SESSION['user']['name']))?$_SESSION['user']['name']:'' ?>"
				}

				ajaxPost(
					null,
					baseUrl+"/costum/tierslieuxgenerique/subscribetonewsletter",
					paramsData,
					function(response){
						if(typeof response.ok != "undefined"){
							toastr[response.status](response.msg);
						}
						$("#email<?= $kunik ?>").val("");
					}
				)
			}
		})
	})
</script>