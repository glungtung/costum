<?php
$keyTpl     = "tab";
$myCmsId    = $blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$tabColor = "#7cb927";
$params     = array();
$paramsData = [
    "dataSource" => [$costum["contextId"] => ["name" => $costum["contextSlug"], "type" => $costum["contextType"]]],
    "tabColor" => $tabColor
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$superCmsList = [];
foreach($cmsList as $e => $v){
  if(isset($v["dontRender"]) && $v["dontRender"] == "true"){
    $superCmsList[ (string) $v["_id"]] = $v;
  }
}
?>
    <style>
    a:hover, a:focus{
			text-decoration: none;
			outline: none;
		}
		.text-green-theme{
			color: <?= $paramsData["tabColor"] ?>;
		}

		.nav-tabs > li {
		    float:none;
		    display:inline-block;
		    zoom:1;
		}

		.nav-tabs {
		    text-align:center;
        text-transform: uppercase;
		}

		#obs-content .tabbable-obs-panel {
      border:1px solid #eee;
      padding: 10px;
    }

     /* Default mode */
     #obs-content .tabbable-obs-line > .nav-tabs {
         border: none;
         margin: 0px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li {
         margin-right: 2px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li > a {
         border: 0;
         margin-right: 0;
         color: #737373;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li > a > i {
         color: #a6a6a6;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li {
         display: inline-block;
         color: #000;
         text-decoration: none;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li::after {
         content: '';
         display: block;
         width: 0;
         height: 4px;
         background: <?= $paramsData["tabColor"] ?>;
         margin-bottom: -4px;
         transition: width .3s;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li:hover::after {
         width: 100%;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li.open {
         border-bottom: 3px solid <?= $paramsData["tabColor"] ?>;
     }
     
     #obs-content .tabbable-obs-line > .nav-tabs > li.open > a, #obs-content .tabbable-obs-line > .nav-tabs > li:hover > a {
         border-bottom: 2px solid <?= $paramsData["tabColor"] ?>;
         background: none !important;
         color: #333333;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.open > a > i, #obs-content .tabbable-obs-line > .nav-tabs > li:hover > a > i {
         color: #a6a6a6;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.open .dropdown-menu, #obs-content .tabbable-obs-line > .nav-tabs > li:hover .dropdown-menu {
         margin-top: 0px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active {
         border-bottom: 3px solid <?= $paramsData["tabColor"] ?>;
         position: relative;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active > a {
         border: 0;
         color: #333333;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active > a > i {
         color: #404040;
     }
     .media{
       border: 1px solid #eee;
       padding: 0.5em;
     }
     #obs-content .tabbable-obs-line > .tab-content {
      margin-top: -3px;
        background-color: #fff;
         border: 0;
         border-top: 1px solid #eee;
         padding: 13px 0;
     }
     #obs-content .tab-pane{
       overflow: hidden;
     }
        .btn-add-tab{
            border: 1px solid #ddd!important;
            border-bottom-color: transparent!important;
        }
        .btn-close-tab{
            cursor: pointer;
            border: 1px solid transparent;
            padding: 2px;
            border-radius: 3px;
        }
        .btn-close-tab:hover{
            border-color: #ddd;
        }
        .btn-edit-tab{
            font-size: 14px;
            cursor: pointer;
        }
    </style>

<div id="tab-<?= $kunik ?>">
    <div id="obs-content" class="container-fluid" style="margin-bottom: 50px;">
   		<div class="carto-n row">
   			<div class="col-md-12">
   				<div class="tabbable-obs-panel">
   					<div class="tabbable-obs-line">
            <ul class="nav nav-tabs">
                <?php if(isset($blockCms["tabs"])){
                  $tabsGraph = [];
                  function getAllGraphBlocs($parentSuperCms)
                  {
                    $allContainerAndGraphBlocks = PHDB::find("cms", array("type" => "blockChild" , "path" => ["\$in" => ["tpls.blockCms.graph.graph", "tpls.blockCms.superCms.container"]], "tplParent" => $parentSuperCms ));
                    $graphBlocks = [];
                    foreach($allContainerAndGraphBlocks as $blockId => $blockValue){
                      if($blockValue["path"] == "tpls.blockCms.superCms.container"){
                        $tmpGraphBlocks = getAllGraphBlocs($blockId);
                        foreach($tmpGraphBlocks as $k => $v){
                          array_push($graphBlocks, $v);
                        }
                      }else if($blockValue["path"] == "tpls.blockCms.graph.graph"){
                        array_push($graphBlocks, $blockId);
                      }
                    }
                    return $graphBlocks;
                  }
                  $sorted = $blockCms["tabs"];
                  function sortByOrder($a, $b)
                  {
                    return (isset($a["order"]) ? $a["order"] : 99999)  > (isset($b["order"]) ? $b["order"] : 99999) ? 1 : -1;
                  }
                  uasort($sorted, "sortByOrder");
                  foreach($sorted as $idSuperCms => $value){
                    if(!isset($superCmsList[$idSuperCms])){
                      continue;
                    }
                    $tabsGraph[$idSuperCms] = getAllGraphBlocs($idSuperCms);
                    ?>
                      <li data-title="<?= isset($value["title"]) ? $value["title"] : "untitled" ?>" data-id="<?= $idSuperCms ?>">
                        <a href="#<?= $idSuperCms ?>" data-toggle="tab"><?= isset($value["title"]) ? $value["title"] : "untitled" ?> 
                        <?php if(Authorisation::isInterfaceAdmin()){ ?>
                          <span class="hiddenPreview" style="margin: 0 15px;"></span>
                          <i class="handle fa fa-arrows text-primary hiddenPreview"></i>
                          <i class="fa fa-pencil btn-rename-tab text-success hiddenPreview"></i>
                          <i class="fa fa-close btn-close-tab hiddenPreview"></i>
                        <?php } ?>
                        </a>
                      </li>
                    <?php
                  }
                } ?>
                <?php if(Authorisation::isInterfaceAdmin()){ ?>
                  <li class="hiddenPreview"><a href="#" class="btn-add-tab"><i class="fa fa-plus"></i></a></li>
                <?php } ?>
            </ul>
        <div class="tab-content">
        <?php if(isset($blockCms["tabs"])){
          $params = [];
          foreach($sorted as $idSuperCms => $value){
            if(!isset($superCmsList[$idSuperCms])){
              continue;
            }
            ?>
              <div class="tab-pane fade" id="<?= $idSuperCms ?>">
                <?php 
                  $content = isset($superCmsList[$idSuperCms]['content']) ? $superCmsList[$idSuperCms]['content'] : [];
                  $params[$idSuperCms] = [
                    "cmsList"   =>  $cmsList,
                    "blockKey"  =>  $idSuperCms,
                    "blockCms"  =>  $superCmsList[$idSuperCms],
                    "page"      =>  $page,
                    "canEdit"   =>  $canEdit,
                    "type"      =>  "tpls.blockCms.superCms.container",
                    "kunik"     =>  "container" . $idSuperCms,
                    "content"   =>  $content,
                    "el"        => $el,
                    'costum'    => $costum,
                    "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg"
                ]; 
                echo $this->renderPartial("costum.views.tpls.blockCms.superCms.container", $params[$idSuperCms]); 
              ?>
              </div>
            <?php
          }
        } ?>
        </div>
             </div>
           </div>
         </div>
       </div>
    </div>
</div>

<script>
  jQuery(document).ready(function() {
    contextData = <?php echo json_encode($el); ?>;
    var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
    var contextType = <?php echo json_encode($costum["contextType"]); ?>;
    var contextName = <?php echo json_encode($el["name"]); ?>;
    contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
    if(!window.initedGraph){
      window.initedGraph = [];
    }
    var tabsGraph<?= $kunik ?> = <?= json_encode($tabsGraph) ?>;
    const toShow = sessionStorage.getItem("tabToShow<?= $kunik ?>");
    if(toShow && $(`#tab-<?= $kunik ?> li[data-id="${toShow}"]`).length > 0){
      $(`#tab-<?= $kunik ?> .nav.nav-tabs li[data-id="${toShow}"] a[data-toggle="tab"]`).tab('show');
      sessionStorage.removeItem("tabToShow<?= $kunik ?>");
    }else{
      $('#tab-<?= $kunik ?> .nav.nav-tabs a[data-toggle="tab"]:first').tab('show');
    }
    $('#tab-<?= $kunik ?> .nav.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      for (const graph of tabsGraph<?= $kunik ?>[$(this).attr("href").substr(1)]) {
        if(!window.initedGraph.includes("graph"+graph)){
          var graphKUnik = "graph" + graph;
          window.initedGraph.push(graphKUnik);
          window["initGraph"+graphKUnik]();
        }
      }
    });
  });
  $(() => {
      $("#tab-<?= $kunik ?> .btn-add-tab").off().on('click', addTab<?= $kunik ?>)
      $("#tab-<?= $kunik ?> .btn-close-tab").off().on('click', removeTab<?= $kunik ?>);
      $("#tab-<?= $kunik ?> .btn-rename-tab").off().on('click', renameTab<?= $kunik ?>);

      $("#tab-<?= $kunik ?> .nav.nav-tabs").sortable({
        cursor: "move",
        handle: ".handle",
        items: "> li[data-id]",
        stop: function(event, ui) {
          const rawOrder = $("#tab-<?= $kunik ?> .nav.nav-tabs").sortable("toArray", {attribute: "data-id"})
          const order = {}
          for (const [k, v] of Object.entries(rawOrder)) {
            order["tabs."+v+".order"] = k;
          }
          toSave = {};
          toSave.id = "<?= (string) $blockCms["_id"] ?>";
          toSave.collection = "cms";
          toSave.path = "allToRoot";
          toSave.value = {};
          toSave.value = order;
          dataHelper.path2Value( toSave, function(res) {
            toastr.success("Position saved");
          })
        }
      })

  })
  function renameTab<?= $kunik ?>(params) {
      const li = $(this).closest("li");
      const superCmsId = li.data("id");
      bootbox.prompt({
        value: li.data("title"),
        title: "Tab title", 
        callback: function (title) {
          if(!title) return;
          toSave = {};
          toSave.id = "<?= (string) $blockCms["_id"] ?>";
          toSave.collection = "cms";
          toSave.path = "allToRoot";
          toSave.value = {};
          toSave.value["tabs."+superCmsId+".title"] = title;
          dataHelper.path2Value( toSave, function(res) {
            const currentTabId = $('#tab-<?= $kunik ?> .nav.nav-tabs li.active').data('id');
            sessionStorage.setItem("tabToShow<?= $kunik ?>", currentTabId);
            toastr.success("Tab bien ajouté");
            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
            // urlCtrl.loadByHash(location.hash);
          })
      }})
  }
  function addTab<?= $kunik ?>(e) {
      e.stopPropagation();
      e.preventDefault();
      createSuperCms<?= $kunik ?>();
  }
  function createSuperCms<?= $kunik ?>() {
      tplCtx = {};
      tplCtx.collection = "cms";
      tplCtx.path = "allToRoot";
      tplCtx.value = {
          name: "Super CMS in Tab",
          type: "blockCopy",
          path: "tpls.blockCms.superCms.container",
          page: pageCms,
          dontRender: true
      };

      tplCtx.value.parent = {};

      tplCtx.value.parent[contextId] = {
          type: contextType,
          name: contextSlug
      };
      tplCtx.value.haveTpl = false;
      if (sessionStorage.getItem("parentCmsIdForChild") != null) {
        tplCtx.value.tplParent = sessionStorage.getItem("parentCmsIdForChild");
        tplCtx.value.type = "blockChild";
        delete tplCtx.value.haveTpl;
      }
      if(typeof tplCtx.value == "undefined")
        toastr.error("value cannot be empty!");
      else {
        dataHelper.path2Value( tplCtx, function(params) {
          if(params.result){
            const superCmsId = params.saved.id;
            sessionStorage.setItem("tabToShow<?= $kunik ?>", superCmsId);
            toSave = {};
            toSave.id = "<?= (string) $blockCms["_id"] ?>";
            toSave.collection = "cms";
            toSave.path = "allToRoot";
            toSave.value = {};
            toSave.value["tabs."+superCmsId] = {
              title: "untitled"
            },
            dataHelper.path2Value( toSave, function(res) {
              toastr.success("Tab bien ajouté");
              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
              // urlCtrl.loadByHash(location.hash);
            })
          }
          else 
            toastr.error(params.msg);
        } );
      }
  }
  function removeTab<?= $kunik ?>(e){
      e.stopPropagation();
      e.preventDefault();
      var id = $(this).closest("li").data("id");
      var type = "cms";
      var urlToSend = baseUrl+"/co2/cms/delete/id/"+id;
      bootbox.confirm(trad.areyousuretodelete,
        function(result){
          if (!result) {
            return;
          } 
          else {
            $.ajax({
              type: "POST",
              url: urlToSend,
              dataType : "json"
            })
            .done(function (data) {
              if ( data && data.result ) {
                toastr.success("Element effacé");
                $("#"+type+id).remove();
              } else {
               toastr.error("something went wrong!! please try again.");
             }
             const currentTabId = $('#tab-<?= $kunik ?> .nav.nav-tabs li.active').data('id');
             if(id != currentTabId){
               sessionStorage.setItem("tabToShow<?= $kunik ?>", currentTabId);
             }
              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
            //  urlCtrl.loadByHash(location.hash);
           });
          }
        }
      );
  }
</script>


<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer le Tab",
        "description" : "Personnaliser votre tab",
        "icon" : "fa-cog",
        "properties" : {
          "tabColor": dyFInputs.colorpicker()
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
            if (k == "dataSource") {
              tplCtx.value[k] = formData.dataSource;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>