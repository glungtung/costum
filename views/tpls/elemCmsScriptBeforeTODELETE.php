<script type="text/javascript">
jQuery(document).ready(function() {
dyFInputs.defaultProperties = {
        description : dyFInputs.textarea( tradDynForm.longDescription, "...",null,true),
        location : dyFInputs.location,
        tags :dyFInputs.tags(),
        urls : dyFInputs.urls,
        image : dyFInputs.image(),
        shortDescription : dyFInputs.textarea(tradDynForm.shortDescription, "...",{ maxlength: 140 }),
        public : dyFInputs.checkboxSimple("true", "public", 
                { "onText" : trad.yes,
                  "offText": trad.no,
                  "onLabel" : tradDynForm.public,
                  "offLabel": tradDynForm.private,
                  //"inputId" : ".amendementDateEnddatetime",
                  "labelText": tradDynForm.makeeventvisible+" ?",
                  //"labelInInput": "Activer les amendements",
                  "labelInformation": tradDynForm.explainvisibleevent
            }),
        organizer : {
                inputType : "finder",
                id : "organizer",
                label : tradDynForm.whoorganizedevent,
                initType: ["projects", "organizations"],
                multiple : true,
                openSearch :true,
            },
        allDay : dyFInputs.allDay(),
        startDate : dyFInputs.startDateInput("datetime"),
        endDate : dyFInputs.endDateInput("datetime"),
        scope : dyFInputs.scope()
    };
});
</script>