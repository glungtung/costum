<?php 
HtmlHelper::registerCssAndScriptsFiles(array( 
    '/js/form.js',
    ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$keyTpl = "coForm";

$paramsData = [];

$typeList = [
    'coForm' => "coForm",
    'coObservatoire' => "coObservatoire"
];

$formParentIds =PHDB::find( Form::COLLECTION,  array( "parent.".$this->costum["contextId"] => array('$exists'=>1) ) );

$formParentIdshtml = "";
foreach ($formParentIds as $kfid => $vfid) {
$name = isset($vfid["name"])?$vfid["name"]:"";
    $formParentIdshtml .= '<option value="'.$kfid.'" >'.$name.'</option>';
}

if($canEdit){ 
?>
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= $this->costum["contextId"]; ?>' data-key='<?php echo $keyTpl ?>'>
     Coform <i class="fa fa-pencil" aria-hidden="true"></i>
    </a>
<?php }?>
<style type="text/css">
    .customselectcontainer {
        width: 100% !important;
        margin: 0px !important;
    }
</style>

<script type="text/javascript">
var tplCtx={};

var coFormPageoption = [];

var terr = <?php echo json_encode($formParentIdshtml); ?>;

jQuery(document).ready(function() {

	cfbObj = formObj.init({});
    cfbObj.events.add(cfbObj);
	$('.createForm').off().click( function(){
		cfbObj.openForm( cfbObj.dynForm.form(cfbObj) )
	});

    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    mylog.log("sectionParamsData----------------", sectionDyf.<?= $keyTpl ?>ParamsData);
 /*pages : {
                    inputType : "properties",
                    label : "Page Statique(s)",
                    labelKey : "Icon",
                    labelValue : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.pages
                },*/
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configuration de la page Coform",
            "description" : "Choissisez quelle modules vous souhaitez activer et créer votre prorpre page statisque",
            "icon" : "fa-cog",
            "properties" : {  
                "name" : {
                    "label" : "Nom de la page",
                    "inputType" : "text",
                    "rules" : { "required" : true }
                },            
                "select" : {
                    "label" : "Séléctionner un questionnaire",
                    "inputType" : "custom",
                    "html" : `<div><div class=" form-group typeselect customselectcontainer"><select class="form-control" name="parentFormId" id="parentFormId" style="width: 100%;height:auto;" data-placeholder=""><?php echo $formParentIdshtml; ?></select></div><div>Si vous n'avez pas encore créé votre questionnaire, vous pouvez créer un ci-dessous</div><a href="javascript:;" class="createForm btn btn-sm btn-link bg-green-k  "><i class="fa fa-plus"></i> Nouveau Formulaire</a> </div>`
                },

                "type" : {
                    "label" : "Type de page",
                    "inputType" : "select",
                    "options" : <?php echo json_encode($typeList); ?>,
                    "rules" : { "required" : true }
                }

            },
            save : function (data) { 
                mylog.log("before",tplCtx);
                tplCtx.value = {};
                // tplCtx.value["formParentId"] = $('formParentId').val();
                // tplCtx.value["type"] = $('type').val();
                //university.view.forms.dir.answer.5ffc53831a2a2e0f008b4588.mode.w

                var nameKeyPage = "#"+$('#name').val().replace(/[^\w]/gi, '').toLowerCase(); 
                tplCtx.path = 'costum.app.'+nameKeyPage;
                tplCtx.collection = '<?= $this->costum["contextType"]; ?>';
                var newAnsId = "";

                ajaxPost(null, baseUrl+'/survey/answer/newAnswer/form/'+$('#parentFormId').val()+'/contextId/<?= $this->costum["contextId"]; ?>/contextType/'+tplCtx.collection ,null,function(data){ 
                        newAnsId = data["_id"]["$id"];

                        var hashform = "#answer.index.id."+newAnsId;

                        if (typeof $('#type').val() !== "undefined" && $('#type').val() == "coObservatoire") {
                            hashform = "#answer.dashboard.answer."+$('#parentFormId').val();
                        }

                        tplCtx.value= {
                            module : "survey", 
                            hash : hashform,
                            icon : "pencil",
                            useHeader : true,
                            isTemplate : true,
                            useFooter : true,
                            useFilter : false,
                            subdomainName : nameKeyPage.replace("#",""),
                            staticPage : true
                        }

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined" )
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) { 
                                $("#ajax-modal").modal('hide');
                                toastr.success(tplCtx.value["type"]+" bien ajouté");
                                // location.reload();
                            } );
                        }

                });

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        // tplCtx.collection = $(this).data("collection");
        // tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
            $('.createForm').off().click( function(){
                dyFObj.openForm( cfbObj.dynForm.form(cfbObj) );
            });
    });

});
</script>

