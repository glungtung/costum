<div class="form-group">
	<h4 style="color:<?php echo ($titleColor) ? $titleColor : "black"; ?>"><?php echo $label?></h4>
	<table class="table table-bordered table-hover  ">
		<thead>
			<tr>
				<th></th>
				<th>Nom</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td rowspan=2>
					<?php 
					$user = PHDB::findOne(Person::COLLECTION,["_id"=>new MongoId($answer["user"])]);
					$imgPath = (@$user["profilMediumImageUrl"] && !empty($user["profilMediumImageUrl"])) ? Yii::app()->createUrl('/'.$user["profilMediumImageUrl"]) : $this->module->getParentAssetsUrl().'/images/thumb/default_citoyens.png'; ?>  
					<img class="img-circle" height=70 width="70" src="<?php echo $imgPath ?>">
				</td>
				<td><a class="lbh-preview-element" data-hash='#page.type.citoyens.id.<?php echo (string)$answer["user"] ?>' href="@<?php echo Yii::app()->createUrl($user["username"]); ?>"><?php echo @$user["name"]; ?></a></td>
			</tr>
		</tbody>
	</table>
</div>