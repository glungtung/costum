<?php 
    $keyTpl = "htmlConstruct";
    $paramsData = [

    ];
    if( isset($this->costum[$keyTpl]) )
        $paramsData =  $this->costum[$keyTpl];

?>

<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>'
        data-collection='<?= $this->costum["contextType"]; ?>'
        data-key='<?php echo $keyTpl ?>'
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-html5" aria-hidden="true"></i>
    </a>
<?php }?>
<style>
    .htmlConstruct.left:before{
        content:"Menu haut à gauche";
        text-transform: uppercase;
        /*border: 2px solid black;*/
        background-color: #008037;
        color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.right:before{
        content:"Menu haut à droite";
        text-transform: uppercase;
        /*border: 2px solid black;*/
        background-color: #2C3E50;
        color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.menuLeft:before{
        content:"Menu à gauche";
        text-transform: uppercase;
        /*border: 2px solid black;*/
        background-color: #742a09;
        color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.menuRightt:before{
        content:"Menu à droite";
        text-transform: uppercase;
       /*border: 2px solid black;*/
       background-color: #2441c1;
       color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.menuBottom:before{
        content:"Menu en bas";
        text-transform: uppercase;
       /*border: 2px solid black;*/
       background-color: #2561c1;
       color: #fff;
        padding: 8px;
        border-radius: 12px;
        margin-top: -20px;
    }
    .htmlConstruct.right{
        margin-top:78px;
        border: 8px solid #2C3E50;
    }
    .htmlConstruct.left{
        margin-top:78px;
        border: 8px solid #008037;
    }
    .htmlConstruct.menuLeft{
        margin-top:78px;
        border: 8px solid #742a09;
    }
    .htmlConstruct.menuRightt{
        margin-top:78px;
        border: 8px solid #2441c1;
    }
    .htmlConstruct.menuBottom{
        margin-top:78px;
        border: 8px solid #2561c1;
    }

    .htmlConstruct.left-xsMenu:before,
    .htmlConstruct.right-xsMenu:before{
        content:"xsMenu";
    }
    .htmlConstruct.left-app:before,
    .htmlConstruct.right-app:before,
    .htmlConstruct.menuLeft-app:before,
    .htmlConstruct.menuRightt-app:before,
    .htmlConstruct.menuBottom-app:before{

        content:"App (Menu)";
    }
    .htmlConstruct.left-dropdown:before,
    .htmlConstruct.right-dropdown:before{
        content:"Menu déroulant";
    }
    .htmlConstruct.left-buttons:before,
    .htmlConstruct.right-buttons:before,
    .htmlConstruct.menuLeft-buttons:before,
    .htmlConstruct.menuRightt-buttons:before,
    .htmlConstruct.menuBottom-buttons:before{
        content:"Bouttons";
    }
    .htmlConstruct.left-logo:before,
    .htmlConstruct.right-logo:before,
    .htmlConstruct.menuLeft-logo:before,
    .htmlConstruct.menuRightt-logo:before,
    .htmlConstruct.menuBottom-logo:before{
        content:"Logo";
    }
    .htmlConstruct.left-userProfil:before,
    .htmlConstruct.right-userProfil:before,
    .htmlConstruct.menuLeft-userProfil:before,
    .htmlConstruct.menuRightt-userProfil:before,
    .htmlConstruct.menuBottom-userProfil:before{
        content:"Profil d'utilisateur";
    }
    .checkbox-htmlConstruct input[type=checkbox]{
      -ms-transform: scale(2); /* IE */
      -moz-transform: scale(2); /* FF */
      -webkit-transform: scale(2); /* Safari and Chrome */
      -o-transform: scale(2); /* Opera */
      transform: scale(2);
      padding: 10px;
    }
    .checkbox-htmlConstruct .label-htmlConstruct{
        margin-top: 4px !important;
        margin-left: 15px !important;
        text-transform: none !important;
    }
</style>

<script>
var intiHtmlConstruction = function(){
$(".edit<?php echo $keyTpl ?>Params").append(tradCms.menuConstruction);

        var htmlConstructParamsData = <?php echo json_encode($paramsData) ?>;
        sectionDyf.<?php echo $keyTpl ?>ParamsData = htmlConstructParamsData;
    // console.log("htmlConstructParamsData",htmlConstructParamsData);
        //top left-------------------
        var hasMenuTopLeft = (exists(htmlConstructParamsData.header) && 
                            exists(htmlConstructParamsData.header.menuTop) &&
                            (htmlConstructParamsData.header.menuTop != null) &&
                            exists(htmlConstructParamsData.header.menuTop.left) &&
                            exists(htmlConstructParamsData.header.menuTop.left.buttonList));
        var optionsButtonListLeft = {};
        if(hasMenuTopLeft){
            var menuTopLeftPath = htmlConstructParamsData.header.menuTop.left.buttonList;
            var menuTopLeftThemeParamsPath = themeParams.header.menuTop.left.buttonList;
            mylog.log(menuTopLeftPath,"govalosy menuTopLeftPath");
            mylog.log(menuTopLeftThemeParamsPath,"govalosy menuTopLeftThemeParamsPath");
            
            if(exists(jsonHelper.getValueByPath(themeParams,"header.menuTop.left.buttonList.app.buttonList")) && jsonHelper.getValueByPath(themeParams,"header.menuTop.left.buttonList.app.buttonList").constructor.name=='Object')
            $.each(htmlConstructParamsData.header.menuTop.left.buttonList.app.buttonList,function(k,v){
                optionsButtonListLeft[k] = k;
            })
        }
        //------------------------------

        //top-right------------------
        var hasMenuTopRight = (exists(htmlConstructParamsData.header) && 
                            exists(htmlConstructParamsData.header.menuTop) &&
                            (htmlConstructParamsData.header.menuTop != null) &&
                            exists(htmlConstructParamsData.header.menuTop.right) &&
                            exists(htmlConstructParamsData.header.menuTop.right.buttonList));
        var optionsButtonListRight = {};
        if(hasMenuTopRight){
            var menuTopRightPath = htmlConstructParamsData.header.menuTop.right.buttonList;
            var menuTopRightThemeParamsPath = themeParams.header.menuTop.right.buttonList;
            if(exists(jsonHelper.getValueByPath(themeParams,"header.menuTop.right.buttonList.app.buttonList")) && jsonHelper.getValueByPath(themeParams,"header.menuTop.right.buttonList.app.buttonList").constructor.name=='Object')
            $.each(htmlConstructParamsData.header.menuTop.right.buttonList.app.buttonList,function(k,v){
                optionsButtonListRight[k] = k;
            })
        }
        //--------------------------------

        //menu left-----------------------
        var hasMenuLeft = (exists(htmlConstructParamsData.menuLeft) && 
                            exists(htmlConstructParamsData.menuLeft.buttonList));
        var optionsButtonListMenuLeft = {};
        if(hasMenuLeft){
            var menuLeftPath = htmlConstructParamsData.menuLeft.buttonList;
            var menuLeftThemeParamsPath = themeParams.menuLeft.buttonList;
            if(exists(jsonHelper.getValueByPath(themeParams,"menuLeft.buttonList.app.buttonList")) && jsonHelper.getValueByPath(themeParams,"menuLeft.buttonList.app.buttonList").constructor.name=='Object')
            $.each(htmlConstructParamsData.menuLeft.buttonList.app.buttonList,function(k,v){
                optionsButtonListMenuLeft[k] = k;
            })
        }
        //--------------------------------

        //menu right-----------------------
        var hasMenuRight = (exists(htmlConstructParamsData.menuRight) && 
                            exists(htmlConstructParamsData.menuRight.buttonList));
        var optionsButtonListMenuRight = {};
        if(hasMenuRight){
            var menuRightPath = htmlConstructParamsData.menuRight.buttonList;
            var menuRightThemeParamsPath = themeParams.menuRight.buttonList;
            if(exists(jsonHelper.getValueByPath(themeParams,"menuRight.buttonList.app.buttonList")) && jsonHelper.getValueByPath(themeParams,"menuRight.buttonList.app.buttonList").constructor.name=='Object')
            $.each(htmlConstructParamsData.menuRight.buttonList.app.buttonList,function(k,v){
                optionsButtonListMenuRight[k] = k;
            })
        }

        //menu bottom-----------------------
        var hasMenuBottom = (exists(htmlConstructParamsData.menuBottom) && 
                            exists(htmlConstructParamsData.menuBottom.buttonList));
        var optionsButtonListMenuBottom = {};
        if(hasMenuBottom){
            var menuBottomPath = htmlConstructParamsData.menuBottom.buttonList;
            var menuBottomThemeParamsPath = themeParams.menuBottom.buttonList;
            if(exists(jsonHelper.getValueByPath(themeParams,"menuBottom.buttonList.app.buttonList")) && jsonHelper.getValueByPath(themeParams,"menuBottom.buttonList.app.buttonList").constructor.name=='Object')
            $.each(htmlConstructParamsData.menuBottom.buttonList.app.buttonList,function(k,v){
                optionsButtonListMenuBottom[k] = k;
            })
        }

        //--------------------------------

        var checkBoxParams = {
                            "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                            "offText" : "<?php echo Yii::t('common', 'No')?>",
                            "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                            "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                            "labelText" : "label"
                        }
        //important variable menuProps
        var menuProps = {};

        //change themeParams seitting href
        if(notNull(userConnected))
            themeParams["mainMenuButtons"]["settings"]["href"] = "#@"+userConnected.slug+".view.settings";

        //get current app
        var optionsApp = {};
        // console.log("costum.app",costum.app);
        if (notNull(costum) && exists(costum.app))
            $.each(costum.app,function(k,v){
                optionsButtonListLeft[k] = k;
                optionsButtonListRight[k] = k;
                optionsButtonListMenuLeft[k] = k;
                optionsButtonListMenuRight[k] = k;
                optionsButtonListMenuBottom[k] = k;
            })

        //create dropdown options
        var dropdownOption = {}
        $.each(themeParams["mainMenuButtons"],function(k,v){
            dropdownOption[k] = k
        })
        mylog.log("dropdownOption",dropdownOption);
        delete dropdownOption["dropdown"];
        delete dropdownOption["xsMenu"];
        delete dropdownOption["app"];
        delete dropdownOption["#Drive"];
        delete dropdownOption["#Kaban"];
        delete dropdownOption["#Pad"];
        delete dropdownOption["#Video"];
        delete dropdownOption["#Visio"];
        delete dropdownOption["#tChat"];
            
        /****************begin function which generate menu properties***********/
        var generateMenuProps = function (key,ifExists,path,themeParamsPath=null){
                        if(key=="left") optionsApp = optionsButtonListLeft;
                        if(key=="right") optionsApp = optionsButtonListRight;
                        if(key == "menuLeft") optionsApp = optionsButtonListMenuLeft;
                        if(key == "menuRight") optionsApp = optionsButtonListMenuRight;
                        if(key == "menuBottom") optionsApp = optionsButtonListMenuBottom;
            $.each(themeParams["mainMenuButtons"],function(k,v){
                if(k=="#Drive" || ["#Drive","#Kaban","#Pad","#Video","#Visio","#tChat"].indexOf(k) > 0){
                    //alert(k);
                }else if(k != "app" && k!="xsMenu" && k!="dropdown" && k!="logo" && k!= "userProfil"){
                    setTimeout(function(){
                        var keys ="";
                        if(key =="left" || key == "right")
                            keys = "header-menuTop-"+key+"-buttonList-";
                        else if(key == "menuLeft" || key == "menuRight" || key == "menuBottom")
                            keys = key+"-buttonList-";
                         menuProps[keys+k] = {
                            "inputType" : "checkboxSimple",
                            "label" : k,
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k])) ? true : false
                        };
                    },100)
                }else if(k == "xsMenu" && key != "menuLeft" && key != "menuRight" && key != "menuBottom"){
                        var keys = "header-menuTop-"+key+"-buttonList-"+k+"-buttonList-app-";

                        menuProps[keys+"label"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show label')?>",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.label) ) ? path[k].buttonList.app.label : false
                        };
                        menuProps[keys+"icon"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show icon')?>",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.icon) ) ? path[k].buttonList.app.icon : false
                        };
                        menuProps[keys+"spanTooltip"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show tooltip')?>",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.spanTooltip) ) ? path[k].buttonList.app.spanTooltip : false
                        };
                        menuProps[keys+"keyListButton"] = {
                            "inputType" : "hidden",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.keyListButton) ) ? path[k].buttonList.app.keyListButton : "pages"
                        };
                        menuProps[keys+"class"] = {
                            "inputType" : isDeveloper ? "text" : "hidden",
                            "label" : isDeveloper ? "<?php echo Yii::t('cms', 'App Xs class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : "",
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.class) ) ? path[k].buttonList.app.class : ""
                        };

                        menuProps[keys+"labelClass"] = {
                            "inputType" : isDeveloper ? "text" : "hidden",
                            "label" : isDeveloper ? "<?php echo Yii::t('cms', 'Label class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : "",
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.labelClass) ) ? path[k].buttonList.app.labelClass : "padding-left-10"
                        };
                        menuProps[keys+"buttonList"] = {
                            "inputType" : "selectMultiple",
                            /*"list" : keys+"buttonList",
                            "optionsValueAsKey" : true,
                            "isSelect2" : true,*/
                            "noOrder" :true,
                            "label" : "<?php echo Yii::t('cms', 'Click, select and organize xs menus (drag and drop to organize)')?>",
                            "options" : optionsApp,
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList) && exists(path[k].buttonList.app) && exists(path[k].buttonList.app.buttonList) && notNull(path[k].buttonList.app.buttonList) ) ? Object.keys(path[k].buttonList.app.buttonList) : [], /*Object.keys(optionsApp)*/
                            dataValue : (ifExists && themeParamsPath!=null && exists(themeParamsPath[k]) && exists(themeParamsPath[k].buttonList) && exists(themeParamsPath[k].buttonList.app) && exists(themeParamsPath[k].buttonList.app.buttonList) ? themeParamsPath[k].buttonList.app.buttonList : {})
                        };                    
                }else if(k == "app"){
                        if(key =="left" || key == "right")
                            var keys = "header-menuTop-"+key+"-buttonList-"+k+"-";
                        else if(key == "menuLeft" || key == "menuRight" || key == "menuBottom")
                            var keys = key+"-buttonList-"+k+"-";

                        menuProps[keys+"label"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show label')?>",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].label)) ? path[k].label : false
                        };
                        menuProps[keys+"icon"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show icon')?>",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].icon)) ? path[k].icon : false
                        };
                        menuProps[keys+"spanTooltip"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show tooltip')?>",
                            "params" : checkBoxParams,
                            "checked" : (ifExists && path!=null && exists(path[k]) && exists(path[k].spanTooltip)) ? path[k].spanTooltip : false
                        };
                        menuProps[keys+"class"] = {
                            "inputType" : isDeveloper ? "text" : "hidden",
                            "label" : isDeveloper ? "<?php echo Yii::t('cms', 'App class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : "",
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].class)) ? path[k].class : false
                        };

                        menuProps[keys+"labelClass"] = {
                            "inputType" : isDeveloper ? "text" : "hidden",
                            "label" : isDeveloper ? "<?php echo Yii::t('cms', 'Label class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : "",
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].labelClass)) ? path[k].labelClass : "padding-left-10"
                        };

                        menuProps[keys+"buttonList"] = {
                            "inputType" : "selectMultiple",
                            /*"list" : keys+"buttonList",
                            "optionsValueAsKey" : true,
                            "isSelect2" : true,*/
                            "label" : "<?php echo Yii::t('cms', 'Click, select and organize menus (drag and drop to organize)')?>",
                            "noOrder" :true,
                            "options" : optionsApp,
                            value : (ifExists && path!=null && exists(path[k]) && notNull(path[k].buttonList)) ? Object.keys(path[k].buttonList) : []/*Object.keys(optionsApp)*/,
                            dataValue : (ifExists && themeParamsPath!=null && exists(themeParamsPath[k]) && exists(themeParamsPath[k].buttonList) ? themeParamsPath[k].buttonList : {})
                        };     

                }else if(k == "dropdown" && key !="menuLeft" && key !="menuRight" && key !="menuBottom"){
                    setTimeout(function(){
                        menuProps["header-menuTop-"+key+"-buttonList-"+k+"-buttonList"] = {
                            "inputType" : "selectMultiple",
                            "select2":true,
                            "label" : "<?php echo Yii::t('cms', 'Choose buttons')?>",
                             "options" : dropdownOption,
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].buttonList)) ? Object.keys(path[k].buttonList) : Object.keys(dropdownOption)
                        }; 
                    },200)       
                }else if(k == "logo"){
                        if(key =="left" || key == "right")
                            var keys = "header-menuTop-"+key+"-buttonList-"+k+"-";
                        else if(key == "menuLeft" || key == "menuRight" || key == "menuBottom")
                            var keys = key+"-buttonList-"+k+"-";

                        menuProps[keys+"width"] = {
                            "inputType" : "text",
                            "label" : "<?php echo Yii::t('cms', 'Width')?>",
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].width)) ? path[k].width : 50
                        };
                        menuProps[keys+"height"] = {
                            "inputType" : "text",
                            "label" : "<?php echo Yii::t('cms', 'Height')?>",
                            value : (ifExists && path!=null && exists(path[k]) && exists(path[k].height)) ? path[k].height : 50
                        };                 
                }else{
                    var j = "userProfil";
                     setTimeout(function(){
                        if(key =="left" || key == "right")
                            var keys = "header-menuTop-"+key+"-buttonList-"+j+"-";
                        else if(key == "menuLeft" || key == "menuRight" || key == "menuBottom")
                            var keys = key+"-buttonList-"+j+"-";

                        menuProps[keys+"name"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show name')?>",
                            "params" : checkBoxParams,
                            checked : (ifExists && path!=null && exists(path[j]) && exists(path[j].name)) ? path[j].name : false
                        };

                        menuProps[keys+"img"] = {
                            "inputType" : "checkboxSimple",
                            "label" : "<?php echo Yii::t('cms', 'Show image')?>",
                            "params" : checkBoxParams,
                            checked : (ifExists && path!=null && exists(path[j]) && exists(path[j].img)) ? path[j].img : false
                        };
                    },150)               
                }
            })

            //setTimeout(function(){
                if(key =="menuLeft")
                    menuProps[key+"-addClass"] = {
                        "inputType" : (isDeveloper ? "text" : "hidden"),
                        "placeholder" : "pos-top-0 fit-content padding-top-20",
                        "label" : (isDeveloper ? "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : ""),
                        value : (exists(htmlConstructParamsData.menuLeft) && exists(htmlConstructParamsData.menuLeft.addClass)) ?
                            htmlConstructParamsData.menuLeft.addClass : "pos-top-0 fit-content padding-top-20"
                    }
                if(key =="menuRight")
                    menuProps[key+"-addClass"] = {
                        "inputType" : (isDeveloper ? "text" : "hidden"),
                        "placeholder" : "pos-top-0 fit-content padding-top-20",
                        "label" : (isDeveloper ? "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : ""),
                        value : (exists(htmlConstructParamsData.menuRight) && exists(htmlConstructParamsData.menuRight.addClass)) ?
                            htmlConstructParamsData.menuRight.addClass : "fit-content"
                    }
                if(key =="menuBottom")
                    menuProps[key+"-addClass"] = {
                        "inputType" : (isDeveloper ? "text" : "hidden"),
                        "placeholder" : "pos-top-0 fit-content padding-top-20",
                        "label" : (isDeveloper ? "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>" : ""),
                        value : (exists(htmlConstructParamsData.menuBottom) && exists(htmlConstructParamsData.menuBottom.addClass)) ?
                            htmlConstructParamsData.menuBottom.addClass : "fit-content"
                    }

                if(key =="left"){
                        menuProps["header-menuTop-"+key+"-addClass"] = {
                            "inputType" : "text",
                            "placeholder" : "text-right col-sm-6 col-lg-7 navigator",
                            "label" : "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                            value : (exists(htmlConstructParamsData.header) && exists(htmlConstructParamsData.header.menuTop) && (htmlConstructParamsData.header.menuTop != null) && exists(htmlConstructParamsData.header.menuTop.left) && exists(htmlConstructParamsData.header.menuTop.left.addClass)) ?
                                htmlConstructParamsData.header.menuTop.left.addClass : ""
                        }
                }

                 if(key =="right")
                     menuProps["header-menuTop-"+key+"-addClass"] = {
                         "inputType" : "text",
                         "placeholder" : "",
                         "label" : "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                         value : (exists(htmlConstructParamsData.header) && exists(htmlConstructParamsData.header.menuTop) && (htmlConstructParamsData.header.menuTop != null) && exists(htmlConstructParamsData.header.menuTop.right) && exists(htmlConstructParamsData.header.menuTop.right.addClass)) ?
                             htmlConstructParamsData.header.menuTop.right.addClass : ""
                     }
           // },500)

                //buttons



            
        }
        /******************end function which generate properties***********/


        //choose menu**********************************
        var possibleMenu = "";
        if(!hasMenuTopLeft)
        possibleMenu += '<div class="checkbox checkbox-htmlConstruct">'+
          '<label><input type="checkbox" name="htmlConstruct" value="left"><h5 class="label-htmlConstruct">'+tradCms.topLeftMenu+'</h5></label>'+
        '</div>';
        if(!hasMenuTopRight)
        possibleMenu+= '<div class="checkbox checkbox-htmlConstruct">'+
          '<label><input type="checkbox" name="htmlConstruct" value="right"><h5 class="label-htmlConstruct">'+tradCms.topRightMenu+'</h5></label>'+
        '</div>';
        if(!hasMenuLeft)
        possibleMenu+= '<div class="checkbox checkbox-htmlConstruct">'+
          '<label><input type="checkbox" name="htmlConstruct" value="menuLeft"><h5 class="label-htmlConstruct">'+tradCms.menuLeft+'</h5></label>'+
        '</div>';
        if(!hasMenuRight)
        possibleMenu+='<div class="checkbox checkbox-htmlConstruct">'+
          '<label><input type="checkbox" name="htmlConstruct" value="menuRight"><h5 class="label-htmlConstruct">'+tradCms.menuRight+'</h5></label>'+
        '</div>';
        if(!hasMenuBottom)
        possibleMenu+='<div class="checkbox checkbox-htmlConstruct">'+
          '<label><input type="checkbox" name="htmlConstruct" value="menuBottom"><h5 class="label-htmlConstruct">'+tradCms.menuAtTheBottom+'</h5></label>'+
        '</div>';
        //choose menu**********************************
        if(hasMenuTopLeft && hasMenuTopRight && hasMenuLeft && hasMenuRight && hasMenuBottom){
            possibleMenu = "<div>Les menus des navigations sont ajoutés, vous pouvez les configurer en cliquant sur le bouton \"Gérer l'existant\" .</div>";
        }        

        /***************begin dyfObj*************************/
        var htmlConstructParams = {
            "jsonSchema" : {    
                "title" : tradCms.buildYourMenu,
                "icon" : "fa-cog",
                "properties" : menuProps,//important
                save : function (data) { 
                    tplCtx.value = {};
                    $.each( htmlConstructParams.jsonSchema.properties , function(k,val) {
                        var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                        var arrayKey = k.split("-");
                        var parsedPath = arrayKey.join("][");
                        var targetKey = arrayKey[arrayKey.length -1 ]; 

                        if(isActiveDivParent && $("#"+k).val() != "" && $("#"+k).val() != null){
                            if(arrayKey[4] != "app" && arrayKey[2] != "app" && arrayKey[4] != "xsMenu" && arrayKey[4] !="dropdown" && arrayKey[4] !="userProfil" && arrayKey[2] !="userProfil" && arrayKey[2] != "logo" && arrayKey[4] != "logo" && $("#"+k).val() != "false"){
                                 if(arrayKey[1] == "addClass" || arrayKey[3] == "addClass")
                                   tplCtx.value[parsedPath] = $("#"+k).val();
                                else if(arrayKey[2] == "right")                         
                                    tplCtx.value[parsedPath] = true;
                                else
                                    tplCtx.value[parsedPath] = themeParams["mainMenuButtons"][targetKey];
                            } 
                            else if(arrayKey[4] == "xsMenu" || arrayKey[3] == "addClass" || arrayKey[1] == "addClass" || arrayKey[4] == "app" || arrayKey[2] == "app" || arrayKey[4] == "dropdown" || arrayKey[4] == "logo" || arrayKey[2] == "logo" || arrayKey[4] =="userProfil" || arrayKey[2] =="userProfil" ){
                                if(targetKey == "buttonList"){
                                    if($("#"+k).val() !=null){
                                        var obj = {};
                                        $.each($("#"+k).val(),function(key,value){
                                            dataValue = $("#"+k +" option[value='"+value+"']" ).data("value");
                                            mylog.log(dataValue,"eoara");
                                            if(typeof dataValue.buttonList != "undefined" && Object.keys(dataValue.buttonList).length !== 0){
                                                obj[value] = {};
                                                obj[value].buttonList = {};
                                                $.each(dataValue.buttonList,function(kbl,vbl){
                                                    obj[value].buttonList[kbl] = true
                                                });
                                            }else
                                                obj[value] = true
                                        });
                                        if(Object.keys(obj).length != 0)
                                            tplCtx.value[parsedPath] = obj;
                                    }
                                }else if(arrayKey[4] == "xsMenu" || arrayKey[4] == "app" || arrayKey[2] == "app"){
                                        tplCtx.value[parsedPath] = $("#"+k).val();
                                }else{
                                    if( $("#"+k).val() != "false")
                                        tplCtx.value[parsedPath] = $("#"+k).val();
                                }
                            }
                        }
                        
                    });

                        mylog.log(tplCtx.value,"gisagisa");
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        tplCtx.updatePartial=true;tplCtx.removeCache=true;
                        tplCtx.format=true;
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                            location.reload();
                        } );
                    }
                }
            }
        };
        /**************end dyfObj ***********************/

        /********************Begin show dyFObj in on click *****************/
        $(document).on("click",".edit<?php echo $keyTpl ?>Params",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            bootbox.dialog({
                /*size: "small",*/
                title: '<h6 class="text-center">'+tradCms.addNavigationBar+'</h6>',
                message: possibleMenu,
                buttons: {
                    cancel: {
                        label: trad.cancel,
                        className: 'btn-danger',
                        callback: function(){
                            console.log('Custom cancel clicked');
                        }
                    },
                    noclose: {
                        label: tradCms.manageExisting,
                        className: 'bg-dark text-white',
                        callback: function(){
                            htmlConstructAddNewMenu(false,function(){
                                openHtmlConsctructForm(function(){
                                    callSelectSortable();
                                });
                            })
                        }
                    },
                    ok: {
                        label: trad.Add,
                        className: 'text-white bg-green-k htmlc-add-menu',
                        callback: function(){
                            htmlConstructAddNewMenu(true,function(){
                                openHtmlConsctructForm(function(){
                                    callSelectSortable();
                                });
                            })
                        }
                    }
                }
            }); 
            $(".htmlc-add-menu").prop("disabled","true");
            $("input[name='htmlConstruct']").click(function(){
                if($("input[name='htmlConstruct']").is(':checked'))
                    $(".htmlc-add-menu").removeAttr("disabled");
                else
                    $(".htmlc-add-menu").prop("disabled","true");
            })
        })
        //($("input[name='htmlConstruct']").prop('checked') == true)

        /********************End show dyFObj in on click *****************/

        var openHtmlConsctructForm = function (callback){
            setTimeout(function(){
                dyFObj.openForm(htmlConstructParams,null, htmlConstructParamsData);

                var arrTopLeft = [],childxsMenuTopLeft = [],childAppTopLeft = [],childDropdownTopLeft=[],childButtonTopLeft=[],childClassTopLeft=[],arrLogoTopLeft=[],arrLogoTopLeft=[],arrUserProfilLeft = [],
                    arrTopRight = [],childxsMenuTopRight = [],childAppTopRight = [],childDropdownTopRight=[],childButtonTopRight=[],childClassTopRight =[],arrLogoTopRight=[],arrUserProfilRight = [],
                    arrMenuLeft = [],childAppMenuLeft = [],childButtonsMenuLeft = [],childClassMenuLeft = [],arrLogoMenuLeft=[],arrUserProfilMenuLeft = [],
                    arrMenuRight = [],childAppMenuRight = [],childButtonsMenuRight = [],childClassMenuRight = [],arrLogoMenuRight=[],arrUserProfilMenuRight = [],
                    arrMenuBottom = [],childAppMenuBottom = [],childButtonsMenuBottom = [],childClassMenuBottom = [],arrLogoMenuBottom=[],arrUserProfilMenuBottom = [];

                $.each(htmlConstructParams.jsonSchema.properties , function(k,val) {
                    var key = k.split("-");
                    //alert(key);
                    if(key[2] == "left"){
                        if(key[4] == "xsMenu")
                            childxsMenuTopLeft.push('.'+k+val.inputType);
                        else if(key[4] == "app")
                            childAppTopLeft.push('.'+k+val.inputType);
                        else if(key[4] == "dropdown")
                            childDropdownTopLeft.push('.'+k+val.inputType);
                        else if(key[4] == "logo")
                            arrLogoTopLeft.push('.'+k+val.inputType);
                        else if(key[4] == "userProfil")
                            arrUserProfilLeft.push('.'+k+val.inputType);
                        else if(key[3] == "addClass")
                            childClassTopLeft.push('.'+k+val.inputType);
                        else
                            childButtonTopLeft.push('.'+k+val.inputType);
                        arrTopLeft.push('.'+k+val.inputType);
                    }

                    else if(key[2] == "right"){
                        if(key[4] == "xsMenu")
                            childxsMenuTopRight.push('.'+k+val.inputType);
                        else if(key[4] == "app")
                            childAppTopRight.push('.'+k+val.inputType);
                         else if(key[4] == "dropdown")
                            childDropdownTopRight.push('.'+k+val.inputType);
                        else if(key[4] == "logo")
                            arrLogoTopRight.push('.'+k+val.inputType);
                        else if(key[4] == "userProfil")
                            arrUserProfilRight.push('.'+k+val.inputType);
                        else if(key[3] == "addClass")
                            childClassTopRight.push('.'+k+val.inputType);
                        else
                            childButtonTopRight.push('.'+k+val.inputType);
                        arrTopRight.push('.'+k+val.inputType);
                    }

                    else if(key[0] == "menuLeft"){
                        if(key[1] == "buttonList" && key[2] == "app")
                            childAppMenuLeft.push('.'+k+val.inputType);
                        else if(key[1] == "buttonList" && key[2] == "logo")
                            arrLogoMenuLeft.push('.'+k+val.inputType);
                        else if(key[1] == "buttonList" && key[2] == "userProfil")
                            arrUserProfilMenuLeft.push('.'+k+val.inputType);
                        else if(key[1] == "addClass")
                            childClassMenuLeft.push('.'+k+val.inputType);
                        else 
                            childButtonsMenuLeft.push('.'+k+val.inputType);
                        arrMenuLeft.push('.'+k+val.inputType);
                    }

                    else if(key[0] == "menuRight"){
                        if(key[1] == "buttonList" && key[2] == "app")
                            childAppMenuRight.push('.'+k+val.inputType);
                        else if(key[1] == "buttonList" && key[2] == "logo")
                            arrLogoMenuRight.push('.'+k+val.inputType);
                        else if(key[1] == "buttonList" && key[2] == "userProfil")
                            arrUserProfilMenuRight.push('.'+k+val.inputType);
                        else if(key[1] == "addClass")
                            childClassMenuRight.push('.'+k+val.inputType);
                        else 
                            childButtonsMenuRight.push('.'+k+val.inputType);
                        arrMenuRight.push('.'+k+val.inputType);
                    }

                    else if(key[0] == "menuBottom"){
                        if(key[1] == "buttonList" && key[2] == "app")
                            childAppMenuBottom.push('.'+k+val.inputType);
                        else if(key[1] == "buttonList" && key[2] == "logo")
                            arrLogoMenuBottom.push('.'+k+val.inputType);
                        else if(key[1] == "buttonList" && key[2] == "userProfil")
                            arrUserProfilMenuBottom.push('.'+k+val.inputType);
                        else if(key[1] == "addClass")
                            childClassMenuBottom.push('.'+k+val.inputType);
                        else 
                            childButtonsMenuBottom.push('.'+k+val.inputType);
                        arrMenuBottom.push('.'+k+val.inputType);
                    }
                })

                    wrapToDiv(arrTopLeft,"htmlConstruct","left",12,"",tplCtx.path,'header.menuTop.left');
                    wrapToDiv(childxsMenuTopLeft,"htmlConstruct","left-xsMenu",3,"",tplCtx.path,'header.menuTop.left.buttonList.xsMenu');
                    wrapToDiv(childAppTopLeft,"htmlConstruct","left-app",3,"",tplCtx.path,'header.menuTop.left.buttonList.app');
                    wrapToDiv(childDropdownTopLeft,"htmlConstruct","left-dropdown",3,"",tplCtx.path,'header.menuTop.left.buttonList.dropdown');
                    wrapToDiv(childButtonTopLeft,"htmlConstruct","left-buttons",3,"");
                    if(isDeveloper) wrapToDiv(childClassTopLeft,"htmlConstruct","left-class",12,"");
                    wrapToDiv(arrLogoTopLeft,"htmlConstruct","left-logo",6,"",tplCtx.path,'header.menuTop.left.buttonList.logo');
                    wrapToDiv(arrUserProfilLeft,"htmlConstruct","left-userProfil",6,"",tplCtx.path,'header.menuTop.left.buttonList.userProfil');
                    $(".htmlConstruct.left-buttons,.htmlConstruct.left-class").children().show();     



                wrapToDiv(arrTopRight,"htmlConstruct","right",12,"",tplCtx.path,'header.menuTop.right');
                    wrapToDiv(childxsMenuTopRight,"htmlConstruct","right-xsMenu",3,"",tplCtx.path,'header.menuTop.right.buttonList.xsMenu');
                    wrapToDiv(childAppTopRight,"htmlConstruct","right-app",3,"",tplCtx.path,'header.menuTop.right.buttonList.app');
                    wrapToDiv(childDropdownTopRight,"htmlConstruct","right-dropdown",3,"",tplCtx.path,'header.menuTop.right.buttonList.dropdown');
                    wrapToDiv(childButtonTopRight,"htmlConstruct","right-buttons",3,"");
                    if(isDeveloper) wrapToDiv(childClassTopRight,"htmlConstruct","right-class",12,"");
                    wrapToDiv(arrLogoTopRight,"htmlConstruct","right-logo",6,"",tplCtx.path,'header.menuTop.right.buttonList.logo');
                    wrapToDiv(arrUserProfilRight,"htmlConstruct","right-userProfil",6,"",tplCtx.path,'header.menuTop.right.buttonList.userProfil');
                    $(".htmlConstruct.right-buttons,.htmlConstruct.right-class").children().show();
                

                wrapToDiv(arrMenuLeft,"htmlConstruct","menuLeft",12,"",tplCtx.path,'menuLeft');
                    wrapToDiv(childAppMenuLeft,"htmlConstruct","menuLeft-app",3,"",tplCtx.path,'menuLeft.buttonList.app');
                    wrapToDiv(childButtonsMenuLeft,"htmlConstruct","menuLeft-buttons",3,"");
                    if(isDeveloper) wrapToDiv(childClassMenuLeft,"htmlConstruct","menuLeft-class",12,"");
                    wrapToDiv(arrLogoMenuLeft,"htmlConstruct","menuLeft-logo",6,"",tplCtx.path,'menuLeft.buttonList.logo');
                    wrapToDiv(arrUserProfilMenuLeft,"htmlConstruct","menuLeft-userProfil",6,"",tplCtx.path,'menuLeft.buttonList.userProfil');
                    $(".htmlConstruct.menuLeft-buttons,.htmlConstruct.menuLeft-class").children().show();       


                wrapToDiv(arrMenuRight,"htmlConstruct","menuRightt",12,"",tplCtx.path,'menuRight');
                    wrapToDiv(childAppMenuRight,"htmlConstruct","menuRightt-app",3,"",tplCtx.path,'menuRight.buttonList.app');
                    wrapToDiv(childButtonsMenuRight,"htmlConstruct","menuRightt-buttons",3,"");
                    if(isDeveloper) wrapToDiv(childClassMenuRight,"htmlConstruct","menuRightt-class",12,"");
                    wrapToDiv(arrLogoMenuRight,"htmlConstruct","menuRightt-logo",6,"",tplCtx.path,'menuRight.buttonList.logo');
                    wrapToDiv(arrUserProfilMenuRight,"htmlConstruct","menuRightt-userProfil",6,"",tplCtx.path,'menuRight.buttonList.userProfil');
                    $(".htmlConstruct.menuRightt-buttons,.htmlConstruct.menuRightt-class").children().show();

                wrapToDiv(arrMenuBottom,"htmlConstruct","menuBottom",12,"",tplCtx.path,'menuBottom');
                    wrapToDiv(childAppMenuBottom,"htmlConstruct","menuBottom-app",3,"",tplCtx.path,'menuBottom.buttonList.app');
                    wrapToDiv(childButtonsMenuBottom,"htmlConstruct","menuBottom-buttons",3,"");
                    if(isDeveloper) wrapToDiv(childClassMenuBottom,"htmlConstruct","menuBottom-class",12,"");
                    wrapToDiv(arrLogoMenuBottom,"htmlConstruct","menuBottom-logo",6,"",tplCtx.path,'menuBottom.buttonList.logo');
                    wrapToDiv(arrUserProfilMenuBottom,"htmlConstruct","menuBottom-userProfil",6,"",tplCtx.path,'menuBottom.buttonList.userProfil');
                    $(".htmlConstruct.menuBottom-buttons,.htmlConstruct.menu-bottom-class").children().show();
                
                    $(".menuLeft-buttonList-app-buttonListselectMultiple,.menuRight-buttonList-app-buttonListselectMultiple,.header-menuTop-left-buttonList-app-buttonListselectMultiple,.header-menuTop-left-buttonList-xsMenu-buttonList-app-buttonListselectMultiple,.header-menuTop-right-buttonList-app-buttonListselectMultiple,.header-menuTop-right-buttonList-xsMenu-buttonList-app-buttonListselectMultiple").removeClass("col-md-3").addClass("col-md-12")

                if(typeof callback =="function")
                    callback()
            },500);


        }

        var htmlConstructAddNewMenu = function (add=true,callback){
            if(add)
                $("input:checkbox[name=htmlConstruct]:checked").each(function(){
                    if($(this).val() == "left")
                        generateMenuProps("left",hasMenuTopLeft,menuTopLeftPath,menuTopLeftThemeParamsPath);
                    if($(this).val() == "right")
                        generateMenuProps("right",hasMenuTopRight,menuTopRightPath,menuTopRightThemeParamsPath);
                    if($(this).val() == "menuLeft")
                        generateMenuProps("menuLeft",hasMenuLeft,menuLeftPath,menuLeftThemeParamsPath);
                    if($(this).val() == "menuRight")
                        generateMenuProps("menuRight",hasMenuRight,menuRightPath,menuRightThemeParamsPath);
                    if($(this).val() == "menuBottom")
                        generateMenuProps("menuBottom",hasMenuBottom,menuBottomPath,menuBottomThemeParamsPath);
                                             
                });

            if(hasMenuTopLeft)
               generateMenuProps("left",hasMenuTopLeft,menuTopLeftPath,menuTopLeftThemeParamsPath);
            if(hasMenuTopRight)
                generateMenuProps("right",hasMenuTopRight,menuTopRightPath,menuTopRightThemeParamsPath);
            if(hasMenuLeft)
                   generateMenuProps("menuLeft",hasMenuLeft,menuLeftPath,menuLeftThemeParamsPath);
            if(hasMenuRight)
               generateMenuProps("menuRight",hasMenuRight,menuRightPath,menuRightThemeParamsPath);
            if(hasMenuBottom)
               generateMenuProps("menuBottom",hasMenuBottom,menuBottomPath,menuBottomThemeParamsPath);

            if(typeof callback =="function")
                callback();
        }
        
        var callSelectSortable = function (){
             if($("#header-menuTop-left-buttonList-app-buttonList").length != 0)
                 selSortableObj.init("#header-menuTop-left-buttonList-app-buttonList",optionsButtonListLeft,{dropdown:true});
            if($("#header-menuTop-right-buttonList-app-buttonList").length != 0)
                selSortableObj.init("#header-menuTop-right-buttonList-app-buttonList",optionsButtonListRight,{dropdown:true});
            if($("#header-menuTop-left-buttonList-xsMenu-buttonList-app-buttonList").length != 0) 
                selSortableObj.init("#header-menuTop-left-buttonList-xsMenu-buttonList-app-buttonList",optionsButtonListLeft,{dropdown:true});
            if($("#header-menuTop-right-buttonList-xsMenu-buttonList-app-buttonList").length != 0) 
                selSortableObj.init("#header-menuTop-right-buttonList-xsMenu-buttonList-app-buttonList",optionsButtonListRight,{dropdown:true});
            if($("#menuLeft-buttonList-app-buttonList").length !=0)
                selSortableObj.init("#menuLeft-buttonList-app-buttonList",optionsButtonListMenuLeft,{dropdown:true});
            if($("#menuRight-buttonList-app-buttonList").length !=0)
                selSortableObj.init("#menuRight-buttonList-app-buttonList",optionsButtonListMenuRight,{dropdown:true});
            if($("#menuBottom-buttonList-app-buttonList").length !=0)
                selSortableObj.init("#menuBottom-buttonList-app-buttonList",optionsButtonListMenuBottom,{dropdown:true});

        }
} 
$(function(){
    intiHtmlConstruction();
})
</script>
