<?php 
    $keyTpl = "htmlConstruct";
    $subkeyTpl= $keyTpl."element";

    $paramsData = [
        "menuTop" => "",
        "tplCss" => [],
        "banner" => "",
        "containerClass" => ""
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl]["element"][$i])){
                $paramsData[$i] =  $this->costum[$keyTpl]["element"][$i];   
            }
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>.element'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> Page Element
    </a>
<?php }?>
<style>
.<?php echo $subkeyTpl ?>.element-left:before {
        content: "Menu gauche";
        font-size: 15px;
}
.<?php echo $subkeyTpl ?>.element-right:before {
        content: "Menu";
}
.<?php echo $subkeyTpl ?>.element-parent-right:before{
    content: "Menu droite";
    font-size: 15px;
}
.<?php echo $subkeyTpl ?>.element-right-buttonList-xsMenu:before {
        content: "MenuXs";
}
.remove-element,.<?php echo $subkeyTpl ?>.app-left .btn-state{
    margin:0 40px;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?= $subkeyTpl ?>MenuElt = ["newhome","newspaper","detail","community","gallery","events","classifieds","cospace","projects","form"];
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Element page')?>",
            "icon" : "fa-cog",
            "properties" : {
                "tplCss" : {
                    "inputType" : "hidden",
                    //"label" : "Template css <span class='text-danger'>(Developer only)</span>",
                    /*"options" :{
                        "v1.0_pageProfil.css" : "v1.0_pageProfil.css"
                    },*/
                    "rules" : {
                        "required" : true
                    },
                    value : "v1.0_pageProfil.css"
                },
                "banner" : {
                    "inputType" : "hidden",
                    value : "co2.views.element.bannerHorizontal"
                },
                "containerClass-centralSection" : {
                    "label":"<?php echo Yii::t('cms', 'Central Section')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "inputType" : "text",
                    value : (
                        typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.containerClass !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.containerClass.centralSection !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.containerClass.centralSection : "col-xs-12 col-lg-10 col-lg-offset-1"

                },
                "menuTop-class" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Top menu class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    value : (
                        typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.class !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.class : "col-xs-12"

                },
                /*"menuTop-color" : {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Top menu color')?>",
                    value : (
                        typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                        && typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.menuTop.color !="undefined") ?
                            sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.menuTop.color : ""

                },*/
                "menuTop-left-class" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    value : (
                        typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left.class !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left.class : "col-lg-3 col-md-3 col-sm-4 hidden-xs no-padding"

                },
                "menuTop-left-buttonList-imgUploader" : {
                    "inputType" : "checkboxSimple",
                    "label" : "<?php echo Yii::t('cms', 'Show image addition')?>",
                    "params" : {
                        "onText" : "Oui",
                        "offText" : "Non",
                        "onLabel" : "Oui",
                        "offLabel" : "Non",
                        "labelText" : "label"
                    },
                    "checked" : (
                        typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left.buttonList !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left.buttonList.imgUploader !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.left.buttonList.imgUploader : false

                },
                "menuTop-right-class" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Class')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    value : (
                        typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right !="undefined"
                        && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.class !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.class : "col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center"

                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    var kk= k.split("-").join("][");
                    if($("#"+k).parent().parent().data("activated")!=false){
                        if(k.split("-")[0] == "tplCss")
                           tplCtx.value[kk] = [$("#"+k).val()];
                        else if( $("#"+k).val() != "false" && $("#"+k).val() != "")
                            tplCtx.value[kk] = $("#"+k).val();
                    }else{
                        if( $("#"+k).val() != "false" && $("#"+k).val() != "")
                            tplCtx.value[kk] = $("#"+k).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    $.each(sectionDyf.<?= $subkeyTpl ?>MenuElt,function(k,v){
        sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties["menuTop-right-buttonList-"+v] = {
            "inputType" : "checkboxSimple",
            "label" : v,
            "params" : {
                "onText" : "Oui",
                "offText" : "Non",
                "onLabel" : "Oui",
                "offLabel" : "Non",
                "labelText" : "label"
            },
            "checked" : (
                typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList[v] !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList[v] : false

        };

        sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties["menuTop-right-buttonList-xsMenu-buttonList-"+v]= {
            "inputType" : "checkboxSimple",
            "label" : v,
            "params" : {
                "onText" : "Oui",
                "offText" : "Non",
                "onLabel" : "Oui",
                "offLabel" : "Non",
                "labelText" : "label"
            },
            "checked" : (
                typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList.xsMenu !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList.xsMenu.buttonList !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList.xsMenu.buttonList[v] !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData.menuTop.right.buttonList.xsMenu.buttonList[v] : false

        }
    })
    $(".edit<?php echo $subkeyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
         var arrLeft = [], arrRightXs = [], arrRight = [], arrRightParent = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            if(kk[1] == "left")
                arrLeft.push('.'+k+val.inputType);
            else if (kk[2] == "buttonList" && kk[3] == "xsMenu")
                arrRightXs.push('.'+k+val.inputType);
            else if (kk[2] == "buttonList" && kk[3] != "xsMenu")
                arrRight.push('.'+k+val.inputType);
            if(kk[1] == "right")
                arrRightParent.push('.'+k+val.inputType);
        });
        // wrapToDiv(list of class[],"parent class","sub class of parent","col-md-4","col-md-offset-4","tplCtx.path","buttonList.app");
        wrapToDiv(arrRightParent,"<?php echo $subkeyTpl ?>","element-parent-right",4,"",tplCtx.path,'menuTop.right');
        wrapToDiv(arrLeft,"<?php echo $subkeyTpl ?>","element-left",4,"",tplCtx.path,'menuTop.left');
        wrapToDiv(arrRight,"<?php echo $subkeyTpl ?>","element-right",4,"",tplCtx.path,'menuTop.right.buttonList');
        wrapToDiv(arrRightXs,"<?php echo $subkeyTpl ?>","element-right-buttonList-xsMenu",4,"",tplCtx.path,'menuTop.right.buttonList.xsMenu');

    });
});
</script>