<?php 
    $keyTpl = "logoIconTitle";

    $paramsData = [
        "slug" => $this->costum["contextSlug"],
        "welcomeTpl" => "costum.views.custom.".$this->costum["slug"].".home",
        "sourceKey" => true,
        "favicon" => "/ico/".$this->costum["contextSlug"]."/votre_ico.ico",
        "metaImg" => "/images/".$this->costum["contextSlug"]."/votre_banner.png",
        "metaTitle" => $this->costum["title"],
        "logoMin" => "/images/".$this->costum["contextSlug"]."/votre_log.png",
        "formId" => "exemple",
        "docId" => "exemple"
    ];
    
    foreach($paramsData as $i => $v) {
        if(isset($this->costum[$i])) 
            $paramsData[$i] =  $this->costum[$i];   
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum'>
        <i class="fa fa-picture-o" aria-hidden="true"></i> Icon et Titre
    </a>
<?php }?>
<?php 
      $favicon = [];
      $metaImg = [];
      $logoMin = [];
      $latestFavicon = [];
      $latestLogoMin = [];
      $latestMetaImg = [];
      $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $this->costum["contextId"],
          "type"=>$this->costum["contextType"],
        ),"image"
    );

    foreach ($initImage as $key => $value) {
      if ($value["subKey"] == "favicon") {
        $favicon[] = $value; 
        //var_dump($value["imageThumbPath"]);
        $latestFavicon[]= $value["imageThumbPath"];
      }
      else if ($value["subKey"] == "metaImg") {
        $metaImg[] = $value; 
         $latestMetaImg[]= $value["imageThumbPath"];
      }
      else if ($value["subKey"] == "logoMin") {
        $logoMin[] = $value; 
         $latestLogoMin[]= $value["imageThumbPath"];
      }
    }
 ?>
<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Icon and Title')?>",
            "icon" : "fa-cog",
            "properties" : {
                "welcomeTpl" : {
                    "inputType" : "hidden",
                    /*"label" : "welcomeTpl",*/
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.welcomeTpl
                },
                "sourceKey" : {
                    "inputType" : "hidden",
                    values : true
                },
                "metaTitle" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Title of the page')?>",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.metaTitle 
                },
                "favicon" : {
                "inputType" : "uploader",
                "label" : "<?php echo Yii::t('cms', 'Favicon (icon on the tab)')?>",
                "domElement" : "favicon",
                "docType": "image",
                "contentKey" : "slider",
                "itemLimit" : 1,
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "showUploadBtn": false,
                "endPoint" :"/subKey/favicon",
                initList : <?php echo json_encode($favicon) ?>
                },
               /* "metaImg" : {
                    "inputType" : "uploader",
                    "label" : "Meta image",
                    "domElement" : "metaImg",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    "endPoint" :"/subKey/metaImg",
                    initList : <?php //echo json_encode($metaImg) ?>
                },  */              
                /*"logoMin" : {
                    "inputType" : "uploader",
                    "label" : "Mini logo",
                    "domElement" : "logoMin",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    "endPoint" :"/subKey/logoMin",
                    initList : <?php //echo json_encode($logoMin) ?>
                },*/
                "formId" : {
                    "inputType" : "hidden",
                    /*"label" : "formId",*/
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.formId
                },
                "docId" : {
                    "inputType" : "hidden",
                    /*"label" : "docId",*/
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.docId
                }
            },
            beforeBuild : function(){
                uploadObj.set(costum.contextType,costum.contextId);
            },
            save : function (data) { 
                tplCtx.format = true;
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                            $("#ajax-modal").modal('hide');
                            location.reload();
                        });
                    });
                }
            }
        }
    };

    $(document).on("click",".edit<?php echo $keyTpl ?>Params",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
