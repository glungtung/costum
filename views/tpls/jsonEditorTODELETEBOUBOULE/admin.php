<?php 
    $keyTpl = "admin";

    $paramsData = [
        "email" => "exemple@gmail.com",
        "accessMember" => ""
    ];
    
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];   
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum'>
        <i class="fa fa-cog" aria-hidden="true"></i> <?php echo Yii::t('common', 'Administrator')?>
    </a>
<?php }?>


<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Email de l'administrateur",
            "icon" : "fa-cog",
            "properties" : {
                "email" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Email of the administrator')?>",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.email
                },
                /*"accessMember" : {
                    "inputType" : "checkboxSimple",
                        "label" : "accessMember",
                        "params" : {
                            "onText" : "Oui",
                            "offText" : "Non",
                            "onLabel" : "Oui",
                            "offLabel" : "Non",
                            "labelText" : "accessMember"
                    },
                    "checked" : sectionDyf.<?php //echo $keyTpl ?>ParamsData.accessMember
                },*/
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(document).on("click",".edit<?php echo $keyTpl ?>Params",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+".<?php echo $keyTpl ?>";
        tplCtx.format=true;
        tplCtx.removeCache= true;
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
