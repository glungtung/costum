<?php 
    $keyTpl = "designSelector";

    $paramsData = [
        
    ];
    
    if(isset($this->costum["css"]["urls"])) 
        $paramsData =  $this->costum["css"]["urls"];   

?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.css.urls'>
        <i class="fa fa-crop" aria-hidden="true"></i> Selection de design
    </a>
<?php }?>

<?php 
$polygone = "";

 ?>
<style>
    .image-menu{
        /*border-radius: 100% !important;*/
    }


    #menuTopLeft .lbh-menu-app{
        background-color: green;
        width: auto;
        height: auto;
        float: right;

        -webkit-clip-path: polygon(10% 0%, 0% 100%, 95% 100%, 100% 50%, 95% 0%);
        clip-path: polygon(10% 0%, 0% 100%, 95% 100%, 100% 50%, 95% 0%);


        /*-webkit-clip-path: polygon(18% 0, 89% 0%, 100% 50%, 90% 100%, 19% 100%, 7% 50%);
        clip-path: polygon(18% 0, 89% 0%, 100% 50%, 90% 100%, 19% 100%, 7% 50%);*/

        /*-webkit-clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);*/

        /*-webkit-clip-path: polygon(6% 0, 100% 0%, 94% 100%, 0% 100%);
        clip-path: polygon(6% 0, 100% 0%, 94% 100%, 0% 100%);

        /*|| octagon*/
        /*-webkit-clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);
        clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);*/

        /* [_] Rabbet*/
        /*-webkit-clip-path: polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%);
        clip-path: polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%);*/

        /* >=> rigth chevron*/
        /*-webkit-clip-path: polygon(92% 0, 100% 50%, 92% 100%, 0% 100%, 8% 49%, 0% 0%);
        clip-path: polygon(92% 0, 100% 50%, 92% 100%, 0% 100%, 8% 49%, 0% 0%);*/


        padding: 11px 10px 10px 26px;
        color: white !important;
        margin-left: -7px;
    }

    #menuTopLeft .lbh-menu-app:hover{
        color: #fff !important;
        background-color: grey;
        text-decoration: none
    }
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    //begin dynForm--------------------
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Icon and Title')?>",
            "icon" : "fa-cog",
            "properties" : {
                "metaTitle" : {
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Menu top')?>",
                    "options" : {
                        "menu-app-round-red.css" : "rouge",
                    },
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.metaTitle 
                }
            },
            save : function (data) { 
                tplCtx.format = true;
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    });
                }
            }
        }
    };
    //end dynForm--------------------

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
