<?php 
    $keyTpl = "";
    $subkeyTpl= $keyTpl."colors";

    $paramsData = [
        "colors" => [],
    ];
    if( isset($this->costum["colors"]) ) {
        foreach($this->costum["colors"] as $i => $v) {
                array_push($paramsData["colors"],array("colorName" => $i,"colorValue" => $v));
        }
    }
    var_dump($this->costum["colors"]);
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum<?php echo $keyTpl ?>.colors'>
        <i class="fa fa-paint-brush" aria-hidden="true"></i> Couleurs
    </a>
<?php }?>


<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Colors')?>",
            "icon" : "fa-cog",
            "properties" : {
                "colors" : {
                    "label" : "<?php echo Yii::t('cms', 'Colors')?>",
                    "inputType" : "lists",
                    "entries":{
                        "colorName":{
                            "type":"text",
                            "label" :"<?php echo Yii::t('cms', 'Color name')?>",
                            "class":"col-xs-5"
                        },
                        "colorValue":{
                            "type":"text",
                            "label" :"<?php echo Yii::t('cms', 'Value')?>",
                            "class":"col-xs-5"
                        }
                    }
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(k == "colors"){
                        $.each(data.colors,function(k,v){
                            var key = data.colors[k]["colorName"];
                            var value = data.colors[k]["colorValue"];
                            tplCtx.value[key] = value;
                        });
                        
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format = true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $subkeyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
        $("input[name^='colorscolorValue']" ).attr('type','color');
        //convert to input to select
            //if empty paramsDAta
        if(sectionDyf.<?php echo $subkeyTpl ?>ParamsData["colors"].length == 0)
            sectionDyf.<?php echo $subkeyTpl ?>ParamsData["colors"].push({colorName:"black",colorValue:"#000"});
        $.each( sectionDyf.<?php echo $subkeyTpl ?>ParamsData["colors"] , function(k,val) { 
            var name = $("input[name='colorscolorName"+k+"']").attr('name');
            var dataEntry = $("input[name='colorscolorName"+k+"']").attr('data-entry');
            var id = $("input[name='colorscolorName"+k+"']").attr('id');
            var classe = $("input[name='colorscolorName"+k+"']").attr('class');
            var value = $("input[name='colorscolorName"+k+"']").val();

            $("input[name='colorscolorName"+k+"']").removeClass('no-padding').replaceWith(
                '<select name="'+name+'" data-entry="'+dataEntry+'" id="'+id+'" class="'+classe+'"></select>');
                $("#"+id).append(inputColor(val.colorName));
        });

        dyFObj.init.addAndBindListEntry = function(idContainer, entry, field, value){
            mylog.log("buildEntryList idContainer",idContainer,"entry", entry,"field",field,"value", value);
            incEntry=$(idContainer+" .listEntry").length;
            htmlList="";
            if(notNull(value)){
                $.each(value, function(e,v){
                    htmlList+=dyFObj.init.buildEntryList(incEntry, entry, field, v);
                    incEntry++;
                });
                
            }else
                htmlList+=dyFObj.init.buildEntryList(incEntry, entry, field);
            
            
            $(idContainer).append(htmlList);
            // THIS CHANGE THE INPUT "TEXT" IN LISTS TO "COLOR";
            $("input[name^='colorscolorValue']" ).attr('type','color');
            $("input[name='colorscolorName"+incEntry+"']").removeClass('no-padding').replaceWith(
            '<select name="colorscolorName'+incEntry+'" data-entry="colorName" id="colors'+incEntry+'" class="addmultifield addmultifield0 form-control input-md col-xs-12"></select>');
            $("#colors"+incEntry).append(inputColor(""));

            $(".removeListLineBtn").off().on("click", function(){
                $(this).parents().eq(1).fadeOut(200).remove();
            });
            $(".addListLineBtn"+field).off().on("click", function(){
                dyFObj.init.addAndBindListEntry(idContainer, entry, field);
            });
        }

        function inputColor(current){
            var html = "";
            $.each(colorCommunecter,function(k,v){
                html+='<option value="'+v+'" '+((current == v) ? "selected" : "" )+' >'+v+'</option>';
            });
            return html;
        }

    });
});
</script>
