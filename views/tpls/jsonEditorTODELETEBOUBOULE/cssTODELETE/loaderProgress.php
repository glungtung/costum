<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl."loaderProgress";

    $paramsData = [
        "loader" => "",
        "progress" => ""
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];   
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> Loader et Barre de progression
    </a>
<?php }?>
<style>
    .<?php echo $subkeyTpl ?>.ring1:before {
        content: "Cercle 1";
    }
    .<?php echo $subkeyTpl ?>.ring2:before {
        content: "Cercle 2";
    }
        .<?php echo $subkeyTpl ?>.progress-co:before {
        content: "Barre de progression";
    }
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Loader and Progress')?>",
            "icon" : "fa-cog",
            "properties" : {
                "loader][background]": {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Loader background color')?>",
                    value : notNull(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.background) ? sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.background : "#ffffff",
                },
                //ring 1**************
                "loader][ring1][color]": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.color : "#edf5f"
                },
                "loader][ring1][height]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Height')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.height !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.height : "360"
                },
                "loader][ring1][width]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Width')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.width !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.width : "360"
                },
                "loader][ring1][left]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin left')?>",
                    "rules":{
                        "number" : true
                    },                    
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.left !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.left : "-15"
                },
                "loader][ring1][top]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin top')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.top !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.top : "-35"
                },
                "loader][ring1][borderWidth]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border size')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.borderWidth !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring1.borderWidth : "4"
                },
                //ring2*************
                "loader][ring2][color]": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.color : "#ccc"
                },
                "loader][ring2][height]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Height')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.height !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.height : "360"
                },
                "loader][ring2][width]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Width')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.width !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.width : "360"
                },
                "loader][ring2][left]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin left')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.left !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.left : "-10"
                },
                "loader][ring2][top]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin top')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.top !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.top : "-30"
                },
                "loader][ring2][borderWidth]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border size')?>",
                    "rules":{
                        "number" : true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2 !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.borderWidth !="undefined") ? 
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.loader.ring2.borderWidth : "5"
                },
                //progress*************
                "progress][value][background]": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color of current load')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.progress.value !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.progress.value.background !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.progress.value.background : "#5b2649"
                },
                "progress][bar][background]": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Progress bar color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.progress.bar !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.progress.bar.background !="undefined") ? 
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.progress.bar.background : "#fff"
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array"){
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    }else{
                        if(k.indexOf("[") && k.indexOf("]"))                
                            kt = k.split("[").join("\\[").split("]").join("\\]");
                            tplCtx.value[k] = $("#"+kt).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    $(document).on("click",".edit<?php echo $subkeyTpl ?>Params",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
        var arrRing1 = [],arrRing2 = [],arrProgress = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
            if(k.indexOf("[") && k.indexOf("]"))                
                kt = k.split("[").join("\\[").split("]").join("\\]");
            if (k[12]==1)
                arrRing1.push('.'+kt+val.inputType);str.split("]")[0];
            if (k[12]==2)
                arrRing2.push('.'+kt+val.inputType);
            if (k.split("]")[0]=="progress")
                arrProgress.push('.'+kt+val.inputType);     
        });
        wrapToDiv(arrRing1,"<?php echo $subkeyTpl ?>","ring1",2); 
        wrapToDiv(arrRing2,"<?php echo $subkeyTpl ?>","ring2",2);
        wrapToDiv(arrProgress,"<?php echo $subkeyTpl ?>","progress-co",6);                 
    });
});
</script>


