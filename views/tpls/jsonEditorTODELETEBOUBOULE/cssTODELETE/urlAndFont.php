<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl."urlAndFont";

    $paramsData = [
        "urls" => [],
        "cssCode"  => "",
        "font" => ""
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];   
        }
    }

    $initFiles = Document::getListDocumentsWhere(
        array(
            "id"=> $this->costum["contextId"],
            "type"=> $this->costum["contextType"],
            "subKey"=>"costumFont"
        ), "file"
    );
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> Police et CSS
    </a>
<?php }?>

<style>
    .<?php echo $subkeyTpl ?>.font:before {
        content: "Font";
}
#font-url{
    font-size:20px;
}

textarea[is="highlighted-code"] { padding: 0px; width:35vw; }

.cssCodecustom{
    position:relative;
    
}

.div-wrapper-high{
    position: relative;
    padding: 0px;
    margin: 0px;
    margin-left:0px
}


</style>

<!-- <link rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/styles/default.min.css">
      <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/highlight.min.js"></script>
      <script src="https://unpkg.com/codeflask/build/codeflask.min.js"></script> -->


    
<script type="text/javascript">

//import htmljs library

(async ({chrome, netscape}) => {
    
    // add Safari polyfill if needed
    if (!chrome && !netscape)
      await import('https://unpkg.com/@ungap/custom-elements');

  
    const {default: HighlightedCode} =
      await import('https://unpkg.com/highlighted-code');

  
    HighlightedCode.useTheme('atom-one-light');
    $(".highlighted-code").css({"left":"0px", "top":"0px"});
  })(self);



jQuery(document).ready(function() {

    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Font and css files')?>",
            "icon" : "fa-cog",
            "properties" : {
                "urls": {
                    "inputType" : "array",
                    "label" : "<?php echo Yii::t('cms', 'Urls')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "css_filename.css",
                    values :  sectionDyf.<?php echo $subkeyTpl ?>ParamsData.urls,
                },
                "cssCode": {
                    "inputType" : "custom",
                    "markdown" : false,
                    "label" : "<?php echo Yii::t('cms', 'CSS code')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "inputType" : "custom",
                    "html":"<div class='col-xs-12 text-left div-wrapper-high' ><textarea id='cssFile' maxlength='' class='form-control textarea col-xs-12 no-padding valid' name='cssFile' is='highlighted-code' language='css' id='txthight' tab-size='1'  style='position:relative;overflow:unset;height:100px;'></textarea></div>",
                },
                "font-useUploader": {
                    "label" : "<?php echo Yii::t('cms', 'Use the uploader')?>",
                    "inputType" : "checkboxSimple",
                    "params" : {
                        "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                        "offText" : "<?php echo Yii::t('common', 'No')?>",
                        "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                        "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                        "labelText" : "label"
                    },
                    checked : exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.useUploader) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.useUploader : false 
                },
                "font-url": {
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Font')?>",
                    "options" : fontObj,
                     "isPolice" : true,

                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.url !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.url : ""
                },
                "font-uploader": {
                    inputType :"uploader",
                     "docType" : "file",
                    "itemLimit" : 1,
                    "filetypes": ["ttf"],
                    "endPoint" : "/subKey/costumFont",
                    initList : <?php echo json_encode($initFiles); ?>,
                }
            },
            beforeBuild : function(){
                uploadObj.set(costum.contextType,costum.contextId);
            },
            save : function (data) { 

                var input = document.querySelector('#cssFile');
                const regex_valid_css = /((?:^\s*)([\w#.@*,:\-.:>,*\s]+)\s*{(?:[\s]*)((?:[A-Za-z\- \s]+[:]\s*['"0-9\w .,\/()\-!%]+;?)*)*\s*}(?:\s*))/mg;
                tplCtx.value = {};
                params = {  
                        "id" : contextId , 
			    		"css" : input.value
                    };
                // save or update ccsFile   
                ajaxPost(
			        	null,
				        baseUrl+"/"+moduleId+"/cssFile/insert",
				        params,
				    );

                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
                    var kk= k.split("-").join("][");
                    var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                    if(val.inputType == "array"){
                        tplCtx.value[kk] = getArray('.'+k+val.inputType);
                    }else{
                        if( isActiveDivParent == true){
                            tplCtx.value[kk] = $("#"+k).val();
                        }
                    }
                });


                if(regex_valid_css.test(input.value)==false)
                    toastr.error("<?php echo Yii::t('cms', 'Invalid css code')?>");
                else if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format = true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        dyFObj.commonAfterSave(params,function(){
                            mylog.log("valiny",params);
                          toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                          location.reload();
                        });
                    });
                }

            }
        }
    };


    // get css from collection cssFile
    function getCss() {
            var result = new Array();
            $.ajax({
                url: baseUrl+"/"+moduleId+"/cssFile/getbycostumid",
                type: 'post',
                data: {id: costum.contextId},
                global: false,
                async: false,
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(key,value) {
                        result.push({"css" : value.css});  
                    });
                }
            });
            return result[0]["css"];

        }

    // Highlight the cssFile

    //display seetings
    $(document).on("mouseenter","#cssFile",function(){
        $(".highlighted-code").css({"left":"0px", "top":"0px"});
        if($('#cssFile').val() === ""){
            $('#cssFile').val( getCss() );
        }
    });
    $(document).on("mouseout","#cssFile",function(){
        $(".highlighted-code").css({"left":"0px", "top":"0px"});
    });
    $(document).on("focus","#cssFile",function(){
        $(".highlighted-code").css({"left":"0px", "top":"0px"});
    });
    $(document).on("keyup","#cssFile",function(){
    $(".highlighted-code").css({"left":"0px", "top":"0px"});
    });


    //validate css front

    $(document).on("keyup","#cssFile",function(){

    var input = document.querySelector('#cssFile');

    const regex_valid_css = /((?:^\s*)([\w#.@*,:\-.:>,*\s]+)\s*{(?:[\s]*)((?:[A-Za-z\- \s]+[:]\s*['"0-9\w .,\/()\-!%]+;?)*)*\s*}(?:\s*))/mg;

    if(regex_valid_css.test(input.value)==true){
        document.getElementById("btn-submit-form").disabled = false;
    }else{
        document.getElementById("btn-submit-form").disabled = true;
    }


    });

    $(document).on("click",".edit<?php echo $subkeyTpl ?>Params",function(event) {  
        

        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        if(isDeveloper==false){
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties.urls;
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties.cssCode;
        }
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
         var arrFont = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            if(kk[0] == "font")
                arrFont.push('.'+k+val.inputType);
        });
        // wrapToDiv(list of class[],"parent class","sub class of parent","col-md-x","col-md-offset-x","tplCtx.path","buttonList.app");
        wrapToDiv(arrFont,"<?php echo $subkeyTpl ?>","font",6,"",tplCtx.path,'font');
        $("#imageUploader_paste").attr("placeholder","Coller une police ou un font (ttf)");
            if(jsonHelper.notNull("costum.css.font.useUploader")){
                if(costum.css.font.useUploader == true){
                   
                    $(".font-uploaderuploader").show();
                    $(".font-urlselect").hide();            
                }else{
                    $(".font-uploaderuploader").hide();
                    $(".font-urlselect").show();
                }     
            }else{
                    $(".font-uploaderuploader").hide();
                    if(jsonHelper.notNull("costum.css.font.url"))
                        $(".font-urlselect").show();
                    
            }


        $('.btn-activator').click(function(){
            $(".font-uploaderuploader").hide();
            if(jsonHelper.notNull("costum.css.font.useUploader")){
                if(costum.css.font.useUploader == true){
                    $(".font-uploaderuploader").show();
                    $(".font-urlselect").hide();            
                }else{
                    $(".font-uploaderuploader").hide();
                    $(".font-urlselect").show();
                }     
            }
        })

      

        $('.font-useUploadercheckboxSimple .btn-dyn-checkbox').on('click',function(){
            if($("#font-useUploader").val()=="true"){
                $(".font-uploaderuploader").show();
                $(".font-urlselect").hide();  
            }else{
                $(".font-uploaderuploader").hide();
                $(".font-urlselect").show(); 
            }
        })
        event.stopImmediatePropagation()
    });



});
</script>
