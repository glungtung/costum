<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl."menuTop";

    $paramsData = [
        "background" => "",
        "boxShadow" => "",
        "app" => "",
        "button" => "",
        "scopeBtn" => "",
        "filterBtn" => "",
        "badge" => "",
        "connectBtn" => "",
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl]["menuTop"][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl]["menuTop"][$i];   
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>.menuTop'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> Menu en haut
    </a>
<?php }?>
<style>
    .<?php echo $subkeyTpl ?>.app-css:before {
        content: "Menu";
    }
    .<?php echo $subkeyTpl ?>.button-css:before {
        content: "Boutton";
    }
    .<?php echo $subkeyTpl ?>.badge-css:before {
        content: "Badge";
    } 
    .<?php echo $subkeyTpl ?>.scopeBtn-css:before {
        content: "Boutton de recherche";
    }
    .<?php echo $subkeyTpl ?>.filterBtn-css:before {
        content: "Boutton filtre";
    }
    .<?php echo $subkeyTpl ?>.connectBtn-css:before {
        content: "Boutton de connexion";
    }
    .<?php echo $subkeyTpl ?>.design-css:before {
        content:"Design menu haut";
    }

    #menuTopLeft .lbh-menu-app{
       /*background-color: green;
        width: auto;
        height: auto;
        float: right;*/


        /*-webkit-clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);
        clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);*/


        /*        -webkit-clip-path: polygon(10% 0%, 0% 100%, 95% 100%, 100% 50%, 95% 0%);
        clip-path: polygon(10% 0%, 0% 100%, 95% 100%, 100% 50%, 95% 0%);*/

        /*-webkit-clip-path: polygon(18% 0, 89% 0%, 100% 50%, 90% 100%, 19% 100%, 7% 50%);
        clip-path: polygon(18% 0, 89% 0%, 100% 50%, 90% 100%, 19% 100%, 7% 50%);*/

        /*-webkit-clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);*/

        /*-webkit-clip-path: polygon(6% 0, 100% 0%, 94% 100%, 0% 100%);
        clip-path: polygon(6% 0, 100% 0%, 94% 100%, 0% 100%);

        /*|| octagon*/
        /*-webkit-clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);
        clip-path: polygon(30% 0%, 70% 0%, 100% 30%, 100% 70%, 70% 100%, 30% 100%, 0% 70%, 0% 30%);*/

        /* [_] Rabbet*/
        /*-webkit-clip-path: polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%);
        clip-path: polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%);*/

        /* >=> rigth chevron*/
        /*-webkit-clip-path: polygon(92% 0, 100% 50%, 92% 100%, 0% 100%, 8% 49%, 0% 0%);
        clip-path: polygon(92% 0, 100% 50%, 92% 100%, 0% 100%, 8% 49%, 0% 0%);*/


        /*padding: 11px 10px 10px 26px;
        margin-left: -7px;*/
    }
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
$(".developer").click(function(){
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Menu top')?>",
            "icon" : "fa-cog",
            "properties" : {
                "background": {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    values : sectionDyf.<?php echo $subkeyTpl ?>ParamsData.background,
                },
                "boxShadow": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Shadow')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "1px 3px 1px #ddd",
                    value : sectionDyf.<?php echo $subkeyTpl ?>ParamsData.boxShadow
                },
                //app*******
                "app-button-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "rules" : {
                        "number": true,
                        "required" :true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontSize : "24"
                },
                "app-button-fontWeight": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font weight')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontWeight !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontWeight : "normal"
                },
                "app-button-textTransform": { 
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Text transformation')?>",
                    "options" : {
                        "capitalize" : "<?php echo Yii::t('cms', 'Capitalize')?>",
                        "uppercase" : "<?php echo Yii::t('cms', 'Uppercase')?>",
                        "lowercase" : "<?php echo Yii::t('cms', 'Lowercase')?>"
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.textTransform !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.textTransform : "capitalize"
                },
                "app-button-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.color : "black"
                },
                //desing******************
                "app-button-design-clipPath": { 
                    "inputType" : "select",
                    "label":"Forme",
                    "options" : {
                        "polygon(16% 0, 83% 0, 100% 30%, 100% 70%, 87% 100%, 14% 100%, 0% 70%, 0% 30%)" : "Octogone",
                        "polygon(4% 0, 100% 0%, 95% 99%, 0% 100%)" : "Parallelogram",
                        "polygon(94% 0, 100% 50%, 94% 100%, 0% 100%, 6% 49%, 0% 0%)" : "Right Chevron",
                        "polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%)" :"Rabbet",
                        "polygon(13% 0, 100% 0, 100% 100%, 0% 100%)":"Trapèze"
                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.clipPath)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.clipPath : ""
                },
                "app-button-design-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.background)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.background : ""
                },
                "app-button-design-width": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.width)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.width : "auto"
                },
                "app-button-design-height": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.height)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.height : "auto"
                }, 
                "app-button-design-paddingLeft": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingLeft)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingLeft : "10"
                },
                "app-button-design-paddingRight": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingRight)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingRight : "10"
                },
                "app-button-design-paddingTop": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingTop)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingTop : "10"
                },
                "app-button-design-paddingBottom": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingBottom)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingBottom : "10"
                },
                "app-button-design-display": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.display)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.display : "inline-block"
                },
                "app-button-design-hover-background": { //hover
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color on hover')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.background)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.background : "grey"
                },
                "app-button-design-hover-color": { //hover
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Text color on hover')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.color)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.color : "white"
                },
                "app-button-design-marginTop": { 
                    "inputType" : "text",
                    "label":"<?php echo Yii::t('cms', 'Margin top')?>",
                    rules : {
                        number:true,
                        min:0,
                        max:100,

                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.marginTop)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.marginTop : "5"
                },                
                "app-button-design-hover-textDecoration": { //hover
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.textDecoration)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.textDecoration : "none"
                },
                //button*************
                "button-paddingTop": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Padding Top')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.paddingTop !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.paddingTop : "0"
                },
                "button-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "rules" : {
                        "number" :true,
                        "required":true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.fontSize : "30"
                },
                "button-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.color : "#000"
                },
                //scopeBtn*************
                "scopeBtn-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.scopeBtn.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.scopeBtn.color : "#000"
                },
                //filter
                "filterBtn-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.filterBtn.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.filterBtn.color : "#000"
                },
                //badge
                "badge-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.background !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.background : "#fff"
                },
                "badge-border": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border')?>",
                    "placeholder" : "50px solid white",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.border !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.border : ""
                },
                //connectBtn*************
                "connectBtn-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.background !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.background : ""
                },
                "connectBtn-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.color : ""
                },
                "connectBtn-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "placeholder" : "18",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.fontSize : "18"
                },
                "connectBtn-borderRadius": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border radius')?>",
                    "placeholder" : "0",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.borderRadius !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.borderRadius : "0"
                },
                "connectBtn-padding": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Padding')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "8px 8px",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.padding !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.padding : ""
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                    if(isActiveDivParent){
                        kt = k.split("-").join("][");
                        tplCtx.value[kt] = $("#"+k).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        dyFObj.closeForm();
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    $(".edit<?php echo $subkeyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        if(isDeveloper==false){
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties.boxShadow;
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties["button][paddingTop]"];
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties["connectBtn][padding]"];
        }
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
        var arrApp = [], arrButton = [],arrScopeBtn = [],arrFilterBtn = [],arrBadge = [],arrConnectBtn = [],arrSelectDesign = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {                
            kt = k.split("-");
            if (kt[0]=="app" && kt[1] == "button" && kt[2] == "design")
                arrSelectDesign.push('.'+k+val.inputType);
            else if (kt[0]=="app" && kt[1] == "button" && kt[2] != "design")
                arrApp.push('.'+k+val.inputType);
            else if(kt[0]=="button")
                arrButton.push('.'+k+val.inputType);
            else if(kt[0]=="badge")
                arrBadge.push('.'+k+val.inputType);
            else if(kt[0]=="scopeBtn")
                arrScopeBtn.push('.'+k+val.inputType);
            else if(kt[0]=="filterBtn")
                arrFilterBtn.push('.'+k+val.inputType);
            else if(kt[0]=="badge")
                arrBadge.push('.'+k+val.inputType);
            else if(kt[0]=="connectBtn")
                arrConnectBtn.push('.'+k+val.inputType);   
        });
        wrapToDiv(arrApp,"<?php echo $subkeyTpl ?>","app-css",3);
        wrapToDiv(arrSelectDesign,"<?php echo $subkeyTpl ?>","design-css",4,"",tplCtx.path,"app.button.design");
        wrapToDiv(arrButton,"<?php echo $subkeyTpl ?>","button-css",4);
        wrapToDiv(arrScopeBtn,"<?php echo $subkeyTpl ?>","scopeBtn-css",6,3);
        wrapToDiv(arrFilterBtn,"<?php echo $subkeyTpl ?>","filterBtn-css",6,3);
        wrapToDiv(arrBadge,"<?php echo $subkeyTpl ?>","badge-css",6);
        wrapToDiv(arrConnectBtn,"<?php echo $subkeyTpl ?>","connectBtn-css",2);                  
    });
});
});
</script>

