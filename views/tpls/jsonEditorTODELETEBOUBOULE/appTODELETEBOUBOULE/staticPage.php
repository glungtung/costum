<?php 
    $keyTpl = "app";

    $paramsData = [    
        "pages"       => [""=>""],
    ];
    
    foreach($paramsData as $i => $v) {
        if(isset($this->costum[$keyTpl]["#".$i])) 
        $paramsData[$i] =  (!empty($this->costum[$keyTpl]["#".$i])) ? true : false;      
    }

    if(isset($this->costum[$keyTpl])){
        foreach($this->costum[$keyTpl] as $k => $v){
            if(isset($this->costum[$keyTpl][$k]["staticPage"])){
                array_push($paramsData["pages"], array("key"=>$k, "icon"=>@$v["icon"], "name"=>$v["subdomainName"]));
            }
        }
    }
?>
<?php if($canEdit){ ?>
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= $this->costum["contextId"]; ?>' data-collection='<?= $this->costum["contextType"]; ?>' data-key='<?php echo $keyTpl ?>' data-path='costum.<?= $keyTpl ?>'>
    <i class="fa fa-caret-right" aria-hidden="true"></i> Page statique
</a><?php }?>


<script type="text/javascript">
var tplCtx={};
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    mylog.log("sectionParamsData----------------", sectionDyf.<?= $keyTpl ?>ParamsData);
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Static page')?>",
            "description" : "<?php echo Yii::t('sms', 'Choose which modules you want to activate and create your own static page.')?>",
            "icon" : "fa-cog",
            "properties" : {
                "pages" : {
                    "label" : "<?php echo Yii::t('cms', 'Static page')?>(s)",
                    "inputType" : "lists",
                    "entries":{
                        "key":{
                            "type":"hidden",
                            "class":""
                        },
                        "icon":{
                            "label":"<?php echo Yii::t('cms', 'Icon')?>",
                            "type":"text",
                            "class":"col-xs-3 no-padding"
                        },
                        "name":{
                            "label":"<?php echo Yii::t('cms', 'Name of the page')?>",
                            "type":"text",
                            "class":"col-xs-8"
                        }
                    }
                }
            },
            save : function (data) { 
                mylog.log("before",tplCtx);
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(k=="pages"){ 
                        $.each(data.pages,function(inc,va){
                            nameKeyPage=(typeof va.key != "undefined" && notEmpty(va.key)) ? va.key : "#"+va.name.replace(/[^\w]/gi, '').toLowerCase();
                            tplCtx.value[nameKeyPage] = {
                                hash : "#app.view",
                                icon : va.icon,
                                urlExtra : "/page/"+nameKeyPage.replace("#","")+"/url/costum.views.tpls.staticPage",
                                useHeader : true,
                                isTemplate : true,
                                useFooter : true,
                                useFilter : false,
                                subdomainName : va.name,
                                staticPage : true
                            }
                        });
                    }
              
                        
                 });
                mylog.log("save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("App bien ajouté");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>