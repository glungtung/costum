<!-- By RAMIANDRISON Jean Dieu Donné -->
<!-- email: ramiandrison.jdd@gmail.com -->
<?php 
    $keyTpl = "typeObj";

    $paramsData = [
        "typeObj" =>  [""=>""]
    ];

    if( isset($this->costum[$keyTpl]) ) {
        $paramsData = $this->costum[$keyTpl];
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> typeObj
    </a>
<?php } ?>
<style>
    <?php foreach($paramsData as $i => $v) { ?>
        .<?php echo $keyTpl ?>.<?php echo $i ?>-typeObj:before {
            content: "<?php echo $i ?>";
        }
    <?php } ?>
</style>
<script>
    var typeObjName = "";
    var props = {};
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $keyTpl ?>usual = function(k){
        props[k+"-color"]= {
            "inputType" : "colorpicker",
            "label" : "color",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].color !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].color : ""
        };
        props[k+"-name"]= {
            "inputType" : "text",
            "label" : "text",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].name !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].name : ""
        };
        props[k+"-add"]= {
            "inputType" : "select",
            "label" : "add",
            "options" : {
                "onlyAdmin" : "onlyAdmin",
                "true" : "libre"
            },
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].add !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].add : ""
        };
        props[k+"-createLabel"]= {
            "inputType" : "text",
            "label" : "createLabel",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].createLabel !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].createLabel : ""
        };
        props[k+"-icon"]= {
            "inputType" : "text",
            "label" : "icon",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].icon !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].icon : ""
        };
        props[k+"-sameAs"]= {
            "inputType" : "text",
            "label" : "sameAs",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].sameAs !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].sameAs : "poi"
        };
        props[k+"-formType"]= {
            "inputType" : "select",
            "label" : "formType",
            "options" : poiFilters,
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].formType !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].formType : null
        };
        props[k+"-formParent"]= {
            "inputType" : "text",
            "label" : "formParent",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].formParent !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].formParent : "poi"
        };
    } 

    sectionDyf.<?php echo $keyTpl ?>CreateArrayForClassAndAddNewProps = function(paramsData){
        $.each(paramsData,function(k,v){
            sectionDyf[k+"<?php echo $keyTpl ?>"]=[];
            sectionDyf.<?php echo $keyTpl ?>usual(k);
            if(typeof v.dynFormCostum != "undefined" && 
                typeof v.dynFormCostum.beforeBuild != "undefined".properties &&
                typeof v.dynFormCostum.beforeBuild.properties != "undefined")
            {
                $.each(v.dynFormCostum.beforeBuild.properties,function(kk,vv){
                    if(vv.inputType == "text")
                        typeObjInput.<?php echo $keyTpl ?>text(k,vv.inputType,kk,vv.label,vv.placeholder)
                })
            }        

        });
    }

    sectionDyf.<?php echo $keyTpl ?>PushToClassArray = function(){
        var arrParamsData = Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData);
        $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(kd,vd){
                  mylog.log('kd',kd,kk[0]);
                if(kk[0] == kd)
                    sectionDyf[kd+"<?php echo $keyTpl ?>"].push('.'+k+val.inputType);
            });
            // if(/*jQuery.inArray(kk[0],arrParamsData)*/arrParamsData.indexOf(kk[0]) > 0 )
            //     sectionDyf[kk[0]+"<?php //echo $keyTpl ?>"].push('.'+k+val.inputType);                      
        });
    } 

    sectionDyf.<?php echo $keyTpl ?>WrapAppToDiv = function(paramsData){
        $.each(paramsData,function(k,v){
                wrapToDiv(sectionDyf[k+"<?php echo $keyTpl ?>"],"<?php echo $keyTpl ?>",k+"-typeObj",3,"",tplCtx.path,k);
                $('.'+k+"-typeObj").after('<div class="col-md-12"><button type="button" data-typeObj="'+k+'" class="btn btn-default pulse-butto addNewInput">add inputType</button></div>');
        });
    }
    /*sectionDyf.<?php //echo $keyTpl ?>newTypeObj = function(k){

        props[k+"-dynFormCostum-beforeBuild-properties-info-html"]= {
            "inputType" : "text",
            "label" : "info-html",
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.info !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.info.html !="undefined") ?
                    sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.info.html : ""
        };
/
        props[k+"-dynFormCostum-beforeBuild-properties-name-label"]= {
            "inputType" : "text",
            "label" : "name-label",
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.name !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.name.label !="undefined") ?
                    sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.name.label : ""
        };
        /*props[k+"-dynFormCostum-beforeBuild-properties-name-placeholder"]= {
            "inputType" : "text",
            "label" : "name-placeholder",
            value : (typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.name !="undefined"
                    && typeof sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.name.label !="undefined") ?
                    sectionDyf.<?php ////echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.name.label : ""
        };

        props[k+"-dynFormCostum-beforeBuild-properties-shortDescription-inputType"]= {
            "inputType" : "hidden",
            value: "textarea"
        };
        props[k+"-dynFormCostum-beforeBuild-properties-shortDescription-order"]= {
            "inputType" : "numeric",
            "label" : "shortDescription-order",
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.shortDescription !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.shortDescription.order !="undefined") ?
                    sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.shortDescription.order : ""
        };
        props[k+"-dynFormCostum-beforeBuild-properties-shortDescription-label"]= {
            "inputType" : "numeric",
            "label" : "shortDescription-label",
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.shortDescription !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.shortDescription.label !="undefined") ?
                    sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.shortDescription.label : ""
        };
        props[k+"-dynFormCostum-beforeBuild-properties-shortDescription-rules"]= {
            "inputType" : "lists",
            "label": "shortDescription-rules",
            "entries":{
                        "rule":{
                            "label":"rule",
                            "type":"text",
                            "class":"col-xs-6"
                        },
                        "value":{
                            "label":"value",
                            "type":"text",
                            "class":"col-xs-6"
                        }
                    }
        };

        props[k+"-dynFormCostum-beforeBuild-properties-parent-label"]= {
            "inputType" : "text",
            "label" : "parent-label",
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.parent !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.parent.label !="undefined") ?
                    sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.parent.label : "Auteur(s)"
        };



        props[k+"-dynFormCostum-beforeBuild-properties-description-label"]= {
            "inputType" : "textarea",
            "label" : "description-label",
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.description !="undefined"
                    && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.description.label !="undefined") ?
                    sectionDyf.<?php //echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties.description.label : ""
        };
        props[k+"-dynFormCostum-beforeBuild-properties-description-inputType"]= {
            "inputType" : "hidden",
            value : "textarea"
        };
    }*/


    sectionDyf.<?php echo $keyTpl ?>addForm =  
            '<form id="<?php echo $keyTpl ?>form" class="form" role="form">'+
                '<div class="form-group">'+
                  '<label for="typeObjName">Nom du typeObj</label>'+
                  '<input type="text" class="form-control" id="typeObjName" name="typeObjName" placeholder="nom du typeObj"/>'+
                '</div>'+
            '</form>';

    sectionDyf.<?php echo $keyTpl ?>newInput =  
            '<form id="<?php echo $keyTpl ?>newInput" class="form" role="form">'+
                '<div class="col-md-12">'+
                    '<div class="form-group">'+
                        '<label for="exampleInputEmail1">input type</label>'+
                        '<select class="form-control" id="inputType" name="inputType" placeholder="type">'+
                            '<option value="text">text</option>'+
                            '<option value="textarea">textarea</option>'+
                            '<option value="select">select</option>'+
                            '<option value="checkboxSimple">checkboxSimple</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-12">'+
                    '<div class="form-group">'+
                        '<label for="exampleInputEmail1">name </label>'+
                        '<input type="text" class="form-control" id="inputName" name="inputName" placeholder="name"/>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-12">'+
                    '<div class="form-group">'+
                        '<label for="exampleInputEmail1">label </label>'+
                        '<input type="text" class="form-control" id="inputLabel" name="inputLabel" placeholder="label"/>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-12">'+
                    '<div class="form-group">'+
                        '<label for="exampleInputEmail1">placeholder </label>'+
                        '<input type="text" class="form-control" id="inputPlaceholder" name="inputPlaceholder" placeholder="placeholder"/>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-12 optionsSelect">'+
                    '<div class="col-md-12">'+
                        '<label class="letter-green-k">options </label>'+
                    '</div>'+
                    '<div class="col-md-12 optionContainer0">'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label for="exampleInputEmail1">value </label>'+
                                '<input type="text" class="form-control" id="optionsSelectValue0" name="optionsSelectValue0" placeholder="value"/>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label>label </label>'+
                                '<input type="text" class="form-control" id="inputPlaceholder0" name="inputPlaceholder0" placeholder="label"/>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="form-group">'+
                                '<label> </label>'+
                                '<button type="button" class="btn btn-danger remove-option text-center"> <i class="fa fa-times"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                        '<button type="button" class="btn btn-success add-option text-center"> <i class="fa fa-plus"></i> ajouter option </button>'+
                    '</div>'+
                '</div>'+
            '</form>';

    sectionDyf.<?php echo $keyTpl ?>openDynform = function(name){
        $('.addNewInput').click(function(){
            sectionDyf.<?php echo $keyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $keyTpl ?>ParamsData);
            dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
            sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
            sectionDyf.<?php echo $keyTpl ?>WrapAppToDiv(sectionDyf.<?php echo $keyTpl ?>ParamsData);
            sectionDyf.<?php echo $keyTpl ?>openDynform(name);
        })      
    }

    jQuery(document).ready(function() {
            sectionDyf.<?php echo $keyTpl ?>Params = {
                "jsonSchema" : {    
                    "title" : "typeObj",
                    "icon" : "fa-cog",
                    "properties" : props,
                    save : function (data) { 
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                            var kk= k.split("-").join("][");
                            tplCtx.value[kk] = $("#"+k).val();
                        });

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            tplCtx.updatePartial=true;tplCtx.removeCache=true;
                            tplCtx.format=true;
                            dataHelper.path2Value( tplCtx, function(params) { 
                                $("#ajax-modal").modal('hide');
                                toastr.success("Bien ajouté");
                                location.reload();
                            } );
                        }
                    }
                }
            };

            $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = $(this).data("path"); 

                bootbox.dialog({
                    title: "<h5 class='text-success text-center'>Les typeObj existants</h5>"+
                           "<h6 class='text-danger text-bold text-center'></h6>",
                    message: sectionDyf.<?php echo $keyTpl ?>addForm,
                    buttons: {
                        cancel: {
                            label: "Afficher existants",
                            className: 'btn-info',
                            callback: function(){
                                   sectionDyf.<?php echo $keyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                                   dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
                                    sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
                                    sectionDyf.<?php echo $keyTpl ?>WrapAppToDiv(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                                    $('.addNewInput').click(function(){
                                        newInput ($(this).data('typeobj'));
                                    }) 
                            }
                        },
                        ok: {
                            label: "Créer nouveau",
                            className: 'btn-success',
                            callback: function(){
                                typeObjName = $('#typeObjName').val();
                                if(typeObjName != ''){
                                    sectionDyf.<?php echo $keyTpl ?>ParamsData[typeObjName] = {};
                                    sectionDyf.<?php echo $keyTpl ?>usual(typeObjName);
                                     sectionDyf.<?php echo $keyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                                    dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
                                    sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
                                    sectionDyf.<?php echo $keyTpl ?>WrapAppToDiv(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                                    $('head').append('<style>.<?php echo $keyTpl ?>.'+typeObjName+'-typeObj:before {content : '+"\'"+typeObjName+"\'"+'}</style>');
                                    //sectionDyf.<?php //echo $keyTpl ?>openDynform();
                                }
                            }
                        }
                    }
                });
                $('.bootbox-accept').prop("disabled",true); 
                    $('#typeObjName').keyup(function(){
                        if($(this).val()!="")
                            $('.bootbox-accept').removeAttr("disabled");
                        else if($(this).val() =="")
                            $('.bootbox-accept').prop("disabled",true);
                })
            });
    })
    function newInput (typeObjNam){
        bootbox.dialog({
            title: "<h5 class='text-success text-center'>New input</h5>",
            message: sectionDyf.<?php echo $keyTpl ?>newInput,
            buttons: {
                cancel: {
                    label: "Fermer",
                    className: 'btn-default',
                    callback: function(){
                       // sectionDyf.<?php //echo $keyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php //echo $keyTpl ?>ParamsData);
                       // dyFObj.openForm( sectionDyf.<?php //echo $keyTpl ?>Params,null, sectionDyf.<?php //echo $keyTpl ?>ParamsData);
                       //   $('.'+k+"-typeObj").after('<div class="col-md-12"><button type="button" data-typeObj="'+k+'" class="btn btn-default pulse-butto addNewInput">add inputType</button></div>');
                       //  sectionDyf.<?php //echo $keyTpl ?>openDynform();
                    }
                },
                ok: {
                    label: "Ajouter",
                    className: 'btn-success',
                    callback: function(){
                        var inputType = $('#inputType').val();
                        var inputName = $('#inputName').val();
                        var inputLabel = $('#inputLabel').val();
                        var inputPlaceholder = $('#inputPlaceholder').val();
                        if(inputType == "text" || inputType == "textarea")
                            typeObjInput.<?php echo $keyTpl ?>text(typeObjNam,inputType,inputName,inputLabel,inputPlaceholder);

                        sectionDyf.<?php echo $keyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
                        sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
                        sectionDyf.<?php echo $keyTpl ?>WrapAppToDiv(sectionDyf.<?php echo $keyTpl ?>ParamsData);
                        sectionDyf.<?php echo $keyTpl ?>openDynform(typeObjNam);
                        $('.addNewInput').click(function(){
                            newInput ($(this).data('typeobj'));
                        }) 
                        
                    }
                }
            }
        });
        var i<?php echo $keyTpl ?> = 1;
        $('.add-option').click(function(){
            $(this).before(
                '<div class="col-md-12 optionContainer0">'+
                    '<div class="col-md-4">'+
                        '<div class="form-group">'+
                            '<label>value </label>'+
                            '<input type="text" class="form-control" id="optionsSelectValue'+i<?php echo $keyTpl ?>+'" name="optionsSelectValue'+i<?php echo $keyTpl ?>+'" placeholder="value"/>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-4">'+
                        '<div class="form-group">'+
                            '<label>label </label>'+
                            '<input type="text" class="form-control" id="inputPlaceholder'+i<?php echo $keyTpl ?>+'" name="inputPlaceholder'+i<?php echo $keyTpl ?>+'" placeholder="label"/>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="form-group">'+
                            '<label> </label>'+
                            '<button type="button" class="btn btn-danger remove-option text-center"> <i class="fa fa-times"></i> remove </button>'+
                        '</div>'+
                    '</div>'+
                '</div>');
                 $('.remove-option').click(function(){
                    $(this).parent().parent().parent().remove();
                 })
            i<?php echo $keyTpl ?>++;
        })
    }
</script>
<?php echo $this->renderPartial("costum.views.tpls.jsonEditor.typeObj.inputObj", array("keyTpl" => $keyTpl)); ?>
