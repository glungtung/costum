<!-- By RAMIANDRISON Jean Dieu Donné -->
<!-- email: ramiandrison.jdd@gmail.com -->
<?php 
    $keyTpl = "typeObj";

    $paramsData = [
        "typeObj" =>  [""=>""]
    ];

    if( isset($this->costum[$keyTpl]) ) {
        $paramsData = $this->costum[$keyTpl];
    }

    $cssAnsScriptFilesModule = array(
            '/plugins/font-awesome/list.js',
          );
        HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
    ?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>'
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-check-square-o"></i> Formulaires
    </a>

<?php }
    $elementsArr = ["organization", "projects", "poi", "ressource", "job", "classified", "proposals"];
?>
<style>

<?php if(count($paramsData) != 0){
        foreach($paramsData as $i => $v) { ?>
            .<?php echo $keyTpl ?>.<?php echo $i ?>-typeObj:before {
                content: "<?php echo $i ?>";
            }
<?php   }
     }else{
        foreach($elementsArr as $i => $v) { ?>
            .<?php echo $keyTpl ?>.<?php echo $v ?>-typeObj:before {
                content: "<?php echo $v ?>";
            }
<?php   } 
} ?>     


</style>

<script>
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $keyTpl ?>element = <?php echo json_encode($elementsArr); ?>;
    var colorOption = {
        "black" : "black",
        "blue" : "blue",
        "brown" : "brown",
        "lightblue" : "lightblue",
        "lightpurple" : "lightpurple",
        "darkblue" : "darkblue",
        "green" : "green",
        "orange" : "orange",
        "red" : "red",
        "yellow" : "yellow",
        "yellow-k" : "yellow-k",
        "purple" : "purple",
        "azure" : "azure",
        "pink" : "pink",
        "phink" : "phink",
        "dark" : "dark",
        "green-k" : "green-k",
        "red-k" : "red-k",
        "blue-k" : "blue-k",
        "nightblue" : "nightblue",
        "turq" : "turq",
        "green-k" : "green-k",
    };

    var props = {};
    sectionDyf.<?php echo $keyTpl ?>init = function(k){
        props[k+"-color"]= {
            "inputType" : "select",
            "label" : "Couleur",
            "options" : colorOption,
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].color !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].color : ""
        };
        props[k+"-name"]= {
            "inputType" : "text",
            "label" : "Nom de l’element",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].name !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].name : ""
        };
        props[k+"-createLabel"]= {
            "inputType" : "text",
            "label" : "Label de création",
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].createLabel !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].createLabel : ""
        };
        props[k+"-icon"]= {
            "inputType" : "select",
            "label" : "icon",
            "options" : fontAwesome,
            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k] !="undefined"
                 && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData[k].icon !="undefined") ?
                sectionDyf.<?php echo $keyTpl ?>ParamsData[k].icon : ""
        };
        /*props[k+"-add"]= {
            "inputType" : "select",
            "label" : "add",
            "options" : {
                "onlyAdmin" : "onlyAdmin",
                "true" : "libre"
            },
            value : (typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData !="undefined"
                && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData[k] !="undefined"
                && typeof sectionDyf.<?php //echo $keyTpl ?>ParamsData[k].add !="undefined") ?
                sectionDyf.<?php //echo $keyTpl ?>ParamsData[k].add : ""
        };*/
        props[k+"-add"]= {
            "inputType" : "hidden",
            value : true
        };
    }

    sectionDyf.<?php echo $keyTpl ?>elementClassArray = function(paramsData){
        if(Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).length == 0){
            $.each(sectionDyf.<?php echo $keyTpl ?>element,function(k,v){
                sectionDyf[v+"<?php echo $keyTpl ?>"]=[];
            })
        }else{
            $.each(paramsData,function(k,v){
                sectionDyf[k+"<?php echo $keyTpl ?>"]=[];
            })
        }
    }
    sectionDyf.<?php echo $keyTpl ?>PushToClassArray = function(){
        var arrParamsData = Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).length != 0 ? Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData) : sectionDyf.<?php echo $keyTpl ?>element ;

        if(Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).length != 0){
            $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) {
                var kk= k.split("-");
                $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(kd,vd){
                      mylog.log('kd',kd,kk[0]);
                    if(kk[0] == kd)
                        sectionDyf[kd+"<?php echo $keyTpl ?>"].push('.'+k+val.inputType);
                });
                // if(/*jQuery.inArray(kk[0],arrParamsData)*/arrParamsData.indexOf(kk[0]) > 0 )
                //     sectionDyf[kk[0]+"<?php //echo $keyTpl ?>"].push('.'+k+val.inputType);                      
            });
        }else{
            $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) {
                var kk= k.split("-");
                $.each(sectionDyf.<?php echo $keyTpl ?>element,function(kd,vd){
                      mylog.log('vd',vd,kk[0]);
                    if(kk[0] == vd)
                        sectionDyf[vd+"<?php echo $keyTpl ?>"].push('.'+k+val.inputType);
                });
                // if(/*jQuery.inArray(kk[0],arrParamsData)*/arrParamsData.indexOf(kk[0]) > 0 )
                //     sectionDyf[kk[0]+"<?php //echo $keyTpl ?>"].push('.'+k+val.inputType);                      
            });
        }
    } 

    jQuery(document).ready(function() {
        if(Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).length == 0){
            $.each(sectionDyf.<?php echo $keyTpl ?>element,function(k,v){
                sectionDyf.<?php echo $keyTpl ?>init(v)
            });
        }else{
             $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(k,v){
                 sectionDyf.<?php echo $keyTpl ?>init(k)
             });
        }
        
        sectionDyf.<?php echo $keyTpl ?>Params = {
            "jsonSchema" : {    
                "title" : "Edition des éléments existants",
                "icon" : "fa-cog",
                "properties" : props,
                save : function (data) { 
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                        if($("#"+k).parent().parent().data("activated")!=false){
                            var kk= k.split("-").join("][");
                            tplCtx.value[kk] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        tplCtx.updatePartial=true;tplCtx.removeCache=true;
                        tplCtx.format=true;
                        dataHelper.path2Value( tplCtx, function(params) { 
                            $("#ajax-modal").modal('hide');
                            toastr.success("Bien ajouté");
                            location.reload();
                        } );
                    }
                }
            }
        };

        $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = $(this).data("path");
            dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
            if(Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).length != 0)
                sectionDyf.<?php echo $keyTpl ?>elementClassArray(sectionDyf.<?php echo $keyTpl ?>ParamsData);
            else
                sectionDyf.<?php echo $keyTpl ?>elementClassArray(sectionDyf.<?php echo $keyTpl ?>element);
            sectionDyf.<?php echo $keyTpl ?>PushToClassArray();
            if(Object.keys(sectionDyf.<?php echo $keyTpl ?>ParamsData).length != 0){
                $.each(sectionDyf.<?php echo $keyTpl ?>ParamsData,function(k,v){
                    wrapToDiv(sectionDyf[k+"<?php echo $keyTpl ?>"],"<?php echo $keyTpl ?>",k+"-typeObj",4,"",tplCtx.path,k);
                })
            }else{
                $.each(sectionDyf.<?php echo $keyTpl ?>element,function(k,v){
                    wrapToDiv(sectionDyf[v+"<?php echo $keyTpl ?>"],"<?php echo $keyTpl ?>",v+"-typeObj",4,"",tplCtx.path,v);
                })
            }
        });

    })
</script>
