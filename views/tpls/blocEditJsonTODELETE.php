<?php

$keyTpl = "blocEditJson";

$paramsData = array(
    "css"   => $this->costum["css"],
    "app"   => $this->costum["app"],
);

if($canEdit): ?>
    <a class=' edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= $this->costum["contextId"]; ?>' data-collection='<?= $this->costum["contextType"]; ?>' data-key='<?php echo $keyTpl ?>' data-path='costum'>
     Config json <i class="fa fa-pencil"></i>
    </a>
<?php endif ?>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {

    var blocCss = JSON.stringify(sectionDyf.<?php echo $keyTpl ?>ParamsData.css);
    var blocApp = JSON.stringify(sectionDyf.<?php echo $keyTpl ?>ParamsData.app);

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer le json",
            "description" : "Vous êtes un développeur ? Modifier directement le json pour encore plus personnalisée votre COstum",
            "icon" : "fa-cog",
            "properties" : {
                "css" : {
                    "inputType" : "textarea",
                    "label" : "JSON CSS",
                    "markdown" : true,
                    value :  blocCss
                },
                "app": {
                    "inputType" : "textarea",
                    "label" : "JSON APP",
                    "markdown" : true,
                    value : blocApp
                }
            },
            save : function () {
                tplCtx.value = {
                    slug : "<?= $this->costum["slug"] ?>",
                    tpls : <?= @json_encode($this->costum["tpls"]) ?>
                };

                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = JSON.parse($("#"+k).val());
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");

        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
        console.log(tplCtx);
    });

});
</script>