<?php 
    /**
     * TPLS QUI PERMET AFFICHAGE DES NEWS
     */
    $keyTpl = "ressource";

    $defaultcolor = "white";
    $tags = "structags";
    $paramsData = [ "titleright" => "",
                    "descriptioneright" =>  "",
                    "titleleft" =>  "",
                    "descriptionleft"   =>  "",
                    "color" => "#333333",
                    "backgroundright" => "#e7e6e6",
                    "backgroundleft" => "#32a2a4",
                    "defaultcolor" => "white",
                    "tags" => "structags"
                    ];

    if( isset($this->costum["tpls"][$keyTpl]) ) {
        foreach ($paramsData as $i => $v) {
            if( isset($this->costum["tpls"][$keyTpl][$i]) ) 
                $paramsData[$i] =  $this->costum["tpls"][$keyTpl][$i];      
        }
    }
    ?>
<style>
.ressource{
    background : white;
    padding:1%;
    box-shadow: 5px 5px 20px #e4dbdb;
    margin-top: -5%;
}
.bloc{
    padding: 7%;
}
.bloc h2{
    text-transform: none;
}
.btn-perso{
    padding: 3%;
    background: white;
    border-radius: 5%;
    font-size: 2rem;
    box-shadow: 5px 5px 20px #746f6f;
    text-decoration: none !important;
}
</style>
<div class="ressource container">
    <div class="bloc col-md-6" style="
    background-color: <?= $paramsData["backgroundright"]; ?>; 
    color : <?= $paramsData["color"]; ?>;
    ">
        <h2> <?= @$this->costum["tpls"]["ressource"]["titleright"]; ?> </h2>
        <p style="margin-bottom: 10%; margin-top: 5%;"> <?= @$this->costum["tpls"]["ressource"]["descriptionright"]; ?> </p>

        <a onclick="dyFObj.openForm('ressources');" href="javascript:;"  class="btn-perso"> Proposer son aide </a>
        </div>

        <div class="bloc col-md-6" style="
        background-color: <?= $paramsData["backgroundleft"]; ?>;    
        color : <?= $paramsData["color"]; ?>;
        ">
        <h2> <?= @$this->costum["tpls"]["ressource"]["titleleft"]; ?> </h2>
        <p style="margin-bottom: 10%; margin-top: 5%;"> <?= @$this->costum["tpls"]["ressource"]["descriptionleft"]; ?> </p>

        <a onclick="dyFObj.openForm('ressources');" href="javascript:;" class="btn-perso"> Demander de l'aide </a>
    </div>

</div>
<?php  
//    echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]);
?>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $keyTpl ?> config",
            "description" : "Liste de question possible",
            "icon" : "fa-cog",
            "properties" : {
                "titleright" : {
                    label : "Titre section droite",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.titleright
                },
                "descriptionright" : {
                    label : "Description section droite",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.descriptionright
                },
                "titleleft" : {
                    label : "Titre section gauche",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.titleleft
                },
                "descriptionleft" : {
                    label : "Description section droite",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.descriptionleft
                },
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                "backgroundright" : {
                    label : "Couleur du fond section droite",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.backgroundright
                },
                "backgroundleft" : {
                    label : "Couleur du fond section gauche",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.backgroundleft
                },
                defaultcolor : {
                    label : "Couleur",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.defaultcolor
                },
                tags : {
                    inputType : "tags",
                    label : "Tags",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.tags
                },
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>