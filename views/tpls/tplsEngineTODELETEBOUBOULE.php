<?php 
    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    HtmlHelper::registerCssAndScriptsFiles([
      "/js/blockcms/fontAwesome/fontAwesome.js",
      "/cmsBuilder/js/cmsBuilder.js",
      "/cmsBuilder/js/template.js"
    ], Yii::app()->getModule('costum')->assetsUrl);

    $thisContextId = isset($this->costum["contextId"]) ? $this->costum["contextId"] : (string) $el["_id"];
    $thisContextType = isset($this->costum["contextType"]) ? $this->costum["contextType"] : $el["collection"];
    $thisContextSlug = isset($this->costum["contextSlug"]) ? $this->costum["contextSlug"] : $el["slug"];
    $thisSlug = isset($this->costum["slug"]) ? $this->costum["slug"] : $el["slug"];
    // $el             = Element::getByTypeAndId($thisContextType, $thisContextId );
    $thisContextName = $el["name"];

    $tplUsingId     = "";
    $tplInitImage   = "";
    $nbrTplUser     = 0; 
    $nbrTplViewer   = 0;
    $paramsData     = [];
    $insideTplInUse = array();
    $cmsList        = array();
    $newCmsId       = array();
    $cmsInUseId     = array();

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $page = ( isset($page) && !empty($page) ) ? $page : "welcome";
    $defaultsHomepage = file_get_contents("../../modules/costum/data/blockCms/config/defaultHomepage.json", FILE_USE_INCLUDE_PATH);/*
?>
 <style>
   <?php if(!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) && isset(Yii::app()->session["user"]["slug"]) && !in_array(Yii::app()->session["user"]["slug"], ["mirana", "gova", "ifals", "devchriston", "nicoss","Jaona","anatolerakotoson","yorre"])){      
     ?>
      .socialEntityBtnActions .editDescBlock, .socialEntityBtnActions .deleteCms{
        display: none;
      }
   <?php } ?>

   .super-href {
    cursor: pointer !important;
   }
   .mytext {
    cursor: default;
   }
   .mytext a{
    color: inherit;
   }

.sp-is-loading  {
    background: #eee;
    background: linear-gradient(110deg, transparent 8%, #d7e2ed6b 18%, transparent 33%);
    border-radius: 5px;
    background-size: 200% 100%;
    animation: 1.5s shine cubic-bezier(0.14, 0.58, 0.81, 0.74) infinite;

}

@keyframes shine {
  100% {
    background-position-x: -200%;
  }
}

 </style>

<?php */
// Get page name to specify blocks-----------------------------------------
$page = ( isset($page) && !empty($page) ) ? $page : "welcome";

// Get all cms can be use-----------------------------------------
$listCmsCanBeUse = Cms::getCmsByWhere(array(
  "type" => "blockCms"),array( 'name' => 1 )
);
// Get all template except back-up-----------------------------------------
$listTemplate = Cms::getCmsByWhere(array(
  "type" => "template",
  "tplsUser.".$thisContextId.".".$page => array('$ne' => "backup"))
);

// Get sample template-----------------------------------------
$sampleTemplate = PHDB::find("cms", array("type" => "template","category"  =>  "Sample"));

// Get tpl in use----------------------------------------------------------
$using = Cms::getCmsByWhere(array("tplsUser.".$thisContextId.".".$page => "using"));

// Get cms haven't tpl parent----------------------------------------------
$newCms = Cms::getCmsByWhere(array(
    "page"=>$page,
    "haveTpl" => "false",
    "parent.".$thisContextId => array('$exists'=>1)),
     array("position" => 1)
   );

/*if(empty($newCms) && ($page == "welcome" || $page == "homeElement")){
  $defaultsHomepage = json_decode($defaultsHomepage,true);
  foreach($defaultsHomepage as $kdef => $vdef){
    $defaultsHomepage[$kdef]["parent"] = [
      $thisContextId => [
        "type" => $thisContextType,
        "name" => $thisContextName
      ]
    ];
    $defaultsHomepage[$kdef]["page"]=$page;
  }
  PHDB::batchInsert(Cms::COLLECTION,$defaultsHomepage);
  $newCms = Cms::getCmsByWhere(array(
    "page"=>$page,
    "haveTpl" => "false",
    "parent.".$thisContextId => array('$exists'=>1)),
     array("position" => 1)
   );
}*/

// Get template in use-----------------------------------------------------
if (!empty($using)) {
  $tplUsingId = isset(array_keys($using)['0']) ? array_keys($using)['0'] : "";
  $insideTplInUse = $using[$tplUsingId]; 
  $tplInitImage = Document::getListDocumentsWhere(array("id"=> $tplUsingId, "type"=>'cms'), "file");
// Get cms liste in use----------------------------------------------------
  $cmsList = Cms::getCmsByWhere(array(
      "tplParent" => $tplUsingId,
      "parent.".$thisContextId => array('$exists'=>1),
      "page" => $page),
      array("position" => 1)
  );
}
$tplCreator   = isset($insideTplInUse["parent"]) ? $insideTplInUse["parent"] : [];
$tplCreatorId = isset(array_keys($tplCreator)["0"]) ? array_keys($tplCreator)["0"] : "";
$tplCms       = isset($insideTplInUse["cmsList"]) ? $insideTplInUse["cmsList"] : [];

// Get template's page original needed for update button condition-----------------------------
$tplPage = isset($insideTplInUse["page"]) ? $insideTplInUse["page"] : "";

// Get each Id of cms in use------------------------------------------------------------------
foreach (array_filter($cmsList) as $key => $value) {
 $cmsInUseId[] = (string)$value['_id'];
}  
// Get each cms haven't template (newCms)-----------------------------------------------------
foreach ($newCms as $key => $value) {
  $cmsList[] = $value;
  $newCmsId[] = (string)$value["_id"];
}

// Merge cms id inside template and new cms---------------------------------------------------
  $idCmsMerged = array_merge($cmsInUseId,$newCmsId);

// Update button Condition--------------------------------------------------------------------
  $tplEdited = false;
  if ($newCmsId != []) {
   $tplEdited = true;
  }

  $btnSave = false;
  $btnSaveChange = false;
  if ($tplUsingId != "") {
    $btnSave = ($idCmsMerged != $tplCms && $idCmsMerged != []);
    $btnSaveChange = ($tplCreatorId == $thisContextId && $idCmsMerged != $tplCms && $tplPage == $page);
  }elseif ($newCmsId != null){
    $btnSave = true;
  }
?>

<script type="text/javascript">
    var isInterfaceAdmin = false;
    var canUse = false;
    var thisContextId = notNull(costum) ? costum.contextId : contextId;
    var thisContextType = notNull(costum) ? costum.contextType : contextType ;
    var thisContextSlug = notNull(costum) ? costum.contextSlug : slug ;
    var thisContextName = <?= json_encode($thisContextName) ?>;
    var costumAssetsPath = "<?php echo $assetsUrl ?>";
    var itemClassArray = [];
    var sectionDyf = {};
    var tplCtx = {};
    var isInterfaceAdmin = false;
    var page = "<?= $page ?>";
    var pageListe = <?php echo json_encode($this->costum["app"]) ?>;
    var sampleStcId = "";
    var sampleStcEl = [];
    var sampleId = "";
    var sampleEl = [];
    var tplUsingId = "";
    var mode = localStorage.getItem("previewMode");
    var cmsChildren = {}  
  <?php if(Authorisation::isInterfaceAdmin()){  ?>
    isInterfaceAdmin = true;
  <?php } ?>
    var DFdata = {
      'tpl'  : '<?php echo $tpl ?>',
      "id"   : "<?php echo $thisContextId ?>"
    };
    if(notNull(costum)){
      costum.col = "<?php echo $thisContextType ?>";
      costum.ctrl = "<?php echo Element::getControlerByCollection($thisContextType) ?>";
    }
   

    var configDynForm = <?php echo json_encode((isset($this->costum['dynForm'])) ? $this->costum['dynForm']:null); ?>;
    var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;
    var tplAuthorisation = <?php echo json_encode((isset($me["roles"]["superAdmin"])) ? $me["roles"]["superAdmin"]:false); ?>;
</script>

<?php
if(Authorisation::isInterfaceAdmin()){  ?>
<div class="saveTemplate whole-page" style="display: none;">
   <div id="saveTemplate">
      <nav class="tpl-engine-btn-list">
         <ul>
            <li class="a hiddenPreview">
               <a href="javascript:;" class="activeRC">
               </a>
            </li>
            <li class="d view-super-cms hiddenPreview" onclick="tplObj.previewTpl()">
               <a class="" href="javascript:;"> <?php echo Yii::t("common", "Preview")?>
               <i class="fa fa-eye"></i>
               </a>
            </li>

            <li class="a hiddenPreview">
               <a href="javascript:;" id="choix" class="tryOtherTpl"> <?php echo Yii::t("common", "Choose a template")?> 
               <i class="fa fa-list"></i>
               </a>
            </li>
            <?php if ($btnSave == true ) { ?>               
            <li class="b hiddenPreview">
               <a href="javascript:;" class="saveThisTpls" onclick="tplObj.saveThisTpls()"> 
                <?php echo Yii::t("common", "Save as new template")?> 
               <i class="fa fa-save"></i>
               </a>
            </li>
            <?php } ?>
            <?php if ($btnSaveChange == true && $idCmsMerged != "") { ?> 
            <li class="c hiddenPreview">
               <a href="javascript:;" class="saveChangeTpl"><?php echo Yii::t("common", "Save change")?>
               <i class="fa fa-refresh"></i>
               </a>
            </li>
            <?php } ?>
            <?php if(isset($this->costum["contextSlug"])){ ?>
                <!-- <li class="e hiddenPreview">
                  <a id="go-to-communecter" href="" data-hash="#page.type.<?= $thisContextType ?>.id.<?= $thisContextSlug ?>" target="_blank"> 
                    <?php echo Yii::t("common", "Go to communecter")?>
                  <i class="fa fa-id-card-o" aria-hidden="true">
                </i> 
                  </a>
                </li> -->
                <li class="g hiddenPreview"><?php echo $this->renderPartial("costum.views.tpls.jsonEditor.app.app2", array("canEdit" => $canEdit)); ?></li>
                <li class="g gCoForm hiddenPreview"><?php echo $this->renderPartial("costum.views.tpls.blocCoForm", array("canEdit" => $canEdit)); ?></li>            
                <li class="i hiddenPreview">
                  <a class="" data-toggle="modal" data-target="#menuJson" href="javascript:;">
                    <?php echo Yii::t("common", "Advanced settings")?>
                    <i class="fa fa-wrench"> </i>
                  </a>
                </li>
            <?php } ?>
            <!-- <li class="j">
                <a class="openFormDocumentation" data-toggle="modal" data-target="" href="javascript:;">
                  Documentation<i class="fa fa-info"> </i>
                </a>
            </li> -->

         </ul>
      </nav>
   </div>
</div>

<ul class="tpl-cms-menu sp-cms-context-menu blur">
 <!--  <li class="tpl-cms-menu-item tpl-cms-menu-item-disabled">
    <button type="button" class="add-other-cms">
      <span class="tpl-cms-menu-text">Dupliquer ce bloc</span>
    </button>
  </li> -->
  <li class="tpl-cms-menu-item" style="order : 90">
    <button type="button" class="tpl-cms-menu-btn add-other-cms">
     <i class="fa fa-level-down"></i>
      <span class="tpl-cms-menu-text"><?php echo Yii::t("common", "Add a block in the page")?></span>
    </button>
  </li>
  <li class="tpl-cms-menu-item" style="order : 100" >
   <a class="tpl-cms-menu-btn" href="javascript:;"> 
     <i class="fa fa-eye"></i>
     <span class="tpl-cms-menu-text hiddenEdit"></span>
   </a>
 </li>
 <li class="tpl-cms-menu-item" style="order : 101">
  <a href="javascript:;" id="choix" class="tpl-cms-menu-btn tryOtherTpl">
    <i class="fa fa-folder-open"></i>
   <span class="tpl-cms-menu-text"> <?php echo Yii::t("common", "Choose a template")?> </span>
 </a>
</li>
<?php if ($btnSave == true ) { ?>               
  <li class="tpl-cms-menu-item" style="order : 102">
    <a href="javascript:;" class="saveThisTpls tpl-cms-menu-btn" onclick="tplObj.saveThisTpls()">    
    <i class="fa fa-download"></i>
    <span class="tpl-cms-menu-text"> <?php echo Yii::t("common", "Save as new template")?> </span>
  </a>
</li>
<?php } ?>
<?php if ($btnSaveChange == true && $idCmsMerged != "") { ?> 
  <li class="tpl-cms-menu-item" style="order : 103">
   <a href="javascript:;" class="saveChangeTpl tpl-cms-menu-btn">
   <i class="fa fa-floppy-o"></i>
    <span class="tpl-cms-menu-text"><?php echo Yii::t("common", "Save change")?></span>
 </a>
</li>
<?php } ?>
<li class="tpl-cms-menu-separator" style="order : 104"></li>
<li class="tpl-cms-menu-item" style="order : 105">
 <a id="go-to-communecter" href="" data-hash="#page.type.<?= $thisContextType ?>.id.<?= $thisContextSlug ?>" class="tpl-cms-menu-btn" target="_blank">
   <i class="fa fa-id-card-o"></i>
   <span class="tpl-cms-menu-text capitalize"><?php echo Yii::t("common", "Go to communecter")?></span>
 </a>
</li>
    <li class="tpl-cms-menu-item tpl-cms-menu-item-submenu" style="order : 105">
        <button type="button" class="tpl-cms-menu-btn">
            <i class="fa fa-cogs"></i>
            <span class="tpl-cms-menu-text"><?php echo Yii::t("common", "Site setting")?></span>
        </button>
        <ul class="tpl-cms-menu">
            <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn editadminParams"  
                data-id='<?= $this->costum["contextId"]; ?>' 
                data-collection='<?= $this->costum["contextType"]; ?>' 
                data-key='admin' 
                data-path='costum'>
                    <i class="fa fa-envelope"></i>
                    <span class="tpl-cms-menu-text">Email de l'administrateur</span>
                </button>
            </li>
            <li class="tpl-cms-menu-item tpl-cms-menu-item-submenu">
                <button type="button" class="tpl-cms-menu-btn">
                    <i class="fa fa-desktop"></i>
                    <span class="tpl-cms-menu-text">Apparence</span>
                </button>
                <ul class="tpl-cms-menu">
                    <li class="tpl-cms-menu-item">
                        <button type="button" class="tpl-cms-menu-btn editlogoIconTitleParams"  
                        data-id='<?= $this->costum["contextId"]; ?>' 
                        data-collection='<?= $this->costum["contextType"]; ?>' 
                        data-key='logoIconTitle' 
                        data-path='costum'>
                            <i class="fa fa-font"></i>
                            <span class="tpl-cms-menu-text">ICON ET TITRE</span>
                        </button>
                    </li>
                    <li class="tpl-cms-menu-item">
                        <button type="button" class="tpl-cms-menu-btn editcsscolorParams"
                        data-id='<?= $this->costum["contextId"]; ?>' 
                        data-collection='<?= $this->costum["contextType"]; ?>' 
                        data-key='css' 
                        data-path='costum.css.color'><i class="fa fa-paint-brush"></i><span class="tpl-cms-menu-text">COULEURS</span>
                        </button>
                    </li>
                    <li class="tpl-cms-menu-item">
                      <button href='javascript:;' 
                      data-id='<?= $this->costum["contextId"]; ?>' 
                      data-collection='<?= $this->costum["contextType"]; ?>' 
                      data-key='css' 
                      data-path='costum.css' type="button" class="tpl-cms-menu-btn editcssurlAndFontParams">
                            <i class="fa fa-css3"></i>
                            <span class="tpl-cms-menu-text">CSS</span>
                        </button>
                    </li>
                    <li class="tpl-cms-menu-item">
                      <button href='javascript:;' 
                      data-id='<?= $this->costum["contextId"]; ?>' 
                      data-collection='<?= $this->costum["contextType"]; ?>' 
                      data-key='css' 
                      data-path='costum.css' type="button" class="tpl-cms-menu-btn editcssloaderProgressParams">
                            <i class="fa fa-superpowers"></i>
                            <span class="tpl-cms-menu-text"> Loader et Barre de progression</span>
                        </button>
                    </li>
                </ul>
            </li>
            <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn editappallAppParams"  
                data-id='<?= $this->costum["contextId"]; ?>' 
                data-collection='<?= $this->costum["contextType"]; ?>' 
                data-key='app' 
                data-path='costum.app'>
                <i class="fa fa-bars"></i>
                    <span class="tpl-cms-menu-text"><?php echo Yii::t('common', 'App (Menu)')?></span>
                </button>
            </li>
            <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn edithtmlConstructParams"
                  data-id='<?= $this->costum["contextId"]; ?>'
                  data-collection='<?= $this->costum["contextType"]; ?>'
                  data-key='htmlConstruct'
                  data-path='costum.htmlConstruct'>
                    <i class="fa fa-html5"></i>
                    <span class="tpl-cms-menu-text">CONSTRUCTION DE MENUS</span>
                </button>
            </li>
            <li class="tpl-cms-menu-item">
              <button type="button" class="tpl-cms-menu-btn editjsParams"
              data-id='<?= $this->costum["contextId"]; ?>' 
              data-collection='<?= $this->costum["contextType"]; ?>' 
              data-key='js' 
              data-path='costum.js'>
              <i class="fa fa-file-code-o"></i>
              <span class="tpl-cms-menu-text">JS <span class="text-red">(DEVELOPER ONLY)</span></span>
              </button>
            </li> 
            <!-- <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn">
                    <i class="fa fa-check-square-o"></i>
                    <span class="tpl-cms-menu-text">FORMULAIRES</span>
                </button>
            </li> 
            <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn">
                    <i class="fa fa-map-o"></i>
                    <span class="tpl-cms-menu-text">MAP</span>
                </button>
            </li> -->
            <li class="tpl-cms-menu-item">
              <button type="button" class="tpl-cms-menu-btn editadminPanelParams"
                data-id='<?= $this->costum["contextId"]; ?>' 
                data-collection='<?= $this->costum["contextType"]; ?>' 
                data-path='costum.htmlConstruct'>
                    <i class="fa fa-user-secret"></i>
                    <span class="tpl-cms-menu-text">PORTAIL D'ADMINISTRATION (DEVELOPER ONLY)</span>
              </button>
            </li>
<!-- 
            <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn">
                    <i class="fa fa-columns"></i>
                    <span class="tpl-cms-menu-text">PAGE ELEMENT</span>
                </button>
            </li> -->
         <!--    <li class="tpl-cms-menu-item">
                <button type="button" class="tpl-cms-menu-btn">
                    <i class="fa fa-user-secret"></i>
                    <span class="tpl-cms-menu-text">PORTAIL D'ADMINISTRATION (DEVELOPER ONLY)</span>
                </button>
            </li> -->
        </ul>
    </li>
</ul>



<div class="row panel-display-cms hiddenPreview">
  <div id="mySidenav" class="sidenav">
    <div class="" ></div>
  </div>

  <div id="sp-main-cms" onmouseover="showCms()" onmouseleave="minimizeCmsMenu()" style="height:100%;background-color: #ffffffdb;"> 
      <div style="height: 70px;display: flex;justify-content: space-around;padding-left: 24px;background-color: #ffffff85;">
        <div class="options-area" style="width: 10px;padding: 10px;">
          <a href="javascript:void(0)" class="close-list-cms">&times;</a>
        </div>
        <div class="searchBar-filters" style="width: 75%;margin-top: 10px;">
          <input onkeyup="tplObj.searchCms()" type="text" class="form-control text-center main-search-bar search-bar searchInput" data-field="text" placeholder="Que recherchez-vous ?">
          <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text">
            <i class="fa fa-arrow-circle-right"></i>
          </span>
        </div>
        <div class="options-area" style="width: 5%;">
          <a href="javascript:;" class="createCMS btn btn-success" title="Créer un bloc"><i class="fa fa-plus padding-right-10"></i></a>
        </div>
      </div>
      <div class="floop" style="height:94%;background-color: #7b7b7b12;text-shadow: 0px 0px 1px #000f10;">
        <nav class="tpl-engine-right">
          <ul id="list-cms-disponible"></ul>
        </nav>
      </div>
    </div>
</div>

<!-- <div class="row panel-display-cms">
  <div class="col-md-10">
    <div id="myModal" class="modal">
      <div class="modal-content content-display-cms">
        <div class="modal-body"id="display-cms"></div>
        <div class="modal-footer">
          <a href="#" class="btn btn-primary"><?php echo Yii::t("cms", "Use this bloc")?></a>
          <a href="#" class="btn btn-primary editParams"><?php echo Yii::t("common", "Settings")?></a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <nav class="tpl-engine-right blur hiddenPreview">
     <ul id="list-cms-disponible"></ul>
   </nav>
 </div>
</div> -->
<?php } ?>

<div class="row" id="all-block-container" style="margin-right: 0;margin-left:0">
    <?php
    $param=[
      "page"       => $page,
      "canEdit"    => true,
      "cmsList"    => $cmsList,
      "me"         => $me,
      "paramsData" => $paramsData,
      "el"         => $el,
      'costum'     => $this->costum
    ]; 
    echo $this->renderPartial("costum.views.tpls.blockCms.cmsEngine",$param); 
    ?>
</div>

<?php 
  $categoryUnique = array();
  $sumCat = 0;
  foreach ($listTemplate as $key => $value) { 
    $categoryUnique[] = isset($value["category"]) ? $value["category"]: "Autre" ;
    $sumCat += count($key);
  }
  $numberCat = array_count_values($categoryUnique);
?>

<!-- menu json -->
<?php if(Authorisation::isInterfaceAdmin() && isset($this->costum["contextId"]) && !empty($this->costum["contextId"])){  
  echo $this->renderPartial("costum.views.tpls.jsonEditor.menuJson",array("canEdit" => $canEdit)); 
}
?>
<?php // if(!empty($me["slug"]) && $me["slug"]=="ramiandrison"){ 
      // echo $this->renderPartial("costum.views.tpls.jsonEditor.newErgo.menuAndApp",array("canEdit" => $canEdit)); 
// } ?>
<!-- Documentation -->
<?php // echo $this->renderPartial("costum.views.tpls.blockDocumentation",array("canEdit" => $canEdit)); ?>

<?php 
    $descriptionTpl = isset($insideTplInUse["description"]) ? $insideTplInUse["description"] : "";
    $nameTpl = isset($insideTplInUse["name"]) ? $insideTplInUse["name"] : "";
    $categoryTpl = isset($insideTplInUse["category"]) ? $insideTplInUse["category"] : "";
?>

<script type="text/javascript"> 
  tplUsingId = "<?php echo $tplUsingId; ?>";
  var mode = localStorage.getItem("previewMode");
  var divSelection;
  var editable = null;
  var current_full_Url = window.location.href;
  var sampleTemplate = <?= json_encode($sampleTemplate); ?>;
  var listCmsCanBeUse = <?= json_encode($listCmsCanBeUse); ?>;
  var callByName = {};
  let defaultCtxMenu = $(".sp-cms-context-menu").html();
  let colorTheme = "#1d3b58";
  let bgTheme = $('#mainNav').css('background-color');
  if (bgTheme == 'rgb(255, 255, 255)') 
    bgTheme = '#bad5f5';
  if (typeof contextData.costum.css !== "undefined" && typeof contextData.costum.css.menuTop !== "undefined" && typeof contextData.costum.css.menuTop.app !== "undefined" && typeof contextData.costum.css.menuTop.app.button !== "undefined")
    colorTheme = contextData.costum.css.menuTop.app.button.color;

  $(document).on("cms-edit-mode", function() {
    // alert("edit")
    $(".mytext").css("cursor", "text")
  })

  $(document).on("cms-view-mode", function() {
   convAndCheckLink(".sp-text");
    $(".mytext").css("cursor", "default")
  })


  $(document).on("keypress" ,".avoid-new-line",function(e){ return e.which != 13; });

function minimizeCmsMenu() {
  $(".panel-display-cms").css("width","10%")
  $(".panel-display-cms").css("z-index","99999")
  document.getElementById("mySidenav").style.width = "95%";
  document.getElementById("sp-main-cms").style.marginLeft = "50%";
}

function showCms() {
  $(".panel-display-cms").show();
  $(".panel-display-cms").css("width","420px")
  $(".panel-display-cms").css("z-index","100000")

  $("#list-cms-disponible li").on("mouseenter",function(){
    $(this).css("background-color", bgTheme);    
    $(this).find("div").css("color", colorTheme);
    $(this).find("i").css("color", colorTheme);

  })
  $("#list-cms-disponible li").on("mouseleave",function(){
    $(this).css("background-color","" );    
    $(this).find("div").css("color", "");
    $(this).find("i").css("color", "");
  })
  $("#sp-main-cms").css("background-color",colorTheme)
  $("#list-cms-disponible li").children().css("color", bgTheme);

  document.getElementById("mySidenav").style.width = "100%";
  document.getElementById("sp-main-cms").style.marginLeft= "5%";
}

$(document).on("click",".add-cms-here",function(event){
  localStorage.removeItem("parentCmsIdForChild");    
  showCms()
  $(".sample-cms").remove();
  $(".add-cms-here").parent().parent().hide();
  $(".cms-area-selected").removeClass("cms-area-selected");
  $(".add-cms-here").siblings(".selected").text("");
  $(".add-cms-here").siblings(".cms-sample-options").hide();
  // $(".add-cms-here").show();

  $(".panel-display-cms").css("width","420px");
  document.getElementById("mySidenav").style.width = "100%";
  document.getElementById("sp-main-cms").style.marginLeft= "5%";
  $(this).siblings(".selected").addClass("cms-area-selected");
  $(this).siblings(".selected").text("Veuillez selectionner un bloc sur le menu à gauche");
  $(this).siblings(".cms-sample-options").show();
  // $(this).hide();
 
  if (typeof $(this).data("desire") !== "undefined" || $(this).data("desire") == "below") {
     $(`<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">
      <div class="selected cms-area-selected blink-info">Veuillez selectionner un bloc sur le menu à gauche</div>
      </div>`).insertAfter("#"+$(this).data("id"));
  }else{
     $(`<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">
      <div class="selected cms-area-selected blink-info">Veuillez selectionner un bloc sur le menu à gauche</div>
      </div>`).insertBefore("#"+$(this).data("id"));
  }
  $([document.documentElement, document.body]).animate({
    scrollTop: $(".other-cms").offset().top - 150
  }, 500);
  event.stopImmediatePropagation()
})

$(".close-list-cms").on("click",function(){
  $(".add-cms-here").parent().parent().show();
  $(".panel-display-cms").hide();
  if($(".sample-cms").text().length > 0){
    $(".sample-cms").remove();
  }
});

 function addApptoCamelCase (str) {
  return str.normalize("NFD").replace(/([\u0300-\u036f])/g, "").replace(/["']/g, "").replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        //return index == 0 ? match.toLowerCase() : match.toUpperCase();
        return index = match.toLowerCase() ;
      });
}


/********************Menu********************/
<?php if(Authorisation::isInterfaceAdmin()){ ?>
  let menuMechanics = {
    applyToMenuTop : function (hashName) {
       let tplCtxMenu = {};
       tplCtxMenu.id ='<?= $this->costum["contextId"]; ?>';
       tplCtxMenu.collection = '<?= $this->costum["contextType"]; ?>';
       tplCtxMenu.path = 'costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.#'+hashName;
       tplCtxMenu.value = true;
       tplCtxMenu.removeCache=true;
       tplCtxMenu.format=true;
       dataHelper.path2Value( tplCtxMenu, function(params) {
         location.reload();
       }) 
     },
  }

  
$('html').on('mousedown', function(e) {
  if (typeof $(e.target).data('original-title') == 'undefined' &&
     !$(e.target).parents().is('.popover.in')) {
    $('[data-original-title]').popover('hide');
  }
});

sectionDyf.appallAppText = '<input type="text" class="form-control" id="appName" name="appName" placeholder="<?php echo Yii::t("cms", "menu name")?>">';
sectionDyf.appallAppSelect = 
    '<select class="form-control" id="appName" name="appName" placeholder="nom du menu">'+
        '<option value="search"><?php echo Yii::t("common", "Search")?></option>'+
        '<option value="live"><?php echo Yii::t("common", "Live")?></option>'+
        '<option value="projects"><?php echo Yii::t("common", "Projects")?></option>'+
        '<option value="annonce"><?php echo Yii::t("common", "Annonces")?></option>'+
        '<option value="agenda"><?php echo Yii::t("common", "Agenda")?></option>'+
        '<option value="dda"><?php echo Yii::t("common", "Sondage")?></option>'+
        '<option value="map"><?php echo Yii::t("common", "Map")?></option>'+
        '<option value="annuaire"><?php echo Yii::t("common", "Annuaire")?></option>'+
    '</select>';
    if ($(".newPage").length === 0) {

      $("#menuTopLeft").append(`<a href="#" class="text-dark hidden-xs line-height-3 padding-left-10 newPage hiddenPreview" data-toggle="tooltip" data-placement="bottom"><span class="label-menu"><span class="name-real-time padding-right-20" style="font-weight: 800;font-size: 20px "></span><i class="fa fa-plus"></i></span></a>`);
      $('.newPage').click(function() {
        $(".name-real-time").text("");
        setTimeout(function(){  
         $('#appName').keyup(function(){
          $(".name-real-time").text($(this).val());
        })
       },100)
      })


      $('.newPage').popover({title: "", content: `
        <div>
        <form id="appForm" class="form" role="form">
        <div>
        <label for="appName"><?php echo Yii::t("cms", "New menu")?></label>
        </div>
        <div class="form-group">
        <label class="radio-inline"><input type="radio" name="appType" value="staticPage" checked><?php echo Yii::t("cms", "Static page")?></label>
        <label class="radio-inline"><input type="radio" name="appType" value="app"><?php echo Yii::t("cms", "App")?></label>
        </div>
        <div class="form-group">
        `+sectionDyf.appallAppText+`
        </div>
        </form>
        <div class="col-md-12 wrapped-child col-md-offset- col-xs-12">
          <label class="col-xs-12 text-left control-label no-padding" for="test-restricted-admins">
           Afficher directement
          <span class="lbl-status-check margin-left-10">
            <span class="letter-green"><i class="fa fa-check-circle"></i> Oui</span>
          </span>
        </label>
          <input type="hidden" class="" name="test-visible" id="sp-visible-menu" value="true"> 
          <div class="col-lg-6 padding-5">
            <a href="javascript:" class="sp-show-hide-menu btn btn-sm col-xs-12" style="border: 4px solid rgba(255, 255, 255, 0.5);font-weight: 600;font-size: inherit;background-color: #59d961;color:#fff ;" data-checkval="true">Oui</a>
          </div>
          <div class="col-lg-6 padding-5">
            <a href="javascript:" class="sp-show-hide-menu btn btn-sm col-xs-12" style="border: 4px solid rgba(255, 255, 255, 0.5);font-weight: 600;font-size: inherit;background-color: #fff; color:#ea4335 ;" data-checkval="false">Non</a>
          </div>
        </div>
        <div class="text-center">
        <button id="submit-new-menu" class="btn btn-default text-bold letter-green" style="width:60%">Valider <i class="fa fa-arrow-circle-right"></i></button>
        </div>
        </div>
        `, html: true, placement: "bottom"}); 
    $(document).on("click", ".sp-show-hide-menu", function () {
        let $this = $(this);
        if ($this.data("checkval") == true) {
          $(".sp-show-hide-menu").css("background-color","#fff")
          $(".sp-show-hide-menu").css("color","#ea4335")
          setTimeout(function(){
            $this.css("background-color","#59d961")
            $this.css("color","#fff")
            $(".lbl-status-check").html(`<span class="letter-green"><i class="fa fa-check-circle"></i> Oui</span>`)
            $("#sp-visible-menu").val("true")
            $(".name-real-time").show()
          },10)
        }else if ($this.data("checkval") == false){      
          $(".sp-show-hide-menu").css("background-color","#fff")
          $(".sp-show-hide-menu").css("color","#59d961")
          setTimeout(function(){
           $this.css("background-color","#ea4335")
           $this.css("color","#fff")
           $(".lbl-status-check").html(`<span class="letter-red"><i class="fa fa-minus-circle"></i> Non</span>`)
           $("#sp-visible-menu").val("false")
           $(".name-real-time").hide()
          },10)
        }
      })
      $(document).on("click",'input[name=appType]',function(){
        if ($("#sp-visible-menu").val() == "false") {
          $(".name-real-time").hide()
        } else {
          $(".name-real-time").show()
        }
        if( $(this).val() == "staticPage"){
          $("#appName").replaceWith(sectionDyf.appallAppText);
          $(".name-real-time").text("");
          $('#appName').keyup(function(){
            $(".name-real-time").text($(this).val());
            if($(this).val()!="")
              $('.bootbox-accept').removeAttr("disabled");
            else if($(this).val() =="")
              $('.bootbox-accept').prop("disabled",true);
          })
        }
        else if($(this).val() == "app"){
          $("#appName").replaceWith(sectionDyf.appallAppSelect);
          $(".name-real-time").text("<?php echo Yii::t("common", "Search")?>");
          $('#appName').change(function(){
            $(".name-real-time").text($('#appName option:selected').text());
          })
          $('.bootbox-accept').removeAttr("disabled");

        }
      })
      $(document).on('click', '#submit-new-menu', function(){
        let $this = $("#appName");
        let appName = $this.val();
        let subdomainName = appName;
        let tplCtx = {};
        let tplCtxMenu = {};
        let hashName = addApptoCamelCase(appName);
        let required = {
         "useFilter" : true,
         "hash" : "#app.search",
         "urlExtra" : "/page/"+hashName,
         "subdomainName" : subdomainName,
         "results" : {
          "renderView" : "directory.elementPanelHtml"
        },
        "searchObject" : {
          "indexStep" : "20",
          "sortBy" : ""
        },
        "filters" : {
          "types" : [ 
          "organizations", 
          "classifieds", 
          "poi", 
          "projects", 
          "events"
          ]
          
        }
      }

      tplCtx.id ='<?= $this->costum["contextId"]; ?>';
      tplCtx.collection = '<?= $this->costum["contextType"]; ?>';
      tplCtx.path = 'costum.app.#'+hashName;
      tplCtx.updatePartial=true;
      tplCtx.format=true;
      if ($('input[name=appType]:checked').val() == "staticPage") {
        if (appName == "") {
          $this.css({"border-color": "#e91e63", 
           "border-width":"1px", 
           "border-style":"solid"})
        }else{
          tplCtx.value = {
            isTemplate : true,
            staticPage : true,
            useFilter : false,
            hash: "#app.view",
            icon : "",
            urlExtra : "/page/"+hashName+"/url/costum.views.tpls.staticPage",
            subdomainName : subdomainName

          }
          dataHelper.path2Value( tplCtx, function(params) { 
            if ($("#sp-visible-menu").val() == "true") {        
              menuMechanics.applyToMenuTop(hashName)
            }else{
               location.reload();
            } 
          }); 
        }
      }else if($('input[name=appType]:checked').val() == "app"){
         tplCtx.value = {};
         if (appName == "live") {          
           tplCtx.value["useFilter"] = true;
           tplCtx.value["subdomainName"] = subdomainName;
         }else if (appName == "agenda") {
          tplCtx.value["useFilter"] = true;
          tplCtx.value["hash"] = "#app.agenda";
          tplCtx.value["urlExtra"] = "/page/agenda";
        }else if (appName == "dda"){
          tplCtx.value["showInviteBtn"] = true;
          tplCtx.value["showAmendmentBlock"] = true;
          tplCtx.value["showExternLinkBlock"] = true;
        }else if (appName == "projects"){
          required.filters.types = ["projects"];
          tplCtx.value = required;
        }else if (appName == "annonce"){
          required.filters.types = ["classifieds"];
          tplCtx.value = required;
        }else{          
          tplCtx.value = required;
        }

        dataHelper.path2Value( tplCtx, function(params) {          
         if ($("#sp-visible-menu").val() == "true") {        
          menuMechanics.applyToMenuTop(hashName)
        }else{
         location.reload();
       } 
      });
      }
    })
    }
    $('#mainNav').off().on('contextmenu', function(e) {
      let $this = $(this);
      let menuEditor = [
      {
        class : "editappallAppParams",
        label : "<?php echo Yii::t("common", "Edit menu bar")?>",
        icon : "bars",
        data : {
          id : contextId,
          key : "app",
          collection : contextData.collection,
          path : "costum.app"
        }
      },
      {
        class : "editcssmenuTopParams",
        label : "<?php echo Yii::t("common", "Menu appearance")?>",
        icon : "paint-brush",
        data : {
          id : contextId,
          key : "css",
          collection : contextData.collection,
          path : "costum.css.menuTop"
        }
      }
      ] 
      cmsCtxMenu.add(menuEditor);
      e.preventDefault()
    });
    $('#mainNav .label-menu').off().on('contextmenu', function(e) {
      let $this = $(this);
      if (!$this.children("span").hasClass("name-real-time")) {
        let jsonListePage = {}
        $.each(pageListe , function(k,hash) {
          let $itsPath = "costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList"
          let $itsChecked = false;
          let $thisIcon = "";
          if (typeof costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList[k] !== "undefined") {
            $thisIcon = "check";
            $itsPath = costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList[k];
            $itsChecked = true;
          }
            jsonListePage[k] = {
             class : "ctx-page",
             label : hash.subdomainName ? hash.subdomainName : k,
             icon : $thisIcon,
             data : {
              id : contextId,
              collection : '<?= $this->costum["contextType"]; ?>',
              path: $itsPath,
              label : hash.subdomainName ? hash.subdomainName : k,
              page : k,
              show : $itsChecked
            }
          }
          })
        let textMenuEditor = [
        {
          class : "editText",
          label : "<?php echo Yii::t("common", "Edit this text")?>",
          icon : "i-cursor",
          data : {
            id : contextId,
            key : "app",
            collection : contextData.collection,
            path : "costum.app"
          }
        },
        {
          class : "",
          label : "<?php echo Yii::t("common", "Menus displayed")?>",
          icon : "eye-slash",
          submenu : {
          }
        },
        {
          class : "change-icon",
          label : "<?php echo Yii::t("common", "Change icon")?>",
          icon : "smile-o",
          data : {
            id : contextId,
            key : "app",
            collection : contextData.collection,
            path : "costum.app"
          }
        }
        ] 
        textMenuEditor[1].submenu = jsonListePage;
        // mylog.log("menuShow", jsonListePage)
        cmsCtxMenu.add(textMenuEditor); 
        setTimeout(function(){  
          $(".editText").on("click", function() {
            $this.parent().addClass("editing")
            $this.attr('contenteditable','true');
            $this.focus();
            $this.keypress(function(e){return e.which != 13; });
            spDefaultText = $this.text()
          })
          $(".change-icon").on("click", function() {
            let iconHtml = ""
            let displayHtml = ""
            let defaultIcon = $this.parent().children("i").attr("class");
            let iconState = ""             
            if (typeof defaultIcon !== "undefined") 
              defaultIcon = `<i class='`+defaultIcon+`' style='font-size:80px;color:`+colorTheme+`'></i><br><br><button class='btn sp-rem-icon'>Effacer</button>`
            else
              defaultIcon = "<br>Choisir une icône"
            $.each( cmsBuilder.iconList , function(k,val) { 
              iconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='"+val+"'><i class='fa "+val+"'></i></a>"
            });
            displayHtml = "<div class='col-md-12 padding-bottom-10 bg-white'><div class='col-md-5 sp-display-icon text-center' style='padding-top: 9%;height:190px;border: solid 1px #cccccc; left:1px;background-color:"+bgTheme+";'>"+defaultIcon+"</div><div class='col-md-7 no-padding' style='height:190px;background-color: #fff;border: solid 1px #cccccc;'><input type='text' style='border-radius: inherit;' class='form-control' id='iconSearcher' onkeyup='tplObj.iconSearcher()' placeholder='Taper pour filtrer'><div id='sp-icon-list' style='position: relative;clear: both;float: none;padding: 12px 0 0 12px;background: #fff;margin: 0;overflow: hidden;overflow-y: auto;max-height: 154px;display: flex;flex-wrap: wrap;'>"+iconHtml+"</div></div></div>"
             bootbox.confirm({  
              title : "Changement de l'icône de la page "+$this.text(),
              message : displayHtml,
              callback: function (result) {
                let tplCtx = {
                 id :'<?= $this->costum["contextId"]; ?>',
                 collection : '<?= $this->costum["contextType"]; ?>',
                 path : 'costum.app.'+$this.parent().attr('href')+'.icon',
                 removeCache : true,
                 format : true
               }
               if (result) {
                 if (iconState == "use"){                
                   tplCtx.value = $(".sp-display-icon i").data("icon").substring(3);
                   dataHelper.path2Value( tplCtx, function(params) {
                     location.reload();
                   })
                 }else if (iconState == "deleted"){  
                   dataHelper.unsetPath( tplCtx, function(params) {
                     location.reload();
                   })
                 }
               }
             }
             });
             $(document).on("click", ".this-icon", function() {
              $(".sp-display-icon").html("<i class='fa "+$(this).data("icon")+"' data-icon='"+$(this).data("icon")+"' style='font-size:80px;color:"+colorTheme+"'></i>")
              iconState = "use"
            })             
             $(document).on("click", ".sp-rem-icon", function() {
              $(".sp-display-icon").html("<br>Choisir une icône")
              iconState = "deleted"
            })
             
          })
          $(".ctx-page").on("click", function() {
            let tplCtx = {
             id :'<?= $this->costum["contextId"]; ?>',
             collection : '<?= $this->costum["contextType"]; ?>',
             path : 'costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.'+$(this).data("page"),
             removeCache : true,
             format : true
            }
            if ($(this).data("show") == true) {
              // alert("true")
              tplCtx.path = 'costum.htmlConstruct.header.menuTop.left.buttonList.app.buttonList.'+$(this).data("page")
              dataHelper.unsetPath( tplCtx, function(params) { 
                location.reload();
              } );
            }else{
              tplCtx.value = true;
              dataHelper.path2Value( tplCtx, function(params) {
               location.reload();
             }) 
            }
          })
        },11)
        e.preventDefault()
      }
    });

    $(document).on("blur","#mainNav .editing span",function () {
      $(this).parent().removeClass("editing")
      $(this).attr("contenteditable", false)
      // $(this).parent().attr('href')
      // alert($(this).parent().attr('href'))
      if (spDefaultText !== $(this).text()) {
        let tplCtx = {
          id :'<?= $this->costum["contextId"]; ?>',
          collection : '<?= $this->costum["contextType"]; ?>',
          path : 'costum.app.'+$(this).parent().attr('href')+'.subdomainName',
          removeCache:true,
          value : $(this).text()
        }
        dataHelper.path2Value( tplCtx, function(params) {    
          location.reload();
        });
      }
    })
  <?php } ?>
/********************End menu********************/

/**************************modal form add***********************/
  sectionDyf.apptext = '<input type="text" class="form-control" id="appName" name="appName" placeholder="<?php echo Yii::t("cms", "menu name")?>">';
  sectionDyf.appAddForm =  
        '<form id="appForm" class="form" role="form">'+
            '<div class="form-group">'+
              '<label for="appName"><?php echo Yii::t("cms", "New menu")?></label>'+
              sectionDyf.apptext+
            '</div>'+
            '<div class="form-group">'+
                '<label class="radio-inline"><input type="radio" name="appType" value="staticPage" checked><?php echo Yii::t("cms", "Static page")?></label>'+
                '<label class="radio-inline"><input type="radio" name="appType" value="app"><?php echo Yii::t("cms", "App")?></label>'+
            '</div>'+
        '</form>';


/**************************end modal form add***********************/

  $.each( listCmsCanBeUse , function(k,val) { 
    let path = val.path;    
    callByName[path.replace(/./g, '')] = val.name;
   $("#list-cms-disponible").append(`
    <li class="cms-list padding-top-5 padding-bottom-5 black-default-cms ${val.path.substring(14).replace(".","")}">${val.profilImageUrl ? '<a href="'+val.profilImageUrl+'" class="btn-link thumb-info display" data-title="Aperçu" data-lightbox="all" title="${val.description}"><img class="fit-picture" style="height: 50px;width: 79px;border: solid 2px #bdbdbd;"src='+val.profilImageUrl+'></a>' : '<div class="sp-div-img"><i class="fa fa-image fa-3x" style="padding-left: 17px;padding-right: 19px;"></i></div>'}<a class="cms-disponible padding-left-10" data-path="${val.path}" data-id="${k}" href="javascript:;"><div style="width: 215px; overflow: hidden;text-overflow: ellipsis;
">${val.name}</div>
     </a><button class="btn editDescBlock" data-id="${k}" style="white-space: nowrap;background-color: #ffffff2e;color:white;border-radius:0px"><i class="fa fa-edit"></i></button>
     <button class="btn deleteCms" data-id="${k}" data-name="${val.name}" style="white-space: nowrap;background-color: #ffffff2e;color:white;border-radius:0px"><i class="fa fa-trash"></i></button>
     </li>`)
 });
  $(".cms-disponible").click(function(){
    loadingMsg = "Chargement en cours...<br><br>"
    coInterface.showLoader(".cms-area-selected" , loadingMsg)
    let idblock = $(this).data("id");
    let path = $(this).data("path");
    canUse = true;

   $('.cms-sample-options').remove();
   $(".cms-area-selected").parent().append(`
      <div class="cms-sample-options col-md-12">
      Affichage de la cms selectionné<br>
        <a class="btn btn-primary use-this" data-path="${path}" data-collection="cms"><?php echo Yii::t("common", "Use")?></a>
        <a class="btn btn-primary cancel-selection"><?php echo Yii::t("common", "Cancel")?></a>
      </div>`)
   if ($(".other-cms").length !== 0) {
     $([document.documentElement, document.body]).animate({
      scrollTop: $(".other-cms").offset().top - 150
    }, 1000);
  }    

   var params = {
    "idblock" : idblock ,
    "page" : page,
    "contextId" : contextId,
    "contextSlug" : thisContextSlug,
    "contextType" : contextType,
    "path" : path,
    "clienturi" : location.href
  };
  ajaxPost(
    null, 
    baseUrl+"/costum/blockcms/loadbloccms",
    params,
      function(data){
        $(".cms-area-selected").append(data.html)
        // $(document).trigger("sp-loaded")
        // setTimeout(function () {
          $(".sp-text").show()
          $(".processingLoader").remove();
        // },100)
      } 
    );

})

//   $(document).on("click",".use-this", function() {
//   alert("yes")
// })

   var tplMenu = `
  <?php if(Authorisation::isInterfaceAdmin()){  ?>
  <div id="listeTemplate">
    <div id="tplFiltered">
  </div>
     <div class="row">
      <div class="col-lg-12" style="border-bottom: 1px solid rgba(100,100,100,0.1);text-align: center;">
         <div class="col-xs-12 padding-20" style="z-index: 1">
            <div style="width: 100%">
              <h3 class="bg-azure" style="padding: 20px; width: 50%; border-radius: 50px; margin : auto; "><?php echo Yii::t("cms", "Choose a template")?></h3>
            </div>
         </div>
      </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="text-left">
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-filter" data-target="all"><?php echo Yii::t("cms", "All")?><span class="filter-badge"><?= $sumCat ?></span></button>
                <?php  foreach (array_unique($categoryUnique) as $tpl_filtered) { ?>
                <button type="button" class="btn btn-default btn-filter" data-target="<?= preg_replace("/[^a-zA-Z]+/", "",$tpl_filtered) ?>"><?php echo Yii::t("cms", $tpl_filtered)?>
                <span class="filter-badge"><?= $numberCat[$tpl_filtered] ?></span>
              </button>
               <?php } ?>               
                <button type="button" class="btn btn-default btn-filter btnRctlyTpl" data-target="backup" onclick="tplObj.backup()"><?php echo Yii::t("cms", "Old template")?></button>
              </div>
            </div>
            <div class="">
              <div class="table table-filter">
                <div class="text-left">
                  <?php foreach ($listTemplate as $kTpl => $value) { 
                    $valCategory = isset($value["category"]) ? $value["category"]: "Autre" ;
                    $listCms = isset($value["cmsList"])?$value["cmsList"]:"";
                    $img = isset($value["img"])?$value["img"]:"";                     
                    $description = isset($value['description'])?$value['description']:'';
                    $name = isset($value['name'])? $value['name']:'';
                    $type = isset($value["type"])? $value["type"]:"";
                    $profil = isset($value["profilImageUrl"]) ?$value["profilImageUrl"]:"";
                    $currentTplsUser = isset($value["tplsUser"]) ?$value["tplsUser"]:[];
                    $counts = array();
                    foreach ($currentTplsUser as $key=>$subarr) {
                      $counts[] = $subarr;
                    }          
                    $countUsing = 0;
                    $countBackup = 0;

                    forEach($counts as $k => $v){
                      $arrayVal = array_values($v);
                      forEach($arrayVal as $k => $v){
                        if($v =="using") $countUsing ++;
                        if($v =="backup") $countBackup ++;
                      }

                    }
                    $tplKeys = $kTpl;
                    $initImage = Document::getListDocumentsWhere(array("id"=> $tplKeys, "type"=>'cms'), "file");
                    $arrayImg = [];
                    foreach ($initImage as $k => $v) {
                      $arrayImg[]= $v["docPath"];
                    }

                     ?>
                  <div class="tplItems" data-status="<?= preg_replace("/[^a-zA-Z]+/", "", $valCategory) ?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">          
                    <?php if($tplUsingId == $value["_id"]){ ?>      
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px #5dd55d, 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }elseif($listCms == ""){?>  
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px rgb(198 43 43), 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }else{ ?>                     
                       <div class="item-slide">
                    <?php } ?> 
                          <div class="entityCenter">
                             <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                             </a>
                          </div>
                          <div class="img-back-card">
                             <div class="div-img">
                                <img src="<?= $profil?>">
                             </div>
                             <div class="text-wrap searchEntity">
                                <div class="entityRight profil no-padding">
                                   <a href="#" class="entityName letter-orange">
                                     <font >
                                     <?= $name ?>                  
                                     </font>
                                   </a>    
                                </div>
                             </div>
                          </div>
                          <!-- hover -->
                          <div class="slide-hover co-scroll">
                             <div class="text-wrap">
                                   <div class="entityPrice">
                                         <font class="letter-orange" ><?= $name ?></font>
                                   </div>
                                   <div class="entityType col-xs-12 no-padding">
                                      <p class="p-short">
                                         <font >
                                         <?= $description ?>                    
                                         </font>
                                      </p>
                                   </div>
                                   <hr>
                                   <ul class="tag-list">
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                      <i class="fa fa-user"></i> <?= $countUsing ?> <?php echo Yii::t("cms", "Users")?>
                                      </font>
                                      </span>
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                          <i class="fa fa-eye"></i> <?= $countUsing+$countBackup ?> <?php echo Yii::t("cms", "Seen")?>
                                      </font>
                                      </span>
                                   </ul>
                                <hr>
                                <div class="desc">
                                   <div class="socialEntityBtnActions btn-link-content">                                   
                                      <a href="<?= $profil ?>" class="btn btn-info btn-link thumb-info" data-title="Aperçu de <?= $name ?>" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye"></i><?php echo Yii::t("cms", "Preview")?></font>
                                      </a>
                                       <?php
                                        if ($me['_id'] == $value['creator'] || @$me["roles"]["superAdmin"]){ ?>
                                        <a href="javascript:;" class="btn btn-info btn-link editDesc" data-id="<?= $value["_id"]?>" data-category="<?= @$value["category"]?>" data-name='<?= $name ?>' data-desc='<?php echo $description ?>' data-img='<?= json_encode($initImage) ?>'>
                                          <font > <?php echo Yii::t("cms", "Edit")?></font>
                                        </a> 
                                      <?php } ?>   
                                      <?php if($tplUsingId == $value["_id"]){ ?>      
                                        <p class="text-green bold"><?php echo Yii::t("cms", "Your current template")?></p>
                                      <?php }elseif($listCms == ""){?>  
                                        <p class="text-red bold"><?php echo Yii::t("cms", "Not available")?></p>
                                      <?php }else{ ?>                             
                                      <a href="javascript:;" class="btn btn-info btn-link" onclick='tplObj.chooseConfirm(<?php echo json_encode($listCms)?>,"<?php echo $value["_id"] ?>");'>
                                        <font ><?php echo Yii::t("cms", "Choose")?></font>
                                      </a> 
                                      <?php } ?>    
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                  </div>
                  <?php }?>              
                </div>
              </div>
              <div id="backupTplsContainer"></div> 
            </div>
          </div>
        </div>
     </div>
  </div> 
  <?php } ?>`;
  var tplHtmlCtxMenu = $(".sp-cms-context-menu").html();
  var cmsCtxMenu = {
    menulists : function(list,name) {
      let menuHtml = "";
      let listSeparator = "";
        // mylog.log("sub length",Object.getPrototypeOf(list) === Object.prototype)
        $.each(list, function(ks,val) {
          if (typeof val.submenu == 'undefined') {

            let listData = '';
            if (typeof val.data !== "undefined") {
              $.each(val.data, function(k,val) {
               listData += 'data-'+k+' = "'+val+'"';
             })
            }
            if (previewMode == false || typeof val.preview == "undefined") {
              menuHtml += `
              <li class="tpl-cms-menu-item">
              <button type="button" ${val.id ? "id ='"+val.id+"'" : ""} class="tpl-cms-menu-btn ${val.class ? val.class : ''}" ${listData ? listData : ''}>
              ${val.icon ? '<i class="fa fa-'+val.icon+'"></i>' : ''}        
              <span class="tpl-cms-menu-text">${val.label ? val.label : ''}</span>
              </button>
              </li>
              `         
             listSeparator = '<li class="tpl-cms-menu-separator"></li>';
            }      
          }
        })
        return menuHtml+listSeparator;
      },

      add : function(menu) {
        let html = "";
        let separator = "";
        let visible = "";
        $(".sp-cms-context-menu").html(tplHtmlCtxMenu);
        html += cmsCtxMenu.menulists(menu)
        $.each(menu, function(ks,val) {
          if (typeof val.submenu !== 'undefined') {
            if (previewMode == false || typeof val.preview == "undefined") {
             html += `
             <li class="tpl-cms-menu-item tpl-cms-menu-item-submenu ${previewMode ? val.preview : ""}">
             <button type="button" ${val.id ? "id ='"+val.id+"'" : ""} class="tpl-cms-menu-btn ${val.class ? val.class : ''}">
             ${val.icon ? '<i class="fa fa-'+val.icon+'"></i>' : ''}        
             <span class="tpl-cms-menu-text">${val.label ? val.label : ''}</span>
             </button>
             <ul class="tpl-cms-menu">
             ${cmsCtxMenu.menulists(val.submenu)}
             </ul>
             </li>
             `
             separator = '<li class="tpl-cms-menu-separator"></li>';
           }
         }
       }) 
        setTimeout(function(){
          $(".sp-cms-context-menu").append(html+separator);            
        },10)
      }
    }

    var tplObj = {

      iconSearcher : function () {
        let input, filter, i, ti;
        input = $('#iconSearcher');
        filter = input.val().toUpperCase();
        ti = Object.keys(cmsBuilder.iconList).length;
        for (i = 0; i < ti; i++) {
         paragraph = cmsBuilder.iconList[i]
         paragraph = paragraph.replace("fa-","")         
         paragraph = paragraph.split('-').join(' ');
         paragraph = paragraph.toUpperCase();
         if (paragraph.includes(filter)) {
          $(".this-icon ."+cmsBuilder.iconList[i]).parent().show()
        } else {
          $(".this-icon ."+cmsBuilder.iconList[i]).parent().hide()
        }
      }
    },searchCms : function () {
      let input, filter, liste, item, i, cmsLen;
      input = $('.searchInput');
      filter = input.val().toUpperCase();
      liste = $("#list-cms-disponible");
      item = $('#list-cms-disponible .cms-list');
      cmsLen = Object.keys(listCmsCanBeUse).length;
      $.each(listCmsCanBeUse,function(key,value){  
        paragraph = value.name+" "+value.path.substring(14);
        paragraph = paragraph.toUpperCase()
        // mylog.log("cms Dis","."+value.path.substring(14).replace(".",""))
        if (paragraph.includes(filter)) {
          $("."+value.path.substring(14).replace(".","")).css("display","");
        } else {
          $("."+value.path.substring(14).replace(".","")).css("display","none");
        }
      })
  },
    backup : function (){
      $('#backupTplsContainer').fadeIn();
      params = {
        "contextId" : DFdata.id,
        "page" : page
      };
      ajaxPost(
            null,
            baseUrl+"/costum/blockcms/gettemplatebycategory",
            params,
            function(data){

                var backupStr = ""
              if (data != "") {
                 $.each(data, function(keyBackup,valueBackup) {
                       backupStr +=`
                      <div class="tplItems" data-status="backup">
                      <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">
                      <div class="item-slide">
                      <div class="entityCenter">
                      <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                      </a>
                      </div>
                      <div class="img-back-card">
                      <div class="div-img">
                      <img src="`+valueBackup.profilImageUrl+`">
                      </div>
                      <div class="text-wrap searchEntity">
                      <div class="entityRight profil no-padding">
                      <a href="#" class="entityName letter-orange">
                      <font >`+valueBackup.name+`</font>
                      </a>
                      </div>
                      </div>
                      </div>
                      <div class="slide-hover co-scroll">
                      <div class="text-wrap">
                      <div class="entityPrice">
                      <font class="letter-orange" >`+valueBackup.name+`</font>
                      </div>
                      <div class="entityType col-xs-12 no-padding">
                      <p class="p-short">
                      <font >`+valueBackup.description+`</font>
                      </p>
                      </div>
                      <hr>
                      <ul class="tag-list">
                      <span class="badge btn-tag tag padding-5">
                      <font >
                      </font>
                      </span>
                      </ul>
                      <hr>
                      <div class="desc">
                      <div class="socialEntityBtnActions btn-link-content">
                      <a href="`+valueBackup.profilImageUrl+`" class="btn btn-info btn-link thumb-info" data-title="Aperçu de Creercostum2" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye"></i><?php echo Yii::t("cms", "Preview")?></font>
                      </a>
                      <a href="javascript:;" class="btn btn-info btn-link" onclick="tplObj.switchTemplate('`+keyBackup+`')">
                      <font > <?php echo Yii::t("cms", "Restore")?></font>
                      </a>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      </div>
                      `;
                     });
              }else{
                   backupStr = `<div class='tplItems' data-status='backup'><?php echo Yii::t("cms", "You don't have any recent template on this page")?>!</div>`
              }
              $('#backupTplsContainer').html(backupStr);
            },
            null,
            "json",
            {async : false}
      );

    },

    //Save template user-------------------------------------------
    saveTplUser : function (idTpl, stat) {
      if (idTpl !== "") {
       var tplCtx = {};
       tplCtx.id = idTpl;
       tplCtx.path = "tplsUser."+thisContextId+".<?= $page?>";
       tplCtx.collection = "cms"; 
       tplCtx.value = stat;
       dataHelper.path2Value( tplCtx, function(params) {});
     }else{
      toastr.error("Erreur lors de la mise à jour de Template")
     }
   },

    //Chose template-----------------------------------------------
    chooseConfirm : function(ide,tplSelected){
    if (<?php echo json_encode($tplEdited); ?>) {
      var adjF = "<?php echo Yii::t("cms", "a section")?>"
      var t = "."
      if (<?php echo json_encode($newCmsId); ?>.length > 1) {
        adjF = "<?php echo Yii::t("cms", "sections")?>";
        t = "s.";
      }
      bootbox.dialog({
        message: `<p class=""><?php echo Yii::t("cms", "It seems that you have added")?> `+adjF+`, <br><?php echo Yii::t("cms", "Please save or fuss your template")?>.</p>
        <div class="alert alert-danger" role="alert">
          <p><b><i class="fa fa-exclamation-triangle"></i> NB</b>: <?php echo Yii::t("cms", "Make sure that if you overwrite, your section will be lost")?>!</p>
        </div>`,
        title: "<?php echo Yii::t("cms", "Template selection")?>",
        onEscape: function() {},
        show: true,
        backdrop: true,
        closeButton: true,
        animate: true,
        className: "my-modal",
        buttons: {
          success: {   
            label: "<?php echo Yii::t("common", "Save")?>",
            className: "btn-success",
            callback: function() {
              tplObj.saveThisTpls();
            }
          },
          "<?php echo Yii::t("cms", "Merge")?>": {
            className: "btn-primary",
            callback: function() {
              tplObj.chooseThis(ide,tplSelected);
            }
          },
          "<?php echo Yii::t("cms", "Overwrite")?>": {
            className: "btn-danger",
            callback: function() {
             bootbox.confirm(`<div role="alert">
          <p><?php echo Yii::t("cms", "<b>Delete for all eternity?</b><br><br>You will lose all the sections you added. We cannot recover them once you delete them.<br><br>Are you sure you want to permanently delete this section(s)?")?>
          </p> 
        </div> `,function(result){
              if (!result) {
                return;
              }else{            
                tplObj.deleteCms(<?php echo json_encode($newCmsId); ?>);            
                tplObj.chooseThis(ide,tplSelected);
              }
              urlCtrl.loadByHash(location.hash);
              }); 
            }
          },
          "<?php echo Yii::t("common", "Cancel")?>": function() {}
        }

      }
      );
      }else{    
          tplObj.chooseThis(ide,tplSelected);
      } 
    },

    chooseThis : function(ide,tplSelected){
      if(Array.isArray(ide) && ide.length > 0){
        var tplUsedData = Object.keys("");
          //Check if this tpl already used before 
          if($.inArray(tplSelected,tplUsedData) != -1){       
            tplObj.switchTemplate(tplSelected);
          }else{
            tplObj.duplicateCms("",tplSelected);
            //Check if page never uses a template
            if(tplUsingId == ""){
              tplObj.saveTplUser(tplSelected,"using");
            }else{
              tplObj.switchTemplate(tplSelected);
            }

            urlCtrl.loadByHash(location.hash);

          }

        }else{ 
          $(`<div class="col-md-12 sample-cms text-center custom-block-cms" data-id="undefined">
            <div id="cms-selected" class="selected cms-area-selected blink-info"><p><strong>Cette page ne contient aucun élément!</strong></p>Veuillez selectionner un bloc sur le menu à gauche</div>
            </div>`).insertBefore("#all-block-container");
          showCms()       
          // toastr.error("Erreur lors de la génération du CMS!");
        }
      },

    //Switch template used------------------------------------------
    switchTemplate : function (tplSelected){ 
      tplObj.saveTplUser(tplSelected,"using");
      tplObj.saveTplUser(tplUsingId,"backup");
      urlCtrl.loadByHash(location.hash);
    },

    //Delete cms----------------------------------------------------
    deleteCms : function (ide){
      var params = {
        "ide" : ide,
        "action" : "delete" 
      }
      ajaxPost(
          null,
          baseUrl+"/costum/blockcms/getcmsaction",
          params,
          function(data){
            toastr.success("<?php echo Yii::t("cms", "Element deleted")?>!");
          },
          null,
          "json",
          {async : false}
      );
    },

    upCmsList : function (idTpl){  
      var params = {
        "tplId" : idTpl,
        "ide" : <?php echo json_encode($idCmsMerged) ?>,
        "action" : "upCmsList" 
      }
      ajaxPost(
          null,
          baseUrl+"/costum/blockcms/getcmsaction",
          params,
          function(data){
            toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
          },
          null,
          "json",
          {async : false}
      );
    },

    // Remove cmsList inside tpl if cms is empty--------------------
    removeCmsList : function (idTpl){  
      var params = {
        "tplId" : idTpl,
        "action" : "tplEmpty" 
      }
      ajaxPost(
          null,
          baseUrl+"/costum/blockcms/getcmsaction",
          params,
          function(data){
                toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
          },
          null,
          "json",
          {async : false}
      );
    },

    // Remove cms' stat---------------------------------------------
    removeCmsStat : function (idTpl,elementId){  
      var params = {
        "tplId" : idTpl,
        "ide" : elementId,
        "action" : "removestat" 
      }
      ajaxPost(
          null,
          baseUrl+"/costum/blockcms/getcmsaction",
          params,
          function(data){
          toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
          },
          null,
          "json",
          {async : false}
      );
    },

    //Duplicate cms-------------------------------------------------
    duplicateCms : function (ide, tplSelected, pending = null){ 
        var params = {
          "cmsList"    : cmsList,
          "tplParent"  : tplSelected,
          "page"       : "<?= $page?>",
          "pending"    : "pending",
          "parentId"   : thisContextId,
          "parentSlug" : thisContextSlug,
          "parentType" : thisContextType,
          "action" : "duplicate" 
        }
        ajaxPost(
            null,
            baseUrl+"/costum/blockcms/getcmsaction",
            params,
            function(data){
                toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
            },
            null,
            "json",
            {async : false}
        );
    },

    saveThisTpls : function(){
      if (<?php echo json_encode($idCmsMerged) ?> == "") {
          alert("<?php echo Yii::t("cms", "You can't save an empty template! Please add at least one block")?>")
        }else{
          var tplCtx = {};
          var activeForm = {
            "jsonSchema" : {
              "title" : "<?php echo Yii::t("cms", "Register template")?>",
              "type" : "object",
              "properties" : {
                page : {
                  inputType : "hidden",
                  value : "<?= $page?>"
                },
                name : {
                  label: "<?php echo Yii::t("cms", "Name of the template")?>",
                  inputType : "text",
                  value : "<?php echo ucfirst($tpl) ?>"
                },
                type : { 
                  "inputType" : "hidden",
                  value : "template" 
                },
                category : {
                  label : "<?php echo Yii::t("common", "category")?>",
                  inputType : "select",
                  rules:{
                    "required":true
                  },
                  options : {
                    "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                    "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                    "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                    "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                    "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                    "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                    "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                    "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                    "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                    "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                    "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                    "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                    "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                    "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                    "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                    "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                    "Other"  :"<?php echo Yii::t("cms", "Others")?>"
                  },
                  value : ""
                },
               image : dyFInputs.image(),
                description : {
                  label: "<?php echo Yii::t("common", "Description")?>",
                  inputType : "text",
                  value : ""
                }
              },
              beforeBuild : function(){
                dyFObj.setMongoId('cms',function(data){
                  uploadObj.gotoUrl = location.hash;
                  tplCtx.id=dyFObj.currentElement.id;
                  tplCtx.collection=dyFObj.currentElement.type;
                });
              },
              save : function () {
                tplCtx.value = {};
                $.each( activeForm.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                });
                tplCtx.value.parent={};
                tplCtx.value.parent[thisContextId] = {
                  type : thisContextType, 
                  name : "<?php echo $tpl ?>"
                };
                tplCtx.value.cmsList = <?php echo json_encode($idCmsMerged) ?>;
                // mylog.log(tplCtx);
                tplCtx.value.tplsUser={};
                tplCtx.value.tplsUser[thisContextId] = {
                  "<?= $page?>": "using"
                };
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t("cms", "Element well added")?>");
                      $("#ajax-modal").modal('hide');                  
                    });
                  } );
                  if(tplUsingId != ""){
                   tplObj.saveTplUser(tplUsingId,"backup");
                  }
                  //if "save as new", duplicate and leave currently cms then save it into the new template
                  if (<?php echo json_encode($cmsInUseId) ?> !="") {                
                    tplObj.duplicateCms("",tplCtx.id);
                  }
                  tplObj.removeCmsStat(tplCtx.id,<?php echo json_encode($newCmsId) ?>);
                 urlCtrl.loadByHash(location.hash);
                }
              }
            }
          };    
      dyFObj.openForm( activeForm );
      }
    },

    //Preview template-------------------------------------------------------------

    previewTpl :function () { 
      // localStorage.setItem("previewMode","v");
      mode = "v";
        $(hideOnPreview).fadeOut();
        // $(".hiddenEdit").show();
        $('.handleDrag').hide();
        var styleHideTitleOnPreview = 
        `<style id="hideBlocksCmsTitleOnHover">
          .block-parent:hover .title:not(:empty):before,.block-parent:hover .title-1:not(:empty):before,
          .block-parent:hover .subtitle:not(:empty):before,.block-parent:hover .title-2:not(:empty):before,
          .block-parent .description:not(:empty):before,.block-parent .title-3:not(:empty):before,
          .block-parent:hover .other:not(:empty):before,.block-parent:hover .title-4:not(:empty):before,
          .block-parent .title-5:not(:empty):before,
          .block-parent:hover .title-6:not(:empty):before{
              display: none !important;
          }
        </style>`;
        $('head').append(styleHideTitleOnPreview);
        previewMode = true;
        $(document).trigger('cms-view-mode');
        /*if(localStorage.getItem("forgetPreview") != "true"){
          bootbox.alert("<p class='text-center'><?php echo Yii::t("cms", "Press the <kbd>Escape</kbd> key to disable the preview mode")?></p>"+
            "<input type='checkbox' name='forgetPreview' id='forgetPreview'>  <?php echo Yii::t('cms', 'No more spotting')?> !",function(){
              if($("#forgetPreview").prop('checked') == true)
                localStorage.setItem("forgetPreview","true");
              else
                localStorage.removeItem("forgetPreview");
            });
        }*/
    },
    bindPreviewTpl : function(){
     /* window.onhashchange = function() {
        if($(hideOnPreview).is(':visible') !=true){
          $(hideOnPreview).fadeIn(); 
          previewMode = false;
        }
      }*/
      $(document).keyup(function(e) {
        if (e.key === "Escape") { 
            mode = "w";
            localStorage.setItem("previewMode","w");
            $(hideOnPreview).fadeIn();            
            $(".saveTemplate").show();
            // $(".hiddenEdit").hide();
             tplObj.textModeEdit();
            $("#hideBlocksCmsTitleOnHover").remove();
            previewMode = false;
          }
        });
    },
    editTpl : function () {      
      $(document).trigger('cms-edit-mode');
      tplObj.textModeEdit();
    },
    textModeEdit : function(){
      $(document).trigger('cms-edit-mode');
      $(".sp-text").css("visibility","visible")
      $('.sp-text').each(function(i, objsptext) {
        let allTexts = $(objsptext).html();
        // if (allTexts.match("Ecrire un texte")) {
        // }
        let p = document.createElement('div');
        p.innerHTML = allTexts;
        let links = p.querySelectorAll('.super-href');
        links.forEach(spx => {
          // mylog.log("sp-text "+spx,spx)
          if (spx.classList.contains("lbh")) {
            allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"](#"+(spx.href).split('#')[1]+")");
          }else if (spx.classList.contains("lbh-preview-element")) {
            allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"](#?preview="+(spx.href).split('#')[1]+")");
          }else{
            allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"]("+spx.href+")");
          }
          // allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"]("+spx.href+")");
        });
        $(objsptext).html(allTexts);
        var spcmstextId = document.querySelector('spcms');
        if (spcmstextId !== null) {
          var texttextId = spcmstextId.textContent;
          spcmstextId.parentNode.replaceChild(document.createTextNode(texttextId), spcmstextId);
        }
      });
    },


    filterValue() {
      // return obj.find(function(v){ return v[key] === value});

    }
  }

  $.each(sampleTemplate,function(key,value){
    if(value.name == "Sample template stc"){
      sampleStcId = value._id.$id
      sampleStcEl = value.cmsList
    }else if(value.name == "Sample template"){          
      sampleId = value._id.$id
      sampleEl = value.cmsList
    }
  })

function rgb2Hex(rgb) {
  return rgb.match(/[0-9|.]+/g).map((x,i) => i === 3 ? parseInt(255 * parseFloat(x)).toString(16) : parseInt(x).toString(16)).join('')
}
jQuery(document).ready(function() {

  /****************Background color changer*************/
  $('.sp-bg').each(function(i, targetDoc) {   
    $(targetDoc).addClass($(targetDoc).data("kunik"));
     if($(targetDoc).data("sptarget") == "border") {
      $(targetDoc).css("border-color", $(targetDoc).data("value"));
    }else{
      $(targetDoc).css("background-color", $(targetDoc).data("value"));  
    }

    $("."+$(targetDoc).data("kunik")+" ."+$(targetDoc).data("team")).each(function(i, targetPart) {
      if($(targetPart).data("sptarget") == "border") {
        $(targetPart).css("border-color", $(targetDoc).data("value"));
      }else{
        $(targetPart).css("background-color", $(targetDoc).data("value"));
      }
    })

    <?php if(Authorisation::isInterfaceAdmin()){  ?>
      if ($(targetDoc).css("background-image") == "none") {
      $( `
        <div class="col-md-12">
        <div class="edit-bg edit-main-bg hiddenPreview">
        <a class="tooltips editable-bg ${i}sp-picker-parent" type="button" data-toggle="tooltip" data-placement="bottom" data-original-title="Changer la couleur du fond">
        <input class="color-apply ${i}sp-bg-picker spectrum sp-colorize" type="color" id="myColor" value="${$(targetDoc).data("value")}"> 
        </a>
      </div>
      </div>
        ` ).insertBefore(targetDoc);
        }
    <?php } ?>

    $(".edit-bg").mouseenter(function () {
      $(this).css("opacity", "1")
    $(".sp-container").show();
    })
    $(".edit-bg").mouseleave(function () {
      $(this).css("opacity", "0.4")
    })
    setTimeout(function(){
      $("."+i+"sp-picker-parent .sp-preview-inner").css("background-color",$(targetDoc).data("value"))
      $(".edit-bg").css("opacity", "0.4")
      $(".sp-dd").html('<i class="fa fa-paint-brush fa-lg" aria-hidden="true"></i>')
      $(".sp-replacer").css("width", "40px")
      $(".sp-replacer").css("height", "20px")
    },1000)
  

  $("."+i+"sp-bg-picker").on("change" , function () {
    let color = $("."+i+"sp-picker-parent .sp-preview-inner").css("background-color");
    // color = "#"+rgb2Hex(color)
    let $this = $(this);
    if($(targetDoc).data("sptarget") == "border") {
      $this.parent().parent().parent().siblings(".sp-bg").css("border-color", color);
    }else{
      $this.parent().parent().parent().siblings(".sp-bg").css("background-color", color);
    }

    $("."+$(targetDoc).data("kunik")+" ."+$(targetDoc).data("team")).each(function(i, targetPart) {
      if ($(targetPart).data("sptarget") == "background") {
        $(targetPart).css("background-color", color);
      }else if($(targetPart).data("sptarget") == "border") {
        $(targetPart).css("border-color", color);
      }
    });

    tplCtx = {};
    tplCtx.id = $(targetDoc).data("id");;
    tplCtx.collection = "cms";
    tplCtx.path = $(targetDoc).data("field");
    tplCtx.value = color;

    dataHelper.path2Value( tplCtx, function(params) {} );
  
  })
      //color
     function loadColorPicker(callback) {
      // mylog.log("loadColorPicker");
      if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
        // mylog.log("loadDateTimePicker2");
        lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
          baseUrl+'/plugins/colorpicker/css/colorpicker.css',
          callback);
      }
    }
    function loadSpectrumColorPicker(callback) {
      // mylog.log("loadSpectrumColorPicker");
      lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
        baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
        callback);

    }
    var initColor = function(){
      // mylog.log("init .colorpickerInput");
      $(".colorpickerInput").ColorPicker({ 
        color : "pink",
        onSubmit: function(hsb, hex, rgb, el) {
          $(el).val(hex);
          $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
          $(this).ColorPickerSetColor(this.value);
        }
      }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
      });
    };

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if(  $("."+i+"sp-bg-picker").length){
        $("."+i+"sp-bg-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.'+i+'sp-bg-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });
  });
  /*************End background color changer*************/

  /*****************Right click options*****************/
  <?php if(Authorisation::isInterfaceAdmin()){  ?>
    var menu = document.querySelector('.tpl-cms-menu');
    let remainsRight = 0;
    let remainsBottom = 0;

    function showMenu(x, y){
      if(previewMode != false){
        $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
      }else{
        $(".hiddenEdit").html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
      }
      menu.style.left = x + 'px';
      menu.style.top = y + 'px';
      menu.classList.add('tpl-menu-show');
      $(".sp-text").attr('contenteditable','false');
      $(".sp-text").removeClass('edit-mode-textId');
      $("#toolsBar").html('');
      $("#toolsBar").hide()
    }

    if (localStorage.getItem("activeRC") == "false") {    
      $(".activeRC").html("Activer le click droit <i class='fa fa-toggle-off'></i>")
      $(".activeRC").parent().css("background-color","#9fbd38")
    }else{        
      $(".activeRC").html("Desactiver le click droit <i class='fa fa-toggle-on'></i>")
      $(".activeRC").parent().css("background-color","")
    }
    $(".activeRC").on("click", function () {
      if (localStorage.getItem("activeRC") == "false") {        
        localStorage.removeItem("activeRC")
        $(this).html("Desactiver le click droit <i class='fa fa-toggle-on'></i>")
        $(this).parent().css("background-color","")
      }else{        
        localStorage.setItem("activeRC", "false")
        $(this).html("Activer le click droit <i class='fa fa-toggle-off'></i>")
        $(this).parent().css("background-color","#9fbd38")
      }
    })

    $(document).bind('contextmenu', function(e) {
      if (localStorage.getItem("activeRC") != "false"){
        e.preventDefault()
        $(".sp-cms-context-menu").html(tplHtmlCtxMenu);
        $(document).trigger('rightclick');
        let bodyH = $("body").height();
        let bodyW = $("body").width();
        let menuPositionTop = e.pageY;
        let menuPositionLeft = e.pageX;
        remainsRight = bodyW - menuPositionLeft
        remainsBottom = pageYOffset + bodyH - menuPositionTop

        setTimeout(function () {
          let menuH = $(".sp-cms-context-menu").height();
          let menuW = $(".sp-cms-context-menu").width();
          let menuChildW = 0;
        // position main menu from top 
        if (remainsBottom < menuH) {
          menuPositionTop = menuPositionTop - menuH
        }
        // position main menu from right 
        if (remainsRight < menuW) {
          menuPositionLeft = menuPositionLeft - menuW
        }

        $(document).on("mouseenter",".tpl-cms-menu-item-submenu" ,function(ev) {
          let submenu = $(this).children(".tpl-cms-menu")
          menuChildW += submenu.width();
          submenu.css("display","flex");
          submenu.css("top", -1)

          // position submenu from top 
          if (pageYOffset + bodyH - ev.pageY < submenu.height()) {
            submenu.css("top", 25-$(this).find(".tpl-cms-menu").height())  
          } 

          // prosition submenu from right 
          if (remainsRight < menuChildW+menuW) {
            submenu.css("left", -menuW) 
          }
        }) 
        $(document).on("mouseleave",".tpl-cms-menu-item-submenu" ,function() {   
          menuChildW = 0;
        })
        showMenu(menuPositionLeft, menuPositionTop);  

        if(location.href.indexOf('/costum/co/index/') != -1)
          $('#go-to-communecter').attr("href",baseUrl+"/#@"+thisContextSlug);
        else
          $('#go-to-communecter').attr("href","https://www.communecter.org/#@"+thisContextSlug);
      },11)

        $(".add-other-cms").on('click',function () {
          if($(".sample-cms").length == 0){          
            $(`<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">
              <div class="selected cms-area-selected blink-info">Veuillez selectionner un bloc sur le menu à gauche</div>
              </div>`).insertAfter("#all-block-container");
            $([document.documentElement, document.body]).animate({
              scrollTop: $(".other-cms").offset().top
            }, 500);
            showCms() 
          }  
        });
      }
    });


    $('body').click(function() {
      $(".tpl-menu-show").removeClass('tpl-menu-show');
    });
    $('block-parent').click(function() {
      $('.sp-text').blur()
    });
    // $('.main-container').bind('contextmenu',function() {
    //   // let $this = $(this);
    //     let addCms = [
    //     {
    //       class : "add-other-cms",
    //       label : "<?php echo Yii::t("common", "Add a block in the page")?>",
    //       preview : "hidden",
    //       icon : "level-down"
    //     }
    //     ] 
    //     cmsCtxMenu.add(addCms);
    // setTimeout(function(){       
    // },12)
      // });

  <?php }  ?>
  /*****************End right click options*****************/

$(".btn_tpl_filter").click(function(){
    params = {
    "category" : $(this).data("tplcategory"),
    "type" : "template"
  };
  ajaxPost(
      null,
      baseUrl+"/costum/blockcms/gettemplatebycategory",
      params,
      function(data){
          $.each(data,function(key,value){
              // console.log("Template"+ value.name);
          })
      },
      null,
      "json",
      {async : false}
  );
});

//$(".editDesc").click(function(){
$('body').on('click',".editDesc", function () {
  // mylog.log("sary", $(this).data("img"));
  var tplCtx = {};
  var tplId = $(this).data("id");
  tplCtx.collection = "cms";
  var activeFormEdit = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t("cms", "Modification of the template")?>",
          "type" : "object",
          "properties" : {
            name : {
              label: "<?php echo Yii::t("cms", "Name of the template")?>",
              inputType : "text",
              value : $(this).data("name")
            },
            type : { 
              "inputType" : "hidden",
              value : "template" 
            },
            category : {
              label : "<?php echo Yii::t("common", "Category")?>",
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                  "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                  "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                  "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                  "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                  "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                  "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                  "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                  "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                  "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                  "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                  "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                  "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                  "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                  "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                  "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                  "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                  "Other"  :"<?php echo Yii::t("cms", "Others")?>"
              },
              value : $(this).data("category")
            },
            image : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t("cms", "Background image")?>",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : $(this).data("img")
          },
            description : {
              label: "<?php echo Yii::t("common", "Description")?>",
              inputType : "text",
              value : $(this).data("desc")
            }
          },

          beforeBuild : function(){
            uploadObj.set("cms",tplId);
          },

          save : function () {
            tplCtx.id = tplId;
            tplCtx.value = {};
            $.each( activeFormEdit.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            // mylog.log(tplCtx);
            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
              tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
             dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                if(params.result){
                  toastr.success("Modification enregistré!");
                  $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              }else
              toastr.error(params.msg);
            });
            } );
           }
         }
       }
     }; 
  dyFObj.openForm( activeFormEdit );
});

$(".saveChangeTpl").click(function(){
  tplCtx.id = tplUsingId;
  tplCtx.collection = "cms";
    var activeForm = {
      "jsonSchema" : {
        "title" : "<?php echo Yii::t("cms", "Others")?>Enregistrement du template",
        "type" : "object",
        onLoads : {
          onload : function(data){
            $(".parentfinder").css("display","none");
          }
        },
        "properties" : {
          name : {
            label: "<?php echo Yii::t("cms", "Name of the template")?>",
            inputType : "text",
            value : `<?php echo $nameTpl ?>`
          }, 
          category : {
              label : "<?php echo Yii::t("common", "Category")?>",
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                  "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                  "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                  "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                  "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                  "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                  "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                  "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                  "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                  "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                  "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                  "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                  "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                  "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                  "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                  "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                  "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                  "Other"  :"<?php echo Yii::t("cms", "Others")?>"
              },
              value : "<?php echo htmlspecialchars_decode($categoryTpl) ?>"
            },
          image : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t("cms", "Background image")?>",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : <?php echo json_encode($tplInitImage); ?>
          },
          description : {
            label: "<?php echo Yii::t("common", "Description")?>",
            inputType : "text",
            value : `<?php echo $descriptionTpl ?>`
          }
        }
      }
    };          
     activeForm.jsonSchema.afterBuild = function(){     
            uploadObj.set("cms",tplUsingId);
      }; 

    activeForm.jsonSchema.save = function () {
      tplCtx.path = "allToRoot";
      tplCtx.value = {};
      $.each( activeForm.jsonSchema.properties , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      tplCtx.value.cmsList = [];
      tplCtx.value.cmsList = <?php echo json_encode($idCmsMerged) ?>;
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        // mylog.log("activeForm save tplCtx",tplCtx);
        tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
        tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
        dataHelper.path2Value( tplCtx, function(params) {
          dyFObj.commonAfterSave(params,function(){
            if(params.result){
              toastr.success("Modification enregistré!");
              $("#ajax-modal").modal('hide'); 
              urlCtrl.loadByHash(location.hash);
              smallMenu.open(tplMenu);
            }  else {
              toastr.error(params.msg);
            tplObj.removeCmsStat(tplCtx.id,<?php echo json_encode($newCmsId) ?>);
            }
          });
        } );
      } 
    }
    dyFObj.openForm( activeForm );
});

$(".editBtn").off().on("click",function() { 
  var activeForm = {
    "jsonSchema" : {
      "title" : "Template config",
      "type" : "object",
      "properties" : {
      }
    }
  };
  if(configDynForm.jsonSchema.properties[ $(this).data("key") ])
    activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
  else
    activeForm.jsonSchema.properties[ $(this).data("key") ] = { label : $(this).data("label") };

  if($(this).data("label"))
    activeForm.jsonSchema.properties[ $(this).data("key") ].label = $(this).data("label");
  if($(this).data("type")){
    activeForm.jsonSchema.properties[ $(this).data("key") ].inputType = $(this).data("type");
    if($(this).data("type") == "textarea" && $(this).data("markdown") )
      activeForm.jsonSchema.properties[ $(this).data("key") ].markdown = true;
  }
  tplCtx.id = contextData.id;
  tplCtx.collection = contextData.type;
  tplCtx.key = $(this).data("key");
  tplCtx.path = $(this).data("path");

  activeForm.jsonSchema.save = function () {  
    tplCtx.value = $( "#"+tplCtx.key ).val();
    // mylog.log("activeForm save tplCtx",tplCtx);
    if(typeof tplCtx.value == "undefined")
      toastr.error('value cannot be empty!');
    else {
      dataHelper.path2Value( tplCtx, function(params) { 
        $("#ajax-modal").modal('hide');
      } );
    }
  }
  dyFObj.openForm( activeForm );
});


   $(".editTpl").off().on("click",function() { 
    var activeForm = {
      "jsonSchema" : {
        "title" : "Edit Question config",
        "type" : "object",
        "properties" : {}
      }
    };

    $.each( formTpls [ $(this).data("key") ] , function(k,val) { 
      // mylog.log("formTpls",k,val); 
      activeForm.jsonSchema.properties[ k ] = {
        label : k,
        value : val
      };
    });

    // mylog.log("formTpls activeForm.jsonSchema.properties",activeForm.jsonSchema.properties); 
    tplCtx.id = $(this).data("id");
    tplCtx.key = $(this).data("key");
    tplCtx.collection = $(this).data("collection");            
    tplCtx.path = $(this).data("path");

    activeForm.jsonSchema.save = function () {  
      tplCtx.value = {};
      $.each( formTpls [ tplCtx.key ] , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      // mylog.log("save tplCtx",tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          $("#ajax-modal").modal('hide');
        } );
      }

    }
    dyFObj.openForm( activeForm );
  });

//check if new costum
if(localStorage.getItem("newCostum"+thisContextSlug) == thisContextSlug){
      bootbox.confirm({
          message: "<h5 class='text-success text-center'><?php echo Yii::t('cms', 'Welcome to your new costum')?> !</h5>"+
                    "<p class='text-success text-center'><?php echo Yii::t('cms', 'You have <b>4 steps</b> to create your costum')?> :</p>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 1:</u> <?php echo Yii::t('cms', 'Choose a template')?></h6>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 2:</u> <?php echo Yii::t('cms', 'Add section or block CMS')?></h6>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 3:</u> <?php echo Yii::t('cms', 'Add APP or MENU')?></h6>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 4:</u> <?php echo Yii::t('cms', 'Adding data to the CMS block')?></h6>",
          buttons: {
              confirm: {
                  label: 'Ok',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'hidden'
              }
          },
          callback: function (result) {
              if(result){
                  localStorage.removeItem("newCostum"+thisContextSlug);
                  $(".tpl-engine-btn-list ul li.a").addClass("btn-pulse").on('click',function(){
                      $(this).removeClass("btn-pulse");
                      $(".openListTpls").addClass("btn-pulse").on('click',function(){
                          $(this).removeClass("btn-pulse");
                          $(".tpl-engine-btn-list ul li.g").addClass("btn-pulse").on('click',function(){
                              $(this).removeClass("btn-pulse");
                          })
                      })
                  });

              }
          }
      });
   }
  tplObj.bindPreviewTpl();
});

var hideOnPreview = '.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.handleDrag,.previewTpl,.openListTpls,.handleDrag,.content-btn-action,.hiddenPreview';
var previewMode = true;
if (localStorage.getItem("previewMode") == "w") {  
 previewMode = false;
}else{    
 previewMode = true;
}
/*$(hideOnPreview).fadeOut();
bootbox.alert('<h6 class="text-center">Appuyer sur ECHAPE pour passer en mode edit</h6>'+
'<p class="text-center"><input type="checkbox" name="not-remember-preview"> Ne plus repéter</p>',function(result){

})*/

/*$(document).on("click",".tryOtherTpl", function(){
  smallMenu.open(tplMenu);
});*/

$('body').on('click',"button.btn-filter", function () {
  var $target = $(this).data('target');
  if ($target != 'all') {
    $('.tplItems').css('display', 'none');
    $('.tplItems[data-status="' + $target + '"]').fadeIn();
  } else {
    $('#backupTplsContainer').hide();
    $('.tplItems').css('display', 'none').fadeIn();
  }
});

  $("#openModal").modal("hide");
 //In case of empty cmsId, delete cmsList inside tpl to avoid conflict when a user choose it 
if (<?= json_encode($idCmsMerged) ?> == "" && <?= json_encode($btnSaveChange) ?>) {
  tplObj.removeCmsList(tplUsingId);
}

/**************Supercms required*************
 **************Keep selected text when focus on input color*************/
 var saveSelection, restoreSelection;

if (window.getSelection && document.createRange) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var range = win.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;

        return {
            start: start,
            end: start + range.toString().length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var charIndex = 0, range = doc.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = win.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
} else if (document.selection) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var selectedTextRange = doc.selection.createRange();
        var preSelectionTextRange = doc.body.createTextRange();
        preSelectionTextRange.moveToElementText(containerEl);
        preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
        var start = preSelectionTextRange.text.length;

        return {
            start: start,
            end: start + selectedTextRange.text.length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var textRange = doc.body.createTextRange();
        textRange.moveToElementText(containerEl);
        textRange.collapse(true);
        textRange.moveEnd("character", savedSel.end);
        textRange.moveStart("character", savedSel.start);
        textRange.select();
    };
}


/*************END Keep selected text when focus on input color*************/

<?php if(Authorisation::isInterfaceAdmin()){  ?>
  $(document).keyup(function(e) {
    if (e.altKey == true && e.keyCode == 80) {
      if(previewMode == false){
        tplObj.previewTpl();
        previewMode = true;
        $(document).trigger("sp-preview")
      }else if(previewMode == true){
        $(document).trigger("sp-edit")
        $(hideOnPreview).show();
        $(".saveTemplate").show();
        // $(".hiddenEdit").hide();
        $("#hideBlocksCmsTitleOnHover").remove();
        setTimeout(function() {
          localStorage.setItem("previewMode","w");
          previewMode = false;
        },900)
         tplObj.editTpl();
         $(".sp-text").css("visibility","visible")
      }
    }else if (e.altKey == true && e.keyCode == 8) {
      $(document).trigger("sp-show-parent-id")
    }
    e.stopImmediatePropagation();
  });

  if (localStorage.getItem("previewMode") == "w" || cmsList == 0) {  
    $(".saveTemplate").show();
    $(".sp-text").fadeIn();
    $(hideOnPreview).fadeIn();
    mode = "w";
  }else{
    tplObj.previewTpl();
    mode = "v";
  }
<?php } ?>
/*****************check for links [text](url)*****************/
// if ($(".sp-text").text() == "") {
//   let text = "Ecrire un texte...";
//   $(".sp-text").html("Ecrire un texte...");
  // console.log("Empty", $(".sp-text"));
// }
$(".sp-text").each(function(i, sptext) {
 let text = "Ecrire un texte...";
 let $sptext = ($(sptext).text()).toLowerCase();
 if($sptext.indexOf('lorem') >= 0 || $sptext.indexOf('ipsum') >= 0){
   $(sptext).html(text);
 }
 if ($(sptext).text() == "") {
  $(sptext).html(text);
 }

})


function convAndCheckLink(elems){  
  $(elems).each(function(i, objelems) {
    var derText = $(objelems).html();
    if ($(objelems).text().match("Ecrire un texte")) {
      $(objelems).css("visibility","hidden")
    }
    /* console.log("super",derText);*/
    $(objelems).fadeIn("show");


    if (derText !== 'undefined') {
      derText = derText.replace(/(?:\r\n|\r|\n)/g, '<br>');
      let elements = derText.match(/\[.*?\)/g);
      if( elements !== null && elements.length > 0){
        for(el of elements){
          if (el.match(/\[(.*?)\]/) !== null) {
            let eltxt = el.match(/\[(.*?)\]/);
            let elurl = el.match(/\((.*?)\)/);
            if (elurl !== null && eltxt !== null) {            
              let txt = eltxt[1];
              let spUrl = elurl[1];
              // mylog.log("all url", spUrl)
              if (spUrl.includes("#")) {                
                let textBeforHtag = spUrl.substring(0, spUrl.indexOf("#"));
                let textBefor_full_UrlHtag = current_full_Url.substring(0, current_full_Url.indexOf("#"));
                let textAfterHtag = spUrl.split('#')[1]
                // mylog.log("all url misy #", textBeforHtag)

                // added 1st condition for internal anchors
                let extLoadableUrls= (typeof costum!="undefined" && costum.app!="undefined") ? Object.keys(urlCtrl.loadableUrls).concat(Object.keys(costum.app)) : Object.keys(urlCtrl.loadableUrls);
                if(textAfterHtag.includes("?preview=")){
                  if (textAfterHtag.includes("preview=poi")) {
                    textAfterHtag = "page.type.poi.id."+textAfterHtag.split('poi.')[1]
                  }else if (textAfterHtag.includes("preview=projects")) {
                    textAfterHtag = "page.type.projects.id."+textAfterHtag.split('projects.')[1]
                  }else if (textAfterHtag.includes("preview=organizations")) {
                    textAfterHtag = "page.type.organizations.id."+textAfterHtag.split('organizations.')[1]
                  }else if (textAfterHtag.includes("preview=citoyens")) {
                    textAfterHtag = "page.type.citoyens.id."+textAfterHtag.split('citoyens.')[1]
                  }else if (textAfterHtag.includes("preview=events")) {
                    textAfterHtag = "page.type.events.id."+textAfterHtag.split('events.')[1]
                  }else if (textAfterHtag.includes("preview=news")) {
                    textAfterHtag = "page.type.news.id."+textAfterHtag.split('news.')[1]
                  }else if (textAfterHtag.includes("preview=cities")) {
                    textAfterHtag = "page.type.cities.id."+textAfterHtag.split('cities.')[1]
                  }
                    textAfterHtag = textAfterHtag.replace("?preview=","")
                    derText = derText.replace(el,'<a class="lbh-preview-element super-href" href="#'+textAfterHtag+'">'+txt+'</a>');
                }else if (textBeforHtag == textBefor_full_UrlHtag || textBeforHtag == "") {   
                  let hashToLoad = spUrl.slice(spUrl.lastIndexOf('#') + 1);
                  derText = derText.replace(el,`<a class="lbh super-href" href="#`+hashToLoad+`">`+txt+`</a>`)          
                }else{
                  derText = derText.replace(el,'<a target="_blank" class="super-href" href="'+spUrl+'">'+txt+'</a>');
                }
              }else{              
              derText = derText.replace(el,'<a target="_blank" class="super-href" href="'+spUrl+'">'+txt+'</a>')
            }
          }else{
            <?php if(Authorisation::isInterfaceAdmin()){?>
              derText = derText.replace(el,'<spcms style="color:red;background-color:whitesmoke;" data-toggle="tooltip" data-placement="top" title="<?php echo Yii::t("cms", "Syntax error. Please try again")?>!">'+el+'</spcms>')
            <?php } ?>
          }
        }
      }
    }
    $(objelems).html(derText);
  }

}); 
  coInterface.bindLBHLinks()
}
/***************End check for links [text](url)*****************/

/************Hide edit btn mouse inactive************/
// var timedelay = 1;
// function delayCheck()
// {
//   if(timedelay == 5)
//   {
//     $('.hiddenEdit').fadeOut( 1600 );
//     timedelay = 1;
//   }
//   timedelay = timedelay+1;
// }
// $(document).mousemove(function() {
//   if (mode == "v") {
//   $('.hiddenEdit').fadeIn();
//   timedelay = 1;
//   clearInterval(_delay);
//   _delay = setInterval(delayCheck, 500);
//   }
// });
// _delay = setInterval(delayCheck, 500)


$(document).on("mousedown", ".hiddenEdit", function(event){
  if(previewMode === false){
    tplObj.previewTpl();
    previewMode = true;
    $(this).html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
  }else if(previewMode === true){
    localStorage.setItem("previewMode","w");
    $(this).html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
    $(hideOnPreview).fadeIn();
        // $(".hiddenEdit").hide();
        $(".saveTemplate").show();
        $("#hideBlocksCmsTitleOnHover").remove();
        setTimeout(function() {
          previewMode = false;
        },100)
        tplObj.editTpl();
      }
      event.stopImmediatePropagation();
    })
/************End hide edit btn mouse inactive************/

  $(document).on("click", ".closeMenu" , function(){
    /*alert("yes")*/
    /*$("#toolsBar").hide();
    $("#toolsBar").html("");*/
  });
/***************End supercms required***************/

</script>

<?php 
/* 
Super text:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/
?>
<style>

    .bold {
      font-weight: bold;
    }
    .italic{
     font-style: italic;
    }

    .right{
     text-align: right;
   }
   .center{
     text-align: center;
   }

   .left{
     text-align: left;
   }
   .justify{
     text-align: justify;
   }
   .form_font{
    display: block;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}



#container #containerHeader{
   /* text-align: center;
    cursor:move;*/
}
#container #itino {
    border: 1px solid grey;
    height: 100px;
    width: 602px;
    margin: 0px auto 0;
    padding:10px;
   }
#container fieldset {
    margin: 2px auto 0px;
    width: 100%;
    min-height:26px;
    background-color: #ffffff;
    border:none;
    padding: 2px;
}
#container button {
    width: 5ex;
    text-align: center;
    padding: 1px 3px;
    height:30px;
    width:30px;
    background-repeat: no-repeat;
    background-size: cover;
    border:none;
}
#container img{
      width:100%;
}
#container .bold{
 font-weight: bold;
}
#container .italic{
    font-style: italic;
}
#container .underline{
   text-decoration: underline;
}
#container .color-apply{
    width: 50px;
}
#container #input-font{
    width: 100px;
    height: 25px;
}
/*[contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#888;
}
*/
[contenteditable]:focus {
    outline: 0px solid transparent;
}

#container .loader {
    border: 6px solid #f3f3f3; 
    border-top: 6px solid #3498db; 
    border-radius: 50%;
    width: 20px;
    height: 20px;
    animation: spin 2s linear infinite;
    margin: 0 auto;
    line-height: 20px;
}

@media (max-width: 800px) {
 .container<?= @$blockCms['tplParent'] ?>{
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 2px;
  padding-bottom: 2px;
  margin-left: 0px;    
  margin-right: 0px;      
  position: sticky; 
 }

     .whole-container<?= @$blockCms['tplParent'] ?> {
      padding-left: 0px;
      padding-right: 0px;
      margin-left: 0px;    
      margin-right: 0px;   
    }
}

/*a {
 color: inherit;
 font-family: inherit !important;
} 

button {
 font-size: 18px !important;
 font-family: FontAwesome; !important;
 color: inherit;
} 

a:hover, a:focus, a:active, a.active { color: inherit; } */

.edit-mode-textId { 
  text-transform:none;
  box-shadow: #038cce24 0px 0px 0px 1px;
    border-radius: 3px;
    height: min-content;
    width: 100%;
    cursor: pointer;

}

.focus-mode-textId {
  box-shadow: #bec8ef -1px 2px 5px 0px;
  border: solid 1px #ffffffa1;
  border-radius: 3px;
  height: min-content;
  width: 100%;
  cursor: text;
}
#mainNav .editing span{
  box-shadow: #bec8ef -1px 2px 5px 0px;
  border: solid 1px #ffffffa1;
  border-radius: 3px;
  height: min-content;
  width: 100%;
  cursor: text;
}
.sp-text{
  text-transform:none;
  display:none;
}

</style>

<script type="text/javascript">

// $("#itino *").attr('contenteditable','true');

/*****************Admin panel*****************/
  <?php if(Authorisation::isInterfaceAdmin()){ ?> 
    let rightclick = false;
    var spDefaultText = "";
    $(document).on('focusin','.sp-text', function(e) {
      e.preventDefault();
   // alert("yes")
    let $this = $(this);
    let timeStamp = Date.now();
    let pasted = false;
    spDefaultText = $this.html()
      // if (mode == "w") {
        // $(this).attr("id","textEditing");
        if (previewMode == false) {
          $(this).addClass("focus-mode-textId");
        }
        $(this).removeClass("edit-mode-textId");
        $this.attr('id', timeStamp);
        text = $(this).text();
        if (text == "Ecrire un texte...") {
          $(this).text("");
        }
        // Copy text as text without styles
        rightclick = false;
        $(document).on("rightclick", function(){
          rightclick = true;
            // alert("es")
          })
        setTimeout(function () {
          // alert(rightclick)
         if (rightclick == false) {
          let ce = document.getElementById(timeStamp);
         // for (let i = 0; i < ce.length; i++) {
            // alert("yes");
            ce.addEventListener('paste', function (e) {
              e.stopImmediatePropagation()
              if (pasted === false) {
                // mylog.log("texts "+pasted);
                pasted = true;
                let texts = (e.clipboardData || window.clipboardData).getData('text/plain');
                $this.append("<span class='ifa hidden'>"+texts+"</span>")
                document.execCommand('insertText', false, $(".ifa").text());
                e.preventDefault();
                $(".ifa").remove();
              }
            })
          }
        // }
      },10)
      // }
    });

    $(document).on('focusout','.sp-text', function() {
        // $(this).removeAttr('id');
      if (previewMode == false) {
        $(this).removeClass("focus-mode-textId");
        $(this).addClass("edit-mode-textId");
      }
    });




   /*function replace_contenttextId(content)
   {
     var exp_match = /(\b(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
     var element_content=content.replace(exp_match, "<a href='$1'>$1</a>");
     var new_exp_match =/(^|[^\/])(www\.[\S]+(\b|$))/gim;
     var new_content=element_content.replace(new_exp_match, `<a target="_blank" href="http://$2">$2</a>`);
     return new_content;
   }  
   $('#itino').html(replace_contenttextId($('#itino').html()));*/


/************Preview and read mode************/
$(document).keyup(function(e) {
  if (e.altKey == true && e.keyCode == 80) {
    convAndCheckLink(".sp-text");
    $(".sp-text").removeClass("edit-mode-textId");
    $(".sp-text").removeClass("selected-mode-textId");
 /*   mode = "v"
    let elems = document.getElementById("itino");
    if (elems !== null) {
      document.getElementById("itino").contentEditable = "false";
    }
  $(".textId-super").removeClass("edit-mode-textId");*/
  }
});



$(".view-super-cms").click(function(){
  mode = "v"
  convAndCheckLink(".sp-text");
  $(".sp-text").removeClass("edit-mode-textId");
  $(".sp-text").removeClass("selected-mode-textId");
  let elems = document.getElementById("itino");
  if (elems !== null) {
    document.getElementById("itino").contentEditable = "false";
  }
});

function spValid(element,sp_message){
 let devElem = $(element).val();
 let strDev = "";
 if (devElem.indexOf('script') > -1 || devElem.indexOf(')') > -1 || devElem.indexOf('}') > -1 || devElem.indexOf(']') > -1){
  $(".sp-message").html(sp_message);
  return false;
}else{
  strDev = devElem;
  $(".sp-message").html("");
  return strDev;
}
}

$(document).keyup(function(e) {
  if (e.key === "Escape") { 
    tplObj.editTpl();
    $(".saveTemplate").show();
    // $(".openListTpls").show();
  }
});

// $(".hiddenEdit").click(function(){
//     tplObj.editTpl();
// })


/************End preview and read mode************/
$("#toolsBar").draggable();
cmsBuilder.init();
/*  document.getElementById("itino").contentEditable = "true";*/

let allText = $(".edit-mode-textId").html();
let p = document.createElement('p');
p.innerHTML = allText;
let links = p.querySelectorAll('a');
links.forEach(x => {
  allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
});
$(".edit-mode-textId").html(allText);


   function insertTextAtCaret(text) {
    var sel, range;
    if (window.getSelection) {
      sel = window.getSelection();
      selectedText = sel.toString();
      if (selectedText !== "") {
        label = "["+selectedText+"](http://www.) ";
      }else{
        label = text;
      }
      if (sel.getRangeAt && sel.rangeCount) {
        range = sel.getRangeAt(0);
        range.deleteContents();
        range.insertNode( document.createTextNode(label) );
      }
    } else if (document.selection && document.selection.createRange) {
      document.selection.createRange().label = label;
    }
  }

// function makeEditableAndHighlighttextId(colour) {
//     var range, sel = window.getSelection();
//     if (sel.rangeCount && sel.getRangeAt) {
//         range = sel.getRangeAt(0);
//     }
//     document.designMode = "on";
//     if (range) {
//         sel.removeAllRanges();
//         sel.addRange(range);
//     }
//     // Use HiliteColor since some browsers apply BackColor to the whole block
//     if (!document.execCommand("HiliteColor", false, colour)) {
//         document.execCommand("Color", false, colour);
//     }
//     document.designMode = "off";
// }

// function highlighttextId(colour) {
//     var range, sel;
//     if (window.getSelection) {
//         // IE9 and non-IE
//         try {
//             if (!document.execCommand("Color", false, colour)) {
//                 makeEditableAndHighlighttextId(colour);
//             }
//         } catch (ex) {
//             makeEditableAndHighlighttextId(colour)
//         }
//     } else if (document.selection && document.selection.createRange) {
//         // IE <= 8 case
//         range = document.selection.createRange();
//         range.execCommand("Color", false, colour);
//     }
// }

  $(".textIdfont-size").change(function() {        
    var item=$(this);
    var val = (item.val())*1;
    $("#itino").css("fontSize", val);
  });


  $(function () {
    var count = 1;

  // Create click event handler on the document.
/*  $(document).on("click", function (event) {
    // If the target is not the container or a child of the container, then process
    // the click event for outside of the container.
    if ($(event.target).closest(".textId-super").length === 0) {
      $(".textIdtoolbar").hide();
    }
  });*/
/*  $(document).off().on('click', '.sp-container', function () {
    alert("yes");
  });*/
  // Create click event handler on the container element.

  /**************Open text settings panel**************/
  
  $(".view-super-cms").on("click", function () { 
     $(".sp-text").attr('contenteditable','false');
     $(".sp-text").removeClass('edit-mode-textId'); 
    $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
  });
  $(document).on("mouseup", ".sp-text", function (e) { 
    if (previewMode == false) {
      editable = $(this)[0];
      divSelection = saveSelection(editable);
    }
    e.stopImmediatePropagation()
  });

  $("#display-cms").hover(function(){
    mode = "w"
  })

  $(document).on("mousedown",".sp-text", function (e) {
    e.stopImmediatePropagation()
    let $this = $(this);
    if (previewMode == false) {
      $this.attr('contenteditable','true');
    }

    sel = window.getSelection();
    $(".sp-container").hide();
     if (previewMode == false) {      
        $(this).children(":first").focus();
        $("#toolsBar").show();
          $("#toolsBar").html(`
                  <div class="container-fluid" style="background-color: #f2f2f2;">
                  <div class="text-center cursor-move">
                    <i style="cursor:default;font-size: large;" aria-hidden="true"><?php echo Yii::t("cms", "Text settings")?></i>
                    <i class="fa fa-window-close closeBtn pull-right" aria-hidden="true"></i>
                  </div>
                  <div id="container" >
                    <fieldset>
                      <button class="fontStyle italic" onclick="document.execCommand('italic',false,null);" title="Italicize Highlighted Text"><i class="fa fa-italic"></i></button>
                      <button class="fontStyle bold" onclick="document.execCommand( 'bold',false,null);" title="Bold Highlighted Text"><i class="fa fa-bold"></i></button>
                      <button class="fontStyle underline" onclick="document.execCommand( 'underline',false,null);"><i class="fa fa-underline"></i></button>
                      <i class="fa fa-text-height"></i>
                      <select id="input-font" class="input">
                        
                      </select>
                      <button class="fontStyle strikethrough" onclick="document.execCommand( 'strikethrough',false,null);"><i class="fa fa-strikethrough"></i></button>
                      <button class="fontStyle align-left" onclick="document.execCommand( 'justifyLeft',false,null);"><i class="fa fa-align-left"></i></button>
                      <button class="fontStyle align-justify" onclick="document.execCommand( 'justifyFull',false,null);"><i class="fa fa-align-justify"></i></button>
                      <button class="fontStyle align-center" onclick="document.execCommand( 'justifyCenter',false,null);"><i class="fa fa-align-center"></i></button>
                      <button class="fontStyle align-right" onclick="document.execCommand( 'justifyRight',false,null);"><i class="fa fa-align-right"></i></button>
                     <!-- 
                      <button class="fontStyle orderedlist disabled" onclick="document.execCommand('insertOrderedList', false, null);"><i class="fa fa-list-ol"></i></button>
                      <button class="fontStyle unorderedlist disabled" onclick="document.execCommand('insertUnorderedList',false, null)"><i class="fa fa-list-ul"></i></button> -->  
                      <input class="color-apply sp-color-picker spectrum sp-colorize" type="color" id="myColor" value="#0b0b0b"> 
                      <button onclick="insertTextAtCaret('[label](http://www)')" class="fontStyle" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ajouter une lien">
                      <i class="fa fa-link" aria-hidden="true"></i><br>
                      </button> 

                      <!-- font size start -->
                      <select id="fontSize">
                        <option value="1">1</option>      
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="14">14</option>
                        <option value="18">18</option>
                      </select>
                      <!-- font size end -->
                      
                    </fieldset>
                    
                  </div>
                
              </div>

          `);

 /*<button class="fontStyle undo-apply" onclick="document.execCommand( 'undo',false,null);"><i class="fa fa-undo">
</i>
</button>
 <button class="fontStyle redo-apply" onclick="document.execCommand( 'redo',false,null);"><i class="fa fa-repeat">
</i>
</button>*/

  $('#input-font').append(fontOptions);
      cmsBuilder.init();
      $(".closeBtn").click(function(){
        $(".sp-text").attr('contenteditable','false');      
        let $this = $(".edit-mode-textId");
        text = $this.html();
        if ($this.text() == "") {
          text = "Ecrire un texte...";
          $this.text(text);
        }

        if (spDefaultText !== text) {
          tplCtx = {};
          tplCtx.id = $this.data("id");
          tplCtx.collection = "cms";
          tplCtx.path = $this.data("field");
          tplCtx.value = text;

          dataHelper.path2Value( tplCtx, function(params) {
            if (text !== "Ecrire un texte...") {     
              toastr.success("Modification enrégistré");
            }  
          } );
        }
        $("#toolsBar").hide();
        $("#toolsBar").html("");
        // set element parent to edit mode
        $(".container<?= @$blockCms['tplParent'] ?>").addClass("edit-container<?= @$blockCms['tplParent'] ?>");
        $(".container<?= @$blockCms['tplParent'] ?>").removeClass("selected-mode-container<?= @$blockCms['tplParent'] ?>");

        $(".sp-text").removeClass('edit-mode-textId');  
      });

      $( "#myColor" ).change(function() {
       var mycolor = "";
       restoreSelection(editable, divSelection); 
       mycolor = $(".sp-preview-inner").css("background-color");
       mycolor = rgb2Hex(mycolor);
       if (mycolor.length < 6) {
        for (var i = 0; mycolor.length < 6; i++) {
          mycolor = mycolor+"0"
        }
      }
      
      setTimeout(function(){
        document.execCommand('foreColor', false, "#"+(mycolor));
      },90);
    });

    $( "#input-font" ).change(function() {
      var myFont = document.getElementById("input-font").value;
      document.execCommand('fontName', false, myFont);
   });

   $( "#fontSize" ).change(function() {
      var mysize = document.getElementById("fontSize").value;
      document.execCommand('fontSize', false, mysize);
   });

    function checkDiv(){
      var editorText = document.getElementById("itino").innerHTML;
      if(editorText === ''){
        document.getElementById("itino").style.border = '5px solid red';
      }
    }

    function removeBorder(){
      document.getElementById("itino").style.border = '1px solid transparent';
    }


     //color
     function loadColorPicker(callback) {
      // mylog.log("loadColorPicker");
      if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
        // mylog.log("loadDateTimePicker2");
        lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
          baseUrl+'/plugins/colorpicker/css/colorpicker.css',
          callback);
      }
    }
    function loadSpectrumColorPicker(callback) {
      // mylog.log("loadSpectrumColorPicker");
      lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
        baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
        callback);

    }
    var initColor = function(){
      // mylog.log("init .colorpickerInput");
      $(".colorpickerInput").ColorPicker({ 
        color : "pink",
        onSubmit: function(hsb, hex, rgb, el) {
          $(el).val(hex);
          $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
          $(this).ColorPickerSetColor(this.value);
        }
      }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
      });
    };

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if( $("#toolsBar .sp-preview").length === 0){
        $(".sp-color-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.sp-color-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });
/*    setTimeout(function(){          
     $( '.sp-choose').on('click', function() {
       var mycolor = "";
       restoreSelection(editable, divSelection); 
        mycolor = document.getElementsByTagName("sp-input").value;
       
       setTimeout(function(){    
        document.execCommand('foreColor', false, mycolor);
      },900);
     });
   },900);*/

  }
/**************End open text settings panel**************/
      
  });
});

  $('.textId-super').click(function(event){
    event.stopPropagation();
  });
  // $( ".textId-super" ).blur(function() {
  //   $(".textIdtoolbar").hide();
  // });

  $(document).on("blur", ".edit-mode-textId",function() {
    let $this = $(this);
    text = $this.html();
    if ($this.text() == "") {
      text = "Ecrire un texte...";
      $this.text(text);
    }
    // mylog.log("sptext", spDefaultText == text)
    if (spDefaultText !== text) {
    tplCtx = {};
    tplCtx.id = $(this).data("id");;
    tplCtx.collection = "cms";
    tplCtx.path = $(this).data("field");
    tplCtx.value = text;

    dataHelper.path2Value( tplCtx, function(params) {} );
    $this.removeAttr('id');
    $this.removeClass("edit-mode-textId");
    }
  });


   $(".textIdbold").on('click', function(event){
    $("#itino").toggleClass("bold");    
    // $("#textIdtextEdit").toggleClass("bold");
  });
   $(".textIditalic").on('click', function(event){ 
    $("#itino").toggleClass("italic");
    // $("#textIdtextEdit").toggleClass("italic");
  });
   $(".textIdright").on('click', function(event){
    $("#itino").toggleClass("right");
    $("#itino").removeClass("center");
    $("#itino").removeClass("left");
    $("#itino").removeClass("justify");

    // $("#textIdtextEdit").toggleClass("right");
    // $("#textIdtextEdit").removeClass("center");
    // $("#textIdtextEdit").removeClass("left");
    // $("#textIdtextEdit").removeClass("justify");
  });
   $(".textIdcenter").on('click', function(event){
    $("#itino").toggleClass("center");
    $("#itino").removeClass("right");
    $("#itino").removeClass("left");
    $("#itino").removeClass("justify");

    // $("#textIdtextEdit").toggleClass("center");
    // $("#textIdtextEdit").removeClass("right");
    // $("#textIdtextEdit").removeClass("left");
    // $("#textIdtextEdit").removeClass("justify");
  });
   $(".textIdleft").on('click', function(event){
    $("#itino").toggleClass("left");
    $("#itino").removeClass("right");
    $("#itino").removeClass("center");
    $("#itino").removeClass("justify");

    // $("#textIdtextEdit").toggleClass("left");
    // $("#textIdtextEdit").removeClass("right");
    // $("#textIdtextEdit").removeClass("center");
    // $("#textIdtextEdit").removeClass("justify");
  });
   $(".textIdjustify").on('click', function(event){
    $("#itino").toggleClass("justify");
    $("#itino").removeClass("right");
    $("#itino").removeClass("center");
    $("#itino").removeClass("left");

    // $("#textIdtextEdit").toggleClass("justify");
    // $("#textIdtextEdit").removeClass("right");
    // $("#textIdtextEdit").removeClass("center");
    // $("#textIdtextEdit").removeClass("left");
  });
 <?php }else{ ?>

   setTimeout(function() {
    convAndCheckLink(".sp-text");
  }, 100);
<?php } ?>

$(".sp-image").click(function(){
if (localStorage.getItem("previewMode") == "w") {   

    last_edited = $(this);

    last_edited.addClass("sp-img-selected");
    $("#toolsBar").show();    
    /*superCmsilay_sary.viewMode();*/
    $("#toolsBar").html(`
         <div class="container-fluid">
    <div class="text-center">
      <i style="cursor:default;font-size: large;" aria-hidden="true"><?php echo Yii::t("cms", "Image settings")?></i>
      <i class="fa fa-window-close closeBtn" aria-hidden="true"></i>
    </div>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home"><?php echo Yii::t("cms", "General")?></a></li>
      <li><a data-toggle="tab" href="#menu1"><?php echo Yii::t("cms", "Shadow")?></a></li>
      <li><a data-toggle="tab" href="#menu2"><?php echo Yii::t("cms", "Border")?></a></li>
    </ul>

    <div class="tab-content padding-top-20">
      <div id="home" class="tab-pane fade in active">
        <div class="row">
          <div class="col-md-6">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    <label><?php echo Yii::t("cms", "Edit photo")?></label>
                    <div class="input-group">
                      <div class="input-group-btn closeMenu" style="width: 0px">                    
                        <button class="edit-imgParams tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("cms", "Edit photo")?>">
                          <i class="fa fa-camera" aria-hidden="true"></i><br>
                        </button> 
                      </div>
                    </div>
                    <hr>
                    <label><?php echo Yii::t("cms", "Background color")?></label>
                    <div class="input-group">
                      <input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="transparent" placeholder="transparent" style="">
                      <div class="input-group-btn closeMenu" style="width: 0px">
                      </div>
                    </div>
                  </div>  
                </div>  
              </div>
            </div>
          </div>

          <div class="col-md-6">      
            <div class="panel-group">
              <label><?php echo Yii::t("cms", "Adjustment of the photo")?></label>
                <select class="form-control" id="objfit">
                  <option value="contain"><?php echo Yii::t("cms", "Contain")?></option>
                  <option value="cover"><?php echo Yii::t("cms", "Cover")?></option>
                  <option value="fill"><?php echo Yii::t("cms", "Fill")?></option>
                  <option value="inherit"><?php echo Yii::t("cms", "Inherit")?></option>
                  <option value="initial"><?php echo Yii::t("cms", "Initial")?></option>
                  <option value="none"><?php echo Yii::t("cms", "None")?></option>
                  <option value="revert"><?php echo Yii::t("cms", "Revert")?></option>
                  <option value="scale-down"><?php echo Yii::t("cms", "Scale-down")?></option>
                  <option value="unset"><?php echo Yii::t("cms", "Unset")?></option>
                </select>
            </div>
          </div>
          
          <div class="col-md-12">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    
                  </div>  
                </div>  
              </div>
            </div>
          </div> 

        </div>  
      </div>

      <div id="menu1" class="tab-pane fade">
        <div class="col-md-12">      
          <div class="panel-group">        
            <label><?php echo Yii::t("common", "Color")?></label>
            <div class="pull-right">
            <input id="check-inset" type="checkbox" >
            <label for="check1"><?php echo Yii::t("cms", "Interior")?></label>
            <p></p>
            </div>
            <input id="shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="transparent" placeholder="transparent" style="">
              <div class="range-slider text-center padding-top-20">
                <span class="range-blur__value"><?php echo Yii::t("cms", "Blur")?> </span>
                <input class="range-slider-blur super-cms-range-slider" type="range" value="0" min="0" max="20" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-spread__value"><?php echo Yii::t("cms", "Spread")?> </span>
                <input class="range-slider-spread super-cms-range-slider" type="range" value="0" min="0" max="20" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-x__value"><?php echo Yii::t("cms", "Axis")?> X: </span>
                <input class="range-slider-x super-cms-range-slider" type="range" value="" min="-10" max="10" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-y__value"><?php echo Yii::t("cms", "Axis")?> Y: </span>
                <input class="range-slider-y super-cms-range-slider" type="range" value="" min="-10" max="10" step="0.2">
              </div>
            </div>
          </div>
        </div>

      <div id="menu2" class="tab-pane fade">
        <div class="col-md-12">      
          <div class="col-md-6">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    <label><?php echo Yii::t("cms", "Border color")?></label>
                    <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="transparent" placeholder="transparent" style="">
                    
                  </div>  
                </div>  
              </div>
            </div>
          </div>

          <div class="col-md-6">      
            <div class="panel-group">
              <label><?php echo Yii::t("cms", "Border radius")?></label>
                <div class="input-group">
                  <input class="form-control elem-control" type="number" min="0" value="" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"> 
                  <div class="input-group-btn" style="width: 0px">                    
                    <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("cms", "More settings")?>">
                      <i class="fa fa-plus" aria-hidden="true"></i><br>
                    </button> 
                  </div>
                </div>
                <div id="border" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field"> 
                    <label><?php echo Yii::t("cms", "Top left")?></label>
                    <input class="form-control top-left border-control" type="number" min="0" value="" placeholder="0" data-path="border.radius" data-given="border-top-left-radius"> 
                  </div>                  
                  <div class="element-field">      
                    <label><?php echo Yii::t("cms", "Top right")?></label>
                    <input class="form-control top-right border-control" type="number" min="0" value="" placeholder="0" data-path="border.radius" data-given="border-top-right-radius"> 
                  </div>
                   <div class="element-field"> 
                    <label><?php echo Yii::t("cms", "Bottom left")?></label>
                    <input class="form-control bottom-left border-control" type="number" min="0" value="" data-path="border.radius" data-given="border-bottom-left-radius" placeholder="0"> 
                  </div>                  
                  <div class="element-field">      
                    <label><?php echo Yii::t("cms", "Bottom right")?></label>
                    <input class="form-control bottom-right border-control" type="number" min="0" value="" data-path="border.radius" data-given="border-bottom-right-radius" placeholder="0"> 
                  </div>
                </div>
              </div>

              <label><?php echo Yii::t("cms", "Padding")?>/label>
              <div class="input-group">
              <input class="form-control elem-control" type="number" min="0" value="" placeholder="0" data-path="padding" data-given="padding" data-what="padding-control"> 
                <div class="input-group-btn" style="width: 0px">                           
                  <button  data-toggle="collapse" data-target="#padding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("cms", "More settings")?>">
                    <i class="fa fa-plus" aria-hidden="true"></i><br>
                  </button> 
                </div>
              </div>
              <div id="padding" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field" style="width:100px"> 
                    <label><?php echo Yii::t("cms", "Top")?></label>
                    <input class="form-control top-left padding-control" type="number" min="0" value="" placeholder="0" data-path="padding" data-given="padding-top"> 
                  </div>                  
                  <div class="element-field" style="width:100px">      
                    <label><?php echo Yii::t("cms", "Right")?></label>
                    <input class="form-control top-right padding-control" type="number" min="0" value="" placeholder="0" data-path="padding" data-given="padding-right"> 
                  </div>
                   <div class="element-field" style="width:100px"> 
                    <label><?php echo Yii::t("cms", "Left")?></label>
                    <input class="form-control bottom-left padding-control" type="number" min="0" value="" data-path="padding" data-given="padding-left" placeholder="0"> 
                  </div>                  
                  <div class="element-field" style="width:100px">      
                    <label><?php echo Yii::t("cms", "Bottom")?></label>
                    <input class="form-control bottom-right padding-control" type="number" min="0" value="" data-path="padding" data-given="padding-bottom" placeholder="0"> 
                  </div>
                </div>
              </div> 

            </div>
          </div>
        </div>
    </div> 
  </div> 
    `);     

    cmsBuilder.init();
      $(".closeBtn, .closeMenu").click(function(){      
       /*superCmsilay_sary.editMode();*/
        $("#toolsBar").html(``);
        $("#toolsBar").hide();
        last_edited.removeClass("sp-img-selected");
      });

  /**********Image settings Color*************/

    /*********Color*********/

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if(  $(".sp-color-picker").length){
        $(".sp-color-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.sp-color-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });

    $( "#fond-color" ).change(function() {
      last_edited.css("background-color",$("#fond-color").val());   
      tplCtx = {};
      tplCtx.id = "iddddd";
      /*tplCtx.collection = "cms";*/
      tplCtx.path = "css.background.color"; 
      tplCtx.value = $("#fond-color").val();

      dataHelper.path2Value( tplCtx, function(params) {} );
    });

    $( "#border-color" ).change(function() {
     last_edited.css("border-color",$("#border-color").val());
     tplCtx = {};
     tplCtx.id = "iddddd";
     // tplCtx.collection = "cms";
     tplCtx.path = "css.border.color"; 
     tplCtx.value = $("#border-color").val();

     dataHelper.path2Value( tplCtx, function(params) {} );
   });

   /*********End color*********/

  /************Border**padding***margin*************/
   var thisetting = {};
   
   $(".border-control" ).on('keyup change', function (){
    $('.elem-value .border-control').each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      last_edited.css(thisIs,pars+"px");
      last_edited.css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });
    $(".padding-control" ).on('keyup change', function (){
    $('.elem-value .padding-control').each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      last_edited.css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });
  //   $(".margin-control" ).on('keyup change', function (){
  //   $('.elem-value .margin-control').each(function(k,v){
  //     pars   = $(this).val() ? parseInt($(this).val()) : '0';
  //     thisIs = $(this).data("given");
  //     $(".ilay_sary" ).css(thisIs,pars+"px");
  //     $(this).val(pars);
  //     thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
  //   })
  // });


   $(".elem-control" ).on('keyup change', function (){
      globalConf = $(this).val() ? parseInt($(this).val()) : '0';
      $("."+$(this).data("what")).val(globalConf);
      if ($(this).data("given") != "padding") {
        last_edited.css($(this).data("given"),globalConf+"px");
      }
      $('.elem-value .'+$(this).data("what")).each(function(k,v){
        pars   = $(this).val() ? parseInt($(this).val()) : '0';
        thisIs = $(this).data("given");
        last_edited.css(thisIs,pars+"px");
        $(this).val(pars);
        thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
      })
    });

   $( ".elem-control, .elem-value .form-control" ).blur(function() {
    tplCtx = {};
    tplCtx.id = "idd";
    // tplCtx.collection = "cms";
    tplCtx.path = "css."+$(this).data("path"); 
    tplCtx.value = thisetting;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

   /************End border**padding***margin***********/

    /***object-fit***/
   $( "#objfit" ).val("");
   $( "#objfit" ).change(function() {
    var myfit = $( "#objfit" ).val();
    last_edited.css("object-fit",myfit);
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "css.object-fit"; 
      tplCtx.value = myfit;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
   /***End object-fit***/

   /*Box shadow*/
     var eltBlur = $('.range-slider-blur').val();
     var eltSpread = $('.range-slider-spread').val();
     var eltAxeX = $('.range-slider-x').val();
     var eltAxeY = $('.range-slider-y').val();
     var eltColor = $("#shw-color").val();
     var inset = "";

     $( "#check-inset" ).change(function() {
      if ($( "#check-inset" ).is( ":checked" ) == true) {      
        inset = "inset";
      }else{
        inset = "";
      }
      last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
       saveShadowilay_sary();
    });

     $( "#shw-color" ).change(function() {
      eltColor = $("#shw-color").val();
      last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      saveShadowilay_sary();
    });
     $('.range-slider').mousedown(function(){
       $('.range-slider-blur').mousemove(function(){
        eltBlur = $('.range-slider-blur').val();
        $(".range-blur__value").html("Blur "+eltBlur+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
       $('.range-slider-spread').mousemove(function(){
        eltSpread = $('.range-slider-spread').val();
        $(".range-spread__value").html("Spread "+eltSpread+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
       $('.range-slider-x').mousemove(function(){
        eltAxeX = $('.range-slider-x').val();
        $(".range-x__value").html("Axe X "+eltAxeX+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });  
       $('.range-slider-y').mousemove(function(){
        eltAxeY = $('.range-slider-y').val();
        $(".range-y__value").html("Axe Y "+eltAxeY+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
     });
     $('.range-slider').mouseup(function(){
      /*saveShadowilay_sary();*/
     });
     function saveShadowilay_sary(){
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "css.box-shadow"; 
      tplCtx.value = {};
      tplCtx.value.inset =  inset;
      tplCtx.value.color =  eltColor;
      tplCtx.value.x     =  eltAxeX;
      tplCtx.value.y     =  eltAxeY;
      tplCtx.value.blur  =  eltBlur;
      tplCtx.value.spread =  eltSpread;

      dataHelper.path2Value( tplCtx, function(params) {} );
    }
   /*End box shadow*/

   /****************Other class option****************/

    $(".other-class" ).on('keyup', function (){
     last_edited.removeClassExcept("super-container ilay_sary super-cms other-css-ilay_sary newCss-ilay_sary");
     last_edited.addClass($(".other-class" ).val());
    });
    $(".other-class" ).on('blur', function (){
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "class.other"; 
      tplCtx.value = $(".other-class" ).val();

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
  /*************End other class option****************/

  /****************Other css option****************/
   var css_pro_and_val = {};
    $(".other-css" ).on('keyup', function (){
      //Validate and apply css
      last_edited.removeClass("other-css-ilay_sary"); 
      last_edited.removeAttr("style");
     var other_css = $(".other-css" ).val()
     var array_val = other_css.split(/[;/^\s*(\n)\s*$/]+/);
     $.each(array_val, function(k,val) {
    /* css validation
     if(val==""  || val == " " || val == /^\s*(\n)\s*$/){
        // $(".css_err").text(css_propertyname+" insupporté!"); 
      }else{   */   
       var css_propertyname = val.split(':')[0];  
       var css_value = val.substr(val.lastIndexOf(":") + 1);
       if(CSS.supports(css_propertyname, css_value)){;
         if(css_propertyname != css_value){
         css_pro_and_val[css_propertyname] = css_value;
         }
       }
 /*    }*/
     });
     // let para = document.querySelector($(".ilay_sary" ));
     // let compStyles = window.getComputedStyle(para);
     // console.log(compStyles);
     $(".ilay_sary" ).css(css_pro_and_val);
    });

    $(".other-css" ).on('blur', function (){
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "css.other"; 
      tplCtx.value = css_pro_and_val;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
  /*************End other css option****************/

    $(".stop-propagation").click(function(e) {
      e.stopPropagation();
    });

    /* Upload d'image */
      jQuery(document).ready(function() {
          sectionDyf.ilayasarParams = {
            "jsonSchema" : {    
              "title" : "<?php echo Yii::t("cms", "Image import")?>",
              "description" : "Personnaliser votre bloc",
              "icon" : "fa-cog",
              
              "properties" : {    
                "image" :{
                 "inputType" : "uploader",
                 "label" : "image",
                 "docType": "image",
                 "contentKey" : "slider",
                 "itemLimit" : 1,
                 "endPoint": "/subKey/block",
                 "domElement" : "image",
                 "filetypes": ["jpeg", "jpg", "gif", "png"],
                 "label": "Image :",
                 "showUploadBtn": false,
                 initList : "echo json_encode($initImage)"
                },
              },
              beforeBuild : function(){
                  uploadObj.set("cms","iddd");
              },
              save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.ilaysarParams.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                  if (k == "parent")
                    tplCtx.value[k] = formData.parent;

                  if(k == "items")
                    tplCtx.value[k] = data.items;
                });
                
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                      dyFObj.commonAfterSave(params,function(){
                        toastr.success("<?php echo Yii::t("cms", "Element well added")?>");
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                      });
                    } );
                }

              }
            }
          };

          $(".edit-imgilaysarParams").off().on("click",function() {  
            tplCtx.subKey = "imgParent";
            tplCtx.id = "iddd";
            // tplCtx.collection = "cms";
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.imgilaysarParams,null, null);
          });
        });

/* End upload d'image */
  }
});

/***************END Admin panel*****************/
/*End document ready*/
</script>
