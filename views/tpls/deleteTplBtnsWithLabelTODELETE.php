<?php if($canEdit){ ?>
 	<a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-path='<?= @$path ?>' data-id='<?= @$id; ?>' data-collection='cms'>
 		<i class='fa fa-trash'></i> <?php echo @$label ?>
 	</a>
<?php }?>

