<?php 
    /**
     * TPLS QUI PERMET AFFICHAGE DES NEWS
     */
    $keyTpl = "appSlideapp";

    $defaultcolor = "white";
    $tags = "structags";

    $paramsData = [ "need"          =>   false,
                    "offer"         =>   false,
                    "project"      =>   false,
                    "event"        =>   false,
                    "organization" =>   false
                    ];

if( isset($this->costum["tpls"][$keyTpl]) ) {
         foreach($paramsData as $i => $v) {
            if( isset($this->costum["tpls"][$keyTpl][$i]) ) 
                $paramsData[$i] =  $this->costum["tpls"][$keyTpl][$i];      
        }
    }
?>
<!-- On le fait en dure --> 
<div id="slideApp" class="carousel slide row no-padding" data-ride="carousel">
    <div id="content-results-slideApp" class="carousel-inner text-center">
    </div>
    <div class="col-md-12" style="margin-top: 5%;">
       <ol class="carousel-indicators" class="indicators-slideApp"> </ol>
       </div>
</div>
<div class="text-center arrow hidden" id="arrowSlideApp" style="font-size: 3rem; color: #abb76b;">
    <a class="left a-caroussel" href="#slideApp" data-slide="prev">  <i class="fa fa-arrow-left" aria-hidden="true"></i>
    </a>
    <a class="right a-caroussel" href="#slideApp" data-slide="next"><i class="fa fa-arrow-right" aria-hidden="true"></i>
    </a>
</div>

<?php  
    echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "keyTpl"=>$keyTpl]);
?>


<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section actualité",
            "description" : "Personnaliser votre section sur l'actualité",
            "icon" : "fa-cog",
            "properties" : {
                need : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer la demande",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                "offLabel" : "Non",
                                "labelText" : "Activer la demande"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.need
                        },
                 offer : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer l'offre",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                "offLabel" : "Non",
                                "labelText" : "Activer l'offre"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.offer
                        },
                 project : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer les projects",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                "offLabel" : "Non",
                                "labelText" : "Activer les projects"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.project
                        },
                event : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer les évènements",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                "offLabel" : "Non",
                                "labelText" : "Activer les évènements"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.event
                        },
                organization : {
                            "inputType" : "checkboxSimple",
                            "label" : "Activer l'organisation",
                            "params" : {
                                "onText" : "Oui",
                                "offText" : "Non",
                                "onLabel" : "Oui",
                                "offLabel" : "Non",
                                "labelText" : "Activer l'organisation"
                            },
                            "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.organization
                        }
            },
            save : function () {  
                tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                        
                 });
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

    // Affichage

    var html = "";
    var li = "";
    var y= 0;
    var i= 0;

    html += "<div class='item active'>";
    li += "<li data-target='#projectsCarousel' data-slide-to='"+y+"' class='active'></li>";

    $.each(<?php echo json_encode( $paramsData ); ?>, function(k,v){
        console.log("la clé " + k + " a pour valeur " + v + "de type " + typeof(v));
        if(v == "true"){
            console.log("je rentre bien ici");
            if(i >= 2){
                html += "</div>";
                html += "<div class='item'>";
                y++;
                li += "<li data-target='#projectsCarousel' data-slide-to='"+y+"'></li>";
                i = 0;
                $("#arrowSlideApp").removeClass("hidden");
            }
            i++;

            html += "<div class='card-info col-md-6'>";
            html += "<a href='javascript:;' onclick='dyFObj.openForm("+k+")' class='btn btn-success btn-lg'>Proposer une demande</a>";
            html += "</div>";
        }
    });

        $("#content-results-slideApp").html(html);
        $("#indicators-slideApp").html(li);
});
</script>