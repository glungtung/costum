<style type="text/css">
  .showslide:hover{
    color:<?php echo @$eltcolor ?>
  }

  .carousel-indicators{
    position: relative !important;
    bottom: 7px !important;
  }
</style>
<!-- <div class="col-md-12"> -->
  <div id="carousel-autre" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
    <?php 
    //todo pour le moment on interprete tout le spoi cms 
    //TODO : filtrer par section 
    foreach ($cmsList as $key => $value) {
      if( strpos( @$value["structags"], $blockName ) !== false )
      {
        $hide = ($blockCt == 1) ? "item active" : "item" ;
        echo '<div id="cms'.$key.'" class=" '.$hide.' ">';
          $params = array( 
              "cmsList" => $cmsList,
              "tag"=> $blockName.$blockCt,
              "color" => @$titlecolor,
              "eltcolor" => @$eltcolor
          );

          $blockType = (isset($blockTpl)) ? $blockTpl : "textWithImg" ;
          echo "<center>";
            echo $this->renderPartial("costum.views.tpls.".$blockType ,$params,true);
          echo "</center>";
        echo '</div> ';
        $blockCt++;
      }
    }?>

    </div>
    <?php
    echo '<div id="carousel-point" class="carousel slide" data-ride="carousel">';
      echo '<ol class="carousel-indicators">';
        for ($i=0; $i < $blockCt-1; $i++) { 
          if ($i == 0) {
             echo '<li data-target="#carousel-autre" data-slide-to="'.$i.'" class="carousel-pagination active"></li>';
          }else{
            echo '<li data-target="#carousel-autre" data-slide-to="'.$i.'" class="carousel-pagination"></li>';
          }  
        }
      echo '</ol>';
    echo '</div>';
    ?>
    <div>
      <!-- Left and right controls -->
      <a style="color:<?php echo @$eltcolor ?>;font-size:28px;" class="left carousel-control" href="#carousel-autre" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left icon-control" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a style="color:<?php echo @$eltcolor ?>;font-size:28px;" class="right carousel-control" href="#carousel-autre" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right icon-control" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
  </div>
</div>

<div class="col-md-12">
<?php


$params = array(
	"cmsList" => $cmsList,
	"tag"=>$blockName.$blockCt,//"cc".$blockCt,
	"color" => "#e6344d"
);
//var_dump($params);
echo "<center style='margin-top:2%;'>";
  echo $this->renderPartial("costum.views.tpls.blockCms.textImg.text",$params,true);
echo "<center>";
                          
 ?>
</div>
 <script type="text/javascript">
 jQuery(document).ready(function() {
 	mylog.log("render","costum.views.tpls.multiblocksCaroussel");

  jQuery('.carousel[data-type="multi"] .item').each(function(){
    var next = jQuery(this).next();
    if (!next.length) {
      next = jQuery(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo(jQuery(this));
    
    for (var i=0;i<2;i++) {
      next=next.next();
      if (!next.length) {
        next = jQuery(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
    }
  });
 });
 </script>