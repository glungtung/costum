<?php 
$keyTpl = "css";

     $paramsData = [ "menuTop"                => "",
                     "loader"                 => "",
                     "menuApp"                => "",
                     "progress"               => "",
                     "button"                 => ""];

        if( isset($this->costum[$keyTpl]) ) {
        foreach ($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];      
            }
        }

?>
<?php
if($canEdit){
    ?>
<a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' data-id='<?= $this->costum["contextId"]; ?>' data-collection='<?= $this->costum["contextType"]; ?>' data-key='<?php echo $keyTpl ?>' data-path='costum.<?= $keyTpl ?>'>
           simple CSS <i class="fa fa-eyedropper" aria-hidden="true"></i>
</a>
<?php } 

//var_dump($paramsData);
?>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
jQuery(document).ready(function() {

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configuration du css",
            "description" : "Modifier l'apparence de votre page en modifiant son css",
            "icon" : "fa-cog",
            "properties" : {
                "menuTop][background]" : {
                    "inputType" : "colorpicker",
                    "label" : "Menu Top",
                    "value" : (
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.background != "undefined") ? sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.background : ""
                },
                "menuTop][app][button][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur app menuTop",
                     value :(
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app !="undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app.button != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app.button.color != "undefined") ? sectionDyf.<?= $keyTpl ?>ParamsData.menuTop.app.button.color : ""

                },
                "menuTop][app][button][fontSize]" : {
                    "inputType" : "text",
                    "label" : "Taille des app menuTop",
                    "placeholder" : "exemple : 20",
                    value :(
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app !="undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app.button != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app.button.fontSize!= "undefined") ? sectionDyf.<?= $keyTpl ?>ParamsData.menuTop.app.button.fontSize : ""
                },
                "menuTop][button][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Color du dropdown menu a droite",
                    value :(
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop != "undefined" 
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app !="undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app.button != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuTop.app.button.color != "undefined") ? sectionDyf.<?= $keyTpl ?>ParamsData.menuTop.app.button.color : ""
                },
                "loader][ring1][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Loader ring 1",
                    "value" :  (
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.loader != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring1 != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring1.color != "undefined") ? sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring1.color :""
                },
                "loader][ring2][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Loader ring 2",
                    "value" : (
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.loader != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring2 != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring2.color != "undefined")? sectionDyf.<?php echo $keyTpl ?>ParamsData.loader.ring2.color:""
                },
                "menuApp][background]": {
                    "inputType" : "colorpicker",
                    "label" : "Menu App",
                    "value" : (
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuApp != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuApp.background != "undefined" ) ? sectionDyf.<?= $keyTpl ?>ParamsData.menuApp.background :""
                },
                "menuApp][button][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Menu App Button",
                    "value" : (
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuApp != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuApp.button != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuApp.button.color != "undefined"
                        ) ? sectionDyf.<?= $keyTpl ?>ParamsData.menuApp.button.color :""
                },
                "button][footer][add][color]" : {
                    "inputType" : "colorpicker",
                    "label" : "Button Footer Add",
                    "value" :  (
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button.footer != "undefined"
                        &&  typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button.footer.add != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button.footer.add.color != "undefined"
                        ) ? sectionDyf.<?= $keyTpl ?>ParamsData.button.footer.add.color :""
                },
                "button][footer][add][background]" : {
                    "inputType" : "colorpicker",
                    "label" : "Background Button Footer Add",
                    "value" :(
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button.footer != "undefined"
                        &&  typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button.footer.add != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.button.footer.add.background != "undefined"
                        ) ?  sectionDyf.<?= $keyTpl ?>ParamsData.button.footer.add.background :""
                },
                "progress][value][background]": {
                    "inputType" : "colorpicker",
                    "label" : "Progress",
                    "value" :(
                        typeof sectionDyf.<?php echo $keyTpl ?>ParamsData != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.progress != "undefined"
                        && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.progress.value != "undefined"
                        &&  typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.progress.value.background != "undefined"  )? sectionDyf.<?= $keyTpl ?>ParamsData.progress.value.background :""
                }
            },
            save : function () { 
                // console.log(document.body.getElementsByTagName("input"));
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(k.indexOf("[") && k.indexOf("]"))                
                        kt = k.split("[").join("\\[").split("]").join("\\]");
                    tplCtx.value[k] = $("#"+kt).val();
                 });
                console.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined"){
                    
                    toastr.error('value cannot be empty!');
                }else {
                    console.log('icicunfe');
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }
                
                

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>