<?php
$defaultcolor = "black";
$structags = "structags";
$keyTpl = "wizardCaroussel";

$paramsData = [ "title" => "",
                "icon"  =>  "",
                "color" => "black",
                "background" => "",
                "nbStep"    =>  3,
                "tags" => "structags"
                ];
$listStep = array();

$p = null;

if( isset($this->costum["tpls"][$keyTpl]) ) {
    foreach ($paramsData as $i => $v) {
        if( isset($this->costum["tpls"][$keyTpl][$i]) ) 
            $paramsData[$i] =  $this->costum["tpls"][$keyTpl][$i];      
    }
}
// $i = 1;
// $y = 0;
// $z= 1;
// while($i < 8){
//     $z++;
    // $i++;
//     if($y < 3){
        // $listStep = Cms::getCmsByStruct($cmsList, "stepCaroussel".$i,$structags );
        // $y++;
        // if($y > 3) $y=0;
    // }
    // echo $z; echo $y;
// }
// var_dump($listStep);exit;
?>

<div id="wizardCaroussel" class="col-xs-12" style="margin-top: 2%;">
    <div id="carousel"  class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        <?php
        foreach ($listStep as $k => $v) {
            if($k % 3 == 0) echo '<div class="item">';
            if( count(Cms::getCmsByStruct($cmsList, "stepCaroussel".$i,$structags ) ) != 0 ) { 
                    $p = Cms::getCmsByStruct($cmsList, "stepCaroussel".$i,$structags )[0];
                ?>
                    <h1><?= @$p["name"] ?></h1>
                    <p><?= @$p["description"] ?></p>
            <?php
            $edit ="update";
            }
            else
            {
                ?>
                        TEXT TODO <br/>
                        as POI type cms + tag : stepCaroussel<?php echo $v ?>
                        <?php  
                        $edit ="create";
            }
            echo $this->renderPartial("costum.views.tpls.openFormBtn",
            array(
                'edit' => $edit,
                'tag' => 'stepCaroussel'.$v,
                'id' => (string)@$p["_id"]
             ),true);

             if($k % 3 == 0) '</div>';
        }
            echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
        ?>
        </div>
        <a class="left carousel-control" href="#carousel" data-slide="prev">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel" data-slide="next">
            <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
  		</a>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.wizard");
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
});
        
        //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    function showStep(id){
    $(".sectionStep").addClass("hide");
    $(id).removeClass("hide");    
}
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });

    });
    </script>
