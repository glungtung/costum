<style type="text/css">
    #modal-preview-forum{
        position: fixed;
        top: 0px;
        bottom: 0px;
        left: 40%;
        right: 0px;
        background-color: white;
        z-index: 100000;
        display:none;
        overflow-y: scroll;
        padding: 10px;
    }
    #modal-preview-forum hr{
        width: 100%;
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        float: left;
    }
    .access-stream-forum{
        border: none;
    border-radius: 4px;
    margin-top: 10px;
    padding: 5px;
    font-weight: 800;
    }
</style>
<?php 
$structField = "structags";
$edit ="create";
$noBtn = false;
if( count(Cms::getCmsByStruct($cmsList,$tag,$structField))!=0 )
{   
    $p = Cms::getCmsByStruct($cmsList,$tag,$structField)[0];
    $el = Slug::getElementBySlug($p["name"]);
    // echo $tag;
    //  echo $el["type"];
    echo "<div style='height:240px;overflow:hidden;'>";    
        if(isset($p["profilMediumImageUrl"])){
            echo "<div style='height:150px;overflow:hidden;'>";            
            echo "<img style='margin:auto; width:100%;' src='".Yii::app()->createUrl($p["profilMediumImageUrl"])."' class='img-responsive'/>";
            echo "</div>";
        }
        $slugHref=($el["type"] == Person::COLLECTION) ? @$el["el"]["username"] : $el["el"]["slug"];
        if(isset($typeApp) && $typeApp=="forum")
            echo "<a href='#page.type.poi.id.".@(string)$p["_id"]."' class='lbh-preview-element' data-id='".@(string)$p["_id"]."' data-type='".@$p["type"]."' style='font-size: 1.9rem;font-weight: bold;'>".@$p["name"]."</a>";
        else
            echo "<a target='_blank' href='#@".$slugHref."' style='font-size: 1.9rem;font-weight: bold;'>".@$el["el"]["name"]."</a>";
        if(isset($p["description"]) && $p["description"])
            echo "<div style='font-size: 1.6rem;'>".$p["description"]."</div>";
        if(isset($typeApp) && $typeApp=="forum"){
            echo "<a href='#page.type.poi.id.".@(string)$p["_id"]."' class='btn lbh-preview-element bg-dark col-xs-12' data-id='".@(string)$p["_id"]."' data-type='".@$p["type"]."'><i class='fa fa-question-circle'></i> Poser une question</button>";
            echo '<div id="modal-preview-forum" class="shadow2"><button class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </a><hr/><div class="content-live col-xs-12"></div></div>';
        }
    echo "</div>";  
    echo "<div style='height:20px;' class='text-center'>";    
        if( !$noBtn )
            echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                        array(
                                            'edit' => "update",
                                            'tag' => $tag,
                                            'id' => (isset($p["_id"])) ? (string)$p["_id"] : null
                                         ),true);
    echo "</div>";    
} 
?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".access-stream-forum").off().on("click", function(){
            $("#modal-preview-forum").show(200);
            $("#modal-preview-forum .content-live").html(coInterface.showLoader());
            ajaxPost('#modal-preview-forum .content-live', baseUrl+"/news/co/index/type/"+$(this).data("type")+"/id/"+$(this).data("id"), 
                {nbCol:1, inline : true, scrollDom: "#modal-preview-forum"},
                function(){ //$("#modal-preview-coop").removeClass("hidden").show();
                    $("#modal-preview-forum .btn-close-preview").click(function(){
                        $("#modal-preview-forum").hide(200); 
                        $("#modal-preview-forum .content-live").html("");
                    });
                    urlCtrl.bindModalPreview();
                },"html");
            /*$.ajax({
                type: "POST",
                url: urlPreview,
                data: {"preview" : true},
                success: function(html){
                    $("#modal-preview-coop").html(html);
                    $("#modal-preview-coop").removeClass("hidden").show();
                    urlCtrl.bindModalPreview();
                }
            });*/
        });
    });
</script>