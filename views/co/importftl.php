<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
    *{
        margin: 0;
        padding: 0;
    }

    .container{
        padding-top: 100px;
    }

    .input-file-container{
        width: 100%;
        height: 200px;
        background-color:rgb(236, 236, 236);
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        font-size: 20px;
    }

    .imported-files{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .imported-files i{
        font-size: 40px;
        color: darkgreen;
    }
    .imported-files p:nth-child(2){
        font-size: 22px;
        margin: 5px 0px;
    }

    .input-file-container .input-file-info, .input-file-container.active .imported-files{
        display: flex;
    }
    .input-file-container .imported-files, .input-file-container.active .input-file-info{
        display: none;
    }

    #form-nbrows{
        display: none;
    }
</style>
<div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="checkbox">
                    <label><input type="checkbox" id="checkbox-nbrows" value="">Définir le nomber des lignes à importer</label>
                </div>
                <div class="form-group" id="form-nbrows">
                    <input type="number" class="form-control">
                </div>
            </div>
        </div>
        <div class="input-file-container">
            <p class="input-file-info"><span style="margin-right: 10px;"><i class="fa fa-upload" aria-hidden="true"></i></span> Choisier un ou des fichiers excel</p>
            <div class="imported-files">
                <p><i class="fa fa-file-excel-o" aria-hidden="true"></i></p>
                <p class="files-number">4 fichier(s) importé(s)</p>
                <div style="display: flex;">
                    <button class="btn btn-success  btn-import" style="margin-right: 10px;">Importer</button>
                    <button class="btn btn-danger btn-cancel-import">Annuler</button>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-sm-6">
                <div class="progress-log" style="font-size: 20px; font-weight:bold;">
                    <p class="progress-log-row"></p>
                    <p class="progress-log-step"></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="error-log" style="color:red;max-height:400px; overflow-y:auto;">
        
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.2/xlsx.full.min.js"></script>
<script>
    var cancelImport = false;
    var nbrows = null;
    function formatRowData(row){
        var inputs = [];
        Object.keys(row).forEach(function(key){
            if(/\[[\s\S]+\]/.test(key)){
                var inputId = /\[[\s\S]+\]/.exec(key)[0].replace("[","").replace("]","");
                inputs.push({
                    id:inputId,
                    value:row[key]
                })
            }
        })
        return inputs;
    }

    function extractStepIdFromFile(file){
        var fileNameWithoutExt = file.name.replace(/\.[^/.]+$/, ""),
            fileNameParts = fileNameWithoutExt.split("-");

        return fileNameParts.reverse()[0]
    }

    function excelToJson(file, callback){
        var reader = new FileReader();
        reader.readAsBinaryString(file);
        reader.onload = function(e){
            var data = e.target.result;
            var workbook = XLSX.read(data, {type:"binary"})

            var rowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[workbook.SheetNames[0]]);
            callback(rowObject)
        }
    }

    function filesToJson(files, callback){
        //transform object to array representation
        var files = Object.keys(files).map(function(key){
            return files[key];
        })

        var counter = 0, res = [];
        var action = function(){
            if(counter < files.length){
                excelToJson(files[counter], function(data){
                    res[extractStepIdFromFile(files[counter])] = data;
                    counter++;
                    action();
                })
            }else{
                callback(res)
            }
        }

        action()
    }

    function groupStepsByRows(data){
        var res = [];
        Object.keys(data).forEach(function(stepId){
            data[stepId].forEach(function(row, index){
                if(!res[index])
                    res[index] = [];

                res[index][stepId] = formatRowData(row);
            })
        })
        return res;
    }

    function processRows(rows, callback, onProgress, onError){
        var counter = 0;
        var action = function(){
            if(!cancelImport){
                if(counter < rows.length){
                    if(onProgress){
                        onProgress("row", {
                            current:counter+1,
                            total:rows.length,
                            subject:rows[counter]
                        })
                    }
                    processSteps(rows[counter], function(){
                        counter++;
                        action();
                    }, function(state){
                        onProgress("step", state)
                    }, function(error){
                        error.row = {
                            current:counter+1,
                            total:rows.length,
                            subject:rows[counter]
                        }
                        onError(error)
                    })
                }else{
                    callback()
                }
            }  
        }

        action()
    }

    function processSteps(steps, callback, onProgress, onError){
        //transform steps to arrray
        steps = Object.keys(steps).map(function(stepId){
            return {
                stepId:stepId,
                inputs:steps[stepId]
            }
        })

        var counter = 0, sameAsLastRow = false;
        var action = function(){
            if(!cancelImport){
                if(counter < steps.length){
                    if(onProgress){
                        onProgress({
                            current:counter+1, 
                            total:steps.length, 
                            subject:steps[counter]
                        })
                    }
                    var dataToSend = steps[counter];
                    dataToSend.sameAsLastRow = sameAsLastRow;
                    sameAsLastRow = true;
                    $.post("/costum/co/importftl", dataToSend, function(data){
                        if(data.error)
                            onError({
                                message:data.message,
                                step:{
                                    current:counter+1, 
                                    total:steps.length, 
                                    subject:steps[counter]
                                }
                            })
                        counter++;
                        action();
                    })
                }else{
                    callback()
                }
            }
        }

        action()
    }

    function processRows2(rows, callback, onProgress, onError){
        var counter = 0;
        var action = function(){
            if(!cancelImport){
                if(counter < rows.length){
                    onProgress({
                        current:counter+1,
                        total:rows.length,
                        subject:rows[counter]
                    })
                    
                    $.post("/costum/co/importftl",rows[counter], function(data){
                        if(data.error){
                            onError({
                                message:data.message,
                                context:{
                                    current:counter+1,
                                    total:rows.length,
                                    subject:rows[counter]
                                }
                            })
                        }

                        counter++;
                        action();
                    })
                }else{
                    callback()
                }
            }  
        }

        action()
    }

    $(function(){
        var $inputFile = $(`<input id="input-file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" multiple="multiple"/>`);
        var files = [];

        $("#input-file").change(function(event){
            filesToJson(event.target.files, function(data){
                var rowsData = groupStepsByRows(data);
                processRows(rowsData,function(){
                    alert("Terminer")
                })
            })
        })

        $(".input-file-container").click(function(){
            $inputFile.trigger("click")
        })

        $inputFile.change(function(e){
            files = e.target.files
            $(".input-file-container").addClass("active")
            $(".imported-files .files-number").html(`${event.target.files.length} fichier(s) importé(s)`)
        })

        $(".btn-import").click(function(e){
            e.stopPropagation();
            filesToJson(files, function(data){
                data = data["db"].map(item => {
                    var formatedItem = {}
                    Object.keys(item).map(key => {
                        var keyParts = key.split(" | ").reverse(),
                            inputId = keyParts[0];
                            stepId = keyParts[1];
                        if(!formatedItem[stepId])
                            formatedItem[stepId] = []

                        formatedItem[stepId].push({
                            inputId:inputId,
                            value:item[key]
                        })
                    })

                    return formatedItem
                })
                if(nbrows)
                    data = data.splice(0, nbrows);
                processRows2(data, function(){
                    alert("terminer")
                }, function(data){
                    console.log(data.current + "/" + data.total)
                }, function(error){
                    console.log(error.message)
                })
                /* var rowsData = groupStepsByRows(data);
                if(nbrows)
                    rowsData = rowsData.splice(0, nbrows);

                processRows(
                    rowsData,
                    function(){
                        alert("Terminer")
                    }, function(type, state){
                        $(".progress-log-"+type).html(`
                            <p>${type=="row"?"Ligne":"Etape"}: ${state.current+"/"+state.total}</p>
                        `)
                    }, function(error){
                        $(".error-log").append(
                            `<p>
                                <span class="badge badge-error">
                                Ligne:${error.row.current+"/"+error.row.total}-
                                Etape:${error.step.current+"/"+error.step.total} (id=${error.step.subject.stepId})
                                </span> ${error.message}
                            </p>`
                        );
                    }
                ) */
            })
        })

        $(".btn-cancel-import").click(function(e){
            e.stopPropagation();
            cancelImport = true;
        })

        $("#checkbox-nbrows").change(function(){
            if($(this).is(':checked')){
                $("#form-nbrows").css({"display":"block"})
                nbrows = 1
                $("#form-nbrows input").val("1")
            }else{
                $("#form-nbrows").css({"display":"none"})
                nbrows = null
            }
        })

        $("#form-nbrows input").change(function(){
            nbrows = parseInt($(this).val())
        })
    })
</script>