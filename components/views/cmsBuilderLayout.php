<?php
$assetsUrl = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl();
?>

<script src="<?= Yii::app()->getModule(Costum::MODULE)->getParentAssetsUrl() ?>/js/admin/admin_directory.js"></script>
<script src="<?= Yii::app()->getModule(Costum::MODULE)->getParentAssetsUrl() ?>/js/admin/panel.js"></script>
<script src="/plugins/spectrum-colorpicker2/spectrum.min.js"></script>
<link rel="stylesheet" href="/plugins/spectrum-colorpicker2/spectrum.min.css">
<link rel="stylesheet" href="<?= $assetsUrl ?>/cmsBuilder/css/costumizerV2.css">
<style>
    #mainNav {
        position: absolute !important;
    }

    .d-none {
        display: none !important;
    }
</style>

<div class="cmsbuilder-container">
    <div class="cmsbuilder-header">
        <nav id="menu-top-costumizer">
            <div class="cmsbuilder-header-left">
                <ul class="costum-title">
                    <!-- <li>
                        <a href="javascript:;" class="btn-menu-tooltips">
                            CO<span class="tooltips-menu-btn"><?php echo Yii::t("cms", "tutoriel"); ?></span>
                        </a>
                    </li> -->
                    <li><span class="pull-left text-white elipsis" style="padding-left:10px;max-width:150px;"><?= $costum["title"] ?> </span></li>
                </ul>
                <ul class="cmsbuilder-toolbar">
                    <li class="cmsbuilder-toolbar-item lbh-costumizer  btn-menu-tooltips" data-link="generalInfos" data-title="<?php echo Yii::t("cms", "General settings of costum"); ?>">
                        <span><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "General Informations"); ?></span>
                    </li>
                    <li class="cmsbuilder-toolbar-item">
                        <span><i class="fa fa-file-o" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Sitemap"); ?> > <?php echo Yii::t("cms", "Settings of pages"); ?>" data-link="pages" data-save-button="false">
                                <span><i class="fa fa-file-o" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Pages"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Edition of menu"); ?> " data-link="htmlConstruct">
                                <span><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Menus & header & footer"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Activated elements of the costums"); ?>" data-link="typeObj">
                                <span><i class="fa fa-connectdevelop" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Add-on element"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="Loader" data-link="loader">
                                <span><i class="fa fa-spinner" aria-hidden="true"></i></span>
                                <span>Loader</span>
                            </li>
                        </ul>
                    </li>
                    <li class="cmsbuilder-toolbar-item">
                        <span><i class="fa fa-paint-brush" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="lbh-costumizer" data-dom="#menu-right-costumizer" data-link="design" data-title="<?php echo Yii::t("cms", "Design"); ?>">
                                <span><i class="fa fa-paint-brush" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Dressing"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-link="css">
                                <span><i class="fa fa-css3" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Customized css style"); ?></span>
                            </li>
                        </ul>
                    </li>
                    <li class="cmsbuilder-toolbar-item">
                        <span><i class="fa fa-cog" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="lbh-costumizer" data-link="communityAdmin" data-title="<?php echo Yii::t("cms", "Administrators of the interface"); ?>">
                                <span><i class="fa fa-users" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Administrators"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-link="medias" data-title="<?php echo Yii::t("cms", "Media manager"); ?>">
                                <span><i class="fa fa-folder-open" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Media manager"); ?></span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div>
                <ul class="screen-size-nav">
                    <li>
                        <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips active" data-mode="md" data-title="<?php echo Yii::t("cms", "Computer view-mode"); ?>">
                            <i class="fa fa-desktop" aria-hidden="true"></i>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Computer view-mode"); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips" data-mode="sm" data-title="<?php echo Yii::t("cms", "Tablet view-mode"); ?>">
                            <i class="fa fa-tablet" aria-hidden="true"></i>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Tablet view-mode"); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips" data-mode="xs" data-title="<?php echo Yii::t("cms", "Mobile view-mode"); ?>">
                            <i class="fa fa-mobile" aria-hidden="true"></i>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Mobile view-mode"); ?></span>
                        </a>
                    </li>
                </ul>
                <ul class="cmsbuilder-toolbar editModeUserSwitcher">
                    <li class="cmsbuilder-toolbar-item">
                        <span class="activeModeConnected">
                            <i class="fa fa-user"></i>
                            <i class="statusConnected fa fa-sign-in" aria-hidden="true"></i>
                        </span>
                        <span class="arrow-down"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="costumizer-edit-mode-connected" data-value="true">
                                <span><i class="fa fa-sign-in" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Loggued mode"); ?></span>
                                 <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                            </li>
                             <li class="costumizer-edit-mode-connected" data-value="false">
                                <span><i class="fa fa-sign-out" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Unloggued mode"); ?></span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div>
                <?php
                    $newUrl = "";
                    $url =  explode("edit", $_SERVER['REQUEST_URI']);
                    if (isset($url[1])) {
                        $newUrl = $url[0];
                    }
                 ?>
                <ul class="action-nav" id="cmsBuilder-right-actions">
                    
                    <li class="btn-menu-tooltips">
                        <a href="javascript:;" class="open-preview-costum" data-url="<?= Yii::app()->baseUrl.$newUrl ?>">
                            <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Preview"); ?></span>
                        </a>
                    </li>
                    <?php if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){ ?>
                    <li class="btn-menu-tooltips">
                        <a href="javascript:;" onclick="cmsBuilder.tpl.saveThis();">
                            <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Save as a template"); ?></span>
                        </a>
                    </li>
                    <?php } ?>
                    <li class="btn-menu-tooltips">
                        <a href="javascript:;" class="lbh-costumizer nav-costumizer" data-link="template">
                            <span><i class="fa fa-folder-open" aria-hidden="true"></i></span>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Choose a template"); ?></span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="cmsbuilder-body">
        <div class="cmsbuilder-side-content cmsbuilder-left-content">
            <button class="btn-toggle-cmsbuilder-sidepanel" data-target="left">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                <span><?php echo Yii::t("cms", "Add a block"); ?></span>
            </button>
            <div id="left-panel" style="width: 100%; height:100%;">

            </div>
        </div>
        <div class="cmsbuilder-center-content">
            <div id="cmsbuilder-content-md" class="cmsbuilder-content-wrapper">
                <?= $content ?>
            </div>
            <div id="cmsbuilder-content-sm" class="d-none cmsbuilder-content-wrapper">
                <div class="device-mockup" style="background-image: url('<?= $assetsUrl ?>/cmsBuilder/img/device-sm.png');"></div>
                <div class="device-screen">
                    <div class="device-status-bar">
                        <div>
                            <span class="device-time">07:58</span>
                        </div>
                        <div>
                            <span><i class="fa fa-signal" aria-hidden="true"></i></span>
                            <span>5G</span>
                            <span><i class="fa fa-battery-three-quarters" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="device-search-bar">
                        <div class="devise-search-bar-url">
                            <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <span><?= $costum["title"] ?></span>
                        </div>
                        <div><i class="fa fa-repeat" aria-hidden="true"></i></div>
                    </div>
                    <div class="device-page">

                    </div>
                </div>
            </div>
            <div id="cmsbuilder-content-xs" class="d-none cmsbuilder-content-wrapper">
                <div class="device-mockup" style="background-image: url('<?= $assetsUrl ?>/cmsBuilder/img/device-xs.png');"></div>
                <div class="device-screen">
                    <div class="device-status-bar">
                        <div>
                            <span class="device-time">07:58</span>
                        </div>
                        <div>
                            <span><i class="fa fa-signal" aria-hidden="true"></i></span>
                            <span>5G</span>
                            <span><i class="fa fa-battery-three-quarters" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="device-search-bar">
                        <div class="devise-search-bar-url">
                            <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <span><?= $costum["title"] ?></span>
                        </div>
                        <div><i class="fa fa-repeat" aria-hidden="true"></i></div>
                    </div>
                    <div class="device-page">

                    </div>
                </div>
            </div>
        </div>
        <div class="cmsbuilder-side-content cmsbuilder-right-content">
            <button class="btn-toggle-cmsbuilder-sidepanel" data-target="right"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <div id="right-panel" style="width: 100%; height:100%;"></div>
        </div>
    </div>
</div>
<div id="modal-content-costumizer-administration">
    <div class="modal-tools-costumizer">
        <div class="costumizer-title">
            <span class="bold uppercase"></span>
        </div>
        <div class="pull-right">
            <button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> <?php echo Yii::t("common", "Save") ?></button>

            <button class="btn btn-default close-dash-costumizer" data-dom="#modal-content-costumizer-administration"><i class="fa fa-close"></i> <?php echo Yii::t("common", "Close") ?></button>
        </div>
    </div>
    <div data-key="" class=" contains-view-admin col-xs-12 no-padding">

    </div>
</div>
<div id="modal-blockcms">
    <div class="modal-blockcms-header">
        <h3><?php echo Yii::t("common", "Select one block") ?></h3>
        <button id="btn-hide-modal-blockcms"><i class="fa fa-times" aria-hidden="true"></i></button>
    </div>
    <div class="modal-blockcms-body">
        <div class="modal-blockcms-filter col-sm-12">
            <div class="input-group">
                <input id="blocklist-search-text" type="text" class="form-control" aria-label="..." placeholder="<?php echo Yii::t("common", "Search a block") ?>...">
                <div class="input-group-btn">
                    <button id="blocklist-dropdown-toggle" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="btn-label">Tout les blocks</span> <span class="caret"></span></button>
                    <ul id="blocklist-dropdown-menu" class="dropdown-menu dropdown-menu-right">
                        <li data-type="all" class="selected"><a href="#"> <?php echo Yii::t("common", "All blocks") ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php foreach ($customBlockTypes as $type) { ?>
                            <li data-type="<?= $type ?>"><a href="#"><?= ucfirst($type) ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-blockcms-list">
            <?php
            $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
            foreach ($customBlocks as $id => $block) {
                $pathParts = explode(".", $block["path"]);
            ?>
                <div class="modal-blockcms-list-item" data-id="<?= $id ?>" data-path="<?= $block["path"] ?>" data-type="<?= isset($pathParts[2]) ? $pathParts[2] : "" ?>" data-name="<?= $block["name"] ?>">
                    <div class="selection-hover"><i class="fa fa-check" aria-hidden="true"></i></div>
                    <div class="modal-blockcms-list-item-body">
                    <?php
                        if ( @$block["profilMediumImageUrl"]) {   
                        ?>
                            <img src="<?= $baseUrl,@$block["profilMediumImageUrl"] ?>" alt="">
                        <?php } else { ?>
                            <img src="<?= $assetsUrl ?>/cmsBuilder/img/custom-block.png" alt="">
                        <?php }?>
                    </div>
                    <div class="modal-blockcms-list-item-footer">
                        <p><?= ucfirst($block["name"]) ?></p>
                    </div>
                    <?php if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){ ?>
                        <div class="text-center padding-5">
                            <button style="width:49%; background-color:#5bc0de70;" class="editCMS btn btn-info" data-id="<?= $id ?>"><i class="fa fa-pencil"></i></button>
                            <button style="width:49%; background-color:#d9534f61;" class="deleteCms btn btn-danger" data-id="<?= $id ?>" data-name="<?= ucfirst($block["name"]) ?>"><i class="fa fa-trash"></i></button>
                        </div>
                    <?php } ?>  
                </div>
            <?php } ?>          
        </div>
    </div>
</div>
<div id="right-sub-panel-container">
    <!-- <button class="btn-close-sub-right-costumizer" onclick="costumizer.actions.closeMenuSubRight()"><span>Glisser et deposer un élément dans le contenu</span> <span><i class="fa fa-chevron-up" aria-hidden="true"></i></span></button> -->
</div>

<style>
    body {
        overflow: hidden;
    }

    #modal-blockcms {
        position: absolute;
        z-index: 9999999;
        width: 40%;
        min-width: 650px;
        height: calc(100vh - 40px);
        top: 40px;
        left: 0px;
        background-color: #3A3A3A;
        color: white;
        transform: translateX(-100%);
        visibility: hidden;
        transition: .3s;
    }

    #modal-blockcms.open {
        transform: translateX(0);
        visibility: visible;
    }

    .modal-blockcms-header {
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 20px;
    }

    .modal-blockcms-header h3 {
        margin: 0;
        padding: 0;
        text-transform: none;
        font-weight: normal;
    }

    .modal-blockcms-header button {
        width: 40px;
        height: 40px;
        border-radius: 100%;
        border: none;
        background-color: #333;
    }

    .modal-blockcms-body {
        height: 100%;
    }

    .modal-blockcms-filter {
        height: 60px;
    }

    .modal-blockcms-filter .input-group input,
    .modal-blockcms-filter .input-group button {
        background: #333;
        color: white;
        border: 1px solid #707070;
    }

    .modal-blockcms-filter .input-group button {
        min-width: 180px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 34px;
    }

    .modal-blockcms-filter .dropdown-menu {
        max-height: 450px;
        overflow: auto;
    }

    .modal-blockcms-list {
        width: 100%;
        max-height: calc(100% - 160px);
        overflow: auto;
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-gap: 20px;
        padding: 0px 20px;
    }

    .modal-blockcms-list-item {
        width: 100%;
        border: 1px solid #707070;
        border-radius: 4px;
        height: fit-content;
        cursor: pointer;
        position: relative;
    }

    .modal-blockcms-list-item:hover {
        border-color: #007fff;
    }

    .modal-blockcms-list-item .selection-hover {
        width: 45px;
        height: 45px;
        position: absolute;
        top: 0;
        right: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        opacity: 0;
        pointer-events: none;
        transition: .2s;
    }

    .modal-blockcms-list-item:hover>.selection-hover {
        opacity: 1;
    }

    .modal-blockcms-list-item .selection-hover i {
        position: absolute;
        top: 10px;
        right: 5px;
        z-index: 1;
    }

    .modal-blockcms-list-item .selection-hover::after {
        position: absolute;
        content: '';
        display: block;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 0 45px 45px 0;
        border-color: transparent #007fff transparent transparent;
    }

    .modal-blockcms-list-item-body {
        height: 150px;
    }

    .modal-blockcms-list-item-footer {
        padding: 10px;
    }

    .modal-blockcms-list-item-body img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 4px 4px 0px 0px;
    }

    .modal-blockcms-list-item-footer p {
        margin: 0px;
        padding: 0px;
        font-size: 16px;
    }

    .ui-widget input,
    .ui-widget select,
    .ui-widget textarea,
    .ui-widget button {
        font-size: initial !important;
    }

    .ui-widget-content {
        background-color: #3A3A3A;
        border: none;
        color: white;
    }

    .ui-accordion .ui-accordion-content {
        padding: 10px !important;
    }

    .ui-accordion .ui-accordion-header {
        font-weight: bold;
        background: #313131;
        text-transform: none;
        color: white;
        border: none;
        padding: 15px 5px;
    }

    .ui-accordion .ui-accordion-header.ui-state-active {
        background: #007fff;
    }

    .mt-10 {
        margin-top: 10px !important;
    }
</style>
<style id="cssLoader"></style>
<script>
    $(function() {
        costumizer.init()
        cmsConstructor.init()
    })
</script>