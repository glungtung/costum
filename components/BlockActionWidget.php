<?php
namespace PixelHumain\PixelHumain\modules\costum\components;

use yii\base\Widget;

class BlockActionWidget extends Widget{
    public $label = "Block";
    public $blockAttributes = [];
    public $actions = [];
    public $kunik;

    public function init(){
        parent::init();
    }

    public function run(){
        return $this->render("blockAction", [
            "label" => $this->label,
            "kunik" => $this->kunik,
            "blockAttributes" => $this->blockAttributes,
            "actions" => $this->actions
        ]);;
    }
}