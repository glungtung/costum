<?php
namespace PixelHumain\PixelHumain\modules\costum\components;

class CostumHelper{
    public static function buildStyle($styles){
        $strStyles = "";

        foreach($styles as $selector => $style){
            $strStyle = $selector."{".PHP_EOL;
            foreach($style as $property => $value){
                $value_without_units = preg_replace('/\s+|px|%|em|rem|vw|vh|ex|ch|/', "", $value);
                if($value_without_units !== ""){
                    $strStyle .= $property.":".$value.";".PHP_EOL;
                }
            }
            $strStyle .= "}";

            $strStyles .= $strStyle.PHP_EOL;
        }

        return $strStyles;
    }
}