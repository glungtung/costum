<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\elements;

use Document;
use Yii;
use yii\base\Widget;

class StandardElementWidget extends Widget{
    private $paramsData = [ 
        "title" => "AGENDA",
        "titleBg" => "transparent",
        "title2" => "Sous titre ",
        "elType" => "events",
        "elPoiType" => "",
        "elDesign" => "1",
        "imgShow" => true,
        "imgDefault" => "",
        "imgFit" => "cover",
        "imgWidth" => "352px",
        "imgHeight"  => "352px",
        "imgRound" => "0%",
        "imgBg" => "#f0ad16",
        "infoBg" => "#f0ad16",
        "imgBorder" => "",
        "imgBorderColor" => "",
        "infoDistanceFromImage" => "-14px",
        "infoMarginLeft" => "0px",
        "infoMarginRight" => "0px",
        "infoDateFormat" => "1",
        "infoOrder" => true,
        "infoAlign" => "center",
        "infoCenter" => true,
        "infoBorderRadius" => "0%",
        "infoHeight" => "auto",
        "infoOnHover" => true,
        "cardBg" => "transparent",
        "cardBoxShadow" => false,
        "cardAutoPlay" => true,
        "cardNumberPhone" => 1,
        "cardNumberTablet" => 2,
        "cardNumberDesktop" => 3,
        "cardSpaceBetween" => 10,
        "cardNbResult" => 6,
        "cardOrderBy" => "",
        "cardOrderType" => 1,
        "cardBtnShowMore" => true,
        "showName" => true,
        "showType" => true,
        "showShortDescription" => true,
        "showDescription" => true,
        "showDate" => true,
        "showAdress" => true,
        "showTags" => true,
        "showRoles" => true,
        "showLinks" => false,
        "showDateOnly" => true,
        "showDayOnly" => true,
        "showDayNumberOnly" => false,
        "showMonthOnly" => true,
        "showYearOnly" => true,
        "formTitle" => "",
        "formName" => true,
        "buttonShowAllContentView" => "1",
        "buttonShowAll" => false,
        "buttonShowAllText" => "Afficher tout",
        "buttonShowAllMore" => false,
        "buttonShowAllWdColor" => "#18BC9C",
        "buttonShowAllWdBorderRadius" => "4px",
        "buttonShowAllWdBorderColor" => "#18BC9C",
        "buttonShowAllWdPaddingVertical" => "6px",
        "buttonShowAllWdPaddingHorizontal" => "12px",
        "buttonShowAllWdFontColor" => "#fff",
        "buttonShowAllWdFontweight" => 400,
        "buttonShowAllWdFontSize" => "14px",
        "buttonShowAllWdFontFamily" => "Arial",
        "buttonShowAllType" => true,
        "btnAdd" => true,
        "btnChoose" => false,
        "btnEditDelete" => true,
        "element" => [],
        "elDataContributors" => false,
        "elDataParent" => true,
        "elDataSourceKey" => true,
        "msgSuccess" => "",
        "btnCreateNewEvent" => 0
    ];
    private $addType = "";
    private $addBtnType = null;
    private $assetsUrl = "";

    public $config = [];
    public $path = "";

    public function init(){
        parent::init();

        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

        // recuperer data dans a base
        if (isset($this->config["blockCms"])) {
            foreach ($this->paramsData as $e => $v) {
                if (  isset($this->config["blockCms"][$e]) )
                    $this->paramsData[$e] = $this->config["blockCms"][$e];
            }
        }
        $initImage = Document::getListDocumentsWhere(
            array(
              "id"=> $this->config["blockKey"],
              "type"=>'cms',
              "subKey"=>'image',
            ), "image"
        );
        foreach ($initImage as $key => $value) {
            $this->paramsData["imgDefault"] = $value["imagePath"];
        }

        $this->addType = $this->paramsData["elType"];
        if($this->paramsData["elType"] == "events" || $this->paramsData["elType"] == "organizations" || $this->paramsData["elType"] == "projects" /*substr($addType, -1) == "s"*/){
            $this->addType = substr($this->addType, 0, -1);
        }

        if(is_array($this->addType) && count($this->addType) ==  1){
            if($this->addType[0] == "events" || $this->addType[0] == "organizations" || $this->addType[0] == "projects"){
                $this->addBtnType = substr($this->addType[0], 0, strlen($this->addType[0])-1);
            }else if($this->addType[0] == "poi"){
                $this->addBtnType = "poi";
            }else{
                $this->addBtnType = $this->addType[0];
            }
        }else if(is_string($this->addType)){
            $this->addBtnType = $this->addType;
        }

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
            "addType" => $this->addType,
            "addBtnType" => $this->addBtnType,
            "assetsUrl" => $this->assetsUrl
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}