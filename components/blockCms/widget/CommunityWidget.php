<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew,HtmlHelper,MongoId;

class CommunityWidget extends Widget{
    public $paramsData = [
        "numberXl" => "3",
        "numberMd" => "2",
        "numberSm" => "1",
        "delay" => "2500",
        "query" => [
            "searchType" => [],
            "filters" => []
        ],
        "swiper" => [
            "pagination" => [
                "clickable" => false,
                "type" => "bullets"
            ],
        ]
    ];
    public $path = "";
    public $config = [];
    private $assetsUrl = "";

    public function init(){
        parent::init();

        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
        HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $this->assetsUrl);
        
        if (!empty($this->paramsData["query"]["orderBy"])) {
            $this->paramsData["query"]["sortBy"][$this->paramsData["query"]["orderBy"]] = $this->paramsData["query"]["orderType"];
        }

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]); 
        }

        $contextId = $this->config["costum"]["contextId"];
        $this->paramsData["query"]["searchType"] = [$this->config["costum"]["contextType"]];
        $this->paramsData["query"]["filters"]["_id"]= [new mongoId($contextId)];

        $list = searchNew::globalautocomplete($this->paramsData["query"]);
        $data = $list["results"][$contextId]["links"]["members"] ?? [];
        foreach ($data as $key => $val){
            $query = [
                "searchType" => ["citoyens"],
                "filters" => [
                    "_id" => [new MongoId($key)]
                ],
            ];
            $about = searchNew::globalautocomplete($query);
            $data[$key] = array_replace_recursive($data[$key],$about["results"][$key]);
        }

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
            "assetsUrl" => $this->assetsUrl,
            "dataResult" => $data
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config );
    }

}