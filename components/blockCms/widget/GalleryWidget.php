<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

    use yii\base\Widget;

    class GalleryWidget extends Widget {
        public $defaultData = [
            "swiper" => [
                "slideDirection" => "horizontal",
                "breakpoints" => [
                    "slidesPerView" => [
                        "xs" => 1,
                        "sm" => 2,
                        "md" => 3
                    ],
                    "spaceBetween" => [
                        "xs" => 10,
                        "sm" => 20,
                        "md" => 30
                    ]
                ],
                "initialSlide" => 0,
                "speed" => 1000,
                "loop" => false,
                "autoplay" => false,
                "autoHeight" => false,
                "effect" => "slide",
                "pagination" => [
                    "clickable" => false,
                    "type" => "bullets"
                ],
                "scrollbar" => [
                    "hide" => false
                ],
                "grabCursor" => false,
                "noSwiping" => false
            ],
            "images" => []
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>