<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew;

class ElementStandardWidget extends Widget{
    public $paramsData = [
        "query" => [
            "searchType" => ["events"],
            "indexStep" => "6",
            "orderBy" => "",
            "orderType" => "1",
            "sortBy" => [],
            "filters" => [],
            "fields"=>["profilRealBannerUrl"],
        ],
        "displayConfig" => [
            "designType" => "1",
        ],
        "swiper" => [
            "slideDirection" => "horizontal",
            "breakpoints" => [
                "slidesPerView" => [
                    "xs" => 1,
                    "sm" => 2,
                    "md" => 3
                ],
                "spaceBetween" => [
                    "xs" => 20,
                    "sm" => 30,
                    "md" => 40
                ]
            ],
            "initialSlide" => 0,
            "speed" => 1000,
            "loop" => false,
            "autoplay" => false,
            "autoHeight" => false,
            "effect" => "slide",
            "pagination" => [
                "clickable" => false,
                "type" => "bullets"
            ],
            "scrollbar" => [
                "hide" => false
            ],
            "grabCursor" => false,
        ],
        "css" => [
            "card" => [
                "background" => "tranparent"
            ]
        ],
    ];
    public $path = "";
    public $config = [];
    private $assetsUrl = "";

    public function init(){
        parent::init();

        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;



        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        

        if (!empty($this->config["blockCms"]["query"]["orderBy"])) {
            $this->config["blockCms"]["query"]["sortBy"][$this->config["blockCms"]["query"]["orderBy"]] = $this->config["blockCms"]["query"]["orderType"];

        }
        $contextParent = "parent.".$this->config["costum"]["contextId"];
        $this->config["blockCms"]["query"]["filters"]['$or'] = [];
        $this->config["blockCms"]["query"]["filters"]['$or'][$contextParent] = ['$exists'=>true];
        $this->config["blockCms"]["query"]["filters"]['$or']["source.keys"] = $this->config["costum"]["contextSlug"];

        $list= searchNew::globalautocomplete($this->config["blockCms"]["query"]);

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
            "assetsUrl" => $this->assetsUrl,
            "dataResult" => $list["results"]
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }

}