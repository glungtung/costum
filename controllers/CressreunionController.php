<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CressreunionController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'importsiret'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\admin\ImportsiretAction::class,
	        'importorga'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\admin\ImportorgaAction::class,
	        'savesiren'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\admin\SaveSirenAction::class,
	        'saveorga'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\admin\SaveOrgaAction::class,
	        'register'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\RegisterAction::class,
	        'answerlink'        => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\AnswerLinkAction::class,
	    );
	}
}
