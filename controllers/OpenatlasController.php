<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class OpenatlasController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'getpersons'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\openatlas\GetPersonsAction::class,
	        'getcampaign'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\openatlas\GetCampaignAction::class,
	    );
	}
}
