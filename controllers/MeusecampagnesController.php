<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Bouboule <clement.damiens@communecter.org>
 * Date: 13/11/2019
 */
class MeusecampagnesController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'importorga'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin\ImportorgaAction::class,
	        'importevent'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin\ImporteventAction::class,
	        'saveorga'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin\SaveOrgaAction::class,
	        'saveevent'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin\SaveEventAction::class,
            'stattrim'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin\StatisticsAction::class,
	    );
	}
}
