<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class AapController extends CommunecterController {

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    public function actions() {
        return array(
            'commonaap' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\AapAction::class,
            'agenda' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\AgendaAction::class,
            'generateconfig'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GenerateConfigAction::class,
            'getviewbypath' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GetViewByPath::class,
            'deleteaap' => 'costum.controllers.actions.aap.DeleteAapAction',
            'publicrate' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\PublicRateAction::class,
            'searchtags' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\SearchTagsAction::class,
            'searchanswer' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\SearchAnswerAction::class,
            'pushtags' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\PushTagsAction::class,
            'getelementbyid' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\getElementById::class,
            'directory' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\DirectoryAction::class,
            "cosindni-export-csv" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\CosindniExportCSVAction::class,
            "aap-export-csv" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\AapGeneriqueExportCSVAction::class,
            "getfinancor" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GetFinancorsByFormIdAction::class,
            "exportpdf" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\ExportpdfAction::class,
            "exportcsvfinancer" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\ExportcsvfinancerAction::class,
            "sendmail" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\MailAction::class,
            "attachedfile" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\AttachedFile::class,
            "addnotified" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\NotificationStatusAction::class,
            "getactionsbyanswer" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GetActionsByAnswer::class,
            "actiondetail" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\ActionDetail::class,
            "getprojectsbyanswers" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GetProjectsByAnswers::class,
            "reloadpanel" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\ReloadPanelAction::class,
            "generateformoceco" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GenerateFormOcecoAction::class,
            "duplicateproposition" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\DuplicatePropositionAction::class,
            "listaap" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\ListAapAction::class,
            "cacs" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\CacsAction::class,
            "sousorga" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\SousOrgaAction::class,
            "proposalbycostumslug" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\ProposalByCostumSlug::class,
            'copyfieldsfromfield' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\CopyAnswerFromOneFieldAction::class,
        );
    }

}