<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class HubultranumeriqueController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'getcommunityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\hubultranumerique\GetCommunityAction::class,
            'geteventcommunityaction'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\hubultranumerique\GetEventCommunityAction::class
	    );
	}
}
