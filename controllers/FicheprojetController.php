<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class FicheprojetController extends CommunecterController {


	public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getallansweraction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ficheprojet\GetAllAnswerAction::class,
			'getansweraction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ficheprojet\GetAnswerAction::class,
			'getfileaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ficheprojet\GetFileAction::class,
			
		);
	}
}
?>