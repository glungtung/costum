<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CmcashautebretagneController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'getevents'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cmcashautebretagne\GetEventAction::class,
	        'getarticles'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cmcashautebretagne\GetArticleAction::class,
	        'element'			=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cmcashautebretagne\ElementAction::class,
	        'updateblock'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cmcashautebretagne\UpdateBlockAction::class
	    );
	}

}
