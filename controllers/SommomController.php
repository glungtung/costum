<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class SommomController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(    
	        'dashboard'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\DashboardAction::class,
	        'graph'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\GraphAction::class,
	        'welcome'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\DashboardAction::class,
	        'member'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\MemberAction::class,
	        'partenaire' 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\PartenaireAction::class,
	        'help' 			=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\HelpAction::class,
	        'territory' 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom\TerritoryAction::class,
	    );
	}

}
