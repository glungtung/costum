<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class FrancetierslieuxController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'answerdirectory'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\AnswerDirectoryAction::class,
			'getallanswers'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetAllAnswersAction::class,
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\DashboardAction::class,
	        'existingreference' =>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ExistingreferenceAction::class,
	        'getnetwork'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetNetworkAction::class,
	        'multireference' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\MultiReferenceAction::class,
	        'dumprecencement2023' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ObservatoireDumpAction::class,
			'notfinishedinforecensement2023' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\NotFinishedInfoRecensement2023Action::class,
	        'diagnoanswers' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\DiagnoAnswersAction::class
	    );
	}

}
