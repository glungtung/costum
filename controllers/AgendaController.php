<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class AgendaController extends CommunecterController {
    public function beforeAction($action) {
        return parent::beforeAction($action);
    }

    public function actions() {
        return [
            'events' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda\EventAction::class,
            'projects' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda\ProjectAction::class
        ];
    }
}
