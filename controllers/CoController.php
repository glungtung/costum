<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
		//parent::initPage();

    	//login auto from cookie if user not connected and checked remember
	    parent::connectCookie();
	    return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
			'index' 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\IndexAction::class,
			'dashboard' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\DashboardAction::class,
			'config'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ConfigAction::class,
			'export'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ExportAction::class,
			'importftl' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ImportAnswerAction::class,
			'oceco'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\OcecoAction::class
	    );
	}

}
