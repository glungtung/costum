<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class ProjectController extends CommunecterController {

    public function beforeAction($action) {
        return parent::beforeAction($action);
    }

    public function actions() {
        return [
            'action' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\project\ActionAction::class,
            'project' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\project\ProjectAction::class
        ];
    }
}
