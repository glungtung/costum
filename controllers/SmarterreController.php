<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SmarterreController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'searchurl'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre\SearchUrlAction::class,
            'crud'              => \PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre\CrudAction::class,
            'getthermatique'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre\GetThematiqueAction::class,
            'getsmarterritoriesaction' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre\GetSmarterritoriesAction::class,
	    );
	}

}
