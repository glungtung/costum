<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;


use CommunecterController;
use PHDB;
use Rest;
use Yii;

class CommentController extends CommunecterController {

    public function beforeAction($action) {
		return parent::beforeAction($action);
	}

  	public function actionIndex(){
		//$comments = PHDB::find("comments");
		$comments = PHDB::findAndSort("comments", ["parent"=>$_GET["comment"]], ["date"=> -1], 8);
        return Rest::json($comments);
	}
	
    public function actionSave(){
		$_POST["date"] = date("d / m / Y - H:i");
		$comment = Yii::app()->mongodb->selectCollection("comments")->insert( $_POST);
		return Rest::json($comment);
	}

    public function actionReply(){
		$_POST["reply"]["date"] = date("d / m / Y - H:i");
		$comment = PHDB::findOneById("comments", $_POST["comment"]);
		$reply = PHDB::update("comments", ['_id' => $comment["_id"]], ['$push'=>["replies" => $_POST["reply"]]]);
		return Rest::json($reply);
	}

}
