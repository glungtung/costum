<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoeurnumeriqueController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'element'           => \PixelHumain\PixelHumain\modules\costum\controllers\actions\coeurnumerique\ElementAction::class,
            'updateblock'       => \PixelHumain\PixelHumain\modules\costum\controllers\actions\coeurnumerique\UpdateBlockAction::class,
            'geteventaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\coeurnumerique\GetEventAction::class,
            'galery'            => \PixelHumain\PixelHumain\modules\costum\controllers\actions\coeurnumerique\GaleryAction::class,
	    );
	}

}
