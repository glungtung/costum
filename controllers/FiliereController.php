<?php 

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController, PHDB, Rest, Yii, Project, Citoyen;
use Crowdfunding;
use MongoId;

class FiliereController extends CommunecterController {

	public function beforeAction($action) {
		return parent::beforeAction($action);
	}

	public function actions() {
      return array(
      	'get-sourcekeys' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere\GetSourceKeysAction::class,
      	'generate'      => \PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere\GenerateAction::class,
      	'update-category'=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere\UpdateCategoryAction::class,
      	'populate'      => \PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere\PopulateAction::class,
      	'get-related-elements' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere\GetRelatedElementsAction::class
      );
  	}
}
?>