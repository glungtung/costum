<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre;

use CAction, Smarterre, Rest;
class GetSmarterritoriesAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $parametres = Smarterre::getSmarterritories();
        
        return Rest::json($parametres);
    }}