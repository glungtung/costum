<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterritoire;

use CAction, Smarterritoire, Rest;
class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Smarterritoire::getEvent();
        
        return Rest::json($params);
    }
}