<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pixelhumain;
use CAction;
use PixelHumain;
use Rest;

class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null, $type = null, $slug = null, $view = null, $page = null)
    {
        $controller = $this->getController();
        $params = PixelHumain::getCommunity($_POST["id"], @$_POST["tags"]);

			return Rest::json($params);
		}
	}
?>