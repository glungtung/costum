<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cmcashautebretagne;
use CAction;
use CmcasHauteBretagne;
use Rest;

/**
 * 
 */
class GetEventAction extends \PixelHumain\PixelHumain\components\Action
{
	
	public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CmcasHauteBretagne::getEvents($_POST["sourceKey"]);
        
        return Rest::json($params);
    }
}
