<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;

use CAction, Organization, Project, PHDB, Slug, Document, Costum, Form, Answer, Cms, Poi, FilesystemIterator, FileHelper, Rest, MongoId, Yii;
class ExportAction extends \PixelHumain\PixelHumain\components\Action
{
    //http://communecter56-dev/costum/co/export/slug/ctenat
    //http://communecter56-dev/costum/co/export/slug/ctenat/mode/json
    //http://communecter56-dev/costum/co/export/slug/ctenat/mode/json/save/1
    //http://communecter56-dev/costum/co/export/slug/ctenat/install/1
    //http://communecter56-dev/costum/co/export/slug/ctenat/install/answers.json
    //http://communecter56-dev/costum/co/export/slug/ctenat/install/costum.json
    //http://communecter56-dev/costum/co/export/slug/ctenat/mode/insert/insert/costum.json
public function run($slug=null,$mode=null,$save=null,$install=null,$insert=null,$id=null,$type=null ) {
    $ctrl=$this->getController();
    
     if ( ! Person::logguedAndValid() && Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) ) 
          $str .= Rest::json(['result' => false, "msg"=>Yii::t("common","Please Login First"),"icon"=>"fa-sign-in"]);
    
    $ctrl->layout = "//layouts/empty";
    $echos = "<h3> Liste des costums enregistrés </h3>";
/* **************************************
*
*    Repertoire de tout les exports
*
***************************************** */
if(!$slug){
    
    foreach ( scandir( "../../modules/costumexport" ) as $i => $f ) {
        if(is_dir("../../modules/costumexport/".$f) && stripos($f,"export_")===0  ){
            $slug = substr( $f,7 );
            $echos .= "<b>".$slug."</b> <a href='/costum/co/export/slug/".$slug."' class='btn btn-primary'>HTML</a> ".
            //"<a href='/costum/co/export/slug/".$slug."/mode/json' class='btn btn-primary'>JSON</a> ".
            //"<a href='/costum/co/export/slug/".$slug."/mode/json/save/1' class='btn btn-primary'>save json</a> ".
            //"<a href='/costum/co/export/slug/".$slug."/save/1' class='btn btn-primary'>save html</a> ".
            "<a href='/costum/co/export/slug/".$slug."/install/1' class='btn btn-danger'>check install</a> ".
            "<a href='/costum/co/index/slug/".$slug."' target='_blank' class='btn btn-danger'>goto costum</a><br/>";
            //$echos .= "<a href='/costum/co/export/slug/".$cost."'> ".$cost."</a>";

            
        }
    }
    //Liste all existing costums
    $cols = [Organization::COLLECTION,Project::COLLECTION];
    foreach ($cols as $key => $c) {
        $l = PHDB::find($c,["costum"=>['$exists'=>1]],["costum.slug","slug","name"]);
        $echos .= "<h3>".count($l)." ".$c." costums</h3>";
        foreach ($l as $i => $e) {
            //$echo .= $e["name"]." : ";
            if(isset($e["slug"]) && isset($e["costum"]["slug"]))
                $echos .= "<br><a href='/costum/co/index/slug/".$e["slug"].
                    "' target='_blank' class='btn btn-danger'>".$e["slug"].
                    " </a> uses ".$e["costum"]["slug"]." costum <br/>";
        }
    }
    return $echos;
} 
else 
{

/* **************************************
*
*    Preparing data 
*
***************************************** */
    $el = Slug::getElementBySlug($slug);
    if(isset($el['el']['links']))
        unset($el['el']['links']);
    //$echos .= "<h1>COport : L'import Export transDocumental : ".$el['type'].$el['id']."</h1>";

    $str = "";        
    $res = [];
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // ELEMENT DB DATA 
    // and documents 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    $size = 0;
    $docs = PHDB::find( Document::COLLECTION , [ "type"=>$el['type'],"id"=>$el['id'] ],["size"] );
    if(!$mode){
        $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
        $str .= "<h2 style='color:teal'>//Slug element</h2>".$slug." type : ".$el['type']."|  id : ".$el['id']."<br/>";
        $str .= "DATA : ".$slug." : ".$el['type']."<br/>";

        unset($el['el']['video']);
        $str .= "<pre style='color:red'>".html_entity_decode( json_encode( $el['el'] ))."</pre>" ;

        if(!empty($docs)){
            $Asize = 0;
            foreach ($docs as $i => $d) {
                $Asize += $d["size"];
            }
            for($i = 0; ($Asize / 1024) > 0.9; $i++, $Asize /= 1024) {}
            $Asize = round($Asize, 2).['B','kB','MB','GB','TB','PB','EB','ZB','YB'][$i];
            $str .= "<span style='color:teal'>//Consommation element ".$Asize." ".count( $docs )." x doc associés</span><br/>";
            $size += intval($Asize);
            $res[Document::COLLECTION] = $docs;
        }
        $str .= "</section>";
        
    } else {
        //unset($el["el"]['_id']);
        $res[$el['type']] = $el['el'];
        $res["slugs"] = PHDB::findOne( Slug::COLLECTION, ["name" => $slug ]);
        if(!empty($docs)){
            $res[Document::COLLECTION] = $docs;                
        }
    }

    
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // COSTUM CONFIG JSON DATA 
    // and documents 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    $costum = null;
    $costumId = (isset($el["costum"]['slug'])) ? $el["costum"]['slug'] : $slug;
    if($costum = PHDB::findOne( Costum::COLLECTION, ["slug" => $costumId ])){
        if(!$mode){
            $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
            $str .= "<h2 style='color:teal'>//collection costum</h3> id :" .$costumId."<br2>";
            $str .= "DATA costum : ".$costumId."<br/>";
            $str .= "<pre style='color:red'>".html_entity_decode( json_encode( $costum ) )."</pre>";
            $str .= "</section>";
        } 

    } else if(isset($el["el"]["costum"]) && $costum = PHDB::findOne( Costum::COLLECTION, ["slug" => $el["el"]["costum"] ])){
        if(!$mode){
            $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
            $str .= "<h2 style='color:teal'>//collection costum</h3> id :" .$costumId."<br2>";
            $str .= "DATA costum : ".$costumId."<br/>";
            $str .= "<pre style='color:red'>".html_entity_decode( json_encode( $costum ) )."</pre>";
            $str .= "</section>";
        } 

    } else if( file_exists("../../modules/costumexport/".$costumId.".json") ){
        $costum = file_get_contents("../../modules/costumexport/".$costumId.".json", FILE_USE_INCLUDE_PATH);
        if(!$mode){
            if( !empty($costum) ){ 
                $str .= "<h3>//costumexport/".$costumId.".json </h3><br/>";
                $str .= "DATA costum : ".$costumId."<br/>";
                $str .= "<prestyle='color:red'>".html_entity_decode( $costum)."</pre>";
            } else 
                $str .= "<pre style='color:red'>".html_entity_decode( json_encode(['result' => false, "msg"=>"No Costum json data","icon"=>"fa-search"]) )."</pre>"; 
        }
    }

    //unset($costum['_id']);
    $res["costum"] = $costum;

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // CONNECTED FORMS AND ANSWERS
    // and documents 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    $formId = (isset($costum["formId"]) ) ? $costum["formId"] : null ;
    $form = PHDB::findOne( Form::COLLECTION , [ "parent.".$el['id'] => [ '$exists' => 1 ] ] );
    if(isset($form) && isset($form["id"])){
        $formId = $form["id"];
    }

    if(isset($formId)){
        
        $form = PHDB::findOne( Form::COLLECTION , [ "id"=>$formId ] );
        if(isset($form)){
            $form_Id = (string)$form["_id"];
            //unset($form["_id"]);
            if(!$mode){
                $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
                $str .= "<h2 style='color:teal'>//collection Forms</h2> ".$formId."<br2>";  
                $str .= "DATA forms : ".$formId."<br/>";
                $str .= "<pre style='color:red'>".html_entity_decode( json_encode( $form ) )."</pre>";
                $str .= "</section>";
            } else {
                $formIdList = [ ];
                $formIdList[] = $formId;
                $formIdList = array_merge( $formIdList, $form["subForms"] );
                $forms = PHDB::find( Form::COLLECTION , [ "id"=>['$in'=>$formIdList ] ] );
                // foreach ($forms as $key => $f) {
                //     unset($forms[$key]['_id']);
                // }
                $res["forms"] = $forms;

            }
            //cascade delete all subforms 
            if(isset($form["subForms"]))
            {
                foreach ($form["subForms"] as $ix => $fid) {
                    
                    $subF = PHDB::findOne( Form::COLLECTION, ["id" => $fid] );
                    //unset($subF["_id"]);

                    if(!$mode){
                        $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
                        $str .= "<h2 style='color:teal'>//collection forms (subForm) : id : ".$fid." </h2>";
                        $str .= "DATA forms : ".$fid."<br/>";
                        $str .= "<pre style='color:red'>".html_entity_decode(json_encode( $subF ) )."</pre>";
                        $str .= "</section>";
                    }                    
                }
            }

            if(!$mode && isset($form["parent"]))
                $str .= "<h3>//this form is used by ".count($form["parent"])." elements </h3>";


            
            if($answers = PHDB::find( Answer::COLLECTION , [ "form"=>$form_Id ],["name","answers","formId","session","user","email","source","priorisation","cterSlug","context","form", "collection" ] )) //"address","financementFait"
            {
                $docs = PHDB::find( Document::COLLECTION , [ "type"=>Answer::COLLECTION, "id"=>['$in'=>array_keys($answers) ]] );
                if(!$mode){
                    $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
                    $str .= "<h2 style='color:teal'>//collection answers : ".$fid." </h2>";
                    $str .= "DATA x".count( $answers )." answers"."<br/>";
                    $str .= "<pre style='color:red'>".html_entity_decode(json_encode( $answers ) )."</pre>";

                    $str .= "<h3>//Consommation answers : ".count( $docs )." x doc associés</h3>";
                    
                    if(!empty($docs)){
                        $Asize = 0;
                        foreach ($docs as $i => $d) {
                            $Asize += $d["size"];
                        }
                        $str .= "<h3 style='color:teal'>//answers size </h3> : ".$Asize."<br/>";    
                        $size += $Asize;
                        
                        $res[Document::COLLECTION] = (!empty($res[Document::COLLECTION])) ? array_merge($res[Document::COLLECTION],$docs) : $docs;

                    }
                    $str .= "</section>";
                } else {
                    foreach ($answers as $key => $d) {
                        //unset($answers[$key]['_id']);
                    }
                    $res["answers"] = $answers;                    
                    
                    if(!$mode){
                        $str .= "<h3>//Consommation Answers</h3>";
                        if(!empty($docs))
                            $res[Document::COLLECTION] = (!empty($res[Document::COLLECTION])) ? array_merge($res[Document::COLLECTION],$docs) : $docs;
                    }

                }
                
            }
        }

        
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // CONNECTED CMS BLOCKS
    // and documents 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if($cms = PHDB::find( Cms::COLLECTION , [ "parent.".$el['id'] => [ '$exists' => 1 ] ] ))
    {
        $docs = PHDB::find( Document::COLLECTION , [ "type"=>Cms::COLLECTION, "id"=>['$in'=>array_keys($cms) ]] );
        
        if(!$mode){
            $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
            $str .= "<h2 style='color:teal'>//collection cms </h2>";
            $str .= "DATA cms x".count($cms)."<br/>";
            $str .= "<pre style='color:red'>".html_entity_decode(json_encode( $cms ) )."</pre>";
            
            $str .= "<h3>//Consommation cms :  ".count($docs)." x doc associés</h3>";

            if(!empty($docs)){
                $Asize = 0;
                foreach ($docs as $i => $d) {
                    $Asize += $d["size"];
                }

                $str .= "<h3 style='color:teal'>//cms size : ".$Asize."</h3> ";    
                $size += $Asize;
                $res[Document::COLLECTION] = (!empty($res[Document::COLLECTION])) ? array_merge($res[Document::COLLECTION],$docs) : $docs;
            }
            $str .= "</section>";
        } else {
            $res["cms"] = $cms;                    
            
            if( !empty($docs) )
                $res[Document::COLLECTION] = (!empty($res[Document::COLLECTION])) ? array_merge($res[Document::COLLECTION],$docs) : $docs;
            

        }
        
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // CONNECTED POIs
    // and documents 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
     if($poi = PHDB::find( Poi::COLLECTION , [ "parent.".$el['id'] => [ '$exists' => 1 ] ] ))
    {
        $docs = PHDB::find( Document::COLLECTION , [ "type"=>Poi::COLLECTION, "id"=>['$in'=>array_keys($poi) ]] );
        
        if(!$mode){
            $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
            $str .= "<h2 style='color:teal'>//collection poi </h2>";
            $str .= "DATA poi x".count($poi)."<br/>";
            $str .= "<pre style='color:red'>".html_entity_decode(json_encode( $poi ) )."</pre>";
            
            $str .= "<h3>//Consommation poi :  ".count($docs)." x doc associés</h3>";

            if(!empty($docs)){
                $Asize = 0;
                foreach ($docs as $i => $d) {
                    $Asize += $d["size"];
                }

                $str .= "<h3 style='color:teal'>//poi size : ".$Asize."</h3> ";    
                $size += $Asize;
                $res[Document::COLLECTION] = (!empty($res[Document::COLLECTION])) ? array_merge($res[Document::COLLECTION],$docs) : $docs;
            }
            $str .= "</section>";
        } else {
            $res["poi"] = $poi;                    
            
            if( !empty($docs) )
                $res[Document::COLLECTION] = (!empty($res[Document::COLLECTION])) ? array_merge($res[Document::COLLECTION],$docs) : $docs;
            

        }
        
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // CONNECTED upload folder
    // and documents 
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if( !$mode && !$insert && isset($el["type"]) && isset($el['id']) && file_exists( "../../pixelhumain/ph/upload/communecter/".$el["type"]."/".$el['id'] )){
            $fi = new FilesystemIterator("../../pixelhumain/ph/upload/communecter/".$el["type"]."/".$el['id'], FilesystemIterator::SKIP_DOTS);
        $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
        $str .= "<h2 style='color:teal'>//Existing upload folder </h2>";
        $str .= "contains files : ".iterator_count($fi);
        $str .= "</section>";
    }

    if(!$mode && isset($res[Document::COLLECTION])){
        $str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
        $str .= "<h2 style='color:teal'>//collection ".Document::COLLECTION." </h2>";
        $str .= "DATA ".Document::COLLECTION." x".count($res[Document::COLLECTION])."<br/>";
        $str .= "<pre style='color:red'>".html_entity_decode(json_encode( $res[Document::COLLECTION] ) )."</pre>";
        $str .= "</section>";
    }

    if(!$mode){
        for($i = 0; ($size / 1024) > 0.9; $i++, $size /= 1024) {}
        $size = round($size, 2).['B','kB','MB','GB','TB','PB','EB','ZB','YB'][$i];
        $str .= "<h3 style='color:teal'>//TOTAL size : ".$size."</h3>"; 
    }
/* **************************************
*
*    INTERFACE and REndering 
*
***************************************** */

    //cms data
    // if($cms = PHDB::find( Cms::COLLECTION , [ "form"=>$formId ] ))
    // {
    //     if(!$mode){
    //$str .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
    //         $str .= "<h2 style='color:teal'>//collection answers : ".$fid." </h2>";
    //         $str .= "<pre style='color:red'>".html_entity_decode(json_encode( $answers ) )."</pre>";
    //     } else 
    //$str .= "</section>";
    //         $res["answers"] = $answers;                    
        
    // }
    if( !in_array($mode,["json"]) && !isset($save))
        $echos .= "<h1><a href='/costum/co/export'>COport : L'import Export transDocumental</a></h1>";

    $btns = "<a target='_blank' href='/costum/co/index/slug/".$slug."' class='btn btn-primary'>Goto Costum </a> ".
    "<a href='/costum/co/export/slug/".$slug."' class='btn btn-primary'>HTML</a> ".
    "<a href='/costum/co/export/slug/".$slug."/mode/json' class='btn btn-primary'>JSON</a> ".
    "<a href='/costum/co/export/slug/".$slug."/mode/json/save/1' class='btn btn-primary'>save json</a> ".
    "<a href='/costum/co/export/slug/".$slug."/save/1' class='btn btn-primary'>save html</a> ".
    "<a href='/costum/co/export/slug/".$slug."/install/1' class='btn btn-danger'>check install</a><br/>";
    $exports = "";



    //************************************
    // SAVE SAVE SAVE SAVE SAVE SAVE SAVE 
    // créer le dossier costumexport/export_xxx contenant une sauvegarde de tout les documents lié au slug 
    // avant de sauvegarder vous pouvez les visualiser avec /slug/xxx/mode/json
    //************************************
    if(isset($save))
    {
        if(!file_exists("../../modules/costumexport/export_".$slug))
            mkdir("../../modules/costumexport/export_".$slug);
        
        $str = "<h3>".date("Y/m/d h:m")." saved to ../../modules/costumexport/export_".$slug."/".$slug.".html</h3><br/>".$str;
        file_put_contents("../../modules/costumexport/export_".$slug."/".$slug.".html", $str );

        
        if(!file_exists("../../modules/costumexport/export_".$slug))
            mkdir("../../modules/costumexport/export_".$slug);
        $ressaved = [];
        //$echos .= $el["type"];
        foreach (array_keys($res) as $i => $col) 
        {

            file_put_contents("../../modules/costumexport/export_".$slug."/".$col.".json", json_encode( $res[$col] ) );
            $ressaved[] = "../../modules/costumexport/export_".$slug."/".$col.".json";

            //copy upload folders
//$res["dox"] = $res[Document::COLLECTION];
            if($col == "documents" ){
                foreach ( $res[Document::COLLECTION] as $id => $d ) {
                    //unset($cms[$key]['_id']);
//$res[$id] = $d;
                    if( isset($d["type"]) && isset($d['id']) && file_exists( "../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'] )){
                        //$res[$d['id']] = $d['type'];
                        if(!file_exists("../../modules/costumexport/export_".$slug."/upload"))
                            mkdir("../../modules/costumexport/export_".$slug."/upload" );
                        if(!file_exists("../../modules/costumexport/export_".$slug."/upload/".$d["type"]))
                            mkdir("../../modules/costumexport/export_".$slug."/upload/".$d["type"] );
                        if(!file_exists("../../modules/costumexport/export_".$slug."/upload/".$d["type"]."/".$d['id']))
                            mkdir("../../modules/costumexport/export_".$slug."/upload/".$d["type"]."/".$d['id'] );

                        $fi = new FilesystemIterator("../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'], FilesystemIterator::SKIP_DOTS);
                        
                        $res["upload"] = "copied upload files : ".iterator_count($fi);
                        

                        FileHelper::copy_folder( "../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'] , "../../modules/costumexport/export_".$slug."/upload/".$d["type"]."/".$d['id'] ); 
                    }

                }
            } 
            //copier les images pour l'element 
            else if( $el['type'] == $col && file_exists( "../../pixelhumain/ph/upload/communecter/".$col."/".$el['id'] )){
                $res["element"] = $col;
                    //$res[$d['id']] = $d['type'];
                    if(!file_exists("../../modules/costumexport/export_".$slug."/upload"))
                        mkdir("../../modules/costumexport/export_".$slug."/upload" );
                    if(!file_exists("../../modules/costumexport/export_".$slug."/upload/".$col))
                        mkdir("../../modules/costumexport/export_".$slug."/upload/".$col );
                    if(!file_exists("../../modules/costumexport/export_".$slug."/upload/".$col."/".$el['id']))
                        mkdir("../../modules/costumexport/export_".$slug."/upload/".$col."/".$el['id'] );

                    $fi = new FilesystemIterator("../../pixelhumain/ph/upload/communecter/".$col."/".$el['id'], FilesystemIterator::SKIP_DOTS);
                    
                    $res["uploadElement"] = "copied ELEMENT upload files : ".iterator_count($fi);
                    

                    FileHelper::copy_folder( "../../pixelhumain/ph/upload/communecter/".$col."/".$el['id'] , "../../modules/costumexport/export_".$slug."/upload/".$col."/".$el['id'] ); 
                }

            
        }


        $res["saved"] = $ressaved;

        $res["status"] = date("Y/m/d h:m")." saved to ../../modules/costumexport/export_".$slug."/*.json";
        
        $str .= Rest::json($res);
    } 
    //************************************
    // JSON JSON JSON JSON JSON JSON JSON 
    //permet de lire en JSON tout les datas et documents rattaché au slug
    //************************************
    else if( $mode == "json")
    {
        $str .= Rest::json($res);
    }
    //************************************
    // INSERT INSERT INSERT INSERT INSERT INSERT INSERT 
    //params : insert , nom du document collection.json à traiter
    // import dans la DB locale les data rattaché au slug et contenu dans costumexport/export_".$slug
    // traiter les collection un par un 
    //************************************
    else if( isset($insert))
    {
        $backup = false;
        $col = substr( $insert,0 ,stripos($insert,".json") );
        $echos .= "<h1 style='color:red'>INSERTING : <b style='color:purple; font-size:40px;'>".$col."</b> </h1><br>";
        if(file_exists( "../../modules/costumexport/export_".$slug."/".$insert) )
        {
            $jsonTxt = file_get_contents("../../modules/costumexport/export_".$slug."/".$insert);
            $fjson = json_encode($jsonTxt); 
            $fjson = json_decode($jsonTxt,true);
              //var_dump(substr_count($jsonTxt,"_id"));
              //var_dump($insert);
             //var_dump(array_keys($fjson)); 
            //var_dump($jsonTxt);
            
            
            
            //var_dump($fjson);
            if($backup && !file_exists("../../modules/costumexport/export_".$slug."/backup"))
                mkdir("../../modules/costumexport/export_".$slug."/backup");
        

            if(isset($fjson['_id']['$id']))
            {
                
                $id = $fjson['_id']['$id'];
                //remplacer les $id par des vrai Objet mongoId
                //sinon ca plante l'insert
                $fjson['_id'] = new MongoId($id);
                if( PHDB::count($col,["_id" => new MongoId($id)]) )
                {
                    $d = PHDB::findOne($col,["_id" => new MongoId($id)]);
                    if($backup){
                        file_put_contents("../../modules/costumexport/export_".$slug."/backup/".$col."_".$id.".json", json_encode( $d ) );
                    }
                    //$echos .= " check folder ../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id']." <br/>";
                    if( isset($d["type"]) && isset($d['id']) && file_exists( "../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'] ) ) {
                        FileHelper::rrmdir( "../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'] );
                        $echos .= "<b>- deleted folder ../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id']." <br/>";
                        
                         //temporary save copy before remove 
                    } 
                    $echos .= "- db.".$col.".id.".$id." exists removed</h1><br>";
                    PHDB::remove($col,["_id" => new MongoId($id)] );
                    

                 }

                 //return Rest::json($fjson);exit;
                 $check = Yii::app()->mongodb->selectCollection($col)->insert($fjson);
                    $echos .= "- ".$id." inserted <br/>";
            } 
            else {
                
                foreach ( $fjson as $id => $d) 
                {
                    if( PHDB::count($col,["_id" => new MongoId($id)]) )
                    {
                        $dexist = PHDB::find($col,["_id" => new MongoId($id)]);
                        if($backup){
                            file_put_contents("../../modules/costumexport/export_".$slug."/backup/".$col."_".$id.".json", json_encode( $dexist ) );
                        }
                        //$echos .= " check folder ../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id']." <br/>";
                        if( isset($d["type"]) && isset($d['id']) && file_exists("../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'] ) ){
                            FileHelper::rrmdir( "../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id'] );
                            $echos .= "<b>- [x] deleted folder ../../pixelhumain/ph/upload/communecter/".$d["type"]."/".$d['id']."</b> <br/>";
                        }
                        $echos .= "- db.".$col.".id.".$id." exists removed<br>";
                        PHDB::remove($col,["_id" => new MongoId($id)] );
                    }
                    
                    $d['_id'] = new MongoId($id);
                    //var_dump($d);
                    $check = Yii::app()->mongodb->selectCollection($col)->insert($d);
                    $name = "";
                    if( isset($d["id"]) ) 
                        $name = $d["id"];
                    else if(isset($d["name"]))
                        $name = $d["name"]; 

                    $echos .= "- inserted ".$id." name : ".$name."<br/>";
                }

            }
            if($col == "documents" && file_exists("../../modules/costumexport/export_".$slug."/upload")){
                $echos .= "<b>- [x] copied upload folder </b><br/>";
                FileHelper::copy_folder("../../modules/costumexport/export_".$slug."/upload", "../../pixelhumain/ph/upload/communecter/" ); 
            }
            

            $echos .= "<h1 style='color:red'>ELEMENT <b style='color:purple; font-size:40px;'>".$insert."</b><br/><a href='/costum/co/export/slug/".$slug."/install/1' class='btn btn-primary'>Back to INSTALL Check</a>";
        } else 
            $echos .= "<h1 style='color:red'>JSON FILE doesn't exist!!</h1><br>";

        return $echos;
    }
     //************************************
    // INSTALL INSTALL INSTALL INSTALL INSTALL INSTALL INSTALL 
    // basé sur le contenu du dossier costumexport/export_".$slug
    // permet de connaitre l'état de la data de la DB locale concernant le slug 
    // - différencie les documents à un ou plusieurs elements
    // - rapport des données installé
    // - bouton pour lancer insert
    // - Forcer une install 
    //************************************
    else if( isset($install)  )
    {
        $echos .= "<h1>Install Check : <a><b style='color:purple; font-size:40px;'>@".$slug." : ".$el['el']["name"]."</b></h1><br>";
        
        //verifier les elements qui sont à installer
        $echos .= "<a href='/costum/co/index/slug/".$slug."' target='_blank'>Tester le costum </a><br>";
        $echos .= "<b>Checking folder modules/costumexport/export_".$slug."</b><br><br>";

        if(file_exists( "../../modules/costumexport/export_".$slug ))
        {
            $ctuptoDate = true;
            foreach ( scandir( "../../modules/costumexport/export_".$slug ) as $i => $f ) 
            {
                if(stripos($f,".json") )
                {
                    //check if 
                    if($install == 1 || $install == $f){
                        $col = substr( $f,0 ,stripos($f,".json") );
                        $echos .= "<section  style='border:1px solid #000; padding:10px;margin:5px; '>";
                        $echos .= "<h2>CHECK INSTALL : <b style='color:purple; font-size:40px;'>".$col."</b></h2><br>";

                        $hasLastVersion = false;
                        $jsonTxt = file_get_contents("../../modules/costumexport/export_".$slug."/".$f);
                        $jsonTxt = json_encode($jsonTxt);
                        //$jsonTxt = str_replace('"', "'", $jsonTxt);
                        //$echos .= "last errors : ".json_last_error();
                        $fjson = json_decode($jsonTxt,true);
                        $fjson = json_decode($fjson,true);

                        //$echos .= "last errors : ".json_last_error();
                        //var_dump(substr_count($jsonTxt,"_id"));
                        //var_dump($f);
                        //var_dump($fjson);
                        if (is_string($fjson)) 
                        {
                            $echos .= "<h1 style='color:red'>Houston there's a problem on ".$f.", <br/>unable to decode json</h1><br>";
                            var_dump($fjson);
                            //$fjson = json_decode($fjson,true);
                            //exit;
                        }
                        $ids = [];
                        if(in_array($f,["answers.json","documents.json","forms.json","cms.json"]))
                        {
                            foreach (array_keys($fjson) as $i => $id) 
                            {
                                $ids[] = new MongoId($id);
                            }
                            $echos .= count($fjson)." documents to check"."<br>";
                            if(in_array($f,["forms.json","cms.json"])){
                                
                                $firstId = explode(",",implode(",",$ids) )[0];
                                $p = PHDB::findOne($col,["_id"=>new MongoId($firstId)],["parent"]);
                                if(isset($fjson[$firstId]["parent"])){
                                    $echos .= "parent id : ".array_keys($fjson[$firstId]["parent"])[0]."<br>";    
                                    $echos .= "<span style='color:red'>must be the same as slug element or make a manuel replace in export files </span><br>";
                                }
                            }
                        } else {
                            if(isset($fjson["_id"]['$id']))
                            {
                                $ids[] = new MongoId($fjson["_id"]['$id']);
                                $echos .= "only one element to check , id : ".$fjson["_id"]['$id']."<br>";
                                //if element with slug exists in slugs and type 
                                if(isset($fjson["slug"]) && !in_array($col, ["costum"])){
                                    $echos .= "[x] check if element exists with slug : ".$fjson["slug"]."<br>";
                                    $el = Slug::getElementBySlug($fjson["slug"]);
                                    if(isset($el)){
                                        $echos .= "Element exists on this DB : col : ".$el["type"].", id: ".$el["id"]."<br>";
                                        if(isset($fjson["slug"]))
                                            $echos .= "<a target='_blank' href='/co2#@".$fjson["slug"]."'>Check Existing Element Page for ".$fjson["slug"]."</a><br>";
                                        if($el["id"] != $fjson["_id"]['$id']){
                                            $echos .= "<span style='color:red'>ID is different !! <br>".
                                            "need to remap cms, forms parents<br>";

                                        }
                                    } else 
                                        $echos .= ">> No Element exists on this DB with this slug <br>";
                                }
                                
                            } else 
                                $echos .= "<h1 style='color:red'>NO _ID on this element</h1><br>";
                        }
                        //check if any other slugs exist
                        if($col == "slugs"){
                            $slugs = PHDB::find(Slug::COLLECTION, ["name"=>$slug]);
                            
                            if(!empty($slugs)){
                                foreach ($slugs as $i => $s) {
                                    $echos .= "same slug exists : type ".$s["type"].", id : ".$s["id"]."<br>";
                                }  
                            } else 
                                $echos .= ">> No such slug on this DB <br>";   
                        }

                        $where = [ "_id" => [ '$in' => $ids] ];
                        $check = PHDB::find($col,$where,["name","updated"]);
                        
                        //TODO : here we only check the id exists , we could do a date upadte check 
                        $hasLastVersion = ( count(array_keys($check)) == count($ids) ) ? true : false ;
                    
                        if($hasLastVersion) 
                        {
                            
                            $echos .= count(array_keys($check))." found ".$col." on this system <br/>".
                                " ---------------------------------- <br/>".
                                count(array_keys($ids))." installé(s)<br><br>".
                                "<b style='color:green'>You are up to date</b><br/>";
                            $echos .= "you can <a href='/costum/co/export/slug/".$slug."/insert/".$f."'>FORCE UPDATE</a><br><br>";
                            $ctuptoDate++;
                        } else {
                            $echos .= " <b><a href='/costum/co/export/slug/".$slug."/insert/".$f."' style='color:blue'>INSERT OR UPDATE</a></b><br><br>";
                            $ctuptoDate = false;
                        }

                        if($install == $f)
                            $echos .= "<a href='/costum/co/export/slug/".$slug."/install/1' class='btn btn-primary'>Back to INSTALL Check</a>";
                        $echos .= "</section>";
                    }
                }
            }
            if($ctuptoDate){
                $echos .= "<h1 style='color:green'>You are All up to date</h1><br/>".
                    "<a href='/costum/co/export/slug/".$slug."' class='btn btn-primary'>Back to analyze </a>".
                    "<a target='_blank' href='/costum/co/index/slug/".$slug."' class='btn btn-primary'>Check out the Costum </a>";
            }
        
        }  else 
            $echos .= "<h1 style='color:red'>No Folder exists for this slug !!</h1><br>";

        return $echos;
    }
     //************************************
    // LECTURE  LECTURE  LECTURE  LECTURE  LECTURE  LECTURE  LECTURE  
    // basé sur la base de donnée locale
    // permet de lister les éléments et la consommation rattaché 
    // - l'élement rattaché au slug : orga, event, project, person...
    // - costums
    // - forms
    // - answers
    //************************************
    else if(!$mode)
    {            
        $echos .= "<h1 style='color:red'>Analysons ".$slug." !!</h1><br>";
        return $echos.$btns.$exports.$str; 
    }
}
}
}