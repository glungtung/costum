<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coeurnumerique;
use CAction;
use CoeurNumerique;
use Rest;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CoeurNumerique::getEvent();
        
        return Rest::json($params);
    }
}