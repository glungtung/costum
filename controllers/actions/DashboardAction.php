<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;
use CAction;
use Ctenat;
use Organization;
use PHDB;
use Yii;

class DashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($sk=null,$tag=null,$tpl=null,$type=null)
    {
    	$controller = $this->getController();    	

    	$tpl = (!empty($tpl)) ? $tpl : "costum.views.co.dashboard";
    	$lists = null;
    	$blocks = [];
    	if( isset($controller->costum["lists"]) ) 
    		$lists = $controller->costum["lists"];
    	$title = "Tableau de bord";

    	if($type == "community"){
    		$title = "Community Observatory";

    		$lists = [
				"projects" =>[
					"title"=>"<i class='fa fa-2x fa-lightbulb-o'></i><br/>Projects",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"personne impliquées","icon"=>"group","type"=>"success"],
						["data"=>45,"name"=>"durée","type"=>"danger","icon"=>"clock"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"orgas" =>[
					"title"=>"<i class='fa fa-2x fa-users'></i><br/>Organizations",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"Pour","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"Contre","type"=>"danger","icon"=>"thumbs-down"],
						["data"=>45,"name"=>"Nombres de Décisions Validés","type"=>"danger","icon"=>"gavel"],
						["data"=>60,"name"=>"Nombres de Décisions Refusés","type"=>"danger","icon"=>"hand-stop-o"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"events" =>[
					"title"=>"<i class='fa fa-2x fa-calendar'></i><br/>Events",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"personne impliquées","icon"=>"group","type"=>"success"],
						["data"=>45,"name"=>"durée","type"=>"danger","icon"=>"clock"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"ressources" =>[
					"title"=>"<i class='fa fa-2x fa-cubes'></i><br/>Ressources",
					"blocksize"=>"3 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"Pour","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"Contre","type"=>"danger","icon"=>"thumbs-down"],
						["data"=>45,"name"=>"Nombres de Décisions Validés","type"=>"danger","icon"=>"gavel"],
						["data"=>60,"name"=>"Nombres de Décisions Refusés","type"=>"danger","icon"=>"hand-stop-o"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				
				"timeline" =>[
					"col12"=>true, 
					"title"=>"Point d'activité",
					"data" => [32,6,36,21,10],
					"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.line"
				],
				"activtiyType" =>[
					"title"=>"Type d'activité",
					"data" => [32,65,6,21,10],
					"lbls" => ["Dev","Graphisme","Fiancement","Dossier","Administratif"],
					"url"  => "/graph/co/dash/g/graph.views.co.bar"
				],
				"activtiyType2" =>[
					"title"=>"Type d'activité 2",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.polar"
				],
				"activtiyType3" =>[
					"title"=>"Type d'activité 3",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.radar"
				],
			];
			foreach ($lists as $ki => $list) 
			{
				$kiCount = 0;
				foreach ($list["data"] as $ix => $v) {
					if(is_numeric($v))
						$kiCount += $v;
					else 
						$kiCount ++;
				}
				$blocks[$ki] = [
					"title"   => $list["title"],
					"counter" => $kiCount,
				];
				if(isset($list["tpl"])){
					$blocks[$ki] = $list;
				}
				else 
					$blocks[$ki]["graph"] = [
						"url"=>$list["url"],
						"key"=>"graph".$ki,
						"data"=> [
							"datasets"=> [
								[
									"data"=> $list["data"],
									"backgroundColor"=> (isset($list["colors"])) ? $list["colors"] :Ctenat::$COLORS
								]
							],
							"labels"=> $list["lbls"]
						]
					];
			}

    	}
    	else if($tag && false){
    	}
    	else if( $lists ){ 
			
			$listData = [];
			$listLbls = [];
			foreach ($lists as $family => $lbls) 
			{
				$listData[$family] = [];
				$listLbls[$family] = [];
				$familyCount = 0;
				foreach ($lbls as $ix => $lbl) 
				{
					$fmc = PHDB::count( Organization::COLLECTION, array("source.key"=>$sk,"tags"=>$lbl ) );
					$listData[$family][] = $fmc;
					$familyCount += $fmc;
					$listLbls[$family][] = $lbl;
				}
				
				$blocks["pie".$family] = [
					"title"   => $family,
					"counter" => $familyCount,
					"graph" => [
						"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/size/S",
						"key"=>"pieMany".$family,
						"data"=> [
							"datasets"=> [
								[
									"data"=> $listData[$family],
									"backgroundColor"=> Ctenat::$COLORS
									]
								],
							"labels"=> $listLbls[$family]
						]
					]
				];
			}
		
		}
    	$params = [
				"title" => $title,
	    		"blocks" 	=> $blocks
	    	];	
    	 
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
    		$controller->layout = "//layouts/empty";
    		return $controller->renderPartial($tpl, $params);
        }
    	
    }
}