<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere;
use CAction;
use City;
use Organization;
use PHDB;
use Slug;
use Yii;
use Zone;
use function ctype_digit;

class UpdateCategoryAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $costum = PHDB::findOne("organizations", array("slug" => $_POST["slug"]));

        $categories = [];

        if(isset($_POST["category"])){
             $category = $_POST["category"];
        }else{
            $category = [];
        }

        if(!empty($_POST['filters'])){
            $mergedApp = array_merge($_POST['filters'], $costum["costum"]["app"]);

            $categories = PHDB::update("organizations", ['_id' => $costum["_id"]], ['$set' =>array("costum.typeObj.organization.dynFormCostum.beforeBuild.properties.category" => $category, "costum.app" => $mergedApp)]);
        }
        
        return Rest::json($categories);
    }
}