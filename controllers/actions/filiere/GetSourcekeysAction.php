<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere;
use PHDB;
use Rest;
use Yii;

class GetSourcekeysAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $element = PHDB::findOne($_POST["type"],  array('slug' => $_POST["slug"]));
        return Rest::json($element);
    }
}