<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere;
use PHDB;
use Yii;
use Rest;
use Project;
use Crowdfunding;
use Citoyen;
use MongoId;

class GetRelatedElementsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $request = Yii::app()->request;
        $id = $request->getPost("id");
        $elementType = $request->getPost("element", "");
        $source=$_POST["source"];
        $priority =isset($_POST["priority"]) ? $_POST["priority"] : null;
        $category =isset($_POST["category"]) ? $_POST["category"] : null;
        $elements = array();

        //var_dump($_POST);

        if($elementType=="projects"){
            $query = array('parent.'.$id => array('$exists' => true));
            $field = ['_id', 'name', 'slug', 'profilImageUrl', 'shortDescription', 'collection'];
            if(!empty($category)){
                $query["category"]=$category;
                array_push($field,"category");
            }

            // Fields :, ['name', 'slug', 'profilImageUrl', 'shortDescription', 'collection']
            $elements = PHDB::find(Project::COLLECTION, $query, $field );
        }

        if($elementType=="crowdfunding"){
            // Fields :, ['name', 'slug', 'profilImageUrl', 'shortDescription', 'collection']
            $query = array("type"=>"campaign","source.key"=>$source);
            if (isset($priority)){
                $query["priority"]=$priority;
            }

            //var_dump($query);exit;
            
            $elements = PHDB::find(Crowdfunding::COLLECTION, $query);
            //var_dump($elements);

            //var_dump($elements);exit;
            foreach ($elements as $key => $value){
                $elements[$key]=Crowdfunding::getCampaignById($key);

                $elt=PHDB::findOne(Project::COLLECTION,array("_id" => new MongoId(key($value["parent"]))));
                
                $elements[$key]["project"]=[];
                $elements[$key]["project"]=$elt;

                //$elements[$key]=array_merge($value,$elt);
                
            } 

        }

        if($elementType=="citoyens"){
            // Fields :, ['name', 'slug', 'profilImageUrl', 'email', 'collection']
            $elements = PHDB::find(Citoyen::COLLECTION, array('links.memberOf.'.$id => ['$exists' => true]), ['_id', 'name', 'slug', 'profilImageUrl', 'email', 'collection']);
        }

        return Rest::json(array('elt' => $elements, 'result'=>(count($elements)!=0)));
    }
}