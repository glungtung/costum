<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\altFramadate;

use CAction, Rest, PHDB, Person, MongoId;
class GetUserByIdAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($id = null){
		$controller = $this->getController();
        if(isset($id)) {
		    $params = PHDB::find(Person::COLLECTION, array('_id' => new MongoId($id)), array("name", "username", "slug"));
        } else $params =[];

		return Rest::json($params);
	}
}
?>