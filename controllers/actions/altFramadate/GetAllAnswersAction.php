<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\altFramadate;

use CAction, Rest, PHDB, Form, Person, MongoId;
class GetAllAnswersAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($form = null){
		$controller = $this->getController();
        $res = array();
        if(isset($form)) {
		    $params = PHDB::find(Form::ANSWER_COLLECTION, array('form' => $form));
            if(count($params) > 0) {
                foreach($params as $elem) {
                    $elem['userinfo'] = PHDB::find(Person::COLLECTION, array('_id' => new MongoId($elem['user'])), array("name", "username", "slug"));
                    array_push($res, $elem);
                }
            }
        }

		return Rest::json($res);
	}
}
?>