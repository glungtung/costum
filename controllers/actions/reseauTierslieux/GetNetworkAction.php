<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\reseauTierslieux;

use CAction, ReseauTierslieux, Rest;
class GetNetworkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = ReseauTierslieux::getNetwork($_POST["costumSlug"]);
        
        return Rest::json($params);
    }
    	
}
    	
