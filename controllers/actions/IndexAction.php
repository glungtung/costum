<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;
use Authorisation;
use CacheHelper;
use CO2;
use Costum;
use PixelHumain\PixelHumain\modules\costum\components\CostumPage;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    //$id 
    // if given with a type it concerns an element
    // otherwise it's just costum can be the slugname or the digital id 
    //$type : 
        // if given with an id it concerns an element
    //$slug 
        // it's the slug of an element
    //$init this action can render a result for a page request
    
    public function run($id=null,$type=null,$slug=null, $edit=false, $view=null,$page=null,$test=null, $h=null, $hp=null)
    { 	
        //CacheHelper::flush();die;
        //TODO faire tout ca dans Costum::init()
    	//$activeCOs = array("cocampagne");
    	$controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        if(empty(CacheHelper::get("appConfig")))
          CacheHelper::set("appConfig", CO2::getThemeParams());
        
        $controller->appConfig = CacheHelper::get("appConfig");
        

         if($slug || $id)
             $slugCache = !empty($slug) ? $slug : $id;
         else {
            $slugCache = !empty(@$_GET["host"]) ? Costum::getSlugByHost($_GET["host"]) : null;
         }
        
        if(!$controller->cacheCostumInit($slugCache, $edit)){
            $returnCostum = Costum::init ($controller,$id,$type,$slug,$edit, $view,$test,"costum/controllers/actions/IndexAction.php");
            $controller->cacheCostumInit($slugCache,$edit, $returnCostum, true);
            $controller->costum["editMode"]=false;
            if(!empty($edit) && $edit && Authorisation::isCostumAdmin())
                $controller->costum["editMode"]=true;
        
        }else{
            $controller->costum["editMode"]=false;
            if(!empty($edit) && $edit && Authorisation::isCostumAdmin())
                $controller->costum["editMode"]=true;
        
        }
                // var_dump($edit);exit;
            
        $controller->meta($controller->costum);
        //var_dump($this->getController()->appConfig);
        //var_dump($this->getController()->costum);

        //$canEdit = false;

        //if( isset(Yii::app()->session["userId"])  && isset($controller->costum["contextType"]) && isset($controller->costum["contextId"]) )
          //  $canEdit = Authorisation::canEditItem(Yii::app()->session["userId"],@$controller->costum["contextType"], @$controller->costum["contextId"]);
        
        //TODO if no canEdit et test exist then 
          //redirect unTpl 

        $params = array();//["canEdit" => $canEdit];
        if( isset($test) ){
            $params["tpl"]=$id;
            $params["test"]=$test;
        }
        //use another page inside the same costum folder
    	if($page && $controller->costum["welcomeTpl"])
        {
            //pages are relative to the current context
            //will sit in the same destination foilder as the welcomeTpl of the costum
            $path = explode(".", $controller->costum["welcomeTpl"]);
            array_pop($path);
            $path[] = $page;
            if(Yii::app()->request->isAjaxRequest){
                return $controller->renderPartial(implode(".", $path), $params, true);
            }
            else
              return $controller->render(implode(".", $path), $params, true);
        }
	  	else if( isset($controller->costum["welcomeTpl"])){
            $page = "";
            $appPages = $controller->appConfig["pages"];

            if($h !== null){
                if(trim($h) !== ""){
                    $page = $h;
                }else{
                    $redirectLevel = isset($_SESSION["userId"]) ? "logged":"unlogged";
                    if(isset($appPages["#app.index"]["redirect"][$redirectLevel]))
                        $page = $appPages["#app.index"]["redirect"][$redirectLevel];
                }
            }

            //ne fait pas le rendu côté serveur du page s'il ne s'aggit page d'une app de type view
            if(!isset($appPages["#".$page]["hash"]) || $appPages["#".$page]["hash"] != "#app.view")
                $page = "";

            /*
                ceci permet de bien verifier si le hash correspond bien à la page
                par exemple le cas pour "#accueil" qui correspond à la page "welcome"
            */
            $is_app_view = (isset($appPages["#".$page]["hash"]) && $appPages["#".$page]["hash"] == "#app.view");
            $is_app_view_page = (isset($appPages["#".$page]["urlExtra"])) && preg_match("/^(\/page\/)\w+$/", $appPages["#".$page]["urlExtra"]);
            if($is_app_view && $is_app_view_page){
                $page = array_reverse(explode("/", $appPages["#".$page]["urlExtra"]))[0];
            }

            //initialize costum page
            $costumPage = new CostumPage($controller, $controller->costum);
            
            if(Yii::app()->request->isAjaxRequest)
                return $costumPage->renderPartial($page, null, null, $hp);
            
            return $costumPage->render($page, null, null, $hp);
        }
    }
}


