<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;
use CTKAction;
use Rest;

class TestAction extends CTKAction
{
    public function run()
    {
    	//echo "access token :".$_SESSION['access_token'];
    	
    	$user = $this->userLogguedAndValid();
    	$user["token"] = $this->getCheckBearerToken();
    	return Rest::json( $user );

    	// $this->getController()->layout = "//layouts/empty";
   		// $this->getController()->render("test");


    }
}