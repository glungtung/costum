<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use Form;
use PHDB;
use Rest;
use Yii;

class DuplicateFormAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null,$type= null,$slug = null){
        $controller = $this->getController();

        $contextSlug = $_POST["contextSlug"];
        $contextId = $_POST["contextId"];
        $contextType = $_POST["contextType"];
        $userId = Yii::app()->session["userId"];

        //$tierlieu = PHDB::findOne(Organization::COLLECTION, $contextSlug)

        $form = PHDB::findOneById(Form::COLLECTION, array('source.key' => $contextSlug));

        $subForms = PHDB::find(Form::COLLECTION, array('id' => ['$in'=>$form["subForms"]]));

        $form["parent"] = [
            $contextId => ['type'=>$contextType, 'name'=>$contextSlug]
        ];

        $form["creator"] = $userId;

        $newSubForms = array();
        $newSubFormsId = array();
        foreach ($subForms as $key => $value) {
            unset($value["_id"]);
            $value["creator"] = $userId;
            $value["id"] = $contextSlug.$value["id"];
            array_push($newSubFormsId, $value["id"]);
            array_push($newSubForms, $value);
        }

        unset($form["_id"]);

        $data = PHDB::insert(Form::COLLECTION, $form);
        PHDB::insert(Form::COLLECTION, $newSubForms);
        
        return Rest::json($data);
    }
}