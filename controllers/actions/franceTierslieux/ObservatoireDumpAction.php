<?php


namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use CAction;
use City;
use Organization;
use PHDB;
use Slug;
use Yii;
use Zone;
use Form;

class ObservatoireDumpAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($slug=null,$tag=null,$action=null,$format=null)
    {
        $mapSub=array(
            "franceTierslieux2322023_1440_11"=> "INTRODUCTION",
            "franceTierslieux1522023_1341_0"=> "AUTORISATION D’UTILISATION DES DONNÉES",
            "franceTierslieux1522023_1352_1"=> "FICHE D'IDENTITÉ",
            "franceTierslieux1522023_146_2"=> "INFORMATIONS GÉNÉRALES ",
            "franceTierslieux1522023_1549_3"=> "FONCIER",
            "franceTierslieux1822023_1721_4"=> "VOS ACTIVITÉS",
            "franceTierslieux1922023_1231_5"=> "PUBLICS",
            "franceTierslieux1922023_1231_6"=> "RESSOURCES HUMAINES",
            "franceTierslieux1922023_1417_7"=> "GOUVERNANCE",
            "franceTierslieux1922023_1523_8"=> "PARTENARIATS",
            "franceTierslieux2022023_732_9"=> "MODÈLE ÉCONOMIQUE",
            "franceTierslieux2022023_753_10"=> "PERSPECTIVES FUTURES"
        );

        $subForms = [ 
            "franceTierslieux2322023_1440_11", 
            "franceTierslieux1522023_1341_0", 
            "franceTierslieux1522023_1352_1", 
            "franceTierslieux1522023_146_2", 
            "franceTierslieux1522023_1549_3", 
            "franceTierslieux1822023_1721_4", 
            "franceTierslieux1922023_1231_5", 
            "franceTierslieux1922023_1231_6", 
            "franceTierslieux1922023_1417_7", 
            "franceTierslieux1922023_1523_8", 
            "franceTierslieux2022023_732_9", 
            "franceTierslieux2022023_753_10"
        ];
        
        $bySteps = array();
        $unfinished=0;
        $finished=0;
        // * les lieux qui ont commencé à répondre mais pas fini
        // * les lieux qui ont répondu
        // * les lieux qui ont pas répondu
        $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form"=>"63e0a8abeac0741b506fb4f7","answers"=>['$exists'=>1], "draft"=>['$exists'=>0]));
        
        // Here foreach php
        foreach($answers as $key => $ans){
            if(isset($ans["step"])){
                if(!isset($bySteps[$mapSub[$ans["step"]]])){
                    $bySteps[$mapSub[$ans["step"]]]=0;
                }
                $bySteps[$mapSub[$ans["step"]]]++;
            }   
            
        }
        // bySteps["Réponse commencée et non finie"]=unfinished;
        // bySteps["Réponses finies"]=finished;
       // print_r($bySteps);

        
        
        
        // Second script

        $byRegions=array();
        $unfinished=0;
        $finished=0;
        // * les lieux qui ont commencé à répondre mais pas fini
        // * les lieux qui ont répondu
        // * les lieux qui ont pas répondu
        
        foreach($answers as $key => $ans){
            if(isset($ans["step"])){
        //        if(isset($byRegions[mapSub[ans.step]]=="undefined"){
        //             by$byRegions[mapSub[ans.step]]=0;
        //        }
        //         by$byRegions[mapSub[ans.step]]++;   
            
            if(isset($ans["answers"]["franceTierslieux2022023_753_10"]) && isset($ans["answers"]["franceTierslieux2022023_753_10"]["franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s"])){
                $finished++;    
            }else{
                $unfinished++;
            }

            if(isset($ans["links"]["organizations"])){
                $orga = $ans["links"]["organizations"];
                $tierslieu = PHDB::findOneById(Organization::COLLECTION , array_key_first($orga));
                $region = "Adresse non précisée" ;
                
                if(isset($tierslieu)){
                    if(isset($tierslieu["address"]) && isset($tierslieu["address"]["level3Name"])){
                        $region = $tierslieu["address"]["level3Name"]; 
                    }    
                    if(!isset($byRegions[$region])){
                        $byRegions[$region] = array();
                    }
    
                    if(isset($ans["answers"]["franceTierslieux2022023_753_10"]) && isset($ans["answers"]["franceTierslieux2022023_753_10"]["franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s"])){
                        if(!isset($byRegions[$region]["Réponses complètes"])){
                            $byRegions[$region]["Réponses complètes"] = [];
                            $byRegions[$region]["Total réponses complètes"]=0;
                        }
                        $byRegions[$region]["Total réponses complètes"]++;   
                        array_push($byRegions[$region]["Réponses complètes"], $tierslieu["name"]);
                    }else{
                        if(!isset($byRegions[$region]["Réponses non complètes"])){
                            $byRegions[$region]["Réponses non complètes"]=array();
                            $byRegions[$region]["Total réponses non complètes"]=0;
                        }
                        $byRegions[$region]["Total réponses non complètes"]++;
                        $byRegions[$region]["Réponses non complètes"][$tierslieu["name"]]=[
                                "Email" => isset($tierslieu["email"]) ? $tierslieu["email"] : "Non reseigné",
                                "Téléphone" => (isset($tierslieu["telephone"]) && $tierslieu["telephone"]!="[object Object]") ? $tierslieu["telephone"] : "Non reseigné",
                                "Ville" => (isset($tierslieu["address"]) && isset($tierslieu["address"]["addressLocality"])) ? $tierslieu["address"]["addressLocality"] : "Non reseignée",
                                "Département" => (isset($tierslieu["address"]) && isset($tierslieu["address"]["level4Name"])) ? $tierslieu["address"]["level4Name"] : "Non reseigné",
                        ];
                    }
                }
            }
            
        }      
        }
        // byRegions["Réponse commencée et non finie"]=unfinished;
        // byRegions["Réponses finies"]=finished;
        //var_dump($byRegions);

        
        $tpl = "costum.views.custom.franceTierslieux.dumpobs";
        $this->getController()->layout = "//layouts/empty";
        //return $this->getController()->render($tpl,$params);
        return $this->getController()->render($tpl, array("bySteps" => $bySteps, "byRegions" => $byRegions,),true);
    }
}