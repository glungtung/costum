<?php


namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use CAction;
use City;
use Organization;
use PHDB;
use Slug;
use Yii;
use Zone;
use Form;

class DiagnoAnswersAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($form=null)
    {
        $answersBySteps = array();
        $coformId = "63e0a8abeac0741b506fb4f7"; // recensement TL 2023
        if($form!=null){
            $coformId = $form;
        }
        // Form data
        $coform = PHDB::findOneById( Form::COLLECTION, $coformId, ["name", "params", "subForms"] );
        // Steps forms
        $subForms = PHDB::find(Form::COLLECTION, array('id'=>array('$in'=>$coform["subForms"])));

        $answersAllCount = PHDB::count(Form::ANSWER_COLLECTION, array(
            "form" => $coformId,
            "draft"  => ['$exists' => false], 
            "answers"  => ['$exists' => true]
            )
        );
        // required inputs
        $requiredInputs = array();
        foreach ($coform["params"] as $key => $value) {
            if(str_contains($key, "validateStep") && isset($value["inputList"]) && $value["inputList"]!=null){
                $requiredInputs = array_merge($requiredInputs, $value["inputList"]);
            }
        }
        
        foreach ($subForms as $fkey => $fvalue) {
            $stepKey = $fvalue["id"];
            $question = array(
                "id" => $stepKey,
                "step" => $fvalue["name"],
                "answer" => array()
            );

            foreach ($fvalue["inputs"] as $ikey => $ivalue) {
                $type = ["multiRadio", "radiocplx", "multiCheckboxPlus", "checkboxcplx"];
                $e = explode('.', $ivalue["type"]);
                $prefix = $e[(count($e)-1)];
                if(!in_array($prefix, $type)){
                    $prefix = "";
                }
                
                if(!str_contains($ivalue["type"], "validateStep") && !str_contains($ivalue["type"], "sectionDescription") && !str_contains($ivalue["type"], "sectionTitle")){
                    $count = 0;
                    if(str_contains($ivalue["type"], "finder")){
                        $count = PHDB::count(Form::ANSWER_COLLECTION, array(
                            "form" => $coformId,
                            "links.organizations"  => ['$exists' => true],
                            "draft"  => ['$exists' => false])
                        );
                    }else{
                        $count = PHDB::count(Form::ANSWER_COLLECTION, array(
                            "form" => $coformId,
                            "answers.$stepKey.$prefix$ikey"  => ['$exists' => true],
                            "draft"  => ['$exists' => false])
                        );
                    }
                    $is_required = in_array($ikey, $requiredInputs)?"Oui":"Non";
                    $relevance = "";

                    if(($count/$answersAllCount)==0){
                        $relevance = "null";
                    }else if(0.2>($count/$answersAllCount)){
                        $relevance = "bas";
                    }else if(0.6>($count/$answersAllCount)){
                        $relevance = "moyenne";
                    }else{
                        $relevance = "haut";
                    }

                    $question["answer"][$ikey] = array(
                        "question" => $ivalue["label"],
                        "nbAnswers" => $count,
                        "type" => $ivalue["type"],
                        "relevance" => $relevance,
                        "path" =>"answers.$stepKey.$prefix$ikey",
                        "is_required" => $is_required
                    );
                }
            }
            array_push($answersBySteps, $question);
        }
        
        $tpl = "costum.views.custom.franceTierslieux.diagnoanswers";
        $this->getController()->layout = "//layouts/empty";
        return $this->getController()->render($tpl, array("answersBySteps" => $answersBySteps, "nbTotalAnswers" => $answersAllCount),true);
    }
}