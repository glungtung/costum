<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use CAction;
use City;
use Organization;
use PHDB;
use Slug;
use Yii;
use Zone;
use function ctype_digit;

class DashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($slug=null,$tag=null,$action=null,$format=null)
    {

    	$controller = $this->getController();
    	$params=array("page"=>"pie");


    	  
    	$tpl = "costum.views.custom.franceTierslieux.dashboard";

        $checkedSlug = isset($_POST["costumSlug"])?$_POST["costumSlug"]:$slug;
    
    //compteur total
        $where = array('$or' =>
                    array(
                        array("source.key" => $checkedSlug),
                        array("reference.costum"=> array('$in' => 
                                                            array($checkedSlug)
                                                        )
                        )
                    )
                );

        $orga = PHDB::find(Organization::COLLECTION,$where);
        $countOrga=0;
        $nbComp=0;
        $nbFabrique = 0;
        $nbFabnum = 0;
        $area = [];
         foreach ($orga as $id => $value){
            
            if(isset($value["tags"])) {
                // Compteur compagnon
                if(in_array("Compagnon France Tiers-Lieux",$value["tags"])){
                    $nbComp++;
                }

                //compteur Fabriques
                if(in_array("Fabrique de Territoire",$value["tags"])){
                    $nbFabrique++;
                }
                else{
                    if(in_array("Fabrique Numérique de Territoire",$value["tags"])){
                    $nbFabnum++;
                    }
                }

                //superficie
                foreach($value["tags"] as $tags){
                        if (ctype_digit($tags)){
                            array_push($area,$tags);
                                       
                        }    
                    } 
            }

            //compteur orga  
            $countOrga++;
         }

     
         
         
      // debut label
        
      
        $totFab = $nbFabnum + $nbFabrique ;
        $fabPart= round($totFab / $countOrga * 100) ;
        $nbFabrique = strval($nbFabrique);
        $nbFabnum = strval($nbFabnum);
        $fabLabel = ["Fabrique de Territoire","Fabrique Numérique de Territoire"];
        $fabValue= [$nbFabrique,$nbFabnum];
        $fabColor=["#E3A297","#B6325C"];
      // fin label


       //debut TypePlace
        $colorPlace = [
    //"#0A2F62",
    "#0B2D66",
    "#064191",
    "#2C97D0",
    "#16A9B1",
    "#0AA178",
    "#74B976",
    // "#0AA178",
    // "#16A9B1",
     "#2A99D1"
    // "#064191",
    // "#0B2E68"
    ];

       $typePlace = ["Coworking",
            "Ateliers artisanaux partagés",
            "Fablab / Atelier de Fabrication Numérique",
            "Tiers-lieu culturel",
            "Tiers-lieu agricole",
            "Cuisine partagée / Foodlab",
            "LivingLab / Laboratoire d’innovation sociale"
        ];

        $nbByTypes = [];


            foreach ($typePlace as $type){
                $nbType=0;
                foreach($orga as $id => $value){
                    if(isset($value["tags"])) {
                        if(in_array($type,$value["tags"])){
                            $nbType++;
                        }
                    }
                }
                $nbType=strval($nbType);
                array_push($nbByTypes, $nbType);
            }

       //Fin TypePlace

        //superficie debut
        
            
            $count = count($area);
                             sort($area);
                            $mid = floor(($count-1)/2);
                            $median = ($area)?($area[$mid]+$area[$mid+1-$count%2])/2:0;


        //superficie fin 


        //SERVICE DEBUT
        $services=[
            "Accompagnement des publics",
            "Action sociale",
            "Aiguillage / Orientation",
            "Art thérapie",
            "Bar / café",
            "Boutique / Épicerie",
            "Coopérative d’Activités et d’Emploi / Groupement d’employeur",
            "Cantine / restaurant",
            "Centre de ressources",
            "Chantier participatif",
            "Complexe évènementiel",
            "Conciergerie ",
            "Domiciliation",
            "Espace de stockage",
            "Espace détente",
            "Espace enfants",
            "Formation / Transfert de savoir-faire / Éducation",
            "Habitat",
            "Incubateur",
            "Lieu d’éducation populaire et nouvelles formes d’apprentissage ",
            "Maison de services au public / France Services",
            "Marché",
            "Média et son",
            "MediaLab",
            "Médiation numérique",
            "Pratiques culturelles ou artistiques",
            "Démarches de création partagée",
            "Action culturelle, médiation artistique",
            "Pépinière d'entreprises",
            "Point d'appui à la vie associative",
            "Point Information Jeunesse",
            "Point d’information touristique",
            "Résidences d'artistes",
            "Ressourcerie / recyclerie",
            "point collecte de déchets ou produits usagers ",
            "Service enfance-jeunesse",
            "Services liés à la mobilité"
        ];
$countService = [];
        foreach($services as $service){
            $count=0;
            foreach ($orga as $id => $value){
                if(isset($value["tags"])) {
                    if (in_array($service,$value["tags"])){
                        $count++;
                    }

                }
            }
            array_push($countService,$count);
        }
        $countedServices = array_combine($services,$countService);
        asort($countedServices);
        $ranked=count($countedServices);

        $majorServices=array_slice($countedServices,-5,5);
        $majorServicesVal=array_values($majorServices);
        $majorServicesLab=array_keys($majorServices);
        $colorMajServ = ["#0AA178","#16A9B1","#2A99D1","#064191","#0B2E68"];

        $minorServices=array_slice($countedServices,0,5);
        $minorServicesVal=array_values($minorServices);
        $minorServicesLab=array_keys($minorServices);
        $colorMinServ = ["#0AA178","#AC6A21","#AA16B1","#7588B9","#88C7AC"];

        //var_dump("<pre>",$majorServices,"</pre>");    exit;                      

        //SERVICE fin

     // Management Model

   //     $typeModel = Yii::app()->request->baseUrl["lists"]["manageModel"];
        $typeModel = [
            "Association",
            "Collectif citoyen",
            "SARL-SA-SAS",
            "SCIC-SCOP",
            "Société Publique Locale",
            "Pôle d’Equilibre Territorial Rural",
            "Département",
            "Région",
            "Groupement d'Intérêt Public",
            "Intercommunalité",
            "Université",
            "Collège",
            "Lycée"
        ];
        $orderedModel = [
                "Education" => [
                                    "Université",
                                    "Collège",
                                    "Lycée",
                                    "Ecole"
                                ],

                "Public" => [
                  "Pôle d’Equilibre Territorial Rural",
                  "Département",
                  "Région",
                  "Société Publique Locale",
                  "Groupement d'Intérêt Public",
                  "EPIC ",
                  "Intercommunalité"
                ],
                "Association" => [
                    "Association",
                    "Collectif citoyen",
                    "Organisation d’utilité publique"
                ],

                "Coopérative" => [
                    "SCIC-SCOP"
                ],
                "Privé" => [
                "SARL-SA-SAS"
                ]
            ];

$nbByModels = [];
// $education = 
// $public = 0;
// $addociation= 0;
// $cooperation= 0;
// $prive= 0;

    foreach ($typeModel as $model){
        $nbModel=0;
        foreach($orga as $id => $value){
            if(isset($value["tags"])) {
                if(in_array($model,$value["tags"])){
                    $nbModel++;
                }
            }
        }
        array_push($nbByModels, $nbModel);
    }

$education = $nbByModels[10]+$nbByModels[11]+$nbByModels[12];
$education=strval($education);
$public = $nbByModels[4]+$nbByModels[5]+$nbByModels[6]+$nbByModels[7];$nbByModels[8]+$nbByModels[9];
$public=strval($public);
$association=$nbByModels[0]+$nbByModels[1];
$association=strval($association);
$cooperation=$nbByModels[3];
$cooperation=strval($cooperation);
$prive=$nbByModels[2];
$prive=strval($prive);

$valueModel=[$education,$public,$association,$cooperation,$prive];

$labelModel=["Enseignement","Public","Association","Coopérative","Privé"];

$colorsModel = [
    // "#0A2F62",
    "#0B2D66",
    //"#064191",
    "#2C97D0",
    // "#16A9B1",
     "#0AA178",
    // "#74B976",
     //"#0AA178",
    // "#16A9B1",
     "#2A99D1",
     "#064191"
    // "#0B2E68"
    ];
// fin managementModel


    //début ZONE

   $colorZone = [
    // "#0A2F62",
    "#0B2D66",
    //"#064191",
    "#2C97D0",
    // "#16A9B1",
     "#0AA178",
    // "#74B976",
     //"#0AA178",
    // "#16A9B1",
    // "#2A99D1",
     "#064191"
    // "#0B2E68"
    ];
    $typeZone = [
            "En agglomérations",
            "En métropole",
            "En milieu rural",
            "En ville moyenne (entre 20000 et 100000 habitants)"
        ] ;
$nbByZones = [];


    foreach ($typeZone as $zone){
        $nbZone=0;
        foreach($orga as $id => $value){
            if(isset($value["tags"])) {
                if(in_array($zone,$value["tags"])){
                    $nbZone++;
                }
            }
        }
        $nbZone=strval($nbZone);
        array_push($nbByZones, $nbZone);
    }

    // FIn ZONE


// debut region


    $colorsRegion = [
    // "#0A2F62",
    "#0B2D66",
    //"#064191",
    "#2C97D0",
    // "#16A9B1",
     "#0AA178",
     "#AC6A21",
     "#AA16B1",
     "#7588B9",
     "#88C7AC",
     "#E6BE8B",
     "#6845F4",
     "#74B976",
     "#0AA178",
     "#16A9B1",
     "#2A99D1",
     "#064191",
     "#0B2E68",
     "#5A4189",
     "#2509EE",
     //"#070C20",
     "#ED0EF4"

    ];
$typeRegion = ["Auvergne-Rhône-Alpes","Bourgogne-Franche-Comté","Bretagne","Centre-Val de Loire","Corse","Grand Est","Hauts-de-France","Normandie","Nouvelle-Aquitaine","Occitanie","Pays de la Loire","Provence-Alpes-Côte d'Azur","Île-de-France","Guyane","Guadeloupe","Martinique","Réunion","Mayotte"]; 

$parent = Slug::getElementBySlug($checkedSlug);
//var_dump($parent["el"]);exit;
 //var_dump($parent["el"]["level"]);exit;
$level=3;

if (isset($parent["el"]["level"]) && $parent["el"]["level"]!="level1"){
    $level=$parent["el"]["level"];
    $level=intval(substr($parent["el"]["level"],-1))+1;
    $level=(string)$level;
    //var_dump($level);exit;

    
}   

$parentLevel=(isset($parent["el"]["level"])) ? intval(substr($parent["el"]["level"],-1)) : "1" ;
//var_dump($parentLevel);exit;
//var_dump($parent["el"]["address"]["level".$parentLevel."Name"]);exit;

$nbByRegions = [];
$whereReg=array('$and' => array(
                array("level"=>$level),
                array('$or'=>array(
                    array("level".$parentLevel."Name"=>strtoupper($parent["el"]["address"]["level".$parentLevel."Name"])),
                    array("level".$parentLevel."Name"=>$parent["el"]["address"]["level".$parentLevel."Name"])))));

$collec=Zone::COLLECTION;

if($level==6){
    $whereReg=array("level".$parentLevel=>$parent["el"]["address"]["level".$parentLevel]);
    $collec=City::COLLECTION;
}

//var_dump($whereReg);exit;
$regions=PHDB::find($collec,$whereReg,array("name"));
//var_dump($regions);exit;
$zones=[];
foreach ($regions as $k =>$v){
    
    $nbRegion=0;
        foreach($orga as $id => $value){
            if(isset($value["address"]) && isset($value["address"]["level".$level."Name"]) && $level!=6) {
                if($v["name"]==$value["address"]["level".$level."Name"]  ){
                    $nbRegion++;
                }
            }
            else if($level==6 && isset($value["address"]) && isset($value["address"]["localityId"])){
                if($value["address"]["localityId"]==(string)$k){
                    $nbRegion++;  
                }
            }      
        }
        $nbRegion=strval($nbRegion);
        if($nbRegion!="0"){
            array_push($zones, $v["name"]);
            array_push($nbByRegions, $nbRegion);
        }    
}
$typeRegion=(!empty($regions) ? $zones : $typeRegion );
//var_dump($typeRegion);exit;
$countRegion=count($typeRegion);

if($level!=3){
    $colorsRegion=[];
    for($i=0;$i<$countRegion;$i++){
         $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)]; 

        array_push($colorsRegion, $color);  
    }
}

//var_dump($orga);exit;

    // foreach ($typeRegion as $region){
    //     $nbRegion=0;
    //     foreach($orga as $id => $value){
    //         if(isset($value["address"]) && isset($value["address"]["level".$level."Name"]) && $level!=6) {
    //             if($region==$value["address"]["level".$level."Name"]  ){
    //                 $nbRegion++;
    //             }
    //         }
    //         else if($level==6 && isset($value["address"]) && isset($value["address"]["localityId"])){
    //             //if($region)
    //         }      
    //     }
    //     $nbRegion=strval($nbRegion);
    //     array_push($nbByRegions, $nbRegion);
    // }


// fin region

    // coform params for dashboard

    //var_dump($controller->costum["contextType"]);exit;
    $coformDashboard = [];
    $costumData=[];
    

    

    $costumData = (isset($controller->costum["dashboard"])) ? $controller->costum : PHDB::findOneById($controller->costum["contextType"], $controller->costum["contextId"])["costum"];

    //var_dump($costumData);exit;

    if(isset($costumData["dashboard"]["config"])){
        $coformDashboard = [
            "blocks" => $costumData["dashboard"]["config"]
        ];
    }




            // params


            $params = [
                "elements" => null,
                "title" => "Observatoire des Tiers-lieux",
                "blocks"    => [
                    "barType" => [
                        "title"   => "Typologie d'espaces de travail",
                        "counter" => "",
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.barCommon/size/S",
                            "key"=>"barType",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $nbByTypes,
                                        "backgroundColor"=> $colorPlace
                                    ]
                                ],
                                "labels"=> $typePlace
                            ]
                        ],
                        "width" => 6
                    ],
                    "barMajorServices" => [
                        "title"   => "Les 5 services les plus proposés",
                        "counter" => null,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.barCommon/size/S",
                            "key"=>"barMajorServices",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $majorServicesVal,
                                        "backgroundColor"=> $colorMajServ
                                    ]
                                ],
                                "labels"=> $majorServicesLab,
                            ]
                        ],
                        "width" => 6
                    ],
                    "PieModel" => [
                        "title"   => "modèles de gestion",
                        "counter" => 5,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.pieCommon/size/S",
                            "key"=>"pieModel",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $valueModel,
                                        "backgroundColor"=> $colorsModel
                                    ]
                                ],
                                "labels"=> $labelModel
                            ]
                        ],
                        "width" => 6

                    ],
                    "pieZone" => [
                        "title"   => "typologies de territoire",
                        "counter" => "4",
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.pieCommon/size/S",
                            "key"=>"pieZone",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $nbByZones,
                                        "backgroundColor"=> $colorZone
                                    ]
                                ],
                                "labels"=> $typeZone
                            ]
                        ],
                        "width" => 6
                    ],
                    // "barMinorServices" => [
                    //     "title"   => "Les 5 services les moins proposés",
                    //     "counter" => null,
                    //     "graph" => [
                    //         "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.barCommon/size/S",
                    //         "key"=>"barMinorServices",
                    //         "data" => [
                    //             "datasets"=> [
                    //                 [
                    //                     "data"=> $minorServicesVal,
                    //                     "backgroundColor"=> $colorMinServ
                    //                 ]
                    //             ],
                    //             "labels"=> $minorServicesLab
                    //         ]
                    //     ]
                    // ],
                    "pieGeo" => [
                        "title"   => "Partitions géographiques",
                        "counter" => $countRegion,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.pieCommon/size/S",
                            "key"=>"pieGeo",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $nbByRegions,
                                        "backgroundColor"=> $colorsRegion
                                    ]
                                ],
                                "labels"=> $typeRegion
                            ]
                        ],
                        "width" => 8
                    ],

                    "Fabrique" => [
                        "title"   => "fabriques (numériques) de territoire",
                        "counter" => $totFab,
                        "graph" => [
                            "url"=>"/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.doughnutCommon/size/S",
                            "key"=>"Fabrique",
                            "data" => [
                                "datasets"=> [
                                    [
                                        "data"=> $fabValue,
                                        "backgroundColor"=> $fabColor
                                    ]
                                ],
                                "labels"=> $fabLabel
                            ]
                        ],
                        "width" => 4
                    ],
                    "Compagnon" => [
                        "title"   => "Tiers-lieux compagnons",
                        "counter" => $nbComp,
                        "html" => "<img class='img-responsive' style='filter: drop-shadow(0px 0px 7px #FF286B) invert(100%) hue-rotate(82deg);  margin:30px auto 0px auto;height:275px' src='".Yii::app()->getModule("costum")->assetsUrl."/images/franceTierslieux/compagnon.png'>"
                    ],
                    "Superficie" => [
                        "title"   => "m2 de superficie médiane",
                        "counter" => $median,
                        "html" => "<img class='img-responsive' style='height:275px;  filter: hue-rotate( 115deg);margin:30px auto 0px auto;opacity:0,75;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/franceTierslieux/superficie.png'>"
                    ],
                    "Label" => [
                        "title"   => "% de fabriques (numériques)",
                        "counter" => $fabPart,
                        "html" => "<img class='img-responsive' style='height:275px;margin: auto; ' src='".Yii::app()->getModule("costum")->assetsUrl."/images/franceTierslieux/fabrique.png'>"
                    ]
                ]
            ];

//            Yii::$app->cache->set('ftlDashboard',$params, 720);
         
         if(isset($coformDashboard["blocks"]) && count($coformDashboard["blocks"])!=0){
            $graphblocks = array_merge($coformDashboard["blocks"], $params["blocks"]);
            $params["blocks"] = $graphblocks;
         }

         //var_dump($params["blocks"]);exit;
        
        
        
            if(Yii::app()->request->isAjaxRequest)
                return $controller->renderPartial($tpl,$params,true);
            else {
                $this->getController()->layout = "//layouts/empty";
                return $this->getController()->render($tpl,$params);
            }
    }
}
    	
