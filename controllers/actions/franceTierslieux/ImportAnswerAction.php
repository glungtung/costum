<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use Answer;
use Authorisation;
use DateTime;
use Exception;
use Form;
use MongoDate;
use MongoId;
use Organization;
use PHDB;
use Rest;

class ImportAnswerAction extends \PixelHumain\PixelHumain\components\Action
{
    const FORM_ID = "617ab9a16c9be060211a975f";
    const SLUG_FTL = "franceTierslieux";

    public function run()
    {
        $controller = $this->getController();

        $element = PHDB::findOne(Organization::COLLECTION, ["slug" => self::SLUG_FTL]);

        if (!$element)
            return Rest::json(["error" => true, "message" => "Element not found"]);
        else if (!Authorisation::isInterfaceAdmin((string)$element["_id"], Organization::COLLECTION))
            return Rest::json(["error" => true, "message" => "You must be an admin to perform this operation"]);
        else {
            if ($_SERVER["REQUEST_METHOD"] == "GET")
                return $controller->renderPartial("importftl");
            else {
                $INPUT_NAME_ID = "franceTierslieux28102021_1857_332";
                /**
                 * Todo: ne pas ajouter la clé si la valeur est vide: remarque
                 */
                /* $INPUT_NAME_ID = "franceTierslieux28102021_1857_332";
                try {
                    $step = PHDB::findOne(Form::COLLECTION, ["id" => $_POST["stepId"]]);
                    if (filter_var($_POST["sameAsLastRow"], FILTER_VALIDATE_BOOLEAN)) {
                        foreach ($_POST["inputs"] as $input) {
                            $inputId = $input["id"];
                            $inputValue = $input["value"];
                            $inputType = $step["inputs"][$input["id"]]["type"];

                            $answer = $this->getValueStructureByInputType($_POST["stepId"], $inputType, $inputId, $inputValue);
                            if ($answer) {
                                PHDB::update(Answer::COLLECTION, ["tempId" => $_SESSION["tempId"]], [
                                    '$set' => [
                                        'answers.' . $_POST["stepId"] . '.' . $answer["index"] => $answer["value"]
                                    ]
                                ]);
                            }
                        }
                    } else {
                        $tempId = uniqid();
                        $_SESSION["tempId"] = $tempId;

                        $answers = [
                            $_POST["stepId"] => []
                        ];

                        $elementParent = null;

                        foreach ($_POST["inputs"] as $input) {
                            $inputId = $input["id"];
                            $inputValue = $input["value"];
                            $inputType = $step["inputs"][$input["id"]]["type"];

                            $answer = $this->getValueStructureByInputType($_POST["stepId"], $inputType, $inputId, $inputValue);
                            if ($answer)
                                $answers[$_POST["stepId"]][$answer["index"]] = $answer["value"];

                            if ($inputId == $INPUT_NAME_ID)
                                $elementParent = PHDB::findOne(Organization::COLLECTION, ["name" => $inputValue]);
                        }

                        $data = [
                            "collection" => "answers",
                            "user" => $_SESSION["userId"],
                            "links" => [
                                "answered" => [$_SESSION["userId"]]
                            ],
                            "cterSlug" => "franceTierslieux",
                            "form" => self::FORM_ID,
                            "formId" => [
                                0,
                                1,
                                2,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                11,
                                12,
                                13,
                                14,
                                15,
                                16,
                                17,
                                18,
                                19,
                                20,
                                21,
                                22,
                                23,
                                24,
                                25,
                                26,
                                27,
                                28,
                                29,
                                30,
                                31,
                                32,
                                33,
                                34
                            ],
                            "formList" => 35,
                            "tempId" => $tempId,
                            "answers" => $answers,
                            "source" => [
                                "insertOrigin" => "import",
                                "date" => (new DateTime())->format("c")
                            ]
                        ];
                        if ($elementParent) {
                            $data["links"][Organization::COLLECTION] = [(string)$elementParent["_id"]];
                            $data["source"]["key"] = $elementParent["slug"];
                            $data["source"]["keys"] = [$elementParent["slug"]];
                        }

                        PHDB::insert(Answer::COLLECTION, $data);
                    }
                } catch (Exception $e) {
                    return Rest::json(["error" => true, "message" => $e->getMessage()]);
                } */

                try{
                    $answers = [];
                    $elementParent = null;

                    foreach($_POST as $stepId => $inputs){
                        $step = PHDB::findOne(Form::COLLECTION, ["id" => $stepId]);
                        $answers[$stepId] = [];

                        foreach($inputs as $input){
                            $inputId = $input["inputId"];
                            $inputValue = $input["value"];
                            $inputType = $step["inputs"][$inputId]["type"];

                            $answer = $this->getValueStructureByInputType($stepId, $inputType, $inputId, $inputValue);
                            if($answer){
                                $answers[$stepId][$answer["index"]] = $answer["value"]; 
                            }

                            if ($inputId == $INPUT_NAME_ID)
                                $elementParent = PHDB::findOne(Organization::COLLECTION, ["name" => $inputValue]);

                        }
                    }

                    $dataTosave = [
                        "collection" => "answers",
                        "user" => $_SESSION["userId"],
                        "links" => [
                            "answered" => [$_SESSION["userId"]]
                        ],
                        "cterSlug" => "franceTierslieux",
                        "form" => self::FORM_ID,
                        "formId" => [
                            0,
                            1,
                            2,
                            3,
                            4,
                            5,
                            6,
                            7,
                            8,
                            9,
                            10,
                            11,
                            12,
                            13,
                            14,
                            15,
                            16,
                            17,
                            18,
                            19,
                            20,
                            21,
                            22,
                            23,
                            24,
                            25,
                            26,
                            27,
                            28,
                            29,
                            30,
                            31,
                            32,
                            33,
                            34
                        ],
                        "formList" => 35,
                        "answers" => $answers,
                        "source" => [
                            "insertOrigin" => "import",
                            "date" => new MongoDate(time())
                        ]
                    ];
                    if ($elementParent) {
                        $dataTosave["links"][Organization::COLLECTION] = [(string)$elementParent["_id"]];
                        $dataTosave["source"]["key"] = $elementParent["slug"];
                        $dataTosave["source"]["keys"] = [$elementParent["slug"]];
                    }

                    PHDB::insert(Answer::COLLECTION, $dataTosave);
                }catch (Exception $e) {
                    return Rest::json(["error" => true, "message" => $e->getMessage()]);
                }
            }
        }
    }

    private function getValueStructureByInputType($subformId, $inputType, $inputId, $inputValue)
    {
        $formParent = PHDB::findOneById(Form::COLLECTION, self::FORM_ID);
        if ($inputType == "tpls.forms.cplx.multiRadio") {
            return [
                "index" => "multiRadio" . $inputId,
                "value" => [
                    "value" => $inputValue,
                    "type" => "simple"
                ]
            ];
        } else if (in_array($inputType, ["text", "tel", "email", "url", "number", "date"])) {
            return [
                "index" => $inputId,
                "value" => $inputValue
            ];
        } else if ($inputType == "tpls.forms.cplx.multiCheckboxPlus") {
            if (isset($formParent["params"]["multiCheckboxPlus" . $inputId]["global"]["list"])) {
                $list = $formParent["params"]["multiCheckboxPlus" . $inputId]["global"]["list"];
                $inputValueparts = explode(";", $inputValue);
                $values = [];
                foreach ($inputValueparts as $value) {
                    if (in_array($value, $list) && $value != "Autre") {
                        $value = str_replace(".", "", $value);
                        $values[] = [
                            $value => [
                                "value" => $value,
                                "type" => "simple"
                            ]
                        ];
                    } else {
                        $values[] = [
                            "Autre" => [
                                "value" => "Autre",
                                "type" => "cplx",
                                "textsup" => $value
                            ]
                        ];
                    }
                }
                return [
                    "index" => "multiCheckboxPlus".$inputId,
                    "value" => $values
                ];
            }
        }else if($inputType == "tpls.forms.select"){
            $options = $formParent["params"][$inputId]["options"];
            return [
                "index" => $inputId,
                "value" => array_search($inputValue, $options) ? array_search($inputValue, $options) : -1
            ];
        }

        return null;
    }

    private function transform_to_mulicheckbox_plus()
    {
        $form = PHDB::findOneById(Form::COLLECTION, "617ab9a16c9be060211a975f");
        $params = [];
        foreach ($form["params"] as $key => $param) {
            if (preg_match("/multiCheckbox/", $key) > 0) {
                $newKey = preg_replace("/multiCheckbox/", "multiCheckboxPlus", $key);
                foreach ($param["list"] as $key => $item) {
                    if (strtolower($item) == "autre")
                        $param["list"][$key] = "Autre";
                }
                $params[$newKey] = [
                    "global" => [
                        "list" => $param["list"]
                    ]
                ];

                if (in_array("Autre", $param["list"])) {
                    $params[$newKey]["tofill"] = [];
                    foreach ($param["list"] as $item) {
                        if ($item == "Autre")
                            $params[$newKey]["tofill"]["Autre"] = "cplx";
                    }
                }
            } else {
                $params[$key] = $param;
            }
        }
        PHDB::update(Form::COLLECTION, ["_id" => new MongoId("617ab9a16c9be060211a975f")], [
            '$set' => [
                "params" => $params
            ]
        ]);

        foreach ($form["subForms"] as $subFormId) {
            $subForm = PHDB::findOne(Form::COLLECTION, ["id" => $subFormId]);
            $inputs = [];
            foreach ($subForm["inputs"] as $key => $input) {
                if ($input["type"] == "tpls.forms.cplx.multiCheckbox")
                    $input["type"] = "tpls.forms.cplx.multiCheckboxPlus";
                $inputs[$key] = $input;
            }
            PHDB::update(Form::COLLECTION, ["id" => $subFormId], [
                '$set' => [
                    "inputs" => $inputs
                ]
            ]);
        }
    }
}
    	
