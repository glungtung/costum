<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use Rest;
use FranceTierslieux;
use Organization;
use Citoyen;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class AnswerDirectoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($source=null, $form=null, $distinct=null, $filterOrga=false){
		$controller = $this->getController();
		$res=array();
		$searchParams = $_POST;
		$searchParams["distinct"] = $distinct;
		$searchParams["filterOrga"] = $filterOrga;
		if ($filterOrga) {
			$searchParams["fields"] = array (
				"answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb" => 1,
				"links.organizations" => 1,
				"created" => 1,
				"creator" => 1,
				"reseau" => 1,
				"user" => 1
			);				
		}
		$answers= FranceTierslieux::globalAutocomplete($form, $searchParams);
		if(isset($answers["count"]))
			$res["count"]=$answers["count"];

		$res["results"] = $answers["results"];
		if ($filterOrga) 
			$res["results"] = FranceTierslieux::addOrganizationAndPerson($res["results"]);

		return Rest::json( $res );
	}
}
