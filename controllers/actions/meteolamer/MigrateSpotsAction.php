<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;

use PHDB;
use Organization;

class MigrateSpotsAction extends \PixelHumain\PixelHumain\components\Action {
    public function run(){
        if(isset($_SESSION["userId"])){
            $el = PHDB::findOne(Organization::COLLECTION, ["slug"=>"meteolamer"]);
            if(isset($el["links"]["members"][$_SESSION["userId"]]) && $el["links"]["members"][$_SESSION["userId"]]["isAdmin"]){
                Meteolamer::migrateSpots();
            }else{
                return "You are not authorized to perform this action";
            }
        }else{
            return "You must log in to perform this operation";
        }
    }
}