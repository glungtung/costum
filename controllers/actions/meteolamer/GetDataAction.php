<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;

use CAction, PHDB, Rest;
class GetDataAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($start_date, $end_date=NULL, $spot){
        if($end_date !== NULL){
            $where = [
                "date"=> [
                    '$gte'=> strtotime($start_date),
                    '$lte'=> strtotime($end_date)
                ]
            ];
        }else{
            $where = [
                "date" => strtotime($start_date)
            ];
        }
        $where["spot"] = $spot;
        $result = PHDB::find('--meteolamer-forecast', $where);
        return Rest::json($result);
    }
}