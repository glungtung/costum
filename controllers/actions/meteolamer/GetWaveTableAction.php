<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;

use Meteolamer;

class GetWaveTableAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($spot, $start_date, $end_date){
        $weekly_data = Meteolamer::getDataBetweenDates($spot, $start_date, $end_date);

        return $this->getController()->renderPartial("costum.views.tpls.blockCms.meteolamer.tables.wave.index", [
            "weekly_data"=>$weekly_data,
            "start_date"=>$start_date,
            "end_date"=>$end_date
        ]);
    }
}