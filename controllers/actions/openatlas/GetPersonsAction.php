<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\openatlas;

use CAction, OpenAtlas, Rest;
class GetPersonsAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
    	$controller = $this->getController();
        $params = OpenAtlas::getPersons($_POST["collection"],$_POST["id"]);
        
        return Rest::json($params);
    }
	   
}