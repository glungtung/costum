<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\openatlas;

use OpenAtlas;
use Rest;

class GetCampaignAction extends  \PixelHumain\PixelHumain\components\Action
{

    public function run() {
    	$controller = $this->getController();
        $params = OpenAtlas::getCampaign($_POST["costumSlug"]);
        
        return Rest::json($params);
    }
	   
}