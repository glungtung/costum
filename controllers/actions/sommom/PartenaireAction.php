<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom;

use CAction, Element, PHDB, Form, Yii;
class PartenaireAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$controller = $this->getController(); 

    	// if (isset($controller->costum["contextType"]) and isset($controller->costum["contextId"])) {
	    //     $communityLinks = Element::getByTypeAndId($controller->costum["contextType"],$controller->costum["contextId"]);
	    // }

        $communityLinks = Element::getCommunityByTypeAndId($controller->costum["contextType"],$controller->costum["contextId"]);


    	$partenaire = false;


        $formId =PHDB::find( Form::COLLECTION,  array( "parent.".$controller->costum["contextId"] => array('$exists'=>1) ) );

        reset($formId);
        $first_key = key($formId);

        $territoire = [];

        $answers = PHDB::find( Form::ANSWER_COLLECTION, [ "form" => $first_key, "answers" => array('$exists'=>1) ] );

        foreach($answers as $key => $value){
            if(isset($value["answers"])){
                if(isset($value["answers"]["sommomForm1"]["sommomForm120"])){
                    //array_push($territoire, $value["answers"]["sommomForm1"]["sommomForm120"]);

                    $territoire[$value["answers"]["sommomForm1"]["sommomForm120"]] = $value["answers"]["sommomForm1"]["sommomForm120"];
                }
            }
        }

    	// if (isset($communityLinks["links"]["members"])) {
     //  		foreach ($communityLinks["links"]["members"] as $memberId => $memberValue) {
     //            if(isset(Yii::app()->session["userId"])){
     //                if($memberId != ""){
     //                    if($memberId == Yii::app()->session["userId"]){
     //                        if(isset($memberValue["roles"])){
     //                       		foreach ($memberValue["roles"] as $rolesId => $rolesValue) {
     //                          		if($rolesValue == "Partenaire"){
     //                              		$partenaire = true;
     //                          		}
     //                        	}
     //                    	}
     //                    }
     //                }
     //            }
     //        }
    	// }   	

        foreach ($communityLinks as $memberId => $memberValue) {
            if(isset(Yii::app()->session["userId"])){
                if($memberId != ""){
                    if($memberId == (string)Yii::app()->session["userId"]){
                        if(isset($memberValue["roles"])){
                            foreach ($memberValue["roles"] as $rolesId => $rolesValue) {
                                if($rolesValue == "Partenaire"){
                                    $partenaire = true;
                                }
                            }
                        }
                    }
                }
            }
        }

    	$tpl = "costum.views.custom.sommom.partenaire";

    	$params = [
    		"partenaire" => $partenaire,
            "territoire" => $territoire
    	];
    	
    	return $controller->renderPartial($tpl,$params,true);
    }
}