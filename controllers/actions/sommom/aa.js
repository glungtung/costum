function afficheMap() {
    mylog.log("afficheMap");

    //Si contextData.geo est undefined caché la carte
    if (typeof contextData.geo == "undefined") {
        $("#divMapContent").addClass("cacher");
    };



    var paramsMapContent = {
        container: "divMapContent",
        latLon: contextData.geo,
        activeCluster: false,
        zoom: 16,
        activePopUp: false
    };

    contextData.geo = { @type: "GeoCoordinates", latitude: "-34.6075682", longitude: "-58.4370894" }


    mapAbout = mapObj.init(paramsMapContent);

    var paramsPointeur = {
        elt: {
            id: contextData.id,
            type: contextData.type,
            geo: contextData.geo
        },
        center: true
    };
    mapAbout.addMarker(paramsPointeur);
    mapAbout.hideLoader();


};