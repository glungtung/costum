<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom;

use CAction;
class HelpAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.custom.sommom.help";

    	$params = [];
    	
    	return $controller->renderPartial($tpl,$params,true);
    }
}