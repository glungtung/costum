<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom;
use CAction;
use Document;
use Element;
use Form;
use MongoId;
use PHDB;
use Project;
use Yii;

class DashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($sk=null,$answer=null,$activegraph=null)
    {
    	$controller = $this->getController();

    	$tpl = "costum.views.custom.sommom.dashboard";
    	$formId =PHDB::find( Form::COLLECTION,  array( "parent.".$controller->costum["contextId"] => array('$exists'=>1) ) );

    	$answers = [];

    	reset($formId);
		$first_key = key($formId);

		if($first_key != ""){
			$answers = PHDB::find( Form::ANSWER_COLLECTION, [ "form" => $first_key ] );
		}


    	$communityLinks = Element::getCommunityByTypeAndId($controller->costum["contextType"],$controller->costum["contextId"]);


    	$partenaire = false;
    	$roleList = [];

    	// if (isset($communityLinks["links"]["members"])) {
     //  		foreach ($communityLinks["links"]["members"] as $memberId => $memberValue) {
     //            if(isset(Yii::app()->session["userId"])){
     //                if($memberId != ""){
     //                    if($memberId == Yii::app()->session["userId"]){
     //                        if(isset($memberValue["roles"])){
     //                       		foreach ($memberValue["roles"] as $rolesId => $rolesValue) {
     //                          		if($rolesValue == "Partenaire"){
     //                              		$partenaire = true;
     //                          		}
     //                        	}
     //                    	}
     //                    }
     //                }
     //            }
     //        }
    	// }

        foreach ($communityLinks as $memberId => $memberValue) {
            if(isset(Yii::app()->session["userId"])){
                if($memberId != ""){
                    if($memberId == (string)Yii::app()->session["userId"]){
                        if(isset($memberValue["roles"])){
                            foreach ($memberValue["roles"] as $rolesId => $rolesValue) {
                                if($rolesValue == "Partenaire"){
                                    $partenaire = true;
                                }
                                $rolesValue = trim($rolesValue);
                                $roleList[$rolesValue] = $rolesValue;
                            }
                        }
                    }
                }
            }
        }

    	$blocks = [];
    	$el = PHDB::findOne( Project::COLLECTION, [ "_id" => new MongoId($controller->costum["contextId"]) ], ["name"]);
    	$title = $el["name"] ;

			$lists = [];
			// *********** List ************
			$formName = [];
			$totalChamp = 0;
			$totalValide = 0;
			$totalRep = 0;

			for($i = 1; $i < 6; $i++){
				${"sommomForm" . $i ."Label"} = [];
				${"sommomForm" . $i ."Count"} = [];
				${"sommomForm" . $i ."Variable"} = [];
				${"sommomForm" . $i ."Data"} = [];
				${"form". $i} = PHDB::findOne(Form::COLLECTION, array('id' => 'sommomForm'.$i));
			}

			for($i = 1; $i < 6; $i++){
				if(!is_null(${"form". $i})){
					foreach(${"form". $i}["inputs"] as $key => $value){
						array_push(${"sommomForm" . $i ."Label"}, $value["label"]);
						array_push(${"sommomForm" . $i ."Variable"}, $key);
						$totalChamp++;
					}
					array_push($formName, ${"form". $i}["name"]);
				}
			}

			for($i = 1; $i < 6; $i++){
				for($t = 0; $t < count(${"sommomForm" . $i ."Label"}); $t++){
					${"sommomForm" . $i ."Count"}[$t] = 0;
					${"sommomForm" . $i ."Data"}[$t] = [];
				}
			}

			foreach($answers as $key => $value){
				if(isset($value["answers"])){
	                foreach ($value["answers"] as $kn => $vn) {
						foreach ($vn as $k => $v) {
							for($i = 1; $i < 6; $i++){
								for($t = 0; $t < count(${"sommomForm" . $i ."Variable"}); $t++){
									if(strpos($k, ${"sommomForm" . $i ."Variable"}[$t]) !== false){
										$ck = count($v);
										${"sommomForm" . $i ."Count"}[$t] = ${"sommomForm" . $i ."Count"}[$t] + $ck;
										$totalValide++;
										$totalRep = $totalRep + $ck;

										//ajoute les data
										$j = 0;
										for($j = 0; $j < count($v); $j++){
											if(isset($v[$j])){
												array_push(${"sommomForm" . $i ."Data"}[$t], $v[$j]);
											}
										}
									}
								}
							}
						}
	                }
	            }
			}

			$users = [];
			$ids = [];
			$links = [];
			$photo = [];
			$territoire = [];
			$ile = [];
			$organisme = [];
			$gpsterritoire = [];

			$zoneobs = [];
			$espobs = [];
			$act = [];

			$espgeo = [];
			$counter = 0;
			foreach($answers as $key => $value){
				if(isset($value["answers"])){
					array_push($users, $value["user"]);
					array_push($ids, $key);
						if(isset($value["links"]["answered"])){
							array_push($links, $value["links"]["answered"]);
						}else {
							array_push($links,[]);
						}

						if(isset($value["answers"]["sommomForm1"]["sommomForm120"])){
							array_push($territoire, $value["answers"]["sommomForm1"]["sommomForm120"]);
						}else {
							array_push($territoire,"non défini");
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm121"])){
							array_push($ile, $value["answers"]["sommomForm1"]["sommomForm121"]);
						}else {
							array_push($ile,"");
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm122"])){
							array_push($organisme, $value["answers"]["sommomForm1"]["sommomForm122"]);
						}else {
							array_push($organisme, []);
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm128"][0]["coord"])){
							array_push($gpsterritoire, $value["answers"]["sommomForm1"]["sommomForm128"][0]["coord"]);
						}else {
							array_push($gpsterritoire, []);
						}
						// if(isset($value["answers"]["sommomForm1"]["sommomForm128"][0]["pointGPS"])){
						// 	array_push($gpsterritoire, $value["answers"]["sommomForm1"]["sommomForm128"][0]["pointGPS"]);
						// }else {
						// 	array_push($gpsterritoire, []);
						// }
						if(isset($value["answers"]["sommomForm1"]["sommomForm12"])){
							array_push($zoneobs, $value["answers"]["sommomForm1"]["sommomForm12"]);
						}else {
							array_push($zoneobs, []);
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm13"])){
							array_push($espobs, $value["answers"]["sommomForm1"]["sommomForm13"]);
						}else {
							array_push($espobs, []);
						}
						if(isset($value["answers"]["sommomForm2"]["sommomForm21"])){
							array_push($act, $value["answers"]["sommomForm2"]["sommomForm21"]);
						}else {
							array_push($act, []);
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm11"])){
							array_push($espgeo, $value["answers"]["sommomForm1"]["sommomForm11"]);
						}else {
							array_push($espgeo, []);
						}

						for($i = 1; $i < 6; $i++){
							${"sommomForm" . $i ."Data".$counter} = [];
							${"sommomForm" . $i ."Count".$counter} = [];
						}

						for($i = 1; $i < 6; $i++){
							for($t = 0; $t < count(${"sommomForm" . $i ."Label"}); $t++){
								${"sommomForm" . $i ."Count".$counter}[$t] = 0;
								${"sommomForm" . $i ."Data".$counter}[$t] = [];
							}
						}

						foreach ($value["answers"] as $kn => $vn) {
						foreach ($vn as $k => $v) {
							for($i = 1; $i < 6; $i++){
								for($t = 0; $t < count(${"sommomForm" . $i ."Variable"}); $t++){
									if(strpos($k, ${"sommomForm" . $i ."Variable"}[$t]) !== false){
										$ck = count($v);
										${"sommomForm" . $i ."Count".$counter}[$t] = ${"sommomForm" . $i ."Count".$counter}[$t] + $ck;
										$totalValide++;
										$totalRep = $totalRep + $ck;

										//ajoute les data
										$j = 0;
										for($j = 0; $j < count($v); $j++){
											if(isset($v[$j])){
												array_push(${"sommomForm" . $i ."Data".$counter}[$t], $v[$j]);
											}
										}
									}
								}
							}
						}
	                }

	                $counter++;
				}

			}

			for($k = 0; $k < count($users); $k++){
				$name =  PHDB::find(Document::COLLECTION, array("id"=>$ids[$k]),array("name"));

				$cntu = "";

				foreach ($name as $key => $value) {
					if(isset($value['name'])){
						$cntu = $value['name'];
					}
				}

				if(count($name) == 0){
					array_push($photo, "");
				}else {
					array_push($photo, $cntu);
				}

			}


			$home = [$users, $territoire, $ile, $organisme, $gpsterritoire, $zoneobs, $espobs, $act, $ids, $photo, $links, $espgeo];


		$params = [
			"title" => $title,
    		"blocks" 	=> $blocks,
    		"territoire" => $home,
    		"formId" => $first_key,
    		"partenaire" => $partenaire,
    		"roleList" => $roleList
    		// "hgraph" => $hgraph

    	];
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }

    }
}
