<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;
use CAction;
use Classified;
use Ctenat;
use Element;
use Event;
use Organization;
use PHDB;
use Project;
use Yii;
use Person;

class GraphAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($sk=null,$answer=null,$aj="false", $territoires="[0]", $firstkey = "", $ter = "")
    {
    	$controller = $this->getController(); 
        $el = Element::getByTypeAndId($controller->costum["contextType"], $controller->costum["contextId"] );
        $tpl = "costum.views.custom.cocity.observatory";
        $allsFilieres =PHDB::find(Organization::COLLECTION, array("cocity"=>(String)$controller->costum["contextId"]));
        $where = [];
        if ($el["address"]["postalCode"] != ""){
            $where = array('$or'=> array(
                        array("source.keys" =>$controller->costum["slug"]) ,
                        array( '$and' => array(
                                array("address.postalCode"=>$el["address"]["postalCode"]),
                                array("address.level1Name" =>$el["address"]["level1Name"]),
					            array("address.addressLocality" =>$el["address"]["addressLocality"])
                            )
                        ),
                        array("links.memberOf.".(String)$el['_id'] => array('$exists'=>1))
                    )
                );
        }else {
            $where = array('$or'=> array(
                    array("source.keys" =>$controller->costum["slug"]) ,
                    array( '$and' => array(
                            array("address.addressLocality"=>$el["address"]["addressLocality"]),
                            array("address.level1Name" =>$el["address"]["level1Name"])
                        )
                    ),
                    array("links.memberOf.".(String)$el["_id"] => array('$exists'=>1))
                )
            );
        }

    	$projects = PHDB::find(Project::COLLECTION, $where,["tags"]);
		//$agricultures = PHDB::find(Organization::COLLECTION,["source.key" =>$controller->costum["contextSlug"],"tags"=>"agricultures"]);
		$organization = PHDB::find(Organization::COLLECTION, $where,["tags"]);
        $citoyens = PHDB::find(Person::COLLECTION, $where,["tags"]);
        //$citoyens = Element::getCommunityByTypeAndId($controller->costum["contextType"], $controller->costum["contextId"], "citoyens");
        $event = PHDB::find(Event::COLLECTION, array('$or'=> array(
            array("source.keys" =>$controller->costum["slug"]) ,
            array("organizer.".$controller->costum["contextId"] => array('$exists'=>1))
        )),["tags"]);
        $classifieds = PHDB::find(Classified::COLLECTION, array('$or'=> array(
            array("source.keys" =>$controller->costum["slug"]) ,
            array("parent.".$controller->costum["contextId"] => array('$exists'=>1))
        )),["tags"]);
        $elementLabel = ["organization","projects","event","classifieds","citoyens" ];
        $elementTrueLabel = ["Organisation","Projets","Evènement","Annonces","Personnes" ];
        $nombreElement = [];
        for($i = 0; $i < count($elementLabel); $i++){
            $nombreElement[$i] = count(${$elementLabel[$i]});
        }
        $allTags = [];
        $tagsOrga = [];
        foreach ($organization as $key => $value) {
            $tagsOrga = array_merge($value["tags"] ?? [],$tagsOrga);
        }
        $allTags = array_merge($tagsOrga,$allTags); 
        $tagsProjet = [];
        foreach ($projects as $key => $value) {
            $tagsProjet = array_merge($value["tags"] ?? [],$tagsProjet);
        }
        $allTags = array_merge($tagsProjet,$allTags);
        $tagsEvent = [];
        foreach ($event as $key => $value) {
            $tagsEvent = array_merge($value["tags"] ?? [],$tagsEvent);
        }
        $allTags = array_merge($tagsEvent,$allTags);
        $listTags = [];
        $blocks = [];
        $graph = [];

        $graph += ["nombrecocity" => [
            "title"=>"EXISTANT DANS COCITY",
            "data" => $nombreElement,
            "lbls" => $elementTrueLabel,
            "url"  => "/graph/co/dash/g/graph.views.co.costum.cocity.barMany",
            "yAxesLabel" => "Nombre "
        ]];

        $filiere = [];
        $activityFiliere = [];
        if ($allsFilieres) {
            foreach ($allsFilieres as $key => $value) {
                $filiere [] = urldecode($value["thematic"]);
                $projectFiliere= PHDB::find(Project::COLLECTION, array("parent.".(String)$value["_id"] => array('$exists'=>1)),["tags"]);
                $organizationFiliere = PHDB::find(Organization::COLLECTION, array("source.keys"=>$value["slug"] ),["tags"]);
                $eventsFiliere = PHDB::find(Event::COLLECTION, array("source.keys"=>$value["slug"] ),["tags"]);
                $activityFiliere[] = count($projectFiliere)+count($organizationFiliere)+count( $eventsFiliere);         
            }
        }
        $graph += ["activityFiliere" => [
            "title"=>"Activité filière",
            "data" => $activityFiliere,
            "lbls" => $filiere,
            "url"  => "/graph/co/dash/g/graph.views.co.costum.cocity.barMany",
            "yAxesLabel" => "Nombre "
        ]];

        foreach ($graph as $ki => $list) {
           $kiCount = 0;
           foreach ($list["data"] as $ix => $v) {
                if(is_numeric($v))
                    $kiCount += $v;
                else 
                    $kiCount ++;
            }
            $blocks[$ki] = [
                "title"   => $list["title"],
                "counter" => $kiCount,
            ];
            if(isset($list["tpl"])){
                $blocks[$ki] = $list;
            }
            else 
                $blocks[$ki]["graph"] = [
                    "url"=>$list["url"],
                    "key"=>"pieMany".$ki,
                    "data"=> [
                        "datasets"=> [
                            [
                                "data"=> $list["data"],
                                "backgroundColor"=> Ctenat::$COLORS
                            ]
                        ],
                        "labels"=> $list["lbls"]
                    ]
                ];

            if (isset($list["yAxesLabel"])) {
                $blocks[$ki]["graph"]["data"]["yAxesLabel"] = $list["yAxesLabel"];
            }

        }
		$params = [
			"projects" => $projects,
			"organization" => $organization,
            "event"    => $event,
            "citoyens"=> $citoyens,
            "filieres" => $filiere,
            "blocks" => $blocks,
            "allTags"=> $allTags
		];
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    	
    }
}
