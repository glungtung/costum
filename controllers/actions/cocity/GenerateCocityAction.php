<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use CAction, Cocity, Rest, Person, Organization, Project, PHDB, City, Yii, Element;
class GenerateCocityAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
		$controller = $this->getController(); 
		if (isset($_POST["allCities"])  &&( $_POST["allCities"] == 'false' || $_POST["allCities"] == false)) {
			$params = Cocity::generateCocity($_POST);
			return Rest::json($params);
		}
		else {
            ob_start();
			$colElts = array(Person::COLLECTION, Organization::COLLECTION, Project::COLLECTION);
			$cps = [];
			$cpUni = [];
			foreach ($colElts as $key => $elt) {
				$cps[$key] =  PHDB::distinct($elt,"address.postalCode",array("address.postalCode"=>array('$exists'=>1)));
			}
			foreach ($cps as $key => $cp) {
				$cpUni = array_merge($cpUni,$cp);
				$cpUni = array_unique($cpUni);
			}
			
			foreach ($cpUni as $key => $value) {
				if($value != ""){
					$city = City::getByPostalCode($value);
					foreach ($city as $kCity => $vCity) {
						foreach($vCity["postalCodes"] as $kp => $vp){
							if($vp["name"] == $vCity["alternateName"]){
								$organization = PHDB::findOne(Organization::COLLECTION, array("name" => $vCity["alternateName"] , "costum.slug" => "cocity"));
								$elCtCP = 0;
								if (empty($organization )) {
									foreach ($colElts as $key => $v) {
										$number = PHDB::count( $v,["address.postalCode"=>$vp["postalCode"]]);
										$elCtCP += $number;
									}
									if ($elCtCP  > 9 ) {
										$str = '';
										$str .= "generation cocity ".$vCity["alternateName"];

										if (!is_string($vp["geo"]["latitude"]))
											$latitude = strval($vp["geo"]["latitude"]);
										else
											$latitude = $vp["geo"]["latitude"];
										if (!is_string($vp["geo"]["longitude"]))
											$longitude = strval($vp["geo"]["longitude"]);
										else 
											$longitude = $vp["geo"]["longitude"];
										$paramsOrga = [
											"collection" => "organizations",
											"name" => $vCity["alternateName"], 
											"type" => "GovernmentOrganization",
											"geoPosition" => $vp["geoPosition"],
											"geo" => [
												'@type' => $vp["geo"]["@type"],
												'latitude' => $latitude,
												'longitude' => $longitude
											],
											"address" =>[
												"addressLocality" => $vCity["alternateName"] ,   
												"codeInsee" => $vCity["insee"] ,
												"@type" => "PostalAddress",
												"addressCountry" => $vCity["country"],
												"streetAddress" => "",
												"postalCode" => $vp["postalCode"] ,
												"level1" => isset($vCity["level1"])?$vCity["level1"]:"",
												"level1Name" => isset($vCity["level1Name"])?$vCity["level1Name"]:"",
												"level3" => isset($vCity["level3"])?$vCity["level3"]:"" ,
												"level3Name" => isset($vCity["level3Name"])?$vCity["level3Name"]:"",
												"level4" => isset($vCity["level4"])?$vCity["level4"]:"",
												"level4Name" => isset($vCity["level4Name"])?$vCity["level4Name"]:"" ,
												"localityId" =>(String)$vCity["_id"] 
											]
			
										];
										$assetUrl =  Yii::app()->createUrl('/costum');
									
										$str .= 'Generation de cocity pour la ville de '.$vCity["alternateName"]."</br>";
										$res = Element::save($paramsOrga);
										Cocity::generateCocity($res);
										$str .="La cocity pour la ville <a href='".$assetUrl."/co/index/slug/".$res["afterSave"]["organization"]["slug"]."'  target='_blank'>". $vCity["alternateName"]."</a>  est crée </br>";
										return $str;
									} 
								}
							}
						}
					}
				}
			}
            return ob_get_clean();
		}
	}
} 