<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa;

use CAction, AssoKosasa, Rest;
class GetActeurAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = AssoKosasa::getActeur($_POST);
        
        return Rest::json($params);
    }
}