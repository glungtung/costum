<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\deal;
use Answer;
use CAction;
use Ctenat;
use MongoId;
use PHDB;
use Yii;

class DashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($sk=null,$answer=null)
    {
    	$controller = $this->getController();    	

    	$tpl = "costum.views.custom.process.dashboard";
    	
    	// $answers = PHDB::find( Form::ANSWER_COLLECTION,[ //"formId"     => $params["formId"],
     //                                          			 "parentSlug" => $controller->costum["contextSlug"] ] );
    	//var_dump($answers);
    	$answers = PHDB::find( Answer::COLLECTION, [ "source.keys" => array('$in' => array($controller->costum["contextSlug"]) ) ] );
    	$blocks = [];
    	$title = "Observatoire <br/>de l'amélioration de l'habitat à la Réunion";
    	if(isset($answer)){
    		$answer = PHDB::findOne( Answer::COLLECTION,[ "_id" => new MongoId( $answer ) ] );
    		$title = (isset($answer["answers"]["opalProcess1"]["titre"])) ? "Observatoire<br/>".$answer["answers"]["opalProcess1"]["titre"] : "Observatoire <br/>de l'amélioration de l'habitat Réunionxx";
    		$lists = [
				"chiffresDepense" =>[
					"title"=>"<i class='fa fa-2x fa-question-circle'></i><br/>Dépense",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"personne impliquées","icon"=>"group","type"=>"success"],
						["data"=>45,"name"=>"durée","type"=>"danger","icon"=>"clock"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresDecide" =>[
					"title"=>"<i class='fa fa-2x fa-thumbs-up'></i><br/>Décision",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"Pour","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"Contre","type"=>"danger","icon"=>"thumbs-down"],
						["data"=>45,"name"=>"Nombres de Décisions Validés","type"=>"danger","icon"=>"gavel"],
						["data"=>60,"name"=>"Nombres de Décisions Refusés","type"=>"danger","icon"=>"hand-stop-o"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFin" =>[
					"title"=>"<i class='fa fa-2x fa-money'></i><br/>Finances",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"Total à financer","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"Total financé","type"=>"danger","icon"=>"map-marker"],
						["data"=>60,"name"=>"Pourcentage financé","type"=>"danger","icon"=>"money"],
						["data"=>45,"name"=>"Resta à financer","type"=>"danger","icon"=>"money"],
						["data"=>45,"name"=>"Combien de financeurs","type"=>"danger","icon"=>"group"],
						["data"=>"??","name"=>"Open projects Finance distribution","icon"=>"thumbs-up","type"=>"success"],
						["data"=>"??","name"=>"for open projects Finance distribution per task types","type"=>"danger","icon"=>"map-marker"],
						["data"=>"??","name"=>"for each worker Finances distribution per project","type"=>"danger","icon"=>"money"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFollow" =>[
					"title"=>"<i class='fa fa-2x fa-cogs'></i><br/>Suivi",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"nombre de tache","icon"=>"pie-chart","type"=>"success"],
						["data"=>24,"name"=>"nombre d'acteur","icon"=>"group","type"=>"success"],
						["data"=>45,"name"=>"Pourcentage d'avancement","type"=>"danger","icon"=>"fa-hourglass-half"],
						["data"=>34,"name"=>"Nombres de Travaux Validés","type"=>"danger","icon"=>"thumbs-up"],
						["data"=>5,"name"=>"Nombres de Taches Cloturés","type"=>"danger","icon"=>"fa-handshake-o"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"proposal" =>[
					"title"=>"Propositions",
					"data" => [32,65,6,21,10],
					"lbls" => ["Propositions 0","Propositions 1","Propositions 2","Propositions 3","Propositions 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.bar"
				],
				"projects" =>[
					"title"=>"Projects",
					"data" => [32,65,36,1,10],
					"lbls" => ["Projects 0","Projects 1","Projects 2","Projects 3","Projects 4"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"tasks" =>[
					"title"=>"Taches",
					"data" => [32,6,36,21,10],
					"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.line"
				],
				"finance" =>[
					"title"=>"Finance",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.scatter"
				],
			];
    	} else {
	    	/*echo count($answers)." Dossiers";
	    	echo "<br/> <b>Dossier per status pie</b>";
	    	echo "<br/>---- validated Dossiers";
	    	echo "<br/>---- pending decision Dossiers";
	    	echo "<br/>---- pending financing Dossiers";
	    	echo "<br/>---- no participation Dossiers";
	    	echo "<br/> <b>Decisions per status pie</b>";
	    	echo "<br/>---- en cours de Decisions";
	    	echo "<br/>---- Decisions validé";
	    	echo "<br/>---- Decisions interompu";
	    	echo "<br/> <b>Travaux per status pie</b>";
	    	echo "<br/>----  for open projects Tasks distribution";
	    	echo "<br/>----  for each worker Tasks distribution per project";
	    	echo "<br/>----  xxx Tasks";
	    	echo "<br/> <b>Fianncement per type or contexte pie</b>";
	    	echo "<br/>----  for open projects Tasks distribution per task types";
	    	echo "<br/>----  for each worker Finances distribution per project";
	    	echo "<br/>----  xxx Tasks";*/
	    	//count 
	    	$countAnswersWithOperators = 0;
	    	$countAnswersWithOperatorsUnset = 0;
	    	$countAnswersFinanced = 0;
	    	$countAnswersFinished = 0;
	    	$countAnswersRefused = 0;
	    	$totalAsked = 0;
	    	$totalFinanced = 0;
	    	$totalProjetNonFinanced = 0;
	    	$totalProjetEnCours = 0;
	    	$totalProjetEnValidation = 0;
	    	$totalProjetFinished = 0;

	    	$pieBudgetPerOperatorLbl = [];
	    	$pieBudgetPerOperatorData = [];
	    	$pieBudgetPerFinancer = [];
	    	$pieBudgetPerFinancerLbl = [];
	    	$pieBudgetPerFinancerData = [];

	    	$barTasksPerProject = [];
	    	$barTasksPerProjectLbl = [];
	    	$barTasksPerProjectData = [];

	    	foreach ($answers as $key => $ans) {

	    		if( isset($ans["step"]) && $ans["step"] == "deal5" )
	    			$countAnswersFinanced++;

	    		if( isset($ans["links"]["operators"]) ){
	    			$hasOperator = false;
	    			foreach ( $ans["links"]["operators"] as $oid => $op) {
	    				if($op != "0")
	    					$hasOperator = true;
	    			}

	    			if($hasOperator)
	    				$countAnswersWithOperators++;
	    			else 	
	    				$countAnswersWithOperatorsUnset++;
	    		} else 	
	    				$countAnswersWithOperatorsUnset++;

	    		if( isset($ans["validation"]) ){
	    			$refused = false;
	    			foreach ( $ans["validation"] as $step => $v) {
	    				if($v["valid"] == "notValid")
	    					$refused = true;
	    			}
	    			if( isset($ans["validation"]['deal5']["finished"]) && $ans["validation"]['deal5']["finished"] == true )
	    				$countAnswersFinished++;

	    			if($refused)
	    				$countAnswersRefused++;
	    		}

	    		//var_dump(@$ans["deal3"]["deal3"]);
	    		if( isset($ans["answers"]["deal3"]["budget"]) ){
	    			//var_dump( count( array_keys($ans["deal3"]["budget"])));
	    			foreach ($ans["answers"]["deal3"]["budget"] as $bix => $bud) {
	    				$totalAsked += (int)$bud["price"];
	    				//var_dump($bud["price"]);
	    				if( isset($bud["financer"]) ){
			    			foreach ($bud["financer"] as $bfix => $fin) {
			    				$totalFinanced += (int)$fin["amount"];
			    				if(!isset($pieBudgetPerFinancer[$fin["name"]]))
			    					$pieBudgetPerFinancer[ $fin["name"] ] = 0;
			    				 $pieBudgetPerFinancer[ $fin["name"] ] += (int)$fin["amount"];
			    			}
			    			if( isset($ans["step"]) && $ans["step"] == "deal4" )
			    				$totalProjetEnValidation += (int)$bud["price"];
			    		} else 
			    			$totalProjetNonFinanced += (int)$bud["price"];

			    		if( isset($ans["step"]) && $ans["step"] == "deal5" && isset($bud["todo"]) ){
			    			foreach ($bud["todo"] as $bfix => $todo) {
			    				$totalFinanced += (int)$fin["amount"];
			    				if(isset($ans["deal1"]["deal13"])){
			    					if( !isset($barTasksPerProject[ $ans["deal1"]["deal13"] ]))
			    						$barTasksPerProject[ $ans["deal1"]["deal13"] ] = 0;	
			    					if(isset($todo["done"]) && $todo["done"] == "0")
			    						$barTasksPerProject[ $fin["name"] ]++;
			    					else 
			    				 		$barTasksPerProject[ $fin["name"] ]++;
			    				}
			    			}
			    			if( isset($ans["step"]) && $ans["step"] == "deal4" )
			    				$totalProjetEnValidation += (int)$bud["price"];
			    		}

			    		//if( isset($ans["validation"]['deal5']["finished"]) && $ans["validation"]['deal5']["finished"] == true )
			    		if( isset($ans["step"]) && $ans["step"] == "deal5" )
			    			$totalProjetEnCours += (int)$bud["price"];
			    		else if( isset($ans["validation"]['deal5']["finished"]) && $ans["validation"]['deal5']["finished"] == true )
			    			$totalProjetFinished += (int)$bud["price"];
	    			}
	    		}

	    	}

	    	$pieBudgetPerFinancerLbl = array_keys($pieBudgetPerFinancer);
	    	$pieBudgetPerFinancerData = array_values($pieBudgetPerFinancer);

	    	$barTasksPerProjectLbl = array_keys($barTasksPerProject);
	    	$barTasksPerProjectData = array_values($barTasksPerProject);
			
			 // var_dump($barTasksPerProject);
			 // var_dump($barTasksPerProjectLbl);
			 // var_dump($barTasksPerProjectData);

			$lists = [
				"chiffresDoss" =>[
					"title"=>"<i class='fa fa-2x fa-folder-open-o'></i><br/>Dossiers",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>count($answers),"name"=>"Dossier déposés","icon"=>"folder-open","type"=>"default"],
						["data"=>$countAnswersWithOperators,"name"=>"Dossier géré avec opérateur","icon"=>"handshake-o"],
						["data"=>$countAnswersWithOperatorsUnset,"name"=>"Dossier sans opérateur","icon"=>"gavel","type"=>"danger"],
						["data"=>$countAnswersFinanced,"name"=>"Dossier financé","icon"=>"money","type"=>"success"],
						["data"=>$countAnswersFinished,"name"=>"Dossier clôturé","icon"=>"check-circle-o","type"=>"success"],
						["data"=>$countAnswersRefused,"name"=>"Dossier refusé","icon"=>"thumbs-down","type"=>"danger"],
						
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresDecis" =>[
					"title"=>"<i class='fa fa-2x fa-cogs'></i><br/>Travaux",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>"??","name"=>"for open projects Tasks distribution","type"=>"danger","icon"=>"money"],
						["data"=>"??","name"=>"for each worker Tasks distribution per project","icon"=>"hourglass-half"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFin" =>[
					"title"=>"<i class='fa fa-2x fa-money'></i><br/>Finances",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>$totalProjetEnCours."€","name"=>"Budget des projets en cours","icon"=>"cogs","type"=>"success"],
						["data"=>$totalProjetEnValidation."€","name"=>"Budget en cours de validation","type"=>"danger","icon"=>"gavel"],
						["data"=>$totalProjetNonFinanced."€","name"=>"Budget des projets non financés","type"=>"danger","icon"=>"question-o"],
						["data"=>$totalProjetFinished."€","name"=>"Budget des projets cloturés","type"=>"success","icon"=>"money"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				// "dossier" =>[
				// 	"title"=>"Dossier",
				// 	"data" => [32,65,36,21,10],
				// 	"lbls" => ["Dossier 0","Dossier 1","Dossier 2","Dossier 3","Dossier 4"],
				// 	"url"  => "/graph/co/dash/g/graph.views.co.line"
				// ],
				"financeBalance" => [
					"title"=>"Finance Demandé / Financé",
					"colors" => [ Ctenat::$COLORS[0],Ctenat::$COLORS[4]],
					"data" => [$totalAsked,$totalFinanced],
					"lbls" => ["Total Demandé","Total Financé"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"budgetPerFinancer" => [
					"title"=>"Budget par Financeur",
					"data" => $pieBudgetPerFinancerData,
					"lbls" => $pieBudgetPerFinancerLbl,
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"tasks" =>[
					"title"=>"Travaux en cours par projet" ,
					"data" => $barTasksPerProjectData ,
					"lbls" => $barTasksPerProjectLbl ,
					"url"  => "/graph/co/dash/g/graph.views.co.barMany"
				],
				"finance" =>[
					"title"=>"Finance",
					"data" => [["x"=>32,"y"=>65],["x"=>42,"y"=>58],["x"=>2,"y"=>5],["x"=>3,"y"=>6]],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.scatter"
				],
			];
		}
		foreach ($lists as $ki => $list) 
			{
				$kiCount = 0;
				foreach ($list["data"] as $ix => $v) {
					if(is_numeric($v))
						$kiCount += $v;
					else 
						$kiCount ++;
				}
				$blocks[$ki] = [
					"title"   => $list["title"],
					"counter" => $kiCount,
				];
				if(isset($list["tpl"])){
					$blocks[$ki] = $list;
				}
				else 
					$blocks[$ki]["graph"] = [
						"url"=>$list["url"],
						"key"=>"pieMany".$ki,
						"data"=> [
							"datasets"=> [
								[
									"data"=> $list["data"],
									"backgroundColor"=> (isset($list["colors"])) ? $list["colors"] : Ctenat::$COLORS
								]
							],
							"labels"=> $list["lbls"]
						]
					];
			}
	
		$params = [
			"title" => $title,
    		"blocks" 	=> $blocks
    	];	
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    	
    }
}