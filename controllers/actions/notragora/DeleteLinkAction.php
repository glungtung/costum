<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora;

use CAction, PHDB, Poi, MongoId, Rest;
class DeleteLinkAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run() {
	   	$set=array($_POST["connect"].".".$_POST["id"]=>true);    	
		PHDB::update(Poi::COLLECTION,array("_id"=>new MongoId($_POST["parentId"])),array('$unset'=>$set));
		$result = array("result"=>true, "msg" => "ok");
		
		return Rest::json($result);
    }
}