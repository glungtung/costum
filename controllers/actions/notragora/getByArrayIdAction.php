<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora;

use Rest;

class getByArrayIdAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($type = null, $contextId = null, $contextType = null)
    {

        $controller = $this->getController();
        $arrayId = ($_POST["arrayIds"]);
        $coll = ($type != null) ? $type : "";

        $content = $controller->renderPartial('co2.views.pod.listItems', array("title" => $_POST["title"], "links" => $arrayId, "connectType" => "", "number" => $_POST["number"], "titleClass" => "col-xs-12 title text-gray", "heightWidth" => 50, "containerClass" => "text-center no-padding margin-top-10 margin-bottom-10 " . $_POST["className"], "contextId" => $contextId, "contextType" => $contextType), true);

        return Rest::json($content);
    }
}