<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora;

use CAction, PHDB, MongoId, Rest;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class UpdateCategoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		//$project = PHDB::findOneById( $_POST["collection"] , $_POST["id"], array("name", "categoryNA"));
		if(is_string($_POST["value"])){
			$_POST["value"]=array($_POST["value"]);
		}
		
		$set = array("categoryNA" => $_POST["value"]);

		
		$resUpdate = PHDB::update( $_POST["collection"],
				    array("_id"=>new MongoId($_POST["id"])), 
				    array('$set' => $set) ) ;

		$res = array(
			"resUpdate" => $resUpdate
		);

		return Rest::json($res); exit;
	}
}
