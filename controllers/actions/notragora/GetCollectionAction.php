<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora;

use InflectorHelper;
use MongoRegex;
use PHDB;
use Rest;
use SearchNew;

class GetCollectionAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($type = null)
    {
        $name=$_POST["name"];

    	$id="";
        $res=[];
    	if($type!="poi"){
            
            $name=str_replace("-", " ", $name);
            $textRegExp = SearchNew::accentToRegex($name);

    		$res = PHDB::findOne($type,array("name" => new MongoRegex("/.*{$textRegExp}.*/i")),array("_id"));
    	}else{
    		$collec=PHDB::find($type,array(),array("name"));
            $name=urldecode($name);
    		$name=InflectorHelper::slugifyLikeJs($name);
    		foreach ($collec as $key => $value) {
    			$valueName=InflectorHelper::slugifyLikeJs(htmlspecialchars_decode($value["name"]));
    			
				if(strrpos($valueName,$name)!==false){
					$id=$key;
					break;
				}
    		}
    		$res=$collec[$id] ?? null;
    	}	
		
		return Rest::json($res);
    }

}