<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora;
use PHDB;
use Poi;
use Rest;

class GetProductionByPartnerAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($id) {
	    //assert('!empty($_POST["childType"])'); //The child type is mandatory');
	    $query = array('$or' => array(
	    	array("producors.".$id => array('$exists' => 1)),
	    	array("supports.".$id => array('$exists' => 1)),
	    	array("partner.".$id => array('$exists' => 1))
	    ) );

	    $productions = PHDB::find(Poi::COLLECTION, $query);
		
	 	//$result = array("result"=>true, "msg" => $msg,"productions" => $productions);
		
		return Rest::json($productions);
    }
}