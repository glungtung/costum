<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin;

use CAction, Yii;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ImporteventAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$params = array() ;
		$page = "importevent";
		if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial("/custom/meuseCampagnes/".$page,$params,true);
		
		
	}
}
