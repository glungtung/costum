<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin;

use CAction, Meusecampagnes, Rest;
class SaveEventAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	
    	//$params = $_POST;
		 // $params = Import::newStart($params);
      $res = Meusecampagnes::saveEvent($_POST);
      //$params = Import::setWikiDataID($_POST);
      return Rest::json($res);
    }
}
?>