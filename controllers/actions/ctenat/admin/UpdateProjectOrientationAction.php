<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin;

use CAction, PHDB, Project, Badge, MongoId, Rest;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class UpdateProjectOrientationAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$project = PHDB::findOneById( Project::COLLECTION , $_POST["id"], array("links", "name"));
		$cterId=$_POST["contextId"];
		// "Territoire Candidat",
		// 				"Territoire Candidat refusé", 
		// 				"Territoire lauréat", 
		// 				"Dossier Territoire Complet",
		// 				"CTE Signé"
		if(isset($_POST["orientations"])){
			$linksOrientations=$_POST["orientations"];
			$orientationAlreadyLinks=array();
			// ON check si on unlink les projet des badges (un lien iteratif est fait s'il est déjà lié ou nom)
			if(!empty($project["links"]) && isset($project["links"]["orientations"])){
				foreach($project["links"]["orientations"] as $k => $v){
					$currentB=PHDB::findOne(Badge::COLLECTION,array("_id" => new MongoId($k)),array("parent"));
					if(isset($currentB["parent"][$cterId])){
						if(!isset($linksOrientations[$k])){
							PHDB::update( Badge::COLLECTION, 
				                       array("_id" => new MongoId($k)), 
				                       array('$unset' => array("links.projects.".(string)$project["_id"]=>"")));
						}else{
							array_push($orientationAlreadyLinks, $k);
						}
					}
				}
			}
			foreach($linksOrientations as $k => $v){
				$currentB=PHDB::findOne(Badge::COLLECTION,array("_id" => new MongoId($k)),array("parent"));
				if(isset($currentB["parent"][$cterId])){
					if(empty($orientationAlreadyLinks) || !in_array($k, $orientationAlreadyLinks)){
		  				PHDB::update( Badge::COLLECTION, 
		                   array("_id" => new MongoId($k)) , 
		                   array('$set' => array("links.projects.".(string)$project["_id"]=>array("type"=>Project::COLLECTION, "name"=>$project["name"]))));
						
					}
				}
			}
			// Avant de purger les orientations links sur le projet on vérifie que les orientations appartiennent bien au context en cours sinon on rajoute les orientations de l'autres contextes (cte ou crte)!
			$linksBadgeToRemoveInResults=[];
			if(isset($project["links"]["orientations"])){
				foreach($project["links"]["orientations"] as $k => $v){
					$currentB=PHDB::findOne(Badge::COLLECTION,array("_id" => new MongoId($k)),array("parent"));
					if(!isset($currentB["parent"][$cterId])){
						$linksOrientations[$k]=$v;
						array_push($linksBadgeToRemoveInResults,$k);
					}
				}
			}
			$project["links"]["orientations"]=$linksOrientations;
			PHDB::update( Project::COLLECTION, 
	                   array("_id" => $project["_id"]) , 
	                   array('$set' => array("links"=>$project["links"])));
			// ON enlève dans les resultats retour les liens badge qui ne sont pas du dispositif en cours
			if(!empty($linksBadgeToRemoveInResults)){
				foreach($linksBadgeToRemoveInResults as $v){
					unset($project["links"]["orientations"][$v]);
					unset($linksOrientations[$v]);
				}
				if(empty($project["links"]["orientations"]))
					unset($project["links"]["orientations"]);
			}
		}else{
			//KeepOrientations vont permettre de regarder si des orientations n'existe pas déjà dans un autre dispositif (cte ou crte) dans ce cas on les garde quand même !! On enleve que les orientations sur le dispositif en question
			$keepOrientations=$project["links"]["orientations"];
			$badgeIdsToUp=[];
			if(isset($project["links"]["orientations"])){
				foreach($project["links"]["orientations"] as $k => $v){
					$currentB=PHDB::findOne(Badge::COLLECTION,array("_id" => new MongoId($k)),array("parent"));
					if(isset($currentB["parent"][$cterId])){
						unset($keepOrientations[$k]);
						array_push($badgeIdsToUp, new MongoId($k));
					}
				}
				if(empty($keepOrientations))
			 		unset($project["links"]["orientations"]);
			 	else
			 		$project["links"]["orientations"]=$keepOrientations;
			}
			if(!isset($project["links"]["orientations"])){
				PHDB::update( Project::COLLECTION, 
		                   array("_id" => $project["_id"]) , 
		                   array('$unset' => array("links.orientations"=>"")));
			}else{
				PHDB::update( Project::COLLECTION, 
		                   array("_id" => $project["_id"]) , 
		                   array('$set' => array("links.orientations"=>$project["links"]["orientations"])));
			}
			PHDB::update( Badge::COLLECTION, 
	                   array("_id"=> array('$in'=>$badgeIdsToUp), "category" => "strategy", "links.projects.".(string)$project["_id"] => array('$exists'=>true)) , 
	                   array('$unset' => array("links.projects.".(string)$project["_id"]=>"")));
			$linksOrientations=[];
			
		}		
		$res = array(
			"project" => $project,
			"orientations"=>$linksOrientations
		);

		return Rest::json($res);
	}
}
