<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin;

use Answer;
use CAction, PHDB, Project, MongoDate, MongoId, Rest , CacheHelper , Element;
use Link;
use Slug;
use Yii;

/**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class UpdateCteToCrteAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$newName=$_POST["name"];
		$dispositifId=$_POST["dispositifId"];
		$dispositifType=$_POST["dispositifType"];
		$duplicate=($_POST["duplicate"] === "false" || $_POST["duplicate"]===false) ? false : true;
		$idCter=$_POST["cteId"];
		$res=array("result"=>false, "msg"=> "Un problème est survenu");
		$costum = CacheHelper::getCostum();
		$session = end($costum['lists']['sessionCte']);
		if(!$duplicate){
			PHDB::update(Project::COLLECTION,array("_id"=> new MongoId($idCter)),
				array(
					'$set'=>array(
						"name"=>$newName,
						"dispositifId" => $dispositifId, 
						"dispositif"=> $dispositifType,
						"session" => $session
					)
				)
			);
			$res=array("result"=>true, "newCRTE"=> false, "hash"=>"#page.type.projects.id.".$idCter);
		}else{
			$cteR=PHDB::findOne(Project::COLLECTION,array("_id"=> new MongoId($idCter)));

			$newCRTE=array(
				"name"=>$newName,
				"dispositif"=> "CRTE",
				"dispositifId" => $dispositifId,
				"session" => $session,
				"copyOf" => [ "type" => Project::COLLECTION,
							  "id" => $idCter ],
				"category"=> "cteR",
				"tags"=> @$cteR["tags"],
				"autreElu"=> @$cteR["autreElu"],
				"siren"=>@$cteR["siren"],
				"preferences"=>@$cteR["preferences"],
				"nbHabitant"=>@$cteR["nbHabitant"],
				"planPCAET"=>@$cteR["planPCAET"],
				"singulartiy"=>@$cteR["singulartiy"],
				"caracteristique"=>@$cteR["caracteristique"],
				"economy"=>@$cteR["economy"],
				"inCharge"=>@$cteR["inCharge"],
				"actions"=>@$cteR["actions"],
				"actors"=>@$cteR["actors"],
				"why"=>@$cteR["why"],
				"contact"=>@$cteR["contact"],
				"creator"=>Yii::app()->session["userId"],
				"source"=>$cteR["source"],
				"collection"=>Project::COLLECTION,
				"modified" => new MongoDate(time()),
				"updated" => time(),
				"created" => time(),
				"links"=>array()
			);
			if(@$cteR["parent"])
				$newCRTE["parent"]=$cteR["parent"];
			if(@$cteR["scope"])
				$newCRTE["scope"]=$cteR["scope"];
			if(@$cteR["address"])
				$newCRTE["address"]=$cteR["address"];
			if(@$cteR["geo"])	
				$newCRTE["geo"]=$cteR["geo"];
			if(@$cteR["geoPosition"])	
				$newCRTE["geoPosition"]=$cteR["geoPosition"];
			if(isset($cteR["links"])){
				if(isset($cteR["links"]["contributors"]))
					$newCRTE["links"]["contributors"]=$cteR["links"]["contributors"];
				if(isset($cteR["links"]["projects"]))
					$newCRTE["links"]["projects"]=$cteR["links"]["projects"];
			}
			// INSERTION DU CRTE EN DB
			Yii::app()->mongodb->selectCollection(Project::COLLECTION)->insert( $newCRTE );
			// CREATION DU SLUG DU NOUVEAU CRTE
			$slugCRTE = Slug::checkAndCreateSlug( $newName, Project::COLLECTION, (String) $newCRTE["_id"] );
            Slug::save(Project::COLLECTION, (String) $newCRTE["_id"], $slugCRTE);
            Element::updateField( Project::COLLECTION, (String) $newCRTE["_id"], "slug", $slugCRTE);
            // On ajoute le fait que le CTE a déjà été dupliqué en crte avec l'id au cas où
            PHDB::update( Project::COLLECTION, array("_id"=>new MongoId($idCter)),array('$set'=>
            	 array("duplicateCRTE"=> (string)$newCRTE["_id"])));
            
            // COPY DES LINKS CONTRIBUTOR CHEZ LES ENFANTS (links.projects)
            if(isset($newCRTE["links"]["contributors"])){
            	foreach($newCRTE["links"]["contributors"] as $k => $v){
            		$roles=(isset($v["roles"])) ? $v["roles"] : "";
            		$isAdmin=(@$v["isAdmin"])? $v["isAdmin"] : false; 
            		$pendingAdmin=(@$v[Link::IS_ADMIN_PENDING]) ? $v[Link::IS_ADMIN_PENDING] : false;
            		$isPending=(@$v[Link::TO_BE_VALIDATED]) ? $v[Link::TO_BE_VALIDATED] : false; 
            		$isInviting=false;
            		if(@$v[Link::IS_INVITING])
            			$isInviting=true;
            		if(isset($v[Link::IS_ADMIN_INVITING]))
            			$isInviting="admin";
            		Link::connect($k, $v["type"], (String) $newCRTE["_id"] , Project::COLLECTION, Yii::app()->session["userId"], "projects",$isAdmin, $pendingAdmin,$isPending, $isInviting, $roles);
            	}
            }
            // COPY DES LINKS PROJECTS CHEZ LES FICHES ACTIONS LIEES (links.projects)
            if(isset($newCRTE["links"]["projects"])){
            	foreach($newCRTE["links"]["projects"] as $k => $v){
            		Link::connect($k, $v["type"], (String) $newCRTE["_id"] , Project::COLLECTION, Yii::app()->session["userId"], "projects",false, false,false, false, ["Dispositif Porteur"]);
            	}
            }
            //TRAITEMENT DES ANSWERS POUR LES DUPLIQUER !!!!!!

            // DUPLICATION DES ANSWERS DU DISPOSITIFS CTE
            $answersCTE=PHDB::find(Answer::COLLECTION, array("cterSlug"=>$cteR["slug"]));
            if(!empty($answersCTE)){
            	foreach($answersCTE as $k => $v){
            		$newAnswer=$v;
            		unset($newAnswer["_id"]);
            		$newAnswer["cterSlug"]=$slugCRTE;
            		$newAnswer["formId"]="action|caracter|murir|suivre";
					if(isset($newAnswer["context"])){
						$newContext=$newAnswer["context"];
						foreach($newContext as $key => $par){
            				if($par["type"]==Project::COLLECTION)
            					unset($newContext[$key]);
            			}
            			$newContext[(String) $newCRTE["_id"]]=array("type"=>Project::COLLECTION, "name"=>$newCRTE["name"]);
            			$newAnswer["context"]=$newContext;
					}
					Yii::app()->mongodb->selectCollection(Answer::COLLECTION)->insert( $newAnswer );
					if(isset($newAnswer["answers"]["project"]["id"])){
						Link::connect($newAnswer["answers"]["project"]["id"], Project::COLLECTION, (string)$newAnswer["_id"] , Answer::COLLECTION, Yii::app()->session["userId"], "answers");
					}
            	}
            }
            //Copies DES Orientation
            // $badges=Badge::getByWhere(array("parent.".$idCter => array('$exists'=>true)));
            // foreach($badges as $k => $v)
            // {
            //     PHDB::update( Badge::COLLECTION, ["_id" => new MongoId($k) ], 
            //         ['$set'=>["parent.".(String) $newCRTE["_id"] => ["type"=>Project::COLLECTION, "name"=>$newName ] ] ] );       
            // }
            $res=array("result"=>true, "newCRTE"=> true, "hash"=>"#page.type.projects.id.".(String) $newCRTE["_id"]);
            
		}
		return Rest::json($res);
	}
}
