<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\script;

use CAction, Role, Yii, PHDB, Organization, Project, Form, Ctenat, MongoId;
 /**
  * TEst Data Tree structure and show statistics for a source key 
  * @param String $srcKey show by source key
  * echo 
  - red : blockers
  - orange : warnings 
  - black : informations ...
  */
class ValidataAction extends \PixelHumain\PixelHumain\components\Action
{

	/*
	params 
	no params 
	by source.key
	by cter slug

	*/
	const SOURCEKEY = "ctenat";
	const SEP = "<br/><hr style='border-top: 5px solid red;'/><br/>";
	public function run( $srcKey=null, $cter=null, $errorsOnly = null ){
		$controller = $this->getController();

		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"])))
		{
			

			/*
			test si orga.source.key:ctenat openEdition:fasle > warning red
			*/
			$str = self::SEP;
			$str .= "[X] organizations openEdition";
			$elms=PHDB::find(Organization::COLLECTION, ["source.key"=>self::SOURCEKEY,"preferences.isOpenEdition"=>"true"],["name","slug"]);
			if(count($elms)){
				$str .= "[X] organizations.source.key:".self::SOURCEKEY." openEdition:true".count($elms)."<br/>";
	  			foreach($elms as $k => $v){
	  				if(!$errorsOnly)$str .= "<span style='color:orange'><a href='/costum/co/index/id/".self::SOURCEKEY."#@".$v["slug"]."'>".$v["name"]."</a></span><br/>";
	  			}
	  		} 
			/*
			test si projects.source.key:ctenat openEdition:fasle > warning red
			*/
			$str .= self::SEP;
			$str .= "[X] projects openEdition";
			$elms=PHDB::find(Project::COLLECTION, ["source.key"=>self::SOURCEKEY,"preferences.isOpenEdition"=>"true"],["name","slug"]);
			if(count($elms)){
				
				$str .= "[X] projects.source.key:".self::SOURCEKEY." openEdition:true".count($elms)."<br/>";
	  			foreach($elms as $k => $v){
	  				if(!$errorsOnly)$str .= "<span style='color:orange'><a href='/costum/co/index/id/".self::SOURCEKEY."#@".$v["slug"]."'>".$v["name"]."</a></span><br/>";
	  			}
	  		} 

	  		/*
			test count answers.source.key:ctenat
			*/
			$str .= self::SEP;
			$str .= "[X] Nb de Fiche Actions (answers.source.key:".self::SOURCEKEY.") : <b>".PHDB::count(Form::ANSWER_COLLECTION,["source.key"=>self::SOURCEKEY]).'</b>';

			/*
			test projects.catgeory.cteR
			*/
			$str .= self::SEP;
			$str .= "[X] Nb de Fiche Actions (projects.category.cteR ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"cteR"])."</b>";
			$str .= "<br/>[X] Nb de Fiche Actions avec status(projects.category.cteR && source.status.ctenat ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"cteR","source.status.ctenat"=>['$exists'=>1]])."</b>";
			


			/*
				count projects.category.ficheAction 
			*/
			$str .= self::SEP;
			$str .= "[X] Nb de Fiche Actions (projects.category.ficheAction ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"ficheAction"])."</b>";
			$str .= "<br/>[X] Nb de Fiche Actions avec status(projects.category.ficheAction && source.status.ctenat ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"ficheAction","source.status.ctenat"=>['$exists'=>1]])."</b>";

			
			$actions = PHDB::find(Form::ANSWER_COLLECTION,["source.key"=>"ctenat"]);
				/*
				check source.status.ctenat dans cette liste
					//refactor à rapatrier dans le costum 
			*/
			$actionStatus = [
				Ctenat::STATUT_ACTION_COMPLETED ,
			    Ctenat::STATUT_ACTION_VALID ,
			    Ctenat::STATUT_ACTION_MATURATION ,
			    Ctenat::STATUT_ACTION_LAUREAT ,
			    Ctenat::STATUT_ACTION_REFUSE ,
			    Ctenat::STATUT_ACTION_CANDIDAT ,
			    Ctenat::STATUT_ACTION_CONTRACT ,
			];
			$countNoProject = 0;
			$str .= self::SEP;
			$str .= "<h3> Nb de Fiche Actions sans PROJECT</h3>";
			foreach ($actions as $id => $a) {
				if( !isset($a["source"]["status"]) || !in_array($a["source"]["status"],$actionStatus )  ){
					if( isset($a["answers"]) && isset($a["answers"][ $a["formId"] ] ["answers"]["project"]["id"]) )
						if(isset($a["answers"]))
						{
							$p = PHDB::findOne(Project::COLLECTION,[
												"_id" => new MongoId( $a["answers"][ $a["formId"] ] ["answers"]["project"]["id"] ) 
												] );
							if(!isset($p) )
							{
								$str .= "<br/><span style='font-size:10px;'>answer id : ".$id."</span>";
							 	$str .= "<br/><span style='color:orange'>project not exist :  (".$a["answers"][ $a["formId"] ] ["answers"]["project"]["id"].") cte : ".$a["formId"]."</span>";
							 	$str .= "<br/><span style='font-size:10px;'>>>>>".$a["answers"][ $a["formId"] ] ["answers"]["project"]["name"]."</span>";
							 	//$str .= "<br/><span style='font-size:10px;'>>>>>".@$a["source"]["status"]."</span>";
							 	$str .= "<br/><span style='font-size:10px;'>>>>>".( (!empty($a["priorisation"])) ? $a["priorisation"] : "<b style='color:red'>no status</b>")."</span>";
							 	$countNoProject++;
							 	if(isset($a["answers"][ $a["formId"] ] ["answers"]["project"]["name"] ) )
								{
									$p = PHDB::findOne(Project::COLLECTION,[
											"name" => $a["answers"][ $a["formId"] ] ["answers"]["project"]["name"] ] );
									if(isset($p) )
										$str .= "<br/><span style='font-size:10px;color:red'>project found by name:  (".$p["_id"].") cte : ".$a["formId"]."</span>";
									
								}
								$str .= "<br/>";
							} 
						} else {
							$str .= "<br/><span style='color:red'>".$a["formId"]."_NO ANWSER</span>";
							$countNoProject++;
						}
						
						
					} else {
						$countNoProject++;
						$str .= "<br/><span style='color:orange'>no project: ".$a["_id"]."</span>";
					}
			}

			$str .= "<br/>[X] Nb de Fiche Actions sans project : <b>".$countNoProject."/".count($actions)."</b>";

			$countNoPorteur = 0;
			$str .= self::SEP;
			$str .= "<h3> Nb de Fiche Actions sans PORTEUR</h3>";
			foreach ($actions as $id => $a) {
//<<<<<<< HEAD
				
					if( isset($a["answers"][ $a["formId"] ] ["answers"]["organization"]["id"]) )
					{
						
							$o = PHDB::findOne(Organization::COLLECTION,[
												"_id" => new MongoId( $a["answers"][ $a["formId"] ] ["answers"]["organization"]["id"] ) 
												] );
							if(!isset($o) )
							{
								$str .= "<br/><span style='font-size:10px;'>answer id : ".$id."</span>";
							 	$str .= "<br/><span style='color:orange'>organization not exist :  (".$a["answers"][ $a["formId"] ] ["answers"]["organization"]["id"].") cte : ".$a["formId"]."</span>";
							 	$str .= "<br/><span style='font-size:10px;'>>>>>".$a["answers"][ $a["formId"] ] ["answers"]["organization"]["name"]."</span>";
							 	//$str .= "<br/><span style='font-size:10px;'>>>>>".@$a["source"]["status"]."</span>";
							 	$str .= "<br/><span style='font-size:10px;'>>>>>".( (!empty($a["priorisation"])) ? $a["priorisation"] : "<b style='color:red'>no status</b>")."</span>";
							 	$countNoPorteur++;
							 	
								$str .= "<br/>";
							} 
					}
// =======
// 				if( !isset($a["source"]["status"]) || !in_array($a["source"]["status"],$actionStatus ) )
// 					//$p = PHDB::findOne(Project::COLLECTION,["_id"=>new MongoId()]);
// 					if(isset($a["answers"][ $a["formId"] ] ["answers"]["project"])){
// 						if(!$errorsOnly)$str .= "<br/><span style='color:orange'><a href=''>".$a["answers"][ $a["formId"] ] ["answers"]["project"]["name"]."</a></span>";
// 					}
// 					else
// 						$str .= "<br/><span style='color:red'>No Project on answer ".$a["_id"]." : cte : ".$a["formId"]."</span>";
// >>>>>>> master
			}

			$str .= "<br/>[X] Nb de Fiche Actions sans PORTEUR : <b>".$countNoPorteur."/".count($actions)."</b>";

			//all projects of CTER
			if(isset($cter)){
				//db.getCollection('answers').find({category:"ficheAction",formId:""})
				$answers=PHDB::find(Form::ANSWER_COLLECTION, ["formId"=>$cter]);
				if(count($answers)){
		  			foreach($answers as $k => $v){
		  				if(isset($v["answers"][$cter]["answers"]["project"])){
			  				$p = PHDB::findOne(Project::COLLECTION,["_id"=>new MongoId($v["answers"][$cter]["answers"]["project"]["id"])]);
			  				if(!$errorsOnly)$str .= "<br/><span style='color:orange'><a href='/costum/co/index/id/".self::SOURCEKEY."#@".$p["slug"]."'>".$v["answers"][$cter]["answers"]["project"]["name"]."</a></span>";
		  				} else 
		  					$str .= "<br/><span style='color:red'>No Project on answer ".$v["_id"]."</span>";
		  			}
		  		} 
			}
			/*
			test pour chaque answers test 
				project exist
					links contributors valid 
					links organisations valid 
					links projects 
					links answers
				orga 
				actionPrincipale valid badge 
				actionsecondaire valid badge 
				ciblePrincipale valid badge
				cibleSecondaire valid badge
				indicateur valid POI
					checlk list domaineAction et objectifDD valid 
					 $indicators = PHDB::find(Poi::COLLECTION, $whereI, array("name", "domainAction", "objectifDD"));
						

				check priorisation status dans cette liste
					//refactor à rapatrier dans le costum 
					list : [
						"Action validée",
						"Action en maturation",
						"Action lauréate",
						"Action refusée",
						"Action Candidate"
						]

			*/

			$str .= self::SEP;
			$str .= "<h2>Feature List</h2>";
			$str .= self::SEP;
			$str .= "IMPORT BADGE<br/>";
			$str .= "> create badge <br/>";
			$str .= "> Link <br/>";
			$str .= self::SEP;
			$str .= "IMPORT INDICATEUR<br/>";
			$str .= "> create poi.indicator <br/>";
			$str .= self::SEP;
			$str .= "IMPORT ACTIONS <br/>";
			$str .= "> create cter <br/>";
			$str .= "> create answer <br/>";
			$str .= "> create orga <br/>";
			$str .= "> create project(action) <br/>";
			$str .= "> create all links <br/>";


			$params = array() ;
			if(Yii::app()->request->isAjaxRequest)
				return $controller->renderPartial("/custom/ctenat/".$page,$params,true);
			else 
				return $str;
			
		}else{
  			return "Accès réservé au Big Bosses !";
  		}	
	}
}