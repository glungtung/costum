<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api;

use CAction, Ctenat, SearchNew, PHDB, Form, Organization, MongoId, Project, ArrayHelper, Rest;
class AnswersctercsvAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($slug = null,$admin=null,$id=null,$type=null) {
		$controller=$this->getController();
		ini_set('max_execution_time',10000000);

		$params = array("searchType"=>["answers"], "filters" => array("cterSlug"=>$slug , "priorisation"=>Ctenat::STATUT_ACTION_VALID), "indexStep" => 0, "source.key" => "ctenat");

		if ($id != null && $type != null) {
			$resS = SearchNew::searchAdmin($params, $type, $id);
		}else{
			$resS = SearchNew::searchAdmin($_POST);
		}

		$answerList = $resS['results'];

		if (sizeof($answerList) != 0 ) {
			$formParent =  PHDB::findOneById(Form::COLLECTION, array_values($answerList)[0]["form"]);


			$from = $formParent["params"]["period"]["from"];
			$to = $formParent["params"]["period"]["to"];

		}

		$newAnswer = array();
		$allPath = array();

		foreach ($answerList as $key => $value) {

			$newList = array();

			$newList["Statut Action"] = @$answerList[$key]["priorisation"];
			
			// $newList = array();
				
			$newList["Id Action"] = $key;

			if(!empty($answerList[$key]["numAction"])){
				$newList["Numéro Action"] = $answerList[$key]["numAction"];
			}
				
			if(!empty($answerList[$key]["answers"]["action"]["parents"][0]["id"])){
				$cter = PHDB::findOne(Organization::COLLECTION, array("_id"=>new MongoId($answerList[$key]["answers"]["action"]["parents"][0]["id"])),array("name"));
				$newList["Nom du dispositif"] = $cter["name"] ;
			}
			
			if(!empty($answerList[$key]["answers"]["project"]) && !empty($answerList[$key]["answers"]["project"]["name"])){
				$newList["Nom Action"] = $answerList[$key]["answers"]["project"]["name"];
				// unset($answerList[$key]["answers"]["project"]);
			}

			if(!empty($answerList[$key]["answers"]["project"][0]["id"])){
				$actn = PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId($answerList[$key]["answers"]["project"][0]["id"])),array("name"));
				$newList["Nom Action"] = $actn["name"] ;

				// $newList["Nom Action"] = $answerList[$key]["answers"]["project"]["name"];
				// unset($answerList[$key]["answers"]["project"]);
			}

			if(!empty($answerList[$key]["answers"]["action"]["project"][0]["id"])){
				$actn = PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId($answerList[$key]["answers"]["action"]["project"][0]["id"])),array("name"));
				$newList["Nom Action"] = $actn["name"] ;

				// $newList["Nom Action"] = $answerList[$key]["answers"]["project"]["name"];
				// unset($answerList[$key]["answers"]["project"]);
			}

			if(!empty($answerList[$key]["answers"]["project"][0]["id"])){
				$actn = PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId($answerList[$key]["answers"]["project"][0]["id"])),array("name"));
				$newList["Nom Action"] = $actn["name"] ;

				// $newList["Nom Action"] = $answerList[$key]["answers"]["project"]["name"];
				// unset($answerList[$key]["answers"]["project"]);
			}

			// Rest::json($answerList[$key]);exit();

				if( !isset($answerList[$key]["answers"]) ) 
					$answerList[$key]["answers"] = array();

				/*if(!empty($answerList[$key]["answers"]["project"]) && !empty($answerList[$key]["answers"]["project"]["id"])){
					$project = PHDB::findOneById(Project::COLLECTION,$answerList[$key]["answers"]["project"]["id"], array("name", "description", "shortDescription", "tags", "expected") );
					$newList[$key]["Nom Action"] = $project["name"];
					unset($answerList[$key]["answers"]["project"]);
				}*/

				
				// if(!empty($project["shortDescription"])){
				// 	$newList[$key]["Description courte"] = $project["shortDescription"];
				// }

				/*$newList[$key]["Porteurs"] = "";
				if(!empty($answerList[$key]["answers"]["organization"])  && !empty($answerList[$key]["answers"]["organization"]["id"])){
					$org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"]["organization"]["id"], array("name", "type") );
					$newList[$key]["Porteurs"] .= $org["name"]." " ;
					unset($answerList[$key]["answers"]["organization"]);
				}*/

				$newList["Porteurs"] = "";
			if(!empty($answerList[$key]["answers"]["organizations"]) || !empty($answerList[$key]["answers"]["organization"])) {
				$answerorga = "";
				if (!empty($answerList[$key]["answers"]["organizations"])) {
					$answerorga = "organizations";
				}
				else if (isset($answerList[$key]["answers"]["organization"]) && !empty($answerList[$key]["answers"]["organization"])) {
					$answerorga = "organization";
				}
				if ($answerorga != "") {
					if (isset($answerList[$key]["answers"][$answerorga]["id"])) {
						$org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"][$answerorga]["id"], array("name", "type") );
								// $newList["Porteurs"][] = array("Nom" => $org["name"],
								// 											"Type" => $org["type"]) ;
								$newList["Porteurs"] .= "\n".$org["name"]." " ;

					}else{
						foreach ($answerList[$key]["answers"][$answerorga] as $keyORG => $valueORG) {
							if($keyORG != "orgasLinked"){
								$org = PHDB::findOneById(Organization::COLLECTION,$valueORG["id"], array("name", "type") );
								// $newList["Porteurs"][] = array("Nom" => $org["name"],
								// "Type" => $org["type"]) ;
								$newList["Porteurs"] .= "\n".$org["name"]." " ;
							}elseif (isset($answerList[$key]["answers"][$answerorga][$keyORG]["id"])) {
								$org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"][$answerorga][$keyORG]["id"], array("name", "type") );
								// $newList["Porteurs"][] = array("Nom" => $org["name"],
								// 											"Type" => $org["type"]) ;
								$newList["Porteurs"] .= "\n".$org["name"]." " ;
							}else {
		                        foreach ($valueORG as $keyORG2 => $valueORG2) {
		                            if($keyORG2 !== "orgasLinked" && isset($valueORG2["id"])){
		                                $org = PHDB::findOneById(Organization::COLLECTION,$valueORG2["id"], array("name", "type") );
		                                if(!empty($org) && !empty($org["name"])){

		                                    $newList["Porteurs"] .= "\n".$org["name"]." " ;
		                                }
		                            }
		                        }
		                    }
						}
					}
					unset($answerList[$key]["answers"][$answerorga]);
				}
			} elseif (!empty($answerList[$key]["answers"]["action"]["parents"])) {
				
					foreach ($answerList[$key]["answers"]["action"]["parents"] as $keyORG => $valueORG) {
						if($keyORG != "orgasLinked"){
							$org = PHDB::findOneById(Organization::COLLECTION,$valueORG["id"], array("name", "type") );
							// $newList["Porteurs"][] = array("Nom" => $org["name"],
							// "Type" => $org["type"]) ;
							$newList["Porteurs"] .= "\n".$org["name"]." " ;
						}elseif (isset($answerList[$key]["answers"]["action"]["parents"][$keyORG]["id"])) {
							$org = PHDB::findOneById(Organization::COLLECTION,$answerList[$key]["answers"]["action"]["parents"][$keyORG]["id"], array("name", "type") );
							// $newList["Porteurs"][] = array("Nom" => $org["name"],
							// 											"Type" => $org["type"]) ;
							$newList["Porteurs"] .= "\n".$org["name"]." " ;
						}else {
	                        foreach ($valueORG as $keyORG2 => $valueORG2) {
	                            if($keyORG2 !== "orgasLinked" && isset($valueORG2["id"])){
	                                $org = PHDB::findOneById(Organization::COLLECTION,$valueORG2["id"], array("name", "type") );
	                                if(!empty($org) && !empty($org["name"])){

	                                    $newList["Porteurs"] .= "\n".$org["name"]." " ;
	                                }
	                            }
	                        }
	                    }
					}
					
					unset($answerList[$key]["answers"]["action"]["parents"]);
			}

			if(!empty($answerList[$key]["priorisation"])){
				$newList["Statut Action"] = $answerList[$key]["priorisation"];
			}

			$totalF = 0;
			$totalI = 0;
			$totalregion = 0;
			$totaletat = 0;
			$totaldepartement = 0;
			$totalpublic = 0;
			$totalautre = 0;
			$totaleurope = 0;
			$totalprivate = 0;
			$totalacteur = 0;
			$totalcolfinanceur = 0;
			$totalademe = 0;
			$totalcerema = 0;
			$totalbpi = 0;
			$totaleau = 0;
			$totalbio = 0;
			$totalafd = 0;
			$totalvnf = 0;
			$totalfam = 0;
			$totalbdt = 0;
			$totalofficeNatForet = 0;
								

			if( !empty($answerList[$key]["answers"]["murir"]) ) {
				if(!empty($answerList[$key]["answers"]["murir"]["budget"])){
					foreach ($answerList[$key]["answers"]["murir"]["budget"] as $keyB => $valB) {

						$fin = [];
						$fin["Libellé"] = $valB["poste"];

						$soustotal = 0;

						for ($i = $from; $i <= $to ; $i++) { 
							if (isset($valB["amount".$i])) {
								$fin[$i] = $valB["amount".$i];
								$soustotal += $valB["amount".$i];
							}
						}
								
						if($valB["nature"] == "fonctionnement"){
							$totalF += $soustotal;
								
						} else if($valB["nature"] == "investissement"){
									$totalI += $soustotal;
						}
						
					}
				}

				if(!empty($answerList[$key]["answers"]["murir"]["planFinancement"])){
					foreach ($answerList[$key]["answers"]["murir"]["planFinancement"] as $keyB => $valB) {
							if(!empty($valB["valid"]) && $valB["valid"]=="validated"){

							$fin = [];
							$fin["Libellé"] = $valB["title"];

							$soustotal = 0;

							for ($i = $from; $i <= $to ; $i++) { 
								if (isset($valB["amount".$i])) {
									$fin[$i] = $valB["amount".$i];
									$soustotal += $valB["amount".$i];
								}
							}

							$fin["validation"] = $valB["valid"];

							if($valB["financerType"] == "acteursocioeco"){
								//$acteur[] = $fin ;
								$totalacteur += $soustotal;
							}else if($valB["financerType"] == "region"){
								//$region[] = $fin ;
								$totalregion += $soustotal;
							} else if($valB["financerType"] == "etat"){
								//$etat[] = $fin ;
								$totaletat += $soustotal;
							} else if($valB["financerType"] == "departement"){
								//$departement[] = $fin ;
								$totaldepartement += $soustotal;
							} else if($valB["financerType"] == "autre"){
								$totalautre += $soustotal;
							} else if($valB["financerType"] == "public"){
								//$public[] = $fin ;
								$totalpublic += $soustotal;
							} else if($valB["financerType"] == "europe"){
								//$europe[] = $fin ;
								$totaleurope += $soustotal;
							}else if($valB["financerType"] == "private"){
								//$private[] = $fin ;
								$totalprivate += $soustotal;
							}else if($valB["financerType"] == "ademe"){
								//$ademe[] = $fin ;
								$totalademe += $soustotal;
							}else if($valB["financerType"] == "cerema"){
								//$cerema[] = $fin ;
								$totalcerema += $soustotal;
							}else if($valB["financerType"] == "bpi"){
								//$bpi[] = $fin ;
								$totalbpi += $soustotal;
							}else if($valB["financerType"] == "bdt"){
								//$bdt[] = $fin ;
								$totalbdt += $soustotal;
								
							}else if($valB["financerType"] == "agenceLeau"){
								//$eau[] = $fin ;
								$totaleau += $soustotal;
							}else if($valB["financerType"] == "agenceBiodiv"){
								//$bio[] = $fin ;
								$totalbio += $soustotal;
							}else if($valB["financerType"] == "afd"){
								//$afd[] = $fin ;
								$totalafd += $soustotal;
							}else if($valB["financerType"] == "vn2f"){
								//$vnf[] = $fin ;
								$totalvnf += $soustotal;
							}else if($valB["financerType"] == "franceAgirMer"){
								//$fam[] = $fin ;
								$totalfam += $soustotal;
							}else if($valB["financerType"] == "colfinanceur"){
								//$totalcolfinanceur[] = $fin ;
								$totalcolfinanceur += $soustotal;
							}else if($valB["financerType"] == "officeNatForet"){
								//$totalcolfinanceur[] = $fin ;
								$totalofficeNatForet += $soustotal;
							}
						}
					}
				}
			}



				$newList["Somme des dépenses"] = $totalF + $totalI ;
				$newList["En Fonctionnement"] = $totalF ;
				$newList["En Investissement"] = $totalI ;
				
				$newList["Somme Acteur socio-économique"] = $totalacteur;
				$newList["Somme Communes - intercommunalité-syndicat"] = $totalcolfinanceur;
				$newList["Somme Département"] = $totaldepartement;
				$newList["Somme Region"] = $totalregion;
				$newList["Somme Europe"] = $totaleurope;
				//$newList["Somme Autre"] = $totalautre;
				$newList["Somme Etat"] = $totaletat + $totalademe + $totalcerema + $totalbdt + $totalbpi + $totaleau + $totalbio + $totalafd + $totalvnf + $totalfam + $totalautre + $totalofficeNatForet;
				$newList["Somme Etat - Services déconcentrés / préfecture"] = $totaletat;
				$newList["Somme Etat - ADEME"] = $totalademe;
				$newList["Somme Etat - CEREMA"] = $totalcerema;
				$newList["Somme Etat - Banque des territoires"] = $totalbdt;
				$newList["Somme Etat - BPI"] = $totalbpi;
				$newList["Somme Etat - Agence / office de l’eau"] = $totaleau;
				$newList["Somme Etat - Office français de la biodiversité"] = $totalbio;
				$newList["Somme Etat - Office national des forêts"] = $totalofficeNatForet;
				$newList["Somme Etat - Agence française de développement"] = $totalafd;
				$newList["Somme Etat - Voies navigables de France"] = $totalvnf;
				$newList["Somme Etat - FranceAgriMer"] = $totalfam;
				$newList["Somme Etat - Autre"] = $totalautre;

				$newList["Somme de tous les financements validés"] = $totalacteur + $totalcolfinanceur + $totaldepartement + $totalregion + $totaleurope + $newList["Somme Etat"] ;

				
				//$newList["Somme Public"] = $public;
				
				//$newList["Somme Private"] = $private;

				// $newList["Plan Financement Total - Etat"] = $totaletat;
				// $newList["Plan Financement Total - Département"] = $totaldepartement;
				// $newList["Plan Financement Total - Region"] = $totalregion;
				// $newList["Plan Financement Total - Autre"] = $totalautre;
				// $newList["Plan Financement Total - Europe"] = $totaleurope;
				// $newList["Plan Financement Total - Private"] = $totalprivate;
			// }

				$allPath = ArrayHelper::getAllPathJson(json_encode($newList), $allPath, false);
				$newAnswer[] = $newList;

		}

					$financerToSum = [];
		$financerToSum["Somme des dépenses"] = 0;
		$financerToSum["En Fonctionnement"] = 0;
		$financerToSum["En Investissement"] = 0;
		$financerToSum["Somme de tous les financements validés"] = 0;
		$financerToSum["Somme Acteur socio-économique"] = 0;
		$financerToSum["Somme Communes - intercommunalité-syndicat"] = 0;
		$financerToSum["Somme Département"] = 0;
		$financerToSum["Somme Region"] = 0;
		$financerToSum["Somme Europe"] = 0;
		//"Somme Autre",
		$financerToSum["Somme Etat"] = 0;
		$financerToSum["Somme Etat - Services déconcentrés / préfecture"] = 0;
		$financerToSum["Somme Etat - ADEME"] = 0;
		$financerToSum["Somme Etat - CEREMA"] = 0;
		$financerToSum["Somme Etat - Banque des territoires"] = 0;
		$financerToSum["Somme Etat - BPI"] = 0;
		$financerToSum["Somme Etat - Agence / office de l’eau"] = 0;
		$financerToSum["Somme Etat - Office français de la biodiversité"] = 0;
		$financerToSum["Somme Etat - Office national des forêts"] = 0;
		$financerToSum["Somme Etat - Agence française de développement"] = 0;
		$financerToSum["Somme Etat - Voies navigables de France"] = 0;
		$financerToSum["Somme Etat - FranceAgriMer"] = 0;
		$financerToSum["Somme Etat - Autre"] = 0;

		foreach ($newAnswer as $keynewL => $valuenewL) {
			foreach ($valuenewL as $keyvaluenewL => $valuevaluenewL) {
				if (isset($financerToSum[$keyvaluenewL])) {
					$financerToSum[$keyvaluenewL] += $valuevaluenewL;
				}
			}
		}

		$newAnswer[] = $financerToSum;

		$order = array(
			"Numéro Action",
			"Nom Action",
			"Statut Action",
			"Porteurs",
			"Somme des dépenses",
			"En Fonctionnement",
			"En Investissement",
			"Somme de tous les financements validés",
			"Somme Acteur socio-économique",
			"Somme Communes - intercommunalité-syndicat",
			"Somme Département",
			"Somme Region",
			"Somme Europe",
			//"Somme Autre",
			"Somme Etat",
			"Somme Etat - Services déconcentrés / préfecture",
			"Somme Etat - ADEME",
			"Somme Etat - CEREMA",
			"Somme Etat - Banque des territoires",
			"Somme Etat - BPI",
			"Somme Etat - Agence / office de l’eau",
			"Somme Etat - Office français de la biodiversité",
			"Somme Etat - Office national des forêts",
			"Somme Etat - Agence française de développement",
			"Somme Etat - Voies navigables de France",
			"Somme Etat - FranceAgriMer",
			"Somme Etat - Autre"
		);

		$sortOrder = array();
		$fieldsMultiple=array();
		// $newAnswer = (array) $newList;

		foreach ($order as $keysortOrder => $valsortOrder) {

			if(empty( $fieldsMultiple[$valsortOrder]))
				$fieldsMultiple[$valsortOrder] = array();
			foreach ($allPath as $key => $value) {
				$pos = strpos($value, $valsortOrder);
				if($pos !== false)
					$fieldsMultiple[$valsortOrder][] = $value;
			}
			sort($fieldsMultiple[$valsortOrder], SORT_FLAG_CASE);
			$sortOrder = array_merge($sortOrder, $fieldsMultiple[$valsortOrder]);
		}

		// Rest::json($newList);
		$reees = array("results" => $newAnswer , "fields"=> $sortOrder  );
		return Rest::json($reees);

		//Rest::csv($newList, false, false, null, $order);

		// Yii::app()->end();

	}
}