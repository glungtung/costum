<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api;

use CAction, CacheHelper, SearchNew, PHDB, Form, Project, Organization, Yii, Badge, Poi, Ctenat, ArrayHelper, Rest;
class AnswerscsvAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($typecsv = null, $id = null, $type = null) {
		$controller=$this->getController();
		$costum = CacheHelper::getCostum();
		ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '-1');
		// On récupere la donnée en fonction des filtres
		// Rest::json($id);exit();
		if ($id != null && $type != null) {
			$resS = SearchNew::searchAdmin($_POST, $type, $id);
		}else{
			$resS = SearchNew::searchAdmin($_POST);
		}
		$answers = $resS["results"];

		$newList = array();

		$listParents = array(); // Liste des parents afin d'éviter de faire plusieurs requetes si le parent se retrouve sur plusieur answers 
		$listProjects = array(); // Liste des projet afin d'éviter de faire plusieurs requetes sur les projets qui peuvent se retrouver sur plusieurs answers 
		$orderIndicateurs = array();

		$allPath = array();

		$formParent =  PHDB::findOneById(Form::COLLECTION, array_values($answers)[0]["form"]);

		$from = $formParent["params"]["period"]["from"];
		$to = $formParent["params"]["period"]["to"];
		
		// On parcourt les answers afin de restructurer la donnée pour le parser
		foreach ($answers as $key => $answer) {
			
			$project = null;
			// Si l'answer n'a pas de projet, on ne l'integre pas dans la liste
            // Si le contexte de l'answer n'existe plus, on ne l'integre pas dans la liste'
			if(	!empty($answer["user"]) && 
				!empty($answer["answers"]) &&
				!empty($answer["answers"]["action"]["project"]) && 
				!empty($answer["answers"]["action"]["project"][0]) && 
				!empty($answer["answers"]["action"]["project"][0]["id"]) &&
                !empty(array_keys($answer["context"])[1]) &&
                !empty(PHDB::findOneById($answer["context"][array_keys($answer["context"])[1]]["type"], array_keys($answer["context"])[1]))

            ) {
			    $newAnswer = array();

				if(isset($answer["numAction"])){
					$newAnswer["Numéro Action"] = $answer["numAction"];
				}

				$newAnswer["Id Action"] = $answer["answers"]["action"]["project"][0]["id"];
				// On récupere la donnée du projet
				if( !empty($answer["answers"]["action"]["project"]) && 
					!empty($answer["answers"]["action"]["project"][0]["id"])){
					if(!empty($listProjects[$answer["answers"]["action"]["project"][0]["id"]])) {
                        $project = $listProjects[$answer["answers"]["action"]["project"][0]["id"]];

                    }
					$project = PHDB::findOneById(Project::COLLECTION,$answer["answers"]["action"]["project"][0]["id"]);
                    if (!empty($project)){
                        if (!empty($project["dispositif"])){
                            $newAnswer["Dispositif"] = $project["dispositif"];
                        }else{
                            $newAnswer["Dispositif"] = "";
                        }

                        if (!empty($project["dispositifId"])){
                            $newAnswer["DispositifId"] = $project["dispositifId"];
                        }else{
                            $newAnswer["DispositifId"] = "";
                        }

                    }
					unset($answer["answers"]["action"]["project"]);
				}

				if(!empty($answer["numAction"])){
					$newAnswer["Numéro Action"] = $answer["numAction"];
				}

				// En fonction du $typecsv on retroune certaines informations
				if(empty($typecsv) || (!empty($typecsv) && $typecsv === "indicateursfinancements")){
					$orgsListName = array();
					// On récupere les parents
					if( !empty($answer["answers"]["action"]["parents"]) ){
	                    foreach ($answer["answers"]["action"]["parents"] as $keyP => $valP) {
	                        $org = null ;
	                        if(!empty($valP["id"])){
	                            if(!empty($listParents[$valP["id"]]))
	                                $org = $listParents[$valP["id"]];
	                            else
	                                $org = PHDB::findOneById(Organization::COLLECTION,$valP["id"], array("name", "type", "typeStruct") );
	                            
	                            if(!empty($org["name"])){
	                            	$orgsListName[] = array("Nom" => $org["name"],
															"Type" => Yii::t("common",$org["type"]),"statut" => @$org["typeStruct"]) ;
	                            }

	                            
	                        }
	                    }
	                    unset($answer["answers"]["action"]["parents"]);
	                }

	                $newAnswer["Porteurs"] = $orgsListName;


					if(!empty($answer["name"])){
						$newAnswer["Nom Createur"] = $answer["name"];
					}

					if(!empty($answer["created"])){
						$newAnswer["Date Creation"] = date("d/m/Y", $answer["created"]);
					}
				}

				// if(!empty($answer["formId"])){
				// 	$cter = PHDB::findOne(Project::COLLECTION,array("slug" => $answer["formId"]), array("name") );
				// 	$newAnswer["Nom CTE"] = $cter["name"] ;
				// }
				$scope = array(
					"region" => "",
					"dep" => "",
					"epci" => "",
				);
				// On récupere les scopes associer au cter
        		if( !empty($answer["context"]) && 
	           		!empty($costum) && !empty($costum["contextId"])){
        			//var_dump("HERE"); exit;
		            foreach ($answer["context"] as $keyC => $valC) {
		                if($keyC != $costum["contextId"]){
		                    $cter = PHDB::findOneById($valC["type"], $keyC);
		                    if(!empty($cter) && !empty($cter["name"]))
		                        $newAnswer["Nom du dispositif"] = $cter["name"];

		                    if(!empty($cter["dispositif"])){
                                $newAnswer["Dispositif"] = $cter["dispositif"];
                            }

		                    if(!empty($cter["scope"])){
		                    	foreach ($cter["scope"] as $keyScope => $valScope) {
		                    		if(!empty($valScope['level']) && !empty($valScope['name'])){
		                    			if(in_array("5", $valScope["level"]) ||  $valScope["type"] == "level5"){
		                    				if(!empty($scope["epci"]))
		                    					$scope["epci"] .= " \n";
		                    				$scope["epci"] .= $valScope["name"];
		                    			} else if(in_array("4", $valScope["level"])){
		                    				if(!empty($scope["dep"]))
		                    					$scope["dep"] .= " \n";
		                    				$scope["dep"] .= $valScope["name"];
		                    			} else if(in_array("3", $valScope["level"])){
		                    				if(!empty($scope["region"]))
		                    					$scope["region"] .= " \n";
		                    				$scope["region"] .= $valScope["name"];
		                    			}
		                    		}
		                    	}
	                        } 

                        	if(isset($cter["address"]["level3Name"])){
	                    		$scope["region"] = $cter["address"]["level3Name"];
	                    	}

	                    	if(isset($cter["address"]["level4Name"])){
	                    		$scope["dep"] = $cter["address"]["level4Name"];
	                    	}

		                }
		            }
		        }

		        $newAnswer["Régions"] = $scope["region"];
		        $newAnswer["Départements"] = $scope["dep"];
		        $newAnswer["EPCI"] = $scope["epci"];

				if(!empty($project["name"])){
					$newAnswer["Nom Action"] = $project["name"];
				}

				if(!empty($answer["priorisation"])){
					$newAnswer["Statut Action"] = $answer["priorisation"];
				}

				if(empty($typecsv) || (!empty($typecsv) && $typecsv === "indicateursfinancements")){
					if(!empty($project["shortDescription"])){
						$newAnswer["Description courte"] = str_replace("#", "",$project["shortDescription"]);
					}

					if(!empty($project["expected"])){
						$newAnswer["Attentes"] = $project["expected"];
					}

					if(!empty($project["description"])){
						$newAnswer["Description détaillée"] = str_replace("#", "",$project["description"]);
					}

					$newAnswer["Mot-clés"] = array();
					if(!empty($project["tags"])){
						$t = array();
						foreach ($project["tags"] as $keyT => $valT) {
							$t[] = $valT;
						}
						$where = array("name" => array('$in' => $t),
										"source.key" => "ctenat" ) ;
						$badges = PHDB::find(Badge::COLLECTION, $where, array("name") );
						if( !empty($badges)){
							foreach ($badges as $keyB => $valB) {
								if(in_array($valB["name"], $t)){
									$i = array_search($valB["name"], $t);
									unset( $t[$i]);
								}
							}
						}
						$t2 = array();
						foreach ($t as $keyT => $valT) {
							$t2[] = $valT;
						}
						$newAnswer["Mot-clés"] = implode("; ", $t2);
					}

					if( !empty($answer["answers"]["caracter"]) ) {

						if(!empty($answer["answers"]["caracter"]["actionPrincipal"])){
							$newAnswer["Domaine action principal"] = $answer["answers"]["caracter"]["actionPrincipal"];
						}else
							$newAnswer["Domaine action principal"] = "";
							
						if(!empty($answer["answers"]["caracter"]["actionSecondaire"])){
							$newAnswer["Domaine action secondaire"] = implode("; ",$answer["answers"]["caracter"]["actionSecondaire"]);
						}else
							$newAnswer["Domaine action secondaire"] = "";

						if(!empty($answer["answers"]["caracter"]["cibleDDPrincipal"])){

							if(is_string($answer["answers"]["caracter"]["cibleDDPrincipal"]))
								$newAnswer["Objectif principal"] = $answer["answers"]["caracter"]["cibleDDPrincipal"];
							else
								$newAnswer["Objectif principal"] = $answer["answers"]["caracter"]["cibleDDPrincipal"][0];
						}else
							$newAnswer["Objectif principal"] = "";

						if(!empty($answer["answers"]["caracter"]["cibleDDSecondaire"])){
							$newAnswer["Objectif secondaire"] = implode("; ",$answer["answers"]["caracter"]["cibleDDSecondaire"]);
						}else
							$newAnswer["Objectif secondaire"] = "";
					} else {
						$newAnswer["Domaine action principal"] = "";
						$newAnswer["Domaine action secondaire"] = "";
						$newAnswer["Objectif principal"] = "";
						$newAnswer["Objectif secondaire"] = "";
					}

					if(!empty($answer["answers"]["murir"]["calendar"])){
						foreach ($answer["answers"]["murir"]["calendar"] as $keyC => $valC) {
						    if (!empty($valC["step"])) {
                                $newAnswer["Calendrier"][] = array("Etape" => str_replace("#", "", $valC["step"]),
                                    "Date Début" => $valC["startDate"],
                                    "Date Fin" => $valC["endDate"]);
                            }
						}
					}

					if(!empty($answer["answers"]["murir"]) && !empty($answer["answers"]["murir"]["partenaires"])){
						foreach ($answer["answers"]["murir"]["partenaires"] as $keyC => $valP) {
							$part = array("Qui" => $valP["qui"],
											"Engagement" => str_replace("#", "", $valP["engagement"]),
											"Statut" => str_replace("#", "", $valP["statut"]),
											"Next" => str_replace("#", "", $valP["next"]) ) ;
							if(!empty($valP["role"])){
								$part["Roles"] =  implode(";", $valP["role"]);
							}
							$newAnswer["Partenaires"][] = $part;
						}
					}
				}

				if(!empty($typecsv) && ($typecsv === "indicateurs" || $typecsv === "indicateursfinancements")){
					$newAnswer["Indicateurs"] = array();
					if( !empty($answer["answers"]["murir"]) ) {
						if(!empty($answer["answers"]["murir"]["results"])){

							$ind = 1;
							foreach ($answer["answers"]["murir"]["results"] as $keyC => $valR) {
								if(!empty($valR["indicateur"])){
									$name = PHDB::findOneById(Poi::COLLECTION, $valR["indicateur"], array("name", "unity1") );

									$indStr = $ind;
									if(strlen($indStr) == 1)
										$indStr = "0".$indStr;
									if(!empty($name)){
										$nene = array( "indicateur" => $name["name"].' ( '.@$name["unity1"].' )' );
										//$newAnswer["Indicateur ".$indStr] = $name["name"];
										if(!in_array("Indicateur ".$indStr, $orderIndicateurs))
											$orderIndicateurs[] = "Indicateur ".$indStr;
										if(!empty($valR["objectif"])){
											$nene["objectif"] = $valR["objectif"];
											//$newAnswer["Indicateur ".$indStr." objectif"] = $valR["objectif"];
										}
										if(!empty($valR["reality"])){
											$nene["reality"] = $valR["reality"];
											//$newAnswer["Indicateur ".$indStr." reality"] = $valR["reality"];
										}
										$ind++;
										//$newAnswer["Indicateurs"][] = $nene;
										$newAnswer["Indicateur ".$indStr] = $nene;
									}
								}
							}
						}
					}
				}

				if(!empty($typecsv) && ($typecsv === "financements" || $typecsv === "indicateursfinancements")){
					$dependFonction = array();
					$dependInvest = array();
					$totalF = 0;
					$totalI = 0;

					$region = array();
					$etat = array();
					$departement = array();
					$autre = array();
					$public = array();
					$europe = array();
					$private = array();
					$acteur = array();
					$totalregion = 0;
					$totaletat = 0;
					$totaldepartement = 0;
					$totalpublic = 0;
					$totalautre = 0;
					$totaleurope = 0;
					$totalprivate = 0;
					$totalacteur = 0;

					$colfinanceur = array() ;
					$totalcolfinanceur = 0;

					$ademe= array();
					$totalademe = 0;
					$cerema= array();
					$totalcerema = 0;
					$bpi= array();
					$totalbpi = 0;
					$eau= array();
					$totaleau = 0;
					$bio= array();
					$totalbio = 0;
					$afd= array();
					$totalafd = 0;
					$vnf= array();
					$totalvnf = 0;
					$fam= array();
					$totalfam = 0;
					$bdt= array();
					$totalbdt = 0;
					$officeNatForet= array();
					$totalofficeNatForet = 0;

					
									

					if( !empty($answer["answers"]["murir"]) ) {
						if(!empty($answer["answers"]["murir"]["budget"])){
							foreach ($answer["answers"]["murir"]["budget"] as $keyB => $valB) {
								$fin = [];
								if (!empty($valB["poste"])) {
                                    $fin["Libellé"] = $valB["poste"];
                                }

								$soustotal = 0;

								for ($i = $from; $i <= $to ; $i++) { 
									if (isset($valB["amount".$i])) {
										$fin[$i] = $valB["amount".$i];
										if (is_numeric($valB["amount".$i])) {
                                            $soustotal += $valB["amount" . $i];
                                        }
									}
								}

								if(!empty($valB["nature"]) && $valB["nature"] == "fonctionnement"){
									$dependFonction[] = $fin ;
									$totalF += $soustotal;
								} else if(!empty($valB["nature"]) && $valB["nature"] == "investissement"){
									$dependInvest[] = $fin ;
									$totalI += $soustotal;
								}
							}
						}

						if(!empty($answer["answers"]["murir"]["planFinancement"])){
							// $newAnswer["Depense fonctionnement"] = array();
							// $newAnswer["Depense Investissement"] = array();

							
							foreach ($answer["answers"]["murir"]["planFinancement"] as $keyB => $valB) {

								// if(!empty($valB["poste"]))
								// 	$valB["title"] = $valB["poste"];

								// if(!empty($valB["nature"]))
								// 	$valB["financer"] = $valB["nature"];

								$fin = [];
								if (!empty($valB["title"])) {
                                    $fin["Libellé"] = $valB["title"];
                                }else{
                                    $fin["Libellé"] = "";
                                }

								$soustotal = 0;

								for ($i = $from; $i <= $to ; $i++) { 
									if (isset($valB["amount".$i])) {
										$fin[$i] = $valB["amount".$i];
                                        if (is_numeric($valB["amount".$i])) {
                                            $soustotal += $valB["amount" . $i];
                                        }
									}
								}
								if(isset($valB["valid"])){
									$fin["validation"] = $valB["valid"];
								}

                                if (!empty($valB["financerType"])) {
                                    if ($valB["financerType"] == "acteursocioeco") {
                                        $acteur[] = $fin;
                                        $totalacteur += $soustotal;
                                    } else if ($valB["financerType"] == "region") {
                                        $region[] = $fin;
                                        $totalregion += $soustotal;
                                    } else if ($valB["financerType"] == "etat") {
                                        $etat[] = $fin;
                                        $totaletat += $soustotal;
                                    } else if ($valB["financerType"] == "departement") {
                                        $departement[] = $fin;
                                        $totaldepartement += $soustotal;
                                    } else if ($valB["financerType"] == "autre") {
                                        $autre[] = $fin;
                                        $totalautre += $soustotal;
                                    } else if ($valB["financerType"] == "public") {
                                        $public[] = $fin;
                                        $totalpublic += $soustotal;
                                    } else if ($valB["financerType"] == "europe") {
                                        $europe[] = $fin;
                                        $totaleurope += $soustotal;
                                    } else if ($valB["financerType"] == "private") {
                                        $private[] = $fin;
                                        $totalprivate += $soustotal;
                                    } else if ($valB["financerType"] == "ademe") {
                                        $ademe[] = $fin;
                                        $totalademe += $soustotal;
                                    } else if ($valB["financerType"] == "cerema") {
                                        $cerema[] = $fin;
                                        $totalcerema += $soustotal;
                                    } else if ($valB["financerType"] == "bpi") {
                                        $bpi[] = $fin;
                                        $totalbpi += $soustotal;
                                    } else if ($valB["financerType"] == "bdt") {
                                        $bdt[] = $fin;
                                        $totalbdt += $soustotal;

                                    } else if ($valB["financerType"] == "agenceLeau") {
                                        $eau[] = $fin;
                                        $totaleau += $soustotal;
                                    } else if ($valB["financerType"] == "agenceBiodiv") {
                                        $bio[] = $fin;
                                        $totalbio += $soustotal;
                                    } else if ($valB["financerType"] == "afd") {
                                        $afd[] = $fin;
                                        $totalafd += $soustotal;
                                    } else if ($valB["financerType"] == "vn2f") {
                                        $vnf[] = $fin;
                                        $totalvnf += $soustotal;
                                    } else if ($valB["financerType"] == "franceAgirMer") {
                                        $fam[] = $fin;
                                        $totalfam += $soustotal;
                                    } else if ($valB["financerType"] == "colfinanceur") {
                                        $colfinanceur[] = $fin;
                                        $totalcolfinanceur += $soustotal;
                                    } else if ($valB["financerType"] == "officeNatForet") {
                                        $officeNatForet[] = $fin;
                                        $totalofficeNatForet += $soustotal;
                                    }
                                }
							}
						}

						// if(!empty($answer["answers"]["murir"]["calendar"])){
						// 	foreach ($answer["answers"]["murir"]["calendar"] as $keyC => $valC) {
						// 		$newAnswer["Calendrier"][] = array("Etape" => str_replace("#", "",$valC["step"]),
						// 							"Date Début" => $valC["startDate"],
						// 							"Date Fin" => $valC["endDate"] ) ;
						// 	}
						// }


						// if(!empty($answer["answers"]["murir"]["partenaires"])){
						// 	foreach ($answer["answers"]["murir"]["partenaires"] as $keyC => $valP) {
						// 		$part = array("Qui" => $valP["qui"],
						// 						"Engagement" => str_replace("#", "", $valP["engagement"]),
						// 						"Statut" => str_replace("#", "", $valP["statut"]),
						// 						"Next" => str_replace("#", "", $valP["next"]) ) ;
						// 		if(!empty($valP["role"])){
						// 			$part["Roles"] =  implode(";", $valP["role"]);
						// 		}
						// 		$newAnswer["Partenaires"][] = $part;
						// 	}
						// }
					}
					
					$newAnswer["Total Dépenses HT"] = $totalF + $totalI;
					$newAnswer["Total dépenses fonctionnement (toutes années)"] = $totalF ;
					$newAnswer["Depense fonctionnement"] = $dependFonction;
					$newAnswer["Total dépenses investissement (toutes années)"] = $totalI ;
					$newAnswer["Depense Investissement"] = $dependInvest;
					$newAnswer["Plan Financement - Acteur socio-économique"] = $acteur;
					$newAnswer["Plan Financement - Communes - intercommunalité-syndicat"] = $colfinanceur;
					$newAnswer["Plan Financement - Département"] = $departement;
					$newAnswer["Plan Financement - Region"] = $region;
					$newAnswer["Plan Financement - Europe"] = $europe;
					$newAnswer["Plan Financement - Etat - Services déconcentrés / préfecture"] = $etat;
					$newAnswer["Plan Financement - Etat - ADEME"] = $ademe;
					$newAnswer["Plan Financement - Etat - CEREMA"] = $cerema;
					$newAnswer["Plan Financement - Etat - Banque des territoires"] = $bdt;
					$newAnswer["Plan Financement - Etat - BPI"] = $bpi;
					$newAnswer["Plan Financement - Etat - Agence / office de l’eau"] = $eau;
					$newAnswer["Plan Financement - Etat - Office français de la biodiversité"] = $bio;
					$newAnswer["Plan Financement - Etat - Office national des forêts"] = $officeNatForet;
					$newAnswer["Plan Financement - Etat - Agence française de développement"] = $afd;
					$newAnswer["Plan Financement - Etat - Voies navigables de France"] = $vnf;
					$newAnswer["Plan Financement - Etat - FranceAgriMer"] = $fam;
					$newAnswer["Plan Financement - Etat - Autre"] = $autre;
					//$newAnswer["Plan Financement - Public"] = $public;
					
					//$newAnswer["Plan Financement - Private"] = $private;

					// $newAnswer["Plan Financement Total - Etat"] = $totaletat;
					// $newAnswer["Plan Financement Total - Département"] = $totaldepartement;
					// $newAnswer["Plan Financement Total - Region"] = $totalregion;
					// $newAnswer["Plan Financement Total - Autre"] = $totalautre;
					// $newAnswer["Plan Financement Total - Europe"] = $totaleurope;
					// $newAnswer["Plan Financement Total - Private"] = $totalprivate;
				}
				if(empty($typecsv) || (!empty($typecsv) && $typecsv === "indicateursfinancements")){
					if(!empty($answer["validation"])){
						$newAnswer["État de la validation copil"] = (( !empty($answer["validation"]["cter"]) && !empty($answer["validation"]["cter"]["valid"])) ? Ctenat::$listValid[$answer["validation"]["cter"]["valid"]] :  " ") ;
						$newAnswer["Commentaire copil"] = (( !empty($answer["validation"]["cter"]) && !empty($answer["validation"]["cter"]["description"])) ? str_replace("#", "",$answer["validation"]["cter"]["description"]) :  " ") ;
						$newAnswer["Date validation copil"] = (( !empty($answer["validation"]["cter"]) && !empty($answer["validation"]["cter"]["date"])) ? str_replace("#", "",$answer["validation"]["cter"]["date"]) :  " ") ;



						$newAnswer["Etat de la validation régional"] = (( !empty($answer["validation"]["etat"]) && !empty($answer["validation"]["etat"]["valid"])) ? Ctenat::$listValid[$answer["validation"]["etat"]["valid"]] :  " ") ;
						$newAnswer["Commentaire régional"] = (( !empty($answer["validation"]["etat"]) && !empty($answer["validation"]["etat"]["description"])) ? str_replace("#", "",$answer["validation"]["etat"]["description"]) :  " ") ;
						$newAnswer["Date validation régional"] = (( !empty($answer["validation"]["etat"]) && !empty($answer["validation"]["etat"]["date"])) ? str_replace("#", "",$answer["validation"]["etat"]["date"]) :  " ") ;



						$newAnswer["Etat de la validation national"] = (( !empty($answer["validation"]["ctenat"]) && !empty($answer["validation"]["ctenat"]["valid"])) ? Ctenat::$listValid[$answer["validation"]["ctenat"]["valid"]] :  " ") ;
						$newAnswer["Commentaire national"] = (( !empty($answer["validation"]["ctenat"]) && !empty($answer["validation"]["ctenat"]["description"])) ? str_replace("#", "",$answer["validation"]["ctenat"]["description"]) :  " ") ;
						$newAnswer["Date validation national"] = (( !empty($answer["validation"]["ctenat"]) && !empty($answer["validation"]["ctenat"]["date"])) ? str_replace("#", "",$answer["validation"]["ctenat"]["date"]) :  " ") ;
					}
				}
				

				$allPath = ArrayHelper::getAllPathJson(json_encode($newAnswer), $allPath, false);
				$newList[] = $newAnswer;
			}
			
		}

		// L'ordre d'affichage des collone pour le csv
		$order = array(
			"Numéro Action",
			"Id Action",
			"Numéro Action",
			"Nom Createur",
			"Date Creation",
			"Nom du dispositif",
			"Nom Action",
			"Statut Action",
			"Description courte",
			"Description détaillée",
			"Régions",
			"Départements",
			"EPCI",
			"Mot-clés",
			"Attentes",
			"Porteurs",
			"Domaine action principal",
			"Domaine action secondaire",
			"Objectif principal",
			"Objectif secondaire",
		    "Dispositif",
		    "DispositifId",
		    //"Indicateurs",
			"Partenaires",
			"Calendrier",
			"Depense fonctionnement",
			"Depense Investissement",
			"Total Dépenses HT",
			"Total dépenses fonctionnement (toutes années)",
			"Total dépenses investissement (toutes années)",
			"Plan Financement - Acteur socio-économique",
			"Plan Financement - Communes - intercommunalité-syndicat",
			"Plan Financement - Département",
			"Plan Financement - Region",
			"Plan Financement - Europe",
			"Plan Financement - Etat - Services déconcentrés / préfecture",
			"Plan Financement - Etat - ADEME",
			"Plan Financement - Etat - CEREMA",
			"Plan Financement - Etat - Banque des territoires",
			"Plan Financement - Etat - BPI",
			"Plan Financement - Etat - Agence / office de l’eau",
			"Plan Financement - Etat - Office français de la biodiversité",
			"Plan Financement - Etat - Office national des forêts",
			"Plan Financement - Etat - Agence française de développement",
			"Plan Financement - Etat - Voies navigables de France",
			"Plan Financement - Etat - FranceAgriMer",
			"Plan Financement - Etat - Autre",
			"État de la validation copil",
			"Commentaire copil",
			"Date validation copil",
			"Etat de la validation régional",
			"Commentaire régional",
			"Date validation régional",
			"Etat de la validation national",
			"Commentaire national",
			"Date validation national"
		);

		sort($orderIndicateurs, SORT_FLAG_CASE);
		$order = array_merge($order, $orderIndicateurs);
		$sortOrder = array();
		$fieldsMultiple=array();
		foreach ($order as $keysortOrder => $valsortOrder) {

			if(empty( $fieldsMultiple[$valsortOrder]))
				$fieldsMultiple[$valsortOrder] = array();
			foreach ($allPath as $key => $value) {
				$pos = strpos($value, $valsortOrder);
				if($pos !== false)
					$fieldsMultiple[$valsortOrder][] = $value;
			}
			sort($fieldsMultiple[$valsortOrder], SORT_FLAG_CASE);
			$sortOrder = array_merge($sortOrder, $fieldsMultiple[$valsortOrder]);
		}
		
		$res = array("results" => $newList, "fields"=> $sortOrder, "allPath"=>$allPath);

		return Rest::json($res);

	}
}