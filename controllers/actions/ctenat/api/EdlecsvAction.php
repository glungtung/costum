<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api;

use CAction ;
use Element;
use PHDB;
use Poi;
use Rest;

class EdlecsvAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null, $type=null) {
        $controller = $this->getController();

        $indecolos = PHDB::find( Poi::COLLECTION, [ "type" => "indecolo" ] );
        $indecolosActifs = Element::getByTypeAndId ( $type, $id, [ "indecolo" => 1, "name" => 1 ] );

        $keyTpl = "indecolo";
        $kunik = $keyTpl;
        $answerPath = "indecolo";

        $params=[
            "title" => @$_POST["title"],
            "name" => @$indecolosActifs["name"],
            "subject" => "CTE",
            "footer" => true,
            "tplData" => "cteDossier",

            "id" => $id,
            "type" => $type,
            "indecolos" => $indecolos,
            "indecolosActifs" => @$indecolosActifs["indecolo"],
            "canEdit" => false,
            "keyTpl"=>$keyTpl,
            "kunik" => $kunik,
            "answerPath" => $answerPath,
            "editable" => false,

            "docName" => "ETAT DES LIEUX ECOLOGIQUE",
            "saveOption" => "D",

            "mode" => "pdf"
        ];

        $title = ( @$answer["answers"]["action"]["project"]  ) ?  $answer["answers"]["action"]["project"][0]["name"] : "Dossier";

//        if(isset($_POST["copil"])){
//            $res=Document::checkFileRequirements([], null,
//                array(
//                    "dir"=>"communecter",
//                    "typeEltFolder"=> Form::ANSWER_COLLECTION,
//                    "idEltFolder"=> $id,
//                    "docType"=> "file",
//                    "nameUrl"=>"/pdf.pdf",
//                    "sizeUrl"=>1000
//                )
//            );
//            $params["comment"]=true;
//            $params["saveOption"]="F";
//            $params["urlPath"]=$res["uploadDir"];
//            $params["docName"]="copil".date("d-m-Y",strtotime($_POST["date"])).".pdf";
//            if(isset($_POST["subKey"]))
//                $params["docName"]=$_POST["subKey"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
//
//        }

//        $html= $controller->renderPartial('costum.views.custom.ctenat.pdf.copil', $params, true);

//        $html = $controller->renderPartial("costum.views.custom.ctenat.element.tableEDLE", $params);
//        var_dump($html);
        $params["html"] = $controller->renderPartial("costum.views.custom.ctenat.element.tableEDLE", $params, true);

        $newAnswer = array();

        if (!empty($indecolosActifs["indecolo"])){
            foreach ($indecolosActifs["indecolo"] as $indid => $ind){
                $newList = array();

                $newList["Indicateur"] = ( !empty($indecolos[$indid]) ?  $indecolos[$indid]["name"] : "" );
                $newList["Année Réf1"] = ((isset($ind["ref1year"])) ? $ind["ref1year"] : "");
                $newList["Valeur Réf1"] = ((isset($ind["ref1"])) ? $ind["ref1"] : "");
                $newList["Année Réf2"] = ((isset($ind["ref2year"])) ? $ind["ref2year"] : "");
                $newList["Valeur Réf2"] = ((isset($ind["ref2"])) ? $ind["ref2"] : "");
                $newList["Objectif"] = ((isset($ind["objres2026"])) ? $ind["objres2026"] : "");
                $newList["Réalisé"] = ((isset($ind["realres2026"])) ? $ind["realres2026"] : "");

                $newAnswer[] = $newList;
            }
        }

        $fields = [
            "Indicateur",
            "Année Réf1",
            "Valeur Réf1",
            "Année Réf2",
            "Valeur Réf2",
            "Objectif",
            "Réalisé",
        ];

        $res = array("results" => $newAnswer, "fields"=> $fields);

        return Rest::json($res);
    }
}