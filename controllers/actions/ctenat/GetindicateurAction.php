<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat;

use CAction, PHDB, Poi, Rest;
class GetindicateurAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null)
    {
    	$poilist = PHDB::findByIds(Poi::COLLECTION, array($id) );
    	
    	 return Rest::json([ "result"=> $poilist ]);
    }
}