<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element;
use Badge;
use CAction;
use Document;
use Element;
use Form;
use MongoId;
use Organization;
use PHDB;
use Project;
use Ctenat;

class HomeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
    	$controller = $this->getController();
    	$params=array();
    	$el= PHDB::findOne($type,
                array("_id" => new MongoId($id)));
        $params["el"]=$el;
        $where=array("id"=>@$id, "type"=>$type, "doctype"=>"image", "contentKey"=> "slider");
        $params["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
        if(isset($el["category"])){
            // RECUPERE LE FORM DU CTE R
        	if($el["category"]=="cteR"){
                $survey=PHDB::findOne(Form::COLLECTION, array("parentType"=>$id, "parentType"=>$type));
            	if(!empty($survey))
            		$params["survey"]=$survey;
            }
            //RECUPERE LA REPONSE DE LA FICHE ACTION
            else if($el["category"]=="ficheAction"){
                $answer=PHDB::findOne(Form::ANSWER_COLLECTION, array("parentType"=>$id, "parentType"=>$type));
                if(!empty($answer))
                    $params["answer"]=$answer;
            }
        }
        $badges=[];
        if($el["category"]=="ficheAction"){

            if(isset($el["links"]) && isset($el["links"]["projects"])) {
                foreach($el["links"]["projects"] as $k => $v){
                    $ter=Element::getElementById( $k, Project::COLLECTION, null, array("name", "slug","profilThumbImageUrl"));
                    $ter["id"]=$k;
                    $params["cteRParent"]=array_merge($el["links"]["projects"][$k], $ter); }
            }
            if(isset($el["tags"])){
                foreach($params["el"]["tags"] as $v){
                    $badge=Badge::getOneByWhere(array("source.keys"=> $controller->costum["slug"], "name"=>$v));
                    if(!empty($badge)) $badges[(string)$badge['_id']]=$badge;
                }
            }
        }else if($el["category"]=="cteR"){
            $badges=Badge::getByWhere(array("parent.".$id => array('$exists'=>true)));
            foreach($badges as $k => $v){
                if(isset($v["links"]) && isset($v["links"]["projects"]) && !empty($v["links"]["projects"])){
                    foreach($v["links"]["projects"] as $i => $p){
                        $elt=Element::getElementById($i, $p["type"], null, array("name", "slug","profilThumbImageUrl"));
                        if(!empty($elt))
                            $badges[$k]["links"]["projects"][$i]=array_merge($badges[$k]["links"]["projects"][$i], $elt);
                        else{
                            PHDB::update( Badge::COLLECTION, array("links.projects.".$i=>array('$exists'=>true), "category"=> "strategy" ), array('$unset'=>array("links.projects.".$i=>"")));
                            unset($badges[$k]["links"]["projects"][$i]);
                        }
                    }
                }
                $where=array("id"=>$k, "type"=>Badge::COLLECTION, "doctype"=> "file");
                $badges[$k]["files"] = Document::getListDocumentsWhere($where,"file");

            }
        }
        $params["badges"]=$badges;
        $financers=[];
        $partners=[];
        $porteurCTE=[];
        $porteurAction=[];

        $params["nbContributors"]=0;
        if(@$el["parent"] && !empty($el["parent"]) && !@$el["parent"]["name"]){
            foreach($el["parent"] as $k => $v){
                $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                if(!empty($elt))
                    $el['parent'][$k]=array_merge($el['parent'][$k], $elt);
            }
        }
        if(isset($el["links"]) && isset($el["links"]["contributors"])){
            $params["nbContributors"]=count($el["links"]["contributors"]);
            foreach($el["links"]["contributors"] as $k => $v){
                if(isset($v["roles"]) && isset($v["type"]) && $v["type"]==Organization::COLLECTION){
                    foreach($v["roles"] as $role){
                        if(in_array($role, ["financeur", "financeurs", "Financeur", "Financeurs"])){
                            $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                            if(!empty($elt))
                                $financers[$k]=array_merge($el["links"]["contributors"][$k], $elt);
                        }
                        if(in_array($role, ["partenaire", "Partenaire", "partenaires", "Partenaires"])){
                            $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                            if(!empty($elt))
                                $partners[$k]=array_merge($el["links"]["contributors"][$k], $elt);
                        }
                        if($role=="Porteur d'action"){
                            $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                            if(!empty($elt))
                                $porteurAction[$k]=array_merge($el["links"]["contributors"][$k], $elt);
                        }
                        if($role==Ctenat::ROLE_PORTEUR_CTER){
                            $elt=Element::getElementById( $k, $v["type"], null, array("name", "slug","profilThumbImageUrl"));
                            if(!empty($elt))
                                $porteurCTE[$k]=array_merge($el["links"]["contributors"][$k], $elt);
                        }
                    }
                }
            }
        }
        $params["financers"]=$financers;
        $params["partners"]=$partners;
        $params["porteurCTE"]=$porteurCTE;
        $params["porteurAction"]=$porteurAction;
    	return $controller->renderPartial("costum.views.custom.ctenat.element.home",$params,true);
    }
}