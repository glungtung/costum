<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element;

use Poi,Document,Element, PHDB,MongoId,Badge,Authorisation,Yii;
class AmbitionsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
    	$controller = $this->getController();
    	$params=array();
    	$el= PHDB::findOne($type, 
                array("_id" => new MongoId($id)));
        $params["el"]=$el;
        $badges=[];
            $badges=Badge::getByWhere(array("parent.".$id => array('$exists'=>true)));
            foreach($badges as $k => $v){
                if(isset($v["links"]) && isset($v["links"]["projects"]) && !empty($v["links"]["projects"])){
                    foreach($v["links"]["projects"] as $i => $p){
                        $elt=Element::getElementById($i, $p["type"], null, array("name", "slug","profilThumbImageUrl"));
                        if(!empty($elt))
                            $badges[$k]["links"]["projects"][$i]=array_merge($badges[$k]["links"]["projects"][$i], $elt);
                        else{
                            PHDB::update( Badge::COLLECTION, array("links.projects.".$i=>array('$exists'=>true), "category"=> "strategy" ), array('$unset'=>array("links.projects.".$i=>"")));
                            unset($badges[$k]["links"]["projects"][$i]);
                        }
                    }
                }
                $where=array("id"=>$k, "type"=>Badge::COLLECTION, "doctype"=> "file");
                $badges[$k]["files"] = Document::getListDocumentsWhere($where,"file");
                       
            }
        $params["badges"]=$badges;

        $indecolos = PHDB::find( Poi::COLLECTION, [ "type" => "indecolo" ] );
        $indecolosActifs = Element::getByTypeAndId ( $type, $id, [ "indecolo" => 1 ] );
        $params["indecolos"] = $indecolos;
        $params["indecolosActifs"] = @$indecolosActifs["indecolo"];
        $params["id"] = $id;
        $params["type"] = $type;
        $params["canEdit"] = Authorisation::isElementAdmin($id, $type, Yii::app()->session["userId"]);
        
        return $controller->renderPartial("costum.views.custom.ctenat.element.ambitions",$params,true);              
    }
}