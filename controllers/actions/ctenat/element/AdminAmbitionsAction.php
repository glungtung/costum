<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element;

use Authorisation;
use PHDB;
use Poi;
use Element;
use Yii;
class AdminAmbitionsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
    	$controller = $this->getController();
        if(!Authorisation::isElementAdmin($id, $type, Yii::app()->session["userId"])){
            return Yii::t("common","Can not update the element : you are not authorized to update that element !");
        } 

        $indecolos = PHDB::find( Poi::COLLECTION, [ "type" => "indecolo" ] );
        $indecolosActifs = Element::getByTypeAndId ( $type, $id, [ "indecolo" => 1 ] );
        
    	$params=[
            "id" => $id,
            "type" => $type,
            "indecolos" => $indecolos,
            "indecolosActifs" => @$indecolosActifs["indecolo"],
            "canEdit" => Authorisation::isElementAdmin($id, $type, Yii::app()->session["userId"])
        ];
    	
        //var_dump($indecolosActifs["indecolo"]);
        return $controller->renderPartial("costum.views.custom.ctenat.element.adminambitions",$params,true);              
    }
}