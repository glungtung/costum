<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat;
use CAction;
use PHDB;
use Project;
use Rest;
use Yii;

class CterAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($slug = null) {
		$controller=$this->getController();

		if(isset($slug))
			$result = PHDB::findOne( Project::COLLECTION, ["category"=>"cteR","slug"=>$slug], ["name","scope"] );
		else
			$result = PHDB::find( Project::COLLECTION, ["category"=>"cteR"], ["name","scope"] );

		return Rest::json($result);

	}
}