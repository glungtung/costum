<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf;

use CAction, PHDB ;
use Element;
use Pdf;
use Poi;
use Yii;

class EdlepdfAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null, $type=null) {
        $controller = $this->getController();

        $indecolos = PHDB::find( Poi::COLLECTION, [ "type" => "indecolo" ] );
        $indecolosActifs = Element::getByTypeAndId ( $type, $id, [ "indecolo" => 1, "name" => 1 ] );

        $keyTpl = "indecolo";
        $kunik = $keyTpl;
        $answerPath = "indecolo";

        $params=[
            "title" => @$_POST["title"],
            "name" => @$indecolosActifs["name"],
            "subject" => "CTE",
            "footer" => true,
            "tplData" => "cteDossier",

            "id" => $id,
            "type" => $type,
            "indecolos" => $indecolos,
            "indecolosActifs" => @$indecolosActifs["indecolo"],
            "canEdit" => false,
            "keyTpl"=>$keyTpl,
            "kunik" => $kunik,
            "answerPath" => $answerPath,
            "editable" => false,

            "docName" => "ETAT DES LIEUX ECOLOGIQUE.pdf",
            "saveOption" => "D",

            "mode" => "pdf"
        ];

        //$title = ( @$answer["answers"]["action"]["project"]  ) ?  $answer["answers"]["action"]["project"][0]["name"] : "Dossier";

//        if(isset($_POST["copil"])){
//            $res=Document::checkFileRequirements([], null,
//                array(
//                    "dir"=>"communecter",
//                    "typeEltFolder"=> Form::ANSWER_COLLECTION,
//                    "idEltFolder"=> $id,
//                    "docType"=> "file",
//                    "nameUrl"=>"/pdf.pdf",
//                    "sizeUrl"=>1000
//                )
//            );
//            $params["comment"]=true;
//            $params["saveOption"]="F";
//            $params["urlPath"]=$res["uploadDir"];
//            $params["docName"]="copil".date("d-m-Y",strtotime($_POST["date"])).".pdf";
//            if(isset($_POST["subKey"]))
//                $params["docName"]=$_POST["subKey"].date("d-m-Y",strtotime($_POST["date"])).".pdf";
//
//        }

//        $html= $controller->renderPartial('costum.views.custom.ctenat.pdf.copil', $params, true);

//        $html = $controller->renderPartial("costum.views.custom.ctenat.element.tableEDLE", $params);
//        var_dump($html);
        $server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];

        $imgHead1 = $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/PictoCTE.png";
        $imgHead2 =  $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/Logo_republique-francaise.png" ;

        $params["html"] = '<table><tr><td><img class="img-responsive" style="height: 120px; float: right; text-align: right;" src="'.$imgHead2.'"/></td><td style="width: 300px"></td><td><img class="img-responsive" style="width: 120px;height: 120px; float: right; text-align: right;" src="'.$imgHead1.'"/></td></tr></table>' ;
        $params["html"] .= $controller->renderPartial("costum.views.custom.ctenat.element.tableEDLE", $params, true);

        $res=Pdf::createPdf($params);

    }
}