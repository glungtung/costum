<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat;

use CAction, CO2Stat, Slug, PHDB, Form, Ctenat, MongoId, Project, Yii, SearchNew, Answer, Rest;
class DashgraphAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($slug=null,$tag=null,$action=null,$format=null)
    {
    	
    	$controller = $this->getController();
    	$params=array("page"=>"pie");
    	  
    	$tpl = "costum.views.custom.ctenat.dashboard";
    	
    	$actionCount =0;
		$actionIds= [];
		
    	//$data = 
    	//pour les graphs d'un tag d'un CTER
    	//onglet en chiffres
    	if($tag && $slug){
    		CO2Stat::incNbLoad("co2-dashboard-cter-tag"); 
    		$el = Slug::getElementBySlug($slug);
    		$tagsLbls = $controller->costum["lists"]["domainAction"][$tag];
			$answers = PHDB::find( Form::ANSWER_COLLECTION, [   "source.key"=>Ctenat::KEY,
    															"formId"=>$slug,
    															"priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );

    		$actionsIds = [];
    		$actionsByDA = [];
    		foreach ($answers as $key => $ans) {
    			$formId = $ans["formId"];
    			if(    isset( $ans["answers"]["action"]["project"][0]["id"] ) 
					&& isset($ans["answers"]["caracter"]["actionPrincipal"] )
					&& in_array($ans["answers"]["caracter"]["actionPrincipal"],$tagsLbls ) ) 
				{
					$actionPrincipal = $ans["answers"]["caracter"]["actionPrincipal"];
					$projectId = $ans["answers"]["action"]["project"][0]["id"];
					$actionsIds[] = new MongoId($projectId);


					$actionsDA[$projectId] = ["family"=>$tag, 
											  "1st" =>$actionPrincipal];
					if( !isset( $ans["answers"]["caracter"]["actionSecondaire"]))
						$actionsDA[$projectId]['2nd'] = @$ans["answers"]["caracter"]["actionSecondaire"];
					
					if( !isset( $actionsByDA[$actionPrincipal]))
						$actionsByDA[$actionPrincipal] = [];
					$actionsByDA[$actionPrincipal][] = $projectId;
				}
    		}

    		$actions = PHDB::find( Project::COLLECTION, array("_id" => array( '$in'=>$actionsIds ) ),
    												   ["name","slug","description","tags","profilMediumImageUrl","links","geo","geoPosition","address"]);
    		//rajouter les DA dans 
			foreach ($actions as $key => $value) {
				$actions[$key]["domaineAction"] = $actionsDA[$key];
			}
    		$countActions = [];
			foreach ($tagsLbls as $i => $t) {
					$tagsLbls[] =  $t;
					$countActions[] = (isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0;
			}
			$params = array(
				"elements" => $actions,
				"slug" => $slug,
				"css" => array('border' => false ),
	    		"title" => "<span style='font-size:12px'>".$el["el"]["name"]."</span><br/>Tableau de bord",
	    		"blocks" 	=> array(
					"pieActionsByTagBySlug" => array(
						"title"   => $tag,
						"counter" => count( $actionsIds ),
						"graph" => array (
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/size/S",
							"key"=>"pieManyByTagBySlug",
							"data"=> array(
								"datasets"=> array(
									array(
										"data"=> $countActions,
										"backgroundColor"=> Ctenat::$COLORS
										)
									),
								"labels"=> $tagsLbls
							)
						)
					)
				)
	    	);
    	}
    	//quand on click sur une thematique donnée, ex type d'ation Energie
    	else if( $tag ){
    		CO2Stat::incNbLoad("co2-dashboard-tag"); 
    		//var_dump($this->getController()->costum);exit;
    		$tagsLbls = $controller->costum["lists"]["domainAction"][$tag];
			$answers = PHDB::find( Form::ANSWER_COLLECTION, [ "source.key"=>Ctenat::KEY,
    														  "priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );

    		$actionsIds = [];
    		$actionsIdsStr = [];
    		$actionsByDA = [];
    		//gather all answers avec caraction.actionPrincipale
    		//fill $actionsIds with all projectIds and tags for carater in $actionsDA
    		//var_dump($controller->costum["lists"]["domainAction"]);
    		foreach ($answers as $key => $ans) {
    			$formId = $ans["formId"];
    			if(    isset( $ans["answers"]["action"]["project"][0]["id"] ) 
					&& isset($ans["answers"]["caracter"]["actionPrincipal"] )
					&& in_array($ans["answers"]["caracter"]["actionPrincipal"],$tagsLbls ) ) 
				{
					$actionPrincipal = $ans["answers"]["caracter"]["actionPrincipal"];
					$projectId = $ans["answers"]["action"]["project"][0]["id"];
					//regle un probleme de doublon dans la liste des actions
					if(!isset($actionsDA[$projectId]))
					{
						$actionsIds[] = new MongoId($projectId);
						$actionsDA[$projectId] = ["family"=>$tag, 
												   "1st" =>$actionPrincipal];
						if( !isset( $ans["answers"]["caracter"]["actionSecondaire"]))
							$actionsDA[$projectId]['2nd'] = @$ans["answers"]["caracter"]["actionSecondaire"];

						if( !isset( $actionsByDA[$actionPrincipal]))
							$actionsByDA[$actionPrincipal] = [];
						$actionsByDA[$actionPrincipal][] = $projectId;
					} else {
						// var_dump(count($formId));
						// var_dump(count($projectId));
					}
				}
    		}
    		
    		$actions = PHDB::find(Project::COLLECTION, ["_id" => array( '$in'=>$actionsIds ) ],
    												   ["name","slug","description","tags","profilMediumImageUrl","links","geo","geoPosition","address"]);
    		
    		// var_dump(count($actionsIds));
    		// var_dump($actionsIds);exit;
    		// var_dump(count($actions));exit;

    		foreach ( $actions as $key => $value ) {
    			$actions[$key]["domaineAction"] = $actionsDA[$key];
    		}

    		$countActions = [];
			foreach ($tagsLbls as $i => $t) {
					//$tagsLbls[] =  $t;
					$countActions[] = (isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0;
			}
			//var_dump($tagsLbls);
			$params = [
				"elements" => $actions,
				"css" => ['border' => false ],
	    		"title" => "Tableau de bord",

	    		"blocks" 	=> [
					"barActionsByTag" => [
						"title"   => $tag,
						"counter" => count( $actions ),
						"noborder" => 1,
						"graph" => [
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barMany",
							"key"=>"barManyByTag",
							"data"=> [
								"data"=>  $countActions,
								"labels"=> $tagsLbls
							]
						]
					]
					// ,
					// "mapView" => [
					// 	"map" => ["id"=>$tag]
					// ]
				]
	    	];
    	}
    	//onglet en chiffre d'un CTER
    	//onglet en chiffre d'une actoin
    	else if($slug || $action){

    		if(!empty($action)){
    			CO2Stat::incNbLoad("co2-dashboard-action"); 
    			$slug = $action;
    		}else {
    			CO2Stat::incNbLoad("co2-dashboard-cter"); 
			}
			$params=Yii::$app->cache->get('ctenatDashboard'.$slug);

if($params===false)
{
    		$el = Slug::getElementBySlug($slug);
    		$title = "<span style='font-size:12px'>".$el["el"]["name"]."</span><br/> Tableau de bord";
    		
    		//financement 
			$financeData = [];
			$financeLbls = [];
			//var_dump($action); 
			if(!empty($action)){
				$answers = PHDB::find( Form::ANSWER_COLLECTION, ["source.key"=>Ctenat::KEY,
	    														 "answers.action.project.id"=>$el["id"],
	    														 "priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );
			}
			else 
				$answers = PHDB::find( Form::ANSWER_COLLECTION, ["source.key"=>Ctenat::KEY,
	    														 "cterSlug"=>$slug,
	    														 "priorisation" => ['$in'=> Ctenat::$validActionStates ] ] );
			//var_dump($answers);
			$finance = Ctenat::chiffreFinancementByType($slug);
			$financeData = $finance["data"];
			$financeLbls = $finance["lbls"];
			
			//actions 
			$tagsLbls = $controller->costum["lists"]["domainAction"];
			$actionData = [];
			$actionLbls	 = [];

    		
    		$actionsIds = [];
    		$actionsByDA = [];
    		foreach ($answers as $key => $ans) {
    			if(    isset( $ans["answers"]["action"]["project"][0]["id"] ) 
					&& isset($ans["answers"]["caracter"]["actionPrincipal"] ) ) 
				{
					$actionPrincipal = $ans["answers"]["caracter"]["actionPrincipal"]; 
					$projectId = $ans["answers"]["action"]["project"][0]["id"];
					$parentBadge = "";
					foreach ($controller->costum["lists"]["domainAction"] as $key => $childBadges) {
						foreach ($childBadges as $ic => $cb) {
							if($actionPrincipal == $cb)
								$parentBadge = $key;
						}			                
				    }
				    $actionsDA[$projectId] = ["family"=>$tag, 
											  "1st" =>$actionPrincipal];
				    if( !isset( $ans["answers"]["caracter"]["actionSecondaire"]))
						$actionsDA[$projectId]['2nd'] = @$ans["answers"]["caracter"]["actionSecondaire"];

				    //check $parentBadge is really a familyBadge
				    if(isset($tagsLbls[$parentBadge])){
						$projectId = $ans["answers"]["action"]["project"][0]["id"];
						$actionsIds[] = new MongoId($projectId);

						if( !isset( $actionsByDA[$parentBadge]))
							$actionsByDA[$parentBadge] = [];
						$actionsByDA[$parentBadge][] = $projectId;

					}
				}
    		}
    		$actions = PHDB::find(Project::COLLECTION, [ "_id" => [ '$in'=>$actionsIds ] ] ,
    												   ["name","slug","description","tags","profilMediumImageUrl","links","geo","geoPosition","address"]);
    		
    		foreach ($actions as $key => $value) {
    			$actions[$key]["domaineAction"] = $actionsDA[$key];
    		}
    		$totalCountActions = 0;
			foreach ($tagsLbls as $t => $childBadges) {
				$actionLbls[] =  $t;
				$actionData[] = (isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0;
				$totalCountActions = $totalCountActions + ((isset($actionsByDA[$t])) ? count($actionsByDA[$t]) : 0);
			}

			$params = array(
				"elements" => null,
				"slug" => $slug,
	    		"title" => $title,
	    		"blocks" 	=> []
	    	);
	    	if(empty($action))
	    		$params['blocks']["barActions"] = [
						"title"   => "Actions",
						"counter" => $totalCountActions,
						"graph" => [
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barActionsCTErr",
							"key"=>"barActionsCTErr",
							"data"=> [
								"datasets"=> [
									[
										"data"=> $actionData, 
										"backgroundColor"=> Ctenat::$COLORS,
										"borderWidth"=> 1
										]
									],
								"labels"=> $actionLbls
							]
						]
					];

			if(isset($finance["total"]) && empty($action))
					$params["blocks"]["pieFinance"] = [
						"title"   => "Millions €",
						"counter" => $finance["total"],
						"graph" => [
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany",
							"key"=>"pieManyFinance",
							"data"=> [
								"datasets"=> [
									[ "data"=> $financeData,
									  "backgroundColor"=> Ctenat::$COLORS ]],
								"labels"=> $financeLbls
							]
						]
					];

			$states = [ Ctenat::STATUT_ACTION_VALID,
	 					Ctenat::STATUT_ACTION_COMPLETED,
	 					Ctenat::STATUT_ACTION_MATURATION,
	 					Ctenat::STATUT_ACTION_CONTRACT ];
			$answersList2 = PHDB::find(Form::ANSWER_COLLECTION,
											[ "cterSlug"     => $slug,
	 										  "priorisation" => ['$in'=>$states]], [ "_id", "priorisation" ] );
			$stateData = [0,0,0];
			foreach ($answersList2 as $k => $v) {
				if($v["priorisation"] == Ctenat::STATUT_ACTION_VALID)
					$stateData[0] = $stateData[0]+1; 
	 			if($v["priorisation"] == Ctenat::STATUT_ACTION_COMPLETED)
	 				$stateData[1] = $stateData[1]+1;
	 			if($v["priorisation"] == Ctenat::STATUT_ACTION_MATURATION)
	 				$stateData[2] = $stateData[2]+1;
			}
			//clear zero values break the pie
			// foreach ($stateData as $k => $v) {
			// 	if($v == 0) {
			// 		array_splice($stateData,$k,1);
			// 		array_splice($states,$k,1);
			// 	}
			// }
			$colors = array_splice(Ctenat::$COLORS ,0,2); 
			if(empty($action)){
				$params["blocks"]["pieStatuts"] = [
						"title"   => "Statut des actions",
						"counter" => null,
						"graph" => [
							"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany",
							"key"=>"pieManyState",
							"data"=> [
								"datasets"=> [
									[ "data"=> $stateData,
									  "backgroundColor"=> Ctenat::$COLORS ]],
								"labels"=> $states
							]
						]
					];
			}
			$indicators = [];
			$indicateurs = Ctenat::getIndicator();

			foreach ($answers as $i => $a) {
				//var_dump($a);	
				if( isset( $a["answers"]["murir"]["results"] ) )
				{
					foreach ( $a["answers"]["murir"]["results"] as $ind => $vind ) 
					{
						if(isset( $indicateurs[ $vind["indicateur"] ]))
						{
					
							if(!isset( $indicators[$vind["indicateur"]] ))
								$indicators[$vind["indicateur"]] = [ "title" => $indicateurs[ $vind["indicateur"] ] , 
																	 "obj"=>0,
																	 "done"=>0 ];
							if(isset( $vind["reality"] ))
							{
								foreach (Ctenat::$years as $i => $y) {
								if($i > 0 && isset($vind["reality"]["res".$y]))
									$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res".$y ];
								}
								// if(isset($vind["reality"]["res2020"]))
								// 	$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2020"];
								// if(isset($vind["reality"]["res2021"]))
								// 	$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2021"];
								// if(isset($vind["reality"]["res2022"]))
								// 	$indicators[$vind["indicateur"]]["done"] += $vind["reality"]["res2022"];
							}
							if(isset($vind["objectif"]))
							{
								foreach (Ctenat::$years as $i => $y) {
									if($i > 0 && isset($vind["objectif"]["res".$y]))
										$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res".$y ];
								}
								// if(isset($vind["objectif"]["res2020"]))
								// 	$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2020"];
								// if(isset($vind["objectif"]["res2021"]))
								// 	$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2021"];
								// if(isset($vind["objectif"]["res2022"]))
								// 	$indicators[$vind["indicateur"]]["obj"] += $vind["objectif"]["res2022"];
							}
						}
					}
				}
			}
			$titleIndic = "Résultats attendus des actions";
			if( !empty($action)){
				if( count($indicators) == 0) 
					$titleIndic = "<span class='text-red'>Aucun résultat pour cette actions</span>";
				if(!isset($answers["priorisation"]) || !in_array($answers["priorisation"],Ctenat::$validActionStates) )
					$titleIndic = "<span class='text-red'>Cette action n'est pas encore validé</span>";

			}
			$params["blocks"]["indicateurs"] = [
					"title"   => $titleIndic,
					"counter" => null,
					"indicator" => $indicators
				];
			//$tpl = "costum.views.custom.ctenat.dashboardCTErr";
			

	Yii::$app->cache->set('ctenatDashboard'.$slug,$params, 360 );
}
Yii::$app->cache->set('ctenatDashboard'.$slug,$params, 10 );
    	}
    	//dashboard vision globale, aggregation 
    	else{ 
			CO2Stat::incNbLoad("co2-dashboard"); 
			
$params=Yii::$app->cache->get('ctenatDashboard');
$params=false;
if($params===false)
{
		
			$query = SearchNew::getQueries( $_POST );
			
			// var_dump($query["global"]);
			// var_dump($_POST);

    		$answers = PHDB::find( Answer::COLLECTION , 
    								$query["global"], 
    								[ "answers.caracter.actionPrincipal",
    								"answers.action.project",
    								"answers.murir.planFinancement",
    								"answers.murir.results","cterSlug" ] );
			$ssBadgeFamily = [];
			foreach ($controller->costum["lists"]["domainAction"] as $pb => $bChild) {
				foreach ($bChild as $key => $bName) {
					$ssBadgeFamily[$bName] = $pb;
				}
			}
			$indicatorCount = [
				"co2"=>0,
				"job"=>0,
				"kwInstalled"=>0,
				"building"=>0,
				"agricol"=>0,
				"person"=>0,
				"friches"=>0,
				"cyclo"=>0,
				"water"=>0,
				"trees"=>0,
				"haie"=>0,
				"tonnes"=>0];
			$financeTotal = 0;


				$financeTotal = 0;
              $actionCount = 0;

              
        $finance = [];
        $financeLbl = [];
        $res = ["data"=>[],"lbls"=>[],"total"=>0];
		$parentForm = PHDB::findOne(Form::COLLECTION,["id"=>Ctenat::PARENT_FORM ], ["_id", "id","params.period"] );

			foreach ( $answers as $id => $ans ) {

				$daPrinci = @$ans["answers"]["caracter"]["actionPrincipal"];
				if( isset( $ssBadgeFamily[$daPrinci] )
					&& isset($ans["answers"]["action"]["project"][0]["id"]) ) 
					$actionCount++;

				//indicator counts
				if(isset($ans["answers"]["murir"]["results"]) ) {
					$results = $ans["answers"]["murir"]["results"];
					
					//name" : "Emissions de gaz à effet de serre du projet"
					}		

				//$financeTotalSlug = Ctenat::chiffreFinancementByType($ans["cterSlug"]);
				//var_dump($financeTotalSlug["total"]);
				//$financeTotal += $financeTotalSlug['total'];

			}

			foreach ($answers  as $i => $a) {
            if(isset($a["answers"]["murir"]["planFinancement"]))
            {
                $fin = $a["answers"]["murir"]["planFinancement"];
                
                foreach ($fin as $ix => $f) {

                    $cumul = 0;
                    if(isset($parentForm["params"]["period"])){
                        
                        $from = intval($parentForm["params"]["period"]["from"])+1;
                        $to = intval($parentForm["params"]["period"]["to"]);
                        while ( $from <= $to) {
                            if(!empty($f["amount".$from]))
                                $cumul += intval( $f["amount".$from] );
                            $from++;
                        }
                    }
                    
                    if(isset($f["financerType"])){
	                    if(!isset($finance[$f["financerType"]])){
	                        $finance[ $f["financerType"] ] = $cumul;
	                        $financeLbl[ $f["financerType"] ] = (isset(Ctenat::$financerTypeList[$f["financerType"]])) ? Ctenat::$financerTypeList[$f["financerType"]] : $f["financerType"] ;
	                    }
	                    else 
	                        $finance[$f["financerType"]] += $cumul;
	                }
                }
            }

                foreach (array_keys($finance) as $k => $v) {
                $financeTotal += intval($finance[$v]);
                  }
                  $finance = [];
                $financeLbl = [];
            }
            
			$financeTotal= round($financeTotal/1000000,1);

	 		$cteCount = 0;
	 		$cteCountData = [];
	 		$cteCountLbls = [];
	 		
	 		foreach (Ctenat::$sessionsTags as $lbl => $tag) {
	 			$cteCountLbls[] = $lbl;
	 			$c = PHDB::count(Project::COLLECTION, 
								[ "category" => Ctenat::CATEGORY_CTER,
								  "source.status.ctenat"=> ['$in'=> Ctenat::$validCter ], 
								  "session" => $tag ]);	
	 			$cteCountData[] = $c;
	 		}
	 		$cteCountData[] = PHDB::count(Project::COLLECTION, 
											[ "category" => Ctenat::CATEGORY_CTER,
											  "source.status.ctenat"=> ['$in'=> Ctenat::$validCter ] ]);
	 		$cteCountLbls[] = "auj.";

			 // var_dump($cteCountLbls);
			 // var_dump($cteCount);exit;
			 
			 if(count($cteCountData) > 0)
				$cteCount =  $cteCountData[count($cteCountData)-1];
			 
	 		
	    	$params = [
	    		"elements" => null,
	    		"title" => "Tableau de bord national",
	    		"blocks" 	=> [
					// "lineCTE" => [
					// 	"title"   => "CTE lancés",
					// 	"counter" => $cteCount,
					// 	"graph" => [
					// 		"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.lineCTE/size/S",
					// 		"key"=>"lineCTE",
					// 		"data"=> [
					// 				"data"=> $cteCountData,
					// 				"labels"=> $cteCountLbls
					// 			]
					// 		]
					// ],
					"contracts" => [
						"title"   => "Contrats",
						"counter" => $cteCount,
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/job.png'><div class='title2'>Nombre de contrats<br/> engagés dans la transition écologique</div>",
						"colSize" => 4
					],
					"barPorteurbyDomaine" => [
						"title"   => "Actions",
						"counter" => $actionCount,
						"graph" => ["url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.barPorteurbyDomaine/size/S",
							"key"=>"barPorteurbyDomaine"],
						"colSize" => 4
					],
					"pieFinance" => [
						"title"   => "Millions €",
						"counter" => $financeTotal,
						"graph" => ["url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieFinance/size/S",
							"key"=>"pieFinance"],
						"colSize" => 4
					],
					"co2" => [
						"title"   => "(TeqCO2/an)",
						"counter" => $indicatorCount["co2"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/co2.png'><div class='title2'>Nombre de Tonnes de CO2 équivalent évitées</div>"
					],
					"job" => [
						"title"   => "(Personnes)",
						"counter" => $indicatorCount["job"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/job.png'><div class='title2'>Nombre d'emplois directs créés et maintenus</div>"
					],
					"kwInstalled" => [
						"title"   => "(KWh/an)",
						"counter" => $indicatorCount["kwInstalled"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/kwInstalled.png'><div class='title2'>Production d'energie renouvelable</div>"
					],
					"building" => [
						"title"   => "(Bâtiments et logements)",
						"counter" => $indicatorCount["building"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/building.png'><div class='title2'>Nombre de bâtiments et logements rénovés</div>"
					],
					"agricol" => [
						"title"   => "(Hectares)",
						"counter" => $indicatorCount["agricol"] ,
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/agricol.png'><div class='title2'>Surfaces concernées par des évolutions de pratiques agricoles</div>"
					],
					"person" => [
						"title"   => "(Personnes)",
						"counter" => $indicatorCount["person"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/person.png'><div class='title2'>Nombre de personnes sensibilisées et formées</div>"
					],
					"friches" => [
						"title"   => "(Hectares)",
						"counter" => $indicatorCount["friches"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/friches.png'><div class='title2'>Surface de friche réhabilitée</div>"
					],
					"cyclo" => [
						"title"   => "(Mètres linéaire total)",
						"counter" => $indicatorCount["cyclo"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/cyclo.png'><div class='title2'>Linéaire de pistes cyclables créées</div>"
					],
					"water" => [
						"title"   => "(M3/an)",
						"counter" => $indicatorCount["water"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/water.png'><div class='title2'>Mètres cube d’eau économisés</div>"
					],
					"trees" => [
						"title"   => "(Arbres)",
						"counter" => $indicatorCount["trees"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/trees.png'><div class='title2'>Nombre d’arbres plantés</div>"
					],
					"haie" => [
						"title"   => "Mètres",
						"counter" => $indicatorCount["haie"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/haie.png'><div class='title2'>Kilomètres de haie plantée ou restaurée</div>"
					],
					"tonnes" => [
						"title"   => "(Tonnes/an)",
						"counter" => $indicatorCount["tonnes"],
						"html" => "<img class='img-responsive' style='margin:30px auto 0px auto;' src='".Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/tonnes.png'><div class='title2'>Quantité de déchets valorisés</div>"
					]
				]
			];		

			Yii::$app->cache->set('ctenatDashboard',$params, 720);
		 }
		}
    	
    	if($format == "json")
    		return Rest::json($params);
    	else { 
	    	if(Yii::app()->request->isAjaxRequest)
	            return $controller->renderPartial($tpl,$params,true);              
	        else {
	    		$this->getController()->layout = "//layouts/empty";
	    		return $this->getController()->render($tpl,$params);
	        }
	    }
    	
    }
}