<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\project;
    
    use Action;
    use Person;
    use Document;
    use MongoId;
    use MongoRegex;
    use PHDB;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    use Project;
    use Rest;
    use RocketChat;
    
    class ActionAction extends \PixelHumain\PixelHumain\components\Action {
        public function run($request = '', $scope = '') {
            $output = null;
            switch ($request) {
                case 'actions':
                    // Récupère la liste des actions à partir du filtre
                    $where = $_POST['filters'];
                    if (!empty($_POST['text'])) {
                        $text = $_POST['text'];
                        $special_commands = [':mine', ':other', ':all'];
                        $first_text = substr($text, 0, 1);
                        if ($first_text === '#' || $first_text === ':') {
                            // Supprimer les espaces de trop
                            $text = preg_replace('/\s+/', ' ', trim($text));
                            // Normaliser les espaces
                            $text = str_replace(',#', ', #', $text);
                            $text = str_replace(',:', ', :', $text);
                            // Diviser en tableau selon la normalisation
                            $splited = explode(', ', $text);
                            foreach ($splited as $split) {
                                if (in_array($split, $special_commands)) {
                                    $split = strtolower($split);
                                    if ($split === ':mine') $where['links.contributors.' . \Yii::app()->session['userId']] = ['$exists' => true];
                                    else if ($split === ':other') $where['links.contributors.' . \Yii::app()->session['userId']] = ['$exists' => false];
                                    else if ($split === ':all') unset($where['urls']);
                                } else {
                                    $search = substr($split, 1);
                                    if (empty($where['$or'])) $where['$or'] = [['status' => ['$in' => []]], ['tags' => ['$in' => []]]];
                                    $where['$or'][0]['status']['$in'][] = $search;
                                    
                                    // échaper les caractères spéciaux
                                    foreach ([
                                                 '.', '+', '*', '?', '^', '$', '(', ')', '[', ']', '{', '}', '|', '\\'
                                             ] as $special) $text = str_replace($special, '\\' . $special, $text);
                                    $where['$or'][1]['tags']['$in'][] = new MongoRegex('/' . $search . '/i');
                                }
                            }
                        } else $where['name'] = new MongoRegex("/$text/i");
                    }
                    $where['status'] = ['$ne' => 'disabled'];
                    
                    if (!empty($scope)) {
                        switch ($scope) {
                            case 'mine';
                                $where['links.contributors.' . \Yii::app()->session['userId']] = ['$exists' => true];
                                unset($where['urls']);
                                break;
                            case 'all':
                                unset($where['urls']);
                                break;
                        }
                    }
                    
                    $db_actions = PHDB::findAndSort(Action::COLLECTION, $where, ['updated' => 1], 0, [
                        'name', 'tags', 'medias', 'status', 'tracking', 'links.contributors'
                    ]);
                    $actions = [];
                    foreach ($db_actions as $db_id => $db_action) {
                        if (empty($db_action['name'])) continue;
                        $is_contributor = !empty($db_action['links']) && !empty($db_action['links']['contributors']) && !empty($db_action['links']['contributors'][\Yii::app()->session['userId']]);
                        $actions[] = [
                            'id'             => $db_id,
                            'name'           => $db_action['name'],
                            'status'         => $db_action['status'],
                            'tags'           => $db_action['tags'] ?? [],
                            'tracking'       => !empty($db_action['tracking']) ? UtilsHelper::parse_values($db_action['tracking']) : false,
                            'is_contributor' => $is_contributor
                        ];
                    }
                    $output = Rest::json($actions);
                    break;
                case 'media':
                    $db_medias = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['media.images']);
                    $medias = [];
                    if (!empty($db_medias['media']) && !empty($db_medias['media']['images'])) {
                        $db_images = PHDB::find(Document::COLLECTION, [
                            '_id' => [
                                '$in' => array_map(function ($__id) {
                                    return new MongoId($__id);
                                }, $db_medias['media']['images'])
                            ]
                        ],                      ['folder', 'name']);
                        foreach ($db_images as $db_image) {
                            $medias[] = $db_image['folder'] . '/' . $db_image['name'];
                        }
                    }
                    $output = Rest::json($medias);
                    break;
                case 'action':
                    $db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action']);
                    $output = Rest::json($db_action);
                    break;
                case 'tags':
                    $db_tags = PHDB::findOneById(Action::COLLECTION, $_POST['id'], ['tags']);
                    $tags = [];
                    if (!empty($db_tags) && !empty($db_tags['tags'])) $tags = $db_tags['tags'];
                    $output = Rest::json($tags);
                    break;
                case 'action_detail_data':
                    $db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id']);
                    $parent_fields = [];
                    // Problème d'une action qui n'a pas de créateur
                    if (empty($db_action['creator']) && !empty($db_action['parentType'])) {
                        $parent_fields[] = 'creator';
                    }
                    
                    // action sans date de départ
                    if (empty($db_action['startDate']) && empty($db_action['created']) && !empty($db_action['parentType'])) {
                        $parent_fields[] = 'startDate';
                        $parent_fields[] = 'created';
                    }
                    
                    if (!empty($parent_fields)) {
                        $db_parent = PHDB::findOneById($db_action['parentType'], $db_action['parentId'], $parent_fields);
                        $db_action['creator'] = $db_action['creator'] ?? $db_parent['creator'];
                        $db_action['created'] = $db_action['created'] ?? ($db_parent['created'] ?? null);
                        $db_action['startDate'] = $db_action['startDate'] ?? ($db_parent['startDate'] ?? null);
                    }
                    $db_citoyen = PHDB::findOneById(Person::COLLECTION, $db_action['creator'], ['name', 'profilThumbImageUrl']);
                    
                    $start_datetime = new \DateTime();
                    $end_datetime = new \DateTime();
                    
                    $start_date = UtilsHelper::get_as_timestamp(['startDate', 'created'], $db_action);
                    $start_datetime->setTimestamp($start_date);
                    
                    $now = strtotime(date('Y-m-d H:i'));
                    $addition = max($now, strtotime($start_datetime->format('c') . ' + 1day'));
                    
                    $end_date = UtilsHelper::get_as_timestamp(['endDate'], $db_action, $addition);
                    $end_datetime->setTimestamp($end_date);
                    
                    $contributors = [];
                    if (!empty($db_action['links']) && !empty($db_action['links']['contributors'])) {
                        foreach ($db_action['links']['contributors'] as $id => $contributor) {
                            if ($db_action['creator'] === $id) {
                                $contributors[$id] = [
                                    'name'  => $db_citoyen['name'],
                                    'image' => $db_citoyen['profilThumbImageUrl'] ?? \Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_citoyens.png'
                                ];
                            } else {
                                $db_contributor = PHDB::findOneById($contributor['type'], $id, ['name', 'profilThumbImageUrl']);
                                $contributors[$id] = [
                                    'name'  => $db_contributor['name'],
                                    'image' => $db_contributor['profilThumbImageUrl'] ?? \Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_citoyens.png'
                                ];
                            }
                        }
                    }
                    
                    $tasks = [];
                    if (!empty($db_action['tasks'])) {
                        foreach ($db_action['tasks'] as $task) {
                            $user = null;
                            if (!empty($task['userId'])) {
                                $db_user = PHDB::findOneById(Person::COLLECTION, $task['userId'], ['name']);
                                $user = [
                                    'id'   => $task['userId'],
                                    'name' => $db_user['name']
                                ];
                            }
                            $tasks[] = [
                                'id'      => $task['taskId'] ?? '',
                                'name'    => $task['task'] ?? '(No name)',
                                'user'    => $user,
                                'checked' => !empty($task['checked']) ? filter_var($task['checked'], FILTER_VALIDATE_BOOL) : false
                            ];
                        }
                    }
                    
                    $action = [
                        'id'                 => $_POST['id'],
                        'creator'            => [
                            'name'  => $db_citoyen['name'],
                            'image' => $db_citoyen['profilThumbImageUrl'] ?? \Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_citoyens.png'
                        ],
                        'startDate'          => $start_datetime->format('c'),
                        'endDate'            => $end_datetime->format('c'),
                        'credits'            => $db_action['credits'] ?? 0,
                        'contributor_number' => !empty($db_action['links']) && !empty($db_action['links']['contributors']) ? count($db_action['links']['contributors']) : 0,
                        'min'                => $db_action['min'] ?? 0,
                        'max'                => $db_action['max'] ?? max(count($contributors), 1),
                        'contributors'       => $contributors,
                        'tasks'              => $tasks
                    ];
                    $output = Rest::json($action);
                    break;
                case 'action_detail_html':
                    $db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id'], ['name', 'parentId', 'parentType', 'status']);
                    $db_parent = PHDB::findOneById($db_action['parentType'] ?? Project::COLLECTION, $db_action['parentId'], [
                        'name',
                        'slug'
                    ]);
                    $output = $this->getController()->renderPartial('costum.views.custom.appelAProjet.action_modal', [
                        'action'      => $_POST['id'],
                        'action_name' => $db_action['name'],
                        'parent_name' => $db_parent['name'],
                        'status'      => $db_action['status'],
                        'parent_slug' => $db_parent['slug']
                    ]);
                    break;
                case 'task':
                    $db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['tasks']);
                    $task = $db_action['tasks'][$_POST['task']];
                    
                    if (!empty($task['createdAt'])) {
                        $created_timestamp = new \DateTime();
                        $created_timestamp->setTimestamp(UtilsHelper::get_as_timestamp(['createdAt'], $task));
                        $task['createdAt'] = $created_timestamp->format('c');
                    }
                    
                    if (!empty($task['checkedAt'])) {
                        $checked_timestamp = new \DateTime();
                        $checked_timestamp->setTimestamp(UtilsHelper::get_as_timestamp(['checkedAt'], $task));
                        $task['checkedAt'] = $created_timestamp->format('c');
                    }
                    
                    $output = Rest::json($task);
                    break;
                case 'task_recap':
                    $db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id'], ['tasks']);
                    $recap = [
                        'total'      => !empty($db_action['tasks']) ? count($db_action['tasks']) : 0,
                        'done'       => 0,
                        'percentage' => 0
                    ];
                    if (!empty($db_action['tasks'])) {
                        foreach ($db_action['tasks'] as $task) {
                            if ($task['checked']) $recap['done']++;
                        }
                    }
                    $recap['percentage'] = $recap['total'] === 0 ? 0 : ceil($recap['done'] * 100 / $recap['total']);
                    $output = Rest::json($recap);
                    break;
                case 'contribution_rocker_chat':
                    $messages = [];
                    if (!empty($_POST['action']) && !empty($_POST['project_name']) && !empty($_POST['contributors'])) {
                        $db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['name']);
                        foreach ($_POST['contributors'] as $contributor) {
                            $db_citoyen = PHDB::findOneById(Person::COLLECTION, $contributor, ['username']);
                            $message = ' @' . $db_citoyen['username'] . ' participe à l\'action **' . $db_action['name'] . '** de **' . $_POST["project_name"] . '**';
                            $messages[] = $message;
                            RocketChat::post($_POST['channel'], $message, '', true);
                        }
                    }
                    $output = Rest::json(['status' => true, 'msg' => $messages]);
                    break;
                case 'new_action_rocker_chat':
                    $messages = [];
                    if (!empty($_POST['action_name']) && !empty($_POST['project'])) {
                        $db_project = PHDB::findOneById(Project::COLLECTION, $_POST['project'], ['name', 'slug']);
                        $message = ' @' . \Yii::app()->session['user']['username'] . ' a ajouté une action **' . $_POST['action_name'] . '** dans **' . $db_project['name'] . '**';
                        $messages[] = $message;
                        RocketChat::post($db_project['slug'], $message, '', true);
                    }
                    $output = Rest::json(['status' => true, 'msg' => $messages]);
                    break;
                default:
                    $output = Rest::json(['status' => false, 'msg' => 'Bad request']);
                    break;
            }
            return $output;
        }
    }
