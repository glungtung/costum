<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\project;

use Answer;
use Form;
use MongoRegex;
use PHDB;
use Project;
use Rest;
use Room;

class ProjectAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($request = '') {
        $output = null;
        switch ($request) {
            case 'project_names':
                // Récupère la liste des projects à partir du filtre
                $connected_user = \Yii::app()->session['userId'];
                $where = [
                    'name' => ['$exists' => true],
                    'slug' => ['$exists' => true],
                    '$or' => [
                        ["links.contributors.$connected_user" => ['$exists' => true]],
                        ["links.attendees.$connected_user" => ['$exists' => true]],
                        ["links.creator.$connected_user" => ['$exists' => true]]
                    ]
                ];
                if (!empty($_POST['text'])) {
                    $text = $_POST['text'];
                    $where['name'] = new MongoRegex("/$text/i");
                    unset($_POST['text']);
                }
                if (!empty($_POST['params'])) {
                    foreach ($_POST['params'] as $post_key => $post_value) $where[$post_key] = $post_value;
                }
                $db_projects = PHDB::findAndSort(Project::COLLECTION, $where, ['updated' => 1], 0, ['name', 'slug', 'profilBannerUrl', 'profilMediumImageUrl']);
                $projects = [];
                foreach ($db_projects as $db_id => $db_project) {
                    $projects[$db_id] = [
                        'name' => $db_project['name'],
                        'slug' => $db_project['slug'],
                        'background' => $db_project['profilBannerUrl'] ?? ($db_project['profilMediumImageUrl'] ?? '')
                    ];
                }
                $output = Rest::json($projects);
                break;
            case 'room':
                $db_project = PHDB::findOneById(Project::COLLECTION, $_POST['project'], ['name', 'description']);
                $db_exists = PHDB::findOne(Room::COLLECTION, ['parentId' => $_POST['project'], 'parentType' => Project::COLLECTION]);
                if (empty($db_exists)) {
                    $insert_room = [
                        'collection' => Room::COLLECTION,
                        'name' => $db_project['name'],
                        'description' => $db_project['description'] ?? '',
                        'parentId' => $_POST['project'],
                        'parentType' => Project::COLLECTION,
                        'status' => 'open',
                        'created' => time(),
                        'creator' => \Yii::app()->session['userId']
                    ];
                    \Yii::app()->mongodb->selectCollection(Room::COLLECTION)->insert($insert_room);
                    $db_room = (string)$insert_room['_id'];
                } else {
                    $db_room = (string)$db_exists['_id'];
                }
                $output = Rest::json($db_room);
                break;
            case 'aap_project_ids':
                $look_for_sub_organization = [
                    'coSinDni'
                ];
                $form = $_POST['form'];
                if (in_array($_POST['costumSlug'], $look_for_sub_organization)) {
                    $forms = [$form];
                    $db_suborganization_ids = PHDB::find($_POST['costumType'], ['parentId' => $_POST['costumId']], ['_id']);
                    $db_suborganization_ids = array_keys($db_suborganization_ids);

                    $where_suborganizations = ['type' => 'aap', '$or' => []];
                    foreach ($db_suborganization_ids as $id) {
                        $where_suborganizations['$or'][] = ["parent.$id" => ['$exists' => true]];
                    }

                    $db_form_ids = PHDB::find(Form::COLLECTION, $where_suborganizations, ['_id']);
                    $forms = array_merge($forms, array_keys($db_form_ids));
                } else $forms = [$form];
                $db_answers = PHDB::find(Answer::COLLECTION, ['form' => ['$in' => $forms], 'project.id' => ['$exists' => true]], ['project.id']);

                $ids = [];
                foreach ($db_answers as $db_answer) {
                    if (!in_array($db_answer['project']['id'], $ids)) $ids[] = $db_answer['project']['id'];
                }
                $output = Rest::json($ids);
                break;
            case 'cd_from_costum':
                $db_cd_project = PHDB::findOneById($_POST['collection'], $_POST['id'], ['costum.cd_project']);
                $db_cd_project = $db_cd_project['costum']['cd_project'] ?? [];
                $db_cd_project['admin_only'] = $db_cd_project['admin_only'] ?? false;
                $output = Rest::json($db_cd_project);
                break;
            default:
                $output = Rest::json(['status' => false, 'message' => 'Bad request']);
                break;
        }
        return $output;
    }
}
