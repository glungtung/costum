<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumdescostums;

use CAction, CostumDesCostums, Rest;
class ParsingAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params = CostumDesCostums::parsing($_POST);

        return Rest::json($params);
    }
}