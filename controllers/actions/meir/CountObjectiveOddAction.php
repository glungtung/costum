<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meir;
use MongoId;
use PHDB;
use Rest;
use Organization;

class CountObjectiveOddAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $slug)
    {
        $results = array();
        if(isset($_POST["odd"])){
            foreach ($_POST['odd'] as $key => $value) {
                $count = PHDB::count(Organization::COLLECTION, 
                    array('$or'=>array(
                        array("source.keys" => $slug),
                        array("links.memberOf.".$id.".type"=>Organization::COLLECTION)
                    ),
                    "objectiveOdd"=> $value,
                    "category" => "startup"
                ));

                array_push($results, $count);
            }
        }
        return Rest::json($results);
    }
}