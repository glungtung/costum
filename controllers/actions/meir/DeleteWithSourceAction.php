<?php 
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meir;
use  Yii, CTKException, Rest, Meir;

class DeleteWithSourceAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run(){
        $controller = $this->getController(); 
        $params = array();
        $params=$_POST;

        $res = array("result"=>false, "msg"=>Yii::t("common", "Something went wrong!"));
        try {
            Meir::removeSourceFromElement($params);
            $res = array("result"=>true);
        } catch (CTKException $e) {
            $res = array("result"=>false, "msg"=>$e->getMessage());
        }
        return Rest::json($res);
    }

}