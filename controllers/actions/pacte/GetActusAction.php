<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;

use CAction, SiteDuPactePourLaTransition, Rest;
/**
 * 
 */
class GetActusAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$params = SiteDuPactePourLaTransition::getActus($_POST["sourceKey"]);

		return Rest::json($params);
	}
}

?>