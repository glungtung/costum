<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;

use CAction, SiteDuPactePourLaTransition, Rest;
class SaveContratAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');
		//Rest::json($_POST); exit;
        $params = SiteDuPactePourLaTransition::saveContrat($_POST);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($params);
    }
}
?>