<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;
use CAction;
use City;
use Element;
use Mail;
use MongoRegex;
use Organization;
use Person;
use PHDB;
use Rest;
use Slug;
use Yii;
use function mb_strtolower;

class RegisterAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){ 	
		$controller = $this->getController();
		
		$res = array(
			"result" => false,
			"msg"  => ""
		);
		
		if(!empty($_POST["scope"]) && !empty($_POST["email"])){
			$html = "";
			//Check if user exist
			// - Create account with no mail sended
			$userMail=$_POST["email"];
			$user = PHDB::findOne(Person::COLLECTION, array( "email" => new MongoRegex( '/^'.preg_quote(trim($_POST["email"])).'$/i' ) ), array("_id", "name", "email"));
			if(empty($user)){
				$newPerson = array( "name" => $_POST["name"],
									"email" => $_POST["email"],
									"invitedBy" => "");
				$forced=array("scope"=>$_POST["scope"]);
				//if(@$_POST["isCandidate"] && $_POST["isCandidate"])
				//	$forced["tags"]=array("candidat2020");
				$user=Person::insert($newPerson, Person::REGISTER_MODE_MINIMAL, null, $forced);
				//$user = Person::createAndInvite($newPerson, "");
				$html .= "<span>Un nouvelle utilisateur s'est connecté : ".$_POST["name"]."</span><br/>";
			}else{
				$user["id"] = (String) $user["_id"];
				$html .= "<span>Un utilisateur s'est connecté : ".$user["name"]."</span><br/>";
			}

			// boolean to send mail notification to pacte in order to validate the group for email frama process
			$sendTopacteCreate=false;
			
			/***** Begin - process to know which locality is concerned *****
			* - Get the name and the postal code selected by the user
			* - Cities with multi postal code (ex : Lille , Rennes , etc) will return the first entry considering as main entry by Pacte's collectif
			***********************************************************/
			foreach ($_POST["scope"] as $key => $value) {
				$name = (isset($value["cityName"]) && !empty($value["cityName"])) ? ucfirst(strtolower($value["cityName"])) : $value["name"];
				$checkScope=$key;
				$postalCode=$value["postalCode"];
				$city = City::getById($value["city"], array("name","postalCodes", "geo", "geoPosition"));
				$scopeToRegister=$_POST["scope"];
				//var_dump($city);
				if(count($city["postalCodes"]) > 1){
					$postalCode=$city["postalCodes"][0]["postalCode"];
					$name = (isset($city["name"])) ? $city["name"] : $name;
					$checkScope=(string)$city["_id"]."cities".$postalCode;
					$scopeToRegister=array($checkScope=>$value);
				}
			}
			// END PREPARING LOCALITY PROCESS
			//var_dump($checkScope);
			// CHECK IF COLLECTIF IS ALREADY CREATED
			$name = "Collectif local ".trim($name);
			$where = array( "source.key" => $controller->costum["slug"],
							"scope.".$checkScope => array('$exists' => true )  );
			$exist = PHDB::findOne(Organization::COLLECTION, $where, array("_id", "name", "email", "source"));
			if(!empty($exist)){
				$res["exist"] = true;
				$res["elt"] = $exist;
				$res["email"] = $exist["email"];
				//On rentre dans le cas ou l'orga existe et la mailing est créée 

				if(!isset($exist["source"]["toBeValidated"]) || empty($exist["source"]["toBeValidated"])){
					// TODO MAIL TO FRAMA
					//Si message groupe on envoie le mail avec le texte de l'utilisateur
					$sub=explode("@", $exist["email"])[0];
					if(isset($_POST["newsletterCollectif"]) && !empty($_POST["newsletterCollectif"])){
						$paramsMails = array("tplMail" => $sub."-subscribe@listes.transition-citoyenne.org",
										"tplObject" => "sub ".$sub,
										"tpl" => "basic",
										"fromMail"=>$userMail,
										"html" => $html);
						Mail::createAndSend($paramsMails);
					}
					/*if(isset($_POST["msgGroup"])){
						$tplMsgList='<span><b>'.$_POST["name"].'</b> a écrit ce message :</span><br/><span style="padding:15px; margin-top:10px;background-color:#f9f9f9; border:1px solid #eee; border-radius: 10px;float:left;">'.$_POST["msgGroup"].'</span>';
						if(!empty($_POST["isCandidate"])){
							$tplMsgList.='<span><br/><br/><br/><b>Email de contact</b> : '.$_POST["email"].'</span>';
						}
						$objMail=(empty($_POST["isCandidate"])) ? "Message d'un nouveau membre sur le ".$sub : "Vous avez un message du candidat ".$_POST["name"]." sur le ".$sub ;
						$paramsMails = array("tplMail" => $exist["email"],
									"tplObject" =>  $objMail,//"Message d'un nouveau membre sur le ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $tplMsgList);
						Mail::createAndSend($paramsMails);
					}*/
				}else
					$sendTopacteCreate=true;
			}else{
				$res["exist"] = false;
				$res["email"] = "pacte-".mb_strtolower($postalCode)."@listes.transition-citoyenne.org";
				$orga = array(	"name" => $name,
								"collection" => Organization::COLLECTION, 
								"type" => Organization::TYPE_GROUP,
								"email" => $res["email"],
								"creator" => $user["id"],
								"scope" => $scopeToRegister,
								"geo"=>$city["geo"],
								"geoPosition"=>$city["geoPosition"],
								"preferences"=>array("private"=>true,"isOpenData"=>true, "isOpenEdition"=>false),
								"created" => time(),
								"source" => array(
									"key" => $controller->costum["slug"],
									"keys" => array($controller->costum["slug"]),
									"origin" => "costum")
								);
				/**** Begin - process to know if a group exist with the postal code *****
				* - Case of cities who shared the same cp (62223 => Anzin-st-aubin / Roclincourt / ecurie / )
				* - Will create the group to see it on the map 
				* - Collectif will be linked with the same email to mutualize 
				************************************************************************/
				$where = array( "source.key" => $controller->costum["slug"],
							"email"=> $orga["email"] );
				$findSameEmail = PHDB::findOne(Organization::COLLECTION, $where, array("_id", "name", "email", "source"));
				if(empty($findSameEmail) || (isset($findSameEmail["source"]["toBeValidated"]) && !empty($findSameEmail["source"]["toBeValidated"]))){
					$orga["source"]["toBeValidated"]=true;
				}
				// CREATE COLLECTIF LOCAL
				//if(empty($_POST["isCandidate"])){
					$orga = Element::prepData($orga) ;
					$orga["category"]="soutien";
	                Yii::app()->mongodb->selectCollection(Organization::COLLECTION)->insert( $orga );

				$slug=Slug::checkAndCreateSlug($orga["name"]);
    			Slug::save(Organization::COLLECTION,(String)$orga["_id"],$slug);
    			PHDB::update(Organization::COLLECTION,
							array("_id" => (String)$orga["_id"]) , 
							array('$set' => array("slug" => $slug)));
    			//}
    			// Cas ou un groupe est créé mais le code postal est commun donc mail commun
				if(!empty($findSameEmail)){
					if(!isset($findSameEmail["source"]["toBeValidated"]) || empty($findSameEmail["source"]["toBeValidated"])){
						// TODO MAIL TO FRAMA
						$sub=explode("@", $findSameEmail["email"])[0];
						if(isset($_POST["newsletterCollectif"]) && !empty($_POST["newsletterCollectif"])){
							$paramsMails = array("tplMail" => $sub."-subscribe@listes.transition-citoyenne.org",
												"tplObject" => "sub ".$sub,
												"tpl" => "basic",
												"fromMail"=>$userMail,
												"html" => $html);
							Mail::createAndSend($paramsMails);
						}
						/*if(isset($_POST["msgGroup"])){
								$tplMsgList='<span><b>'.$_POST["name"].'</b> a écrit ce message :</span><br/><span style="padding:15px; margin-top:10px;background-color:#f9f9f9; border:1px solid #eee; border-radius: 10px;float:left;">'.$_POST["msgGroup"].'</span>';
								
								if(!empty($_POST["isCandidate"])){
									$tplMsgList.='<span><br/><br/><br/><b>Email de contact</b> : '.$_POST["email"].'</span>';
								}
								$objMail=(empty($_POST["isCandidate"])) ? "Message d'un nouveau membre sur le ".$sub : "Vous avez un message du candidat ".$_POST["name"]." sur le ".$sub ;
								$paramsMails = array("tplMail" => $findSameEmail["email"],
									"tplObject" => $objMail,//"Message d'un nouveau membre sur le ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $tplMsgList);

							Mail::createAndSend($paramsMails);
						}*/
					}else{
						$sendTopacteCreate=true;
					}
				}else
					$sendTopacteCreate=true;
			}
			$res["result"] = true;

			if( $sendTopacteCreate===true) {
				$objMail="Un nouveau membre ".$_POST["name"];
				$html = "<span>Une nouvelle personne a crée le collectif de soutien: ".$name."</span><br/><span>Mail de la liste à créer : ".$res["email"]."</span>";
				$html.="<br/><span>Mail de l'utilisateur : ".$_POST["email"]."</span><br/>";
				$paramsMails = array("tplMail" => $controller->costum["admin"]["email"],
								"tplObject" => $objMail,
								"tpl" => "basic",
								"html" => $html);
				Mail::createAndSend($paramsMails);
			}
			if(isset($_POST["newsletter"]) && !empty($_POST["newsletter"])){
				$paramsMails = array("tplMail" => "collectifs-locaux-subscribe@listes.transition-citoyenne.org",
								"tplObject" => "sub ".$_POST["email"],
								"tpl" => "basic",
								"fromMail"=>$_POST["email"],
								"html" => "");
				Mail::createAndSend($paramsMails);
			}
			
		}

		
		return Rest::json($res);
	}
}