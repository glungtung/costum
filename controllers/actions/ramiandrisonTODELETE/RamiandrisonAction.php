<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ramiandrison;

use CAction, Ramiandrison, Rest;
class RamiandrisonAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');

        $params = Ramiandrison::getAnswer($_POST["id"]);
      
        return Rest::json($params);
    }
}
?>