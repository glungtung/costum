<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\chtitube;
use CAction;
use Element;
use Event;
use Mail;
use MongoId;
use Person;
use PHDB;
use Project;
use Proposal;
use Rest;
use type;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class ValidateAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$elt = PHDB::findOneById( $_POST["type"] , $_POST["id"], array("name", "preferences", "source", "creator", "title"));
		$res=array("result"=>false);
		if(!empty($elt)){
			$preferences=array("isOpenData"=> true, "isOpenEdition"=>false);
			$resUpdate = PHDB::update( $_POST["type"],
				    array("_id"=>new MongoId($_POST["id"])), 
				    array('$set' => array("preferences"=>$preferences)) );
			$pts=0;
			if(in_array($_POST["type"], [Organization::COLLECTION, Poi::COLLECTION]))
				$pts=1;
			else if($_POST["type"]==Event::COLLECTION)
				$pts=2;
			else if($_POST["type"]==Project::COLLECTION)
				$pts=3;
			else if($_POST["type"]==Proposal::COLLECTION)
				$pts=4;
			$user=Element::getElementById($elt["creator"], Person::COLLECTION, null, array("name","gamification", "email"));
			if(!empty($user["gamification"])){
				if(isset($user["gamification"]["source"])){
					if(isset($user["gamification"]["source"]["chtitube"]) && isset($user["gamification"]["source"]["chtitube"]["total"])){
						$user["gamification"]["source"]["chtitube"]["total"]=($user["gamification"]["source"]["chtitube"]["total"]+$pts);
					}else{
						$user["gamification"]["source"]["chtitube"]=array("total"=>$pts);
					}
				}else{
					$user["gamification"]["source"]=array("chtitube"=>array("total"=>$pts));					
				}
			}else{
				$user["gamification"]=array("source"=>array("chtitube"=>array("total"=>$pts)));
			}
			$resUpdate = PHDB::update( Person::COLLECTION,
				    array("_id"=>new MongoId($elt["creator"])), 
				    array('$set' => array("gamification"=>$user["gamification"])) );
			$html="<span>Bonjour ".$user["name"].",<br/><br/>Votre contenu a été validé !!<br/><br/> <b>Félicitations</b>, vous avez gagné <b>".$pts." point".(($pts>1) ? "s": "")."</b><br/><br/></span>".
				"<span>Retrouvez toutes vos références sur chtitube</span>";
			$titleElt=(@$elt["name"]) ? $elt["name"] : $elt["title"];
			$mailParams=array(
				"tplMail" => $user["email"],
				"tpl" => "basic",
				"tplObject" => "[chtitube] ".$titleElt." a été validé",
				"html"=> $html
			);
			Mail::createAndSend($mailParams);
			unset($elt["preferences"]["private"]);
			$res = array(
				"resUpdate" => $resUpdate,
				"elt" => $elt
			);
		}
		

		return Rest::json($res); 
	}
}
