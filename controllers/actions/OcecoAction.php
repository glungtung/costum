<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;

use Form;
use PHDB;
use Rest;
use Slug;
use Yii;

class OcecoAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($formid = null, $parentformid = null)
    {
        $ctrl= $this->getController();
        // $this->getController()->layout = "//layouts/mainSearch";
        $el_slug = PHDB::findOne(Slug::COLLECTION, array('id' => $parentformid));
        // $slug = "mieuxVoter";
        $el = Slug::getElementBySlug($el_slug, array("name", "type", "collection", "profilImageUrl", "slug", "oceco", "links"));
        $answerid = null;
        $el_form_id = $formid; //'6266d2f9f4861320bd12fa85';

        $elform = PHDB::findOneById(Form::COLLECTION, $el_form_id);
        if (!empty($elform)) {
            $el_configform = PHDB::findOneById(Form::COLLECTION, $elform["config"]);
        }
        $params = [
            "answerid" => @$answerid,
            // "slug" => @$slug, // slug de l'element qui abbrite le form config
            "el_slug" => @$el_slug,  // element qui abbrite le form config
            "elform" => @$elform, //le form parent
            "el" => @$el, //element qui abbrite le form parent
            "el_configform" => @$el_configform,
            "el_form_id" => @$el_form_id
        ];
        if(Yii::app()->request->isAjaxRequest){
            // $answers = PHDB::find(Form::ANSWER_COLLECTION,array("form" => (string)$elform["_id"],"answers.aapStep1.titre" => ['$exists' => true]));
            // return Rest::json($answers);
            return $ctrl->renderPartial('costum.views.custom.appelAProjet.callForProjects.listOceco', $params);
            // return $ctrl->renderPartial('costum.views.custom.appelAProjet.callForProjects.list', $params);
        }

    }
}