<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda;

use CacheHelper;
use DateTime;
use DateTimeZone;
use Event;
use MongoId;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use Yii;

class EventAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($request = '', $reorder = 0) {
        $output = [];
        switch ($request) {
            case 'all':
                $traduction_map = [
                    'getTogether' => 'Get together',
                    'participativeWork' => 'Participative work'
                ];

                $db_events = PHDB::findOneById($_POST['costumType'], $_POST['costumId'], ['links.events']);
                $events = [];
                $ids = [];
                if (!empty($db_events['links']) && !empty($db_events['links']['events'])) {
                    $keys = array_keys($db_events['links']['events']);
                    foreach ($keys as $key) {
                        $events = array_merge($events, $this->get_event_and_all_subevents($key));
                    }
                }
                foreach ($events as $event) $ids[] = new MongoId($event['id']);

                // search by key
                $db_events = PHDB::find(Event::COLLECTION, ['source.key' => $_POST['costumSlug'], '_id' => ['$not' => ['$in' => $ids]]], ['_id']);
                $keys = array_keys($db_events);
                foreach ($keys as $key) {
                    $events = array_merge($events, $this->get_event_and_all_subevents($key));
                }

                $groups = [];
                $color_cache = CacheHelper::get('color_cache') ?? [];
                foreach ($events as $event) {
                    $type = $event['type'];
                    if (!array_key_exists($type, $groups)) {
                        if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", Yii::t('category', $traduction_map[$type] ?? ucfirst($type)))) {
                            continue;
                        }
                        $color_cache["event$type"] = $color_cache["event$type"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
                        $groups[$type] = [
                            'value' => $type,
                            'color' => $color_cache["event$type"],
                            'count' => 1
                        ];
                    } else {
                        $groups[$type]['count'] ++;
                    }
                }
                $groups = array_values($groups);
                CacheHelper::set('color_cache', $color_cache);

                if (!empty($_POST['exclude'])) {
                    $temporary = [];
                    foreach ($events as $event) {
                        if (!in_array($event['type'], $_POST['exclude'])) {
                            if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type'])))) {
                                continue;
                            }
                            $event['color'] = $color_cache['event' . $event['type']];
                            $event['group'] = Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type']));
                            $temporary[] = $event;
                        }
                    }
                    $events = $temporary;
                } else {
                    $temporary = [];
                    foreach ($events as $event) {
                        if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type'])))) {
                            continue;
                        }
                        $event['color'] = $color_cache['event' . $event['type']];
                        $event['group'] = Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type']));
                        $temporary[] = $event;
                    }
                    $events = $temporary;
                }

                if ($reorder) $events = $this->reorder_by_date('startDate', $events);
                
                $output = Rest::json(['groups' => $groups, 'data' => $events]);
                break;
        }
        return $output;
    }

    private function get_event_and_all_subevents($id) {
        $db_current = PHDB::findOneById(Event::COLLECTION, $id, ['creator', 'name', 'description', 'shortDescription', 'startDate', 'endDate', 'slug', 'timeZone', 'type', 'links.subEvents', 'profilImageUrl']);
        if ($db_current === null) return [];
        $timezone = $db_current['timeZone'] ?? date_default_timezone_get();
        $start = new DateTime();
        $start->setTimestamp(UtilsHelper::get_as_timestamp(['startDate'], $db_current));
        $start->setTimezone(new DateTimeZone($timezone));
        $end = new DateTime();
        $end->setTimestamp(UtilsHelper::get_as_timestamp(['endDate'], $db_current));
        $end->setTimezone(new DateTimeZone($timezone));
        $events = [
            [
                'id' => $id,
                'slug' => $db_current['slug'] ?? '',
                'type' => $db_current['type'],
                'name' => $db_current['name'],
                'shortDescription' => $db_current['shortDescription'] ?? '',
                'description' => $db_current['description'] ?? '',
                'profilImageUrl' => $db_current['profilImageUrl'] ?? '',
                'startDate' => $start->format('c'),
                'endDate' => $end->format('c'),
            ]
        ];
        if (!empty($db_current['links']) && !empty($db_current['links']['subEvents'])) {
            $ids = array_keys($db_current['links']['subEvents']);
            foreach ($ids as $_id) {
                $events = array_merge($events, $this->get_event_and_all_subevents($_id));
            }
        }
        return $events;
    }

    private function reorder_by_date($field, $input) {
        $output = $input;
        $length = count($input);
        $has_permutation = true;
        while ($has_permutation) {
            $has_permutation = false;
            for ($i = 0; $i < $length - 1; $i++) {
                if (UtilsHelper::get_as_timestamp([$field], $output[$i]) > UtilsHelper::get_as_timestamp([$field], $output[$i + 1])) {
                    $has_permutation = true;
                    $temporary = $output[$i];
                    $output[$i] = $output[$i + 1];
                    $output[$i + 1] = $temporary;
                }
            }
        }
        return $output;
    }
}
