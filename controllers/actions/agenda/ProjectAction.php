<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda;
    
    use PHDB;
    use Action;
    use CacheHelper;
    use Project;
    use DateTime;
    use \PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    
    class ProjectAction extends \PixelHumain\PixelHumain\components\Action {
        public function run($request = '', $reorder = 0) {
            $output = [];
            switch ($request) {
                case 'all':
                    $fields = ['slug', 'name', 'shortDescription', 'description', 'created', 'startDate', 'endDate', 'profilImageUrl'];
                    $sort_criteria = ['startDate' => 1, 'created' => 1];
                    
                    if (!empty($_POST['ids'])) {
                        $where = ['_id' => ['$in' => array_map(fn($__id) => new \MongoId($__id), $_POST['ids'])]];
                    } else {
                        $where = ['$or' => [['parent.' . $_POST['costumId'] => ['$exists' => true]], ['parentId' => $_POST['costumId']]]];
                    }
                    $where['$and'] = $where['$and'] ?? [];
                    $where['$and'][] = ['state' => ['$nin' => ['uncomplete', 'deleted']]];
                    $where['$and'][] = ['status' => ['$nin' => ['uncomplete', 'deleted', 'deletePending']]];
                    $where['name'] = ['$exists' => true];
                    
                    $db_projects = PHDB::findAndSort(Project::COLLECTION, $where, $sort_criteria, 0, $fields);
                    $projects = [];
                    
                    $groups = [];
                    // cache de couleur
                    $color_cache = CacheHelper::get('color_cache') ?? [];
                    // cache de couleur
                    
                    foreach ($db_projects as $db_id => $db_project) {
                        if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", $db_project['name'])) {
                            continue;
                        }
                        
                        $color_cache["projectproj$db_id"] = $color_cache["projectproj$db_id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
                        
                        $project_start = new DateTime();
                        $project_end = new DateTime();
                        
                        $project_start_time = UtilsHelper::get_as_timestamp(['startDate', 'created'], $db_project);
                        $project_end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_project, strtotime(date('c', $project_start_time) . ' +1day'));
                        if (date('H:i:s', $project_end_time) === '00:00:00') $project_end_time -= 1;
                        
                        $db_actions = PHDB::findAndSort(Action::COLLECTION, ['parentId' => $db_id], ['startDate' => 1, 'created' => 1], 0, [
                            'name', 'slug', 'description', 'shortDescription', 'profilImageUrl', 'created', 'startDate', 'endDate', 'status', 'tasks'
                        ]);
                        $actions = [];
                        foreach ($db_actions as $db_action_id => $db_action) {
                            $action_start = new DateTime();
                            $action_end = new DateTime();
                            
                            $action_start_time = UtilsHelper::get_as_timestamp(['startDate', 'created'], $db_action, $project_start_time);
                            
                            $now = strtotime(date('Y-m-d H:i'));
                            $addition = max($now, strtotime(date('c', $action_start_time) . ' + 1day'));
                            
                            $action_end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_action, $addition);
                            if (date('H:i:s', $action_end_time) === '00:00:00') $action_end_time -= 1;
                            
                            $tasks = [];
                            if (!empty($db_action['tasks'])) {
                                foreach ($db_action['tasks'] as $db_task) {
                                    
                                    if (!is_array($db_task)) continue;
                                    $task_start = new DateTime();
                                    $task_end = new DateTime();
                                    
                                    $task_start_time = UtilsHelper::get_as_timestamp(           [
                                                                                                 'startDate', 'createdAt', 'created'
                                                                                             ], $db_task, $action_start_time);
                                    $task_end_time = UtilsHelper::get_as_timestamp(['checkedAt'], $db_task, strtotime(date('c', $task_start_time) . ' + 1day'));
                                    if (date('H:i:s', $task_end_time) === '00:00:00') $task_end_time -= 1;
                                    
                                    $task_start->setTimestamp($task_start_time);
                                    $task_end->setTimestamp($task_end_time);
                                    $tasks[] = [
                                        'id'        => 'task' . ($db_task['taskId'] ?? sha1(strval(time()))),
                                        'name'      => $db_task['task'] ?? '',
                                        'checked'   => !empty($db_task['checked']) ? filter_var($db_task['checked'], FILTER_VALIDATE_BOOLEAN) : false,
                                        'startDate' => $task_start->format('c'),
                                        'endDate'   => $task_end->format('c'),
                                        'color'     => $color_cache["projectproj$db_id"]
                                    ];
                                }
                                if ($reorder) {
                                    $tasks = $this->reorder_by_date('startDate', $tasks);
                                    if (count($tasks) > 0) {
                                        $action_start_time = min(strtotime($tasks[0]['startDate']), $action_start_time);
                                        $action_end_time = max(strtotime(end($tasks)['endDate']), $action_end_time);
                                    }
                                }
                            }
                            
                            $action_start->setTimestamp($action_start_time);
                            $action_end->setTimestamp($action_end_time);
                            
                            $actions[] = [
                                'id'               => 'act' . $db_action_id,
                                'slug'             => $db_action['slug'] ?? '',
                                'name'             => $db_action['name'],
                                'group'            => $db_project['name'],
                                'shortDescription' => $db_action['shortDescription'] ?? '',
                                'description'      => $db_action['description'] ?? '',
                                'profilImageUrl'   => $db_action['profilImageUrl'] ?? '',
                                'startDate'        => $action_start->format('c'),
                                'endDate'          => $action_end->format('c'),
                                'color'            => $color_cache["projectproj$db_id"],
                                'subs'             => $tasks
                            ];
                        }
                        if ($reorder) {
                            $actions = $this->reorder_by_date('startDate', $actions);
                            if (count($actions) > 0) {
                                $project_start_time = min(strtotime($actions[0]['startDate']), $project_start_time);
                                $project_end_time = max(strtotime(end($actions)['endDate']), $project_end_time);
                            }
                        }
                        
                        $project_start->setTimestamp($project_start_time);
                        $project_end->setTimestamp($project_end_time);
                        
                        $project = [
                            'id'               => "proj$db_id",
                            'slug'             => $db_project['slug'] ?? '',
                            'name'             => $db_project['name'],
                            'shortDescription' => $db_project['shortDescription'] ?? '',
                            'description'      => $db_project['description'] ?? '',
                            'profilImageUrl'   => $db_project['profilImageUrl'] ?? '',
                            'startDate'        => $project_start->format('c'),
                            'endDate'          => $project_end->format('c'),
                            'color'            => $color_cache["projectproj$db_id"],
                            'original_created' => $db_project['created'] ?? 0,
                            'subs'             => $actions
                        ];
                        
                        $groups[] = [
                            'value' => $db_id,
                            'name'  => $project['name'],
                            'color' => $color_cache["projectproj$db_id"],
                            'count' => count($project['subs']),
                            'info'  => $project['name']
                        ];
                        $projects[] = $project;
                    }
                    // cache de couleur
                    CacheHelper::set('color_cache', $color_cache);
                    // cache de couleur
                    if ($reorder) $projects = $this->reorder_by_date('startDate', $projects);
                    
                    if (!empty($_POST['exclude'])) {
                        $temporary = [];
                        foreach ($projects as $project) {
                            if (!in_array(substr($project['id'], 4), $_POST['exclude'])) {
                                $project['color'] = $color_cache['project' . $project['id']];
                                $temporary[] = $project;
                            }
                        }
                        $projects = $temporary;
                    } else {
                        $temporary = [];
                        foreach ($projects as $project) {
                            $project['color'] = $color_cache['project' . $project['id']];
                            $temporary[] = $project;
                        }
                        $projects = $temporary;
                    }
                    
                    $output = \Rest::json(['groups' => $groups, 'data' => $projects]);
                    break;
                case 'actions':
                    $db_project_ids = [];
                    // cache de couleur
                    $color_cache = CacheHelper::get('color_cache') ?? [];
                    // cache de couleur
                    
                    if ($_POST['costumType'] === 'projects') $db_project_ids[$_POST['costumId']] = PHDB::findOneById(Project::COLLECTION, $_POST['costumId'], ['name']);
                    else $db_project_ids = PHDB::find(Project::COLLECTION, ['parent.' . $_POST['costumId'] => ['$exists' => true]], ['name']);
                    
                    $ids = array_keys($db_project_ids);
                    $wheres = [];
                    foreach ($ids as $id) {
                        $wheres[] = ['parentId' => $id];
                        $color_cache["projectproj$id"] = $color_cache["projectproj$id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
                    }
                    
                    // cache de couleur
                    CacheHelper::set('color_cache', $color_cache);
                    // cache de couleur
                    
                    $db_actions = PHDB::findAndSort(Action::COLLECTION, ['$or' => $wheres], ['startDate' => 1, 'created' => 1], 0, [
                        'name', 'slug', 'description', 'shortDescription', 'profilImageUrl', 'created', 'startDate', 'endDate', 'status', 'parentId'
                    ]);
                    $actions = [];
                    foreach ($db_actions as $db_action) {
                        $start = new DateTime();
                        $end = new DateTime();
                        
                        $start_time = UtilsHelper::get_as_timestamp(['startDate', 'created'], $db_action);
                        $end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_action, strtotime(date('c', $start_time) . ' +1day'));
                        if (date('H:i:s', $end_time) === '00:00:00') $end_time -= 1;
                        
                        $start->setTimestamp($start_time);
                        $end->setTimestamp($end_time);
                        
                        $actions[] = [
                            'id'               => (string)$db_action['_id'],
                            'slug'             => $db_action['slug'] ?? '',
                            'name'             => $db_action['name'],
                            'group'            => $db_project_ids[$db_action['parentId']]['name'],
                            'shortDescription' => $db_action['shortDescription'] ?? '',
                            'description'      => $db_action['description'] ?? '',
                            'profilImageUrl'   => $db_action['profilImageUrl'] ?? '',
                            'startDate'        => $start->format('c'),
                            'color'            => $color_cache['projectproj' . $db_action['parentId']],
                            'endDate'          => $end->format('c')
                        ];
                    }
                    if ($reorder) $actions = $this->reorder_by_date('startDate', $actions);
                    $output = \Rest::json($actions);
                    break;
                case 'actions_tasks':
                    $db_project_ids = [];
                    // cache de couleur
                    $color_cache = CacheHelper::get('color_cache') ?? [];
                    // cache de couleur
                    
                    if ($_POST['costumType'] === 'projects') $db_project_ids[$_POST['costumId']] = PHDB::findOneById(Project::COLLECTION, $_POST['costumId'], ['name']);
                    else $db_project_ids = PHDB::find(Project::COLLECTION, ['parent.' . $_POST['costumId'] => ['$exists' => true]], ['name']);
                    
                    $ids = array_keys($db_project_ids);
                    $wheres = [];
                    foreach ($ids as $id) {
                        $wheres[] = ['parentId' => $id];
                        $color_cache["projectproj$id"] = $color_cache["projectproj$id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
                    }
                    
                    // cache de couleur
                    CacheHelper::set('color_cache', $color_cache);
                    // cache de couleur
                    
                    $db_actions = PHDB::findAndSort(Action::COLLECTION, ['$or' => $wheres], ['startDate' => 1, 'created' => 1], 0, [
                        'name', 'slug', 'description', 'shortDescription', 'profilImageUrl', 'created', 'startDate', 'endDate', 'status', 'parentId',
                        'tasks'
                    ]);
                    $actions = [];
                    foreach ($db_actions as $db_action) {
                        $action_start = new DateTime();
                        $action_end = new DateTime();
                        
                        $action_start_time = UtilsHelper::get_as_timestamp(['startDate', 'created'], $db_action);
                        $action_end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_action, strtotime(date('c', $action_start_time) . ' +1day'));
                        if (date('H:i:s', $action_end_time) === '00:00:00') $action_end_time -= 1;
                        
                        $tasks = [];
                        if (!empty($db_action['tasks'])) {
                            foreach ($db_action['tasks'] as $db_task) {
                                $task_start = new DateTime();
                                $task_end = new DateTime();
                                
                                $task_start_time = UtilsHelper::get_as_timestamp(['startDate', 'createdAt', 'created'], $db_task);
                                $task_end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_task, strtotime(date('c', $task_start_time) . ' +1day'));
                                if (date('H:i:s', $task_end_time) === '00:00:00') $task_end_time -= 1;
                                
                                $task_start->setTimestamp($task_start_time);
                                $task_end->setTimestamp($task_end_time);
                                
                                $tasks[] = [
                                    'id'        => 'task' . ($db_task['taskId'] ?? sha1(strval(time()))),
                                    'name'      => $db_task['task'],
                                    'checked'   => filter_var($db_task['checked'], FILTER_VALIDATE_BOOLEAN),
                                    'startDate' => $task_start->format('c'),
                                    'endDate'   => $task_end->format('c'),
                                    'color'     => $color_cache['project' . $db_action['parentId']]
                                ];
                            }
                            if ($reorder) $tasks = $this->reorder_by_date('startDate', $tasks);
                        }
                        
                        if (count($tasks) > 0) {
                            $action_start_time = min(strtotime($tasks[0]['startDate']), $action_start_time);
                            $action_end_time = min(strtotime(end($tasks)['endDate']), $action_end_time);
                        }
                        
                        $action_start->setTimestamp($action_start_time);
                        $action_end->setTimestamp($action_end_time);
                        
                        $actions[] = [
                            'id'               => 'act' . (string)$db_action['_id'],
                            'slug'             => $db_action['slug'] ?? '',
                            'name'             => $db_action['name'],
                            'group'            => $db_project_ids[$db_action['parentId']]['name'],
                            'shortDescription' => $db_action['shortDescription'] ?? '',
                            'description'      => $db_action['description'] ?? '',
                            'profilImageUrl'   => $db_action['profilImageUrl'] ?? '',
                            'startDate'        => $action_start->format('c'),
                            'endDate'          => $action_end->format('c'),
                            'color'            => $color_cache['projectproj' . $db_action['parentId']],
                            'subs'             => $tasks
                        ];
                    }
                    if ($reorder) $actions = $this->reorder_by_date('startDate', $actions);
                    $output = \Rest::json($actions);
                    break;
            }
            return $output;
        }
        
        private function reorder_by_date($field, $input) {
            $output = $input;
            $length = count($input);
            $has_permutation = true;
            while ($has_permutation) {
                $has_permutation = false;
                for ($i = 0; $i < $length - 1; $i++) {
                    if (UtilsHelper::get_as_timestamp([$field], $output[$i]) > UtilsHelper::get_as_timestamp([$field], $output[$i + 1])) {
                        $has_permutation = true;
                        $temporary = $output[$i];
                        $output[$i] = $output[$i + 1];
                        $output[$i + 1] = $temporary;
                    }
                }
            }
            return $output;
        }
    }
