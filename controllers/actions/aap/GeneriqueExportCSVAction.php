<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Answer;
use Form;
use PHDB;

class GeneriqueExportCSVAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $output = ["heads" => [], "bodies" => []];
        $debug = [];
        $depenseFinancementLabels = [];
        // $form = PHDB::findOneById(Form::COLLECTION, $_POST['form'], ['config']);
        // $config = PHDB::findOneById(Form::COLLECTION, $form['config'], ['subForms']);
        $answers = PHDB::find(Answer::COLLECTION, ["form" => $_POST["form"], "answers.aapStep1.titre" => ['$exists' => true]], ["answers"]);
        foreach ($answers as $answer) {
            $bodies = [];
            foreach ($_POST["champs"] as $champ) {
                $aaStep = $champ['step'];
                $index = $champ["label"];
                $debug[] = ((string)$answer['_id']) . ' Testing ' . $champ["value"] . ' in ' . $aaStep . ' : ' . (isset($answer["answers"][$aaStep][$champ["value"]]) ? 'YES' : 'NO');
                if (isset($answer["answers"][$aaStep][$champ["value"]])) {
                    switch ($champ["type"]) {
                        case "tpls.forms.cplx.address":
                            $address = $answer["answers"][$aaStep][$champ["value"]]["address"];
                            $bodies["address_name"] = $address["name"];
                            $bodies["address_level1"] = $address["level1Name"];
                            $bodies["address_level3"] = $address["level3Name"];
                            $bodies["address_level4"] = $address["level4Name"];
                            break;
                        case "tpls.forms.cplx.checkboxNew":
                        case "tpls.forms.tagsFix":
                        case "tpls.forms.tags":
                            $bodies[$index] = implode(", ", $answer["answers"][$aaStep][$champ["value"]]);
                            break;
                        case "tpls.forms.ocecoform.budget":
                            // restructuration du budget
                            $budgets = [];
                            $depenseTotal = 0;
                            $financeTotal = 0;
                            $depenseIndexe = 0;
                            $financeIndexe = 0;
                            foreach ($answer["answers"][$aaStep][$champ["value"]] as $depense) {
                                $_budgets = [
                                    "poste" => isset($depense["poste"]) ? $depense["poste"] : "",
                                    "montant" => isset($depense["price"]) ? $depense["price"] : "",
                                    "groupe" => isset($depense["group"]) ? $depense["group"] : "",
                                    "nature" => isset($depense["nature"]) ? $depense["nature"] : ""
                                ];
                                $depenseIndexe++;
                                $depenseTotal += empty($depense["price"]) ? 0 : intval($depense["price"]);
                                if (isset($depense["financer"]) && !empty($depense["financer"])) {
                                    $_budgets["financer"] = [];
                                    foreach ($depense["financer"] as $financer) {
                                        $_budgets["financer"][] = [
                                            "ligne" => isset($financer["line"]) ? $financer["line"] : "",
                                            "montant" => isset($financer["amount"]) ? $financer["amount"] : "",
                                            "financeur" => isset($financer["name"]) ? $financer["name"] : ""
                                        ];
                                        $financeIndexe++;
                                        $financeTotal += empty($financer["amount"]) ? 0 : intval($financer["amount"]);
                                    }
                                }
                                $budgets[] = $_budgets;
                            }

                            foreach ($budgets as $clefBudget => $budget) {
                                foreach ($budget as $_clefBudget => $_budget) {
                                    if ($_clefBudget == "financer" && (is_countable($_budget) ? count($_budget) : 0) > 0) {
                                        foreach ($_budget as $clefFinancer => $financer) {
                                            foreach ($financer as $_clefFinancer => $_financer) {
                                                $bodies[$index . "." . $clefBudget . "." . $_clefBudget . "." . $clefFinancer . "." . $_clefFinancer] = $_financer;
                                            }
                                        }
                                    } else {
                                        $bodies[$index . "." . $clefBudget . "." . $_clefBudget] = $_budget;
                                    }
                                }
                            }

                            $depenseFinancementLabels[$index . ' depense'] = max(isset($depenseFinancementLabels[$index . ' depense']) ? $depenseFinancementLabels[$index . ' depense'] : 0, $depenseIndexe);
                            $depenseFinancementLabels[$index . ' financer'] = max(isset($depenseFinancementLabels[$index . ' financer']) ? $depenseFinancementLabels[$index . ' financer'] : 0, $financeIndexe);
                            $bodies["$index Total"] = $depenseTotal;
                            $bodies["$index Financement Total"] = $financeTotal;
                            break;
                        default:
                            $bodies[$index] = $answer["answers"][$aaStep][$champ["value"]];
                            break;
                    }
                } else {
                    $bodies[$index] = "";
                }
            }
            // enlever les lignes vides
            foreach ($bodies as $body) {
                if (!empty($body)) {
                    $output["bodies"][] = $bodies;
                    break;
                }
            }
        }

        foreach ($_POST["champs"] as $champ) {
            switch ($champ["type"]) {
                case "tpls.forms.cplx.address":
                    array_push($output["heads"], "address_name", "address_level1", "address_level3", "address_level4");
                    break;
                case "tpls.forms.ocecoform.budget":
                    for ($index = 0; $index < $depenseFinancementLabels[$champ["label"] . ' depense']; $index++) {
                        $output["heads"][] = $champ['label'] . '.' . $index . ".poste";
                        $output["heads"][] = $champ['label'] . '.' . $index . ".montant";
                        $output["heads"][] = $champ['label'] . '.' . $index . ".groupe";
                        $output["heads"][] = $champ['label'] . '.' . $index . ".nature";
                    }
                    $output["heads"][] = $champ['label'] . " Total";
                    for ($index = 0; $index < $depenseFinancementLabels[$champ["label"] . ' depense']; $index++) {
                        for ($jndex = 0; $jndex < $depenseFinancementLabels[$champ["label"] . ' financer']; $jndex++) {
                            $output["heads"][] = $champ['label'] . '.' . $index . ".financer." . $jndex . '.ligne';
                            $output["heads"][] = $champ['label'] . '.' . $index . ".financer." . $jndex . '.montant';
                            $output["heads"][] = $champ['label'] . '.' . $index . ".financer." . $jndex . '.financeur';
                        }
                    }
                    $output["heads"][] = $champ['label'] . " Financement Total";
                    break;
                default:
                    $output["heads"][] = $champ["label"];
                    break;
            }
        }

        return json_encode($output);
    }
}
