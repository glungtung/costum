<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use MongoId;
use Person;
use PHDB;
use Rest;

class getElementById extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type,$id=null) {
        $controller=$this->getController();
        $fields = array();
        if(isset($_POST["fields"])){
            $fields = $_POST["fields"];
        }
        if(isset($_POST["id"]) && is_array($_POST["id"])){
            $arrId = array();
            foreach ($_POST["id"] as $key => $value) {
                $arrId[] = new MongoId($value);
            }
            $res = PHDB::find($type,array("_id" =>['$in' => $arrId]),$fields);
        }elseif(!empty($id))
            $res = PHDB::findOneById($type,$id);
            
        return Rest::json($res);
    }
}