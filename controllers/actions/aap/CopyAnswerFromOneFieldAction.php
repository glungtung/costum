<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Form;
use MongoId;
use MongoRegex;
use PHDB;
use Rest;
use SearchNew;
use Document;

class CopyAnswerFromOneFieldAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        if(!empty($_POST["answersSource"]) && !empty($_POST["answersTarget"]) && !empty($_POST["form"])){
            $form = PHDB::findOneById(Form::COLLECTION,$_POST["form"],array("params.tagsassociation"));
            $inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent"=> $_POST["form"],"step"=>"aapStep1"));
            $sourceAnswer = PHDB::findOneById(Form::ANSWER_COLLECTION,$_POST["answersSource"],array("answers.aapStep1"));
            $targetAnswer = $_POST["answersTarget"];

            if(!empty($sourceAnswer["answers"]["aapStep1"]) && !empty($form["params"]["tagsassociation"]["list"])){
                try {
                    $uploaderKeys = [];
                    foreach ($sourceAnswer["answers"]["aapStep1"] as $keyS => $valueS) {
                        if(in_array($keyS,$form["params"]["tagsassociation"]['list'])){
                            $where = [
                                "_id" => new MongoId($targetAnswer)
                            ];
                            $action = [
                                '$set' => array("answers.aapStep1.".$keyS => $valueS)
                            ];
                            PHDB::update(Form::ANSWER_COLLECTION,$where,$action);
                            
                            if(!empty($inputs["inputs"][$keyS]["type"]) && $inputs["inputs"][$keyS]["type"] == "tpls.forms.uploader"){
                                $uploaderKeys[] = "aapStep1.".$keyS;
                            }
                        }
                    }
                    if(!empty($uploaderKeys)){
                        $documents = PHDB::find(Document::COLLECTION,array(
                            "id" => (string)$sourceAnswer["_id"],
                            "subKey" => ['$in'=> $uploaderKeys]
                        ));
                        if(!empty($documents)){
                            foreach ($documents as $key => $value) {
                                unset($documents[$key]["_id"]);
                                $documents[$key]["id"] = $targetAnswer;
                            }
                            PHDB::batchInsert(Document::COLLECTION,array_values($documents));
                        }
                    }

                    $targetAnswer = PHDB::findOneById(Form::ANSWER_COLLECTION,$_POST["answersTarget"]);
                    return Rest::json(array("result"=>true, "answers"=> $targetAnswer));
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        }
    }
}