<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Form;
use MongoId;
use PHDB;
use Rest;
use Yii;

class PushTagsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id,$tag,$key) {
    	    $controller=$this->getController();
            PHDB::update(Form::COLLECTION,
                array( "_id" => new MongoId($id)), 
                array( '$addToSet' => ["params.".$key.".list"=>$tag] ) 
            );
            $res = array('result' => true , 'msg'=>Yii::t("common","Saved"));
                return Rest::json($res);
    }
}