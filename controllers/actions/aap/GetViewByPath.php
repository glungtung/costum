<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

class GetViewByPath extends \PixelHumain\PixelHumain\components\Action
{
    public function run($path=null,$params=null) {
    	$controller=$this->getController();
        if(empty($path))
            return "<h4>No view found</h4>" ;
        else{
            $_POST["params"] = $params;
            return $controller->renderPartial($path,$_POST, true);
        }    
            
    }
}