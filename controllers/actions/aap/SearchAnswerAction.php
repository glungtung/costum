<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Document;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Form;
use MongoId;
use MongoRegex;
use PHDB;
use Rest;
use SearchNew;
use Yii;

class SearchAnswerAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $keyword = preg_quote($_POST["key"], '/');
        $where = array(
            $_POST["path"] => new MongoRegex("/".$keyword."/i"),
            "answers.aapStep1.titre" => ['$exists'=>true],
        );
        $sort = array(
            "created" => 1
        );
        $data = PHDB::findAndSort($_POST["collection"],$where,$sort);
        $retained = "";
        $tempCount = 0;
        foreach ($data as $key => $value) {
            if(count($value["answers"]["aapStep1"]) > $tempCount){
                $tempCount = count($value["answers"]["aapStep1"]);
                $retained = $key;
            }
        }

    if(!empty($data) && !empty($retained)){
        $data = $data[$retained]; 
        $allD = PHDB::findAndSort( Document::COLLECTION
            ,array(
                "id"=>(string)$data["_id"],
                "type"=>$_POST["collection"]) , array( 'created' => -1 ));

        $data["docans"] = [];

        foreach ($allD as $idD => $doc){
            if (isset($doc["subKey"])){
                $subK = explode('.' , $doc["subKey"])[1];
                if (isset($data["docans"][$subK])){
                    array_push($data["docans"][$subK] , $doc);
                }else {
                    $data["docans"][$subK] = [$doc];
                }
            }
        }
    }

    return Rest::json(array( "result" => "success" , "data" => $data ));


    }
}