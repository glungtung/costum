<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use PHDB;
use Aap;
use Action;
use MongoId;
use Rest;
use Document;
use Project;
use Form;

class GetActionsByAnswer extends \PixelHumain\PixelHumain\components\Action{
    public function run($answerid){
        $controller = $this->getController();
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$answerid,array("project"));
        $projectid = null;
        $params = array('$or' => [
                array("answerId" => $answerid),
            ]
        );
        if(!empty($answer["project"]["id"])){
            $params['$or'][] = array("parentId" => $answer["project"]["id"]);
        }
        $actions = PHDB::find(Action::COLLECTION,$params);
        $details = [
            "actionsImgIds" => [],
            "actionsImgs" => [],

            "projectIds" => [],
            "projects" => [],

            "answerIds" => [],
            "answers" => [],

            "userIds" => [],
            "users" => []
        ];
        foreach($actions as $kact => $vact){
            if(!empty($vact["media"]["images"])){
                foreach($vact["media"]["images"] as $kimg => $vimg){
                    $details["actionsImgIds"][] = new MongoId($vimg);
                }
            }
            $details["projectIds"][] = new MongoId($vact["parentId"]);
            if(!empty($vact["answerId"])){
                $details["answerIds"][] = new MongoId($vact["answerId"]);
            }
            if(!empty($vact["links"]["contributors"])){
                foreach(array_keys($vact["links"]["contributors"]) as $kcontr => $vcontr){
                    if(!in_array($vcontr,$details["userIds"])){
                        $details["userIds"][] = new MongoId($vcontr);
                    }
                }
                $details["userIds"] = array_keys($vact["links"]["contributors"]);
            }
        }

        $details["actionsImgs"] = PHDB::find(Document::COLLECTION,array("_id" => ['$in' => $details["actionsImgIds"]]));
        if(!empty($details["actionsImgs"])){
            foreach($details["actionsImgs"] as $kdoc => $vdoc){
                $details["actionsImgs"][$kdoc]["docPath"] = Document::getDocumentPath($vdoc, true);
            }
        }
        
        $details["projects"] = PHDB::find(Project::COLLECTION,array("_id" => ['$in' => $details["projectIds"]]),array("name"));
        return Rest::json(array(
            "actions" => $actions,
            "images" => $details["actionsImgs"],
            "projects" => $details["projects"]
        ));
    }
}