<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
    
    use Actions;
    use Answer;
    use CacheHelper;
    use Form;
    use Person;
    use PHDB;
    use PixelHumain\PixelHumain\components\Action;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    use Project;

    class AgendaAction extends Action {
        public function run($request = '') {
            $output = [];
            switch ($request) {
                case 'get_projects':
                    $output = $this->get_project_cache($_POST['form']);
                    break;
                case 'set_project_state':
                    $output = $this->set_project_state($_POST['project']);
                    break;
                case 'get_tasks':
                    $output = $this->get_tasks_from($_POST['action']);
                    break;
                case 'get_task':
                    $output = $this->get_tasks_from($_POST['action'], intval($_POST['index']));
                    break;
                case 'get_actions':
                    $output = $this->get_actions_from($_POST['project']);
                    break;
                case 'get_action':
                    $output = $this->get_action($_POST['action']);
                    break;
                case 'get_project':
                    $output = $this->get_project($_POST['project']);
                    break;
                case 'get_projects':
                    $output = $this->get_projects_from($_POST['form']);
                    break;
                case 'get_project_names':
                    $output = $this->get_project_names_from($_POST['form']);
                    break;
            }
            return json_encode($output);
        }
        
        private function get_project($project) {
            $cache = CacheHelper::get("agenda_project_$project");
            $output = [];
            if (!empty($cache) || $cache === false) $output = $cache;
            else {
                $this->get_project_cache($_POST['form']);
                $output = CacheHelper::get("agenda_project_$project");
            }
            return $output;
        }
        
        private function set_project_state($project) {
            $cache = $this->get_project($project);
            $cache['filter_show'] = $_POST['show'];
            CacheHelper::set("agenda_project_$project", $cache);
            return $cache['filter_show'];
        }
        
        private function get_project_cache($form) {
            $look_for_sub_organization = [
                'coSinDni'
            ];
            
            if (in_array($_POST['costumSlug'], $look_for_sub_organization)) {
                $forms = [$form];
                $db_suborganization_ids = PHDB::find($_POST['costumType'], ['parentId' => $_POST['costumId']], ['_id']);
                $db_suborganization_ids = array_keys($db_suborganization_ids);
                
                $where_suborganizations = ['type' => 'aap', '$or' => []];
                foreach ($db_suborganization_ids as $id) {
                    $where_suborganizations['$or'][] = ["parent.$id" => ['$exists' => true]];
                }
                
                $db_form_ids = PHDB::find(Form::COLLECTION, $where_suborganizations, ['_id']);
                $forms = array_merge($forms, array_keys($db_form_ids));
            } else $forms = [$form];
            
            $controller = $this->getController();
            $known_contributors = [];
            $projects = [];
            $ids = !empty($_POST['ids']) ? $_POST['ids'] : [];
            $answers_having_projects_database = PHDB::find(Answer::COLLECTION, [
                'form' => ['$in' => $forms], 'project.id' => ['$exists' => true]
            ],                                             ['project.id', 'project.startDate']);
            $db_projects = [];
            $iteration_project = [];
            
            if (count($ids) > 0) {
                foreach ($ids as $id) {
                    if (!in_array($id, $iteration_project)) {
                        $db_projects[] = ['project' => ['id' => $id]];
                        $iteration_project[] = $id;
                    }
                }
            } else {
                foreach ($answers_having_projects_database as $one) {
                    if (!in_array($one['project']['id'], $iteration_project)) {
                        $db_projects[] = $one;
                        $iteration_project[] = $one['project']['id'];
                    }
                }
            }
            
            foreach ($db_projects as $answer_project_database) {
                $project_id = $answer_project_database['project']['id'];
                $project_cache = CacheHelper::get("agenda_project_$project_id");
                
                if (!empty($project_cache) && $project_cache !== false) {
                    $projects[] = $project_cache;
                    continue;
                }
                
                $project_database = PHDB::findOneById(Project::COLLECTION, $project_id, [
                    'name',
                    'startDate',
                    'endDate',
                    'creator',
                    'created',
                    'oceco'
                ]);
                
                if (count($project_database) == 0) continue;
                
                if (!empty($answer_project_database['project']['startDate']) && empty($project_database['startDate'])) {
                    $project_database['startDate'] = $answer_project_database['project']['startDate'];
                }
                
                $action_database['links']['contributors'] = $action_database['links']['contributors'] ?? [];
                
                $project_start_date = UtilsHelper::get_as_timestamp(['startDate', 'created'], $project_database, strtotime('now'));
                $project_end_date = UtilsHelper::get_as_timestamp(['endDate'], $project_database, strtotime(date('Y-m-d H:i:s', $project_start_date) . ' +1day'));
                if (date('H:i:s', $project_end_date) === '00:00:00') $project_end_date -= 1;
                
                $creator = $known_contributors[$project_database['creator']] ?? PHDB::findOneById(Person::COLLECTION, $project_database['creator'], [
                    'name', 'profilThumbImageUrl'
                ]);
                if (empty($known_contributors[$project_database['creator']])) $known_contributors[$project_database['creator']] = [
                    'id'    => $project_database['creator'], 'name' => $creator['name'],
                    'image' => $creator['profilThumbImageUrl'] ?? $controller->module->assetsUrl . '/images/thumbnail-default.jpg'
                ];
                
                $color = '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
                $a_project = [
                    'id'          => $project_id,
                    'background'  => $color,
                    'name'        => $project_database['name'],
                    'filter_show' => true,
                    'start_date'  => date('c', $project_start_date),
                    'creator'     => $known_contributors[$project_database['creator']],
                    'actions'     => []
                ];
                
                $actions_database = PHDB::find(Actions::COLLECTION, ['parentId' => $project_id], [
                    'name',
                    'credits',
                    'status',
                    'creator',
                    'created',
                    'startDate',
                    'min',
                    'max',
                    'links.contributors',
                    'milestone.name',
                    'milestone.startDate',
                    'milestone.endDate',
                    'milestone.status',
                    'finishedBy',
                    'endDate',
                    'tasks'
                ]);
                
                $actions_start_date = $project_start_date;
                foreach ($actions_database as $id => $action_database) {
                    $action_start_date = UtilsHelper::get_as_timestamp(['startDate', ' created'], $action_database, $actions_start_date);
                    $action_end_date = UtilsHelper::get_as_timestamp(['endDate'], $action_database, strtotime(date('Y-m-d H:i:s', $action_start_date) . ' +1day'));
                    if (date('H:i:s', $action_end_date) === '00:00:00') $action_end_date -= 1;
                    
                    $creator_id = $action_database['creator'] ?? $project_database['creator'];
                    $creator = $known_contributors[$creator_id] ?? PHDB::findOneById(Person::COLLECTION, $creator_id, [
                        'name', 'profilThumbImageUrl'
                    ]);
                    if (empty($known_contributors[$creator_id])) $known_contributors[$creator_id] = [
                        'id'    => $creator_id, 'name' => $creator['name'],
                        'image' => $creator['profilThumbImageUrl'] ?? $controller->module->assetsUrl . '/images/thumbnail-default.jpg'
                    ];
                    
                    $an_action = [
                        'id'           => $id,
                        'parent'       => $project_id,
                        'credit'       => $action_database['credits'] ?? 0,
                        'name'         => $action_database['name'],
                        'min'          => $action_database['min'] ?? 0,
                        'max'          => $action_database['max'] ?? 1,
                        'creator'      => $known_contributors[$creator_id],
                        'contributors' => [],
                        'start_date'   => date('c', $action_start_date),
                        'tasks'        => []
                    ];
                    
                    $contributors = $action_database['links']['contributors'] ?? [];
                    
                    foreach ($contributors as $id => $content) {
                        $contributor = $known_contributors[$id] ?? PHDB::findOneById($content['type'], $id, ['name', 'profilThumbImageUrl']);
                        if (empty($known_contributors[$id])) $known_contributors[$id] = [
                            'id' => $id, 'name' => $contributor['name'], 'image' => $contributor['profilThumbImageUrl'] ?? ''
                        ];
                        $an_action['contributors'][] = $known_contributors[$id];
                    }
                    
                    $tasks = $action_database['tasks'] ?? [];
                    $tasks_start_date = $action_start_date;
                    foreach ($tasks as $task) {
                        $task_start_date = UtilsHelper::get_as_timestamp(['startDate', 'createdAt'], $task, $tasks_start_date);
                        $task_end_date = UtilsHelper::get_as_timestamp(['endDate'], $task, strtotime(date('Y-m-d H:i:s', $task_start_date) . ' +1day'));
                        if (date('H:i:s', $task_end_date) === '00:00:00') $task_end_date -= 1;
                        
                        $a_task = [
                            'id'         => $task['taskId'] ?? strval(base_convert(rand(0, 16777215), 10, 16)),
                            'parent'     => $id,
                            'name'       => $task['task'] ?? '',
                            'amount'     => $task['credits'] ?? 0,
                            'duration'   => round(($task_end_date - $task_start_date) / 60 / 60 / 24, 0),
                            'start_date' => date('c', $task_start_date),
                            'end_date'   => date('c', $task_end_date),
                            'done'       => $task['checked'] ?? false
                        ];
                        if ($task_end_date <= $task_start_date) {
                            $task_end_date = strtotime($a_task['start_date'] . ' +1day');
                            if (date('H:i:s', $task_end_date) === '00:00:00') $task_end_date -= 1;
                            $a_task['end_date'] = date('c', $task_end_date);
                        }
                        
                        $action_end_date = max($action_end_date, strtotime($a_task['end_date']));
                        $action_start_date = min($action_start_date, strtotime($a_task['start_date']));
                        $an_action['tasks'][] = $a_task;
                    }
                    $an_action['tasks'] = $this->reorder_content($an_action['tasks'], 'start_date');
                    
                    if ($action_end_date <= $action_start_date) {
                        $action_end_date = strtotime($an_action['start_date'] . ' +1day');
                        if (date('H:i:s', $action_end_date) === '00:00:00') $action_end_date -= 1;
                        $an_action['end_date'] = date('c', $action_end_date);
                    }
                    
                    $an_action['end_date'] = date('c', $action_end_date);
                    $an_action['start_date'] = date('c', $action_start_date);
                    $an_action['duration'] = round(($action_end_date - $action_start_date) / 60 / 60 / 24, 0);
                    $a_project['actions'][] = $an_action;
                    
                    $project_end_date = max($project_end_date, strtotime($an_action['end_date']));
                    $project_start_date = min($project_start_date, strtotime($an_action['start_date']));
                }
                $a_project['actions'] = $this->reorder_content($a_project['actions'], 'start_date');
                
                $associative_actions = [];
                foreach ($a_project['actions'] as $action) {
                    $associative_actions[$action['id']] = $action;
                    CacheHelper::set('agenda_action_' . $action['id'], $action);
                }
                
                if ($project_end_date <= $project_start_date) {
                    $project_end_date = strtotime($a_project['start_date'] . ' +1day');
                    if (date('H:i:s', $project_end_date) === '00:00:00') $project_end_date -= 1;
                    $a_project['end_date'] = date('c', $project_end_date);
                }
                
                $a_project['end_date'] = date('c', $project_end_date);
                $a_project['start_date'] = date('c', $project_start_date);
                
                $a_project['duration'] = round(($project_end_date - $project_start_date) / 60 / 60 / 24, 0);
                $projects[] = $a_project;
            }
            $projects = $this->reorder_content($projects, 'start_date');
            foreach ($projects as $project) {
                CacheHelper::set('agenda_project_' . $project['id'], $project);
            }
            return $projects;
        }
        
        private function reorder_content($projects, $criteria) {
            $permutted = false;
            $array = $projects;
            $length = count($array);
            do {
                $permutted = false;
                for ($i = 1; $i < $length; $i++) {
                    if (is_bool($array[$i])) {
                        echo "<pre>";
                        print_r($projects);
                        echo "</pre>";
                        exit();
                    }
                    $timestamp_first = strtotime($array[$i - 1][$criteria]);
                    $timestamp_last = strtotime($array[$i][$criteria]);
                    if ($timestamp_first > $timestamp_last) {
                        $permutted = true;
                        [$array[$i - 1], $array[$i]] = [$array[$i], $array[$i - 1]];
                    }
                }
            } while ($permutted);
            return $array;
        }
        
        private function get_tasks_from($action, $index = -1) {
            $tasks_datbase = PHDB::findOneById(Actions::COLLECTION, $action, ['tasks']);
            $tasks_datbase['tasks'] = $tasks_datbase['tasks'] ?? [];
            $tasks = [];
            foreach ($tasks_datbase['tasks'] as $task) {
                $task_start_date = UtilsHelper::get_as_timestamp(['startDate', 'createdAt'], $task, date('u'));
                $task_end_date = UtilsHelper::get_as_timestamp(['endDate'], $task, strtotime(date('Y-m-d H:i:s', $task_start_date) . ' +1day'));
                if (date('H:i:s', $task_end_date) === '00:00:00') $task_end_date -= 1;
                
                $task['taskId'] = $task['taskId'] ?? strval(base_convert(rand(0, 16777215), 10, 16));
                $task['credits'] = intval($task['credits'] ?? 0);
                $task['createdAt'] = date('c', $task_start_date);
                $task['startDate'] = date('c', $task_start_date);
                $task['endDate'] = date('c', $task_end_date);
                $task['checked'] = $task['checked'] ?? false;
                if (!empty($task['checkedAt'])) date('c', UtilsHelper::get_as_timestamp('checkedAt', $task, $task_end_date));
                $tasks[] = $task;
            }
            $tasks = $index >= 0 ? $tasks[$index] : $tasks;
            return $tasks;
        }
        
        private function get_action($action) {
            //CacheHelper::flush();
            CacheHelper::delete('agenda_action_' . $action);
            $cache_action = CacheHelper::get('agenda_action_' . $action);
            $output = $cache_action;
            if (empty($cache_action) || $cache_action === false) {
                $db_action = PHDB::findOneById(Actions::COLLECTION, $action, ['parentId']);
                CacheHelper::delete('agenda_project_' . $db_action['parentId']);
                $project = CacheHelper::get('agenda_project_' . $db_action['parentId']);
                if ($project === false) {
                    $this->get_project_cache($_POST['form']);
                    $project = CacheHelper::get('agenda_project_' . $db_action['parentId']);
                }
                $output = CacheHelper::get('agenda_action_' . $action);
            }
            return $output;
        }
        
        private function get_actions_from($project) {
            $output = [];
            $project = empty($project) ? (boolval($_POST['is_array']) ? [] : '') : $project;
            if (is_array($project)) {
                foreach ($project as $a_project) {
                    $cache_project = CacheHelper::get('agenda_project_' . $a_project);
                    if (empty($cache_project) || $cache_project === false) {
                        $cache_project = $this->get_project_cache($_POST['form'])[$a_project];
                    }
                    $output = array_merge($output, $cache_project['actions']);
                }
            } else if (is_string($project)) {
                $cache_project = CacheHelper::get('agenda_project_' . $project);
                if ($cache_project === false) {
                    $cache_project = $this->get_project_cache($_POST['form'])[$project];
                }
                $output = $cache_project['actions'];
            } else {
                $caches = $this->get_project_cache($_POST['form']);
                foreach ($caches as $cache) {
                    $output = array_merge($output, $cache['actions']);
                }
            }
            $output = $this->reorder_content($output, 'start_date');
            return $output;
        }
        
        private function get_project_names_from($form) {
            $caches = $this->get_project_cache($form);
            $projects = [];
            foreach ($caches as $cache) {
                $projects[] = [
                    'id'   => $cache['id'],
                    'name' => $cache['name']
                ];
            }
            return $projects;
        }
    }
