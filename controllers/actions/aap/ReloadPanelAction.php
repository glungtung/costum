<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Form;
use Aap;
use PHDB;
class ReloadPanelAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        $params = [
            "answerId" => $_POST["answerId"],
            "formId" => $_POST["formId"],
            "formConfigId" => $_POST["formConfigId"],
            "el" => $_POST["el"],
            "view" => $_POST["view"],
            "slug" => $_POST["slug"]
        ];
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$params["answerId"]);
        $data = Aap::prepareData([$params["answerId"]],Form::ANSWER_COLLECTION,$params["el"],$params["formId"],$params["formConfigId"]);
        $ansVar = Aap::answerVariables($answer,$data,$data["stepEval"]);
        $renderParams = array(
            "pos" => 1, 
            "view" => $params["view"],
            "answer" => $answer,
            "is_coform" => false,
            "el_form_id" => $params["formId"],
            "data" => $data,
            "answerid" => $params["answerId"],
            "ansVar" => $ansVar,
            "slug" => $params["slug"],
            "urgentState " => $ansVar["urgentState"],
            "coremu" => false,
            "timezone" => $_POST["timezone"],
        );
        if (!empty($data["elform"]['coremu']) && $data["elform"]['coremu'] == true){
            $anscoremuVar = Aap::answerCoremuVariables($answer);
            $renderParams["anscoremuVar"] = $anscoremuVar;
        }
        return $controller->renderPartial("costum.views.custom.appelAProjet.callForProjects.listItemView.".$params["view"],$renderParams,true);
    }
}