<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use CAction, CTKException, Rest;
use Mail,Aap;

class MailAction extends \PixelHumain\PixelHumain\components\Action {
    
    public function run($preview=false) {
        $controller=$this->getController();
        try {
            $_POST["preview"] = $preview;
            
            $_POST["controller"] = $controller;
            $res = Aap::sendMail($_POST);
            return Rest::json($res);
        } catch (CTKException $e) {
            Rest::sendResponse(450, "Webhook : ".$e->getMessage());
            die;
        }
         //return Rest::json(array("result"=>true, "msg"=>"Ok : webhook handdled"));
 //       Rest::sendResponse(200, "Ok : webhook handdled");
    }
}