<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Form;
use MongoId;
use MongoDate;
use Person;
use PHDB;
use Rest;
use Yii;

class PublicRateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        if(Person::logguedAndValid()){
            $me = Yii::app()->session['userId'];
            /*if(!empty($_POST["id"]) && !empty($_POST["value"])){
                PHDB::update( Form::ANSWER_COLLECTION,
						    [ "_id" => new MongoId($_POST["id"]) ], 
						    [ '$set' => [ "publicRating.".$me => (boolean) $_POST["value"] ] ]);
                $msg=Yii::t("common","Everything is allRight");
                return Rest::json(array("result"=>true, "msg"=>$msg,"params"=> $_POST["value"]));
            }*/
            if(!empty($_POST["id"]) && !empty($_POST["value"])){
                if($_POST["value"] == "true"){
                    PHDB::update( Form::ANSWER_COLLECTION,
                        [ "_id" => new MongoId($_POST["id"]) ], 
                        [ '$set' => [ "vote.".$me => array("status" => "love","date" => new MongoDate(time())) ] ]
                    );
                }elseif ($_POST["value"] == "false") {
                    PHDB::update( Form::ANSWER_COLLECTION,
                        [ "_id" => new MongoId($_POST["id"]) ], 
                        [ '$unset' => [ "vote.".$me => true] ]
                    );
                }


                $msg=Yii::t("common","Everything is allRight");
                return Rest::json(array("result"=>true, "msg"=>$msg,"params"=> $_POST["value"]));
            }
        }else
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));
    }
}