<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Form;
use Aap;
use PHDB;
use Rest;
use Yii;
use Cms;
use MongoDate;
use MongoId;
use Authorisation;
use CacheHelper;

class GenerateFormOcecoAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller = $this->getController();
        $params = $_POST;
        if(empty($params["elId"]) && empty($params["elType"])){
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Invalid request")));
        }elseif(!Authorisation::isElementAdmin($params["elId"],$params["elType"],Yii::app()->session["userId"])){
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","You are not authorized")));
        }else{
            $params["el"] = PHDB::findOneById($params["elType"],$params["elId"]);
        }

        if(!empty($params["createOceco"])){
            $res = self::createOceco($controller,$params["elId"],$params["elType"],$params["formName"]);
            return Rest::json($res);
        }

        if(!empty($params["createForm"])){
            $res = null;
            if(!empty($params["inputs"]))//default inputs
                $res = self::createForm($controller,$params["elId"],$params["elType"],$params["inputs"],$params["formName"]);
            else
                $res = self::createForm($controller,$params["elId"],$params["elType"],null,$params["formName"]);
            return Rest::json($res);
        }
    }

    private function createOceco ($controller,$idEl,$typeEl,$formName){
        $el = PHDB::findOne($typeEl,array("_id" => new MongoId($idEl)),array("slug","name","collection","costum"));
        $el = self::createCostumOceco($el,$controller);
        self::addAapCms($el);
        //begin create form config
        $templateConfig = null;
        $templateConfig = PHDB::findOne(Form::COLLECTION,array(
                "parent.".(string)$el["_id"] => ['$exists' => true],
                "type" => "aapConfig"
            ) 
        );
        if(empty($templateConfig)){
            $templateConfig = json_decode(file_get_contents("../../modules/survey/data/ocecotools/configFormOcecoform.json", FILE_USE_INCLUDE_PATH),true);
            $parentConfig = [];
            $parentConfig[$idEl] = array(
                "name" => $el["name"],
                "type" => $typeEl
            );
            $templateConfig["parent"] = $parentConfig;
    
            $inputsConfig = self::addInputsOceco();
            foreach ($inputsConfig as $keyinp => $valueinp) {
                unset($inputsConfig[$keyinp]["formParent"]);
            }
            Yii::app()->mongodb->selectCollection(Form::INPUTS_COLLECTION)->batchInsert($inputsConfig);
            $templateConfig["subForms"]["aapStep1"]["inputs"] = (string)$inputsConfig[0]["_id"];
            $templateConfig["subForms"]["aapStep2"]["inputs"] = (string)$inputsConfig[1]["_id"];
            $templateConfig["subForms"]["aapStep3"]["inputs"] = (string)$inputsConfig[2]["_id"];
            $templateConfig["subForms"]["aapStep4"]["inputs"] = (string)$inputsConfig[3]["_id"];
    
            Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($templateConfig);
        }


        $configId = (string)$templateConfig["_id"];
        $config = $templateConfig;
        $configElemId = array_keys($config["parent"])[0];
        $configElem = PHDB::findOneById($config["parent"][$configElemId]["type"],$configElemId);

        //begin create form parent
        $form = json_decode(file_get_contents("../../modules/survey/data/ocecotools/ocecoformtemplate.json", FILE_USE_INCLUDE_PATH),true);
        $form["config"] = $configId;
        $form["name"] = $formName;
        $p = [];
        $p[$idEl] = array(
            "name" => $el["name"],
            "type" => $typeEl
        );
        $form["parent"] = $p;
        $form["creator"] = Yii::app()->session["userId"];
        $form["created"] = time();
        $form["startDate"] = date('d/m/Y');//date('d/m/Y H:m')
        $form["startDateNoconfirmation"] = date('d/m/Y');
        $form["endDate"] =  date('d/m/Y', strtotime(' + 60 days'));
        $form["endDateNoconfirmation"] = date('d/m/Y', strtotime(' + 60 days'));
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($form);

        return array(
            "result"=> true,
            "msg"=> "Oceco crée",
            "parentForm" => (string) $form["_id"],
            "aap link" => Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")."/co/index/slug/".$configElem["slug"]."#oceco.slug.".$el["slug"].".formid.".(string) $form["_id"] .".aappage.list?view=detailed&inproject=false",
        );
    }

    private function addInputsOceco($idformParent=null){
        $inputs = json_decode(file_get_contents("../../modules/survey/data/ocecotools/inputsocecoform.json", FILE_USE_INCLUDE_PATH),true);
        if($idformParent != null){
            foreach ($inputs as $keyinp => $vinp) {
                $inputs[$keyinp]["formParent"] = $idformParent;
                if(!empty($vinp["inputs"])){
                    foreach ($vinp["inputs"] as $k => $v) {
                        if(!empty($inputs[$keyinp]["inputs"][$k]["positions"])){
                            $pos = array_values($inputs[$keyinp]["inputs"][$k]["positions"])[0];
                            //$kpos = array_keys($inputs[$keyinp]["inputs"][$k]["positions"])[0];
                            $inputs[$keyinp]["inputs"][$k]["positions"][$idformParent] = $pos;
                        }
                    }
                }
            }
        }
        return $inputs;
    }
    private function createCostumOceco($el,$controller){
        if(!isset($el["costum"]["slug"])){
            $costumTemplate = json_decode(file_get_contents("../../modules/survey/data/ocecotools/costumTemplateOcecoform.json", FILE_USE_INCLUDE_PATH),true);
            //$el["costum"] = $costumTemplate["costum"];
            PHDB::update($el["collection"],
                array("_id" => new MongoId((string)$el["_id"])),
                array(
                    '$set' => ["costum" => $costumTemplate["costum"]]
                )
            );
        }else if(empty($el["costum"]["app"]["#oceco"])){
            if(!empty($el["costum"]["app"])){
                PHDB::update(
                    $el["collection"],
                    array("_id" => new MongoId((string)$el["_id"])),
                    array(
                        '$set' => ["costum.app.#oceco" => [
                                        "isTemplate" => true,
                                        "staticPage" => true,
                                        "hash" => "#app.view",
                                        "urlExtra" => "/page/oceco",
                                        "subdomainName" => "Oceco"
                                    ],
                                    "costum.type" => "aap"
                        ]
                    )
                );
            }
        }
        CacheHelper::delete($el["slug"]);
        if(CacheHelper::get($_SERVER['SERVER_NAME']))
            CacheHelper::delete($_SERVER['SERVER_NAME']);
        return $el;
    }
    private function addAapCms($el){
        $cmsExists = PHDB::findOne(Cms::COLLECTION,array(
                "parent.".(string)$el["_id"] => ['$exists' => true],
                "path" => "tpls.blockCms.projects.callForProjects"
            )
        );
        if(empty($cmsExists)){
            $cms = json_decode(file_get_contents("../../modules/survey/data/ocecotools/cmsOcecoform.json", FILE_USE_INCLUDE_PATH),true);
            $parentCms = [];
            $parentCms[(string)$el["_id"]] = [
                "name" => $el["name"],
                "type" => $el["collection"]
            ];
            $cms["parent"] = $parentCms;
            Yii::app()->mongodb->selectCollection(Cms::COLLECTION)->insert($cms);
        }
    }

    private function createForm ($controller,$idEl,$typeEl,$defaultInputs=null,$formName){
        $el = PHDB::findOne($typeEl,array("_id" => new MongoId($idEl)),array("slug","name"));
        $form = json_decode(file_get_contents("../../modules/survey/data/ocecotools/emptyForm.json", FILE_USE_INCLUDE_PATH),true);

        $p = [];
        $p[$idEl] = array(
            "name" => $el["name"],
            "type" => $typeEl
        );
        $times = time();
        $form["id"] = $el["slug"].$times;
        $form["name"] = $formName;
        $form["parent"] = $p;
        $form["creator"] = Yii::app()->session["userId"];
        $form["created"] = time();
        $form["startDate"] = date('d/m/Y');//date('d/m/Y H:m')
        $form["startDateNoconfirmation"] = date('d/m/Y');
        $form["endDate"] =  date('d/m/Y', strtotime(' + 60 days'));
        $form["endDateNoconfirmation"] = date('d/m/Y', strtotime(' + 60 days'));
        $form["subForms"] = [$el["slug"].$times."_0"];
        $subForms = array();;
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($form);

        if(!empty($defaultInputs) && is_array($defaultInputs))
            $subForms = self::createSubForms((string) $form["_id"],$el["slug"].$times."_0",$el,$defaultInputs);
        else
            self::createSubForms((string) $form["_id"],$el["slug"].$times."_0",$el);

        $configLink = Yii::app()->getRequest()->getBaseUrl(true)."/#@".$el["slug"].".view.forms.dir.form.".(string) $form["_id"];
        $standaloneLink =  Yii::app()->getRequest()->getBaseUrl(true)."/#answer.index.id.new.form.".(string) $form["_id"].".mode.w.standalone.true";

        if(!empty($el["costum"]["slug"])){
            $standaloneLink =  Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")."#answer.index.id.new.form.".(string) $form["_id"].".mode.w.standalone.true";
        }
        return array(
            "result"=> true,
            "msg"=> "Oceco crée",
            "parentForm" => (string) $form["_id"],
            "config link" => $configLink,
            "standalone link" => $standaloneLink,
            "contextSlug" => $el["slug"],
            "keyUnik" => isset($subForms["inputs"]) ? array_keys($subForms["inputs"])[0] : ""
        );
    }
    private function createSubForms($parentForm,$subFormKeys,$el,$defaultInputs = null){
        $subform = array(
            "id" => $subFormKeys,
            "name" => "Etape 1",
            "type" => "openForm",
            "created" => time(),
            "creator" => Yii::app()->session["userId"],
            "updated" => time()
        );

        if(!empty($defaultInputs) && is_array($defaultInputs)){
            $subform["inputs"] = [];
            foreach ($defaultInputs as $key => $value) {
                $rand = random_int(1, 200); 
                $key = $el["slug"].time()."_".$rand;
                $paramsId = $value["id"];
                $subform["inputs"][$key] = [
                    "label" => $value["label"],
                    "type" => $value["type"]
                ];
                if($paramsId == "timeEvaluation"){
                    self::timeEvaluationParams($parentForm,$key);
                }
            }
        }
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($subform);
        return $subform;
    }

    private function timeEvaluationParams($formId ,$keyInput){
        $defaultConfig = json_decode(file_get_contents("../../modules/survey/data/ocecotools/timeEvaluation.json", FILE_USE_INCLUDE_PATH),true);
        foreach ($defaultConfig as $key => $value) {
            $defaultConfig[$key.$keyInput] = $value;
            unset($defaultConfig[$key]);
        }
        return PHDB::update(Form::COLLECTION,array("_id"=> new MongoId($formId)),array(
            '$set' => ["params" => $defaultConfig]
        ));
    }
}