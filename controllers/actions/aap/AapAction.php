<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use PHDB;
use Action;
use MongoId;
use Rest;
use Project;
use Form;
use Person;
use Document;

class AapAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($action){
        $controller = $this->getController();
        $params = $_POST;
        $params["controller"] = $controller;
        return self::$action($params);
    }
    
    private function notification_history_by_anwerid($params){
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,($params["answerId"]));
        return $params["controller"]->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.NotificationHistory",array(
            "answer" => $answer
        ),true);
    }
}