<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Form;
use Aap;
use Authorisation;
use PHDB;
use Rest;
use Yii;
use Cms;
use MongoDate;
use MongoId;

class DuplicatePropositionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($form=null,$answerid=null,$year=null) {
    	$controller = $this->getController();
        $params = $_POST;
        if(!empty($form)) $params["form"] = $form;
        if(!empty($answerid)) $params["answers"] = $answerid;
        if(!empty($year)) $params["newYear"] = $year;

        if(!empty($params["newYear"])){
            $forms = PHDB::findOneById(Form::COLLECTION,$params["form"],array("params"));
            if(!empty($forms["params"]["duplicationYear"]) && $params["newYear"] != $forms["params"]["duplicationYear"]){
                if(Yii::app()->request->isAjaxRequest){
                    return Rest::json(array("result" => false,"msg" => Yii::t("common","Cannot be duplicated"))); 
                }else{
                    return '<h1 style="color:#f00;text-align:center">'.Yii::t("common", "You are not authorized").' '.Yii::t("common", "or").' '.Yii::t("common", "Cannot be duplicated").' '.Yii::t("common", "or").' '.Yii::t("common", "Already duplicated").'</h1>';
                }
            }
        }


        if(!empty($params["form"]) && !empty($params["answers"]) && !empty($params["newYear"])){
            $params["answers"] = explode("-",$params["answers"]);

            $params["answersIds"] = array_map(function($val){
                return new MongoId($val);
            },$params["answers"]);

            //find answers
            $answers = PHDB::find(Form::ANSWER_COLLECTION,[
                "_id" => array('$in' => $params["answersIds"]),
                "answers.aapStep1.year" => ['$ne' => $params["newYear"]]
            ]);
            //var_dump($params["answersIds"]);exit;

            //find if answers already duplicated
            $duplicatedAnswers = PHDB::find(Form::ANSWER_COLLECTION,[
                "duplicatedFrom" => array('$in' => $params["answers"]),
                "answers.aapStep1.year" => $params["newYear"]
            ]);

            $existsDuplications = [];
            foreach ($duplicatedAnswers as $kd => $vd) {
                $id = $vd["duplicatedFrom"];
                if(!empty($answers[$id])){
                    $existsDuplications[$kd] = $vd;
                    unset($answers[$id]);
                }    
            }

            //create new duplication array
            $duplications = [];
            foreach ($answers as $k => $v) {
                unset($v["_id"]);
                $v["duplicatedFrom"] = $k;
                $v["answers"]["aapStep1"]["year"] = $params["newYear"];
                $v["status"] = [];
                $duplications[] = $v;
            }

            if(!empty($duplications))
                Yii::app()->mongodb->selectCollection(Form::ANSWER_COLLECTION)->batchInsert($duplications);

            if(Yii::app()->request->isAjaxRequest){
                    return Rest::json(array("result" => true,"duplicated" => $duplications,"existsduplicated" => $existsDuplications, "msg" => Yii::t("common", "Duplicated")));
            }else{
                if(empty($duplications)){
                    return '<h1 style="color:#f00;text-align:center">'.Yii::t("common", "You are not authorized").' '.Yii::t("common", "or").' '.Yii::t("common", "Cannot be duplicated").' '.Yii::t("common", "or").' '.Yii::t("common", "Already duplicated").'</h1>';
                }else{
                    return '<h1 style="color:#0ea60e;text-align:center">'.Yii::t("common", "Duplicated").'</h1>';
                }
            }
                
            
            /*}else{
                return Rest::json(array("result" => false,"data" => $duplications, "msg" => "Déja dupliqué"));
            }*/
        }else{
            return Rest::json(array("result" => false,"msg" => Yii::t("common","Invalid request"))); 
        }
    }
}