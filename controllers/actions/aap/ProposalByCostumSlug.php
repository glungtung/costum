<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Form;
use Aap;
use Authorisation;
use PHDB;
use Rest;
use Yii;
use Cms;
use MongoDate;
use MongoId;
use Organization;
use Slug;
use SearchNew;
use Document;
use MongoRegex;
use Zone;
use Person;

class ProposalByCostumSlug extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action,$slug=null) {
    	$controller = $this->getController();
        $params= $_POST;
        $params["controller"] = $controller;

        $action = strtolower($action);
        return self::$action($params,$slug);
    }

    private function add($params,$slug){
        $elementConfig = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug));
        $newElement = $elementConfig;
        $newElement["name"] = $params["name"];
        $newElement["shortDescription"] = $params["shortDescription"];
        $newElement["parentId"] = (string)$elementConfig["_id"];
        $newElement["parentType"] = $elementConfig["collection"];
        $newElement["parent"] = [
            (string)$elementConfig["_id"] => [
                "type" => "organizations"
            ]
        ];
        $newElement["slug"] = Slug::checkAndCreateSlug($newElement["name"]);
        $newElement["updated"] = time();
        $newElement["created"] = time();
        $newElement["creator"] = Yii::app()->session["userId"];
        unset($newElement["_id"]);
        Yii::app()->mongodb->selectCollection(Organization::COLLECTION)->insert($newElement);
        $newSlug = Slug::checkAndCreateSlug($params["name"],$newElement["collection"],(string)$newElement["_id"]);
	    Slug::save($newElement["collection"],(string)$newElement["_id"],$newSlug);

        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent"));
        
        $formParent = PHDB::findOne(Form::COLLECTION,array(
                "parent.".(string)$elementConfig["_id"] => ['$exists'=>true],
                "type" => "aap",
                "config"=>(string)$config["_id"]
            )
        );
        $newFormParent = $formParent;
        $newFormParent["name"] = $params["name"];
        $newFormParent["shortDescription"] = "Création et vie du CAC ".$params["name"];
        $newFormParent["parent"] = [
            (string)$newElement["_id"] => [
                "type" => $newElement["collection"],
                "name" => $newElement["name"]
            ]
        ];
        $newFormParent["updated"] = time();
        $newFormParent["created"] = time();
        $newFormParent["creator"] = Yii::app()->session["userId"];
        unset($newFormParent["_id"]);
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($newFormParent);

        $formParentInputs = PHDB::find(Form::INPUTS_COLLECTION,array("formParent" => (string)$formParent["_id"]));
        foreach ($formParentInputs as $key => $value) {
            unset($formParentInputs[$key]["_id"]);
            $formParentInputs[$key]['formParent'] = (string)$newFormParent["_id"];
        }
        Yii::app()->mongodb->selectCollection(Form::INPUTS_COLLECTION)->batchInsert($formParentInputs);
        return Rest::json(array("result" => true,"msg" => Yii::t("common","Created Successfully")));
    }



    private function getparentforms($slug){
        $elementConfig = PHDB::findOne(Organization::COLLECTION,["slug" => $slug],array("name","slug","collection"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));
        $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
        return $parentForms;
    }

    private function getparentforms2($slug){ 
        // Je met le slug en statique provisoirement parce que j'ai rencontré un bug en utilisant le variable $slug
        $elementConfig = PHDB::findOne(Organization::COLLECTION,["slug" => "lesCommunsDesTierslieux"],array("name","slug","collection"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));
        $parentForms  = PHDB::findOne(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name","params"));
        return Rest::json($parentForms);
    }

    private function idsToMongoIds($arrayIds){
        $ids = [];
        foreach ($arrayIds as $key => $value) {
            $ids[] = new MongoId($value);
        }
        return $ids;
    }

    /*private function globalautocompleteaction2($searchParams,$slug){
        $form = self::getparentforms($slug);
        if(!empty($form)){
            $answers= Aap::globalAutocomplete(array_keys($form)[0], $searchParams);
            $answers["results"] = self::prepareGlobalautocompleteActionResult($answers["results"]);
            return Rest::json( $answers );
        }else{
            return Rest::json(array("result"=> false,"msg"=>"Empty form"));
        }
    }*/

    private function globalautocompleteaction($p,$slug){
        unset($p["controller"]);
        $p["searchType"] = $p["searchType"][0];
        $parentForms =  self::getparentforms($slug);
        $parentFormsIds = array_keys($parentForms);

        $query = array();
        $query = array("form"=> ['$in' =>$parentFormsIds],"answers.aapStep1.titre" => ['$exists' => true]);

        if(!empty($p["filters"]["context"])){
            $q = array('$or' => []);
            foreach ($p["filters"]["context"] as $kc => $vc) {
                $q['$or'][] = array("context.".$vc => ['$exists'=>true]);
            }
            $query = SearchNew::addQuery($query,$q);
            unset($p["filters"]["context"]);
        }

        if(!empty($p["filters"]["answers.aapStep1.tags"])){
            $q = ["answers.aapStep1.tags" => array( '$in'=> $p["filters"]["answers.aapStep1.tags"])];
            $query = SearchNew::addQuery($query,$q);
            unset($p["filters"]["answers.aapStep1.tags"]);
        }
        if(!empty($p["searchTags"])){
            $q = ["answers.aapStep1.tags" => array( '$in'=> $p["searchTags"])];
            $query = SearchNew::addQuery($query,$q);
        }

        if(!empty($p["filters"]["quartier"])){
            $q = array('$or' => []);
            foreach ($p["filters"]["quartier"] as $kc => $vc) {
                $q['$or'][] = array("answers.aapStep1.quartiers" => ['$in'=> $p["filters"]["quartier"]]);
            }
            $query = SearchNew::addQuery($query,$q);
            unset($p["filters"]["quartier"]);
        }

        if(!empty($p["filters"])){
            array_walk_recursive(
                $p["filters"],
                function (&$value) {
                    if ($value=="true")
                        $value = true;
                    else if($value == "false")
                        $value = false;
                }
            );
            $query = SearchNew::addQuery($query,$p["filters"]);
        }

        //search by name
        if(!isset($p["isZones"])){ 
			if(!empty($p["name"]) && !empty($p["textPath"]))
				$query = SearchNew::searchText($p["name"], $query, array("textPath"=>$p["textPath"]));

            $res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($p["searchType"],$query,array("user","context","form","answers"),array("updated" => -1), $p["indexStep"], $p["indexMin"]);
            //$res["results"] = PHDB::find($p["searchType"],$query);
        }else{
            $res["results"] = PHDB::find($p["searchType"],$query);
        }
        $res["query"] = $query;
        $res["params"] = $p;

        
        $res["results"] = self::prepareGlobalautocompleteActionResult($res["results"]);
        $res["count"][$p["searchType"]] = PHDB::count($p["searchType"], $query);
        return Rest::json( $res );
    }

    private function prepareGlobalautocompleteActionResult($result){
        $allImages = [
			"ansId" => [],
			"data" => []
		];

        $allContexts = [
			"contextIds" => [],
			"data" => []
		];

        foreach ($result as $key => $value) {
            $allImages["ansId"][] = $key;
            $allContexts["contextIds"][] = new MongoId(array_keys($value["context"])[0]);
        }

        $allImages["data"] = Document::getListDocumentsWhere(array(
			"id"=> ['$in'=>$allImages["ansId"]],  
			"type"=> Form::ANSWER_COLLECTION,
			"subKey"=>"aapStep1.image"), "file");

        $allContexts["data"] = PHDB::find(Organization::COLLECTION,array("_id" => ['$in' => $allContexts["contextIds"]]),array("name","slug"));

        foreach ($allImages["data"] as $kimg => $img) {
            if(!isset($result[$img["id"]]["answers"]["aapStep1"]["images"]))
                $result[$img["id"]]["answers"]["aapStep1"]["images"] = [];
            if(Document::urlExists(Yii::app()->getRequest()->getBaseUrl(true).$img["docPath"]))
                $result[$img["id"]]["answers"]["aapStep1"]["images"][] = $img["docPath"];
        }

        foreach($result as $kr => $vr){
            $cid =  array_keys($result[$kr]["context"])[0];
            //var_dump(@$allContexts["data"]);
            $result[$kr]["context"][$cid]["slug"] = $allContexts["data"][$cid]["slug"];
        }
        return $result;
    }

    private function openCacsListByQuartier($params){
        return $params["controller"]->renderPartial("costum.views.tpls.blockCms.projects.specifics.cacsList",array(
            "idquartier" => $params["idquartier"],
            "idcacs" => $params["idcacs"],
            "quartiername" => $params["quartiername"]
        ), true);
    }
    private function openActionListByCacs($params){
        return $params["controller"]->renderPartial("costum.views.tpls.blockCms.projects.specifics.cacsAction",array(
            /*"idquartier" => $params["idquartier"],
            "quartiername" => $params["quartiername"],*/
            "idcacs" => $params["idcacs"],
            "cacsname" => $params["cacsname"],
        ), true);
    }
}