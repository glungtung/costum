<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use MongoId;
use Person;
use PHDB;
use Rest;
use Ctenat;
use Form;
use Element;
use Link;
use Organization;
use Project;
use Aap;

class GetFinancorsByFormIdAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id) {
        $orgs = Aap::getAllFinancorsByFormId($id);
        return Rest::json($orgs);
    }
}