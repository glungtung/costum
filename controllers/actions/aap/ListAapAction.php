<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Form;
use Aap;
use Authorisation;
use PHDB;
use Rest;
use Yii;
use Cms;
use MongoDate;
use MongoId;
use Organization;

class ListAapAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($slug) {
    	$controller = $this->getController();

        $BASE_URL = Yii::app()->getRequest()->getBaseUrl(true);
        $COSTUM_HOST = isset($controller->costum["host"])? $controller->costum["host"] : "";
       
        $elementConfig = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection","oceco"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));

        $COSTUM_BASE_URL = ((parse_url($BASE_URL, PHP_URL_HOST))==$COSTUM_HOST)?$BASE_URL:$BASE_URL."/costum/co/index/slug/".$elementConfig["slug"];
        if(!empty($config["_id"])){
            $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
            $elemtsIds = [];
            foreach($parentForms as $id => $val){
                if(!empty($val["parent"])){
                    $tempId = array_keys($val["parent"])[0];
                    $elemtsIds[] = new MongoId($tempId);
                }
            }
    
            
            if($slug == "coSinDni" && Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )){
                $cacs = PHDB::find(Organization::COLLECTION,array("parentId" => (string) $elementConfig["_id"]),array("name","slug","profilImageUrl","collection"));
                $elements = PHDB::find(Organization::COLLECTION,array('_id' => ['$in' => $elemtsIds]),array("name","slug","profilImageUrl"));
                foreach ($elements as $key => $value) {
                    unset($cacs[$key]);
                }
                
                $parentFormTemp = PHDB::findOne(Form::COLLECTION,array("config" => (string)$config["_id"]));
                unset($parentFormTemp["_id"]);
                $newForm = [];
                if(!empty($cacs)){
                    foreach ($cacs as $kc => $vc) {
                        $parentFormTemp["parent"] = [
                            $kc => [
                                "type" => $vc["collection"],
                                "name" => $vc["name"],
                            ]
                        ];
                        $parentFormTemp["name"] = $vc["name"];
                        $newForm[] = $parentFormTemp;
                    }
                    
                    Yii::app()->mongodb->selectCollection(Form::COLLECTION)->batchInsert($newForm);
                    $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
                    $elemtsIds = [];
                    foreach($parentForms as $id => $val){
                        if(!empty($val["parent"])){
                            $tempId = array_keys($val["parent"])[0];
                            $elemtsIds[] = new MongoId($tempId);
                        }
                    }
                }
            }
            
            $elements = PHDB::find(Organization::COLLECTION,array('_id' => ['$in' => $elemtsIds]),array("name","slug","profilImageUrl"));
    
            return $controller->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.whoUseTemplate",array(
                "parentForms" => $parentForms,
                "elements" => $elements,
                "costumUrl" => $COSTUM_BASE_URL,
                "elementConfig" => $elementConfig,
                "config" => $config,
                "cacs" => isset($cacs) ? $cacs : null
            ));
        }else{
            return Yii::t("common","No data");
        }
    }
}