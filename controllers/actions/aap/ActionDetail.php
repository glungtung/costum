<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use PHDB;
use Action;
use MongoId;
use Rest;
use Person;
use Document;
use Yii;

class ActionDetail extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $actionIds = !empty($_POST["ids"]) ? $_POST["ids"] : [];
        $controller = $this->getController();
        $othersIds = $actionIds;
        $actionIds = array_map(function($val){
			return new MongoId($val);
		},$actionIds);
        $arrActions = [];
        if(!empty($_POST["userId"])){
            $arrActions = PHDB::find(Action::COLLECTION,array( 
                '$or' =>array(
                    ['_id' => array('$in' => $actionIds)],
                    ["parentId" => array('$in' => $othersIds) ]
                ),
                'links.contributors.'.$_POST["userId"] =>  ['$exists' => true]
            ));
        }else{
            $arrActions = PHDB::find(Action::COLLECTION,array( '$or' =>array(
                    ['_id' => array('$in' => $actionIds)],
                    ["parentId" => array('$in' => $othersIds) ]
                ) 
            ));
        }

        $projectIds = [];
        $userIds= [];
        $answerIds = [];
        $mediaImageIds = [];

        foreach ($arrActions as $key => $value) {
            $value["parentId"] = isset($value["parentId"]['$id']) ? $value["parentId"]['$id'] : $value["parentId"];
            /*if(!in_array(new MongoId($value["parentId"]),$projectIds))
                $projectIds[] = new MongoId($value["parentId"]);*/

            if(isset($value["links"]["contributors"])){
                foreach ($value["links"]["contributors"] as $kContrib => $vContrib) {
                    if(!in_array(new MongoId($kContrib),$userIds))
                        $userIds[] = new MongoId($kContrib);
                }
            }
            
            if(!in_array(new MongoId($value["idUserAuthor"]),$userIds))
                $userIds[] = new MongoId($value["idUserAuthor"]);
            /*if(isset($value["answerId"]) && !in_array(new MongoId($value["answerId"]),$answerIds))
                $answerIds[] = $value["answerId"];*/
            if(!empty($value["media"]["images"])){
                foreach ($value["media"]["images"] as $ki => $vi) {
                    $mediaImageIds[] = new MongoId($vi);
                }
            }
            if(isset($value["tasks"])){
                foreach ($value["tasks"] as $kTask => $vTask) {
                    if(!empty($vTask["contributors"]))
                    foreach ($vTask["contributors"] as $kTaskContrib => $vTaskContrib) {
                        if(!in_array(new MongoId($kTaskContrib),$userIds))
                            $userIds[] = new MongoId($kTaskContrib);
                    }
                }
            }
        }

        //$projects = PHDB::find(Project::COLLECTION,array('_id' => ['$in' => $projectIds]),array("name"));
        $users = PHDB::find(Person::COLLECTION,array('_id' => ['$in' => $userIds]),array("profilThumbImageUrl","name"));
        $mediaImages = PHDB::find(Document::COLLECTION,array("_id" => ['$in' => $mediaImageIds]));
        //$answers = PHDB::find(Form::ANSWER_COLLECTION,array('_id' => ['$in' => $answerIds]),array("answers"));

        foreach ($arrActions as $key => $value) {
            $value["parentId"] = isset($value["parentId"]['$id']) ? $value["parentId"]['$id'] : $value["parentId"];
            $arrActions[$key]["parentId"] = $value["parentId"];
            //$arrActions[$key]["parent"] = $projects[$value["parentId"]];
            $arrActions[$key]["userAuthor"] = $users[$value["idUserAuthor"]] ;
            /*if(isset($value["answerId"]))
                $arrActions[$key]["ans"] = $answers[$value["answerId"]] ;*/
            if(!empty($value["media"]["images"])){
                //var_dump($value["media"]["images"]);
                $arrActions[$key]["images"] = [];
                foreach($value["media"]["images"] as $kimg => $vimg){
                    if(!empty($mediaImages[$vimg]))
                        $arrActions[$key]["images"][$vimg] = Document::getDocumentPath($mediaImages[$vimg], true);
                }
            }
            if(isset($value["links"]["contributors"])){
                foreach ($value["links"]["contributors"] as $kContrib => $vContrib) {
                    if(!empty($users[$kContrib]["profilThumbImageUrl"]))
                        $arrActions[$key]["links"]["contributors"][$kContrib]["profilThumbImageUrl"] = $users[$kContrib]["profilThumbImageUrl"];
                    else
                        $arrActions[$key]["links"]["contributors"][$kContrib]["profilThumbImageUrl"] = Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg";
                    if(!empty($users[$kContrib]["name"]))
                        $arrActions[$key]["links"]["contributors"][$kContrib]["name"] = $users[$kContrib]["name"];
                }
            }
            if(isset($value["tasks"])){
                foreach ($value["tasks"] as $kTask => $vTask) {
                    if(!empty($vTask["contributors"]))
                    foreach ($vTask["contributors"] as $kTaskContrib => $vTaskContrib) {
                        $arrActions[$key]["tasks"][$kTask]["contributors"][$kTaskContrib]["data"] = $users[$kTaskContrib] ; 
                    }
                }
            }
        }

        return Rest::json(array("results" => $arrActions, "count" => count($arrActions)));
    }
}