<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Answer;
use Authorisation;
use CAction;
use Form;
use Rest;
use type;
use Yii;
use Aap;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class DirectoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($source=null, $form=null){
		$controller = $this->getController();
		$res=array();
		$searchParams = $_POST;
		$answers= Aap::globalAutocomplete($form, $searchParams);

		if(isset($answers["count"]))
			$res["count"]=$answers["count"];
		$res["results"] = $answers["results"];
		return Rest::json( $res );
	}
}
