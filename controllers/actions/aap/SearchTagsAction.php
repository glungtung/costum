<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Form;
use MongoId;
use MongoRegex;
use PHDB;
use Rest;
use SearchNew;

class SearchTagsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($q,$formId,$key="tags") {
        $controller=$this->getController();
        $regex = SearchNew::accentToRegex($q);
        $params= array(
            $key => new MongoRegex("/".$regex."/i"),
            "_id" => new MongoId($formId),
        );
        $tagsList = PHDB::findOne(Form::COLLECTION, $params,array($key));
        
        $temp = $tagsList;
        foreach(explode('.',$key) as $key) {
            $temp = &$temp[$key];
        }
        $tagsList = $temp; unset($temp);

        $tags = [];

        foreach ($tagsList as $k => $v) {
            if(Api::stringStartsWith(strtolower($v),strtolower($q))){
                if(!in_array([$key=>$v],$tags))
                    $tags[] = array("tag"=>$v);
            }
        }
        return Rest::json($tags);
    }
}