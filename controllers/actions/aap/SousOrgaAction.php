<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;
use Form;
use Aap;
use Authorisation;
use PHDB;
use Rest;
use Yii;
use Cms;
use MongoDate;
use MongoId;
use Organization;
use Slug;
use SearchNew;
use Document;
use MongoRegex;
use Zone;

class SousOrgaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($action,$slug=null) {
    	$controller = $this->getController();
        $params= $_POST;
        $params["controller"] = $controller;

        //$action = strtolower($action);
        return self::$action($params,$slug);
    }

    private function add($params,$slug){
        $elementConfig = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug));
        $newElement = $elementConfig;
        $newElement["name"] = $params["name"];
        $newElement["shortDescription"] = $params["shortDescription"];
        $newElement["parentId"] = (string)$elementConfig["_id"];
        $newElement["parentType"] = $elementConfig["collection"];
        $newElement["parent"] = [
            (string)$elementConfig["_id"] => [
                "type" => "organizations"
            ]
        ];
        $newElement["slug"] = Slug::checkAndCreateSlug($newElement["name"]);
        $newElement["updated"] = time();
        $newElement["created"] = time();
        $newElement["creator"] = Yii::app()->session["userId"];
        unset($newElement["_id"]);
        Yii::app()->mongodb->selectCollection(Organization::COLLECTION)->insert($newElement);
        $newSlug = Slug::checkAndCreateSlug($params["name"],$newElement["collection"],(string)$newElement["_id"]);
	    Slug::save($newElement["collection"],(string)$newElement["_id"],$newSlug);

        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent"));
        
        $formParent = PHDB::findOne(Form::COLLECTION,array(
                "parent.".(string)$elementConfig["_id"] => ['$exists'=>true],
                "type" => "aap",
                "config"=>(string)$config["_id"]
            )
        );
        $newFormParent = $formParent;
        $newFormParent["name"] = $params["name"];
        $newFormParent["shortDescription"] = "Création et vie du CAC ".$params["name"];
        $newFormParent["parent"] = [
            (string)$newElement["_id"] => [
                "type" => $newElement["collection"],
                "name" => $newElement["name"]
            ]
        ];
        $newFormParent["updated"] = time();
        $newFormParent["created"] = time();
        $newFormParent["creator"] = Yii::app()->session["userId"];
        unset($newFormParent["_id"]);
        Yii::app()->mongodb->selectCollection(Form::COLLECTION)->insert($newFormParent);

        $formParentInputs = PHDB::find(Form::INPUTS_COLLECTION,array("formParent" => (string)$formParent["_id"]));
        foreach ($formParentInputs as $key => $value) {
            unset($formParentInputs[$key]["_id"]);
            $formParentInputs[$key]['formParent'] = (string)$newFormParent["_id"];
        }
        Yii::app()->mongodb->selectCollection(Form::INPUTS_COLLECTION)->batchInsert($formParentInputs);
        return Rest::json(array("result" => true,"msg" => Yii::t("common","Created Successfully")));
    }

    private function globalautocompletequartier($params,$slug){
        $query = array();
        $params["isZones"] = true;
        $answers = self::globalautocompleteaction($params,$slug);

        $quarties = [];
        $counterActions = [];
        $counterCacs = [];
        foreach ($answers["results"] as $key => $value) {
            if(!empty($value["answers"]["aapStep1"]["quartiers"])){
                foreach ($value["answers"]["aapStep1"]["quartiers"] as $qt) {
                    if(!isset($counterActions[$qt])){
                        $counterActions[$qt] = [];
                        $counterActions[$qt][] = $key;
                    }else{
                        if(!in_array($key,$counterActions[$qt]))
                            $counterActions[$qt][] = $key;
                    }

                    $contextkey = array_keys($value["context"])[0];
                    //var_dump($contextkey);
                    if(!isset($counterCacs[$qt])){
                        $counterCacs[$qt]=[];
                        $counterCacs[$qt][] = $contextkey;
                    }else{
                        if(!in_array($contextkey,$counterCacs[$qt]))
                            $counterCacs[$qt][] = $contextkey;
                    }
                                        
                }
                $quarties = array_merge($quarties,$value["answers"]["aapStep1"]["quartiers"]);
            }
        } 
        //var_dump($counterCacs);exit;
        $quarties = array_unique($quarties);
        $quarties = array_values($quarties);
        $quarties = array_map(function($val){
            return new MongoId($val);
        },$quarties);

        $query = SearchNew::addQuery($query,array("_id" => ['$in' => $quarties]));
        if(!empty($params["name"])){
            $q = array("name" => new MongoRegex("/.*{$params["name"]}.*/i"));
            $query = SearchNew::addQuery( $query ,$q);
        }

        if(!empty($quarties)){
            //$quarties = PHDB::find(Zone::COLLECTION,$query);
            $quarties = PHDB::findAndFieldsAndSortAndLimitAndIndex(Zone::COLLECTION,$query,array("name"),array("updated" => -1), @$params["indexStep"], @$params["indexMin"]);
            $quariesCount = PHDB::count(Zone::COLLECTION,$query);
            foreach ($quarties as $k => $v) {
                $quarties[$k]["actions"] = $counterActions[$k];
                $quarties[$k]["cacs"] = $counterCacs[$k];
            }
        }
        //***************get actions by quartier*/

        return Rest::json(array(
            "results" => $quarties,
            "count" => [
                "answers" => $quariesCount
            ]
        ));
    }

    private function listSousOrga($params,$slug){
        $BASE_URL = Yii::app()->getRequest()->getBaseUrl(true);
        $COSTUM_HOST = isset($params["controller"]->costum["host"])? $params["controller"]->costum["host"] : "";
        $elemtsIds = [];
        $query = [];
        $elementConfig = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));

        $COSTUM_BASE_URL = ((parse_url($BASE_URL, PHP_URL_HOST))==$COSTUM_HOST)?$BASE_URL:$BASE_URL."/costum/co/index/slug/".$elementConfig["slug"];

        $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
        
        
        if(!empty($params["filters"]["idcacs"])){
            $idCacs = explode(",",$params["filters"]["idcacs"]);
            $idCacs = array_map(function($val){
                return new MongoId($val);
            },$idCacs);
            $elemtsIds = $idCacs;
        }else{
            foreach($parentForms as $id => $val){
                if(!empty($val["parent"])){
                    $tempId = array_keys($val["parent"])[0];
                    $elemtsIds[] = new MongoId($tempId);
                }
            }
        }

        /*if($slug == "coSinDni" && Authorisation::isElementAdmin( (string)$elementConfig["_id"], $elementConfig["collection"],Yii::app()->session["userId"] )){
            $cacs = PHDB::find(Organization::COLLECTION,array("parentId" => (string) $elementConfig["_id"]),array("name","slug","profilImageUrl","collection"));
            $elements = PHDB::find(Organization::COLLECTION,array('_id' => ['$in' => $elemtsIds]),array("name","slug","profilImageUrl"));
            foreach ($elements as $key => $value) {
                unset($cacs[$key]);
            }
            
            $parentFormTemp = PHDB::findOne(Form::COLLECTION,array("config" => (string)$config["_id"]));
            unset($parentFormTemp["_id"]);
            $newForm = [];
            if(!empty($cacs)){
                foreach ($cacs as $kc => $vc) {
                    $parentFormTemp["parent"] = [
                        $kc => [
                            "type" => $vc["collection"],
                            "name" => $vc["name"],
                        ]
                    ];
                    $parentFormTemp["name"] = $vc["name"];
                    $newForm[] = $parentFormTemp;
                }
                
                Yii::app()->mongodb->selectCollection(Form::COLLECTION)->batchInsert($newForm);
                $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
                $elemtsIds = [];
                foreach($parentForms as $id => $val){
                    if(!empty($val["parent"])){
                        $tempId = array_keys($val["parent"])[0];
                        $elemtsIds[] = new MongoId($tempId);
                    }
                }
            }
        }*/

        if(!empty($params["filters"]["context"])){

        }else{
            $q = array('_id' => ['$in' => $elemtsIds]);
            $query = SearchNew::addQuery( $query ,$q);
        }

        if(!empty($params["name"]))
            $q = array("name" => new MongoRegex("/.*{$params["name"]}.*/i"));
            $query = SearchNew::addQuery( $query ,$q);
            $elements = PHDB::findAndFieldsAndSortAndLimitAndIndex(Organization::COLLECTION,$query,array("name","slug","profilImageUrl","collection","shortDescription"),array("updated" => -1), $params["indexStep"], @$params["indexMin"]);
            $countElements = PHDB::count(Organization::COLLECTION,$query);
        foreach($parentForms as $id => $val){
            if(!empty($val["parent"])){
                $tempId = array_keys($val["parent"])[0];
                if(!empty($elements[$tempId])){
                    $elements[$tempId]["aapForm"] =  $id;
                }
            }
        }

        return Rest::json(
            array(
            "result" => true,
            /*"data" => [
                "parentForms" => $parentForms,
                "elements" => $elements,
                "costumUrl" => $COSTUM_BASE_URL,
                "elementConfig" => $elementConfig,
                "config" => $config,
                "cacs" => $cacs
            ],*/
            "results" => $elements,
            "count" => ["organizations" => $countElements]
        ));
    }

    private function getParentForms($slug){
        $elementConfig = PHDB::findOne(Organization::COLLECTION,array("slug" => $slug),array("name","slug","collection"));
        $config = PHDB::findOne(Form::COLLECTION,array("parent.".(string)$elementConfig["_id"] => ['$exists'=>true],"type" => "aapConfig"),array("parent","hiddenAap"));
        $parentForms  = PHDB::find(Form::COLLECTION,array("config" => (string)$config["_id"]),array("parent","name"));
        return $parentForms;
    }

    private function idsToMongoIds($arrayIds){
        $ids = [];
        foreach ($arrayIds as $key => $value) {
            $ids[] = new MongoId($value);
        }
        return $ids;
    }

    private function globalautocompleteaction($p,$slug){
        unset($p["controller"]);
        $p["searchType"] = $p["searchType"][0];
        $parentForms =  self::getParentForms($slug);
        $parentFormsIds = array_keys($parentForms);

        $query = array();
        $query = array("form"=> ['$in' =>$parentFormsIds],"answers.aapStep1.titre" => ['$exists' => true]);

        if(!empty($p["filters"]["context"])){
            $q = array('$or' => []);
            foreach ($p["filters"]["context"] as $kc => $vc) {
                $q['$or'][] = array("context.".$vc => ['$exists'=>true]);
            }
            $query = SearchNew::addQuery($query,$q);
        }
        if(!empty($p["filters"]["quartier"])){
            $q = array('$or' => []);
            foreach ($p["filters"]["quartier"] as $kc => $vc) {
                $q['$or'][] = array("answers.aapStep1.quartiers" => ['$in'=> $p["filters"]["quartier"]]);
            }
            $query = SearchNew::addQuery($query,$q);
        }
        //search by name
        if(!isset($p["isZones"])){ 
			if(!empty($p["name"]) && !empty($p["textPath"]))
				$query = SearchNew::searchText($p["name"], $query, array("textPath"=>$p["textPath"]));
            $res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($p["searchType"],$query,array(),array("updated" => -1), $p["indexStep"], $p["indexMin"]);
		}else{
            $res["results"] = PHDB::find($p["searchType"],$query);
        }
        $res["query"] = $query;
        $res["params"] = $p;

        
        $res["results"] = self::prepareGlobalautocompleteActionResult($res["results"]);
        $res["count"][$p["searchType"]] = PHDB::count($p["searchType"], $query);
        return Rest::json( $res );
    }

    private function prepareGlobalautocompleteActionResult($result){
        $allImages = [
			"ansId" => [],
			"data" => []
		];

        $allContexts = [
			"contextIds" => [],
			"data" => []
		];

        foreach ($result as $key => $value) {
            $allImages["ansId"][] = $key;
            $allContexts["contextIds"][] = new MongoId(array_keys($value["context"])[0]);
        }

        $allImages["data"] = Document::getListDocumentsWhere(array(
			"id"=> ['$in'=>$allImages["ansId"]],  
			"type"=> Form::ANSWER_COLLECTION,
			"subKey"=>"aapStep1.image"), "file");

        $allContexts["data"] = PHDB::find(Organization::COLLECTION,array("_id" => ['$in' => $allContexts["contextIds"]]),array("name","slug"));

        foreach ($allImages["data"] as $kimg => $img) {
            if(!isset($result[$img["id"]]["answers"]["aapStep1"]["images"]))
                $result[$img["id"]]["answers"]["aapStep1"]["images"] = [];
            if(Document::urlExists(Yii::app()->getRequest()->getBaseUrl(true).$img["docPath"]))
                $result[$img["id"]]["answers"]["aapStep1"]["images"][] = $img["docPath"];
        }

        foreach($result as $kr => $vr){
            $cid =  array_keys($result[$kr]["context"])[0];
            //var_dump(@$allContexts["data"]);
            $result[$kr]["context"][$cid]["slug"] = $allContexts["data"][$cid]["slug"];
        }
        return $result;
    }

    private function openCacsListByQuartier($params){
        return $params["controller"]->renderPartial("costum.views.tpls.blockCms.projects.specifics.cacsList",array(
            "idquartier" => $params["idquartier"],
            "idcacs" => $params["idcacs"],
            "quartiername" => $params["quartiername"]
        ), true);
    }
    private function openActionListByCacs($params){
        return $params["controller"]->renderPartial("costum.views.tpls.blockCms.projects.specifics.cacsAction",array(
            /*"idquartier" => $params["idquartier"],
            "quartiername" => $params["quartiername"],*/
            "idcacs" => $params["idcacs"],
            "cacsname" => $params["cacsname"],
        ), true);
    }
}