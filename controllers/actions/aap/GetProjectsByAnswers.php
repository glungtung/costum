<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use PHDB;
use MongoId;
use Rest;
use Project;
use Form;
use Room;
use Action;

class GetProjectsByAnswers extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $answerIds = $_POST["answerIds"];
        $elementId = $_POST["elt"];
        $answerIds = array_map(function($val){
            return new MongoId($val);
        },$answerIds);
        $where = [];
        if(!empty($answerIds)) {
            $where['_id'] = ['$in' => $answerIds];
        }
        $answers = PHDB::find(Form::ANSWER_COLLECTION,$where,array("project"));
        
        $projectId1 = [];
        $projectId2 = [];
        foreach ($answers as $key => $value) {
            if(empty($value['project'])) continue;
            $projectId1[] = new MongoId($value["project"]["id"]);
            $projectId2[] = $value["project"]["id"];
        }

        $projects = PHDB::find(Project::COLLECTION,array(
            '$or' => [
                array("_id" => ['$in' => $projectId1 ]),
                array("parent.".$elementId => ['$exists' => true])
            ]
        ),array("name","slug"));
        $actions = PHDB::findAndSort(Action::COLLECTION,array("parentId" => ['$in' => array_keys($projects)]),array("updated" => -1));
        $projectTemps = [];
        foreach ($projects as $kpro => $vpro) {
            $arrUpdated = [];
            foreach($actions as $kact => $vact){
                if($vact["parentId"] == $kpro && !empty($vact["updated"])){
                    $arrUpdated[] = (gettype($vact["updated"]) == "object" && !empty($vact["updated"]->sec)) ? $vact["updated"]->sec : $vact["updated"];  
                }
            }
            $projects[$kpro]["actionUpdated"] = max($arrUpdated);
            $projects[$kpro]["name"] = ucfirst($vpro["name"]);
        }
        $updated = array_column($projects, 'actionUpdated');
        array_multisort($updated, SORT_DESC, $projects);
        //var_dump($projects);
        foreach ($answers as $kans => $vans) {
            if(empty($vans['project'])) continue;
            $projects[$vans["project"]["id"]]["answerId"] = $kans;
        }

        $rooms = PHDB::find(Room::COLLECTION,array("parentId" => ['$in' => $projectId2 ]));
        foreach ($rooms as $kroom => $vroom) {
            $projects[$vroom["parentId"]]["roomId"] = $kroom;
        }


        return Rest::json($projects);
    }
}