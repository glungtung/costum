<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Answer;
use Form;
use Pdf;
use PHDB;
use Yii;
use MongoId;
use Document;
use ExportToWord;
use Element;

class ExportpdfAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($formid,$answerid=null){
        $controller = $this->getController();
        $parentForm = PHDB::findOneById(Form::COLLECTION, $formid);
        if($answerid == null){
            $answers = PHDB::find(Answer::COLLECTION, ["form" => $formid, "answers.aapStep1.titre" => ['$exists' => true]], ["answers" , "status"]);
        }else{
            $answers = PHDB::findOne(Answer::COLLECTION, ["_id" => new MongoId($answerid)]);
        }        
        $user_session_id = strval(Yii::app()->session['userId']);
        $totalfi = [];
        $totalde = [];
        $prices  = [];
        if (!empty($answers["answers"]["aapStep1"]["depense"])){            
            $icont = 1;
            foreach ($answers["answers"]["aapStep1"]["depense"] as $depId => $dep){
                if (!isset($dep["price"]) || $dep["price"] == "" || $dep["price"] == null){
                    $prices[$depId]  = 0;
                }
                if (isset($dep["price"]) && is_numeric($dep["price"])){
                    $totalfi[$depId] = 0;
                    $totalde[$depId] = 0;
                    if (!empty($dep["financer"])){
                        foreach ($dep["financer"] as $fiid => $fi){
                            if (isset($fi["amount"]))
                            {
                                $totalfi[$depId] += (int)$fi["amount"];
                            }
                        }

                    }

                    if (!empty($dep["payement"])){
                        foreach ($dep["payement"] as $deid => $de){
                            if (isset($de["amount"]))
                            {
                                $totalde[$depId] += (int)$de["amount"];
                            }
                        }
                    }
                    $percent[$depId] = round(($totalfi[$depId] * 100) / (int)$dep["price"]);
                    $depensed[$depId] = round(($totalde[$depId] * 100) / (int)$dep["price"]);
                }
                else{
                    $percent[$depId] = 0;
                    $depensed[$depId] = 0;
                }
                if ( isset($dep["price"]) &&  !empty($dep["price"] || $dep["price"] == 0) && is_numeric($totalfi)){
                    $totalfi[$depId] = rtrim(rtrim(number_format($totalfi, 3, ".", " ") , '0') , '.');
                }
                if (isset($dep["price"]) && !empty($dep["price"] || $dep["price"] == 0) && is_numeric($dep["price"])){
                    $prices[$depId] = rtrim(rtrim(number_format($dep["price"], 3, ".", " ") , '0') , '.');
                }
                if (isset($dep["price"]) && !empty($dep["price"] || $dep["price"] == 0) && is_numeric($totalde)){
                    $totalde[$depId] =  rtrim(rtrim(number_format($totalde[$depId], 3, ".", " ") , '0') , '.');
                }
            }
           
        }

        if (!empty($parentForm["type"]) && $parentForm["type"] == "aap" ){   
            $inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent" => (string)$parentForm["_id"],"step" => "aapStep1"));
            $configEl = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
            
            $formsubForms= PHDB::find(Form::INPUTS_COLLECTION, array("formParent" => (string)$parentForm["_id"] ));
            $predefinedCriteria = isset($parentForm["params"]["configSelectionCriteria"]) ? $parentForm["params"]["configSelectionCriteria"] : array();
            $voteType = "starCriterionBased"; if(!empty($predefinedCriteria["type"])) $voteType = $predefinedCriteria["type"];
            $noteMax = !empty($predefinedCriteria["noteMax"]) ? $predefinedCriteria["noteMax"] : 10;
            if($voteType == "starCriterionBased") $noteMax = 5;
            
            $parsedPredefinedCriteria = [];
        
            /**for default criteria ************************************/
            if(!empty($predefinedCriteria["criterions"])){
                foreach ($predefinedCriteria["criterions"] as $kcrit => $vcrit) {
                    if(isset($vcrit["fieldKey"]))
                        $parsedPredefinedCriteria[$vcrit["fieldKey"]] = array("coeff" => @$vcrit["coeff"] , "fieldLabel" => @$vcrit["fieldLabel"]);
                }
            }
            $acceptedPreparedData = [];
            foreach($parsedPredefinedCriteria as $kPcrit => $vPcrit){
                $valueAnswer = !empty($answers["answers"]["aapStep1"][$kPcrit]) ? $answers["answers"]["aapStep1"][$kPcrit] : "";
                if($kPcrit == "depense" || $kPcrit == "budget"){
                    $price = 0;
                    foreach ($valueAnswer as $kdepense => $vdepense) {
                        if(!empty($vdepense["price"]))
                            $price += (float) $vdepense["price"];
                    }
                    $valueAnswer = $price;
                }
                $acceptedPreparedData[$kPcrit] = [
                    "label" => $inputs["inputs"][$kPcrit]["label"],
                    "fieldLabel" => @$vPcrit["fieldLabel"], //personalized name, show "fieldLabel" if exist ,"label" if not
                    "value" => $valueAnswer,
                    "coeff" => @$vPcrit["coeff"],
                    
                ];
            }
        
        
            /**for personalized criteria ************************************/
            if(!empty($predefinedCriteria["unassociatedCriterions"])){
                foreach ($predefinedCriteria["unassociatedCriterions"] as $key => $value) {
                    if(isset($value["fieldKey"]))
                        $acceptedPreparedData[$value["fieldKey"]] = [
                            "fieldLabel" => @$value["fieldLabel"], //personalized name, show "fieldLabel" if exist ,"label" if not
                            "value" => 'Critère libre',
                            "coeff" => @$value["coeff"],
                        ];
                }
            } 
            /*** means */
            $means = Element::getByTypeAndId(Form::ANSWER_COLLECTION,$answerid);
            $sumCoeff = 0;
            $keyCoeff = [];
            foreach($predefinedCriteria as $kPc => $vPc ){
                if($kPc=="criterions" || $kPc== "unassociatedCriterions"){
                    foreach($vPc as $kc => $vv ){            
                        $keyCoeff[$vv["fieldKey"]] = $vv["coeff"];
                    }
                }
            }
            $myMean = 0;
            $allUserMean = 0;
            if(isset($means["answers"])){
                $answerMean = $means["answers"];
                if(isset($answerMean["aapStep2"]['selection'])){
                    foreach($keyCoeff as $kCoeff => $vCoeff ){
                        $sumCoeff += intval($vCoeff);
                    }
                
                    foreach($answerMean["aapStep2"]['selection'] as $kMean => $vMean){
                        if(isset($answerMean["aapStep2"]['selection'][$kMean])){
                            $current = 0;
                            foreach($answers['answers']["aapStep2"]['selection'][$kMean] as $kk => $vv){
                                if(isset($keyCoeff[$kk]))
                                    $current = $current + ($vv * $keyCoeff[$kk]);
                            }
                            $current = $current / $sumCoeff;
                            if($kMean==Yii::app()->session['userId']){
                                $myMean = number_format($current, 3, ',', ' ');
                            } 
                            $allUserMean = $allUserMean + $current;
                            }
                    }
                }
                
            }  
        }
        $paramsPdf = array( 
            "saveOption" => "D" , 
            "header" => false,
            "textShadow" => false,
            "docName" => "export_".$answers["answers"]["aapStep1"]["titre"].".pdf",
        );
        $initImage = Document::getListDocumentsWhere(
            array(
                "id"=> (String)$parentForm["_id"],
                "type"=>'form',
                "subKey"=>'imageSign',
            ), "image"
        );
        $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ; 
        $profil = Document::getListDocumentsWhere(
            array(
                "id"=> $answerid,
                "type"=>'answers',
                "subKey"=>'aapStep1.image',
            ), "image"
        );
        $imageProfil = isset($profil["0"]["imagePath"])?$profil["0"]["imagePath"]:"empty" ;
        $params = array(
            "parentForm" => $parentForm , 
            "answerid" => $answerid , 
            "user_session_id" => $user_session_id,
            "answers" => $answers,
            "noteMax" => $noteMax , 
            "voteType" => $voteType , 
            "acceptedPreparedData" => $acceptedPreparedData , 
            "predefinedCriteria" => $predefinedCriteria , 
            "myMean" => $myMean , 
            "allUserMean" => $allUserMean , 
            "totalfi" => $totalfi , 
            "price" => $prices, 
            "totalde" => $totalde ,
            "imageSign" => $latestImg,
            "imageProfil" => $imageProfil
        );
        if (!empty($answers["answers"]["aapStep1"]["depense"])){ 
            $params["depense"] = $answers["answers"]["aapStep1"]["depense"];
            $params["percent"] = $percent;
            $params["depensed"] = $depensed;
        }
        $paramsPdf["html"] = "";
        $paramsPdf["html"] .= $controller->renderPartial("costum.views.custom.appelAProjet.pdf.answer", $params, true);        
        $res=Pdf::createPdf($paramsPdf);
    }
}