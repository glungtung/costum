<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Answer;
use Form;
use Pdf;
use PHDB;
use Yii;
use MongoId;
use Document;
use ExportToWord;
use Element;
use Aap;

class AttachedFile extends \PixelHumain\PixelHumain\components\Action{
    public function run($answerid,$template){
        $controller = $this->getController();
        $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$answerid);
        $inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent"=>$answer["form"],"step" => "aapStep1"));
        $templateHtml =  Aap::templateEmail($controller)[$template]["html"];
        $templateHtml = str_replace("Télécharger le fichier PDF","",$templateHtml);
        $initImage = Document::getListDocumentsWhere(
            array(
                "id"=> $answer["form"],
                "type"=>'form',
                "subKey"=>'imageSignatureEmail',
            ), "image"
        );
        $financors = Aap::getAllFinancorsByFormId($answer["form"]);
        $answer["signature"] = !empty($initImage["0"]["imagePath"]) ? $initImage["0"]["imagePath"] :"empty" ;
        $params =[];
        $params["preview"] = false;
        $html = Aap::parseVariable($templateHtml,$answer,$inputs,$financors,$params);

        $paramsPdf = array( 
            "saveOption" => "D" , 
            "header" => false,
            "footer" => false,
            "textShadow" => false,
            "docName" => "notif_".$answerid.".pdf",
        );
        $paramsPdf["html"] =  $html;    
        return Pdf::createPdf($paramsPdf);
    }
}