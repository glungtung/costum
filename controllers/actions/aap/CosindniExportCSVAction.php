<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Answer;
use Rest;
use PHDB;
use Form;
use MongoRegex;

class CosindniExportCSVAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $post = $_POST['query'];
        $filters = $_POST["query"]['filters'];
        $query = ['form' => $filters['form'], 'answers.aapStep1.titre' => ['$exists' => true]];

        $evaluation_type = '';
        $answers = [];
        $form_database = PHDB::findOneById(Form::COLLECTION, $query['form'], ['inputConfig', 'evaluationCriteria']);
        $inputs1 = PHDB::findOne(Form::INPUTS_COLLECTION, ['formParent' => $query['form'], 'step' => 'aapStep1'], ['inputs']);
        if (!empty($form_database['inputConfig']['multiDecide'])) {
            $evaluation_type = $form_database['inputConfig']['multiDecide'];
        }
        $output = ["heads" => [], "bodies" => []];
        $head_evaluation = [];
        $head_admissibility = [];
        $optionsList = [
            "progress"          => "En cours",
            "underconstruction" => "En construction",
            "vote"              => "En evaluation",
            "finance"           => "En financement",
            "call"              => "Appel à participation",
            "newaction"         => "nouvelle proposition",
            "projectstate"      => "En projet",
            "prevalided"        => "Pré validé",
            "validproject"      => "Projet validé",
            "voted"             => "Voté",
            "finish"            => "Términé",
            "suspend"           => "Suspendu"
        ];

        $mixedInputs = [
            'tpls.forms.ocecoform.multiDecide' => [],
            'tpls.forms.ocecoform.financementFromBudget' => [],
        ];

        // search field
        if (!empty($post['textPath']) && !empty($post['text'])) {
            $text = $post['text'];
            $query[$post['textPath']] = ['$regex' => new MongoRegex("/$text/i")];
        }

        //years
        if (!empty($filters['answers.aapStep1.year'])) {
            $field = 'answers.aapStep1.year';
            if (is_array($filters[$field])) {
                $query[$field] = ['$in' => []];
                foreach ($filters[$field] as $in) {
                    $query[$field]['$in'][] = strval($in);
                }
            } else $query[$field] = strval($filters[$field]);
        }

        //status
        if (!empty($filters['status']) && empty($filters['status']['$not'])) {
            $field = 'status';
            if (is_array($filters[$field])) {
                $query[$field] = ['$in' => []];
                foreach ($filters[$field] as $in) {
                    $query[$field]['$in'][] = $in;
                }
            } else $query[$field] = $filters[$field];
        } else {
            $query['status'] = ['$not' => new MongoRegex('/finish/i')];
        }

        if (empty($query['answers.aapStep1.titre'])) {
            $query['answers.aapStep1.titre'] = ['$exists' => true];
        }

        foreach ($post['sortBy'] as $field => $value) {
            $post['sortBy'][$field] = intval($value);
        }

        if (filter_var($_POST['settings']['takePagination'], FILTER_VALIDATE_BOOLEAN))
            $answers = PHDB::findAndFieldsAndSortAndLimitAndIndex(Answer::COLLECTION, $query, ['answers', 'status'], $post['sortBy'], intval($post['indexStep']), intval($post['indexMin']));
        else $answers = PHDB::findAndSort(Answer::COLLECTION, $query, $post['sortBy'], 0, ['answers', 'status']);

        $output["heads"][] = "Status";
        $financerList = [];
        $haveBudget = false;
        $haveSubvention = false;

        foreach ($answers as $answer) {
            $bodies = [];
            $statusArray = [];

            if (!empty($answer["answers"]["aapStep1"]["depense"])) {
                foreach ($answer["answers"]["aapStep1"]["depense"] as $valuenf) {
                    if (!empty($valuenf["financer"])) {
                        foreach ($valuenf["financer"] as $valuevv) {
                            if (isset($valuevv["name"]) && !in_array($valuevv["name"], $financerList, true)) {
                                $financerList[] = $valuevv["name"];
                            }
                        }
                    }
                }
            }

            if (isset($answer["status"])) {
                foreach ($answer["status"] as $st_index => $status) {
                    if (isset($optionsList[$status])) {
                        $statusArray[] = $optionsList[$status];
                    }
                }
            }

            $bodies["Status"] = implode(' | ', $statusArray);
            foreach ($_POST["champs"] as $champ) {
                $aapStep = $champ['step'];
                $index = $champ["label"];
                if (isset($answer["answers"][$aapStep][$champ["value"]])) {
                    switch ($champ["type"]) {
                        case "tpls.forms.cplx.address":
                            $addresses = $answer["answers"][$champ['step']][$champ["value"]]['address'];
                            if ($_POST['settings']['address'] == 'multicolumn') {
                                $bodies['StreetAddress'] = $addresses['streetAddress'];
                                $bodies['postalCode'] = $addresses['postalCode'];
                                $bodies['addressLocality'] = $addresses['addressLocality'];
                                $bodies['addressCountry'] = $addresses['addressCountry'];
                            } else {
                                $bodies[$index] = implode(', ', [
                                    $addresses['streetAddress'],
                                    $addresses['postalCode'],
                                    $addresses['addressLocality'],
                                    $addresses['addressCountry']
                                ]);
                            }
                            break;
                        case "tpls.forms.cplx.checkboxNew":
                        case "tpls.forms.tagsFix":
                        case "tpls.forms.tags":
                            $bodies[$index] = implode(", ", $answer["answers"][$aapStep][$champ["value"]]);
                            break;
                        case "tpls.forms.ocecoform.budget":
                            if (isset($answer["answers"][$aapStep][$champ["value"]][1]["poste"]) && $answer["answers"][$aapStep][$champ["value"]][1]["poste"] == "<b>Subvention AAP Politique de la Ville 2022</b> <br/><small>Montant Global incluant Ville, Etat, Bailleurs </small>") {
                                $bodies["Subvention AAP Politique de la Ville"] = !empty($answer["answers"][$aapStep][$champ["value"]][1]["price"]) ? $answer["answers"][$aapStep][$champ["value"]][1]["price"] : 0;
                                $haveSubvention = true;
                            } else {
                                $haveBudget = true;
                                // restructuration du budget
                                $depenseTotal = 0;

                                foreach ($answer["answers"][$aapStep][$champ["value"]] as $depense) {
                                    if (isset($depense["price"])) {
                                        $depenseTotal += intval($depense["price"]);
                                    }
                                    $bodies[$index] = $depenseTotal;
                                }
                            }
                            break;
                        case 'tpls.forms.ocecoform.suiviFromBudget':
                            $totalDepense = 0;
                            foreach ($answer["answers"][$aapStep][$champ["value"]] as $depense) {
                                if (empty($depense["price"])) {
                                    $totalDepense += intval($depense["price"]);
                                }
                                $bodies[$index] = $totalDepense;
                            }
                            break;
                        default:
                            $bodies[$index] = $answer["answers"][$aapStep][$champ["value"]];
                            break;
                    }
                } else if ($aapStep === 'aapStep2' && !empty($evaluation_type)) {
                    switch ($evaluation_type) {
                        case 'tpls.forms.aap.selection':
                            $selections = $answer["answers"][$aapStep]['selection'] ?? [];
                            $admissibilities = $answer["answers"][$aapStep]['admissibility'] ?? [];

                            // évaluation des questionnaires dans l'étape 1
                            $voters = [];
                            $keys = [];
                            foreach ($selections as $user_id => $selection) {
                                foreach ($selection as $evaluated_field => $evaluation) {
                                    $key = !empty($inputs1['inputs'][$evaluated_field]) ? 'Critère.' . $inputs1['inputs'][$evaluated_field]['label'] : 'Critère.' . $evaluated_field;
                                    $value = intval($answer["answers"][$aapStep]['selection'][$user_id][$evaluated_field]);
                                    $bodies[$key] = !empty($bodies[$key]) ? intval($bodies[$key]) + $value : $value;
                                    $voters[$key] = !empty($voters[$key]) ? intval($voters[$key]) + 1 : 1;
                                    if (!in_array($key, $head_evaluation)) $head_evaluation[] = $key;
                                    if (!in_array($key, $keys)) $keys[] = $key;
                                }
                            }
                            foreach ($keys as $key) $bodies[$key] = $bodies[$key] . ' note' . (intval($bodies[$key]) > 1 ? 's' : '') . '/' . $voters[$key] . ' votant' . (intval($voters[$key]) > 1 ? 's' : '');

                            foreach ($admissibilities as $user_id => $admissibility) {
                                $key = 'admissibility.' . $user_id;
                                $bodies[$key] = $admissibility;
                                if (!in_array($key, $head_evaluation)) {
                                    $head_evaluation[] = $key;
                                }
                            }
                            break;
                    }
                } else if ($aapStep === 'aapStep3' && $champ['type'] === 'tpls.forms.ocecoform.financementFromBudget') {
                    $total = 0;
                    $depenses = $answer["answers"]['aapStep1']['depense'] ?? [];
                    foreach ($depenses as $depense) {
                        $finances = $depense['financer'] ?? [];
                        foreach ($finances as $finance) {
                            $newIndex = "$index : " . $finance['name'];
                            if (!empty($bodies[$newIndex])) $bodies[$newIndex] = (intval(substr($bodies[$newIndex], 0, -2)) + ($finance['amount'] ?? 0)) . ' €';
                            else $bodies[$newIndex] = ($finance['amount'] ?? 0) . ' €';
                            if (!in_array($newIndex, $mixedInputs[$champ['type']])) $mixedInputs[$champ['type']][] = $newIndex;
                            $total += intval($finance['amount'] ?? 0);
                        }
                    }
                    $newIndex = "$index : Total";
                    $bodies[$newIndex] = "$total €";
                } else {
                    $bodies[$index] = "";
                }
            }

            foreach ($financerList as $flid => $finl) {
                $bodies[$finl] = 0;
            }

            $finTotal = 0;
            if ($haveBudget) {
                if (isset($answer["answers"]["aapStep1"]["depense"])) {
                    foreach ($answer["answers"]["aapStep1"]["depense"] as $depense) {


                        if (isset($depense["financer"])) {

                            foreach ($depense["financer"] as $fid => $fin) {
                                if (isset($fin["id"])) {
                                }
                                if (isset($fin["amount"])) {
                                    foreach ($financerList as $flid => $finl) {
                                        if (!empty($fin["name"]) && $finl == $fin["name"]) {
                                            $bodies[$finl] += $fin["amount"];
                                        }
                                        $finTotal += $fin["amount"];
                                    }
                                }
                            }
                        }
                    }
                }

                $bodies["Financement total"] = $finTotal;
            }

            // enlever les lignes vides
            foreach ($bodies as $body) {
                if (!empty($body)) {
                    $output["bodies"][] = $bodies;
                    break;
                }
            }
        }

        foreach ($_POST["champs"] as $champ) {
            switch ($champ["type"]) {
                case "tpls.forms.cplx.address":
                    if ($_POST['settings']['address'] == 'multicolumn') $output["heads"] = array_merge($output["heads"], ['StreetAddress', 'postalCode', 'addressLocality', 'addressCountry']);
                    else $output["heads"][] = $champ['label'];
                    break;
                case "tpls.forms.ocecoform.multiDecide":
                    switch ($evaluation_type) {
                        case 'tpls.forms.aap.selection':
                            $output["heads"] = array_merge($output["heads"], $head_evaluation, $head_admissibility);
                            break;
                    }
                    break;
                case "tpls.forms.ocecoform.budget":
                    $output["heads"][] = $champ["label"] . " Total";
                    break;
                case 'tpls.forms.ocecoform.financementFromBudget':
                    $output['heads'] = array_merge($output['heads'], $mixedInputs[$champ['type']]);
                    $output['heads'][] = $champ['label'] . ' : Total';
                    break;
                default:
                    $output["heads"][] = $champ["label"];
                    break;
            }
        }
        if ($haveSubvention) {
            $output["heads"][] = "Subvention AAP Politique de la Ville";
        }

        if ($haveBudget) {
            foreach ($financerList as $financId => $financer) {
                $output["heads"][] = $financer;
            }
            $output["heads"][] = "Financement total";
        }

        return Rest::json($output);
    }
}
