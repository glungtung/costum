<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Answer;
use MongoRegex;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use SearchNew;

class ExportcsvfinancerAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($request = '') {
        $output = [];
        if (empty($request)) {
            $output = ["heads" => [], "bodies" => []];

            $output["heads"] = ["Nom du projet", "Subvention AAP Politique de la Ville", "Nom du financeur", "Label du fond", "Montant"];

            if (!empty($_POST["query"])) {
                $prepquery = [];
                if (!empty($_POST["query"]["filters"]["text"])) {
                    foreach ($_POST["query"]["filters"]["text"] as $qq => $querytext) {
                        if (isset($_POST["query"]["filters"][$qq])) {
                            $newquerytext = "/" . $querytext . "/";
                            array_push($newquerytext, $_POST["query"][$qq]);
                        }
                    }
                }
                foreach ($_POST["query"]["filters"] as $q => $quer) {
                    if (!empty($quer) && $q != "text") {
                        $prepquery[$q] = $quer;
                    }
                }

                if (isset($prepquery["answers.aapStep1.titre"]) && isset($prepquery["answers.aapStep1.titre"][0])) {
                    $prepquery["answers.aapStep1.titre"] = $prepquery["answers.aapStep1.titre"][0];
                }

                $answersq = SearchNew::querySearchAll(["searchType" => Answer::COLLECTION, "filters" => $prepquery, "ranges" => null, "indexStep" => 10000], ["answers", "status"]);

                if (isset($answersq["results"])) {
                    $answers = $answersq["results"];
                } else {
                    $answers = [];
                }

                foreach ($answers as $ansid => $ans) {
                    $bodies = [];
                    if (isset($ans["answers"]["aapStep1"]["depense"])) {

                        if (isset($ans["answers"]["aapStep1"]["depense"][1]["financer"])) {
                            foreach ($ans["answers"]["aapStep1"]["depense"][1]["financer"] as $keyvv => $valuevv) {
                                $bodies["Nom du projet"] = $ans["answers"]["aapStep1"]["titre"] ? $ans["answers"]["aapStep1"]["titre"] : "";

                                $bodies["Subvention AAP Politique de la Ville"] = $ans["answers"]["aapStep1"]["depense"][1]["price"] ? $ans["answers"]["aapStep1"]["depense"][1]["price"] : "0";

                                $bodies["Nom du financeur"] = $valuevv["name"] ? $valuevv["name"] : "";

                                $bodies["Label du fond"] = $valuevv["line"] ? $valuevv["line"] : "";

                                $bodies["Montant"] = $valuevv["amount"] ? $valuevv["amount"] : "0";

                                foreach ($bodies as $body) {
                                    if (!empty($body)) {
                                        $output["bodies"][] = $bodies;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            switch ($request) {
                case 'aap':
                    $filters = $_POST['filters'] ?? [];
                    $where = $filters['filters'] ?? [];
                    $fields = $filters['fields'] ?? [];
                    $fields[] = 'notifications';

                    // Vérifier et normaliser les filtres
                    if (!empty($where) && !empty($where['status']) && !empty($where['status']['$not'])) {
                        $where['status']['$ne'] = $where['status']['$not'];
                        unset($where['status']['$not']);
                    }
                    if (!empty($where) && !empty($where['answers.aapStep1.year'])) {
                        if (is_array($where['answers.aapStep1.year'])) {
                            $where['answers.aapStep1.year'] = ['$in' => $where['answers.aapStep1.year']];
                        }
                    }
                    if (!empty($filters['text']) && !empty($filters['textPath'])) {
                        $where[$filters['textPath']] = UtilsHelper::mongo_regex($filters['text']);
                    }
                    $where = UtilsHelper::parse_values($where, ['answers.aapStep1.year']);

                    // Vérifier les champs à exporter
                    if (!in_array('status', $fields)) $fields[] = 'status';

                    $year = !empty($where['answers.aapStep1.year']) ?
                        (!empty($where['answers.aapStep1.year']['$in']) ? 
                            implode(', ', $where['answers.aapStep1.year']['$in']) :
                            $where['answers.aapStep1.year']) : 'Toutes les années confondues';
                    $output = [
                        'headers' => [
                            'Status',
                            'Nom de l\'association',
                            'Intitulé du projet',
                            'Thématique - Lien avec les directions internes',
                            'Axes AAP ' . $year . ' Impact / Objectif',
                            'Quartier',
                            'Régle 1€ Etat > 1€ Ville',
                        ],
                        'bodies' => []
                    ];
                    $bailleurs = [];

                    $db_answers = \PHDB::find(Answer::COLLECTION, $where, $fields);
                    foreach ($db_answers as $db_answer) {
                        $answer = $db_answer['answers']['aapStep1'];
                        $body = [
                            'Status' => implode(",\n", array_map(fn ($__status) => \Yii::t('common', $__status), $db_answer['status'] ?? [])),
                            'Nom de l\'association' => $answer['association'] ?? '',
                            'Intitulé du projet' => $answer['titre'] ?? '',
                            'Thématique - Lien avec les directions internes' => implode(",\n", $answer['tags'] ?? []),
                            'Axes AAP ' . $year . ' Impact / Objectif' => $answer['axesTFPB'] ?? '',
                            'Quartier' => implode(",\n", $answer['interventionArea'] ?? [])
                        ];

                        $sous_total_bailleurs = 0;
                        if (!empty($answer['depense'])) {
                            foreach ($answer['depense'] as $depense) {
                                if (!empty($depense['financer'])) {
                                    foreach ($depense['financer'] as $financer) {
                                        if (empty($financer['name'])) continue;

                                        // Ajouter dans la liste des colonnes
                                        if (!in_array($financer['name'], $bailleurs)) $bailleurs[] = $financer['name'];
                                        // Incrémenter les valeurs dans les cellules existantes
                                        if (!empty($body[$financer['name']])) $body[$financer['name']] += intval($financer['amount']);
                                        else $body[$financer['name']] = intval($financer['amount']);

                                        $sous_total_bailleurs += intval($financer['amount']);
                                    }
                                }
                            }
                        }

                        $body['Sous-Total bailleurs'] = $sous_total_bailleurs . ' €';

                        $notifications = [];
                        if (!empty($db_answer['notifications'])) {
                            foreach ($db_answer['notifications'] as $notification) {
                                if (!empty($notification['status']) && !empty($notification['status']['sendDate'])) {
                                    $notifications[] = date('d.m.y', UtilsHelper::parse_to_timestamp(['sendDate'], $notification['status']));
                                }
                            }
                        }
                        $body['Envoi des notifs via Smarter'] = implode(",\n", $notifications);
                        $output['bodies'][] = $body;
                    }

                    // Compléter les colonnes des bailleurs
                    foreach ($output['bodies'] as $body_index => $body_value) {
                        foreach ($bailleurs as $bailleur) {
                            if (empty($body_value[$bailleur])) $body_value[$bailleur] = '0 €';
                            else $body_value[$bailleur] .= ' €';
                        }
                        $output['bodies'][$body_index] = $body_value;
                    }

                    // post traitements de la sortie
                    $output['headers'] = [...$output['headers'], ...$bailleurs];
                    $output['headers'] = [...$output['headers'], ...[
                        'Sous-Total bailleurs',
                        'Subvention AAP Politique de la Ville',
                        'Date de validation Comité des financeurs',
                        'Date de validation en CM',
                        'Envoi des notifs via Smarter'
                    ]];
                    break;
                default:
                    $output = [
                        'status' => false,
                        'msg' => 'Bad request'
                    ];
                    break;
            }
        }
        return Rest::json($output);
    }
}
