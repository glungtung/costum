<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Answer;
use Form;
use Pdf;
use PHDB;
use Yii;
use MongoId;
use Document;
use ExportToWord;
use Element;
use Aap;
use MongoDate;

class NotificationStatusAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($answerid,$notificationid=null){
        $controller = $this->getController();
        $params=[];
        $params['$addToSet'] = ["status"=>"notified"];
        if(!empty($notificationid)){
            $params['$set'] = [
                "notifications.".$notificationid.".status.seen" =>true,
                "notifications.".$notificationid.".status.seenDate" => time(),
                "statusUpdated" => time()
            ];
        }
        PHDB::update(Form::ANSWER_COLLECTION,
            array("_id" => new MongoId($answerid)),$params);
        
        return  Yii::app()->getRequest()->getBaseUrl(true)."/".Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg";
    }
}