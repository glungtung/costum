<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cordCollaborativeOpenRd;
use CAction;
use Ctenat;
use Form;
use MongoId;
use PHDB;
use Yii;

class DashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($sk=null,$answer=null)
    {
    	$controller = $this->getController();


    	$tpl = "costum.views.custom.process.dashboard";

    	$answers = PHDB::find( Form::ANSWER_COLLECTION,[ //"formId"     => $params["formId"],
                                              			 "parentSlug" => $controller->costum["contextSlug"] ] );

    	$title = "Observatoire des process Opal";
    	$blocks = [];

    	if(isset($answer)){
    		$answer = PHDB::findOne( Form::ANSWER_COLLECTION,[ "_id" => new MongoId( $answer ) ] );
    		$title = (isset($answer["answers"]["opalProcess1"]["titre"])) ? "Observatoire<br/>".$answer["answers"]["opalProcess1"]["titre"] : "Observatoire spécifique";
    		$title = "<a href='".Yii::app()->createUrl("/costum")."/co/index/slug/".$answer["parentSlug"]."/answer/".$answer["_id"]."'>".$title."</a>";
    		$tasks = [
    			"todo" => 0,
    			"done" => 0,
    			"tasksPerson" => [],
    			"allTasksPerson" => []
    		];
    		$finance = [
    			"total" => 0,
    			"financed" => [
    				"total" => 0,
    				"byAmount" => [],
    				"byName" => [],
    				"colors" => []
    			],
    			"byPerson" => [],
    			"byBlock" => [],
    			"byBlockName" => [],

    		];
    		foreach (Yii::app()->session["forms"] as $fid => $f) {
				if( isset( $answer["answers"][$fid]["depense"] ) ){
		    		foreach ( $answer["answers"][$fid]["depense"]  as $ix => $dep) {
						if( isset( $dep["price"] ) ){
							$finance["total"] += $dep["price"];
							$finance["byBlock"][] = $dep["price"];
							$finance["byBlockName"][] = $dep["poste"];
						}

						if( isset( $dep["financer"] ) ){
							foreach ($dep["financer"] as $fix => $fin) {
								if(isset($fin["amount"])){
									$finance["financed"]["total"] += $fin["amount"];
									$finance["financed"]["byAmount"][] = $fin["amount"];
									$finance["financed"]["byName"][] = ( isset($fin["name"]) ) ? $fin["name"] : "?";
								}
							}
						}

						if( isset( $dep["todo"] ) ){
							foreach ($dep["todo"] as $ixx => $t) {
								if(!isset($t["done"]) || $t["done"] == "0"){
									$tasks["todo"]++;
									$whos = (is_array($t["who"])) ? $t["who"] : explode(",",$t["who"]);
									foreach ($whos as $whoix => $who) {
										if( !isset( $tasks["tasksPerson"][ $who ] ) )
											$tasks["tasksPerson"][ $who ] = 0;
										$tasks["tasksPerson"][ $who ]++;
									}
								}
								else{
									$tasks["done"]++;
								}
								$whos = (is_array($t["who"])) ? $t["who"] : explode(",",$t["who"]);
								foreach ($whos as $whoix => $who) {
									if( !isset( $tasks["allTasksPerson"][ $who ] ) )
										$tasks["allTasksPerson"][ $who ] = 0;
									$tasks["allTasksPerson"][ $who ]++;
								}

							}
						}
					}
				}
			}
			$finance["percentFinanced"] = ( isset( $finance["financed"]["total"] ) && isset( $finance["total"] ) ) ? floor($finance["financed"]["total"] * 100 / $finance["total"] ) : 0;

			$cix = 0;
			foreach ( $finance["financed"]["byAmount"] as $ix => $who) {
				if( $cix >= count(Ctenat::$COLORS) )
					$cix = 0;
				$finance["financed"]["colors"][] = Ctenat::$COLORS[$cix];
				$cix++;
			}
			$finance["financed"]["missing"] = "";
			if( isset( $finance["financed"]["total"] ) &&
				isset( $finance["total"] ) &&
				( $finance["total"] - $finance["financed"]["total"] ) >= 0 ) {
				$finance["financed"]["missing"] = "<br/>manque <span class='text-red'>".( $finance["total"] - $finance["financed"]["total"] )." €</span>";
				$finance["financed"]["byAmount"][] = $finance["total"] - $finance["financed"]["total"];
				$finance["financed"]["byName"][] = "MISSING";
				$finance["financed"]["colors"][] = "#ff0000";
			}
			$tasks["percentDone"] = ( isset( $tasks["todo"] ) || isset( $tasks["done"] ) ) ? floor( ($tasks["todo"]) * 100 / ($tasks["todo"]+$tasks["done"]) ) : 0;
			$tasks["tasksPersonCount"] = array_values($tasks["tasksPerson"]);
			$tasks["tasksPersonLabel"] = array_keys($tasks["tasksPerson"]);
			//var_dump($tasks);exit;

    		$lists = [
				// "chiffresDepense" =>[
				// 	"title"=>"<i class='fa fa-2x fa-question-circle'></i><br/>Dépense",
				// 	"blocksize"=>"3 col-xs-6",
				// 	"bgColor" => Ctenat::$COLORS[0],
				// 	"color" => "#fff",
				// 	"data" => [
				// 		["data"=>24,"name"=>"personne impliquées","icon"=>"group","type"=>"success"],
				// 		["data"=>45,"name"=>"durée","type"=>"danger","icon"=>"clock"]
				// 	],
				// 	"tpl"  => "costum.views.tpls.list"
				// ],
				// "chiffresDecide" =>[
				// 	"title"=>"<i class='fa fa-2x fa-thumbs-up'></i><br/>Décision",
				// 	"blocksize"=>"4 col-xs-6",
				// 	"bgColor" => Ctenat::$COLORS[0],
				// 	"color" => "#fff",
				// 	"data" => [
				// 		["data"=>24,"name"=>"Pour","icon"=>"thumbs-up","type"=>"success"],
				// 		["data"=>45,"name"=>"Contre","type"=>"danger","icon"=>"thumbs-down"],
				// 		["data"=>45,"name"=>"Nombres de Décisions Validés","type"=>"danger","icon"=>"gavel"],
				// 		["data"=>60,"name"=>"Nombres de Décisions Refusés","type"=>"danger","icon"=>"hand-stop-o"]
				// 	],
				// 	"tpl"  => "costum.views.tpls.list"
				// ],
				"chiffresFin" =>[
					"title"=>"<i class='fa fa-2x fa-money'></i><br/>Finances",
					"blocksize"=>"6 col-xs-6",
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>(100-$finance["percentFinanced"])." %","name"=>"Resta à financer","type"=>"danger","icon"=>"money"],
						["data"=>$finance["total"]." €","name"=>"Total à financer","icon"=>"thumbs-up","type"=>"success"],
						["data"=>$finance["financed"]["total"]." €","name"=>"Total financé","type"=>"danger","icon"=>"map-marker"],
						["data"=>$finance["percentFinanced"]." %","name"=>"Pourcentage financé","type"=>"danger","icon"=>"money"],
						["data"=>count(array_keys($finance["financed"]["byName"])),"name"=>"Combien de financeurs","type"=>"danger","icon"=>"group"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFollow" =>[
					"title"=>"<i class='fa fa-2x fa-cogs'></i><br/>Suivi",
					"blocksize"=>"6 col-xs-6",
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>$tasks["done"],"name"=>"Nombres de Taches Cloturés","type"=>"success","icon"=>"fa-handshake-o"],
						["data"=>$tasks["todo"],"name"=>"Nombres de Taches à faire ","type"=>"danger","icon"=>"fa-handshake-o"],
						["data"=>$tasks["percentDone"]." %","name"=>"Pourcentage d'avancement","type"=>"danger","icon"=>"fa-hourglass-half"],
						["data"=>$tasks['todo']+$tasks['done'],"name"=>"nombre de tache","icon"=>"pie-chart","type"=>"success"],
						["data"=>count(array_keys($tasks["allTasksPerson"])),"name"=>"nombre d'acteur","icon"=>"group","type"=>"success"],

						//["data"=>$tasks["done"],"name"=>"Nombres de Travaux Validés","type"=>"danger","icon"=>"thumbs-up"],

					],
					"tpl"  => "costum.views.tpls.list"
				],
				"financer" =>[
					"title"=>"Financé en € ".$finance["financed"]["missing"],
					"data" => $finance["financed"]["byAmount"],
					"lbls" => $finance["financed"]["byName"],
					"colors" => $finance["financed"]["colors"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"cost" =>[
					"title"=>"Cout en € par Block ",
					"data" => $finance["byBlock"],
					"lbls" => $finance["byBlockName"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],

				"tasks" =>[
					"title"=>"Taches",
					"data" => $tasks["tasksPersonCount"],
					"lbls" => $tasks["tasksPersonLabel"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"timeline" =>[
					"col12"=>true,
					"title"=>"Point d'activité",
					"data" => [32,6,36,21,10],
					"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.line"
				],
				"activtiyType" =>[
					"title"=>"Type d'activité",
					"data" => [32,65,6,21,10],
					"lbls" => ["Dev","Graphisme","Fiancement","Dossier","Administratif"],
					"url"  => "/graph/co/dash/g/graph.views.co.bar"
				],
				"activtiyType2" =>[
					"title"=>"Type d'activité 2",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.polar"
				],
				"activtiyType3" =>[
					"title"=>"Type d'activité 3",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.radar"
				],
			];
    	} else {
	    	/*echo count($answers)." Proposals";
	    	echo "<br/> <b>Proposals per status pie</b>";
	    	echo "<br/>---- validated Proposals";
	    	echo "<br/>---- pending decision Proposals";
	    	echo "<br/>---- pending financing Proposals";
	    	echo "<br/>---- Proposals with pending intents no commited ";
	    	echo "<br/>---- Proposals pending evaluations and intent proposals ";
	    	echo "<br/>---- no participation Proposals";
	    	echo "<br/> <b>Projects per status pie</b>";
	    	echo "<br/>---- active Projects";
	    	echo "<br/>---- finished Projects";
	    	echo "<br/> <b>Tasks per status pie</b>";
	    	echo "<br/>----  for open projects Tasks distribution";
	    	echo "<br/>----  for each worker Tasks distribution per project";
	    	echo "<br/>----  xxx Tasks";
	    	echo "<br/> <b>Fianncement per type or contexte pie</b>";
	    	echo "<br/>----  for open projects Finance distribution per presta/ commons";
	    	echo "<br/>----  for open projects Finance distribution per task types";
	    	echo "<br/>----  for each worker Finances distribution per project";
	    	echo "<br/>----  xxx Tasks";*/
	    	//count

			$lists = [
				"chiffresProp" =>[
					"title"=>"<i class='fa fa-2x fa-question-circle'></i><br/>Propositions",
					"blocksize"=>"3 col-xs-6",
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"validated Proposals","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"pending decision Proposals","type"=>"danger","icon"=>"map-marker"],
						["data"=>60,"name"=>"pending financing Proposals","type"=>"danger","icon"=>"money"],
						["data"=>24,"name"=>"Proposals with pending intents no commited","icon"=>"hourglass-half"],
						["data"=>45,"name"=>"no participation Proposals","icon"=>"group"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresProj" =>[
					"title"=>"<i class='fa fa-2x fa-lightbulb-o'></i><br/>Projets",
					"blocksize"=>"3 col-xs-6",
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"active Projects","icon"=>"thumbs-up","type"=>"refresh"],
						["data"=>45,"name"=>"finished Projects","type"=>"danger","icon"=>"hourglass"],
						["data"=>45,"name"=>"pending Projects","type"=>"danger","icon"=>"pause-circle"],
						["data"=>60,"name"=>"for open projects Tasks distribution","type"=>"danger","icon"=>"cogs"],
						["data"=>24,"name"=>"for each worker Tasks distribution per project","icon"=>"pie-chart"],
						["data"=>24,"name"=>"Shitty Tasks identified","icon"=>"meh-o"],
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresFin" =>[
					"title"=>"<i class='fa fa-2x fa-money'></i><br/>Finances",
					"blocksize"=>"3 col-xs-6",
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"for open projects Finance distribution per presta/ commons","icon"=>"thumbs-up","type"=>"success"],
						["data"=>45,"name"=>"for open projects Finance distribution per task types","type"=>"danger","icon"=>"map-marker"],
						["data"=>60,"name"=>"for each worker Finances distribution per project","type"=>"danger","icon"=>"money"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"chiffresUsage" =>[
					"title"=>"<i class='fa fa-2x fa-line-chart'></i><br/>Usage",
					"blocksize"=>"3 col-xs-6",
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>24,"name"=>"utilisateurs par projet","icon"=>"thumbs-up","type"=>"pie-chart"],
						["data"=>45,"name"=>"avergae user time spend","type"=>"danger","icon"=>"clock"]
					],
					"tpl"  => "costum.views.tpls.list"
				],
				"proposal" =>[
					"title"=>"Propositions",
					"data" => [32,65,6,21,10],
					"lbls" => ["Propositions 0","Propositions 1","Propositions 2","Propositions 3","Propositions 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.bar"
				],
				"projects" =>[
					"title"=>"Projects",
					"data" => [32,65,36,1,10],
					"lbls" => ["Projects 0","Projects 1","Projects 2","Projects 3","Projects 4"],
					"url"=>"/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany"
				],
				"tasks" =>[
					"title"=>"Taches",
					"data" => [32,6,36,21,10],
					"lbls" => ["Taches 0","Taches 1","Taches 2","Taches 3","Taches 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.line"
				],
				"finance" =>[
					"title"=>"Finance",
					"data" => [3,65,36,21,10],
					"lbls" => ["Finance 0","Finance 1","Finance 2","Finance 3","Finance 4"],
					"url"  => "/graph/co/dash/g/graph.views.co.scatter"
				],
			];

		}

		foreach ($lists as $ki => $list)
		{
			$kiCount = 0;
			foreach ($list["data"] as $ix => $v) {
				if(is_numeric($v))
					$kiCount += $v;
				else
					$kiCount ++;
			}
			$blocks[$ki] = [
				"title"   => $list["title"],
				"counter" => $kiCount,
			];
			if(isset($list["tpl"])){
				$blocks[$ki] = $list;
			}
			else
				$blocks[$ki]["graph"] = [
					"url"=>$list["url"],
					"key"=>"graph".$ki,
					"data"=> [
						"datasets"=> [
							[
								"data"=> $list["data"],
								"backgroundColor"=> (isset($list["colors"])) ? $list["colors"] :Ctenat::$COLORS
							]
						],
						"labels"=> $list["lbls"]
					]
				];
		}
		$params = [
			"title" => $title,
    		"blocks" 	=> $blocks
    	];

    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);
        else {
    		$this->getController()->layout = "//layouts/empty";
    		$this->getController()->render($tpl,$params);
        }

    }
}