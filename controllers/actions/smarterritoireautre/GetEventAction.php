<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterritoireautre;
use CAction;
use Rest;
use Smarterritoireautre;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $parametres = Smarterritoireautre::getEvent($_POST["contextId"],$_POST["contextType"],$_POST["source"]);
        return Rest::json($parametres);
    }
}
