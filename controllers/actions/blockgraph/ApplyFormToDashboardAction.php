<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph;

use CAction, Form, Cms, PHDB, MongoId, Rest;

class ApplyFormToDashboardAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $contextId = $_POST["contextId"]??null;
        $page = $_POST["page"]??null;
        $coform = $_POST["coform"]??null;

        $response = array();

        //var_dump(array('page'=>$page, 'coform'=>['$exists'=>true], 'parent.'.$contextId=>['$exists' => true] ));exit();
        
        if(isset($contextId) && isset($page) && isset($coform)){
            $response = PHDB::updateWithOptions(
                Cms::COLLECTION, 
                array('page'=>$page, 'coform'=>['$exists'=>true], 'parent.'.$contextId=>['$exists' => true] ),
                array('$set' => ['coform' => $coform]),
                array('multiple' => true)
            );
        }

        return Rest::json($response);
    }
}