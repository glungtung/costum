<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ficheprojet;

use CAction, FicheProjet, Rest;
class GetAllAnswerAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = FicheProjet::getAllAnswer($_POST);
        
        return Rest::json($params);
    }
}