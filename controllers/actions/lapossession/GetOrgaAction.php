<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\lapossession;

use CAction, LaPossession, Rest;
/**
* Update an information field for a element
*/
class GetOrgaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $res = LaPossession::getOrganization($_POST["contextType"],$_POST["contextSlug"]);
		return Rest::json($res);
    }
}