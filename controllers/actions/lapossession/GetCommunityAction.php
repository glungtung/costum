<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\lapossession;
use CAction;
use LaPossession;
use Rest;

class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params = LaPossession::getCommunity($_POST["contextType"],$_POST["contextSlug"]);
        
        return Rest::json($params);
    }
}