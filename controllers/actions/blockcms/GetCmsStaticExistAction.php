<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Cms, Rest;
class GetCmsStaticExistAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cms::getCmsStaticExist($_POST);
  
        return Rest::json($params);
    }
}