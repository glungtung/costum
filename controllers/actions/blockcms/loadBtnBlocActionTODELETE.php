<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Yii;
class loadBtnBlocAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $params = [
            "canEdit" => $_POST['canEdit'],
            "kunik"   => $_POST['kunik'],
            "parentN" => $_POST['name'],
            "parentId"=> $_POST["blockParent"],
            "name"    => $_POST["name"],
            "path"    => $_POST["path"],
            "subtype" => isset($_POST["subtype"]) ?? "",
            "id"      => $_POST["blockKey"]
        ];
        $btnEdit = $controller->renderPartial("costum.views.tpls.editSuperCms",$params);
        return $btnEdit;
    }
}