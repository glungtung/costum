<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, PHDB, Person, Rest;
class GetUsersAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"],
        ];
        $result = PHDB::findAndSortAndLimitAndIndex(Person::COLLECTION,$where);
        
        return Rest::json($result);
    }
}