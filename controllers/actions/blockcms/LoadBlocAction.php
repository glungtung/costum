<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest,CacheHelper;
class LoadBlocAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        
        $block = Cms::getByIdWithChildreen($_POST["idblock"]);
        $path = $block["path"];
        $orderBlock = isset($_POST["orderBlock"])?$_POST["orderBlock"]:"";
        $pathExplode = explode('.', $block["path"]);
        $count = count($pathExplode);
        $content = isset($block['content']) ? $block['content'] : [];
        $kunik = $pathExplode[$count - 1] . $block["_id"];
        $blockKey = (string)$block["_id"];
        $blockName = (string)@$block["name"];
        $costum = CacheHelper::getCostum();
        $el = $costum;
//         $el = Element::getElementById($costum["contextId"],$costum["contextType"]);
// var_dump($el);exit();
/*        echo "<pre>";
        var_dump($costum["editMode"]);
        echo "</pre>";
*/
        $params = [
            "cmsList"   =>  [],
            "blockKey"  => $blockKey,
            "blockCms"  => $block,
            "page"      => $block['page'] ?? "",
            "canEdit"   => $costum["editMode"],
            "type"      => $path,
            "kunik"     => $kunik,
            "content"   => $content,
            "el"        => $el,
            'costum'    => $costum ,
            'blockName' => $blockName,
            "clienturi" => $_POST["clienturi"],
            // 'range'     => $i,
            "defaultImg" => Yii::app()->controller->module->assetsUrl . "/images/thumbnail-default.jpg"
        ];
        
        if (!isset($block["position"]) || $block["position"] == "undefined" || empty($block["position"])) {
            PHDB::update( "cms",  array("_id" => new MongoId($_POST["idblock"])), array('$set' => array("position" => $orderBlock) ));
        }

        $resp = array("html" => $controller->renderPartial("costum.views.".$path, $params),"params" => $params, "order" => $orderBlock);
        return Rest::json($resp);
    }
}