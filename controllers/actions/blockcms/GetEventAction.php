<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;
use CAction;
use PHDB;
use Rest;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"]
        ];
        $result = PHDB::find($_POST["collection"],$where);
        
        return Rest::json($result);
    }
}