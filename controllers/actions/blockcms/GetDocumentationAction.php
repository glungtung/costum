<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, PHDB, Cms, Rest;
class GetDocumentationAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $result = PHDB::find(Cms::COLLECTION,array("type" => "documentation"));
        
        return Rest::json($result);
        exit();
    }
}