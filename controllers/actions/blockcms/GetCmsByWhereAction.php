<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use Cms;
use Rest;

class GetCmsByWhereAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params = array();
        $params["source.key"]=$_POST["source_key"];
        $params["type"]=$_POST["type"];
       // $params["source"]["key"]
        $result = Cms::getCmsByWhere($params);
        //var_dump($result);exit;
        return Rest::json($result);
    }
}
