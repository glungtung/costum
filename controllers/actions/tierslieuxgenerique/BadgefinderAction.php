<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique;

use Yii;

class BadgefinderAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($email = false, $source) {
        $controller = $this->getController();
        return $controller->renderPartial("costum.views.custom.tiersLieuxGenerique.badge-finder", ["email" => $email, "sourceKey" => $source]);
    }
}