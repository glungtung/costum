<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique;

use Yii, Cms, PHDB, Element, Rest, CacheHelper, MongoId, Organization;

class GenerateFromTemplateAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();

        $context = PHDB::findOne(Organization::COLLECTION, array("slug"=>$_POST["slug"]), ["slug", "costum"]);
        
        if(isset($context["costum"]) && (empty($context["costum"]["loaded"]) || $context["costum"]["loaded"]==false)){
            $params = array();
            
            $params["parent.62b02eb9f7ba25171a273b34"] = array('$exists'=>true);
            $params["type"] = array('$in' => ["blockChild"]);
            
            $blocks_in_template = Cms::getCmsByWhere($params, array("position" => 1));
            
            foreach ($blocks_in_template as $bk => $blockcms) {
                unset($blocks_in_template[$bk]["_id"]);
                unset($blocks_in_template[$bk]["blockParent"]);
                
                $parentData = [];
                $parentData[$_POST["id"]] =  ["type" => $_POST["type"], "name" => $_POST["slug"]];
                
                $blocks_in_template[$bk]["page"] = $blockcms["page"];
                $blocks_in_template[$bk]["haveTpl"] = "false";
                $blocks_in_template[$bk]["creator"] = $_SESSION["userId"]??$blockcms["creator"];
                $blocks_in_template[$bk]["parent"] = $parentData;

            }
            
            PHDB::batchInsert(Cms::COLLECTION, $blocks_in_template);
            return Rest::json(array('result' => true, 'msg' => "Votre costum de Tiers lieux est généré"));
        }else{
            return Rest::json(array('result' => false, 'msg' => "C'est déjà un costum Tiers lieux"));
        }
    }
}