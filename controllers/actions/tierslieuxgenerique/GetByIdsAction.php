<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique;

use Yii, Rest, PHDB;

class GetByIdsAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {

        $ids = $_POST['ids'];
        $collection = $_POST['collection'];
        $fields = ["tags", "name"];
        if(isset($_POST["fields"])){
            $fields = $_POST["fields"];
        }

        $response = array('ok' => false );

        if(count($ids) > 0 && $collection != ""){
            $response["ok"] = true;
            $response["results"] = PHDB::findByIds($collection, $ids, $fields);
        }
        
        return Rest::json($response);
    }
}