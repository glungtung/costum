<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent;
    
    use Citoyen;
    use DateTime;
    use DateTimeZone;
    use Event;
    use MongoDate;
    use MongoId;
    use Organization;
    use PHDB;
    use PixelHumain\PixelHumain\components\Action;
    use Project;
    
    class EventAction extends Action {
        public function run($request = '', $event = null, $filter = null) {
            $output = [];
            switch ($request) {
                case 'actors':
                    $output = $this->get_actors_of($event);
                    break;
                case 'subevents':
                    $output = $this->get_subevents($event, $filter);
                    break;
                case 'categories':
                    $output = $this->get_categories($event);
                    break;
                case 'dates':
                    $output = $this->get_subevent_dates($event, $filter);
                    break;
                case 'event':
                    $output = $this->get_event($event);
                    break;
            }
            return json_encode($output);
        }
        
        private function get_subevent_dates($event, $types) {
            $output = [];
            $indexed_outputs = [];
            $where = ["parent.$event" => ['$exists' => true]];
            if (!empty($types)) {
                $where['type'] = $types;
            }
            $db_subevents = PHDB::findAndSort(Event::COLLECTION, $where, ['startDate' => 1], 0, ['startDate', 'timeZone']);
            foreach ($db_subevents as $db_subevent) {
                $date = $this->get_formated(['startDate'], $db_subevent, null, $db_subevent['timeZone'] ?? '');
                $datetime = new DateTime($date);
                $key = $datetime->format('d.m.Y');
                if (!array_key_exists($key, $indexed_outputs)) {
                    $indexed_outputs[$key] = [
                        'date_group' => $key,
                        'year'       => $datetime->format('Y'),
                        'month'      => $datetime->format('F'),
                        'date'       => $datetime->format('d'),
                        'day'        => $datetime->format('l'),
                    ];
                }
            }
            foreach ($indexed_outputs as $indexed_output) $output[] = $indexed_output;
            return $output;
        }
        
        private function get_actors_of($event) {
            $output = [];
            $indexed_outputs = [];
            $link_types = $_POST['types'] ?? [];
            $fields = [
                'name'
            ];
            $where = [
                '$or' => [['_id' => new MongoId($event)]]
            ];
            foreach ($link_types as $link_type) {
                $fields[] = $link_type;
            }
            
            if (empty($_POST['parent_only']) || !filter_var($_POST['parent_only'], FILTER_VALIDATE_BOOLEAN)) {
                $where['$or'][] = ["parent.$event" => ['$exists' => true]];
            }
            
            $db_events = PHDB::findAndSort(Event::COLLECTION, $where, ['startDate' => 1], 0, $fields);
            foreach ($db_events as $id_event => $db_event) {
                $actor_fields = [
                    'name',
                    'profilImageUrl'
                ];
                $attendees = !empty($db_event['links']) && !empty($db_event['links']['attendees']) ? $db_event['links']['attendees'] : [];
                $organizers = !empty($db_event['links']) && !empty($db_event['links']['organizer']) ? $db_event['links']['organizer'] : [];
                $creators = !empty($db_event['links']) && !empty($db_event['links']['creator']) ? $db_event['links']['creator'] : [];
                
                $creator = $db_event['creator'] ?? [];
                if (!empty($creator)) {
                    $creators = array_merge($creators, [$creator => ['type' => 'citoyens']]);
                }
                
                if (!empty($db_event['organizer'])) {
                    $organizers = array_merge($organizers, $db_event['organizer']);
                }
                
                foreach ($organizers as $id => $content) {
                    if (!array_key_exists($id, $indexed_outputs)) {
                        $actor = PHDB::findOneById($content['type'], $id, $actor_fields);
                        $indexed_outputs[$id] = [
                            'id'    => $id,
                            'name'  => $actor['name'],
                            'type'  => $content['type'],
                            'image' => $actor['profilImageUrl'] ?? '',
                            'roles' => [
                                $id_event => [
                                    'id'   => $id_event,
                                    'name' => $db_event['name'],
                                    'role' => 'organizer'
                                ]
                            ]
                        ];
                    } else $indexed_outputs[$id]['roles'][$id_event] = ['id' => $id_event, 'name' => $db_event['name'], 'role' => 'organizer'];
                }
                
                foreach ($attendees as $id => $content) {
                    if (!array_key_exists($id, $indexed_outputs)) {
                        $actor = PHDB::findOneById($content['type'], $id, $actor_fields);
                        if (!empty($actor)) {
                            $indexed_outputs[$id] = [
                                'id'    => $id,
                                'name'  => $actor['name'],
                                'type'  => $content['type'],
                                'image' => $actor['profilImageUrl'] ?? '',
                                'roles' => [
                                    $id_event => [
                                        'id'   => $id_event,
                                        'name' => $db_event['name'],
                                        'role' => !empty($content['isAdmin']) && $content['isAdmin'] ? 'speaker' : 'attendee'
                                    ]
                                ]
                            ];
                        }
                    } else $indexed_outputs[$id]['roles'][$id_event] = ['id' => $id_event, 'name' => $db_event['name'], 'role' => 'attendee'];
                }
                
                foreach ($creators as $id => $content) {
                    if (!array_key_exists($id, $indexed_outputs)) {
                        $actor = PHDB::findOneById($content['type'], $id, $actor_fields);
                        if (!empty($actor)) {
                            $indexed_outputs[$id] = [
                                'id'    => $id,
                                'name'  => $actor['name'],
                                'type'  => $content['type'],
                                'image' => $actor['profilImageUrl'] ?? '',
                                'roles' => [
                                    $id_event => [
                                        'id'   => $id_event,
                                        'name' => $db_event['name'],
                                        'role' => $id_event === $event ? 'creator' : 'speaker'
                                    ]
                                ]
                            ];
                        }
                    } else $indexed_outputs[$id]['roles'][$id_event] = [
                        'id' => $id_event, 'name' => $db_event['name'], 'role' => $id_event === $event ? 'creator' : 'speaker'
                    ];
                }
                if (!empty($db_event['organizerName'])) {
                    $organizer = $db_event['organizerName'];
                    if (!array_key_exists($organizer, $indexed_outputs)) {
                        $indexed_outputs[$organizer] = [
                            'id'    => $organizer,
                            'name'  => $organizer,
                            'type'  => 'organizations',
                            'image' => '',
                            'roles' => [
                                $id_event => [
                                    'id'   => $id_event,
                                    'name' => $organizer,
                                    'role' => 'organizer'
                                ]
                            ]
                        ];
                    } else $indexed_outputs[$organizer]['roles'][$id_event] = ['id' => $id_event, 'name' => $organizer, 'role' => 'organizer'];
                }
            }
            
            // remove indexes
            foreach ($indexed_outputs as $indexed_output) $output[] = $indexed_output;
            return $output;
        }
        
        private function get_categories($event) {
            $output = [];
            $categories = PHDB::distinct(Event::COLLECTION, 'type', ["parent.$event" => ['$exists' => true]]);
            foreach ($categories as $category) $output[] = $category;
            return $output;
        }
        
        private function get_subevents($parent, $type) {
            $output = [];
            $fields = [
                'description',
                'shortDescription',
                'endDate',
                'name',
                'startDate',
                'timeZone',
                'slug',
                'type',
                'links',
                'profilRealBannerUrl',
                'address.streetAddress',
                'address.addressLocality',
                'address.postalCode',
                'organizerName',
                'profilImageUrl'
            ];
            $where = [
                '_id'            => ['$ne' => new MongoId($parent)],
                "parent.$parent" => ['$exists' => true]
            ];
            
            if (!empty($type)) $where['type'] = $type;
            if (!empty($_POST['type'])) $where['type'] = $_POST['type'];
            
            if (!empty($_POST['date'])) {
                if ($_POST['date'] === 'begining') {
                    $first = PHDB::findAndFieldsAndSortAndLimitAndIndex(Event::COLLECTION, $where, [
                        'startDate', 'timeZone'
                    ],                                                  ['startDate' => 1], 1, 0);
                    foreach ($first as $one) {
                        $start_formated = $this->get_formated(['startDate'], $one, null, $db_event['timeZone'] ?? '');
                        $where['startDate'] = [
                            '$gte' => new MongoDate(strtotime($start_formated)),
                            '$lte' => new MongoDate(strtotime($start_formated . ' + 1 day'))
                        ];
                    }
                } else {
                    $where['startDate'] = [
                        '$gte' => new MongoDate(strtotime($_POST['date'])),
                        '$lte' => new MongoDate(strtotime($_POST['date'] . ' + 1 day'))
                    ];
                }
            }
            
            $db_subevents = PHDB::findAndSort(Event::COLLECTION, $where, ['startDate' => 1], 0, $fields);
            foreach ($db_subevents as $id => $db_subevent) {
                $subevent = [
                    'id'                => $id,
                    'name'              => $db_subevent['name'],
                    'slug'              => $db_subevent['slug'],
                    'start_date'        => $this->get_formated(['startDate'], $db_subevent, null, $db_subevent['timeZone'] ?? ''),
                    'end_date'          => $this->get_formated(['endDate'], $db_subevent, null, $db_subevent['timeZone'] ?? ''),
                    'full_description'  => $db_subevent['description'] ?? '',
                    'short_description' => $db_subevent['shortDescription'] ?? '',
                    'image'             => $db_subevent['shortDescription'] ?? '',
                    'profile'           => $db_subevent['profilImageUrl'] ?? '',
                    'banner'            => $db_subevent['profilRealBannerUrl'] ?? '',
                    'communities'       => [],
                    'links'             => []
                ];
                
                if (!empty($db_subevent['links'])) {
                    $attendees = $db_subevent['links']['attendees'] ?? [];
                    $organizers = $db_subevent['links']['organizers'] ?? [];
                    
                    foreach (array_keys($attendees) as $attendee) {
                        $subevent['links']['attendees'][$attendee] = $attendees[$attendee]['type'];
                        if (!in_array($attendee, $subevent['communities'])) $subevent['communities'][] = $attendee;
                    }
                    foreach (array_keys($organizers) as $organizer) {
                        $subevent['links']['organizers'][$organizer] = $organizers[$organizer]['type'];
                        if (!in_array($organizer, $subevent['communities'])) $subevent['communities'][] = $organizer;
                    }
                }
                
                $output[] = $subevent;
            }
            return $output;
        }
        
        private function get_event($event) {
            $output = [];
            $fields = [
                'description',
                'shortDescription',
                'endDate',
                'name',
                'startDate',
                'timeZone',
                'type',
                'links.organizer',
                'links.attendees',
                'profilRealBannerUrl',
                'address.streetAddress',
                'address.addressLocality',
                'address.postalCode',
                'organizerName',
                'slug'
            ];
            $links = [
                Organization::COLLECTION => [],
                Project::COLLECTION      => [],
                Citoyen::COLLECTION      => [],
            ];
            $db_event = PHDB::findOneById(Event::COLLECTION, $event, $fields);
            
            $output['id'] = $event;
            $output['name'] = $db_event['name'];
            $output['full_description'] = $db_event['description'] ?? '';
            $output['short_description'] = $db_event['shortDescription'] ?? '';
            $output['start_date'] = $this->get_formated(['startDate'], $db_event, null, $db_event['timeZone'] ?? '');
            $output['end_date'] = $this->get_formated(['endDate'], $db_event, null, $db_event['timeZone'] ?? '');
            $output['links'] = ['attendees' => [], 'organizers' => []];
            $output['communities'] = [];
            
            if (!empty($db_event['address'])) {
                $address_grouped = [];
                if (!empty($db_event['address']['streetAddress'])) $address_grouped[] = $db_event['address']['streetAddress'];
                if (!empty($db_event['address']['postalCode'])) $address_grouped[] = $db_event['address']['postalCode'];
                if (!empty($db_event['address']['addressLocality'])) $address_grouped[] = $db_event['address']['addressLocality'];
                $output['address'] = implode(', ', $address_grouped);
            }
            
            $organizers = $db_event['links']['organizer'] ?? [];
            $attendees = $db_event['links']['attendees'] ?? [];
            
            foreach ($organizers as $id => $organizer) {
                if (array_key_exists($id, $links[$organizer['type']])) continue;
                $db_organizer = PHDB::findOneById($organizer['type'], $id, ['name', 'email', 'profilImageUrl']);
                if (!empty($db_organizer)) {
                    $links[$organizer['type']][$id] = [
                        'id'    => $id,
                        'name'  => $db_organizer['name'],
                        'email' => $db_organizer['email'] ?? '',
                        'image' => $db_organizer['profilImageUrl'] ?? ''
                    ];
                    $output['links']['organizers'][] = $links[$organizer['type']][$id];
                    if (!in_array($id, $output['communities'])) $output['communities'][] = $id;
                }
            }
            
            if (!empty($db_event['organizerName'])) $output['communities'][] = $db_event['organizerName'];
            
            foreach ($attendees as $id => $attendee) {
                if (array_key_exists($id, $links[$attendee['type']])) continue;
                $db_attendee = PHDB::findOneById($attendee['type'], $id, ['name', 'email', 'profilImageUrl']);
                if (!empty($db_attendee)) {
                    $links[$attendee['type']][$id] = [
                        'id'    => $id,
                        'name'  => $db_attendee['name'],
                        'email' => $db_attendee['email'],
                        'image' => $db_attendee['profilImageUrl'] ?? ''
                    ];
                    $output['links']['attendees'][] = $links[$attendee['type']][$id];
                    if (!in_array($id, $output['communities'])) $output['communities'][] = $id;
                }
            }
            
            return $output;
        }
        
        private function get_formated($fields, $array, $default = null, $timezone = '') {
            foreach ($fields as $field) {
                if (!empty($array[$field])) {
                    $date = $array[$field];
                    $exploded = explode('/', $date);
                    $date = count($exploded) === 3 ? $exploded[2] . '-' . $exploded[1] . '-' . $exploded[0] : $date;
                    $date = !is_integer($date) ? (is_string($date) ? strtotime($date) : (is_array($date) && array_key_exists('sec', $date) ? intval($date['sec']) : $date->sec)) : $date;
                    $datetime = new DateTime();
                    $datetime->setTimestamp($date);
                    if (!empty($timezone)) $datetime->setTimezone(new DateTimeZone($timezone));
                    if ($date > 0) return $datetime->format('c');
                }
            }
            $date = $default ?? date('Y-m-d');
            $date = count(explode('/', $date)) === 3 ? explode('/', $date) : $date;
            $date = is_array($date) ? $date[2] . '-' . $date[1] . '-' . $date[0] : $date;
            $date = !is_integer($date) ? (is_string($date) ? strtotime($date) : $date->sec) : $date;
            $datetime = new DateTime();
            $datetime->setTimestamp($date);
            if (!empty($timezone)) $datetime->setTimezone(new DateTimeZone($timezone));
            return $datetime->format('c');
        }
    }
