<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent;

use CAction;
class GetInfoTypeAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $html = $controller->renderPartial("costum.views.tpls.events.blockProgramme",array("poiList" => $_POST["poiList"], "canEdit" => true, "subevent" => $_POST["subEvent"], "typeofevent" => $_POST["filterType"]));
    }
}