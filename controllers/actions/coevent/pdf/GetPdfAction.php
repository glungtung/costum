<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\pdf;

use CAction, Pdf;
use Event;
use PHDB;

class GetPdfAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$controller = $this->getController();
		// var_dump($eventOrga);exit;
		$html = $controller->renderPartial('costum.views.custom.coevent.pdf', [], true);
		$event = PHDB::findOneById(Event::COLLECTION, $_GET['event'], ['name']);
		$params = [
			'header'     => false,
			'footer'     => false,
			'textShadow' => false,
			'docName'    => $event['name'].' COevent.pdf',
			'html'       => $html,
			'saveOption' => 'D'
		];
		Pdf::createPdf($params);
	}
}
