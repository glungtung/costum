<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\mednumrhonealpe;

use CAction;
use Document;
use MongoId;
use PHDB;

/**
 * 
 */
class ElementAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($id,$type){
		$controller = $this->getController();
    	$params=array();

    	$element= PHDB::findOne($type, 
                array("_id" => new MongoId($id)));
        
        $params["element"]=$element;
        $params["type"] = $type;

        $where=array("id"=>@$id, "type"=>$type, "doctype"=>"image", "contentKey"=> "slider");
        $params["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);

        return $controller->renderPartial("costum.views.custom.mednumRhoneAlpe.element.home",$params,true);    
	}
}

?>
