<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\mednumrhonealpe;
use CAction;
use MednumRhoneAlpe;
use Rest;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $parametres = MednumRhoneAlpe::getEventCommunity($_POST);
        return Rest::json($parametres);
    }
}