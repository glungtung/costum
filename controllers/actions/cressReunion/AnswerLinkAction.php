<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion;

use Yii;
use Mail;
use Rest;
use Slug;
use MongoDate;
use Preference;
use CacheHelper;
use CTKException;
use Role;
use Person;
use PHDB;
use mongoId;
use Answer;
/**
   * Register a new user for the application
   * Data expected in the post : name, email, postalCode and pwd
   * @return Array as json with result => boolean and msg => String
   */
class AnswerLinkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
       // var_dump($_POST['key']);exit;
    	$form=$_POST["form"];
    	$currentAnswer=$_POST["currentAnswer"];
    	$linkedElementType=$_POST["linkedElementType"];
    	$linkedElementId=$_POST["linkedElementId"];
    	$query = array(
            "form" => $form, "_id"=> array('$ne' => new mongoId($currentAnswer)),"links.".$linkedElementType.".".$linkedElementId => array('$exists'=> true)
    	);

    	$linkedAnswers = PHDB::find(Answer::COLLECTION,$query);

      //var_dump($linkedAnswers);exit;


        return Rest::json(array( "result" => "success" , "msg" => "ewaa" , "data" => $linkedAnswers ));


    }
}

