<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\hubterritoires;
use CAction;
use CTKException;
use HubTerritoires;
use Rest;
use Yii;

/**
* Update an information field for a element
*/
class UpdateBlockAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        if(!empty($_POST["block"])) {
            try {
                $res = HubTerritoires::updateBlock($_POST);
                return Rest::json($res);
            } catch (CTKException $e) {
                return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), "data"=>$_POST));
            }
        }
        return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Invalid request")));
    }
}
