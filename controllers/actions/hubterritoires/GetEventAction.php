<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\hubterritoires;
use CAction;
use HubTerritoires;
use Rest;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $parametres = HubTerritoires::getEvent($_POST["source"]);
        
        return Rest::json($parametres);
    }
}