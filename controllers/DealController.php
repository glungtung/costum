<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class DealController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\deal\DashboardAction::class,
	        'importzourit'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\deal\ImportzouritAction::class,
	        'adddatazourit'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\deal\AddDataZouritAction::class,
	    );
	}

}
