<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SmarterritoireautreController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'getcommunityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterritoireautre\GetCommunityAction::class,
            'geteventaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterritoireautre\GetEventAction::class
	    );
	}
}
