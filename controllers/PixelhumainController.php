<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class PixelhumainController extends CommunecterController {

		public function beforeAction($action){
			return parent::beforeAction($action);
		}

		public function actions()
		{
			return array(
				"getcommunityaction"	=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\pixelhumain\GetCommunityAction::class
			);
		}
	}
?>