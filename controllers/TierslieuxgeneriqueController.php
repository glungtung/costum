<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class TierslieuxgeneriqueController extends CommunecterController {

	public function beforeAction($action) {
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'badgefinder' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique\BadgefinderAction::class,
			'getbyids' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique\GetByIdsAction::class,
			'describeproject' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique\DescribeProjectAction::class,
			'subscribetonewsletter' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique\SubscribeToNewsLetterAction::class,
			'generatefromtemplate' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique\GenerateFromTemplateAction::class,
			'setprojectcategories' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique\SetProjectCategoriesAction::class,
		);
	}
}
?>