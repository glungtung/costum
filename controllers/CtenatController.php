<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CtenatController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'elementhome'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element\HomeAction::class,
          'ambitions'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element\AmbitionsAction::class,
          'copyorientations'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element\CopyOrientationsAction::class,
          'adminambitions'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element\AdminAmbitionsAction::class,
	        //'answersadmin'  	=> 'costum.controllers.actions.ctenat.admin.AnswersAdminAction',
	        'prio'  			=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\PrioAction::class,
	        'updatestatuscter'	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\UpdateStatusCterAction::class,
	        'importctenat'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\ImportctenatAction::class,
	        'importindicateur'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\ImportindicateurAction::class,
	        'importorga'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\ImportorgaAction::class,
	        'saveorga'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\SaveOrgaAction::class,
	        'previewdata'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\PreviewDataAction::class,
	        'saveindicateur'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\SaveIndicateurAction::class,
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\DashboardAction::class,
	        'dashgraph'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\DashgraphAction::class,
	    	"dashboardaccount" 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element\DashboardAccountAction::class,
	    	"validata" 			=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\script\ValidataAction::class,
	    	"cter" 				=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\CterAction::class,
	    	"answerscsv" 		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api\AnswerscsvAction::class,
	    	"answersctercsv"	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api\AnswersctercsvAction::class,
	    	"generatecopil" 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf\GenerateCopilAction::class,
	    	"generateorientation" 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf\GenerateOrientationAction::class,
	    	"allanswers" 		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf\AllAnswersAction::class,
	    	"slide" 			=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\SlideAction::class,
	    	"notifinancer" 		=> PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\NotifinancerAction::class,
	    	"convertanswers" 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\script\AnswersAction::class,
	    	"updateorientationscandidatures" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\UpdateProjectOrientationAction::class,
	    	"getindicateur" 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\GetindicateurAction::class,
	    	"selectindicateur" 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\SelectindicateurAction::class,
	    	"updatectetocrte" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin\UpdateCteToCrteAction::class,
            "edlepdf" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf\EdlepdfAction::class,
            "edlecsv" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api\EdlecsvAction::class,
	    	"getaction" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api\GetProjectAction::class
	    );
	}
	public function actionAnalyseDispositifAndLinksCTE(){
    $str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", "category"=>"cteR"), array("links", "name"));
  			$str .= "<h1 style='float:left;width:100%;'>".count($projects)." territoire CTE vont recevoir un attr 'dispositif' : 'CTE'</h1>";
  			//$iTer=0;
  			foreach($projects as $k => $v){
  				$str .= "<div style='float:left;width:100%;border-bottom:4px dotted; padding:30px;'>";
  				$str .= "<h2 style='float:left;width:100%;'>".$v["name"]."</h2>";
  				$countContributors=(isset($v["links"]["contributors"])) ? count($v["links"]["contributors"]) : 0;
  				$str .= "<span style='float:left;width:100%;font-weight:bold'>Analyse de la donnée des ".$countContributors." links sur le project</span>";
				$countOrgaPorteurLinksCTE=0;
				$countPersonPorteurLinksCTE=0;
				$countPersonPorteurLinksDispositf=0;
				$countOrgaPorteurLinksDispositif=0;
				if(isset($v["links"]["contributors"])){	
  					foreach($v["links"]["contributors"] as $idContrib => $value){
  						if(@$value["roles"]){
  							if(in_array("Porteur du CTE", $value["roles"])){
  								if($value["type"]==Person::COLLECTION)
  									$countPersonPorteurLinksCTE++;
  								else
  									$countOrgaPorteurLinksCTE++;
  							}
  							if(in_array(Ctenat::ROLE_PORTEUR_CTER, $value["roles"])){
  								if($value["type"]==Person::COLLECTION)
  									$countPersonPorteurLinksDispositf++;
  								else
  									$countOrgaPorteurLinksDispositif++;
  							}
  						}
  					}
  				}
  				$countCommunauteExt=0;
  				foreach([Person::COLLECTION,Organization::COLLECTION] as $col){
  					$count=PHDB::count($col, array("links.projects.".$k=>array('$exists'=>true)));
  					$countCommunauteExt+=$count;
  				}
  				$str .= "<span style='float:left;width:100%;'>".$countPersonPorteurLinksCTE." citoyen(s) ont le role 'Porteur du CTE'</span>";
				$str .= "<span style='float:left;width:100%;'>".$countOrgaPorteurLinksCTE." orga(s) ont le role 'Porteur du CTE'</span>";
  				$str .= "<span style='float:left;width:100%;font-weight:bold;margin-top:15px;'>Analyse de la donnée chez les ".$countCommunauteExt." orga/citoyens liées au CTE</span>";
  				foreach([Person::COLLECTION,Organization::COLLECTION] as $col){
  					$res=PHDB::find($col, array("links.projects.".$k.".roles"=>array('$in'=>["Porteur du CTE"])));
  					$str .= "<span style='float:left;width:100%;'>".count($res)." ".$col." ont dans leur lien le CTE avec le role 'Porteur du CTE'</span>";
  				}
  				$str .= "<h3 style='margin-top:15px;float:left;width:100%;'>APRES BASH:</h3>";
  				$str .= "<span style='float:left;width:100%;font-weight:bold'>Analyse de la donnée des ".$countContributors." links sur le project</span>";
  				$str .= "<span style='float:left;width:100%;'>".$countPersonPorteurLinksDispositf." citoyen(s) ont le role 'Porteur du dispositif'</span>";
				$str .= "<span style='float:left;width:100%;'>".$countOrgaPorteurLinksDispositif." orga(s) ont le role 'Porteur du dispositif'</span>";
  				$str .= "<span style='float:left;width:100%;font-weight:bold;margin-top:15px;'>Analyse de la donnée chez ".$countCommunauteExt." orga/citoyens liées au CTE</span>";
  				foreach([Person::COLLECTION,Organization::COLLECTION] as $col){
  					$res=PHDB::find($col, array("links.projects.".$k.".roles"=>array('$in'=>[Ctenat::ROLE_PORTEUR_CTER])));
  					$str .= "<span style='float:left;width:100%;'>".count($res)." ".$col." ont dans leur lien le CTE avec le role 'Porteur du dispositif'</span>";
  				}
  				$str .= "</div>";
  			}
  		}
      return $str;
	}
	public function actionMigrationDispositifAndLinksCTE(){
    $str = "";
		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
  			$projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", "category"=>"cteR"), array("links", "name"));
  			$str .=  "<h1 style='float:left;width:100%;'>".count($projects)." territoire CTE ont recu un attr 'dispositif' : 'CTE'</h1>";

  			//$iTer=0;
  			foreach($projects as $k => $v){
  				$set=array("dispositif"=>"CTE");
				$countCTE=0;
				$countCTEPorteur=0;
				$str .=  "<div style='float:left;width:100%;border-bottom:4px dotted; padding:30px;'>";
  					$str .=  "<h2 style='float:left;width:100%;'>".$v["name"]."</h2>";
  				
				if(isset($v["links"]["contributors"])){	
					$countCTE=count($v["links"]["contributors"]);
					$set["links"]=$v["links"];
  					foreach($v["links"]["contributors"] as $idContrib => $value){
  						if(@$value["roles"]){
  							if(in_array("Porteur du CTE", $value["roles"])){
  								$countCTEPorteur++;
  								array_push($set["links"]["contributors"][$idContrib]["roles"], Ctenat::ROLE_PORTEUR_CTER);
  								if (($index = array_search("Porteur du CTE", $set["links"]["contributors"][$idContrib]["roles"])) !== false) {
								   // unset($set["links"]["contributors"][$idContrib]["roles"][$key]);
									   array_splice($set["links"]["contributors"][$idContrib]["roles"], $index,1);
							
								}
  							}
  						}
  					}
  				}
  				PHDB::update(Project::COLLECTION, array("_id"=> new MongoId($k)), array('$set'=>$set));
  				$str .=  $countCTEPorteur." contributeurs ont recu le role 'porteur du dispositif' sur ".$countCTE;
  				$countCommunauteExt=0;
  				foreach([Person::COLLECTION,Organization::COLLECTION] as $col){
  					$results=PHDB::find($col, array("links.projects.".$k.".roles"=>array('$in'=>["Porteur du CTE"])), array("links", "name"));
  					foreach($results as $id => $res){
	  					$setExt=array("links"=>$res["links"]);
	  					foreach($setExt["links"]["projects"] as $idContrib => $value){
	  						array_push($setExt["links"]["projects"][$idContrib]["roles"], Ctenat::ROLE_PORTEUR_CTER);
							//array_filter($arr, fn($e) => !in_array($e, $remove));
							if (($index = array_search("Porteur du CTE", $setExt["links"]["projects"][$idContrib]["roles"])) !== false) {
							    //unset($setExt["links"]["projects"][$idContrib]["roles"][$key]);
							    array_splice($setExt["links"]["projects"][$idContrib]["roles"], $index,1);
							
							}
	  					}
	  					//$str .=  Rest::json($setExt);exit;
	  					PHDB::update($col, array("_id"=> new MongoId($id)), array('$set'=>$setExt));
  				
	  				}
  					$str .=  "<span style='float:left;width:100%;'>".count($results)." ".$col." ont recu le role 'Porteur du dispositf'</span>";
  				}
  				$str .=  "</div>";
  			}
  		}
  	
      return $str;
	}
  public function actionMigrationROLESReferentCTERAndActions(){
    $str = "";
    if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
          foreach(["cteR", "ficheAction"] as $category){
            $projects=PHDB::find(Project::COLLECTION, array("source.key"=>"ctenat", "category"=>$category), array("links", "name"));
            $str .= "<h1 style='float:left;width:100%;'>Analyse des porteurs et partenaires des ".count($projects)."  ".$category." </h1>"; 
            $rolesPorteur=($category=="cteR") ? Ctenat::ROLE_PORTEUR_CTER : "Porteur d'action";
            foreach($projects as $k => $proj){
              if(isset($proj["name"])){
                $str .= "<div style='float:left;width:100%;border-bottom:4px dotted; padding:30px;'>";
                $str .= "<h2 style='float:left;width:100%;'>".$proj["name"]."</h2>";
                //$countCommunautePorteur=0;
                $results=PHDB::find(Organization::COLLECTION, array("links.projects.".$k.".roles"=>array('$in'=>[$rolesPorteur , "Partenaire"])), array("links", "name"));
                foreach($results as $id => $res){
                  $countMemberRefent=0;
                  if(isset($res["links"]["members"])){
                    foreach($res["links"]["members"] as $key => $member){
                      if(isset($member["roles"]) && in_array("Référent CTE", $member["roles"])){
                        $countMemberRefent++;
                        array_push($member["roles"], Ctenat::ROLE_REFERENT);
                        //array_filter($arr, fn($e) => !in_array($e, $remove));
                        if (($index = array_search("Référent CTE", $member["roles"])) !== false) {
                            //unset($setExt["links"]["projects"][$idContrib]["roles"][$key]);
                            array_splice($member["roles"], $index,1);
                        }
                        //ON UP LE ROLES CHEZ les membres de l'orga
                        PHDB::update(Organization::COLLECTION, array("_id"=> new MongoId($id)), array(
                            '$set'=>array(
                              "links.members.".$key.".roles"=>$member["roles"]
                            )
                          )
                        );
                         //PUIS CHEZ LES REFERENT MEMBEROF
                        PHDB::update(Person::COLLECTION, array("_id"=> new MongoId($key)), array(
                            '$set'=>array(
                              "links.memberOf.".$id.".roles"=>$member["roles"]
                            )
                          )
                        );
                      }
                    }
                  //$str .= Rest::json($setExt);exit;
                  //
              
                  }
                  $str .= "<span style='float:left;width:100%;'>L'orga ".$res["name"]." compte ".$countMemberRefent." membres avec le roles 'Référent CTE'</span>";
                }
                //$resRefCte=PHDB::count(Person::COLLECTION, array("links.projects.".$k.".roles"=>array('$in'=>["Référent CTE"])), array("links", "name"));
                //if(!empty($resRefProj)){
                if(isset($proj["links"]["contributors"])){
                  foreach($proj["links"]["contributors"] as $key => $member){
                    if(isset($member["roles"]) && in_array("Référent CTE", $member["roles"])){    
                      array_push($member["roles"], Ctenat::ROLE_REFERENT);
                        //array_filter($arr, fn($e) => !in_array($e, $remove));
                      if (($index = array_search("Référent CTE", $member["roles"])) !== false) {
                          //unset($setExt["links"]["projects"][$idContrib]["roles"][$key]);
                          array_splice($member["roles"], $index,1);
                      }
                      //ON UP LE ROLES CHEZ les membres de l'orga
                      PHDB::update(Project::COLLECTION, array("_id"=> new MongoId($k)), array(
                          '$set'=>array(
                            "links.contributors.".$key.".roles"=>$member["roles"]
                          )
                        )
                      );
                       //PUIS CHEZ LES REFERENT MEMBEROF
                      PHDB::update(Person::COLLECTION, array("_id"=> new MongoId($key)), array(
                          '$set'=>array(
                            "links.projects.".$k.".roles"=>$member["roles"]
                          )
                        )
                      );
                    }
                  }
                }
                //}
                //$str .= "<span style='float:left;width:100%;'>".count($resRefCte)." citoyens sur les CTER ont le role 'Référent CTE'</span>";
                $str .= "</div>";
              }
            }
          }

    }
    return $str;
  }
    
  public function actionMigrationROLESReferentCTENAT(){
    $str = "";
    if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
      $str .=  "<h1>Partie CTENAT</h1>";
      $cteNat=PHDB::findOne(Organization::COLLECTION, array("slug"=>"ctenat"));
      $results=PHDB::find(Organization::COLLECTION, array("links.memberOf.".(string)$cteNat["_id"].".roles"=>array('$in'=>["Partenaire"])), array("links", "name"));
      foreach($results as $id => $res){
        $countMemberRefent=0;
        if(isset($res["links"]["members"])){
          foreach($res["links"]["members"] as $key => $member){
            if(isset($member["roles"]) && in_array("Référent CTE", $member["roles"])){
              $countMemberRefent++;
              array_push($member["roles"], Ctenat::ROLE_REFERENT);
              //array_filter($arr, fn($e) => !in_array($e, $remove));
              if (($index = array_search("Référent CTE", $member["roles"])) !== false) {
                  //unset($setExt["links"]["projects"][$idContrib]["roles"][$key]);
                  array_splice($member["roles"], $index,1);
              }
              //ON UP LE ROLES CHEZ les membres de l'orga
              PHDB::update(Organization::COLLECTION, array("_id"=> new MongoId($id)), array(
                  '$set'=>array(
                    "links.members.".$key.".roles"=>$member["roles"]
                  )
                )
              );
               //PUIS CHEZ LES REFERENT MEMBEROF
              PHDB::update(Person::COLLECTION, array("_id"=> new MongoId($key)), array(
                  '$set'=>array(
                    "links.memberOf.".$id.".roles"=>$member["roles"]
                  )
                )
              );
            }
          }
        }
        $str .=  "<span style='float:left;width:100%;'>L'orga ".$res["name"]." compte ".$countMemberRefent." membres avec le roles 'Référent CTE'</span>";
      }
      if(isset($cteNat["links"]["members"])){
        foreach($cteNat["links"]["members"] as $key => $member){
          if(isset($member["roles"]) && in_array("Référent CTE", $member["roles"])){    
            array_push($member["roles"], Ctenat::ROLE_REFERENT);
              //array_filter($arr, fn($e) => !in_array($e, $remove));
            if (($index = array_search("Référent CTE", $member["roles"])) !== false) {
                //unset($setExt["links"]["projects"][$idContrib]["roles"][$key]);
                array_splice($member["roles"], $index,1);
            }
            //ON UP LE ROLES CHEZ les membres de l'orga
            PHDB::update(Organization::COLLECTION, array("_id"=> new MongoId((string)$cteNat["_id"])), array(
                '$set'=>array(
                  "links.members.".$key.".roles"=>$member["roles"]
                )
              )
            );
             //PUIS CHEZ LES REFERENT MEMBEROF
            PHDB::update(Person::COLLECTION, array("_id"=> new MongoId($key)), array(
                '$set'=>array(
                  "links.memberOf.".(string)$cteNat["_id"].".roles"=>$member["roles"]
                )
              )
            );
          }
        }
      }

    }
    return $str;
  }
  public function actionMigrationStatusCTESigned(){
    $str = "";
    if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
        $projects=PHDB::find(Project::COLLECTION, array("source.status.ctenat"=>"CTE Signé"), array("source", "name"));
        $str .= "<h1 style='float:left;width:100%;'>".count($projects)." territoire CTE ont le statut CTE SIGNE</h1>";

        //$iTer=0;
        foreach($projects as $k => $v){
          //$str .= "<div style='float:left;width:100%;border-bottom:4px dotted; padding:30px;'>";
          $str .= "<h2 style='float:left;width:100%;'>".$v["name"]."</h2>";
          
          $v["source"]["status"]["ctenat"]=Ctenat::STATUT_CTER_SIGNE;
          PHDB::update(Project::COLLECTION, array("_id"=> new MongoId($k)), array('$set'=>array("source"=>$v["source"])));
          //$str .= $countCTEPorteur." contributeurs ont recu le role 'porteur du dispositif' sur ".$countCTE;
        }
      }
      return $str;
  }
   public function actionMigrationSessionCTE(){
    $str = "";
    if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))){
        $projects=PHDB::find(Project::COLLECTION, array(
          '$or'=>array(
            array("sessionCte"=>array('$exists'=>true)),
            array("session"=>array('$exists'=>true))
            )), array("session", "sessionCte", "name"));
        $str .= "<h1 style='float:left;width:100%;'>".count($projects)." ptrésente une sessionCte</h1>";

        //$iTer=0;
        foreach($projects as $k => $v){
          //$str .= "<div style='float:left;width:100%;border-bottom:4px dotted; padding:30px;'>";
          $str .= "<h2 style='float:left;width:100%;'>".$v["name"]."</h2>";
          if(@$v["sessionCte"]){
             $str .=  "<span style='float:left;width:100%;'>sessionCTE:".$v["sessionCte"]."</span>";
             // $v["source"]["status"]["ctenat"]=Ctenat::STATUT_CTER_SIGNE;
              PHDB::update(Project::COLLECTION, array("_id"=> new MongoId($k)), 
                array(
                  '$set'=>array("session"=>$v["sessionCte"]),
                  '$unset'=>array("sessionCte"=>"")
                )
              );
          }else if(@$v["session"])
            $str .= "<span style='float:left;width:100%;'>session: ".$v["session"]."</span>"; 

          //$str .= $countCTEPorteur." contributeurs ont recu le role 'porteur du dispositif' sur ".$countCTE;
        }
      }
      return $str;
  }
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
