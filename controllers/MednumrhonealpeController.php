<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class MednumrhonealpeController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'geteventaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\mednumrhonealpe\GetEventAction::class,
            'element'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\mednumrhonealpe\ElementAction::class,
            'updateblock'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\mednumrhonealpe\UpdateBlockAction::class
	    );
	}
}
