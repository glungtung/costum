<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class MeteolamerController extends CommunecterController {


	public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'importdata' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\ImportDataAction::class,
			'getdata' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\GetDataAction::class,
			'importspots' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\ImportSpotsAction::class,
			'getspots' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\GetSpotsAction::class,
			'savevote' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\SaveVoteAction::class,
			'getwavetable' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\GetWaveTableAction::class,
			'getwindtable' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\GetWindTableAction::class,
			'migratespots' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer\MigrateSpotsAction::class
		);
	}
}
?>