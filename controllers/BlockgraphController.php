<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class BlockgraphController extends CommunecterController {


	public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getcoformconfigs' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph\GetCoformConfigsAction::class,
			'countanswer' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph\CountAnswerAction::class,
			'getprogressbaranswsersdata' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph\GetProgressBarAnswersDataAction::class,
			'getdashboarddata' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph\GetDashboardDataAction::class,
			'applyformtodashboard' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph\ApplyFormToDashboardAction::class
		);
	}
}
?>