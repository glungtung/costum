<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class AltFramadateController extends CommunecterController {


	public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getallanswers' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\altFramadate\GetAllAnswersAction::class,
			'getuserbyid'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\altFramadate\GetUserByIdAction::class,
		);
	}
}
?>