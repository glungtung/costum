<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class CostumgeneriqueController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'getprojects'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetProjectsAction::class,
	        'getarticles'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetArticlesAction::class,
	        'getpoi'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetPoiAction::class,
	 		'getarticlescommunity' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetArticlesCommunityAction::class,
	 		'getarticlescarousel' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetArticlesCarouselAction::class,
	        'geteventdesc'		=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetEventDescAction::class,
	        'getcommunity'		=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetCommunityAction::class,
	        'geteventslide'		=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetEventSlideAction::class,
	        'geteventaction'	=>  \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetEventAction::class,
	        'geteventcommunity'	=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetEventCommunityAction::class,
	        'getressources'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetRessourcesAction::class,
	        'getdda'			=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetDdaAction::class,
	        'getjson'			=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique\GetJsonAction::class
	    );
	}

}
