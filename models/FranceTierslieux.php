<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;

class FranceTierslieux {

	public static function orgaRules($field,$value,$costum=null){
		if(empty($costum)){
			$costum=CacheHelper::getCostum();
		}
		if($field=="manageModel")
		    //On vérifie si la valeur 
			foreach($value as $ind=>$tag){
				$pos=array_search($tag,$costum["lists"][$field]);
				if($pos==false){
					unset($value[$ind]);	
				}
			}
		    if(is_array($value) && sizeof($value)>1){
			// on prend la dernière valeur si plusieurs
			$value=array($value[sizeof($value)-1]);
			// voir in_array pour valeur non conforme
		}
		return $value;
	}
	public static function answerRules($inputKey,$value){
		// bénévoles
		// var_dump($inputKey);exit;

		//ETP gestion lieu
		// all number inputs
		$nbInputs=[
			"franceTierslieux1522023_1549_3le5si0qhpywv18p94ne",
			"franceTierslieux1522023_1549_3le5si0qhkwg9705sfr",
			"franceTierslieux1822023_1721_4lea64x9vdukt4t2p92",
			"franceTierslieux1822023_1721_4lf2b0atwtu662i9uor",
			"franceTierslieux1822023_1721_4lea64xa0lop8kirjb8",
			"franceTierslieux1922023_1231_5lebb61gnmph1vnin1a",
			"franceTierslieux1922023_1231_5lebb61gosevh7nembco",
			"franceTierslieux1922023_1231_5lebb61goil3uftiqkwo",
			"franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve",
			"franceTierslieux1922023_1231_6lebdout8uyagpr9ehyg",
			"franceTierslieux1922023_1231_6lebdout9qov9grn8e08",
			"franceTierslieux1922023_1231_6leh2y401jm987cubh8a",
			"franceTierslieux1922023_1231_6lebdoutagu828mxc7dd",
			"franceTierslieux1922023_1231_6lebdoutam5ehxcno6gm",
			"franceTierslieux1922023_1417_7lf3yrvyxn59y74gq51i",
			"franceTierslieux1922023_1417_7lebeyrj9zthl8xraoim",
			"franceTierslieux2022023_732_9lecfwuxw4ol3ib2zcyc",
			"franceTierslieux2022023_732_9lecfwuxx9q1jro8u6xf",
			"franceTierslieux2022023_732_9lecfwuxx2a0p6im4uhn",
			"franceTierslieux2022023_732_9lecfwuxz95rar0lzc65",
			"franceTierslieux2022023_732_9lecfwuxzsgkob33wl6d"
		];
		if(in_array($inputKey,$nbInputs)!==false){
			$value=str_replace(",",".",$value);
			$value=floatval($value);
			$value=abs($value);
		}

		

		if(str_contains($inputKey,"franceTierslieux1822023_1721_4lea64x9zefdbjqbhl7l")){
			// var_dump($value);exit;
			$length=count(array_keys($value));
			$indexLoyer=-1;
			$loyer="";
			foreach($value as $ind=>$opt){
				
				if(isset($value[$ind]["Loyer / Participation aux frais"]) && isset($value[$ind]["Loyer / Participation aux frais"]["textsup"])){
					$indexLoyer=$ind;
					$loyer=$value[$ind]["Loyer / Participation aux frais"]["textsup"];
				   break;
				}
			}
			
			if(!preg_match('~[0-9]+~', $loyer)){
				// var_dump($loyer);exit;
				$loyer="";
			}
		
			// $correc=[];
			
		
			if(!empty($loyer)){
				// var_dump($loyer);exit;
				// var_dump(preg_match_all("/[0-9]+/",$val,$out));
				
				// $correc[$ind]=[];
				
				if(preg_match('(m2|mètre carré|m2/mois|m²)', $loyer) === 1){
					$meter=["m2","mètre carré","m2/mois","m²"];
					foreach($meter as $mind => $m){
						if(str_contains($loyer,$m)){
							$sub=explode($m,$loyer);
						//    var_dump($sub);
							preg_match("/[0-9]+/",$sub[0],$out);
							if(preg_match("/[0-9]+/",$sub[0],$out)!==0){
								// var_dump($out[0]);
								 $len=sizeof($out[0]);
								$area=15;
								$coef=1;
								// $cost=$out[0][$len-1];
								$cost=$out[0];
								$month=12;
		
								if($cost<=20){
									$coef==1;
		
								}elseif($cost>20){
									$coef=1/12;
								}
								$loyer=$out[0][$len-1];
								// $correc[$ind]["coût m2"]=$cost;
								// $correc[$ind]["coût mois"]=$cost*$area*$coef;
								// $correc[$ind]["original"]=$value;
								// var_dump($correc[$ind]);
							}    
						}
					}
				}else if(preg_match('(mois|MOIS|/m)', $loyer) === 1) { 
					$mois=["mois","MOIS","m"];
					foreach($mois as $jind => $j){
						if(str_contains($loyer,$j)){
							$sub=explode($j,$loyer);
						   // var_dump($sub);
							preg_match_all("/[0-9]+/",$sub[0],$out);
							if(preg_match_all("/[0-9]+/",$sub[0],$out)!==0){
								$len=sizeof($out[0]);
								$loyer=$out[0][$len-1];
								// $correc[$ind]["coût mois"]=$out[0][$len-1];
								// // $correc[$ind]["coût mois"]=$out[0][$len-1]*30;
								// $correc[$ind]["original"]=$value;
								// var_dump($correc[$ind]);
							}    
						}
					}
				}else if(preg_match('(jour|journée|/j)', $loyer) === 1) { 
					// var_dump($loyer);exit;
					$day=["jour","journée","/j"];
					foreach($day as $jind => $j){
						if(str_contains($loyer,$j)){
							$sub=explode($j,$loyer);
						   // var_dump($sub);
							preg_match_all("/[0-9]+/",$sub[0],$out);
							if(preg_match_all("/[0-9]+/",$sub[0],$out)!==0){
								$len=sizeof($out[0]);
								$loyer=$out[0][$len-1];

								$loyer=$loyer*20;
								// var_dump($loyer);exit;
								// $correc[$ind]["coût jour"]=$out[0][$len-1];
								// $correc[$ind]["coût mois"]=$out[0][$len-1]*30;
								// $correc[$ind]["original"]=$value;
								// var_dump($correc[$ind]);
							}    
						}
					}
				}else if(preg_match('(heure|/h|/H)', $loyer) === 1) { 
					$hour=["heure","/h","/H"];
					foreach($hour as $jind => $j){
						if(str_contains($loyer,$j)){
							$sub=explode($j,$loyer);
						   // var_dump($sub);
							// preg_match("[+-]?[0-9]+(\.[0-9]+)?([Ee][+-]?[0-9]+)?",$sub[0],$out);
                            $preg = preg_match("[+-]?[0-9]+(\.[0-9]+)?([Ee][+-]?[0-9]+)?",$sub[0],$out);
                            if($preg !== false && $preg !==0) {
                                $len=sizeof($out[0]);
                                $loyer=$out[0][$len-1];
                                
                                $loyer=$loyer*7*20;
                                // var_dump($ind, $out[0][$len-1]);
                                // $correc[$ind]["coût heure"]=$out[0][$len-1];
                                // $correc[$ind]["coût mois"]=$out[0][$len-1]*7*30;
                                // $correc[$ind]["original"]=$value;
                                // var_dump($correc[$ind]);
                            }
						}
					}
				}else if(preg_match_all("/[0-9]+/",$loyer,$out)){
					if($loyer!="100"){
					// var_dump($loyer);exit;
					}
					$len=sizeof($out[0]);
					$loyer=$out[0][$len-1];
					if($loyer<=100){
						$loyer=$loyer*20;

					}
					// $match[$ind]=array($out[0],$value[$ind]);
					// array_push($match,$out);
		
				}else if(preg_match("/[0-9]+/",$loyer,$out)!==1){
				    //   var_dump($loyer);exit;
				}

				
				
			    
				// var_dump($value);exit;
			}
			if($indexLoyer>-1){ 
			    $value[$indexLoyer]["Loyer / Participation aux frais"]["textsup"]=$loyer;
			}	
			// var_dump($value);exit;
		}
		
		return $value;
		
	}
	
	public static function getSortedZone(){
		$orderBy = array("countryCode" => 1, "name" => 1);

		return $orderBy;
	}

	public static function getNetwork($costumSlug){
		//var_dump($costumSlug);
		$network=PHDB::find(Organization::COLLECTION,array("network"=>"ANCTNetwork"));
		return $network;
	}

	public static function referenceFromFtl($costumSlug){
		$elements=PHDB::findByIds(Organization::COLLECTION, $ids);			

				 if(!empty($elements)){
				 	$countRef=0;
				 	$countNew=0;
				 	foreach($elements as $data){
				 			$newreference = array();
				 			if(!empty($data["reference"])){	 				
								$newreference["costum"] = $data["reference"]["costum"];
									array_push($newreference["costum"],"franceTierslieux");
									try {
										$res = PHDB::update( Organization::COLLECTION, 
									  		array("_id"=>new MongoId($data["_id"])),
				                        	array('$set' => array(	"reference.costum" => $newreference["costum"])));
									} catch (MongoWriteConcernException $e) {
										echo("Erreur à la mise à jour de la référence existante de ".Organization::COLLECTION." avec l'id ".$data);
										die();
									}
				 					$countRef++;

				 				
				 			}
				 			else{
				 				try {
										$res = PHDB::update( Organization::COLLECTION, 
									  		array("_id"=>new MongoId($data["_id"])),
				                        	array('$set' => array("reference.costum"=> array("franceTierslieux"))));
										$countNew++;
									} catch (MongoWriteConcernException $e) {
										echo("Erreur de la création de la référence à jour de l'élément ".Organization::COLLECTION." avec l'id ".$data);
										die();
									}
				 					$countNew++;
				 			}
				 	}
				 	echo $countNew ." tiers-lieux avec nouvelle référence !";
				 	echo "\n".$countRef." tiers-lieux mis a jour avec référence en plus !" ;
				}
	}


	public static function elementAfterUpdate($params){
		$user=Person::getById(Yii::app()->session['userId']);
		$userOrgaAdmin=(isset($user["links"]) && isset($user["links"]["memberOf"]) && isset($user["links"]["memberOf"][$params["id"]]) && isset($user["links"]["memberOf"][$params["id"]]["isAdmin"])) ? true : false;
		if($params["collection"]==Organization::COLLECTION && !$userOrgaAdmin){
		    $isAdmin=true;
		    Link::connect((string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "members", $isAdmin);
            Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf", $isAdmin);
        }
	}

	public static function prepDataForUpdate($data){
		// var_dump($data["images"]);
		$typeEl=(in_array($data["collection"], [Event::CONTROLLER, Project::CONTROLLER, Organization::CONTROLLER])) ? Element::getCollectionByControler($data["collection"]) : $data["collection"]; 
		  		$where=array(
		  			"id"=>(string)$data["_id"], "type"=>$typeEl, "doctype"=>"image", 
		  			"contentKey"=>"slider"
		  		);
		  		$data["photo"] = Document::getListDocumentsWhere($where, "image");

		// var_dump($data["images"]);exit; 		

		return $data;  		

		  		
	}			

	public static function elementAfterSave($data){
        
		
    	 $elt=Element::getElementById($data["id"], $data["collection"]);
    	 $isInviting=false;
    	if($data["collection"]==Organization::COLLECTION && isset($elt["category"]) && $elt["category"]=="network"){ 

	    	$where=array("tags"=>$elt["name"]);
	    	$community=PHDB::find(Organization::COLLECTION,$where);
	        
	        if($data["collection"]==Organization::COLLECTION && isset($community)){
	        	foreach ($community as $key => $val){
	        			Link::connect($data["id"],$data["collection"],$val["_id"],$val["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
	        			Link::connect($val["_id"],$val["collection"],$data["id"],$data["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
	        		
				}

			
	        }
	    } 
	    else if($data["collection"]==Organization::COLLECTION){ 
			// $mappingLevel3ToNetwork=[];
			// $networks=PHDB::find(Organization::COLLECTION,array("network"=>"ANCTNetwork"),array("address.level3"));
			// foreach($networks as $id=>$net){
			// 	$mappingLevel3ToNetwork[$net["address.level3"]]=$net["slug"];
			// }

			// if(!empty($data["params"]["address"]) && isset($data["params"]["address"]["level3"])){
				

			// }

	    	$costum = CacheHelper::getCostum();
			$networks=$costum["lists"]["network"];
	    	$isInviting=false;
	    	$child=[];
	    	$child["childId"]=$elt["_id"];
        	$child["childType"]=$elt["collection"];
	    	//$network=PHDB::find(Organization::COLLECTION,$where);
	        foreach($elt["tags"] as $tag){
	        	if(in_array($tag, $networks)){
	        		$where=array("name"=>$tag);
	        		$net=Element::getElementByWhere(Organization::COLLECTION,$where);
	        		if($net){
	        		// Link::connectParentToChild($net["_id"],$net["collection"],$child,false,Yii::app()->session["userId"]);
		        		Link::connect($net["_id"],$net["collection"],$elt["_id"],$elt["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        		Link::connect($elt["_id"],$elt["collection"],$net["_id"],$net["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        	}	
		        }
	        }

			// size of third place from absolute to filter criterias in tags
			// $size="";
			// if(intval($data["buildingSurfaceArea"])<60){
			// 	$size="Moins de 60m²";
			// }else if(intval($data["buildingSurfaceArea"])<=200){
			// 	$size="Entre 60 et 200m²";
			// }else{
			// 	$size="Plus de 200m²";
			// }
			
			// PHDB::update( Organization::COLLECTION, 
			// 		  array("_id"=>new MongoId($id)),
			// 		array('$push' => array("tags"=> $size )));
	    }


	    if($data["collection"]==Organization::COLLECTION){
			//var_dump("expression");exit;
	    	Admin::addSourceInElement($data["id"],$data["collection"],"tiersLieux","reference");
	    }
    
    }

	public static function getInputsOrElement ($collection="", $form="", $field=[]) {
		$results = [];
		if ($collection == Form::COLLECTION) {
			$getForm =  PHDB::findOneById($collection,$form) ?? [];		
			if (isset($getForm["subForms"])) {
				foreach ($getForm["subForms"] as $subFormsKey => $subFormsValue) {
					$getOneStepOrElement = PHDB::findOne($collection, array("id" => $subFormsValue)) ?? [];
					$idStep = (string) $getOneStepOrElement["_id"];
					$results[$subFormsValue] = array($idStep => $getOneStepOrElement);
				}
			}
		} else {
			$results = PHDB::findOneById($collection, $form, $field) ?? [];
		}
		return $results;
	} 

	public static function globalAutocomplete($form, $searchParams){
		$form = PHDB::findOneById(Form::COLLECTION,$form);
		$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;

		$query = array();
		if(isset($searchParams["searchType"]) && $searchParams["searchType"][0]=="answers" && empty($searchParams["filters"]["sousOrganisation"]))
			$query = array("form"=>(string)@$form["_id"]);

		if(!empty($form["params"]["onlyAdminCanSeeList"]) && $form["params"]["onlyAdminCanSeeList"] && !$formStandalone){
			if(!$isAdmin){
				if (!$isMembers || ($isMembers && count(array_intersect($roles,$aapStep2Roles)) == 0) || ($isMembers && count(array_intersect($roles,$aapStep3Roles)) == 0)) {
					$query = SearchNew::addQuery( $query , array("user"=> Yii::app()->session['userId']));
				}else{
					$params=array('$or' => array(
						array("user"=> Yii::app()->session['userId'])
					));
					if ($isMembers && count(array_intersect($roles,$aapStep2Roles)) > 0)
						$params['$or'][] = array("status"=> "vote");

					if ($isMembers && count(array_intersect($roles,$aapStep3Roles)) > 0)
						$params['$or'][] = array("status"=> "vote");

					$query = SearchNew::addQuery( $query , $params);
				}
			}
		}

		if(!empty($searchParams['sortBy'])){
			if(Api::isAssociativeArray($searchParams['sortBy'])){
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			}else{
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		}else
			$searchParams['sortBy'] = array("updated"=> -1);

		if(!empty($searchParams["name"])){
			if(!empty($searchParams["textPath"]))
				$query = SearchNew::searchText($searchParams["name"], $query, array("textPath"=>$searchParams["textPath"]));
			else
				$query = SearchNew::searchText($searchParams["name"], $query);
		}

		if(!empty($searchParams['filters'])){
			$query = SearchNew::searchFilters($searchParams['filters'], $query);
		}

		if(!isset($searchParams['fields']))
			$searchParams['fields'] = ["_id"];
		if(is_string($searchParams["fields"])) $searchParams["fields"] = [];
		$path = "";
		if (isset($searchParams["textPath"])) {
			$path = explode(":", $searchParams["textPath"]);
		}
		
		if ( isset($searchParams["textPath"]) && isset($path[0]) && $path[0] == "default" && count($query) > 0 && isset($query['$and'][0]['$or'][0]['name']) && ( $searchParams["filterOrga"] == "true" || $searchParams["filterOrga"] == true )) {
			$path = explode(":", $searchParams["textPath"]);
			$orgaQuery['$and'] = $query['$and'];
			$getOrga = PHDB::findAndFieldsAndSortAndLimitAndIndex(Organization::COLLECTION, $orgaQuery, $searchParams["fields"],$searchParams['sortBy'], $searchParams["indexStep"], $searchParams["indexMin"]);
			$getOrga = array_keys($getOrga);
			$getRegex = $query['$and'][0]['$or'][0]['name'];
			$pathOrga = "";
			foreach ($getOrga as $key => $value) {
				$pathOrga = "links.organizations.".$value.".name";
				array_push($query['$and'][0]['$or'], array($pathOrga => $getRegex));
			}
			if (isset($query['$and'][1])) {
				array_push($query['$and'][0]['$or'], $query['$and'][1]);
			}
			if (isset($path[1])) {
				array_push($query['$and'][0]['$or'], array($path[1] => $getRegex));
			}
			unset($query['$and'][1]);
		}

		$res= array();
		if(!empty($searchParams["indexStep"]) && $searchParams["indexStep"] == 30){	
			$res["results"] = PHDB::findAndSort($searchParams["searchType"][0],$query,$searchParams['sortBy'], 25,  $searchParams["fields"]);
			if(isset($searchParams["count"]))
			$res["count"][$searchParams["searchType"][0]] = PHDB::count( $searchParams["searchType"][0] ,$query);
			
		}else{
			if (isset($searchParams["searchType"]))
				$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($searchParams["searchType"][0],$query, $searchParams["fields"],$searchParams['sortBy'], $searchParams["indexStep"], $searchParams["indexMin"]);
			
				if(isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count( $searchParams["searchType"][0] ,$query);
		} 

		if (isset($searchParams["distinct"])) {
			if (isset($searchParams["searchType"]))
				$res["results"] = PHDB::distinct( $searchParams["searchType"][0], $searchParams["distinct"],$query) ?? [];
				unset($res["results"][0]);
				array_push($res["results"], 'Aucun');
			if(isset($searchParams["count"])) {
				$res["count"][$searchParams["searchType"][0]] = count($res["results"]);
			}
		}
		
		return $res;
	}

	public static function addOrganizationAndPerson ($data=[]) {
		$result = [];
		$newArray = [];
		if (count($data) > 0) {
			$orgaId = "";
			$user = "";
			$getOrgaImage = [];
			$getUserName = [];
			$moreContent = [];
			foreach ($data as $key => $value) {
				# code...
				$orgaId = (isset(array_keys($value["links"]["organizations"])[0])) ? array_keys($value["links"]["organizations"])[0] : "";
				$user = (isset($value["user"])) ? $value["user"] : "";
				if ($orgaId != "") {
					$getOrgaImage = PHDB::findOneById(Organization::COLLECTION, $orgaId, array("profilImageUrl" => 1, "_id" => 0));
				} 
				if ($user != "") {
					$getUserName = PHDB::findOneById(Citoyen::COLLECTION, $user, array("name" => 1, "_id" => 0));
				}
				$moreContent["profilImageUrl"] = $getOrgaImage;
				$moreContent["nameUser"] = $getUserName;
				$result[] = $moreContent;
				$result[] = $value;
				// var_export($moreContent, true);
				$newArray[$key] = $result;
				$result = [];
			}
		}
		return $newArray;
	}

	public static function getMarkersAnswers ($where=[], $arrayFormPath=[], $showAnswers=false) {
		$markerData = [];
		$getAllanswerTest = PHDB::find(Answer::COLLECTION, $where , array(
			"links.organizations" => 1, 
			"_id" => 1, 
		));
		$getAllAnswers = [];
		foreach ($getAllanswerTest as $key => $value) {
			if (isset(array_keys($value["links"]["organizations"])[0])) {
				$organizationId = array_keys($value["links"]["organizations"])[0];
				$markerData [$key] [$organizationId] = PHDB::findOneById(Organization::COLLECTION, $organizationId, 
				array(
					'geoPosition' => 1,
					'geo' => 1,
					'name' => 1
				));
				if (count($arrayFormPath) > 0) {
					foreach ($arrayFormPath as $key => $value) {
					
						$findExistAnswers = PHDB::find(Answer::COLLECTION, array(
							"form" => $key, 
							$value .".". $organizationId =>  ['$exists' => true]), ["answers"]);
						$getAllAnswers = array_merge($getAllAnswers,$findExistAnswers);
					}
				}
				
			}
		}
		return array ("markerData" => $markerData, "supplementAnswer" => $getAllAnswers);
	}

	

	// public static function getNameOrUrl ($type, $key) {
	// 	$result = [];
	// 	$getElement = PHDB::
	// }

}


?>