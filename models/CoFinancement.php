<?php

class CoFinancement {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	

    //db.getCollection('organizations').find({"source.key" : "cressReunion", famille : {$exists : 0}})
    //db.getCollection('organizations').count({famille : {$exists : 1}})
    //db.getCollection('organizations').count({"source.key" : "cressReunion"})


    public static $dataBinding_allOrganization = array(
        "id"     => array("valueOf" => "id"),
        "siren"     => array("valueOf" => "siren"),
        "Nom de l'organisme"      => array("valueOf" => "name"),
        "Type : ( NGO, Group, LocalBusiness ou GovernmentOrganization )"       => array("valueOf" => "type"),

        "Rue"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "streetAddress")
                                    )),
        "Code Postal"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "postalCode")
                                    )),
        "Ville"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "addressLocality")
                                    )),
        "Code Pays"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "streetAddress"     => array("valueOf" => "addressCountry")
                                    )),
        "Email"     => array("valueOf" => "email"),
        "Site web"       => array(   "valueOf" => 'slug',
                        "type"  => "url", 
                        "prefix"   => "/#@",
                        "suffix"   => ""),
        "Description courte"      => array("valueOf" => "shortDescription"),
        "Description longue"       => array("valueOf" => "description"),
        "tags"      => array("valueOf" => "tags"),
    );

    public static function saveSiren($params){
        //var_dump("HelloThere"); exit;

        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $resultImport = array();
        $resI = array();
        //Rest::json($res); exit;
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"]) && !empty($value["siren"])){

                    $where = array( "name" => $value["name"],
                                    "source.key" => "cressReunion"
                                    ,"siren" => array('$exists' => 0) 
                                );

                    if(!empty($value["address"])){
                        if(!empty($value["address"]["streetAddress"]))
                            $where["address.streetAddress"] = $value["address"]["streetAddress"];
                    }

                    if( !empty($value["NIC"]) && !empty($value["siren"]) ){
                        $s = $value["NIC"];
                        $len = strlen($value["NIC"]);
                        $c = 5 - $len ;

                        if($c > 0){
                            for ($i=0; $i < $c; $i++) { 
                                $s = "0".$s;
                            }
                        }
                        $value["siret"] = $value["siren"].$s;
                        unset($value["NIC"]);
                    }

                    $org = PHDB::findOne(Organization::COLLECTION, $where);
                    if(!empty($org)){
                        $org["siren"] = $value["siren"];
                        $set["siren"] = $value["siren"];

                        if( !empty($value["sigle"]) ){
                            $set["sigle"] = $value["sigle"];
                            $org["siren"] = $value["siren"];
                        }

                        if( !empty($value["siret"]) ){
                            $set["siret"] = $value["siret"];
                            $org["siret"] = $value["siret"];
                        }

                        if( !empty($value["secteur"]) ){
                            $set["secteur"] = $value["secteur"];
                            $org["secteur"] = $value["secteur"];
                        }
                        if( !empty($value["famille"]) ){
                            $set["famille"] = $value["famille"];
                            $org["famille"] = $value["famille"];
                        }
                        if( !empty($value["arrondissement"]) ){
                            $set["arrondissement"] = $value["arrondissement"];
                            $org["arrondissement"] = $value["arrondissement"];
                        }
                        if( !empty($value["interco"]) ){
                            $set["interco"] = $value["interco"];
                            $org["interco"] = $value["interco"];
                        }
                        if( !empty($value["APET700"]) ){
                            $set["APET700"] = $value["APET700"];
                            $org["APET700"] = $value["APET700"];
                        }

                        PHDB::update(Organization::COLLECTION,
                            array("_id"=>new MongoId($org["_id"]) ) , 
                            array('$set' => $set)
                        );
                        // $resultImport[] = $org; 
                    } else {
                        //$value["testCress"] = true;
                        if(!empty($value["address"])){
                            $value["address"]["addressCountry"] = "RE";
                        }
                        
                        $value["type"] = Organization::TYPE_GROUP;
                        $value = Import::checkElement($value, Organization::COLLECTION);
                        if(!empty($value["msgError"]))
                            unset($value["msgError"]);
                        $value["collection"] = Organization::COLLECTION;
                        $value["key"] = Organization::CONTROLLER;
                        $save =Element::save($value);
                    }
                    $resI[] = $value;
                }
            } 
        }
        //Rest::json($resultImport); exit;
        //$res["resultImport"] = $resultImport;
        $res["resultImport"] = array();
        $res["elementsObj"] = array();
        return $resI ;
    }

    public static function saveOrga($params){
        ini_set('memory_limit', '-1');
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $resultImport = array();
        $resI = array(
            "sirenDouble" => array(),
            "siret" => array(),
            "siretNotDB" => array(),
            "noOrg" => array(),
            "orgSearch" => array(),
            "org" => array(),
            "noaddress" => array(),
            "newOrg" => array()
        );
        //Rest::json($res); exit;
        if(!empty($res) && !empty($res["elementsObj"])){
            $countUpdate =PHDB::updateWithOptions(Organization::COLLECTION, 
                            array( "source.key" => "cressReunion",
                                    "source.new" => true), 
                            array('$unset' => array('source.new'=> false)),
                            array('multiple' => true) );
            foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["siren"]) && !empty($value["siret"])){
                    $count = PHDB::find(Organization::COLLECTION, 
                                    array( "siret" => $value["siret"],
                                            "source.key" => "cressReunion") );
                    if(!empty($value["famille"])){
                        if($value["famille"] == "Associations")
                            $value["famille"] == "Association";
                        if($value["famille"] == "Coopératives")
                            $value["famille"] == "Coopérative";
                        if($value["famille"] == "Fondations")
                            $value["famille"] == "Fondation";
                        if($value["famille"] == "Mutuelles")
                            $value["famille"] == "Mutuelle";
                
                        if($value["famille"] == "Association")
                            $value["type"] = Organization::TYPE_NGO;
                        else if($value["famille"] == "Entreprise de l'ESS")
                            $value["type"] = Organization::TYPE_BUSINESS;
                    }

                    if(empty($value["type"]))
                        $value["type"] = Organization::TYPE_GROUP;
                    $tags = array();
                    if(count($count) == 0){
                        $id = null;
                        $where = array( "siren" => $value["siren"],
                                        "source.key" => "cressReunion");
                        $where["siret"] = array('$exists' => 0);
                        if(empty($value["address"]["postalCode"]))
                            $resI["noaddress"][] = $vale["siret"];
                        if(!in_array($value["siret"], $resI["siret"]))
                            $resI["siretNotDB"][] = $value["siret"];
                        else
                            $resI["sirenDouble"][] = $value["siret"];
                        
                        $org = PHDB::find(Organization::COLLECTION, $where);
                        if(count($org) == 1){
                            //$resI["org"][] = $org;
                            foreach ($org as $keyO => $valO) {
                                $id = $keyO;
                                if(!empty($valO["tags"]))
                                    $tags = $valO["tags"];
                                break;
                            }
                        } else if(count($org) > 1) {
                            $Os = array();
                            $Os2 = array();
                            $Os3 = array();
                            $Os4 = array();
                            $Os5 = array();
                            foreach ($org as $keyO => $valO) {
                                if(  !empty($valO["address"]) &&
                                    !empty($value["apet700"]) &&
                                    !empty($valO["apet700"]) &&
                                    !empty($value["address"]["postalCode"]) &&
                                    !empty($valO["address"]["postalCode"]) &&
                                    !empty($value["address"]["streetAddress"]) &&
                                    !empty($valO["address"]["streetAddress"]) &&
                                    $valO["apet700"] == $value["apet700"] &&
                                    $valO["address"]["postalCode"] == $value["address"]["postalCode"] &&
                                    $valO["address"]["streetAddress"] == $value["address"]["streetAddress"] ){
                                    $Os[] = $keyO;
                                }

                                if( !empty($valO["address"]) &&
                                    !empty($value["interco"]) &&
                                    !empty($valO["interco"]) &&
                                    !empty($value["address"]["postalCode"]) &&
                                    !empty($valO["address"]["postalCode"]) &&
                                    !empty($value["address"]["streetAddress"]) &&
                                    !empty($valO["address"]["streetAddress"]) &&
                                    $valO["interco"] == $value["interco"] &&
                                    $valO["address"]["postalCode"] == $value["address"]["postalCode"] &&
                                    $valO["address"]["streetAddress"] == $value["address"]["streetAddress"] ){
                                    $Os2[] = $keyO;
                                }

                                if(  !empty($valO["address"]) &&
                                    !empty($value["address"]["postalCode"]) &&
                                    !empty($valO["address"]["postalCode"]) &&
                                    !empty($value["address"]["streetAddress"]) &&
                                    !empty($valO["address"]["streetAddress"]) &&
                                    $valO["address"]["postalCode"] == $value["address"]["postalCode"] &&
                                    $valO["address"]["streetAddress"] == $value["address"]["streetAddress"] ){
                                    $Os3[] = $keyO;
                                }

                                if(  !empty($valO["address"]) &&
                                    !empty($value["address"]["streetAddress"]) &&
                                    !empty($valO["address"]["streetAddress"]) &&
                                    $valO["address"]["streetAddress"] == $value["address"]["streetAddress"] ){
                                    $Os4[] = $keyO;
                                }

                                if(  !empty($valO["address"]) &&
                                    !empty($value["address"]["postalCode"]) &&
                                    !empty($valO["address"]["postalCode"]) &&
                                    $valO["address"]["postalCode"] == $value["address"]["postalCode"] ){
                                    $Os5[] = $keyO;
                                }
                            }

                            if(count($Os) == 1)
                                $id = $Os[0];
                            else if(count($Os2) == 1)
                                $id = $Os2[0];
                            else if(count($Os3) == 1)
                                $id = $Os3[0];
                            else if(count($Os4) == 1)
                                $id = $Os4[0];
                            else if(count($Os5) == 1)
                                $id = $Os5[0];
                            else if(count($Os) == 2)
                                $id = $Os[0];
                            else{
                                $resI["noOrg"][] = array(
                                    "org" => $org,
                                    "value" => $value,
                                    "where" => $where
                                );$value["siret"];
                            }
                            if(!empty($id)){
                                if(!empty($valO["tags"]))
                                    $tags = $valO["tags"];
                                $resI["orgSearch"][] = array("id" => $id,
                                    "value" => $value);
                            }
                        } else{
                            if(!empty($value["address"])){
                                $value["address"]["addressCountry"] = "RE";
                            }
                            $value["tags"] =self::defaultTags($tags, @$value["famille"]);
                            $value = Import::checkElement($value, Organization::COLLECTION);
                            if(!empty($value["msgError"]))
                                unset($value["msgError"]);

                            $value["collection"] = Organization::COLLECTION;
                            $value["key"] = Organization::CONTROLLER;
                            $value["source"]["new"] = true;
                            $save = Element::save($value);
                            $resI["newOrg"][] = array("save"=> $save, "value"=>$value);
                            $id = null;
                        }

                        
                    }else{
                        foreach ($count as $keyO => $valO) {
                            $id = $keyO ;
                            if(!empty($valO["tags"]))
                                $tags = $valO["tags"];
                            $resI["org"][] = array("value"=>$value, "id"=>$keyO);
                            break;
                        }
                    }

                    if(!empty($id)){
                        $set["siret"] = $value["siret"];
                        if(!empty($value["apet700"]))
                            $set["apet700"] = $value["apet700"];
                        if(!empty($value["arrondissement"]))
                            $set["arrondissement"] = $value["arrondissement"];
                        $f = null;
                        if(!empty($value["famille"])){
                            $set["famille"] = $value["famille"];
                            $f = Tags::checkAndGetTag($value["famille"]);
                        }
                        if(!empty($value["secteurEtablissement"]))
                            $set["secteurEtablissement"] = $value["secteurEtablissement"];
                        if(!empty($value["sigle"]))
                            $set["sigle"] = $value["sigle"];
                        if(!empty($value["type"]))
                            $set["type"] = $value["type"];

                        $set["tags"] =self::defaultTags($tags, $f);

                        $set["source.new"] = true;
                        PHDB::update(Organization::COLLECTION,
                            array("_id"=>new MongoId($id) ) , 
                            array('$set' => $set)
                        );

                    }
                }   
            }
        }
        //Rest::json($resultImport); exit;
        $res["resultImport"] = $resultImport;
        return Rest::json($resI); exit;
    }

    public static function defaultTags($tags = array(), $famille = null){
        if( !in_array("ess", $tags))
            $tags[] = "ess";

        if( !empty($famille) && !in_array($famille, $tags))
            $tags[] = $famille;
         ; 
        if( !in_array("ess", $tags))
            $tags[] = "ess";
        if( in_array("cress", $tags) ){
            array_splice($tags, array_search('cress', $tags), 1);
        }
        $tags = Tags::filterAndSaveNewTags($tags);
        return $tags;
        
    }


    public static function elementAfterSave($data){
        //WARNING Yii::app()->session["costum"] est ce que l'on veut la data en cache
        $costum = CacheHelper::getCostum();
        // if(!empty(Yii::app()->session["costum"] ) ) {
        if(!empty($costum) ) {
            if($data["collection"] == Organization::COLLECTION){
                $listAdmins = Element::getCommunityByTypeAndId($costum['contextType'], $costum['contextId'], "all", 'isAdmin');
                //Rest::json($listAdmins); exit;
                foreach ($listAdmins as $keyAdmin => $valAdmin) {
                    if(!empty($valAdmin['isAdmin']) && $valAdmin['isAdmin'] === true ){
                        $admin = PHDB::findOneById(Person::COLLECTION, $keyAdmin, array('email'));
                        if(!empty($admin['email'])){
                            $paramsMails = array(
                                "tplMail" => $admin['email'],
                                "tplObject" => "[ARESS] Nouvelle struture",
                                "tpl" => "basic",
                                "html" => "Une nouvelle structure a été renseignée : ".$data["params"]["name"]);
                            Mail::createAndSend($paramsMails);
                        }
                    }    
                }
            }
        }
        //Rest::json($params); exit;
        return $data;
    }

    public static function prepData($params){
    	$costum = CacheHelper::getCostum();
        if($params["collection"] == Organization::COLLECTION ){
            if( empty($params["siret"]) )
                throw new CTKException("Le numéro de SIRET est obligatoire.");
            $siretExist = self::siretExist($params["siret"]);

            if( self::siretExist($params["siret"]) === true)
                throw new CTKException("Le numéro de SIRET existe déjà.");

            $params["siren"] = self::getSirenBySiret($params["siret"]);
            //Rest::json($params); exit;

            $params = self::famille($params);
            $tags = ( !empty($params["tags"]) ? $params["tags"] : array() );
            $params["tags"] = self::defaultTags($tags, (!empty($params["famille"]) ? $params["famille"] : null ) ) ;
        }

        if( $params["collection"] == Organization::COLLECTION &&
            $params["collection"] == Project::COLLECTION &&
            $params["collection"] == Event::COLLECTION){
            $params["preferences"] =  array();
        }

        if( !empty($params["source"]) && !empty($params["source"]["toBeValidated"]) ) {
            if(!empty($params["source"]["toBeValidated"][$costum["slug"]]))
                unset($params["source"]["toBeValidated"][$costum["slug"]]);
            $validated = true;
            if( !empty($costum["typeObj"]) && 
                ( (!empty($costum["typeObj"][$params["collection"]]) &&
                    !empty($costum["typeObj"][$params["collection"]]["validatedParent"]) ) ||
                    (!empty($costum["typeObj"][$params["key"]]) &&
                    !empty($costum["typeObj"][$params["key"]]["validatedParent"]) ) ) ) {

                if($params["collection"] == Event::COLLECTION && !empty($params["organizer"])) {
                    $parent = "organizer";
                }else{
                    $parent = "parent";
                }
                $validated = false;
                foreach ($params[$parent] as $key => $value) {
                    if(!empty($value["type"])){
                        $eltParent = PHDB::findOneById($value["type"],$key, array('name','source'));
                        // var_dump( $eltParent);
                        if( !empty($eltParent) && 
                            !empty($eltParent["source"]) && 
                            !empty($eltParent["source"]["toBeValidated"]) && 
                            !empty($eltParent["source"]["toBeValidated"][$costum["slug"]]) &&
                            $eltParent["source"]["toBeValidated"][$costum["slug"]] )
                            $validated = true;
                    }else
                        $validated = true;
                    
                }
            }
            //var_dump( $validated);
            if($validated === true)
                $params["source"]["toBeValidated"] = array($costum["slug"] => true);
        }
        return $params;
    }

    public static function siretExist($siret){
        $orgs = PHDB::count( Organization::COLLECTION, 
                            array( "source.key" => "cressReunion",
                                    "siret" => trim($siret) ) );
        $res = false ;
        if( $orgs > 0 )
            $res = true ;
        return $res ;
    }

    public static function removeSourceNotNew(){
        $orgs = PHDB::find(  Organization::COLLECTION, 
                            array( "source.key" => "cressReunion",
                                    "source.new" => array('$exists' => 0)),
                            array("name", "source") );

        if(!empty($orgs)){
            foreach ($orgs as $key => $value) {
                if(!empty($value["source"]["keys"])){
                    $new =array();
                    foreach ($value["source"]["keys"] as $k => $v) {
                        if($v != "cressReunion")
                            $new[] = $v;
                    }
                    $value["source"]["keys"] = $new;
                }

                if(!empty($value["source"]["key"]) && $value["source"]["key"] == "cressReunion")
                    $value["source"]["key"] = "";

                PHDB::update(Organization::COLLECTION,
                    array("_id"=>new MongoId($id) ) , 
                    array('$set' => array("source" => $value["source"]) )
                ); 
            }
        }
        
    }


    public static function getSirenBySiret($siret){
        $siren = substr( $siret , 0, 9 );
        return $siren;
    }

    public static function updateBlock($params){
        $set = array();
        $params = self::famille($params);
        
        if(!empty($params["waldec"]))
            $set["waldec"] = $params["waldec"];
        if(!empty($params["siret"])){
            $set["siret"] = $params["siret"];
            $set["siren"] = self::getSirenBySiret($params["siret"]);
        }
        if(!empty($params["sigle"]))
            $set["sigle"] = $params["sigle"];
        if(!empty($params["famille"]))
            $set["famille"] = $params["famille"];
        if(!empty($params["typeEtablissement"]))
            $set["typeEtablissement"] = $params["typeEtablissement"];
        if(!empty($params["arrondissement"]))
            $set["arrondissement"] = $params["arrondissement"];
        if(!empty($params["secteurEtablissement"]))
            $set["secteurEtablissement"] = $params["secteurEtablissement"];

        if(!empty($params["doYouHaveEmployees"]))
            $set["doYouHaveEmployees"] = $params["doYouHaveEmployees"];
        if(!empty($params["ifYesEmployees"]))
            $set["ifYesEmployees"] = $params["ifYesEmployees"];
        if(!empty($params["doYouHaveVolunteers"]))
            $set["doYouHaveVolunteers"] = $params["doYouHaveVolunteers"];
        if(!empty($params["ifYesVolunteers"]))
            $set["ifYesVolunteers"] = $params["ifYesVolunteers"];


        if(!empty($params["ambassadeurName"]))
            $set["ambassadeurName"] = $params["ambassadeurName"];
        if(!empty($params["ambassadeurFirstName"]))
            $set["ambassadeurFirstName"] = $params["ambassadeurFirstName"];
        if(!empty($params["ambassadeurTel"]))
            $set["ambassadeurTel"] = $params["ambassadeurTel"];
        if(!empty($params["ambassadeurMail"]))
            $set["ambassadeurMail"] = $params["ambassadeurMail"];
        if(!empty($params["ambassadeurFonction"]))
            $set["ambassadeurFonction"] = $params["ambassadeurFonction"];

        if(!empty($set)){
        	PHDB::update($params["typeElement"],
                    array("_id"=>new MongoId($params["id"]) ) , 
                    array('$set' => $set ) );
        }
        $res = array("result"=>true,"msg"=>"Update Cress Reunion", "fieldName" => "cressREunion", "value" => $set);
        return $res ;
    }

    public static function generateElementBeforeSave($elt){
		//Rest::json($elt); exit;
		$costum = CacheHelper::getCostum();
		$elt["source"] = Costum::getSource($costum["slug"]);
		if(!empty($elt["source"]) && $elt["collection"] == Organization::COLLECTION )
			$elt["source"]["toBeValidated"] = true;

        return $elt ;
    }

    public static function famille($params){
        if(!empty($params["typeEtablissement"])){
            if(in_array($params["typeEtablissement"], ["Association non employeuse", "Association employeuse"]))
                $params["famille"] = "Association";
            else if(in_array($params["typeEtablissement"], ["SCOP (société coopérative et participative)", "SCIC (société coopérative d'intérêt collectif)", "CAE (coopérative d'activités et d'emploi)", "Banque Coopérative", "Groupement d'Employeurs", "Autre Coopérative"]))
                $params["famille"] = "Coopérative";
            else if(in_array($params["typeEtablissement"], ["Mutuelle"]))
                $params["famille"] = "Mutuelle";
            else if(in_array($params["typeEtablissement"], ["Mutuelle"]))
                $params["famille"] = "Mutuelle";
            else if(in_array($params["typeEtablissement"], ["Fondation"]))
                $params["famille"] = "Fondation";
            else if(in_array($params["typeEtablissement"], ["Entreprise de l'ESS"]))
                $params["famille"] = "Entreprise de l'ESS";

        }

        if(!empty($params["famille"])){
            if($params["famille"] == "Association")
                $params["type"] = Organization::TYPE_NGO;
            else if($params["famille"] == "Entreprise de l'ESS")
                $params["type"] = Organization::TYPE_BUSINESS;
        }

        if(empty($params["type"]))
            $params["type"] = Organization::TYPE_GROUP;
        return $params ;
    }

    public static function addGlobalSearchFields($params){
        $siret=array( "siret" => new MongoRegex("/.*{$params}.*/i"));
        return $siret ;
    }

    public static function addSearchFields(){
        $donation=["campaignSum"];
        return $donation ;
    }
}
?>