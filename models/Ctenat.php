<?php

class Ctenat {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";   
	
    const KEY = "ctenat";
    const CATEGORY_CTER = "cteR";
    const CATEGORY_FICHEACTION = "ficheAction";

    const PARENT_FORM = "ctenatForm";

    const STATUT_CTER_LAUREAT = "Territoire lauréat";
    const STATUT_CTER_SIGNE = "Contrat signé";
    const STATUT_CTER_CANDIDAT =  "Territoire Candidat";
    const STATUT_CTER_REFUSE = "Territoire Candidat refusé"; 
    const STATUT_CTER_COMPLET = "Dossier Territoire Complet";
    const STATUT_CTER_ARCHIVED = "Territoire archivé";

    const STATUT_ACTION_COMPLETED = "Action Réalisée";
    const STATUT_ACTION_VALID = "Action validée";
    const STATUT_ACTION_CONTRACT = "Passage en réunion de finalisation";
    const STATUT_ACTION_MATURATION = "Action en maturation";
    const STATUT_ACTION_LAUREAT = "Action lauréate";
    const STATUT_ACTION_REFUSE = "Action refusée";
    const STATUT_ACTION_CANDIDAT = "Action Candidate";
    const ROLE_REFERENT = "Référent";
    const INDICATOR_EMPLOI_ID = "5dc3b8e7690864700f8b4571";
    const ROLE_PORTEUR_CTER="Porteur du dispositif";

    public static $validCter = [ self::STATUT_CTER_LAUREAT,
                                  self::STATUT_CTER_SIGNE];
                                  
    public static $validActionStates = [ self::STATUT_ACTION_VALID,
                                          self::STATUT_ACTION_COMPLETED,
                                          self::STATUT_ACTION_CONTRACT ];
                                          
    public static $sessionsTags = [ 
        "01/18"=>"candidat2018janv",
        "09/18"=>"candidat2018sept",
        "01/19"=>"candidat2019janv",
        "09/19"=>"candidat2019sept",
        "06/20"=>"candidat2020juin",
        "06/21"=>"CRTE2021" 
    ];

    public static $years = [ "2018","2019","2020","2021","2022","2023","2024","2025","2026"];    

	public static $COLORS = [
        "#0A2F62",
        "#0B2D66",
        "#064191",
        "#2C97D0",
        "#16A9B1",
        "#0AA178",
        "#74B976",
        "#0AA178",
        "#16A9B1",
        "#2A99D1",
        "#064191",
        "#0B2E68"
        ];

    public static $FINANCERS = [ 
        "Communes / intercommunalités / Syndicats mixtes",
        "Départements",
        "Régions",
        "Europe",
        "Etat",   
        "Privé"];
    public static $financerTypeList = [
        "acteursocioeco" => "Acteur Socio-économique",
        "colfinanceur"  => "Communes / intercommunalités / Syndicats mixtes",
        "departement"   => "Départements",
        "region"        => "Région",
        "europe"=>"Europe",
        "etat" => "Etat - Services déconcentrés / préfecture",
        "ademe" => "Etat - Ademe",
        "cerema" => "Etat - Cerema",
        "bdt"=> "Etat - Banque des territoires",
        "bpi"=> "Etat - BPI",
        "agenceLeau"=> "Etat - Agence / Office de l'eau",
        "officeNatForet"=>"Etat - Office national des forêts",
        "agenceBiodiv"=> "Etat - Office de la biodiversité",
        "afd"=> "Etat - Agence Francaise de développement",
        "vn2f"=>"Etat - Voies navigable de France",
        "franceAgirMer"=>"Etat - FranceAgriMer",
        "autre" => "Etat - Autre"
    ];
    public static $financeurTypePublic = ["colfinanceur","departement","region" ,"europe","etat","ademe","cerema","bdt","bpi","agenceLeau","officeNatForet","agenceBiodiv","afd","vn2f","franceAgirMer"];


    public static $listValid = array(
        "valid" => "Validé sans réserves",
        "validReserve" => "Validé avec réserves",
        "notValid" => "Non validé" );

    public static $dataBinding_allProject  = [
        "id"     => ["valueOf" => "id"],
        "type"     => "Project",
        "name"      => ["valueOf" => "name"],
		"siren"     => ["valueOf" => "siren"],
		"why"     => ["valueOf" => "why"],
		"nbHabitant"     => ["valueOf" => "nbHabitant"],
		"dispositifEnPlace"     => ["valueOf" => "dispositifEnPlace"],
		"planPCAET"     => ["valueOf" => "planPCAET"],
		"singulartiy"     => ["valueOf" => "singulartiy"],
		"caracteristique"     => ["valueOf" => "caracteristique"],
		"actions"     => ["valueOf" => "actions"],
		"economy"     => ["valueOf" => "economy"],
		"inCharge"     => ["valueOf" => "inCharge"],
		"autreElu"     => ["valueOf" => "autreElu"],
		"contact"     => ["valueOf" => "contact"],
        "url"       => ["valueOf" => [
		                // "pdf"           => [   "valueOf"   => '_id.$id', 
                                //                                 "type"  => "url", 
                                //                                 "prefix"   => "/co2/export/pdfelement/type/projects/id/",
                                //                                 "suffix"   => "" ],
                                "website"       => [   "valueOf" => 'url']]],
        "address"   => ["parentKey"=>"address", 
                             "valueOf" => [
                                    "@type"             => "PostalAddress", 
                                    "streetAddress"     => ["valueOf" => "streetAddress"],
                                    "postalCode"        => ["valueOf" => "postalCode"],
                                    "addressLocality"   => ["valueOf" => "addressLocality"],
                                    "codeInsee"         => ["valueOf" => "codeInsee"],
                                    "addressRegion"     => ["valueOf" => "addressRegion"],
                                    "addressCountry"    => ["valueOf" => "addressCountry"]
                                    ]],
        "dispositifId" => ["valueOf" => "dispositifId"],
        "dispositif" => ["valueOf" => "dispositif"],
        "startDate"     => ["valueOf" => "startDate"],
        "endDate"       => ["valueOf" => "endDate"],
        "geo"   => ["parentKey"=>"geo", 
                             "valueOf" => [
                                    "@type"             => "GeoCoordinates", 
                                    "latitude"          => ["valueOf" => "latitude"],
                                    "longitude"         => ["valueOf" => "longitude"]
                                    ]],

    ];

    public static $dataHead_allProject  = [
		"type"     => "Type",
		"name"      => "Nom",
		"siren"     => "Siren",
		"why"     => "Pourquoi souhaitez-vous réaliser un CTE ? Quelles sont vos attentes vis-à-vis d’un CTE ?",
		"nbHabitant"     => "Nombre d'habitants concernés?",
		"dispositifEnPlace"     => "Quels sont les dispositifs actuellement mis en place sur tout ou partie du territoire (contractuels et/ou en lien avec la transition écologique) ?",
		"planPCAET"     => "Avez élaboré un Plan climat-air-énergie territorial (PCAET) sur votre territoire ?",
		"singulartiy"     => "Qu'est ce qui constitue la singularité de votre territoire ? Quel est le contexte économique et quels sont ses enjeux en termes de transition écologique ?",
		"caracteristique"     => "Quelles sont les caractéristiques de votre territoire sur lesquelles vous agissez/vous souhaiteriez agir dans le cadre du CTE ? (exemples : problèmes majeurs/risques auxquels vous êtes confrontés, thèmes sur lesquels vous vous êtes engagés à agir)",
		"actions"     => "Quelles sont les actions envisagées dans votre projet de CTE ? Décrivez au moins 3 pistes d’actions.",
		"economy"     => "Quels acteurs socio-économiques prévoyez-vous de mobiliser pour le portage d’actions du CTE ?",
		"inCharge"     => "Qui serait en charge du projet au sein de la collectivité (chargé de mission, équipe dédiée, élu...) ?",
		"autreElu"     => "Autres élus engagés politiquement dans la réussite du projet (député, sénateur, maires) ?",
		"contact"     => "Nom du référent local, Adresse postale, numéro de téléphone et adresse mail ?",
		// "url.communecter"     => "Url ctenat",
		// "url.pdf"     => "PDF",
		"url.website"     => "Site internet",
		"address.streetAddress"     => "Rue",
		"address.postalCode"     => "Code Postal",
		"address.addressLocality"     => "Ville",
		"address.codeInsee"     => "Insee",
		"address.addressCountry"     => "Code Pays",
		"geo.latitude"     => "Latitude",
		"geo.longitude"     => "Longitude",

    ];

    public static $dataBinding_allPoi  = [
        "id"     => ["valueOf" => "id"],
        "name"      => ["valueOf" => "name"],
        "nameLong" => ["valueOf" => "nameLong"],
        "type" => ["valueOf" => "type"],
        "echelleApplication" => ["valueOf" => "echelleApplication"],
        "definition" => ["valueOf" => "definition"],
        "methodeCalcul" => ["valueOf" => "methodeCalcul"],
        "declinaison1" => ["valueOf" => "declinaison1"],
        "declinaison2" => ["valueOf" => "declinaison2"],
        "declinaison3" => ["valueOf" => "declinaison3"],
        "declinaison4" => ["valueOf" => "declinaison4"],
        "unity1" => ["valueOf" => "unity1"],
        "unity2" => ["valueOf" => "unity2"],
        "unity3" =>["valueOf" => "unity3"],
        "perimetreGeo" => ["valueOf" => "perimetreGeo"],
        "periode" => ["valueOf" => "periode"],
        "sourceData1" => ["valueOf" => "sourceData1"],
        "sourceData2" => ["valueOf" => "sourceData2"],
        "sourceData3" => ["valueOf" => "sourceData3"],
        "origin.citykeys" => ["valueOf" => "origin.citykeys"],
        "origin.ISO" => ["valueOf" => "origin.ISO"],
        "origin.RFSC" => ["valueOf" => "origin.RFSC"],
        "origin.ecoCite" => ["valueOf" => "origin.ecoCite"],
        "origin.CTE" => ["valueOf" => "origin.CTE"],
        "origin.other" => ["valueOf" => "origin.other"],
        "domainAction" => ["valueOf" => "domainAction"],
        "objectifDD" => ["valueOf" => "objectifDD"]
    ];

    public static $dataHead_allPoi  = [
        "id"     => "id",
        "name"      => "name",
        "nameLong" => "nameLong",
        "type" => "type",
        "echelleApplication" => "echelleApplication",
        "definition" => "definition",
        "methodeCalcul" => "methodeCalcul",
        "declinaison1" => "declinaison1",
        "declinaison2" => "declinaison2",
        "declinaison3" => "declinaison3",
        "declinaison4" => "declinaison4",
        "unity1" => "unity1",
        "unity2" => "unity2",
        "unity3" => "unity3",
        "perimetreGeo" => "perimetreGeo",
        "periode" => "periode",
        "sourceData1" => "sourceData1",
        "sourceData2" => "sourceData2",
        "sourceData3" => "sourceData3",
        "origin.citykeys" => "origin.citykeys",
        "origin.ISO" => "origin.ISO",
        "origin.RFSC" => "origin.RFSC",
        "origin.ecoCite" => "origin.ecoCite",
        "origin.CTE" => "origin.CTE",
        "origin.other" => "origin.other",
        "domainAction" => "domainAction",
        "objectifDD" => "objectifDD"
    ];

    public static function prepData($params){
        $costum = CacheHelper::getCostum();
		if($params["collection"] == Project::COLLECTION 
			&& isset($params["category"]) && $params["category"]=="cteR"){
            $costum = CacheHelper::getCostum();
			$proj = PHDB::findOne(Project::COLLECTION, array("_id"=> new MongoId($params["id"])));
			$status = (!isset($proj["source"]) || !isset($proj["source"]["status"])|| empty($proj["source"]["status"]) || empty($proj["source"]["status"]["ctenat"])) ? array("ctenat" => "Territoire Candidat") : array("ctenat" => $proj["source"]["status"]["ctenat"]);
				$params["source"]["status"] = $status;
            $params["session"] = end($costum['lists']['sessionCte']);
            //$params["dispositif"] = "CRTE";
		}
		return $params;
    }

    public static function elementBeforeSave($params){
    	//var_dump("elementBeforeSave");
        $elt=Element::getElementById($params["id"], $params["collection"], null, array("links", "category"));
        if($params["collection"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="cteR"){
            if(isset($elt["links"]["contributors"])){
                foreach($elt["links"]["contributors"] as $k => $v){
                    if(isset($v["roles"]) && in_array(Ctenat::ROLE_PORTEUR_CTER, $v["roles"]) && isset($elt["links"]) && isset($elt["parent"][$k])){
                        Link::disconnect($k, $v["type"],$data["id"], $data["collection"], Yii::app()->session["userId"], "projects");
                        Link::disconnect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors");
                    }
                }
            }
            if(isset($data["params"]["parent"]) && !empty($data["params"]["parent"])){
                foreach($data["params"]["parent"] as $k => $v){
                    Link::connect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors",false,false,false,false, [Ctenat::ROLE_PORTEUR_CTER]);
                    Link::connect($k, $v["type"], $data["id"], $data["collection"], Yii::app()->session["userId"], "projects",false,false,false, false, [Ctenat::ROLE_PORTEUR_CTER]);
                }
            }
        }
		if( !empty($params["collection"]) && 
			$params["collection"] == Badge::COLLECTION && 
			!empty($params["id"]) && 
			!empty($params["elt"]) && 
			!empty($params["elt"]["name"]) && 
            !empty($params["elt"]["category"]) && $params["elt"]["category"] != "strategy"){
			$oldBadge = PHDB::findOneById($params["collection"], $params["id"]);
			if(!empty($oldBadge) && !empty($oldBadge["name"])){
				$where =  array("source.key" => "ctenat",
								"tags" => array('$in' => array($oldBadge["name"]) ) ) ;
				$projects = PHDB::find(Project::COLLECTION, $where, array("name", "tags"));
				if(!empty($projects)){
					foreach ($projects as $key => $project) {
						if(!empty($project["tags"])){
							//unset($project["tags"][array_search($oldBadge["name"], $project["tags"])]);
							//array_push($project["tags"], $params["elt"]["name"]);
                            //$project["tags"][] = $params["elt"]["name"];


                            $tagA = array();
                            foreach ($project["tags"] as $keyTA => $valTA) {
                                if( $valTA != $oldBadge["name"] && !empty($valTA) )
                                    $tagA[] = $valTA;
                            }
                            $tagA[] = $params["elt"]["name"];

							PHDB::update(Project::COLLECTION,
								array("_id"=>new MongoId($key)) , 
								array('$set' => array("tags" => $tagA))
							);
						}
					}
				}

                $whereA =  array("source.key" => "ctenat" ) ;
                $answers = PHDB::find("answers", $whereA, array("answers", "formId","cterSlug") );
                //Rest::json($answers); exit;
                if(!empty($answers)){

                    foreach ($answers as $keyA => $answer) {
                        if( !empty($answer["answers"]) &&
                            !empty($answer["answers"]["caracter"]) ) {

                            //Rest::json($c); exit;
                            //unset($project["tags"][array_search($oldBadge["name"], $project["tags"])]);
                            $c = $answer["answers"]["caracter"];
                            if(!empty($c["actionPrincipal"]) && $c["actionPrincipal"] == $oldBadge["name"])
                                $c["actionPrincipal"] = $params["elt"]["name"] ;

                            if(!empty($c["actionSecondaire"]) && array_search($oldBadge["name"], $c["actionSecondaire"]) !== false ) {
                                $aS = array();
                                foreach ($c["actionSecondaire"] as $keyDA => $valDA) {
                                    if( $valDA != $oldBadge["name"] && !empty($valDA) )
                                        $aS[] = $valDA;
                                }
                                $aS[] = $params["elt"]["name"];
                                $c["actionSecondaire"] = $aS ;
                            }

                            if(!empty($c["cibleDDPrincipal"]) && $c["cibleDDPrincipal"] == $oldBadge["name"])
                                $c["cibleDDPrincipal"] = $params["elt"]["name"] ;

                            if(!empty($c["cibleDDSecondaire"]) && array_search($oldBadge["name"], $c["cibleDDSecondaire"]) !== false ) {
                                $aS = array();
                                foreach ($c["cibleDDSecondaire"] as $keyDA => $valDA) {
                                    if( $valDA != $oldBadge["name"] && !empty($valDA) )
                                        $aS[] = $valDA;
                                }
                                $aS[] = $params["elt"]["name"];
                                $c["cibleDDSecondaire"] = $aS ;
                            }
                            //Rest::json($c); exit;
                            $answer["answers"]["caracter"] = $c;
                            PHDB::update("answers",
                                array("_id"=>new MongoId($keyA)) , 
                                array('$set' => array("answers"=> $answer["answers"]))
                            );
                        }
                    }
                }
                $whereI =  array("source.key" => "ctenat",
                                '$or' => array(
                                    array( "domainAction" => array('$in' => array($oldBadge["name"]) ) ),
                                    array( "objectifDD" => array('$in' => array($oldBadge["name"]) ) )
                                ) ) ;
                
                $indicators = PHDB::find(Poi::COLLECTION, $whereI, array("name", "domainAction", "objectifDD"));
                
                if(!empty($indicators)){
                    foreach ($indicators as $key => $poi) {
                        if(isset($poi["domainAction"]) && !empty($poi["domainAction"]) && array_search($oldBadge["name"], $poi["domainAction"]) !== false ) {
                            //unset($poi["domainAction"][array_search($oldBadge["name"], $poi["domainAction"])]);

                            // $dA = array_splice($poi["domainAction"], array_search($oldBadge["name"], $poi["domainAction"])+1, -1);
                            $dA = array();
                            foreach ($poi["domainAction"] as $keyDA => $valDA) {
                                if( $valDA != $oldBadge["name"] && !empty($valDA) )
                                    $dA[] = $valDA;
                            }
                            $dA[] = $params["elt"]["name"];

                            // $dA = array_splice($poi["domainAction"], array_search("Connaissance et protection de la biodiversité et milieux naturels", $poi["domainAction"]), -1);
                            
                            //array_push($poi["domainAction"], $params["elt"]["name"]);
                            //$poi["domainAction"][] = $params["elt"]["name"];
                            
                            PHDB::update(Poi::COLLECTION,
                                array("_id"=>new MongoId($key)) , 
                                array('$set' => array("domainAction" => $dA))
                            );
                        } else if(isset($poi["objectifDD"]) && !empty($poi["objectifDD"]) && array_search($oldBadge["name"], $poi["objectifDD"]) !== false ){
                            // unset($poi["objectifDD"][array_search($oldBadge["name"], $poi["objectifDD"])]);
                            // $poi["objectifDD"][] = $params["elt"]["name"];
                            $oD = array();
                            foreach ($poi["objectifDD"] as $keyOd => $valOd) {
                                if( $valOd != $oldBadge["name"] && !empty($valOd) )
                                    $oD[] = $valOd;
                            }
                            $oD[] = $params["elt"]["name"];
                            PHDB::update(Poi::COLLECTION,
                                array("_id"=>new MongoId($key)) , 
                                array('$set' => array("objectifDD" => $oD))
                            );
                        }
                    }

                }
			}
		}
        //exit;
		return $params;
    }
    public static function init($params){
        return $params;
    }
    public static function getCheckCustomUser($params){
        $userCTENAT = $params["costumUserArray"];
        $userCTENAT["rolesCTE"]=array(
            "adminCTENat"=>false, 
            "partnerCTENat" => false, 
            "referentCTENat"=>array(), 
            "adminCTER"=>array(),
            "partnerCTER"=>array(),
            "referentCTER"=>array(),
            "adminAction"=>array(),
            "partnerAction"=>array(),
            "referentAction"=>array(),
            "userCTENAT"=>array()
        );


        $userCTENAT["listStatutAction"]=array(
            Ctenat::STATUT_ACTION_COMPLETED => Ctenat::STATUT_ACTION_COMPLETED,
            Ctenat::STATUT_ACTION_VALID => Ctenat::STATUT_ACTION_VALID,
            Ctenat::STATUT_ACTION_CONTRACT => Ctenat::STATUT_ACTION_CONTRACT,
            //Ctenat::STATUT_ACTION_MATURATION,
            Ctenat::STATUT_ACTION_MATURATION => Ctenat::STATUT_ACTION_MATURATION,
            Ctenat::STATUT_ACTION_REFUSE => Ctenat::STATUT_ACTION_REFUSE,
            Ctenat::STATUT_ACTION_CANDIDAT => Ctenat::STATUT_ACTION_CANDIDAT,
            Ctenat::STATUT_ACTION_CONTRACT => Ctenat::STATUT_ACTION_CONTRACT
        );

        if(isset(Yii::app()->session["userId"]) && !empty(Yii::app()->session["userId"])){
            $links=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null, array("links"));
            $memberOf=(isset($links["links"]["memberOf"])) ? $links["links"]["memberOf"] : array();
            $infos=array("name", "profilThumbImageUrl", "profilMediumImageUrl", "slug", "why", "description", "links");
            //if(isset($userCTENAT["isMember"]) && $userCTENAT["isMember"]){
                if(isset($userCTENAT["isCostumAdmin"]) && $userCTENAT["isCostumAdmin"]){
                    $userCTENAT["rolesCTE"]["adminCTENat"]=true;
                    $userCTENAT["isMember"]=true;
                  //  return $userCTENAT;
                }else if(!empty($params["communityLinks"]) &&  isset($params["communityLinks"]["links"]) && isset($params["communityLinks"]["links"]["members"])){
                    foreach($params["communityLinks"]["links"]["members"] as $e => $v){
                        if(isset($v["roles"]) && in_array("Partenaire", $v["roles"])){
                            if($e==Yii::app()->session["userId"] && !isset($v["toBeValidated"])){
                                $userCTENAT["isMember"]=true;
                                $userCTENAT["rolesCTE"]["partnerCTENat"]=true; break;          
                            }
                            if($v["type"]==Organization::COLLECTION 
                                && isset($memberOf[$e]) && !isset($memberOf[$e]["toBeValidated"])
                                    && isset($memberOf[$e]["roles"]) && in_array(Ctenat::ROLE_REFERENT, $memberOf[$e]["roles"])){
                                    $userCTENAT["rolesCTE"]["referentCTENat"][$e]=Element::getElementById($e, $v["type"], null, $infos);
                                    $userCTENAT["isMember"]=true;
                                }
                        }
                    }
                }
            //}
            $whereAdminCTER=array(
               "category"=>"cteR",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                '$or'=> array(
                    array(
                        "links.contributors.".Yii::app()->session["userId"].".isAdminPending"=>array('$exists'=>false),
                        "links.contributors.".Yii::app()->session["userId"].".isAdmin"=>array('$exists'=>true)
                    ),
                    array(
                        "links.contributors.".Yii::app()->session["userId"].".roles"=>array('$in'=>[Ctenat::ROLE_PORTEUR_CTER])
                    )
                )
            );
            $userCTENAT["rolesCTE"]["adminCTER"]=PHDB::find(Project::COLLECTION, $whereAdminCTER, $infos);
            
            $wherePartnerCTER=array(
                "category"=>"cteR",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".roles"=>array('$in'=>["Partenaire"])
            );
            $userCTENAT["rolesCTE"]["partnerCTER"]=PHDB::find(Project::COLLECTION, $wherePartnerCTER, $infos);
            $whereAdminAction=array(
                "category"=>"ficheAction",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                '$or'=> array(
                    array(
                        "links.contributors.".Yii::app()->session["userId"].".isAdminPending"=>array('$exists'=>false),
                        "links.contributors.".Yii::app()->session["userId"].".isAdmin"=>array('$exists'=>true)
                    ),
                    array(
                        "links.contributors.".Yii::app()->session["userId"].".roles"=>array('$in'=>["Porteur d'action"])
                    )
                )
            );
            $userCTENAT["rolesCTE"]["adminAction"]=PHDB::find(Project::COLLECTION, $whereAdminAction, $infos);
            $wherePartnerAction=array(
                "category"=>"ficheAction",
                "links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true),
                "links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false),
                "links.contributors.".Yii::app()->session["userId"].".roles"=>array('$in'=>["Partenaire"])
            );
            $userCTENAT["rolesCTE"]["partnerAction"]=PHDB::find(Project::COLLECTION, $wherePartnerAction, $infos);
            // CHECK si l'uilisateur est membre d'une action ou d'un cteR celui qui l'ui ouvre les vue sur la grande communauté du CTE
            $where=array(
                '$and'=>array(
                    array('$or'=>array(
                        array("category"=>"cteR"), 
                        array("category"=>"ficheAction")
                        )
                    ),
                    array("links.contributors.".Yii::app()->session["userId"]=>array('$exists'=>true)),
                    array("links.contributors.".Yii::app()->session["userId"].".toBeValidated"=>array('$exists'=>false))
            ));
    		$project=PHDB::findOne(Project::COLLECTION, $where);
    		if(!empty($project))
    			$userCTENAT["isMember"]=true;

    		if(!empty($memberOf)){
	    		$or=array();
                foreach($memberOf as $k => $v){
    				if(!isset($v["toBeValidated"])){
    					array_merge($or,  array("links.contributors.".$k => array('$exists'=>1)) );
    				    if(isset($v["roles"]) && in_array(Ctenat::ROLE_REFERENT, $v["roles"])){
                            $whereReferentPartnerAction=array(
                                "category"=>"ficheAction",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Partenaire"])
                            );
                            $userCTENAT["rolesCTE"]["referentAction"]=PHDB::find(Project::COLLECTION, $whereReferentPartnerAction, $infos);
                            $wherePorteurAction=array(
                                "category"=>"ficheAction",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Porteur d'action"])
                            );
                            $porterAction=PHDB::find(Project::COLLECTION, $wherePorteurAction, $infos);
                            $whereReferentPartnerCTER=array(
                                "category"=>"cteR",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>["Partenaire"])
                            );
                            $userCTENAT["rolesCTE"]["referentCTER"]=PHDB::find(Project::COLLECTION, $whereReferentPartnerCTER, $infos);
                            $wherePorteurCtE=array(
                                "category"=>"cteR",
                                "links.contributors.".$k=>array('$exists'=>true),
                                "links.contributors.".$k.".toBeValidated"=>array('$exists'=>false),
                                "links.contributors.".$k.".roles"=>array('$in'=>[Ctenat::ROLE_PORTEUR_CTER])
                            );
                            $porterCTE=PHDB::find(Project::COLLECTION, $wherePorteurCtE, $infos);
                            if(!empty($userCTENAT["rolesCTE"]["referentCTER"]) 
                                || !empty($userCTENAT["rolesCTE"]["referentAction"])
                                || !empty($porterCTE)
                                || !empty($porterAction)){
                                $partner=Element::getElementById($k, $v["type"], null, $infos);
                                $userCTENAT["isMember"]=true;
                                if(!empty($userCTENAT["rolesCTE"]["referentCTER"])){
                                    foreach($userCTENAT["rolesCTE"]["referentCTER"] as $e => $v){
                                        $userCTENAT["rolesCTE"]["referentCTER"][$e]["orgaPartner"]=array_merge($partner, array("id"=>$k));
                                    }
                                }
                                if(!empty($userCTENAT["rolesCTE"]["referentAction"])){
                                    foreach($userCTENAT["rolesCTE"]["referentAction"] as $e => $v){
                                        $userCTENAT["rolesCTE"]["referentAction"][$e]["orgaPartner"]=array_merge($partner, array("id"=>$k));
                                    }
                                }
                                if(!empty($porterCTE)){
                                    foreach($porterCTE as $e => $v){
                                        $porterCTE[$e]["orgaPorter"]=array_merge($partner, array("id"=>$k));
                                    }
                                    $userCTENAT["rolesCTE"]["adminCTER"]= array_merge($userCTENAT["rolesCTE"]["adminCTER"], $porterCTE);
                                }
                                if(!empty($porterAction)){
                                    foreach($porterAction as $e => $v){
                                        $porterAction[$e]["orgaPorter"]=array_merge($partner, array("id"=>$k));
                                    }
                                    $userCTENAT["rolesCTE"]["adminAction"]= array_merge($userCTENAT["rolesCTE"]["adminAction"], $porterAction);
                                }
                            }
                        }
                    }
    			}
    			if($or && $userCTENAT["isMember"]===false){
    				$where = array('$and'=>array(
    						'$or'=>array(array("category"=>"cteR"), array("category"=>"ficheAction")),
    						'$or'=>$or
    					)
    				);
    				$project=PHDB::findOne(Project::COLLECTION, $where);
    				if(!empty($project))
						$userCTENAT["isMember"]=true;
	
    			}
	    	}
        }
    	return $userCTENAT;
    }
    public static function removeLinksAnswer($params){
        if(!empty($params["answer"]["cterSlug"]) && 
            isset($params["answer"]["answers"]["action"]["project"][0])){
            $cteR = Slug::getElementBySlug($params["answer"]["cterSlug"], array("name") ) ;

            $project= $params["answer"]["answers"]["action"]["project"][0] ;
                       
            //$project = $answer["answers"][$answer["formId"]]["answers"]["project"] ;
            //disconnect cter.links.action(project)
            Link::disconnect($cteR["id"], Project::COLLECTION, $project["id"], Project::COLLECTION,Yii::app()->session["userId"], "projects", null, null);
            //Add notification - email label in child link
            //disconnect project.links.cter(project)
            Link::disconnect($project["id"], Project::COLLECTION, $cteR["id"], Project::COLLECTION, Yii::app()->session["userId"], "projects", null, null);
            //disconnect project.links.answers
            Link::disconnect($project["id"], Project::COLLECTION, (string)$params["answer"]["_id"], Answer::COLLECTION, Yii::app()->session["userId"], "answers", null, null);

            $where = array("_id" => new MongoId($project["id"]));
            $action = array('$unset' => array('category' => ""));
            PHDB::update( Project::COLLECTION, $where, $action );
            PHDB::update( Badge::COLLECTION, array("links.projects.".$project["id"]=>array('$exists'=>true), "category"=> "strategy" ), array('$unset'=>array("links.projects.".$project["id"]=>"")));
                            
        }
        return true;
    }
    public static function canAccessAnswer($answer){
        $res = false;
        $answer=$answer["answer"];
        if($answer["answers"]["action"]["project"]){
            $res = Authorisation::isElementMember( $answer["answers"]["action"]["project"][0]["id"]  , Project::COLLECTION, Yii::app()->session["userId"] );
       // if($res == false && $answer["answers"]["action"]["organization"][0]){
         //           $res = Authorisation::isElementMember( $answer["answers"]["action"]["organization"][0], Organization::COLLECTION, Yii::app()->session["userId"] );
        }
        return $res; 
    }
    public static function canAdminAnswer($answer){
        $res = false;
        if(isset($answer["answer"]["cterSlug"])){
            $parent = Slug::getElementBySlug($answer["answer"]["cterSlug"], array("name") ) ;
            if(Authorisation::isElementAdmin($parent["id"], $parent["type"], Yii::app()->session["userId"])){
                return true;
            }
        }
        return $res; 
    }
    

    public static function canEditAnswer($answer){
        $res = false;
        if(isset($answer["cterSlug"])){
            $parent = Slug::getElementBySlug($ans["cterSlug"], array("name") ) ;
            if(Authorisation::isElementAdmin($parent["id"], $parent["type"], Yii::app()->session["userId"])){
                return true;
            }
        }

        if(isset($answer["answers"]["action"]["project"]) && !empty($answer["answers"]["action"]["project"]))
            $res = Authorisation::canEditItem(Yii::app()->session["userId"], Project::COLLECTION, $answer["answers"]["action"]["project"][0]["id"]  );
        else if(isset($answer["answer"]["answers"]["action"]["project"]) && !empty($answer["answer"]["answers"]["action"]["project"])) {
            $res = self::isElementMember(array( "elementType" => Project::COLLECTION, "elementId" => $answer["answer"]["answers"]["action"]["project"][0]["id"], "userId" => Yii::app()->session["userId"]));
        }
        return $res;
    }
    public static function authorizedPanelAdmin($params){
        $costum = CacheHelper::getCostum();
        $res=self::isElementMember(array("elementType"=>$costum["contextType"], "elementId"=>$costum["contextId"], "userId"=>Yii::app()->session["userId"]));
        if( $res === false && !empty($params) ){
            $res=self::isElementMember(array("elementType"=>$params["type"], "elementId"=>$params["id"], "userId"=>Yii::app()->session["userId"]));    
        }
        return $res;
    }

    public static function isElementAdmin($params){
    	//echo "//".$params['elementId']."//";echo $params['elementType']; echo $params['userId'];exit;
    //	print_r($params); exit;
    	$elt=Element::getElementById( $params['elementId'], $params['elementType'], null, array("links", "parent", "category"));
    	if($params["elementType"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="ficheAction"){
    		if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["projects"])){
    			foreach($elt["links"]["projects"] as $k => $v){
    				$res=Authorisation::isElementAdmin($k, $v["type"], Yii::app()->session["userId"], true);
    				if(!empty($res))
                        return $res;

    			}
    		}
    	}
        if($params["elementType"]==Project::COLLECTION && isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["contributors"])){
            foreach($elt["links"]["contributors"] as $k => $v){
                if(isset($v["roles"]) && 
                    (($elt["category"]=="ficheAction" && in_array("Porteur d'action", $v["roles"])) 
                        || ($elt["category"]=="cteR" && in_array(Ctenat::ROLE_PORTEUR_CTER, $v["roles"])))){
                    if($k==$params["userId"]){
                        return true;
                    }
                    if($v["type"]==Organization::COLLECTION){
                        $orga=Element::getElementById( $k, $v["type"], null, array("links"));
                        if(isset($orga["links"]["members"]) 
                            && isset($orga["links"]["members"][$params["userId"]]) 
                                && ((isset($orga["links"]["members"][$params["userId"]]["isAdmin"]) && !isset($orga["links"]["members"][$params["userId"]]["isAdminPending"])) || (!isset($orga["links"]["members"][$params["userId"]]["toBeValidated"]) && isset($orga["links"]["members"][$params["userId"]]["roles"]) 
                                        && in_array(Ctenat::ROLE_REFERENT,$orga["links"]["members"][$params["userId"]]["roles"]))))
                            return true; 
                    }
                    if(!empty($res))
                        return $res;
                }
            }
        }
    	return false;
    }
    public static function isElementMember($params){
    	//echo "//".$params['elementId']."//";echo $params['elementType']; echo $params['userId'];exit;
        //	print_r($params); exit;
        $costum = CacheHelper::getCostum();
    	$elt=Element::getElementById( $params['elementId'], $params['elementType'], null, array("links", "parent", "category", "slug"));
    	// CHECK IF PARTENAIRE CTENAT
    	if($elt["slug"] != "ctenat"){
    		$cteNat=Element::getElementById( $costum["contextId"], $costum["contextType"], null, array("links", "parent", "category", "slug"));
    	}else{
    		$cteNat=$elt;
    	}
    	if(!empty($cteNat["links"]) && isset($cteNat["links"]["members"]) && !empty($cteNat["links"]["members"]) ){
    		foreach($cteNat["links"]["members"] as $k => $v){
    			if(isset($v["roles"]) && in_array("Partenaire",$v["roles"])){
	    			if($k==$params["userId"] && !isset($v["toBeValidated"]))
	    				return true;
	    			if($v["type"]==Organization::COLLECTION){
	    				$orga=Element::getElementById( $k, $v["type"], null, array("links"));
    						if(isset($orga["links"]["members"]) 
    							&& isset($orga["links"]["members"][$params["userId"]]) 
    								&& ((isset($orga["links"]["members"][$params["userId"]]["isAdmin"]) && !isset($orga["links"]["members"][$params["userId"]]["isAdminPending"])) || (!isset($orga["links"]["members"][$params["userId"]]["toBeValidated"]) && isset($orga["links"]["members"][$params["userId"]]["roles"]) 
    										&& in_array(Ctenat::ROLE_REFERENT,$orga["links"]["members"][$params["userId"]]["roles"]))))
    							return true; 
	    			}
    			}
    		}
    	}
        // CHECK IF PARTENAIRE CTER
    	if($params["elementType"]==Project::COLLECTION){
    		if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["contributors"])){
    			foreach($elt["links"]["contributors"] as $k => $v){
    				if(isset($v["roles"]) && in_array("Partenaire", $v["roles"])){
    					if($k==$params["userId"])
    						return true;
    					if($v["type"]==Organization::COLLECTION){
    						$orga=Element::getElementById( $k, $v["type"], null, array("links"));
                            //var_dump("iciiiii");var_dump($orga["links"]);var_dump($params["userId"]);exit;
    						if(isset($orga["links"]["members"]) 
    							&& isset($orga["links"]["members"][$params["userId"]]) 
    								&& ((isset($orga["links"]["members"][$params["userId"]]["isAdmin"]) && !isset($orga["links"]["members"][$params["userId"]]["isAdminPending"])) || (!isset($orga["links"]["members"][$params["userId"]]["toBeValidated"]) && isset($orga["links"]["members"][$params["userId"]]["roles"]) 
    										&& in_array(Ctenat::ROLE_REFERENT,$orga["links"]["members"][$params["userId"]]["roles"]))))
    							return true; 
    					}
    					if(!empty($res))
                        	return $res;
	    			}
	    		}
	    	}
    		if($params["elementType"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="ficheAction"){
    			if(!isset($params["checkParent"])){
		    		if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["projects"])){
		    			foreach($elt["links"]["projects"] as $k => $v){
		    				$res=self::isElementMember(array("elementId"=>$k, "elementType"=>$v["type"], "userId"=>Yii::app()->session["userId"], "checkParent" => true));
		    				if(!empty($res))
		                        return $res;

		    			}
		    		}
		    	}
	    	}
    	}
    	return false;
    }
    public static function elementAfterUpdate($data){
        $elt=Element::getElementById($data["id"], $data["collection"], null, array("name", "links", "category", "type", "typeStruct"));
        if($data["collection"]==Organization::COLLECTION){
            if( !empty($elt['links']['answers']) ){
                foreach ($elt['links']['answers'] as $keyA => $valA) {
                    $answersID = $keyA;
                    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answersID);
                    if(isset($answer["answers"]["action"]["parents"])){
                        foreach ($answer["answers"]["action"]["parents"] as $i => $o) {
                            if($data["id"]== $o["id"]){
                                $set = array("answers.action.parents.".$i.".name" => $elt["name"], "answers.action.parents.".$i.".type" => $elt["type"], "answers.action.parents.".$i.".typeStruct" => $elt["typeStruct"]);
                                 //var_dump($set);
                                PHDB::update(Form::ANSWER_COLLECTION,
                                                array("_id" => new MongoId($answersID) ) , 
                                                array('$set' => $set)
                                                );
                            }
                        }
                    }
                    
                }
            }
        }

        
    	self::elementAfterSave($data);
    }

    public static function elementAfterSave($data){
        //var_dump($data); exit;
    	$elt=Element::getElementById($data["id"], $data["collection"], null, array("links", "category", "type", 'parent'));
        // var_dump($data);

        //creation d'un indicator 
        //update l'answers.murir.results
        if($data["collection"]==Poi::COLLECTION && $data["params"]["type"] == "indicator"){
            // var_dump("here 1");
            if( !empty($data["params"]['parent']) ){
                // var_dump("here 2");
                foreach ($data["params"]['parent'] as $keyP => $valP) {
                    // var_dump("here 3");
                    $parent=Element::getElementById($keyP, $valP["type"], null, array("links"));
                    
                    if(!empty($parent["links"]) && !empty($parent["links"]["answers"])){
                        // var_dump("here 4");
                        foreach ($parent["links"]["answers"] as $keyA => $valA) {
                            $answer=PHDB::findOneById("answers", $keyA,array("answers", "formId"));
                            // var_dump("here 5");
                            if(!empty($answer["answers"]) &&  
                                !empty($answer["answers"]["murir"])) {
                                // var_dump("here 6");
                                    $r = array();
                                    if(!empty($answer["answers"]["murir"]["results"]))
                                        $r = $answer["answers"]["murir"]["results"];

                                    $r[] = array("indicateur" => $data["id"]);

                                    $answer["answers"]["murir"]["results"] = $r;
                                    PHDB::update(Form::ANSWER_COLLECTION,
                                        array("_id" => new MongoId($keyA)) , 
                                        array('$set' => array("answers" => $answer["answers"]))
                                    );
                            }
                        }
                    }
                }
            }
        }

        // Save orga in CTER and Link it
        // adds
        // cter.projects.links.contributors
        // orga.links.projects
        if($data["collection"]==Organization::COLLECTION){
            if(isset($data['postParams']['links']['projects']) && isset($data['postParams']["roles"])){
                $roles=(!empty($data['postParams']["roles"])) ?  explode(",", $data['postParams']["roles"]) : null;
                Link::connect($data['postParams']['links']['projects'], Project::COLLECTION, $data["id"], Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, $roles);
                            //links.projects rattaché le projet à l'orga porteuse 
                Link::connect($data["id"], Organization::COLLECTION, $data['postParams']['links']['projects'], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, $roles);
            }
        }

        // create new Territory : project.category = CTER
        // adds
        // projects(ctenat).links.contributors
        // projects(ctenat).links.projects(cteR)
    	if($data["collection"]==Project::COLLECTION && isset($elt["category"]) && $elt["category"]=="cteR"){
    		/*if(isset($elt["links"]["contributors"])){
    			foreach($elt["links"]["contributors"] as $k => $v){
    				if(isset($v["roles"]) && in_array(Ctenat::ROLE_PORTEUR_CTER, $v["roles"]) && isset($elt["links"])){
    					Link::disconnect($k, $v["type"],$data["id"], $data["collection"], Yii::app()->session["userId"], "projects");
						Link::disconnect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors");
    				}
    			}
    		}*/
            
    		if(isset($data["params"]["parent"]) && !empty($data["params"]["parent"])){
    			foreach($data["params"]["parent"] as $k => $v){
	    			Link::connect($data["id"], $data["collection"], $k, $v["type"], Yii::app()->session["userId"], "contributors",false,false,false,false, [Ctenat::ROLE_PORTEUR_CTER]);
	    			Link::connect($k, $v["type"], $data["id"], $data["collection"], Yii::app()->session["userId"], "projects",false,false,false, false, [Ctenat::ROLE_PORTEUR_CTER]);
    			}
    		}
    	}

        //create new Project : project.category = ficheAction
        // adds
        // projects(ficheAction).links.contributors.personID
        // projects(ficheAction).links.answers
        // projects(cteR).links.answers
        // projects(ficheAction).links.projects(cteR)
        // projects(cteR).links.projects(ficheAction)
        // projects.links.contributors.orgaID
        // orga.links.projects.projectID
        if( $data["collection"] == Project::COLLECTION && 
            isset( $elt["category"] ) && 
            $elt["category"] == "ficheAction"){
            
            $answersID = null;
            $cter = null;
            $answer = null ;
            if( isset( $data['postParams']['links']['answers'] ) ) {
                
                if(!is_array($data['postParams']['links']['answers']))
                    $data['postParams']['links']['answers'] = [$data['postParams']['links']['answers'] => []];

                foreach ($data['postParams']['links']['answers'] as $keyA => $valA) {
                    //links.answers rattaché la réponse au projet
                    // projects(ficheAction).links.answers
                    $answersID = $keyA;
                    Link::connect($data["id"], Project::COLLECTION, $keyA, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
                }
            }

            //links.contributors rattaché le projet action à l'auteur
            // projects(ficheAction).links.contributors.personID
            Link::connect($data["id"], Project::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Créateur d'action"]);


            if( isset($data['postParams']['links']['projects']) ){
                
                if(!is_array($data['postParams']['links']['projects']))
                    $data['postParams']['links']['projects'] = [ $data['postParams']['links']['projects'] => [] ];

                foreach ($data['postParams']['links']['projects'] as $cterId => $valP) {
                    $projectCTER =  PHDB::findOneById(Project::COLLECTION, $cterId, array("category", "name", "slug"));

                    if(isset($projectCTER["category"]) && $projectCTER["category"] == "cteR"){
                        $cter = $projectCTER["slug"];
                        //links.projects rattaché le projet action au projet cter associé
                        // projects(ficheAction).links.projects(cteR)
                        Link::connect($data["id"], Project::COLLECTION, $cterId, Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false,false, ["Dispositif Porteur"]);

                        //links.projects rattaché le projet action au projet cter associé
                        // projects(cteR).links.projects(ficheAction)
                        Link::connect($cterId, Project::COLLECTION, $data["id"], Project::COLLECTION,  Yii::app()->session["userId"], "projects",false,false,false,false, ["Action Dispositif"]);
                    }
                }
            }

            //update connected answer 
            if(!empty($data["id"]) && !empty($cter) && !empty($answersID)){
                $set = array("answers.action.project.0" => 
                                array( "type" => $data["collection"], 
                                        "id" => $data["id"],
                                        "name" => $data["params"]["name"],
                                        "slug" => $data["params"]["slug"])) ;
                
                PHDB::update(Form::ANSWER_COLLECTION,
                            array("_id" => new MongoId($answersID) ) , 
                            array('$set' => $set)
                            );

                $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answersID, array("answers"));

                if( isset($answer["answers"]["action"]["parents"]) ){
                    foreach ($answer["answers"]["action"]["parents"] as $i => $o) {
                        $orgaId = $o["id"]; 
                        //links.contributors rattaché le projet action à l'organization
                        // projects.links.contributors.orgaID
                        Link::connect($data["id"], Project::COLLECTION, $orgaId, Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Porteur d'action"]);
                        //links.projects rattaché le projet à l'orga porteuse 
                        // orga.links.projects.projectID
                        Link::connect($orgaId, Organization::COLLECTION, $data["id"], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, ["Porteur d'action"]);
                    }
                    
                   
                }
            }
            if(isset($data["params"]["parent"]) && !empty($data["params"]["parent"]) && !empty($answersID)){
                $territory = [];
                foreach($data["params"]["parent"] as $k => $v){
                    $territory = PHDB::findOneById(Project::COLLECTION, $k, array("dispositif","dispositifId"));
                }
                $setPorteur = array("answers.action.porteur" => 
                            array(
                                "dispositif" => $territory["dispositif"],
                                "dispositifId" => $territory["dispositifId"]
                            )
                        );
                PHDB::update(Form::ANSWER_COLLECTION,
                    array("_id" => new MongoId($answersID) ) , 
                    array('$set' => $setPorteur)
                    );
            }
            
        }

        //on create BADGE
        // badge.links.projects des actions
        if($data["collection"]==Badge::COLLECTION && !empty($data['postParams'])
            && !empty($data['postParams']['links']) && !empty($data['postParams']['links']["projects"]) ){
                PHDB::update( $data["collection"],
                            array("_id" => new MongoId($data["id"]) ) , 
                            array('$set' => array("links.projects"=>$data['postParams']['links']["projects"]))
                            );
                PHDB::update( Project::COLLECTION, array("links.orientations".$data["id"]=>array('$exists'=>true)), array('$unset'=>array("links.orientations".$data["id"]=>"")));

                if(!is_array($data['postParams']['links']['projects']))
                        $data['postParams']['links']['projects'] = [$data['postParams']['links']['projects'] => []];

                foreach($data['postParams']['links']["projects"] as $k => $v){
                    $project=PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId($k)), array("links.orientations"));
                    $linksOrientation=array("orientations"=>array());
                    if(isset($project["links"]) && isset($project["links"]["orientations"])){
                        $linksOrientation["orientations"]=$project["links"]["orientations"];
                    }
                    $linksOrientation["orientations"][$k]=array("type"=>"badges");
                    PHDB::update(Project::COLLECTION,array("_id"=>new MongoId($k)), 
                        array('$set'=>array("links.orientations"=>$linksOrientation["orientations"])));
                }
        }

        //on create orga
        // orga.links.answers
        // project(cter).links
        if($data["collection"]==Organization::COLLECTION && !empty($data['postParams'])
            && !empty($data['postParams']['links']) ){

            $answersID = null;
            $cter = null;
            if( !empty($data['postParams']['links']['answers']) ){
                if(!is_array($data['postParams']['links']['answers']))
                    $data['postParams']['links']['answers'] = [$data['postParams']['links']['answers'] => []];

                
                foreach ($data['postParams']['links']['answers'] as $keyA => $valA) {
                    //links.answers rattaché l'orga à la réponse
                    Link::connect($data["id"], Organization::COLLECTION, $keyA, Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
                    $answersID = $keyA ;
                }
                
            }


            if( !empty($data['postParams']['links']['projects']) ){
                if(!is_array($data['postParams']['links']['projects']))
                    $data['postParams']['links']['projects'] = [$data['postParams']['links']['projects'] => []];

                foreach ($data['postParams']['links']['projects'] as $keyP => $valP) 
                {
                    $project =  PHDB::findOneById(Project::COLLECTION, $keyP, array("category", "name", "slug"));
                    if(!empty($project["category"]) && $project["category"] == "cteR")
                        $cter = $project["slug"];   
                }
            }
            
            //var_dump($data);exit;
            if(!empty($data["id"]) && !empty($cter) && !empty($data['postParams']['links']['answers'])){
                
                //on create
                $path = "answers.action.parents";
                $verb = '$addToSet';
                
                if(!isset($answer))
                    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answersID, array("answers"));
                
                if(isset($answer["answers"]["action"]["parents"] )){
                    foreach ($answer["answers"]["action"]["parents"] as $i => $o) {
                        if( $o["id"] == $data["id"] ){
                            // on update
                            $path = "answers.action.parents.".$i;
                            $verb = '$set';
                        }
                    }
                }

                $set = [ $path => 
                            [ "type" => $data["collection"], 
                              "id" => $data["id"],
                              "name" => $data["params"]["name"],
                              "slug" => $data["params"]["slug"],
                              "typeStruct" => $data["params"]["typeStruct"] ] ] ;
                
                //var_dump($verb);
                // commenté pour ne pas faire de doublon, le lien est fait par le javascript
                // PHDB::update( Form::ANSWER_COLLECTION,
                //             [ "_id" => new MongoId( $answersID ) ] , 
                //             [ $verb => $set]);
                            
            }
        }
    }

    public static function surveyAfterSave($data){
    	//var_dump("surveyAfterSave");
    	if($data["collection"]==Form::ANSWER_COLLECTION && 
            !empty($data["id"])
    		/* !empty($data["params"]) && 
    		!empty($data["params"]["answers"]) && 
    		!empty($data["params"]["answers"]["action"]["project"][0]) && 
    		!empty($data["params"]["answers"]["action"]["project"][0]["id"])&& 
    		!empty($data["params"]["answers"]["action"]["organization"][0]) && 
    		!empty($data["params"]["answers"]["action"]["organization"][0]["id"]) */ ){
            $upMap = [ "priorisation" => "Action Candidate"];

            //add scope and adress to addressed
            if($data["cterSlug"]){
                $cter = PHDB::findOne( Project::COLLECTION, ["slug" => $data["cterSlug"] ] );
                if(isset($cter['address']))
                    $upMap['address'] = $cter['address'];
                if(isset($cter['scope']))
                    $upMap['scope'] = $cter['scope'];
                if(isset($cter['geo']))
                    $upMap['geo'] = $cter['geo'];
                if(isset($cter['geoPosition']))
                    $upMap['geoPosition'] = $cter['geoPosition'];
            }
            
            PHDB::update(   $data["collection"], 
                                ["_id" => new MongoId($data["id"])], 
                                ['$set' => $upMap ] );

            //$cterPorteur = Slug::getElementBySlug($data["params"]["formId"]);

    		//$pElt = $data["params"]["answers"]["action"]["project"][0];
    		//$elt=Element::getElementById($pElt["id"], Project::COLLECTION, null, array("links", "category"));
    		//$org = $data["params"]["answers"]["action"]["organization"][0];

            //links.contributors rattaché le projet action à l'organization
    		//Link::connect($pElt["id"], Project::COLLECTION, $org["id"], Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Porteur d'action"]);

            // //links.projects rattaché le projet action au projet cter associé
            // Link::connect($pElt["id"], Project::COLLECTION, $cterPorteur["id"], $cterPorteur["type"], Yii::app()->session["userId"], "projects",false,false,false,false, ["CTE Porteur"]);

            // //links.projects rattaché le projet action au projet cter associé
            // Link::connect($cterPorteur["id"], $cterPorteur["type"],$pElt["id"], Project::COLLECTION,  Yii::app()->session["userId"], "projects",false,false,false,false, ["Action CTE"]);

            //links.contributors rattaché le projet action à l'auteur
            //Link::connect($pElt["id"], Project::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Créateur d'action"]);

            //links.answers rattaché le projet à la réponse
            //Link::connect($pElt["id"], Project::COLLECTION, $data['id'], Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
	    	
            //links.projects rattaché le projet à l'orga porteuse 
            //Link::connect($org["id"], Organization::COLLECTION, $pElt["id"], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false, ["Porteur d'action"]);
            
            //links.answers rattaché l'orga à la réponse
            //Link::connect($org["id"], Organization::COLLECTION, $data["id"], Form::ANSWER_COLLECTION, Yii::app()->session["userId"], "answers",false,false,false,false);
    		
    	}
    }

    public static  function importPreviewData($post, $notCheck=false){

        $params = array("result"=>false);
        $elements = array();
        $saveCities = array();
        $headFile = array();
        $nb = 0 ;
        $elementsWarnings = array();
        //Rest::json($post['file']); exit;
        
        if( !empty($post['file']) && !empty($post['nameFile']) && !empty($post['typeFile']) ){
            // $mapping = json_decode(file_get_contents("../../modules/co2/data/import/".Element::getControlerByCollection($post["typeElement"]).".json", FILE_USE_INCLUDE_PATH), true);
            if($post['typeFile'] == "csv"){
                $file = $post['file'];

                $headFile = $file[0];
                unset($file[0]);
            }elseif ((!isset($post['pathObject'])) || ($post['pathObject'] == "")) {
                $file = json_decode($post['file'][0], true);
            }else {
                $file = json_decode($post['file'][0], true);
                $file = @$file[$post["pathObject"]];
            }

            //Rest::json($headFile); exit;
            //Rest::json($file); exit;
            $source =  array("source.key" => "ctenat");
            $cteterList = array();
            $cteterListName = array();
            $cteterListNameErreur = array();

            $porteurTerList = array();
            $porteurTerName = array();
            $projectList = array();
            $addressList = array();
            
            $sourceU = array("insertOrign" => "import",
								"key" => "ctenat",
								"keys" => array("ctenat") );
            $bigboss = PHDB::findOne(Person::COLLECTION, array("email" => "antoine.daval@gmail.com"));
            $listType = array(
				"entreprise" =>	Organization::TYPE_BUSINESS,
				"agriculteur / groupements" =>	Organization::TYPE_BUSINESS,
				"Enseignement" =>	Organization::TYPE_GOV,
				"enseignement" =>	Organization::TYPE_GOV,
				"EPCI" =>	Organization::TYPE_GOV,
				"Préfecture" =>	Organization::TYPE_GOV,
				"préfecture" =>	Organization::TYPE_GOV,
				"DR" =>	Organization::TYPE_GOV,
				"ADEME" =>	Organization::TYPE_GOV,
				"bailleur" =>	Organization::TYPE_GOV,
				"Chambre consulaire / fédérations profesionnelles" =>	Organization::TYPE_GOV,
				"syndicat" =>	Organization::TYPE_NGO,
				"Syndicat" =>	Organization::TYPE_NGO,
				"VNF" =>	Organization::TYPE_GOV,
				"Commune"  =>	Organization::TYPE_GOV,
				"commune" =>	Organization::TYPE_GOV,
				"Groupement EPCI" =>	Organization::TYPE_GOV,
				"groupement EPCI" =>	Organization::TYPE_GOV,
				"association" =>	Organization::TYPE_NGO,
				"Association" =>	Organization::TYPE_NGO,
				"région" =>	Organization::TYPE_GOV,
				"Région" =>	Organization::TYPE_GOV,
				"établissement public" =>	Organization::TYPE_GOV,
				"Transport" =>	Organization::TYPE_GOV,
				"SCI" =>	Organization::TYPE_GOV,
				"SEM" =>	Organization::TYPE_BUSINESS,
				"département" =>	Organization::TYPE_GOV,
				"mixte" =>	Organization::TYPE_GOV,
				"Mixte" =>	Organization::TYPE_GOV,
				"DDT" =>	Organization::TYPE_GOV,
				"Pays" =>	Organization::TYPE_GOV,
				"groupement communes" =>	Organization::TYPE_GOV,
				"Département" =>	Organization::TYPE_GOV,
			);

            if(!empty($file)){
            	//Rest::json($file); exit;
            	foreach ($file as $keyFile => $vFile){
					$nb++;

					if(!empty($vFile)){
						//************* Gestion CTETER
						if( !isset($cteterListName[$vFile[0]]) ){

							$where = $source;
							$where["name"] = $vFile[0];
							$cteter = PHDB::findOne(Project::COLLECTION, $where);
							if(!empty($cteter)){
								$cteterListName[$vFile[0]] = (String)$cteter["_id"];
								$cteterList[(String)$cteter["_id"]] = $cteter;

								//************* Gestion address CTETER
								if(!empty($vFile[1])){
									$strA =$vFile[1].$vFile[2].$vFile[3].$vFile[4];
									if(empty($cteter["address"]) && !empty($strA)){
										if(!isset($addressList[$vFile[1].$vFile[2].$vFile[3].$vFile[4]])){
											$address = array(
												"addressCountry" => $vFile[1],
												"postalCode" => $vFile[2],
												"addressLocality" => $vFile[3],
												"streetAddress" => $vFile[4]
											);

											$addressList[$strA] = array();
											$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								            if($detailsLocality["result"] == true){
								                $cteter["address"] = $detailsLocality["address"] ;
								                $cteter["geo"] = $detailsLocality["geo"] ;
								                $cteter["geoPosition"] = $detailsLocality["geoPosition"] ;

								                $addressList[$strA] = array(
								                	"address" => $detailsLocality["address"],
									                "geo" => $detailsLocality["geo"],
									                "geoPosition" => $detailsLocality["geoPosition"]
								                );

								                PHDB::update( Project::COLLECTION,
												    array("_id"=>new MongoId( (String)$cteter["_id"]) ), 
													array('$set' => array( "address" => $cteter["address"] ,
																			"geo" => $cteter["geo"] ,
																			"geoPosition" => $cteter["geoPosition"]) ) );
								            }
										}else if(!empty($addressList[$strA]["adresse"])){
											PHDB::update( Project::COLLECTION,
												    array("_id"=>new MongoId( (String)$cteter["_id"]) ), 
													array('$set' => array( "address" => $addressList[$strA]["address"] ,
																			"geo" => $addressList[$strA]["geo"] ,
																			"geoPosition" => $addressList[$strA]["geoPosition"]) ) );
										}
									}
								}
								
								// //************* Gestion porteur CTETER
								// if(!isset($porteurTerName[$vFile[5]]) && !empty($vFile[5]) ){
								// 	$whereP = $source;
								// 	$whereP["name"] = $vFile[5];
								// 	$porteurTer1 = PHDB::findOne(Organization::COLLECTION, $whereP);
								// 	if(!empty($porteurTer1)){
								// 		$porteurTerName[$vFile[5]] = (String)$porteurTer1["_id"];
								// 		$porteurTerList[(String)$porteurTer1["_id"]] = $porteurTer1;
								// 	}else{
								// 		$porteurTerName[$vFile[5]] = array();
								// 		$eltPorteurTer = array(	"name" => $vFile[5],
								// 								"type" => Organization::TYPE_GOV,
								// 								"collection" => Organization::COLLECTION,
								// 								"source" => $sourceU );
								// 		if(!empty($vFile[6])){
								// 			$strA =$vFile[6].$vFile[7].$vFile[8].$vFile[9];
								// 			if(!empty($strA)){
								// 				if(!isset($addressList[$strA])){
								// 					$address = array(
								// 						"addressCountry" => $vFile[6],
								// 						"postalCode" => $vFile[7],
								// 						"addressLocality" => $vFile[8],
								// 						"streetAddress" => $vFile[9]
								// 					);

								// 					$addressList[$strA] = array();
								// 					$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								// 		            if($detailsLocality["result"] == true){
								// 						$eltPorteurTer["address"] = $detailsLocality["address"] ;
								// 						$eltPorteurTer["geo"] = $detailsLocality["geo"] ;
								// 						$eltPorteurTer["geoPosition"] = $detailsLocality["geoPosition"] ;

								// 						$addressList[$strA] = array(
								// 							"address" => $detailsLocality["address"],
								// 							"geo" => $detailsLocality["geo"],
								// 							"geoPosition" => $detailsLocality["geoPosition"]
								// 						);
								// 		            }
								// 				}else if(!empty($addressList[$strA]["adresse"])){
								// 					$eltPorteurTer["address"] = $addressList[$strA]["address"] ;
								// 	                $eltPorteurTer["geo"] = $addressList[$strA]["geo"] ;
								// 	                $eltPorteurTer["geoPosition"] = $addressList[$strA]["geoPosition"] ;
								// 				}
												
								// 			}
								// 		}
								// 		//Rest::json($eltPorteurTer); exit;
								// 		if(!empty($eltPorteurTer) && !empty($eltPorteurTer["name"]) )
								// 			$res = Element::save($eltPorteurTer);
								// 		else{
								// 			var_dump($vFile[5]);
								// 			var_dump($eltPorteurTer);
								// 		}
								// 		//
								// 		if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
								// 			$porteurTerName[$vFile[5]] = $res["id"];
								// 			$porteurTer1 = $res["map"];
								// 			$porteurTerList[$res["id"]] = $res["map"];
								// 		}
								// 	}
								// } else {
								// 	if(!empty($vFile[5]))
								// 		$porteurTerName[$vFile[5]] = array();
								// }

								// //************* Gestion porteur CTETER 2
								// if(!isset($porteurTerName[$vFile[12]]) ){
								// 	$whereP = $source;
								// 	$whereP["name"] = $vFile[12];
								// 	$porteurTer2 = PHDB::findOne(Organization::COLLECTION, $whereP);
								// 	if(!empty($porteurTer2)){
								// 		$porteurTerName[$vFile[12]] = (String)$porteurTer2["_id"];
								// 		$porteurTerList[(String)$porteurTer2["_id"]] = $porteurTer2;
								// 	}else if(!empty($vFile[12])){
								// 		$porteurTerName[$vFile[12]] = array();
								// 		$eltPorteurTer = array(	"name" => $vFile[12],
								// 								"type" => Organization::TYPE_GOV,
								// 								"collection" => Organization::COLLECTION,
								// 								"source" => $sourceU );
								// 		if(!empty($vFile[13])){
								// 			$strA =$vFile[13].$vFile[14].$vFile[15].$vFile[16];
								// 			if(!empty($strA)){
								// 				if(!isset($addressList[$strA])){
								// 					$address = array(
								// 						"addressCountry" => $vFile[13],
								// 						"postalCode" => $vFile[14],
								// 						"addressLocality" => $vFile[15],
								// 						"streetAddress" => $vFile[16]
								// 					);

								// 					$addressList[$strA] = array();
								// 					$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								// 		            if($detailsLocality["result"] == true){
								// 						$eltPorteurTer["address"] = $detailsLocality["address"] ;
								// 						$eltPorteurTer["geo"] = $detailsLocality["geo"] ;
								// 						$eltPorteurTer["geoPosition"] = $detailsLocality["geoPosition"] ;

								// 						$addressList[$strA] = array(
								// 							"address" => $detailsLocality["address"],
								// 							"geo" => $detailsLocality["geo"],
								// 							"geoPosition" => $detailsLocality["geoPosition"]
								// 						);
								// 		            }
								// 				}else if(!empty($addressList[$strA]["adresse"])){
								// 					$eltPorteurTer["address"] = $addressList[$strA]["address"] ;
								// 	                $eltPorteurTer["geo"] = $addressList[$strA]["geo"] ;
								// 	                $eltPorteurTer["geoPosition"] = $addressList[$strA]["geoPosition"] ;
								// 				}
												
								// 			}
								// 		}
								// 		//Rest::json($eltPorteurTer); exit;

								// 		$res = Element::save($eltPorteurTer);
								// 		//
								// 		if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
								// 			$porteurTerName[$vFile[12]] = $res["id"];
								// 			$porteurTer2 = $res["map"];
								// 			$porteurTerList[$res["id"]] = $res["map"];
								// 		}
								// 	}
								// } else {
								// 	$porteurTerName[$vFile[12]] = array();
								// }

								// //************* Gestion porteur CTETER 3
								// if(!isset($porteurTerName[$vFile[19]]) ){
								// 	$whereP = $source;
								// 	$whereP["name"] = $vFile[19];
								// 	$porteurTer3 = PHDB::findOne(Organization::COLLECTION, $whereP);
								// 	if(!empty($porteurTer3)){
								// 		$porteurTerName[$vFile[19]] = (String)$porteurTer3["_id"];
								// 		$porteurTerList[(String)$porteurTer3["_id"]] = $porteurTer3;
								// 	}else if(!empty($vFile[19])){
								// 		$porteurTerName[$vFile[19]] = array();
								// 		$eltPorteurTer = array(	"name" => $vFile[19],
								// 								"type" => Organization::TYPE_GOV,
								// 								"collection" => Organization::COLLECTION,
								// 								"source" => $sourceU );
								// 		if(!empty($vFile[20])){
								// 			$strA =$vFile[20].$vFile[21].$vFile[22].$vFile[23];
								// 			if(!empty($strA)){
								// 				if(!isset($addressList[$strA])){
								// 					$address = array(
								// 						"addressCountry" => $vFile[20],
								// 						"postalCode" => $vFile[21],
								// 						"addressLocality" => $vFile[22],
								// 						"streetAddress" => $vFile[23]
								// 					);

								// 					$addressList[$strA] = array();
								// 					$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
								// 		            if($detailsLocality["result"] == true){
								// 						$eltPorteurTer["address"] = $detailsLocality["address"] ;
								// 						$eltPorteurTer["geo"] = $detailsLocality["geo"] ;
								// 						$eltPorteurTer["geoPosition"] = $detailsLocality["geoPosition"] ;

								// 						$addressList[$strA] = array(
								// 							"address" => $detailsLocality["address"],
								// 							"geo" => $detailsLocality["geo"],
								// 							"geoPosition" => $detailsLocality["geoPosition"]
								// 						);
								// 		            }
								// 				}else if(!empty($addressList[$strA]["adresse"])){
								// 					$eltPorteurTer["address"] = $addressList[$strA]["address"] ;
								// 	                $eltPorteurTer["geo"] = $addressList[$strA]["geo"] ;
								// 	                $eltPorteurTer["geoPosition"] = $addressList[$strA]["geoPosition"] ;
								// 				}
												
								// 			}
								// 		}
								// 		//Rest::json($eltPorteurTer); exit;

								// 		$res = Element::save($eltPorteurTer);
								// 		//
								// 		if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
								// 			$porteurTerName[$vFile[19]] = $res["id"];
								// 			$porteurTer3 = $res["map"];
								// 			$porteurTerList[$res["id"]] = $res["map"];
								// 		}
								// 	}
								// } else {
								// 	$porteurTerName[$vFile[19]] = array();
								// }

								

								// //************* LINK porteur CTETER AND CTETER
								// if(!empty($porteurTer1) && !empty($porteurTer1["_id"]) ){
								// 	if(empty($porteurTer1["links"]))
								// 		$porteurTer1["links"] = array();

								// 	if(empty($porteurTer1["links"]["projects"]))
								// 		$porteurTer1["links"]["projects"] = array();

								// 	if(empty($porteurTer1["links"]["projects"][(String)$cteter["_id"]]) ) {
								// 		$porteurTer1["links"]["projects"][(String)$cteter["_id"]] = array( "type" => Project::COLLECTION);

								// 		PHDB::update( Organization::COLLECTION,
								// 		    array("_id"=>new MongoId( (String)$porteurTer1["_id"]) ), 
								// 			array('$set' => array( "links" => $porteurTer1["links"] ) ) );

								// 		if(!empty($porteurTer1) && !empty($vFile[5]) ){
								// 			if(empty($porteurTerName[$vFile[5]]))
								// 				$porteurTerName[$vFile[5]] = (String)$porteurTer1["_id"] ;
								// 			$porteurTerList[$porteurTerName[$vFile[5]]] = $porteurTer1;
								// 		}
										
								// 	}

								// 	$linksUser = (!empty($vFile[11]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[11])): null );
								// 	if(empty($linksUser))
								// 		$linksUser = $bigboss;
								// 	//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
								// 	if(!empty($linksUser)){
								// 		$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
								// 		Link::connectParentToChild( (String)$porteurTer1["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
								// 	}
								// }


								// if(!empty($porteurTer2) && !empty($porteurTer2["_id"])){

								// 	// Rest::json($porteurTer2);
								// 	if(empty($porteurTer2["links"]))
								// 		$porteurTer2["links"] = array();

								// 	if(empty($porteurTer2["links"]["projects"]))
								// 		$porteurTer2["links"]["projects"] = array();

								// 	if(empty($porteurTer2["links"]["projects"][(String)$cteter["_id"]]) ) {
								// 		$porteurTer2["links"]["projects"][(String)$cteter["_id"]] = array( "type" => Project::COLLECTION);

								// 		PHDB::update( Organization::COLLECTION,
								// 		    array("_id"=>new MongoId( (String)$porteurTer2["_id"]) ), 
								// 			array('$set' => array( "links" => $porteurTer2["links"] ) ) );

								// 		if(!empty($porteurTer2) && !empty($vFile[12]) ){
								// 			if(empty($porteurTerName[$vFile[12]]))
								// 				$porteurTerName[$vFile[12]] = (String)$porteurTer2["_id"] ;
								// 			$porteurTerList[$porteurTerName[$vFile[12]]] = $porteurTer2;
								// 		}
								// 		else{
								// 			var_dump($vFile[12]);
								// 			var_dump($porteurTerName[$vFile[12]]);
								// 			Rest::json($porteurTer2);
								// 		}
								// 	}

								// 	$linksUser = (!empty($vFile[18]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[18])) : null );
								// 	if(empty($linksUser))
								// 		$linksUser = $bigboss;
								// 	//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
								// 	if(!empty($linksUser)){
								// 		$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
								// 		//Rest::json($child); exit;
								// 		Link::connectParentToChild( (String)$porteurTer2["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
								// 	}
								// }

								// if(!empty($porteurTer3) && !empty($porteurTer3["_id"])){
								// 	if(empty($porteurTer3["links"]))
								// 		$porteurTer3["links"] = array();

								// 	if(empty($porteurTer3["links"]["projects"]))
								// 		$porteurTer3["links"]["projects"] = array();

								// 	if(empty($porteurTer3["links"]["projects"][(String)$cteter["_id"]]) ) {
								// 		$porteurTer3["links"]["projects"][(String)$cteter["_id"]] = array( "type" => Project::COLLECTION);

								// 		PHDB::update( Organization::COLLECTION,
								// 		    array("_id"=>new MongoId( (String)$porteurTer3["_id"]) ), 
								// 			array('$set' => array( "links" => $porteurTer3["links"] ) ) );

								// 		$porteurTerList[$porteurTerName[$vFile[19]]] = $porteurTer3;
								// 	}

								// 	$linksUser = (!empty($vFile[25]) ?  PHDB::findOne(Person::COLLECTION, array("email" => $vFile[25])): null );
								// 	if(empty($linksUser))
								// 		$linksUser = $bigboss;
								// 	//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
								// 	if(!empty($linksUser)){
								// 		$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
								// 		Link::connectParentToChild( (String)$porteurTer3["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
								// 	}
								// }


								// if(empty($cteter["parent"]))
								// 	$cteter["parent"] = array();

								// if(!empty($porteurTer1) && empty($cteter["parent"][(String)$porteurTer1["_id"]]) ) {
								// 	$cteter["parent"][(String)$porteurTer1["_id"]] = array( "type" => Organization::COLLECTION);
								// }

								// if(!empty($porteurTer2) && empty($cteter["parent"][(String)$porteurTer2["_id"]]) ) {
								// 	$cteter["parent"][(String)$porteurTer2["_id"]] = array( "type" => Organization::COLLECTION);
								// }

								// if(!empty($porteurTer3) && empty($cteter["parent"][(String)$porteurTer3["_id"]]) ) {
								// 	$cteter["parent"][(String)$porteurTer3["_id"]] = array( "type" => Organization::COLLECTION);
								// }

								// if(count($cteter["parent"]) > 0){
								// 	PHDB::update( Project::COLLECTION,
								// 	    array("_id"=>new MongoId( (String)$cteter["_id"]) ), 
								// 		array('$set' => array( "parent" => $cteter["parent"] ) ) );
								// }
								

							} else {
								if(!in_array($vFile[0], $cteterListNameErreur))
									$cteterListNameErreur[]= $vFile[0];
							}
						} else {
							$cteter = $cteterList[$cteterListName[$vFile[0]]] ;
						}
						//************* ANSWER
						$aPrincipal = "";
						$aSecondaire = array();
						$cibleDD = array();
						if(!empty( $vFile[78]))
							$aPrincipal = $vFile[78];
						if(!empty( $vFile[79]))
							$aSecondaire[] = $vFile[79];
						if(!empty( $vFile[80]))
							$aSecondaire[] = $vFile[80];
						if(!empty( $vFile[81]))
							$cibleDD[] = $vFile[81];
						if(!empty( $vFile[82]))
							$cibleDD[] = $vFile[82];
						if(!empty( $vFile[83]))
							$cibleDD[] = $vFile[83];

						if(!empty($cteter["slug"])){
							$answers = array(
								"formId" => $cteter["slug"],
								"session" => "1",
								"user" => (String)$bigboss["_id"],
								"name" => $bigboss["name"],
								"email" => $bigboss["email"],
								"answers" => array(
									"caracter" => array(
										"actionPrincipal" => $aPrincipal,
										"actionSecondaire" => $aSecondaire,
										"cibleDDPrincipal" => $cibleDD ),
									"financement"  => array(
										"depensesfonctionnement" => array(
											"label" => "Dépenses de fonctionnement",
											"total" => ( !empty($vFile[58]) ? floatval(str_replace(" ", "", $vFile[58])) : "" )
										),
										"depensesinvestissement" => array(
											"label" => "Dépenses d'investissement",
											"total" => ( !empty($vFile[59]) ? floatval(str_replace(" ", "", $vFile[59])) : "" )
										),"totaldepenses" => array(
											"label" => "Total des dépenses",
											"total" => ( !empty($vFile[60]) ? floatval(str_replace(" ", "", $vFile[60])) : "" )
										),"totalfinancements" => array(
											"label" => "Total des financements",
											"total" => ( !empty($vFile[61]) ? floatval(str_replace(" ", "", $vFile[61])) : "" )
										),"financeursnonprecises" => array(
											"label" => "Financeurs non précisés",
											"total" => ( !empty($vFile[63]) ? floatval(str_replace(" ", "", $vFile[63])) : "" )
										),"autofinancement" => array(
											"label" => "Acteurs économiques (auto-financement)",
											"total" => ( !empty($vFile[64]) ? floatval(str_replace(" ", "", $vFile[64])) : "" )
										),"financeur" => array(
											"label" => "Acteurs économiques (financeur)",
											"total" => ( !empty($vFile[65]) ? floatval(str_replace(" ", "", $vFile[65])) : "" )
										),"colautofinancement)" => array(
											"label" => "Collectivité (auto-financement)",
											"total" => ( !empty($vFile[66]) ? floatval(str_replace(" ", "", $vFile[66])) : "" )
										),"colfinanceur" => array(
											"label" => "collectivité (financeur)",
											"total" => ( !empty($vFile[67]) ? floatval(str_replace(" ", "", $vFile[67])) : "" )
										),"dep" => array(
											"label" => "Département",
											"total" => ( !empty($vFile[68]) ? floatval(str_replace(" ", "", $vFile[68])) : "" )
										),"region" => array(
											"label" => "Région",
											"total" => ( !empty($vFile[69]) ? floatval(str_replace(" ", "", $vFile[69])) : "" )
										),"etat" => array(
											"label" => "Etat (DSIL, villes respirables, CPER,AFD,FNADT)",
											"total" => ( !empty($vFile[70]) ? floatval(str_replace(" ", "", $vFile[70])) : "" )
										),"ademe" => array(
											"label" => "ADEME",
											"total" => ( !empty($vFile[71]) ? floatval(str_replace(" ", "", $vFile[71])) : "" )
										),"caisseconsignations" => array(
											"label" => "Caisse des Consignations",
											"total" => ( !empty($vFile[72]) ? floatval(str_replace(" ", "", $vFile[72])) : "" )
										),"banque" => array(
											"label" => "Banque",
											"total" => ( !empty($vFile[73]) ? floatval(str_replace(" ", "", $vFile[73])) : "" )
										),"europe" => array(
											"label" => "Europe (FEDER, FEAMP)",
											"total" => ( !empty($vFile[74]) ? floatval(str_replace(" ", "", $vFile[74])) : "" )
										),"chambresconsulaires" => array(
											"label" => "Chambres consulaires",
											"total" => ( !empty($vFile[75]) ? floatval(str_replace(" ", "", $vFile[75])) : "" )
										),"agencedeleau" => array(
											"label" => "Agence de l'eau",
											"total" => ( !empty($vFile[76]) ? floatval(str_replace(" ", "", $vFile[76])) : "" )
										),"fondations" => array(
											"label" => "Fondations, crownfounding",
											"total" => ( !empty($vFile[77]) ? floatval(str_replace(" ", "", $vFile[77])) : "" )
										) ) ) ,
								"source" => $sourceU 
							);
							//Create Project ANSWERS
							
							if(!empty($vFile[26])){

								$where = $source;
								$where["name"] = $vFile[26];
								$projectA = PHDB::findOne(Project::COLLECTION, $where);
								if(empty($projectA)){


									$projectA = array(	"name" => $vFile[26],
														"collection" => Project::COLLECTION,
														"category" => "ficheAction",
														"preferences" => array(
															"private" => true
														),
														"source" => $sourceU );

									if(!empty($vFile[27])){
										$strA =$vFile[27].$vFile[28].$vFile[29].$vFile[30];
										if(!empty($strA)){
											if(!isset($addressList[$strA])){
												$address = array(
													"addressCountry" => $vFile[27],
													"postalCode" => $vFile[28],
													"addressLocality" => $vFile[29],
													"streetAddress" => $vFile[30]
												);

												$addressList[$strA] = array();
												$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
									            if($detailsLocality["result"] == true){
													$projectA["address"] = $detailsLocality["address"] ;
													$projectA["geo"] = $detailsLocality["geo"] ;
													$projectA["geoPosition"] = $detailsLocality["geoPosition"] ;

													$addressList[$strA] = array(
														"address" => $detailsLocality["address"],
														"geo" => $detailsLocality["geo"],
														"geoPosition" => $detailsLocality["geoPosition"]
													);
									            }
											}else if(!empty($addressList[$strA]["adresse"])){
												$projectA["address"] = $addressList[$strA]["address"] ;
								                $projectA["geo"] = $addressList[$strA]["geo"] ;
								                $projectA["geoPosition"] = $addressList[$strA]["geoPosition"] ;
											}
											
										}
									}
									
									
									$res = Element::save($projectA);
									

									if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
										//$projectList[$vFile[5]] = $res["id"];
										$projectA = $res["map"];
										$projectList[$res["id"]] = $res["map"];
									}
								} else {
									$projectList[(String)$projectA["_id"]] = $projectA;
								}



								if(!empty($projectA)){
									$child = array("childId" => (String)$projectA["_id"], "childType" => Project::COLLECTION);
									Link::connectParentToChild( (String)$cteter["_id"], Project::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;

									$answers["answers"]["action"]["project"][0] = array(
										"type" => Project::COLLECTION,
										"id" => (String) $projectA["_id"],
										"name" => $projectA["name"],
										"email" => ( ( !empty($projectA["email"]) ) ? $projectA["email"] : "" )
									);
								}



								//Create Porteur ANSWERS
								$eltPorteurA1 = array();
								if(!empty($vFile[31]) && !empty($vFile[33])){
									if(!empty($porteurTerName) && !empty($porteurTerName[$vFile[31]])){
										$eltPorteurA1 = $porteurTerList[$porteurTerName[$vFile[31]]];
									} else {

										$where = $source;
										$where["name"] = $vFile[31];
										$eltPorteurA1 = PHDB::findOne(Organization::COLLECTION, $where);
										if(empty($eltPorteurA1)){
											$porteurTerName[$vFile[31]] = array();
											$eltPorteurA1 = array(	"name" => $vFile[31],
																	"type" => $listType[$vFile[33]],
																	"collection" => Organization::COLLECTION,
																	"source" => $sourceU,
																	"tags" => array($vFile[32], $vFile[33]) );
											if(!empty($vFile[34])){
												$strA =$vFile[34].$vFile[35].$vFile[36].$vFile[37];
												if(!empty($strA)){
													if(!isset($addressList[$strA])){
														$address = array(
															"addressCountry" => $vFile[34],
															"postalCode" => $vFile[35],
															"addressLocality" => $vFile[36],
															"streetAddress" => $vFile[37]
														);

														$addressList[$strA] = array();
														$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
											            if($detailsLocality["result"] == true){
															$eltPorteurA1["address"] = $detailsLocality["address"] ;
															$eltPorteurA1["geo"] = $detailsLocality["geo"] ;
															$eltPorteurA1["geoPosition"] = $detailsLocality["geoPosition"] ;

															$addressList[$strA] = array(
																"address" => $detailsLocality["address"],
																"geo" => $detailsLocality["geo"],
																"geoPosition" => $detailsLocality["geoPosition"]
															);
											            }
													}else if(!empty($addressList[$strA]["adresse"])){
														$eltPorteurA1["address"] = $addressList[$strA]["address"] ;
										                $eltPorteurA1["geo"] = $addressList[$strA]["geo"] ;
										                $eltPorteurA1["geoPosition"] = $addressList[$strA]["geoPosition"] ;
													}
													
												}
											}

											$res = Element::save($eltPorteurA1);
												//
											if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
												$porteurTerName[$vFile[31]] = $res["id"];
												$eltPorteurA1 = $res["map"];
												$porteurTerList[$res["id"]] = $res["map"];
											}
										} else {
											$projectList[(String)$eltPorteurA1["_id"]] = $eltPorteurA1;
											
										}
									}
								}
								

								//Create Porteur ANSWERS 2
								$eltPorteurA2 = array();
								if(!empty($vFile[40]) && !empty($vFile[42])){
									if(!empty($porteurTerName) && !empty($porteurTerName[$vFile[40]])){
										$eltPorteurA2 = $porteurTerList[$porteurTerName[$vFile[40]]];
									} else {
										$where = $source;
										$where["name"] = $vFile[40];
										$eltPorteurA2 = PHDB::findOne(Organization::COLLECTION, $where);
										if(empty($eltPorteurA2)){
											$porteurTerName[$vFile[40]] = array();
											$eltPorteurA2 = array(	"name" => $vFile[40],
																	"type" => $listType[$vFile[42]],
																	"collection" => Organization::COLLECTION,
																	"source" => $sourceU,
																	"tags" => array($vFile[41], $vFile[42]) );
											if(!empty($vFile[43])){
												$strA =$vFile[43].$vFile[44].$vFile[45].$vFile[46];
												if(!empty($strA)){
													if(!isset($addressList[$strA])){
														$address = array(
															"addressCountry" => $vFile[43],
															"postalCode" => $vFile[44],
															"addressLocality" => $vFile[45],
															"streetAddress" => $vFile[46]
														);

														$addressList[$strA] = array();
														$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
											            if($detailsLocality["result"] == true){
															$eltPorteurA2["address"] = $detailsLocality["address"] ;
															$eltPorteurA2["geo"] = $detailsLocality["geo"] ;
															$eltPorteurA2["geoPosition"] = $detailsLocality["geoPosition"] ;

															$addressList[$strA] = array(
																"address" => $detailsLocality["address"],
																"geo" => $detailsLocality["geo"],
																"geoPosition" => $detailsLocality["geoPosition"]
															);
											            }
													}else if(!empty($addressList[$strA]["adresse"])){
														$eltPorteurA2["address"] = $addressList[$strA]["address"] ;
										                $eltPorteurA2["geo"] = $addressList[$strA]["geo"] ;
										                $eltPorteurA2["geoPosition"] = $addressList[$strA]["geoPosition"] ;
													}
													
												}
											}

											$res = Element::save($eltPorteurA2);
												//
											if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
												$porteurTerName[$vFile[40]] = $res["id"];
												$eltPorteurA2 = $res["map"];
												$porteurTerList[$res["id"]] = $res["map"];
											}
										} else {
											$projectList[(String)$eltPorteurA2["_id"]] = $eltPorteurA2;
											
										}
									}
								}
								

								//Create Porteur ANSWERS 3
								$eltPorteurA3 = array();
								if(!empty($vFile[49]) && !empty($vFile[51])){
									if(!empty($porteurTerName) && !empty($porteurTerName[$vFile[49]])){
										$eltPorteurA3 = $porteurTerList[$porteurTerName[$vFile[49]]];
									} else {

										$where = $source;
										$where["name"] = $vFile[49];
										$eltPorteurA3 = PHDB::findOne(Organization::COLLECTION, $where);
										if(empty($eltPorteurA3)){
											$porteurTerName[$vFile[49]] = array();
											$eltPorteurA3 = array(	"name" => $vFile[49],
																	"type" => $listType[$vFile[51]],
																	"collection" => Organization::COLLECTION,
																	"source" => $sourceU,
																	"tags" => array($vFile[50], $vFile[51]) );
											if(!empty($vFile[52])){
												$strA =$vFile[52].$vFile[53].$vFile[54].$vFile[55];
												if(!empty($strA)){
													if(!isset($addressList[$strA])){
														$address = array(
															"addressCountry" => $vFile[52],
															"postalCode" => $vFile[53],
															"addressLocality" => $vFile[54],
															"streetAddress" => $vFile[55]
														);

														$addressList[$strA] = array();
														$detailsLocality = Import::getAndCheckAddressForEntity($address, null) ;
											            if($detailsLocality["result"] == true){
															$eltPorteurA3["address"] = $detailsLocality["address"] ;
															$eltPorteurA3["geo"] = $detailsLocality["geo"] ;
															$eltPorteurA3["geoPosition"] = $detailsLocality["geoPosition"] ;

															$addressList[$strA] = array(
																"address" => $detailsLocality["address"],
																"geo" => $detailsLocality["geo"],
																"geoPosition" => $detailsLocality["geoPosition"]
															);
											            }
													}else if(!empty($addressList[$strA]["adresse"])){
														$eltPorteurA3["address"] = $addressList[$strA]["address"] ;
										                $eltPorteurA3["geo"] = $addressList[$strA]["geo"] ;
										                $eltPorteurA3["geoPosition"] = $addressList[$strA]["geoPosition"] ;
													}
													
												}
											}

											$res = Element::save($eltPorteurA3);
												//
											if(!empty($res) && !empty($res["result"]) && $res["result"] == true ){
												$porteurTerName[$vFile[49]] = $res["id"];
												$eltPorteurA3 = $res["map"];
												$porteurTerList[$res["id"]] = $res["map"];
											}
										} else {
											$projectList[(String)$eltPorteurA3["_id"]] = $eltPorteurA3;
										}
									}
								}

								if(!empty($eltPorteurA1)){
									$answers["answers"]["action"]["parents"][0] = array(
										"type" => Organization::COLLECTION,
										"id" => (String) $eltPorteurA1["_id"],
										"name" => $eltPorteurA1["name"],
										"email" => ( ( !empty($eltPorteurA1["email"]) ) ? $eltPorteurA1["email"] : "" )
									);

									if(empty($eltPorteurA1["links"]))
										$eltPorteurA1["links"] = array();

									if(empty($eltPorteurA1["links"]["projects"]))
										$eltPorteurA1["links"]["projects"] = array();

									if(empty($eltPorteurA1["links"]["projects"][(String)$projectA["_id"]]) ) {
										$eltPorteurA1["links"]["projects"][(String)$projectA["_id"]] = array( "type" => Project::COLLECTION);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$eltPorteurA1["_id"]) ), 
											array('$set' => array( "links" => $eltPorteurA1["links"] ) ) );

										//$porteurTerList[$porteurTerName[$vFile[31]]] = $eltPorteurA1;

										if(!empty($eltPorteurA1) && !empty($vFile[31]) ){
								 			if(empty($porteurTerName[$vFile[31]]))
								 				$porteurTerName[$vFile[31]] = (String)$eltPorteurA1["_id"] ;
								 			$porteurTerList[$porteurTerName[$vFile[31]]] = $eltPorteurA1;
								 		}
								 		else{
								 			var_dump($vFile[31]);
								 			var_dump($porteurTerName[$vFile[31]]);
								 			return Rest::json($eltPorteurA1);
								 		}
									}

									$linksUser = (!empty($vFile[39]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[39])): null );
									if(empty($linksUser))
										$linksUser = $bigboss;
									//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
									if(!empty($linksUser)){
										$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
										Link::connectParentToChild( (String)$eltPorteurA1["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
									}
								}

								if(!empty($eltPorteurA2)){
									if(empty($answers["answers"]["action"]["parents"]))
										$answers["answers"]["action"]["parents"] = array();

									$answers["answers"]["action"]["parents"][(String) $eltPorteurA2["_id"]] = array(
										"type" => Organization::COLLECTION,
										"id" => (String) $eltPorteurA2["_id"],
										"name" => $eltPorteurA2["name"],
										"email" => ( ( !empty($eltPorteurA2["email"]) ) ? $eltPorteurA2["email"] : "" )
									);

									if(empty($eltPorteurA2["links"]))
										$eltPorteurA2["links"] = array();

									if(empty($eltPorteurA2["links"]["projects"]))
										$eltPorteurA2["links"]["projects"] = array();

									if(empty($eltPorteurA2["links"]["projects"][(String)$projectA["_id"]]) ) {
										$eltPorteurA2["links"]["projects"][(String)$projectA["_id"]] = array( "type" => Project::COLLECTION);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$eltPorteurA2["_id"]) ), 
											array('$set' => array( "links" => $eltPorteurA2["links"] ) ) );

										//$porteurTerList[$porteurTerName[$vFile[40]]] = $eltPorteurA2;


										if(!empty($eltPorteurA2) && !empty($vFile[40]) ){
								 			if(empty($porteurTerName[$vFile[40]]))
								 				$porteurTerName[$vFile[40]] = (String)$eltPorteurA2["_id"] ;
								 			$porteurTerList[$porteurTerName[$vFile[40]]] = $eltPorteurA2;
								 		}
								 		else{
								 			var_dump($vFile[40]);
								 			var_dump($porteurTerName[$vFile[40]]);
								 			return Rest::json($eltPorteurA2);
								 		}
									}

									$linksUser = (!empty($vFile[48]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[48])): null );
									if(empty($linksUser))
										$linksUser = $bigboss;
									//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
									if(!empty($linksUser)){
										$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
										Link::connectParentToChild( (String)$eltPorteurA2["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
									}
								}


								if(!empty($eltPorteurA3)){
									if(empty($answers["answers"]["organizations"]))
										$answers["answers"]["organizations"] = array();

									$answers["answers"]["organizations"][(String) $eltPorteurA3["_id"]] = array(
										"type" => Organization::COLLECTION,
										"id" => (String) $eltPorteurA3["_id"],
										"name" => $eltPorteurA3["name"],
										"email" => ( ( !empty($eltPorteurA3["email"]) ) ? $eltPorteurA3["email"] : "" )
									);

									if(empty($eltPorteurA3["links"]))
										$eltPorteurA3["links"] = array();

									if(empty($eltPorteurA3["links"]["projects"]))
										$eltPorteurA3["links"]["projects"] = array();

									if(empty($eltPorteurA3["links"]["projects"][(String)$projectA["_id"]]) ) {
										$eltPorteurA3["links"]["projects"][(String)$projectA["_id"]] = array( "type" => Project::COLLECTION);

										PHDB::update( Organization::COLLECTION,
										    array("_id"=>new MongoId( (String)$eltPorteurA3["_id"]) ), 
											array('$set' => array( "links" => $eltPorteurA3["links"] ) ) );

										$porteurTerList[$porteurTerName[$vFile[49]]] = $eltPorteurA3;

										if(!empty($eltPorteurA3) && !empty($vFile[49]) ){
								 			if(empty($porteurTerName[$vFile[49]]))
								 				$porteurTerName[$vFile[49]] = (String)$eltPorteurA3["_id"] ;
								 			$porteurTerList[$porteurTerName[$vFile[49]]] = $eltPorteurA3;
								 		}
								 		else{
								 			var_dump($vFile[49]);
								 			var_dump($porteurTerName[$vFile[49]]);
								 			return Rest::json($eltPorteurA3);
								 		}
									}

									$linksUser = (!empty($vFile[57]) ? PHDB::findOne(Person::COLLECTION, array("email" => $vFile[57])): null );
									if(empty($linksUser))
										$linksUser = $bigboss;
									//$linksUser = (!empty($perscteter) ? $perscteter : $bigboss );
									if(!empty($linksUser)){
										$child = array("childId" => (String)$linksUser["_id"], "childType" => Person::COLLECTION);
										Link::connectParentToChild( (String)$eltPorteurA3["_id"], Organization::COLLECTION, $child, true, Yii::app()->session["userId"], "" ) ;
									}
								}

								if(empty($projectA["parent"]))
									$projectA["parent"] = array();

								if(!empty($eltPorteurA1) && empty($projectA["parent"][(String)$eltPorteurA1["_id"]]) ) {
									$projectA["parent"][(String)$eltPorteurA1["_id"]] = array( "type" => Organization::COLLECTION);
								}

								if(!empty($eltPorteurA2) && empty($projectA["parent"][(String)$eltPorteurA2["_id"]]) ) {
									$projectA["parent"][(String)$eltPorteurA2["_id"]] = array( "type" => Organization::COLLECTION);
								}

								if(!empty($eltPorteurA3) && empty($projectA["parent"][(String)$eltPorteurA3["_id"]]) ) {
									$projectA["parent"][(String)$eltPorteurA3["_id"]] = array( "type" => Organization::COLLECTION);
								}

								if(!empty($projectA["parent"])){
									PHDB::update( Project::COLLECTION,
									    array("_id"=>new MongoId( (String)$projectA["_id"]) ), 
										array('$set' => array( "parent" => $projectA["parent"] ) ) );
								}
								

								if(!empty($answers)){
									$res = Yii::app()->mongodb->selectCollection("answers")->insert( $answers);
									//Rest::json($answers); exit;


									$p = PHDB::findOneById(Project::COLLECTION, (String)$projectA["_id"]);
									if(empty($p["links"]))
										$p["links"] = array();
									if(empty($p["links"]["answers"]))
										$p["links"]["answers"] = array();
									if(empty($p["links"]["answers"][(String)$answers["_id"]])) {
										$p["links"]["answers"][(String)$answers["_id"]] = array( "type" => "answers");
									}

									PHDB::update( Project::COLLECTION,
									    array("_id"=>new MongoId( (String)$p["_id"]) ), 
										array('$set' => array( "links" => $p["links"] ) ) );
								}
							}
							
						}
							

					}
	                
	            }
            }

            //Rest::json($cteterListName); exit;

            $params = array( 
            				"nb" => $nb,
            				"headFile" => $headFile,
            				"cteterList" => $cteterList,
		        			"cteterListName" => $cteterListName,
		        			"cteterListNameErreur" => $cteterListNameErreur,
		        			"porteurTerList" => $porteurTerList,
		        			"porteurTerName" => $porteurTerName,
		        			"projectList" => $projectList,
		        			"addressList" => $addressList);
            
            // $params = array("result"=>true,
            //                 "elements"=>json_encode(json_decode(json_encode($elements),true)),
            //                 "elementsWarnings"=>json_encode(json_decode(json_encode($elementsWarnings),true)),
            //                 "saveCities"=>json_encode(json_decode(json_encode($saveCities),true)),
            //                 "listEntite"=>self::createArrayList(array_merge($elements, $elementsWarnings)));
        }
        return $params;
    }

    public static function getIndicator($actions = null, $cible = null, $fields = ["name","unity1","isIndicateurPrincipale"], $idElt=null, $principal=null){

        $where = [ "type" => "indicator", 
                   "source.key" => "ctenat" ];

        if($principal)
            $where["isIndicateurPrincipale"] = "true";

        if(!empty($actions))
            $whereA["domainAction"] = [ '$in' => $actions ];

        if(!empty($cible))
            $whereC["objectifDD"] = ['$in' => $cible];

        if(!empty($actions) && !empty($cible)){
            $where['$or'] = [$whereA, $whereC];
        } else {
            if(!empty($actions))
               $where["domainAction"] = ['$in' => $actions]; 
            if(!empty($cible))
               $where["objectifDD"] = ['$in' => $cible];
        }

        if(!empty($idElt)){
            $wherep['$or'] = [ $where,  [
                        "type" => "indicator", 
                        "source.key" => "ctenat",
                        "parent.".$idElt => ['$exists' => 1]]];

            $where = $wherep;
        }


        //Rest::json($where); 
        $pois = PHDB::find(Poi::COLLECTION, $where, $fields );
        $res = [];
        foreach ($pois as $key => $value) {
            if(!empty($value["name"])){
                $res[$key] = $value["name"];
            
                if(!empty($value["unity1"]))
                    $res[$key] = $res[$key]."<br/>( ".$value["unity1"]." )";
                    if(!empty($value["isIndicateurPrincipale"]))
                        $res[$key] = "1 - ".$res[$key];
                    
                
            }
        }

        return $res;
    }

    public static function indicatorCount($results, $indicId)
    {
        $res = 0;
        foreach ( $results as $ix => $ind) 
        {
            if( isset($ind["indicateur"]) && $ind["indicateur"] == $indicId && isset($ind["objectif"]) )
            {
                if(isset($ind["objectif"]["res2019"]))
                    $res += $ind["objectif"]["res2019"];
                if(isset($ind["objectif"]["res2020"]))
                    $res += $ind["objectif"]["res2020"];
                if(isset($ind["objectif"]["res2021"]))
                    $res += $ind["objectif"]["res2021"];
                if(isset($ind["objectif"]["res2022"]))
                    $res += $ind["objectif"]["res2022"];
            }
        }
        return $res;
    }

    public static function saveOrga($params){
        //var_dump("HelloThere"); exit;
        $params = Import::newStart($params);
        $res = Import::previewData($params);
        //Rest::json($res); exit;
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){

            foreach ($res["elementsObj"] as $key => $value) {

                if(!empty($value["name"])){
                    $listParent = null ;
                    if(!empty($value["parent"])){
                        $listParent = $value["parent"];
                        unset($value["parent"]);
                    }

                    $admins = null ;
                    if(!empty($value["admins"])){
                        $admins = explode(";", $value["admins"]);;
                        unset($value["admins"]);
                    }


                    if(!empty($value["address"]) && !empty($value["address"]["osmID"])){
                        unset($value["address"]);
                        unset($value["geo"]);
                        unset($value["geoPosition"]);
                    }

                    $update = false;
                    $id = false;
                    
                    $resElt = array(
                        "update" => false,
                        "id" => false,
                        "name" => @$value["name"]
                    );

                    if(!empty($value["id"])){
                        $p = PHDB::findOneById(Organization::COLLECTION, $value["id"], array("name", "_id") );
                        if(!empty($p)){
                            $resElt["id"] = $value["id"];
                            $resElt["update"] = true;
                        }
                        unset($value["id"]);
                    }else{
                        $value["creator"] = Yii::app()->session["userId"];
                        $value["created"] = time();
                    }

                    if($resElt["update"] == true){
                        PHDB::update(Organization::COLLECTION,
                            array("_id" => new MongoId($resElt["id"]) ) , 
                            array('$set' => $value)
                        );
                    } else {
                        //Element::save($value);
                        Yii::app()->mongodb->selectCollection(Organization::COLLECTION)->insert( $value );
                        $slug = Slug::checkAndCreateSlug( $value["name"], Organization::COLLECTION, (String) $value["_id"] );
                        Slug::save(Organization::COLLECTION, (String) $value["_id"], $slug);
                        $value["slug"]=$slug;
                        Element::updateField( Organization::COLLECTION, (String) $value["_id"], "slug", $slug);
                    }

                    
                    

                    if( !empty($listParent) ){
                        $where = array( "source.key" => "ctenat",
                                        "name" => $listParent);
                       
                        $parent = PHDB::findOne( Organization::COLLECTION, $where, array("name", "_id") );
                        
                        //Rest::json(array("parent" => $parent, "value" => $value)); exit;
                        if(!empty($parent) && !empty($parent["_id"]) && !empty($value["_id"])){
                            // Link::connect((String) $value["_id"], Organization::COLLECTION, (String)$parent["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf",false,false,false,false, null);

                            PHDB::update(Organization::COLLECTION,
                                array("_id" => new MongoId((String) $value["_id"]) ) , 
                                array('$set' => array("parent" => array(
                                            (String)$parent["_id"] => array("type" => Organization::COLLECTION))))
                            );


                            Link::connect((String)$parent["_id"], Organization::COLLECTION, (String) $value["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "members",false,false,false, false, null);
                        }
                    }

                    if( !empty($admins) ){

                        foreach ($admins as $keyA => $valA) {
                            $where = array("email" => trim($valA) );
                            $adminP = PHDB::findOne( Person::COLLECTION, $where, array("name", "_id", "links") );
                            
                            if(!empty($adminP) && !empty($adminP["_id"]) ){
                                Link::connect( (String)$value["_id"], Organization::COLLECTION, (String)$adminP["_id"], Person::COLLECTION, Yii::app()->session["userId"], "members",true,false,false,false, null);

                                Link::connect((String)$adminP["_id"], Person::COLLECTION, (String)$value["_id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf",true,false,false, false, null);
                            }
                        }
                        
                    }
                    $resultImport[] = $resElt;
                }
            } 
        }

        $res["resultImport"] = $resultImport;
        
        return Rest::json($res); exit;
    }

    public static function saveIndicateur($params){
        //var_dump("HelloThere"); exit;
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true);
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
            $where = array( "source.key" => "ctenat",
                            "type" => "indicator" );

            // if(!empty($res["indexStart"]) && ( $res["indexStart"] == 1 || $res["indexStart"] == "1") )
            //     PHDB::remove( Poi::COLLECTION, $where );

            //Rest::json($res["elementsObj"]);
            foreach ($res["elementsObj"] as $key => $value) {

                if(!empty($value["name"])){
                    $value["type"] = "indicator";
                    $value["creator"] = Yii::app()->session["userId"];
                    $value["created"] = time();

                    if(!empty($value["periode"])){
                        $value["periode"] = explode(";", $value["periode"]);
                    }
                    if(!empty($value["echelleApplication"])){
                        $value["echelleApplication"] = explode(";", $value["echelleApplication"]);
                    }

                    if(!empty($value["perimetreGeo"])){
                        $value["perimetreGeo"] = explode(";", $value["perimetreGeo"]);
                    }
                    if(!empty($value["domainAction"])){
                        $value["domainAction"] = explode(";", $value["domainAction"]);
                    }
                    if(!empty($value["objectifDD"])){
                        $value["objectifDD"] = explode(";", $value["objectifDD"]);
                    }

                    $update = false;
                    $id = false;

                    
                    $resElt = array(
                        "update" => false,
                        "id" => false,
                        "name" => @$value["name"]
                    );
                    if(!empty($value["id"])){
                        $p = PHDB::findOneById(Poi::COLLECTION, $value["id"], array("name") );
                        if(!empty($p)){
                            $resElt["id"] = $value["id"];
                            $resElt["update"] = true;
                        }

                        unset($value["id"]);
                    }
                    if($resElt["update"] == true){
                        PHDB::update(Poi::COLLECTION,
                            array("_id" => new MongoId($resElt["id"]) ) , 
                            array('$set' => $value)
                        );
                    } else {
                        //Element::save($value);
                        Yii::app()->mongodb->selectCollection(Poi::COLLECTION)->insert( $value );
                    }
                    $resultImport[] = $resElt;
                }
                
            } 
        }

        $res["resultImport"] = $resultImport;
        
        return Rest::json($res); exit;
    }


    public static function pdfElement($init) {
        // $controller=$this->getController();
        // ini_set('max_execution_time',1000);

        $controller = $init["controller"];
        $slug =  $init["slug"]; 
        $admin= $init["admin"]; 
        $id= $init["id"]; 
        $idElt= $init["idElt"];
        $cterId= (!empty($init["cterId"]) ? $init["cterId"] : null ) ;
        $answerList = array();
        //Rest::json($controller->costum['forms']); exit;
        $costum = CacheHelper::getCostum();
        $allforms = array();
        $form = array();

        
        

        $html = array();
        if(empty($idElt)){

            // $fields = array('name', "user", "answers", "collection");
            // $fields = array('name', "collection");
            $post =  array("context.".$cterId => array('$exists' => 1 ) );

            if (!empty($init["status"])){
                $post["priorisation"] = $init["status"];
            }

            if (!empty($init["text"])){
                $post["answers.project.name"] = new MongoRegex('/'.$init["text"].'/i');
            }

            $answerList = PHDB::find( Answer::COLLECTION, $post);

            // Rest::json($answerList);exit;
            // $resS = SearchNew::searchAdmin($post);
            // Rest::json($resS)
            // $answers = $resS["results"];
            // $answerList = Form::listForAdmin($answers) ;

            //$answerList = $resS["results"];
        } else {
            $answer = PHDB::findOneById( Answer::COLLECTION, $idElt);
            //Rest::json($answer); exit;
            $answers[$idElt] = $answer ;
            //$answerList = Form::listForAdmin($answers) ;
            $answerList = $answers ;
        }
        $listProjects = array();
        $listOrganizations = array();
        $listContext = array();
        $newArray = array();
        $htmls = array();
        //Rest::json($answerList); exit;

        // RAPHA TO MODYFI CTERID
        $orientations=null;

        foreach ($answerList as $key => $value) {
            if( !empty($idElt) ||
                (empty($idElt) // &&
                // !empty($answerList[$key]["priorisation"]) &&
                // $answerList[$key]["priorisation"] == Ctenat::STATUT_ACTION_VALID
                )
            ){

                if(empty($form[$value['form']])){
                    $form[$value['form']] = PHDB::findOneById( Form::COLLECTION , $value['form'] );
                    $allforms[$value['form']] = Form::getDataForm(array("form" => $form));
                }

                if( !empty($value["answers"]) && 
                    !empty($value["answers"]["action"]) &&
                    !empty($value["answers"]["action"]["parents"]) 
                ) {

                    foreach ($value["answers"]["action"]["parents"] as $keyP => $valueP) {
                        if (empty($valueP["name"]) || empty($valueP["typeStruct"]) ) {

                            $actparent = PHDB::findOneById( Organization::COLLECTION , $valueP["id"], array("name", "typeStruct") ) ;
                            if(!empty($actparent) && !empty($actparent["name"])){

                                $value["answers"]["action"]["parents"][$keyP]["name"] = $actparent["name"];
                            } 
                            if (!empty($actparent) && !empty($actparent["typeStruct"])){
                                $value["answers"]["action"]["parents"][$keyP]["typeStruct"] = $actparent["typeStruct"];
                            }
                        }
                    }
                }

                if( !empty($answerList[$key]) &&
                    !empty($answerList[$key]["answers"]) &&
                    !empty($answerList[$key]["answers"]["action"]) &&
                    !empty($answerList[$key]["answers"]["action"]["project"]) && 
                    !empty($answerList[$key]["answers"]["action"]["project"][0]) &&
                    !empty($answerList[$key]["answers"]["action"]["project"][0]["id"]) ){
                    $p = PHDB::findOneById( Project::COLLECTION, $answerList[$key]["answers"]["action"]["project"][0]["id"], array("name", "description", "shortDescription", "tags", "expected", "links.orientations") );
                    if(!empty($p)){
                        $newArray[$key]["project"] = $p;
                    }
                    
                }

                $orientationsName="";

                if( !empty($newArray[$key]["project"]["links"]["orientations"]) ){
                    foreach ($newArray[$key]["project"]["links"]["orientations"] as $keyOr => $valOr) {
                                                   
                            $orientations=PHDB::findOneById(Badge::COLLECTION, $keyOr);

                            if(!empty($orientations['name']) ){
                                if($orientationsName !== "")
                                    $orientationsName .=", ";
                                $orientationsName .= $orientations['name'];
                            } 
                    }
                }
                $newArray[$key] = $value;
                $newArray[$key]["orientationsName"] = $orientationsName;

                if( !empty($answerList[$key]) &&
                    !empty($answerList[$key]["answers"]) &&
                    !empty($answerList[$key]["answers"]["action"]) &&
                    !empty($answerList[$key]["answers"]["action"]["project"]) && 
                    !empty($answerList[$key]["answers"]["action"]["project"][0]) &&
                    !empty($answerList[$key]["answers"]["action"]["project"][0]["id"]) ){
                    $p = PHDB::findOneById( Project::COLLECTION, $answerList[$key]["answers"]["action"]["project"][0]["id"], array("name", "description", "shortDescription", "tags", "expected", "links.orientations") );
                    if(!empty($p)){
                        $newArray[$key]["project"] = $p;
                    }
                    
                }

                // $forms = PHDB::find( Form::COLLECTION , array("parentSurvey"=>$elt["cterSlug"]));
                // foreach ($forms as $key => $value) {
                //     $forms[$value["id"]] = $value;
                // }
                // $formSrc = null;
                // if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false){
                //     $pathT = explode(".",$form["scenario"]); 
                //     $formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
                //     $form["scenario"] = $formSrc[$pathT[4]];
                // }

                $orgsListName = "";
                //tib in refactor cte3 
                if( !empty($answerList[$key]["answers"]["action"]["parents"]) ){
                    foreach ($answerList[$key]["answers"]["action"]["parents"] as $keyP => $valP) {
                        $org = null ;
                        if(!empty($valP["id"])){
                            if(!empty($listParents[$valP["id"]]))
                                $org = $listParents[$valP["id"]];
                            else
                                $org = PHDB::findOneById(Organization::COLLECTION,$valP["id"], array("name", "type") );
                              
                            if(!empty($org["name"])){
                                if( !empty($orgsListName) )
                                    $orgsListName .= " <br>" ;
                                $orgsListName = $org["name"] ;
                            }
                        }
                    }
                }
                $newArray[$key]["answers"]["organizations"] = $orgsListName;

                $actionhtml = array(  "id"=> $key,
                    "answer"=>$newArray[$key],
                    "form" => $allforms[$value['form']]["form"],
                    "forms" => @$allforms[$value['form']]["forms"],
                    "orientations"=> $orientations );

                if (!empty($init["status"])){
                        $actionhtml["statushtml"] = "Descriptifs de toutes les « ".$init["status"]." » ";

                }

                $htmls[] = $controller->renderPartial(
                        'costum.views.custom.ctenat.pdf.answers', 
                        $actionhtml, true);

            } else
                unset($answerList[$key]);
        }
        //Rest::json($answerList); exit;
        $params = array("answers" => $answerList, "htmls" => $htmls);
        if(isset($init["saveOption"])){
            $params["comment"]=$init["comment"];
            $params["saveOption"]=$init["saveOption"];
            $params["urlPath"]=$init["urlPath"];
            $params["docName"]=$init["docName"];
        }

        if (empty($params["htmls"])){
            $params["htmls"][0] = "Il n'y a aucun action validée";
        }

        // foreach ($answerList as $key => $elt) {
        //     $form = PHDB::findOne( Form::COLLECTION , array("id"=>$elt["cterSlug"]));
        //     $forms = PHDB::find( Form::COLLECTION , array("parentSurvey"=>$elt["cterSlug"]));
        //     foreach ($forms as $key => $value) {
        //         $forms[$value["id"]] = $value;
        //     }
        //     $formSrc = null;
        //     if( is_string($form["scenario"]) && stripos( $form["scenario"] , "db.") !== false){
        //         $pathT = explode(".",$form["scenario"]); 
        //         $formSrc = PHDB::findOne( $pathT[1] , array( $pathT[2] => $pathT[3] ) );
        //         $form["scenario"] = $formSrc[$pathT[4]];
        //     }
        //     $params["htmls"][] = $controller->renderPartial('costum.views.custom.ctenat.pdf.answers', array("id"=> $key, "answer"=>$elt, "form" => $form, "forms" => $forms, "orientations"=> $orientations), true);
        // }

        $res=Pdf::createPdf($params);
    }

    public static function updatePrioAnswers($answerId, $formId, $status, $section) {
        $answer = PHDB::findOne( Form::ANSWER_COLLECTION , array("_id"=>new MongoId( $answerId ) ));

        //$form = PHDB::findOne( Form::COLLECTION , array("id"=> $formId ) );

        //$formEl = PHDB::findOne( $form["parentType"] , array("_id" =>new MongoId($form["parentId"] ) ) );
        
        $costum = CacheHelper::getCostum();
        //Rest::json($formEl); exit;
        $addTags = array( "actionPrincipal", "actionSecondaire", "cibleDDPrincipal", "cibleDDSecondaire");

        $res = array("result"=> false, "answer"=> $answer);
        if(!empty($answer) /* && !empty($form)  && !empty($formEl) */){




            $key = $section;
            $value = (!empty($status) ? $status : null);
            $verb = '$set';

            PHDB::update( Form::ANSWER_COLLECTION,
                array("_id"=>new MongoId($answerId)), 
                array($verb => array($key => $value)));
            $answer[$key]=$value;
            $setP =  array();
            $setF =  array();
            if( !empty($answer["answers"]) && 
                !empty($answer["answers"]["action"]["project"][0]) ){
                $idP = $answer["answers"]["action"]["project"][0]["id"] ;
                
                $project = PHDB::findOne( Project::COLLECTION , array("_id"=>new MongoId( $idP ) ));
                
                if( $status == Ctenat::STATUT_ACTION_VALID ){
                    if(!empty($answer["answers"]["caracter"])){
                        $caracter = $answer["answers"]["caracter"];
                        $listTagsAdd = array();
                        foreach ($addTags as $kLT => $valLT) {
                            //Rest::json(Yii::app()->session["costum"]); exit;
                            $listC = ( in_array($valLT, array("actionPrincipal", "actionSecondaire")) ? $costum["lists"]["domainAction"] : $costum["lists"]["cibleDD"] );

                            if( !empty($caracter[$valLT]) ){
                                if( empty($project["tags"]) )
                                    $project["tags"] = array();

                                // if( empty($formEl["tags"]) )
                                //     $formEl["tags"] = array();

                                if(is_array($caracter[$valLT])){
                                    foreach ($caracter[$valLT] as $kT => $valT) {
                                        $listTagsAdd[] = $valT;

                                        if(!in_array($valT, $project["tags"]))
                                            $project["tags"][] = $valT ;

                                        if(!empty($listC)){
                                            foreach ($listC as $key => $childBadges) {
                                                if(!empty($childBadges)){
                                                    foreach ($childBadges as $ic => $cb) {
                                                        if($valT == $cb){
                                                            $listTagsAdd[] = $valT;

                                                            if( !in_array($key, $project["tags"]) )
                                                                $project["tags"][] = $key;
                                                        }
                                                        // if( !in_array($key, $formEl["tags"]) && $valT == $cb )
                                                        //     $formEl["tags"][] = $key;
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }else if(is_string($caracter[$valLT])){
                                    $listTagsAdd[] = $caracter[$valLT];

                                    if(!in_array($caracter[$valLT], $project["tags"]))
                                        $project["tags"][] = $caracter[$valLT] ;

                                    // if(!in_array($caracter[$valLT], $formEl["tags"]))
                                    //     $formEl["tags"][] = $caracter[$valLT] ;
                                    if(!empty($listC)){
                                        foreach ($listC as $key => $childBadges) {
                                            if(!empty($childBadges)){
                                                foreach ($childBadges as $ic => $cb) {
                                                    if($caracter[$valLT] == $cb){
                                                        $listTagsAdd[] = $key;
                                                        if( !in_array($key, $project["tags"]) )
                                                            $project["tags"][] = $key;
                                                    }
                                                   

                                                    // if( !in_array($key, $formEl["tags"]) && $caracter[$valLT] == $cb )
                                                    //     $formEl["tags"][] = $key;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        unset($project["preferences"]["private"]);
                        if(!empty($project["tags"]) )
                            $setP["tags"] = $project["tags"];
                        if(!empty($project["preferences"]))
                            $setP["preferences"] = $project["preferences"];
                        if( !empty($listTagsAdd) && 
                            !empty($answer["answers"]) && 
                            !empty($answer["context"]) && !empty($costum["contextId"]) ) {

                            foreach ($answer["context"] as $keyContext => $valContext) {
                                if($keyContext != $costum["contextId"]){
                                    //var_dump($listTagsAdd);
                                    $eltContext = PHDB::findOneById($valContext["type"], $keyContext, array("tags") ) ;
                                    $setF = false;
                                    if(!empty($eltContext) && !empty($eltContext["tags"])){
                                        foreach ($listTagsAdd as $key => $value) {
                                            if(!in_array($value, $eltContext["tags"])){
                                                $eltContext["tags"][] = $value;
                                                $setF = true;
                                            }
                                        }

                                        if($setF === true){
                                            //var_dump($eltContext["tags"]); exit;
                                            PHDB::update( $valContext["type"],
                                                array("_id"=>new MongoId($keyContext)), 
                                                array($verb => array("tags" => $eltContext["tags"] ) ) );
                                        }
                                    }
                                }

                            }
                            
                        }
                    }
                } else if( $status == Ctenat::STATUT_ACTION_MATURATION ||
                            $status == Ctenat::STATUT_ACTION_CONTRACT ||
                            $status == Ctenat::STATUT_ACTION_REFUSE || 
                            $status == Ctenat::STATUT_ACTION_CANDIDAT){
                    $project["preferences"]["private"] = true ;
                    $setP["preferences"] = $project["preferences"];
                }

                if(!empty($setP)){
                    PHDB::update( Project::COLLECTION,
                    array("_id"=>new MongoId($idP)), 
                    array($verb => $setP) ) ;
                }
                
                

            }

            $res = [ "result"=> true, "msg"=> "Le statut a bien été mis à jour", "answer"=> $answer, "setF"=> $setF, "setP"=> $setP];
        }
        
        return $res ;
    }


    public static function urlAfterSave($data){
        //var_dump($data); 
        if($data["collection"]==Poi::COLLECTION && $data["elt"]["type"] = "indicator"
            && !empty($data["elt"]["parent"]) ){
           //var_dump("here 1");
            foreach ($data["elt"]['parent'] as $keyP => $valP) {
                $parent=Element::getElementById($keyP, $valP["type"], null, array("category", "slug"));
                //var_dump("here 2");
                if(!empty($parent["category"]) && $parent["category"] == "ficheAction"){
                    //var_dump("here 3");
                    return "#@".$parent["slug"].".view.answers.subview.murir";
                }
            }

        }
        return null ;

    }

    public static function chiffreFinancement($answers,$slug){
        $res = ["total"=>0,"public"=>0,"private"=>0,"lbl"=>"Millions"];
        $parentForm = PHDB::findOne(Form::COLLECTION,["id"=>self::PARENT_FORM]);
        foreach ($answers as $i => $a) {
            if(isset($a["answers"]["murir"]["planFinancement"]))
            {
                $fin = $a["answers"]["murir"]["planFinancement"];
                
                foreach ($fin as $ix => $f) {
                    $cumul = 0;
                    if(isset($parentForm["params"]["period"])){
                        
                        $from =  intval($parentForm["params"]["period"]["from"])+1;
                        $to =  intval($parentForm["params"]["period"]["to"]);
                        while ( $from <= $to) {
                            if(!empty($f["amount".$from]))
                                $cumul += intval( $f["amount".$from] );
                            $from++;
                        }
                    } 
                    // if(!empty($f["amount2019"]))
                    //     $cumul += intval( $f["amount2019"] );
                    // if(!empty($f["amount2020"]))
                    //     $cumul += intval( $f["amount2020"] );
                    // if(!empty($f["amount2021"]))
                    //     $cumul += intval( $f["amount2021"] );
                    // if(!empty($f["amount2022"]))
                    //     $cumul += intval( $f["amount2022"] );
                    // if(!empty($f["amount2023"]))
                    //     $cumul += intval( $f["amount2023"] );
                    
                    $res["total"] += $cumul;
                    if(isset($f["financerType"])){
                        if( in_array($f["financerType"], Ctenat::$financeurTypePublic) )
                            $res["public"] += $cumul;
                        else 
                            $res["private"] += $cumul;
                    }
                }
            }
        }
        $res["total"] = round($res["total"]/1000000,1);
        $res["public"] = round($res["public"]/1000000,1);
        $res["private"] = round($res["private"]/1000000,1);
        return $res;
    }

    public static function chiffreFinancementByType($slug=null,$returnRes = true){
        $answers = PHDB::find(Form::ANSWER_COLLECTION,[
                                    "cterSlug"=>$slug,
                                    "priorisation" => ['$in'=>[ Ctenat::STATUT_ACTION_VALID,
                                                                Ctenat::STATUT_ACTION_COMPLETED,
                                                                Ctenat::STATUT_ACTION_CONTRACT ]]
                                    ], ["_id","answers.murir.planFinancement"] );
        $finance = [];
        $financeLbl = [];
        $res = ["data"=>[],"lbls"=>[],"total"=>0];
        $parentForm = PHDB::findOne(Form::COLLECTION,["id"=>self::PARENT_FORM ], ["_id", "id","params.period", "cterSlug"] );

        foreach ($answers as $i => $a) {
            if(empty($slug))
                $slug = $a["cterSlug"];
            if(isset($a["answers"]["murir"]["planFinancement"]))
            {
                $fin = $a["answers"]["murir"]["planFinancement"];
                
                foreach ($fin as $ix => $f) {

                    $cumul = 0;
                    if(isset($parentForm["params"]["period"])){
                        
                        $from = intval($parentForm["params"]["period"]["from"])+1;
                        $to = intval($parentForm["params"]["period"]["to"]);
                        while ( $from <= $to) {
                            if(!empty($f["amount".$from]))
                                $cumul += intval( $f["amount".$from] );
                            $from++;
                        }
                    }
                    // if(!empty($f["amount2019"]))
                    //     $cumul += intval( $f["amount2019"] );
                    // if(!empty($f["amount2020"]))
                    //     $cumul += intval( $f["amount2020"] );
                    // if(!empty($f["amount2021"]))
                    //     $cumul += intval( $f["amount2021"] );
                    // if(!empty($f["amount2022"]))
                    //     $cumul += intval( $f["amount2022"] );
                    // if(!empty($f["amount2023"]))
                    //     $cumul += intval( $f["amount2023"] );
                    if(isset($f["financerType"])){
                        if(!isset($finance[$f["financerType"]])){
                            $finance[ $f["financerType"] ] = $cumul;
                            $financeLbl[ $f["financerType"] ] = (isset(Ctenat::$financerTypeList[$f["financerType"]])) ? Ctenat::$financerTypeList[$f["financerType"]] : $f["financerType"] ;
                        }
                        else 
                            $finance[$f["financerType"]] += $cumul;
                    }
                }
            }
        }
        if($returnRes){
            foreach (array_keys($finance) as $k => $v) {
                $res["data"][] = $finance[$v];
                $res["total"] += intval($finance[$v]);
                $res["lbls"][]   = $financeLbl[$v];
            }
            $res["total"] = round($res["total"]/1000000,1);
            return $res;
        } else {
            return $finance;
        }
    }

    public static function deleteElement($params){
       
        //Rest::json($params); exit;
        if($params["elementType"] == Project::COLLECTION && 
            !empty($params["elementToDelete"]["category"])){
            if($params["elementToDelete"]["category"] == "cteR"){
                $answers = PHDB::find(Answer::COLLECTION, array("formId" => $params["elementToDelete"]["slug"]));
                //Rest::json($answers); exit;
                if(!empty($answers)){
                    foreach ($answers as $keyA => $answer) {
                        if( !empty($answer) && 
                            !empty($answer["answers"]) && 
                            !empty($answer["answers"]["action"]["project"][0]) ){
                            $cteR = Slug::getElementBySlug($answer["cterSlug"], array("name") ) ;


                            $project = $answer["answers"]["action"]["project"][0] ;
                            //disconnect cter.links.action(project)
                            Link::disconnect($params["elementId"], Project::COLLECTION, $project["id"], Project::COLLECTION,Yii::app()->session["userId"], "projects", null, null);
                            //Add notification - email label in child link
                            //disconnect project.links.cter(project)
                            Link::disconnect($project["id"], Project::COLLECTION, $params["elementId"], Project::COLLECTION, Yii::app()->session["userId"], "projects", null, null);
                            //disconnect project.links.answers
                            Link::disconnect($project["id"], Project::COLLECTION, $keyA, Answer::COLLECTION, Yii::app()->session["userId"], "answers", null, null);

                            $where = array("_id" => new MongoId($project["id"]));
                            $action = array('$unset' => array('category' => ""));
                            PHDB::update( Project::COLLECTION, $where, $action );
                        }
                        $res = Answer::delete( $keyA );
                    }
                }
            }else if($params["elementToDelete"]["category"] == "ficheAction"){
                if(isset($params["elementToDelete"]["links"]["answers"])){
                    foreach($params["elementToDelete"]["links"]["answers"] as $kAnsw=>$v){
                        $res = Answer::delete( $kAnsw );
                    }                            
                }
                PHDB::update( Badge::COLLECTION, array("links.projects.".$params["elementId"]=>array('$exists'=>true), "category"=> "strategy" ), array('$unset'=>array("links.projects.".$params["elementId"]=>"")));
                   
            }
        }

        //Rest::json($params); exit;
    }


    public static function generateAnswerBeforeSave($params){
        $costum = CacheHelper::getCostum();
        if( !empty($params["answer"]) && !empty($params["answer"]["context"]) && 
            !empty($costum) && !empty($costum["contextId"])){
            
            foreach ($params["answer"]["context"] as $keyC => $valC) {
                if($keyC != $costum["contextId"]){
                    $cter = PHDB::findOneById($valC["type"], $keyC, array("slug","address"));
                    if(!empty($cter) ){
                        if(!empty($cter["slug"]))
                            $params["answer"]["cterSlug"] = $cter["slug"];
                    
                        if(!empty($cter["address"]))
                            $params["answer"]["address"] = $cter["address"];
                    }
                }
            }
            
            //Rest::json($answers); exit;
            
        }
        return $params;
    }

    public static function searchAnswers($params){
        $costum = CacheHelper::getCostum();
        $allAnswers = PHDB::findAndFieldsAndSortAndLimitAndIndex(Answer::COLLECTION, $params["query"], $params["fields"], $params["sortBy"], $params["indexStep"], $params["indexMin"]);

        if( !empty($allAnswers) ){
            foreach ($allAnswers as $key => $value) {

                if( !empty($value["answers"]) && 
                    !empty($value["answers"]["action"]) &&
                    !empty($value["answers"]["action"]["parents"]) 
                ) {
                    foreach ($value["answers"]["action"]["parents"] as $keyP => $valueP) {
                        if (empty($valueP["name"])) {

                            $actparent = PHDB::findOneById( Organization::COLLECTION , $valueP["id"], array("name") ) ;
                            if(!empty($actparent) && !empty($actparent["name"])){

                                $value["answers"]["action"]["parents"][$keyP]["name"] = $actparent["name"];
                            }
                        }
                    }
                }

                if( !empty($value["answers"]) && 
                    !empty($value["answers"]["action"]) &&
                    !empty($value["answers"]["action"]["project"]) &&
                    !empty($value["answers"]["action"]["project"][0]) && 
                    !empty($value["answers"]["action"]["project"][0]["id"]) ) {
                    $project = PHDB::findOneById(Project::COLLECTION, $value["answers"]["action"]["project"][0]["id"], array("preferences", "links", "collection","name","dispositifid", "dispositif") ) ;
                    if(!empty($project) && !empty($project["preferences"])){
                        $project["id"] = $value["answers"]["action"]["project"][0]["id"];
                        $value["project"] = $project;
                        $allAnswers[$key] = $value;
                    }
                }
            }
        }
        
        return $allAnswers;
    }

    public static function getdatasetsnature($naturek, $labels, $datasetsbudget, $answers, $labelsnature){
        $lbsnatu = [];
        foreach ($answers as $key => $value) {
            // foreach ($value as $i => $imp) {
                foreach ($labelsnature as $ln => $vn) {
                    if($naturek == $vn){
                        $range = array_search($value["poste"], $labels);
                        if(sizeof($lbsnatu) == 0){
                            $lbsnatu = $datasetsbudget[$range];
                        } else {
                            $lbsnatu = array_map(function () {
                                        return array_sum(func_get_args());
                                    }, $lbsnatu, $datasetsbudget[$range]);
                        }
                    }
                }
            // }
        }
        
        return $lbsnatu;
    }


    public static function getdatasetsposte($postek, $labels, $datasetsbudget){
        $range = array_search($postek, $labels);
        if($range != false){
            return $datasetsbudget[$range];
        } else {
            return [];
        }
    }


    public static function getdatasetsamount($amount,$answers,$datasetsbudget, $fromto){
        $returnnData = [];
        $dtbudgt = [];

            if (sizeof($fromto) != 0) {
                $f = $fromto["from"];
                $t = $fromto["to"];
                while ( $f <= $t) {
                    array_push($dtbudgt, $f);
                    $f++;
                }

                $range = array_search($amount, $dtbudgt);

                foreach ($datasetsbudget as $dtsbgid => $dtsbgvalue) {
                    array_push($returnnData, $dtsbgvalue[$range]);
                }
            }
        return $returnnData;
    }

    public static function getdatasetstitle($postek, $labels, $datasetsbudget){
        $range = array_search($postek, $labels);
        return $datasetsbudget[$range];
    }

    public static function getdatasetsfinancier($naturek, $labels, $datasetsbudget, $answers, $labelsnature){
        $lbsnatu = [];
        foreach ($answers as $key => $value) {
            // foreach ($value as $i => $imp) {
                foreach ($labelsnature as $ln => $vn) {
                    if($naturek == $vn){
                        $range = array_search($value["financer"], $labels);
                        if(sizeof($lbsnatu) == 0){
                            $lbsnatu = $datasetsbudget[$range];
                        } else {
                            $lbsnatu = array_map(function () {
                                        return array_sum(func_get_args());
                                    }, $lbsnatu, $datasetsbudget[$range]);
                        }
                    }
                }
            // }
        }
        return $lbsnatu;
    }

    public static function getdatasetsfinanciertype($naturek, $labels, $datasetsbudget, $answers, $labelsnature){
        $lbsnatu = [];
        foreach ($answers as $key => $value) {
            // foreach ($value as $i => $imp) {
                foreach ($labelsnature as $ln => $vn) {
                    if($naturek == $vn){
                        $range = array_search($value["financerType"], $labels);
                        if(sizeof($lbsnatu) == 0){
                            $lbsnatu = $datasetsbudget[$range];
                        } else {
                            $lbsnatu = array_map(function () {
                                        return array_sum(func_get_args());
                                    }, $lbsnatu, $datasetsbudget[$range]);
                        }
                    }
                }
            // }
        }
        return $lbsnatu;
    }


    

    public static function convertDataForBadges($results){
        foreach($results as $k => $v){
            if(isset($v["links"]) && isset($v["links"]["projects"]) && !empty($v["links"]["projects"])){
                foreach($v["links"]["projects"] as $i => $p){
                    $elt=Element::getElementById($i, $p["type"], null, array("name", "slug","profilThumbImageUrl"));
                    $results[$k]["links"]["projects"][$i]=array_merge($results[$k]["links"]["projects"][$i], $elt);
                }
            }
            $where=array("id"=>$k, "type"=>Badge::COLLECTION, "doctype"=> "file");
            $results[$k]["files"] = Document::getListDocumentsWhere($where,"file");
                   
        }
        return $results;
    }
    public static function buildLineCopilComment($idAnswer, $title, $dateComment, $step, $key, $ct){
        $str="";
        $res = Comment::buildCommentsTree($idAnswer, "answers", null, array("currentDay"=>$dateComment), $step.$key.$ct);
        $commentstr="";
        $countComment=count($res["comments"]);
        foreach($res["comments"] as $comment){
            $commentstr.= '<p style="color:#616164;font-size:13px;">'.$comment["author"]["name"].' :</p><p style="padding:10px;font-size:12px;">'.str_replace("\n","<br>",$comment["text"]).'</p><br/>';
            if(isset($comment["replies"]) && !empty($comment["replies"])){
                $countRep=count($comment["replies"]);
                $countComment+=$countRep;
                $commentstr.='<div style="border:1px solid #ccc;">';
                $inc=1;
                foreach($comment["replies"] as $rep){
                    $commentstr.='<p style="color:#616164;font-size:12px"> &#x21B3; '.$rep["author"]["name"].' :</p><p style="padding:5px 10px 10px 10px;font-size:11px;">'.str_replace("\n","<br>",$rep["text"]).'</p>';
                    if($inc != $countRep) $commentstr.='<br/>';
                    $inc++;
                }
                $commentstr.='</div>';
            }
        }
        $strCountComment=($countComment>1) ? $countComment." commentaires" : $countComment." commentaire";
            $str.='<p style="color:#5EAC87;">'.$title.' <span style="font-size:12px; color:#ccc;font-style:italic;">('.$strCountComment.')</span></p>';
            if(!empty($commentstr))
                $str.=$commentstr;
        return $str;
    }

    public static function csvElement($post, $type=null, $id=null, $canSee=null){
        $results = array("results"=>[]);
        $fields = (!empty($_POST["fields"]) ? $_POST["fields"] :  array() );
        if( Authorisation::isInterfaceAdmin() ||
            (!empty($id) && !empty($type)
                && (Authorisation::isElementAdmin($id,$type, Yii::app()->session["userId"])
                    || $canSee==true && Authorisation::canSee($type ,$id)))){
            $results = SearchNew::querySearchAll($post, $fields, true);
        }else if(Costum::isSameFunction("authorizedPanelAdmin")) {
            $opt=(!empty($id) && !empty($type)) ? array("id"=>$id, "type"=>$type) : [];
            if(Costum::sameFunction("authorizedPanelAdmin", $opt )  || (!empty($id) && !empty($type) && ( $canSee==true && Authorisation::canSee($type ,$id))))
                $results = SearchNew::querySearchAll($post, $fields, true);
        }

        if (!empty($post["filters"]["type"]) && $post["filters"]["type"] == "indicator" && !empty($post["searchType"]) && in_array("poi", $post["searchType"] ) ){
            foreach ($results["results"] as $idr => $res){
                if(!empty($res["domainAction"]) && is_string($res["domainAction"])){
                    $results["results"][$idr]["domainAction"] = [$results["results"][$idr]["domainAction"]];
                }
            }
        }
        return $results;
    }

}
?>