<?php
    /**
     * contains all the utility methods for meteolamer
     * 
     * @author Yorre Rajaonarivelo
     * @description 
     */

     class Meteolamer{
        public static $API_KEY = "qVRtB4AJuj";
        public static $COLLECTION = "--meteolamer-forecast";
        public static $BASE_URL = "https://minio.communecter.org/meteolamer/web";
        public static $TIMES = ["00", "06", "12", "18"];
        public static $WEATHER_SIGN_NAMES = ["CLEAR", "PARTLY_CLOUDY", "CLOUDY", "SHOWERS", "HEAVY_RAIN"];

        public static function fetchData($type, $date, $time){
            $filename = self::$BASE_URL."/tables/".$type."_".date("Ymd", strtotime($date))."_$time"."0000.js";
            
            /**
             * ensure that the allow_url_fopen module is enabled to use file_get_contents
            */
            $file_content = @file_get_contents($filename);
            if($file_content){
                //retrieve the json string inside the js file
                preg_match("#'{(.*?)}'#", $file_content, $matches);
                //get the first string matches
                $data = $matches[0];
                //remove "'" in the string to have a clean json
                $data = str_replace("'", "", $data);
                //solve the encoding problem and decode the json to array
                $data = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $data), true );

                return $data['spot'];
            }

            //send mail on error
            self::sendMailErrorImport([
                "url"=>"/costum/meteolamer/importdata/key/".self::$API_KEY,
                "date"=>$date,
                "time"=>$time,
                "type"=>$type
            ]);

            throw new Exception("Impossible de récuperer les données sur: ".$filename);
        }

        public static function mergeData($date, $time, $data_to_merge){
            $all_data = [];
            $region_data = $data_to_merge["region_data"];
            $local_data = $data_to_merge["local_data"];
            $wind_data = $data_to_merge["wind_data"];
    
            foreach(array_merge($region_data, $local_data) as $data){
                $spot_name = $data['Name'];
                if(!array_key_exists($spot_name, $all_data)){
                    $all_data[$spot_name] = [
                        "wave" => NULL,
                        "tide" => NULL,
                        "wind" => NULL
                    ];
                }
    
                $all_data[$spot_name]["wave"] = [
                    "map" => $data['Map'],
                    "imap" => $data['Imap'],
                    "height" => $data['H'],
                    "swellHeight" => $data['Hswell'],
                    "period" => $data['P'],
                    "direction" => $data['D'],
                    "windSpeed" => $data['Ws'],
                    "windDirection" => $data['Wd']
                ];
    
                $tide_histr = explode(" ", $data['HiStr']);
                $tide_lostr = explode(" ", $data['LoStr']);
                $all_data[$spot_name]['tide'] = [
                    "map" => in_array($spot_name, ["Reunion", "Home"]) ? 
                             "./data/maree/reunion_maree_".date("Ymd", strtotime($date))."_$time"."0000.png":NULL,
                    "high" => [
                        [
                            "time" => isset($tide_histr[0])?str_replace(".", ":", $tide_histr[0]):"",
                            "level" => $data['HiLev']
                        ],
                        [
                            "time" => isset($tide_histr[1])?str_replace(".", ":", $tide_histr[1]): "",
                            "level" => $data['HiLev']
                        ]
                    ],
                    "low" => [
                        [
                            "time" => isset($tide_lostr[0])?str_replace(".", ":", $tide_lostr[0]):"",
                            "level" => $data['LoLev']
                        ],
                        [
                            "time" => isset($tide_lostr[1])?str_replace(".", ":", $tide_lostr[1]):"",
                            "level" => $data['LoLev']
                        ]
                    ]
                ];
            }
    
            foreach($wind_data as $data){
                $spot_name = $data['Name'];
                if(array_key_exists($spot_name, $all_data)){
                    $all_data[$spot_name]["wind"] = [
                        "map" => $data["Map"],
                        "imap" => $data['Imap'],
                        "speed" => $data['Ws'],
                        "direction" => $data['Wd'],
                        "atmosphericPressure" => $data['MSLP'],
                        "temperature" => $data['TEMP'],
                        "cloud" => self::$WEATHER_SIGN_NAMES[intval($data['CLOUD'])]
                    ];
                }
            }
    
            return $all_data;
        }

        public static function fetchAndMergeData($date){
            $output_data = [];
        
            foreach(self::$TIMES as $time){
                $data_to_merge = [
                    "region_data" => self::fetchData("region", $date, $time),
                    "local_data" => self::fetchData("spot", $date, $time),
                    "wind_data" => self::fetchData("vent", $date, $time)
                ];

                $data_merged = self::mergeData($date, $time, $data_to_merge);
                foreach($data_merged as $spot_name => $data){
                    if(!array_key_exists($spot_name, $output_data)){
                        $output_data[$spot_name] = [
                            "date" => strtotime($date),
                            "spot" => strtolower($spot_name),
                            "data" => []
                        ];
                    }
                    $output_data[$spot_name]["data"]["h$time"] = $data;
                } 
            }

            return $output_data;
        }

        public static function fetchDataBetweenDates($start_date, $end_date){
            $all_data = [];
            $current_date = $start_date;
            while(strtotime($current_date) <= strtotime($end_date)){
                $sub_data = self::fetchAndMergeData($current_date);
                foreach($sub_data as $item)
                    $all_data[] = $item;
                $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            }
            return $all_data;
        }

        public static function importData($start_date, $end_date){
            $data = self::fetchDataBetweenDates($start_date, $end_date);
            foreach($data as $item){
                $res = PHDB::findOne(self::$COLLECTION, [
                    "date" => $item['date'],
                    "spot" => $item['spot']
                ]);
                if($res)
                    PHDB::update(self::$COLLECTION, [ 
                        "date" => $item['date'],
                        "spot" => $item['spot']
                    ], ['$set'=>$item]);
                else
                    Yii::app()->mongodb->selectCollection(self::$COLLECTION)->insert($item);
            }
            PHDB::remove(self::$COLLECTION, [
                "date" => [
                    '$lte' => strtotime(date("Y-m-d", time())." -1 day")
                ]
            ]);
        }

        public static function getDataBetweenDates($spot, $start_date, $end_date){
            $data = [];
            $current_date = $start_date;
            while(strtotime($current_date) <= strtotime($end_date)){
                $data[$current_date] = PHDB::findOne("--meteolamer-forecast", [
                                        "date" => strtotime($current_date),
                                        "spot" => $spot
                                    ]);
                $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            }
            return $data;
        }

        public static function getSpotsDataBetweenDates($spots, $start_date, $end_date){
            $data = [];
            foreach($spots as $spot)
                $data[$spot["name"]] = self::getDataBetweenDates($spot["name"], $start_date, $end_date);
            return $data;
        }

        public static function getSpotByName($name){
            return PHDB::findOne('--meteolamer-spots',  ["name" => $name], ["label"=>1]);
        }

        public static function sendMailErrorImport($data){
            $res = Organization::getWhere(["slug"=>"meteolamer"]);
            if($res && is_array($res)){
                $element = array_shift(array_values($res));
                if(isset($element["links"]["members"])){
                    foreach($element["links"]["members"] as $id => $link){
                        if($link["type"]==Citoyen::COLLECTION && $link["isAdmin"]){
                            $person = PHDB::findOne(Citoyen::COLLECTION, ["_id"=> new MongoId($id)]);
                            if(isset($person["email"])){
                                $params = [
                                    "tpl"=>"meteolamerErrorImport",
                                    "tplObject"=>"Erreur de l'importation de données dans Meteolamer",
                                    "tplMail"=>$person["email"]
                                ];
                                $params = array_merge($params, $data);

                                $mailParams = [
                                    "type" => Cron::TYPE_MAIL,
                                    "tpl"=>$params["tpl"],
                                    "subject" => $params["tplObject"],
                                    "from"=>Yii::app()->params['adminEmail'],
                                    "to" => $params["tplMail"],
                                    "tplParams" => Mail::initTplParams($params)
                                ];
                                $fromMail=(isset($params["fromMail"]) && !empty($params["fromMail"])) ? $params["fromMail"] : null;
                                $mailParams=Mail::getCustomMail($mailParams, $fromMail);
                                Mail::send($mailParams);
                            }
                        }
                    }
                }
            }
        }

        public static function getSpots(){
            $res = PHDB::find("--meteolamer-spots");
            $spots = [];
            foreach($res as $spot){
                $spots[$spot["name"]] = $spot;
            }
            return $spots;
        }

        public static function migrateSpots(){
            $SPOTS = [
                [
                    "name" => "home",
                    "label" => "La Réunion",
                    "lat" => "-21",
                    "lon" => "55",
                    "type" => "region"
                ],
                [
                    "name" => "reunion",
                    "label" => "Océan Indien",
                    "lat" => "-21",
                    "lon" => "55",
                    "type" => "region"
                ],
                [
                    "name" => "maurice",
                    "label" => "Maurice",
                    "lat" => "-20",
                    "lon" => "57.5",
                    "type" => "region"
                ],
                [
                    "name" => "mayotte",
                    "label" => "Mayotte",
                    "lat" => "-11.5",
                    "lon" => "45",
                    "type" => "region"
                ],
                [
                    "name" => "seychelles",
                    "label" => "Seychelles",
                    "lat" => "-5",
                    "lon" => "55",
                    "type" => "region"
                ],
                [
                    "name" => "toamasina",
                    "label" => "Toamasina",
                    "lat" => "-18",
                    "lon" => "50",
                    "type" => "region"
                ],
                [
                    "name" => "toliara",
                    "label" => "Toliara",
                    "lat" => "-24",
                    "lon" => "42.5",
                    "type" => "region"
                ],
                [
                    "name" => "durban",
                    "label" => "Durban",
                    "lat" => "-30",
                    "lon" => "32.5",
                    "type" => "region"
                ],
                [
                    "name" => "kerguelen",
                    "label" => "Kerguelen",
                    "lat" => "-48",
                    "Lon" => "70",
                    "type" => "region"
                ],
                [
                    "name" => "boucan",
                    "label" => "Boucan",
                    "lat" => "-21.0283",
                    "lon" => "5.2168",
                    "type" => "spot"
                ],
                [
                    "name" => "etangsal",
                    "label" => "Etang Salé",
                    "lat" => "-21.2692",
                    "lon" => "55.3278",
                    "type" => "spot"
                ],
                [
                    "name" => "leport",
                    "label" => "Le Port",
                    "lat" => "-20.9157",
                    "lon" => "55.3154",
                    "type" => "spot"
                ],
                [
                    "name" => "portoues",
                    "label" => "Le Port Ouest",
                    "lat" => "-20.9375",
                    "lon" => "55.267",
                    "type" => "spot"
                ],
                [
                    "name" => "lhermit",
                    "label" => "L'hermitage",
                    "lat" => "-21.0897",
                    "lon" => "55.2192",
                    "type" => "spot"
                ],
                [
                    "name" => "manapany",
                    "label" => "Manapany",
                    "lat" => "-21.3794",
                    "lon" => "55.5844",
                    "type" => "spot"
                ],
                [
                    "name" => "roches",
                    "label" => "Roches Noires",
                    "lat" => "-21.0521",
                    "lon" => "55.219",
                    "type" => "spot"
                ],
                [
                    "name" => "stbenoit",
                    "label" => "St Benoit",
                    "lat" => "-21.0255",
                    "lon" => "55.7207",
                    "type" => "spot"
                ],
                [
                    "name" => "stdenis",
                    "label" => "St Denis",
                    "lat" => "-20.872",
                    "lon" => "55.4591",
                    "type" => "spot"
                ],
                [
                    "name" => "stpaul",
                    "label" => "St Paul",
                    "lat" => "-21.005",
                    "lon" => "55.2697",
                    "type" => "spot"
                ],
                [
                    "name" => "stleu",
                    "label" => "St Leu",
                    "lat" => "-21.1648",
                    "lon" => "55.2795",
                    "type" => "spot"
                ],
                [
                    "name" => "stpierre",
                    "label" => "St Pierre",
                    "lat" => "-21.3526",
                    "lon" => "55.472",
                    "type" => "spot"
                ],
                [
                    "name" => "troisbas",
                    "label" => "Trois Bassins",
                    "lat" => "-21.114",
                    "lon" => "55.2505",
                    "type" => "spot"
                ],
                [
                    "name" => "stesuzanne",
                    "label" => "Ste Suzanne",
                    "lat" => "-20.904797963509232",
                    "lon" => "55.60648096928164",
                    "type" => "spot"
                ],
                [
                    "name" => "stemarie",
                    "label" => "Ste Marie",
                    "lat" => "-20.89611266078328",
                    "lon" => "55.5494519133064",
                    "type" => "spot"
                ],
                [
                    "name" => "stphil",
                    "label" => "St Philippe",
                    "lat" => "-21.358018780141254",
                    "lon" => "55.765940639187306",
                    "type" => "spot"
                ]
            ];

            PHDB::remove("--meteolamer-spots", []);
            PHDB::batchInsert("--meteolamer-spots", $SPOTS);
        }
     }