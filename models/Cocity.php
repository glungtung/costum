<?php 
/**
 * 
 */
class Cocity {
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	public static function getCity($post){
		$name = PHDB::find(Organization::COLLECTION,array("name"=>new MongoRegex("/".$post["nameCity"]."/i")));
		return($name);
	}
	public static function getListCocity($post){  
		$where = [
			"costum.slug"=>$post["slug"],
			"ville" => ['$exists'=> 0]
		];
		if(isset($post["tags"]) && $post["tags"]!= ""){
			$where["tags"] = array('$in' => $post["tags"]);
		}
		$listCocity = PHDB::find(Organization::COLLECTION,$where,["name","shortDescription","description","urls","address","geo","geoPosition", "tags", "type","collection","slug","ville"]);
		return($listCocity);
	}

	public static function getFiliere($post){
		$orga = Element::getElementSimpleById($post["contextId"],"organizations",null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere","cocity"]);
		return ($orga["filiere"]);
	}
	public static function getOrgaFiliere ($post){
		$orgaFiliere = PHDB::find(Organization::COLLECTION,array(
			"thematic"=>$post["thematic"],
			"cocity"=>$post["cocity"]
		));
		return($orgaFiliere);
	}
	public static function generateCocity ($post) {
		
		
		// cms blocks
		if(isset($post["map"]["template"]) && $post["map"]["template"] == "unnotremonde" )
		 {
			$block = ["textImg.text_img_prez", "menu.menuWithSearh", "elements.manyTypeElement","textImg.text_img_prez","footer.footerCocity"];
			foreach ($block as $key => $value) {
				$arrayInsert = array(
					"path" => "tpls.blockCms.".$value,
					"page" => "welcome",
					"type" => "blockCopy",
					"parent" => [
						$post['id'] => [
							"type" => $post['map']['collection'],
							"name" => $post['map']['name']
						]
					],
					"haveTpl" => "false",
					"position" => $key,
					"blockCmsColorTitle3" => "white",
					"creator" => $post['map']['creator']

				);
				if ( $key == 0 && $value == "textImg.text_img_prez") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsBgColor"=> "linear-gradient(0.25turn,#3b9ca3, #ea631f);",
						"blockCmsBgType"=> "color",
						"borderBg"=> "#e8f1dc",
						"borderLeftRadius"=> "no",
						"btnBgColor"=> "#e64b16",
						"btnBorderColor"=> "transparent",
						"btnLabel"=> "nous soutenir",
						"btnLabelColor"=> "white",
						"btnRadius"=> "12",
						"colordecor"=> "#ffe599",
						"imagePosition"=> "right",
						"imagesBackground" => Yii::app()->getModule('costum')->assetsUrl."/images/cocity/unNotreMonde/ILLU.png",
						"logo"=>  Yii::app()->getModule('costum')->assetsUrl."/images/cocity/unNotreMonde/LOGOUNAUTREMONDE.png",
						"paragraph"=> "<br> \t\t\t\t<font color=\"#ffffff\" style=\"font-size: 30px;\" face=\"Please_write_me_a_song\">Un nôtre monde est un collectif de citoyens en dehors de tout parti politique. Notre volonté est de mettre en place une démocratie participative opérationnelle</font> <br>\t\t\t ",
						"showBtn"=> "show",
						"logoHeight"=> "auto",
						"logoWidth"=> "93%",
						"btnPadding"=> "3% 6% 3% 6%",
						"btnPolice"=> "/font/blockcms/Please write me a song.ttf",
						"btnfontWeight"=> " ",
						"btnfontSize"=> "30",
						"btnMarginTop"=> "10%",
						"colImagePadding"=> "10%",
						"colImageParentPadding"=> "",
						"colInfoPadding"=> "0 10% 0 10%",
						"logoHeightResponsive"=> "100px"
					]);
				};
				if ( $value == "menu.menuWithSearh") {
					$arrayInsert =array_merge($arrayInsert, [
						"paddingLeft"=> "0",
						"paddingRight"=> "0",

					]);
				}
				
				if ( $value == "elements.manyTypeElement") {
					$arrayInsert =array_merge($arrayInsert, [    
						"blockCmsColorTitle1"=> "#3b9ca3",
						"blockCmsColorTitle3"=> "black",
						"blockCmsColorTitle4"=> "black",
						"blockCmsColorTitle5"=> "black",
						"blockCmsColorTitle6"=> "black",
						"blockCmsPoliceTitle1"=> "SharpSans-No1-BoldItalic",
						"blockCmsPoliceTitle3"=> "SharpSans-No1-Book",
						"blockCmsPoliceTitle4"=> "SharpSans-No1-Book",
						"blockCmsPoliceTitle5"=> "SharpSans-No1-Book",
						"blockCmsPoliceTitle6"=> "SharpSans-No1-BoldItalic",
						"blockCmsTextAlignTitle1"=> "center",
						"blockCmsTextAlignTitle3"=> "left",
						"blockCmsTextAlignTitle4"=> "center",
						"blockCmsTextAlignTitle5"=> "left",
						"blockCmsTextAlignTitle6"=> "left",
						"blockCmsTextSizeTitle1"=> "30",
						"blockCmsTextSizeTitle3"=> "15",
						"blockCmsTextSizeTitle4"=> "16",
						"blockCmsTextSizeTitle5"=> "15",
						"blockCmsTextSizeTitle6"=> "14",
						"cardNbResult"=> "3",
						"elType"=> "poi"
					]);
				};
				if ( $key == 3 && $value == "textImg.text_img_prez") {
					$arrayInsert =array_merge($arrayInsert, [
						"paragraph"=> "<font style=\"font-size: 25px;\" face=\"SharpSans-No1-Bold\" color=\"#000000\">Transmettre ses coordonnées</font><div><font color=\"#000000\" face=\"SharpSans-No1-Bold\"><span style=\"font-size: 25px;\"><br></span></font><div><div><font face=\"SharpSans-No1-Book\" size=\"4\" color=\"#000000\">Le soutien financier est en cours de maintenance pour quelques jours. Si cous souhaiter manifester votre soutien, merci de compléter ce formuaire sur le site inter-régional afin qu'on puisse avoir vos coordonnnées pour vous informer de la suite de notre collectif</font></div><div><font face=\"SharpSans-No1-Book\" size=\"4\" color=\"#000000\"><br></font></div><div><font face=\"SharpSans-No1-Book\" size=\"4\" color=\"#000000\">Merci</font></div></div>\t\t\t   </div>",
						"blockCmsBgColor"=> "#e7f6f6",
						"blockCmsBgType"=> "color",
						"borderLeftRadius"=> "no",
						"btnBgColor"=> "#ea631f",
						"btnBorderColor"=> "#ea631f",
						"btnLabel"=> "Faire un don en ligne",
						"btnLabelColor"=> "white",
						"btnMarginTop"=> "5%",
						"btnPadding"=> "3% 6% 3% 6%",
						"btnPolice"=> "/font/blockcms/Please write me a song.ttf",
						"btnRadius"=> "12",
						"btnfontSize"=> "25",
						"btnfontWeight"=> " ",
						"imagePosition"=> "right",
						"logo"=> "",
						"imagesBackground" => Yii::app()->getModule('costum')->assetsUrl."/images/cocity/unNotreMonde/photo.jpeg",
						"marginBottom"=> "0",
						"marginTop"=> "0",
						"paddingBottom"=> "0",
						"paddingLeft"=> "0",
						"paddingRight"=> "0",
						"paddingTop"=> "0",
						"showBtn"=> "show",
						"colImagePadding"=> " 0",
						"colParentImagePadding"=> "0",
						"colImageParentPadding"=> "0",
						"colInfoPadding"=> "0 10% 0 22%"
					]);
				};
				if ( $value == "footer.footerCocity") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsBgColor" => "#3b9ca3"

					]);
				}
				Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsert);


			}
			
		}else {
			$block = ["header.bannerWithSearchGlobal", "menu.menu", "menu.linkWithbg","text.text2Column","app.blocApp","text.text4Columns","elements.standardElement", "map.mapSourcekeyAndAddress","footer.footerCocity"];
			foreach ($block as $key => $value) {
				$arrayInsert = array(
					"path" => "tpls.blockCms.".$value,
					"page" => "welcome",
					"type" => "blockCopy",
					"parent" => [
						$post['id'] => [
							"type" => $post['map']['collection'],
							"name" => $post['map']['name']
						]
					],
					"haveTpl" => "false",
					"position" => $key,
					"blockCmsColorTitle3" => "white",
					"creator" => $post['map']['creator']

				);
				if ( $value == "header.bannerWithSearchGlobal") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsBgColor" => "#052434",			
						"blockCmsPoliceTitle1" => "SharpSans-No1-Bold",
						"blockCmsPoliceTitle3" => "SharpSans-No1-Semibold",
						"blockCmsPoliceTitle5" => "SharpSans-No1-Semibold",
						"blockCmsColorTitle1" => "white",
						"blockCmsColorTitle2" => "white",
						"blockCmsColorTitle3" => "white",
						"blockCmsColorTitle5" => "#052434",
						"blockCmsTextSizeTitle1" => "130",
						"blockCmsTextSizeTitle3" => "25",
						"blockCmsTextSizeTitle5" => "25",
						"blockCmsTextAlignTitle1" => "left",
						"blockCmsTextAlignTitle3" => "left"
					]);
				};
				if ( $value == "menu.menu") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsColorTitle1" => "#63aabc",
						"blockCmsPoliceTitle1" => "SharpSans-No1-Bold",
						"blockCmsUnderlineColorTitle1" => "#052434",
						"blockCmsUnderlineTitle1" => "underline",
						"blockCmsTextSizeTitle1" => "51",
						"blockCmsUnderlineWidthTitle1" => "100px",
						"blockCmsTextAlignTitle1" => "left"
					]);
				};
				if ( $value == "menu.linkWithbg") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsBgColor" => "#f0fcff",					
						"blockCmsColorTitle3" => "#052434",
						"blockCmsColorTitle6" => "#052434",
						"blockCmsPoliceTitle3" => "SharpSans-No1-Bold",
						"blockCmsPoliceTitle6" => "Sharp_sans",
						"blockCmsTextSizeTitle3" => "32",
						"blockCmsTextSizeTitle6" => "20"
					]);
				};
				if ( $value == "text.text2Column") {
					$arrayInsert =array_merge($arrayInsert, [
						"title" =>"",
						"blockCmsColorTitle2" => "#63aabc",
						"blockCmsColorTitle6" => "black",
						"blockCmsPoliceTitle2" => "SharpSans-No1-Bold",
						"blockCmsPoliceTitle6" => "Sharp_sans",
						"blockCmsTextAlignTitle2" => "left",
						"blockCmsTextAlignTitle6" => "left"
					]);
				};
				if ( $value == "app.blocApp") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsColorTitle1" => "#63aabc",
						"blockCmsColorTitle2" => "white",
						"blockCmsColorTitle6" => "white",
						"blockCmsTextAlignTitle1" => "left",
						"blockCmsPoliceTitle1" => "SharpSans-No1-Bold",
						"blockCmsPoliceTitle2" => "SharpSans-No1-Bold",
						"blockCmsPoliceTitle6" => "SharpSans-No1-Medium",
						"blockCmsTextSizeTitle1" => "40",
						"blockCmsTextSizeTitle2" => "36",
						"blockCmsTextSizeTitle6" => "20"
					]);
				};
				if ( $value == "elements.standardElement") {
					$arrayInsert =array_merge($arrayInsert, [
						"elPoiType" =>"article",
						"elType" => "poi",
						"title" => "Actualité",
						"blockCmsColorTitle1" => "#005E6F",
						"blockCmsColorTitle6" => "#005E6F",
						"blockCmsColorTitle5" => "#000000",
						"blockCmsColorTitle3" => "#000000",
						"blockCmsTextSizeTitle1" => "35",
						"imgBorderColor" => "#ffffff",
						"showAdress" => true,
						"imgHeight" => "400px",
						"imgBg" => "#ffffff",
						"infoBg" => "#ffffff",
						"imgRound" => "5%",
						"imgWidth" => "300px",
						"showDate" => true,
						"showDescription" => true,
						"showName" => true,
						"showRoles" => true,
						"showShortDescription" => true,
						"showTags" => true,
						"showType" => true,
						"title" => "  ",
						"title2" => " ",
						"title" => "Actualité", 
						"formTitle" => "",
						"infoOnHover" => true
					]);
				};
				if ( $value == "footer.footerCocity") {
					$arrayInsert =array_merge($arrayInsert, [
						"blockCmsBgColor" => "#052434",
						"marginTop" => "20"

					]);
				}
				Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsert);
			}
		}
				
		
		$typeObj = array(
			"organizations" => [
				"add" => true
			],
			"citoyens" => [
				"add" => true
			],
			"poi" => [
				"add" => true,
				"name" => "Actus",
				"dynFormCostum" => [
					"beforeBuild" => [
						"properties" => [
							"name" => [
								"label" => "Titre de l'actualité",
								"placeholder" => "Entrez le titre de votre Actualité..."
							],
							"parent" => [
								"label" => "Auteur(s)"
							],
							"shortDescription" => [
								"inputType" => "textarea",
								"label" => "Court résumé de l'actualité ",
								"placeholder" => "Entrez un résumé de votre actualité (250 caractères max.)...",
								"rules" => [
									"maxlength" => 250
								],
								"order" => 5
							],
							"description" => [
								"markdown" => true,
								"label" => "Texte à ajouter popur plus d'informations",
								"order" => 7
							],
							"image" => [
								"label" => "Image(s)",
								"order" => 8
							],
							"urls" => [
								"label" => "Lien(s) => vidéo, page internet ou document...",
								"order" => 9
							]
						]
					],
					"onload" => [
						"actions" => [
							"setTitle" => "Ecrire un article",
							"html" => [
								"infocustom" => "Formulaire de création d'un article"
							],
							"presetValue" => [
								"type" => "article"
							],
							"hide" => [
								"typeselect" => 1,
								"breadcrumbcustom" => 1,
								"locationlocation" => 1
							]
						]
					]
				]
			] 

		);
		$filiere = [
			"food"=> array(
				"name" => "Food",
				"icon" => "fa-cutlery",
				"tags" => [ Yii::t("tags","agriculture"),Yii::t("tags","food"),Yii::t("tags","nutrition"),Yii::t("tags","AMAP") ]
			),  
			"health"=> array(
				"name" => "Health",
				"icon" => "fa-heart-o",
				"tags" => [ Yii::t("tags","health") ]
			),  
			"waste"=> array(
				"name" => "Waste",
				"icon" => "fa-trash-o ",
				"tags" => [ Yii::t("tags","waste") ]
			),  
			"transport"=> array(
				"name" => "Transport",
				"icon" => "fa-bus",
				"tags" => [ Yii::t("tags","urbanism"),Yii::t("tags","transport"),Yii::t("tags","construction") ]
			),  
			"education"=> array(
				"name" => "Education",
				"icon" => "fa-book",
				"tags" => [ Yii::t("tags","education"),Yii::t("tags","childhood") ]
			),  
			"citizen"=> array(
				"name" => "Citizenship",
				"icon" => "fa-user-circle-o",
				"tags" => [ Yii::t("tags","citizen"),Yii::t("tags","society") ]
			),  
			"economy"=> array(
				"name" => "Economy",
				"icon" => "fa-money",
				"tags" => [ Yii::t("tags","ess") ,Yii::t("tags","social solidarity economy") ]
			),  
			"energy"=> array(
				"name" => "Energy",
				"icon" => "fa-sun-o",
				"tags" => [ Yii::t("tags","energy"),Yii::t("tags","climat") ]
			),  
			"culture"=> array(
				"name" => "Culture",
				"icon" => "fa-universal-access",
				"tags" => [ Yii::t("tags","culture"),Yii::t("tags","animation") ]
			),  
			"environnement"=> array(
				"name" => "Environnement",
				"icon" => "fa-tree",
				"tags" => [ Yii::t("tags","environment"),Yii::t("tags","biodiversity"),Yii::t("tags","ecology") ]
			),  
			"numeric"=> array(
				"name" => "Numeric",
				"icon" => "fa-laptop",
				"tags" => [ Yii::t("tags","computer"),Yii::t("tags","ict"),Yii::t("tags","internet"),Yii::t("tags","web") ]
			),
			"sport" => array( 
				"name" => "Sport",
				"icon" => "fa-futbol-o",
				"tags" => [ Yii::t("tags","sport") ]
			)
		];
		

		if (isset($post["afterSave"])) 
			PHDB::update($post['map']["collection"], 
				array('slug' =>$post["afterSave"]["organization"]["slug"]), 
				array('$set' => ["filiere"=>$filiere, "costum"=>["slug"=>"cocity",  "typeObj"=>$typeObj]]));
		else 
			PHDB::update($post['map']["collection"], 
				array('slug' => $post['map']["slug"]), 
				array('$set' => ["filiere"=>$filiere, "costum"=>["slug"=>"cocity",  "typeObj"=>$typeObj]]));
	}
}

?>