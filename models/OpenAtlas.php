<?php 
class OpenAtlas{
    const COLLECTION = "costum";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function getPersons ($paramsCol,$paramsId) {

        $params["results"] = false;
        $collection = (isset($paramsCol)) ? $paramsCol : "citoyens";
        $id = (isset($paramsId)) ? $paramsId : "";

        $where = [
            "links.memberOf.".$id => array('$exists' => 1)
        ];

        $req = PHDB::findAndSortAndLimitAndIndex($collection,$where);
        
        if ( isset($req) ) {
            $params["results"] = $req;
        }else{
            $params = "erreur";
        }
        return $params;
    }

    public static function getCampaign ($sourceKey) {

        //$params["results"] = false;
        $collection = Crowdfunding::COLLECTION;
        //$id = (isset($paramsId)) ? $paramsId : "";

        $where = [
            "source.key" => $sourceKey
        ];

        $req = PHDB::find($collection,$where);
        
        if ( isset($req) ) {
            $params["results"] = $req;
        }else{
            $params = "erreur";
        }
        return $params;
    }
}
