<?php
	class PixelHumain {

		const COLLECTION = "costum";
		const CONTROLLER = "costum";
		const MODULE = "costum";


		public static function getCommunity($id,$tags){
			$res = array();
			$results = array();
			

			//Récupère la communauté
			$community=Element::getCommunityByTypeAndId(Organization::COLLECTION,$id);
			foreach ($community as $keyElt => $valueElt) {
				$res[$keyElt] = @$valueElt["roles"];
			}
			// Vérifie si community est vide et si il a une valeur
			if (isset($res)) {
          		$results = self::getDataCommunity($res,$tags);
       		}
       		return $results;
		}

		public static function getDataCommunity($data,$tags){
			$element = array();
			$params = array(
               	"result" =>  false
           	);
			//Si la data envoyée n'est pas vide
			if (!empty($data)) {
				//Parcours de la data
				$costum = CacheHelper::getCostum();
				foreach ($data as $key => $value) {
					array_push($element,Element::getElementSimpleById($key,"citoyens",null,["name", "profilMediumImageUrl", "tags" ,"links.memberOf.".$costum["contextId"]]));
					// var_dump($element);
				}
			}
			//Si le résultat de notre requête en haut est saisie
			if (isset($element)) {
				$res = array();

				$params = array(
               		"result" =>  true
           		);

           		$results = self::CreateResultCommunity($element,$tags);
           		return array_merge($params,$results);
			}
			return $params;
		}

		public static function CreateResultCommunity($params,$tags){
			// $compteur = 1;
			// $y = 0;
			// $results[$y] = array();
			$res["elt"] = array();
			// var_dump($tags);
			//Parcours la data envoyé
			// $costum = CacheHelper::getCostum();
			foreach ($params as $key => $value) {
				$imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");

				if (isset($tags) || !empty($tags)) {
					
					// var_dump("entrée dans la condition tags", $tags == @$value["tags"]);
					if ($tags == @$value["tags"]) {
						array_push($res["elt"],array(
							"id"			=>	(String) $value["_id"],
							"name"			=>	$value["name"],
							"imgProfil"		=>	$imgMedium,
							"tags"			=> 	@$value["tags"],
							"roles"			=> 	@$value["links"]["memberOf"][$costum["contextId"]]
						));
						// $res["elt"][$y][$compteur] = array(
						// 	"id"			=>	(String) $value["_id"],
						// 	"name"			=>	$value["name"],
						// 	"imgProfil"		=>	$imgMedium,
						// 	"tags"			=> 	@$value["tags"],
						// 	"roles"			=> 	@$value["links"]["memberOf"][$costum["contextId"]]
						// );
					}
				}else{
					array_push($res["elt"],array(
							"id"			=>	(String) $value["_id"],
							"name"			=>	$value["name"],
							"imgProfil"		=>	$imgMedium,
							"tags"			=> 	@$value["tags"],
							"roles"			=> 	@$value["links"]["memberOf"][$costum["contextId"]]
						));
					// $res["elt"][$y][$compteur] = array(
					// 		"id"			=>	(String) $value["_id"],
					// 		"name"			=>	$value["name"],
					// 		"imgProfil"		=>	$imgMedium,
					// 		"tags"			=> 	@$value["tags"],
					// 		"roles"			=> 	@$value["links"]["memberOf"][$costum["contextId"]]
					// 	);
				}

				// if ($compteur > 0 && $compteur < 3) {
				// 	$compteur++;
				// }else{
				// 	$compteur = 1;
				// 	$y++;
				// }

				// $results = $res;
				// var_dump($results);
			}
			return $res;
		}
	}
?>