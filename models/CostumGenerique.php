<?php
class CostumGenerique{

    const COLLECTION = "costum";
	const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function getProjects($post){
        $params = array(
            "result" => false
        );

        $limit = @$post["limit"] ? $post["limit"] : null;
        $allProjet = PHDB::findAndLimitAndIndex("projects",array("source.keys" => $post["source"]), $limit);
        
        if($allProjet){
            $params = array(
                "result"    =>  true
            );

            $res["elt"] = array();

            foreach($allProjet as $key => $value){
                array_push($res["elt"], array(
                    "id"			        =>	(String) $value["_id"],
                    "name"			        =>	$value["name"],
                    "slug"			        =>	$value["slug"],
                    "description"           =>  (@$value["description"] ? $value["description"] : "Non renseigné"),
                    "shortDescription"      =>  (@$value["shortDescription"] ? $value["shortDescription"] : "Non renseigné"),
                    "profilMediumImageUrl"  => @$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none")
                );
            }

            return array_merge($params,$res);
        }

        return $params;
    }

    public static function getEvent($post){
        $parametres = array(
           "result" => false
       );

       date_default_timezone_set('UTC');

       $date_array = getdate();
       $numdays = $date_array["wday"];
       $filters = (!empty($post["filters"])) ? $post["filters"] : "";
       $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
       $startDate = strtotime(date("Y-m-d H:i"));
    
       
       $where = array(
                 "source" => array(
                    "insertOrign" =>    "costum",
                    "keys"         =>    array(
                                        $post["source"]),
                    "key"          =>  $post["source"]),            
                 "startDate"   =>  array('$gte'   =>  new MongoDate($startDate)),
                 "endDate"     =>  array('$lt'    => new MongoDate($endDate))
                 );

       if (!empty($filters)) {
           $where["thematique"] = $filters;
       }


       $allEventSmarterritoire = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where,3);

       if(isset($allEventSmarterritoire)){

            $params = array(
               "result" =>  true
            );

            $res["element"] = array();

           foreach($allEventSmarterritoire as $key => $value){
            $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
            $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
            $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

               array_push($res["element"], array(
                   "id"               => (String) $value["_id"],
                   "name"             =>  $value["name"], 
                   "type"             =>  Yii::t("event",$value["type"]),
                   "startDate"        =>  date(DateTime::ISO8601, $value["startDate"]->sec),
                   "imgMedium"        =>  $imgMedium,
                   "img"              =>  $img,
                   "slug"             =>  $value["slug"]
               ));
            }
          return array_merge($params,$res);  
        }
        return $params;
    }

    public static function getRessources($post){
        $params = array(
            "result" => false
        );

        if (!empty($post["source"]) && isset($post["source"])) {
            $allRessources = PHDB::findAndSort(Classified::COLLECTION,
                                            array(
                                                "source.key"    =>  $post["source"]
                                            ),
                                            array("updated"=>-1),$post["limit"]);
        }

        if (@$allRessources) {
            $params = array(
                "result" => true
            );

            $res["element"] = array();

            foreach ($allRessources as $key => $value) {
                $description = @$value["description"] != null ? $value["description"] : "Aucune description";
                array_push($res["element"],array(
                    "id"  =>  (String) $value["_id"],
                    "name" => $value["name"],
                    "description" => $description,
                    "profilImageUrl"  =>  @$value["profilImageUrl"],
                    "profilMediumImageUrl"  =>  @$value["profilMediumImageUrl"],
                    "profilThumbImageUrl"  =>  @$value["profilThumbImageUrl"]
                ));
            }
            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getDda ($post){
        $params = array(
            "result" => false
        );

        if (!empty($post["source"]) && isset($post["source"])) {
            $allDda = PHDB::findAndSort(Proposal::COLLECTION,
                                            array(
                                                "source.key"    =>  $post["source"]
                                            ),
                                            array("updated"=>-1),$post["limit"]);
        }
        if (@$allDda) {
            $params = array(
                "result" => true
            );

            $res["element"] = array();

            foreach ($allDda as $key => $value) {
                array_push($res["element"],$value);
            }
            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getArticlesCarousel($post){
        $params = array(
            "result" => false
        );

        $limit = isset($post["limit"]) ? $post["limit"] : 3;

        if (!empty($post["source"]) && isset($post["source"])) {
            $allArticles = PHDB::findAndSort(Poi::COLLECTION, 
                                    array( 
                                        "source.key"=>$post["source"],
                                        "type"=>"article"), array("updated"=>-1), $limit );
        }

        if ($allArticles) {
            $params =array(
                "result" => true
            );

            $res["elt"] = array();

            foreach ($allArticles as $key => $value) {
                array_push($res["elt"],array(
                    "id"  =>  (String) $value["_id"],
                    "type"  =>  $value["type"],
                    "name"  =>  $value["name"],
                    "tags"  =>  @$value["tags"],
                    "shortDescription"  =>  @$value["shortDescription"],
                    "description"  =>  @$value["description"],
                    "profilImageUrl"  =>  @$value["profilImageUrl"],
                    "profilMediumImageUrl"  =>  @$value["profilMediumImageUrl"],
                    "profilThumbImageUrl"  =>  @$value["profilThumbImageUrl"],
                    "comment"  =>  @$value["comment"],
                    "commentCount"  =>  @$value["commentCount"],
                    "collectionCount"  =>  @$value["collectionCount"]
                ));
            }
            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getCommunity($post){
        $orga = [];

        $params = array(
            "result" => false
        );

        $community = Element::getCommunityByTypeAndId($post["type"],$post["id"]);

        if ($community) {
            foreach ($community as $key => $value) {
                // if ($value["type"] == "organizations") {
                    $orga[$key] = Element::getElementById($key,$value["type"] ,null, array("_id","name", "profilImageUrl","profilRealBannerUrl","links","slug", "address","geo", "geoPosition", "profilThumbImageUrl", "profilMarkerImageUrl","type" ));
                // }
            }
        }

        if (isset($orga)) {

            $params = array(
                "result" => true
            );

            $res["elt"] = array();
            // var_dump($orga);exit;
            foreach ($orga as $key => $value) {
                
                if(@$value["type"] != "")
                $value["typeOrga"] = $value["type"];
                    $value["type"] = "organizations";
                    $value["typeSig"] = Organization::COLLECTION;
                    
                $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
                $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
                $imgBanner = (@$value["profilRealBannerUrl"] ? $value["profilRealBannerUrl"] : "none");

                $res["elt"][$key] = array(
                    "_id"               => (String) $value["_id"],
                    "name"             =>  $value["name"],
                    "imgMedium"        =>  $imgMedium,
                    "img"              =>  $img,
                    "imgBanner"        =>  $imgBanner,
                    "type"             => $value["typeSig"],
                    "profilMarkerImageUrl" => @$value["profilMarkerImageUrl"],
                    "address"          =>  @$value["address"],
                    "geo"              =>  @$value["geo"],
                    "geoPosition"      =>  @$value["geoPosition"],
                    "profilThumbImageUrl" => @$value["profilThumbImageUrl"],
                    "slug"             =>  $value["slug"]
                );
            }
            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getArticles($post){
        $params =array(
            "result" => false
        );
        $limit = isset($post["limit"])?$post["limit"]:3;
        if (!empty($post["source"]) && isset($post["source"])) {
            $pList = PHDB::findAndSort(Poi::COLLECTION, 
                    array( 
                      "source.key"=>$post["source"],
                      "type"=>"article"), array("updated"=>-1), $limit );
        }

        if ($pList) {
            $params =array(
                "result" => true
            );

            $res["element"] = array();

            foreach ($pList as $key => $value) {
                array_push($res["element"],array(
                    "id"  =>  (String) $value["_id"],
                    "type"  =>  $value["type"],
                    "name"  =>  $value["name"],
                    "shortDescription"  =>  @$value["shortDescription"],
                    "description"  =>  @$value["description"],
                    "profilImageUrl"  =>  @$value["profilImageUrl"],
                    "profilMediumImageUrl"  =>  @$value["profilMediumImageUrl"],
                    "profilThumbImageUrl"  =>  @$value["profilThumbImageUrl"],
                    "comment"  =>  @$value["comment"],
                    "commentCount"  =>  @$value["commentCount"],
                    "collectionCount"  =>  @$value["collectionCount"]
                ));
            }
            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getArticlesCommunity($post){
        $params = array(
            "result" => false
        );
        $limit = isset($post["limit"])?$post["limit"]:3;

        $pList = array();
        $results["elt"] = array();

        if ((!empty($post["id"]) && isset($post["id"])) && (isset($post["type"]) && !empty($post["type"]))) {
            $articlesCommunity = Element::getCommunityByTypeAndId($post["type"],$post["id"]); 
        }

        if ($articlesCommunity) {
            foreach ($articlesCommunity as $key => $value) {
                $Community[$key] = Element::getElementById($key,$value["type"]);
            }
        }

        if ($Community) {
            foreach ($Community as $key => $value) {
               $pList += PHDB::findAndSort(Poi::COLLECTION, 
                    array( 
                      "source.key"=>$value["slug"],
                      "type"=>"article"), array("updated"=>-1), $limit );
            }
        }

        if ($pList){  
            $params = array(
                "result" => true
            );

            foreach ($pList as $key => $value) {

                array_push($results["elt"],array(
                    "id"  =>  (String) $value["_id"],
                    "type"  =>  $value["type"],
                    "name"  =>  $value["name"],
                    "shortDescription"  =>  @$value["shortDescription"],
                    "description"  =>  @$value["description"],
                    "profilImageUrl"  =>  @$value["profilImageUrl"],
                    "profilMediumImageUrl"  =>  @$value["profilMediumImageUrl"],
                    "profilThumbImageUrl"  =>  @$value["profilThumbImageUrl"],
                    "comment"  =>  @$value["comment"],
                    "commentCount"  =>  @$value["commentCount"],
                    "collectionCount"  =>  @$value["collectionCount"]
                ));
            }
            return array_merge($params,$results);
        }
        return $params;
    }
        
    public static function getEventDesc($post){
        $params = array(
            "result" => false
        );

        $filters = (!empty($post["filters"])) ? $post["filters"] : "";

        // A crée dans un modèle a part ce code
        date_default_timezone_set('UTC');
        //On récupère sur ce mois-ci les évènements
        $date_array = getdate ();
        $numdays = $date_array["wday"];

        $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
        $startDate = strtotime(date("Y-m-d H:i"));

        // Préparation de la requête
        $where = array("source.key"     => $post["source"]);

        if (!empty($filters)) {
            $where["thematique"] = $filters;
        }

        $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where, 3);

        if($allEvent){

            $params = array(
                "result"    =>  true
            );

            $res["element"] = array();
            foreach($allEvent as $key => $value){
                
                $date = date(DateTime::ISO8601, $value["startDate"]->sec);
                $img = isset($value["profilMediumImageUrl"])?$value["profilMediumImageUrl"]:"";
                $shortDescription =isset($value["shortDescription"])?$value["shortDescription"]:"";
                array_push($res["element"], array(
                    "id"                    =>  (String) $value["_id"],
                    "name"                  =>  $value["name"],
                    "slug"                  =>  $value["slug"],
                    "shortDescription"      =>  $shortDescription,
                    "imageProfil"           =>  $img)
                );
            }

            return array_merge($params,$res);
        }

        return $params;
    }

    public static function getEventSlide($post){
        $params = array(
            "result" => false
        );

        // A crée dans un modèle a part ce code
        date_default_timezone_set('UTC');
        //On récupère sur ce mois-ci les évènements
        $date_array = getdate ();
        $numdays = $date_array["wday"];
        $filters = (!empty($post["filters"])) ? $post["filters"] : "";
        $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
        $startDate = strtotime(date("Y-m-d H:i"));

        // Préparation de la requête
        $where = array("source.key"     => $post["source"]);
                       // "startDate"      => array('$gte' => new MongoDate($startDate)),
                       // "endDate"        => array('$lt' => new MongoDate($endDate)));

        if (!empty($post["filters"])) {
            $where["thematique"] = $filters;
        }

        $allEvent = PHDB::findAndLimitAndIndex(Event::COLLECTION, $where);

        if($allEvent){

            $params = array(
                "result"    =>  true
            );

            $res["element"] = array();
            foreach($allEvent as $key => $value){
                
                $date = date(DateTime::ISO8601, $value["startDate"]->sec);
                $img = isset($value["profilMediumImageUrl"])?$value["profilMediumImageUrl"]:"";
                $description = isset($value["shortDescription"])?$value["shortDescription"]:"";
                array_push($res["element"], array(
                    "id"                    =>  (String) $value["_id"],
                    "name"                  =>  $value["name"],
                    "slug"                  =>  $value["slug"],
                    "shortDescription"      =>  $description,
                    "imageProfil"           =>  $img)
                );
            }

            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getEventCommunity($post){
        $params = array(
            "result" => false
        );

        // A crée dans un modèle a part ce code
        date_default_timezone_set('UTC');
        //On récupère sur ce mois-ci les évènements
        $date_array = getdate ();
        $numdays = $date_array["wday"];

        $endDate = strtotime(date("Y-m-d H:i", time() + ((30 - $numdays) * 24*60*60)));
        $startDate = strtotime(date("Y-m-d H:i"));

        $community = Element::getCommunityByTypeAndId($post["contextType"],$post["contextId"]);
        $allEventCommunity = array();
        $resultsData =array();
        foreach ($community as $key => $value) {
            $resultsData[$key] = Element::getElementById($key,$value["type"]);
        }

        if (isset($resultsData) && !empty($resultsData)) {
            foreach ($resultsData as $key => $value) {
                $allEventCommunity[$key] = PHDB::findAndLimitAndIndex(Event::COLLECTION, array("source.key" => $value["slug"]),3);
            }
        }
        if ($allEventCommunity) {
            
            $params = array(
                "result" => true
            );

            $res["element"] = array();

            foreach ($allEventCommunity as $keya => $valuea) {
                foreach ($valuea as $k => $v) {

                    $shortDescription =isset($v["shortDescription"])?$v["shortDescription"]:"";
                    $description = isset($v["description"])?$v["description"]:"";
                    $profilImageUrl = isset($v["profilImageUrl"])?$v["profilImageUrl"]:"";
                    $profilMediumImageUrl = isset($v["profilMediumImageUrl"])?$v["profilMediumImageUrl"]:"";
                    $profilThumbImageUrl = isset($v["profilThumbImageUrl"])?$v["profilThumbImageUrl"]:"";
                    $comment = isset($v["comment"])?$v["comment"]:"";
                    $commentCount = isset($v["commentCount"])?$v["commentCount"]:"";
                    $collectionCount = isset($v["collectionCount"])?$v["collectionCount"]:"";
                    array_push($res["element"], array(
                        "id"  =>  (String) $v["_id"],
                        "type"  =>  $v["type"],
                        "name"  =>  $v["name"],
                        "shortDescription"  => $shortDescription,
                        "description"  =>  $description,
                        "profilImageUrl"  =>  $profilImageUrl,
                        "profilMediumImageUrl"  =>  $profilMediumImageUrl,
                        "profilThumbImageUrl"  =>  $profilThumbImageUrl,
                        "comment"  =>  $comment,
                        "commentCount"  =>  $commentCount,
                        "collectionCount"  =>  $collectionCount
                ));
                }                
            }
           return array_merge($params,$res);
        }
        return $params;
    }
    public static function getPoi($post){
        $params =array(
            "result" => false
        );
        $limit = isset($post["limit"])?$post["limit"]:3;
        if (!empty($post["source"]) && isset($post["source"])) {
            $pList = PHDB::findAndSort(Poi::COLLECTION, 
                    array( 
                      "source.key"=>$post["source"],
                      "type"=>$post["type"]), array("updated"=>-1), $limit );
        }

        if ($pList) {
            $params =array(
                "result" => true
            );

            $res["element"] = array();
            foreach ($pList as $key => $value) {
                $res["element"][$key] = [
                    "id"  =>  (String) $value["_id"],
                    "type"  =>  $value["type"],
                    "name"  =>  $value["name"],
                    "shortDescription"  =>  ($value["shortDescription"])??"",
                    "description"  =>  ($value["description"])??"",
                    "profilImageUrl"  =>  ($value["profilImageUrl"])??"",
                    "profilMediumImageUrl"  =>  ($value["profilMediumImageUrl"])??"",
                    "profilThumbImageUrl"  =>  ($value["profilThumbImageUrl"])??"",
                    "comment"  =>  ($value["comment"])??"",
                    "commentCount"  =>  ($value["commentCount"])??"",
                    "collectionCount"  =>  ($value["collectionCount"])??"",
                    "sizeInColonne"  =>  ($value["sizeInColonne"])??"",
                ];
            }
            return array_merge($params,$res);
        }
        return $params;
    }

    public static function getJson($post){
        $json=file_get_contents("../../modules/costum/data/".$post.".json", FILE_USE_INCLUDE_PATH);
        return $json;

    }
    
}