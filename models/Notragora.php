<?php

class Notragora {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	

    public static $dataBinding_allPoi  = array(
        "id"        => array("valueOf" => "id"),
        "name"      => array("valueOf" => "name"),
        "links"       => array("valueOf" => "links"),
        "parent"       => array("valueOf" => "parent"),
        "image"     => array("valueOf" => "image",
                             "type"     => "url"),
        "medias"       => array("valueOf" => "medias"),
        "address"   => array("parentKey"=>"address", 
                             "valueOf" => array(
                                    "@type"             => "PostalAddress", 
                                    "streetAddress"     => array("valueOf" => "streetAddress"),
                                    "postalCode"        => array("valueOf" => "postalCode"),
                                    "addressLocality"   => array("valueOf" => "addressLocality"),
                                    "codeInsee"         => array("valueOf" => "codeInsee"),
                                    "addressRegion"     => array("valueOf" => "addressRegion"),
                                    "addressCountry"    => array("valueOf" => "addressCountry")
                                    )),
        "geo"   => array("parentKey"=>"geo", 
                             "valueOf" => array(
                                    "@type"             => "GeoCoordinates", 
                                    "latitude"          => array("valueOf" => "latitude"),
                                    "longitude"         => array("valueOf" => "longitude")
                                    )),
        "description"       => array("valueOf" => "description"),
        "tags"      => array("valueOf" => "tags"),
    );
	public static function setSourceAllElement($params){
        if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"]) )){
            $colElts = array(Person::COLLECTION, Organization::COLLECTION, Poi::COLLECTION);
            foreach ($colElts as $keyCol => $col) {
                $source = array(
                    'insertOrign' => "import",
                    'date'=> new MongoDate(time()),
                    "key" => "notragora",
                    "keys" => array("notragora") );
                      
                PHDB::update($col,
                    array() , 
                    array('$set' => array("source" => $source))
                );

            }
        }
        return $params;
    }

     public static function elementBeforeSave($data){
        if($data["collection"]==Badge::COLLECTION){
            $elt=Element::getElementById($data["id"], $data["collection"], null, array("name", "category"));
            if(!empty($data["elt"]["parent"])){
                unset($data["elt"]["parent"]);
            }


            if(!empty($elt["name"])){
                $where = array( "source.key" => "notragora",
                                "tags" => array('$in' => array($elt["name"]) ) ) ;

                //Rest::json( $where) ; exit; 
                $actionPush = array('$push' => array('tags' => $data["elt"]["name"] ) );

                $actionPull = array('$pull' => array('tags' => $elt["name"] ) );

                $options = array('multiple' => 1);
                //$rescount = PHDB::count(Poi::COLLECTION, $where);
                $resPush = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPush, $options );
                $resPull = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPull, $options );

                // $res = array("rescount" => $rescount,
                //     "resPush" => $resPush,
                //     "resPull" => $resPull,
                //     "actionPull" => $actionPull,
                //     "actionPush" => $actionPush,
                //     "options" => $options);
                //Rest::json($res); exit; 
            }
            //Rest::json($data); exit; 
        }
        //var_dump($data);
        return $data;
    }

    public static function prepData($data){
        //var_dump("here");Rest::json($data); exit;

        if($data["collection"] == Badge::COLLECTION){
            if(!empty($data["tags"]))
                $data["tags"][] = $data["name"]; 
            else
                $data["tags"] = array($data["name"]);
        }else if($data["collection"] == Organization::COLLECTION){
            if(empty($data["categoryNA"])){
                $data["categoryNA"] = array("group");
            }
        }
        return $data;
    }

    public static function elementBeforeDelete($data){
        
        if($data["collection"]==Badge::COLLECTION){
            $elt=Element::getElementById($data["id"], $data["collection"], null, array("name", "category"));

            if(!empty($elt["name"])){
                // $where = array( "source.key" => "notragora",
                //                 "tags" => array('$in' => array($elt["name"]) ) ) ;
                // $actionPull = array('$pull' => array('tags' => $elt["name"] ) );
                // $options = array('multiple' => 1);
                // $resPull = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPull, $options );


                //Rest::json( $where) ; exit; 
                //$actionPush = array('$push' => array('tags' => $data["elt"]["name"] ) );
                // $rescount = PHDB::count(Poi::COLLECTION, $where);
                // $resPush = PHDB::updateWithOptions( Poi::COLLECTION, $where, $actionPush, $options );
                // $res = array("rescount" => $rescount,
                //     "resPush" => $resPush,
                //     "resPull" => $resPull,
                //     "actionPull" => $actionPull,
                //     "actionPush" => $actionPush,
                //     "options" => $options);
            }
        }

        return $data;
    }

    const TRAD = array(
        "{who} added a new point of interest : {what} on {where}" => "{who} a ajouté une production : {what} dans {where}",
        "{who} have added points of interest on {where}"=>"{who} ont ajouté des productions sur {where}",
        "{who} added points of interest on {where}"=>"{who} a ajouté des productions sur {where}",
    );

    public static function translateLabel($data){
        $res = null;
        if( !empty($data["label"]) && array_key_exists($data["label"], self::TRAD) ) {
            $data["label"] = self::TRAD[ $data["label"] ];
        }
        return $data;
    }

    public static function addSearchFields(){
        // $fields=array();
        // if(in_array("poi", $searchType)){
            $fields=["medias","urls","year","producors","supports"];
        //}
        return $fields;

    }

    public static function extractUrl($url){
        $expl=explode("#", $url);
        $id="";
        $type="";
        $hash=(isset($expl)) ? explode(".",$expl[1]) : $url;

        if(isset($hash) && count($hash)>1 && in_array($hash[0], ["poi","collection","genre"])){

            $name=$hash[1];
            $collec=($hash[0]=="poi") ? Poi::COLLECTION : Badge::COLLECTION ;
            $prod=PHDB::find($collec,array("source.key"=>"notragora"));

            foreach($prod as $key => $value){
                $valName=InflectorHelper::slugify2($value["name"]);
                if($name=$valName){
                    $id=$key;
                    $type=$value["collection"];

                }
            }
            $url=$expl[0]."/#page.type.".$type.".id.".$id;
        }       
        return $url;
    }

}
?>
