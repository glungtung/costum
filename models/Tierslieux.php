<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;

class Tierslieux {

	public static function getSortedZone(){
		$orderBy = array("countryCode" => 1, "name" => 1);

		return $orderBy;
	}

	public static function getNetwork($costumSlug){
		//var_dump($costumSlug);
		$network=PHDB::find(Organization::COLLECTION,array('$and' => array(array("category" => "network"),array("source.key"=>$costumSlug))));
		return $network;
	}

	public static function referenceFromFtl($costumSlug){
		$elements=PHDB::findByIds(Organization::COLLECTION, $ids);			

				 if(!empty($elements)){
				 	$countRef=0;
				 	$countNew=0;
				 	foreach($elements as $data){
				 			$newreference = array();
				 			if(!empty($data["reference"])){	 				
								$newreference["costum"] = $data["reference"]["costum"];
									array_push($newreference["costum"],"franceTierslieux");
									try {
										$res = PHDB::update( Organization::COLLECTION, 
									  		array("_id"=>new MongoId($data["_id"])),
				                        	array('$set' => array(	"reference.costum" => $newreference["costum"])));
									} catch (MongoWriteConcernException $e) {
										echo("Erreur à la mise à jour de la référence existante de ".Organization::COLLECTION." avec l'id ".$data);
										die();
									}
				 					$countRef++;

				 				
				 			}
				 			else{
				 				try {
										$res = PHDB::update( Organization::COLLECTION, 
									  		array("_id"=>new MongoId($data["_id"])),
				                        	array('$set' => array("reference.costum"=> array("franceTierslieux"))));
										$countNew++;
									} catch (MongoWriteConcernException $e) {
										echo("Erreur de la création de la référence à jour de l'élément ".Organization::COLLECTION." avec l'id ".$data);
										die();
									}
				 					$countNew++;
				 			}
				 	}
				 	echo $countNew ." tiers-lieux avec nouvelle référence !";
				 	echo "\n".$countRef." tiers-lieux mis a jour avec référence en plus !" ;
				}
	}			

	

	public static function elementAfterSave($data){
        
		
    	 $elt=Element::getElementById($data["id"], $data["collection"]);
    	 $isInviting=false;
    	if($data["collection"]==Organization::COLLECTION && isset($elt["category"]) && $elt["category"]=="network"){ 

	    	$where=array("tags"=>$elt["name"]);
	    	$community=PHDB::find(Organization::COLLECTION,$where);
	        
	        if($data["collection"]==Organization::COLLECTION && isset($community)){
	        	foreach ($community as $key => $val){
	        			Link::connect($data["id"],$data["collection"],$val["_id"],$val["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
	        			Link::connect($val["_id"],$val["collection"],$data["id"],$data["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
	        		
				}

			
	        }
	    } 
	    else if($data["collection"]==Organization::COLLECTION && !isset($elt["category"])){ 
	    	$costum = CacheHelper::getCostum();
			$networks=$costum["lists"]["network"];
	    	$isInviting=false;
	    	$child=[];
	    	$child["childId"]=$elt["_id"];
        	$child["childType"]=$elt["collection"];
	    	//$network=PHDB::find(Organization::COLLECTION,$where);
	        foreach($elt["tags"] as $tag){
	        	if(in_array($tag, $networks)){
	        		$where=array("name"=>$tag);
	        		$net=Element::getElementByWhere(Organization::COLLECTION,$where);
	        		if($net){
	        		// Link::connectParentToChild($net["_id"],$net["collection"],$child,false,Yii::app()->session["userId"]);
		        		Link::connect($net["_id"],$net["collection"],$elt["_id"],$elt["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        		Link::connect($elt["_id"],$elt["collection"],$net["_id"],$net["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        	}	
		        }
	        }
	    }

	    if($data["collection"]==Organization::COLLECTION){
    		//var_dump("expression");exit;
	    	Admin::addSourceInElement($data["id"],$data["collection"],"franceTierslieux","reference");
	    }    
    }

}


?>