<?php

class Meir {
    const COLLECTION = "costum";
    const CONTROLLER = "costum";
    const MODULE = "costum";
    public static function removeSourceFromElement($params){
        $elt=Element::getElementById($params["id"], $params["type"]);
        $costum = CacheHelper::getCostum();
        if(isset($elt["tags"])){ 
            foreach($costum["lists"]["family"] as $kF => $vT){                
                if (($key = array_search($kF, $elt["tags"])) !== false) {
                    unset($elt["tags"][$key]);
                } 
            }
            foreach($costum["lists"]["themes"] as $kT => $vT){                
                if (($keyT = array_search($kT, $elt["tags"])) !== false || ($keyT = array_search(str_replace("&","&amp;",$kT), $elt["tags"])) !== false) {
                    unset($elt["tags"][$keyT]);
                } 
            }
            foreach($costum["lists"]["status"] as $kS => $vS){                
                if (($keyS = array_search($kS, $elt["tags"])) !== false) {
                    unset($elt["tags"][$keyS]);
                } 
            }
        } 
        $unset = [
            "legalStatus" => true,
            "responsable" => true,
            "formLocality" => true,
            "mobile" => true,
            "jobFamily" => true,
            "objective" => true,
            "serviceOffers" => true,
            "thematic" => true,
            "status" => true,
            "linkFinancialDevice" => true,
            "modality" => true,
            "typeFinancing" => true,
            "financialPartners" => true,
            "maximumAmount" => true,
            "publicCible" => true,
            "referentId" => true,
            "toBeValidated" => true,
            "category" => true
        ];

        $set = [
            "tags"=> $elt["tags"]
        ];
        if(isset($elt["source"])){
            if($elt["source"]["key"] == "meir") 
                $unset["source"] = true;
            if($elt["source"]["key"] != "meir" && ($keyS = array_search("meir", $elt["source"]["keys"]) !== false)){
                unset($set["source"]["keys"][$keyS]);
                $set["source.keys"] =  $elt["source"]["keys"];
            }

        }
        PHDB::update($params["type"], array( "_id" => new MongoId($params["id"])),array('$unset' => $unset,'$set' => $set )); 

    }
    public static function urlAfterSave($data){
        if($data["collection"] == Organization::COLLECTION && isset($data["elt"]["category"]) && $data["elt"]["category"] == "acteurMeir"){
            if(isset($data["elt"]["role"])){
                $params["role"] = $data["elt"]["role"];
                $params["_id"] = $data["id"];
                Link::afterSave($data["collection"], $params);
            }

        }
        return null ;
    }
    public static function canEditFiche($userId , $element) {
        $res = false;
        if(Authorisation::isInterfaceAdmin() && isset($element["category"]) && $element["category"] == "acteurMeir" ){
            if(!isset($element["links"]["members"]))
                $res = true;
            if (Authorisation::isOpenEdition((string)$element["_id"], @$element["collection"]))
                $res = true;
            if( isset($element["links"]["members"][Yii::app()->session["userId"]]["isAdmin"])  
                && !isset($element["links"]["members"][Yii::app()->session["userId"]]["isAdminPending"]) 
                && !isset($element["links"]["members"][Yii::app()->session["userId"]]["toBeValidated"])) 
                    $res = true;
            if(isset($element["reference"]["costum"]) 
                && array_search("meir", $element["reference"]["costum"]) === false 
                && isset($element["category"]) && $element["category"] == "acteurMeir")
                $res = true;
        }
        return $res;
    }
}