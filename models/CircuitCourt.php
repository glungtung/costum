<?php

class CircuitCourt {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	public static function prepData($params){

        if(!empty($params["badges"])){
            if(is_string($params["badges"]))
                $params["badges"] = array($params["badges"]);
            $badges = PHDB::find(Badge::COLLECTION, array('name' => array('$in' => $params["badges"]) ), array( 'name') );
            $resB = array();
            if(!empty($badges)){

                if(empty($params["tags"]))
                    $params["tags"] = array();

                foreach ($badges as $keyB => $valB) {
                    $resB[] = $keyB;
                    if(!empty($valB["name"]) && !in_array($valB["name"], $params["tags"]))
                        $params["tags"][] = $valB["name"];
                }
            }
            $params["badges"] =  $badges ;
        }

        if(!empty($params["phone"])){
            $split = explode(",", $params["phone"]);
            $fieldValue = array();
            foreach ($split as $key => $value) {
                $fieldValue[] = trim($value);
            }
            $params["telephone"] = array("fixe" => $fieldValue);
        }
        //Rest::json($params); exit;
        return $params;
    }
}
?>