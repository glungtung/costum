<?php
	class LaPossession{
		const COLLECTION = "costums";
		const CONTROLLER =	"costum";
		const MODULE = "costum";

		public static function getOrganization($type,$slug) {
			$params["result"] = false;

			$res = PHDB::find( $type , ["source.key" => $slug] , ["_id","name","type","address","geo","geoPosition","slug","source","profilThumbImageUrl","profilMediumImageUrl","profilMarkerImageUrl","collection"]);

			if (@$res) {
				$results = array();
				$params["result"] = true;

				$results = self::DataResults($res);
				return array_merge($params,$results);
				// array_merge($params,$res);
			}

			return $params;
		}

		public static function DataResults($community){
			$res["elt"] = array();

	       foreach($community as $key => $value){
	        // var_dump($key);
	        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : null);
	        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : null);
			$imgBanner = (@$value["profilRealBannerUrl"] ? $value["profilRealBannerUrl"] : null);
			$profilThumbImageUrl = (@$value["profilThumbImageUrl"] ? $value["profilThumbImageUrl"] : null);
			$profilMarkerImageUrl = (@$value["profilMarkerImageUrl"] ? $value["profilMarkerImageUrl"] : null);
	        //$countActus = PHDB::count(News::COLLECTION,array("source.key" => $value["source"]["key"]));

	           $res["elt"][$key] = array(
	               "id"               => (String) $value["_id"],
	               "name"             =>  $value["name"],
	               //"imgMedium"        =>  $imgMedium,
	               //"img"              =>  $img,
	               //"imgBanner"        =>  $imgBanner,
	               "type"             => $value["type"],
	               //"profilMarkerImageUrl" => @$profilMarkerImageUrl,
	               "address"          =>  @$value["address"],
	               "geo"              =>  @$value["geo"],
	               "geoPosition"      =>  @$value["geoPosition"],
	               //"profilThumbImageUrl" => @$profilThumbImageUrl,
				   "slug"             =>  $value["source"]["key"],  
				   "collection" => @$value["collection"],
	               //"countEvent"       =>  count(@$value["links"]["events"]),
	               //"countActeurs"     =>  count(@$value["links"]["members"]),
	               //"countProjet"      =>  count(@$value["links"]["projects"]),
	               //"countActus"       =>  @$countActus
			   );
			   if($imgMedium){
				   $res["elt"][$key]["imgMedium"] = $imgMedium;
			   }
			   if($img){
				   $res["elt"][$key]["img"] = $img;
			   }
			   if($imgBanner){
				   $res["elt"][$key]["imgBanner"] = $imgBanner;
			   }
			   if($profilMarkerImageUrl){
				   $res["elt"][$key]["profilMarkerImageUrl"] = $profilMarkerImageUrl;
			   }
			   if($profilThumbImageUrl){
				   $res["elt"][$key]["profilThumbImageUrl"] = $profilThumbImageUrl;
			   }
			   
			   
			   
			   
	        }
	        // var_dump($res);
    		return $res;
		}


		public static function updateBlock($params){
		$block = $params["block"];
		$collection = $params["typeElement"];
		$id = $params["id"];
		$res = array();
		try {

			if($block == "info"){
				if(isset($params["name"])){
					$res[] = Element::updateField($collection, $id, "name", $params["name"]);
					/*PHDB::update( $collection,  array("_id" => new MongoId($id)), 
			 										array('$unset' => array("hasRC"=>"") ));*/
				}
				if(isset($params["username"]) && $collection == Person::COLLECTION)
					$msgError = Yii::t("common","Username cannot be changed.");
					//$res[] = Element::updateField($collection, $id, "username", $params["username"]);
				if(isset($params["avancement"]) && $collection == Project::COLLECTION)
					$res[] = Element::updateField($collection, $id, "avancement", $params["avancement"]);
				if(isset($params["tags"]))
					$res[] = Element::updateField($collection, $id, "tags", $params["tags"]);
				if(isset($params["type"])  && ( $collection == Event::COLLECTION || $collection == Organization::COLLECTION) )
					$res[] = Element::updateField($collection, $id, "type", $params["type"]);
				if(isset($params["email"])){
					if(!empty($params["email"])){
						$mail = Mail::authorizationMail($params["email"]);
						if($mail == false){
							unset($params["email"]);
							throw new CTKException("Vous ne pouvez pas renseigner cette adresse mail.");
						}
						
					}

					$resEmail=Element::updateField($collection, $id, "email", $params["email"]);
					$res[] = $resEmail;
					// Mail reference inivite on communecter
					if($resEmail["result"] && in_array($collection,[Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION])){
						if(@$params["email"] && !empty($params["email"]) && $params["email"]!=@Yii::app()->session["userEmail"]){
							Mail::referenceEmailInElement($collection, $id, $params["email"]);
						}
					}
				}

				if(isset($params["slug"])){
					$el = PHDB::findOne($collection,array("_id"=>new MongoId($id)));
					$oldslug = @$el["slug"];
					if(!empty(Slug::getByTypeAndId($collection,$id)))
						Slug::update($collection,$id,$params["slug"]);
					else
						Slug::save($collection,$id,$params["slug"]);
					$res[] = Element::updateField($collection, $id, "slug", $params["slug"]);
				}
				//update RC channel name if exist
				if(@$el["hasRC"]){
					RocketChat::rename( $oldslug, $params["slug"], @$el["preferences"]["isOpenEdition"] );
				}
				if(isset($params["url"]))
					$res[] = Element::updateField($collection, $id, "url", Element::getAndCheckUrl($params["url"]));
				if(isset($params["birthDate"]) && $collection == Person::COLLECTION)
					$res[] = Element::updateField($collection, $id, "birthDate", $params["birthDate"]);
				if(isset($params["fixe"]))
					$res[] = Element::updateField($collection, $id, "fixe", $params["fixe"]);
				if(isset($params["fax"]))
					$res[] = Element::updateField($collection, $id, "fax", $params["fax"]);
				if(isset($params["mobile"]))
					$res[] = Element::updateField($collection, $id, "mobile", $params["mobile"]);
				
				if( !empty($params["parentId"]) ){
					$parent["parentId"] = $params["parentId"] ;
					$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
					$resParent = Element::updateField($collection, $id, "parent", $parent);
					if($parent["parentType"] != "dontKnow" && $parent["parentId"] != "dontKnow")
						$resParent["value"]["parent"] = Element::getByTypeAndId( $params["parentType"], $params["parentId"]);
					$res[] = $resParent;
				}
				if( !empty($params["parent"]) ){
					//$parent["parentId"] = $params["parentId"] ;
					//$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
					$resParent = Element::updateField($collection, $id, "parent", $params["parent"]);
					foreach($resParent["value"] as $key => $value) {
						//var_dump($value["type"]); exit;
						$elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl"));
                		$resParent["value"][$key]=array_merge($resParent["value"][$key], $elt);
					}
					$res[] = $resParent;
				}
				if(!empty($params["organizerId"]) ){
					$organizer["organizerId"] = $params["organizerId"] ;
					$organizer["organizerType"] = ( !empty($params["organizerType"]) ? $params["organizerType"] : "dontKnow" ) ;
					$resOrg = Element::updateField($collection, $id, "organizer", $organizer);

					if($params["organizerType"]!="dontKnow" && $params["organizerId"] != "dontKnow"){
						$resOrg["value"]["organizer"] = Element::getByTypeAndId( $params["organizerType"], $params["organizerId"]);
					}
					$res[] = $resOrg;
				}
				if( !empty($params["organizer"]) ){
					//$parent["parentId"] = $params["parentId"] ;
					//$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
					$resOrg = Element::updateField($collection, $id, "organizer", $params["organizer"]);
					foreach($resOrg["value"] as $key => $value) {
						$elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl"));
                		$resOrg["value"][$key]=array_merge($resOrg["value"][$key], $elt);
					}
					$res[] = $resOrg;
				}

			}else if($block == "network"){
				if(isset($params["telegram"]) && $collection == Person::COLLECTION)
					$res[] = Element::updateField($collection, $id, "telegram", $params["telegram"]);
				if(isset($params["facebook"]))
					$res[] = Element::updateField($collection, $id, "facebook", Element::getAndCheckUrl($params["facebook"]));
				if(isset($params["twitter"]))
					$res[] = Element::updateField($collection, $id, "twitter", Element::getAndCheckUrl($params["twitter"]));
				if(isset($params["github"]))
					$res[] = Element::updateField($collection, $id, "github", Element::getAndCheckUrl($params["github"]));
				if(isset($params["gpplus"]))
					$res[] = Element::updateField($collection, $id, "gpplus", Element::getAndCheckUrl($params["gpplus"]));
				if(isset($params["skype"]))
					$res[] = Element::updateField($collection, $id, "skype", Element::getAndCheckUrl($params["skype"]));
				if(isset($params["diaspora"]))
					$res[] = Element::updateField($collection, $id, "diaspora", Element::getAndCheckUrl($params["diaspora"]));
				if(isset($params["mastodon"]))
					$res[] = Element::updateField($collection, $id, "mastodon", Element::getAndCheckUrl($params["mastodon"]));
				if(isset($params["instagram"]))
					$res[] = Element::updateField($collection, $id, "instagram", Element::getAndCheckUrl($params["instagram"]));

			}else if( $block == "when" && ( $collection == Event::COLLECTION || $collection == Project::COLLECTION) ) {
				
				if(isset($params["allDayHidden"]) && $collection == Event::COLLECTION)
					$res[] = Element::updateField($collection, $id, "allDay", (($params["allDayHidden"] == "true") ? true : false));
				if(isset($params["startDate"]))
					$res[] = Element::updateField($collection, $id, "startDate", $params["startDate"],@$params["allDay"]);
				if(isset($params["endDate"]))
					$res[] = Element::updateField($collection, $id, "endDate", $params["endDate"],@$params["allDay"]);
			
			}else if($block == "toMarkdown"){

				$res[] = Element::updateField($collection, $id, "description", $params["value"]);
				$res[] = Element::updateField($collection, $id, "descriptionHTML", null);

			}else if($block == "descriptions"){

				if(isset($params["tags"]))
					$res[] = Element::updateField($collection, $id, "tags", $params["tags"]);

				if(isset($params["description"])){
					$res[] = Element::updateField($collection, $id, "description", $params["description"]);
					Element::updateField($collection, $id, "descriptionHTML", null);
				}
				
				if(isset($params["shortDescription"]))
					$res[] = Element::updateField($collection, $id, "shortDescription", strip_tags($params["shortDescription"]));

				if(isset($params["category"]))
					$res[] = Element::updateField($collection, $id, "category", strip_tags($params["category"]));
				// $res[] = Costum::sameFunction("category", $params["category"]);
			
			}else if($block == "activeCoop"){

				if(isset($params["status"]))
					$res[] = Element::updateField($collection, $id, "status", $params["status"]);
				if(isset($params["voteActivated"]))
					$res[] = Element::updateField($collection, $id, "voteActivated", $params["voteActivated"]);
				if(isset($params["amendementActivated"]))
					$res[] = Element::updateField($collection, $id, "amendementActivated", $params["amendementActivated"]);
			
			}else if($block == "amendement"){

				if(isset($params["txtAmdt"]) && isset($params["typeAmdt"]) && isset($params["id"]) && @Yii::app()->session['userId']){
					$proposal = Proposal::getById($params["id"]);
					$amdtList = @$proposal["amendements"] ? $proposal["amendements"] : array();
					$rand = rand(1000, 100000);
					while(isset($amdtList[$rand])){ $rand = rand(1000, 100000); }

					$amdtList[$rand] = array(
										"idUserAuthor"=> Yii::app()->session['userId'],
										"typeAmdt" => $params["typeAmdt"],
										"textAdd"=> $params["txtAmdt"]);
					Notification::constructNotification ( ActStr::VERB_AMEND, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$proposal["parentType"],"id"=>$proposal["parentId"]),array( "type"=>Proposal::COLLECTION,"id"=> $params["id"] ) );
					$res[] = Element::updateField($collection, $id, "amendements", $amdtList);
				}
			
			}else if($block == "curiculum.skills"){
				$parent = Element::getByTypeAndId($params["typeElement"], $params["id"]);
				$cv = @$parent["curiculum"] ? $parent["curiculum"] : array();

				$CVAttrs = array("competences", "mainQualification", "hasVehicle", "languages",
								"motivation", "driverLicense", "url");
				foreach ($CVAttrs as $att) {
					if(@$params[$att]) 
					$cv["skills"][$att] = @$params[$att];
				}
				$res[] = Element::updateField($collection, $id, "curiculum", $cv);
				//var_dump($params);
			}else if($block == "curiculum.lifepath"){
				$parent = Element::getByTypeAndId($params["typeElement"], $params["id"]);
				$cv = @$parent["curiculum"] ? $parent["curiculum"] : array();
				$indexLP = @$cv["lifepath"] ? sizeof($cv["lifepath"]) : 0;
				
				$CVAttrs = array("title", "description", "startDate", "endDate",
								"location");
				foreach ($CVAttrs as $att) {
					if(@$params[$att]) 
					$cv["lifepath"][$indexLP][$att] = @$params[$att];
				}
				$res[] = Element::updateField($collection, $id, "curiculum", $cv);
				//var_dump($params);
			}else if($block == "localities"){

				$set = array();
				$unset = array();
				if(!empty($params["address"])){
					$set["address"] = $params["address"];
				} else {
					$unset["address"] = array();
				}

				if(!empty($params["geo"])){

					$set["geo"] = SIG::getFormatGeo($params["geo"]["latitude"], $params["geo"]["longitude"]);
					$set["geoPosition"] = SIG::getFormatGeoPosition($params["geo"]["latitude"], $params["geo"]["longitude"]);
				} else {
					$unset["geo"] = array();
					$unset["geoPosition"] = array();
				}

				if(!empty($params["addresses"])){
					$set["addresses"] = $params["addresses"];
				} else {
					$unset["addresses"] = array();
				}

				//Rest::json($set) ; exit;
				if(!empty($set)){
					PHDB::update( 	$collection, 
										array("_id" => new MongoId($id)), 
			                          	array('$set' => $set ) );
				}

				if(!empty($unset)){
					PHDB::update( 	$collection, 
										array("_id" => new MongoId($id)), 
			                          	array('$unset' => $unset ) );
				}
				

				$res[] = array("result"=>true, "value" => $set, "fieldName"=> "localities");
			}

			if(Import::isUncomplete($id, $collection)){
				Import::checkWarning($id, $collection, Yii::app()->session['userId'] );
			}

			if( $collection == Event::COLLECTION || $collection == Project::COLLECTION || $collection == Organization::COLLECTION ){
				$el = PHDB::findOneById($collection,$id,array('slug'));
        		Slug::updateElemTime( $el['slug'], time() );
			}


			$result = array("result"=>true);
			$resultGoods = array();
			$resultErrors = array();
			$values = array();
			$msg = "";
			$msgError = "";
			foreach ($res as $key => $value) {
				if($value["result"] == true){
					if($msg != "")
						$msg .= ", ";
					$msg .= Yii::t("common",$value["fieldName"]);
					$values[$value["fieldName"]] = $value["value"];
				}else{
					if($msgError != "")
						$msgError .= ". ";
					$msgError .= $value["mgs"];
				}
			}

			if($msg != ""){
				$resultGoods["result"]=true;
				$resultGoods["msg"]= Yii::t("common", "Les attributs ont étés mis à jour")." ".Yii::t("common",$msg);
				$resultGoods["values"] = $values ;
				$result["resultGoods"] = $resultGoods ;
				$result["result"] = true ;
			}

			if($msgError != ""){
				$resultErrors["result"]=false;
				$resultErrors["msg"]=Yii::t("common", $msgError);
				$result["resultErrors"] = $resultErrors ;
			}
		} catch (CTKException $e) {
			$resultErrors["result"]=false;
			$resultErrors["msg"]=$e->getMessage();
			$result["resultErrors"] = $resultErrors ;
		}
		return $result;
	}
	}

?>