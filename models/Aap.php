<?php
use PhpParser\Node\Expr\AssignOp\Div;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Pdf;
class Aap{
    const COLLECTION = "aap";
    const CONTROLLER = "aap";
    const MODULE = "costum";
	public static $badgelabel = [
		"progress" => "En cours",
		"vote" => "En evaluation",
		"funded" => "En financement",
		"call" => "Appel à participation",
		"newaction" => "nouvelle proposition",
		"prevalided" => "Pré validé",
		"validproject" => "Projet validé",
		"voted" => "Voté",
		"finish" => "Términé",
		"suspend" => "Suspendu",
		"underconstruction" => "En construction",
		"projectstate" => "En projet",
		"notified" => "Notification vue",
		"notificationSent" => "Notification envoyée",
		"unnotified" => "Non notifié",
		"renewed" => "Reconduit"
	];
	public static $statusicon = [
		"progress" => "hourglass" ,
		"finished" => "check-circle" ,
		"abandoned" => "times-circle",
		"newaction" => "plus-circle",
		"call" => "tag",
		"projectstate" => "rocket",
		"funded" => "euro",
		"vote" => "gavel",
		"underconstruction" => "flag" ,
		"prevalided" => "archive",
		"validproject" => "check" ,
		"voted" => "suitcase",
		"notified" => "envelope",
		"notificationSent" => "paper-plane",
		"renewed" => "check"
	];
	public static function generateConfig($post){
        if(isset($post["aapConfig"]))
            $addAapConfig = PHDB::insert(Form::COLLECTION,$post["aapConfig"]);
        if(isset($post["formParent"]))
            $addFormParent = PHDB::insert(Form::COLLECTION,$post["formParent"]);
        if(isset($post["stepInputs"])){
            $steps = array();
            foreach ($post["stepInputs"] as $ks => $vs) {
                $steps[$ks] = $vs;
            }
            if(!empty($steps))
                $addSteps = PHDB::batchInsert("inputs",$steps);
        }

        if($addAapConfig && $addFormParent && $addSteps)
            return Rest::json(array("result"=>true,"msg"=>Yii::t("common","Information updated")));
    }
	public static function description($answer,$defaultLength=200){
		$description = !empty($answer["answers"]["aapStep1"]["description"]) ? $answer["answers"]["aapStep1"]["description"] : '';
		$html = "";
		$html.= '<div id="less-'. (string) $answer["_id"] .'" class="list-group-item-text project-description list-item-description list-markdown">'.substr($description, 0, $defaultLength);
		$html.= strlen($description) > $defaultLength ? '<b>. . .</b> <button onclick="document.getElementById(\'less-'.(string) $answer["_id"].'\').style.display = \'none\';document.getElementById(\'more-'.(string) $answer["_id"].'\').style.display = \'table\';" class="moreLinkaap btn-xs btn btn-default">'.Yii::t("form","Read more").'</button>' : "";
		$html.= '</div>';
		if(strlen($description) > $defaultLength){
			$html.= '<div id="more-'. (string) $answer["_id"] .'" style="display:none" class="list-group-item-text project-description list-item-description list-markdown">'. $description;
			$html.= '<button onclick="document.getElementById(\'less-'.(string) $answer["_id"].'\').style.display = \'table\';document.getElementById(\'more-'.(string) $answer["_id"].'\').style.display = \'none\';" class="btn btn-xs lessLinkaap btn-xs btn btn-default">'.Yii::t("common","Read less").'</button>';
			$html.= '</div>';
		}
		return $html;
	}

	public static function canEditOrSee($elId,$elType,$form,$step){
		$canEditItem = Authorisation::canEditItem(Yii::app()->session['userId'],$elType,$elId);
		$canRead = Authorisation::canSee($elType,$elId);
		
		$canEditRoles = !empty($form["params"][$step]["canEdit"]) ? explode(",",$form["params"][$step]["canEdit"]) : [];
		$canReadRoles = !empty($form["params"][$step]["canRead"]) ? explode(",",$form["params"][$step]["canRead"]) : [];
		$haveEditingRules = !empty($form["params"][$step]["haveEditingRules"]) ? explode(",",$form["params"][$step]["haveEditingRules"]) : [];
		$haveReadingRules = !empty($form["params"][$step]["haveReadingRules"]) ? explode(",",$form["params"][$step]["haveReadingRules"]) : [];

		$hasCanEditRoles = Link::hasRoles($elId,$elType,$canEditRoles);
		$hasCanReadRoles = Link::hasRoles($elId,$elType,$canReadRoles);
		return array(
			"canEdit" => $canEditItem,
			"hasCanEditRoles" => $hasCanEditRoles || $canEditItem,
			"canSee" => $canRead,
			"hasCanReadRoles" => ($hasCanReadRoles && $canRead) || $canEditItem,
		);
	}

    public static function globalAutocomplete($form, $searchParams){
		$form = PHDB::findOneById(Form::COLLECTION,$form);
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$mappingData=(isset($form["mapping"])) ? $form["mapping"] : array("name"=>"name", "address"=>"address","answers.aapStep1.titre"=>"answers.aapStep1.titre");
		$query = array();
		$formStandalone = false;
		if(!empty($searchParams["filters"]["formStandalone"])){
			$formStandalone = (boolean) $searchParams["filters"]["formStandalone"];
			unset($searchParams["filters"]["formStandalone"]);
		}

		$elId = array_keys($form["parent"])[0];
		$elType = $form["parent"][$elId]["type"];
		
		if(!empty($form["params"]["onlyAdminCanSeeList"]) && $form["params"]["onlyAdminCanSeeList"] && !$formStandalone){
			$canEditORsee = self::canEditOrSee($elId,$elType,$form,"aapStep2");

			if(!$canEditORsee["hasCanEditRoles"]){
				$params=array('$or' => array(
					array("user"=> Yii::app()->session['userId'])
				));
				$query = SearchNew::addQuery( $query , $params);
			}
		}

		if(!empty($searchParams["filters"]["seen"]) && !empty($searchParams["filters"]["form"])){
			if(!empty($searchParams["filters"]["seen"][0])){
				$seenKey = $searchParams["filters"]["seen"][0];
				if($seenKey == "notSeen"){
					$q = array(
						"user" => ['$ne' => Yii::app()->session['userId']],
						"views.".Yii::app()->session['userId'] => ['$exists' => false]
					);
					$query = SearchNew::addQuery( $query , $q);
				}elseif($seenKey == "seen"){
					$q = array(
						'$or' => [
							array("user" => Yii::app()->session['userId']),
							array("views.".Yii::app()->session['userId'] => ['$exists' => true])
						]
					);
					$query = SearchNew::addQuery( $query , $q);
				}
			}
			unset($searchParams["filters"]["seen"]);
		}

		if(!empty($searchParams["filters"]["admissibility"])){
			$admissibility = $searchParams["filters"]["admissibility"][0];
			$q = array('$and'=>[
					array('$or' => [])
				]);
			$members = Element::getCommunityByTypeAndId($elType,$elId, "citoyens");
			foreach ($members as $kMemb => $vMemb) {
				if($admissibility=="admissible")
					$q['$and'][0]['$or'][] = array("answers.aapStep2.admissibility.".$kMemb => [ '$ne'=> "inadmissible" ]);
				elseif($admissibility=="inadmissible") {
					$q['$and'][0]['$or'][] = array("answers.aapStep2.admissibility.".$kMemb => "inadmissible");
				}
			}
			if(!empty($members))
				$query = SearchNew::addQuery( $query , $q);
			unset($searchParams["filters"]["admissibility"]);
		}
		if(!empty($searchParams["filters"]["answers.aapStep1.depense.financer.idAndName"])){
			$dataFinancors = explode("-idAndName-",$searchParams["filters"]["answers.aapStep1.depense.financer.idAndName"][0]);
			$idFinancor = $dataFinancors[0];
			$nameFinancor = $dataFinancors[1];
			$queryFinancors = array('$and'=>[
				array('$or' => [
					array("answers.aapStep1.depense.financer.name" => $nameFinancor),
					array("answers.aapStep1.depense.financer.id" => $idFinancor),
				])
			]);

			$query = SearchNew::addQuery( $query , $queryFinancors);
			unset($searchParams["filters"]["answers.aapStep1.depense.financer.idAndName"]);
		}

		if(!empty($searchParams["filters"]["sousOrganisation"]) && !empty($searchParams["filters"]["form"])){
			$prms = $searchParams["filters"]["sousOrganisation"];
			//$prms[] = $searchParams["filters"]["form"]; //form of orga parent
			$parseParams = [];
			foreach ($prms as $kprms => $vprms) {
				if(strpos($vprms,"-")){
					$vprms = explode('-',$vprms);
					foreach ($vprms as $kk => $vv) {
						$parseParams[]=$vv;
					}
				}else{
					$parseParams[]=$vprms;
				}
			}
			$query=SearchNew::addQuery($query,array("form"=>['$in' => $parseParams]));
			unset($searchParams["filters"]["sousOrganisation"]);
			if(!empty($searchParams["filters"]["allSousOrganisation"]))
				unset($searchParams["filters"]["allSousOrganisation"]);
			unset($searchParams["filters"]["form"]);
		}

		if(!empty($searchParams["filters"]["allSousOrganisation"])){
			$prms = $searchParams["filters"]["allSousOrganisation"];
			$prms[] = $searchParams["filters"]["form"];

			$query=array("form"=>['$in' => $prms]);
			unset($searchParams["filters"]["allSousOrganisation"]);
			unset($searchParams["filters"]["form"]);
		}


		if(!empty($searchParams["userId"]))
			$query = SearchNew::addQuery( $query , array("user" => $searchParams["userId"] ));

		if(!empty($searchParams["name"])){
			if(!empty($searchParams["textPath"]))
				$query = SearchNew::searchText($searchParams["name"], $query, array("textPath"=>$searchParams["textPath"]));
			else
				$query = SearchNew::searchText($searchParams["name"], $query);
		}
		if(!empty($searchParams["filters"]["address"])){
			$pathToAddress=(isset($mappingData["address"]["path"])) ? $mappingData["address"]["path"] : $mappingData["address"];
			$searchRegExp = SearchNew::accentToRegex(trim(urldecode($searchParams["filters"]['address'])));
			$query = SearchNew::addQuery($query ,
							array('$or'=> array(
										array($pathToAddress.".postalCode" => new MongoRegex("/.*{$searchRegExp}.*/i")),
										array($pathToAddress.".name" => new MongoRegex("/.*{$searchRegExp}.*/i"))
									)
							)
					);
			unset($searchParams["filters"]["address"]);
		}

		if(!empty($searchParams["filters"]["status"]) && is_array($searchParams["filters"]["status"])){
			if(in_array("unnotified",$searchParams["filters"]["status"])){
				$query = SearchNew::addQuery($query ,[
					'$and' => [
						array("status" => ['$ne' => "notified"]),
						array("status" => ['$ne' => "notificationSent"])
					]
				]);
				if(count($searchParams["filters"]["status"]) == 1){
					unset($searchParams["filters"]["status"]);
				}
			}elseif(in_array("unnotified",$searchParams["filters"]["status"])){

			}
		}

		if( !empty($searchParams["searchTags"]) &&
			( count($searchParams["searchTags"]) > 1  || count($searchParams["searchTags"]) == 1 && $searchParams["searchTags"][0] != "" ) ) {
			$operator=(!empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
			if(!empty($searchParams["tagsPath"])){
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query,$searchParams["tagsPath"]);
				unset($searchParams["tagsPath"]);
			}else
				$query = SearchNew::searchTags($searchParams["searchTags"], $operator, $query);
			unset($searchParams["searchTags"]);
		}

		if(!empty($searchParams['filters'])){
			$query = SearchNew::searchFilters($searchParams['filters'], $query);
		}

		if(!empty($searchParams['sortBy'])){
			if(Api::isAssociativeArray($searchParams['sortBy'])){
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			}else{
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		}else
			$searchParams['sortBy'] = array("updated"=> -1);

		if(!isset($searchParams['fields']))
			$searchParams['fields'] = ["_id"];



		$res= array();
		if(is_string($searchParams["fields"])) $searchParams["fields"] = [];
		if(!empty($searchParams["indexStep"]) && $searchParams["indexStep"] == 30){
			$res["results"] = PHDB::findAndSort($searchParams["searchType"][0],$query,$searchParams['sortBy'], 0,  $searchParams["fields"]);
			if(isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count( $searchParams["searchType"][0] ,$query);
		}else{
			$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($searchParams["searchType"][0],$query, $searchParams["fields"],$searchParams['sortBy'], $searchParams["indexStep"], $searchParams["indexMin"]);
			if(isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count( $searchParams["searchType"][0] ,$query);
		}

		if (isset($searchParams["distinct"])) {
			$res["results"] = PHDB::distinct( $searchParams["searchType"][0], $searchParams["distinct"],$query) ?? [];
			if(isset($searchParams["count"])) {
				$res["count"][$searchParams["searchType"][0]] = count($res["results"]);
			}
		}
		
		return $res;
	}
	public static function contributors($answer){
		$contributorCounter = 0;
		$collection = Form::ANSWER_COLLECTION;
		$collectionId = (is_array($answer["_id"]) && !empty($answer["_id"]['$id'])) ? $answer["_id"]['$id'] : (!empty($answer["_id"]) ? (string)$answer["_id"] : "");
		if (!empty($answer["answers"]) && isset($answer["project"]["id"])){
			$collection = Project::COLLECTION;
			$collectionId = $answer["project"]["id"];
			$project = PHDB::findOneById(Project::COLLECTION,$answer["project"]["id"],array("links"));
			if(!empty($project["links"]["contributors"])){
				$contributorCounter = "<span class='count-contributor'>".count(array_keys($project["links"]["contributors"]))."</span> ".Yii::t("common","contributor")."(s)";
			}
		}elseif(!empty($answer["links"]["contributors"])){
			$contributorCounter = "<span class='count-contributor'>".count(array_keys($answer["links"]["contributors"]))."</span> ".Yii::t("common","contributor")."(s)";
		}else{
			$contributorCounter = "<span class='count-contributor'>0</span> ".Yii::t("common","contributor")."(s)";
		}
			$res = array("data" => $contributorCounter,"html" => '<a href="javascript:;" class="view-contributors padding-left-10 padding-right-10" data-type="'.$collection.'" data-id="'.$collectionId.'"><i class="fa fa-users" style="color: #7da53d"></i> <b>'.$contributorCounter.'</b></a>');
		return $res;
	}
	public static function actions($answer){
		$answerId = (is_array($answer["_id"]) && !empty($answer["_id"]['$id'])) ? $answer["_id"]['$id'] : (!empty($answer["_id"]) ? (string)$answer["_id"] : "");
		$totalActions = PHDB::find(Action::COLLECTION,["answerId"=>$answerId]);
		$done = 0;
		//$closed = 0;
		foreach ($totalActions as $key => $value) {
			if(!empty($value["status"]) && $value["status"] == "done")
				$done++;

		}
		$res = array("done" =>$done,"total" =>count($totalActions), "html"=>count($totalActions) !=0 ? "<div class='padding-left-5 padding-right-5 text-center'>Actions<br><b>".$done."/".count($totalActions)."</b></div>":"");
		return $res;
	}
	public static function sendMail($params){
		if(is_array($params["emailValues"])){
			$idToMongoId = [];
			foreach($params["emailValues"] as $kAns => $vans){
				$idToMongoId[] = new MongoId($kAns);
			}
			$inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent"=>$params["formId"],"step" => "aapStep1"));
			$proposition = PHDB::find(Form::ANSWER_COLLECTION,array("_id"=>['$in'=>$idToMongoId]));
			$previewHtml = "";
			$count = 1;
			$html = $params["template"];
			$controller = $params["controller"];
			$emailValues = $params["emailValues"];

			if($params["useTemplate"]){
				$html = self::templateEmail($controller)[$params["template"]]["html"];
				$params['tplObject'] = self::templateEmail($controller)[$params["template"]]["object"];
				$params["noLogoHeader"] = true;
			}
			unset($params["template"]);unset($params["emailValues"]);unset($params["controller"]);
			$initImage = Document::getListDocumentsWhere(
				array(
					"id"=> $params["formId"],
					"type"=>'form',
					"subKey"=>'imageSignatureEmail',
				), "image"
			);
			$financors = self::getAllFinancorsByFormId($params["formId"]);
			foreach($proposition as $key => $value){
				$temp = $html;
				$value["signature"] = !empty($initImage["0"]["imagePath"]) ? Yii::app()->getRequest()->getBaseUrl(true).$initImage["0"]["imagePath"] :"empty" ;
				$params['tplMail'] = array_values($emailValues[$key]);
				if(!empty($params["preview"]) && $params["preview"]){
					$temp = self::parseVariable($temp,$value,$inputs,$financors,$params);
					$params['temp'] = $temp;
					$params['html'] = $params['temp'];
					//`.Yii::app()->getRequest()->getBaseUrl(true).`/costum/aap/attachedfile/answerid/`.$key.`
					$params['temp'] .= `<h1>ghkfdghjfgjhsd</h1>`;
					$params['propostion'] = $value;
					$previewHtml .= self::previewEmail($params,$count);
				}else{
					$random = rand().time();
					$params["notificationid"] = $random;
					$temp = self::parseVariable($temp,$value,$inputs,$financors,$params);
					$params["html"] = $temp;
					$params["answerId"] = $key;
					Mail::createAndSend($params);

					$params["status"] = [
						"sent" => true,
						"sentDate" => time()
					];
					PHDB::update(Form::ANSWER_COLLECTION,
						array("_id" => new MongoId($key)),
						array(
							'$set' => ["notifications.".$random => $params],
							'$addToSet' => ["status" => "notificationSent"]
						),
					);
				}
				$count++;
			}
			return $previewHtml;
		}
	}
	public static function previewEmail($params,$count=0){
		$html = "<style>
		.email-template-container{
			padding: 50px !important
		}
		.email-template-container p,.email-template-container span,.email-template-container td,.email-template-container li{
			font-size : 13px !important;
		}
		</style>";
		if($params["useTemplate"]){
			$html.= "
			<div class='preview-item'>
				<div class='count'>{$count}</div>
				<div class='preview-item-content'>
					<p>{$params['temp']}</p>
				</div>
			</div>
			";
		}else{
			$html.= "
			<div class='preview-item'>
				<div class='count'>{$count}</div>
				<h6>{$params['propostion']['answers']['aapStep1']['titre']}</h6>
				<div class='preview-item-content'>
					<p>Objet : {$params['tplObject']}</p>
					<p>{$params['temp']}</p>
				</div>
			</div>
			";
		}
		return $html;
	}
	public static function parseVariable($str,$answer,$inputs,$financors=null,$params=null){
		$matches = [];
		preg_match_all('/{(.*?)}/', $str, $matches);
		$data = self::budgetStatus2($answer,null,$inputs)["data"];
		$depenseFields = [];
		if(!empty($inputs["inputs"])){
			foreach ($inputs["inputs"] as $ki => $vi) {
				if(!empty($vi["type"]) && $vi["type"] == "tpls.forms.ocecoform.budget"){
					$depenseFields[] = $ki;
				}
			}
		}
		foreach($matches[1] as $km => $vm){
			foreach ($depenseFields  as $deFildK => $deFildV) {
				/*switch ($vm) {
					case "$deFildV.total":
						$str = str_replace("{".$vm."}", self::get_value(explode(".",$vm),$data)." €", $str);
					break;
					case "$deFildV.funded":
						$str = str_replace("{".$vm."}", self::get_value(explode(".","$deFildV.funded"),$data)." €", $str);
					break;
					case "$deFildV.spent":
						$str = str_replace("{".$vm."}", self::get_value(explode(".","$deFildV.spent"),$data)." €", $str);
					break;
					case "$deFildV.subvention":
						$str = str_replace("{".$vm."}", self::get_value(explode(".","$deFildV.subvention"),$data)." €", $str);
					break;
					case "$deFildV.subvention-funded":
						$str = str_replace("{".$vm."}", self::get_value(explode(".","$deFildV.subvention-funded"),$data)." €", $str);
					break;
					default:
						echo " ";
				}*/
			}
			switch ($vm) {
				case "prop.title":
					$str = str_replace("{".$vm."}", self::get_value(["answers","aapStep1","titre"],$answer), $str);
				break;
				case "prop.association":
					$str = str_replace("{".$vm."}", self::get_value(["answers","aapStep1","association"],$answer), $str);
				break;
				case "prop.table.subvention":
					$str = str_replace("{".$vm."}", self::getSubvention($answer,$financors)["table"], $str);
				break;
				case "prop.subvention":
					$str = str_replace("{".$vm."}", self::getSubvention($answer,$financors)["total"], $str);
				break;
				case "prop.today":
					$str = str_replace("{".$vm."}", date("d/m/Y") , $str);
				break;
				case "prop.id":
					$str = str_replace("{".$vm."}",(string)$answer['_id'], $str);
				break;
				case "prop.html":
					$str = str_replace("{".$vm."}", htmlspecialchars("<h1>TRALALALALALALA</h1>") , $str);
				break;
				case "prop.signature":
					$str = str_replace("{".$vm."}", $answer["signature"] , $str);
				break;
				default:
					echo " ";
			}
			if($params["preview"]== false && $vm == "prop.fakeImg"){
				$img =  Yii::app()->getRequest()->getBaseUrl(true)."/costum/aap/addnotified/answerid/".(string)$answer['_id']."/notificationid/".$params["notificationid"] ;
				$str = str_replace("{".$vm."}", $img , $str);
			}
		}
		return $str;
	}

	public static function budgetStatus2($answer,$element=null,$inputs=null){
		$depenseFields = [];
		if(!empty($inputs["inputs"])){
			foreach ($inputs["inputs"] as $ki => $vi) {
				if(!empty($vi["type"]) && $vi["type"] == "tpls.forms.ocecoform.budget"){
					$depenseFields[] = $ki;
				}
			}
		}
		$res = array();
		$view = "";
		foreach ($depenseFields  as $deFildK => $deFildV) {
			$res[$deFildV] = [
				"total" => 0,
				"funded" => 0,
				"spent" => 0,
				"subvention" => 0,
				"subvention-funded" => 0,
			];
			$colorprice = "black";
			if (!empty($answer["answers"]["aapStep1"][$deFildV])){
				foreach ($answer["answers"]["aapStep1"][$deFildV] as $depId => $dep){
					if (!empty($dep["poste"]) && !empty($dep["price"]) && strpos(strtolower($dep["poste"]), strtolower("subvention")) !== false){
						$res[$deFildV]["subvention"] += (float)$dep["price"];
						if(!empty($dep["financer"])){
							foreach ($dep["financer"] as $kfin => $vfin) {
								if(!empty($vfin["amount"])){
									$res[$deFildV]["subvention-funded"]+= (float)$vfin["amount"];
								}
							}
						}
						$view .="<div class='padding-left-10 padding-right-10 text-center' >";
							$view .= "Subvention <br> <span class='bold' style='color: #000'>" .number_format($res[$deFildV]["subvention-funded"],0, ',', ' ')."<i class='fa fa-euro'></i> / ".number_format($res[$deFildV]["subvention"],0, ',', ' ')."<i class='fa fa-euro'></i></span>";
						$view .="</div >";
					}else if (!empty($dep["price"])){
						$res[$deFildV]["total"] += (float) $dep["price"];

						$totalfi = 0;
						if (!empty($dep["financer"]))
						{
							foreach ($dep["financer"] as $fiid => $fi)
							{
								if (isset($fi["amount"]))
								{
									$totalfi += (int)$fi["amount"];
								}
							}

						}
						$res[$deFildV]["funded"] = $totalfi;
						$totalPayed = 0;
						if (!empty($dep["payement"]))
						{
							foreach ($dep["payement"] as $payId => $payV)
							{
								if (isset($payV["amount"]))
								{
									$totalPayed += (int)$payV["amount"];
								}
							}

						}
						$res[$deFildV]["spent"] += $totalPayed;

					}
				}

				if ($res[$deFildV]["total"] <= $res[$deFildV]["funded"]){
					$colorprice = "green";
				} elseif ($res[$deFildV]["funded"] > 0){
					$colorprice = "orange";
				}
				if($deFildV != "depense"){
					$view .="<div class='padding-left-10 padding-right-10 text-center' >";
						$view .= $inputs["inputs"][$deFildV]["label"]. " <br> <span class='bold' style='color: #000'>" .number_format($res[$deFildV]["total"],0, ',', ' ')."<i class='fa fa-euro'></i></span>";
					$view .="</div >";
				}else{
					$view .="<div class='padding-left-10 padding-right-10 text-center' >";
						$view .= $inputs["inputs"][$deFildV]["label"]. " <br> <span class='bold' style='color: #000'>" .number_format($res[$deFildV]["funded"],0, ',', ' ')."<i class='fa fa-euro'></i> / ".number_format($res[$deFildV]["total"],0, ',', ' ')."<i class='fa fa-euro'></i> (" .number_format($res[$deFildV]["spent"],0, ',', ' ')."<i class='fa fa-euro'></i> depensé)</span>";
					$view .="</div >";
				}

			}
		}
		return array("data" => $res,"html" => $view);
	}

	public static function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public static function get_value($indexes, $arrayToAccess){
		if(count($indexes) > 1)
			return self::get_value(array_slice($indexes, 1), $arrayToAccess[$indexes[0]]);
		else
			return !empty($arrayToAccess[$indexes[0]]) ? $arrayToAccess[$indexes[0]] : "";
	}
	public static function getSubvention($answer,$financors){
		$total = 0;
		$table ="";
		$table .= '<table border="1" cellpadding="2" style="border-color : grey;width: 100%;font-size:12px;"> <tbody>';
		if(!empty($answer["answers"]["aapStep1"]["depense"])){
			foreach ($answer["answers"]["aapStep1"]["depense"] as $kdep => $vdep) {
				if(!empty($vdep["poste"])){
					if(strpos(strtolower($vdep["poste"]), strtolower("Subvention AAP Politique de la Ville")) !== false){
						$table.="<tr style='padding-top:5px'>
						";
						if(!empty($vdep["financer"])){
							foreach ($vdep["financer"] as $kfin => $vfin) {
								if(!empty($vfin["name"]) && !empty($financors[$vfin['id']])){
									$table.= "<td style='font-size:8px;padding-top:5px'><small>".$financors[$vfin['id']]."</small></td>";
								}else
									$table.= "<td style='font-size:8px;padding-top:5px'><small>&nbsp;</small></td>";
							}
							$table.= "<td style='font-size:8px;padding-top:5px'><small>TOTAL</small></td>";
						}else{
							$table.= "<td style='font-size:8px;padding-top:5px'><small>&nbsp;</small></td>";
						}
						$table.= "</tr>";

						$table.="<tr>
						";
						if(!empty($vdep["financer"])){
							foreach ($vdep["financer"] as $kfin => $vfin) {
								if(!empty($vfin["amount"])){
									$total += (float) $vfin['amount'];
									$table.= "<td style='font-size:8px;padding-top:5px'><small>".(float) $vfin['amount']." €</small></td>";
								}else
									$table.= "<td style='font-size:8px;padding-top:5px'><small>0 €</small></td>";
							}
							$table.= "<td style='font-size:8px;padding-top:5px'><small>".$total." €</small></td>";
						}else{
							$table.= "<td style='font-size:8px;padding-top:5px'><small>&nbsp;</small></td>";
						}

						$table.= "</tr>";
					}
				}
			}
		}
		$table .= '</tbody></table>';
		$res = array("total" => $total." €","table" => $table);
		return $res;
	}
	public static function templateEmail($controller,$params=array()){
		$res = array(
			"template-1-coSindniSmarterre" => [
				"object" => "Notification d’attribution de subvention dans le cadre de l’Appel à Projet Contrat de Ville    2022",
				"html" => $controller->renderPartial("costum.views.custom.appelAProjet.callForProjects.templateEmail.template-1-coSindniSmarterre", $params)
			],
			"template-2-coSindniSmarterre" => [
				"object" => "Notification d’attribution de subvention dans le cadre de l’Appel à Projet Contrat de Ville    2022",
				"html" => $controller->renderPartial("costum.views.custom.appelAProjet.callForProjects.templateEmail.template-2-coSindniSmarterre", $params)
			]
		);
		return $res;
	}
	public static function getAllFinancorsByFormId($formId){
		$parentForm =  PHDB::findOneById(Form::COLLECTION,$formId);
        $paramsData = ["financerTypeList" => Ctenat::$financerTypeList, "limitRoles" => ["Financeur"], "openFinancing" => true];
        if (isset($parentForm["type"]) && ($parentForm["type"] == "aapConfig" || $parentForm["type"] == "aap")){
            foreach ($parentForm["parent"] as $parId => $parValue)
            {
                $contextId = $parId;
                $contextType = $parValue["type"];
            }
        }

        $contextIdType = $parentForm["parent"];
        $communityLinks = Element::getCommunityByParentTypeAndId($contextIdType);
        if (isset($parentForm["params"]["financerLimitRoles"])) $paramsData["limitRoles"] = $parentForm["params"]["financerLimitRoles"];
        $organizations = Link::groupFindByType(Organization::COLLECTION, $communityLinks, ["name", "links", "type"]);

        $citoyens = Link::groupFindByType(Person::COLLECTION, $communityLinks, ["name", "links", "type"]);

        $projects = Link::groupFindByType(Project::COLLECTION, $communityLinks, ["name", "links", "type"]);

        $orgs = [];
        foreach ($organizations as $id => $or){
            $roles = null;

            if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

            if ($paramsData["limitRoles"] && !empty($roles))
            {
                foreach ($roles as $i => $r)
                {
                    if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
                }
            }
        }

        foreach ($citoyens as $id => $or){
            $roles = null;

            if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

            if ($paramsData["limitRoles"] && !empty($roles))
            {
                foreach ($roles as $i => $r)
                {
                    if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
                }
            }
        }

        foreach ($projects as $id => $or){
            $roles = null;

            if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

            if ($paramsData["limitRoles"] && !empty($roles))
            {
                foreach ($roles as $i => $r)
                {
                    if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
                }
            }
        }
		return $orgs;
	}
	public static function cacs($cacs,$answer){
		$cac = $cacs[$answer["form"]];
		$cacId = array_keys($cac["parent"])[0];
		$cacValue = $cac["parent"][$cacId];
		return array("cacId"=>$cacId , "cacValue" => $cacValue);
	}
	public static function heartVote($answer,$el,$elform,$me=null){
		$activeFor = false;
		$allowRestricted = false;
		$alert = "";
		$html = "";
		if(!empty($elform["params"]["voteConfig"])){
			$configs =  $elform["params"]["voteConfig"];
			if(!empty($configs["activeFor"])){
				if($configs["activeFor"] == "date"){
					if(!empty($configs["startDate"]) && !empty($configs["endDate"]) && $configs["startDate"]->sec <= strtotime("today") && $configs["endDate"]->sec>= strtotime("today")){
						$activeFor = true;
					}else{
						$alert = "Veuller voter entre le ".date('d/m/Y', $configs["startDate"]->sec)." et ".date('d/m/Y', $configs["endDate"]->sec);
					}
				}elseif($configs["activeFor"] == "status"){
					if(!empty($answer["status"]) && in_array("vote",$answer["status"])){
						$activeFor = true;
					}else{
						$alert = "Veullez voter les propositions ayant le status <i><b>En evaluation</b></i>";
					}
				}
			}

			if($configs["restricted"] == "members"){
				if(!empty(Yii::app()->session['userId']) && Authorisation::isElementMember((string)$el["_id"], @$el["collection"], Yii::app()->session['userId'])){
					$allowRestricted = true;
				}else{
					$alert = Yii::t("survey","Members only");
				}
			}elseif($configs["restricted"] == "roles"){
				if( !empty(Yii::app()->session['userId']) &&
					Authorisation::isElementMember((string)$el["_id"], @$el["collection"], Yii::app()->session['userId']) &&
					!empty($configs["roles"]) && !empty($me["links"]["memberOf"][(string)$el["_id"]]["roles"]) &&
					!empty(
						array_intersect(explode(",",$configs["roles"]), $me["links"]["memberOf"][(string)$el["_id"]]["roles"])
					)){
						$allowRestricted = true;
				}else{
					$alert = Yii::t("survey","Members only").", ayant le role ".$configs["roles"]." sont autorisées" ;
				}
			}elseif($configs["restricted"] == "all"){
				$allowRestricted = true;
			}
		}else{
			$activeFor = true;
			$allowRestricted = true;
		}

		$vote = 0;
		$myRate = false;
		$countLike = 0;
		if (isset($answer["vote"]))
		{
			foreach ($answer["vote"] as $idPublic => $valuePublic)
			{
				if ($idPublic == Yii::app()->session['userId']) $myRate = true;
				$countLike ++;
			}
		}
		if($activeFor && $allowRestricted){
			$html .='
				<div class="btn-group btn-group-xs pull-right like-project-container">
					<a href="javascript:;" class="like-project letter-red tooltips margin-right-10" data-value="'.(($myRate == "true" || $myRate == true) ? "true" : "false")
					.'" data-ans="'.(string) $answer["_id"] .'" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J&#039;adore">
						<i class="fa '.(($myRate == "true" || $myRate == true) ? "fa-heart" : "fa-heart-o") .'"></i>
						<small class="counter" style="font-size: 12px">'.$countLike .'</small>
					</a>
					<a href="javascript:;" class="pull-right votant-modal-heart"  data-answerid="'.(string) $answer["_id"] .'"></a>
				</div>';
		}else{
			$html .='<a href="javascript:toastr.error(escapeHtml(\''.$alert.'\'));" class="like-project letter-red tooltips pull-right margin-right-10"  data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J&#039;adore">
					<i class="fa fa-heart-o"></i>
					<small class="counter" style="font-size: 12px">'.$countLike .'</small>
				</a>';
		}
			return $html;
	}

	public static function listItemCoFormButtons($p_active =false, $adminRight = false, $canEditEachotherAnswer = false, $canReadEachOtherAnswer = false, $answerId = '', $useranswer = false) {
		$socialBtn = '';
		if (isset(Yii::app()->session["userId"]) && ($p_active || $adminRight)) {
			$btnclass = "";
			$btndataw = "";
			$btndatar = "";
			$btnpdfclass = "";
			$btndeleteclass = "";
			$btndatatype = "";

			$btnclass = "getanswer";
			$btndataw = "data-mode='w' data-ansid='" . $answerId . "'";
			$btndatar = "data-mode='r' data-ansid='" . $answerId . "'";
			$btnpdfclass = "exportanswer";
			$btndeleteclass = "deleteanswer";
			$socialBtn = '
			  <div class="col-md-12">
				<div class="social-links justify-content-center">';
				  if ($adminRight || $canEditEachotherAnswer || (!$canEditEachotherAnswer && $useranswer)) {
					  $socialBtn .= '<div class="social-btn flex-center '. $btnclass .'" '.$btndataw . $btndatatype .'>
						<i class="fa fa-pencil-square-o editdeleteicon"></i>
						<span style="font-size: 14px;">'. Yii::t('common', 'Edit') .'</span>
					  </div>';
					}

					if ($adminRight || $canReadEachOtherAnswer || (!$canReadEachOtherAnswer && $useranswer)) {
					  $socialBtn .= '<div class="social-btn flex-center '.$btnclass.'" '.$btndatar.'>
						<i class="fa fa-sticky-note-o editdeleteicon"></i><span style="font-size: 14px;">'. Yii::t('common', 'Read') .'</span>
					  </div>';

					  if ($adminRight) {
						$socialBtn .= '<div class="social-btn flex-center '. $btndeleteclass .'" '. $btndataw .'>
						  <i class="fa fa-trash-o editdeleteicon" style="color: #ff5722"></i><span style="font-size: 14px;">'. Yii::t('common', 'Delete') .'</span>
						</div>';
					  }

					//   if ($opalContextId != null) {
					// 	$socialBtn .= '<div class="social-btn flex-center getansweropal " data-ansid="'. $answerId .'">
					// 	  <i class="fa fa-area-chart editdeleteicon"></i><span style="font-size: 14px;">'. Yii::t('common', 'Dashboard') .'</span>
					// 	</div>';
					//   }
					}
				$socialBtn .= '
				  </div>
				</div>';
		  }
		  return $socialBtn;
	}
	public static function getImageItem($answer,$allImages){
		$initAnswerFiles = [];
		foreach ($allImages as $kimg => $vimg) {
			if(!empty($vimg["id"]) && $vimg["id"] == (string) $answer["_id"]){
				$initAnswerFiles[$kimg] = $vimg;
			}
		}

		if(!empty($initAnswerFiles)){
			foreach ($initAnswerFiles as $keyImg => $valueImg) {
				$imgAnsw = $valueImg["docPath"];break;
			}
		}else
			$imgAnsw = Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl() . "/images/thumbnail-default.jpg";
	}
	public static function prepareData($allAnswers,$types,$el,$el_form_id,$el_configform_id){
		$allAnswersParsed = array_map(function($val){
			return new MongoId($val);
		},$allAnswers);
		$allAnsTemp = PHDB::find($types,array("_id"=>['$in' => $allAnswersParsed]));
		$allAnswers = [];
		foreach ($allAnswersParsed as $key => $value) {
			$allAnswers[(string)$value] = $allAnsTemp[(string)$value];
		}
		unset($allAnswersParsed);unset($allAnsTemp);

		$users = [
			"ids" => [],
			"data" => [],
		];
		$cacs = [
			"idForm" => [],
			"data" => []
		];
		$checkSeen = [
			"ansId" => [],
			"data" => []
		];
		$allActions = [
			"ansId" => [],
			"projectId" => [],
			"projectId2" => [],
			"data" => []
		];
		$allImages = [
			"ansId" => [],
			"data" => []
		];
		if(!empty($allAnswers)){
			foreach($allAnswers as $k => $v){
				if(!empty($v["user"])){
					$users["ids"][] = new MongoId($v["user"]);
				}
				if(!empty($v["form"])){
					$cacs["idForm"][] = new MongoId($v["form"]);
				}
				if(!empty($v["project"]["id"])){
					$allActions["projectId"][] = $v["project"]["id"];
					$allActions["projectId2"][] = new MongoId($v["project"]["id"]);
				}
				$allActions["ansId"][] = $k;

				$allImages["ansId"][] = $k;
				$checkSeen["ansId"][] = $k;
			}
		}
		$users["data"] = PHDB::find(Person::COLLECTION,array("_id"=>['$in'=>$users["ids"]]),array("name"));
		$cacs["data"] = PHDB::find(Form::COLLECTION,array("_id" => ['$in'=>$cacs["idForm"]]),array("parent"));
		$checkSeen["data"] = PHDB::find("views",array("parentId" => ['$in'=>$checkSeen["ansId"]],"userId"=>Yii::app()->session['userId']));
		$allActions["data"] = PHDB::find(Action::COLLECTION, [
			'$or'=>[
				array("parentId" => ['$in'=>$allActions["projectId"]]),
				array("parentId" =>['$in'=>$allActions["projectId2"]]),
				array("answerId" =>['$in'=>$allActions["ansId"]])
			],
			"parentType" => Project::COLLECTION,
		]);
		$allImages["data"] = Document::getListDocumentsWhere(array(
			"id"=> ['$in'=>$allImages["ansId"]],
			"type"=> Form::ANSWER_COLLECTION,
			"subKey"=>"aapStep1.image"), "file");

		$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
		$elform = PHDB::findOneById(Form::COLLECTION,$el_form_id);
		//$voteType = "starCriterionBased"; if(!empty($elform["params"]["configSelectionCriteria"]["type"])) $voteType = $elform["params"]["configSelectionCriteria"]["type"];
		//$noteMax = !empty($elform["params"]["configSelectionCriteria"]["noteMax"]) ? $elform["params"]["configSelectionCriteria"]["noteMax"]:10;

		$costum = CacheHelper::getCostum();
		$configform = PHDB::findOneById(Form::COLLECTION,$el_configform_id);
		$el = PHDB::findOneById($el["type"],$el["id"]);

		$canSeeDetail = false;
		$canEvaluate = false;
		$isActiveSousOrga = isset($el["oceco"]["subOrganization"]) ? true : false;
		if(isset(Yii::app()->session["userId"]) && isset($el["links"]["members"][Yii::app()->session["userId"]])){
			$canSeeDetail = true;
			if(!empty($el["links"]["members"][Yii::app()->session["userId"]]["roles"]) && !empty($elform["params"]["aapStep2"]["canEdit"])){
				$myRoles = $el["links"]["members"][Yii::app()->session["userId"]]["roles"];
				$evaluatorsRoles = explode(",",$elform["params"]["aapStep2"]["canEdit"]) ;
				if(count(array_intersect($myRoles,$evaluatorsRoles)))
					$canEvaluate = true;
			}
			if(Form::canAdmin((string)$elform["_id"]))
				$canEvaluate = true;
		}

		$starbg = Yii::app()->getModule('costum')->assetsUrl . "/images/blockCmsImg/green-star.png";
		$canAdmin = false;
		if((isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) || Form::canAdmin((string)$elform["_id"]))
			$canAdmin = true;

		$stepEval = "aapStep2";
		$stepFinancor = "aapStep3";
		if (isset($configform["subForms"]["aapStep2"]["params"]["config"]["criterions"]))
		{
			$stepEval = "aapStep2";
			$stepFinancor = "aapStep3";
			$stepAction = "aapStep4";
		}
		elseif (isset($configform["subForms"]["aapStep3"]["params"]["config"]["criterions"]))
		{
			$stepEval = "aapStep3";
			$stepFinancor = "aapStep4";
			$stepAction = "aapStep5";
		} else {
			$stepAction = "";
		}

		$inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent"=>$el_form_id,"step" => "aapStep1"));
		if(!empty($inputs["inputs"])){
			foreach ($inputs["inputs"] as $ki => $vi) {
				if(!empty($vi["type"]) && $vi["type"]=="tpls.forms.titleSeparator"){
					unset($inputs["inputs"][$ki]);
				}
			}
		}

		return array(
			"users" => $users["data"],
			"cacs" => $cacs["data"],
			"checkSeen" => $checkSeen["data"],
			"allActions" => $allActions["data"],
			"allImages" => $allImages["data"],
			"allAnswers" => $allAnswers,
			"elform" => $elform,
			"configform" => $configform,
			"el" => $el,

			"me" => $me,
			"stepEval" => $stepEval,
			"stepFinancor" => $stepFinancor,
			"stepAction" => $stepAction,
			"canAdmin" => $canAdmin,
			"inputs" => $inputs,
			"starbg" => $starbg,

			"isActiveSousOrga" => $isActiveSousOrga,
			"canSeeDetail" => $canSeeDetail,
			"canEvaluate" => $canEvaluate
		);
	}

	public static function answerVariables($answer,$data,$stepEval){
		$completedFields = 0;
		if(!empty($answer["answers"]["aapStep1"])){
			foreach ($answer["answers"]["aapStep1"] as $kAnsInput => $vAnsInput) {
				if(!empty($vAnsInput)) $completedFields++;
			}
		}

		$percentageTasks = "--";
		$dataPer = 0;
		$iconTasks = "";
		$actions = [];
		$currentDate = new DateTime();
		$currentDate->format("d/m/Y");
		$currentTimestamp = $currentDate->getTimestamp();
		$cur = 0;
		$nbFinishedTask = 0;
		$nbNotFinishedTask = 0;
		$haveDepense = false;
		$nbFinancedDepense = 0;
		$seen = false;
		$haveProject = array(
			"is" => false,
			"html" => ''
		) ;
		if (!empty(Yii::app()->session['userId'])){
			foreach($data["checkSeen"] as $kcs => $vcs){
				if(!empty($vcs["parentId"]) && $vcs["parentId"] == (string)$answer["_id"]){
					$seen = true ;
				}
			}
		}

		if (empty(Yii::app()->session['userId']) || $answer["user"] == Yii::app()->session['userId'] ){
			$seen = true ;
		}

		if (!empty($answer["answers"]["aapStep1"]["depense"])){
			foreach ($answer["answers"]["aapStep1"]["depense"] as $key => $value) {
				if(!empty($value["financer"]))
					$nbFinancedDepense++;
			}

			$haveDepense = true;
		}


		$statusAction = "<span class='label label-default'>Aucun tache</span>";
		$urgentState = !empty($answer['answers']['aapStep1']["urgency"]) ? "Urgent" : "";

		$maxDate = "Inconnu";
		if (isset($answer["project"]["id"]))
		{
			if(isset($answer["status"]) && !in_array("projectstate",$answer["status"]))
				$answer["status"][] = "projectstate";
			elseif(!isset($answer["status"])){
				$answer["status"] = [];
				//$answer["status"][] = "projectstate";
			}

			$actions = [];
			foreach($data["allActions"] as $kact => $vact){
				if(
					(!empty((string) $vact["parentId"]) && $vact["parentId"] == (string) $answer["project"]["id"]) ||
					(!empty($vact["answerId"]) && (string) $answer["_id"] == (string) $vact["answerId"] )
				){
					$actions[$kact] = $vact;
				}
			}
			$tasks = array_reduce($actions, function ($carry, $item)
			{
				if (!empty($item["tasks"]))
				{
					$carry = array_merge($carry, $item["tasks"]);
				}
				return $carry;
			}
				, []);
			$percent = count(array_filter($tasks, function ($ele)
				{
					return !empty($ele["checked"]) && ($ele["checked"] == "true");
				})) / count($tasks) * 100;
			$percent = is_nan($percent) ? 0 : $percent;
			$percentageTasks = round($percent) . "%";
			$dataPer = round($percent, -1);
			if ($percent == 100)
			{
				$iconTasks = "fa-check";
			}
			else
			{
				$iconTasks = "fa-times";
			}

			//get max date in task and status
			$countTask = count($tasks);
			foreach ($tasks as $kaction => $vaction)
			{
				if (isset($vaction["checked"]) && ($vaction["checked"] === true || $vaction["checked"] === "true"))
					$nbFinishedTask++;
				else
					$nbNotFinishedTask++;
				$df = null;
				if (is_string(@$vaction["endDate"]))
					if (DateTime::createFromFormat("d/m/Y", @$vaction["endDate"]))
						$df = DateTime::createFromFormat("d/m/Y", @$vaction["endDate"])->getTimestamp();
				if ($df > $cur)
					$cur = $df;
				else
					$cur = 0;

			}

			if ($nbNotFinishedTask != 0 && $cur < $currentTimestamp) $statusAction = "<span class='label label-danger'>Date dépassé</span>";
			/*elseif ($nbFinishedTask == $countTask) $statusAction = "<span class='label label-success'>Terminé</span>";
			elseif ($countTask != 0 && $cur >= $currentTimestamp) $statusAction = "<span class='label label-info'>En cours</span>";*/

			//end task story
				$maxDate = $cur != 0 ? date("d/m/Y", $cur) : "Inconnu";

			// check if finished date change
			if (isset($answer["projectEndDateStory"]) && is_array($answer["projectEndDateStory"]))
			{
				if (!in_array($maxDate, $answer["projectEndDateStory"]))
				{
					$answer["projectEndDateStory"][] = $maxDate;
					PHDB::update(Form::ANSWER_COLLECTION, array(
						"_id" => new MongoId($answer["_id"])
					) , array(
						'$set' => ["projectEndDateStory" => $answer["projectEndDateStory"]]
					));
				}
			}
			$haveProject["is"] = true;
			/* $haveProject["html"] = '<a href="#page.type.projects.id.'.$answer["project"]["id"].'" class="project-bulb text-green lbh-preview-element tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Aller à la page du projet"  style="top: 0; margin-right: 6%;'.(count($actions) == 0 ? "right:5px;" : "right:5px;").'">
										<i class="fa fa-2x fa-lightbulb-o"></i>
									</a>'; */
			$haveProject["html"] = '<a href="#page.type.projects.id.'.$answer["project"]["id"].'" class="project-bulb text-green lbh-preview-element tooltips hidden" data-toggle="tooltip" data-placement="bottom" data-original-title="Aller à la page du projet"  style="position: relative; top: 0">
				<i class="fa fa-2x fa-lightbulb-o"></i>
			</a>';
		}

		//get admin and evaluator rating
		$cummulMean = !empty($answer["answers"][$stepEval]["allVotes"]) ? $answer["answers"][$stepEval]["allVotes"] : 0;
		if($cummulMean > 5){
			$cummulMean = 5;/*(5*$cummulMean)/$noteMax;*/
		}

		$votant = [];
		if(!empty($answer["answers"][$stepEval]["selection"]) && is_array($answer["answers"][$stepEval]["selection"]))
			$votant = array_merge($votant,$answer["answers"][$stepEval]["selection"]);
		if(!empty($answer["answers"][$stepEval]["admissibility"]) && is_array($answer["answers"][$stepEval]["admissibility"]))
			$votant = array_merge($votant,$answer["answers"][$stepEval]["admissibility"]);
		$votantCounter = count($votant);

		//name
		$name = !empty(@$answer["answers"]["aapStep1"]["titre"]) ? @$answer["answers"]["aapStep1"]["titre"] : "<span class='text-red'>".Yii::t("common","No title")."</span>";
		if(!empty($answer["answers"]["aapStep1"]["year"]))
			$name.= " <i>(".$answer["answers"]["aapStep1"]["year"].")</i>";
		// description
		$description = @$answer["answers"]["aapStep1"]["description"];
		$tags = @$answer["answers"]["aapStep1"]["tags"];

		//img profil
		$imgAnsw = (isset($answer["profilMediumImageUrl"])) ? Yii::app()->createUrl($answer["profilMediumImageUrl"]) : Yii::app()->getModule(Yii::app()
				 ->params["module"]["parent"])
				 ->getAssetsUrl() . "/images/thumbnail-default.jpg";
		if(!isset($answer["profilMediumImageUrl"])){
			$initAnswerFiles = [];
			foreach ($data["allImages"] as $kimg => $vimg) {
				if(!empty($vimg["id"]) && $vimg["id"] ==  (string) $answer["_id"]){
					$initAnswerFiles[$kimg] = $vimg;
				}
			}

			if(!empty($initAnswerFiles)){
				foreach ($initAnswerFiles as $keyImg => $valueImg) {
					$imgAnsw = $valueImg["docPath"];break;
				}
			}else
				$imgAnsw = Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl() . "/images/thumbnail-default.jpg";
		}

		$retain = ["label" => "En attente", "class" => "btn-primary", "retain" => false, "icon" => "hand-stop-o"];
		if(isset($answer["acceptation"]) && $answer["acceptation"] == "retained"){
			$retain["label"] = "Retenu";
			$retain["class"] = "btn-success";
			$retain["retain"] = true;
			$retain["icon"] = "hand-rock-o";

		}elseif(isset($answer["acceptation"]) && $answer["acceptation"] == "rejected"){
			$retain["label"] = "Non retenu";
			$retain["class"] = "btn-success";
			$retain["retain"] = false;
			$retain["icon"] = "thumbs-down";
		}

		return array(
			"name" => $name,
			"description" => $description,
			"completedFields" => $completedFields,
			"percentageTasks" => $percentageTasks,
			"dataPer" => $dataPer,
			"iconTasks" => $iconTasks,
			"haveDepense" => $haveDepense,
			"nbFinancedDepense" => $nbFinancedDepense,
			"seen" => $seen,
			"haveProject" => $haveProject,
			"countTask" => @$countTask,
			"actions" => $actions,
			"imgAnsw" => $imgAnsw,
			"retain" => $retain,
			"urgentState" => $urgentState,
			"cummulMean" => $cummulMean,
			"votantCounter" => $votantCounter
		);
	}

    public static function answerCoremuVariables($answer) {
        $coremu = false;
        $needCandidate = false;

        $candidateNeed = 0;
        $coremuLDD = array();
        $totalprice = 0;
        $totalvalid = 0;
	    if(!empty($answer["answers"]["aapStep1"]["depense"])){
            $coremu = true;

            foreach($answer["answers"]["aapStep1"]["depense"] as $depId => $dep){
                $candidatNumber = 0;

                if(!empty($dep["price"])){
                    $totalprice += $dep["price"];
                }

                if(!empty($dep["estimates"])){
                    $needCandidate = true;
                    foreach($dep["estimates"] as $estimatekey => $estimate){
                        if (MongoId::isValid($estimatekey) && (empty($estimate["deny"]) || $estimate["deny"] == false)){
                            $needCandidate = false;
                            $candidatNumber++;
                            if (!empty($estimate["validate"]) && $estimate["validate"] == "validated") {
                                if (isset($dep["switchAmountAttr"]) && $dep["switchAmountAttr"] == "assignBudget" && !empty($estimate["assignBudget"])) {
                                    $totalvalid += $estimate["assignBudget"];
                                }else{
                                    $candidatperc = 100 / intval(Aap::countAvalaiblecandidate($dep));
                                    if (!empty($estimate["percentage"])) {
                                        $candidatperc = $estimate["percentage"];
                                    }
                                    $totalvalid += round(($dep["price"] * $candidatperc) / 100,2);
                                }
                            }
                        }
                    }
                    if ( !empty($dep["candidateNumber"]) && intval($dep["candidateNumber"]) < $candidatNumber){
                        $needCandidate = true;
                    }
                }else{
                    $needCandidate = true;
                }

                $dep["candidateTotal"] = $candidatNumber;
                $coremuLDD[] = $dep;
            }
        }

	    return array(
	        "totalprice" => $totalprice,
            "totalvalid" => $totalvalid,
	        "coremu" => $coremu,
            "needCandidate" => $needCandidate,
            //"candidatNumber" => $candidatNumber,
            "coremuLDD" => $coremuLDD
            //"candidateNeed" => $candidateNeed
        );
    }

    public static function answerCoremuTableVariables($answers, $onefield) {
        $tableAnswers = array();
        $tableSankey = array();
        $tablepercentage = array();
        $tableprice = array();
        $tablestatus = array();
        $tablecandidatename = array();
        $tablecandidatestatus = array();
        $propopiedata = array();
        $propopielabels = array();
        $propobarlabel = array("validé" , "non validé" , "pas mentioné");
        $tableSankeyLinks = array();
        $tableSankeyNodes = array();
        $notificationbar = array();
        $realpropositiondata = array();
        $propobardata = array();
        $totalcandidattovalid = 0;
        $totalpricetovalid = 0;
        $totalneedcandidat = 0;
        $totalansweraction = 0;
	    foreach ($answers as $idanswer => $answer){
            if (isset($answer['answers']["aapStep1"]["titre"]) && isset($answer['answers']["aapStep1"]["depense"])){
                foreach ($answer['answers']["aapStep1"]["depense"] as $iddepense => $depense){
                    $candidatebtn = $depense["poste"];
                    if(empty($depense["price"])){
                        $depense["price"] = 0;
                    }
                    if(empty($depense["estimates"][Yii::app()->session["userId"]])){
                        $candidatebtn = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtninfo dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            ".$depense['poste']."
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='btn btn-sm candidatebtn' type='button' data-pos='".$iddepense."' data-id='" . $idanswer . "' data-action='validated'> candidater</a></li>
                                          </ul>
                                        </div>";
                    }
                    $haveghostcandidate = false;
                    if (!empty($depense["estimates"])){
                        $haveghostcandidate = true;
                        foreach ($depense["estimates"] as $bg_id => $bdg){
                            if (MongoId::isValid($bg_id) && (empty($bdg["deny"]) || $bdg["deny"] == false)){
                                $haveghostcandidate = false;
                            }
                        }
                    }

                    if (!empty($depense["estimates"]) && !$haveghostcandidate){
                        $badge = [];
                        if (isset($depense["badge"])){
                            foreach ($depense["badge"] as $bdg){
                                $badgeEl = PHDB::findOneById(Badge::COLLECTION , $bdg);
                                if(isset($badgeEl["name"])) {
                                    $badge[] = $badgeEl["name"];
                                }
                            }
                        }

                        $candidateCount = self::countAvalaiblecandidate($depense);

                        $candidatePart = $candidateCount;
                        if($candidatePart != 0) {
                            $candidatperc = 100 / intval($candidatePart);
                        }else{
                            $candidatperc = 0;
                        }

                        foreach ($depense["estimates"] as $bg_id => $bdg){
                            if (!empty($bdg["percentage"])){
                                $candidatperc = $bdg["percentage"];
                            }

                            /*if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
                                $candidatassignBudget = 0;
                                if (!empty($bdg["assignBudget"])) {
                                    $candidatassignBudget = $bdg["assignBudget"];
                                }
                            }*/

                            $candidatassignBudget = 0;
                            if (!empty($bdg["AssignBudgetArray"])) {
                                foreach ($bdg["AssignBudgetArray"] as $index => $value) {
                                    if(!empty($value["price"])) {
                                        $candidatassignBudget += $value['price'];
                                    }
                                }
                            }

                            if (MongoId::isValid($bg_id) && (empty($bdg["deny"]) || $bdg["deny"] == false)){


                                $personname = PHDB::findOneById(Person::COLLECTION, $bg_id)["name"];
                                $tablecandidatename[] = $personname;

                                if(!empty($bdg["validate"]) && $bdg["validate"] == "validated") {
                                    $tablecandidatestatus[] = "validé";
                                    if (!empty($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
                                        $personnamecell = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            ".$personname."
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='coremudrline valideline ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".validate' data-id='" . $idanswer . "' data-action=''>invalider candidat</a></li>
                                          </ul>
                                        </div>";
                                    }else{
                                        $personnamecell = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            ".$personname."
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='coremudrline btnAssignBudgetArrayT' data-max='".$candidatassignBudget."' data-assignpricelist='".json_encode(@$bdg["AssignBudgetArray"])."' data-uid='".$bg_id."' data-key='depense' data-form='aapStep1' data-pos='".$iddepense."' data-id='" . $idanswer . "' data-action='validated'> ajouter corénumeration</a></li>
                                            <li role='presentation' class='divider'></li>
                                            <li role='presentation'><a class='coremudrline valideline ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".validate' data-id='" . $idanswer . "' data-action=''>invalider candidat</a></li>
                                          </ul>
                                        </div>";
                                    }
                                }else{
                                    $tablecandidatestatus[] = "non validé";
                                    $personnamecell = "<div class='dropdown'>
                                          <a class='dropdown-toggle coremudrbtn coremubtnerror dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                            ".$personname."
                                            <i class='fa fa-add-user'></i> <span class='caret'></span>
                                          </a>
                                          <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                            <li role='presentation'><a class='coremudrline valideline ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".validate' data-id='" . $idanswer . "' data-action='validated'>valider candidat</a></li>
                                          </ul>
                                        </div>";
                                }

                                if(!empty($bdg["validate"]) && $bdg["validate"] == "validated") {
                                    if (!empty($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
                                        $arrLine = array();

                                        $sousMontant = "<div class='dropdown'>
                                              <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                                ".round(($depense["price"] * $candidatperc) / 100, 2)."
                                                <i class='fa fa-add-user'></i> <span class='caret'></span>
                                              </a>
                                              <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                                <li role='presentation'><a class='coremudrline validebudgetperc ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id . ".percentageState' data-id='" . $idanswer . "' data-action=''>invalider corému</a></li>
                                              </ul>
                                            </div>";

                                        if (!empty($bdg["percentageState"]) && $bdg["percentageState"] == "validated" ) {

                                            if($onefield) {
                                                $arrLine = array(
                                                    "poste de depense" => @$candidatebtn,
                                                    "badge requis" => (!empty($badge) ? $badge : " "),
                                                    "montant du depense" => @$depense["price"],
                                                    "candidat(e)" => $personnamecell,
                                                    "montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),
                                                    "lib corému" => $candidatperc . "%",
                                                    "corému" => $sousMontant,
                                                );
                                            }else{
                                                $arrLine = array(
                                                    "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                    "poste de depense" => @$candidatebtn,
                                                    "badge requis" => (!empty($badge) ? $badge : " "),
                                                    "montant du depense" => @$depense["price"],
                                                    "candidat(e)" => $personnamecell,
                                                    "montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),
                                                    "lib corému" => $candidatperc . "%",
                                                    "corému" => $sousMontant,
                                                );
                                            }

                                            $tablestatus[] = "validé";
                                        }else{

                                            if($onefield) {
                                                $arrLine = array(
                                                    "poste de depense" => @$candidatebtn,
                                                    "badge requis" => (!empty($badge) ? $badge : " "),
                                                    "montant du depense" => @$depense["price"],
                                                    "candidat(e)" => $personnamecell,
                                                    "montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),
                                                    "lib corému" => $candidatperc . "%",
                                                    "corému" => $sousMontant
                                                );
                                            }else{
                                                $arrLine = array(
                                                    "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                    "poste de depense" => @$candidatebtn,
                                                    "badge requis" => (!empty($badge) ? $badge : " "),
                                                    "montant du depense" => @$depense["price"],
                                                    "candidat(e)" => $personnamecell,
                                                    "montant total pour le candidat" => round(($depense["price"] * $candidatperc) / 100, 2),
                                                    "lib corému" => $candidatperc . "%",
                                                    "corému" => $sousMontant
                                                );
                                            }

                                            $tablestatus[] = "non validé";
                                        }
                                        $tableAnswers[] = $arrLine;

                                    }else{
                                        if(!empty($bdg["AssignBudgetArray"] )) {
                                            foreach ($bdg["AssignBudgetArray"] as $index => $value) {
                                                $arrLine = array();
                                                if (isset($value["check"]) && $value["check"] == "true") {
                                                    $sousMontant = "<div class='dropdown'>
                                                      <a class='dropdown-toggle coremudrbtn coremubtnsuccess dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                                        ".$value["price"]."
                                                        <i class='fa fa-add-user'></i> <span class='caret'></span>
                                                      </a>
                                                      <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                                        <li role='presentation'><a class='coremudrline validebudgetasg ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id .".AssignBudgetArray.".$index.".check' data-id='" . $idanswer . "' data-action='false'>invalider corému</a></li>
                                                      </ul>
                                                    </div>";

                                                    if($onefield){
                                                        $arrLine = array(
                                                            "poste de depense" => @$candidatebtn,
                                                            "badge requis" => (!empty($badge)? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget ,
                                                            "lib corému" => $value["label"] ,
                                                            "corému" => $sousMontant
                                                        );

                                                        $tablestatus[] = "validé";

                                                    }else{
                                                        $arrLine = array(
                                                            "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                            "poste de depense" => @$candidatebtn,
                                                            "badge requis" => (!empty($badge)? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget ,
                                                            "lib corému" => $value["label"] ,
                                                            "corému" => $sousMontant
                                                        );

                                                        $tablestatus[] = "non validé";
                                                    }
                                                }else {
                                                    $sousMontant = "<div class='dropdown'>

                                                      <a class='dropdown-toggle coremudrbtn coremubtnerror dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown'>
                                                        ".@$value["price"]."
                                                        <i class='fa fa-add-user'></i> <span class='caret'></span>
                                                      </a>
                                                      <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu1'>
                                                        <li role='presentation'><a class='coremudrline validebudgetasg ' data-path='answers.aapStep1.depense." . $iddepense . ".estimates." . $bg_id .".AssignBudgetArray.".$index.".check' data-id='" . $idanswer . "' data-action='true'>valider corému</a></li>
                                                      </ul>
                                                    </div>";

                                                    if($onefield){
                                                        $arrLine = array(
                                                            "poste de depense" => @$candidatebtn,
                                                            "badge requis" => (!empty($badge)? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget ,
                                                            "lib corému" => $value["label"] ,
                                                            "corému" => $sousMontant
                                                        );

                                                        $tablestatus[] = "non validé";
                                                    }else{
                                                        $arrLine = array(
                                                            "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                            "poste de depense" => @$candidatebtn,
                                                            "badge requis" => (!empty($badge)? $badge : " "),
                                                            "montant du depense" => @$depense["price"],
                                                            "candidat(e)" => $personnamecell,
                                                            "montant total pour le candidat" => $candidatassignBudget ,
                                                            "lib corému" => @$value["label"] ,
                                                            "corému" => $sousMontant
                                                        );

                                                        $tablestatus[] = "non validé";
                                                    }
                                                }
                                                $tableAnswers[] = $arrLine;
                                            }
                                        }else {

                                            if($onefield){
                                                $arrLine = array(
                                                    "poste de depense" => @$candidatebtn,
                                                    "badge requis" => (!empty($badge)? $badge : " "),
                                                    "montant du depense" => @$depense["price"],
                                                    "candidat(e)" => $personnamecell,
                                                    "montant total pour le candidat" => $candidatassignBudget ,
                                                    "lib corému" => "(aucun)" ,
                                                    "corému" => "(aucun)" ,
                                                );

                                                $tablestatus[] = "non assigné";

                                            }else{
                                                $arrLine = array(
                                                    "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                    "poste de depense" => @$candidatebtn,
                                                    "badge requis" => (!empty($badge)? $badge : " "),
                                                    "montant du depense" => @$depense["price"],
                                                    "candidat(e)" => $personnamecell,
                                                    "montant total pour le candidat" => $candidatassignBudget ,
                                                    "lib corému" => "(aucun)" ,
                                                    "corému" => "(aucun)" ,
                                                );

                                                $tablestatus[] = "non assigné";
                                            }
                                            $tableAnswers[] = $arrLine;
                                        }
                                    }

                                    if(!in_array($personname, $propopielabels)) {
                                        $propopielabels[] = $personname;
                                        $propopiedata[array_search($personname, $propopielabels)] = 0;
                                        $propobardata[0][array_search($personname, $propopielabels)] = 0;
                                        $propobardata[1][array_search($personname, $propopielabels)] = 0;
                                    }

                                    if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
                                        $propobardata[0][array_search($personname, $propopielabels)] += $candidatassignBudget;
                                    }else{
                                        $propobardata[0][array_search($personname, $propopielabels)] += round(($depense["price"] * $candidatperc) / 100, 2);
                                    }

                                }else{
                                    if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage") {
                                        if($onefield){
                                            $arrLine = array(
                                                "poste de depense" => @$candidatebtn,
                                                "badge requis" => (!empty($badge)? $badge : " "),
                                                "montant du depense" => @$depense["price"],
                                                "candidat(e)" => $personnamecell,
                                                "montant total pour le candidat" => " " ,
                                                "lib corému" => "(candidat(e) pas validé(e))" ,
                                                "corému" => " ",
                                            );

                                            $tablestatus[] = "non assigné";
                                        }else{
                                            $arrLine = array(
                                                "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                "poste de depense" => @$candidatebtn,
                                                "badge requis" => (!empty($badge)? $badge : " "),
                                                "montant du depense" => @$depense["price"],
                                                "candidat(e)" => $personnamecell,
                                                "montant total pour le candidat" => " " ,
                                                "lib corému" => "(candidat(e) pas validé(e))" ,
                                                "corému" => " ",
                                            );

                                            $tablestatus[] = "non assigné";
                                        }
                                        $tableAnswers[] = $arrLine;

                                    }else{
                                        if($onefield){
                                            $arrLine = array(
                                                "poste de depense" => @$candidatebtn,
                                                "badge requis" => (!empty($badge)? $badge : " "),
                                                "montant du depense" => @$depense["price"],
                                                "candidat(e)" => $personnamecell,
                                                "montant total pour le candidat" => $candidatassignBudget ,
                                                "lib corému" => "(candidat(e) pas validé(e))",
                                                "corému" => " " ,
                                            );

                                            $tablestatus[] = "non assigné";
                                        }else{
                                            $arrLine = array(
                                                "contexte" => $answer['answers']["aapStep1"]["titre"],
                                                "poste de depense" => @$candidatebtn,
                                                "badge requis" => (!empty($badge)? $badge : " "),
                                                "montant du depense" => @$depense["price"],
                                                "candidat(e)" => $personnamecell,
                                                "montant total pour le candidat" => $candidatassignBudget ,
                                                "lib corému" => "(candidat(e) pas validé(e))",
                                                "corému" => " " ,
                                            );

                                            $tablestatus[] = "non assigné";
                                        }
                                        $tableAnswers[] = $arrLine;
                                    }

                                    if(!in_array($personname, $propopielabels)) {
                                        $propopielabels[] = $personname;
                                        $propopiedata[array_search($personname, $propopielabels)] = 0;
                                        $propobardata[0][array_search($personname, $propopielabels)] = 0;
                                        $propobardata[1][array_search($personname, $propopielabels)] = 0;
                                    }

                                    if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
                                        $propobardata[1][array_search($personname, $propopielabels)] += $candidatassignBudget;
                                    }else{
                                        $propobardata[1][array_search($personname, $propopielabels)] += round(($depense["price"] * $candidatperc) / 100, 2);
                                    }
                                }

                                $tablepercentage[] = $candidatperc;
                                if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
                                    $tableprice[] = $candidatassignBudget;
                                }else{
                                    $tableprice[] = round(($depense["price"] * $candidatperc) / 100, 2);
                                }
                                if(!in_array($personname, $propopielabels)) {
                                    $propopielabels[] = $personname;
                                    $propopiedata[array_search($personname, $propopielabels)] = 0;
                                    $propobardata[0][array_search($personname, $propopielabels)] = 0;
                                    $propobardata[1][array_search($personname, $propopielabels)] = 0;
                                }

                                if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
                                    $propopiedata[array_search($personname, $propopielabels)] += $candidatassignBudget;
                                }else{
                                    $propopiedata[array_search($personname, $propopielabels)] += round(($depense["price"] * $candidatperc) / 100, 2);
                                }

                                if (isset($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "assignBudget") {
                                    $tableSankeyLinks[] = array(
                                        "target" =>  str_replace(["'" , '"'], "&#039",@$depense["poste"]),
                                        "source" => str_replace(["'" , '"'], "&#039",@$depense["poste"]),
                                        "value" => $candidatassignBudget
                                    );
                                }else{
                                    $tableSankeyLinks[] = array(
                                        "target" =>  str_replace(["'" , '"'], "&#039",@$depense["poste"]),
                                        "source" => str_replace(["'" , '"'], "&#039",@$depense["poste"]),
                                        "value" => round(($depense["price"] * $candidatperc) / 100, 2)
                                    );
                                }

                                if(!in_array(array("name" => str_replace(["'" , '"'], "&#039",@$depense["poste"]) , "level" => 0 ), $tableSankeyNodes , true)) {
                                    $tableSankeyNodes[] = array("name" => str_replace(["'" , '"'], "&#039",@$depense["poste"]) , "level" => 0 );
                                }

                                if(!in_array(array("name" => str_replace(["'" , '"'], "&#039",@$depense["poste"]) , "level" => 1 ), $tableSankeyNodes, true)) {
                                    $tableSankeyNodes[] = array("name" => str_replace(["'" , '"'], "&#039",@$depense["poste"]) , "level" => 1 );
                                }
                            }
                        }

                    }else{
                        if($onefield){
                            $arrLine = array(
                                "poste de depense" => @$candidatebtn,
                                "badge requis" => (!empty($badge)? $badge : " "),
                                "montant du depense" => @$depense["price"],
                                "candidat(e)" => " ",
                                "montant total pour le candidat" => " " ,
                                "lib corému" => " ",
                                "corému" => " " ,
                            );

                            $tablestatus[] = "non assigné";
                        }else{
                            $arrLine = array(
                                "contexte" => $answer['answers']["aapStep1"]["titre"],
                                "poste de depense" => @$candidatebtn,
                                "badge requis" => (!empty($badge)? $badge : " "),
                                "montant du depense" => @$depense["price"],
                                "candidat(e)" => " ",
                                "montant total pour le candidat" => " " ,
                                "lib corému" => " ",
                                "corému" => " " ,
                            );

                            $tablestatus[] = "non assigné";
                        }
                        $tableAnswers[] = $arrLine;
                    }

                    if(empty($depense["estimates"][Yii::app()->session["userId"]])){

                        //candidater

                        $badge = [];
                        $candidate = [];
                        if (isset($depense["badge"])){
                            foreach ($depense["badge"] as $bdg){
                                $badgeEl = PHDB::findOneById(Badge::COLLECTION , $bdg);
                                if(isset($badgeEl["name"])) {
                                    $badge[] = $badgeEl["name"];
                                }
                            }
                        }

                        $tableprice[] = $depense["price"];

                        if(!in_array('non spécifié', $propopielabels)) {
                            $propopielabels[] = 'non spécifié';
                            $propopiedata[array_search('non spécifié', $propopielabels)] = 0;
                        }

                        $propopiedata[array_search('non spécifié', $propopielabels)] += $depense["price"];

                        $tableSankeyLinks[] = array(
                            "target" =>  str_replace(["'" , '"'], "&#039",@$depense["poste"]),
                            "source" => "non spécifié",
                            "value" => $depense["price"]
                        );

                        if(!in_array(array("name" => "non spécifié" , "level" => 0 ), $tableSankeyNodes , true)) {
                            $tableSankeyNodes[] = array("name" => "non spécifié" , "level" => 0 );
                        }

                        if(!in_array(array("name" => str_replace(["'" , '"'], "&#039",@$depense["poste"]) , "level" => 1 ) , $tableSankeyNodes , true)) {
                            $tableSankeyNodes[] = array("name" => str_replace(["'" , '"'], "&#039",@$depense["poste"]) , "level" => 1 );
                        }

                    }

                }
                if(!empty($answer["answers"]["aapStep1"]["depense"])) {
                    $depenseAction = Aap::getDepenseAction($answer["answers"]["aapStep1"]["depense"]);
                    $totalcandidattovalid = $depenseAction["totalcandidattovalid"];
                    $totalpricetovalid = $depenseAction["totalpricetovalid"];
                    $totalneedcandidat = $depenseAction["totalneedcandidat"];
                }

                $totalansweraction = $totalcandidattovalid + $totalpricetovalid + $totalneedcandidat;
            }
        }

	    return array (
            "tableAnswers" => $tableAnswers,
            "tableSankey" => $tableSankey,
            "tablepercentage" => $tablepercentage,
            "tableprice" => $tableprice,
            "tablestatus" => $tablestatus,
            "tablecandidatename" => $tablecandidatename,
            "tablecandidatestatus" => $tablecandidatestatus,
            "propopiedata" => $propopiedata,
            "propopielabels" => $propopielabels,
            "propobarlabel" => $propobarlabel,
            "tableSankeyLinks" => $tableSankeyLinks,
            "tableSankeyNodes" => $tableSankeyNodes,
            "notificationbar" => $notificationbar,
            "realpropositiondata" => $realpropositiondata,
            "propobardata" => $propobardata,
            "totalcandidattovalid" => $totalcandidattovalid,
            "totalpricetovalid"  => $totalpricetovalid,
            "totalneedcandidat" => $totalneedcandidat,
            "totalansweraction" => $totalansweraction,

        );
    }

    public static function answerCoremuFinancerVariables($answers) {
        $tableAnswers = array();

        $tableSankeyLinks = array();
        $tableSankeyNodes = array(array("name" =>"sans financeur"));
        foreach ($answers as $idanswer => $answer){

            $answerform = PHDB::findOneById( Form::ANSWER_COLLECTION , (string)$answer["_id"]);
            $formAns = PHDB::findOneById(Form::COLLECTION , $answerform["form"]);
            $contextAns = PHDB::findOneById($formAns["parent"][array_keys($formAns["parent"])[0]]["type"] , array_keys($formAns["parent"])[0] );

            $tableSankeyNodes[0] = array("name" => $contextAns["name"]);

            if (isset($answer['answers']["aapStep1"]["titre"]) && isset($answer['answers']["aapStep1"]["depense"])){
                foreach ($answer['answers']["aapStep1"]["depense"] as $iddepense => $depense){
                    if (!empty($depense["financer"])){

                        foreach ($depense["financer"] as $bg_id => $bdg){

                                $tableAnswers[] = array(
                                    "poste de depense" => $depense["poste"] ,
                                    "depense" => $depense["price"] ,
                                    "libelé financeur" => (!empty($bdg["name"]) ? $bdg["name"] : $contextAns["name"]),
                                    "libelé financement" => @$bdg["line"] ,
                                    "recette" => @$bdg["amount"]
                                    );

                        }

                    }

                }
            }
        }

        return array (
            "tableAnswers" => $tableAnswers,
        );
    }

    public static function matchBadge($depenseLine, $communityLine){
        $haveBadges = false;
	    if (!empty($depenseLine["badge"])) {
            $haveBadges = false;
            if(!empty($communityLine["badge"])) {
                $haveBadges = true;
                foreach ($depenseLine["badge"] as $badge) {
                    if (empty($communityLine["badge"][$badge])){
                        $haveBadges = false;
                    }
                }
            }else{
                $haveBadges = false;
            }
        }else{
            $haveBadges = true;
        }
        return $haveBadges;
    }

    public static function countAvalaiblecandidate($depenseLine){
        $candidatecount = 0;
        if (!empty($depenseLine["estimates"])) {
            foreach ($depenseLine["estimates"] as $depenseEstimateId => $depenseEstimate) {
                if (MongoId::isValid($depenseEstimateId) && (empty($depenseEstimate["deny"]) || $depenseEstimate["deny"] == false)) {
                    $candidatecount++;
                }
            }
        }
        return $candidatecount;
    }

    public static function countValidecandidate($depenseLine){
        $candidatecount = 0;
        if (!empty($depenseLine["estimates"])) {
            foreach ($depenseLine["estimates"] as $depenseEstimateId => $depenseEstimate) {
                if (MongoId::isValid($depenseEstimateId) && (empty($depenseEstimate["deny"]) || $depenseEstimate["deny"] == false) && (!empty($depenseEstimate["validate"]) && $depenseEstimate["validate"] == "validated")) {
                    $candidatecount++;
                }
            }
        }
        return $candidatecount;
    }

    public static function countPriceEstimate($depenseLine){
        $countpriceestimate = 0;
        if( isset($depenseLine["estimates"] ))
        {

            foreach ( $depenseLine["estimates"] as $estimateId => $estimate )
            {
                if( !MongoId::isValid($estimateId)  && (empty($estimate["ispriceselected"]) || $estimate["ispriceselected"] != true ))
                {
                    $countpriceestimate += 1;
                }
            }
        }
        return $countpriceestimate;
    }

    public static function getpriceEstimateSelected($depenseLine){
        if( isset($depenseLine["estimates"] ))
        {
            foreach ( $depenseLine["estimates"] as $estimateId => $estimate )
            {
                if( !empty($estimate["ispriceselected"]) && $estimate["ispriceselected"] == true )
                {
                    return $estimateId;
                }
            }
        }
        return false;
    }

    public static function getTotalDep($depenseLine){
        $total = 0;
	    if (isset($depenseLine["switchAmountAttr"]) && $depenseLine["switchAmountAttr"] == "assignBudget") {
            if (!empty($depenseLine["price"])) {
                if (!empty($depenseLine["estimates"])) {
                    foreach ($depenseLine["estimates"] as $index => $estimate) {
                        if(!empty($estimate["AssignBudgetArray"])){
                            foreach ($estimate["AssignBudgetArray"] as $index2 => $value2) {
                                $total += $value2["price"];
                            }
                        }
                    }
                    return $total;
                }else{
                    return $total;
                }
            }
        }
        return false;
    }

    public static function getEstimateInfo($estimateId , $estimate){
        $actualCandidate = array();
        $profilPicture = "";
	    if( MongoId::isValid($estimateId) && (empty($estimate["deny"]) || $estimate["deny"] == false)){
            $actualCandidate = PHDB::findOneById(Person::COLLECTION, $uid);
            if (isset($actualCandidate["profilImageUrl"])) {
                $profilPicture = Yii::app()->createUrl('/' . $actualCandidate["profilImageUrl"]);
            } else {
                $profilPicture = Yii::app()->getModule("co2")->assetsUrl . '/images/thumb/default_citoyens.png';
            }


        }

	    return array (
	        "actualCandidate" => $actualCandidate,
            "profilPicture" => $profilPicture,
        );
    }

    public static function getDepenseAction($answer){
        $totalAction = 0;

        $totalcandidattovalid = 0;
        $totalpricetovalid = 0;
        $totalneedcandidat = 0;

        $candidattovalid = array();
        $pricetovalid = array();
        $needcandidat = array();

        foreach ($answer as $depId => $depense){
            $candidattovalid[$depId] = 0;
            $pricetovalid[$depId] = 0;
            $needcandidat[$depId] = 0;

            if (!empty($depense["estimates"])){
                if(
                    self::countAvalaiblecandidate($depense) == 0
                    || (!empty($depense["candidateNumber"]) && self::countValidecandidate($depense) != $depense["candidateNumber"] )
                ){
                    $totalneedcandidat ++;
                    $needcandidat[$depId] ++;
                }

                foreach($depense["estimates"] as $esti => $estimate){
                    if (MongoId::isValid($esti)) {
                        if (empty($estimate["validate"]) || $estimate["validate"] != "validated"){
                            $candidattovalid[$depId] ++;
                            $totalcandidattovalid ++;
                        }
                        if (!empty($depense["switchAmountAttr"]) && $depense["switchAmountAttr"] == "percentage"){
                            if (empty($estimate["percentageState"]) || $estimate["percentageState"] != "validated"){
                                $pricetovalid[$depId] ++;
                                $totalpricetovalid ++;
                            }
                        } else {
                            if (!empty($estimate["AssignBudgetArray"])){
                                foreach ($estimate["AssignBudgetArray"] as $bdgarray){
                                    if (empty($bdgarray["check"]) || $bdgarray["check"] != "true"){
                                        $pricetovalid[$depId] ++;
                                        $totalpricetovalid ++;
                                    }
                                }
                            }
                        }
                    }
                }

            }else{
                $totalneedcandidat ++;
                $needcandidat[$depId] ++;
            }
        }

        return array (
            'totalcandidattovalid' => $totalcandidattovalid,
            'totalpricetovalid' => $totalpricetovalid,
            'totalneedcandidat' => $totalneedcandidat,
            'candidattovalid' => $candidattovalid,
            'pricetovalid' => $pricetovalid,
            'needcandidat' => $needcandidat,
        );
    }
}
