<?php

class DashboardData {
    
    public static  function simplePie($contextId, $countAllAnswers, $paramsData, $extraQuery=null){
        $dataArray = [];
        $array_values = [];
        $nextTo = 0;
        $paramsData["pieColors"] = ["#918CC4", "#C2BEDF","#FFFFFF"];

        if(isset($paramsData["coform"]) && !empty($paramsData["coform"]) && isset($paramsData["answerValue"]) && is_array($paramsData["answerValue"])){

            $k = explode(".", $paramsData["answerPath"]);
            $query = array(
                'form' => $paramsData["coform"]
            );
            // For supplement query
            if(isset($extraQuery)){
                array_push($query, $extraQuery);
            }
            foreach ($paramsData["answerValue"] as $avkey => $avvalue) {
                if(str_contains($paramsData["answerPath"], "multiCheckboxPlus")){
                    $paramsData["pieColors"] = ["#E16E4A","#F6D5C4","#FFFFFF"];
                    array_push($query, ['answers.'.$paramsData["answerPath"] => array(
                            '$elemMatch' => [$avvalue.'' => ['$exists' => true]]
                        )]
                    );
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }else if(str_contains($paramsData["answerPath"], "multiRadio")){
                    array_push($query, ['answers.'.$paramsData["answerPath"].'.value' => $avvalue] );
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }
                $randColor = $paramsData["pieColors"][$avkey] ?? "blue";
                array_push($dataArray, array('name' => $avvalue, 'count' => $countThis, 'percentage' => round(($countThis*100)/$countAllAnswers), 'color'=>$randColor));

                array_push($array_values, (($countThis*100)/$countAllAnswers));
            }

            if(str_contains($paramsData["answerPath"], "multiCheckboxPlus") && isset($array_values[0])){
                $nextTo = round($array_values[0]);
                array_push($dataArray, array('name' => "", 'count' => ($countAllAnswers-$countThis), 'percentage' => "", 'color'=>$paramsData["pieColors"][1]));

                array_push($array_values, ((($countAllAnswers-$countThis)*100)/$countAllAnswers));
            }
        }

        return array('nameCountArray' => $dataArray, 'percentageArray' => $array_values, "nextTo"=>$nextTo);
    }

    public static function horizontalBar($contextId, $countAllAnswers, $paramsData, $extraQuery=null){
        $dataArray = [];
        $array_values = [];
        if(!isset($paramsData["answerPath"])){
            $paramsData["answerPath"]="";
        }

        if(!isset($paramsData["answerValue"])){
            $paramsData["answerValue"]="";
        }
        // Default Form
        if(isset($paramsData["coform"]) && !empty($paramsData["coform"]) && is_array($paramsData["answerValue"])){
            $k = explode(".", $paramsData["answerPath"]);
            $query = array('form' => $paramsData["coform"]);
            // For supplement query
            if(isset($extraQuery)){
                $query = array_merge($query, $extraQuery);
            }
            foreach ($paramsData["answerValue"] as $avkey => $avvalue) {
                if((str_contains($paramsData["answerPath"], "multiCheckboxPlus")||str_contains($paramsData["answerPath"], "checkboxcplx")) && is_array($paramsData["answerValue"])){
                    $query["answers.".$paramsData["answerPath"]] = array('$elemMatch' => [$avvalue."" => ['$exists' => true]]);
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }else if(str_contains($paramsData["answerPath"], "multiRadio") || str_contains($paramsData["answerPath"], "radiocplx")){
                    $query["answers.".$paramsData["answerPath"].".value"] = $avvalue;
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query); 
                }else if(str_contains($paramsData["answerPath"], "checkboxNew") && is_array($paramsData["answerValue"])){
                    $answerPath = str_replace("checkboxNew", "", $paramsData["answerPath"]);
                    $query['answers.'.$answerPath.''] = $avvalue;
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }else if(str_contains($paramsData["answerPath"], "radioNew") && is_array($paramsData["answerValue"])){
                    $answerPath = str_replace("radioNew", "", $paramsData["answerPath"]);
                    $query['answers.'.$answerPath.''] = $avvalue;
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }else{
                    $query['answers.'.$paramsData["answerPath"]] = $avvalue;
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }

                $roundedValue=round(($countThis*100)/$countAllAnswers);
                if($roundedValue==0){
                    $roundedValue = round(($countThis*100)/$countAllAnswers, 1);
                    array_push($dataArray, array('label' => $avvalue, 'value' => $roundedValue));
                    array_push($array_values, $roundedValue);
                }else{
                    array_push($dataArray, array('label' => $avvalue, 'value' => $roundedValue));
                    array_push($array_values, $roundedValue);
                }
                
            }
        }
        return array('labelValueArray' => $dataArray, 'percentageArray' => $array_values);
    }

    public static function progressBar($contextId, $countAllAnswers, $paramsData, $extraQuery=null){
        $countThis = 0;
        if(isset($paramsData["coform"]) && !empty($paramsData["coform"])){
            $query = array('form' => $paramsData["coform"]);
            // ajouter requete supplémentaire
            if(isset($extraQuery)){
                $query = array_merge($query, $extraQuery);
            }

            try{
                if((str_contains($paramsData["answerPath"], "multiCheckboxPlus")||str_contains($paramsData["answerPath"], "checkboxcplx")) && is_array($paramsData["answerValue"])){
                    $subQuery = ['$or' => array()];
                    foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                        array_push($subQuery['$or'], [$avvalue.'' => ['$exists' => true]]);
                    }
                    $query['answers.'.$paramsData["answerPath"]] = array('$elemMatch' => $query);
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query); 
                }else if(str_contains($paramsData["answerPath"], "multiRadio") || str_contains($paramsData["answerPath"], "radiocplx")){
                    $query['answers.'.$paramsData["answerPath"].'.value'] = array('$in' =>$paramsData["answerValue"]);
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $qeury); 
                }else if(str_contains($paramsData["answerPath"], "checkboxNew") && is_array($paramsData["answerValue"])){
                    $query['$or'] = array();
                    $answerPath = str_replace("checkboxNew", "", $paramsData["answerPath"]);
                    foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                        array_push($query['$or'], ['answers.'.$answerPath.'' => $avvalue]);
                    }
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }else if(str_contains($paramsData["answerPath"], "radioNew") && is_array($paramsData["answerValue"])){
                    $query['$or'] = array();
                    $answerPath = str_replace("radioNew", "", $paramsData["answerPath"]);
                    foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                        array_push($query['$or'], ['answers.'.$answerPath.'' => $avvalue]);
                    }
                    $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
                }
            }catch(\Throwable $th){

            }
        }

        return array('countAllAnswers' => $countAllAnswers, 'value' => $countThis);
    }

    public static function progressCircle($contextId, $countAllAnswers, $paramsData, $extraQuery=null){
        $countThis = 0;

        if(isset($paramsData["coform"]) && !empty($paramsData["coform"]) && isset($paramsData["answerValue"]) && is_array($paramsData["answerValue"])){
            $query = array('form' => $paramsData["coform"]);
            // For supplement query
            if(isset($extraQuery)){
                $query = array_merge($query, $extraQuery);
            }
            if((str_contains($paramsData["answerPath"], "multiCheckboxPlus")||str_contains($paramsData["answerPath"], "checkboxcplx")) && is_array($paramsData["answerValue"])){
                $subQuery= ['$or' => array()];
                foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                    array_push($subQuery['$or'], [$avvalue.'' => ['$exists' => true]]);
                }
                $query['answers.'.$paramsData["answerPath"]] = array('$elemMatch' => $subQuery);
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query); 
            }else if(str_contains($paramsData["answerPath"], "multiRadio") || str_contains($paramsData["answerPath"], "radiocplx")){
                $query['answers.'.$paramsData["answerPath"].'.value'] = array('$in' =>$paramsData["answerValue"]);
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query); 
            }else if(str_contains($paramsData["answerPath"], "checkboxNew") && is_array($paramsData["answerValue"])){
                $query['$or'] = array();
                $answerPath = str_replace("checkboxNew", "", $paramsData["answerPath"]);
                foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                    array_push($query['$or'], ['answers.'.$answerPath.'' => $avvalue]);
                }
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
            }else if(str_contains($paramsData["answerPath"], "radioNew") && is_array($paramsData["answerValue"])){
                $query['$or'] = array();
                $answerPath = str_replace("radioNew", "", $paramsData["answerPath"]);
                foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                    array_push($query['$or'], ['answers.'.$answerPath.'' => $avvalue]);
                }
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
            }
        }

        return array('allCount' => $countAllAnswers, 'value' => $countThis);
    }

    public static function simpleValue($contextId, $countAllAnswers, $counts, $paramsData, $extraQuery=null){
        $countThis = 0;
        $query = array();
        if(empty($paramsData["answerPath"])){
            $paramsData["answerPath"] = "";
        }

        if(empty($paramsData["answerValue"])){
            $paramsData["answerValue"]="";
        }
        if(isset($paramsData["coform"]) && !empty($paramsData["coform"])){
            $query = array('form' => $paramsData["coform"]);
            // For Spécific query (Case here: territorial obs)
            if(isset($extraQuery)){
                $query = array_merge($query, $extraQuery);
            }

            if((str_contains($paramsData["answerPath"], "multiCheckboxPlus")||str_contains($paramsData["answerPath"], "checkboxcplx")) && is_array($paramsData["answerValue"])){
                $subQuery = ['$or' => array()];
                foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                    array_push($subQuery['$or'], [$avvalue.'' => ['$exists' => true]]);
                }
                $query['answers.'.$paramsData["answerPath"]] = array('$elemMatch' => $subQuery);
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query); 
            }else if(str_contains($paramsData["answerPath"], "multiRadio") || str_contains($paramsData["answerPath"], "radiocplx")){
                $query['answers.'.$paramsData["answerPath"].'.value'] = array('$in' =>$paramsData["answerValue"]);
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query); 
            }else if(str_contains($paramsData["answerPath"], "checkboxNew") && is_array($paramsData["answerValue"])){
                $query['$or'] = array();
                $answerPath = str_replace("checkboxNew", "", $paramsData["answerPath"]);
                foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                    array_push($query['$or'], ['answers.'.$answerPath.'' => $avvalue]);
                }
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
            }else if(str_contains($paramsData["answerPath"], "radioNew") && is_array($paramsData["answerValue"])){
                $query['$or'] = array();
                $answerPath = str_replace("radioNew", "", $paramsData["answerPath"]);
                foreach ($paramsData["answerValue"] as $avkey => $avvalue){
                    array_push($query['$or'], ['answers.'.$answerPath.'' => $avvalue]);
                }
                $countThis = PHDB::count(Form::ANSWER_COLLECTION, $query);
            }else{
                try{
                    $match_query = array('$match' => $query);
                    $sum_query = array(
                        '$group' => array(
                            '_id'=> array("form"=>'$form'),
                            "count"=> array( '$sum' => 1 )
                        )
                    );

                    if(isset($paramsData["answerPath"]) && $paramsData["answerPath"]!=""){
                        $sum_query['$group']["total"] = array( '$sum'=> array('$convert' => array('input'=> '$answers.'.$paramsData["answerPath"], 'to'=>'decimal', 'onError' => "arror accured") ));
                    }

                    $results = PHDB::aggregate(Form::ANSWER_COLLECTION, array($match_query, $sum_query));

                    if(isset($results["ok"]) && $results["ok"]==1.0 && isset($results["result"]) && isset($results["result"][0])){
                        $countThis = $results["result"][0]; //("answerPath", $paramsData["answerPath"]);
                    }
                }catch(Exception $except){
                    //var_dump($except);
                }
            }
        }

        $text_value = $countAllAnswers;

        if(isset($countThis) && isset($paramsData["answerPath"])){
            if($paramsData["answerPath"]!="" && $paramsData["answerValue"]!="" && !is_array($countThis)){
                $text_value = round(($countThis*100)/$countAllAnswers)."%";
            }else if($paramsData["answerPath"]!="" && is_array($countThis)){
                $text_value = $countThis["total"];
            }
        }

        if( (isset($paramsData["unit"]) && str_contains($paramsData["unit"],"avg")) || (!isset($paramsData["answerValue"]) || $paramsData["answerValue"]=="") && isset($paramsData["isAverage"]) && ($paramsData["isAverage"]==true || $paramsData["isAverage"]=="true")){
            $s = (string)$text_value;
            $text_value = round($s/PHDB::count(Form::ANSWER_COLLECTION, $query));
        }

        if(isset($paramsData["roundMillionValue"]) && ($paramsData["roundMillionValue"]==true || $paramsData["roundMillionValue"]=="true")&& strlen($text_value)>6){
            $text_value = substr($text_value, 0, 1).",".substr($text_value, 1, 1)." M";
        }

        if(!str_contains($text_value, "%") && isset($paramsData["unit"]) && !str_contains($paramsData["unit"],"avg") ){
            $text_value = $text_value." ".$paramsData["unit"];
        }

        if(!str_contains($text_value, "%") && isset($paramsData["unit"]) && str_contains($paramsData["unit"],"%") ){
            $text_value = $text_value." %";
        }

        if($counts==0){
            $text_value = 0;
        }

        return $text_value;
    }

    public static function answerByMembers($id, $slug, $countAllAnswers, $extraQuery=null)
    {
        $query = array(
            '$or' => [
                array("source.keys"=>$slug),
                array("reference.costum"=>$slug)
            ]
        );

        if(isset($extraQuery)){
            $query = array_merge($extraQuery, $query);
        }

        $countCommunity = PHDB::count(Organization::COLLECTION, $query);
        
        if($countCommunity==0){
            $countCommunity=1;
        }

        $text_value = round(($countAllAnswers/$countCommunity)*100);

        return array('text_value' => $text_value, 'countCommunity' => $countCommunity);
    }

    public static function doubleProgressBar($contextId, $paramsData, $extraQuery=null){
        $value1 = 1;
        $value2 = 1;

        $query = array("form" => $paramsData["coform"]);

        if(isset($extraQuery)){
            $query = array_merge($query, $extraQuery);
        }

        $sum_query1 = array(
            '$group' => array(
                '_id'=> array("form"=>'$form'),
                "total" => array( '$sum'=>  array('$convert' => array('input'=> '$answers.'.$paramsData["answerPath1"], 'to'=>'decimal', 'onError' => "arror accured") ) ),
                "count"=> array( '$sum' => 1 )
            )
        );

        $sum_query2 = array(
            '$group' => array(
                '_id'=> array("form"=>'$form'),
                "total" => array( '$sum'=>  array('$convert' => array('input'=> '$answers.'.$paramsData["answerPath2"], 'to'=>'int', 'onError' => "arror accured") ) ),
                "count"=> array( '$sum' => 1 )
            )
        );

        try{
            $match_query = array(
                '$match' => $query
            );

            $ca1 = PHDB::aggregate(Form::ANSWER_COLLECTION, array($match_query, $sum_query1));
            $ca2 = PHDB::aggregate(Form::ANSWER_COLLECTION, array($match_query, $sum_query2));

            if(isset($ca1["ok"]) && $ca1["ok"]==1.0 && !empty($ca1["result"])){
                $value1 = $ca1["result"][0]["total"]; //("answerPath", $paramsData["answerPath"]);
            }
            if(isset($ca2["ok"]) && $ca2["ok"]==1.0 && !empty($ca2["result"])){
                $value2 = $ca2["result"][0]["total"]; //("answerPath", $paramsData["answerPath"]);
            }
        }catch(Exception $except){
            //var_dump($except);
        }

        return array('value1' => $value1, 'value2' => $value2);
    }

    public static function nbTLgeomapped($contextId, $countByScope){
        if(isset($countByScope) && $countByScope!=0){
            $number = $countByScope;
        }else{
            $number = PHDB::count(
                Organization::COLLECTION, 
                array(
                    '$or' => [
                        array('source.keys'=>'franceTierslieux'), 
                        array('reference.costum'=>'franceTierslieux'),
                        array('links.memberOf.'.$contextId => ['$exists'=>1])
                    ]
                )
            );
        }
        
        return $number;
    }

    public static function percentageByRegion($paramsData, $bId, $scopeId){
        $block = PHDB::findOneById(Cms::COLLECTION, $bId);
        $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form"=>$paramsData["coform"], "draft"=>['$exists'=>false]));
        $regionsId = array();
        $data_count = array();
        $data_percent = array();
        
        foreach($answers as $key => $ans){
            if(isset($ans["links"]["organizations"])){
                $orga = $ans["links"]["organizations"];
                array_push($regionsId, array_key_first($orga));
            }
        }

        $geoMappedTL = PHDB::findByIds(Organization::COLLECTION, $regionsId, ["address"]);

        foreach($geoMappedTL as $key => $org ){
            if(isset($scopeId) && $scopeId!=""){
                if( isset($org["address"]) && isset($org["address"]["level3Name"]) && isset($org["address"]["level3"]) && $org["address"]["level3"]==$scopeId){
                    if(isset($data_count[$org["address"]["level3Name"]])){
                        $data_count[$org["address"]["level3Name"]]["value"] = $data_count[$org["address"]["level3Name"]]["value"]+1;
                    }else{
                        $data_count[$org["address"]["level3Name"]] = array("label" => $org["address"]["level3Name"], 'value' => 1 );
                    }
                }
            }else{
                if( isset($org["address"]) && isset($org["address"]["level3Name"])){
                    if(isset($data_count[$org["address"]["level3Name"]])){
                        $data_count[$org["address"]["level3Name"]]["value"] = $data_count[$org["address"]["level3Name"]]["value"]+1;
                    }else{
                        $data_count[$org["address"]["level3Name"]] = array("label" => $org["address"]["level3Name"], 'value' => 1 );
                    }
                }else{
                    if(isset($data_count["Pas d'adresse"])){
                        $data_count["Pas d'adresse"]["value"]=$data_count["Pas d'adresse"]["value"]+1;
                    }else{
                        $data_count["Pas d'adresse"] = array("label" => "Pas d'adresse", "value" => 1 );
                    }
                }
            }
            
        }

        foreach($data_count as $key => $d ){
            $data_count[$key]["value"] = round(($data_count[$key]["value"]*100)/count($answers));
        }

        //if((empty($block["answerValue"]) || $block["answerValue"]=="") || (isset($block["answerValue"]) &&  count(array_keys($data_count) > count($block["answerValue"])) )){
            PHDB::update(Cms::COLLECTION, array("_id"=>new MongoId($bId)), array('$set' => ["answerValue" => array_keys($data_count)]));
        //}

        return array('labelValueArray' => array_values($data_count), 'percentageArray' => $data_percent);
    }

    public static function bail36mois($coform, $extraQuery=null){
        $lowerThan36mois = 0;
        
        $query = array(
            "form" => $coform ,
            "answers.franceTierslieux1522023_1549_3.franceTierslieux1522023_1549_3le5si0qhpywv18p94ne" => [
                '$exists' => true
            ]
        );

        if(isset($extraQuery)){
            $query = array_merge($query, $extraQuery);
        }
        
        $bails = PHDB::find(Form::ANSWER_COLLECTION, $query );

        array_merge($query, $extraQuery);

        foreach($bails as $key => $value){
            $vb = $value["answers"]["franceTierslieux1522023_1549_3"]["franceTierslieux1522023_1549_3le5si0qhpywv18p94ne"];
            if($vb <= 36 ){
                $lowerThan36mois++;
            }
        }

        return round(($lowerThan36mois*100)/count($bails))." %";
    }

    public static function countByElementTags($contextId , $paramsData, $scopeId){
        $data_count = array();
        $data_percent = array();

        $query = [
            '$or' => [
                array('source.keys'=>'franceTierslieux'), 
                array('reference.costum'=>'franceTierslieux'), 
                array('links.memberOf.'.$contextId => ['$exists'=>1])
            ]
        ];
        
        if(isset($scopeId) && $scopeId!=""){
            $query['address.level3'] = $scopeId;
        }

        $tls = PHDB::count( Organization::COLLECTION, $query );
        
        $counts = 0;
            
        foreach ($paramsData["answerValue"] as $key => $value) {
            $query['tags'] = $value;
            $num = PHDB::count( Organization::COLLECTION, $query );
            if($tls > 0){
                if(isset($paramsData["path"]) && str_contains($paramsData["path"], "progressCircleMultiple") ){
                    $data_count[$value] = array( 'label' => $value, 'value' => round(($num*100)/$tls) );
                }else if(isset($paramsData["path"]) && str_contains($paramsData["path"], "progressCircle")){
                    $counts=$counts+$num;
                }else{
                    $counts=$counts+$num;
                }
            }else{
                $count=0;
            }
        }
        
        if(isset($paramsData["path"]) && str_contains($paramsData["path"], "progressCircleMultiple") ){
            return array('labelValueArray' => array_values($data_count), 'percentageArray' => $data_percent);
        }else if(isset($paramsData["path"]) && str_contains($paramsData["path"], "progressCircle")){
            $data_percent = array('allCount' => $tls, 'value' => $counts);
            return $data_percent;
        }else{
            return $counts;
        }
    }

    public static function countByCodeInsee($contextId, $densityDegree, $scopeId){
        $countThis = 0;
        $tlQuery = [
            '$or' => [
                array('source.keys'=>'franceTierslieux'), 
                array('reference.costum'=>'franceTierslieux'), 
                array('links.memberOf.'.$contextId => ['$exists'=>1])
            ]
        ];
        if(isset($scopeId) && $scopeId!=""){
            $tlQuery["address.level3"]=$scopeId;
        }

        $tl = PHDB::find( Organization::COLLECTION, $tlQuery, ["address"]);
        
        $postalCodes = array();
        $matched = array();
        foreach ($tl as $key => $t) {
            if( isset($t["address"]) && isset($t["address"]["postalCode"])){
                if(isset($postalCodes[$t["address"]["postalCode"]])){
                    $postalCodes[$t["address"]["postalCode"]] = $postalCodes[$t["address"]["postalCode"]]+1;
                }else{
                    $postalCodes[$t["address"]["postalCode"]] = 1;
                }
            }
        }
        
        $cityQuery = array(
            'inseeNum' => [ '$exists' => 1 ], 
            'density' => [ '$exists' => 1 ], 
            "country" => "FR",
            "postalCodes.0.postalCode" => ['$in' => array_keys($postalCodes)],
            'density.degréDensité' => $densityDegree 
        );

        if(isset($scopeId) && $scopeId!=""){
            $cityQuery["level3"]=$scopeId;
        }

        $postalCodesOfTL = PHDB::find( City::COLLECTION, $cityQuery, ["postalCodes"]);
        
        foreach ($postalCodesOfTL as $key => $p) {
            $pc = $p["postalCodes"]["0"]["postalCode"];
            if(isset($pc) && isset($postalCodes[$pc])){
                if(!isset($matched[$pc])){
                    $countThis = $countThis + $postalCodes[$pc];
                    $matched[$pc]=1;
                }
            }
        }

        return array('allCount' => count($tl), 'value' => $countThis);
    }


    public static function countQPV($contextId, $paramsData, $scopeId){
        $existInQPV = array();
        $zoneQuery = array('type' =>"QPV", "level1Name" => "France");
        if(isset($scopeId) && $scopeId!=""){
            $zoneQuery["level3"] = $scopeId;
        }
        $zonesQPV = PHDB::find(Zone::COLLECTION, $zoneQuery, ["insee"]);
        $codeInseeQPV = array();
        foreach ($zonesQPV as $key => $z) {
            array_push($codeInseeQPV, $z["insee"]);
        }
        
        $tlQuery = [
            '$or' => [
                array('source.keys' => 'franceTierslieux'), 
                array('reference.costum' => 'franceTierslieux'),
                array('links.memberOf.'.$contextId => ['$exists'=>1])
            ],
            'address.codeInsee' => ['$in' => $codeInseeQPV]
        ];

        if(isset($scopeId) && $scopeId!=""){
            $tlQuery["address.level3"] = $scopeId;
        }
        
        $tls = PHDB::find( Organization::COLLECTION, $tlQuery , ["address.codeInsee"]);

        foreach ($tls as $key => $tl) {
            $existInQPV[$tl["address"]["codeInsee"]]=1;
        }

        if(str_contains($paramsData["answerPath"], "countQPVTL")){
            //return count($tls); 
            return count(array_keys($existInQPV));
        }else{
            //return count(array_keys($existInQPV));
            return count($zonesQPV);
        }
    }
}

?>

