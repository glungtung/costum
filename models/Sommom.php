<?php
class Sommom {
	public static function canEditAnswer($answer){
        $res = false;
        // var_dump($answer);
        if(isset($answer['answer']["form"])){
        	$fid = $answer['answer']["form"];
	        $form = PHDB::findOne( Form::COLLECTION, [ "_id" => new MongoId($fid) ],["parent"] ) ;
	        $res = Authorisation::canEditItem( Yii::app()->session["userId"],$form["parent"][array_keys($form["parent"])[0]]["type"], array_keys($form["parent"])[0] ) || self::isPartenaire();
	       
	        return $res; 
        }
        
    }


    public static function isPartenaire(){
        $costum = CacheHelper::getCostum();
        if(isset($costum["communityLinks"]["contributors"][Yii::app()->session["userId"]]["roles"]) && is_array($costum["communityLinks"]["contributors"][Yii::app()->session["userId"]]["roles"]) && in_array("Partenaire", $costum["communityLinks"]["contributors"][Yii::app()->session["userId"]]["roles"])){
        	return true;
        } else {
        	return false;
        }
    }
}