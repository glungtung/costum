<?php

class Costum {
    const COLLECTION = "costum";
    const CONTROLLER = "costum";
    const MODULE = "costum";
    
    //used in initJs.php for the modules definition
    public static function getConfig($context=null){
        return array(
            "collection"    => self::COLLECTION,
            "controller"   => self::CONTROLLER,
            "module"   => self::MODULE,
            "categories" => CO2::getModuleContextList(self::MODULE, "categories", $context),
            "lbhp" => true
        );
    }

    public static $paramsCo= array(
        "label",
        "subdomainName",
        "placeholderMainSearch",
        "icon",
        "useFilter",
        "slug",
        "formCreate",
        "initType", 
        "inMenu",
        "types",
        "actions", 
        "height",
        "href",
        //ELEMENT CONFIG
        "labelClass",
        "view",
        "action",
        "dir",
        "edit",
        "dataTarget", 
        "id",  
        "class",
        "dataAttr",
        "target",
        "toggle",
        "typeAllow",
        "userConnected"
    );
        
    //$ctrl if == null we are jsut initing the session data, no rendering 
    //$id 
    // if given with a type it concerns an element
    // otherwise it's just costum can be the slugname or the digital id 
    //$type : 
        // if given with an id it concerns an element
    //$slug 
        // it's the slug of an element
    //$init this action can render a result for a page request
    //$jsonOrDB defines if working with the data/xxxx.json file
    public static function init($ctrl, $id=null,$type=null,$slug=null, $edit=false, $source="db",$test=null,$where=null){
        //$params = CO2::getThemeParams();
        //Yii::app()->session['paramsConfig']=$params;
        $elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl","profilBannerUrl", "name", "tags", "description","costum", "links", "profilRealBannerUrl","slug","rocketchatMultiEnabled","reference"];
        $tmp = array();
        //ex : http://127.0.0.1/ph/costum/co/index/slug/cocampagne
        //le slug appartient à un élément
        $debug = false;
        if($debug){
            var_dump("where ".$where);
            var_dump("id ".$id);
            var_dump("type ".$type);
            var_dump("slug ".$slug);
         //exit;
        }
        if ( isset($test) ) { 
            if($debug)var_dump('by test '.$test);
            $id = $test; 
            $el = Slug::getElementBySlug($slug, $elParams );
            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            $tmp['contextSlug'] = $el["el"]["slug"];
        }
        else if ( isset($slug) ) { 
            if($debug)var_dump('by element slug '.$slug);
            $el = Slug::getElementBySlug($slug, $elParams );

            if($debug)var_dump("el exists:".!empty($el));

            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            $tmp['contextSlug'] = $el["el"]["slug"];
            if(isset($el["el"]["costum"]['slug'])){
                $id = (isset($el["el"]["costum"]['id'])) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
            else {
                if($debug)var_dump('by default costum slug costumBuilder');
                //generate the costum tag on the element with a default COstum template 
                //can be changed in the costum admin 
                $cacheCostum = Costum::getCostumCache();
                $id = (isset($cacheCostum["slug"])) ? $cacheCostum["slug"] : "costumBuilder"; 
            }         


        } 

        //ex : http://127.0.0.1/ph/costum/co/index/id/xxxx/type/organizations
        //l'id et le type permettent de retrouver un élément
        else if(isset($type) && isset($id)){
            //<<<<<<< HEAD
            var_dump('by type and ID '.$slug." : ".$type);
            var_dump($elParams);
            // =======
            //             // Rest::json($type); Rest::json($id);  exit;
            // >>>>>>> qa
            $el = array("el"=>Element::getByTypeAndId($type, $id, $elParams ));
            $tmp['contextType'] = $type;
            $tmp['contextId'] = $id;
            $tmp['contextSlug'] = $el["el"]["slug"];
            if(@$el["el"]["costum"]){
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
        }
        //ex : http://127.0.0.1/ph/costum/co/index/id/cocampagne
        //l'id est celui du costum
        else if(isset($_GET["id"])){
            $id=$_GET["id"];
            // var_dump("else"); Rest::json($id);  exit;
        } 
                
                    
        if($debug){
            var_dump('contextId : '.@$tmp['contextId']);
            var_dump('contextType : '.@$tmp['contextType']);
            var_dump('contextSlug : '.@$tmp['contextSlug']);
            var_dump("id ".$id);
        }
        if(!empty($id)){
            if(strlen($id) == 24 && ctype_xdigit($id)  ){
                $c = PHDB::findOne( Costum::COLLECTION , array("_id"=> new MongoId($id))); 
            }
            else {
                $c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $id));
            }

                       
            //ATTENTION : only for dev and debug purposes
            //to be sure this never appears on prod
            /*if(YII_DEBUG) {
                if(isset($c["slug"])) {
                $docsJSON = file_get_contents("../../modules/costum/data/".$c["slug"].".json", FILE_USE_INCLUDE_PATH);
                $c = json_decode($docsJSON,true);
                if($source == "json"){
                    echo "********** FROM costum/data/".$c["slug"].".json **********<br/>";
                    if($c == null){
                        echo "********************<br/>";
                        echo "Check you json file : BAD STRUCTURE. <br/>";
                        echo "Le data ".$c["slug"]." json ne charge<br/>";
                        echo "********************<br/>";
                        echo @$docsJSON;
                        exit;
                    }
                    else {
                        //could be cool to show in json render 
                        var_dump($c);
                        exit;
                    }
                    echo "********** FROM db custom/slug : ".$c["slug"]." **********<br/>";
                }
                
            } else {
                $docsJSON = file_get_contents("../../modules/costum/data/".$id.".json", FILE_USE_INCLUDE_PATH);
                $c = json_decode($docsJSON,true);
            }
            }*/
            
            
        }
        else if(@$_GET["host"]){
            $c = PHDB::findOne( Costum::COLLECTION , array("host"=> $_GET["host"]));
            // var_dump($c);
            if(empty($c)){
                $s = PHDB::findOne( Slug::COLLECTION , array("host"=> $_GET["host"]));
                // var_dump($s);exit;
                $el = Slug::getElementBySlug($s["name"], $elParams );
                //var_dump($el);
                // var_dump($el);exit;
                $tmp['contextType'] = $el["type"];
                $tmp['contextId'] = $el["id"];
                $tmp['contextSlug'] = $el["el"]["slug"];
                if(isset($el["el"]["costum"]['slug'])){
                    $id = (isset($el["el"]["costum"]['id'])) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
                }
                else {
                    //generate the costum tag on the element with a default COstum template 
                    //can be changed in the costum admin 
                    if(isset(Yii::app()->session["costum"]["slug"]))
                        $id = Yii::app()->session["costum"]["slug"]; 
                    else 
                        $id = "costumBuilder"; 
                }
                $c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $el["el"]["costum"]['slug']));
            }
        }

        //if($debug)var_dump($c);
        if($debug)var_dump("el exists:".!empty($el));
        if(isset($c) && !empty($c)){ 

            // var_dump($slug);exit;
            if(isset($c["redirect"])){
               // $slug=explode(".", $_GET["el"])[1];
                $redirect = PHDB::findOne( Costum::COLLECTION , array("slug"=> $c["redirect"]));
                $url = (isset($redirect["host"])) ? "https://".$redirect["host"] : Yii::app()->createUrl("/costum/co/index/slug/".$redirect["slug"]);
                header('Location: '.$url);//Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]));
                exit;
            }

            if(!isset($slug) && empty($el)){
                $el = Slug::getElementBySlug($c["slug"], $elParams );
//if($debug)var_dump("el exists:".!empty($el));exit;
                $slug = $c["slug"];
                $tmp['contextType'] = $el["type"];
                $tmp['contextId'] = $el["id"];
                $tmp['contextSlug'] = $el["el"]["slug"];
            }         
            if(isset($tmp['contextType']) && empty($el) ){
                $el = Slug::getElementBySlug($c["slug"], $elParams );
                $c['contextType'] = $tmp['contextType'];
                $c['contextId'] = $tmp['contextId'];
                $c['contextSlug'] = $tmp['contextSlug'];
            }

            if($debug) var_dump($el); 
            if($debug)var_dump("el exists:".!empty($el));
            
            if(!empty($el["el"]))
            {
                $element = [
                    "contextType" => $el["type"],
                    "contextId" => $el["id"],
                    "contextSlug"=> $el["el"]["slug"],
                    "title"=> $el["el"]["name"],
                    "description"=> @$el["el"]["shortDescription"],
                    "descriptions" => @$el["el"]["description"],
                    "tags"=> @$el["el"]["tags"],
                    "communityLinks"=> @$el["el"]["links"],
                    "isCostumAdmin"=>false,
                    "assetsUrl"=> Yii::app()->getModule("costum")->getAssetsUrl(true),
                    "url"=> "/costum/co/index/slug/".$c["slug"],
                    "rocketchatMultiEnabled" => @$el["el"]["rocketchatMultiEnabled"] ? @$el["el"]["rocketchatMultiEnabled"] : false,
                    "banner" => @$el["el"]["profilRealBannerUrl"], 
                    "reference" => @$el["el"]["reference"]
                ];

//  var_dump($element);
// exit;
                //$form = (isset($_GET["form"])) ? [ "form" => PHDB::findOne( Form::COLLECTION, [ "_id" => new MongoId($_GET["form"]) ] )] : [];  
//  var_dump($form);
// exit;        
                if(isset($c["logo"]) && !empty($c["logo"])){
                    $c["logo"] = Yii::app()->getModule( "costum" )->getAssetsUrl().$c["logo"];
                }
                else if( isset($el["el"]["profilImageUrl"] )) 
                    $c["logo"] = Yii::app()->createUrl($el["el"]["profilImageUrl"]);
                

                if(isset($c["mailsConfig"]["logo"])){
                    $c["mailsConfig"]["logo"] = Yii::app()->getModule("costum")->getAssetsUrl().$c["mailsConfig"]["logo"];
                }

                if(isset($c["logoMin"]))
                    $c["logoMin"] = Yii::app()->getModule( "costum" )->getAssetsUrl().$c["logoMin"];

                
                $c = array_merge( $c , $element);//, $form );

                
                //possibly overload the costum template 
                if( isset( $el["el"]["costum"] ) && $c["slug"] == @$el["el"]["costum"]["slug"]  ){
                    unset($el["el"]["costum"]['id']);
                    unset($el["el"]["costum"]['slug']);
                    $c = self::combine($c,$el["el"]["costum"] );
                    $c["assetsSlug"]=$c["slug"];
                    $c["slug"]=$el["el"]["slug"];
                    //if(!empty($el["el"]["costum"]["app"])){
                      //  $appInMenu=array();
                      /*  if(isset($c["app"])){
                            foreach($c["app"] as $k => $v){
                                if(!isset($el["el"]["costum"]["app"][$k])){
                                    unset($c["app"][$k]);
                                }
                            }    
                        }
                        
                        foreach($el["el"]["costum"]["app"] as $k => $v){
                            if(!empty($v)){
                                $appInMenu[$k]=true;
                            }
                        }
                        if(isset($c["appMenusPath"])){
                            foreach($c["appMenusPath"] as $v){
                                $path='htmlConstruct.'.$v.'.buttonList';
                               // $pathArray=explode(".", $path);
                                $c=ArrayHelper::setValueByDotPath($c, $path, $appInMenu);
                             //   self::set_recursive($c, $pathArray, $appInMenu);
                                //$c=self::findAndReplaceInArray($c, $pathArray, $appInMenu);
                            }
                        }*/
                   // }
                    //var_dump($c);exit;

                }
                
                if(isset($c["htmlConstruct"]["loadingModal"]["logo"])){
                    $c["htmlConstruct"]["loadingModal"]["logo"] =  Yii::app()->getModule( "costum" )->getAssetsUrl().$c["htmlConstruct"]["loadingModal"]["logo"];
                }

                // Besoin d'approfindir ce sujet des jsons et de leurs utilisations
                // Ici je l'ai utilisé pour injecter des tags spécifiques au pacte / on pourrait aussi penser au catégories d'événement
//var_dump($c["tags"]); exit;
                if(isset($c["lists"]))
                    $c=Costum::getAndConvertLists($c);
                if(isset($c["json"])){
                    foreach($c["json"] as $k => $v){
                        
                        $c[$k]=Costum::getContextList($c["slug"],$v);
                        if($k=="tags")
                            $c["request"]["searchTag"]=$c["tags"];
                            //$c["request"]["searchTag"]=[$c["tags"]];          
                    }
                }
                
                if( isset($c["searchTag"]) ) 
                    $c["request"]["searchTag"]=[$c["searchTag"]];

                if(isset($c["searchTag"])) $c["request"]["searchTag"]=[$c["searchTag"]];
                if(isset($c["scopeSelector"]))
                    $c=Costum::initScopeSelector($c);
                /* metadata */

                //Costum::getCheckCustomUser($c,$el);
            }

            //Vérifie si le slug du costum est générique ou non
            if ([$c["slug"]] != [$el["el"]["slug"]]) {
                $c["request"]["sourceKey"] = [$el["el"]["slug"]];
            }else{
                $c["request"]["sourceKey"] = [$c["slug"]];
            }
            // var_dump($c["request"]["sourceKey"]);exit;

            // $c["contextId"] = $ctxId;
            // $c["contextType"] = $ctxType;
            if(self::isSameFunction("init"))
                $c = self::sameFunction("init",$c);

            if(!empty($c["forms"]) && $c["forms"] === true){
                $c["forms"] = PHDB::find( Form::COLLECTION, 
                        array(  "parent.".$c['contextId'] => array('$exists' => true),
                                "source.keys" => array('$in' => array($c["slug"]) ) ),
                        array("name", "mapping") );
            }
          
            //if(!@Yii::app()->session['paramsConfig'] || empty(Yii::app()->session['paramsConfig'])) 
            //Yii::app()->session['paramsConfig'] = CO2::getThemeParams();
            $c["appConfig"]=self::filterThemeInCustom($c,CacheHelper::get("appConfig"));
           // $c["htmlConstruct"]= self::checkAndReplace($c["htmlConstruct"],$c["appConfig"]);
                   
        // var_dump($c);exit;

        //var_dump($_POST["costumSlug"]);

        //var_dump($c);


        return $c;

        }    
 //if($debug)var_dump('session contextId : '.@Yii::app()->session['costum']['contextId']);
 //if($debug)var_dump('session contextType : '.@Yii::app()->session['costum']['contextType']);

 

    }
   /* public static function set_recursive(&$array, $path, $value)
    {
      $key = array_shift($path);
      if (empty($path)) {
        $array[$key] = $value;
      } else {
        if (!isset($array[$key]) || !is_array($array[$key])) {
          $array[$key] = array();
        }
        self::set_recursive($array[$key], $path, $value);
      }
    }*/
    /*public static function findAndReplaceInArray($final, $pathArray, $value, $result=array()){
        
        foreach($pathArray as $v){
            $result[$v]=array();
        }
        if(!empty($pathArray)){
            $result[]=$final[$pathArray[0]];

        }else{

            $result=self
        }
        return $result;

    }*/
    public static function set_nested_array_value(&$array, $path, &$value, $delimiter = '/') {
        $pathParts = explode($delimiter, $path);

        $current = &$array;
        foreach($pathParts as $key) {
            $current = &$current[$key];
        }

        $backup = $current;
        $current = $value;
        return $backup;
    }
    public static function getCostumCache(){
        return CacheHelper::getCostum();
    }
    
    public static function getCheckCustomUser($c,$el){
        if(isset(Yii::app()->session["userId"])){
            $costumUserArray = array();
            $costumUserArray["admins"]= Element::getCommunityByTypeAndId($el["type"], $el["id"], Person::COLLECTION,"isAdmin");
            $communityLinks=Element::getElementById($el["id"],$el["type"], null, array("links"));
            $costumUserArray["isMember"]= ( $el["type"] != City::COLLECTION) ? Link::isLinked($el["id"], $el["type"], Yii::app()->session["userId"], @$communityLinks["links"]) : null;

            if(Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) && !isset($costumUserArray["admins"][Yii::app()->session["userId"]]) || Authorisation::isUserSuperAdminCms()){
                $costumUserArray["admins"][Yii::app()->session["userId"]]=array("type"=>Person::COLLECTION, "isAdmin"=>true);
            }
            if(isset($costumUserArray["admins"][Yii::app()->session["userId"]])){

                //Yii::app()->session['isCostumAdmin']=true;
                $costumUserArray["isCostumAdmin"]=true;
            }else{
                //Yii::app()->session['isCostumAdmin']=null;
                $costumUserArray["isCostumAdmin"]=false;
            }
            //hasRoles
            $links=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null, array("links"));
            $costumRoles=array();

            if($el["type"] != City::COLLECTION && isset($links["links"][Link::$linksTypes[Person::COLLECTION][$el["type"]]])){
                if( isset($communityLinks["links"][Link::$linksTypes[$el["type"]][Person::COLLECTION]])){ 
                    foreach($links["links"][Link::$linksTypes[Person::COLLECTION][$el["type"]]] as $k =>$v){
                        if(!empty($v["isAdmin"]) && !isset($v["isAdminPending"]) && isset($communityLinks["links"][Link::$linksTypes[$el["type"]][Person::COLLECTION]][$k]["roles"])){
                            $costumRoles=array_merge($costumRoles, $communityLinks["links"][Link::$linksTypes[$el["type"]][Person::COLLECTION]][$k]["roles"]);
                        }
                    }
                }
            }
            
            if(!empty($costumRoles))
                $costumUserArray["hasRoles"]=$costumRoles;
            //var_dump(self::isSameFunction("getCheckCustomUser")); exit;
            if(self::isSameFunction("getCheckCustomUser")){
                // CARREFUL $costumUserArray need to be return as result : return $params["costumUserArray"]
                $costumUserArray=Costum::sameFunction("getCheckCustomUser", array(
                    "costumUserArray"=>$costumUserArray, 
                    "communityLinks"=>$communityLinks,
                    "el"=>$el,
                    "userLinks"=>$links));

                //var_dump($costumUserArray); exit;
            }
            
            //
            $costumUserArray=Costum::checkUserPreferences($costumUserArray, Yii::app()->session["userId"], $c["slug"]);
            //Rest::json($costumUserArray); exit;
            Yii::app()->session["costum"] = array($c["slug"] => $costumUserArray);

            return Yii::app()->session["costum"][$c["slug"]];
        }

    }

    //merge and update existing fields 
    // to be used when applying specific changes defined on an element into a template costum object
    public static function combine($a1, $a2) {
        $deleteValueOfParentCostum=["buttonList"];
        foreach($a2 as $k => $v) {
            if(is_array($v)) {
                if(!isset($a1[$k]))
                    $a1[$k] = null;
                if(!in_array($k, $deleteValueOfParentCostum))
                    $a1[$k] = self::combine($a1[$k], $v);
                else 
                    $a1[$k]=$v;
            } else {
                $a1[$k] = $v;
            }
        }
        return $a1;
    }

    /**
     * get a Poi By Id
     * @param String $id : is the mongoId of the poi
     * @return poi
     */
    public static function getById($id) { 
        $classified = PHDB::findOneById( self::COLLECTION ,$id );
        //$classified["parent"] = Element::getElementByTypeAndId()
        $classified["typeClassified"]=@$classified["type"];
        $classified["type"]=self::COLLECTION;
        $where=array("id"=>@$id, "type"=>self::COLLECTION, "doctype"=>"image");
        $classified["images"] = Document::getListDocumentsWhere($where, "image");//(@$id, self::COLLECTION);
        return $classified;
    }

    public static function getBySlug($id) { 
        return PHDB::findOne( self::COLLECTION ,array("slug"=>$id) );
    }
    public static function getAndConvertLists($costum){
        foreach($costum["lists"] as $key => $v){
            if(isset($v["type"]) && $v["type"]==Badge::COLLECTION){
                $costum=self::getListByBadges($key, $v, $costum);
            }
            else if(isset($v["collection"]) && isset($v["distinct"]))
                $costum["lists"][$key] = PHDB::distinct($v["collection"], $v["distinct"], $v["where"]);
            else if(isset($v["collection"]) && isset($v["where"])){
                if(isset($v["fields"]))
                    $costum["lists"][$key] = PHDB::find($v["collection"], $v["where"], $v["fields"]);
                else 
                    $costum["lists"][$key] = PHDB::find($v["collection"], $v["where"]);
            }
        }
        return $costum;
    }
    public static function update($params){
        $cacheCostum = Costum::getCostumCache();
        if(isset($cacheCostum["assetsSlug"])){
            $set=array();
            $unset=array();
            //var_dump($params);exit;
            foreach($params as $k => $v ){
              //  var_dump($v);exit;
                if(is_string($v) && $v==='$unset'){
                    $unset["costum.".$k]="";
                   // $cacheCostum=ArrayHelper::deleteValueByDotPath($cacheCostum, $k);   
            //        var_dump($cacheCostum["htmlConstruct"]["header"]["menuTop"]);
                }else{
                    //$cacheCostum=ArrayHelper::setValueByDotPath($cacheCostum, $k, $v);
                    $set["costum.".$k]=$v;
                }
            }
            $cond=[];
            if(!empty($set))
                $cond['$set']=$set;
            if(!empty($unset))
                $cond['$unset']=$unset;
            if(!empty($cond)){
                PHDB::update($cacheCostum["contextType"], 
                            array("_id"=> new MongoId($cacheCostum["contextId"])),
                            $cond
                );
                self::forceCacheReset();
            }
           
            //var_dump($newOne["htmlConstruct"]["header"]["menuTop"]);   
        }
        return true;
           
    }
    public static function forceCacheReset(){
        $cacheCostum = Costum::getCostumCache();
        if(!isset($cacheCostum["resetCache"])){ 
            $cacheCostum["resetCache"]=true;
            CacheHelper::set($cacheCostum["contextSlug"], $cacheCostum);
            if(CacheHelper::get($_SERVER['SERVER_NAME'])){
                $serverCostum=CacheHelper::get($_SERVER['SERVER_NAME']);
                $serverCostum["resetCache"]=true;
                CacheHelper::set($_SERVER['SERVER_NAME'], $serverCostum);
            }
        }
    }
    public static function getListByBadges($keyList, $search, $costum, $parentId=null){
        if(empty($parentId))
            $where=array("parent"=>array('$exists'=>false));
        else
            $where=array("parent.".$parentId=>array('$exists'=>true));
        if(isset($search["sourceKey"]) && !empty($search["sourceKey"]))
            $where["source.key"]=$costum["slug"];
        if(isset($search["category"]))
            $where["category"]=$search["category"];
        $badges=Badge::getByWhere($where, true);


        $typeList = null;

        if(!empty($costum["lists"][$keyList]["typeList"]) && $costum["lists"][$keyList]["typeList"] == "array" )
            $typeList = "array";


        if(empty($parentId))
            $costum["lists"][$keyList]=[];
        if(!empty($badges)){
            if(!isset($costum["badges"])) $costum["badges"]=array();
            if(!isset($costum["badges"][$keyList])) $costum["badges"][$keyList]=array();
            foreach($badges as $k => $v){
                $costum["badges"][$keyList][$k]=$v;
                if(!empty($parentId))
                    array_push($costum["lists"][$keyList][$costum["badges"][$keyList][$parentId]["name"]], $v["name"]);
                else if(!empty($typeList) && $typeList == "array" )
                     $costum["lists"][$keyList][]=$v["name"];
                else
                    $costum["lists"][$keyList][$v["name"]]=array();
                $costum=self::getListByBadges($keyList, $search, $costum, $k);
            }
        }else if(empty($badges) && empty($parentId)){
            if(!isset($costum["badges"])) $costum["badges"]=array();
            if(!isset($costum["badges"][$keyList])) $costum["badges"][$keyList]=array();
        }
        // var_dump($costum);
        return $costum;
    }
    /**
    * Function called by @self::filterThemeInCustom in order to treat params.json of communecter app considering like the config
    * This loop permit to genericly update value, add values or delete value in the tree
    * return the communecter entry filtered by costum expectations
    **/
    public static function checkCOstumList($check, $filter,$k=null){
        $newObj=array();
        $arrayInCustom=["label","subdomainName","placeholderMainSearch","icon","height","imgPath","useFilter","dropdownResult", "showMap", "useMapBtn","slug","formCreate", "setParams", "show", "initType", "inMenu","types","actions", "dataTarget", "dynform", "id", "filters", "extendFilters", "tagsList", "calendar", "class", "onlyAdmin", "restricted", "img", "nameMenuTop", "header", "name", "inMenuTop", "results", "filterObj", "module", "hash", "loadEvent","urlExtra"];
        foreach($check as $key => $v){
            
            if(!empty($v)){
                $newObj[$key]=(isset($filter[$key])) ? $filter[$key] : $v;
                $checkArray=true;
                foreach($arrayInCustom as $entry){
                    if(isset($v[$entry])){
                        $newObj[$key][$entry]=$v[$entry];
                        $checkArray=false;
                    }
                }
            }
            if(is_array($v) && $checkArray)
                $newObj[$key]=self::checkCOstumList($v, $newObj[$key], $k);
        }
        // var_dump($newObj);
        return $newObj;
    }
    public static function checkAndReplace($costum, $coParams){
        $filteringObj=array();
        if(empty($costum)) return false;
        foreach($costum as $key => $v){
            if(!empty($v)){
                $filteringObj[$key]=(isset($coParams[$key])) ? $coParams[$key] : $v;
                //Si simplement à true on récupére la valeur des paramètres de communecter
                if(is_bool($v) && isset($coParams[$key]) && is_bool($coParams[$key]) )
                    $filteringObj[$key]=$v;
                else if($v!==true && (is_string($v) || is_numeric($v)) && isset($filteringObj[$key]))
                    $filteringObj[$key]=$v;
                else if(is_array($v) && isset($filteringObj[$key])){
                    foreach($filteringObj[$key] as $i => $p){
                        if(!isset($v[$i]) && in_array($i, self::$paramsCo) ) $v[$i]=$p;
                    }
                    $filteringObj[$key]=self::checkAndReplace($v, $filteringObj[$key]);
                }
            }
        }
        return (!empty($filteringObj)) ? $filteringObj : false;
    }
    /*
    * @filterThemeInCustom permits to clean, update or add entry in paramsConfig of communecter thanks to costumParams 
    *   Receive Yii::app()->session["paramsConfig"] in order to modify it if Yii::app()->session["costum"] is existed
    *   
    */
    public static function filterThemeInCustom($c,$params){
        //filter menu app custom 
        // Html construction of communecter
        $paramCombine = $params;
        $appToKeep=$params["pages"];
        $menuApp = array("#annonces", "#search", "#agenda", "#live", "#dda", "#admin","#badge");
        if(isset($c["app"]) && !empty($c["app"])){
            $paramCombine["numberOfApp"]=count($c["app"]);
            $menuPages=self::checkCOstumList($c["app"], $paramCombine["pages"]);
            // Rest::json($menuPages); exit ;
            //var_dump($menuPages); exit ;
            foreach($paramCombine["pages"] as $hash => $v){
                if(!in_array($hash,$menuApp) && !isset($c["app"][$hash]))
                    $menuPages[$hash]=$v;
            }
            foreach($menuApp as $v){
                if(!isset($menuPages[$v])) $menuPages[$v]=$appToKeep[$v];
            }
            $paramCombine["pages"]=$menuPages;
        }
        if(isset($c["htmlConstruct"])){
            $constructParams=$c["htmlConstruct"];
            $listButtonMenus=array(
                "pages"=>$paramCombine["pages"],
                "mainMenuButtons"=>@$paramCombine["mainMenuButtons"],
                "elementMenuButtons"=>@$paramCombine["elementMenuButtons"]
            );
            //var_dump($paramCombine);exit;
                    // Check about app (#search, #live, #etc) if should costumized
           
//var_dump("ici");exit;
            // Check about header construction
            // - Entry banner to adda tpl path to a custom banner, by default not isset
            // - Entry menuTop will config btn present in menu top (navLeft & navRight)
            if(isset($constructParams["header"])){
                if(isset($constructParams["header"]["banner"])) //URL
                    $paramCombine["header"]["banner"]=$constructParams["header"]["banner"];
                if(isset($constructParams["header"]["css"]))
                    $paramCombine["header"]["css"]=$constructParams["header"]["css"];
                /*if(isset($constructParams["header"]["menuTop"]) && !empty($constructParams["header"]["menuTop"])){
                    $paramCombine["header"]["menuTop"]=self::constructMenu($constructParams["header"]["menuTop"],$paramCombine["header"]["menuTop"],$listButtonMenus, null, @$c["logo"]);
                }*/
            }
            foreach (["header.menuTop", "subMenu", "menuLeft", "menuRight", "menuBottom", "element.menuTop.left", "element.menuTop.right", "element.menuLeft"] as $keyMenu) {
                $costumMenuConfig = (strpos($keyMenu, ".") !== false) ? ArrayHelper::getValueByDotPath($constructParams, $keyMenu) : @$constructParams[$keyMenu];
                $themeMenuConfig=(strpos($keyMenu, ".") !== false) ? ArrayHelper::getValueByDotPath($paramCombine, $keyMenu) : @$paramCombine[$keyMenu];
                //echo "icccciiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii";
           
              // print_r($costumMenuConfig); 
               // var_dump($costumMenuConfig);
               // echo $keyMenu;
                // Get value of menu themeParams config with costum config 
                $appConfigMenu=(!empty($costumMenuConfig)) ? self::constructMenu($costumMenuConfig,$themeMenuConfig , $listButtonMenus, null, @$c["logo"]) : false;
                if(strpos($keyMenu, ".") !== false) 
                    $paramCombine=ArrayHelper::setValueByDotPath($paramCombine , $keyMenu, $appConfigMenu);
                else
                    $paramCombine[$keyMenu]=$appConfigMenu;
               // echo "result:::";
              //  if($keyMenu=="header.menuTop")
                //    print_r($paramCombine["header"]["menuTop"]); 
            }
            if(isset($constructParams["element"]["menuTop"]) && empty($constructParams["element"]["menuTop"]))
                $paramCombine["element"]["menuTop"]=false;  
         
        
            if(isset($constructParams["footer"])){
                if(!empty($constructParams["footer"]))
                    $paramCombine["footer"]=self::checkCOstumList($constructParams["footer"],$paramCombine["footer"]);
                else
                    $paramCombine["footer"]=$constructParams["footer"];
            }
            
            // Check about admin Panel navigation (cosstumized space for costum administration expectations)
            if(isset($constructParams["adminPanel"])){
                 if(isset($constructParams["adminPanel"]["js"]))
                    $paramCombine["adminPanel"]["js"]=$constructParams["adminPanel"]["js"];
               
                if(isset($constructParams["adminPanel"]["add"]))
                    $paramCombine["adminPanel"]["add"]=$constructParams["adminPanel"]["add"];
                if(isset($constructParams["adminPanel"]["menu"]))
                    $paramCombine["adminPanel"]["menu"]=self::checkCOstumList($constructParams["adminPanel"]["menu"],$paramCombine["adminPanel"]["menu"]);

                
            }
            // if(isset($constructParams["directory"]))
              //  $paramCombine["directory"]=self::checkCOstumList($constructParams["directory"],$paramCombine["directory"]);
                
            // Check about element page organization and content
            // - #initView is a param to give first view loaded when arrived in a page (ex : newstream, detail, agenda or another)
            // - #menuLeft of element => TODO PLUG ON checkCOstumList
            // - #menuTop of element => TODO finish customization & add it in params.json
            if(isset($constructParams["element"])){
                if(isset($constructParams["element"]["initView"]))
                    $paramCombine["element"]["initView"]=$constructParams["element"]["initView"];
                if(isset($constructParams["element"]["tplCss"]))
                    $paramCombine["element"]["tplCss"]=$constructParams["element"]["tplCss"];
                
              
                if(isset($constructParams["element"]["banner"])){
                    if(is_string($constructParams["element"]["banner"]))
                        $paramCombine["element"]["banner"]=$constructParams["element"]["banner"];
                    else
                        $paramCombine["element"]["banner"]=self::checkAndReplace($constructParams["element"]["banner"],$paramCombine["element"]["banner"]);
                }
                if(isset($constructParams["element"]["containerClass"])){
                  //  var_dump($constructParams["element"]["containerClass"]);exit;
                    $paramCombine["element"]["containerClass"]=self::checkAndReplace($constructParams["element"]["containerClass"],$paramCombine["element"]["containerClass"]);
                }
                if(isset($constructParams["element"]["js"]))
                    $paramCombine["element"]["js"]=$constructParams["element"]["js"];
                
                if(isset($constructParams["element"]["css"]))
                    $paramCombine["element"]["css"]=$constructParams["element"]["css"];
                
            }
             if(isset($constructParams["preview"])){
                if(isset($constructParams["preview"]["toolBar"]))
                    $paramCombine["preview"]["toolBar"]=self::checkAndReplace($constructParams["preview"]["toolBar"],$paramCombine["preview"]["toolBar"]);
                if(isset($constructParams["preview"]["banner"])){
                    $paramCombine["preview"]["banner"]=self::checkAndReplace($constructParams["preview"]["banner"],$paramCombine["preview"]["banner"]);
                }
                if(isset($constructParams["preview"]["body"])){
                    $paramCombine["preview"]["body"]=self::checkAndReplace($constructParams["preview"]["body"],$paramCombine["preview"]["body"]);
                }
            }
            // #appRendering permit to get horizontal and vertical organization of menu 
            //   - horizontal = first version of co2 with menu of apps in horizontal 
            //   - vertical = current version of co2 with menu of apps on the left 
            if(isset($constructParams["appRendering"]))
                $paramCombine["appRendering"]=$constructParams["appRendering"];
            if(isset($constructParams["loadingModal"]["url"]))
                $paramCombine["loadingModal"]=$constructParams["loadingModal"]["url"];
            
            if(isset($constructParams["directory"])){
                //var_dump($paramCombine["directory"]);
                $directoryP=$paramCombine["directory"];
                //var_dump($directoryP);
                $paramCombine["directory"]=$constructParams["directory"];
                //var_dump($paramCombine["directory"]);
                foreach($directoryP as $k => $v){
                    if(!isset($paramCombine["directory"][$k]))
                        $paramCombine["directory"][$k]=$v;
                }
            }

            // # redirect permits to application in case of undefined view or error or automatic redirection to fall on costum redirect
            // To be setting for logged user && unlogged user
            if(isset($constructParams["redirect"]))
                $paramCombine["pages"]["#app.index"]["redirect"]=$constructParams["redirect"];
        }
        if(isset($c["typeObj"]))
            $paramCombine["typeObj"]=$c["typeObj"];
        return $paramCombine;
    }
    public static function constructMenu($costum, $config, $completeMenuButton, $listConfigParams=array(),$logoUrl=""){
        $menuConfig=$config; 
        $menuKeyList=(isset($config["keyListButton"])) ? $config["keyListButton"] :  "mainMenuButtons";
        foreach($costum as $key => $v){
                if(!empty($key) && $key=="buttonList" && !empty($v)){
                    foreach($v as $k => $button){
                        $menuList=(empty($listConfigParams) && isset($completeMenuButton[$menuKeyList])) ? $completeMenuButton[$menuKeyList] : $listConfigParams;
                        $btnEntry="";

                        if(isset($menuList[$k])){
                            $btnEntry=$menuList[$k];
                            if(isset($btnEntry['keyListButton']) && isset($completeMenuButton[$btnEntry['keyListButton']])){
                                $menuList=$completeMenuButton[$btnEntry['keyListButton']]; 
                            }
                            if(!is_bool($button) && !is_string($button))
                                $btnEntry=self::constructMenu($button, $btnEntry, $completeMenuButton, $menuList);
                        }
                        else if(!empty($button) && is_array($button))
                            $btnEntry=$button;

                        if($k=="logo" && !empty($logoUrl))
                            $btnEntry["url"]=$logoUrl;
                        //if($k=="app")
                        //var_dump($btnEntry);
                        
                        if(isset($btnEntry["lbhAnchor"]) && $btnEntry["lbhAnchor"] === "true") $btnEntry["lbhAnchor"] = true;
                        if(isset($btnEntry["lbhAnchor"]) && $btnEntry["lbhAnchor"] === "false") $btnEntry["lbhAnchor"] = false;

                        $menuConfig[$key][$k]=$btnEntry;
                    }
                }else if(is_string($v) || is_bool($v) || is_numeric($v)){
                    $menuConfig[$key]=$v;
                }else if(is_array($v)){
                    if(isset($menuConfig[$key])){
                        $menuConfig[$key]=self::constructMenu($v, $menuConfig[$key], $completeMenuButton, [], $logoUrl);    
                    }
                    else        
                        $menuConfig[$key]=$v;
                }
          
        }
       // if(isset($menuConfig["buttonList"]) && isset($menuConfig["buttonList"]["xsMenu"]))
       // var_dump($menuConfig["buttonList"]["xsMenu"]["buttonList"]["app"]["buttonList"]);
        return $menuConfig;
       
    }
    public static function getContextList($slug, $contextName){

        $layoutPath ="../../modules/costum/json/".$slug."/".$contextName.".json";

        $str = file_get_contents($layoutPath);
        $list = json_decode($str, true);
        return $list;
    }

     /*
    * @checkUserPreferences will check for a costum presence of user settings
    * Example : Geographical selction to oriented a user experience (cf : @meuseCampagnes)
    * String $userId to get preferences 
    * Array $costum to find specific user preferences
    * Return costum array with or without user settings about this costum 
    */
    public static function checkUserPreferences($costum, $userId, $slug){
        $userPref=Preference::getPreferencesByTypeId($userId, Person::COLLECTION);
        if(!empty($userPref) && isset($userPref["costum"]) && isset($userPref["costum"][$slug])){
            $costum["userPreferences"]=$userPref["costum"][$slug];
        }
        return $costum;
    }
    public static function checkUserAndReference(){
        if(isset(Yii::app()->session["userId"])){
            $cacheCostum = Costum::getCostumCache();
            $getPerson=Element::getElementById(Yii::app()->session["userId"], Person::COLLECTION, null , array("source", "reference"));
            $hasCostumK=false;
            if(!empty($getPerson["source"])){
                if(isset($getPerson["source.key"]) && $getPerson["source.key"]== $cacheCostum["slug"])
                    $hasCostumK=true;
                if(isset($getPerson["source.keys"]) && in_array( $cacheCostum["slug"], $getPerson["source.keys"]))
                    $hasCostumK=true; 
            }
            if(!empty($getPerson["reference"]) && !empty($getPerson["reference"]["costum"]) && in_array( $cacheCostum["slug"], $getPerson["reference"]["costum"]))
                $hasCostumK=true;
            if(!$hasCostumK){
                if(empty($getPerson["reference"]))
                    $set=array("costum"=>[ $cacheCostum["slug"]]);
                else if(!isset($getPerson["reference"]["costum"]))
                    $set=$getPerson["reference"]["costum"]=[$cacheCostum["slug"]];
                else if(!empty($getPerson["reference"]["costum"]))
                    $set=array_push($getPerson["reference"]["costum"],  $cacheCostum["slug"]);
                PHDB::update(Person::COLLECTION, 
                    array("_id"=> new MongoId(Yii::app()->session["userId"])), 
                    array('$set'=>array("reference"=>$set))
                );
            }
        }
    }
    /*
    *   @initMetaData will initialize variable to get set metaDatas in mainSearch.php
    *   Meta is composed of title, description, keywords, image and author
    */
    public static function initMetaData($c){

        if(isset($c["metaDesc"]))
            $shortDesc=$c["metaDesc"];
        else{
            $shortDesc =  @$c["shortDescription"] ? $c["shortDescription"] : "";
                if($shortDesc=="")
                    $shortDesc = @$c["description"] ? $c["description"] : "";
        }
        $this->getController()->module->description = $shortDesc;
        $this->getController()->module->pageTitle = (isset($c["metaTitle"])) ? $c["metaTitle"] : @$c["title"];
        if(isset($c["metaKeywords"]))
            $this->getController()->module->keywords = $c["metaKeywords"];
        else
           $this->getController()->module->keywords = (@$c["tags"]) ? implode(",", @$c["tags"]) : "";
        if (isset($c["favicon"])) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["favicon"], '#') ){
                $pieces = explode("#", $c["favicon"]);
                $mod = $pieces[0];
                $c["favicon"] = $pieces[1];
            }
            $this->getController()->module->favicon = Yii::app()->getModule( $mod )->getAssetsUrl().$c["favicon"];
        }
        if (@$c["metaImg"]) {
            $mod = $this->getController()->module->id;
            //ex images can be given 
            if( substr_count($c["metaImg"], '#') ){
                $pieces = explode("#", $c["metaImg"]);
                $mod = $pieces[0];
                $c["metaImg"] = $pieces[1];
            }
            $this->getController()->module->image = Yii::app()->getModule( $mod )->getAssetsUrl().$c["metaImg"];
        }
        else if(@$c["logo"]){
            $this->getController()->module->image = $c["logo"];
        }
        //if(@)

    }
    /*
    *   @initScopeSelector will init geographical object of selection and user choice in costum
    *   return costum with entry necessary :
    *   Scopeseclector with entiere specifity of scope
    *   UserPreferences refering to its costum geographical experience
    */
    public static function initScopeSelector($params){
        $params["scopeSelector"]=Zone::getScopeByIds($params["scopeSelector"])["scopes"];
        /*foreach(["cities", "cp", "zones"] as $impact){
            if(isset($params["userPreferences"]) && isset($params["userPreferences"][$impact])){
                foreach($params["userPreferences"][$impact] as $key){
                    $valueScope=$params["scopeSelector"][$key];
                    $idScope=$params["scopeSelector"][$key]["id"];
                }
                //$params["filters"]=(!isset($params["filters"])) ? [] : $params["filters"];
                //$params["filters"]["scopes"][$impact]=$idScope;         
            }
        }*/
        return $params;
    }

    public static function sameFunction($function, $params=null){
        if(self::isSameFunction($function)) {
            $cacheCostum = Costum::getCostumCache();
           // var_dump("<pre>",$cacheCostum["assetsSlug"],"</pre>");exit;
            $slugContext = isset($cacheCostum["assetsSlug"]) ? $cacheCostum["assetsSlug"] : $cacheCostum["slug"];
            $slugContext = ucfirst($slugContext);
            
            //var_dump($function);exit;
            if(class_exists($slugContext)){
                $params = $slugContext::$function($params);
            }
            
        }
        return $params;
    }

    public static function isSameFunction($function){
        $res = false ;
        $cacheCostum = Costum::getCostumCache();
        if(!empty($cacheCostum["slug"]) && 
            !empty($cacheCostum["class"]) && 
            !empty($cacheCostum["class"]["function"]) && 
            in_array($function, $cacheCostum["class"]["function"])) {
            $res = true;
        }
        return $res;
    }

    public static function prepData($params){
        $cacheCostum = Costum::getCostumCache();
        if(!empty($cacheCostum) ) {

            if( !empty($params["preferences"]) && !empty($params["preferences"]["toBeValidated"]) ) {
                $params["preferences"]["toBeValidated"] = array($cacheCostum["slug"] => true);
            }


        }

        // if(!empty(Yii::app()->session["costum"]["slug"]) && 
        //     !empty(Yii::app()->session["costum"]["class"]) && 
        //     !empty(Yii::app()->session["costum"]["class"]["function"]) && 
        //     in_array("prepData", Yii::app()->session["costum"]["class"]["function"])) {
        //     $slug = ucfirst(Yii::app()->session["costum"]["slug"]) ;
        //     $params = $slug::prepData($params);
        // }
        if(self::isSameFunction("prepData"))
            $params = self::sameFunction("prepData", $params);
        return $params;
    }

    public static function value($value){
        $cacheCostum = Costum::getCostumCache();
        if( substr_count($value , 'this.') > 0 ){
            $field = explode( ".", $value );
            if( isset( $cacheCostum[ $field[1] ] ) )
                return $cacheCostum[ $field[1] ];
        }
        else 
            return $cacheCostum[ $value ];
    }


    public static function getRedirect(){
        $url = null ;
        $cacheCostum = Costum::getCostumCache();
        if(isset($cacheCostum) && isset($cacheCostum["slug"]) ){
           // $slug=explode(".", $_GET["el"])[1];
            $redirect = PHDB::findOne( Costum::COLLECTION , array("slug"=> $cacheCostum["slug"]));
            $url = (isset($redirect["host"])) ? "https://".$redirect["host"] : Yii::app()->createUrl("/costum/co/index/slug/".$redirect["slug"]);
            // var_dump($url);//Yii::app()->createUrl("/costum/co/index/id/".$redirect["slug"]));
            // exit;
        }
        return $url;
    }


    public static function getSource($slug){
        $s =  array(
            "key" => $slug,
            "keys" => array($slug),
            "insertOrign" => "costum",
            "date" => new MongoDate(time())
        );
        return $s;
    }

    public static function getAllFonts($costum=null){
        $cacheFonts = CacheHelper::get("costumFonts");
        if($costum == null){
            return null;
        }
        if($cacheFonts === false){
            $fonts = [];
            $fontsParsed = [];
            $fontsKeyValue = [];
            //get costum font directory
            $dirs = array_filter(glob('../../modules/costum/assets/font/*'), 'is_dir');
            if(isset($costum["contextType"]) && isset($costum["contextId"])){
            $fontUpload = "../../pixelhumain/ph/upload/communecter/".$costum["contextType"]."/".$costum["contextId"]."/file";
            $dirs[] = $fontUpload;
            }
            $dirs[] = "../../modules/costum/assets/font/blockcms/fontpack";
            // get font otf or ttf on each directory
            foreach ($dirs as $kdir => $vdir) {
                $fonts = array_merge($fonts,glob($vdir."/*.{ttf,otf}", GLOB_BRACE));
            }
            //parse font to assetsUrl directory
            $fontsParsed = array_map(
                function($str) {
                    if(strpos($str, "../../modules/costum/assets") !== false){
                        return str_replace('../../modules/costum/assets', '', $str);
                    }elseif (strpos($str, "../../pixelhumain/ph") !== false) {
                        return str_replace('../../pixelhumain/ph', Yii::app()->getRequest()->getBaseUrl(), $str);
                    }
                },
                $fonts
            );
            //convert to array assoc
            foreach ($fontsParsed as $fk => $fv) {
                $explode = explode('.', basename($fv));
                if(end($explode) == "ttf")
                    $fontsKeyValue[(string) $fv] = basename($fv,'.ttf');
                if(end($explode) == "otf")
                    $fontsKeyValue[(string) $fv] = basename($fv,'.otf');
            }
            asort($fontsKeyValue);
            CacheHelper::set("costumFonts", $fontsKeyValue);
            return CacheHelper::get("costumFonts");
        }else{
            return CacheHelper::get("costumFonts");
        }
    }

    public static function getSlugByHost($host){
        $costum = PHDB::findOne(Costum::COLLECTION, ["host" => $host]);
        if($costum)
            return $costum["slug"];
            
        $organization = PHDB::findOne(Organization::COLLECTION, ["costum.host" => $host]);
        if($organization)
            return $organization["slug"];

        $project = PHDB::findOne(Project::COLLECTION, ["costum.host" => $host]);
        if($project)
            return $project["slug"];

        $event = PHDB::findOne(Event::COLLECTION, ["costum.host" => $host]);
        if($event)
            return $event["slug"];


        $slug = PHDB::findOne( Slug::COLLECTION , array("host"=> $host));
        if($slug["name"])
            return $slug["name"];
            
        return null;
    }

    
    public static function getPageParams($costum, $page){
        if(isset($costum["type"]) && $costum["type"] !=="aap" && !isset($costum["app"]["#".$page]))
            $page="welcome";

        $params = [
            "slug"=>$costum["slug"],
            "page" => $page,
            "tplUsingId"     => "",
            "tplKey"   => "",
            "tplInitImage"   => "",
            "nbrTplUser"     => 0,
            "nbrTplViewer"   => 0,
            "paramsData"     => [],
            "insideTplInUse" => array(),
            "cmsList"        => array(),
            "newCmsId"       => array(),
            "costum"         => $costum,
            "cmsInUseId"     => array()
        ];

        $params["cmsList"] = [];
        if($page !== "")
            $params["cmsList"] = Cms::getCmsWithChildren($costum["contextId"], $page, array("position" => 1));

            // $footerPage = Cms::getCmsFooter($costum["contextId"]);

            // $params["cmsList"] = array_merge($params["cmsList"], $footerPage);
        
        // if (empty($params["cmsList"])) {
        //     $underConstCondition=array(
        //         "subtype" => "underConstruction"
        //     );
        //     $underConstruction = PHDB::findOne("cms",
        //           $underConstCondition,
        //          array("position" => 1)
        //     );
        //     if(!empty($underConstruction)){
        //       $data = array(
        //           "parentId"   => $costum["contextId"], 
        //           "parentType" => $costum["contextType"],
        //           "parentSlug" => $costum['contextSlug'],
        //           "page"       => $page
        //       );
        //       $sample = Cms::duplicateBlock(array((string)$underConstruction["_id"]), "", $data);
        //       $params["cmsList"] = array((string)$sample[0]["_id"] => PHDB::findOneById(CMS::COLLECTION, (string)$sample[0]["_id"]));
        //     }
        // }

        return $params;
    }

    public static function getCostumMsgNotAuthorized($page){
        /* Vérification autorisation page dans un costum */
        $costum = CacheHelper::getCostum();

        $pageDetail = !empty($costum["app"]["#".$page]) ? $costum["app"]["#".$page] : [];
        if($costum == false || Menu::showButton($pageDetail) )
            return "";
        $msgNotAuthorized = "";
        if(!isset($costum["msgNotAuthorized"])){
            $msgNotAuthorized = '<div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">	
                                    <i class="fa fa-lock fa-4x "></i><br/>
                                    <h1 class="">'.Yii::t("organization", "Unauthorized Access.").'</h1>
                                </div>';
        }else{
            $msgNotAuthorized = $costum["msgNotAuthorized"];
        }
        return $msgNotAuthorized;
    }
}
?>