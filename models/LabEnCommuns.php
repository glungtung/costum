<?php

class LabEnCommuns {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
    public static $category=array(
        "common"=>"Commun",
        "research"=>"Recherche-action"
    );
    
    public static  $avancement=array(
        "idea"=>"Idée",
        "development"=>"En cours"
    );
	

    public static function updateBlock($params){

        $res = array();
        if($params["block"] == "info"){

                if(isset($params["category"]) && $params["typeElement"] == Project::COLLECTION){
                    $res[] = Element::updateField($params["typeElement"], $params["id"], "category", $params["category"]);
                    if(isset($params["tags"])){
                        foreach(self::$category as $key=>$cat){
                                if(array_search($cat,$params["tags"])!==false){
                                    $index=array_search($cat,$params["tags"]);
                                    unset($params["tags"][$index]);
                                }
                        }

                    }
                    $params["tags"]=(isset($params["tags"]) && !empty($params["tags"])) ? $params["tags"] : [];
                    array_push($params["tags"],self::$category[$params["category"]]);


                    if($params["category"]=="common" && !empty($params["avancement"])){
                        $params["avancement"]="";
                    }
                    
                }

                if(isset($params["avancement"]) && $params["typeElement"] == Project::COLLECTION){

                    $res[] = Element::updateField($params["typeElement"], $params["id"], "avancement", $params["avancement"]);
                    if(isset($params["tags"])){
                            foreach(self::$avancement as $key=>$ste){
                                if(array_search($ste,$params["tags"])!=false){
                                    $index=array_search($ste,$params["tags"]);
                                    unset($params["tags"][$index]);
                                }
                            }
                    }
                    $params["tags"]=(isset($params["tags"]) && !empty($params["tags"])) ? $params["tags"] : array();
                    if(!empty($params["avancement"])){
                        array_push($params["tags"],self::$avancement[$params["avancement"]]); 
                    }
                }

                $params["tags"]=array_unique($params["tags"]);                

                if(isset($params["tags"]) && $params["typeElement"] == Project::COLLECTION) {

                    $res[] = Element::updateField($params["typeElement"], $params["id"], "tags", $params["tags"]);
                }

        }
        
        return $res;
    }
    
}

?>