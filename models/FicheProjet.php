<?php 
/**
 * 
 */
class FicheProjet 
{
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	public static function getAllAnswer($post){
		$listAnswer = Array(); 
		$form = PHDB::findOne(Form::COLLECTION, array('id' => $post["formId"],"name" => "Fiche projet"));
		$answer= PHDB::find(Form::ANSWER_COLLECTION,array('user' =>Yii::app()->session["userId"], "form" =>(string)$form["_id"] ));
		//var_dump($answer);
		foreach ($answer as $key => $value) {
			$listAnswer[] = $value;
		}
		return($listAnswer);
	}
	public static function getAnswer($post){
		$allAnswer = PHDB::find(Form::ANSWER_COLLECTION);
		$answer = $allAnswer[$post["idFiche"]]["answers"];
		return ($answer);
	}

	public static function getFile($post){
		$listFile = Array();
		$file = Document::getListDocumentsWhere(array(
			"id"=>(string)$post["idFiche"], 
			"type"=>'answers',
			"subKey" => $post["subKey"]), "file");
		foreach ($file as $key => $value) {
			$listFile[] = $value;
		}
		return($listFile);
	}
}
?>