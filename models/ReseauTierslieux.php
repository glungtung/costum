<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;

class ReseauTierslieux {

	public static function elementAfterSave($data){
		

		// var_dump($data);exit;
    	
  
		
    	if($data["collection"]==Organization::COLLECTION){ 
            // add source FTL
			$sources=["franceTierslieux","tiersLieux"];
			foreach ($sources as $i => $source){
	    		Admin::addSourceInElement($data["id"],$data["collection"],$source,"reference");
	        }

	    	$costum = CacheHelper::getCostum();
	    	
	    	$isInviting=false;
	    	$child=[];
	    	$child["childId"]=$data["id"];
        	$child["childType"]=$data["collection"];
			// var_dump($controller->costum["contextId"]);exit;
	        
			// Link third place to parent costum (network)
			Link::connectParentToChild($costum["contextId"], $costum["contextType"], $child, false, Yii::app()->session["userId"]);
	        		// $net=Element::getElementById($costum["contextId"],Organization::COLLECTION);
	        		// if($net){

		        	// 	Link::connect($net["_id"],$net["collection"],$data["id"],$data["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        	// 	Link::connect($data["id"],$data["collection"],$net["_id"],$net["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        	// }

			// size of third place from absolute to filter criterias in tags
			$size="";
			if(intval($data["params"]["buildingSurfaceArea"])<60){
				$size="Moins de 60m²";
			}else if(intval($data["params"]["buildingSurfaceArea"])<=200){
				$size="Entre 60 et 200m²";
			}else{
				$size="Plus de 200m²";
			}

			PHDB::update( Organization::COLLECTION, 
					  array("_id"=>$data["params"]["_id"]),
					array('$push' => array("tags"=> $size )));

	    }       
	       
    }

    public static function elementAfterUpdate($params){
		$user=Person::getById(Yii::app()->session['userId']);
		$userOrgaAdmin=(isset($user["links"]) && isset($user["links"]["memberOf"]) && isset($user["links"]["memberOf"][$params["id"]]) && isset($user["links"]["memberOf"][$params["id"]]["isAdmin"])) ? true : false;
		if($params["collection"]==Organization::COLLECTION && !$userOrgaAdmin){
		    $isAdmin=true;
		    Link::connect((string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "members", $isAdmin);
            Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf", $isAdmin);
        }
	}

	public static function prepDataForUpdate($data){
		// var_dump($data["images"]);
		$typeEl=(in_array($data["collection"], [Event::CONTROLLER, Project::CONTROLLER, Organization::CONTROLLER])) ? Element::getCollectionByControler($data["collection"]) : $data["collection"]; 
		  		$where=array(
		  			"id"=>(string)$data["_id"], "type"=>$typeEl, "doctype"=>"image", 
		  			"contentKey"=>"slider"
		  		);
		  		$data["photo"] = Document::getListDocumentsWhere($where, "image");

		// var_dump($data["images"]);exit; 		

		return $data;  		

		  		
	}

    public static function getNetwork($costumSlug){
		//var_dump($costumSlug);
		$network=PHDB::find(Organization::COLLECTION,array("category" => "network"));
		return $network;
	}

}
